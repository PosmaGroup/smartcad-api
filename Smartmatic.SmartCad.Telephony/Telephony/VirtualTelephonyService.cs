using System;
using System.Collections.Generic;
using System.Text;
using System.ServiceModel;
using SmartCadCore.Core;
using Smartmatic.SmartCad.Telephony;

using System.ServiceModel.Channels;
using SmartCadCore.ClientData;
using SmartCadCore.Common;

namespace Smartmatic.SmartCad.Service
{
#if DEBUG
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession, ConcurrencyMode = ConcurrencyMode.Multiple, IncludeExceptionDetailInFaults = true)]
#else
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession, ConcurrencyMode = ConcurrencyMode.Multiple)]
#endif
    public class VirtualTelephonyService : ITelephonyService, IDisposable
    {
        private ITelephonyServiceCallback callback;

        public VirtualTelephonyService()
        {
            callback = OperationContext.Current.GetCallbackChannel<ITelephonyServiceCallback>();
        }

        #region IDisposable

        private bool disposed = false;

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    // Dispose managed resources.
                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                disposed = true;
            }
        }

        ~VirtualTelephonyService()
        {
            Dispose(false);
        }

        #endregion

        #region Host

        public static ServiceHost Host()
        {
            String address = ApplicationUtil.BuildServiceUrl(
                "net.tcp",
                "localhost",
                SmartCadConfiguration.TELEPHONY_SERVICE_PORT,
                SmartCadConfiguration.TELEPHONY_SERVICE_ADDRESS);

            Binding binding = BindingBuilder.GetNetTcpBinding();

            ServiceHost host = ServiceHostBuilder.ConfigureHost(typeof(VirtualTelephonyService), typeof(ITelephonyService), binding, address);

            return host;
        }

        #endregion

        #region ITelephonyService Members

        public void SetConfiguration(ConfigurationClientData ConfigurationProperties)
        {
            
        }

        public void Login(string login, string password, string agentId, string agentPassword, bool recordCalls, string extNumber, bool isReady)
        {
            try
            {
                SmartLogger.Print(ResourceLoader.GetString2("Login"));
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        public void Answer()
        {
            try
            {
                SmartLogger.Print(ResourceLoader.GetString2("Answer"));
                telephonyManager_EstablishedCall(null, new CallEstablishedEventArgs("", "", "", TimeSpan.Zero));
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        public void Disconnect()
        {
            try
            {
                SmartLogger.Print(ResourceLoader.GetString2("Disconnect"));
                telephonyManager_EndingCall(null, new CallReleasedEventArgs("", "", "", TimeSpan.Zero));
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        public void SetIsReady(bool isReady)
        {
            try
            {
                SmartLogger.Print(ResourceLoader.GetString2("SetIsReady"));
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        public void Logout()
        {
            try
            {
                SmartLogger.Print(ResourceLoader.GetString2("Logout"));
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        public void Ping()
        {
        }

        public void GenerateCall(string number)
        {
            CallRingingEventArgs args = new CallRingingEventArgs(number != null ? number : "555555", "", "", TimeSpan.Zero);
            telephonyManager_IncomingCall(null, args);
        }

        public bool IsVirtual()
        {
            return true;
        }

        #endregion

        private void HandleException(Exception ex)
        {
            SmartLogger.Print(ex);

            throw ApplicationUtil.NewFaultException(ex);
        }

        private void telephonyManager_IncomingCall(object sender, CallRingingEventArgs args)
        {
            try
            {
                this.callback.OnTelephonyAction(args);
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private void telephonyManager_EndingCall(object sender, CallReleasedEventArgs e)
        {
            try
            {
                this.callback.OnTelephonyAction(e);
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private void telephonyManager_EstablishedCall(object sender, CallEstablishedEventArgs e)
        {
            try
            {
                this.callback.OnTelephonyAction(e);
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }


        #region ITelephonyService Members


        public void SetVirtualCall(bool virtualCall)
        {
            
        }

        #endregion

        #region ITelephonyService Members


        public bool InVirtualCall()
        {
            return true;
        }

        public void ParkCall(string extensionParking, string parkId)
        {
         
        }

        #endregion        
    

        public void RecoverConnection()
        {
        }


        public void DivertCall()
        {
        }


        public void SetPhoneReportCustomCode(string customCode)
        {
            
        }
    }
}
