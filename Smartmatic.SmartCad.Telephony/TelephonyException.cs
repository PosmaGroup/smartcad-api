using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    public class TelephonyException : ApplicationException
    {
        public TelephonyException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
