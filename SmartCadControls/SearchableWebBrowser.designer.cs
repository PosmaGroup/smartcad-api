﻿using SmartCadControls.Controls;

namespace SmartCadControls.Controls
{
    partial class SearchableWebBrowser
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl = new DevExpress.XtraLayout.LayoutControl();
            this.btnNext = new DevExpress.XtraEditors.SimpleButton();
            this.edtSearch = new DevExpress.XtraEditors.TextEdit();
            this.btnPrev = new DevExpress.XtraEditors.SimpleButton();
            this.webBrowserEx = new WebBrowserEx();
            this.layoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciSearchTxt = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciWebBrowser = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciPrev = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciNext = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).BeginInit();
            this.layoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edtSearch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciSearchTxt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciWebBrowser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciPrev)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciNext)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl
            // 
            this.layoutControl.Controls.Add(this.btnNext);
            this.layoutControl.Controls.Add(this.edtSearch);
            this.layoutControl.Controls.Add(this.btnPrev);
            this.layoutControl.Controls.Add(this.webBrowserEx);
            this.layoutControl.Location = new System.Drawing.Point(0, 0);
            this.layoutControl.Margin = new System.Windows.Forms.Padding(0);
            this.layoutControl.Name = "layoutControl";
            this.layoutControl.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(433, 448, 250, 350);
            this.layoutControl.Root = this.layoutControlGroup;
            this.layoutControl.Size = new System.Drawing.Size(212, 181);
            this.layoutControl.TabIndex = 0;
            this.layoutControl.Text = "layoutControl1";
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(144, 1);
            this.btnNext.MinimumSize = new System.Drawing.Size(50, 15);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(50, 23);
            this.btnNext.StyleController = this.layoutControl;
            this.btnNext.TabIndex = 6;
            this.btnNext.Text = ">>";
            this.btnNext.Click += new System.EventHandler(this.search_event);
            // 
            // edtSearch
            // 
            this.edtSearch.EditValue = "";
            this.edtSearch.Location = new System.Drawing.Point(1, 1);
            this.edtSearch.Margin = new System.Windows.Forms.Padding(0);
            this.edtSearch.MinimumSize = new System.Drawing.Size(0, 15);
            this.edtSearch.Name = "edtSearch";
            this.edtSearch.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edtSearch.Properties.Appearance.Options.UseFont = true;
            this.edtSearch.Size = new System.Drawing.Size(93, 22);
            this.edtSearch.StyleController = this.layoutControl;
            this.edtSearch.TabIndex = 7;
            this.edtSearch.EditValueChanged += new System.EventHandler(this.edtSearch_EditValueChanged);
            this.edtSearch.TextChanged += new System.EventHandler(this.search_event);
            this.edtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.edtSearch_KeyDown);
            // 
            // btnPrev
            // 
            this.btnPrev.Location = new System.Drawing.Point(94, 1);
            this.btnPrev.MinimumSize = new System.Drawing.Size(50, 15);
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Size = new System.Drawing.Size(50, 23);
            this.btnPrev.StyleController = this.layoutControl;
            this.btnPrev.TabIndex = 5;
            this.btnPrev.Text = "<<";
            this.btnPrev.Click += new System.EventHandler(this.search_event);
            // 
            // webBrowserEx
            // 
            this.webBrowserEx.Location = new System.Drawing.Point(2, 25);
            this.webBrowserEx.Margin = new System.Windows.Forms.Padding(0);
            this.webBrowserEx.MinimumSize = new System.Drawing.Size(0, 20);
            this.webBrowserEx.Name = "webBrowserEx";
            this.webBrowserEx.ScriptErrorsSuppressed = true;
            this.webBrowserEx.Size = new System.Drawing.Size(208, 154);
            this.webBrowserEx.TabIndex = 8;
            this.webBrowserEx.XmlText = "<HTML></HTML>\0";
            // 
            // layoutControlGroup
            // 
            this.layoutControlGroup.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup.GroupBordersVisible = false;
            this.layoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciSearchTxt,
            this.lciWebBrowser,
            this.lciPrev,
            this.lciNext,
            this.emptySpaceItem1});
            this.layoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup.Name = "Root";
            this.layoutControlGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlGroup.Size = new System.Drawing.Size(212, 181);
            this.layoutControlGroup.Text = "Root";
            this.layoutControlGroup.TextVisible = false;
            // 
            // lciSearchTxt
            // 
            this.lciSearchTxt.Control = this.edtSearch;
            this.lciSearchTxt.CustomizationFormText = "layoutControlItem4";
            this.lciSearchTxt.Location = new System.Drawing.Point(0, 0);
            this.lciSearchTxt.MaxSize = new System.Drawing.Size(0, 23);
            this.lciSearchTxt.MinSize = new System.Drawing.Size(93, 23);
            this.lciSearchTxt.Name = "lciSearchTxt";
            this.lciSearchTxt.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lciSearchTxt.Size = new System.Drawing.Size(93, 23);
            this.lciSearchTxt.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciSearchTxt.Text = "lciSearchTxt";
            this.lciSearchTxt.TextSize = new System.Drawing.Size(0, 0);
            this.lciSearchTxt.TextToControlDistance = 0;
            this.lciSearchTxt.TextVisible = false;
            // 
            // lciWebBrowser
            // 
            this.lciWebBrowser.Control = this.webBrowserEx;
            this.lciWebBrowser.CustomizationFormText = "lciWebBrowser";
            this.lciWebBrowser.Location = new System.Drawing.Point(0, 23);
            this.lciWebBrowser.Name = "lciWebBrowser";
            this.lciWebBrowser.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.lciWebBrowser.Size = new System.Drawing.Size(210, 156);
            this.lciWebBrowser.Text = "lciWebBrowser";
            this.lciWebBrowser.TextSize = new System.Drawing.Size(0, 0);
            this.lciWebBrowser.TextToControlDistance = 0;
            this.lciWebBrowser.TextVisible = false;
            // 
            // lciPrev
            // 
            this.lciPrev.Control = this.btnPrev;
            this.lciPrev.CustomizationFormText = "layoutControlItem2";
            this.lciPrev.Location = new System.Drawing.Point(93, 0);
            this.lciPrev.MaxSize = new System.Drawing.Size(50, 23);
            this.lciPrev.MinSize = new System.Drawing.Size(50, 23);
            this.lciPrev.Name = "lciPrev";
            this.lciPrev.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lciPrev.Size = new System.Drawing.Size(50, 23);
            this.lciPrev.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciPrev.Text = "lciPrev";
            this.lciPrev.TextSize = new System.Drawing.Size(0, 0);
            this.lciPrev.TextToControlDistance = 0;
            this.lciPrev.TextVisible = false;
            // 
            // lciNext
            // 
            this.lciNext.Control = this.btnNext;
            this.lciNext.CustomizationFormText = "layoutControlItem3";
            this.lciNext.Location = new System.Drawing.Point(143, 0);
            this.lciNext.MaxSize = new System.Drawing.Size(50, 23);
            this.lciNext.MinSize = new System.Drawing.Size(50, 23);
            this.lciNext.Name = "lciNext";
            this.lciNext.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lciNext.Size = new System.Drawing.Size(50, 23);
            this.lciNext.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciNext.Text = "lciNext";
            this.lciNext.TextSize = new System.Drawing.Size(0, 0);
            this.lciNext.TextToControlDistance = 0;
            this.lciNext.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(193, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(17, 23);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(17, 23);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.emptySpaceItem1.Size = new System.Drawing.Size(17, 23);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // SearchableWebBrowser
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.AutoSize = true;
            this.Controls.Add(this.layoutControl);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "SearchableWebBrowser";
            this.Size = new System.Drawing.Size(212, 181);
            this.Resize += new System.EventHandler(this.SearchableWebBrowser_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).EndInit();
            this.layoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.edtSearch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciSearchTxt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciWebBrowser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciPrev)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciNext)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlItem lciPrev;
        private DevExpress.XtraLayout.LayoutControlItem lciNext;
        private DevExpress.XtraLayout.LayoutControlItem lciSearchTxt;
        private DevExpress.XtraLayout.LayoutControlItem lciWebBrowser;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        internal DevExpress.XtraEditors.TextEdit edtSearch;
        internal DevExpress.XtraEditors.SimpleButton btnNext;
        internal DevExpress.XtraEditors.SimpleButton btnPrev;
        private WebBrowserEx webBrowserEx;
    }
}
