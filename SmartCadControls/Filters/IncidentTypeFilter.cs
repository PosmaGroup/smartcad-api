using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraNavBar;
using SmartCadCore.Common;
using System.Collections;
using SmartCadCore.Core;
using SmartCadCore.ClientData;
using Smartmatic.SmartCad.Service;

//using Smartmatic.SmartCad.Service;

namespace SmartCadControls.Controls
{
    public partial class IncidentTypeFilter : SearchFilter
    {
        public IncidentTypeFilter(UserApplicationClientData app)
        {
            InitializeComponent();

            Title = ResourceLoader.GetString2("IncidentTypes");
            Image = ResourceLoader.GetImage("IconIncidentType");
            gridColumnFriendlyName.Caption = ResourceLoader.GetString2("IncidentType");
            gridColumnSelect.Caption = ResourceLoader.GetString2("Select");
            checkEditSelectAll.Text = "  " + ResourceLoader.GetString2("SelectAll");

            IList incidentTypes = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(
                SmartCadHqls.IncidentTypesForApp, app.Code));
            List<GridControDataIncType> source = new List<GridControDataIncType>();
            foreach (IncidentTypeClientData type in incidentTypes)
            {
                source.Add(new GridControDataIncType(type));
            }
            gridControlIncTypes.BeginUpdate();
            gridControlIncTypes.DataSource = source;
            gridControlIncTypes.EndUpdate();
        }

        #region SearchFilter Members


        public override Dictionary<string, List<object>> SelectedOptions
        {
            get
            { 
                Dictionary<string, List<object>> results = new Dictionary<string, List<object>>();
                List<object> objs = new List<object>();
                foreach (GridControDataIncType gridData in gridControlIncTypes.DataSource as List<GridControDataIncType>)
                {
                    if(gridData.Selected)
                        objs.Add(gridData.Name);
                }
                results.Add("IncidentType", objs);

                return results;
            }
        }

        #endregion

        private void checkEditSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            gridControlIncTypes.BeginUpdate();
            foreach (GridControDataIncType data in gridControlIncTypes.DataSource as List<GridControDataIncType>)
            {
                data.Selected = checkEditSelectAll.Checked;
            }
            gridControlIncTypes.EndUpdate();

            if (!checkEditSelectAll.Checked)
            {
                checkEditSelectAll.Text = "  " + ResourceLoader.GetString2("SelectAll");
            }
            else
            {
                checkEditSelectAll.Text = "  " + ResourceLoader.GetString2("UnSelectAll");
            }
        }
    }

    public class GridControDataIncType : GridControlData
    {
        public string FriendlyName { get; set; }
        public string Name { get; set; }
        public bool Selected { get; set; }

        public GridControDataIncType(IncidentTypeClientData incType)
            : base(incType)
        {
            Name = incType.Name;
            FriendlyName = incType.FriendlyName;
        }


        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            return ((GridControDataIncType)obj).Name == Name;
        }
    }
}
