﻿using SmartCadCore.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadControls.Filters
{
    public class GridResultsData
    {
        public string Name { get; set; }
        public int Count { get; set; }
        public List<GeoPoint> Points { get; set; }

        public GridResultsData(string name, List<GeoPoint> points)
        {
            this.Name = name;
            this.Points = points;
            if (points != null) this.Count = points.Count;
        }

        public override bool Equals(object obj)
        {
            if (obj is GridResultsData == false)
                return false;
            GridResultsData data = obj as GridResultsData;
            if (data.Name == Name)
                return true;
            return false;
        }
    }
}
