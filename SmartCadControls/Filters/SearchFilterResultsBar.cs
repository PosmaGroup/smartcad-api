using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraNavBar;
using System.Linq;
using SmartCadCore.Common;
using SmartCadCore.Core;

namespace SmartCadControls.Controls
{
    public partial class SearchFilterResultsBar: DevExpress.XtraEditors.XtraUserControl
    {
        public event FilterRequestEventHandler FilterRequest;
        public delegate void FilterRequestEventHandler(List<GeoPoint> points);
        public static Dictionary<string, List<GeoPoint>> Results { get; set; }

        public SearchFilterResultsBar()
        {
            InitializeComponent();
            LoadLanguage();
        }

        private void LoadLanguage()
        {
            gridColumnCount.Caption = ResourceLoader.GetString2("QuantityUnit");
            gridColumnIncidentType.Caption = ResourceLoader.GetString2("IncidentType");
        }

        public void ShowResults(Dictionary<string,List<GeoPoint>> results, List<GeoPoint> allPoints)
        {
            List<GridResultsData> list = new List<GridResultsData>();
            foreach (string key in results.Keys)
            {
                list.Add(new GridResultsData(key, results[key]));
            }
            list.Add(new GridResultsData(ResourceLoader.GetString2("All"), allPoints));
            
            gridControlExResults.BeginUpdate();
            gridControlExResults.DataSource = list;
            gridControlExResults.EndUpdate();

            gridViewExPreviousIncidents.ViewTotalRows = false;
            gridViewExPreviousIncidents.OptionsView.ShowFooter = false;
            
            Results = results;
        }

        private void buttonFilter_Click(object sender, EventArgs e)
        {
            FormUtil.InvokeRequired(this, delegate
            {
                if (gridControlExResults.SelectedItems.Count > 0)
                {
                    if (FilterRequest != null)
                        FilterRequest(((GridResultsData)gridControlExResults.SelectedItems[0]).Points);
                }
            });
           
        }

        private void gridControlExResults_MouseMove(object sender, MouseEventArgs e)
        {
            if (gridViewExPreviousIncidents.CalcHitInfo(e.X, e.Y).RowHandle > -1)
            {
                gridControlExResults.Cursor = System.Windows.Forms.Cursors.Hand;
            }
            else
            {
                gridControlExResults.Cursor = System.Windows.Forms.Cursors.Default;
            }
        }
    }

    public class GridResultsData
    {
        public string Name { get; set; }
        public int Count { get; set; }
        public List<GeoPoint> Points { get; set; }

        public GridResultsData(string name, List<GeoPoint> points)
        {
            this.Name = name;
            this.Points = points;
            if(points!=null) this.Count = points.Count;
        }

        public override bool Equals(object obj)
        {
            if (obj is GridResultsData == false)
                return false;
            GridResultsData data = obj as GridResultsData;
            if (data.Name == Name)
                return true;
            return false;
        }
    }
}
