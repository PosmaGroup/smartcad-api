using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace SmartCadControls.Controls
{
    public class SearchFilter : DevExpress.XtraEditors.XtraUserControl
    {
        public string Title { get; set; }
        public Image Image { get; set; }
        public virtual Dictionary<string, List<object>> SelectedOptions { get { return new Dictionary<string, List<object>>(); } }

        public SearchFilter()
        {
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // SearchFilter
            // 
            this.Name = "SearchFilter";
            this.ResumeLayout(false);

        }
    }
}
