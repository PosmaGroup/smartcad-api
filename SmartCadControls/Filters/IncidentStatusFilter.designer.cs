namespace SmartCadControls.Controls
{
    partial class IncidentStatusFilter
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkEditOpen = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditClosed = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditOpen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditClosed.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // checkEditOpen
            // 
            this.checkEditOpen.EditValue = true;
            this.checkEditOpen.Location = new System.Drawing.Point(18, 9);
            this.checkEditOpen.Name = "checkEditOpen";
            this.checkEditOpen.Properties.Caption = "Open";
            this.checkEditOpen.Size = new System.Drawing.Size(86, 19);
            this.checkEditOpen.TabIndex = 0;
            this.checkEditOpen.CheckedChanged += new System.EventHandler(this.checkEditOpen_CheckedChanged);
            // 
            // checkEditClosed
            // 
            this.checkEditClosed.Location = new System.Drawing.Point(118, 9);
            this.checkEditClosed.Name = "checkEditClosed";
            this.checkEditClosed.Properties.Caption = "Closed";
            this.checkEditClosed.Size = new System.Drawing.Size(76, 19);
            this.checkEditClosed.TabIndex = 1;
            this.checkEditClosed.CheckedChanged += new System.EventHandler(this.checkEditClosed_CheckedChanged);
            // 
            // IncidentStatusFilter
            // 
            this.Controls.Add(this.checkEditClosed);
            this.Controls.Add(this.checkEditOpen);
            this.Name = "IncidentStatusFilter";
            this.Size = new System.Drawing.Size(202, 29);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditOpen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditClosed.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.CheckEdit checkEditOpen;
        private DevExpress.XtraEditors.CheckEdit checkEditClosed;
    }
}
