using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraNavBar;
using SmartCadCore.Common;

namespace SmartCadControls.Controls
{
    public partial class IncidentStatusFilter : SearchFilter
    {
        public IncidentStatusFilter()
        {
            InitializeComponent();
            Title = ResourceLoader.GetString2("IncidentsStatus");
            Image = ResourceLoader.GetImage("IconIncidentType");
            checkEditOpen.Text = ResourceLoader.GetString2("OpenIncidents");
            checkEditClosed.Text = ResourceLoader.GetString2("ClosedIncidents");
        }

        #region SearchFilter Members


        public override Dictionary<string, List<object>> SelectedOptions
        {
            get
            { 
                Dictionary<string, List<object>> results = new Dictionary<string, List<object>>();  
                if(checkEditOpen.Checked)
                    results.Add("Open",new List<object>());
                if (checkEditClosed.Checked)
                    results.Add("Closed", new List<object>());

                return results;
            }
        }

        #endregion

        private void checkEditOpen_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEditOpen.Checked == false && checkEditClosed.Checked == false)
                checkEditOpen.Checked = true;
        }

        private void checkEditClosed_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEditOpen.Checked == false && checkEditClosed.Checked == false)
                checkEditClosed.Checked = true;
        }
    }
}
