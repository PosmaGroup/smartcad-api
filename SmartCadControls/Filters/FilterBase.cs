using System;
using System.Collections;
using System.Text;
using System.Data;

using SmartCadCore.Core;

namespace SmartCadControls.Filters
{
    public abstract class FilterBase
    {
        private DataGridEx dataGrid;
        protected IList parameters;

        public DataGridEx DataGrid
        {
            get
            {
                return dataGrid;
            }
        }

        public IList Parameters
        {
            get
            {
                return parameters;
            }
            set
            {
                parameters = value;
            }
        }

        public FilterBase(DataGridEx dataGrid, IList parameters)
        {
            this.dataGrid = dataGrid;
            this.parameters = parameters;
        }

        public abstract bool Filter(object objectData);

        public abstract bool FilterByText(object objectData, string text);

        public void Filter()
        {
            Filter(true);
        }

        public void Filter(bool clearSelection)
        {
            DataTable table = DataGrid.Table;

            DataGrid.BeginChanges();

            foreach (DataRow row in table.Rows)
            {
                string key = row["Key"].ToString();

                DataGridExData gridData = DataGrid.GetData(key);

                object objectData = gridData.Tag as object;

                bool visible = Filter(objectData);

                FormUtil.InvokeRequired(DataGrid, delegate
                {
                    DataGrid.SetDataVisible(row, visible);
                });
            }

            FormUtil.InvokeRequired(DataGrid, delegate
            {
                DataGrid.EndChanges(clearSelection);
            });
        }

        public void FilterByText(string text)
        {
            FilterByText(text, true);
        }

        public void FilterByText(string text, bool clearSelection)
        {
            DataRow[] rows = DataGrid.GetVisibleRows();

            DataGrid.BeginChanges();

            foreach (DataRow row in rows)
            {
                string key = row["Key"].ToString();

                DataGridExData gridData = DataGrid.GetData(key);

                object objectData = gridData.Tag as object;

                bool visible = (string.IsNullOrEmpty(text)) ? true : FilterByText(objectData, text);

                FormUtil.InvokeRequired(DataGrid, delegate
                {
                    DataGrid.SetDataVisibleByText(row, visible);
                });
            }

            DataGrid.EndChanges(clearSelection);
        }
    }
}
