using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraNavBar;
using SmartCadCore.Common;

namespace SmartCadControls.Controls
{
    public partial class SearchFilterBar : DevExpress.XtraEditors.XtraUserControl
    {
        public event SearchRequestEventHandler SearchRequest;
        public delegate void SearchRequestEventHandler(Dictionary<string, Dictionary<string, List<object>>> filters);

        public SearchFilterBar()
        {
            InitializeComponent();
            this.buttonSearch.Text = ResourceLoader.GetString2("Search");
        }

        public void AddFilter(SearchFilter filter)
        {
            NavBarGroupControlContainer container = new NavBarGroupControlContainer();
            NavBarGroup group = new NavBarGroup(filter.Title);
            group.LargeImage = filter.Image;
            
            //First add the group to the NavBar
            navBarControl.Groups.Add(group);
            group.GroupStyle = NavBarGroupStyle.ControlContainer;
            group.ControlContainer = container;            
            container.Controls.Add(filter);

            group.GroupClientHeight = filter.Height + 10;
            group.Expanded = true;
            group.GroupCaptionUseImage = NavBarImage.Large;
            
            filter.Dock = DockStyle.Fill;
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            Dictionary<string, Dictionary<string, List<object>>> filters = new Dictionary<string, Dictionary<string, List<object>>>();

            foreach (NavBarGroup group in navBarControl.Groups)
            {
                SearchFilter filter = (SearchFilter)group.ControlContainer.Controls[0];
                filters.Add(filter.Name, filter.SelectedOptions);
            }

            if (SearchRequest != null)
                SearchRequest(filters);
        }
    }
}
