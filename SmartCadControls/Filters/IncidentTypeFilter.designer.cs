namespace SmartCadControls.Controls
{
    partial class IncidentTypeFilter
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridControlIncTypes = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnSelect = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnFriendlyName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.checkEditSelectAll = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlIncTypes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSelectAll.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // gridControlIncTypes
            // 
            this.gridControlIncTypes.Dock = System.Windows.Forms.DockStyle.Top;
            this.gridControlIncTypes.Location = new System.Drawing.Point(0, 0);
            this.gridControlIncTypes.MainView = this.gridView1;
            this.gridControlIncTypes.Name = "gridControlIncTypes";
            this.gridControlIncTypes.Size = new System.Drawing.Size(218, 370);
            this.gridControlIncTypes.TabIndex = 5;
            this.gridControlIncTypes.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnSelect,
            this.gridColumnFriendlyName});
            this.gridView1.GridControl = this.gridControlIncTypes;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsCustomization.AllowFilter = false;
            this.gridView1.OptionsMenu.EnableColumnMenu = false;
            this.gridView1.OptionsNavigation.UseTabKey = false;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowDetailButtons = false;
            this.gridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView1.OptionsView.ShowGroupExpandCollapseButtons = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.PaintStyleName = "Skin";
            // 
            // gridColumnSelect
            // 
            this.gridColumnSelect.Caption = "gridColumn1";
            this.gridColumnSelect.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumnSelect.FieldName = "Selected";
            this.gridColumnSelect.Name = "gridColumnSelect";
            this.gridColumnSelect.OptionsFilter.AllowFilter = false;
            this.gridColumnSelect.Visible = true;
            this.gridColumnSelect.VisibleIndex = 0;
            this.gridColumnSelect.Width = 27;
            // 
            // gridColumnFriendlyName
            // 
            this.gridColumnFriendlyName.Caption = "gridColumn2";
            this.gridColumnFriendlyName.FieldName = "FriendlyName";
            this.gridColumnFriendlyName.Name = "gridColumnFriendlyName";
            this.gridColumnFriendlyName.OptionsColumn.AllowEdit = false;
            this.gridColumnFriendlyName.OptionsFilter.AllowAutoFilter = false;
            this.gridColumnFriendlyName.OptionsFilter.AllowFilterModeChanging = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnFriendlyName.OptionsFilter.FilterBySortField = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnFriendlyName.OptionsFilter.ImmediateUpdateAutoFilter = false;
            this.gridColumnFriendlyName.Visible = true;
            this.gridColumnFriendlyName.VisibleIndex = 1;
            this.gridColumnFriendlyName.Width = 187;
            // 
            // checkEditSelectAll
            // 
            this.checkEditSelectAll.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.checkEditSelectAll.Location = new System.Drawing.Point(0, 386);
            this.checkEditSelectAll.Name = "checkEditSelectAll";
            this.checkEditSelectAll.Properties.Caption = "checkEdit1";
            this.checkEditSelectAll.Size = new System.Drawing.Size(218, 19);
            this.checkEditSelectAll.TabIndex = 7;
            this.checkEditSelectAll.CheckedChanged += new System.EventHandler(this.checkEditSelectAll_CheckedChanged);
            // 
            // IncidentTypeFilter
            // 
            this.Controls.Add(this.checkEditSelectAll);
            this.Controls.Add(this.gridControlIncTypes);
            this.Name = "IncidentTypeFilter";
            this.Size = new System.Drawing.Size(218, 405);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlIncTypes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSelectAll.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.GridControl gridControlIncTypes;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnSelect;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnFriendlyName;
        private DevExpress.XtraEditors.CheckEdit checkEditSelectAll;

    }
}
