﻿using SmartCadControls.Controls;
using SmartCadCore.ClientData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadControls.Filters
{
    public class GridControDataIncType : GridControlData
    {
        public string FriendlyName { get; set; }
        public string Name { get; set; }
        public bool Selected { get; set; }

        public GridControDataIncType(IncidentTypeClientData incType)
            : base(incType)
        {
            Name = incType.Name;
            FriendlyName = incType.FriendlyName;
        }


        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            return ((GridControDataIncType)obj).Name == Name;
        }
    }
}
