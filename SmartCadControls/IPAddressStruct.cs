
using System.ComponentModel;

namespace SmartCadGuiCommon
{
	/// <summary>
	/// Summary description for IPAddressStruct.
	/// </summary>
	[TypeConverter(typeof (IPAddressStructConverter))]
	public class IPAddressStruct
	{
		private byte byte1;
		private byte byte2;
		private byte byte3;
		private byte byte4;

		public byte Byte1
		{
			get { return byte1; }
			set { byte1 = value; }
		}

		public byte Byte2
		{
			get { return byte2; }
			set { byte2 = value; }
		}

		public byte Byte3
		{
			get { return byte3; }
			set { byte3 = value; }
		}

		public byte Byte4
		{
			get { return byte4; }
			set { byte4 = value; }
		}

		public bool IsEmpty()
		{
			return ((byte1 == 0) && (byte2 == 0) && (byte3 == 0) && (byte4 == 0));
		}

		public ulong Address
		{
			get
			{
				ulong res = (((ulong) Byte4) << 24);
				res += (((ulong) Byte3) << 16);
				res += (((ulong) Byte2) << 8);
				res += (((ulong) Byte1));
				return res;
			}
			set
			{
				Byte1 = (byte) (value & 255);
				Byte2 = (byte) ((value >> 8) & 255);
				Byte3 = (byte) ((value >> 16) & 255);
				Byte4 = (byte) ((value >> 24) & 255);
			}
		}

		public static IPAddressStruct Parse(string s)
		{
			string[] v = s.Split(new char[] {'.'});
			return new IPAddressStruct(byte.Parse(v[0]), byte.Parse(v[1]), byte.Parse(v[2]), byte.Parse(v[3]));
		}

		public override bool Equals(object obj)
		{
			IPAddressStruct objStr = obj as IPAddressStruct;
			if ((!object.Equals(objStr, null)) && (objStr.Address == Address))
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public static bool Equals(IPAddressStruct a, IPAddressStruct b)
		{
			if (!object.Equals(a, null))
			{
				return a.Equals(b);
			}
			else if (object.Equals(b, null))
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public static bool operator ==(IPAddressStruct a, IPAddressStruct b)
		{
			return Equals(a, b);
		}

		public static bool operator !=(IPAddressStruct a, IPAddressStruct b)
		{
			return !Equals(a, b);
		}

		public override int GetHashCode()
		{
			return Address.GetHashCode();
		}

		public override string ToString()
		{
			return (Byte1 + "." + Byte2 + "." + Byte3 + "." + Byte4);
		}

		public IPAddressStruct(byte byte1I, byte byte2I, byte byte3I, byte byte4I)
		{
			byte1 = byte1I;
			byte2 = byte2I;
			byte3 = byte3I;
			byte4 = byte4I;
		}

		public IPAddressStruct() :
			this(0, 0, 0, 0)
		{
		}
	}
}