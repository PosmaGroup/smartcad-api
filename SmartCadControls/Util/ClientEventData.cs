﻿using SmartCadCore.ClientData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadControls.Util
{
    public class ClientEventData : ClientData
    {
        private string _deviceEventName;
        private DateTime _eventTime;

        public string CameraId { get; set; }

        public string DeviceEventName
        {
            get { return _deviceEventName; }
            set { _deviceEventName = value; }
        }

        public DateTime EventTime
        {
            get { return _eventTime; }
            set { _eventTime = value; }
        }
    }
}
