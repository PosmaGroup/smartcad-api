﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.Drawing;

using SmartCadCore.ClientData;
using SmartCadCore.Core;
using SmartCadCore.Common;
using Smartmatic.SmartCad.Service;
using SmartCadControls.Controls;
using Smartmatic.SmartCad.Map;



namespace SmartCadControls.Util
{
    public class MapTableUnits : MapTable<MapObjectUnit>
    {
        #region -- Constructor --
        
        public MapTableUnits(MapControlEx map, List<UnitClientData> objectsList)
            : base(map, new MapObjectUnit(), map.DynTableUnitsName, true, true, 11, 10, objectsList.ConvertAll<MapObjectUnit>((unit) => new MapObjectUnit(unit)))
        {
            //MakeAllVisible(true);
        }

        #endregion
        
        #region -- Insert --

        public void InsertObject(UnitClientData unit)
        {
            InsertObject(new MapObjectUnit(unit));
        }

        #endregion

        #region -- Update --

        public void UpdateObject(UnitClientData unit)
        {
            UpdateObject(new MapObjectUnit(unit));
        }

        public void UpdateObject(GPSClientData gps)
        {
            MapObjectUnit mapUnit = (MapObjectUnit)MapObjects[gps.UnitCode];
            if (mapUnit != null && ObjectsOn.ContainsKey(gps.UnitCode))
            {
                mapUnit.Position = new GeoPoint(gps.Lon, gps.Lat);
                UpdateObject(mapUnit);
            }
        }

        #endregion

        #region -- Delete --

        public void DeleteObject(UnitClientData unit)
        {
            DeleteObject(new MapObjectUnit(unit));
        }
        #endregion

        #region -- Search --
        
        public IList SearchUnitInMap(string text)
        {
            IList retval = new ArrayList();

            if (MapObjects.Count > 0)
            {
                string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetUnitsByCustomCodeTypeAndDepartmentTypeName, text);
                IList list = (IList)ServerServiceClient.GetInstance().SearchClientObjects(hql);

                if (list != null)
                {
                    foreach (UnitClientData unit in list)
                    {
                        if (ObjectsOn.ContainsKey(unit.DepartmentStation.Code))
                        {
                            try
                            {
                                AddressClientData add = new AddressClientData();
                                add.Lat = unit.Lat;
                                add.Lon = unit.Lon;

                                retval.Add(new KeyValuePair<UnitClientData, AddressClientData>(unit, add));
                            }
                            catch { }
                        }
                    }
                }
            }
            return retval;
        }
        
        #endregion

        #region -- Commons --
        public MapObjectUnit Get(int code)
        {
            return MapObjects[code] as MapObjectUnit;
        }

        public new void MakeObjectsVisible(bool state, List<int> stationsCode, params object[] args)
        {
            foreach (int stationCode in stationsCode)
            {
                foreach (MapObject obj in MapObjects.Values)
                {
                    if (int.Parse(obj.Fields["DepartmentStationCode"].ToString()) == stationCode)
                    {
                        MakeObjectVisible(state, obj.Code);
                    }
                }
            }
        }
        #endregion
    }

    public class MapTableIncidents : MapTable<MapObjectIncident>
    {
        #region -- Constructor --
        
        public MapTableIncidents(MapControlEx map, List<IncidentClientData> objectsList)
            : base(map, new MapObjectIncident(), map.DynTableIncidentsName, true, true, 11, 10, objectsList.ConvertAll<MapObjectIncident>((incident) => new MapObjectIncident(incident)))
        {
            MakeAllVisible(true);
        }

        #endregion
        
        #region -- Insert --

        public void InsertObject(IncidentClientData incident)
        {
            InsertObject(new MapObjectIncident(incident));
            //MapObjectIncident obj = new MapObjectIncident(incident);
            //if (Incidents.Count(inc => inc.Code == incident.Code) > 0)
            //{
            //    obj.IconName = ResourceLoader.GetString("DefaultIconIncidentType") + ".BMP";
            //    MapControl.UpdateObject(obj as MapObject);
            //}
            //else
            //{
            //    Incidents.Add(obj);
            //    if (incident.Address.IsSynchronized)
            //    {
            //        MapControl.ClearActions(MapActions.ClearTempTable);
            //        MapControl.InsertObject(obj as MapObject);
            //    }
            //}
        }

        public void InsertObjects(List<IncidentClientData> incidents)
        {
            foreach (IncidentClientData inc in incidents)
            {
                InsertObject(inc);    
            }
        }

        #endregion

        #region -- Update --

        public void UpdateObject(IncidentClientData incident)
        {
            UpdateObject(new MapObjectIncident(incident));
        }

        #endregion

        #region -- Delete --

        public void DeleteObject(IncidentClientData incident)
        {
            DeleteObject(new MapObjectIncident(incident));
        }

        #endregion

        #region -- Search --
       
        public IList SearchIncidents(string text)
        {
            string hql = SmartCadHqls.NewIncidentRuleEvaluateSearch;
            string hqlf = SmartCadHqls.GetCustomHql(hql, text);
            IList list = (IList)ServerServiceClient.GetInstance().SearchClientObjects(hqlf, true);
            IList retval = new ArrayList();
            foreach (IncidentClientData var in list)
            {
                if (ObjectsOn.ContainsKey(var.Code) && var.Address != null && var.Address.IsSynchronized)
                {
                    retval.Add(var);
                }
            }
            return retval;
        }

        #endregion

        #region -- Commons --
        
        private List<MapObjectIncident> GetIncidentsByPrefix(string prefix, string isSuffix)
        {
            List<MapObjectIncident> subList;
            if (isSuffix.Equals(false.ToString()))
            {
                subList = MapObjects.Values.Where(inc => ((string)inc.Fields["CustomCode"]).StartsWith(prefix)).Cast<MapObjectIncident>().ToList();
            }
            else
            {
                subList = MapObjects.Values.Where(inc => ((string)inc.Fields["CustomCode"]).EndsWith(prefix)).Cast<MapObjectIncident>().ToList();
            }
            return subList;
        }

        public new void MakeObjectsVisible(bool state, List<int> incidentsCode, params object[] args)
        {
            if (state == true)
            {
                string prefix = args[0] as string;
                string isSuffix = args[1] as string;
                foreach (MapObjectIncident inc in GetIncidentsByPrefix(prefix, isSuffix))
                {
                    MakeObjectVisible(true,inc.Code);
                }
            }
            else
            {
                string prefix = args[0] as string;
                string isSuffix = args[1] as string;
                foreach (MapObjectIncident inc in GetIncidentsByPrefix(prefix, isSuffix))
                {
                    MakeObjectVisible(false,inc.Code);
                }
            }
        }
        #endregion

        public MapObjectIncident Get(int code)
        {
            return MapObjects[code] as MapObjectIncident;
        }
    }

    public class MapTableStructs : MapTable<MapObjectStruct>
    {
        #region -- Fields --
        public const string ICON_POST_DEFAULT = "estructuras.BMP";
        #endregion

        #region -- Constructor --

        public MapTableStructs(MapControlEx map, List<StructClientData> objectsList)
            : base(map, new MapObjectStruct(), map.DynTablePostName, true, false, 11, 10, objectsList.ConvertAll<MapObjectStruct>((str) => new MapObjectStruct(str)))
        {
        }

        #endregion
        
        #region -- Insert --

        public void InsertObject(StructClientData strct)
        {
            InsertObject(new MapObjectStruct(strct));
        }

        public void InsertObjects(int zoneCode)
        {
            foreach (MapObjectStruct obj in GetStructsByZoneCode(zoneCode))
            {
                InsertObject(obj);
            }
        }

        #endregion

        #region -- Update --

        public void UpdateObject(StructClientData strct)
        {
            UpdateObject(new MapObjectStruct(strct));
        }

        #endregion
        
        #region -- Delete --

        public void DeleteObject(StructClientData strct)
        {
            DeleteObject(new MapObjectStruct(strct));

            //if (zoneOn.ContainsKey((int)obj.Fields["CctvZoneCode"]) == true)
            //{
            //    if (zoneOn[(int)obj.Fields["CctvZoneCode"]] == true)
            //    {
            //        MapControl.DeleteObject(obj);
            //    }
            //    Structs.RemoveAll(st => st.Code == obj.Code);
            //}
        }
        
        public void DeleteObjects(int zoneCode)
        {
            foreach (MapObjectStruct obj in GetStructsByZoneCode(zoneCode))
            {
                DeleteObject(obj);
            }
        }

        #endregion

        #region -- Search --

        public IList SearchStructs(string text)
        {
            string hqlf = SmartCadHqls.GetCustomHql(SmartCadHqls.SearchStructByText, text);
            IList list = ServerServiceClient.GetInstance().SearchClientObjects(hqlf);
            IList retval = new ArrayList();
            foreach (StructClientData var in list)
            {
                if (ObjectsOn.ContainsKey(var.Code))
                {
                    retval.Add(var);
                }
            }
            return retval;
        }

        #endregion

        #region -- Commons --

        private IEnumerable<MapObjectStruct> GetStructsByZoneCode(int code)
        {
            IEnumerable<MapObjectStruct> fetList = MapObjects.Values.Where(str => (int)((MapObjectStruct)str).Fields["CctvZoneCode"] == code).Cast<MapObjectStruct>();
            return fetList;
        }

        public override void MakeObjectVisible(bool state, int zoneCode)
        {
            foreach (MapObjectStruct obj in GetStructsByZoneCode(zoneCode))
            {
                base.MakeObjectVisible(state, obj.Code);
            }
        }
        //public void MakeObjectVisible(bool state, int zoneCode)
        //{
        //    if (state == true)
        //    {
        //        zoneOn[zoneCode] = true;
        //        InsertObjects(zoneCode);
        //    }
        //    else
        //    {
        //        DeleteObjects(zoneCode);
        //        zoneOn[zoneCode] = false;
        //    }
        //}

        //public void MakeObjectsVisible(bool state, List<int> structsZoneCode, params object[] args)
        //{
        //    if (state == true)
        //    {
        //        foreach (int code in structsZoneCode)
        //        {
        //            zoneOn[code] = true;
        //            InsertObjects(code);
        //        }
        //    }
        //    else
        //    {
        //        foreach (int code in structsZoneCode)
        //        {
        //            DeleteObjects(code);
        //            zoneOn[code] = false;
        //        }
        //    }
        //}

        #endregion
    }

    public class MapTableZones : MapTable<MapObjectZone>
    {
        #region -- Constructor --

        public MapTableZones(MapControlEx map, List<DepartmentZoneClientData> objectsList)
            : base(map, new MapObjectZone(), map.DynTablePolygonZoneName, true, false, 100, 100, objectsList.ConvertAll<MapObjectZone>((zone) => new MapObjectZone(zone)))
        {
        }

        #endregion

        #region -- Initialize --

        //public void InitializeListObjects(List<DepartmentZoneClientData> list)
        //{
        //    List<DepartmentZoneClientData> listZones = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetDepartmentZonesWithAddress).Cast<DepartmentZoneClientData>().ToList();
        //    foreach (DepartmentZoneClientData depZone in listZones)
        //    {
        //        InsertObject(new MapObjectZone(depZone));
        //    }
        //}

        #endregion

        #region -- Insert --

        public void InsertObject(DepartmentZoneClientData zone)
        {
            InsertObject(new MapObjectZone(zone));
        }

        #endregion

        #region -- Update --
        
        public void UpdateObject(DepartmentZoneClientData zone)
        {
            UpdateObject(new MapObjectZone(zone));
        }

        public void UpdateObject(DepartmentTypeClientData depClientData)
        {
            List<int> zonesInDepType = GetZonesInDepartmentType(depClientData.Code);
            foreach (int zoneCode in zonesInDepType)
            {
                if (MapObjects.ContainsKey(zoneCode))
                {
                    MapObjectZone obj = MapObjects[zoneCode] as MapObjectZone;
                    obj.Color = depClientData.Color;
                    obj.Fields["DepartmentTypeCode"] = depClientData.Code;

                    UpdateObject(obj);
                }
            }
        }

        private List<int> GetZonesInDepartmentType(int code)
        {
            IList stations = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetDepartmentZonesByDepartmentType, code));
            return stations.Cast<DepartmentZoneClientData>().ToList().ConvertAll<int>(delegate(DepartmentZoneClientData dep)
            {
                return dep.Code;
            });
        }

        #endregion

        #region -- Delete --

        public void DeleteObject(DepartmentZoneClientData zone)
        {
            DeleteObject(new MapObjectZone(zone));
        }

        #endregion
    }

    public class MapTableRoutes : MapTable<MapObjectRoute>
    {
        #region -- Constructor --

        public MapTableRoutes(MapControlEx map, List<RouteClientData> objectsList)
            : base(map, new MapObjectRoute(), map.DynTableRoute, true, false, 11, 10, objectsList.ConvertAll<MapObjectRoute>((route) => new MapObjectRoute(route)))
        {
        }

        #endregion

        #region -- Insert --

        public void InsertObject(RouteClientData route)
        {
            InsertObject(new MapObjectRoute(route));
        }

        #endregion

        #region -- Update --

        public void UpdateObject(RouteClientData clientdata)
        {
            MapObjectRoute route = new MapObjectRoute(clientdata);

            lock (mapLock)
            {
                if (ObjectsOn.ContainsKey(clientdata.Code))
                {
                    List<MapObjectBusStop> oldStops = ((MapObjectRoute)MapObjects[route.Code]).Stops;
                    foreach (MapObjectBusStop stop in oldStops)
                    {
                        if (!route.Stops.Contains(stop))
                            MapControl.DeleteObject(stop);
                    }
                    foreach (MapObjectBusStop stop in route.Stops)
                    {
                        if (!oldStops.Contains(stop))
                            MapControl.InsertObject(stop);
                        else
                            MapControl.UpdateObject(stop);
                    }
                }
            }

            UpdateObject(route);
        }

        public void UpdateObject(DepartmentTypeClientData depClientData)
        {
            foreach (MapObjectRoute route in MapObjects.Cast<MapObjectRoute>())
            {
                if ((int)route.Fields["ParentCode"] == depClientData.Code)
                {
                    route.Color = depClientData.Color;
                    UpdateObject(route);
                }
            }
        }

        #endregion

        #region -- Delete --

        public void DeleteObject(RouteClientData route)
        {
            DeleteObject(new MapObjectRoute(route));
            foreach (RouteAddressClientData item in route.RouteAddress)
            {
                if (string.IsNullOrEmpty(item.Name))
                {
                    lock (mapLock)
                    {
                        MapControl.DeleteObject(new MapObjectBusStop(item, route.Code, route.Image) as MapObject);
                    }
                }
            }
        }

        #endregion

        #region -- Commons --

        public override void MakeObjectVisible(bool state, int objCode)
        {
            lock (mapLock)
            {
                if (state == true)
                {
                    if (!ObjectsOn.ContainsKey(objCode))
                    {
                        if (MapObjects.ContainsKey(objCode))
                        {
                            MapObjectRoute route = MapObjects[objCode] as MapObjectRoute;
                            MapControl.InsertObject(route);
                            foreach (MapObjectBusStop stop in route.Stops)
                            {
                                MapControl.InsertObject(stop);
                            }
                        }
                        ObjectsOn.Add(objCode, true);
                    }
                }
                else
                {
                    if (ObjectsOn.ContainsKey(objCode))
                    {
                        if (MapObjects.ContainsKey(objCode))
                        {
                            MapObjectRoute route = MapObjects[objCode] as MapObjectRoute;
                            MapControl.DeleteObject(route);
                            foreach (MapObjectBusStop stop in route.Stops)
                            {
                                MapControl.DeleteObject(stop);
                            }
                        }
                        ObjectsOn.Remove(objCode);
                    }
                }
            }
        }

        #endregion
    }

    public class MapTableStations : MapTable<MapObjectStation>
    {
        #region -- Constructor --

        public MapTableStations(MapControlEx map, List<DepartmentStationClientData> objectsList)
            : base(map, new MapObjectStation(), map.DynTablePolygonStationName, true, false, 100, 100, objectsList.ConvertAll<MapObjectStation>((station) => new MapObjectStation(station)))
        {
        }

        #endregion

        #region -- Insert --

        public void InsertObject(DepartmentStationClientData zone)
        {
            InsertObject(new MapObjectStation(zone));
        }

        #endregion

        #region -- Update --

        public void UpdateObject(DepartmentStationClientData station)
        {
            UpdateObject(new MapObjectStation(station));
        }

        public void UpdateObject(DepartmentTypeClientData depClientData)
        {
            foreach (int stationCode in GetStationsInDepartmentType(depClientData.Code))
            {
                if (MapObjects.ContainsKey(stationCode))
                {
                    MapObjectStation obj = MapObjects[stationCode] as MapObjectStation;
                    obj.Color = depClientData.Color;
                    obj.Fields["DepartmentTypeCode"] = depClientData.Code;

                    UpdateObject(obj);
                }
            }
        }

        private List<int> GetStationsInDepartmentType(int depTypeCode)
        {
            IList stations = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetDepartmentStationCodeByDepartmentType, depTypeCode));
            return stations.Cast<DepartmentStationClientData>().ToList().ConvertAll<int>(delegate(DepartmentStationClientData dep)
            {
                return dep.Code;
            });
        }

        #endregion

        #region -- Delete --

        public void DeleteObject(DepartmentStationClientData station)
        {
            DeleteObject(new MapObjectStation(station));
        }

        #endregion
    }

    public class MapTable<T> where T : MapObject
    {
        public MapControlEx MapControl { get; set; }
        public Dictionary<int, T> MapObjects { get; set; }
        internal Dictionary<int, bool> ObjectsOn;
        private string TableName { get; set; }
        internal object mapLock = new object();

        #region -- Constructor --

        public MapTable()
        {
        }

        public MapTable(MapControlEx map, T obj, string tableName, bool objEnable, bool lblEnable, int objZoom, int lblZoom, List<T> objectsList)
        {
            MapControl = map;
            MapObjects = new Dictionary<int, T>();
            ObjectsOn = new Dictionary<int, bool>();
            TableName = tableName;

            //Create table in map
            lock (mapLock)
            {
                MapControl.CreateTable(obj, tableName, objEnable, lblEnable, objZoom, lblZoom);
            }
            if (objectsList != null && objectsList.Count > 0)
                InitializeListObjects(objectsList);
        }

        #endregion

        #region -- Initialize --
        public void InitializeListObjects(List<T> objectsList)
        {
            foreach (T obj in objectsList)
            {
                InsertObject(obj);
            }
        }
        #endregion

        #region -- Insert --

        internal void InsertObject(T obj)
        {
            if (obj.Drawable && !MapObjects.ContainsKey(obj.Code))
                MapObjects.Add(obj.Code, obj);
        }

        #endregion

        #region -- Update --

        internal void UpdateObject(T obj)
        {
            lock (mapLock)
            {
                if (obj.Drawable)
                {
                    //si estaba visible la actualiza
                    if (MapObjects.ContainsKey(obj.Code))
                    {
                        MapObjects[obj.Code] = obj;
                        if (ObjectsOn.ContainsKey(obj.Code))
                        {
                            MapControl.UpdateObject(obj);
                        }
                    }
                    else
                    {
                        MapObjects.Add(obj.Code, obj);
                        if (ObjectsOn.ContainsKey(obj.Code))
                        {
                            MapControl.InsertObject(obj);
                        }
                    }
                }
                else
                {
                    if (MapObjects.ContainsKey(obj.Code))
                    {
                        MapObjects.Remove(obj.Code);

                        //si estaba visible la elimina
                        if (ObjectsOn.ContainsKey(obj.Code))
                        {
                            MapControl.DeleteObject(obj);
                        }
                    }
                }
            }
        }

        #endregion

        #region -- Delete --

        public void DeleteObject(T obj)
        {
            lock (mapLock)
            {
                if (ObjectsOn.ContainsKey(obj.Code) == true)
                {
                    if (MapObjects.ContainsKey(obj.Code))
                    {
                        MapControl.DeleteObject(obj);
                        MapObjects.Remove(obj.Code);
                    }
                    ObjectsOn.Remove(obj.Code);
                }
                if (MapObjects.ContainsKey(obj.Code))
                {
                    MapObjects.Remove(obj.Code);
                }
            }
        }

        #endregion

        #region -- Commons --

        public bool CheckObjectVisible(T obj)
        {
            return ObjectsOn.ContainsKey(obj.Code);
        }

        public virtual void MakeObjectVisible(bool state, int objCode)
        {
            lock (mapLock)
            {
                if (state == true)
                {
                    if (!ObjectsOn.ContainsKey(objCode))
                    {
                        if (MapObjects.ContainsKey(objCode))
                        {
                            MapControl.InsertObject(MapObjects[objCode]);
                        }
                        ObjectsOn.Add(objCode, true);
                    }
                }
                else
                {
                    if (ObjectsOn.ContainsKey(objCode))
                    {
                        if (MapObjects.ContainsKey(objCode))
                        {
                            MapControl.DeleteObject(MapObjects[objCode]);
                        }
                        ObjectsOn.Remove(objCode);
                    }
                }
            }
        }

        public void MakeObjectsVisible(bool state, List<int> objsCode, params object[] args)
        {
            foreach (int objCode in objsCode)
            {
                MakeObjectVisible(state, objCode);
            }
        }

        public virtual void MakeAllVisible(bool state)
        {
            lock (mapLock)
            {
                if (state)
                {
                    MapControl.EnableLayer(TableName, false);
                    foreach (T obj in MapObjects.Values.Where(o => !ObjectsOn.ContainsKey(o.Code)))
                    {
                        MapControl.InsertObject(obj);
                        ObjectsOn.Add(obj.Code, true);
                    }
                    MapControl.EnableLayer(TableName, true);
                }
                else
                {
                    MapControl.ClearTable(TableName);
                    ObjectsOn.Clear();
                }
            }
        }
        #endregion
    }
}
