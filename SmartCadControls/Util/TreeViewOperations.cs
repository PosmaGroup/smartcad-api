﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Collections;
using Smartmatic.SmartCad.Service;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadCore.Core;

namespace SmartCadControls.Util
{
    public interface ITreeViewOperations<T> 
    {
        TreeListNodeEx Root { get; set; }
    }

    public class TreeViewUnits : ITreeViewOperations<UnitClientData>
    {
        #region -- Fields --
        
        public TreeListNodeEx Root { get; set; }
        private Dictionary<int, bool> stationsOn;
        
        #endregion

        #region -- Constructor --
        
        public TreeViewUnits()
        {
            stationsOn = new Dictionary<int, bool>();
        }
        
        #endregion

        #region  -- Zones --

        public void InsertNode(DepartmentZoneClientData zone)
        {
            for (int i = 0; i < Root.Nodes.Count; i++)
            {
                TreeListNodeEx ndx = Root.Nodes[i] as TreeListNodeEx;
                if (ndx.TypeNodeLeaf == TreeListNodeExType.DepartamentType)
                {
                    if ((int)ndx.Tag == zone.DepartmentType.Code)
                    {
                        ndx.TreeList.BeginUpdate();
                        TreeListNodeEx zoneNode = (TreeListNodeEx)ndx.TreeList.AppendNode(null, ndx);
                        zoneNode.LayerName = zone.Name;
                        zoneNode.TypeNodeLeaf = TreeListNodeExType.DepartamentTypeZone;
                        zoneNode.Tag = zone.Code;
                        ndx.TreeList.EndUpdate();
                        ndx.TreeList.BeginSort();
                        ndx.TreeList.EndSort();
                    }
                }
            }
        }

        public void UpdateNode(DepartmentZoneClientData zone)
        {
            try
            {
                TreeListNodeEx backup = FindZoneSon(zone.Code);
                TreeListNodeEx father = FindFatherZone(zone);
                Root.TreeList.BeginUpdate();
                Root.TreeList.DeleteNode(backup);
                TreeListNodeEx zoneNode = (TreeListNodeEx)Root.TreeList.AppendNode(null, father);
                zoneNode.LayerName = zone.Name;
                zoneNode.TypeNodeLeaf = TreeListNodeExType.DepartamentTypeZone;
                zoneNode.Tag = zone.Code;

                foreach (TreeListNodeEx var in backup.Nodes)
                {
                    zoneNode.Nodes.Add(var);
                }
                Root.TreeList.EndUpdate();
                Root.TreeList.BeginSort();
                Root.TreeList.EndSort();
            }
            catch (Exception e)
            {
                MessageForm.Show(e.Message, MessageFormType.Error);
                SmartLogger.Print(e);
            }
        }

        private TreeListNodeEx FindFatherZone(DepartmentZoneClientData zone)
        {
            for (int i = 0; i < Root.Nodes.Count; i++)
            {
                TreeListNodeEx ndx = Root.Nodes[i] as TreeListNodeEx;
                if (ndx.TypeNodeLeaf == TreeListNodeExType.DepartamentType)
                {
                    if ((int)ndx.Tag == zone.DepartmentType.Code)
                    {
                        return ndx;
                    }
                }
            }
            return null;
        }

        public void DeleteNode(DepartmentZoneClientData zone)
        {
            for (int i = 0; i < Root.Nodes.Count; i++)
            {
                TreeListNodeEx ndx = Root.Nodes[i] as TreeListNodeEx;
                if (ndx.TypeNodeLeaf == TreeListNodeExType.DepartamentType)
                {
                    if ((int)ndx.Tag == zone.DepartmentType.Code)
                    {
                        for (int j = 0; j < ndx.Nodes.Count; j++)
                        {
                            TreeListNodeEx son = ndx.Nodes[j] as TreeListNodeEx;
                            if ((int)son.Tag == zone.Code)
                            {
                                Root.TreeList.BeginUpdate();
                                Root.TreeList.DeleteNode(son);
                                Root.TreeList.EndUpdate();
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region  -- Stations --

        public void InsertNode(DepartmentStationClientData station)
        {
            for (int i = 0; i < Root.Nodes.Count; i++)
            {
                TreeListNodeEx ndx = Root.Nodes[i] as TreeListNodeEx;
                if (ndx.TypeNodeLeaf == TreeListNodeExType.DepartamentType)
                {
                    if ((int)ndx.Tag == station.DepartamentTypeCode)
                    {
                        for (int j = 0; j < ndx.Nodes.Count; j++)
                        {

                            if ((int)ndx.Nodes[j].Tag == station.DepartmentZone.Code)
                            {
                                ndx.TreeList.BeginUpdate();
                                TreeListNodeEx stationNode = (TreeListNodeEx)ndx.TreeList.AppendNode(null, ndx.Nodes[j]);
                                stationNode.LayerName = station.Name;
                                stationNode.TypeNodeLeaf = TreeListNodeExType.DepartamentStation;
                                stationNode.Tag = station.Code;
                                stationsOn[station.Code] = false;
                                ndx.TreeList.EndUpdate();
                                ndx.TreeList.BeginSort();
                                ndx.TreeList.EndSort();
                            }
                        }
                    }
                }
            }
        }

        public void DeleteNode(DepartmentStationClientData station)
        {
            for (int i = 0; i < Root.Nodes.Count; i++)
            {
                TreeListNodeEx depType = Root.Nodes[i] as TreeListNodeEx;
                if (depType.TypeNodeLeaf == TreeListNodeExType.DepartamentType)
                {
                    if ((int)depType.Tag == station.DepartamentTypeCode)
                    {
                        for (int j = 0; j < depType.Nodes.Count; j++)
                        {
                            TreeListNodeEx depZone = depType.Nodes[j] as TreeListNodeEx;
                            if ((int)depZone.Tag == station.DepartmentZone.Code)
                            {
                                for (int k = 0; k < depZone.Nodes.Count; k++)
                                {
                                    TreeListNodeEx depStation = depZone.Nodes[k] as TreeListNodeEx;
                                    if ((int)depStation.Tag == station.Code)
                                    {
                                        Root.TreeList.BeginUpdate();
                                        Root.TreeList.DeleteNode(depStation);
                                        Root.TreeList.EndUpdate();
                                        j = depType.Nodes.Count;
                                        i = Root.Nodes.Count;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public void UpdateNode(DepartmentStationClientData station)
        {
            TreeListNodeEx backup = FindSon(station);
            TreeListNodeEx father = FindFather(station);
            Root.TreeList.BeginUpdate();
            backup.LayerName = station.Name;
            backup.Tag = station.Code;
            Root.TreeList.MoveNode(backup, father);
            Root.TreeList.EndUpdate();
            Root.TreeList.BeginSort();
            Root.TreeList.EndSort();
        }

        private TreeListNodeEx FindSon(DepartmentStationClientData station)
        {
            TreeListNodeEx son = null;
            for (int i = 0; i < Root.Nodes.Count; i++)
            {
                TreeListNodeEx ndx = Root.Nodes[i] as TreeListNodeEx;
                if (ndx.TypeNodeLeaf == TreeListNodeExType.DepartamentType)
                {

                    for (int j = 0; j < ndx.Nodes.Count; j++)
                    {

                        TreeListNodeEx ndex = ndx.Nodes[j] as TreeListNodeEx;
                        for (int k = 0; k < ndex.Nodes.Count; k++)
                        {
                            if ((int)ndex.Nodes[k].Tag == station.Code)
                            {
                                j = ndx.Nodes.Count;
                                i = Root.Nodes.Count;
                                son = ndex.Nodes[k] as TreeListNodeEx;
                                break;
                            }
                        }
                    }
                }
            }
            return son;
        }

        private TreeListNodeEx FindFather(DepartmentStationClientData station)
        {
            TreeListNodeEx father = null;
            for (int i = 0; i < Root.Nodes.Count; i++)
            {
                TreeListNodeEx ndx = Root.Nodes[i] as TreeListNodeEx;
                if (ndx.TypeNodeLeaf == TreeListNodeExType.DepartamentType)
                {
                    if ((int)ndx.Tag == station.DepartamentTypeCode)
                    {
                        for (int j = 0; j < ndx.Nodes.Count; j++)
                        {
                            if ((int)ndx.Nodes[j].Tag == station.DepartmentZone.Code)
                            {
                                father = ndx.Nodes[j] as TreeListNodeEx;
                            }
                        }
                    }
                }
            }
            return father;
        }

        #endregion

        #region  -- DepartamentType --

        public void InsertNode(DepartmentTypeClientData dep)
        {
            Root.TreeList.BeginUpdate();
            TreeListNodeEx depNode = (TreeListNodeEx)Root.TreeList.AppendNode(null, Root);
            depNode.LayerName = dep.Name;
            depNode.TypeNodeLeaf = TreeListNodeExType.DepartamentType;
            depNode.Tag = dep.Code;
            Root.TreeList.BeginSort();
            Root.TreeList.EndSort();
            Root.TreeList.EndUpdate();
        }

        public void DeleteNode(DepartmentTypeClientData dep)
        {
            for (int i = 0; i < Root.Nodes.Count; i++)
            {
                TreeListNodeEx ndx = Root.Nodes[i] as TreeListNodeEx;
                if (ndx.TypeNodeLeaf == TreeListNodeExType.DepartamentType)
                {
                    if ((int)ndx.Tag == dep.Code)
                    {
                        Root.TreeList.BeginUpdate();
                        Root.TreeList.DeleteNode(ndx);
                        Root.TreeList.EndUpdate();
                        break;
                    }
                }
            }
        }

        public void UpdateNode(DepartmentTypeClientData dep)
        {
            for (int i = 0; i < Root.Nodes.Count; i++)
            {
                TreeListNodeEx ndx = Root.Nodes[i] as TreeListNodeEx;
                if (ndx.TypeNodeLeaf == TreeListNodeExType.DepartamentType)
                {
                    if ((int)ndx.Tag == dep.Code)
                    {
                        Root.TreeList.BeginUpdate();
                        ndx.LayerName = dep.Name;
                        Root.TreeList.BeginSort();
                        Root.TreeList.EndSort();
                        Root.TreeList.EndUpdate();
                        break;
                    }
                }
            }
        }

        #endregion

        #region -- Commons --

        private TreeListNodeEx FindZoneSon(int code)
        {
            for (int i = 0; i < Root.Nodes.Count; i++)
            {
                TreeListNodeEx ndx = Root.Nodes[i] as TreeListNodeEx;
                if (ndx.TypeNodeLeaf == TreeListNodeExType.DepartamentType)
                {

                    for (int j = 0; j < ndx.Nodes.Count; j++)
                    {
                        if ((int)ndx.Nodes[j].Tag == code)
                        {
                            return ndx.Nodes[j] as TreeListNodeEx;
                        }
                    }

                }
            }

            return null;
        }

        #endregion
    }

    public class TreeViewIncidents : ITreeViewOperations<IncidentClientData>
    {
        #region -- Fields --
        
        public TreeListNodeEx Root { get; set; }
        
        #endregion

        #region -- Constructor --

        public TreeViewIncidents()
        {
        }

        #endregion
    }

    public class TreeViewStructs : ITreeViewOperations<StructClientData>
    {
        #region -- Fields --
        public const string ICON_POST_DEFAULT = "estructuras.BMP";
        public TreeListNodeEx Root { get; set; }

        #endregion

        #region -- Constructor --

        public TreeViewStructs()
        {
        }

        #endregion
    }

    public class TreeViewZones : ITreeViewOperations<DepartmentZoneClientData>
    {
        #region -- Fields --

        public TreeListNodeEx Root { get; set; }
        public Dictionary<int, bool> zonesOn;

        #endregion

        #region -- Constructor --

        public TreeViewZones()
        {
            zonesOn = new Dictionary<int, bool>();
        }

        #endregion

        #region -- Zones --
        
        public void InsertNode(DepartmentZoneClientData dep)
        {
            for (int i = 0; i < Root.Nodes.Count; i++)
            {
                TreeListNodeEx ndx = Root.Nodes[i] as TreeListNodeEx;
                if (ndx.TypeNodeLeaf == TreeListNodeExType.DepartamentZoneUbicationRoot)
                {
                    if ((int)ndx.Tag == dep.DepartmentType.Code)
                    {
                        zonesOn.Add(dep.Code, (ndx.CheckState == CheckState.Checked) ? (true) : (false));
                        ndx.TreeList.BeginUpdate();
                        TreeListNodeEx zoneNode = (TreeListNodeEx)ndx.TreeList.AppendNode(null, ndx);
                        zoneNode.LayerName = dep.Name;
                        zoneNode.TypeNodeLeaf = TreeListNodeExType.DepartamentZoneUbicationLayer;
                        zoneNode.Tag = dep.Code;
                        ndx.TreeList.EndUpdate();
                        ndx.TreeList.BeginSort();
                        ndx.TreeList.EndSort();
                    }
                }
            }
        }

        public void UpdateNode(DepartmentZoneClientData dep)
        {
            TreeListNodeEx father = null;
            TreeListNodeEx son = null;
            for (int i = 0; i < Root.Nodes.Count; i++)
            {
                TreeListNodeEx ndx = Root.Nodes[i] as TreeListNodeEx;
                if (ndx.TypeNodeLeaf == TreeListNodeExType.DepartamentZoneUbicationRoot)
                {
                    if ((int)ndx.Tag == dep.DepartmentType.Code)
                    {
                        father = ndx;
                        break;
                    }
                }
            }

            for (int i = 0; i < Root.Nodes.Count; i++)
            {
                TreeListNodeEx ndx = Root.Nodes[i] as TreeListNodeEx;
                if (ndx.TypeNodeLeaf == TreeListNodeExType.DepartamentZoneUbicationRoot)
                {
                    for (int j = 0; j < ndx.Nodes.Count; j++)
                    {
                        if ((int)ndx.Nodes[j].Tag == dep.Code)
                        {
                            son = ndx.Nodes[j] as TreeListNodeEx;
                            i = Root.Nodes.Count;
                            break;
                        }
                    }
                }
            }

            Root.TreeList.BeginUpdate();
            Root.TreeList.DeleteNode(son);
            TreeListNodeEx zoneNode = (TreeListNodeEx)Root.TreeList.AppendNode(null, father);
            zoneNode.LayerName = dep.Name;
            zoneNode.TypeNodeLeaf = TreeListNodeExType.DepartamentZoneUbicationLayer;
            zoneNode.Tag = dep.Code;
            zoneNode.CheckState = son.CheckState;
            Root.TreeList.EndUpdate();
            Root.TreeList.BeginSort();
            Root.TreeList.EndSort();
        }

        public void DeleteNode(DepartmentZoneClientData dep)
        {
            for (int i = 0; i < Root.Nodes.Count; i++)
            {
                TreeListNodeEx ndx = Root.Nodes[i] as TreeListNodeEx;
                if (ndx.TypeNodeLeaf == TreeListNodeExType.DepartamentZoneUbicationRoot)
                {
                    if ((int)ndx.Tag == dep.DepartmentType.Code)
                    {
                        for (int j = 0; j < ndx.Nodes.Count; j++)
                        {
                            TreeListNodeEx son = ndx.Nodes[j] as TreeListNodeEx;
                            if ((int)son.Tag == dep.Code)
                            {
                                Root.TreeList.BeginUpdate();
                                Root.TreeList.DeleteNode(son);
                                Root.TreeList.EndUpdate();
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region -- DepartmentTypes --

        public void InsertNode(DepartmentTypeClientData depClientData)
        {
            Root.TreeList.BeginUpdate();
            TreeListNodeEx cloneDepNode = (TreeListNodeEx)Root.TreeList.AppendNode(null, Root);
            cloneDepNode.LayerName = depClientData.Name;
            cloneDepNode.TypeNodeLeaf = TreeListNodeExType.DepartamentZoneUbicationRoot;
            cloneDepNode.Tag = depClientData.Code;
            Root.TreeList.EndUpdate();
            Root.TreeList.BeginSort();
            Root.TreeList.EndSort();
        }

        public void DeleteNode(DepartmentTypeClientData depClientData)
        {
            for (int i = 0; i < Root.Nodes.Count; i++)
            {
                TreeListNodeEx ndx = Root.Nodes[i] as TreeListNodeEx;
                if (ndx.TypeNodeLeaf == TreeListNodeExType.DepartamentZoneUbicationRoot)
                {
                    if ((int)ndx.Tag == depClientData.Code)
                    {
                        Root.TreeList.BeginUpdate();
                        Root.TreeList.DeleteNode(ndx);
                        Root.TreeList.EndUpdate();
                        break;
                    }
                }
            }
        }

        public void UpdateNode(DepartmentTypeClientData depClientData)
        {
            for (int i = 0; i < Root.Nodes.Count; i++)
            {
                TreeListNodeEx ndx = Root.Nodes[i] as TreeListNodeEx;
                if (ndx.TypeNodeLeaf == TreeListNodeExType.DepartamentZoneUbicationRoot)
                {
                    if ((int)ndx.Tag == depClientData.Code)
                    {
                        Root.TreeList.BeginUpdate();
                        ndx.LayerName = depClientData.Name;
                        Root.TreeList.BeginSort();
                        Root.TreeList.EndSort();
                        Root.TreeList.EndUpdate();
                        break;
                    }
                }
            }
        }

        #endregion
    }

    public class TreeViewStations : ITreeViewOperations<DepartmentStationClientData>
    {
        #region -- Fields --
        public Dictionary<int, bool> stationsOn;
        public TreeListNodeEx Root { get; set; }
        #endregion

        #region -- Constructor --

        public TreeViewStations()
        {
            stationsOn = new Dictionary<int, bool>();
        }

        #endregion

        #region TreeViewOperations Members

        public void InsertObject(DepartmentStationAddressClientData station)
        {
        }

        public void DeleteObject(DepartmentStationAddressClientData station)
        {
        }

        public void UpdateObject(DepartmentStationAddressClientData station)
        {
        }

        #endregion

        #region -- DepartmentType --

        public void InsertNode(DepartmentTypeClientData depClientData)
        {
            Root.TreeList.BeginUpdate();
            TreeListNodeEx depNode = (TreeListNodeEx)Root.TreeList.AppendNode(null, Root);
            depNode.LayerName = depClientData.Name;
            depNode.TypeNodeLeaf = TreeListNodeExType.DepartamentStationUbicationRoot;
            depNode.Tag = depClientData.Code;
            Root.TreeList.BeginSort();
            Root.TreeList.EndSort();
            Root.TreeList.EndUpdate();

        }

        public void DeleteNode(DepartmentTypeClientData depClientData)
        {
            for (int i = 0; i < Root.Nodes.Count; i++)
            {
                TreeListNodeEx ndx = Root.Nodes[i] as TreeListNodeEx;
                if (ndx.TypeNodeLeaf == TreeListNodeExType.DepartamentStationUbicationRoot)
                {
                    if ((int)ndx.Tag == depClientData.Code)
                    {
                        Root.TreeList.BeginUpdate();
                        Root.TreeList.DeleteNode(ndx);
                        Root.TreeList.EndUpdate();
                        break;
                    }
                }
            }
        }

        public void UpdateNode(DepartmentTypeClientData depClientData)
        {
            for (int i = 0; i < Root.Nodes.Count; i++)
            {
                TreeListNodeEx ndx = Root.Nodes[i] as TreeListNodeEx;
                if (ndx.TypeNodeLeaf == TreeListNodeExType.DepartamentStationUbicationRoot)
                {
                    if ((int)ndx.Tag == depClientData.Code)
                    {
                        Root.TreeList.BeginUpdate();
                        ndx.LayerName = depClientData.Name;
                        Root.TreeList.BeginSort();
                        Root.TreeList.EndSort();
                        Root.TreeList.EndUpdate();
                        break;
                    }
                }
            }
        }

        #endregion

        #region -- Zone --
        public void DeleteNode(DepartmentStationClientData dep)
        {
            for (int i = 0; i < Root.Nodes.Count; i++)
            {
                TreeListNodeEx depType = Root.Nodes[i] as TreeListNodeEx;
                if (depType.TypeNodeLeaf == TreeListNodeExType.DepartamentStationUbicationRoot)
                {
                    if ((int)depType.Tag == dep.DepartamentTypeCode)
                    {
                        for (int j = 0; j < depType.Nodes.Count; j++)
                        {
                            TreeListNodeEx depZone = depType.Nodes[j] as TreeListNodeEx;
                            if ((int)depZone.Tag == dep.DepartmentZone.Code)
                            {
                                for (int k = 0; k < depZone.Nodes.Count; k++)
                                {
                                    TreeListNodeEx depStation = depZone.Nodes[k] as TreeListNodeEx;
                                    if ((int)depStation.Tag == dep.Code)
                                    {
                                        Root.TreeList.BeginUpdate();
                                        Root.TreeList.DeleteNode(depStation);
                                        Root.TreeList.EndUpdate();
                                        j = depType.Nodes.Count;
                                        i = Root.Nodes.Count;
                                        break;
                                    }

                                }
                            }
                        }
                    }
                }
            }

        }

        public void InsertNode(DepartmentZoneClientData dep)
        {
            for (int i = 0; i < Root.Nodes.Count; i++)
            {
                TreeListNodeEx ndx = Root.Nodes[i] as TreeListNodeEx;
                if (ndx.TypeNodeLeaf == TreeListNodeExType.DepartamentStationUbicationRoot)
                {
                    if ((int)ndx.Tag == dep.DepartmentType.Code)
                    {
                        ndx.TreeList.BeginUpdate();
                        TreeListNodeEx zoneNode = (TreeListNodeEx)ndx.TreeList.AppendNode(null, ndx);
                        zoneNode.LayerName = dep.Name;
                        zoneNode.TypeNodeLeaf = TreeListNodeExType.DepartamentStationUbicationRoot;
                        zoneNode.Tag = dep.Code;
                        ndx.TreeList.EndUpdate();
                        ndx.TreeList.BeginSort();
                        ndx.TreeList.EndSort();
                    }
                }
            }
        }

        public void UpdateNode(DepartmentZoneClientData dep)
        {
            TreeListNodeEx backup = FindZoneSon(dep.Code);
            TreeListNodeEx father = FindFatherZone(dep);
            Root.TreeList.BeginUpdate();
            backup.LayerName = dep.Name;
            backup.Tag = dep.Code;
            Root.TreeList.MoveNode(backup, father);
            Root.TreeList.EndUpdate();
            Root.TreeList.BeginSort();
            Root.TreeList.EndSort();
        }

        private TreeListNodeEx FindFatherZone(DepartmentZoneClientData dep)
        {
            for (int i = 0; i < Root.Nodes.Count; i++)
            {
                TreeListNodeEx ndx = Root.Nodes[i] as TreeListNodeEx;
                if (ndx.TypeNodeLeaf == TreeListNodeExType.DepartamentStationUbicationRoot)
                {
                    if ((int)ndx.Tag == dep.DepartmentType.Code)
                    {
                        return ndx;
                    }
                }
            }
            return null;
        }

        private TreeListNodeEx FindZoneSon(int code)
        {
            for (int i = 0; i < Root.Nodes.Count; i++)
            {
                TreeListNodeEx ndx = Root.Nodes[i] as TreeListNodeEx;
                if (ndx.TypeNodeLeaf == TreeListNodeExType.DepartamentStationUbicationRoot)
                {

                    for (int j = 0; j < ndx.Nodes.Count; j++)
                    {
                        if ((int)ndx.Nodes[j].Tag == code)
                        {
                            return ndx.Nodes[j] as TreeListNodeEx;
                        }
                    }

                }
            }
            return null;
        }

        public void DeleteNode(DepartmentZoneClientData dep)
        {

            for (int i = 0; i < Root.Nodes.Count; i++)
            {
                TreeListNodeEx ndx = Root.Nodes[i] as TreeListNodeEx;
                if (ndx.TypeNodeLeaf == TreeListNodeExType.DepartamentStationUbicationRoot)
                {
                    if ((int)ndx.Tag == dep.DepartmentType.Code)
                    {
                        for (int j = 0; j < ndx.Nodes.Count; j++)
                        {
                            TreeListNodeEx son = ndx.Nodes[j] as TreeListNodeEx;
                            if ((int)son.Tag == dep.Code)
                            {
                                Root.TreeList.BeginUpdate();
                                Root.TreeList.DeleteNode(son);
                                Root.TreeList.EndUpdate();
                            }
                        }
                    }
                }
            }


        }
        #endregion

        #region -- Stations --
        public void InsertNode(DepartmentStationClientData dep)
        {
            for (int i = 0; i < Root.Nodes.Count; i++)
            {
                TreeListNodeEx ndx = Root.Nodes[i] as TreeListNodeEx;
                if (ndx.TypeNodeLeaf == TreeListNodeExType.DepartamentStationUbicationRoot)
                {
                    if ((int)ndx.Tag == dep.DepartamentTypeCode)
                    {
                        for (int j = 0; j < ndx.Nodes.Count; j++)
                        {

                            if ((int)ndx.Nodes[j].Tag == dep.DepartmentZone.Code)
                            {
                                stationsOn.Add(dep.Code, (ndx.CheckState == CheckState.Checked) ? (true) : (false));
                                ndx.TreeList.BeginUpdate();
                                TreeListNodeEx stationNode = (TreeListNodeEx)ndx.TreeList.AppendNode(null, ndx.Nodes[j]);
                                stationNode.LayerName = dep.Name;
                                stationNode.TypeNodeLeaf = TreeListNodeExType.DepartamentStationUbicationLayer;
                                stationNode.Tag = dep.Code;
                                ndx.TreeList.EndUpdate();
                                ndx.TreeList.BeginSort();
                                ndx.TreeList.EndSort();
                            }
                        }
                    }
                }
            }
        }

        public void UpdateNode(DepartmentStationClientData dep)
        {
            TreeListNodeEx backup = FindSon(dep);
            TreeListNodeEx father = FindFather(dep);
            Root.TreeList.BeginUpdate();
            backup.LayerName = dep.Name;
            backup.Tag = dep.Code;
            Root.TreeList.MoveNode(backup, father);
            Root.TreeList.EndUpdate();
            Root.TreeList.BeginSort();
            Root.TreeList.EndSort();
        }

        private TreeListNodeEx FindSon(DepartmentStationClientData dep)
        {
            TreeListNodeEx son = null;
            for (int i = 0; i < Root.Nodes.Count; i++)
            {
                TreeListNodeEx ndx = Root.Nodes[i] as TreeListNodeEx;
                if (ndx.TypeNodeLeaf == TreeListNodeExType.DepartamentStationUbicationRoot)
                {

                    for (int j = 0; j < ndx.Nodes.Count; j++)
                    {
                        TreeListNodeEx ndex = ndx.Nodes[j] as TreeListNodeEx;
                        for (int k = 0; k < ndex.Nodes.Count; k++)
                        {
                            if ((int)ndex.Nodes[k].Tag == dep.Code)
                            {
                                j = ndx.Nodes.Count;
                                i = Root.Nodes.Count;
                                son = ndex.Nodes[k] as TreeListNodeEx;
                                j = ndx.Nodes.Count;
                                i = Root.Nodes.Count;
                                break;
                            }
                        }

                    }

                }
            }
            return son;
        }

        private TreeListNodeEx FindFather(DepartmentStationClientData dep)
        {
            TreeListNodeEx father = null;
            for (int i = 0; i < Root.Nodes.Count; i++)
            {
                TreeListNodeEx ndx = Root.Nodes[i] as TreeListNodeEx;
                if (ndx.TypeNodeLeaf == TreeListNodeExType.DepartamentStationUbicationRoot)
                {
                    if ((int)ndx.Tag == dep.DepartamentTypeCode)
                    {
                        for (int j = 0; j < ndx.Nodes.Count; j++)
                        {
                            if ((int)ndx.Nodes[j].Tag == dep.DepartmentZone.Code)
                            {
                                father = ndx.Nodes[j] as TreeListNodeEx;
                            }
                        }
                    }
                }
            }
            return father;
        }

        #endregion

        #region -- Commons --

        private List<int> GetStationsInDepartmentType(int depTypeCode)
        {
            IList stations = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetDepartmentStationCodeByDepartmentType, depTypeCode));
            return stations.Cast<DepartmentStationClientData>().ToList().ConvertAll<int>(delegate(DepartmentStationClientData dep)
            {
                return dep.Code;
            });

        }

        #endregion

    }

    public class TreeViewRoutes : ITreeViewOperations<RouteClientData>
    {
        #region -- Fields --
        public Dictionary<int, bool> routesOn;
        public TreeListNodeEx Root { get; set; }
        #endregion

        #region -- Constructor --

        public TreeViewRoutes()
        {
            routesOn = new Dictionary<int, bool>();
        }

        #endregion

        #region -- DepartmentType --

        public void InsertNode(DepartmentTypeClientData depClientData)
        {
            Root.TreeList.BeginUpdate();
            TreeListNodeEx depNode = (TreeListNodeEx)Root.TreeList.AppendNode(null, Root);
            depNode.LayerName = depClientData.Name;
            depNode.TypeNodeLeaf = TreeListNodeExType.RouteDepartmetTypeRoot;
            depNode.Tag = depClientData.Code;
            Root.TreeList.BeginSort();
            Root.TreeList.EndSort();
            Root.TreeList.EndUpdate();

        }

        public void DeleteNode(DepartmentTypeClientData depClientData)
        {
            for (int i = 0; i < Root.Nodes.Count; i++)
            {
                TreeListNodeEx ndx = Root.Nodes[i] as TreeListNodeEx;
                if (ndx.TypeNodeLeaf == TreeListNodeExType.RouteDepartmetTypeRoot)
                {
                    if ((int)ndx.Tag == depClientData.Code)
                    {
                        Root.TreeList.BeginUpdate();
                        Root.TreeList.DeleteNode(ndx);
                        Root.TreeList.EndUpdate();
                        break;
                    }
                }
            }
        }

        public void UpdateNode(DepartmentTypeClientData depClientData)
        {
            for (int i = 0; i < Root.Nodes.Count; i++)
            {
                TreeListNodeEx ndx = Root.Nodes[i] as TreeListNodeEx;
                if (ndx.TypeNodeLeaf == TreeListNodeExType.RouteDepartmetTypeRoot)
                {
                    if ((int)ndx.Tag == depClientData.Code)
                    {
                        Root.TreeList.BeginUpdate();
                        ndx.LayerName = depClientData.Name;
                        Root.TreeList.BeginSort();
                        Root.TreeList.EndSort();
                        Root.TreeList.EndUpdate();
                        break;
                    }
                }
            }
        }

        #endregion

        #region -- Routes --

        public void InsertNode(RouteClientData route)
        {
            for (int i = 0; i < Root.Nodes.Count; i++)
            {
                TreeListNodeEx ndx = Root.Nodes[i] as TreeListNodeEx;
                if (ndx.TypeNodeLeaf == TreeListNodeExType.RouteDepartmetTypeRoot)
                {
                    if ((int)ndx.Tag == route.Department.Code)
                    {
                        routesOn.Add(route.Code, (ndx.CheckState == CheckState.Checked) ? (true) : (false));
                        Root.TreeList.BeginUpdate();
                        TreeListNodeEx routeNode = (TreeListNodeEx)ndx.TreeList.AppendNode(null, ndx);
                        routeNode.LayerName = route.Name;
                        routeNode.TypeNodeLeaf = TreeListNodeExType.RouteLayer;
                        routeNode.Tag = route.Code;
                        ndx.TreeList.BeginSort();
                        ndx.TreeList.EndSort();
                        ndx.TreeList.EndUpdate();
                        break;
                    }
                }
            }
        }

        public void UpdateNode(RouteClientData route)
        {
            TreeListNodeEx backup = FindSon(route);
            TreeListNodeEx father = FindFather(route);
            Root.TreeList.BeginUpdate();
            backup.LayerName = route.Name;
            Root.TreeList.MoveNode(backup, father);
            Root.TreeList.EndUpdate();
            Root.TreeList.BeginSort();
            Root.TreeList.EndSort();
        }

        public void DeleteNode(RouteClientData route)
        {
            Root.TreeList.BeginUpdate();
            Root.TreeList.DeleteNode(FindSon(route));
            Root.TreeList.EndUpdate();
        }

        private TreeListNodeEx FindSon(RouteClientData route)
        {
            TreeListNodeEx son = null;
            for (int i = 0; i < Root.Nodes.Count; i++)
            {
                TreeListNodeEx ndx = Root.Nodes[i] as TreeListNodeEx;
                if (ndx.TypeNodeLeaf == TreeListNodeExType.RouteDepartmetTypeRoot)
                {
                    for (int j = 0; j < ndx.Nodes.Count; j++)
                    {
                        TreeListNodeEx ndex = ndx.Nodes[j] as TreeListNodeEx;
                        if ((int)ndex.Tag == route.Code)
                        {
                            son = ndex;
                            i = Root.Nodes.Count;
                            break;
                        }
                    }
                }
            }
            return son;
        }

        private TreeListNodeEx FindFather(RouteClientData route)
        {
            TreeListNodeEx father = null;
            for (int i = 0; i < Root.Nodes.Count; i++)
            {
                TreeListNodeEx ndx = Root.Nodes[i] as TreeListNodeEx;
                if (ndx.TypeNodeLeaf == TreeListNodeExType.RouteDepartmetTypeRoot)
                {
                    if ((int)ndx.Tag == route.Department.Code)
                    {
                        father = ndx;
                        break;
                    }
                }
            }
            return father;
        }

        #endregion
    }
}
