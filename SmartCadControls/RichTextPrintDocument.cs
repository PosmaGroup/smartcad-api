using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Printing;
using System.Runtime.InteropServices;

using SmartCadCore.Core;
using SmartCadCore.Common;

namespace SmartCadControls.Controls
{
    public class RichTextPrintDocument : PrintDocument
    {
        private const double anInch = 14.4;

        private int checkPrint;

        public RichTextPrintDocument(RichTextBox richTextBox)
        {
            this.richTextBox = richTextBox;
        }

        private RichTextBox richTextBox;

        public RichTextBox RichTextBox
        {
            get
            {
                return richTextBox;
            }
        }

        private static int Print(RichTextBox richTextBox, int charFrom, int charTo, PrintPageEventArgs e)
        {
            //Calculate the area to render and print
            SmartCadCore.Common.RECT rectToPrint;
            rectToPrint.top = (int)(e.MarginBounds.Top * anInch);
            rectToPrint.bottom = (int)(e.MarginBounds.Bottom * anInch);
            rectToPrint.left = (int)(e.MarginBounds.Left * anInch);
            rectToPrint.right = (int)(e.MarginBounds.Right * anInch);

            //Calculate the size of the page
            SmartCadCore.Common.RECT rectPage;
            rectPage.top = (int)(e.PageBounds.Top * anInch);
            rectPage.bottom = (int)(e.PageBounds.Bottom * anInch);
            rectPage.left = (int)(e.PageBounds.Left * anInch);
            rectPage.right = (int)(e.PageBounds.Right * anInch);

            IntPtr hdc = e.Graphics.GetHdc();

            FORMATRANGE fmtRange;
            fmtRange.chrg.cpMax = charTo;				//Indicate character from to character to 
            fmtRange.chrg.cpMin = charFrom;
            fmtRange.hdc = hdc;                    //Use the same DC for measuring and rendering
            fmtRange.hdcTarget = hdc;              //Point at printer hDC
            fmtRange.rc = rectToPrint;             //Indicate the area on page to print
            fmtRange.rcPage = rectPage;            //Indicate size of page

            IntPtr res = IntPtr.Zero;

            IntPtr wparam = IntPtr.Zero;
            wparam = new IntPtr(1);

            //Get the pointer to the FORMATRANGE structure in memory
            IntPtr lparam = IntPtr.Zero;
            lparam = Marshal.AllocCoTaskMem(Marshal.SizeOf(fmtRange));
            Marshal.StructureToPtr(fmtRange, lparam, false);

            //Send the rendered data for printing 
            res = Win32.SendMessage(richTextBox.Handle, Win32.EM_FORMATRANGE, (IntPtr)wparam, (IntPtr)lparam);

            //Free the block of memory allocated
            Marshal.FreeCoTaskMem(lparam);

            //Release the device context handle obtained by a previous call
            e.Graphics.ReleaseHdc(hdc);

            //Return last + 1 character printer
            return res.ToInt32();
        }

        protected override void OnBeginPrint(PrintEventArgs e)
        {
            base.OnBeginPrint(e);

            checkPrint = 0;
        }

        protected override void OnPrintPage(PrintPageEventArgs e)
        {
            base.OnPrintPage(e);

            // Print the content of RichTextBox. Store the last character printed.
            checkPrint = Print(richTextBox, checkPrint, richTextBox.TextLength, e);

            // Check for more pages
            if (checkPrint < richTextBox.TextLength)
                e.HasMorePages = true;
            else
                e.HasMorePages = false;
        }
    }
}
