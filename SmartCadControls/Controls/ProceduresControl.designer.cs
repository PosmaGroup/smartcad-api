namespace SmartCadControls.Controls
{
    partial class ProceduresControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProceduresControl));
			this.browserProcedures = new System.Windows.Forms.RichTextBox();
			this.browserGenericProcedure = new System.Windows.Forms.RichTextBox();
			this.imageList = new System.Windows.Forms.ImageList(this.components);
			this.groupControlBody = new DevExpress.XtraEditors.GroupControl();
			this.xtraTabControlProcedures = new DevExpress.XtraTab.XtraTabControl();
			this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
			this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
			((System.ComponentModel.ISupportInitialize)(this.groupControlBody)).BeginInit();
			this.groupControlBody.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.xtraTabControlProcedures)).BeginInit();
			this.xtraTabControlProcedures.SuspendLayout();
			this.xtraTabPage1.SuspendLayout();
			this.xtraTabPage2.SuspendLayout();
			this.SuspendLayout();
			// 
			// browserProcedures
			// 
			this.browserProcedures.BackColor = System.Drawing.SystemColors.Window;
			this.browserProcedures.Dock = System.Windows.Forms.DockStyle.Fill;
			this.browserProcedures.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
			this.browserProcedures.Location = new System.Drawing.Point(0, 0);
			this.browserProcedures.MinimumSize = new System.Drawing.Size(20, 20);
			this.browserProcedures.Name = "browserProcedures";
			this.browserProcedures.ReadOnly = true;
			this.browserProcedures.ShortcutsEnabled = false;
			this.browserProcedures.Size = new System.Drawing.Size(383, 127);
			this.browserProcedures.TabIndex = 0;
			this.browserProcedures.Text = "";
			this.browserProcedures.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.browserProcedures_PreviewKeyDown);
			// 
			// browserGenericProcedure
			// 
			this.browserGenericProcedure.BackColor = System.Drawing.SystemColors.Window;
			this.browserGenericProcedure.Dock = System.Windows.Forms.DockStyle.Fill;
			this.browserGenericProcedure.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.browserGenericProcedure.Location = new System.Drawing.Point(0, 0);
			this.browserGenericProcedure.MinimumSize = new System.Drawing.Size(20, 20);
			this.browserGenericProcedure.Name = "browserGenericProcedure";
			this.browserGenericProcedure.ReadOnly = true;
			this.browserGenericProcedure.ShortcutsEnabled = false;
			this.browserGenericProcedure.Size = new System.Drawing.Size(383, 127);
			this.browserGenericProcedure.TabIndex = 0;
			this.browserGenericProcedure.Text = "";
			this.browserGenericProcedure.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.browserGenericProcedure_PreviewKeyDown);
			// 
			// imageList
			// 
			this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
			this.imageList.TransparentColor = System.Drawing.Color.Transparent;
			this.imageList.Images.SetKeyName(0, "procedimientos_por respuesta.gif");
			this.imageList.Images.SetKeyName(1, "procedimientos-genericos.gif");
			// 
			// groupControlBody
			// 
			this.groupControlBody.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(252)))), ((int)(((byte)(226)))));
			this.groupControlBody.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(252)))), ((int)(((byte)(226)))));
			this.groupControlBody.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(94)))), ((int)(((byte)(50)))));
			this.groupControlBody.Appearance.Options.UseBackColor = true;
			this.groupControlBody.Appearance.Options.UseBorderColor = true;
			this.groupControlBody.AppearanceCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(172)))), ((int)(((byte)(68)))));
			this.groupControlBody.AppearanceCaption.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(94)))), ((int)(((byte)(50)))));
			this.groupControlBody.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.groupControlBody.AppearanceCaption.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
			this.groupControlBody.AppearanceCaption.Options.UseBackColor = true;
			this.groupControlBody.AppearanceCaption.Options.UseFont = true;
			this.groupControlBody.Controls.Add(this.xtraTabControlProcedures);
			this.groupControlBody.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupControlBody.Location = new System.Drawing.Point(0, 0);
			this.groupControlBody.LookAndFeel.SkinName = "Blue";
			this.groupControlBody.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
			this.groupControlBody.LookAndFeel.UseDefaultLookAndFeel = false;
			this.groupControlBody.Name = "groupControlBody";
			this.groupControlBody.Size = new System.Drawing.Size(396, 180);
			this.groupControlBody.TabIndex = 0;
			// 
			// xtraTabControlProcedures
			// 
			this.xtraTabControlProcedures.Dock = System.Windows.Forms.DockStyle.Fill;
			this.xtraTabControlProcedures.Images = this.imageList;
			this.xtraTabControlProcedures.Location = new System.Drawing.Point(2, 19);
			this.xtraTabControlProcedures.LookAndFeel.SkinName = "Black";
			this.xtraTabControlProcedures.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
			this.xtraTabControlProcedures.Name = "xtraTabControlProcedures";
			this.xtraTabControlProcedures.SelectedTabPage = this.xtraTabPage1;
			this.xtraTabControlProcedures.Size = new System.Drawing.Size(392, 159);
			this.xtraTabControlProcedures.TabIndex = 42;
			this.xtraTabControlProcedures.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
			// 
			// xtraTabPage1
			// 
			this.xtraTabPage1.Controls.Add(this.browserProcedures);
			this.xtraTabPage1.ImageIndex = 0;
			this.xtraTabPage1.Name = "xtraTabPage1";
			this.xtraTabPage1.Size = new System.Drawing.Size(383, 127);
			this.xtraTabPage1.Text = "xtraTabPage1";
			// 
			// xtraTabPage2
			// 
			this.xtraTabPage2.Controls.Add(this.browserGenericProcedure);
			this.xtraTabPage2.ImageIndex = 1;
			this.xtraTabPage2.Name = "xtraTabPage2";
			this.xtraTabPage2.Size = new System.Drawing.Size(383, 127);
			this.xtraTabPage2.Text = "xtraTabPage2";
			// 
			// ProceduresControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.groupControlBody);
			this.Name = "ProceduresControl";
			this.Size = new System.Drawing.Size(396, 180);
			this.Load += new System.EventHandler(this.ProceduresControl_Load);
			((System.ComponentModel.ISupportInitialize)(this.groupControlBody)).EndInit();
			this.groupControlBody.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.xtraTabControlProcedures)).EndInit();
			this.xtraTabControlProcedures.ResumeLayout(false);
			this.xtraTabPage1.ResumeLayout(false);
			this.xtraTabPage2.ResumeLayout(false);
			this.ResumeLayout(false);

        }

        #endregion

		private DevExpress.XtraEditors.GroupControl groupControlBody;
        private System.Windows.Forms.ImageList imageList;
        public System.Windows.Forms.RichTextBox browserProcedures;
        public System.Windows.Forms.RichTextBox browserGenericProcedure;
		public DevExpress.XtraTab.XtraTabControl xtraTabControlProcedures;
		public DevExpress.XtraTab.XtraTabPage xtraTabPage1;
		public DevExpress.XtraTab.XtraTabPage xtraTabPage2;
    }
}
