namespace SmartCadControls.Controls
{
	partial class OperatorChatControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.SupervisorPicture = new DevExpress.XtraEditors.PictureEdit();
            this.OperatorChatControlConvertedLayout = new DevExpress.XtraLayout.LayoutControl();
            this.sendSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this.OperatorPicture = new DevExpress.XtraEditors.PictureEdit();
            this.richTextEditWriter = new System.Windows.Forms.RichTextBox();
            this.richTextEditReader = new System.Windows.Forms.RichTextBox();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.memoEditReaderitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutConverter1 = new DevExpress.XtraLayout.Converter.LayoutConverter(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.SupervisorPicture.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OperatorChatControlConvertedLayout)).BeginInit();
            this.OperatorChatControlConvertedLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OperatorPicture.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditReaderitem)).BeginInit();
            this.SuspendLayout();
            // 
            // SupervisorPicture
            // 
            this.SupervisorPicture.Location = new System.Drawing.Point(367, 7);
            this.SupervisorPicture.Name = "SupervisorPicture";
            this.SupervisorPicture.Size = new System.Drawing.Size(99, 126);
            this.SupervisorPicture.StyleController = this.OperatorChatControlConvertedLayout;
            this.SupervisorPicture.TabIndex = 7;
            // 
            // OperatorChatControlConvertedLayout
            // 
            this.OperatorChatControlConvertedLayout.Appearance.DisabledLayoutGroupCaption.ForeColor = System.Drawing.SystemColors.GrayText;
            this.OperatorChatControlConvertedLayout.Appearance.DisabledLayoutGroupCaption.Options.UseForeColor = true;
            this.OperatorChatControlConvertedLayout.Appearance.DisabledLayoutItem.ForeColor = System.Drawing.SystemColors.GrayText;
            this.OperatorChatControlConvertedLayout.Appearance.DisabledLayoutItem.Options.UseForeColor = true;
            this.OperatorChatControlConvertedLayout.Controls.Add(this.sendSimpleButton);
            this.OperatorChatControlConvertedLayout.Controls.Add(this.SupervisorPicture);
            this.OperatorChatControlConvertedLayout.Controls.Add(this.OperatorPicture);
            this.OperatorChatControlConvertedLayout.Controls.Add(this.richTextEditWriter);
            this.OperatorChatControlConvertedLayout.Controls.Add(this.richTextEditReader);
            this.OperatorChatControlConvertedLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OperatorChatControlConvertedLayout.Location = new System.Drawing.Point(0, 0);
            this.OperatorChatControlConvertedLayout.LookAndFeel.SkinName = "Blue";
            this.OperatorChatControlConvertedLayout.LookAndFeel.UseDefaultLookAndFeel = false;
            this.OperatorChatControlConvertedLayout.Name = "OperatorChatControlConvertedLayout";
            this.OperatorChatControlConvertedLayout.OptionsFocus.AllowFocusControlOnActivatedTabPage = true;
            this.OperatorChatControlConvertedLayout.Root = this.layoutControlGroup1;
            this.OperatorChatControlConvertedLayout.Size = new System.Drawing.Size(472, 472);
            this.OperatorChatControlConvertedLayout.TabIndex = 10;
            // 
            // sendSimpleButton
            // 
            this.sendSimpleButton.Enabled = false;
            this.sendSimpleButton.Location = new System.Drawing.Point(367, 447);
            this.sendSimpleButton.Name = "sendSimpleButton";
            this.sendSimpleButton.Size = new System.Drawing.Size(99, 12);
            this.sendSimpleButton.StyleController = this.OperatorChatControlConvertedLayout;
            this.sendSimpleButton.TabIndex = 9;
            this.sendSimpleButton.Click += new System.EventHandler(this.sendSimpleButton_Click);
            // 
            // OperatorPicture
            // 
            this.OperatorPicture.Location = new System.Drawing.Point(367, 307);
            this.OperatorPicture.Name = "OperatorPicture";
            this.OperatorPicture.Size = new System.Drawing.Size(99, 129);
            this.OperatorPicture.StyleController = this.OperatorChatControlConvertedLayout;
            this.OperatorPicture.TabIndex = 6;
            // 
            // richTextEditWriter
            // 
            this.richTextEditWriter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.richTextEditWriter.Location = new System.Drawing.Point(7, 307);
            this.richTextEditWriter.Name = "richTextEditWriter";
            this.richTextEditWriter.Size = new System.Drawing.Size(349, 129);
            this.richTextEditWriter.TabIndex = 5;
            this.richTextEditWriter.Text = "";
            this.richTextEditWriter.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.richTextEditWriter_PreviewKeyDown);
            this.richTextEditWriter.TextChanged += new System.EventHandler(this.richTextEditWriter_TextChanged);
            // 
            // richTextEditReader
            // 
            this.richTextEditReader.BackColor = System.Drawing.Color.White;
            this.richTextEditReader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.richTextEditReader.Location = new System.Drawing.Point(7, 7);
            this.richTextEditReader.Name = "richTextEditReader";
            this.richTextEditReader.ReadOnly = true;
            this.richTextEditReader.Size = new System.Drawing.Size(349, 289);
            this.richTextEditReader.TabIndex = 4;
            this.richTextEditReader.Text = "";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.DefaultLayoutType = DevExpress.XtraLayout.Utils.LayoutType.Horizontal;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.memoEditReaderitem});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(472, 472);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.sendSimpleButton;
            this.layoutControlItem1.CustomizationFormText = "sendSimpleButtonitem";
            this.layoutControlItem1.Location = new System.Drawing.Point(360, 440);
            this.layoutControlItem1.Name = "sendSimpleButtonitem";
            this.layoutControlItem1.Size = new System.Drawing.Size(110, 30);
            this.layoutControlItem1.Text = "sendSimpleButtonitem";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.SupervisorPicture;
            this.layoutControlItem2.CustomizationFormText = "SupervisorPictureitem";
            this.layoutControlItem2.Location = new System.Drawing.Point(360, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(110, 0);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(110, 31);
            this.layoutControlItem2.Name = "SupervisorPictureitem";
            this.layoutControlItem2.Size = new System.Drawing.Size(110, 137);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "SupervisorPictureitem";
            this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.OperatorPicture;
            this.layoutControlItem3.CustomizationFormText = "OperatorPictureitem";
            this.layoutControlItem3.Location = new System.Drawing.Point(360, 300);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(110, 0);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(110, 31);
            this.layoutControlItem3.Name = "OperatorPictureitem";
            this.layoutControlItem3.Size = new System.Drawing.Size(110, 140);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "OperatorPictureitem";
            this.layoutControlItem3.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.richTextEditWriter;
            this.layoutControlItem4.CustomizationFormText = "richTextEditWriteritem";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 300);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(21, 27);
            this.layoutControlItem4.Name = "richTextEditWriteritem";
            this.layoutControlItem4.Size = new System.Drawing.Size(360, 140);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "richTextEditWriteritem";
            this.layoutControlItem4.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            this.layoutControlItem4.TrimClientAreaToControl = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 440);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(110, 30);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(360, 30);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(360, 137);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(110, 0);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(110, 30);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(110, 163);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // memoEditReaderitem
            // 
            this.memoEditReaderitem.Control = this.richTextEditReader;
            this.memoEditReaderitem.CustomizationFormText = "memoEditReaderitem";
            this.memoEditReaderitem.Location = new System.Drawing.Point(0, 0);
            this.memoEditReaderitem.MinSize = new System.Drawing.Size(31, 31);
            this.memoEditReaderitem.Name = "memoEditReaderitem";
            this.memoEditReaderitem.Size = new System.Drawing.Size(360, 300);
            this.memoEditReaderitem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.memoEditReaderitem.Text = "memoEditReaderitem";
            this.memoEditReaderitem.TextLocation = DevExpress.Utils.Locations.Left;
            this.memoEditReaderitem.TextSize = new System.Drawing.Size(0, 0);
            this.memoEditReaderitem.TextToControlDistance = 0;
            this.memoEditReaderitem.TextVisible = false;
            this.memoEditReaderitem.TrimClientAreaToControl = false;
            // 
            // OperatorChatControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.OperatorChatControlConvertedLayout);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "OperatorChatControl";
            this.Size = new System.Drawing.Size(472, 472);
            ((System.ComponentModel.ISupportInitialize)(this.SupervisorPicture.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OperatorChatControlConvertedLayout)).EndInit();
            this.OperatorChatControlConvertedLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.OperatorPicture.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditReaderitem)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private DevExpress.XtraEditors.PictureEdit SupervisorPicture;
		private DevExpress.XtraEditors.PictureEdit OperatorPicture;
		private DevExpress.XtraEditors.SimpleButton sendSimpleButton;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
		private DevExpress.XtraLayout.Converter.LayoutConverter layoutConverter1;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
		private System.Windows.Forms.RichTextBox richTextEditReader;
		private DevExpress.XtraLayout.LayoutControlItem memoEditReaderitem;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
		private System.Windows.Forms.RichTextBox richTextEditWriter;
		public DevExpress.XtraLayout.LayoutControl OperatorChatControlConvertedLayout;
		public DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
	}
}
