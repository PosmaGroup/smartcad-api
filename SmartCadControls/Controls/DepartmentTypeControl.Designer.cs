
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
namespace SmartCadControls.Controls
{
    partial class DepartmentTypesControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.radioButtonExPriority2 = new RadioButtonEx();
            this.radioButtonExPriority3 = new RadioButtonEx();
            this.toolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.radioButtonExPriority1 = new RadioButtonEx();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.gridControlExDepartmentType = new GridControlEx();
            this.gridViewExDepartmentType = new GridViewEx();
            this.checkBoxDepartmentType = new CheckBoxEx2();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemDepartmentCheckBox = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemRadioButton1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemRadioButton2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemRadioButton3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemGridControlEx = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExDepartmentType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExDepartmentType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartmentCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemRadioButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemRadioButton2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemRadioButton3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGridControlEx)).BeginInit();
            this.SuspendLayout();
            // 
            // radioButtonExPriority2
            // 
            this.radioButtonExPriority2.Enabled = false;
            this.radioButtonExPriority2.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.radioButtonExPriority2.Location = new System.Drawing.Point(193, 2);
            this.radioButtonExPriority2.Name = "radioButtonExPriority2";
            this.radioButtonExPriority2.Size = new System.Drawing.Size(76, 21);
            this.radioButtonExPriority2.TabIndex = 2;
            this.radioButtonExPriority2.UseVisualStyleBackColor = true;
            this.radioButtonExPriority2.CheckedChanged += new System.EventHandler(this.radioButtonExPriority_CheckedChanged);
            // 
            // radioButtonExPriority3
            // 
            this.radioButtonExPriority3.Enabled = false;
            this.radioButtonExPriority3.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.radioButtonExPriority3.Location = new System.Drawing.Point(273, 2);
            this.radioButtonExPriority3.Name = "radioButtonExPriority3";
            this.radioButtonExPriority3.Size = new System.Drawing.Size(66, 21);
            this.radioButtonExPriority3.TabIndex = 3;
            this.radioButtonExPriority3.UseVisualStyleBackColor = true;
            this.radioButtonExPriority3.CheckedChanged += new System.EventHandler(this.radioButtonExPriority_CheckedChanged);
            // 
            // radioButtonExPriority1
            // 
            this.radioButtonExPriority1.Enabled = false;
            this.radioButtonExPriority1.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.radioButtonExPriority1.Location = new System.Drawing.Point(109, 2);
            this.radioButtonExPriority1.Name = "radioButtonExPriority1";
            this.radioButtonExPriority1.Size = new System.Drawing.Size(80, 21);
            this.radioButtonExPriority1.TabIndex = 1;
            this.radioButtonExPriority1.UseVisualStyleBackColor = true;
            this.radioButtonExPriority1.CheckedChanged += new System.EventHandler(this.radioButtonExPriority_CheckedChanged);
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowCustomizationMenu = false;
            this.layoutControl1.Controls.Add(this.gridControlExDepartmentType);
            this.layoutControl1.Controls.Add(this.radioButtonExPriority3);
            this.layoutControl1.Controls.Add(this.checkBoxDepartmentType);
            this.layoutControl1.Controls.Add(this.radioButtonExPriority2);
            this.layoutControl1.Controls.Add(this.radioButtonExPriority1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.layoutControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsFocus.EnableAutoTabOrder = false;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(341, 139);
            this.layoutControl1.TabIndex = 5;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // gridControlExDepartmentType
            // 
            this.gridControlExDepartmentType.EnableAutoFilter = false;
            this.gridControlExDepartmentType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExDepartmentType.Location = new System.Drawing.Point(5, 30);
            this.gridControlExDepartmentType.LookAndFeel.SkinName = "Blue";
            this.gridControlExDepartmentType.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gridControlExDepartmentType.MainView = this.gridViewExDepartmentType;
            this.gridControlExDepartmentType.Name = "gridControlExDepartmentType";
            this.gridControlExDepartmentType.Size = new System.Drawing.Size(331, 104);
            this.gridControlExDepartmentType.TabIndex = 4;
            this.gridControlExDepartmentType.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExDepartmentType});
            this.gridControlExDepartmentType.ViewTotalRows = false;
            // 
            // gridViewExDepartmentType
            // 
            this.gridViewExDepartmentType.AllowFocusedRowChanged = true;
            this.gridViewExDepartmentType.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExDepartmentType.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExDepartmentType.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExDepartmentType.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExDepartmentType.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewExDepartmentType.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExDepartmentType.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExDepartmentType.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExDepartmentType.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExDepartmentType.EnablePreviewLineForFocusedRow = true;
            this.gridViewExDepartmentType.GridControl = this.gridControlExDepartmentType;
            this.gridViewExDepartmentType.Name = "gridViewExDepartmentType";
            this.gridViewExDepartmentType.OptionsLayout.Columns.RemoveOldColumns = false;
            this.gridViewExDepartmentType.OptionsMenu.EnableColumnMenu = false;
            this.gridViewExDepartmentType.OptionsMenu.EnableFooterMenu = false;
            this.gridViewExDepartmentType.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridViewExDepartmentType.OptionsNavigation.AutoMoveRowFocus = false;
            this.gridViewExDepartmentType.OptionsNavigation.UseTabKey = false;
            this.gridViewExDepartmentType.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExDepartmentType.OptionsView.AutoCalcPreviewLineCount = true;
            this.gridViewExDepartmentType.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExDepartmentType.OptionsView.ShowGroupPanel = false;
            this.gridViewExDepartmentType.OptionsView.ShowIndicator = false;
            this.gridViewExDepartmentType.OptionsView.ShowPreview = true;
            this.gridViewExDepartmentType.PaintStyleName = "Skin";
            this.gridViewExDepartmentType.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowForFocusedRow;
            this.gridViewExDepartmentType.ViewTotalRows = false;
            this.gridViewExDepartmentType.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewExDepartmentType_CellValueChanging);
            // 
            // checkBoxDepartmentType
            // 
            this.checkBoxDepartmentType.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxDepartmentType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.checkBoxDepartmentType.Location = new System.Drawing.Point(2, 2);
            this.checkBoxDepartmentType.Name = "checkBoxDepartmentType";
            this.checkBoxDepartmentType.NotVisibleText = "";
            this.checkBoxDepartmentType.Size = new System.Drawing.Size(35, 21);
            this.checkBoxDepartmentType.TabIndex = 0;
            this.checkBoxDepartmentType.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.checkBoxDepartmentType.UseVisualStyleBackColor = true;
            this.checkBoxDepartmentType.CheckedChanged += new System.EventHandler(this.checkBoxDepartmentType_CheckedChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.BackColor = System.Drawing.Color.Transparent;
            this.layoutControlGroup1.AppearanceGroup.Options.UseBackColor = true;
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemDepartmentCheckBox,
            this.layoutControlItemRadioButton1,
            this.layoutControlItemRadioButton2,
            this.layoutControlItemRadioButton3,
            this.layoutControlItemGridControlEx});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup1.Size = new System.Drawing.Size(341, 139);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItemDepartmentCheckBox
            // 
            this.layoutControlItemDepartmentCheckBox.Control = this.checkBoxDepartmentType;
            this.layoutControlItemDepartmentCheckBox.CustomizationFormText = "layoutControlItemDepartmentCheckBox";
            this.layoutControlItemDepartmentCheckBox.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemDepartmentCheckBox.MaxSize = new System.Drawing.Size(107, 25);
            this.layoutControlItemDepartmentCheckBox.MinSize = new System.Drawing.Size(107, 25);
            this.layoutControlItemDepartmentCheckBox.Name = "layoutControlItemDepartmentCheckBox";
            this.layoutControlItemDepartmentCheckBox.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 8, 2, 2);
            this.layoutControlItemDepartmentCheckBox.Size = new System.Drawing.Size(107, 25);
            this.layoutControlItemDepartmentCheckBox.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemDepartmentCheckBox.Text = "Department";
            this.layoutControlItemDepartmentCheckBox.TextLocation = DevExpress.Utils.Locations.Right;
            this.layoutControlItemDepartmentCheckBox.TextSize = new System.Drawing.Size(57, 13);
            this.layoutControlItemDepartmentCheckBox.TextToControlDistance = 5;
            // 
            // layoutControlItemRadioButton1
            // 
            this.layoutControlItemRadioButton1.Control = this.radioButtonExPriority1;
            this.layoutControlItemRadioButton1.CustomizationFormText = "layoutControlItemRadioButton1";
            this.layoutControlItemRadioButton1.Location = new System.Drawing.Point(107, 0);
            this.layoutControlItemRadioButton1.MaxSize = new System.Drawing.Size(84, 25);
            this.layoutControlItemRadioButton1.MinSize = new System.Drawing.Size(84, 25);
            this.layoutControlItemRadioButton1.Name = "layoutControlItemRadioButton1";
            this.layoutControlItemRadioButton1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItemRadioButton1.Size = new System.Drawing.Size(84, 25);
            this.layoutControlItemRadioButton1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemRadioButton1.Text = "1";
            this.layoutControlItemRadioButton1.TextLocation = DevExpress.Utils.Locations.Right;
            this.layoutControlItemRadioButton1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemRadioButton1.TextToControlDistance = 0;
            this.layoutControlItemRadioButton1.TextVisible = false;
            // 
            // layoutControlItemRadioButton2
            // 
            this.layoutControlItemRadioButton2.Control = this.radioButtonExPriority2;
            this.layoutControlItemRadioButton2.CustomizationFormText = "2";
            this.layoutControlItemRadioButton2.Location = new System.Drawing.Point(191, 0);
            this.layoutControlItemRadioButton2.MaxSize = new System.Drawing.Size(110, 25);
            this.layoutControlItemRadioButton2.MinSize = new System.Drawing.Size(80, 25);
            this.layoutControlItemRadioButton2.Name = "layoutControlItemRadioButton2";
            this.layoutControlItemRadioButton2.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItemRadioButton2.Size = new System.Drawing.Size(80, 25);
            this.layoutControlItemRadioButton2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemRadioButton2.Text = "2";
            this.layoutControlItemRadioButton2.TextLocation = DevExpress.Utils.Locations.Right;
            this.layoutControlItemRadioButton2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemRadioButton2.TextToControlDistance = 0;
            this.layoutControlItemRadioButton2.TextVisible = false;
            // 
            // layoutControlItemRadioButton3
            // 
            this.layoutControlItemRadioButton3.Control = this.radioButtonExPriority3;
            this.layoutControlItemRadioButton3.CustomizationFormText = "layoutControlItemRadioButton3";
            this.layoutControlItemRadioButton3.Location = new System.Drawing.Point(271, 0);
            this.layoutControlItemRadioButton3.MaxSize = new System.Drawing.Size(70, 0);
            this.layoutControlItemRadioButton3.MinSize = new System.Drawing.Size(70, 25);
            this.layoutControlItemRadioButton3.Name = "layoutControlItemRadioButton3";
            this.layoutControlItemRadioButton3.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItemRadioButton3.Size = new System.Drawing.Size(70, 25);
            this.layoutControlItemRadioButton3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemRadioButton3.Text = "3";
            this.layoutControlItemRadioButton3.TextLocation = DevExpress.Utils.Locations.Right;
            this.layoutControlItemRadioButton3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemRadioButton3.TextToControlDistance = 0;
            this.layoutControlItemRadioButton3.TextVisible = false;
            // 
            // layoutControlItemGridControlEx
            // 
            this.layoutControlItemGridControlEx.Control = this.gridControlExDepartmentType;
            this.layoutControlItemGridControlEx.CustomizationFormText = "layoutControlItemGridControlEx";
            this.layoutControlItemGridControlEx.Location = new System.Drawing.Point(0, 25);
            this.layoutControlItemGridControlEx.Name = "layoutControlItemGridControlEx";
            this.layoutControlItemGridControlEx.Size = new System.Drawing.Size(341, 114);
            this.layoutControlItemGridControlEx.Text = "layoutControlItemGridControlEx";
            this.layoutControlItemGridControlEx.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemGridControlEx.TextToControlDistance = 0;
            this.layoutControlItemGridControlEx.TextVisible = false;
            // 
            // DepartmentTypesControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.layoutControl1);
            this.Name = "DepartmentTypesControl";
            this.Size = new System.Drawing.Size(341, 139);
            this.Load += new System.EventHandler(this.DepartmentTypesControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExDepartmentType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExDepartmentType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartmentCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemRadioButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemRadioButton2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemRadioButton3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGridControlEx)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion


        private CheckBoxEx2 checkBoxDepartmentType;
        private RadioButtonEx radioButtonExPriority2;
        private RadioButtonEx radioButtonExPriority3;
		private System.Windows.Forms.ToolTip toolTipMain;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDepartmentCheckBox;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemRadioButton2;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemRadioButton3;
		public DevExpress.XtraLayout.LayoutControl layoutControl1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemGridControlEx;
		public GridControlEx gridControlExDepartmentType;
		public GridViewEx gridViewExDepartmentType;
        private RadioButtonEx radioButtonExPriority1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemRadioButton1;
    }
}
