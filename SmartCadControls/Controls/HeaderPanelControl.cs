using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace SmartCadControls.Controls
{
    public partial class HeaderPanelControl : UserControl
    {
        #region Fields

        private string activeColor;
        private string activeHeaderImageName;
        private string activeNumberImageName;

        private string inactiveColor;
        private string inactiveHeaderImageName;
        private string inactiveNumberImageName;

        private bool active;

        #endregion

        public HeaderPanelControl()
        {
            InitializeComponent();           
        }

        #region Properties

        [RefreshProperties(RefreshProperties.All),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
        Browsable(false)]
        public string ActiveColor
        {
            get
            {
				return activeColor;
            }
            set
            {
				activeColor = value;
            }
        }

        [RefreshProperties(RefreshProperties.All),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
        Browsable(false)]
        public string ActiveHeaderImageName
        {
            get
            {
                return activeHeaderImageName;
            }
            set
            {
                activeHeaderImageName = value;
            }
        }

        [RefreshProperties(RefreshProperties.All),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
        Browsable(false)]
        public string ActiveNumberImageName
        {
            get
            {
                return activeNumberImageName;
            }
            set
            {
                activeNumberImageName = value;
            }
        }

        [RefreshProperties(RefreshProperties.All),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
        Browsable(false)]
        public string InactiveColor
        {
			get
			{
				return inactiveColor;
			}
			set
			{
				inactiveColor = value;
			}
        }

        [RefreshProperties(RefreshProperties.All),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
        Browsable(false)]
        public string InactiveHeaderImageName
        {
            get
            {
                return inactiveHeaderImageName;
            }
            set
            {
                inactiveHeaderImageName = value;
            }
        }

        [RefreshProperties(RefreshProperties.All),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
        Browsable(false)]
        public string InactiveNumberImageName
        {
            get
            {
                return inactiveNumberImageName;
            }
            set
            {
                inactiveNumberImageName = value;
            }
        }

        #endregion

        [RefreshProperties(RefreshProperties.All),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
        Browsable(false)]
        public virtual bool Active
        {
            get
            {
                return active;
            }
            set
            {
                active = value;                
            }
        }

		//private string GetHeaderImageName()
        //{
        //    return active ? ActiveHeaderImageName : InactiveHeaderImageName;
        //}

        protected string GetNumberImageName()
        {
            return active ? ActiveNumberImageName : InactiveNumberImageName;
        }

        private void toolStrip_Layout(object sender, LayoutEventArgs e)
        {
            //this.toolStripLabel.Size = new Size(this.toolStrip.Size.Width, this.toolStripLabel.Size.Height);
            //this.toolStrip.Size = new Size(this.Size.Width, this.toolStrip.Size.Height);

        }

        private void HeaderPanelControl_Load(object sender, EventArgs e)
        {
            //this.toolStrip.Enabled = false;
        }



		public static DevExpress.LookAndFeel.LookAndFeelStyle GetStyle(string activeColor)
		{
			switch (activeColor)
			{
				case "Flat":
					return DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
				case "Office2003":
					return DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
				case "Style3D":
					return DevExpress.LookAndFeel.LookAndFeelStyle.Style3D;
				case "UltraFlat":
					return DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
				default:
					return DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
			}
		}
    }
}
