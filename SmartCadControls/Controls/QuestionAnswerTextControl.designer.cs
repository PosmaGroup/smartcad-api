namespace SmartCadControls.Controls
{
    partial class QuestionAnswerTextControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QuestionAnswerTextControl));
            this.textBoxExNLQuestions = new System.Windows.Forms.RichTextBox();
            this.buttonPrintPhoneReportText = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.buttonSavePhoneReportText = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.saveFileDialogMain = new System.Windows.Forms.SaveFileDialog();
            this.printDialogMain = new System.Windows.Forms.PrintDialog();
            this.toolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.groupControlBody = new DevExpress.XtraEditors.GroupControl();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlBody)).BeginInit();
            this.groupControlBody.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxExNLQuestions
            // 
            this.textBoxExNLQuestions.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxExNLQuestions.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxExNLQuestions.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.textBoxExNLQuestions.Location = new System.Drawing.Point(5, 38);
            this.textBoxExNLQuestions.Name = "textBoxExNLQuestions";
            this.textBoxExNLQuestions.ReadOnly = true;
            this.textBoxExNLQuestions.ShortcutsEnabled = false;
            this.textBoxExNLQuestions.Size = new System.Drawing.Size(254, 134);
            this.textBoxExNLQuestions.TabIndex = 43;
            this.textBoxExNLQuestions.Text = "";
            this.textBoxExNLQuestions.TextChanged += new System.EventHandler(this.textBoxExNLQuestions_TextChanged);
            this.textBoxExNLQuestions.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.textBoxExNLQuestions_PreviewKeyDown);
            // 
            // buttonPrintPhoneReportText
            // 
            this.buttonPrintPhoneReportText.Enabled = false;
            this.buttonPrintPhoneReportText.Image = ((System.Drawing.Image)(resources.GetObject("buttonPrintPhoneReportText.Image")));
            this.buttonPrintPhoneReportText.Location = new System.Drawing.Point(35, 2);
            this.buttonPrintPhoneReportText.Name = "buttonPrintPhoneReportText";
            this.buttonPrintPhoneReportText.Size = new System.Drawing.Size(29, 29);
            this.buttonPrintPhoneReportText.TabIndex = 42;
            this.toolTipMain.SetToolTip(this.buttonPrintPhoneReportText, "Imprimir");
            this.buttonPrintPhoneReportText.Click += new System.EventHandler(this.buttonPrintPhoneReportText_Click);
            this.buttonPrintPhoneReportText.MouseLeave += new System.EventHandler(this.questionAnswerButton_MouseLeave);
            this.buttonPrintPhoneReportText.MouseHover += new System.EventHandler(this.questionAnswerButton_MouseHover);
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowCustomizationMenu = false;
            this.layoutControl1.Controls.Add(this.buttonSavePhoneReportText);
            this.layoutControl1.Controls.Add(this.textBoxExNLQuestions);
            this.layoutControl1.Controls.Add(this.buttonPrintPhoneReportText);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 19);
            this.layoutControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.layoutControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsView.ShareLookAndFeelWithChildren = false;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(264, 177);
            this.layoutControl1.TabIndex = 44;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // buttonSavePhoneReportText
            // 
            this.buttonSavePhoneReportText.Enabled = false;
            this.buttonSavePhoneReportText.Image = ((System.Drawing.Image)(resources.GetObject("buttonSavePhoneReportText.Image")));
            this.buttonSavePhoneReportText.Location = new System.Drawing.Point(2, 2);
            this.buttonSavePhoneReportText.Name = "buttonSavePhoneReportText";
            this.buttonSavePhoneReportText.Size = new System.Drawing.Size(29, 29);
            this.buttonSavePhoneReportText.TabIndex = 41;
            this.toolTipMain.SetToolTip(this.buttonSavePhoneReportText, "Guardar");
            this.buttonSavePhoneReportText.Click += new System.EventHandler(this.buttonSavePhoneReportText_Click);
            this.buttonSavePhoneReportText.MouseLeave += new System.EventHandler(this.questionAnswerButton_MouseLeave);
            this.buttonSavePhoneReportText.MouseHover += new System.EventHandler(this.questionAnswerButton_MouseHover);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(252)))), ((int)(((byte)(226)))));
            this.layoutControlGroup1.AppearanceGroup.Options.UseBackColor = true;
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.emptySpaceItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup1.Size = new System.Drawing.Size(264, 177);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.buttonSavePhoneReportText;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(33, 33);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(33, 33);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem1.Size = new System.Drawing.Size(33, 33);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.buttonPrintPhoneReportText;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(33, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(33, 33);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(33, 33);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem2.Size = new System.Drawing.Size(33, 33);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.textBoxExNLQuestions;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 33);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(264, 144);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(66, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(198, 33);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // printDialogMain
            // 
            this.printDialogMain.UseEXDialog = true;
            // 
            // groupControlBody
            // 
            this.groupControlBody.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(252)))), ((int)(((byte)(226)))));
            this.groupControlBody.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(252)))), ((int)(((byte)(226)))));
            this.groupControlBody.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(94)))), ((int)(((byte)(50)))));
            this.groupControlBody.Appearance.ForeColor = System.Drawing.Color.Red;
            this.groupControlBody.Appearance.Options.UseBackColor = true;
            this.groupControlBody.Appearance.Options.UseBorderColor = true;
            this.groupControlBody.Appearance.Options.UseForeColor = true;
            this.groupControlBody.AppearanceCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(172)))), ((int)(((byte)(68)))));
            this.groupControlBody.AppearanceCaption.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(94)))), ((int)(((byte)(50)))));
            this.groupControlBody.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControlBody.AppearanceCaption.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.groupControlBody.AppearanceCaption.Options.UseBackColor = true;
            this.groupControlBody.AppearanceCaption.Options.UseFont = true;
            this.groupControlBody.Controls.Add(this.layoutControl1);
            this.groupControlBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControlBody.Location = new System.Drawing.Point(0, 0);
            this.groupControlBody.LookAndFeel.SkinName = "Blue";
            this.groupControlBody.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.groupControlBody.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControlBody.Name = "groupControlBody";
            this.groupControlBody.Size = new System.Drawing.Size(268, 198);
            this.groupControlBody.TabIndex = 0;
            // 
            // QuestionAnswerTextControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupControlBody);
            this.Name = "QuestionAnswerTextControl";
            this.Size = new System.Drawing.Size(268, 198);
            this.Load += new System.EventHandler(this.QuestionAnswerTextControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlBody)).EndInit();
            this.groupControlBody.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox textBoxExNLQuestions;
        private DevExpress.XtraEditors.SimpleButton buttonPrintPhoneReportText;
		private DevExpress.XtraEditors.SimpleButton buttonSavePhoneReportText;
        private System.Windows.Forms.SaveFileDialog saveFileDialogMain;
        private System.Windows.Forms.PrintDialog printDialogMain;
		private System.Windows.Forms.ToolTip toolTipMain;
		private DevExpress.XtraEditors.GroupControl groupControlBody;
		private DevExpress.XtraLayout.LayoutControl layoutControl1;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
    }
}
