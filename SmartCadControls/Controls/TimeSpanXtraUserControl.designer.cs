namespace SmartCadControls.Controls
{
    partial class TimeSpanXtraUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.spinEditSeconds = new DevExpress.XtraEditors.SpinEdit();
            this.spinEditMinutes = new DevExpress.XtraEditors.SpinEdit();
            this.spinEditHours = new DevExpress.XtraEditors.SpinEdit();
            this.spinEditDays = new DevExpress.XtraEditors.SpinEdit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditSeconds.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditMinutes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditHours.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditDays.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // spinEditSeconds
            // 
            this.spinEditSeconds.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditSeconds.Location = new System.Drawing.Point(147, 3);
            this.spinEditSeconds.Name = "spinEditSeconds";
            this.spinEditSeconds.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditSeconds.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.spinEditSeconds.Properties.Mask.EditMask = "([0-9]|[1-5][0-9])";
            this.spinEditSeconds.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.spinEditSeconds.Properties.MaxValue = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.spinEditSeconds.Size = new System.Drawing.Size(42, 20);
            this.spinEditSeconds.TabIndex = 10;
            this.spinEditSeconds.EditValueChanged += new System.EventHandler(this.TimeSpanXtraUserControl_EditValueChanged);
            // 
            // spinEditMinutes
            // 
            this.spinEditMinutes.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditMinutes.Location = new System.Drawing.Point(99, 3);
            this.spinEditMinutes.Name = "spinEditMinutes";
            this.spinEditMinutes.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditMinutes.Properties.Mask.EditMask = "([0-9]|[1-5][0-9])";
            this.spinEditMinutes.Properties.Mask.IgnoreMaskBlank = false;
            this.spinEditMinutes.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.spinEditMinutes.Properties.MaxValue = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.spinEditMinutes.Size = new System.Drawing.Size(42, 20);
            this.spinEditMinutes.TabIndex = 9;
            this.spinEditMinutes.EditValueChanged += new System.EventHandler(this.TimeSpanXtraUserControl_EditValueChanged);
            // 
            // spinEditHours
            // 
            this.spinEditHours.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditHours.Location = new System.Drawing.Point(51, 3);
            this.spinEditHours.Name = "spinEditHours";
            this.spinEditHours.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditHours.Properties.Mask.EditMask = "([0-9]|[0-1][0-9]|[0-2][0-3])";
            this.spinEditHours.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.spinEditHours.Properties.MaxValue = new decimal(new int[] {
            23,
            0,
            0,
            0});
            this.spinEditHours.Size = new System.Drawing.Size(42, 20);
            this.spinEditHours.TabIndex = 8;
            this.spinEditHours.EditValueChanged += new System.EventHandler(this.TimeSpanXtraUserControl_EditValueChanged);
            // 
            // spinEditDays
            // 
            this.spinEditDays.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditDays.Location = new System.Drawing.Point(3, 3);
            this.spinEditDays.Name = "spinEditDays";
            this.spinEditDays.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditDays.Properties.Mask.EditMask = "([0-9]|[1-9][0-9]|[1-9][0-9][0-9])";
            this.spinEditDays.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.spinEditDays.Properties.MaxValue = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.spinEditDays.Size = new System.Drawing.Size(42, 20);
            this.spinEditDays.TabIndex = 7;
            this.spinEditDays.EditValueChanged += new System.EventHandler(this.TimeSpanXtraUserControl_EditValueChanged);
            // 
            // TimeSpanXtraUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.spinEditSeconds);
            this.Controls.Add(this.spinEditMinutes);
            this.Controls.Add(this.spinEditHours);
            this.Controls.Add(this.spinEditDays);
            this.Name = "TimeSpanXtraUserControl";
            this.Size = new System.Drawing.Size(192, 27);
            ((System.ComponentModel.ISupportInitialize)(this.spinEditSeconds.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditMinutes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditHours.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditDays.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SpinEdit spinEditSeconds;
        private DevExpress.XtraEditors.SpinEdit spinEditMinutes;
        private DevExpress.XtraEditors.SpinEdit spinEditHours;
        private DevExpress.XtraEditors.SpinEdit spinEditDays;

    }
}
