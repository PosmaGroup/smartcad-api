﻿namespace SmartCadControls.Controls
{
    partial class SoftPhoneControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.bHash = new SimpleButtonEx();
            this.button0 = new SimpleButtonEx();
            this.bAsterisk = new SimpleButtonEx();
            this.button9 = new SimpleButtonEx();
            this.button8 = new SimpleButtonEx();
            this.button7 = new SimpleButtonEx();
            this.button6 = new SimpleButtonEx();
            this.button5 = new SimpleButtonEx();
            this.button4 = new SimpleButtonEx();
            this.button3 = new SimpleButtonEx();
            this.button2 = new SimpleButtonEx();
            this.button1 = new SimpleButtonEx();
            this.cbPhoneEntry = new System.Windows.Forms.ComboBox();
            this.bCall = new SimpleButtonEx();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.bHash);
            this.groupBox1.Controls.Add(this.button0);
            this.groupBox1.Controls.Add(this.bAsterisk);
            this.groupBox1.Controls.Add(this.button9);
            this.groupBox1.Controls.Add(this.button8);
            this.groupBox1.Controls.Add(this.button7);
            this.groupBox1.Controls.Add(this.button6);
            this.groupBox1.Controls.Add(this.button5);
            this.groupBox1.Controls.Add(this.button4);
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Location = new System.Drawing.Point(2, 32);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(157, 153);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            // 
            // bHash
            // 
            this.bHash.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.bHash.Appearance.Options.UseFont = true;
            this.bHash.Location = new System.Drawing.Point(112, 116);
            this.bHash.Name = "bHash";
            this.bHash.Size = new System.Drawing.Size(34, 25);
            this.bHash.TabIndex = 11;
            this.bHash.Text = "#";
            this.bHash.Click += new System.EventHandler(this.buttonDialer_Click);
            this.bHash.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.buttonDialer_KeyPress);
            // 
            // button0
            // 
            this.button0.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.button0.Appearance.Options.UseFont = true;
            this.button0.Location = new System.Drawing.Point(62, 116);
            this.button0.Name = "button0";
            this.button0.Size = new System.Drawing.Size(34, 25);
            this.button0.TabIndex = 10;
            this.button0.Text = "0";
            this.button0.Click += new System.EventHandler(this.buttonDialer_Click);
            this.button0.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.buttonDialer_KeyPress);
            // 
            // bAsterisk
            // 
            this.bAsterisk.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bAsterisk.Appearance.Options.UseFont = true;
            this.bAsterisk.Location = new System.Drawing.Point(12, 116);
            this.bAsterisk.Name = "bAsterisk";
            this.bAsterisk.Size = new System.Drawing.Size(34, 25);
            this.bAsterisk.TabIndex = 9;
            this.bAsterisk.Text = "*";
            this.bAsterisk.Click += new System.EventHandler(this.buttonDialer_Click);
            this.bAsterisk.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.buttonDialer_KeyPress);
            // 
            // button9
            // 
            this.button9.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.button9.Appearance.Options.UseFont = true;
            this.button9.Location = new System.Drawing.Point(112, 81);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(34, 25);
            this.button9.TabIndex = 8;
            this.button9.Text = "9";
            this.button9.Click += new System.EventHandler(this.buttonDialer_Click);
            this.button9.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.buttonDialer_KeyPress);
            // 
            // button8
            // 
            this.button8.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.button8.Appearance.Options.UseFont = true;
            this.button8.Location = new System.Drawing.Point(62, 81);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(34, 25);
            this.button8.TabIndex = 7;
            this.button8.Text = "8";
            this.button8.Click += new System.EventHandler(this.buttonDialer_Click);
            this.button8.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.buttonDialer_KeyPress);
            // 
            // button7
            // 
            this.button7.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.button7.Appearance.Options.UseFont = true;
            this.button7.Location = new System.Drawing.Point(12, 81);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(34, 25);
            this.button7.TabIndex = 6;
            this.button7.Text = "7";
            this.button7.Click += new System.EventHandler(this.buttonDialer_Click);
            this.button7.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.buttonDialer_KeyPress);
            // 
            // button6
            // 
            this.button6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.button6.Appearance.Options.UseFont = true;
            this.button6.Location = new System.Drawing.Point(112, 46);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(34, 25);
            this.button6.TabIndex = 5;
            this.button6.Text = "6";
            this.button6.Click += new System.EventHandler(this.buttonDialer_Click);
            this.button6.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.buttonDialer_KeyPress);
            // 
            // button5
            // 
            this.button5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.button5.Appearance.Options.UseFont = true;
            this.button5.Location = new System.Drawing.Point(62, 46);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(34, 25);
            this.button5.TabIndex = 4;
            this.button5.Text = "5";
            this.button5.Click += new System.EventHandler(this.buttonDialer_Click);
            this.button5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.buttonDialer_KeyPress);
            // 
            // button4
            // 
            this.button4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.button4.Appearance.Options.UseFont = true;
            this.button4.Location = new System.Drawing.Point(12, 46);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(34, 25);
            this.button4.TabIndex = 3;
            this.button4.Text = "4";
            this.button4.Click += new System.EventHandler(this.buttonDialer_Click);
            this.button4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.buttonDialer_KeyPress);
            // 
            // button3
            // 
            this.button3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.button3.Appearance.Options.UseFont = true;
            this.button3.Location = new System.Drawing.Point(112, 11);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(34, 25);
            this.button3.TabIndex = 2;
            this.button3.Text = "3";
            this.button3.Click += new System.EventHandler(this.buttonDialer_Click);
            this.button3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.buttonDialer_KeyPress);
            // 
            // button2
            // 
            this.button2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.button2.Appearance.Options.UseFont = true;
            this.button2.Location = new System.Drawing.Point(62, 11);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(34, 25);
            this.button2.TabIndex = 1;
            this.button2.Text = "2";
            this.button2.Click += new System.EventHandler(this.buttonDialer_Click);
            this.button2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.buttonDialer_KeyPress);
            // 
            // button1
            // 
            this.button1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.button1.Appearance.Options.UseFont = true;
            this.button1.Location = new System.Drawing.Point(12, 11);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(34, 25);
            this.button1.TabIndex = 0;
            this.button1.Text = "1";
            this.button1.Click += new System.EventHandler(this.buttonDialer_Click);
            this.button1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.buttonDialer_KeyPress);
            // 
            // cbPhoneEntry
            // 
            this.cbPhoneEntry.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPhoneEntry.FormattingEnabled = true;
            this.cbPhoneEntry.Location = new System.Drawing.Point(2, 8);
            this.cbPhoneEntry.Name = "cbPhoneEntry";
            this.cbPhoneEntry.Size = new System.Drawing.Size(157, 27);
            this.cbPhoneEntry.TabIndex = 20;
            this.cbPhoneEntry.TextChanged += new System.EventHandler(this.tbPhoneEntry_TextChanged);
            this.cbPhoneEntry.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbPhoneEntry_KeyPress);
            // 
            // bCall
            // 
            this.bCall.Appearance.Options.UseTextOptions = true;
            this.bCall.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.bCall.Enabled = false;
            this.bCall.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.bCall.Location = new System.Drawing.Point(36, 191);
            this.bCall.Name = "bCall";
            this.bCall.Size = new System.Drawing.Size(88, 40);
            this.bCall.TabIndex = 19;
            this.bCall.Text = "Call";
            this.bCall.Click += new System.EventHandler(this.bCall_Click);
            this.bCall.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.buttonDialer_KeyPress);
            // 
            // SoftPhoneControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.bCall);
            this.Controls.Add(this.cbPhoneEntry);
            this.Controls.Add(this.groupBox1);
            this.Name = "SoftPhoneControl";
            this.Size = new System.Drawing.Size(160, 234);
            this.Load += new System.EventHandler(this.SoftPhoneControl_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.buttonDialer_KeyPress);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private SimpleButtonEx button1;
        private SimpleButtonEx button2;
        private SimpleButtonEx button3;
        private SimpleButtonEx button4;
        private SimpleButtonEx button5;
        private SimpleButtonEx button6;
        private SimpleButtonEx button7;
        private SimpleButtonEx button8;
        private SimpleButtonEx button9;
        private SimpleButtonEx bAsterisk;
        private SimpleButtonEx button0;
        private SimpleButtonEx bHash;
        private System.Windows.Forms.GroupBox groupBox1;
        private SimpleButtonEx bCall;
        //private DevExpress.XtraEditors.TextEdit tbPhoneEntry;
        private System.Windows.Forms.ComboBox cbPhoneEntry;
    }
}
