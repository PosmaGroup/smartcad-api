﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadControls
{
    public class GridControlRowInfoDataAttribute : Attribute
    {
        private int width;
        private bool visible = true;
        private Type repositoryType;
        private string measureUnit;
        private string realFieldName;
        private string toolTip;
        private int groupIndex = -1;
        private string displayFormat;
        private int imageIndex = -1;
        private bool searchable = true;
        private bool editable = false;

        private DevExpress.Data.ColumnSortOrder columnOrder = DevExpress.Data.ColumnSortOrder.None;

        public DevExpress.Data.ColumnSortOrder ColumnOrder
        {
            get
            {
                return columnOrder;
            }
            set
            {
                columnOrder = value;
            }
        }

        public string ToolTip
        {
            get
            {
                return toolTip;
            }
            set
            {
                toolTip = value;
            }
        }

        public string MeasureUnit
        {
            get
            {
                return measureUnit;
            }
            set
            {
                measureUnit = value;
            }
        }

        public string RealFieldName
        {
            get
            {
                return realFieldName;
            }
            set
            {
                realFieldName = value;
            }
        }
        public int Width
        {
            get
            {
                return width;
            }
            set
            {
                width = value;
            }
        }

        public bool Visible
        {
            get
            {
                return visible;
            }
            set
            {
                visible = value;
            }
        }

        public Type RepositoryType
        {
            get
            {
                return repositoryType;
            }
            set
            {
                repositoryType = value;
            }
        }

        public int GroupIndex
        {
            get
            {
                return groupIndex;
            }
            set
            {
                groupIndex = value;
            }
        }

        public int ImageIndex
        {
            get
            {
                return imageIndex;
            }
            set
            {
                imageIndex = value;
            }
        }

        public string DisplayFormat
        {
            get
            {
                return displayFormat;
            }
            set
            {
                displayFormat = value;
            }
        }

        public bool Searchable
        {
            get
            {
                return searchable;
            }
            set
            {
                searchable = value;
            }
        }

        public bool Editable
        {
            get
            {
                return editable;
            }
            set
            {
                editable = value;
            }
        }

        public int MaxWidth { get; set; }

        public bool NoCaption { get; set; }
    }
}
