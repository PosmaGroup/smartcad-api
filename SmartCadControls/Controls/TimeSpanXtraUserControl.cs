using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace SmartCadControls.Controls
{
    public partial class TimeSpanXtraUserControl : DevExpress.XtraEditors.XtraUserControl
    {
        public event EventHandler EditValueChanged;
        public TimeSpanXtraUserControl()
        {
            InitializeComponent();
        }

        private Font font;
        private TimeSpan timespanValue;

        public TimeSpan TimespanValue
        {
            get
            {
                timespanValue = new TimeSpan(
                    Convert.ToInt32(this.spinEditDays.Value), Convert.ToInt32(this.spinEditHours.Value),
                    Convert.ToInt32(this.spinEditMinutes.Value), Convert.ToInt32(this.spinEditSeconds.Value));
                return timespanValue;
            }
            set
            {
                timespanValue = value;
                this.spinEditDays.Value = Convert.ToDecimal(Math.Truncate(timespanValue.TotalDays));
                this.spinEditHours.Value = Convert.ToDecimal(timespanValue.Hours);
                this.spinEditMinutes.Value = Convert.ToDecimal(timespanValue.Minutes);
                this.spinEditSeconds.Value = Convert.ToDecimal(timespanValue.Seconds);
            }
        }


        [Browsable(true), Category("Appearance")]
        public new Font Font
        {
            get
            {
                return font;
            }
            set
            {
                font = value;
                this.spinEditDays.Font = font;
                this.spinEditHours.Font = font;
                this.spinEditMinutes.Font = font;
                this.spinEditSeconds.Font = font;
            }
        }

        private void TimeSpanXtraUserControl_EditValueChanged(object sender, EventArgs e)
        {
            if (EditValueChanged != null)
            {
                EditValueChanged(null, EventArgs.Empty);
            }
        }

    }
}
