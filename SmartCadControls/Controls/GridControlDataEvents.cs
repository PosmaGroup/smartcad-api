﻿using SmartCadControls.Controls;
using SmartCadControls.Util;
using SmartCadCore.ClientData;
using SmartCadCore.Core;
using Smartmatic.SmartCad.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadControls
{
    public class GridControlDataEvents : GridControlData
    {
        private ClientEventData cameraEvent;
        public CameraClientData Camera { get; set; }

        public GridControlDataEvents(ClientEventData cameraEvent)
            : base(cameraEvent)
        {
            this.cameraEvent = cameraEvent;
            if (cameraEvent.CameraId != "")
            {
                Camera = ServerServiceClient.GetInstance().SearchClientObject(
                        SmartCadHqls.GetCustomHql(SmartCadHqls.GetCamerasByCustomId, cameraEvent.CameraId)) as CameraClientData;
            }
        }

        [GridControlRowInfoData(Visible = true, Width = 70, Searchable = true)]
        public string CameraName
        {
            get
            {
                return Camera != null ? Camera.Name : "";
            }
        }


        [GridControlRowInfoData(Visible = true, Width = 70, Searchable = true)]
        public string DeviceEventName
        {
            get
            {
                return cameraEvent.DeviceEventName;
            }
        }

        [GridControlRowInfoData(Visible = true, Width = 60, Searchable = true, DisplayFormat = "MM/dd/yyyy HH:mm.ss")]
        public DateTime DateTime
        {
            get
            {
                return cameraEvent.EventTime;
            }
        }

        [GridControlRowInfoData(Visible = false)]
        public ClientEventData CameraEvent
        {
            get
            {
                return cameraEvent;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj is ClientEventData == false)
                return false;
            ClientEventData data = obj as ClientEventData;
            if (data.EventTime == cameraEvent.EventTime)
                return true;
            return false;
        }
    }
}
