﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraLayout;
using DevExpress.XtraLayout.Dragging;
using DevExpress.XtraLayout.Customization;
using SmartCadControls.Interfaces;

namespace SmartCadControls.Controls
{

    [Serializable]
    public class DragDropControlEventArgs : EventArgs
    {
        public DragDropControlEventArgs(LayoutControlItem item)
        {
            this.Item = item;
        }

        public LayoutControlItem Item { get; set; }
    }

    public partial class DragDropLayoutControl : UserControl
    {
        public event EventHandler<DragDropControlEventArgs> ControlCreated;
        public bool CalcHitInfo = true;

        internal IDragManager DragManager { 
            get
            {
                System.Windows.Forms.Control parent = this.Parent;
                while (parent != null && !(parent is IDragManager))
                    parent = parent.Parent;

                return parent as IDragManager; 
            } 
        }
        LayoutControlItem newDragItem = null;

        public DragDropLayoutControl()
        {
            InitializeComponent();
        }

        public DragDropLayoutControl(bool calcHitInfo)
        {
            CalcHitInfo = calcHitInfo;
            InitializeComponent();
        }

        private void layoutControl_MouseDown(object sender, MouseEventArgs e)
        {
            if(CalcHitInfo)
                newDragItem = layoutControl.CalcHitInfo(new Point(e.X, e.Y)).Item as LayoutControlItem;
        }
        private void layoutControl_MouseMove(object sender, MouseEventArgs e)
        {
            if (newDragItem == null || e.Button != MouseButtons.Left|| DragManager == null) return;
            DragManager.DragItem = newDragItem;
            layoutControl.DoDragDrop(DragManager.DragItem, DragDropEffects.Copy);
            newDragItem = null;
        }

        internal virtual void layoutControl_DragDrop(object sender, DragEventArgs e)
        {
            if (dragController != null && DragManager.DragItem != null)
            {
                dragController = new LayoutItemDragController(DragManager.DragItem, dragController);
                if (DragManager.DragItem.Owner == null || DragManager.DragItem.Parent == null)
                    dragController.DragWildItem();
                else
                    dragController.Drag();
            }
            HideDragHelper();
            Parent.Cursor = Cursors.Default;
            if (ControlCreated != null)
            {
                ControlCreated(this, new DragDropControlEventArgs(DragManager.DragItem));
            }
            DragManager.DragItem = null;    
        }

        private void layoutControl_DragEnter(object sender, DragEventArgs e)
        {
            dragController = null;
            ShowDragHelper();
        }
        private void layoutControl_DragLeave(object sender, EventArgs e)
        {
            HideDragHelper();
        }
        private void layoutControl_DragOver(object sender, DragEventArgs e)
        {
            UpdateDragHelper(new Point(e.X, e.Y));
            e.Effect = DragDropEffects.Copy;
            DragManager.SetDragCursor(e.Effect);
        }
        private void layoutControl_GiveFeedback(object sender, GiveFeedbackEventArgs e)
        {
            e.UseDefaultCursors = false;
        }
        //dragHelper
        DragFrameWindow window;
        internal LayoutItemDragController dragController = null;
        protected DragFrameWindow DragFrameWindow
        {
            get
            {
                if (window == null) window = new DragFrameWindow(layoutControl);
                return window;
            }
        }
        protected void ShowDragHelper()
        {
            if (DragManager.DragItem == null) return;
            DragFrameWindow.Visible = true;
            DragFrameWindow.BringToFront();
        }
        protected void HideDragHelper()
        {
            DragFrameWindow.Reset();
            DragFrameWindow.Visible = false;
        }
        protected void UpdateDragHelper(Point p)
        {
            if (DragManager.DragItem == null) return;
            p = layoutControl.PointToClient(p);
            dragController = new LayoutItemDragController(null, layoutControl.Root, new Point(p.X, p.Y));
            DragFrameWindow.DragController = dragController;
        }
    }
}
