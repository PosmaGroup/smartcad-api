using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using SmartCadCore.Core;
using SmartCadCore.Common;

namespace SmartCadControls.Controls
{
    public partial class QuestionAnswerTextControl : HeaderPanelControl
    {
        public event PreviewKeyDownEventHandler QuestionAnswerTextControlPreviewKeyDown;

        public QuestionAnswerTextControl()
        {
            InitializeComponent();                
        }

        private void buttonSavePhoneReportText_Click(object sender, EventArgs e)
        {
            if (textBoxExNLQuestions.Text != string.Empty)
            {
                string oldDirectory = Environment.CurrentDirectory;
                saveFileDialogMain.Filter = ResourceLoader.GetString2("PhoneReport") + "(*.rtf)|*.rtf";
                saveFileDialogMain.FileName = ResourceLoader.GetString2("Report") + DateTime.Now.ToString("yyyyMMdd");
                
                if (saveFileDialogMain.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        textBoxExNLQuestions.SaveFile(saveFileDialogMain.FileName, RichTextBoxStreamType.RichText);
                        Environment.CurrentDirectory = oldDirectory;
                    }
                    catch (Exception ex)
                    {
                        MessageForm.Show(ex.Message, MessageFormType.Error);
                    }
                }
            }
        }

        private void buttonPrintPhoneReportText_Click(object sender, EventArgs e)
        {
            if (textBoxExNLQuestions.Text != string.Empty)
            {
                RichTextPrintDocument richTextPrintDocument = new RichTextPrintDocument(textBoxExNLQuestions);

                printDialogMain = new PrintDialog();
                printDialogMain.Document = richTextPrintDocument;

                if (printDialogMain.ShowDialog() == DialogResult.OK)
                {
                    richTextPrintDocument.Print();
                }
            }
        }

        public RichTextBox RichTextArea
        {
            get
            {
                return this.textBoxExNLQuestions;
            }
        }

        public bool EnabledButtons
        {
            set
            {
                //this.buttonPrintPhoneReportText.Enabled = value;
                //this.buttonSavePhoneReportText.Enabled = value;
            }
        }

		public override bool Active
		{
			get
			{
				return base.Active;
			}
			set
			{
				base.Active = value;

				if (Active)
				{
					this.groupControlBody.LookAndFeel.Style = GetStyle(ActiveColor);
					this.layoutControl1.LookAndFeel.Style = GetStyle(ActiveColor);
				}
				else
				{
					this.groupControlBody.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
					this.layoutControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
					this.groupControlBody.LookAndFeel.SkinName = InactiveColor;
					this.layoutControl1.LookAndFeel.SkinName = InactiveColor;
				}
				buttonPrintPhoneReportText.LookAndFeel.UseDefaultLookAndFeel = true;
				buttonSavePhoneReportText.LookAndFeel.UseDefaultLookAndFeel = true;
			}
		}

        private void textBoxExNLQuestions_TextChanged(object sender, EventArgs e)
        {
            if (textBoxExNLQuestions.Text != null && textBoxExNLQuestions.Text != string.Empty)
            {
                this.buttonPrintPhoneReportText.Enabled = true;
                this.buttonSavePhoneReportText.Enabled = true;
            }
            else
            {
                this.buttonPrintPhoneReportText.Enabled = false;
                this.buttonSavePhoneReportText.Enabled = false;
            }
        }

        private void textBoxExNLQuestions_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (QuestionAnswerTextControlPreviewKeyDown != null)
            {
                QuestionAnswerTextControlPreviewKeyDown(this, e);
            }
        }

        private void questionAnswerButton_MouseHover(object sender, EventArgs e)
        {
            if (sender.Equals(buttonSavePhoneReportText))
            {

                toolTipMain.SetToolTip(buttonSavePhoneReportText, ResourceLoader.GetString2("Save"));

            }
            else if (sender.Equals(buttonPrintPhoneReportText))
            {

                toolTipMain.SetToolTip(buttonPrintPhoneReportText, ResourceLoader.GetString2("Print"));
            }
        }

        private void questionAnswerButton_MouseLeave(object sender, EventArgs e)
        {
            toolTipMain.Hide((DevExpress.XtraEditors.SimpleButton)sender);
        }

        private void QuestionAnswerTextControl_Load(object sender, EventArgs e)
        {
            this.groupControlBody.Text = ResourceLoader.GetString2("PhoneReport");
        }

    }
}
