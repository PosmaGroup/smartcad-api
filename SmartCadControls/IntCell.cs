﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SmartCadControls
{
    public class IntCell : DataGridViewTextBoxCell
    {
        public IntCell()
            : base()
        {
        }

        public IntCell(int len)
            : base()
        {
            this.MaxInputLength = len;
        }

        internal bool ProcessKeyDown(Keys keyData)
        {
            if (keyData == Keys.D0 ||
                keyData == Keys.D1 ||
                keyData == Keys.D2 ||
                keyData == Keys.D3 ||
                keyData == Keys.D4 ||
                keyData == Keys.D5 ||
                keyData == Keys.D6 ||
                keyData == Keys.D7 ||
                keyData == Keys.D8 ||
                keyData == Keys.D9 ||
                keyData == Keys.NumPad0 ||
                keyData == Keys.NumPad1 ||
                keyData == Keys.NumPad2 ||
                keyData == Keys.NumPad3 ||
                keyData == Keys.NumPad4 ||
                keyData == Keys.NumPad5 ||
                keyData == Keys.NumPad6 ||
                keyData == Keys.NumPad7 ||
                keyData == Keys.NumPad8 ||
                keyData == Keys.NumPad9 ||
                keyData == Keys.Clear ||
                keyData == Keys.Delete ||
                keyData == Keys.Back ||
                keyData == Keys.Right ||
                keyData == Keys.Left
                )
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
