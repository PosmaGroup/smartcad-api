using System;
using System.ComponentModel;
using System.ComponentModel.Design.Serialization;
using System.Globalization;
using System.Reflection;

namespace SmartCadGuiCommon
{
	/// <summary>
	/// Summary description for IPAddressStructConverter.
	/// </summary>
	public class IPAddressStructConverter : TypeConverter
	{
		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			if (sourceType == typeof (string))
			{
				return true;
			}
			return base.CanConvertFrom(context, sourceType);
		}

		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			if (value is string)
			{
				string[] v = ((string) value).Split(new char[] {'.'});
				return new IPAddressStruct(byte.Parse(v[0]), byte.Parse(v[1]), byte.Parse(v[2]), byte.Parse(v[3]));
			}
			return base.ConvertFrom(context, culture, value);
		}

		public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
		{
			if (destinationType == typeof (InstanceDescriptor))
			{
				return true;
			}
			else if (destinationType == typeof (string))
			{
				return true;
			}
			return base.CanConvertTo(context, destinationType);
		}

		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			if (value is IPAddressStruct)
			{
				IPAddressStruct address = (IPAddressStruct) value;
				if (destinationType == typeof (InstanceDescriptor))
				{
					ConstructorInfo constructor = typeof (IPAddressStruct).GetConstructor(new Type[] {typeof (byte), typeof (byte), typeof (byte), typeof (byte)});
					if (constructor != null)
					{
						return new InstanceDescriptor(constructor, new object[] {address.Byte1, address.Byte2, address.Byte3, address.Byte4});
					}
				}
				else if (destinationType == typeof (string))
				{
					return address.ToString();
				}
			}
			return base.ConvertTo(context, culture, value, destinationType);
		}

		public override bool GetPropertiesSupported(ITypeDescriptorContext context)
		{
			if (context != null)
			{
				return true;
			}
			return base.GetPropertiesSupported(context);
		}

		public override PropertyDescriptorCollection GetProperties(ITypeDescriptorContext context, object value, Attribute[] attributes)
		{
			if (context != null)
			{
				PropertyDescriptorCollection proDcol = TypeDescriptor.GetProperties(typeof (IPAddressStruct));
				PropertyDescriptor[] retProDarray = new PropertyDescriptor[4];
				retProDarray[0] = proDcol["Byte1"];
				retProDarray[1] = proDcol["Byte2"];
				retProDarray[2] = proDcol["Byte3"];
				retProDarray[3] = proDcol["Byte4"];
				return new PropertyDescriptorCollection(retProDarray);
			}
			return base.GetProperties(context, value, attributes);
		}
	}
}