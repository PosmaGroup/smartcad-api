﻿namespace SmartCadControls.Controls
{
    using Microsoft.DirectX.DirectInput;
    using System;

    public class JoystickAxisInput : JoystickInputBase
    {
        internal AxisActions _axisAction;
        internal AxisTypes _axisType;
        protected int _deadZone;
        protected bool _invert;
        protected int _range;
        protected int _threshold;
        internal readonly int HARDWARE_AXIS_DEADZONE;
        internal readonly int HARDWARE_AXIS_RANGE;

        internal JoystickAxisInput(Device joystickDevice, DeviceObjectInstance inputObjectInstance, string displayName, AxisTypes axisType) : base(joystickDevice, inputObjectInstance, displayName)
        {
            this.HARDWARE_AXIS_RANGE = 0xf4240;
            this._range = 0x2710;
            this._deadZone = 0x7d0;
            lock (base.ressourceLockObejct)
            {
                this._axisType = axisType;
                this._axisAction = AxisActions.Unknown;
                base._joystickDevice.Properties.SetRange(ParameterHow.ById, this._inputObjectInstance.ObjectId, new InputRange(0, this.HARDWARE_AXIS_RANGE));
                base._joystickDevice.Properties.SetDeadZone(ParameterHow.ById, this._inputObjectInstance.ObjectId, this.HARDWARE_AXIS_DEADZONE);
            }
        }

        internal override void SetValue(int newValue)
        {
            int num = 0;
            bool flag = false;
            lock (base.ressourceLockObejct)
            {
                num = (int) Normalization.NormalizeValue((double) newValue, 0.0, (double) this._range, 0.0, (double) this.HARDWARE_AXIS_RANGE);
                if ((num > ((this._range / 2) - (this._deadZone / 2))) && (num < ((this._range / 2) + (this._deadZone / 2))))
                {
                    num = this._range / 2;
                }
                else if (num < (this._range / 2))
                {
                    num = (int) Normalization.NormalizeValue((double) num, 0.0, (double) (this._range / 2), 0.0, (double) ((this._range / 2) - (this._deadZone / 2)));
                }
                else
                {
                    num = (int) Normalization.NormalizeValue((double) num, (double) (this._range / 2), (double) this._range, (double) ((this._range / 2) + (this._deadZone / 2)), (double) this._range);
                }
                if (this._invert)
                {
                    num = this._range - num;
                }
                if (Math.Abs((int) (this.Value - num)) >= this._threshold)
                {
                    flag = true;
                }
            }
            if (flag)
            {
                base.SetValue(num);
            }
        }

        public AxisActions AxisAction
        {
            get
            {
                lock (base.ressourceLockObejct)
                {
                    return this._axisAction;
                }
            }
            set
            {
                lock (base.ressourceLockObejct)
                {
                    this._axisAction = value;
                }
            }
        }

        public AxisTypes AxisType
        {
            get
            {
                lock (base.ressourceLockObejct)
                {
                    return this._axisType;
                }
            }
            set
            {
                lock (base.ressourceLockObejct)
                {
                    this._axisType = value;
                }
            }
        }

        public int DeadZone
        {
            get
            {
                lock (base.ressourceLockObejct)
                {
                    return this._deadZone;
                }
            }
            set
            {
                lock (base.ressourceLockObejct)
                {
                    this._deadZone = value;
                }
            }
        }

        public bool Invert
        {
            get
            {
                lock (base.ressourceLockObejct)
                {
                    return this._invert;
                }
            }
            set
            {
                bool flag = false;
                lock (base.ressourceLockObejct)
                {
                    if (this._invert != value)
                    {
                        this._invert = value;
                        flag = true;
                    }
                }
                if (flag)
                {
                    this.FireInputValueChangedEvent(new JoystickInputBase.InputValueChangedEventArgs(base._inputValue));
                }
            }
        }

        public int Range
        {
            get
            {
                lock (base.ressourceLockObejct)
                {
                    return this._range;
                }
            }
            set
            {
                lock (base.ressourceLockObejct)
                {
                    this._range = value;
                }
            }
        }

        public int ThresHold
        {
            get
            {
                lock (base.ressourceLockObejct)
                {
                    return this._threshold;
                }
            }
            set
            {
                lock (base.ressourceLockObejct)
                {
                    this._threshold = value;
                }
            }
        }

        public enum AxisActions
        {
            Unknown,
            Pan,
            Tilt,
            Zoom
        }

        public enum AxisTypes
        {
            X,
            Y,
            Z,
            RotationX,
            RotationY,
            RotationZ,
            SliderA,
            SliderB
        }
    }
}

