﻿namespace SmartCadControls.Controls
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [ComImport, ClassInterface((short) 0), TypeLibType((short) 2), Guid("FB5AC08E-3B4B-4E45-9551-8BEFD75F5B54")]
    public class ConnectTesterClass : IConnectTester, ConnectTester
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime), DispId(1)]
        public virtual extern void Test();
    }
}

