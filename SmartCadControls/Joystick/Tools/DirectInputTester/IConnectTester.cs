﻿namespace SmartCadControls.Controls
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [ComImport, TypeLibType((short) 0x10c0), Guid("AB61039F-2529-41D3-A005-C36425674ACF")]
    public interface IConnectTester
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime), DispId(1)]
        void Test();
    }
}

