﻿namespace SmartCadControls.Controls
{
    using System.Runtime.InteropServices;

    [ComImport, CoClass(typeof(ConnectTesterClass)), Guid("AB61039F-2529-41D3-A005-C36425674ACF")]
    public interface ConnectTester : IConnectTester
    {
    }
}

