﻿namespace SmartCadControls.Controls
{
    using Microsoft.DirectX.DirectInput;
    using System;
    using System.Diagnostics;
    using System.Management;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class JoystickManager
    {
        private ManagementEventWatcher _instanceCreationEventWatcher;
        private ManagementEventWatcher _instanceDeletionEventWatcher;
        protected JoystickDevice[] _joystickDevices = new JoystickDevice[0];

        public event EventHandler JoystickAddedEvent;
        public event EventHandler JoystickRemovedEvent;

        public void Close()
        {
            if (this._instanceCreationEventWatcher != null)
            {
                this._instanceCreationEventWatcher.EventArrived -= new EventArrivedEventHandler(this.UsbEventOccurred);
                this._instanceCreationEventWatcher.Stop();
                this._instanceCreationEventWatcher.Dispose();
                this._instanceCreationEventWatcher = null;
            }
            if (this._instanceDeletionEventWatcher != null)
            {
                this._instanceDeletionEventWatcher.EventArrived -= new EventArrivedEventHandler(this.UsbEventOccurred);
                this._instanceDeletionEventWatcher.Stop();
                this._instanceDeletionEventWatcher.Dispose();
                this._instanceDeletionEventWatcher = null;
            }
        }

        public bool ConnectToJoysticks()
        {
            if (this._joystickDevices != null)
            {
                foreach (JoystickDevice device in this._joystickDevices)
                {
                    device.Close();
                }
                this._joystickDevices = new JoystickDevice[0];
            }
            DeviceList devices = null;
            try
            {
                try
                {
                    IConnectTester o = new ConnectTesterClass();
                    o.Test();
                    Marshal.ReleaseComObject(o);
                }
                catch (Exception exception)
                {
                    Trace.Write("ConnectToJoysticks() - An expected error occured while getting joystick devices via DirectInput. The DirectInput might be used by another application. ExceptionMessage: " + exception.Message);
                    this._joystickDevices = new JoystickDevice[0];
                    return false;
                }
                devices = Manager.GetDevices(DeviceType.Joystick, EnumDevicesFlags.AttachedOnly);
            }
            catch (Exception exception2)
            {
                Trace.Write("ConnectToJoysticks() - An unexpected error occured while getting joystick devices via DirectInput. ExceptionMessage: " + exception2.Message);
                this._joystickDevices = new JoystickDevice[0];
                return false;
            }
            if (devices == null)
            {
                return false;
            }
            this._joystickDevices = new JoystickDevice[devices.Count];
            int num = 0;
            while (devices.MoveNext())
            {
                DeviceInstance current = (DeviceInstance) devices.Current;
                JoystickDevice device2 = new JoystickDevice(new Device(current.InstanceGuid));
                this._joystickDevices[num++] = device2;
            }
            return true;
        }

        protected virtual void FireJoystickAddedOrRemovedEvent(bool flag)
        {
            if (flag)
            {
                if (this.JoystickAddedEvent != null)
                {
                    this.JoystickAddedEvent(this, new EventArgs());
                }
            }
            else
            {
                if (this.JoystickRemovedEvent != null)
                {
                    this.JoystickRemovedEvent(this, new EventArgs());
                }
            }
        }

        public JoystickDevice GetJoystickDevice(Guid joystickId)
        {
            foreach (JoystickDevice device in this._joystickDevices)
            {
                if (device.Id == joystickId)
                {
                    return device;
                }
            }
            return null;
        }

        protected bool HasJoysticksChanged()
        {
            bool flag2;
            Trace.WriteLine("HasJoysticksChanged() is called");
            try
            {
                DeviceList devices = Manager.GetDevices(DeviceType.Joystick, EnumDevicesFlags.AttachedOnly);
                bool flag = false;
                if (devices.Count != this._joystickDevices.Length)
                {
                    goto Label_00BD;
                }
            Label_00B0:
                while (devices.MoveNext() && !flag)
                {
                    DeviceInstance current = (DeviceInstance) devices.Current;
                    flag = true;
                    foreach (JoystickDevice device in this._joystickDevices)
                    {
                        if (device.Id == current.InstanceGuid)
                        {
                            flag = false;
                            goto Label_00B0;
                        }
                    }
                }
                goto Label_00BF;
            Label_00BD:
                flag = true;
            Label_00BF:
                flag2 = flag;
            }
            catch
            {
                flag2 = false;
            }
            return flag2;
        }

        public void Init()
        {
            ManagementScope scope = null;
            try
            {
                scope = new ManagementScope(@"root\CIMV2");
            }
            catch (Exception exception)
            {
                Trace.WriteLine("System.Management.ManagementScope construction threw an exception. Message: " + exception.Message);
            }
            if (scope != null)
            {
                scope.Options.EnablePrivileges = true;
                WqlEventQuery query = new WqlEventQuery();
                query.EventClassName = "__InstanceCreationEvent";
                query.WithinInterval = new TimeSpan(0, 0, 10);
                query.Condition = "TargetInstance ISA 'Win32_USBControllerDevice' ";
                WqlEventQuery query2 = new WqlEventQuery();
                query2.EventClassName = "__InstanceDeletionEvent";
                query2.WithinInterval = new TimeSpan(0, 0, 10);
                query2.Condition = "TargetInstance ISA 'Win32_USBControllerDevice' ";
                this._instanceCreationEventWatcher = new ManagementEventWatcher(scope, query);
                this._instanceDeletionEventWatcher = new ManagementEventWatcher(scope, query2);
                this._instanceCreationEventWatcher.EventArrived += new EventArrivedEventHandler(this.UsbEventOccurred);
                this._instanceDeletionEventWatcher.EventArrived += new EventArrivedEventHandler(this.UsbEventOccurred);
                try
                {
                    this._instanceDeletionEventWatcher.Start();
                    this._instanceCreationEventWatcher.Start();
                }
                catch (Exception exception2)
                {
                    Trace.WriteLine("System.Management.ManagementEventWatcher.Start threw an exception. Message: " + exception2.Message);
                }
            }
        }

        private void UsbEventOccurred(object sender, EventArrivedEventArgs e)
        {
            this.FireJoystickAddedOrRemovedEvent(this.HasJoysticksChanged());
        }

        public JoystickDevice[] JoystickDevices
        {
            get
            {
                return this._joystickDevices;
            }
        }
    }
}

