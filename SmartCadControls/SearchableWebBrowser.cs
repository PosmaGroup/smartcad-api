﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using mshtml;
using SmartCadCore.Common;

namespace SmartCadControls.Controls
{
    public partial class SearchableWebBrowser : UserControl
    {
        private string prevString = string.Empty;
        private bool prevStringFound = false;

        public SearchableWebBrowser()
        {
            InitializeComponent();
            LoadLanguage();
        }

        private void LoadLanguage()
        {
            btnPrev.ToolTip = ResourceLoader.GetString2("SearchPrevious");
            btnNext.ToolTip = ResourceLoader.GetString2("SearchNext");
        }

        private void SearchableWebBrowser_Resize(object sender, EventArgs e)
        {
            layoutControl.Size = new Size(this.Size.Width - 6, this.Size.Height - 6);
        }

        private void search_event(object sender, EventArgs e)
        {
            try
            {
                IHTMLDocument2 htmlDocument = webBrowserEx.Document.DomDocument as IHTMLDocument2;
                IHTMLSelectionObject sel = (IHTMLSelectionObject)htmlDocument.selection;
                IHTMLTxtRange tr = (IHTMLTxtRange)sel.createRange() as IHTMLTxtRange;

                string senderName = (sender as System.Windows.Forms.Control).Name;
                bool stringFound = false;

                sel = (IHTMLSelectionObject)htmlDocument.selection;
                tr = (IHTMLTxtRange)sel.createRange() as IHTMLTxtRange;
                string s = edtSearch.Text;
                if (s.Trim() != string.Empty)
                {
                    if (senderName == "btnPrev")
                    {
                        tr.collapse(true);
                        stringFound = tr.findText(s, -1, 0);
                    }
                    else if (senderName == "btnNext")
                    {
                        tr.collapse(false);
                        stringFound = tr.findText(s, 1, 0);
                    }
                    else if (senderName == "edtSearch")
                    {
                        tr.collapse(false);
                        tr.moveStart("textedit", -1);
                        tr.moveEnd("textedit", -1);
                        tr.select();
                        stringFound = tr.findText(s, 1, 0);
                    }
                    tr.select();
                    if (stringFound == false)
                    {
                        tr.moveStart("textedit", -1);
                        tr.moveEnd("textedit", -1);
                        if (prevStringFound == true && prevString == s)
                        {
                            if (senderName == "btnPrev")
                            {
                                tr.moveStart("textedit", 1);
                                tr.moveEnd("textedit", 1);
                                stringFound = tr.findText(s, -1, 0);
                            }
                            else if (senderName == "btnNext" || senderName == "edtSearch")
                            {
                                stringFound = tr.findText(s, 1, 0);
                            }
                        }
                        tr.select();
                    }
                    prevStringFound = stringFound;
                    prevString = s;
                }
                else
                {
                    tr.moveStart("textedit", -1);
                    tr.moveEnd("textedit", -1);
                    tr.select();
                    prevStringFound = false;
                    prevString = string.Empty;
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void edtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnNext.PerformClick();
            }
        }

        public string DocumentText
        {
            get
            {
                return webBrowserEx.DocumentText;
            }
            set
            {
                webBrowserEx.DocumentText = value;
            }
        }

        public void ShowSaveAsDialog()
        {
            webBrowserEx.ShowSaveAsDialog();
        }

        public void ShowPrintDialog()
        {
            webBrowserEx.ShowPrintDialog();
        }

        public IHTMLDocument2 DomDocument {
            get
            {
                return (IHTMLDocument2)webBrowserEx.Document.DomDocument;
            }
        }

        private void edtSearch_EditValueChanged(object sender, EventArgs e)
        {

        }

        public bool AllowWebBrowserDrop 
        {
            get
            {
                return webBrowserEx.AllowWebBrowserDrop;
            }
            set
            {
                webBrowserEx.AllowWebBrowserDrop = value;
            }
        }

        public void Clear()
        {
            webBrowserEx.Clear();
        }

        public string XmlText { get { return webBrowserEx.XmlText; } set { webBrowserEx.XmlText = value; } }

        public bool IsWebBrowserContextMenuEnabled { get { return webBrowserEx.IsWebBrowserContextMenuEnabled; } set { webBrowserEx.IsWebBrowserContextMenuEnabled = value; } }

        public bool WebBrowserShortcutsEnabled { get { return webBrowserEx.WebBrowserShortcutsEnabled; } set { webBrowserEx.WebBrowserShortcutsEnabled = value; } }

        public void Navigate(string urlString, bool keepScrollIndex = false)
        {
            webBrowserEx.Navigate(urlString, keepScrollIndex);
        }

        public void Print()
        {
            webBrowserEx.Print();
        }

        public event KeyEventHandler KeyDownEx
        {
            add
            {
                webBrowserEx.KeyDownEx += value;
            }

            remove
            {
                webBrowserEx.KeyDownEx -= value;
            }
        }
    }
}