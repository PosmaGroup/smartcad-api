using System.Windows.Forms.Design;
using SelectionRulesEnum = System.Windows.Forms.Design.SelectionRules;

namespace Smartmatic.SmartCad.Controls.ControlDesigners
{
	/// <summary>
	/// Summary description for IPAddressControlDesigner.
	/// </summary>
	public class IPAddressControlDesigner : ControlDesigner
	{
		public override SelectionRulesEnum SelectionRules
		{
			get { return (base.SelectionRules & (~SelectionRulesEnum.BottomSizeable) & (~SelectionRulesEnum.TopSizeable)); }
		}
	}
}