﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NAudio.Wave;
using System.IO;
using SmartCadCore.Core;
using SmartCadCore.Common;
using System.Reflection;

namespace SmartCadControls.Controls
{
    public partial class CallAudioPlayer : DevExpress.XtraEditors.XtraUserControl
    {
        WaveOut wavePlayer;
        WaveStream waveStream;
        string currentAudioFile;
        string exportFileName;

        public CallAudioPlayer()
        {
            InitializeComponent();
        }

        public CallAudioPlayer(string audioFileName, string exportName)
        :this(){
            currentAudioFile = audioFileName;
            exportFileName = exportName;
        }

        public void Init()
        {
            if (currentAudioFile != "")
            {
                wavePlayer = new WaveOut();
                waveStream = new RawSourceWaveStream(File.OpenRead(currentAudioFile), new WaveFormat(8000, 16, 2));


                //wavePlayer.Volume = 10.0f;
                wavePlayer.Init(waveStream);
                wavePlayer.PlaybackStopped += new EventHandler<StoppedEventArgs>(wavePlayer_PlaybackStopped);
                trackBarControlProgress.Properties.Maximum = (int)waveStream.Length / 100;
                labelPosition.Text = "0:00:00";
                labelDuration.Text = waveStream.TotalTime.Hours.ToString("D1") + ":" +
                                    waveStream.TotalTime.Minutes.ToString("D2") + ":" +
                                    waveStream.TotalTime.Seconds.ToString("D2");
            }
        }

        public void Disable()
        {
            checkButtonPlay.Checked = false;
            checkButtonPlay.Image = global::SmartCadControls.Properties.ResourcesGui.Play;
            Enabled = false;
            Stop();
        }

        public void Stop()
        {
            if (wavePlayer != null)
            {
                wavePlayer.Stop();
                wavePlayer.Dispose();
                wavePlayer = null;
            }
            playerTimer.Stop();
            trackBarControlProgress.Value = 0;
            labelPosition.Text = "0:00:00";
        }

        void wavePlayer_PlaybackStopped(object sender, StoppedEventArgs e)
        {
            FormUtil.InvokeRequired(this, delegate
            {
                checkButtonPlay.Image = global::SmartCadControls.Properties.ResourcesGui.Play;
                if (checkButtonPlay.Checked)
                {
                    checkButtonPlay.Checked = false;
                    Stop();
                }
            });
        }

        #region PlayBack controls

        private void simpleButtonPlay_Click(object sender, EventArgs e)
        {
            try
            {
                if (checkButtonPlay.Checked)
                {
                    checkButtonPlay.Image = global::SmartCadControls.Properties.ResourcesGui.Play;
                    wavePlayer.Pause();
                }
                else
                {
                    checkButtonPlay.Image = global::SmartCadControls.Properties.ResourcesGui.pausa;
                    if (wavePlayer == null)
                    {
                        Init();
                    }
                    wavePlayer.Play();

                    playerTimer.Start();
                }
            }
            catch (Exception ex)
            {
                Stop();
                Init();

                if (!checkButtonPlay.Checked) { wavePlayer.Play(); }
            }
        }

        private void playerTimer_Tick(object sender, EventArgs e)
        {
            FormUtil.InvokeRequired(this, delegate
            {
                trackBarControlProgress.Value = (int)waveStream.Position / 100;
                labelPosition.Text = waveStream.CurrentTime.Hours.ToString("D1") 
                                    + ":" + waveStream.CurrentTime.Minutes.ToString("D2")
                                    + ":" + waveStream.CurrentTime.Seconds.ToString("D2");
            });
        }

        private void simpleButtonStop_Click(object sender, EventArgs e)
        {
            FormUtil.InvokeRequired(this, delegate
            {
                checkButtonPlay.Checked = false;
                checkButtonPlay.Image = global::SmartCadControls.Properties.ResourcesGui.Play;
                Stop();
            });
        }

        private void trackBarControlVolume_EditValueChanged(object sender, EventArgs e)
        {
            if (wavePlayer != null)
                wavePlayer.Volume = (float)trackBarControlVolume.Value / 10.0f;
        }

        private void checkButtonMute_CheckedChanged(object sender, EventArgs e)
        {
            if (checkButtonMute.Checked)
            {
                this.checkButtonMute.Image = global::SmartCadControls.Properties.ResourcesGui.soundMuteSmall;
                trackBarControlVolume.Value = 0;
            }
            else
            {
                this.checkButtonMute.Image = global::SmartCadControls.Properties.ResourcesGui.soundSmall;
                trackBarControlVolume.Value = 5;
            }
        }
        #endregion

        private void trackBarControlProgress_EditValueChanged(object sender, EventArgs e)
        {
            if (waveStream != null && wavePlayer != null && wavePlayer.PlaybackState != PlaybackState.Playing)
            {
                playerTimer.Stop();
                waveStream.Position = trackBarControlProgress.Value * 100;
                labelPosition.Text = waveStream.CurrentTime.Minutes.ToString("D2")
                                    + ":" + waveStream.CurrentTime.Seconds.ToString("D2");
            }
        }

        private void simpleButtonExportAudio_Click(object sender, EventArgs e)
        {
            SaveFileDialog savefile = new SaveFileDialog();

            // set a default file name
            savefile.FileName = exportFileName + ".wav";
            // set filters - this can be done in properties as well
            savefile.Filter = "Wav files (*.wav)|*.wav";

            if (savefile.ShowDialog() == DialogResult.OK)
            {
                BackgroundProcessForm processForm = new BackgroundProcessForm(ResourceLoader.GetString2("Exporting"),
                    this, new MethodInfo[1] {
                        GetType().GetMethod("ExportAudio", BindingFlags.NonPublic | BindingFlags.Instance) },
                        new object[1][] { new object[1] { savefile.FileName } });
                processForm.CanThrowError = true;
                processForm.ShowDialog();
            }
        }

        private void ExportAudio(object destinationFile)
        {
            //Close player before to export
            //Stop(); 
            WaveFileWriter writer = new WaveFileWriter(destinationFile.ToString(), new WaveFormat(8000, 16, 2));
            byte[] bytes = File.ReadAllBytes(currentAudioFile);

            writer.Write(bytes, 0, bytes.Length);
            writer.Close();
            writer.Dispose();

            MessageForm.Show(ResourceLoader.GetString2("CallExportedSuccessfully"), MessageFormType.Information);
        }
    }
}
