﻿namespace SmartCadControls.Controls
{
    partial class CallAudioPlayer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CallAudioPlayer));
            this.labelPosition = new System.Windows.Forms.Label();
            this.labelDuration = new System.Windows.Forms.Label();
            this.trackBarControlVolume = new DevExpress.XtraEditors.ZoomTrackBarControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.simpleButtonExportAudio = new DevExpress.XtraEditors.SimpleButton();
            this.checkButtonMute = new DevExpress.XtraEditors.CheckButton();
            this.checkButtonPlay = new DevExpress.XtraEditors.CheckButton();
            this.simpleButtonStop = new DevExpress.XtraEditors.SimpleButton();
            this.trackBarControlProgress = new DevExpress.XtraEditors.TrackBarControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.playerTimer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControlVolume)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControlVolume.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControlProgress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControlProgress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            this.SuspendLayout();
            // 
            // labelPosition
            // 
            this.labelPosition.BackColor = System.Drawing.Color.Transparent;
            this.labelPosition.Location = new System.Drawing.Point(90, 22);
            this.labelPosition.MaximumSize = new System.Drawing.Size(45, 12);
            this.labelPosition.MinimumSize = new System.Drawing.Size(45, 12);
            this.labelPosition.Name = "labelPosition";
            this.labelPosition.Size = new System.Drawing.Size(45, 12);
            this.labelPosition.TabIndex = 77;
            this.labelPosition.Text = "0:00:00";
            // 
            // labelDuration
            // 
            this.labelDuration.BackColor = System.Drawing.Color.Transparent;
            this.labelDuration.Location = new System.Drawing.Point(203, 22);
            this.labelDuration.MaximumSize = new System.Drawing.Size(45, 12);
            this.labelDuration.MinimumSize = new System.Drawing.Size(45, 12);
            this.labelDuration.Name = "labelDuration";
            this.labelDuration.Size = new System.Drawing.Size(45, 12);
            this.labelDuration.TabIndex = 76;
            this.labelDuration.Text = "0:00:00";
            // 
            // trackBarControlVolume
            // 
            this.trackBarControlVolume.EditValue = 5;
            this.trackBarControlVolume.Location = new System.Drawing.Point(286, 7);
            this.trackBarControlVolume.Name = "trackBarControlVolume";
            this.trackBarControlVolume.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.trackBarControlVolume.Properties.HighlightSelectedRange = false;
            this.trackBarControlVolume.Properties.ScrollThumbStyle = DevExpress.XtraEditors.Repository.ScrollThumbStyle.Bar;
            this.trackBarControlVolume.Size = new System.Drawing.Size(66, 19);
            this.trackBarControlVolume.StyleController = this.layoutControl1;
            this.trackBarControlVolume.TabIndex = 71;
            this.trackBarControlVolume.Value = 5;
            this.trackBarControlVolume.EditValueChanged += new System.EventHandler(this.trackBarControlVolume_EditValueChanged);
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowCustomizationMenu = false;
            this.layoutControl1.Controls.Add(this.simpleButtonExportAudio);
            this.layoutControl1.Controls.Add(this.trackBarControlVolume);
            this.layoutControl1.Controls.Add(this.checkButtonMute);
            this.layoutControl1.Controls.Add(this.checkButtonPlay);
            this.layoutControl1.Controls.Add(this.labelDuration);
            this.layoutControl1.Controls.Add(this.labelPosition);
            this.layoutControl1.Controls.Add(this.simpleButtonStop);
            this.layoutControl1.Controls.Add(this.trackBarControlProgress);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(52, 281, 250, 350);
            this.layoutControl1.OptionsView.ItemBorderColor = System.Drawing.Color.Empty;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(438, 38);
            this.layoutControl1.TabIndex = 79;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // simpleButtonExportAudio
            // 
            this.simpleButtonExportAudio.Image = global::SmartCadControls.Properties.ResourcesGui.SaveIcon;
            this.simpleButtonExportAudio.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonExportAudio.Location = new System.Drawing.Point(386, 2);
            this.simpleButtonExportAudio.Name = "simpleButtonExportAudio";
            this.simpleButtonExportAudio.Padding = new System.Windows.Forms.Padding(0, 12, 0, 0);
            this.simpleButtonExportAudio.Size = new System.Drawing.Size(30, 30);
            this.simpleButtonExportAudio.StyleController = this.layoutControl1;
            this.simpleButtonExportAudio.TabIndex = 78;
            this.simpleButtonExportAudio.Click += new System.EventHandler(this.simpleButtonExportAudio_Click);
            // 
            // checkButtonMute
            // 
            this.checkButtonMute.AutoWidthInLayoutControl = true;
            this.checkButtonMute.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.checkButtonMute.Image = global::SmartCadControls.Properties.ResourcesGui.soundSmall;
            this.checkButtonMute.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.checkButtonMute.Location = new System.Drawing.Point(252, 2);
            this.checkButtonMute.Name = "checkButtonMute";
            this.checkButtonMute.Size = new System.Drawing.Size(30, 30);
            this.checkButtonMute.StyleController = this.layoutControl1;
            this.checkButtonMute.TabIndex = 74;
            this.checkButtonMute.CheckedChanged += new System.EventHandler(this.checkButtonMute_CheckedChanged);
            // 
            // checkButtonPlay
            // 
            this.checkButtonPlay.Image = global::SmartCadControls.Properties.ResourcesGui.Play;
            this.checkButtonPlay.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.checkButtonPlay.Location = new System.Drawing.Point(22, 2);
            this.checkButtonPlay.Name = "checkButtonPlay";
            this.checkButtonPlay.Size = new System.Drawing.Size(30, 30);
            this.checkButtonPlay.StyleController = this.layoutControl1;
            this.checkButtonPlay.TabIndex = 80;
            this.checkButtonPlay.Click += new System.EventHandler(this.simpleButtonPlay_Click);
            // 
            // simpleButtonStop
            // 
            this.simpleButtonStop.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonStop.Image")));
            this.simpleButtonStop.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonStop.Location = new System.Drawing.Point(56, 2);
            this.simpleButtonStop.Name = "simpleButtonStop";
            this.simpleButtonStop.Size = new System.Drawing.Size(30, 30);
            this.simpleButtonStop.StyleController = this.layoutControl1;
            this.simpleButtonStop.TabIndex = 72;
            this.simpleButtonStop.Click += new System.EventHandler(this.simpleButtonStop_Click);
            // 
            // trackBarControlProgress
            // 
            this.trackBarControlProgress.EditValue = 1;
            this.trackBarControlProgress.Location = new System.Drawing.Point(90, 0);
            this.trackBarControlProgress.Margin = new System.Windows.Forms.Padding(2, 0, 2, 2);
            this.trackBarControlProgress.MaximumSize = new System.Drawing.Size(0, 30);
            this.trackBarControlProgress.MinimumSize = new System.Drawing.Size(150, 30);
            this.trackBarControlProgress.Name = "trackBarControlProgress";
            this.trackBarControlProgress.Properties.Maximum = 100;
            this.trackBarControlProgress.Properties.Minimum = 1;
            this.trackBarControlProgress.Properties.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBarControlProgress.Size = new System.Drawing.Size(158, 30);
            this.trackBarControlProgress.StyleController = this.layoutControl1;
            this.trackBarControlProgress.TabIndex = 75;
            this.trackBarControlProgress.Value = 1;
            this.trackBarControlProgress.EditValueChanged += new System.EventHandler(this.trackBarControlProgress_EditValueChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.ContentImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.emptySpaceItem2,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.emptySpaceItem3,
            this.emptySpaceItem4});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(438, 38);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.ControlAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(20, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(20, 1);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(20, 38);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.checkButtonPlay;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(20, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(34, 34);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(34, 34);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(34, 38);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.simpleButtonStop;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(54, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(34, 34);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(34, 34);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(34, 38);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            this.layoutControlItem2.TrimClientAreaToControl = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.trackBarControlProgress;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(88, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(0, 20);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(160, 20);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 0, 2);
            this.layoutControlItem3.Size = new System.Drawing.Size(162, 20);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.labelPosition;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(88, 20);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(49, 18);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(137, 20);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(64, 18);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.labelDuration;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(201, 20);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(49, 18);
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.checkButtonMute;
            this.layoutControlItem6.ControlAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(250, 0);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(34, 34);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(34, 34);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(34, 38);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            this.layoutControlItem6.TrimClientAreaToControl = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.trackBarControlVolume;
            this.layoutControlItem7.ControlAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(284, 0);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(120, 34);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(70, 34);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 7, 2);
            this.layoutControlItem7.Size = new System.Drawing.Size(70, 38);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.simpleButtonExportAudio;
            this.layoutControlItem8.ControlAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(384, 0);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(34, 34);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(34, 34);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(34, 38);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(354, 0);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(30, 30);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(30, 30);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(30, 38);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.ControlAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(418, 0);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(20, 0);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(20, 1);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(20, 38);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // playerTimer
            // 
            this.playerTimer.Enabled = true;
            this.playerTimer.Tick += new System.EventHandler(this.playerTimer_Tick);
            // 
            // CallAudioPlayer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Name = "CallAudioPlayer";
            this.Size = new System.Drawing.Size(438, 38);
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControlVolume.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControlVolume)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControlProgress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControlProgress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButtonExportAudio;
        private System.Windows.Forms.Label labelPosition;
        private System.Windows.Forms.Label labelDuration;
        private DevExpress.XtraEditors.SimpleButton simpleButtonStop;
        public DevExpress.XtraEditors.ZoomTrackBarControl trackBarControlVolume;
        public DevExpress.XtraEditors.TrackBarControl trackBarControlProgress;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        public DevExpress.XtraEditors.CheckButton checkButtonPlay;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private System.Windows.Forms.Timer playerTimer;
        private DevExpress.XtraEditors.CheckButton checkButtonMute;
    }
}
