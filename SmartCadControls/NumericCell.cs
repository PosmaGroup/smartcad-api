﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SmartCadControls
{
    public class NumericCell : DataGridViewTextBoxCell
    {
        public NumericCell()
            : base()
        {
        }

        public NumericCell(int len)
            : base()
        {
            this.MaxInputLength = len;
        }

        internal bool ProcessKeyDown(Keys keyData)
        {
            if (keyData == Keys.D0 ||
                keyData == Keys.D1 ||
                keyData == Keys.D2 ||
                keyData == Keys.D3 ||
                keyData == Keys.D4 ||
                keyData == Keys.D5 ||
                keyData == Keys.D6 ||
                keyData == Keys.D7 ||
                keyData == Keys.D8 ||
                keyData == Keys.D9 ||
                keyData == Keys.NumPad0 ||
                keyData == Keys.NumPad1 ||
                keyData == Keys.NumPad2 ||
                keyData == Keys.NumPad3 ||
                keyData == Keys.NumPad4 ||
                keyData == Keys.NumPad5 ||
                keyData == Keys.NumPad6 ||
                keyData == Keys.NumPad7 ||
                keyData == Keys.NumPad8 ||
                keyData == Keys.NumPad9 ||
                keyData == Keys.Clear ||
                keyData == Keys.Delete ||
                keyData == Keys.Back
                )
            {
                return true;
            }
            else
            {
                if (System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator == ",")
                {
                    if (keyData == Keys.Oemcomma)
                        return true;
                }
                else if (System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator == ".")
                {
                    if (keyData == Keys.Decimal || keyData == (Keys.RButton | Keys.MButton | Keys.Back | Keys.ShiftKey | Keys.Space | Keys.F17))
                        return true;
                }
                return false;
            }
        }
    }
}
