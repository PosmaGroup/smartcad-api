﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadControls.Controls;
using SmartCadCore.ClientData;

namespace SmartCadControls.SyncBoxes
{
    public class GridControlDataDepartmentZoneCoord : GridControlData
    {
        [GridControlRowInfoData]
        public string PointNumber
        {
            get
            {
                return Address.PointNumber;
            }
        }

        [GridControlRowInfoData]
        public double Lat
        {
            get
            {
                return Address.Lat;
            }
        }

        [GridControlRowInfoData]
        public double Lon
        {
            get
            {
                return Address.Lon;
            }
        }

        public DepartmentZoneAddressClientData Address
        {
            get
            {
                return this.Tag as DepartmentZoneAddressClientData;
            }
        }

        public GridControlDataDepartmentZoneCoord(DepartmentZoneAddressClientData address)
            : base(address)
        {
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is DepartmentZoneAddressClientData)
            {
                DepartmentZoneAddressClientData address = (DepartmentZoneAddressClientData)obj;
                if ((this.Lon == address.Lon) && (this.Lat == address.Lat))
                {
                    result = true;
                }
            }
            return result;
        }
    }
}
