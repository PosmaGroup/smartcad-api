using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Windows.Forms;
using System.Drawing;
using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadControls.Filters;

namespace SmartCadControls.SyncBoxes
{
    public abstract class SyncBox2
    {
        private DataGridEx dataGrid;

        public SyncBox2()
        {
        }

        public DataGridEx DataGrid
        {
            get
            {
                return dataGrid;
            }
            set
            {
                this.dataGrid = value;
            }
        }

        public abstract DataGridExData BuildGridData(object objectData);

        public virtual void Sync(IList list, bool unselectAnyWay)
        {
            DataGrid.BeginChanges();
            if (DataGrid.SelectedItemsCount == 0)
            {
                unselectAnyWay = true;
            }
            foreach (object obj in list)
            {
                DataGridExData gridData = BuildGridData(obj);

                FormUtil.InvokeRequired(DataGrid, delegate
                {
                    if (DataGrid.ContainsData(gridData) == false)
                    {
                        DataGrid.AddData(gridData);
                    }
                    else
                    {
                        DataGrid.UpdateData(gridData);
                    }
                });
            }

            FormUtil.InvokeRequired(DataGrid, delegate
            {
                if (unselectAnyWay || DataGrid.SelectedItemsCount == 0)
                    DataGrid.EndChanges(true);
                else if (DataGrid.SelectedItemsCount > 0)
                    DataGrid.EndChanges(false, false);                
            });
        }

        public virtual void Sync(IList list)
        {
            Sync(list, false);
        }

        public virtual void Sync(object objectData, CommittedDataAction action)
        {
            Sync(objectData, action, null);
        }

        public virtual void Sync(object objectData, CommittedDataAction action, FilterBase filter)
        {
            DataGridExData gridData = BuildGridData(objectData);

            bool visible = true;

            switch (action)
            { 
                case CommittedDataAction.Save:
                    {
                        if (filter != null)
                            visible = filter.Filter(objectData);
                       
                        FormUtil.InvokeRequired(DataGrid, delegate
                        {
                            DataGrid.AddData(gridData, visible);
                        });
                    }
                    break;
                case CommittedDataAction.Update:
                    {
                        if (filter != null)
                            visible = filter.Filter(objectData);

                        if (DataGrid.ContainsData(gridData))
                        {
                            FormUtil.InvokeRequired(DataGrid, delegate
                            {
                                DataGrid.UpdateData(gridData, visible);
                            });
                        }
                        else
                        {
                            FormUtil.InvokeRequired(DataGrid, delegate
                            {
                                DataGrid.AddData(gridData, visible);
                            });
                        }
                    }
                    break;
                case CommittedDataAction.Delete:
                    if (DataGrid.ContainsData(gridData) == true)
                    {
                        FormUtil.InvokeRequired(DataGrid, delegate
                        {
                            DataGrid.RemoveData(gridData);
                        });
                    }
                    break;
            }
        }
    }
}
