﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using SmartCadControls.Controls;
using SmartCadCore.ClientData;
using SmartCadCore.Common;

namespace SmartCadControls.SyncBoxes
{
    public class GridControlDataAssignedIncidentNotifications: GridControlData
    {
        private IncidentNotificationClientData incidentNotificationLight;

        public GridControlDataAssignedIncidentNotifications(IncidentNotificationClientData incidentNotificationLight)
            : base(incidentNotificationLight)
        {
            this.incidentNotificationLight = incidentNotificationLight;

            if (incidentNotificationLight.Status != null)
            {
                if (incidentNotificationLight.Status.Name == IncidentNotificationStatusClientData.Delayed.Name ||
                    incidentNotificationLight.Status.Name == IncidentNotificationStatusClientData.Updated.Name ||
                    incidentNotificationLight.Status.Name == IncidentNotificationStatusClientData.Reassigned.Name)
                {
                    incidentNotificationLight.InAttention = true;
                    this.InAttention = true;
                }

                if (incidentNotificationLight.Status.Name == IncidentNotificationStatusClientData.Assigned.Name)
                {
                    if (incidentNotificationLight.InAttention == true)
                    {
                        this.BackColor = Color.Red;
                    }
                    else if (incidentNotificationLight.AvailableRecommendedUnits == false)
                    {
                        this.BackColor = Color.LightGray;
                    }
                    else
                    {
                        this.BackColor = Color.White;
                    }
                }
                else
                {
                    this.BackColor = incidentNotificationLight.Status.Color;
                }
            }
        }

        public bool InAttention
        {
            get
            {
                return incidentNotificationLight.InAttention;
            }
            set
            {
                incidentNotificationLight.InAttention = value;
                if (incidentNotificationLight.Status.Name == IncidentNotificationStatusClientData.Assigned.Name)
                {
                    if (incidentNotificationLight.InAttention == true)
                    {
                        BackColor = Color.Red;
                    }
                    else if (incidentNotificationLight.AvailableRecommendedUnits == false)
                    {
                        BackColor = Color.LightGray;
                    }
                    else
                    {
                        BackColor = Color.White;
                    }
                }
            }
        }

        public int Code
        {
            get
            {
                return incidentNotificationLight.Code;
            }
            set
            {
                throw new Exception(ResourceLoader.GetString2("NotImplementedMethod"));
            }
        }

        public string CustomCode
        {
            get
            {
                return incidentNotificationLight.CustomCode;
            }
            set
            {
                throw new Exception(ResourceLoader.GetString2("NotImplementedMethod"));
            }
        }

        [GridControlRowInfoData(Visible =  true)]
        public string Priority
        {
            get
            {
                return incidentNotificationLight.Priority.ToString();
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string Department
        {
            get
            {
                return incidentNotificationLight.DepartmentType.Name;
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string IncidentTypes
        {
            get
            {
                int count = 0;
                StringBuilder sb = new StringBuilder();
                if (incidentNotificationLight.IncidentTypeCustomCodes != null)
                {
                    foreach (string temp in incidentNotificationLight.IncidentTypeCustomCodes)
                    {
                        sb.Append(temp);
                        if (++count < incidentNotificationLight.IncidentTypeCustomCodes.Count)
                        {
                            sb.Append(", ");
                        }
                        else
                        {
                            sb.Append(".");
                        }
                    }
                }
                return sb.ToString();
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string Zone
        {
            get
            {
                return incidentNotificationLight.DepartmentStation.DepartmentZone.Name;
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string Station
        {
            get
            {
                return incidentNotificationLight.DepartmentStation.Name;
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string Incident
        {
            get
            {
                return incidentNotificationLight.IncidentCustomCode;
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string IncidentNotificationFriendlyCode
        {
            get
            {
                return incidentNotificationLight.FriendlyCustomCode;
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string Status
        {
            get
            {
                return incidentNotificationLight.Status.FriendlyName;
            }
        }

        public IncidentNotificationClientData IncidentNotification
        {
            get
            {
                return this.Tag as IncidentNotificationClientData;
            }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataAssignedIncidentNotifications)
            {
                result = Code.Equals((obj as GridControlDataAssignedIncidentNotifications).Code);
            }
            return result;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
