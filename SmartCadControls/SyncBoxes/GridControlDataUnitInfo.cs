﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using System.Drawing;
using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadControls.Controls;

namespace SmartCadControls.SyncBoxes
{
	public class GridControlDataUnitInfo:GridControlData
	{
		public GridControlDataUnitInfo(UnitClientData tag):base(tag)
		{
		}

		public UnitClientData Unit 
		{
			get
			{
				return Tag as UnitClientData;
			}
			set
			{
				Tag = value;
			}
		}

		[GridControlRowInfoData(Visible = true, Width = 100, Searchable = true/*,GroupIndex = 0*/)]
		public string DepartmentType
		{
			get
			{
				return (Tag as UnitClientData).DepartmentStation.DepartmentZone.DepartmentType.Name;
			}
		}
		
		[GridControlRowInfoData(Visible = true, Width = 100, Searchable = true/*,GroupIndex = 1*/)]
		public string Zone
		{
			get
			{
				return (Tag as UnitClientData).DepartmentStation.DepartmentZone.Name;
			}
		}

		[GridControlRowInfoData(Visible = true, Width = 100, Searchable = true/*, GroupIndex = 2*/)]
		public string Station
		{
			get
			{
				return (Tag as UnitClientData).DepartmentStation.Name;
			}
		}

		[GridControlRowInfoData(Visible = true, Width = 50, Searchable = true)]
		public string CustomCode
		{
			get
			{
				return (Tag as UnitClientData).CustomCode;
			}
		}

		public override bool Equals(object obj)
		{
			if (obj is GridControlDataUnitInfo == false)
				return false;
			else if (((obj as GridControlDataUnitInfo).Tag as UnitClientData).Code != (this.Tag as UnitClientData).Code)
				return false;
			return true;				
		}
	}

	public class GridControlDataUnitDatePosition:GridControlData
	{
		public GridControlDataUnitDatePosition(GPSClientData tag):base(tag)
		{
			
		}

		public GPSClientData UnitPosition
		{
			get
			{
				return Tag as GPSClientData;
			}
			set
			{
				Tag = value;
			}
		}

        [GridControlRowInfoData(Visible = true, Width = 50, Searchable = false)]
        public int Number
        {
            get;
            set;
        }

		[GridControlRowInfoData(Visible = true, Width = 130, Searchable = true, GroupIndex = 0)]
		public string CustomCode
		{
			get
			{
                return UnitPosition.UnitCustomCode;
			}
		}

		[GridControlRowInfoData(Visible = true, Width = 60, Searchable = true)]
		public string Date
		{
			get
			{
                return UnitPosition.CoordinatesDate.ToString("MM/dd/yyyy");
			}
		}

		[GridControlRowInfoData(Visible = true, Width = 60, Searchable = true)]
		public string Hour
		{
			get
			{
                return UnitPosition.CoordinatesDate.ToString("HH:mm:ss");
			}
		}

		public double Lon
		{
			get
			{
                return UnitPosition.Lon;
			}
		}

		public double Lat
		{
			get
			{
                return UnitPosition.Lat;
			}
		}

		private Color color;

		[GridControlRowInfoData(Visible = true, Width = 50, Searchable = false)]
		public Color Color
		{
			get
			{
				return color;
			}
			set
			{
				color = value;
			}
		}

		public override bool Equals(object obj)
		{
			if (obj is GridControlDataUnitDatePosition == false)
				return false;
			else
			{	
				GridControlDataUnitDatePosition gcdudp = obj as GridControlDataUnitDatePosition;
                if ((Tag as GPSClientData).Code != (gcdudp.Tag as GPSClientData).Code)
					return false;
			}
			return true;
		}
	}

	public class GridControlDataAlertNotification : GridControlData
	{
		public GridControlDataAlertNotification(AlertNotificationClientData tag):base(tag)
		{
		}

		[GridControlRowInfoData(Searchable=true,Visible=true)]
		public string Date
		{
			get { return (Tag as AlertNotificationClientData).Date.ToString(); }
		}

		[GridControlRowInfoData(Searchable = true, Visible = true)]
		public string Operator
		{
			get { return (Tag as AlertNotificationClientData).AttendingOperator==null?string.Empty:(Tag as AlertNotificationClientData).AttendingOperator.FirstName + " " + (Tag as AlertNotificationClientData).AttendingOperator.LastName; }
		}

		[GridControlRowInfoData(Searchable = true, Visible = true)]
		public string Plate
		{
			get { return (Tag as AlertNotificationClientData).UnitAlert == null ? string.Empty : (Tag as AlertNotificationClientData).UnitAlert.UnitCustomCode; }
		}

		[GridControlRowInfoData(Searchable = true, Visible = true)]
		public string AlertName
		{
			get
			{
				AlertNotificationClientData alert = (Tag as AlertNotificationClientData);

				if (string.IsNullOrEmpty(alert.SensorName) == true)
					return alert.UnitAlert == null ? string.Empty : (Tag as AlertNotificationClientData).UnitAlert.AlertName;
				else
					return alert.SensorName;
			}
		}

		[GridControlRowInfoData(Searchable = true, Visible = true)]
		public string Priority
		{
			get { return (Tag as AlertNotificationClientData).UnitAlert == null ? string.Empty : ResourceLoader.GetString2((Tag as AlertNotificationClientData).UnitAlert.Priority.ToString()); }
		}

		[GridControlRowInfoData(Searchable = true, Visible = true)]
		public string Status
		{
			get { return ResourceLoader.GetString2((Tag as AlertNotificationClientData).Status.ToString()); }
		}

		[GridControlRowInfoData(Searchable = true, Visible = true)]
		public string Department
		{
			get { return (Tag as AlertNotificationClientData).UnitAlert == null ? string.Empty : (Tag as AlertNotificationClientData).UnitAlert.UnitDepartmentTypeName; }
		}

		public override bool Equals(object obj)
		{
			if (obj is GridControlDataAlertNotification == false)
				return false;
			GridControlDataAlertNotification gcda = obj as GridControlDataAlertNotification;
			if ((gcda.Tag as AlertNotificationClientData).Equals(Tag)==true)
				return true;
			return false;
		}
	}
}
