﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using SmartCadCore.Core;
using DevExpress.XtraGrid.Views.Grid;
using SmartCadCore.Common;
using SmartCadControls.Controls;

namespace SmartCadControls.SyncBoxes
{
    public class GridControlSynBox
    {
        private GridControlEx gridControl;

        public GridControlSynBox(GridControlEx gridControl)
        {
            this.gridControl = gridControl;
            
        }

        public virtual void Sync(IList list)
        {
            FormUtil.InvokeRequired(gridControl,
                delegate
                {
                    gridControl.SetDataSource(list);
                });
        }

        public virtual void Sync(GridControlData data, CommittedDataAction action)
        {
            if (action == CommittedDataAction.Delete)
            {
                gridControl.DeleteItem(data);
            }
            else if (action == CommittedDataAction.Save)
            {
                gridControl.AddOrUpdateItem(data, false);
            }
            else if (action == CommittedDataAction.Update)
            {
                gridControl.AddOrUpdateItem(data);
            }
        }

        public virtual bool Update(IList list)
        {
           bool selectedItem = false;
            FormUtil.InvokeRequired(gridControl,
                delegate
                {
                   selectedItem = gridControl.UpdateDataSource(list);
                });
            return selectedItem;
          
        }
       
               
    }
}
