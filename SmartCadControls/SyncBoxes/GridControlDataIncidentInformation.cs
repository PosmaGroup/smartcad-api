using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using System.Drawing;
using Smartmatic.SmartCad.Service;
using SmartCadCore.Core;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadControls.Controls;

namespace SmartCadControls.SyncBoxes
{
   public class GridControlDataAssociatedCallData : GridControlData
    {
        private PhoneReportClientData phoneReportLight;

		public GridControlDataAssociatedCallData(object tag) :
            base((ClientData)tag)
        {
            this.phoneReportLight = (PhoneReportClientData)Tag;
        }

        [GridControlRowInfoData(Searchable= true)]
        public string Telephone
        {
            get
            {
                if (phoneReportLight.PhoneReportCallerClient.Telephone != null)
                    return phoneReportLight.PhoneReportCallerClient.Telephone;
                else
                    return "";
            }
        }

		[GridControlRowInfoData(Searchable = true)]
        public string OwnerLine
        {
            get
            {
                string name = phoneReportLight.PhoneReportCallerClient.Name;
                if (phoneReportLight.PhoneReportCallerClient.Anonymous != true)
                {
                    name = phoneReportLight.PhoneReportCallerClient.Name;
                }
                else
                {
                    name = ResourceLoader.GetString2("Anonymous");
                }
                return name;
            }
        }

		[GridControlRowInfoData(Searchable = true)]
        public string IncidentTypes
        {
            get
            {
                return phoneReportLight.IncidentTypesText + ".";
            }
        }

        [GridControlRowInfoData(Searchable = true, DisplayFormat = "MM/dd/yyyy HH:mm")]
        public DateTime CallDate
        {
            get
            {
                //return phoneReportLight.RegisteredCallTime.ToString("dd/mm/yyyy HH:mm:ss");
                return phoneReportLight.RegisteredCallTime;
            }
        }

		public override bool Equals(object obj)
		{
			if (obj is GridControlDataAssociatedCallData == false)
				return false;
			GridControlDataAssociatedCallData data = obj as GridControlDataAssociatedCallData;
			if (data.phoneReportLight.CustomCode == this.phoneReportLight.CustomCode)
				return true;
			return false;
		}
    }

	public class GridControlDataAssociatedDispatchOrder : GridControlData
    {
        private DispatchOrderClientData dispatchOrderLight;

		public GridControlDataAssociatedDispatchOrder(object tag) :
			base((ClientData)tag)
        {
            dispatchOrderLight = (DispatchOrderClientData)Tag;
        }

        [GridControlRowInfoData(Searchable = true, Width = 200)]
        public string DepartmentType
        {
            get
            {
                return dispatchOrderLight.DepartmentType.Name;
            }
        }

		[GridControlRowInfoData(Searchable = true, Width = 146)]
        public string UnitCustomCode
        {
            get
            {
                return dispatchOrderLight.Unit.CustomCode;
            }
        }

		[GridControlRowInfoData(Searchable = true, Width = 146)]
        public string UnitTypeName
        {
            get
            {
                return dispatchOrderLight.Unit.Type.Name;
            }
        }

        [GridControlRowInfoData(Searchable = true,Width = 146)]
        public string Status
        {
            get
            {
                return dispatchOrderLight.Status.FriendlyName;
            }
        }

		public override bool Equals(object obj)
		{
			if (obj is GridControlDataAssociatedDispatchOrder == false)
				return false;
			GridControlDataAssociatedDispatchOrder data = obj as GridControlDataAssociatedDispatchOrder;
			if (data.dispatchOrderLight.Code == this.dispatchOrderLight.Code)
				return true;
			return false;
		}
    }
}
