﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using SmartCadCore.ClientData;
using SmartCadControls.Controls;

namespace SmartCadControls.SyncBoxes
{
    public class GridControlDataDispatchOrder: GridControlData
    {
        public DispatchOrderClientData DispatchOrder
        {
            get
            {
                return dispatchOrderLightData;
            }
        }

        private DispatchOrderClientData dispatchOrderLightData;

        public GridControlDataDispatchOrder(DispatchOrderClientData dispatchOrder)
            : base(dispatchOrder)
        {
            if (dispatchOrder.Status != null)
                this.BackColor = dispatchOrder.Status.Color;
            this.dispatchOrderLightData = (DispatchOrderClientData)dispatchOrder;
        }
        
        public int Code
        {
            get
            {
                return dispatchOrderLightData.Code;
            }
            set
            {}
        }

        [GridControlRowInfoData(Visible = true)]
        public string DepartmentType
        {
            get
            {
                return dispatchOrderLightData.DepartmentType.Name;
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string UnitCustomCode
        {
            get
            {
                return dispatchOrderLightData.Unit.CustomCode;
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string UnitType
        {
            get
            {
                return dispatchOrderLightData.Unit.Type.Name;
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string Status
        {
            get
            {
                return dispatchOrderLightData.Unit.Status.FriendlyName;
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string IdRadio
        {
            get
            {
                return dispatchOrderLightData.Unit.IdRadio;
            }
        }

        [GridControlRowInfoData(Visible = true, DisplayFormat = "MM/dd/yyyy HH:mm:ss")]
        public DateTime SendDate
        {
            get
            {
                return dispatchOrderLightData.StartDate;
            }
        }

        [GridControlRowInfoData(Visible = true, DisplayFormat = "MM/dd/yyyy HH:mm:ss")]
        public DateTime ArrivalDate
        {
            get
            {
                return dispatchOrderLightData.ArrivalDate;
            }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataDispatchOrder)
            {
                result = Code.Equals((obj as GridControlDataDispatchOrder).Code);
            }
            return result;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
