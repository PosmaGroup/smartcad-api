﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using SmartCadControls.Controls;
using SmartCadCore.ClientData;
using SmartCadCore.Common;

namespace SmartCadControls.SyncBoxes
{
    public class GridControlDataNewIncidentNotifications : GridControlData
    {

        private IncidentNotificationClientData incidentNotification;

        public GridControlDataNewIncidentNotifications(IncidentNotificationClientData incidentNotification)
            : base(incidentNotification)
        {
            this.BackColor = Color.White;
            this.incidentNotification = incidentNotification;
        }


        public string CustomCode
        {
            get
            {
                return incidentNotification.CustomCode;
            }
            set
            {
                throw new Exception(ResourceLoader.GetString2("NotImplementedMethod"));
            }
        }

        public IncidentNotificationClientData IncidentNotification
        {
            get
            {
                return this.Tag as IncidentNotificationClientData;
            }
        }

        [GridControlRowInfoData(Visible = false)]
        public string CreationDate
        {
            get
            {
                return incidentNotification.CreationDate.ToString("s");
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string Priority
        {
            get
            {
                return incidentNotification.Priority.ToString();
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string Department
        {
            get
            {
                return incidentNotification.DepartmentType.Name;
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string IncidentTypes
        {
            get
            {
                int count = 0;
                StringBuilder sb = new StringBuilder();
                if (incidentNotification.IncidentTypeCustomCodes != null)
                {
                    foreach (string temp in incidentNotification.IncidentTypeCustomCodes)
                    {
                        sb.Append(temp);
                        if (++count < incidentNotification.IncidentTypeCustomCodes.Count)
                        {
                            sb.Append(", ");
                        }
                        else
                        {
                            sb.Append(".");
                        }
                    }
                }
                return sb.ToString();
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string AddressZone
        {
            get
            {
                return incidentNotification.IncidentAddress.Zone;
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string Incident
        {
            get
            {
                return incidentNotification.IncidentCustomCode;
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string IncidentNotificationFriendlyCode
        {
            get
            {
                return incidentNotification.FriendlyCustomCode;
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string MultipleOrganisms
        {
            get
            {
                if (incidentNotification.MultipleOrganisms == true)
                {
                    return ResourceLoader.GetString2("$Message.Yes");
                }
                else
                {
                    return ResourceLoader.GetString2("$Message.No");
                }
            }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataNewIncidentNotifications)
            {
                result = IncidentNotification.Equals((obj as GridControlDataNewIncidentNotifications).IncidentNotification);
            }
            return result;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

    }

}
