﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SmartCadControls
{
    public class TextCell : DataGridViewTextBoxCell
    {
        public TextCell()
            : base()
        {
        }

        public TextCell(int len)
            : base()
        {
            this.MaxInputLength = len;
        }
    }
}
