﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SmartCadControls
{
    public class IntColumn : DataGridViewColumn
    {
        public IntColumn()
            : base(new IntCell())
        {
        }

        public IntColumn(int len)
            : base(new IntCell(len))
        {
        }

        public override DataGridViewCell CellTemplate
        {
            get
            {
                return base.CellTemplate;
            }
            set
            {
                if (value != null &&
                    !value.GetType().IsAssignableFrom(typeof(IntCell)))
                {
                    throw new InvalidCastException("Must be a IntCell");
                }
                base.CellTemplate = value;
            }
        }
    }
}
