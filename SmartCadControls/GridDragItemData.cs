﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadControls
{
    public class GridDragItemData
    {
        #region Private Members

        private DataGridEx dataGrid;
        private ArrayList dragItems;

        #endregion

        #region Public Properties

        public DataGridEx DataGrid
        {
            get
            {
                return dataGrid;
            }
        }

        public ArrayList DragItems
        {
            get
            {
                return dragItems;
            }
        }

        #endregion

        #region Public Methods and Implementation

        public GridDragItemData(DataGridEx dataGrid)
        {
            this.dataGrid = dataGrid;
            this.dragItems = new ArrayList();
        }

        #endregion
    }
}
