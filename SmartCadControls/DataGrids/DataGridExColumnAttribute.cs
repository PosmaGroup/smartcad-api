﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SmartCadControls
{
    public class DataGridExColumnAttribute : Attribute
    {
        protected string headerText;
        protected int width;
        protected Type type;
        protected bool visible = true;
        protected Type column = typeof(DataGridViewTextBoxColumn);
        private string toolTip;
        protected bool searchble;
        protected int maxLength = 1000;
        protected bool creadonly = true;


        public bool ReadOnly
        {
            get
            {
                return creadonly;
            }
            set
            {
                creadonly = value;
            }
        }

        private DataGridViewContentAlignment align;

        public string HeaderText
        {
            get
            {
                return headerText;
            }
            set
            {
                headerText = value;
            }
        }

        public Type Column
        {
            get
            {
                return this.column;
            }
            set
            {
                this.column = value;
            }
        }

        public int Width
        {
            get
            {
                return width;
            }
            set
            {
                width = value;
            }
        }

        public bool Searchble
        {
            get
            {
                return searchble;
            }
            set
            {
                searchble = value;
            }

        }

        public int MaxLength
        {
            get
            {
                return maxLength;
            }
            set
            {
                maxLength = value;
            }
        }

        public Type Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }
        }

        public bool Visible
        {
            get
            {
                return visible;
            }
            set
            {
                visible = value;
            }
        }

        public DataGridViewContentAlignment HeaderAlignment
        {
            get
            {
                return align;
            }
            set
            {
                align = value;
            }
        }

        public string ToolTip
        {
            get
            {
                return toolTip;
            }
            set
            {
                toolTip = value;
            }
        }
    }
}
