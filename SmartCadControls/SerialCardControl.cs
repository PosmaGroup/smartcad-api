using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Smartmatic.SmartCad.Controls.ControlDesigners;
using FontClass = System.Drawing.Font;
using SizeClass = System.Drawing.Size;
using FontFormatClass = SmartCadControls.Controls.FontFormat;
using SmartCadControls.Util;

namespace SmartCadControls
{
	public delegate void ValueChangeEventHandler(object sender, EventArgs e);

	/// <summary>
	/// Summary description for SerialCardControl.
	/// </summary>
	[Designer(typeof (SerialCardControlDesigner)),
		DefaultEvent("ValueChange")]
	public class SerialCardControl : UserControl
	{
		private const int separatorWidth = 1;
		private const int bytePadding = 5;
		private const int controlPadding = 2;
		private NumericTextBox byte4;
		private NumericTextBox byte2;
		private NumericTextBox byte1;
		private NumericTextBox byte3;
		private NumericTextBox byte5;
		private NumericTextBox byte6;

		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private Container components = null;

		public SerialCardControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitForm call

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);

			using (Pen pen = new Pen(Color.Black, 1.9f))
			{
				int y = byte1.Location.Y + byte1.Height - 3;
				int subPadding = (bytePadding - separatorWidth)/2;

				int x1 = byte1.Location.X + byte1.Width + subPadding;
				int x2 = byte2.Location.X - subPadding;
				e.Graphics.DrawLine(pen, x1, y, x2, y);

				x1 = byte2.Location.X + byte2.Width + subPadding;
				x2 = byte3.Location.X - subPadding;
				e.Graphics.DrawLine(pen, x1, y, x2, y);

				x1 = byte3.Location.X + byte3.Width + subPadding;
				x2 = byte4.Location.X - subPadding;
				e.Graphics.DrawLine(pen, x1, y, x2, y);

				x1 = byte4.Location.X + byte4.Width + subPadding;
				x2 = byte5.Location.X - subPadding;
				e.Graphics.DrawLine(pen, x1, y, x2, y);

				x1 = byte5.Location.X + byte5.Width + subPadding;
				x2 = byte6.Location.X - subPadding;
				e.Graphics.DrawLine(pen, x1, y, x2, y);
			}
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.byte4 = new NumericTextBox();
			this.byte2 = new NumericTextBox();
			this.byte1 = new NumericTextBox();
			this.byte3 = new NumericTextBox();
			this.byte6 = new NumericTextBox();
			this.byte5 = new NumericTextBox();
			this.SuspendLayout();
			// 
			// byte4
			// 
            this.byte4.FontFormat = SmartCadControls.Controls.FontFormat.GetFormat("EditBoxFormat");
			this.byte4.InputType = NumericTextBoxType.Byte;
			this.byte4.Location = new System.Drawing.Point(107, 2);
			this.byte4.MaxLength = 3;
			this.byte4.MaxValue = 255;
			this.byte4.MinValue = 0;
			this.byte4.Name = "byte4";
			this.byte4.ShowInHexadecimalFormat = false;
			this.byte4.Size = new System.Drawing.Size(28, 20);
			this.byte4.TabIndex = 3;
			this.byte4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.byte4.Value = 0;
			this.byte4.TextChanged += new System.EventHandler(this.bytes_TextChanged);
			// 
			// byte2
			// 
            this.byte2.FontFormat = SmartCadControls.Controls.FontFormat.GetFormat("EditBoxFormat");
			this.byte2.InputType = NumericTextBoxType.Byte;
			this.byte2.Location = new System.Drawing.Point(37, 2);
			this.byte2.MaxLength = 3;
			this.byte2.MaxValue = 255;
			this.byte2.MinValue = 0;
			this.byte2.Name = "byte2";
			this.byte2.ShowInHexadecimalFormat = false;
			this.byte2.Size = new System.Drawing.Size(28, 20);
			this.byte2.TabIndex = 1;
			this.byte2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.byte2.Value = 0;
			this.byte2.TextChanged += new System.EventHandler(this.bytes_TextChanged);
			// 
			// byte1
			// 
            this.byte1.FontFormat = SmartCadControls.Controls.FontFormat.GetFormat("EditBoxFormat");
			this.byte1.InputType = NumericTextBoxType.Byte;
			this.byte1.Location = new System.Drawing.Point(2, 2);
			this.byte1.MaxLength = 3;
			this.byte1.MaxValue = 255;
			this.byte1.MinValue = 0;
			this.byte1.Name = "byte1";
			this.byte1.ShowInHexadecimalFormat = false;
			this.byte1.Size = new System.Drawing.Size(28, 20);
			this.byte1.TabIndex = 0;
			this.byte1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.byte1.Value = 0;
			this.byte1.TextChanged += new System.EventHandler(this.bytes_TextChanged);
			// 
			// byte3
			// 
            this.byte3.FontFormat = SmartCadControls.Controls.FontFormat.GetFormat("EditBoxFormat");
			this.byte3.InputType =  NumericTextBoxType.Byte;
			this.byte3.Location = new System.Drawing.Point(72, 2);
			this.byte3.MaxLength = 3;
			this.byte3.MaxValue = 255;
			this.byte3.MinValue = 0;
			this.byte3.Name = "byte3";
			this.byte3.ShowInHexadecimalFormat = false;
			this.byte3.Size = new System.Drawing.Size(28, 20);
			this.byte3.TabIndex = 2;
			this.byte3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.byte3.Value = 0;
			this.byte3.TextChanged += new System.EventHandler(this.bytes_TextChanged);
			// 
			// byte6
			// 
            this.byte6.FontFormat = SmartCadControls.Controls.FontFormat.GetFormat("EditBoxFormat");
			this.byte6.InputType =  NumericTextBoxType.Byte;
			this.byte6.Location = new System.Drawing.Point(177, 2);
			this.byte6.MaxLength = 3;
			this.byte6.MaxValue = 255;
			this.byte6.MinValue = 0;
			this.byte6.Name = "byte6";
			this.byte6.ShowInHexadecimalFormat = false;
			this.byte6.Size = new System.Drawing.Size(28, 20);
			this.byte6.TabIndex = 5;
			this.byte6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.byte6.Value = 0;
			this.byte6.TextChanged += new System.EventHandler(this.bytes_TextChanged);
			// 
			// byte5
			// 
            this.byte5.FontFormat = SmartCadControls.Controls.FontFormat.GetFormat("EditBoxFormat");
			this.byte5.InputType = NumericTextBoxType.Byte;
			this.byte5.Location = new System.Drawing.Point(142, 2);
			this.byte5.MaxLength = 3;
			this.byte5.MaxValue = 255;
			this.byte5.MinValue = 0;
			this.byte5.Name = "byte5";
			this.byte5.ShowInHexadecimalFormat = false;
			this.byte5.Size = new System.Drawing.Size(28, 20);
			this.byte5.TabIndex = 4;
			this.byte5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.byte5.Value = 0;
			this.byte5.TextChanged += new System.EventHandler(this.bytes_TextChanged);
			// 
			// SerialCardControl
			// 
			this.Controls.AddRange(new System.Windows.Forms.Control[]
				{
					this.byte6,
					this.byte5,
					this.byte4,
					this.byte3,
					this.byte2,
					this.byte1
				});
			this.Name = "SerialCardControl";
			this.Size = new System.Drawing.Size(207, 24);
			this.ResumeLayout(false);

		}

		#endregion

		[Description("Event fired when the value property of the control is changed"),
			Category("Property Changed")]
		public event ValueChangeEventHandler ValueChange;

		public virtual void OnValueChange(EventArgs e)
		{
			if (ValueChange != null)
			{
				ValueChange(this, e);
			}
		}

		private void bytes_TextChanged(object sender, EventArgs e)
		{
			OnValueChange(EventArgs.Empty);
		}

		public override string Text
		{
			get { return Value.ToString(); }
		}

		[Description("Specifies the Card Serial value of the control"),
			Category("Data")]
		public SerialCardStruct Value
		{
			get { return new SerialCardStruct((byte) byte1.Value, (byte) byte2.Value, (byte) byte3.Value, (byte) byte4.Value, (byte) byte5.Value, (byte) byte6.Value); }
			set
			{
				byte1.Value = (double) value.Byte1;
				byte2.Value = (double) value.Byte2;
				byte3.Value = (double) value.Byte3;
				byte4.Value = (double) value.Byte4;
				byte5.Value = (double) value.Byte5;
				byte6.Value = (double) value.Byte6;
				OnValueChange(EventArgs.Empty);
			}
		}

		[RefreshProperties(RefreshProperties.All),
			DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
			Browsable(false)]
		public override FontClass Font
		{
			get { return base.Font; }
			set
			{
				byte1.Font = value;
				byte2.Font = value;
				byte3.Font = value;
				byte4.Font = value;
				byte5.Font = value;
				byte6.Font = value;
				base.Height = byte1.Height + (2*controlPadding);
				Graphics graph = Graphics.FromHwnd(Handle);
				SizeF sizeF = graph.MeasureString("999", value);
				byte1.Width = ((int) Math.Ceiling((double) sizeF.Width) + 3);
				byte2.Width = byte1.Width;
				byte3.Width = byte1.Width;
				byte4.Width = byte1.Width;
				byte5.Width = byte1.Width;
				byte6.Width = byte1.Width;
				base.Width = (int) ((byte1.Width*6) + ((5*bytePadding) + (2*controlPadding)));
				byte1.Location = new Point(controlPadding, controlPadding);
				byte2.Location = new Point(byte1.Location.X + byte1.Width + bytePadding, 2);
				byte3.Location = new Point(byte2.Location.X + byte2.Width + bytePadding, 2);
				byte4.Location = new Point(byte3.Location.X + byte3.Width + bytePadding, 2);
				byte5.Location = new Point(byte4.Location.X + byte4.Width + bytePadding, 2);
				byte6.Location = new Point(byte5.Location.X + byte5.Width + bytePadding, 2);
				base.Font = value;
			}
		}

		[RefreshProperties(RefreshProperties.All),
			DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
			Browsable(false)]
		public override Color ForeColor
		{
			get { return base.ForeColor; }
			set
			{
				byte1.ForeColor = value;
				byte2.ForeColor = value;
				byte3.ForeColor = value;
				byte4.ForeColor = value;
				byte5.ForeColor = value;
				byte6.ForeColor = value;
				base.ForeColor = value;
			}
		}

		new public int Width
		{
			get { return base.Width; }
			set
			{
				int offset = (int) Math.Round((double) ((value - base.Width)/6), 0);
				byte1.Width += offset;
				byte2.Width += offset;
				byte3.Width += offset;
				byte4.Width += offset;
				byte5.Width += offset;
				byte6.Width += offset;

				byte2.Location = new Point(byte1.Location.X + byte1.Width + bytePadding, byte1.Location.Y);
				byte3.Location = new Point(byte2.Location.X + byte2.Width + bytePadding, byte1.Location.Y);
				byte4.Location = new Point(byte3.Location.X + byte3.Width + bytePadding, byte1.Location.Y);
				byte5.Location = new Point(byte4.Location.X + byte4.Width + bytePadding, byte1.Location.Y);
				byte6.Location = new Point(byte5.Location.X + byte5.Width + bytePadding, byte1.Location.Y);
				base.Width = byte6.Location.X + byte6.Width + controlPadding;
			}
		}

		new public int Height
		{
			get { return base.Height; }
		}

		new public SizeClass Size
		{
			get { return base.Size; }
			set
			{
				Width = value.Width;
				base.Size = new SizeClass(Width, byte1.Height + 4);
			}
		}

		private FontFormatClass fontFormat = FontFormatClass.EditBoxFormat;

		[EditorBrowsable(EditorBrowsableState.Never),
			Browsable(true),
			Description("Specified The font and foreground color used to display text and graphics in the control"),
			Category("Appearance")]
		public FontFormatClass FontFormat
		{
			get { return fontFormat; }
			set
			{
				fontFormat = value;
				ForeColor = fontFormat.Color;
				Font = fontFormat.Font;
			}
		}
	}
}