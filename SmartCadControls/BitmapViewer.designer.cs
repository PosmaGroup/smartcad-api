namespace SmartCadControls.Controls
{
    partial class BitmapViewer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlPictures = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // pnlPictures
            // 
            this.pnlPictures.AccessibleRole = System.Windows.Forms.AccessibleRole.ScrollBar;
            this.pnlPictures.AutoScroll = true;
            this.pnlPictures.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPictures.Location = new System.Drawing.Point(0, 0);
            this.pnlPictures.Name = "pnlPictures";
            this.pnlPictures.Size = new System.Drawing.Size(430, 270);
            this.pnlPictures.TabIndex = 0;
            // 
            // BitmapViewer
            // 
            this.AccessibleRole = System.Windows.Forms.AccessibleRole.ScrollBar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.pnlPictures);
            this.Name = "BitmapViewer";
            this.Size = new System.Drawing.Size(430, 270);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlPictures;


    }
}
