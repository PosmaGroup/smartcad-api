﻿using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadControls
{
    public class GridViewInfoEx : GridViewInfo
    {
        public GridViewInfoEx(DevExpress.XtraGrid.Views.Grid.GridView gridView) : base(gridView) { }
    }
}
