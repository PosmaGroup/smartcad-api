﻿using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Registrator;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Base.Handler;
using DevExpress.XtraGrid.Views.Base.ViewInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadControls
{
    public class GridViewInfoRegistratorEx : GridInfoRegistrator
    {
        public override string ViewName { get { return "GridViewEx"; } }
        public override BaseView CreateView(GridControl grid) { return new GridViewEx(grid as GridControlEx); }
        public override BaseViewInfo CreateViewInfo(BaseView view) { return new GridViewInfoEx(view as GridViewEx); }
        public override BaseViewHandler CreateHandler(BaseView view) { return new GridHandlerEx(view as GridViewEx); }
        public override BaseViewPainter CreatePainter(BaseView view) { return new GridPainterEx(view as GridViewEx); }
    }
}
