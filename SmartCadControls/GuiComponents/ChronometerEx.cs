using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;

using Timer = System.Threading.Timer;
using System.Threading;
using System.Drawing.Text;
using SmartCadCore.Core;

namespace SmartCadControls.Controls
{
    public class ChronometerEx : System.Windows.Forms.Control
    {
        #region Fields

        private Timer timer;
        private int seconds;
        private string showString;
        private bool stopped;

        private event EventHandler secondAdded;

        private Color mColorBackGround = Color.White;

        #endregion

        public ChronometerEx()
        {
            this.MinimumSize = new Size(80, 20);
            this.MaximumSize = new Size(80, 20);
            this.Size = new Size(80, 20);

            this.Region = GetRegion();

            timer = new Timer(new TimerCallback(timer_Tick));
            showString = "00:00:00";

            this.Font = new Font("Microsoft Sans Serif", 11, FontStyle.Bold);

            stopped = true;
        }

        #region Properties

        public Color ColorBackGround
        {
            get
            {
                return mColorBackGround;
            }
            set
            {
                mColorBackGround = value;
            }
        }

        #endregion

        protected override Size DefaultSize
        {
            get
            {
                return new Size(80, 20);;
            }
        }

        protected override void OnPaintBackground(PaintEventArgs pevent)
        {
            base.OnPaintBackground(pevent);

            pevent.Graphics.SmoothingMode = SmoothingMode.AntiAlias;

            Color nextColor = FormUtil.MakeColor(mColorBackGround, -25);
            LinearGradientBrush brush = new LinearGradientBrush(this.ClientRectangle, mColorBackGround, nextColor, LinearGradientMode.Vertical);
            pevent.Graphics.FillRectangle(brush, this.ClientRectangle);
            brush.Dispose();

            GraphicsPath graphPath = GetPath();

            Pen borderPen = new Pen(ControlPaint.Dark(Color.Blue), 1);
            
            pevent.Graphics.DrawPath(borderPen, graphPath);

            borderPen.Dispose();
            graphPath.Dispose();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            e.Graphics.TextRenderingHint = TextRenderingHint.AntiAlias;

            DrawCenterString(e.Graphics, this.ClientRectangle);
        }

        protected virtual Region GetRegion()
        {
            GraphicsPath graphPath = new GraphicsPath();

            try
            {
                Rectangle rect = this.ClientRectangle;
                
                int rectWidth = rect.Width;
                int rectHeight = rect.Height;
                int curveWidth = 8;

                graphPath.AddArc(rectWidth - curveWidth, 0, curveWidth, curveWidth, 270, 90);
                graphPath.AddArc(rectWidth - curveWidth, rectHeight - curveWidth, curveWidth, curveWidth, 0, 90);
                graphPath.AddArc(0, rectHeight - curveWidth, curveWidth, curveWidth, 90, 90);
                graphPath.AddArc(0, 0, curveWidth, curveWidth, 180, 90);

                graphPath.CloseFigure();
            }
            catch
            {
                graphPath.AddRectangle(this.ClientRectangle);
            }


            return new Region(graphPath);
        }

        protected virtual GraphicsPath GetPath()
        {
            GraphicsPath graphPath = new GraphicsPath();

            try
            {
                Rectangle rect = this.ClientRectangle;
                int offset = 0;

                int rectWidth = rect.Width - 1 - offset;
                int rectHeight = rect.Height - 1 - offset;
                int curveWidth = 8;

                graphPath.AddArc(rectWidth - curveWidth, offset, curveWidth, curveWidth, 270, 90);
                graphPath.AddArc(rectWidth - curveWidth, rectHeight - curveWidth, curveWidth, curveWidth, 0, 90);
                graphPath.AddArc(offset, rectHeight - curveWidth, curveWidth, curveWidth, 90, 90);
                graphPath.AddArc(offset, offset, curveWidth, curveWidth, 180, 90);

                graphPath.CloseFigure();
            }
            catch
            {
                graphPath.AddRectangle(this.ClientRectangle);
            }

            return graphPath;
        }

        private void DrawCenterString(Graphics gfx, Rectangle box)
        {
            SizeF ss = gfx.MeasureString(showString, this.Font);

            float left = box.X + (box.Width - ss.Width) / 2;
            float top = box.Y + (box.Height - ss.Height) / 2 + 1;

            /*if (mTextShadow)
            {
                SolidBrush mShadowBrush = new SolidBrush(Color.FromArgb(mTextShadowAlpha, Color.Black));
                gfx.DrawString(this.Text, this.Font, mShadowBrush, left + 1, top + 1);
                mShadowBrush.Dispose();
            }*/
            SolidBrush mTextBrush = new SolidBrush(Color.Black);
            gfx.DrawString(showString, this.Font, mTextBrush, left, top);
            mTextBrush.Dispose();
        }

        private void timer_Tick(object state)
        {
            try
            {
                if (!stopped)
                {
                    seconds++;

                    GenerateShowString();

                    this.Invoke(new MethodInvoker(delegate
                    {
                        OnSecondAdded(EventArgs.Empty);
                        
                        this.Invalidate();
                    }));
                }
            }
            catch (Exception)
            {
            }
        }

        private void GenerateShowString()
        {
            TimeSpan time = TimeSpan.FromSeconds(seconds);

            showString = "";

            if (time.Hours < 10)
            {
                showString = showString + "0" + time.Hours;
            }
            else
            {
                showString = showString + time.Hours;
            }

            showString = showString + ":";

            if (time.Minutes < 10)
            {
                showString = showString + "0" + time.Minutes;
            }
            else
            {
                showString = showString + time.Minutes;
            }

            showString = showString + ":";

            if (time.Seconds < 10)
            {
                showString = showString + "0" + time.Seconds;
            }
            else
            {
                showString = showString + time.Seconds;
            }
        }

        public void Start()
        {
            stopped = false;
            timer.Change(1000, 1000);
        }

        public void Stop()
        {
            stopped = true;
            timer.Change(Timeout.Infinite, Timeout.Infinite);
            this.Invalidate();
        }

        public void Reset()
        {
            seconds = 0;
            showString = "00:00:00";
            this.Invalidate();
        }

        public event EventHandler SecondAdded
        {
            add
            {
                secondAdded += value;
            }
            remove
            {
                secondAdded -= value;
            }
        }

        protected virtual void OnSecondAdded(EventArgs e)
        {
            if (secondAdded != null)
            {
                secondAdded(this, e);
            }
        }

        public int Seconds
        {
            get
            {
                return seconds;
            }
        }
    }
}
