﻿using DevExpress.XtraGrid.Views.Grid;
namespace SmartCadControls
{
    partial class GridControlEx
    {

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeControl()
        {
            (this.MainView as GridView).OptionsView.ShowIndicator = false;
            (this.MainView as GridView).OptionsView.ShowGroupPanel = false;
            //(this.MainView as GridView).OptionsSelection.MultiSelect = false;
            (this.MainView as GridView).OptionsSelection.MultiSelectMode = GridMultiSelectMode.RowSelect;
            (this.MainView as GridView).OptionsSelection.EnableAppearanceFocusedCell = false;
            (this.MainView as GridView).OptionsMenu.EnableFooterMenu = false;
            (this.MainView as GridView).OptionsMenu.EnableColumnMenu = false;
            (this.MainView as GridView).OptionsMenu.EnableGroupPanelMenu = false;
            (this.MainView as GridView).OptionsBehavior.Editable = false;
            this.AllowRestoreSelectionAndFocusedRow = DevExpress.Utils.DefaultBoolean.True;
        }

        #endregion
    }
}

