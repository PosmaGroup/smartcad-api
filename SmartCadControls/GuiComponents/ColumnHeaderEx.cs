﻿using SmartCadControls.Controls;
using SmartCadControls.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SmartCadControls.GuiComponents
{
    public class ColumnHeaderEx : ColumnHeader, IResourceLoadable
    {
        private ColumnType type = ColumnType.Text;

        public ColumnType Type
        {
            get { return type; }
            set { type = value; }
        }

        public ColumnHeaderEx()
        {
        }

        private bool loadFromResources = true;
        #region IResourceLoadable Members

        [Description("This property is used to load text and images resources from Smartmatic.Usp.Resources when it is true."
             + " When false, resources are the same at design time"),
        Category("Behavior"), Browsable(true), DefaultValue(true)]
        public bool LoadFromResources
        {
            get
            {
                // TODO:  Add AlphabeticalTextBox.LoadFromResources getter implementation
                return loadFromResources;
            }
            set
            {
                // TODO:  Add AlphabeticalTextBox.LoadFromResources setter implementation
                loadFromResources = value;
            }
        }

        #endregion
    }
}
