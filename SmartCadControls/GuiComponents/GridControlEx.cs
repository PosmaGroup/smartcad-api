﻿using DevExpress.Utils;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Registrator;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using SmartCadControls.Controls;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadCore.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadControls
{
    public partial class GridControlEx : DevExpress.XtraGrid.GridControl
    {
        public Font defaultFont;
        public Type type;
        public const int MIN_WIDTH = 50;
        public const int MIN_WIDTH_SUMMARY = 70;
        public const int DEFAULT_ROW_HEIGHT = 20;
        public ToolTipController toolTipController;
        public bool enableAutoFilter = false;
        public event ToolTipControllerGetActiveObjectInfoEventHandler CellToolTipNeeded;
        public bool viewTotalRows;

        public bool ViewTotalRows
        {
            get
            {
                return viewTotalRows;
            }
            set
            {
                viewTotalRows = value;
            }
        }

        public GridControlEx()
            : base()
        {
            defaultFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolTipController = new DevExpress.Utils.ToolTipController();
            this.ToolTipController = this.toolTipController;
            this.Font = defaultFont;

        }

        void GridControlEx_SizeChanged(object sender, EventArgs e)
        {
            AutoAdjustColumnWidth();
        }

        protected override BaseView CreateDefaultView()
        {
            return CreateView("GridViewEx");
        }

        protected override void RegisterAvailableViewsCore(InfoCollection collection)
        {
            base.RegisterAvailableViewsCore(collection);
            collection.Add(new GridViewInfoRegistratorEx());
        }

        protected override void OnLoaded()
        {
            base.OnLoaded();
            this.ToolTipController.GetActiveObjectInfo += new ToolTipControllerGetActiveObjectInfoEventHandler(ToolTipController_GetActiveObjectInfo);
            this.SizeChanged += new EventHandler(GridControlEx_SizeChanged);
            GridViewEx gridView = this.MainView as GridViewEx;
            gridView.ViewTotalRows = ViewTotalRows;
        }

        public void ClearData()
        {
            FormUtil.InvokeRequired(this, delegate
            {
                if (this.DataSource != null)
                {
                    (this.DataSource as IList).Clear();
                }
            });
        }

        void ToolTipController_GetActiveObjectInfo(object sender, ToolTipControllerGetActiveObjectInfoEventArgs e)
        {
            if (CellToolTipNeeded != null)
                CellToolTipNeeded(sender, e);
        }

        /// <summary>
        /// This handler avoid the columns dissapear when they are dragged.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void GridControlEx_DragObjectOver(object sender, DragObjectOverEventArgs e)
        {
            GridColumn column = e.DragObject as GridColumn;
            if (column != null && e.DropInfo.Index < 0)
            {
                e.DropInfo.Valid = false;
            }
        }

        public bool EnableAutoFilter
        {
            get
            {
                return enableAutoFilter;
            }
            set
            {
                enableAutoFilter = value;
            }
        }

        public Type Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
                if (value != null)
                {
                    this.DataSource = null;
                    InitializeControl();
                    GridView view = this.MainView as GridView;
                    view.OptionsView.ShowFilterPanelMode = ShowFilterPanelMode.Never;
                    view.OptionsView.ShowAutoFilterRow = EnableAutoFilter;
                    view.OptionsCustomization.AllowFilter = false;
                    //view.OptionsSelection.MultiSelect = false;
                    view.OptionsSelection.UseIndicatorForSelection = false;
                    view.OptionsSelection.EnableAppearanceFocusedCell = false;
                    view.OptionsSelection.EnableAppearanceFocusedRow = true;
                    view.OptionsSelection.MultiSelectMode = GridMultiSelectMode.RowSelect;
                    view.OptionsView.ShowGroupedColumns = true;
                    view.ColumnPanelRowHeight = 20;
                    view.PreviewFieldName = "PreviewFieldValue";
                    view.OptionsView.ColumnAutoWidth = false;
                    (this.MainView as GridView).DragObjectOver += new DragObjectOverEventHandler(GridControlEx_DragObjectOver);
                    IDictionary<PropertyInfo, GridControlRowInfoDataAttribute> columnsInfo = GetColumnsInfo();
                    DevExpress.XtraGrid.Columns.GridColumn column;
                    view.Columns.Clear();
                    IDictionary<int, GridColumn> groupedColumns = new Dictionary<int, GridColumn>();
                    int i = 0;
                    foreach (KeyValuePair<PropertyInfo, GridControlRowInfoDataAttribute> pair in columnsInfo)
                    {
                        try
                        {
                            column = view.Columns.AddField(pair.Key.Name);
                            column.VisibleIndex = i++;
                            column.Visible = pair.Value.Visible;
                            if (pair.Value.NoCaption == false)
                                column.Caption = ResourceLoader.GetString2(pair.Key.Name);
                            column.AppearanceCell.Font = defaultFont;
                            column.AppearanceHeader.Font = defaultFont;
                            column.OptionsFilter.AutoFilterCondition = AutoFilterCondition.Contains;
                            if (pair.Value.MaxWidth == 0)
                                column.MinWidth = MIN_WIDTH;
                            else
                            {
                                column.OptionsColumn.AllowSize = false;
                            }
                            column.GroupIndex = pair.Value.GroupIndex;
                            column.AppearanceCell.Options.UseTextOptions = true;
                            column.FilterMode = ColumnFilterMode.Value;
                            column.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Near;
                            column.OptionsColumn.AllowEdit = pair.Value.Editable;
                            if (pair.Value.ImageIndex != -1)
                            {
                                column.ImageIndex = pair.Value.ImageIndex;
                            }
                            if (pair.Value.GroupIndex != -1)
                            {
                                if (groupedColumns.Keys.Contains(pair.Value.GroupIndex) == false)
                                {
                                    groupedColumns.Add(pair.Value.GroupIndex, column);
                                }
                            }
                            if (pair.Value.ColumnOrder != DevExpress.Data.ColumnSortOrder.None)
                            {
                                column.SortOrder = pair.Value.ColumnOrder;
                            }
                            if (string.IsNullOrEmpty(pair.Value.DisplayFormat) == false)
                            {
                                column.DisplayFormat.FormatString = pair.Value.DisplayFormat;
                                column.DisplayFormat.FormatType = FormatType.Custom;
                            }
                            if (pair.Value.Width > MIN_WIDTH || pair.Value.MaxWidth > 0)
                            {
                                column.Width = pair.Value.Width;
                            }
                            else
                            {
                                if (i == columnsInfo.Count)
                                    column.Width = (Width / columnsInfo.Count) - 4;
                                else
                                    column.Width = Width / columnsInfo.Count;
                            }
                            if (pair.Value.RepositoryType != null)
                            {
                                if (pair.Value.RepositoryType == typeof(RepositoryItemImageComboBox))
                                {
                                    RepositoryItemImageComboBox repository = new RepositoryItemImageComboBox();
                                    repository.SmallImages = GridControlData.ImageListSemaphores;
                                    repository.Appearance.Font = defaultFont;
                                    repository.NullText = " ";
                                    this.RepositoryItems.Add(repository);
                                    column.ColumnEdit = repository;
                                    column.FilterMode = ColumnFilterMode.DisplayText;
                                    column.OptionsColumn.AllowSort = DefaultBoolean.False;
                                }
                                else if (pair.Value.RepositoryType == typeof(RepositoryItemPictureEdit))
                                {
                                    RepositoryItemPictureEdit repository = new RepositoryItemPictureEdit();
                                    repository.CustomHeight = DEFAULT_ROW_HEIGHT;
                                    repository.AutoHeight = false;
                                    repository.SizeMode = PictureSizeMode.Zoom;
                                    repository.NullText = " ";
                                    this.RepositoryItems.Add(repository);
                                    column.ColumnEdit = repository;
                                }
                            }
                            if (pair.Value.Searchable == false)
                            {
                                column.OptionsFilter.AllowAutoFilter = false;
                                column.OptionsFilter.ImmediateUpdateAutoFilter = false;
                                column.OptionsFilter.AllowFilter = false;
                                if (pair.Key.PropertyType == typeof(Color))
                                {
                                    column.RealColumnEdit.CustomDisplayText += new CustomDisplayTextEventHandler(RealColumnEditForColorType_CustomDisplayText);
                                }
                            }
                            else
                            {
                                column.OptionsFilter.AllowAutoFilter = true;
                                column.OptionsFilter.AllowFilter = true;
                                column.OptionsFilter.ImmediateUpdateAutoFilter = true;
                                column.FilterMode = ColumnFilterMode.DisplayText;
                            }

                            if (pair.Value.Width > 0)
                            {
                                column.Width = pair.Value.Width;
                            }
                        }
                        catch (Exception ex)
                        {
                            SmartLogger.Print(ex);
                        }
                    }
                    if (groupedColumns.Count > 0)
                    {
                        List<int> indexList = new List<int>(groupedColumns.Keys);
                        indexList.Sort();
                        foreach (int index in indexList)
                        {
                            GridColumn col = groupedColumns[index];
                            col.GroupIndex = index;
                        }
                    }
                }
            }
        }

        public void AutoAdjustColumnWidth()
        {
            IDictionary<PropertyInfo, GridControlRowInfoDataAttribute> columnsInfo = GetColumnsInfo();
            int i = 0;
            int visibleColumnCount = columnsInfo.Count;
            foreach (KeyValuePair<PropertyInfo, GridControlRowInfoDataAttribute> pair in columnsInfo)
            {
                GridView view = this.MainView as GridView;
                if (view.Columns[pair.Key.Name].Visible == false)
                {
                    visibleColumnCount--;
                }
            }
            foreach (KeyValuePair<PropertyInfo, GridControlRowInfoDataAttribute> pair in columnsInfo)
            {
                try
                {
                    GridView view = this.MainView as GridView;
                    GridColumn column = view.Columns[pair.Key.Name];
                    if (column.Visible == true)
                    {
                        if (i == visibleColumnCount)
                            column.Width = (Width / visibleColumnCount) - 4;
                        else
                            column.Width = Width / visibleColumnCount;
                    }
                }
                catch (Exception ex)
                {
                    SmartLogger.Print(ex);
                }
            }
        }

        void RealColumnEditForColorType_CustomDisplayText(object sender, CustomDisplayTextEventArgs e)
        {
            if (e.Value != null && (e.Value.GetType() == typeof(Color) || e.Value.GetType() == typeof(bool)))
            {
                e.DisplayText = string.Empty;
            }
        }

        public IList SelectedItems
        {
            get
            {
                int[] rowHandles = (this.MainView as GridView).GetSelectedRows();
                IList selectedItems = new ArrayList();
                foreach (int handle in rowHandles)
                {
                    object row = (this.MainView as GridView).GetRow(handle);
                    if (row != null)
                    {
                        selectedItems.Add(row);
                    }
                }
                return selectedItems;
            }
        }

        public IList Items
        {
            get
            {
                IList items = null;
                if (this.DataSource != null)
                {
                    items = this.DataSource as IList;
                }
                else
                {
                    items = new ArrayList();
                }
                return items;
            }
        }

        private IDictionary<PropertyInfo, GridControlRowInfoDataAttribute> GetColumnsInfo()
        {
            IDictionary<PropertyInfo, GridControlRowInfoDataAttribute> rowInfoCollection = new Dictionary<PropertyInfo, GridControlRowInfoDataAttribute>();
            if (type != null)
            {
                PropertyInfo[] properties = type.GetProperties();

                foreach (PropertyInfo propertyInfo in properties)
                {
                    object[] attributes = propertyInfo.GetCustomAttributes(typeof(GridControlRowInfoDataAttribute), false);

                    if (attributes.Length > 0)
                    {
                        GridControlRowInfoDataAttribute attribute = (GridControlRowInfoDataAttribute)attributes[0];
                        rowInfoCollection.Add(propertyInfo, attribute);
                    }
                }
            }
            return rowInfoCollection;
        }

        private GridControlRowInfoDataAttribute GetGridControlRowInfoByProperty(string propertyName)
        {
            GridControlRowInfoDataAttribute result = null;
            PropertyInfo propInfo = Type.GetProperty(propertyName);
            object[] attributes = propInfo.GetCustomAttributes(typeof(GridControlRowInfoDataAttribute), false);

            if (attributes.Length > 0)
            {
                result = (GridControlRowInfoDataAttribute)attributes[0];
            }
            return result;
        }

        public void SetDataSource(IList objectList)
        {
            if (objectList != null && objectList.Count > 0)
            {
                CreateDataSource();
                GridView view = this.MainView as GridView;
                (this.MainView as GridView).BeginDataUpdate();
                int count = 0;

                foreach (object item in objectList)
                {
                    ConstructorInfo dataCI = Type.GetConstructors()[0];
                    GridControlData data = (GridControlData)dataCI.Invoke(new object[] { item });
                    count++;
                    if (count == objectList.Count)
                    {
                        if (this.EnableAutoFilter)
                        {
                            AddOrUpdateItem(data);
                            view.FocusedRowHandle = GridControlEx.AutoFilterRowHandle;
                        }
                        else
                        {
                            AddOrUpdateItem(data, true);
                            view.TopRowIndex = view.GetRowHandle((DataSource as IList).IndexOf(data));
                        }
                    }
                    else
                    {
                        AddOrUpdateItem(data, false, true);
                    }
                }
                FillRepositoryImageComboBox();

                (this.MainView as GridView).EndDataUpdate();
            }
            else
            {
                CreateDataSource();
                GridView view = this.MainView as GridView;
                (this.MainView as GridView).BeginDataUpdate();
                FillRepositoryImageComboBox();

                (this.MainView as GridView).EndDataUpdate();
            }
        }

        public void SelectData(GridControlData data)
        {
            if (DataSource != null)
            {
                if (((GridViewEx)MainView).OptionsSelection.MultiSelect == true)
                    ((GridViewEx)MainView).SelectRow(((GridViewEx)MainView).GetRowHandle(((IList)DataSource).IndexOf(data)));
                else
                    ((GridViewEx)MainView).FocusedRowHandle = ((GridViewEx)MainView).GetRowHandle(((IList)DataSource).IndexOf(data));
            }
        }

        private void FillRepositoryImageComboBox()
        {
            if (type != null)
            {
                PropertyInfo[] properties = type.GetProperties();

                foreach (PropertyInfo propertyInfo in properties)
                {
                    object[] attributes = propertyInfo.GetCustomAttributes(typeof(GridControlRowInfoDataAttribute), false);

                    if (attributes.Length > 0)
                    {
                        GridControlRowInfoDataAttribute attribute = (GridControlRowInfoDataAttribute)attributes[0];
                        if (attribute.RepositoryType != null && attribute.RepositoryType == typeof(RepositoryItemImageComboBox))
                        {
                            GridColumn column = (this.MainView as GridView).Columns[propertyInfo.Name];
                            PropertyInfo realProperty = type.GetProperty(attribute.RealFieldName);
                            RepositoryItem repoBase = column.ColumnEdit;
                            if (repoBase.GetType() == typeof(RepositoryItemImageComboBox))
                            {
                                (repoBase as RepositoryItemImageComboBox).Items.Clear();
                                foreach (GridControlData data in (IList)DataSource)
                                {
                                    double[] val = (double[])realProperty.GetValue(data, null);
                                    (repoBase as RepositoryItemImageComboBox).Items.Add(CreateRepositoryItem(data.Tag.Code, val, attribute.MeasureUnit));
                                }
                            }
                        }
                    }
                }
            }
        }

        private ImageComboBoxItem CreateRepositoryItem(int code, double[] val, string measureUnit)
        {
            //string space = " ";
            double resultValue = 0;
            string description = null;
            int value = 0;
            int imageIndex = 0;
            double threshold = val[1];
            try
            {
                resultValue = val[0];
                //resultValue = Math.Round(resultValue, 1, MidpointRounding.AwayFromZero);
            }
            catch
            {
            }
            if (string.IsNullOrEmpty(measureUnit) == false && measureUnit.Contains("%") == true)
            {
                description = resultValue.ToString("P", CultureInfo.InvariantCulture);
            }
            else
            {
                description = resultValue.ToString() + measureUnit;
            }

            value = code;
            if (threshold == 1) // green
            {
                imageIndex = 1;
            }
            else if (threshold == 2) //yellow
            {
                imageIndex = 2;
            }
            else if (threshold == 3) // red
            {
                imageIndex = 3;
            }
            else // undefined
            {
                imageIndex = 0;
            }
            ImageComboBoxItem item = new ImageComboBoxItem(description, (int)value, imageIndex);
            return item;
        }

        private void CreateDataSource()
        {
            Type t = typeof(BindingList<>);
            t = t.MakeGenericType(new Type[] { Type });
            ConstructorInfo ci = t.GetConstructor(new Type[] { });

            this.DataSource = ci.Invoke(null);
        }

        public void AddOrUpdateItem(GridControlData item)
        {
            FormUtil.InvokeRequired(this, delegate
            {
                AddOrUpdateItemInternal(item, false, false);
            });
        }

        public void AddOrUpdateItem(GridControlData item, bool select)
        {
            FormUtil.InvokeRequired(this, delegate
            {
                AddOrUpdateItemInternal(item, select, false);
            });
        }

        public void AddOrUpdateItem(GridControlData item, bool select, bool multiple)
        {
            FormUtil.InvokeRequired(this, delegate
            {
                AddOrUpdateItemInternal(item, select, multiple);
            });
        }

        public void AddOrUpdateList(IList list)
        {
            FormUtil.InvokeRequired(this,
                     delegate
                     {
                         (this.MainView as GridView).BeginDataUpdate();
                         foreach (GridControlData data in list)
                         {
                             AddOrUpdateItemInternal(data, false, true);
                         }
                         (this.MainView as GridView).EndDataUpdate();
                     });
        }

        private void AddOrUpdateItemInternal(GridControlData item, bool select, bool multiple)
        {
            if (this.DataSource == null)
            {
                CreateDataSource();
            }

            int index = (this.DataSource as IList).IndexOf(item);

            GridViewEx view = this.MainView as GridViewEx;
            GridControlData selectedData = null;
            if (this.SelectedItems.Count > 0)
            {
                selectedData = this.SelectedItems[0] as GridControlData;
            }

            //Avoid raise selection
            if (!select && !multiple)
            {
                view.AllowFocusedRowChanged = false;
            }

            if (index == -1)//Add
            {
                (this.DataSource as IList).Add(item);
            }
            else
            {
                GridControlData oldGridData = (this.DataSource as IList)[index] as GridControlData;
                if ((oldGridData.Tag is ClientData) &&
                    (oldGridData.Tag as ClientData).Version > (item.Tag as ClientData).Version)
                {
                    item = oldGridData;
                }
                (this.DataSource as IList)[index] = item;
            }
            if (select)
            {
                index = (this.DataSource as IList).IndexOf(item);
                int handle = (this.MainView as GridView).GetRowHandle(index);
                (this.MainView as GridView).FocusedRowHandle = handle;
            }
            else if (index == -1)
            {
                if (selectedData != null)
                {
                    index = (this.DataSource as IList).IndexOf(selectedData);
                    int handle = (this.MainView as GridView).GetRowHandle(index);
                    (this.MainView as GridView).FocusedRowHandle = handle;
                }
                else
                {
                    (this.MainView as GridView).FocusedRowHandle = GridControlEx.AutoFilterRowHandle;
                }
            }
            if (!multiple)
                view.AllowFocusedRowChanged = true;
        }

        public void AddOrUpdateRepositoryItemComboBox(GridControlData data)
        {
            if (type != null)
            {
                PropertyInfo[] properties = type.GetProperties();

                foreach (PropertyInfo propertyInfo in properties)
                {
                    object[] attributes = propertyInfo.GetCustomAttributes(typeof(GridControlRowInfoDataAttribute), false);

                    if (attributes.Length > 0)
                    {
                        GridControlRowInfoDataAttribute attribute = (GridControlRowInfoDataAttribute)attributes[0];
                        if (attribute.RepositoryType != null && attribute.RepositoryType == typeof(RepositoryItemImageComboBox))
                        {
                            GridColumn column = (this.MainView as GridView).Columns[propertyInfo.Name];
                            PropertyInfo realProperty = type.GetProperty(attribute.RealFieldName);
                            RepositoryItemImageComboBox repoBase = column.ColumnEdit as RepositoryItemImageComboBox;
                            if (repoBase != null)
                            {
                                double[] val = (double[])realProperty.GetValue(data, null);
                                ImageComboBoxItem comboItem = CreateRepositoryItem(data.Tag.Code, val, attribute.MeasureUnit);
                                int index = RepositoryIndexOf(repoBase, comboItem);
                                try
                                {
                                    repoBase.BeginUpdate();
                                    if (index == -1)
                                    {
                                        repoBase.Items.Add(comboItem);
                                    }
                                    else
                                    {
                                        repoBase.Items[index] = comboItem;
                                    }
                                }
                                finally
                                {
                                    repoBase.EndUpdate();
                                }
                            }
                        }
                    }
                }
            }
        }

        private int RepositoryIndexOf(RepositoryItemImageComboBox repositoryItemImageComboBox, ImageComboBoxItem comboItem)
        {
            int index = -1;
            if (repositoryItemImageComboBox != null && repositoryItemImageComboBox.Items != null && comboItem != null)
            {
                foreach (ImageComboBoxItem existentItem in repositoryItemImageComboBox.Items)
                {
                    if (existentItem.Value.Equals(comboItem.Value))
                    {
                        return repositoryItemImageComboBox.Items.IndexOf(existentItem);
                    }
                }
            }
            return index;
        }

        public void DeleteList(IList list)
        {
            FormUtil.InvokeRequired(this,
                     delegate
                     {
                         (this.MainView as GridView).BeginDataUpdate();
                         
                         (this.MainView as GridView).EndDataUpdate();
                     });
        }

        public void DeleteItem(GridControlData data)
        {
            FormUtil.InvokeRequired(this,
                  delegate
                  {
                      DeleteItemInternal(data);
                  });
        }

        private void DeleteItemInternal(GridControlData data)
        {
            if (type != null)
            {
                PropertyInfo[] properties = type.GetProperties();

                foreach (PropertyInfo propertyInfo in properties)
                {
                    object[] attributes = propertyInfo.GetCustomAttributes(typeof(GridControlRowInfoDataAttribute), false);

                    if (attributes.Length > 0)
                    {
                        GridControlRowInfoDataAttribute attribute = (GridControlRowInfoDataAttribute)attributes[0];
                        if (attribute.RepositoryType != null && attribute.RepositoryType == typeof(RepositoryItemImageComboBox))
                        {
                            GridColumn column = (this.MainView as GridView).Columns[propertyInfo.Name];
                            PropertyInfo realProperty = type.GetProperty(attribute.RealFieldName);
                            RepositoryItem repoBase = column.ColumnEdit;
                            if (repoBase.GetType() == typeof(RepositoryItemImageComboBox))
                            {
                                double[] val = (double[])realProperty.GetValue(data, null);
                                ImageComboBoxItem comboItem = CreateRepositoryItem(data.Tag.Code, val, attribute.MeasureUnit);
                                int index = RepositoryIndexOf(repoBase as RepositoryItemImageComboBox, comboItem);
                                if (index != -1)
                                {
                                    try
                                    {
                                        repoBase.BeginUpdate();
                                        (repoBase as RepositoryItemImageComboBox).Items.RemoveAt(index);
                                    }
                                    finally
                                    {
                                        repoBase.EndUpdate();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (this.DataSource != null)
            {
                int index = (this.DataSource as IList).IndexOf(data);
                if (index != -1)
                {
                    GridViewEx view = (this.MainView as GridViewEx);
                    if (view != null && this.EnableAutoFilter == true)
                    {
                        GridControlData selectedData = null;
                        if (this.SelectedItems.Count > 0)
                        {
                            selectedData = this.SelectedItems[0] as GridControlData;
                        }
                        if ((selectedData != null && selectedData.Tag.Equals(data.Tag)) || selectedData == null)
                        {
                            view.AllowFocusedRowChanged = false;
                        }
                    }
                    try
                    {
                        (this.DataSource as IList).RemoveAt(index);
                    }
                    finally
                    {
                        if (view != null)
                        {
                            if (view.AllowFocusedRowChanged == false)
                            {
                                view.FocusedRowHandle = GridControl.AutoFilterRowHandle;
                                view.AllowFocusedRowChanged = true;
                            }
                        }
                    }
                }
            }
        }

        public GridControlData GetGridControlData(ClientData clientObject)
        {
            GridControlData gridData = null;
            int index = -1;
            ConstructorInfo constructor = type.GetConstructor(new Type[1] { clientObject.GetType() });
            GridControlData data = (GridControlData)constructor.Invoke(new object[1] { clientObject });
            index = (this.DataSource as IList).IndexOf(data);
            if (index != -1)
            {
                gridData = (GridControlData)(this.DataSource as IList)[index];
            }
            return gridData;
        }

        public void SetColumnVisible(string columnName, bool visible)
        {
            (this.MainView as GridView).Columns[columnName].Visible = visible;
        }

        private bool DeleteIfDoesntExist(IList list){
            bool selected = false;
            FormUtil.InvokeRequired(this,
                     delegate
                     {
                         (this.MainView as GridView).BeginDataUpdate();
                         if (this.DataSource != null)
                         {
                             IList dataList = new ArrayList(this.DataSource as IList);
                             foreach (GridControlData data in dataList)
                             {
                                 int index = list.IndexOf(data);
                                 if (index == -1)
                                 {
                                    
                                     if (this.SelectedItems.Count > 0)
                                     {
                                         if ((this.SelectedItems[0] as GridControlData).Equals(data))
                                             selected = true;
                                     }
                                     DeleteItemInternal(data);
                                 }
                             }
                         }
                         (this.MainView as GridView).EndDataUpdate();
                     });
            return selected;
        }

        public bool UpdateDataSource(IList list){
            bool selectedItem;
            IList gridList = new ArrayList();
            if (list != null)
            {
               foreach (object item in list)
                {
                    ConstructorInfo dataCI = Type.GetConstructors()[0];
                    GridControlData data = (GridControlData)dataCI.Invoke(new object[] { item });
                    gridList.Add(data);
                }
            }
             

               selectedItem = DeleteIfDoesntExist(gridList);
                AddOrUpdateListLive(gridList);
                return selectedItem;
        }

     

        private void AddItemLiveInternal(GridControlData data)
        {
            if (this.DataSource == null)
            {
                CreateDataSource();
            }

            GridControlData selectedData = null;
            if (this.SelectedItems.Count > 0)
            {
                selectedData = this.SelectedItems[0] as GridControlData;
            }


            (this.DataSource as IList).Add(data);
            
            /*
                if (selectedData != null)
                {
                    index = (this.DataSource as IList).IndexOf(selectedData);
                    int handle = (this.MainView as GridView).GetRowHandle(index);
                    (this.MainView as GridView).FocusedRowHandle = handle;
                }
                else
                {
                    (this.MainView as GridView).FocusedRowHandle = GridControlEx.AutoFilterRowHandle;
                }
           
            */
        }

        public void AddOrUpdateListLive(IList list)
        {
            FormUtil.InvokeRequired(this,
                     delegate
                     {
                         (this.MainView as GridView).BeginDataUpdate();
                         foreach (GridControlData data in list)
                         {
                             if ((this.DataSource==null) || ((this.DataSource as IList).IndexOf(data) == -1))
                             {
                                 AddItemLiveInternal(data);
                             }
                         }
                         (this.MainView as GridView).EndDataUpdate();
                     });
        }


        
    }
}
