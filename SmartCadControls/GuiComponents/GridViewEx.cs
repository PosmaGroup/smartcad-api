﻿using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using SmartCadControls.Controls;
using SmartCadCore.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SmartCadControls
{
    public class GridViewEx : DevExpress.XtraGrid.Views.Grid.GridView
    {
        private const int MIN_WIDTH_SUMMARY = 80;
        private bool viewTotalRows;
        private Font defaultFont;
        private static GridSummaryItem totalSummaryItem = new GridSummaryItem(DevExpress.Data.SummaryItemType.Count, "Code", ResourceLoader.GetString2("Total") + ": {0:0000}");
        public static readonly Color FilterRowBackColor = Color.FromArgb(247, 250, 190);
        public static readonly Color FilterRowBackColorWhenSomethingWritten = Color.FromArgb(244, 211, 134);
        //public static readonly Color FilterRowBackColor = Color.FromArgb(215, 234, 208);

        public event FocusedRowChangedEventHandler SelectionWillChange;
        public event FocusedRowChangedEventHandler SingleSelectionChanged;

        private System.Threading.Timer timer;
        private FocusedRowChangedEventArgs timerEventArgs;
        private bool enablePreviewLineForFocusedRow;

        public bool ViewTotalRows
        {
            get
            {
                return viewTotalRows;
            }
            set
            {
                viewTotalRows = value;
                if (viewTotalRows == true)
                {
                    this.OptionsMenu.EnableFooterMenu = false;
                    this.OptionsView.ShowFooter = true;
                    this.Appearance.FooterPanel.Font = defaultFont;
                }
            }
        }

        private bool allowFocusedRowChanged = true;

        public bool AllowFocusedRowChanged
        {
            get
            {
                return allowFocusedRowChanged;
            }
            set
            {
                allowFocusedRowChanged = value;
            }
        }

        protected override void RaiseCustomDrawFooter(RowObjectCustomDrawEventArgs e)
        {
            if (ViewTotalRows == true)
            {
                e.Painter.DrawObject(e.Info);

                string sum = string.Format(totalSummaryItem.DisplayFormat, DataRowCount);

                SizeF sumSize = e.Graphics.MeasureString(sum, e.Appearance.Font);
                Point point = new Point(e.Bounds.Right - sumSize.ToSize().Width, e.Bounds.Top + (e.Bounds.Bottom - e.Bounds.Top) / 4);
                e.Graphics.DrawString(sum, e.Appearance.Font, Brushes.Black, point);

                e.Handled = true;
            }
            base.RaiseCustomDrawFooter(e);
        }

        protected override void RaiseCustomDrawRowPreview(RowObjectCustomDrawEventArgs e)
        {
            if (e.RowHandle == DevExpress.XtraGrid.GridControl.AutoFilterRowHandle)
            {
                e.Graphics.DrawString("this string represents the autofilter row", e.Appearance.Font, new SolidBrush(e.Appearance.ForeColor), e.Bounds);
            }
            else
                base.RaiseCustomDrawRowPreview(e);
        }

        private object GetRealValue(int rowHandle, string fieldName)
        {
            object result = null;
            object gridData = this.GetRow(rowHandle);
            if (gridData != null)
            {
                PropertyInfo propInfo = gridData.GetType().GetProperty(fieldName);
                object[] attributes = propInfo.GetCustomAttributes(typeof(GridControlRowInfoDataAttribute), false);
                if (attributes.Length > 0)
                {
                    GridControlRowInfoDataAttribute attribute = (GridControlRowInfoDataAttribute)attributes[0];
                    result = gridData.GetType().InvokeMember(attribute.RealFieldName,
                        BindingFlags.Public | BindingFlags.Instance | BindingFlags.GetProperty, null,
                        gridData, null);
                }
            }
            return result;
        }

        protected override void ApplyColumnsFilterCore(bool updateMRU)
        {
            base.ApplyColumnsFilterCore(updateMRU);
        }

        public override void ApplyColumnsFilter()
        {
            if (this.ActiveFilterCriteria != null)
            {
                string st = this.ActiveFilterCriteria.ToString();
            }
            base.ApplyColumnsFilter();

        }

        protected override void ApplyColumnFilter(GridColumn column, FilterItem listBoxItem)
        {
            base.ApplyColumnFilter(column, listBoxItem);
        }
        private void OnTimer(object state)
        {
            timer.Change(Timeout.Infinite, Timeout.Infinite);

            if (timerEventArgs != null && this.SingleSelectionChanged != null)
            {
                this.SingleSelectionChanged(this, timerEventArgs);
            }
        }

        public GridViewEx() : this(null) { }
        public GridViewEx(GridControlEx grid)
            : base(grid)
        {
            this.enablePreviewLineForFocusedRow = false;
            timer = new System.Threading.Timer(new TimerCallback(OnTimer));
            timerEventArgs = null;
            defaultFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.OptionsSelection.EnableAppearanceFocusedRow = true;
            //TODO: Validar aceptacion de colores
            this.Appearance.FocusedRow.BackColor = Color.FromArgb(116, 179, 225);
            this.Appearance.FocusedRow.ForeColor = Color.Black;
            this.Appearance.FocusedRow.Options.UseBackColor = true;
            this.Appearance.FocusedRow.Options.UseForeColor = true;
            this.Appearance.HideSelectionRow.BackColor = Color.LightGray;
            this.Appearance.HideSelectionRow.ForeColor = Color.Black;
            this.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.Appearance.HideSelectionRow.Options.UseBackColor = true;
        }

        public bool EnablePreviewLineForFocusedRow
        {
            get
            {
                return enablePreviewLineForFocusedRow;
            }
            set
            {
                enablePreviewLineForFocusedRow = value;
                if (enablePreviewLineForFocusedRow == true)
                {
                    this.OptionsView.AutoCalcPreviewLineCount = true;
                    this.OptionsView.ShowPreview = true;
                    this.OptionsView.ShowPreviewLines = true;
                }
            }
        }

        protected override void OnLoaded()
        {
            base.OnLoaded();
            this.OptionsView.ShowFilterPanelMode = ShowFilterPanelMode.Never;
            this.RowCellStyle += new RowCellStyleEventHandler(GridViewEx_RowCellStyle);
            this.CalcPreviewText += new CalcPreviewTextEventHandler(GridViewEx_CalcPreviewText);
        }

        void GridViewEx_RowStyle(object sender, RowStyleEventArgs e)
        {
            if (e.RowHandle > -1 && (e.State & DevExpress.XtraGrid.Views.Base.GridRowCellState.Focused) != DevExpress.XtraGrid.Views.Base.GridRowCellState.Focused)
            {
                e.Appearance.BackColor = (Color)((GridControlData)this.GetRow(e.RowHandle)).BackColor;
            }

        }

        void GridViewEx_CalcPreviewText(object sender, CalcPreviewTextEventArgs e)
        {
            if (enablePreviewLineForFocusedRow == true)
            {
                GridViewEx view = sender as GridViewEx;
                if (!view.IsDataRow(e.RowHandle))
                    return;
                if (e.RowHandle != view.FocusedRowHandle)
                {
                    e.PreviewText = String.Empty;
                    return;
                }
            }
        }

        void GridViewEx_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            if (e.RowHandle == DevExpress.XtraGrid.GridControl.AutoFilterRowHandle)
            {
                e.Appearance.BackColor = FilterRowBackColor;
            }
            else if (e.Column.ColumnType == typeof(Color))
            {
                Color color = (Color)this.GetRowCellValue(e.RowHandle, e.Column);
                e.Appearance.BackColor = color;
            }


        }

        protected override string ViewName { get { return "GridViewEx"; } }

        protected override void RaiseFocusedRowChanged(int prevFocused, int focusedRowHandle)
        {
            if (this.AllowFocusedRowChanged == false)
            {
                focusedRowHandle = GridControlEx.AutoFilterRowHandle;
                prevFocused = GridControlEx.AutoFilterRowHandle;
            }
            if (this.OptionsSelection.MultiSelect == false && this.AllowFocusedRowChanged == true)
            {
                if (this.timer != null)
                {
                    FocusedRowChangedEventArgs e = new FocusedRowChangedEventArgs(prevFocused, focusedRowHandle);
                    if (this.SelectionWillChange != null)
                    {
                        this.SelectionWillChange(this, e);
                    }
                    timerEventArgs = e;
                    timer.Change(1000, Timeout.Infinite);

                    //timer.Stop();
                    //timer.Start();
                }
                if (enablePreviewLineForFocusedRow == true)
                {
                    this.LayoutChanged();
                }
                if (this.CanRaiseEvents == true)
                    base.RaiseFocusedRowChanged(prevFocused, focusedRowHandle);
            }
        }

        //public IList GetVisibleRows(string filter)
        //{

        //    IList rows = new ArrayList();

        //    this.ActiveFilterString = filter;

        //    for (int i = 0; i < this.DataRowCount; i++)
        //    {
        //        rows.Add(this.GetRow(i));
        //    }

        //    this.ActiveFilterString = string.Empty;
        //    return rows;
        //}  

        public IList GetVisibleRows()
        {
            IList rows = new ArrayList();
            for (int i = 0; i < this.RowCount; i++)
            {
                rows.Add(this.GetRow(i) as GridControlData);
            }
            return rows;
        }
    }
}
