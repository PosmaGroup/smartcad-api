using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;

namespace SmartCadControls.Controls
{
    public class ContextMenuStripEx : ContextMenuStrip
    {
        public ContextMenuStripEx()
            : base()
        { }

        public ContextMenuStripEx(IContainer components)
            : base(components)
        { }

        protected override bool ProcessCmdKey(ref Message m, Keys keyData)
        {
            if (Visible == true && CheckValidKeys(keyData) == false)
                return true;
            else
                return base.ProcessCmdKey(ref m, keyData);
        }


        //This keys are valid because they generate the move inside the context menu and the enter
        //key provides the action of the selected toolstrip
        private bool CheckValidKeys(Keys keyPressed)
        {
            bool valid = false;
            if (keyPressed == Keys.Down ||
                keyPressed == Keys.Up ||
                keyPressed == Keys.Left ||
                keyPressed == Keys.Right ||
                keyPressed == Keys.Enter)
                valid = true;
            return valid;
        }
    }

}
