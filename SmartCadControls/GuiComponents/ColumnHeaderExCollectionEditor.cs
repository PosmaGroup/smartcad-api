using System;
using System.ComponentModel.Design;

namespace SmartCadControls.Controls
{
	/// <summary>
	/// Summary description for ColumnHeaderExCollectionEditor.
	/// </summary>
	public class ColumnHeaderExCollectionEditor : CollectionEditor
	{
		protected override Type[] CreateNewItemTypes()
		{
			return new Type[1] {typeof (ColumnHeaderEx)};
		}

		public ColumnHeaderExCollectionEditor(Type type) :
			base(type)
		{
		}
	}
}