using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using MyTimer = System.Threading.Timer;
namespace SmartCadControls.Controls
{
    public partial class BlinkingButtonEx : Button
    {
        private bool blinking = false;
        private Color blinkingColor;
        private Color selectedColor;
        private bool flag = true;
        private MyTimer myTimer;
        private int resolution=500;
        
        public BlinkingButtonEx()
        {
            InitializeComponent();
            blinkingColor = SystemColors.Control;
            selectedColor = SystemColors.Control;
        }

        

        protected override void OnPaint(PaintEventArgs pe)
        {
            // TODO: Add custom paint code here

            // Calling the base class OnPaint
            base.OnPaint(pe);
            
        }

        public bool Blinking
        {
            get
            {
                return blinking;
            }
            set
            {
                blinking = value;
                if (blinking == true)
                {
                    if (myTimer == null)
                    {
                        myTimer = new MyTimer(new TimerCallback(delegate(object state)
                        {
                            if (blinking == true)
                            {
                                if (flag == true)
                                {
                                    flag = false;
                                    this.BackColor = selectedColor;
                                }
                                else
                                {
                                    flag = true;
                                    this.BackColor = blinkingColor;
                                }
                            }
                        }
                        ), null, 0, resolution);
                    }
                    else
                    {
                        myTimer.Change(0, resolution);
                        
                    }
                }
                else
                {
                    if (myTimer != null)
                    {
                        myTimer.Change(Timeout.Infinite, Timeout.Infinite);
                    }
                    this.BackColor = selectedColor;
                }
                this.Invalidate();
            }
        }

        public int Resolution
        {
            get
            {
                return resolution;
            }
            set
            {
                resolution = value;
            }
        }

        public Color BlinkingColor
        {
            get
            {
                return blinkingColor;
            }
            set
            {
                blinkingColor = value;
            }
        }

        public Color SelectedColor
        {
            get
            {
                return selectedColor;
            }
            set
            {
                selectedColor = value;
                this.BackColor = selectedColor;
            }
        }
    }
}
