﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.Globalization;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace SmartCadControls.Controls
{
	public class ListViewEx : ListView, IResourceLoadable
	{
		private bool useTransparentColorForSorterColumnBackGround = false;
		private int sortedColumn = 0;
		private SortOrder sortingOrder = SortOrder.Descending;
		private bool paintSortedColumnBackground = true;
		private ColumnHeaderExCollection columns = null;
		private IContainer components = null;
        private int lastKeyRowIndex = -1;
        private bool mCreating;
        private bool mReadOnly;

		public ListViewEx()
		{
			InitializeComponent();

			columns = new ColumnHeaderExCollection(this);

			ListViewItemSorter = new ListViewExItemsComparer();

			ListViewExItemsComparer comparer = (ListViewItemSorter as ListViewExItemsComparer);

			comparer.SortedColumn = sortedColumn;
       
		}

		public int SortedColumn
		{
			get { return sortedColumn; }
			set
			{
				sortedColumn = value;
				OnColumnClick(new ColumnClickEventArgs(sortedColumn));
			}
		}


        
        public bool ReadOnly
        {
            get { return mReadOnly; }
            set { mReadOnly = value;
                if (mReadOnly)
                {
                    this.BackColor = SystemColors.Control;
                    this.AllowColumnReorder = false;
                }
            }
        }
    
        protected override void OnItemCheck(ItemCheckEventArgs e)
        {
            if (!mCreating && mReadOnly) e.NewValue = e.CurrentValue;
            base.OnItemCheck(e);
        }


		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

        protected override void OnKeyDown(KeyEventArgs e)
        {
            Keys keys = e.KeyData & Keys.KeyCode;

            if ((Keys)e.KeyValue >= Keys.NumPad0 && (Keys)e.KeyValue <= Keys.NumPad9)
            {
                e = new KeyEventArgs((Keys)(e.KeyValue - (int)Keys.D0));
            }
            else if ((Keys)e.KeyValue == Keys.ShiftKey)
            {
                e = new KeyEventArgs((Keys)(e.KeyValue - (int)Keys.OemPeriod));
            }
            else
            {
                base.OnKeyDown(e);
            }

            SelectFirstLetterInAColumn(e.KeyValue);
        }

        private void ClearSelection()
        {
            this.SelectedItems.Clear();
        }

        private void SelectFirstLetterInAColumn(int keyValue)
        {
            
                if (0 <= sortedColumn && sortedColumn < Columns.Count && Items.Count > 0)
                {
                    if (Char.IsLetterOrDigit(Convert.ToChar(keyValue)))
                    {
                        ClearSelection();

                        if (SearchFirstLetterInAColumn(keyValue, lastKeyRowIndex + 1, Items.Count) == false)
                        {
                            SearchFirstLetterInAColumn(keyValue, 0, lastKeyRowIndex + 1);
                        }
                    }
                }
                else
                {
                    lastKeyRowIndex = -1;
                }
            
        }

        private bool SearchFirstLetterInAColumn(int keyValue, int start, int end)
        {
            for (int i = start; (i < end); i++)
            {
                ListViewItem item = Items[i];
                string val = item.SubItems[sortedColumn].Text.ToString();

                if (val.Length > 0 &&
                    (Char.ToUpper(val[0]) == keyValue ||
                    Char.ToLower(val[0]) == keyValue))
                {                   
                    item.Selected = true;
                    item.EnsureVisible();
                    //FirstDisplayedScrollingRowIndex = row.Index;
                    lastKeyRowIndex = item.Index;
                    return true;
                }
            }
            return false;
        }

        protected override void OnItemSelectionChanged(ListViewItemSelectionChangedEventArgs e)
        {
            if (!mCreating && mReadOnly)
                ClearSelection();
            lastKeyRowIndex = e.ItemIndex;
            base.OnItemSelectionChanged(e);
        }

      

		protected override void OnSizeChanged(EventArgs e)
		{
			base.OnSizeChanged(e);
			if (erasedRectangle.Height < Height)
			{
				ListViewExItemsComparer comparer = ListViewItemSorter as ListViewExItemsComparer;
				Rectangle rc = new Rectangle(erasedRectangle.X, erasedRectangle.Y + erasedRectangle.Height, erasedRectangle.Width, Height - erasedRectangle.Height);
                                //Graphics g = CreateGraphics();
                                //using ( Brush b = new SolidBrush(Color.WhiteSmoke) )
                                //{
                                //    g.FillRectangle(b, rc.Left, rc.Top+oldHeight, rc.Width, Height-oldHeight);
                                //}
				Invalidate(rc);
			}
			erasedRectangle = new Rectangle(erasedRectangle.Left, erasedRectangle.Top, erasedRectangle.Width, Height);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}

		#endregion

		public bool UseTransparentColorForSorterColumnBackGround
		{
			get { return useTransparentColorForSorterColumnBackGround; }
			set { useTransparentColorForSorterColumnBackGround = value; }
		}

        protected override void OnColumnClick(ColumnClickEventArgs e)
        {
            if (!(!mCreating && mReadOnly))
            {            

                int lastSortedColumn = sortedColumn;
                sortedColumn = e.Column;

                base.OnColumnClick(e);


                ListViewItemSorter = new ListViewExItemsComparer();

                ListViewExItemsComparer comparer = ListViewItemSorter as ListViewExItemsComparer;

                comparer.SortedColumn = e.Column;

                if (comparer != null)
                {
                    Invalidate();
                    if (lastSortedColumn == e.Column)
                    {
                        //				if(comparer.SortedColumn == e.Column)
                        //				{
                        //					if(comparer.Sorting==SortOrder.Descending)
                        //						comparer.Sorting = SortOrder.Ascending;
                        //					else
                        //						comparer.Sorting = SortOrder.Descending;


                        if (this.sortingOrder == SortOrder.Descending)
                        {
                            comparer.Sorting = SortOrder.Ascending;
                            this.sortingOrder = SortOrder.Ascending;
                        }
                        else
                        {
                            comparer.Sorting = SortOrder.Descending;
                            this.sortingOrder = SortOrder.Descending;
                        }

                        Invalidate(GetHeaderItemRect(e.Column), true);
                    }
                    else
                    {
                        //int lastSortedColumn = comparer.SortedColumn;

                        comparer.Sorting = SortOrder.Ascending;
                        this.sortingOrder = SortOrder.Ascending;

                        Region region = new Region(GetHeaderItemRect(lastSortedColumn));
                        region.Union(GetHeaderItemRect(e.Column));
                        Invalidate(true);
                    }
                }
                Sort();
            }
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content),
			Category("Behavior")]
		new public ColumnHeaderExCollection Columns
		{
			get { return columns; }
		}

		[Category("Behavior")]
		public bool PaintSortedColumnBackground
		{
			get { return paintSortedColumnBackground; }
			set
			{
				paintSortedColumnBackground = value;
				if (paintSortedColumnBackground)
				{
					ListViewItemSorter = ListViewExItemsComparer.Default;
				}
				else
				{
					ListViewItemSorter = null;
				}
			}
		}

		private IntPtr hHeader = IntPtr.Zero;

		protected override void OnHandleCreated(EventArgs e)
        {
            mCreating = true;
            base.OnHandleCreated(e);
            mCreating = false;
		    hHeader = WindowsImports.GetDlgItem(Handle, 0);
		}

        const int WM_CONTEXTMENU = 0x007B;

		protected override void WndProc(ref Message message)
		{
			if (!DesignMode)
			{
				switch (message.Msg)
				{
                    case (int) WM_CONTEXTMENU:
                        if (message.LParam.ToInt32() != -1)
                        {
                            Point pt = this.PointToClient(new Point(message.LParam.ToInt32()));
                            Rectangle rec = GetHeaderCtrlRect();
                            if (rec.Bottom < pt.Y)
                                base.WndProc(ref message);
                        }
                        break;
					case (int) Msg.WM_ERASEBKGND:
						PaintBackground(ref message);
						break;
					case (int) Msg.WM_NOTIFY:
						NMHDR nm1 = (NMHDR) message.GetLParam(typeof (NMHDR));
						if (nm1.code == (int) NotificationMessages.NM_CUSTOMDRAW)
						{
							message.Result = (IntPtr) CustomDrawReturnFlags.CDRF_DODEFAULT;
							NMCUSTOMDRAW nmcd = (NMCUSTOMDRAW) message.GetLParam(typeof (NMCUSTOMDRAW));
							if (nmcd.hdr.hwndFrom == hHeader)
							{
								switch (nmcd.dwDrawStage)
								{
									case (int) CustomDrawDrawStateFlags.CDDS_PREPAINT:
										/**/
										message.Result = (IntPtr) CustomDrawReturnFlags.CDRF_NOTIFYITEMDRAW;
										break;
									case (int) CustomDrawDrawStateFlags.CDDS_ITEMPREPAINT:
										ListViewExItemsComparer comparer = ListViewItemSorter as ListViewExItemsComparer;
										//	if(comparer!=null && ((int)nmcd.dwItemSpec)==comparer.SortedColumn)

										if (((int) nmcd.dwItemSpec) == this.sortedColumn)
										{
											base.WndProc(ref message);
											DoHeaderCustomDrawing(ref message);
										}
										else
										{
											base.WndProc(ref message);
										}
										break;
									default:
										base.WndProc(ref message);
										break;
								}
							}
								/*h*/
							else
							{
								base.WndProc(ref message);
							}
						}
						else
						{
							base.WndProc(ref message);
						}
						break;
					case (int) ReflectedMessages.OCM_NOTIFY:
						NMHDR nm2 = (NMHDR) message.GetLParam(typeof (NMHDR));
						if (nm2.code == (int) NotificationMessages.NM_CUSTOMDRAW)
						{
							message.Result = (IntPtr) CustomDrawReturnFlags.CDRF_DODEFAULT;
							NMCUSTOMDRAW nmcd = (NMCUSTOMDRAW) message.GetLParam(typeof (NMCUSTOMDRAW));

							if (nmcd.hdr.hwndFrom == Handle)
							{
								switch (nmcd.dwDrawStage)
								{
									case (int) CustomDrawDrawStateFlags.CDDS_PREPAINT:
										/**/
										message.Result = (IntPtr) CustomDrawReturnFlags.CDRF_NOTIFYITEMDRAW;
										break;
									case (int) CustomDrawDrawStateFlags.CDDS_ITEMPREPAINT:
										/**/
										message.Result = (IntPtr) CustomDrawReturnFlags.CDRF_NOTIFYSUBITEMDRAW;
										break;
									case (int) (CustomDrawDrawStateFlags.CDDS_ITEMPREPAINT | CustomDrawDrawStateFlags.CDDS_SUBITEM):
										DoListCustomDrawing(ref message);
										break;
									default:
										base.WndProc(ref message);
										break;
								}
							}
								/*h*/
							else
							{
								base.WndProc(ref message);
							}
						}
						else
						{
							base.WndProc(ref message);
						}
						break;
					default:
						base.WndProc(ref message);
						break;
				}
			}
			else
			{
				base.WndProc(ref message);
			}
		}

		private Rectangle GetHeaderCtrlRect()
		{
			RECT rc = new RECT();
			WindowsImports.GetClientRect(hHeader, ref rc);
			return new Rectangle(rc.left, rc.top, rc.right - rc.left, rc.bottom - rc.top);

		}

		private Rectangle GetHeaderItemRect(int index)
		{
			RECT rc = new RECT();
			WindowsImports.SendMessage(hHeader, (int) HeaderControlMessages.HDM_GETITEMRECT, index, ref rc);
			return new Rectangle(rc.left, rc.top, rc.right - rc.left, rc.bottom - rc.top);
		}

		private void DoHeaderCustomDrawing(ref Message m)
		{
			NMCUSTOMDRAW nmcd = (NMCUSTOMDRAW) m.GetLParam(typeof (NMCUSTOMDRAW));
			Graphics g = Graphics.FromHdc(nmcd.hdc);

			int col = (int) nmcd.dwItemSpec;
			Rectangle rc = GetHeaderItemRect(col);

			int itemRight = rc.Left + rc.Width;
			using (Brush b = new SolidBrush(SystemColors.Control))
			{
				g.FillRectangle(b, rc.Left, rc.Top, rc.Width, rc.Height);
				using (Pen pW = new Pen(Color.White, 1), pB = new Pen(Color.Black, 1), pC = new Pen(SystemColors.ControlDark, 1))
				{
					g.DrawLine(pW, rc.Left, rc.Top, rc.Left + rc.Width, rc.Top);
					g.DrawLine(pW, rc.Left, rc.Top, rc.Left, rc.Top + rc.Height);
					g.DrawLine(pB, rc.Left, rc.Top + rc.Height - 1, rc.Left + rc.Width, rc.Top + rc.Height - 1);
					g.DrawLine(pC, rc.Left + 1, rc.Top + rc.Height - 2, rc.Left + rc.Width - 2, rc.Top + rc.Height - 2);
					g.DrawLine(pB, rc.Left + rc.Width - 1, rc.Top, rc.Left + rc.Width - 1, rc.Top + rc.Height);
					g.DrawLine(pC, rc.Left + rc.Width - 2, rc.Top + 1, rc.Left + rc.Width - 2, rc.Top + rc.Height - 2);
				}
			}

			int gap = 4;

			int arrowRectangleX = rc.Left + rc.Width - rc.Height - gap;
			SizeF minStringSize = Size.Empty;
			if (Columns[col].Text.Length != 0)
			{
				minStringSize = g.MeasureString(Columns[col].Text.Substring(0, 1), Font);
			}
			Rectangle arrowRectangle = Rectangle.Empty;
			if (paintSortedColumnBackground)
			{
				if (arrowRectangleX > (rc.Left + (int) (minStringSize.Width + (2*gap))))
				{
					arrowRectangle = new Rectangle(arrowRectangleX, 0, rc.Height, rc.Height);
					ListViewExItemsComparer comparer = ListViewItemSorter as ListViewExItemsComparer;
					//	if (comparer.Sorting == SortOrder.Ascending)
					if (sortingOrder == SortOrder.Ascending)
					{
						DrawUpArrow(g, arrowRectangle);
					}
					else
					{
						DrawDownArrow(g, arrowRectangle);
					}
				}
			}

			RectangleF stringRectangle = new RectangleF(rc.Location.X, rc.Location.Y + 2, rc.Width - arrowRectangle.Width, rc.Height);
			StringFormat format = new StringFormat(StringFormatFlags.NoWrap);
			format.LineAlignment = StringAlignment.Near;
			switch (Columns[col].TextAlign)
			{
				case HorizontalAlignment.Center:
					format.Alignment = StringAlignment.Center;
					break;
				case HorizontalAlignment.Left:
					format.Alignment = StringAlignment.Near;
					stringRectangle.Offset(4, 0);
					break;
				default:
					format.Alignment = StringAlignment.Far;
					stringRectangle.Inflate(-4, 0);
					break;
			}
			format.Trimming = StringTrimming.EllipsisCharacter;
			g.DrawString(Columns[col].Text, Font, new SolidBrush(ForeColor), stringRectangle, format);

			g.Dispose();
			m.Result = (IntPtr) CustomDrawReturnFlags.CDRF_SKIPDEFAULT;
		}

		private void DrawUpArrow(Graphics g, Rectangle rc)
		{
			int xTop = rc.Left + rc.Width/2;
			int yTop = (rc.Height - 6)/2;

			int xLeft = xTop - 6;
			int yLeft = yTop + 6;

			int xRight = xTop + 6;
			int yRight = yTop + 6;

			using (Pen p = new Pen(SystemColors.ControlDarkDark))
			{
				g.DrawLine(p, xLeft, yLeft, xTop, yTop);
			}
			using (Pen p = new Pen(Color.White))
			{
				g.DrawLine(p, xRight, yRight, xTop, yTop);
			}
			using (Pen p = new Pen(Color.White))
			{
				g.DrawLine(p, xLeft, yLeft, xRight, yRight);
			}
		}

		private void DrawDownArrow(Graphics g, Rectangle rc)
		{
			int xBottom = rc.Left + rc.Width/2;

			int xLeft = xBottom - 6;
			int yLeft = (rc.Height - 6)/2;
			;

			int xRight = xBottom + 6;
			int yRight = (rc.Height - 6)/2;

			int yBottom = yRight + 6;

			using (Pen p = new Pen(SystemColors.ControlDarkDark))
			{
				g.DrawLine(p, xLeft, yLeft, xBottom, yBottom);
			}
			using (Pen p = new Pen(Color.White))
			{
				g.DrawLine(p, xRight, yRight, xBottom, yBottom);
			}
			using (Pen p = new Pen(SystemColors.ControlDarkDark))
			{
				g.DrawLine(p, xLeft, yLeft, xRight, yRight);
			}

		}

		private void PaintBackground(ref Message message)
		{
			base.WndProc(ref message);
			ListViewExItemsComparer comparer = ListViewItemSorter as ListViewExItemsComparer;
			IntPtr hDC = (IntPtr) message.WParam;
			if (!paintSortedColumnBackground || comparer == null)
			{
				return;
			}

			Graphics g = Graphics.FromHdc(hDC);

			if (Items.Count == 0)
			{
				return;
			}

			if (!useTransparentColorForSorterColumnBackGround)
			{
				Rectangle rc;
				//	if (comparer.SortedColumn != 0 )
				if (this.sortedColumn != 0)
				{
					//rc = GetSubItemRect(0, comparer.SortedColumn);
					rc = GetSubItemRect(0, sortedColumn);
				}
				else
				{
					//rc = GetSubItemRect(0, comparer.SortedColumn);
					rc = GetSubItemRect(0, sortedColumn);

					//Rectangle headerRect = GetHeaderItemRect(comparer.SortedColumn);
					Rectangle headerRect = GetHeaderItemRect(sortedColumn);
					rc = new Rectangle(headerRect.Left, rc.Top, headerRect.Width, Height);
				}
				using (Brush b = new SolidBrush(Color.WhiteSmoke))
				{
					try
					{
						g.FillRectangle(b, rc.Left, rc.Top, rc.Width, Height);
						erasedRectangle = new Rectangle(rc.Left, rc.Top, rc.Width, Height);
					}
					catch (Exception ex)
					{
						Console.WriteLine("Error fatal al pintar el listviewex :" + ex.Message);
					}
					//				if(rc.Width!=Columns[comparer.SortedColumn].Width || Height!=Size.Height)
					//					Console.WriteLine("El ancho es: " + rc.Width + " y El alto es: " + rc.Height);
					//				else
					//					Console.WriteLine("El ancho es: " + rc.Width + " y El alto es: " + rc.Height);
				}
			}
		}

		private Rectangle erasedRectangle = Rectangle.Empty;

		private Rectangle GetSubItemRect(int row, int col)
		{
			RECT rc = new RECT();
			rc.top = col;
			rc.left = (int) SubItemPortion.LVIR_BOUNDS;
			WindowsImports.SendMessage(Handle, (int) ListViewMessages.LVM_GETSUBITEMRECT, row, ref rc);

			if (col == 0)
			{
				// The LVM_GETSUBITEMRECT message does not give us the rectangle for the first subitem
				// since it is not considered a subitem
				// obtain the rectangle for the header control and calculate from there
				Rectangle headerRect = GetHeaderItemRect(col);
				return new Rectangle(rc.left, rc.top, headerRect.Width, rc.bottom - rc.top);
			}

			return new Rectangle(rc.left, rc.top, rc.right - rc.left, rc.bottom - rc.top);
		}

		private void DoListCustomDrawing(ref Message m)
		{
			ListViewExItemsComparer comparer = ListViewItemSorter as ListViewExItemsComparer;
			NMLVCUSTOMDRAW lvcd = (NMLVCUSTOMDRAW) m.GetLParam(typeof (NMLVCUSTOMDRAW));
			int physicalCol = lvcd.iSubItem;
			int row = (int) lvcd.nmcd.dwItemSpec;
			int logicalCol = OrderToIndex(physicalCol);


			if (Items.Count != 0 && Items.Count > row)
			{
				ListViewItem lvi = Items[row];
				Rectangle rc;
				if (logicalCol == 0 && physicalCol != logicalCol)
				{
					rc = GetSubItemRect(row, physicalCol);
					Rectangle headerRect = GetHeaderItemRect(logicalCol);
					rc = new Rectangle(headerRect.Left, rc.Top, headerRect.Width, rc.Height);
				}
				else
				{
					rc = GetSubItemRect(row, logicalCol);
				}

				Graphics g = Graphics.FromHdc(lvcd.nmcd.hdc);

				Color subItemBackColor = lvi.BackColor;
				//if(PaintSortedColumnBackground && logicalCol==comparer.SortedColumn && !useTransparentColorForSorterColumnBackGround)
				if (PaintSortedColumnBackground && logicalCol == this.sortedColumn && !useTransparentColorForSorterColumnBackGround)
				{
					subItemBackColor = Color.WhiteSmoke;
				}
				else if (Items[row].SubItems.Count > logicalCol && !lvi.UseItemStyleForSubItems)
				{
					ListViewItem.ListViewSubItem subItem = Items[row].SubItems[logicalCol];
					subItemBackColor = subItem.BackColor;
				}
				if (lvi.Selected && Focused && (FullRowSelect || logicalCol == 0))
				{
					subItemBackColor = SystemColors.Highlight;
				}
				else if (lvi.Selected && !HideSelection && (FullRowSelect || logicalCol == 0))
				{
					subItemBackColor = Color.FromArgb(SystemColors.Highlight.A, (SystemColors.Highlight.R + 10 <= 255 ? SystemColors.Highlight.R + 10 : SystemColors.Highlight.R), (SystemColors.Highlight.G + 10 <= 256 ? SystemColors.Highlight.G + 10 : SystemColors.Highlight.G), (SystemColors.Highlight.B + 10 <= 256 ? SystemColors.Highlight.B + 10 : SystemColors.Highlight.B));
				}

				using (Brush b = new SolidBrush(subItemBackColor))
				{
					g.FillRectangle(b, rc.Left, rc.Top, rc.Width, rc.Height);
				}

				int checkBoxesWidth = (CheckBoxes ? 17 : 0);

				if (logicalCol == 0 && lvi.ImageList != null && lvi.ImageList.Images.Count > lvi.ImageIndex && lvi.ImageIndex != -1)
				{
					Image image = lvi.ImageList.Images[lvi.ImageIndex];
					g.DrawImage(image, new Rectangle(rc.Left + 2 + checkBoxesWidth, rc.Top, 16, 16), new Rectangle(0, 0, image.Width, image.Height), GraphicsUnit.Pixel);
				}

				if (logicalCol == 0 && CheckBoxes)
				{
					using (Pen penCheckBox = new Pen(Color.Black, 2f))
					{
						Rectangle rectCheckBox = new Rectangle(rc.Left + 5, rc.Top + 3, 11, 11);
						if (lvi.Checked)
						{
//							using(Pen penCheck = new Pen(Color.Black))
//							{
//								Point point1 = new Point(rectCheckBox.X+2,rectCheckBox.Y+4);
//								Point point2 = new Point(rectCheckBox.X+4,rectCheckBox.Y+7);
//								g.DrawLine(penCheckBox,point1,point2);
//								Point point3 = new Point(rectCheckBox.X+8,rectCheckBox.Y+2);
//								g.DrawLines(penCheck,new Point[]{point1,point2,point3});
//							}

							Image checkImage = Image.FromStream(Assembly.GetAssembly(GetType()).GetManifestResourceStream("Smartmatic.SmartCad.Controls.Images.Check.bmp"));
							g.DrawImage(checkImage, new Rectangle(rectCheckBox.X, rectCheckBox.Y, rectCheckBox.Width, rectCheckBox.Height), new Rectangle(0, 0, checkImage.Width, checkImage.Height), GraphicsUnit.Pixel);
//							g.DrawImage(checkImage,rectCheckBox,new Rectangle(1,0,12,11),GraphicsUnit.Pixel);
						}
						g.DrawRectangle(penCheckBox, rectCheckBox);
					}
				}

				if (Items[row].SubItems.Count > logicalCol)
				{
					ListViewItem.ListViewSubItem subItem = Items[row].SubItems[logicalCol];
					Font subItemFont = lvi.UseItemStyleForSubItems ? lvi.Font : subItem.Font;
					Color subItemForeColor = lvi.UseItemStyleForSubItems ? lvi.ForeColor : subItem.ForeColor;
					subItemForeColor = (lvi.Selected && (logicalCol == 0 || FullRowSelect) && (Focused || !HideSelection)) ? SystemColors.HighlightText : subItemForeColor;

					RectangleF stringRectangle = RectangleF.Empty;
					if (logicalCol == 0 && lvi.ImageList != null /*&& lvi.ImageList.Images.Count>lvi.ImageIndex*/)
					{
						stringRectangle = new RectangleF(rc.Left + 16 + checkBoxesWidth, rc.Top, rc.Width - 16 - checkBoxesWidth, rc.Height);
					}
					else
					{
						stringRectangle = new RectangleF(rc.Left + checkBoxesWidth, rc.Top, rc.Width - checkBoxesWidth, rc.Height);
					}
					StringFormat format = new StringFormat(StringFormatFlags.NoWrap);
					format.LineAlignment = StringAlignment.Near;
					switch (Columns[logicalCol].TextAlign)
					{
						case HorizontalAlignment.Center:
                            format.Alignment = StringAlignment.Center;
							break;
						case HorizontalAlignment.Left:
							format.Alignment = StringAlignment.Near;
							break;
						default:
							format.Alignment = StringAlignment.Far;
							break;
					}
					format.Trimming = StringTrimming.EllipsisCharacter;
					g.DrawString(subItem.Text, subItemFont, new SolidBrush(subItemForeColor), stringRectangle, format);
				}

				g.Dispose();
				Marshal.StructureToPtr(lvcd, m.LParam, true);
				m.Result = (IntPtr) CustomDrawReturnFlags.CDRF_SKIPDEFAULT;
			}
			else
			{
				base.WndProc(ref m);
			}
		}

		private int OrderToIndex(int order)
		{
			int result = WindowsImports.SendMessage(hHeader, (int) HeaderControlMessages.HDM_ORDERTOINDEX, order, 0);
			return result;
		}


		private class ListViewExItemsComparer : IComparer
		{
			private SortOrder sorting = SortOrder.Ascending;

			public SortOrder Sorting
			{
				get { return sorting; }
				set { sorting = value; }
			}

			private int sortedColumn = 0;

			public int SortedColumn
			{
				get { return sortedColumn; }
				set { sortedColumn = value; }
			}

			public int Compare(object a, object b)
			{
				if (a == null || b == null)
				{
					throw new ArgumentException("The parameters can not be null");
				}
				ListViewItem itemA = a as ListViewItem;
				ListViewItem itemB = b as ListViewItem;
				if (itemA == null || itemB == null)
				{
					throw new ArgumentException("The parameters must be ListViewItems");
				}


				//Se puso para arreglar por dos columnas
				string aS2 = "";
				string bS2 = "";

				string aS = "";
				string bS = "";
				if (itemA.SubItems.Count > sortedColumn)
				{
					aS = itemA.SubItems[sortedColumn].Text;
				}
				if (itemB.SubItems.Count > sortedColumn)
				{
					bS = itemB.SubItems[sortedColumn].Text;
				}

				int res = 0;
				switch (((ColumnHeaderEx) itemA.ListView.Columns[sortedColumn]).Type)
				{
					case ColumnType.DateTime:
						DateTime aDateTime = DateTime.Now;
						bool aBool = true;
						try
						{
							aDateTime = DateTime.Parse(aS);
						}
						catch (Exception)
						{
							aBool = false;
						}
						DateTime bDateTime = DateTime.Now;
						bool bBool = true;
						try
						{
							bDateTime = DateTime.Parse(bS);
						}
						catch (Exception)
						{
							bBool = false;
						}
						if (aBool && bBool)
						{
							res = aDateTime.CompareTo(bDateTime);
						}
						else
						{
							res = aS.CompareTo(bS);
						}
						break;
					case ColumnType.Number:
						double aDouble = 0;
						bool aBool1 = double.TryParse(aS, NumberStyles.Float | NumberStyles.AllowThousands, NumberFormatInfo.CurrentInfo, out aDouble);
						double bDouble = 0;
						bool bBool1 = double.TryParse(bS, NumberStyles.Float | NumberStyles.AllowThousands, NumberFormatInfo.CurrentInfo, out bDouble);
						if (aBool1 && bBool1)
						{
							res = aDouble.CompareTo(bDouble);
						}
						else
						{
							res = aS.CompareTo(bS);
						}
						break;
                    case ColumnType.Check:
                        if (itemA.Checked == itemB.Checked)
                        {
                            res = 0;
                        }
                        else if (itemA.Checked == true)
                        {
                            res = 1;
                        }
                        else
                        {
                            res = -1;
                        }
                        break;
					default:
						res = aS.CompareTo(bS);
						break;
				}
				return (sorting == SortOrder.Ascending) ? (res) : (-res);
			}

			public ListViewExItemsComparer()
			{
			}

			public static readonly ListViewExItemsComparer Default = new ListViewExItemsComparer();
		}

		[Editor(typeof (ColumnHeaderExCollectionEditor), typeof (UITypeEditor)),
			TypeConverter(typeof (CollectionConverter))]
		public class ColumnHeaderExCollection : IList, ICollection, IEnumerable
		{
			private ColumnHeaderCollection columnHeaders = null;
			private ListViewEx owner = null;
            
			public ColumnHeaderExCollection(ListViewEx ownerI)
			{
				columnHeaders = new ColumnHeaderCollection(ownerI);
				owner = ownerI;
			}

			private bool isFixedSize = false;

			bool IList.IsFixedSize
			{
				get { return isFixedSize; }
			}

			public int Count
			{
				get { return columnHeaders.Count; }
			}

			public bool IsReadOnly
			{
				get { return columnHeaders.IsReadOnly; }
			}

			object IList.this[int i]
			{
				get { return columnHeaders[i]; }
				set
				{
					//					ColumnHeaderEx columnHeader = value as ColumnHeaderEx;
					//					if(columnHeader != null)
					//					{
					//						columnHeaders[i] = columnHeader;
					//					}
					//					else
					//						throw new ArgumentException("Invalid parameter type"); 
				}
			}

			public ColumnHeaderEx this[int index]
			{
				get { return (ColumnHeaderEx) columnHeaders[index]; }
			}

			int IList.Add(object value)
			{
				ColumnHeaderEx columnHeader = value as ColumnHeaderEx;
				if (columnHeader != null)
				{
					return Add(columnHeader);
				}
				else
				{
					throw new ArgumentException("Invalid parameter type");
				}
			}

			public int Add(ColumnHeaderEx columnHeader)
			{
				return columnHeaders.Add(columnHeader);
			}

			public ColumnHeaderEx Add(string text, int width, HorizontalAlignment alignment)
			{
				ColumnHeaderEx columnHeader = new ColumnHeaderEx();
				columnHeader.Text = text;
				columnHeader.Width = width;
				columnHeader.TextAlign = alignment;
				columnHeaders.Add(columnHeader);
				return columnHeader;
			}

			public void AddRange(ColumnHeaderEx[] values)
			{
				foreach (ColumnHeaderEx columnHeader in values)
				{
					Add(columnHeader);
				}
			}

			public void Clear()
			{
				columnHeaders.Clear();

				owner.sortedColumn = 0;

				ListViewExItemsComparer comparer = owner.ListViewItemSorter as ListViewExItemsComparer;
				if (comparer != null)
				{
					comparer.SortedColumn = 0;
				}
			}

			bool IList.Contains(object value)
			{
				ColumnHeaderEx columnHeader = value as ColumnHeaderEx;
				if (columnHeader != null)
				{
					return Contains(columnHeader);
				}
				else
				{
					throw new ArgumentException("Invalid parameter type");
				}
			}

			public bool Contains(ColumnHeaderEx columnHeader)
			{
				return columnHeaders.Contains(columnHeader);
			}

			public IEnumerator GetEnumerator()
			{
				return columnHeaders.GetEnumerator();
			}

			int IList.IndexOf(object value)
			{
				ColumnHeaderEx columnHeader = value as ColumnHeaderEx;
				if (columnHeader != null)
				{
					return IndexOf(columnHeader);
				}
				else
				{
					throw new ArgumentException("Invalid parameter type");
				}
			}

			public int IndexOf(ColumnHeaderEx columnHeader)
			{
				return columnHeaders.IndexOf(columnHeader);
			}

			void IList.Insert(int index, object value)
			{
				ColumnHeaderEx columnHeader = value as ColumnHeaderEx;
				if (columnHeader != null)
				{
					Insert(index, columnHeader);
				}
				else
				{
					throw new ArgumentException("Invalid parameter type");
				}
			}

			public void Insert(int index, ColumnHeaderEx columnHeader)
			{
				columnHeaders.Insert(index, columnHeader);
			}

			public void Insert(int index, string text, int width, HorizontalAlignment alignment)
			{
				ColumnHeaderEx columnHeader = new ColumnHeaderEx();
				columnHeader.Text = text;
				columnHeader.Width = width;
				columnHeader.TextAlign = alignment;
				columnHeaders.Insert(index, columnHeader);
			}

			void IList.Remove(object value)
			{
				ColumnHeaderEx columnHeader = value as ColumnHeaderEx;
				if (columnHeader != null)
				{
					Remove(columnHeader);
				}
				else
				{
					throw new ArgumentException("Invalid parameter type");
				}
			}

			public void Remove(ColumnHeaderEx columnHeader)
			{
				columnHeaders.Remove(columnHeader);
			}

			public void RemoveAt(int index)
			{
				columnHeaders.RemoveAt(index);
			}

			bool ICollection.IsSynchronized
			{
				get { return false; }
			}

			object ICollection.SyncRoot
			{
				get { return null; }
			}

			void ICollection.CopyTo(Array array, int index)
			{
			}
		}
		
		private bool loadFromResources = true;
		#region IResourceLoadable Members

		[Description("This property is used to load text and images resources from Smartmatic.Usp.Resources when it is true."
			 +" When false, resources are the same at design time"),
		Category("Behavior"), Browsable(true), DefaultValue(true)]
		public bool LoadFromResources
		{
			get
			{
				// TODO:  Add AlphabeticalTextBox.LoadFromResources getter implementation
				return loadFromResources;
			}
			set
			{
				// TODO:  Add AlphabeticalTextBox.LoadFromResources setter implementation
				loadFromResources = value;
			}
		}

		#endregion
	}

	public enum ColumnType
	{
		Text,
		DateTime,
		Number,
        Check
	}

	public class ColumnHeaderEx : ColumnHeader, IResourceLoadable
	{
		private ColumnType type = ColumnType.Text;

		public ColumnType Type
		{
			get { return type; }
			set { type = value; }
		}

		public ColumnHeaderEx()
		{
		}
  
		private bool loadFromResources = true;
		#region IResourceLoadable Members

		[Description("This property is used to load text and images resources from Smartmatic.Usp.Resources when it is true."
			 +" When false, resources are the same at design time"),
		Category("Behavior"), Browsable(true), DefaultValue(true)]
		public bool LoadFromResources
		{
			get
			{
				// TODO:  Add AlphabeticalTextBox.LoadFromResources getter implementation
				return loadFromResources;
			}
			set
			{
				// TODO:  Add AlphabeticalTextBox.LoadFromResources setter implementation
				loadFromResources = value;
			}
		}

		#endregion
	}
}