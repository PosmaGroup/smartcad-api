using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using FontClass = System.Drawing.Font;
using FontFormatClass = SmartCadControls.Controls.FontFormat;

namespace SmartCadControls.Controls
{
	/// <summary>
	/// Summary description for DateTimePickerEx.
	/// </summary>
	public class DateTimePickerEx : DateTimePicker, IResourceLoadable
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private Container components = null;

		public DateTimePickerEx()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitForm call

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.SuspendLayout();
            // 
            // DateTimePickerEx
            // 
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DateTimePickerEx_KeyDown);
            this.ResumeLayout(false);

		}

		#endregion

		[RefreshProperties(RefreshProperties.All),
			DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
			Browsable(false)]
		public override FontClass Font
		{
			get { return base.Font; }
			set { base.Font = value; }
		}

		[RefreshProperties(RefreshProperties.All),
			DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
			Browsable(false)]
		public override Color ForeColor
		{
			get { return base.ForeColor; }
			set { base.ForeColor = value; }
		}

		private FontFormatClass fontFormat = FontFormatClass.EditBoxFormat;

		[EditorBrowsable(EditorBrowsableState.Never),
			Browsable(true),
			Description("Specified The font and foreground color used to display text and graphics in the control"),
			Category("Appearance")]
		public FontFormatClass FontFormat
		{
			get { return fontFormat; }
			set
			{
				fontFormat = value;
				ForeColor = fontFormat.Color;
				Font = fontFormat.Font;
			}
		}
		
		private bool loadFromResources = true;
		#region IResourceLoadable Members

		[Description("This property is used to load text and images resources from Smartmatic.Usp.Resources when it is true."
			 +" When false, resources are the same at design time"),
		Category("Behavior"), Browsable(true), DefaultValue(true)]
		public bool LoadFromResources
		{
			get
			{
				// TODO:  Add AlphabeticalTextBox.LoadFromResources getter implementation
				return loadFromResources;
			}
			set
			{
				// TODO:  Add AlphabeticalTextBox.LoadFromResources setter implementation
				loadFromResources = value;
			}
		}

		#endregion

        private void DateTimePickerEx_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyValue == 188) ||
                (e.KeyValue == 189) ||
                (e.KeyValue == 190) ||
                (e.KeyValue == 110)) 
            {
                e.SuppressKeyPress=true ;
            }
        }
	}
}