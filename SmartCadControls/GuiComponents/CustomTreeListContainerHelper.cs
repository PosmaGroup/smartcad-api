﻿using DevExpress.XtraTreeList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadControls.GuiComponents
{
    public class CustomTreeListContainerHelper : TreeListContainerHelper
    {
        public CustomTreeListContainerHelper(TreeList owner) : base(owner) { }
        public override void ActivateEditor(DevExpress.XtraEditors.Repository.RepositoryItem ritem, DevExpress.XtraEditors.Container.UpdateEditorInfoArgs args)
        {
            args = new DevExpress.XtraEditors.Container.UpdateEditorInfoArgs(args.MakeReadOnly, args.Bounds, args.Appearance, Owner.FocusedNode.Tag, args.LookAndFeel, args.ErrorText);
            base.ActivateEditor(ritem, args);
        }
    }
}
