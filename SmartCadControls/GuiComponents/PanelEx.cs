using System;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;
using System.Drawing.Drawing2D;

namespace SmartCadControls.Controls
{
	/// <summary>
	/// Summary description for PanelEx.
	/// </summary>
	public class PanelEx : Panel, IResourceLoadable
	{
		public PanelEx()
		{
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.DoubleBuffer, true);
            this.SetStyle(ControlStyles.ResizeRedraw, true);
            this.SetStyle(ControlStyles.Selectable, true);
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            this.SetStyle(ControlStyles.UserPaint, true);

            this.BackColor = Color.Empty;
		}
        /*
		protected override void WndProc(ref Message message)
		{
			if (!DesignMode)
			{
				switch (message.Msg)
				{
					case (int) Msg.WM_ERASEBKGND:
						PaintBackground(ref message);
						break;
					default:
						base.WndProc(ref message);
						break;
				}
			}
			else
			{
				base.WndProc(ref message);
			}
		}

		private void PaintBackground(ref Message message)
		{
			IntPtr hDC = (IntPtr) message.WParam;
			Graphics g = Graphics.FromHdc(hDC);

			Rectangle rect = new Rectangle(0, 0, 171, 272);
			g.FillRectangle(new SolidBrush(Color.White), rect);

			if (this.BackgroundImage != null)
			{
				g.DrawImage(this.BackgroundImage, rect);
			}
		}*/
		
		#region IResourceLoadable Members

        private bool loadFromResources = true;

		public bool LoadFromResources
		{
			get
			{
				return loadFromResources;
			}
			set
			{
				loadFromResources = value;
			}
		}

		#endregion

        private Color borderColor;

        [DefaultValue(typeof(Color), "WindowFrame")]
        [Category("Appearance")]
        [Description("The border color used to paint the control.")]
        public Color BorderColor
        {
            get
            {
                return borderColor;
            }
            set
            {
                borderColor = value;

                if (this.DesignMode == true)
                    this.Invalidate();
            }
        }

        private Color panelColor;

        [DefaultValue(typeof(Color), "Control")]
        [Category("Appearance")]
        [Description("The color used to paint the control.")]
        public Color PanelColor
        {
            get
            {
                return panelColor;
            }
            set
            {
                panelColor = value;

                if (this.DesignMode == true)
                    this.Invalidate();
            }
        }

        private int borderWidth;

        [DefaultValue(typeof(int), "1")]
        [Category("Appearance")]
        [Description("The width of the border used to paint the control.")]
        public int BorderWidth
        {
            get
            {
                return borderWidth;
            }
            set
            {
                borderWidth = value;

                if (this.DesignMode == true)
                    this.Invalidate();
            }
        }

        private int curvature;

        [DefaultValue(typeof(int), "0")]
        [Category("Appearance")]
        [Description("The radius of the curve used to paint the corners of the control.")]
        public int Curvature
        {
            get
            {
                return curvature;
            }
            set
            {
                curvature = value;
                if (this.DesignMode == true)
                    this.Invalidate();
            }
        }

        private int DoubleToInt(double val)
        {
            return Decimal.ToInt32(Decimal.Floor(Decimal.Parse((val).ToString())));
        }

        protected GraphicsPath GetPath()
        {
            GraphicsPath graphPath = new GraphicsPath();

            try
            {
                int curve;

                Rectangle rect = this.ClientRectangle;
                int offset = 0;

                if (borderWidth > 1)
                    offset = DoubleToInt(BorderWidth / 2);

                curve = curvature;

                if (curve == 0)
                    graphPath.AddRectangle(Rectangle.Inflate(rect, -offset, -offset));
                else
                {
                    int rectWidth = rect.Width - 1 - offset;
                    int rectHeight = rect.Height - 1 - offset;
                    int curveWidth = (curve * 2);

                    graphPath.AddArc(rectWidth - curveWidth, offset, curveWidth, curveWidth, 270, 90);
                    graphPath.AddArc(rectWidth - curveWidth, rectHeight - curveWidth, curveWidth, curveWidth, 0, 90);
                    graphPath.AddArc(offset, rectHeight - curveWidth, curveWidth, curveWidth, 90, 90);
                    graphPath.AddArc(offset, offset, curveWidth, curveWidth, 180, 90);

                    graphPath.CloseFigure();
                }
            }
            catch
            {
                graphPath.AddRectangle(this.ClientRectangle);
            }

            return graphPath;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
            e.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;

            if (curvature > 0)
            {
                using (GraphicsPath graphPath = GetPath())
                {
                    SolidBrush brush = new SolidBrush(panelColor);
                    Pen pen = new Pen(borderColor, borderWidth);

                    e.Graphics.FillPath(brush, graphPath);
                    e.Graphics.DrawPath(pen, graphPath);

                    pen.Dispose();
                    brush.Dispose();
                }
            }
        }
	}
}