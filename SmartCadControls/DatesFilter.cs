using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraNavBar;
using SmartCadCore.Common;

namespace SmartCadControls.Controls
{
    public partial class DatesFilter : SearchFilter
    {
        public DatesFilter()
        {
            InitializeComponent();
            dateEditStart.DateTime = DateTime.Now.AddDays(-7);
            dateEditEnd.DateTime = DateTime.Now;

            Title = ResourceLoader.GetString2("DatesInterval");
            Image = ResourceLoader.GetImage("IconCalendar");
            layoutControlItemStartDate.Text = ResourceLoader.GetString2("StartDate");
            layoutControlItemEndDate.Text = ResourceLoader.GetString2("EndDate");
        }

        #region SearchFilter Members


        public override Dictionary<string, List<object>> SelectedOptions
        {
            get
            { 
                Dictionary<string, List<object>> results = new Dictionary<string, List<object>>();  
                results.Add("StartDate",new List<object>(){ dateEditStart.DateTime });
                results.Add("EndDate", new List<object>() { dateEditEnd.DateTime });

                return results;
            }
        }

        #endregion

        private void dateEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (dateEditStart.DateTime.CompareTo(dateEditEnd.DateTime) == 1)
                dateEditEnd.DateTime = dateEditStart.DateTime;
        }

    }
}
