
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadCore.Enums;
using SmartCadGuiCommon.Controls;
namespace SmartCadGuiCommon
{
    partial class DefaultMapFormDevX
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DefaultMapFormDevX));
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.mapLayoutDevX1 = new MapLayoutDevX();
            this.ribbonPageGroupMapControls = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.barButtonZoomIn = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonZoomOut = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonCenter = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonSelect = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonPan = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonDistance = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonAddPosition = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonClearMap = new DevExpress.XtraBars.BarButtonItem();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonAddRoute = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageMapControls = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroupLocation = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemImageComboBox1.Appearance.Options.UseImage = true;
            this.repositoryItemImageComboBox1.AppearanceFocused.Options.UseTextOptions = true;
            this.repositoryItemImageComboBox1.AppearanceFocused.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.repositoryItemImageComboBox1.AppearanceFocused.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            this.repositoryItemImageComboBox1.SmallImages = this.imageCollection1;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "Seleccion.png");
            this.imageList.Images.SetKeyName(1, "Seleccion-circular.png");
            this.imageList.Images.SetKeyName(2, "Seleccion-rectangular.png");
            // 
            // mapLayoutDevX1
            // 
            this.mapLayoutDevX1.AddressData = ((AddressClientData)(resources.GetObject("mapLayoutDevX1.AddressData")));
            this.mapLayoutDevX1.CurrentShapeType = ShapeType.None;
            this.mapLayoutDevX1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mapLayoutDevX1.Location = new System.Drawing.Point(2, 2);
            this.mapLayoutDevX1.MapService = null;
            this.mapLayoutDevX1.Name = "mapLayoutDevX1";
            this.mapLayoutDevX1.NetworkCredential = null;
            this.mapLayoutDevX1.ServerServiceClient = null;
            this.mapLayoutDevX1.SetVisibleImageSearch = ImageSearchState.NotPainted;
            this.mapLayoutDevX1.Size = new System.Drawing.Size(1264, 786);
            this.mapLayoutDevX1.TabIndex = 0;
            this.mapLayoutDevX1.TrustedConnection = false;
            this.mapLayoutDevX1.Load += new System.EventHandler(this.mapLayoutDevX1_Load);
            // 
            // ribbonPageGroupMapControls
            // 
            this.ribbonPageGroupMapControls.AllowMinimize = false;
            this.ribbonPageGroupMapControls.AllowTextClipping = false;
            this.ribbonPageGroupMapControls.ItemLinks.Add(this.barButtonZoomIn);
            this.ribbonPageGroupMapControls.ItemLinks.Add(this.barButtonZoomOut);
            this.ribbonPageGroupMapControls.ItemLinks.Add(this.barButtonCenter);
            this.ribbonPageGroupMapControls.ItemLinks.Add(this.barButtonSelect);
            this.ribbonPageGroupMapControls.ItemLinks.Add(this.barButtonPan);
            this.ribbonPageGroupMapControls.ItemLinks.Add(this.barButtonDistance);
            this.ribbonPageGroupMapControls.Name = "ribbonPageGroupMapControls";
            this.ribbonPageGroupMapControls.ShowCaptionButton = false;
            this.ribbonPageGroupMapControls.Text = "ribbonPageGroupMapControls";
            // 
            // barButtonZoomIn
            // 
            this.barButtonZoomIn.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left;
            this.barButtonZoomIn.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.barButtonZoomIn.Caption = "barButtonZoomIn";
            this.barButtonZoomIn.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonZoomIn.Glyph")));
            this.barButtonZoomIn.GroupIndex = 1;
            this.barButtonZoomIn.Id = 4;
            this.barButtonZoomIn.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonZoomIn.Name = "barButtonZoomIn";
            this.barButtonZoomIn.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonZoomIn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonZoomIn_ItemClick);
            // 
            // barButtonZoomOut
            // 
            this.barButtonZoomOut.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left;
            this.barButtonZoomOut.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.barButtonZoomOut.Caption = "barButtonZoomOut";
            this.barButtonZoomOut.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonZoomOut.Glyph")));
            this.barButtonZoomOut.GroupIndex = 1;
            this.barButtonZoomOut.Id = 6;
            this.barButtonZoomOut.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonZoomOut.Name = "barButtonZoomOut";
            this.barButtonZoomOut.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonZoomOut.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonZoomIn_ItemClick);
            // 
            // barButtonCenter
            // 
            this.barButtonCenter.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left;
            this.barButtonCenter.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.barButtonCenter.Caption = "barButtonCenter";
            this.barButtonCenter.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonCenter.Glyph")));
            this.barButtonCenter.GroupIndex = 1;
            this.barButtonCenter.Id = 8;
            this.barButtonCenter.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonCenter.Name = "barButtonCenter";
            this.barButtonCenter.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonCenter.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonZoomIn_ItemClick);
            // 
            // barButtonSelect
            // 
            this.barButtonSelect.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left;
            this.barButtonSelect.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.barButtonSelect.Caption = "barButtonSelect";
            this.barButtonSelect.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonSelect.Glyph")));
            this.barButtonSelect.GroupIndex = 1;
            this.barButtonSelect.Id = 10;
            this.barButtonSelect.ImageIndex = 1;
            this.barButtonSelect.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonSelect.Name = "barButtonSelect";
            this.barButtonSelect.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonSelect.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonZoomIn_ItemClick);
            // 
            // barButtonPan
            // 
            this.barButtonPan.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left;
            this.barButtonPan.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.barButtonPan.Caption = "barButtonPan";
            this.barButtonPan.Down = true;
            this.barButtonPan.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonPan.Glyph")));
            this.barButtonPan.GroupIndex = 1;
            this.barButtonPan.Id = 7;
            this.barButtonPan.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonPan.Name = "barButtonPan";
            this.barButtonPan.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonPan.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonZoomIn_ItemClick);
            // 
            // barButtonDistance
            // 
            this.barButtonDistance.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left;
            this.barButtonDistance.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.barButtonDistance.Caption = "barButtonDistance";
            this.barButtonDistance.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonDistance.Glyph")));
            this.barButtonDistance.GroupIndex = 1;
            this.barButtonDistance.Id = 21;
            this.barButtonDistance.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonDistance.Name = "barButtonDistance";
            this.barButtonDistance.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonDistance.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonDistance_ItemClick);
            // 
            // barButtonAddPosition
            // 
            this.barButtonAddPosition.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left;
            this.barButtonAddPosition.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.barButtonAddPosition.Caption = "barButtonAddPosition";
            this.barButtonAddPosition.Enabled = false;
            this.barButtonAddPosition.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonAddPosition.Glyph")));
            this.barButtonAddPosition.GroupIndex = 1;
            this.barButtonAddPosition.Id = 12;
            this.barButtonAddPosition.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonAddPosition.Name = "barButtonAddPosition";
            this.barButtonAddPosition.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonAddPosition.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonZoomIn_ItemClick);
            // 
            // barButtonClearMap
            // 
            this.barButtonClearMap.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left;
            this.barButtonClearMap.Caption = "barButtonClearMap";
            this.barButtonClearMap.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonClearMap.Glyph")));
            this.barButtonClearMap.GroupIndex = 1;
            this.barButtonClearMap.Id = 15;
            this.barButtonClearMap.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonClearMap.Name = "barButtonClearMap";
            this.barButtonClearMap.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonClearMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonClearMap_ItemClick);
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ApplicationIcon = null;
            this.ribbonControl1.BackColor = System.Drawing.Color.White;
            this.ribbonControl1.Dock = System.Windows.Forms.DockStyle.None;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonZoomIn,
            this.barButtonZoomOut,
            this.barButtonPan,
            this.barButtonCenter,
            this.barButtonSelect,
            this.barButtonAddPosition,
            this.barButtonClearMap,
            this.barButtonDistance,
            this.barButtonAddRoute});
            this.ribbonControl1.Location = new System.Drawing.Point(128, 93);
            this.ribbonControl1.MaxItemId = 28;
            this.ribbonControl1.MdiMergeStyle = DevExpress.XtraBars.Ribbon.RibbonMdiMergeStyle.Always;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPageMapControls});
            this.ribbonControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox1,
            this.repositoryItemImageComboBox1});
            this.ribbonControl1.SelectedPage = this.ribbonPageMapControls;
            this.ribbonControl1.ShowToolbarCustomizeItem = false;
            this.ribbonControl1.Size = new System.Drawing.Size(712, 115);
            this.ribbonControl1.Toolbar.ShowCustomizeItem = false;
            this.ribbonControl1.ShowExpandCollapseButton = DevExpress.Utils.DefaultBoolean.True;
            this.ribbonControl1.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // barButtonAddRoute
            // 
            this.barButtonAddRoute.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left;
            this.barButtonAddRoute.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.barButtonAddRoute.Caption = "barButtonAddRoute";
            this.barButtonAddRoute.Enabled = false;
            this.barButtonAddRoute.Glyph = global::SmartCadGuiCommon.XRDesignRibbonControllerResources.RibbonUserDesigner_HorizSpaceIncrease;
            this.barButtonAddRoute.GroupIndex = 1;
            this.barButtonAddRoute.Id = 27;
            this.barButtonAddRoute.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonAddRoute.Name = "barButtonAddRoute";
            this.barButtonAddRoute.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonAddRoute.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonAddRoute_ItemClick);
            // 
            // ribbonPageMapControls
            // 
            this.ribbonPageMapControls.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupMapControls,
            this.ribbonPageGroupLocation});
            this.ribbonPageMapControls.Name = "ribbonPageMapControls";
            this.ribbonPageMapControls.Text = "ribbonPageMapControls";
            // 
            // ribbonPageGroupLocation
            // 
            this.ribbonPageGroupLocation.AllowMinimize = false;
            this.ribbonPageGroupLocation.AllowTextClipping = false;
            this.ribbonPageGroupLocation.ItemLinks.Add(this.barButtonAddPosition);
            this.ribbonPageGroupLocation.ItemLinks.Add(this.barButtonClearMap);
            this.ribbonPageGroupLocation.ItemLinks.Add(this.barButtonAddRoute);
            this.ribbonPageGroupLocation.Name = "ribbonPageGroupLocation";
            this.ribbonPageGroupLocation.ShowCaptionButton = false;
            this.ribbonPageGroupLocation.Text = "ribbonPageGroupLocation";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.mapLayoutDevX1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1268, 790);
            this.panelControl1.TabIndex = 1;
            // 
            // DefaultMapFormDevX
            // 
            this.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(215)))), ((int)(((byte)(233)))));
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1268, 790);
            this.Controls.Add(this.ribbonControl1);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DefaultMapFormDevX";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "DefaultMapFormDevX";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DefaultMapFormDevX_FormClosing_1);
            this.Load += new System.EventHandler(this.DefaultMapFormDevX_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DefaultMapFormDevX_KeyPress);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public MapLayoutDevX mapLayoutDevX1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupMapControls;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraBars.BarButtonItem barButtonDistance;
        private DevExpress.XtraBars.BarButtonItem barButtonZoomOut;
        private DevExpress.XtraBars.BarButtonItem barButtonPan;
        private DevExpress.XtraBars.BarButtonItem barButtonCenter;
        private DevExpress.XtraBars.BarButtonItem barButtonSelect;
        private DevExpress.XtraBars.BarButtonItem barButtonAddPosition;
        private DevExpress.XtraBars.BarButtonItem barButtonClearMap;
        private DevExpress.XtraBars.BarButtonItem barButtonZoomIn;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPageMapControls;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private System.Windows.Forms.ImageList imageList;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.Utils.ImageCollection imageCollection1;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupLocation;
        private DevExpress.XtraBars.BarButtonItem barButtonAddRoute;
    }
}