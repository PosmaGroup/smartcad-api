using SmartCadControls.Controls;
using SmartCadCore.Core;
using SmartCadCore.Enums;
using SmartCadGuiCommon.Controls;
using Smartmatic.SmartCad.Map;
namespace SmartCadGuiCommon
{
	partial class UnitFollowRibbonForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UnitFollowRibbonForm));
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItemCascade = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemArrange = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemTileHorizontal = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemTileVertical = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.xtraTabbedMdiManager1 = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            this.mapControlEx1 = new MapControlEx();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanel1 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.mapLayersControl1 = new MapLayersControl();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanel1.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ApplicationIcon = null;
            this.ribbonControl1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(241)))));
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItemCascade,
            this.barButtonItemArrange,
            this.barButtonItemTileHorizontal,
            this.barButtonItemTileVertical});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 4;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbonControl1.SelectedPage = this.ribbonPage1;
            this.ribbonControl1.ShowToolbarCustomizeItem = false;
            this.ribbonControl1.Size = new System.Drawing.Size(1270, 115);
            this.ribbonControl1.Toolbar.ShowCustomizeItem = false;
            this.ribbonControl1.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // barButtonItemCascade
            // 
            this.barButtonItemCascade.Caption = "Cascade";
            this.barButtonItemCascade.Id = 0;
            this.barButtonItemCascade.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemCascade.LargeGlyph")));
            this.barButtonItemCascade.Name = "barButtonItemCascade";
            this.barButtonItemCascade.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemCascade.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemCascade_ItemClick);
            // 
            // barButtonItemArrange
            // 
            this.barButtonItemArrange.Caption = "Arrange";
            this.barButtonItemArrange.Id = 1;
            this.barButtonItemArrange.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemArrange.LargeGlyph")));
            this.barButtonItemArrange.Name = "barButtonItemArrange";
            this.barButtonItemArrange.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemArrange.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemArrange_ItemClick);
            // 
            // barButtonItemTileHorizontal
            // 
            this.barButtonItemTileHorizontal.Caption = "Tile horizontal";
            this.barButtonItemTileHorizontal.Id = 2;
            this.barButtonItemTileHorizontal.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemTileHorizontal.LargeGlyph")));
            this.barButtonItemTileHorizontal.Name = "barButtonItemTileHorizontal";
            this.barButtonItemTileHorizontal.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemTileHorizontal.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemTileHorizontal_ItemClick);
            // 
            // barButtonItemTileVertical
            // 
            this.barButtonItemTileVertical.Caption = "Tile vertical";
            this.barButtonItemTileVertical.Id = 3;
            this.barButtonItemTileVertical.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemTileVertical.LargeGlyph")));
            this.barButtonItemTileVertical.Name = "barButtonItemTileVertical";
            this.barButtonItemTileVertical.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemTileVertical.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemTileVertical_ItemClick);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "ribbonPage1";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.AllowMinimize = false;
            this.ribbonPageGroup1.AllowTextClipping = false;
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItemCascade);
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItemArrange);
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItemTileHorizontal);
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItemTileVertical);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.ShowCaptionButton = false;
            this.ribbonPageGroup1.Text = "ribbonPageGroup1";
            // 
            // xtraTabbedMdiManager1
            // 
            this.xtraTabbedMdiManager1.MdiParent = this;
            // 
            // mapControlEx1
            // 
            this.mapControlEx1.Distance = 0;
            this.mapControlEx1.DistanceUnit = "mts";
            this.mapControlEx1.Location = new System.Drawing.Point(262, 138);
            this.mapControlEx1.Name = "mapControlEx1";
            this.mapControlEx1.PostFixTables = null;
            this.mapControlEx1.ShowDistance = false;
            this.mapControlEx1.Size = new System.Drawing.Size(642, 633);
            this.mapControlEx1.TabIndex = 2;
            this.mapControlEx1.ToolInUse = null;
            this.mapControlEx1.Visible = false;
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel1});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // dockPanel1
            // 
            this.dockPanel1.Controls.Add(this.dockPanel1_Container);
            this.dockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanel1.ID = new System.Guid("8b3fbb1f-cc78-4f4c-9707-a6d8cd4a166a");
            this.dockPanel1.Location = new System.Drawing.Point(0, 115);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.Options.AllowDockBottom = false;
            this.dockPanel1.Options.AllowDockFill = false;
            this.dockPanel1.Options.AllowDockTop = false;
            this.dockPanel1.Options.AllowFloating = false;
            this.dockPanel1.Options.FloatOnDblClick = false;
            this.dockPanel1.Options.ShowCloseButton = false;
            this.dockPanel1.Options.ShowMaximizeButton = false;
            this.dockPanel1.Size = new System.Drawing.Size(248, 848);
            this.dockPanel1.Text = "dockPanel1";
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.mapLayersControl1);
            this.dockPanel1_Container.Location = new System.Drawing.Point(3, 25);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(242, 820);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // mapLayersControl1
            // 
            this.mapLayersControl1.CurrentShapeType = ShapeType.None;
            this.mapLayersControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mapLayersControl1.GlobalApplicationPreference = null;
            this.mapLayersControl1.LaspointInRoute = null;
            this.mapLayersControl1.ListDepartmentByUser = null;
            this.mapLayersControl1.Location = new System.Drawing.Point(0, 0);
            this.mapLayersControl1.MapControlEx = null;
            this.mapLayersControl1.Name = "mapLayersControl1";
            this.mapLayersControl1.PermissionsUser = null;
            this.mapLayersControl1.Size = new System.Drawing.Size(242, 820);
            this.mapLayersControl1.TabIndex = 0;
            this.mapLayersControl1.TreeViewIncidents = null;
            this.mapLayersControl1.TreeViewRoutes = null;
            this.mapLayersControl1.TreeViewStations = null;
            this.mapLayersControl1.TreeViewStructs = null;
            this.mapLayersControl1.TreeViewUnits = null;
            this.mapLayersControl1.TreeViewZones = null;
            this.mapLayersControl1.UnitsList = null;
            // 
            // UnitFollowRibbonForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1270, 963);
            this.Controls.Add(this.mapControlEx1);
            this.Controls.Add(this.dockPanel1);
            this.Controls.Add(this.ribbonControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "UnitFollowRibbonForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "UnitFollowRibbonForm";
            this.Load += new System.EventHandler(this.UnitFollowRibbonForm_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.UnitFollowRibbonForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanel1.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            this.ResumeLayout(false);

		}

		#endregion

		private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
		private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
		private DevExpress.XtraBars.BarButtonItem barButtonItemCascade;
		private DevExpress.XtraBars.BarButtonItem barButtonItemArrange;
		private DevExpress.XtraBars.BarButtonItem barButtonItemTileHorizontal;
		private DevExpress.XtraBars.BarButtonItem barButtonItemTileVertical;
		private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager xtraTabbedMdiManager1;
		private MapControlEx mapControlEx1;
		private DevExpress.XtraBars.Docking.DockPanel dockPanel1;
		private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
		private DevExpress.XtraBars.Docking.DockManager dockManager1;
        public MapLayersControl mapLayersControl1;
	}
}