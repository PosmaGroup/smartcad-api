using SmartCadControls;
namespace SmartCadGuiCommon
{
	partial class CommandsForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.labelControlUnitText = new DevExpress.XtraEditors.LabelControl();
            this.labelControlUnit = new DevExpress.XtraEditors.LabelControl();
            this.simpleButtonClose = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonActivate = new DevExpress.XtraEditors.SimpleButton();
            this.gridControlExCommands = new GridControlEx();
            this.gridViewExCommands = new GridViewEx();
            this.labelControlDepartmentType = new DevExpress.XtraEditors.LabelControl();
            this.labelControlDepartmentNameText = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemAccept = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupUnitInfo = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItemUnit = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItemDepatmentType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemDepartmentTypeText = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemUnitText = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupFunctions = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemGridcontrol = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItemActivate = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExCommands)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExCommands)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAccept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupUnitInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepatmentType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartmentTypeText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemUnitText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupFunctions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGridcontrol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemActivate)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.labelControlUnitText);
            this.layoutControl1.Controls.Add(this.labelControlUnit);
            this.layoutControl1.Controls.Add(this.simpleButtonClose);
            this.layoutControl1.Controls.Add(this.simpleButtonActivate);
            this.layoutControl1.Controls.Add(this.gridControlExCommands);
            this.layoutControl1.Controls.Add(this.labelControlDepartmentType);
            this.layoutControl1.Controls.Add(this.labelControlDepartmentNameText);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(525, 372);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // labelControlUnitText
            // 
            this.labelControlUnitText.Location = new System.Drawing.Point(279, 27);
            this.labelControlUnitText.Name = "labelControlUnitText";
            this.labelControlUnitText.Size = new System.Drawing.Size(98, 13);
            this.labelControlUnitText.StyleController = this.layoutControl1;
            this.labelControlUnitText.TabIndex = 11;
            this.labelControlUnitText.Text = "labelControlUnitText";
            // 
            // labelControlUnit
            // 
            this.labelControlUnit.Location = new System.Drawing.Point(381, 27);
            this.labelControlUnit.Name = "labelControlUnit";
            this.labelControlUnit.Size = new System.Drawing.Size(76, 13);
            this.labelControlUnit.StyleController = this.layoutControl1;
            this.labelControlUnit.TabIndex = 10;
            this.labelControlUnit.Text = "labelControlUnit";
            // 
            // simpleButtonClose
            // 
            this.simpleButtonClose.Location = new System.Drawing.Point(441, 346);
            this.simpleButtonClose.Name = "simpleButtonClose";
            this.simpleButtonClose.Size = new System.Drawing.Size(81, 23);
            this.simpleButtonClose.StyleController = this.layoutControl1;
            this.simpleButtonClose.TabIndex = 8;
            this.simpleButtonClose.Text = "simpleButtonClose";
            this.simpleButtonClose.Click += new System.EventHandler(this.simpleButtonClose_Click);
            // 
            // simpleButtonActivate
            // 
            this.simpleButtonActivate.Enabled = false;
            this.simpleButtonActivate.Location = new System.Drawing.Point(436, 312);
            this.simpleButtonActivate.Name = "simpleButtonActivate";
            this.simpleButtonActivate.Size = new System.Drawing.Size(81, 23);
            this.simpleButtonActivate.StyleController = this.layoutControl1;
            this.simpleButtonActivate.TabIndex = 7;
            this.simpleButtonActivate.Text = "simpleButtonActivate";
            this.simpleButtonActivate.Click += new System.EventHandler(this.simpleButtonActivate_Click);
            // 
            // gridControlExCommands
            // 
            this.gridControlExCommands.EnableAutoFilter = false;
            this.gridControlExCommands.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExCommands.Location = new System.Drawing.Point(8, 75);
            this.gridControlExCommands.MainView = this.gridViewExCommands;
            this.gridControlExCommands.Name = "gridControlExCommands";
            this.gridControlExCommands.Size = new System.Drawing.Size(509, 231);
            this.gridControlExCommands.TabIndex = 6;
            this.gridControlExCommands.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExCommands});
            this.gridControlExCommands.ViewTotalRows = false;
            // 
            // gridViewExCommands
            // 
            this.gridViewExCommands.AllowFocusedRowChanged = true;
            this.gridViewExCommands.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExCommands.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExCommands.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExCommands.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExCommands.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExCommands.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExCommands.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExCommands.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExCommands.EnablePreviewLineForFocusedRow = false;
            this.gridViewExCommands.GridControl = this.gridControlExCommands;
            this.gridViewExCommands.Name = "gridViewExCommands";
            this.gridViewExCommands.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExCommands.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExCommands.ViewTotalRows = false;
            this.gridViewExCommands.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewExCommands_FocusedRowChanged);
            // 
            // labelControlDepartmentType
            // 
            this.labelControlDepartmentType.Location = new System.Drawing.Point(117, 27);
            this.labelControlDepartmentType.Name = "labelControlDepartmentType";
            this.labelControlDepartmentType.Size = new System.Drawing.Size(81, 13);
            this.labelControlDepartmentType.StyleController = this.layoutControl1;
            this.labelControlDepartmentType.TabIndex = 5;
            this.labelControlDepartmentType.Text = "DepartmentType";
            // 
            // labelControlDepartmentNameText
            // 
            this.labelControlDepartmentNameText.Location = new System.Drawing.Point(7, 27);
            this.labelControlDepartmentNameText.Name = "labelControlDepartmentNameText";
            this.labelControlDepartmentNameText.Size = new System.Drawing.Size(106, 13);
            this.labelControlDepartmentNameText.StyleController = this.layoutControl1;
            this.labelControlDepartmentNameText.TabIndex = 4;
            this.labelControlDepartmentNameText.Text = "DepartmentNameText";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemAccept,
            this.emptySpaceItem1,
            this.layoutControlGroupUnitInfo,
            this.layoutControlGroupFunctions});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(525, 372);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItemAccept
            // 
            this.layoutControlItemAccept.Control = this.simpleButtonClose;
            this.layoutControlItemAccept.CustomizationFormText = "layoutControlItemAccept";
            this.layoutControlItemAccept.Location = new System.Drawing.Point(438, 343);
            this.layoutControlItemAccept.MaxSize = new System.Drawing.Size(87, 29);
            this.layoutControlItemAccept.MinSize = new System.Drawing.Size(87, 29);
            this.layoutControlItemAccept.Name = "layoutControlItemAccept";
            this.layoutControlItemAccept.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItemAccept.Size = new System.Drawing.Size(87, 29);
            this.layoutControlItemAccept.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemAccept.Text = "layoutControlItemAccept";
            this.layoutControlItemAccept.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemAccept.TextToControlDistance = 0;
            this.layoutControlItemAccept.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 343);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(438, 29);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupUnitInfo
            // 
            this.layoutControlGroupUnitInfo.CustomizationFormText = "layoutControlGroupUnitInfo";
            this.layoutControlGroupUnitInfo.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem4,
            this.layoutControlItemUnit,
            this.emptySpaceItem3,
            this.layoutControlItemDepatmentType,
            this.layoutControlItemDepartmentTypeText,
            this.layoutControlItemUnitText});
            this.layoutControlGroupUnitInfo.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupUnitInfo.Name = "layoutControlGroupUnitInfo";
            this.layoutControlGroupUnitInfo.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupUnitInfo.Size = new System.Drawing.Size(525, 47);
            this.layoutControlGroupUnitInfo.Text = "layoutControlGroupUnitInfo";
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(454, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(61, 17);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItemUnit
            // 
            this.layoutControlItemUnit.Control = this.labelControlUnit;
            this.layoutControlItemUnit.CustomizationFormText = "layoutControlItemUnit";
            this.layoutControlItemUnit.Location = new System.Drawing.Point(374, 0);
            this.layoutControlItemUnit.Name = "layoutControlItemUnit";
            this.layoutControlItemUnit.Size = new System.Drawing.Size(80, 17);
            this.layoutControlItemUnit.Text = "layoutControlItemUnit";
            this.layoutControlItemUnit.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemUnit.TextToControlDistance = 0;
            this.layoutControlItemUnit.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(195, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(77, 17);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItemDepatmentType
            // 
            this.layoutControlItemDepatmentType.Control = this.labelControlDepartmentType;
            this.layoutControlItemDepatmentType.CustomizationFormText = "labelControlDepartmentType";
            this.layoutControlItemDepatmentType.Location = new System.Drawing.Point(110, 0);
            this.layoutControlItemDepatmentType.Name = "layoutControlItemDepatmentType";
            this.layoutControlItemDepatmentType.Size = new System.Drawing.Size(85, 17);
            this.layoutControlItemDepatmentType.Text = "labelControlDepartmentType";
            this.layoutControlItemDepatmentType.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemDepatmentType.TextToControlDistance = 0;
            this.layoutControlItemDepatmentType.TextVisible = false;
            // 
            // layoutControlItemDepartmentTypeText
            // 
            this.layoutControlItemDepartmentTypeText.Control = this.labelControlDepartmentNameText;
            this.layoutControlItemDepartmentTypeText.CustomizationFormText = "layoutControlItemDepartmentTypeText";
            this.layoutControlItemDepartmentTypeText.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemDepartmentTypeText.Name = "layoutControlItemDepartmentTypeText";
            this.layoutControlItemDepartmentTypeText.Size = new System.Drawing.Size(110, 17);
            this.layoutControlItemDepartmentTypeText.Text = "layoutControlItemDepartmentTypeText";
            this.layoutControlItemDepartmentTypeText.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemDepartmentTypeText.TextToControlDistance = 0;
            this.layoutControlItemDepartmentTypeText.TextVisible = false;
            // 
            // layoutControlItemUnitText
            // 
            this.layoutControlItemUnitText.Control = this.labelControlUnitText;
            this.layoutControlItemUnitText.CustomizationFormText = "layoutControlItemUnitText";
            this.layoutControlItemUnitText.Location = new System.Drawing.Point(272, 0);
            this.layoutControlItemUnitText.Name = "layoutControlItemUnitText";
            this.layoutControlItemUnitText.Size = new System.Drawing.Size(102, 17);
            this.layoutControlItemUnitText.Text = "layoutControlItemUnitText";
            this.layoutControlItemUnitText.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemUnitText.TextToControlDistance = 0;
            this.layoutControlItemUnitText.TextVisible = false;
            // 
            // layoutControlGroupFunctions
            // 
            this.layoutControlGroupFunctions.CustomizationFormText = "layoutControlGroupFunctions";
            this.layoutControlGroupFunctions.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemGridcontrol,
            this.emptySpaceItem2,
            this.layoutControlItemActivate});
            this.layoutControlGroupFunctions.Location = new System.Drawing.Point(0, 47);
            this.layoutControlGroupFunctions.Name = "layoutControlGroupFunctions";
            this.layoutControlGroupFunctions.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupFunctions.Size = new System.Drawing.Size(525, 296);
            this.layoutControlGroupFunctions.Text = "layoutControlGroupFunctions";
            // 
            // layoutControlItemGridcontrol
            // 
            this.layoutControlItemGridcontrol.Control = this.gridControlExCommands;
            this.layoutControlItemGridcontrol.CustomizationFormText = "layoutControlItemGridContolCommands";
            this.layoutControlItemGridcontrol.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemGridcontrol.Name = "layoutControlItemGridcontrol";
            this.layoutControlItemGridcontrol.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItemGridcontrol.Size = new System.Drawing.Size(515, 237);
            this.layoutControlItemGridcontrol.Text = "layoutControlItemGridContolCommands";
            this.layoutControlItemGridcontrol.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemGridcontrol.TextToControlDistance = 0;
            this.layoutControlItemGridcontrol.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 237);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(428, 29);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItemActivate
            // 
            this.layoutControlItemActivate.Control = this.simpleButtonActivate;
            this.layoutControlItemActivate.CustomizationFormText = "layoutControlItemActivate";
            this.layoutControlItemActivate.Location = new System.Drawing.Point(428, 237);
            this.layoutControlItemActivate.MaxSize = new System.Drawing.Size(87, 29);
            this.layoutControlItemActivate.MinSize = new System.Drawing.Size(87, 29);
            this.layoutControlItemActivate.Name = "layoutControlItemActivate";
            this.layoutControlItemActivate.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItemActivate.Size = new System.Drawing.Size(87, 29);
            this.layoutControlItemActivate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemActivate.Text = "layoutControlItemActivate";
            this.layoutControlItemActivate.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemActivate.TextToControlDistance = 0;
            this.layoutControlItemActivate.TextVisible = false;
            // 
            // CommandsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(525, 372);
            this.Controls.Add(this.layoutControl1);
            this.MinimumSize = new System.Drawing.Size(533, 406);
            this.Name = "CommandsForm";
            this.Text = "CommandsForm";
            this.Load += new System.EventHandler(this.CommandsForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExCommands)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExCommands)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAccept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupUnitInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepatmentType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartmentTypeText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemUnitText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupFunctions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGridcontrol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemActivate)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private DevExpress.XtraLayout.LayoutControl layoutControl1;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
		private DevExpress.XtraEditors.SimpleButton simpleButtonActivate;
		private GridControlEx gridControlExCommands;
		private GridViewEx gridViewExCommands;
		private DevExpress.XtraEditors.LabelControl labelControlDepartmentType;
		private DevExpress.XtraEditors.LabelControl labelControlDepartmentNameText;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDepartmentTypeText;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDepatmentType;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemGridcontrol;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemActivate;
		private DevExpress.XtraEditors.SimpleButton simpleButtonClose;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemAccept;
		private DevExpress.XtraEditors.LabelControl labelControlUnitText;
		private DevExpress.XtraEditors.LabelControl labelControlUnit;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemUnit;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemUnitText;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupUnitInfo;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupFunctions;
	}
}