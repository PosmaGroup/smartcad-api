using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Smartmatic.SmartCad.Service;

using System.Collections;
using SmartCadCore.ClientData;
using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadControls.Controls;

namespace SmartCadGuiCommon
{
	public partial class CommandsForm : DevExpress.XtraEditors.XtraForm
	{
		#region Fields
		private UnitClientData unit;
		private object syncCommited;
		#endregion

		#region Constructor
		public CommandsForm(UnitClientData unit)
		{
			InitializeComponent();
			ServerServiceClient.GetInstance().CommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(CommandsForm_CommittedChanges);
            this.FormClosing += new FormClosingEventHandler(CommandsForm_FormClosing);
			this.unit = unit;
		}

        void CommandsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            ServerServiceClient.GetInstance().CommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(CommandsForm_CommittedChanges);            
        }
		#endregion

		private void CommandsForm_Load(object sender, EventArgs e)
		{
			LoadLanguage();
			labelControlUnit.Text = unit.CustomCode;
			labelControlDepartmentType.Text = unit.DepartmentType.Name;
			InitializeGridControl();
			FillCommands();
		}

		private void InitializeGridControl()
		{
			gridControlExCommands.Type = typeof(GridControlDataCommands);
			gridControlExCommands.ViewTotalRows = true;
			gridViewExCommands.OptionsView.ShowAutoFilterRow = true;
		}

		private void FillCommands()
		{
			gridControlExCommands.ClearData();
			IList list = ServerServiceClient.GetInstance().SearchClientObjects(
							SmartCadHqls.GetCustomHql("Select sensor From GPSPinSensorData sensor Where sensor.GPS.Code = {0} and sensor.Sensor.Type = {1}",
							unit.GPSCode,
							(int)SensorClientData.SensorType.Output));

			foreach (GPSPinSensorClientData sensor in list)
			{
				gridControlExCommands.AddOrUpdateItem(new GridControlDataCommands(sensor));
			}
		}

		private void LoadLanguage()
		{
            this.Icon = ResourceLoader.GetIcon("$Icon.Commands");
            this.Text = ResourceLoader.GetString2("Commands");
			labelControlDepartmentNameText.Text = ResourceLoader.GetString2("Department") + ": ";
			labelControlUnitText.Text = ResourceLoader.GetString2("Unit") + ": ";
			layoutControlGroupFunctions.Text = ResourceLoader.GetString2("Functions");
			layoutControlGroupUnitInfo.Text = ResourceLoader.GetString2("UnitInfo");
			simpleButtonActivate.Text = ResourceLoader.GetString2("Activate");
			simpleButtonClose.Text = ResourceLoader.GetString2("Close");
		}
	
		void CommandsForm_CommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
		{
			try
			{
				if (e.Objects != null && e.Objects.Count > 0)
				{
					lock (syncCommited)
					{
						try
						{
							if (ServerServiceClient.GetInstance().OperatorClient == null)
							{ }
						}
						catch
						{ }
						if (e.Objects[0] is AlertNotificationClientData)
						{
							AlertNotificationClientData alert = e.Objects[0] as AlertNotificationClientData;
							if (alert.UnitAlert.UnitCode == unit.Code &&
								string.IsNullOrEmpty(alert.SensorName) == false)
							{
								foreach (GridControlDataCommands item in gridControlExCommands.Items)
								{
									if (alert.SensorName.Equals(item.Name))
									{
										FormUtil.InvokeRequired(this, delegate
										{
											item.Status = ResourceLoader.GetString2("Active");
										});
									}
								}
							}
						}
						else if (e.Objects[0] is UnitClientData)
						{
							UnitClientData newUnit = e.Objects[0] as UnitClientData;
							if (unit.Code == newUnit.Code)
							{
								FormUtil.InvokeRequired(this, delegate
								{
									FillCommands();
								});
							}
						}
					}
				}
			}
			catch
			{

			}
		}

		private void simpleButtonClose_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void simpleButtonActivate_Click(object sender, EventArgs e)
		{
			if (gridControlExCommands.SelectedItems.Count > 0)
			{
				IList<int> list = new List<int>();
				foreach (GridControlDataCommands item in gridControlExCommands.SelectedItems)
				{
					list.Add((item.Tag as GPSPinSensorClientData).PinNumber);
				}
				try
				{
					ServerServiceClient.GetInstance().SetGPSOutputs(unit.IdGPS, list, true);
				}
				catch
				{

				}
			}
		}

		private void gridViewExCommands_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
		{
			if (gridControlExCommands.SelectedItems.Count > 0)
				this.simpleButtonActivate.Enabled = true;
			else
				this.simpleButtonActivate.Enabled = false;
		}
	}
}