using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using System.Threading;

using Smartmatic.SmartCad.Service;
using System.IO;
using System.Diagnostics;
using System.Collections;
using DevExpress.Utils;
using SmartCadCore.ClientData;
using SmartCadCore.Core;
using SmartCadControls.Controls;
using SmartCadCore.Common;
using SmartCadControls.Util;
using Smartmatic.SmartCad.Map;


namespace SmartCadGuiCommon
{
	public partial class UnitFollowForm : DevExpress.XtraEditors.XtraForm
	{
		#region Fields
		private UnitTypeClientData unitType;
		private UnitClientData unit;
		private object syncCommited = new object();
        private bool isLoad = false;
        public string IconTable { get; set; }
        public string UnitsTable { get; set; }
		#endregion

		#region Constructor
		public UnitFollowForm(string iconTable,string unitsTable)
		{
			InitializeComponent();
            LoadMap(iconTable, unitsTable);

			this.components = new System.ComponentModel.Container();
            
		}

        private void UnitFollowForm_Load(object sender, EventArgs e)
        {
            ServerServiceClient.GetInstance().CommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(UnitFollowForm_CommittedChanges);

            this.Name = unit.CustomCode;
            this.Text = unit.CustomCode;
            isLoad = true;
            
            mapControlEx1.InitializeMaps();
        }

        private void LoadMap(string iconTable,string unitsTable)
        {
            IconTable = iconTable;
            UnitsTable = unitsTable;
            UnitFollowFormConvertedLayout.BeginUpdate();
            UnitFollowFormConvertedLayout.Controls.Remove(mapControlEx1);
            mapControlEx1 = MapControlEx.GetInstance(SmartCadConfiguration.SmartCadSection.ServerElement.Map.Dll);

            layoutControlItem1.Control = mapControlEx1;

            UnitFollowFormConvertedLayout.Controls.Add(mapControlEx1);
            UnitFollowFormConvertedLayout.Dock = DockStyle.Fill;
            UnitFollowFormConvertedLayout.EndUpdate();

            //Designer
            mapControlEx1.Location = new System.Drawing.Point(7, 7);
            mapControlEx1.Size = new System.Drawing.Size(699, 540);
            mapControlEx1.TabIndex = 0;
            mapControlEx1.ShowDistance = false;
            mapControlEx1.Distance = 0;

            mapControlEx1.Initialized += new EventHandler(mapControlEx1_Initialized);
        }

        void mapControlEx1_Initialized(object sender, EventArgs e)
        {
            if (isLoad == true && CreatedLayers == false)
            {
                UnitFollowRibbonForm parentForm = this.MdiParent as UnitFollowRibbonForm;
                if (parentForm != null)
                {
                    parentForm.mapLayersControl1.MapControlEx = this.mapControlEx1;
                    //parentForm.mapLayersControl1.UpdateMapsLayersTreeView();
                    parentForm.mapLayersControl1.CreateMapsLayersTreeView();
                    parentForm.mapLayersControl1.CreateMapLayersSmartCad("UnitFollow", false);
                    CreatedLayers = true;
                }
            }
            
            mapControlEx1.SetLeftButtonTool("Select");
            // mapControlEx1.FeatureSelected += new MapControlEx.FeatureSelectedEventHandler(Tools_FeatureSelected);

            mapControlEx1.EnableLayer(IconTable, false);
            mapControlEx1.EnableLayer(UnitsTable, false);

            mapControlEx1.zoomTrackBarControl1.Visible = true;
            mapControlEx1.ruler.Visible = false;
            mapControlEx1.ruler.Enabled = false;

            mapControlEx1.CenterMapInPoint(new GeoPoint(unit.Lon, unit.Lat));
            SetUnitPosition(true);
        }
		#endregion

        #region Properties

		public UnitClientData Unit 
		{
			get
			{
				return unit;
			}
			set
			{
				unit = value;
				unitType = (UnitTypeClientData)ServerServiceClient.GetInstance().SearchClientObject(
							SmartCadHqls.GetCustomHql("select type from UnitTypeData type where type.Name = '{0}'", unit.Type.Name));
                mapControlEx1.CreateTable(new MapObjectUnitFollow(), unit.CustomCode, true, false, 11, 100);
			}
		}

        public static bool CreatedLayers { get; set; }
		#endregion

		void UnitFollowForm_CommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
		{
			try
			{
				if (e.Objects != null && e.Objects.Count > 0)
				{
					lock (syncCommited)
					{
						try
						{
							if (ServerServiceClient.GetInstance().OperatorClient == null)
							{ }
						}
						catch
						{ }
                        if (e.Objects[0] is GPSClientData)
                        {
                            foreach (GPSClientData gps in e.Objects)
                                if (unit != null && unit.Code == gps.UnitCode)
                                {
                                    unit.Lon = gps.Lon;
                                    unit.Lat = gps.Lat;

                                    SetUnitPosition(false);
                                }
                        }
					}
				}
			}
			catch
			{

			}
		}
		
		private void SetUnitPosition(bool isFirstTime)
		{
            if (unit != null)
            {
                unit.IconName = unitType != null ? unitType.IconName : "FIRE1-32.BMP";
                
                if (isFirstTime)
                    mapControlEx1.InsertObject(new MapObjectUnitFollow(unit));
                else
                    mapControlEx1.UpdateObject(new MapObjectUnitFollow(unit));

                GeoPoint unitPoint = new GeoPoint(unit.Lon, unit.Lat);
                if (!mapControlEx1.IsVisible(mapControlEx1.GetRelativePoint(unitPoint)))
                {
                    mapControlEx1.CenterMapInPoint(unitPoint);
                }
            }
		}

        private void UnitFollowForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            UnitFollowRibbonForm parentForm = this.MdiParent as UnitFollowRibbonForm;
            if (parentForm != null)
                parentForm.sendingUnits.Remove(this.unit.Code);
            ServerServiceClient.GetInstance().CommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(UnitFollowForm_CommittedChanges);
        }
	}
}
