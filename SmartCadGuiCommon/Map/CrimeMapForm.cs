using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Collections;
using System.Reflection;
using SmartCadCore.Common;
using SmartCadControls.Util;
using SmartCadControls.Controls;
using SmartCadCore.ClientData;
using SmartCadCore.Core;
using Smartmatic.SmartCad.Service;
using Smartmatic.SmartCad.Map;

namespace SmartCadGuiCommon
{
    public partial class CrimeMapForm : DevExpress.XtraEditors.XtraForm
    {
        public bool HeatMap { get; set; }
        List<GeoPoint> allPoints = new List<GeoPoint>();

        public CrimeMapForm(List<MapObjectIncident> incidents, bool heatMap)
        :this(heatMap)
        {
            foreach (MapObjectIncident inc in incidents)
            {
                allPoints.Add(new GeoPoint(inc.Position.Lon, inc.Position.Lat));
            }
        }

        public CrimeMapForm(bool heatMap)
        {
            InitializeComponent();

            HeatMap = heatMap;
            InitializeMaps();
        }

        private void CrimeMapForm_Load(object sender, EventArgs e)
        {
            searchFilterBar1.AddFilter(new IncidentStatusFilter());
            searchFilterBar1.AddFilter(new DatesFilter());
            searchFilterBar1.AddFilter(new IncidentTypeFilter(UserApplicationClientData.FirstLevel));
            searchFilterBar1.SearchRequest += new SearchFilterBar.SearchRequestEventHandler(searchFilterBar1_SearchRequest);
            searchFilterResultsBar1.FilterRequest += new SearchFilterResultsBar.FilterRequestEventHandler(searchFilterResultsBar1_FilterRequest);

            if(HeatMap)
                this.Text = ResourceLoader.GetString2("HeatMapForm"); 
            else
                this.Text = ResourceLoader.GetString2("CrimeMapForm");

            this.dockPanel1.Text = ResourceLoader.GetString2("SearchingToolsMaps");
            dockPanelResults.Text = ResourceLoader.GetString2("SearchResults");
        }

        void searchFilterBar1_SearchRequest(Dictionary<string, Dictionary<string, List<object>>> filters)
        {
            BackgroundProcessForm processForm = new BackgroundProcessForm(
                ResourceLoader.GetString2("Searching"),
                this,
                new MethodInfo[1] {
                    GetType().GetMethod("BackgroundSearch", BindingFlags.NonPublic | BindingFlags.Instance) 
                },
                new object[1][] { new object[] { new object[] { filters } } }
            );
            processForm.CanThrowError = true;
            processForm.ShowDialog();
            
            FormUtil.InvokeRequired(this, delegate
            {
                dockPanelResults.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible;
            });

            ShowMap(allPoints);
        }

        void searchFilterResultsBar1_FilterRequest(List<GeoPoint> points)
        {
            ShowMap(points);
        }

        private void BackgroundSearch(object[] objs)
        {
            Dictionary<string, List<GeoPoint>> results = new Dictionary<string, List<GeoPoint>>();

            Dictionary<string, Dictionary<string, List<object>>> filters = (Dictionary<string, Dictionary<string, List<object>>>)objs[0];
            string HQL = @"SELECT inc FROM IncidentData inc 
            LEFT JOIN FETCH inc.SetReportBaseList reports
            LEFT JOIN FETCH reports.SetIncidentTypes WHERE inc.Address.IsSynchronized = '1' AND ";

            //SET Incident Status filter
            if (filters["IncidentStatusFilter"].Count == 1)
            {
                if (filters["IncidentStatusFilter"].ContainsKey("Open"))
                {
                    HQL += " inc.Status.CustomCode = 'Open' AND";
                }
                else
                {
                    HQL += " inc.Status.CustomCode = 'Closed' AND";
                }
            }
            //SET Incident date interval filter
            string start = ApplicationUtil.GetDataBaseFormattedDate((DateTime)filters["DatesFilter"]["StartDate"][0]);
            string end = ApplicationUtil.GetDataBaseFormattedDate((DateTime)filters["DatesFilter"]["EndDate"][0]);
            HQL += @" inc.StartDate BETWEEN '" + start +
                    "' AND '" + end + "'";
            //SET Incident Type filter
            if (filters["IncidentTypeFilter"]["IncidentType"].Count > 0)
            {
                HQL += @" AND inc.Code in 
                   ( SELECT reportBase.Incident.Code
                        FROM ReportBaseData reportBase 
                        LEFT JOIN reportBase.SetIncidentTypes types
                        WHERE types.Name IN (";
                foreach (object type in filters["IncidentTypeFilter"]["IncidentType"])
                {
                    HQL += "'" + type.ToString() + "', ";
                }
                HQL = HQL.Remove(HQL.Length - 2);
                HQL += "))";
            }

            //IList incidents = ServerServiceClient.GetInstance().SearchClientObjects(HQL);
            IList incidents = ServerServiceClient.GetInstance().SearchClientObjectsMaxRows(HQL, true);
            allPoints = new List<GeoPoint>();
            
            foreach (IncidentClientData inc in incidents)
            {
                GeoPoint gp = new GeoPoint(inc.Address.Lon, inc.Address.Lat);
                allPoints.Add(gp);

                foreach (IncidentTypeClientData incType in inc.IncidentTypes)
                {
                    foreach (object type in filters["IncidentTypeFilter"]["IncidentType"])
                    {
                        if (type.ToString() == incType.Name)
                        {
                            if (results.ContainsKey(incType.FriendlyName))
                            {
                                results[incType.FriendlyName].Add(gp);
                            }
                            else
                            {
                                results.Add(incType.FriendlyName, new List<GeoPoint>() { gp });
                            }
                            break;
                        }
                    }
                }
            }

            FormUtil.InvokeRequired(this, delegate
            {
                searchFilterResultsBar1.ShowResults(results,allPoints);
            });
        }

        private void ShowMap(List<GeoPoint> points)
        {
            if (HeatMap)
                mapControlEx1.ShowHeatMap(points, 12);
            else
                mapControlEx1.ShowCrimeMap(points, 12, 100);
        }

        private void InitializeMaps()
        {
            Controls.Remove(mapControlEx1);

            //if (HeatMap)
                mapControlEx1 = MapControlEx.GetNewInstance(SmartCadConfiguration.SmartCadSection.ServerElement.Map.Dll);
            //else
                //mapControlEx1 = MapControlEx.GetNewInstance("Smartmatic.SmartCAD.Map.Google");

            Controls.Add(mapControlEx1);

            mapControlEx1.Initialized += new EventHandler(mapControlEx1_Initialized);
            mapControlEx1.InitializeMaps();
            
            //Designer
            mapControlEx1.Dock = DockStyle.Fill;
            mapControlEx1.ShowDistance = false;
            mapControlEx1.Distance = 0;
            mapControlEx1.TabIndex = 0;
        }

        void mapControlEx1_Initialized(object sender, EventArgs e)
        {
            mapControlEx1.CreateTable(new MapPoint(0, new GeoPoint(0, 0), ""), mapControlEx1.DynTableTemp, true, false, 11, 100);
        }
        
    }
}