using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Smartmatic.SmartCad.Service;
using System.Net;
using DevExpress.XtraEditors.Controls;
using SmartCadControls.Util;
using SmartCadCore.Core;
using SmartCadControls.Controls;
using SmartCadGuiCommon.Services;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadCore.Enums;
using Smartmatic.SmartCad.Map;


namespace SmartCadGuiCommon
{
    public partial class DefaultMapFormDevX : DevExpress.XtraEditors.XtraForm
    {
        private const string ZOOM_IN_CODE = "ZoomIn";
        private const string ZOOM_OUT_CODE = "ZoomOut";
        private const string PAN_CODE = "Pan";
        private const string CENTER_CODE = "Center";
        private const string ADD_POLYGON_CODE = "AddPolygon";
        private const string SELECT_CODE = "Select";
        private const string ADD_POSITION_CODE = "AddPoint";
       // private const string CLEAR_MAP_CODE = "ClearMap";
        private const string SELECT_RECT_CODE = "SelectRect";
        private const string DISTANCE_CODE = "AddPolyline";
        public const string SELECT_RADIUS_CODE = "SelectRadius";
        private EventHandler idleEventHandler;
        public List<TreeListNodeExType> treeListNodeTypesToActivate = new List<TreeListNodeExType>();

        public DefaultMapFormDevX()
        {

            Console.WriteLine("5   " + DateTime.Now);
            InitializeComponent();
            ApplicationServerService.GisAction += ServerServiceClient_GisAction;
            LoadLanguage();
            LoadBarButtonsComboBox();
        }

        private void LoadBarButtonsComboBox()
        {
            ImageComboBoxItem item = new ImageComboBoxItem("", this.barButtonSelect,0);
            this.repositoryItemImageComboBox1.Items.Add(item);
        }

        private void LoadLanguage()
        {
            this.barButtonPan.Caption = ResourceLoader.GetString2("Pan");
            this.barButtonSelect.Caption = ResourceLoader.GetString2("Select");
            this.barButtonZoomIn.Caption = ResourceLoader.GetString2("ZoomIn");
            this.barButtonZoomOut.Caption = ResourceLoader.GetString2("ZoomOut");
            this.barButtonDistance.Caption = ResourceLoader.GetString2("Distance");
            this.barButtonClearMap.Caption = ResourceLoader.GetString2("ClearMap");
            this.barButtonCenter.Caption = ResourceLoader.GetString2("Center");
            
            this.barButtonAddPosition.Caption = ResourceLoader.GetString2("AddPosition");
            this.barButtonAddRoute.Caption = ResourceLoader.GetString2("AddRoute");
            ribbonPageGroupMapControls.Text = ResourceLoader.GetString2("Tools");
            ribbonPageMapControls.Text = ResourceLoader.GetString2("Options");
			ribbonPageGroupLocation.Text = ResourceLoader.GetString2("Location");
        }

        public ServerServiceClient ServerServiceClient
        {
            get
            {           
                return this.mapLayoutDevX1.ServerServiceClient;
            }
            set
            {
                this.mapLayoutDevX1.ServerServiceClient = value;
            }
        }

        public System.Threading.Timer TimerCheckServer
        {
            get;
            set;
        }

        public NetworkCredential NetworkCredential
        {
            get
            {
                return this.mapLayoutDevX1.NetworkCredential;
            }
            set
            {
                this.mapLayoutDevX1.NetworkCredential = value;
            }
        }

        public bool TrustedConnection
        {
            get
            {
                return this.mapLayoutDevX1.TrustedConnection;
            }
            set
            {
                this.mapLayoutDevX1.TrustedConnection = value;
            }
        }

        void DefaultMapFormDevX_Load(object sender, System.EventArgs e)
        {
            this.Text = ResourceLoader.GetString2("DefaultMapFormDevXText");
            idleEventHandler = OnIdle;
            Application.Idle += idleEventHandler;
            this.FormClosing += DefaultMapFormDevX_FormClosing;
            //mapLayoutDevX1.mapControl.Tools.MiddleButtonTool = PAN_CODE;
            barButtonZoomIn.Tag = ZOOM_IN_CODE;
            barButtonZoomOut.Tag = ZOOM_OUT_CODE;
            barButtonPan.Tag = PAN_CODE;
            barButtonCenter.Tag = CENTER_CODE;

            barButtonAddPosition.Tag = ADD_POSITION_CODE;
            barButtonSelect.Tag = SELECT_CODE;
          //  barButtonClearMap.Tag = CLEAR_MAP_CODE;
            barButtonDistance.Tag = barButtonAddRoute.Tag = DISTANCE_CODE;

            ApplicationServiceClient.Current(UserApplicationClientData.Administration).SendACK(true);
        }


        private void OnIdle(object sender, EventArgs e)
        {
            // Update form's UI when program is idle.

            if (ServerServiceClient != null)
            {
                ApplicationServiceClient.Current(UserApplicationClientData.Administration).SendGeoPoint(new GeoPoint(0, 0));
                ApplicationServiceClient.Current(UserApplicationClientData.FirstLevel).SendEnableButton(true, ButtonType.AddPoint);
                ApplicationServiceClient.Current(UserApplicationClientData.FirstLevel).VerifyStateFrontClient();
            }
            Application.Idle -= idleEventHandler;
            
        }

        void DefaultMapFormDevX_FormClosing(object sender, FormClosingEventArgs e)
        {
            ApplicationServiceClient.Current(UserApplicationClientData.Administration).SendACK(false);
            ApplicationServerService.GisAction -= new EventHandler<GisActionEventArgs>(ServerServiceClient_GisAction);
            ApplicationServerService.GisAction -= new EventHandler<GisActionEventArgs>(mapLayoutDevX1.mapLayersControl.serverServiceClient_GisAction);

            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
            }
        }
        /// <summary>
        /// Metodo para recibir evento que vengan de otros programas hacia mapas...
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ServerServiceClient_GisAction(object sender, GisActionEventArgs e)
        {
            if (e is SendEnableButtonEventArgs)
            {
                if (((SendEnableButtonEventArgs)e).Button == ButtonType.AddPoint)
                {
                    EnableAddPositionButton(((SendEnableButtonEventArgs)e).Enable); 
                }
                else if (((SendEnableButtonEventArgs)e).Button == ButtonType.AddPolygon)
                {
                    EnableAddPolygonButton(((SendEnableButtonEventArgs)e).Enable,ShapeType.None);
                }
                else if (((SendEnableButtonEventArgs)e).Button == ButtonType.AddRoute)
                {
                    EnableAddRouteButton(((SendEnableButtonEventArgs)e).Enable, ShapeType.Route);
                }

                ribbonControl1.Refresh();
                ribbonControl1.Invalidate(true);
            }
            else if (e is SendACKEventArgs)
            {
                if (!((SendACKEventArgs)e).Connected)
                {
                    //EnableAddRouteButton(false, ShapeType.None);
                    FormUtil.InvokeRequired(this, () =>
                    {
                        mapLayoutDevX1.mapLayersControl.MapControlEx.ClearTable(mapLayoutDevX1.mapLayersControl.MapControlEx.DynTableRouteTemp);
                    });
                    EnableAddPolygonButton(false,ShapeType.None);
                    EnableAddPositionButton(false);
                }
                
            }
        }

        public void EnableAddPositionButton(bool enable)
        {
            FormUtil.InvokeRequired(this, () =>
            {
                if (enable == true)
                {
                    barButtonAddPosition.Enabled = true;
                    barButtonAddPosition.PerformClick();
                }
                else
                {
                    mapLayoutDevX1.mapLayersControl.MapControlEx.ClearActions(MapActions.ClearTempTable);
                    barButtonAddPosition.Enabled = false;
                    barButtonSelect.PerformClick();
                }
                //this.Refresh();
            });
        }

        public void EnableAddPolygonButton(bool enable, ShapeType type)
        {
            FormUtil.InvokeRequired(this, () =>
            {
                if (enable == true)
                {
                    barButtonDistance.Enabled = false;
                    mapLayoutDevX1.CurrentShapeType = type;
                    barButtonAddRoute.Enabled = true;

                    barButtonAddRoute.PerformClick();

                }
                else
                {
                    mapLayoutDevX1.mapLayersControl.MapControlEx.ClearActions(MapActions.ClearTempTable);
                    mapLayoutDevX1.mapLayersControl.MapControlEx.ClearActions(MapActions.ClearDistanceTable);
                    mapLayoutDevX1.CurrentShapeType = ShapeType.None;
                    
                    barButtonDistance.Enabled = true;
                    barButtonAddRoute.Enabled = false;

                    //mapLayoutDevX1.Invalidate();   
                    barButtonSelect.PerformClick();
                }
                //this.Refresh();
            });
        }

        public void EnableAddRouteButton(bool enable, ShapeType type)
        {
            FormUtil.InvokeRequired(this, () =>
            {
                if (enable == true)
                {
                    barButtonDistance.Enabled = false;
                    mapLayoutDevX1.CurrentShapeType = type;
                    barButtonAddRoute.Enabled = true;

                    barButtonAddRoute.PerformClick();
                }
                else
                {
                    MapControlEx map = mapLayoutDevX1.mapLayersControl.MapControlEx;
                    map.ClearTable(map.DynTableRouteTemp);
                    map.ClearActions(MapActions.ClearDistanceTable);
                    mapLayoutDevX1.CurrentShapeType = ShapeType.None;
                    
                    barButtonDistance.Enabled = true;
                    barButtonAddRoute.Enabled = false;
                    //mapLayoutDevX1.Invalidate();
                    barButtonSelect.PerformClick();
                }
                //this.Refresh();
            });
        }

        void barButtonZoomIn_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (e.Item.Tag!=null)
            {
                if(e.Item.Tag.ToString() == ADD_POSITION_CODE)
                {
                    mapLayoutDevX1.CurrentShapeType = ShapeType.Incident;
                }
                mapLayoutDevX1.mapLayersControl.MapControlEx.SetLeftButtonTool(e.Item.Tag.ToString());
            }
        }

        private void barButtonDistance_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            mapLayoutDevX1.CurrentShapeType = ShapeType.Distance;
            if (e.Item.Tag!=null)
            mapLayoutDevX1.mapLayersControl.MapControlEx.SetLeftButtonTool(e.Item.Tag.ToString());
        }

        private void barButtonAddRoute_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (e.Item.Tag != null)
                mapLayoutDevX1.mapLayersControl.MapControlEx.SetLeftButtonTool(e.Item.Tag.ToString());
        }

        void barButtonClearMap_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            mapLayoutDevX1.mapLayersControl.ClearMap();
			string s = Keys.Escape.ToString();            
        }

        private void mapLayoutDevX1_Load(object sender, EventArgs e)
        {
            mapLayoutDevX1.mapLayersControl.MapControlEx.SetLeftButtonTool(PAN_CODE);
			mapLayoutDevX1.GlobalApplicationPreference = (this.ParentForm as MapFormDevX).GlobalApplicationPreference;
            mapLayoutDevX1.LoadData();
        }

        private void DefaultMapFormDevX_FormClosing_1(object sender, FormClosingEventArgs e)
        {
            //ApplicationServiceClient.Current(UserApplicationClientData.Administration).SendACK(false);
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
            }
        }

        private void DefaultMapFormDevX_KeyPress(object sender, KeyPressEventArgs e)
        {

        }
    }
}
