using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTab;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using Smartmatic.SmartCad.Service;
using System.Collections;
using SmartCadCore.ClientData;
using DevExpress.XtraTab.ViewInfo;
using SmartCadControls.SyncBoxes;
using SmartCadCore.Core;
using System.Threading;
using SmartCadCore.Common;
using SmartCadControls.Controls;
using SmartCadCore.Enums;
using SmartCadControls;

namespace SmartCadGuiCommon
{
	public partial class OperatorChatForm : DevExpress.XtraEditors.XtraForm
	{
        private object syncCommited = new object();
       private IList supers = new ArrayList();
		private ChatOperatorType chatType;
		private string applicationName = "";
        private int operatorCode;
        private readonly string NOT_CONNECTED = ResourceLoader.GetString("MSG_NotConnected");

		public int DepartmentTypeCode { get; set; }

		public OperatorChatForm(string app, ChatOperatorType chatType)
		{
			InitializeComponent();
			InitializeGridControl();
            LoadLanguage();	

			this.chatType = chatType;
			this.applicationName = app;
            this.operatorCode = ServerServiceClient.GetInstance().OperatorClient.Code;

            this.FormClosing += new FormClosingEventHandler(OperatorChatForm_FormClosing);

		}

        void OperatorChatForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            ServerServiceClient.GetInstance().CommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(OperatorChatForm_CommittedChanges);
        }

		private void InitializeGridControl()
		{			
			gridViewExOpers.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(gridViewEx1_RowCellStyle);	
			gridControlExOpers.ViewTotalRows = true;
			gridControlExOpers.EnableAutoFilter = true;
			gridControlExOpers.Type = typeof(GridControlDataChatOperator);				
		}

		void gridViewEx1_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
		{
			if (e.RowHandle > -1)
				e.Appearance.BackColor = (Color)((GridControlData)(sender as GridViewEx).GetRow(e.RowHandle)).BackColor;
		}

		void OperatorChatForm_Click(object sender, EventArgs e)
		{
			if (ChatXtraTabControl.SelectedTabPage != null)
				gridControlExOpers.GetGridControlData(ChatXtraTabControl.SelectedTabPage.Tag as ClientData).BackColor = Color.Empty;//ChatXtraTabControl.SelectedTabPage.Image = null;
		}

		private void LoadLanguage()
		{
			this.Text = ResourceLoader.GetString2("Chat");
			this.simpleButtonSentToAll.Text = ResourceLoader.GetString2("Send");
            this.layoutControlGroupSendAll.Text = ResourceLoader.GetString2("SendToAll");
		}

		private void gridViewEx1_DoubleClick(object sender, EventArgs e)
		{
            if (((DevExpress.Utils.DXMouseEventArgs)e).Button == MouseButtons.Left)
            {
                if (gridControlExOpers.SelectedItems.Count > 0)
                {
                    GridControlDataChatOperator supItem = ((GridControlDataChatOperator)gridControlExOpers.SelectedItems[0]);
                    OpenNewTab(supItem.Operator, null);
                    ChatXtraTabControl.SelectedTabPage.Focus();
                }
            }
		}

		public void OpenNewTab(OperatorClientData oper, ApplicationMessageClientData amcd)
		{
			OperatorChatControl control = new OperatorChatControl();
			control.Supervisor = oper;
			XtraTabPage tab = new XtraTabPage();
			tab.Tag = oper;
			control.Dock = DockStyle.Fill;
			tab.Controls.Add(control);
			if (amcd != null)
				control.IncomingMessage(amcd);
			tab.Text = oper.FirstName + " " + oper.LastName;
			bool exists = false;
			foreach (XtraTabPage tabs in ChatXtraTabControl.TabPages)
				if (tab.Tag.Equals(tabs.Tag) == true)
				{
					tabs.PageVisible = true;
					//tabs.Image = amcd!=null?ResourceLoader.GetImage("IMAGE_GreenSemaphoreLightSmall"):null;
					if (gridControlExOpers.DataSource != null)
					{
						GridControlDataChatOperator gcdco = gridControlExOpers.GetGridControlData(oper) as GridControlDataChatOperator;
						gcdco.BackColor = amcd != null ? Color.Green : Color.Empty;
						gridControlExOpers.AddOrUpdateItem(gcdco);
					}
					exists = true;
					if (amcd != null)
					{
						control = (tabs.Controls[0] as OperatorChatControl);
						if (control != null)
							control.IncomingMessage(amcd);
					}
                    //ChatXtraTabControl.SelectedTabPage = tabs;
                    //(tabs.Controls[0] as OperatorChatControl).OperatorChatControlConvertedLayout.FocusHelper.FocusElement(
                    //    (tabs.Controls[0] as OperatorChatControl).layoutControlItem4, true);
					break;
				}
			if (exists == false)
			{
				tab.Enter += new EventHandler(tab_Enter);
				ChatXtraTabControl.TabPages.Add(tab);
				if (gridControlExOpers.DataSource != null)
				{
					GridControlDataChatOperator griddata = gridControlExOpers.GetGridControlData(oper) as GridControlDataChatOperator;
					if (griddata != null)
						griddata.BackColor = amcd != null ? Color.Green : Color.Empty;
				}
				//tab.Image = amcd != null ? ResourceLoader.GetImage("IMAGE_GreenSemaphoreLightSmall") : null;
				//ChatXtraTabControl.SelectedTabPage = tab;
                //control.OperatorChatControlConvertedLayout.FocusHelper.FocusElement(
                //        control.layoutControlItem4, true);
			}			
			layoutControlItemTabControl.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
		}

		void tab_Enter(object sender, EventArgs e)
		{
			if (sender != null && gridControlExOpers.DataSource != null)
			{
                GridControlData data = gridControlExOpers.GetGridControlData((sender as DevExpress.XtraTab.XtraTabPage).Tag as ClientData);
                if (data != null)
				    data.BackColor = Color.Empty;
			}
		}


		private void ChatsXtraTabControl_CloseButtonClick(object sender, EventArgs e)
		{
			XtraTabControl tabControl = sender as XtraTabControl;
			ClosePageButtonEventArgs arg = e as ClosePageButtonEventArgs;
			(arg.Page as XtraTabPage).PageVisible = false;
		}

		private void simpleButtonSentToAll_Click(object sender, EventArgs e)
		{
			ApplicationMessageClientData amcd = new ApplicationMessageClientData();
			amcd.Date = ServerServiceClient.GetInstance().GetTime();
			amcd.Message = richTextBox1.Text;
			amcd.Operator = ServerServiceClient.GetInstance().OperatorClient;
			amcd.ToOperators = new ArrayList();
            if (gridControlExOpers.Items.Count > 0)
                foreach (GridControlDataChatOperator item in gridControlExOpers.DataSource as IList)
                    amcd.ToOperators.Add(item.Operator);
				//OpenNewTab(item.Operator, amcd);

			richTextBox1.Text = "";
			
			ServerServiceClient.GetInstance().SendClientData(amcd);
		}

		private void setSupers(IList supers)
		{
			if (supers != null)
			{
				foreach (OperatorClientData sup in supers)
				{
					GridControlDataChatOperator supItem = new GridControlDataChatOperator(sup);
					supItem.applicationName = UserApplicationClientData.Supervision.Name;

					if (supItem.Status.Equals(ResourceLoader.GetString2("MSG_NotConnected")) == false)
						supers.Add(supItem);
					else
						supers.Remove(supItem);
				}
			}
		}

		private void OperatorChatForm_Load(object sender, EventArgs e)
        {
            GetChatOperators();

			ServerServiceClient.GetInstance().CommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(OperatorChatForm_CommittedChanges);
        }

		void OperatorChatForm_CommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
		{
			if (e != null && e.Objects != null && e.Objects.Count > 0)
			{
                if (e.Objects[0] is OperatorClientData)
                {
                    OperatorClientData oper = e.Objects[0] as OperatorClientData;
                    GridControlDataChatOperator operGridData = new GridControlDataChatOperator(oper);
                    if (oper.Code != operatorCode)
                    {
                        if (e.Action != CommittedDataAction.Delete)
                        {
                            if (oper.IsSupervisor == true)
                            {
                                operGridData.applicationName = UserApplicationClientData.Supervision.Name;

                                if (gridControlExOpers.Items.Contains(operGridData) == false)
                                {
                                    if (ServerServiceClient.GetInstance().CheckGeneralSupervisor(oper.RoleCode) == true)
                                    {
                                        UpdateOperator(operGridData);
                                    }
                                    else
                                    {
                                        if (chatType == ChatOperatorType.Operator)
                                        {
                                            DateTime now = SmartCadDatabase.GetTimeFromBD();

                                            //Get my supervisors
                                            IList superCodes = (IList)ServerServiceClient.GetInstance().SearchBasicObjects(
                                                SmartCadHqls.GetCustomHql(SmartCadHqls.GetLoggedInSupervisorCodesByOperatorCode,
                                                operatorCode,
                                                now.ToString(ApplicationUtil.DataBaseFormattedDate),
                                                now.AddDays(7).ToString(ApplicationUtil.DataBaseFormattedDate)));

                                            if (superCodes != null && superCodes.Contains(oper.Code) == true)
                                            {
                                                UpdateOperator(operGridData);
                                            }
                                        }
                                        else
                                        {
                                            //Get another supervisor (specifics or generals)
                                            IList superCodes;

                                            if (applicationName == UserApplicationClientData.FirstLevel.Name)
                                                superCodes = (IList)ServerServiceClient.GetInstance().SearchBasicObjects(SmartCadHqls.GetLoggedInGeneralAndFirstLevelSupervisorsCodes);
                                            else
                                                superCodes = (IList)ServerServiceClient.GetInstance().SearchBasicObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetLoggedInGeneralAndDispatchSupervisorsCodes, DepartmentTypeCode));

                                            if (superCodes != null && superCodes.Contains(oper.Code) == true)
                                            {
                                                UpdateOperator(operGridData);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    UpdateOperator(operGridData);
                                }
                            }
                            else
                            {
                                operGridData.applicationName = applicationName;
                                if (gridControlExOpers.Items.Contains(operGridData) == false)
                                {
                                    if (chatType == ChatOperatorType.SpecificSupervisor)
                                    {
                                        //Get operators supervised by myself
                                        DateTime now = SmartCadDatabase.GetTimeFromBD();

                                        IList myOperatorsCodes = (IList)ServerServiceClient.GetInstance().SearchBasicObjects(
                                            SmartCadHqls.GetCustomHql(SmartCadHqls.GetLoggedInOperatorsSupervisedCodesBySupervisor,
                                            operatorCode,
                                            now.ToString(ApplicationUtil.DataBaseFormattedDate),
                                            now.AddDays(7).ToString(ApplicationUtil.DataBaseFormattedDate), applicationName));

                                        if (myOperatorsCodes != null && myOperatorsCodes.Contains(oper.Code) == true)
                                        {
                                            UpdateOperator(operGridData);
                                        }

                                    }
                                    else if (chatType == ChatOperatorType.GeneralSupervisor)
                                    {
                                        IList operCodes;
                                        if (applicationName.Equals(UserApplicationClientData.FirstLevel.Name))
                                            operCodes = (IList)ServerServiceClient.GetInstance().SearchBasicObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetLoggedInFirstLevelOperatorsCodes, applicationName));
                                        else
                                            operCodes = (IList)ServerServiceClient.GetInstance().SearchBasicObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetLoggedInDispatchOperatorsCodes, DepartmentTypeCode, applicationName));

                                        if (operCodes != null && operCodes.Contains(oper.Code) == true)
                                        {
                                            UpdateOperator(operGridData);
                                        }
                                    }
                                }
                                else 
                                {
                                    UpdateOperator(operGridData);
                                }
                            }

                        }
                        else
                        {
                            gridControlExOpers.DeleteItem(operGridData);
                        }

                    }
                }
			}
		}

        public static IList GetLoggedInSupervisorOperator(int userCode)
        {
            DateTime now = SmartCadDatabase.GetTimeFromBD();
 
            IList supervisors = (IList)ServerServiceClient.GetInstance().SearchClientObjects(
                SmartCadHqls.GetCustomHql(SmartCadHqls.GetLoggedInSupervisorByOperatorCode,
                userCode,
                now.ToString(ApplicationUtil.DataBaseFormattedDate),
                now.AddDays(7).ToString(ApplicationUtil.DataBaseFormattedDate)));
                        
            return supervisors;
        }

        public static IList GetLoggedInOperatorsSupervised(int userCode, string appName)
        {
            DateTime now = SmartCadDatabase.GetTimeFromBD();
            IList operators = (IList)ServerServiceClient.GetInstance().SearchClientObjects(
                SmartCadHqls.GetCustomHql(SmartCadHqls.GetLoggedInOperatorsSupervisedBySupervisor,
                userCode,
                now.ToString(ApplicationUtil.DataBaseFormattedDate),
                now.AddDays(7).ToString(ApplicationUtil.DataBaseFormattedDate), appName));

            return operators;
        }

        public void GetChatOperators()
        {
     
            if (chatType == ChatOperatorType.Operator)
			{
                //Get operator's supervisors logged in
                IList mySupervisors = GetLoggedInSupervisorOperator(operatorCode);

				if (mySupervisors != null)
				{
					foreach (OperatorClientData oper in mySupervisors)
					{
						GridControlDataChatOperator gcdco = new GridControlDataChatOperator(oper);
						gcdco.applicationName = UserApplicationClientData.Supervision.Name;
						UpdateOperator(gcdco);
					}
				}

                //General supervisors by case
                IList generalSupervisors;

                if (applicationName == UserApplicationClientData.FirstLevel.Name)
                    generalSupervisors = (IList)ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetLoggedInGeneralSupervisors);
                else
                {
                    StringBuilder builder = new StringBuilder();
                    builder.Append("(");

                    foreach (DepartmentTypeClientData dep in ServerServiceClient.GetInstance().OperatorClient.DepartmentTypes)
                    {
                        builder.Append(dep.Code);
                        builder.Append(",");
                    }
                    builder.Remove(builder.Length - 1, 1);
                    builder.Append(")");
                    generalSupervisors = (IList)ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetLoggedInGeneralSupervisorsByDepartmentCode, builder.ToString()));

                }
                
                if (generalSupervisors != null)
                {
                    foreach (OperatorClientData oper in generalSupervisors)
                    {
                        GridControlDataChatOperator gcdco = new GridControlDataChatOperator(oper);
                        gcdco.applicationName = UserApplicationClientData.Supervision.Name;
                        UpdateOperator(gcdco);
                    }
                }
				
				
			}
			else
			{
				if (chatType.Equals(ChatOperatorType.SpecificSupervisor))
				{
                    //Get operators supervised by myself
                    IList myOperators = GetLoggedInOperatorsSupervised(operatorCode, applicationName);

					if (myOperators != null)
					{
						foreach (OperatorClientData oper in myOperators)
						{
							GridControlDataChatOperator gcdco = new GridControlDataChatOperator(oper);
							gcdco.applicationName = applicationName;
							UpdateOperator(gcdco);
						}
					}

				}
				else
				{
                    IList operators;
					if (applicationName.Equals(UserApplicationClientData.FirstLevel.Name))
                        operators = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetLoggedInFirstLevelOperators, applicationName));
					else
                        operators = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetLoggedInDispatchOperators, DepartmentTypeCode, applicationName));


                    foreach (OperatorClientData item in operators)
                    {
                        GridControlDataChatOperator gcdco = new GridControlDataChatOperator(item);
                        gcdco.applicationName = applicationName;
                        UpdateOperator(gcdco);
                    }
				}


                //Get another supervisor (specifics or generals)
                IList supervisors;

                if (applicationName == UserApplicationClientData.FirstLevel.Name)
                    supervisors = (IList)ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetLoggedInGeneralAndFirstLevelSupervisors);
                else
                    supervisors = (IList)ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetLoggedInGeneralAndDispatchSupervisors, DepartmentTypeCode));


                if (supervisors != null)
                {
                    foreach (OperatorClientData oper in supervisors)
                    {
                        GridControlDataChatOperator gcdco = new GridControlDataChatOperator(oper);
                        gcdco.applicationName = UserApplicationClientData.Supervision.Name;
                        UpdateOperator(gcdco);
                    }
                }

			}
	           
        }

		private void UpdateOperator(GridControlDataChatOperator oper)
		{
			FormUtil.InvokeRequired(gridControlExOpers,
			  delegate
			  {				
				  if (oper.Status.Equals(NOT_CONNECTED))
					  gridControlExOpers.DeleteItem(oper);
                  else if (oper.Operator.Code != operatorCode &&
					  oper.Status.Equals(ResourceLoader.GetString(NOT_CONNECTED)) == false)
					  gridControlExOpers.AddOrUpdateItem(oper);
			  });
		}
	}

	
}