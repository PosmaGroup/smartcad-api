using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using SmartCadCore.Core;
using SmartCadCore.ClientData;
using Smartmatic.SmartCad.Service;
using System.Collections;
using SmartCadCore.Common;

namespace SmartCadGuiCommon
{
	public partial class ProcessAlertForm : DevExpress.XtraEditors.XtraForm
	{
		private ProcessType processType;
		private AlertNotificationClientData alert;
		public enum ProcessType
		{
			Observation,
			ToProcess,
			ToEnd
		}

		public ProcessAlertForm(ProcessType type,AlertNotificationClientData alert)
		{
			InitializeComponent();
			this.processType = type;
			this.alert = alert;
			
			switch (type)
			{
				case ProcessType.Observation:
					layoutControlItemCheckEditFalseAlarm.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
					break;
				case ProcessType.ToProcess:
					layoutControlItemCheckEditFalseAlarm.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
					break;
				case ProcessType.ToEnd:
					break;
				default:
					break;
			}
		}

		private void ProcessAlertForm_Load(object sender, EventArgs e)
		{
			LoadLanguage();
		}

		private void LoadLanguage()
		{
			switch (processType)
			{
				case ProcessType.Observation:
					labelControlAlertStatus.Text = ResourceLoader.GetString2("AlertStatus") + ":";
					labelControlStatus.Text = ResourceLoader.GetString2(alert.Status.ToString());
					this.Text = ResourceLoader.GetString2("Observations");
					break;
				case ProcessType.ToProcess:
					labelControlAlertStatus.Text = ResourceLoader.GetString2("NewAlertStatus") + ":";
					labelControlStatus.Text = ResourceLoader.GetString2(((AlertNotificationClientData.AlertStatus)((int)alert.Status + 1)).ToString());
					this.Text = ResourceLoader.GetString2("Alert") + ": " + ResourceLoader.GetString2(alert.Status.ToString());
					break;
				case ProcessType.ToEnd:
					labelControlAlertStatus.Text = ResourceLoader.GetString2("NewAlertStatus") + ":";
					labelControlStatus.Text = ResourceLoader.GetString2(((AlertNotificationClientData.AlertStatus)((int)alert.Status + 1)).ToString());
					this.Text = ResourceLoader.GetString2("Alert") + ": " + ResourceLoader.GetString2(alert.Status.ToString());
					break;
				default:
					break;
			}
			simpleButtonAccept.Text = ResourceLoader.GetString2("Accept");
			simpleButtonCancel.Text = ResourceLoader.GetString2("Cancel");
			layoutControlItemObservations.Text = ResourceLoader.GetString2("Observations") + ":*";
			checkEditFalseAlarm.Text = ResourceLoader.GetString2("FalseAlarm");
		}

		private void simpleButtonAccept_Click(object sender, EventArgs e)
		{
			AlertNotificationObservationClientData observation = new AlertNotificationObservationClientData();
			observation.Text = memoEdit1.Text.Trim();
			observation.Date = ServerServiceClient.GetInstance().GetTime();
			observation.AlertNotification = alert;
			alert.AttendingOperator = observation.Operator = ServerServiceClient.GetInstance().OperatorClient;
			if (alert.Observations == null)
				alert.Observations = new ArrayList();
			alert.Observations.Add(observation);
			switch (processType)
			{
				case ProcessType.Observation:
					break;
				case ProcessType.ToProcess:
					alert.Status = AlertNotificationClientData.AlertStatus.InProcess;
					break;
				case ProcessType.ToEnd:
					alert.Status = AlertNotificationClientData.AlertStatus.Ended;
					break;
				default:
					break;
			}
			ServerServiceClient.GetInstance().SaveOrUpdateClientData(alert);
		}

		private void memoEdit1_TextChanged(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(memoEdit1.Text.Trim()) == false)
				simpleButtonAccept.Enabled = true;
			else
				simpleButtonAccept.Enabled = false;
		}
	}
}