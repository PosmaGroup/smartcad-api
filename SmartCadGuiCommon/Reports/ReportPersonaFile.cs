using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Text;
using System.Collections.Generic;
using Smartmatic.SmartCad.Service;
using System.IO;
using SmartCadCore.ClientData;
using SmartCadCore.Common;

namespace SmartCadGuiCommon
{
    public partial class ReportPersonaFile : DevExpress.XtraReports.UI.XtraReport
    {

        //private string labelName;

        public string LabelName
        {
            get { return this.nameLabel.Text; }
            set { this.nameLabel.Text = value; }
        }
	

        public ReportPersonaFile()
        {
            InitializeComponent();
        }

        public ReportPersonaFile(OperatorClientData operatr)
        {
            InitializeComponent();
            SetOperator(operatr);
            LoadLanguage();
        }

        public void LoadLanguage()
        {
            nameLabel.Text = ResourceLoader.GetString2("nameLabel") + ":";
            lastNameLabel.Text = ResourceLoader.GetString2("lastNameLabel") + ":";
            IdLabel.Text = ResourceLoader.GetString2("IdLabel") + ":";
            dateBirthLabel.Text = ResourceLoader.GetString2("dateBirthLabel") + ":";
            phoneLabel.Text = ResourceLoader.GetString2("phoneLabel") + ":";
            addressLabel.Text = ResourceLoader.GetString2("addressLabel") + ":";
            emailLabel.Text = ResourceLoader.GetString2("emailLabel") + ":";
            roleLabel.Text = ResourceLoader.GetString2("roleLabel") + ":";
            departamentsLabel.Text = ResourceLoader.GetString2("departamentsLabel") + ":";
            categoryLabel.Text = ResourceLoader.GetString2("categoryLabel") + ":";
            observationsLabel.Text = ResourceLoader.GetString2("observationsLabel") + ":";
            coursesLabel.Text = ResourceLoader.GetString2("coursesLabel") + ":";
            evaluationsLabel.Text = ResourceLoader.GetString2("evaluationsLabel") + ":";
            categoryTableLabel.Text = ResourceLoader.GetString2("categoryTableLabel");
            dateTableLabel.Text = ResourceLoader.GetString2("dateTableLabel");
            typeObservationLabel.Text = ResourceLoader.GetString2("typeObservationLabel");
            observationLabel.Text = ResourceLoader.GetString2("observationLabel");
            DatetimeLabel.Text = ResourceLoader.GetString2("DatetimeLabel");
            supervisorLabel.Text = ResourceLoader.GetString2("supervisorLabel");
            courseLabel.Text = ResourceLoader.GetString2("courseLabel");
            DateCourseLabel.Text = ResourceLoader.GetString2("DateCourseLabel");
            nameEvaluationLabel.Text = ResourceLoader.GetString2("nameEvaluationLabel");
            qualificationEvaluationLabel.Text = ResourceLoader.GetString2("qualificationEvaluationLabel");
            dateEvaluationLabel.Text = ResourceLoader.GetString2("dateEvaluationLabel");
            evaluatorLabel.Text = ResourceLoader.GetString2("evaluatorLabel");
        }

        public void SetOperator(OperatorClientData ope)
        {
            this.OperatorName.Text = ope.FirstName;
            this.operatorLastName.Text = ope.LastName;
            this.operatorID.Text = ope.PersonID;
            this.operatorDateBirth.Text = ope.Birthday.Value.ToShortDateString();
            this.operatorPhone.Text = ope.Telephone;
            this.operatorAddress.Text = ope.Address;
            this.operatorEmail.Text = ope.Email;
            this.operatorRole.Text = ope.RoleFriendlyName;
            //ServerServiceClient.GetInstance().InitializeLazy(ope, ope.DepartmentTypes);
            this.operatorDepartamets.Text = GetAllDepartaments(ope.DepartmentTypes);
            //ServerServiceClient.GetInstance().InitializeLazy(ope, ope.CategoryList);
            SetCategory(ope.CategoryList);
            OperatorClientData dat = new OperatorClientData();
            dat.Code = ope.Code;
            dat = (OperatorClientData)ServerServiceClient.GetInstance().RefreshClient(dat);
            this.operatorPhoto.Image = GetThumbnailImage(dat.Picture);           
        }


        private Image GetThumbnailImage(byte[] data)
        {
            if (data != null)
            {
                MemoryStream ms = new MemoryStream(data);
                Image image = Image.FromStream(ms);
                Image thumbNailImage = image.GetThumbnailImage(this.operatorPhoto.Width, this.operatorPhoto.Height, new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallback), IntPtr.Zero);
                return thumbNailImage;
            }
            return null;
        }

        private bool ThumbnailCallback()
        {
            return true;
        }

        private void SetCategory(IList ilist)
        {
            int index = 1;
            List<OperatorCategoryHistoryClientData> listSorted = new List<OperatorCategoryHistoryClientData>();
            foreach (OperatorCategoryHistoryClientData var in ilist)
            {
                listSorted.Add(var);
            }

            listSorted.Sort(delegate(OperatorCategoryHistoryClientData opeCat1, OperatorCategoryHistoryClientData opeCat2)
            {
                    return opeCat1.StartDate.Value.CompareTo(opeCat2.StartDate.Value);
            });
            foreach (OperatorCategoryHistoryClientData var in listSorted)
            { 
                XRTableRow row = new XRTableRow();
                XRTableCell cellName = new XRTableCell();
                XRTableCell cellDate = new XRTableCell();

                cellName.Text = var.CategoryFriendlyName;
                cellDate.Text = var.StartDate.Value.ToShortDateString();
                
                row.Cells.AddRange(new XRControl[]{cellName,cellDate});

                this.operatorCategoryTable.Rows.Insert(index, row);
                index++;
            }
        }

        public void SetObservations(List<List<string>> observations)
        {
            SetStrings(observations, this.operatorObservationsTable);
        }

        private void SetStrings(List<List<string>> dataGrid, XRTable table) 
        {
            int index = 1;
            foreach (List<string> list in dataGrid)
            {
                XRTableRow row = new XRTableRow();
                int internIndex = 0;
                foreach (string s in list)
                {
                    XRTableCell cell = new XRTableCell();
                    cell.CanGrow = true;
                    cell.Size = table.Rows[0].Cells[internIndex++].Size;
                    cell.Text = s;
                    row.Cells.Add(cell);
                }
                table.Rows.Insert(index, row);
                index++;
            }
        }

        private string GetAllDepartaments(IList iList)
        {
            StringBuilder sb = new StringBuilder();
            foreach (DepartmentTypeClientData var in iList)
            {
                sb.AppendLine(var.Name + "\r\n");
            }
            return sb.ToString();
        }

        public void SetCourses(List<List<string>> listCourses)
        {
            SetStrings(listCourses, this.operatorCourseTable);
        }

        public void SetEvaluations(List<List<string>> listEvaluations)
        {
            SetStrings(listEvaluations, this.operatorEvaluationsTable);
        }
    }
}
