using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Smartmatic.SmartCad.Service;
using SmartCadCore.Common;
using SmartCadCore.ClientData;

namespace SmartCadGuiCommon
{
    public partial class EvaluationReport : DevExpress.XtraReports.UI.XtraReport
    {

        int[,] locationsRadiosButtons = {{158,75},{225,75},{292,75},{358,75},{425,75}};

        private string Yes = ResourceLoader.GetString2("$Message.Yes");
        private string No = ResourceLoader.GetString2("$Message.No");

        public EvaluationReport()
        {
            InitializeComponent();
        }

        public EvaluationReport(OperatorEvaluationClientData opeEva, string qualificationText)
            : this()
        {
            this.operatorName.Text = opeEva.Operator.FirstName + " " + opeEva.Operator.LastName;
            this.operatorRole.Text = opeEva.RoleOperator;
            this.operatorCategory.Text = opeEva.Category;


            this.evaluationDate.Text = opeEva.Date.ToShortDateString();
            this.evaluationTime.Text = opeEva.Date.ToShortTimeString();
            RenderPreviousEvaluation(opeEva);
            this.score.Text = qualificationText;
            this.evaluationName.Text = opeEva.EvaluationName;
            this.evaluatorFullName.Text = opeEva.EvaluatorFirstName + " " + opeEva.EvaluatorLastName;
        }

        private void RenderPreviousEvaluation(OperatorEvaluationClientData data)
        {
            //int howMany = 0;
            int init = 0;
            int iteratr = 0;
            switch (data.Scale)
            {
                case ClientEvaluationScale.OneToThree:
                    init = 0;
                    iteratr = 2;
                    break;
                case ClientEvaluationScale.OneToFive:
                    init = 0;
                    iteratr = 1;
                    break;
                case ClientEvaluationScale.YesNo:
                    init = 1;
                    iteratr = 2;
                    break;
                default:
                    break;
            }
            int index = 0;
            //ServerServiceClient.GetInstance().InitializeLazy(data, data.Questions);
            for (int i = data.Questions.Count -1; i >= 0; i--)
            {
                OperatorEvaluationQuestionAnswerClientData question = data.Questions[i] as OperatorEvaluationQuestionAnswerClientData;

                XRTableRow row = new XRTableRow();
                row.Name = "row" + i;
                row.Size = new Size(650, 125);
                XRTableCell cell = new XRTableCell();
                cell.CanGrow = true;
                cell.Name = row.Name + "cell";
                cell.Size =  new Size(650, 125);


                //// Number Question
                XRLabel labelNQuestion = new XRLabel();
                labelNQuestion.Name = index + cell.Name;
                labelNQuestion.Text = (data.Questions.Count - i) + ".";
                labelNQuestion.Location = new Point(25, 25);
                labelNQuestion.Size = new Size(33, 25);
                labelNQuestion.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);


                cell.Controls.Add(labelNQuestion);
                //// Question

                XRLabel labelQuestion = new XRLabel();
                labelQuestion.Name = "question" + i;
                labelQuestion.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
                labelQuestion.Location = new System.Drawing.Point(67, 25);
                labelQuestion.Multiline = true;
                labelQuestion.Size = new System.Drawing.Size(450, 25);
                labelQuestion.StylePriority.UseFont = false;
                labelQuestion.Text = question.QuestionName;

                cell.Controls.Add(labelQuestion);

                //// Label Answer.

                XRLabel labelAnswer = new XRLabel();
                labelAnswer.Name = "Answer" + i;
                labelAnswer.Location = new System.Drawing.Point(8, 75);
                labelAnswer.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
                labelAnswer.Size = new System.Drawing.Size(100, 25);
                labelAnswer.Text = ResourceLoader.GetString2("Answer") + ": ";

                cell.Controls.Add(labelAnswer);

                //// Set the answers

                for (int j = init; j < 5; j+=iteratr)
                {
                    XRCheckBox box = new XRCheckBox();

                    box.Location = new Point(locationsRadiosButtons[j,0], locationsRadiosButtons[j,1]);
                    box.Name = "check" + cell.Name;
                    box.Size = new System.Drawing.Size(35, 25);
                    box.Text = SetName(data.Scale, j);
                    if (CheckAnswer(data.Scale,j,question.Answer))
                    {
                        box.CheckState = System.Windows.Forms.CheckState.Checked;
                    }
                    cell.Controls.Add(box);
                }

                //// Add cell to row
                row.Cells.Add(cell);
                this.tableQuestions.Rows.Add(row);
            }
            
        }

        private string SetName(ClientEvaluationScale evaluationScales, int index)
        {
            switch (evaluationScales)
            {
                case ClientEvaluationScale.OneToThree:
                    if (index == 0)
                        return (index + 1).ToString();
                    else if (index == 2)
                        return (index).ToString();
                    else if (index == 4)
                        return (index - 1).ToString();
                    else
                        return index.ToString();
                    //break;
                case ClientEvaluationScale.OneToFive:
                    return (index + 1).ToString();
                    //break;
                case ClientEvaluationScale.YesNo:
                    if (index == 1)
                        return ResourceLoader.GetString2("$Message.Yes");
                    else if (index == 3)
                        return ResourceLoader.GetString2("$Message.No");
                    else
                        return string.Empty;
                        //break;
                default:
                    return string.Empty;
                    //break;
            }
        }


        private bool CheckAnswer(ClientEvaluationScale evaluationScales, int index, int answer)
        {
            switch (evaluationScales)
            {
                case ClientEvaluationScale.OneToThree:
                    if (index == 0 && answer == 1)
                        return true;
                    else if (index == 2 && answer == 2)
                        return true;
                    else if (index == 4 && answer == 3)
                        return true;
                    else
                        return false;
                    //break;
                case ClientEvaluationScale.OneToFive:
                    return (index + 1) == answer;
                    //break;
                case ClientEvaluationScale.YesNo:
                    if (answer == 1 && index == 1)
                        return true;
                    else if ((answer == 0 && index == 3))
                        return true;
                    else
                        return false;
                    //break;
                default:
                    return false;
                    //break;
            }
        }

    }
}
