namespace SmartCadGuiCommon
{
    partial class EvaluationReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EvaluationReport));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.tableQuestions = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCheckBox5 = new DevExpress.XtraReports.UI.XRCheckBox();
            this.xrCheckBox4 = new DevExpress.XtraReports.UI.XRCheckBox();
            this.xrCheckBox3 = new DevExpress.XtraReports.UI.XRCheckBox();
            this.xrCheckBox2 = new DevExpress.XtraReports.UI.XRCheckBox();
            this.xrCheckBox1 = new DevExpress.XtraReports.UI.XRCheckBox();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.headerOperatorData = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.labelOperatorName = new DevExpress.XtraReports.UI.XRTableCell();
            this.operatorName = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.labelOperatorRole = new DevExpress.XtraReports.UI.XRTableCell();
            this.operatorRole = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.evaluationDataHeader = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.labelEvaluationName = new DevExpress.XtraReports.UI.XRTableCell();
            this.labelDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.evaluationDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.labelOperatorCategory = new DevExpress.XtraReports.UI.XRTableCell();
            this.operatorCategory = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.labelEvaluatorFullName = new DevExpress.XtraReports.UI.XRTableCell();
            this.labelTime = new DevExpress.XtraReports.UI.XRTableCell();
            this.evaluationTime = new DevExpress.XtraReports.UI.XRTableCell();
            this.evaluationName = new DevExpress.XtraReports.UI.XRTableCell();
            this.evaluatorFullName = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrRichText1 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.labelObservations = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel2 = new DevExpress.XtraReports.UI.XRPanel();
            this.score = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.tableQuestions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel2,
            this.xrPanel1});
            this.Detail.Height = 1169;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // tableQuestions
            // 
            this.tableQuestions.BorderColor = System.Drawing.Color.Black;
            this.tableQuestions.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.tableQuestions.BorderWidth = 0;
            this.tableQuestions.KeepTogether = true;
            this.tableQuestions.Location = new System.Drawing.Point(8, 217);
            this.tableQuestions.Name = "tableQuestions";
            this.tableQuestions.Size = new System.Drawing.Size(650, 125);
            this.tableQuestions.StylePriority.UseBorderColor = false;
            this.tableQuestions.StylePriority.UseBorders = false;
            this.tableQuestions.StylePriority.UseBorderWidth = false;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Size = new System.Drawing.Size(650, 125);
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrCheckBox5,
            this.xrCheckBox4,
            this.xrCheckBox3,
            this.xrCheckBox2,
            this.xrCheckBox1,
            this.xrLabel3,
            this.xrLabel2,
            this.xrLabel1});
            this.xrTableCell1.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell1.Size = new System.Drawing.Size(650, 125);
            this.xrTableCell1.Text = "xrTableCell1";
            // 
            // xrCheckBox5
            // 
            this.xrCheckBox5.Location = new System.Drawing.Point(425, 75);
            this.xrCheckBox5.Name = "xrCheckBox5";
            this.xrCheckBox5.Size = new System.Drawing.Size(32, 25);
            this.xrCheckBox5.Text = "5";
            // 
            // xrCheckBox4
            // 
            this.xrCheckBox4.Location = new System.Drawing.Point(358, 75);
            this.xrCheckBox4.Name = "xrCheckBox4";
            this.xrCheckBox4.Size = new System.Drawing.Size(32, 25);
            this.xrCheckBox4.Text = "4";
            // 
            // xrCheckBox3
            // 
            this.xrCheckBox3.Location = new System.Drawing.Point(292, 75);
            this.xrCheckBox3.Name = "xrCheckBox3";
            this.xrCheckBox3.Size = new System.Drawing.Size(32, 25);
            this.xrCheckBox3.Text = "3";
            // 
            // xrCheckBox2
            // 
            this.xrCheckBox2.Location = new System.Drawing.Point(225, 75);
            this.xrCheckBox2.Name = "xrCheckBox2";
            this.xrCheckBox2.Size = new System.Drawing.Size(32, 25);
            this.xrCheckBox2.Text = "2";
            // 
            // xrCheckBox1
            // 
            this.xrCheckBox1.Location = new System.Drawing.Point(158, 75);
            this.xrCheckBox1.Name = "xrCheckBox1";
            this.xrCheckBox1.Size = new System.Drawing.Size(33, 25);
            this.xrCheckBox1.Text = "1";
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrLabel3.Location = new System.Drawing.Point(8, 25);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel3.Size = new System.Drawing.Size(33, 25);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.Text = "100.";
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrLabel2.Location = new System.Drawing.Point(67, 25);
            this.xrLabel2.Multiline = true;
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel2.Size = new System.Drawing.Size(450, 25);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.Text = " Atiende la llamda en menos de 3 seg?";
            // 
            // xrLabel1
            // 
            this.xrLabel1.Location = new System.Drawing.Point(8, 75);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel1.Size = new System.Drawing.Size(100, 25);
            this.xrLabel1.Text = "Respuesta:";
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 22;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1,
            this.xrLabel7});
            this.PageFooter.Height = 33;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Location = new System.Drawing.Point(67, 0);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.Size = new System.Drawing.Size(58, 25);
            this.xrPageInfo1.StylePriority.UseTextAlignment = false;
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Location = new System.Drawing.Point(0, 0);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.Size = new System.Drawing.Size(67, 25);
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "Pagina :";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.Height = 55;
            this.TopMargin.Name = "TopMargin";
            // 
            // xrPanel1
            // 
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1,
            this.tableQuestions});
            this.xrPanel1.Location = new System.Drawing.Point(0, 0);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.Size = new System.Drawing.Size(650, 383);
            // 
            // xrTable1
            // 
            this.xrTable1.Location = new System.Drawing.Point(0, 8);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2,
            this.xrTableRow4,
            this.xrTableRow5,
            this.xrTableRow8,
            this.xrTableRow6,
            this.xrTableRow7,
            this.xrTableRow9,
            this.xrTableRow3});
            this.xrTable1.Size = new System.Drawing.Size(650, 200);
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.headerOperatorData});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Size = new System.Drawing.Size(650, 25);
            // 
            // headerOperatorData
            // 
            this.headerOperatorData.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.headerOperatorData.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.headerOperatorData.Location = new System.Drawing.Point(0, 0);
            this.headerOperatorData.Name = "headerOperatorData";
            this.headerOperatorData.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.headerOperatorData.Size = new System.Drawing.Size(650, 25);
            this.headerOperatorData.StylePriority.UseBackColor = false;
            this.headerOperatorData.StylePriority.UseFont = false;
            this.headerOperatorData.StylePriority.UseTextAlignment = false;
            this.headerOperatorData.Text = "Datos del operador";
            this.headerOperatorData.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Size = new System.Drawing.Size(650, 25);
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell5.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell5.Size = new System.Drawing.Size(650, 25);
            this.xrTableCell5.StylePriority.UseBackColor = false;
            this.xrTableCell5.StylePriority.UseFont = false;
            this.xrTableCell5.Text = "Evaluacion";
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.labelOperatorName,
            this.operatorName});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Size = new System.Drawing.Size(650, 25);
            // 
            // labelOperatorName
            // 
            this.labelOperatorName.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.labelOperatorName.Location = new System.Drawing.Point(0, 0);
            this.labelOperatorName.Name = "labelOperatorName";
            this.labelOperatorName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labelOperatorName.Size = new System.Drawing.Size(208, 25);
            this.labelOperatorName.StylePriority.UseFont = false;
            this.labelOperatorName.Text = "Nombre :";
            // 
            // operatorName
            // 
            this.operatorName.Location = new System.Drawing.Point(208, 0);
            this.operatorName.Name = "operatorName";
            this.operatorName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.operatorName.Size = new System.Drawing.Size(442, 25);
            this.operatorName.Text = "operatorName";
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.labelOperatorRole,
            this.operatorRole});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Size = new System.Drawing.Size(650, 25);
            // 
            // labelOperatorRole
            // 
            this.labelOperatorRole.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.labelOperatorRole.Location = new System.Drawing.Point(0, 0);
            this.labelOperatorRole.Name = "labelOperatorRole";
            this.labelOperatorRole.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labelOperatorRole.Size = new System.Drawing.Size(208, 25);
            this.labelOperatorRole.StylePriority.UseFont = false;
            this.labelOperatorRole.Text = "Rol :";
            // 
            // operatorRole
            // 
            this.operatorRole.Location = new System.Drawing.Point(208, 0);
            this.operatorRole.Name = "operatorRole";
            this.operatorRole.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.operatorRole.Size = new System.Drawing.Size(442, 25);
            this.operatorRole.Text = "operatorRole";
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.evaluationDataHeader});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Size = new System.Drawing.Size(650, 25);
            // 
            // evaluationDataHeader
            // 
            this.evaluationDataHeader.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.evaluationDataHeader.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.evaluationDataHeader.Location = new System.Drawing.Point(0, 0);
            this.evaluationDataHeader.Name = "evaluationDataHeader";
            this.evaluationDataHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.evaluationDataHeader.Size = new System.Drawing.Size(650, 25);
            this.evaluationDataHeader.StylePriority.UseBackColor = false;
            this.evaluationDataHeader.StylePriority.UseFont = false;
            this.evaluationDataHeader.Text = "Evaluacion";
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.labelEvaluationName,
            this.evaluationName,
            this.labelDate,
            this.evaluationDate});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Size = new System.Drawing.Size(650, 25);
            // 
            // labelEvaluationName
            // 
            this.labelEvaluationName.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.labelEvaluationName.Location = new System.Drawing.Point(0, 0);
            this.labelEvaluationName.Name = "labelEvaluationName";
            this.labelEvaluationName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labelEvaluationName.Size = new System.Drawing.Size(175, 25);
            this.labelEvaluationName.StylePriority.UseFont = false;
            this.labelEvaluationName.Text = "Nombre :";
            // 
            // labelDate
            // 
            this.labelDate.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.labelDate.Location = new System.Drawing.Point(417, 0);
            this.labelDate.Name = "labelDate";
            this.labelDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labelDate.Size = new System.Drawing.Size(91, 25);
            this.labelDate.StylePriority.UseFont = false;
            this.labelDate.Text = "Fecha :";
            // 
            // evaluationDate
            // 
            this.evaluationDate.Location = new System.Drawing.Point(508, 0);
            this.evaluationDate.Name = "evaluationDate";
            this.evaluationDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.evaluationDate.Size = new System.Drawing.Size(142, 25);
            this.evaluationDate.Text = "evaluationDate";
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.labelOperatorCategory,
            this.operatorCategory});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Size = new System.Drawing.Size(650, 25);
            // 
            // labelOperatorCategory
            // 
            this.labelOperatorCategory.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.labelOperatorCategory.Location = new System.Drawing.Point(0, 0);
            this.labelOperatorCategory.Name = "labelOperatorCategory";
            this.labelOperatorCategory.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labelOperatorCategory.Size = new System.Drawing.Size(208, 25);
            this.labelOperatorCategory.StylePriority.UseFont = false;
            this.labelOperatorCategory.Text = "Categoria";
            // 
            // operatorCategory
            // 
            this.operatorCategory.Location = new System.Drawing.Point(208, 0);
            this.operatorCategory.Name = "operatorCategory";
            this.operatorCategory.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.operatorCategory.Size = new System.Drawing.Size(442, 25);
            this.operatorCategory.Text = "operatorCategory";
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.labelEvaluatorFullName,
            this.evaluatorFullName,
            this.labelTime,
            this.evaluationTime});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Size = new System.Drawing.Size(650, 25);
            // 
            // labelEvaluatorFullName
            // 
            this.labelEvaluatorFullName.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.labelEvaluatorFullName.Location = new System.Drawing.Point(0, 0);
            this.labelEvaluatorFullName.Name = "labelEvaluatorFullName";
            this.labelEvaluatorFullName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labelEvaluatorFullName.Size = new System.Drawing.Size(175, 25);
            this.labelEvaluatorFullName.StylePriority.UseFont = false;
            this.labelEvaluatorFullName.Text = "Evaluador :";
            // 
            // labelTime
            // 
            this.labelTime.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.labelTime.Location = new System.Drawing.Point(417, 0);
            this.labelTime.Name = "labelTime";
            this.labelTime.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labelTime.Size = new System.Drawing.Size(91, 25);
            this.labelTime.StylePriority.UseFont = false;
            this.labelTime.Text = "Hora :";
            // 
            // evaluationTime
            // 
            this.evaluationTime.Location = new System.Drawing.Point(508, 0);
            this.evaluationTime.Name = "evaluationTime";
            this.evaluationTime.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.evaluationTime.Size = new System.Drawing.Size(142, 25);
            this.evaluationTime.Text = "evaluationTime";
            // 
            // evaluationName
            // 
            this.evaluationName.Location = new System.Drawing.Point(175, 0);
            this.evaluationName.Name = "evaluationName";
            this.evaluationName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.evaluationName.Size = new System.Drawing.Size(242, 25);
            this.evaluationName.Text = "evaluationName";
            // 
            // evaluatorFullName
            // 
            this.evaluatorFullName.Location = new System.Drawing.Point(175, 0);
            this.evaluatorFullName.Name = "evaluatorFullName";
            this.evaluatorFullName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.evaluatorFullName.Size = new System.Drawing.Size(242, 25);
            this.evaluatorFullName.Text = "evaluatorFullName";
            // 
            // xrRichText1
            // 
            this.xrRichText1.BorderColor = System.Drawing.Color.Black;
            this.xrRichText1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrRichText1.Location = new System.Drawing.Point(0, 108);
            this.xrRichText1.Name = "xrRichText1";
            this.xrRichText1.SerializableRtfString = resources.GetString("xrRichText1.SerializableRtfString");
            this.xrRichText1.Size = new System.Drawing.Size(650, 192);
            this.xrRichText1.StylePriority.UseBorderColor = false;
            this.xrRichText1.StylePriority.UseBorders = false;
            // 
            // xrLine1
            // 
            this.xrLine1.Location = new System.Drawing.Point(17, 367);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.Size = new System.Drawing.Size(183, 8);
            // 
            // xrLine2
            // 
            this.xrLine2.Location = new System.Drawing.Point(442, 367);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.Size = new System.Drawing.Size(183, 8);
            // 
            // xrLabel4
            // 
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrLabel4.Location = new System.Drawing.Point(33, 392);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.Size = new System.Drawing.Size(158, 25);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.Text = "Firma operador";
            // 
            // xrLabel5
            // 
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrLabel5.Location = new System.Drawing.Point(450, 392);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.Size = new System.Drawing.Size(133, 25);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.Text = "Firma supervisor";
            // 
            // labelObservations
            // 
            this.labelObservations.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.labelObservations.Location = new System.Drawing.Point(0, 83);
            this.labelObservations.Name = "labelObservations";
            this.labelObservations.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labelObservations.Size = new System.Drawing.Size(125, 25);
            this.labelObservations.StylePriority.UseFont = false;
            this.labelObservations.Text = "Observaciones :";
            // 
            // xrPanel2
            // 
            this.xrPanel2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel6,
            this.score,
            this.xrLabel4,
            this.xrLine1,
            this.xrLabel5,
            this.labelObservations,
            this.xrLine2,
            this.xrRichText1});
            this.xrPanel2.Location = new System.Drawing.Point(0, 400);
            this.xrPanel2.Name = "xrPanel2";
            this.xrPanel2.Size = new System.Drawing.Size(650, 442);
            // 
            // score
            // 
            this.score.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.score.Location = new System.Drawing.Point(83, 42);
            this.score.Name = "score";
            this.score.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.score.Size = new System.Drawing.Size(100, 25);
            this.score.StylePriority.UseFont = false;
            this.score.Text = "score";
            // 
            // xrLabel6
            // 
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrLabel6.Location = new System.Drawing.Point(0, 42);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.Size = new System.Drawing.Size(67, 25);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.Text = "Nota :";
            // 
            // EvaluationReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.PageFooter,
            this.TopMargin});
            this.ExportOptions.PrintPreview.SaveMode = DevExpress.XtraPrinting.SaveMode.UsingDefaultPath;
            this.ExportOptions.PrintPreview.ShowOptionsBeforeExport = false;
            this.Margins = new System.Drawing.Printing.Margins(100, 100, 55, 100);
            this.ShowExportWarnings = false;
            this.ShowPrintingWarnings = false;
            this.Version = "8.2";
            ((System.ComponentModel.ISupportInitialize)(this.tableQuestions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.XRCheckBox xrCheckBox1;
        private DevExpress.XtraReports.UI.XRCheckBox xrCheckBox5;
        private DevExpress.XtraReports.UI.XRCheckBox xrCheckBox4;
        private DevExpress.XtraReports.UI.XRCheckBox xrCheckBox3;
        private DevExpress.XtraReports.UI.XRCheckBox xrCheckBox2;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRTable tableQuestions;
        private DevExpress.XtraReports.UI.XRPanel xrPanel1;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell headerOperatorData;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell labelOperatorName;
        private DevExpress.XtraReports.UI.XRTableCell operatorName;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell labelOperatorRole;
        private DevExpress.XtraReports.UI.XRTableCell operatorRole;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell labelOperatorCategory;
        private DevExpress.XtraReports.UI.XRTableCell operatorCategory;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell evaluationDataHeader;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell labelEvaluationName;
        private DevExpress.XtraReports.UI.XRTableCell labelDate;
        private DevExpress.XtraReports.UI.XRTableCell evaluationDate;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell labelEvaluatorFullName;
        private DevExpress.XtraReports.UI.XRTableCell labelTime;
        private DevExpress.XtraReports.UI.XRTableCell evaluationTime;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell evaluationName;
        private DevExpress.XtraReports.UI.XRTableCell evaluatorFullName;
        private DevExpress.XtraReports.UI.XRLabel labelObservations;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRRichText xrRichText1;
        private DevExpress.XtraReports.UI.XRPanel xrPanel2;
        private DevExpress.XtraReports.UI.XRLabel score;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
    }
}
