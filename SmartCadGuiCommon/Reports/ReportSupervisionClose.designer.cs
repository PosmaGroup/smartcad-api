namespace SmartCadGuiCommon
{
    partial class ReportSupervisionClose
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportSupervisionClose));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.supervisorDepartmentTypes = new DevExpress.XtraReports.UI.XRLabel();
            this.supervisorDepartmentTypesLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.Title = new DevExpress.XtraReports.UI.XRLabel();
            this.winControlContainer1 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.IndicatorsPhoto = new System.Windows.Forms.PictureBox();
            this.messagesLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.endReport = new DevExpress.XtraReports.UI.XRLabel();
            this.endReportLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.supervisorType = new DevExpress.XtraReports.UI.XRLabel();
            this.supervisorTypeLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.supervisorName = new DevExpress.XtraReports.UI.XRLabel();
            this.supervisorNameLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.messages = new DevExpress.XtraReports.UI.XRRichText();
            this.supervisorId = new DevExpress.XtraReports.UI.XRLabel();
            this.supervisorIdLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.beginSession = new DevExpress.XtraReports.UI.XRLabel();
            this.beginSessionLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrControlStyle1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.indicatorsLabel = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.IndicatorsPhoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.messages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.indicatorsLabel,
            this.supervisorDepartmentTypes,
            this.supervisorDepartmentTypesLabel,
            this.Title,
            this.winControlContainer1,
            this.messagesLabel,
            this.endReport,
            this.endReportLabel,
            this.supervisorType,
            this.supervisorTypeLabel,
            this.supervisorName,
            this.supervisorNameLabel,
            this.messages,
            this.supervisorId,
            this.supervisorIdLabel,
            this.beginSession,
            this.beginSessionLabel});
            this.Detail.Height = 978;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // supervisorDepartmentTypes
            // 
            this.supervisorDepartmentTypes.BackColor = System.Drawing.Color.Beige;
            this.supervisorDepartmentTypes.CanShrink = true;
            this.supervisorDepartmentTypes.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.supervisorDepartmentTypes.Location = new System.Drawing.Point(175, 165);
            this.supervisorDepartmentTypes.Multiline = true;
            this.supervisorDepartmentTypes.Name = "supervisorDepartmentTypes";
            this.supervisorDepartmentTypes.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 2, 0, 0, 100F);
            this.supervisorDepartmentTypes.Size = new System.Drawing.Size(575, 25);
            this.supervisorDepartmentTypes.StylePriority.UseBackColor = false;
            this.supervisorDepartmentTypes.StylePriority.UseFont = false;
            this.supervisorDepartmentTypes.StylePriority.UsePadding = false;
            this.supervisorDepartmentTypes.StylePriority.UseTextAlignment = false;
            this.supervisorDepartmentTypes.Text = "supervisorDepartmentType";
            this.supervisorDepartmentTypes.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // supervisorDepartmentTypesLabel
            // 
            this.supervisorDepartmentTypesLabel.BackColor = System.Drawing.Color.CornflowerBlue;
            this.supervisorDepartmentTypesLabel.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.supervisorDepartmentTypesLabel.ForeColor = System.Drawing.Color.White;
            this.supervisorDepartmentTypesLabel.Location = new System.Drawing.Point(0, 165);
            this.supervisorDepartmentTypesLabel.Name = "supervisorDepartmentTypesLabel";
            this.supervisorDepartmentTypesLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 2, 0, 0, 100F);
            this.supervisorDepartmentTypesLabel.Size = new System.Drawing.Size(167, 25);
            this.supervisorDepartmentTypesLabel.StylePriority.UseBackColor = false;
            this.supervisorDepartmentTypesLabel.StylePriority.UseFont = false;
            this.supervisorDepartmentTypesLabel.StylePriority.UseForeColor = false;
            this.supervisorDepartmentTypesLabel.StylePriority.UsePadding = false;
            this.supervisorDepartmentTypesLabel.StylePriority.UseTextAlignment = false;
            this.supervisorDepartmentTypesLabel.Text = "Organismos";
            this.supervisorDepartmentTypesLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // Title
            // 
            this.Title.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Title.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Title.ForeColor = System.Drawing.Color.White;
            this.Title.Location = new System.Drawing.Point(0, 0);
            this.Title.Name = "Title";
            this.Title.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Title.Size = new System.Drawing.Size(750, 25);
            this.Title.StylePriority.UseBackColor = false;
            this.Title.StylePriority.UseFont = false;
            this.Title.StylePriority.UseForeColor = false;
            this.Title.StylePriority.UseTextAlignment = false;
            this.Title.Text = "Reporte de Cierre";
            this.Title.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // winControlContainer1
            // 
            this.winControlContainer1.Location = new System.Drawing.Point(175, 267);
            this.winControlContainer1.Name = "winControlContainer1";
            this.winControlContainer1.Size = new System.Drawing.Size(575, 342);
            this.winControlContainer1.WinControl = this.IndicatorsPhoto;
            // 
            // IndicatorsPhoto
            // 
            this.IndicatorsPhoto.Location = new System.Drawing.Point(0, 0);
            this.IndicatorsPhoto.Name = "IndicatorsPhoto";
            this.IndicatorsPhoto.Size = new System.Drawing.Size(552, 328);
            this.IndicatorsPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.IndicatorsPhoto.TabIndex = 0;
            this.IndicatorsPhoto.TabStop = false;
            // 
            // messagesLabel
            // 
            this.messagesLabel.BackColor = System.Drawing.Color.CornflowerBlue;
            this.messagesLabel.BorderWidth = 2;
            this.messagesLabel.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messagesLabel.ForeColor = System.Drawing.Color.White;
            this.messagesLabel.Location = new System.Drawing.Point(0, 633);
            this.messagesLabel.Name = "messagesLabel";
            this.messagesLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 2, 0, 0, 100F);
            this.messagesLabel.Size = new System.Drawing.Size(167, 25);
            this.messagesLabel.StylePriority.UseBackColor = false;
            this.messagesLabel.StylePriority.UseBorderWidth = false;
            this.messagesLabel.StylePriority.UseFont = false;
            this.messagesLabel.StylePriority.UseForeColor = false;
            this.messagesLabel.StylePriority.UsePadding = false;
            this.messagesLabel.StylePriority.UseTextAlignment = false;
            this.messagesLabel.Text = "Observaciones:";
            this.messagesLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // endReport
            // 
            this.endReport.BackColor = System.Drawing.Color.Beige;
            this.endReport.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.endReport.Location = new System.Drawing.Point(175, 225);
            this.endReport.Name = "endReport";
            this.endReport.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 2, 0, 0, 100F);
            this.endReport.Size = new System.Drawing.Size(575, 25);
            this.endReport.StylePriority.UseBackColor = false;
            this.endReport.StylePriority.UseFont = false;
            this.endReport.StylePriority.UsePadding = false;
            this.endReport.StylePriority.UseTextAlignment = false;
            this.endReport.Text = "endReport";
            this.endReport.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // endReportLabel
            // 
            this.endReportLabel.BackColor = System.Drawing.Color.CornflowerBlue;
            this.endReportLabel.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.endReportLabel.ForeColor = System.Drawing.Color.White;
            this.endReportLabel.Location = new System.Drawing.Point(0, 225);
            this.endReportLabel.Name = "endReportLabel";
            this.endReportLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 2, 0, 0, 100F);
            this.endReportLabel.Size = new System.Drawing.Size(167, 25);
            this.endReportLabel.StylePriority.UseBackColor = false;
            this.endReportLabel.StylePriority.UseFont = false;
            this.endReportLabel.StylePriority.UseForeColor = false;
            this.endReportLabel.StylePriority.UsePadding = false;
            this.endReportLabel.StylePriority.UseTextAlignment = false;
            this.endReportLabel.Text = "Fecha de finalizacion:";
            this.endReportLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // supervisorType
            // 
            this.supervisorType.BackColor = System.Drawing.Color.Beige;
            this.supervisorType.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.supervisorType.Location = new System.Drawing.Point(175, 135);
            this.supervisorType.Name = "supervisorType";
            this.supervisorType.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 2, 0, 0, 100F);
            this.supervisorType.Size = new System.Drawing.Size(575, 25);
            this.supervisorType.StylePriority.UseBackColor = false;
            this.supervisorType.StylePriority.UseFont = false;
            this.supervisorType.StylePriority.UsePadding = false;
            this.supervisorType.StylePriority.UseTextAlignment = false;
            this.supervisorType.Text = "supervisorType";
            this.supervisorType.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // supervisorTypeLabel
            // 
            this.supervisorTypeLabel.BackColor = System.Drawing.Color.CornflowerBlue;
            this.supervisorTypeLabel.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.supervisorTypeLabel.ForeColor = System.Drawing.Color.White;
            this.supervisorTypeLabel.Location = new System.Drawing.Point(0, 135);
            this.supervisorTypeLabel.Name = "supervisorTypeLabel";
            this.supervisorTypeLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 2, 0, 0, 100F);
            this.supervisorTypeLabel.Size = new System.Drawing.Size(167, 25);
            this.supervisorTypeLabel.StylePriority.UseBackColor = false;
            this.supervisorTypeLabel.StylePriority.UseFont = false;
            this.supervisorTypeLabel.StylePriority.UseForeColor = false;
            this.supervisorTypeLabel.StylePriority.UsePadding = false;
            this.supervisorTypeLabel.StylePriority.UseTextAlignment = false;
            this.supervisorTypeLabel.Text = "Perfil:";
            this.supervisorTypeLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // supervisorName
            // 
            this.supervisorName.BackColor = System.Drawing.Color.Beige;
            this.supervisorName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.supervisorName.Location = new System.Drawing.Point(175, 75);
            this.supervisorName.Name = "supervisorName";
            this.supervisorName.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 2, 0, 0, 100F);
            this.supervisorName.Size = new System.Drawing.Size(575, 25);
            this.supervisorName.StylePriority.UseBackColor = false;
            this.supervisorName.StylePriority.UseFont = false;
            this.supervisorName.StylePriority.UsePadding = false;
            this.supervisorName.StylePriority.UseTextAlignment = false;
            this.supervisorName.Text = "supervisorName";
            this.supervisorName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // supervisorNameLabel
            // 
            this.supervisorNameLabel.BackColor = System.Drawing.Color.CornflowerBlue;
            this.supervisorNameLabel.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.supervisorNameLabel.ForeColor = System.Drawing.Color.White;
            this.supervisorNameLabel.Location = new System.Drawing.Point(0, 75);
            this.supervisorNameLabel.Name = "supervisorNameLabel";
            this.supervisorNameLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 2, 0, 0, 100F);
            this.supervisorNameLabel.Size = new System.Drawing.Size(167, 25);
            this.supervisorNameLabel.StylePriority.UseBackColor = false;
            this.supervisorNameLabel.StylePriority.UseFont = false;
            this.supervisorNameLabel.StylePriority.UseForeColor = false;
            this.supervisorNameLabel.StylePriority.UsePadding = false;
            this.supervisorNameLabel.StylePriority.UseTextAlignment = false;
            this.supervisorNameLabel.Text = "Supervisor:";
            this.supervisorNameLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // messages
            // 
            this.messages.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.messages.BackColor = System.Drawing.Color.Beige;
            this.messages.CanShrink = true;
            this.messages.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messages.Location = new System.Drawing.Point(0, 667);
            this.messages.Name = "messages";
            this.messages.SerializableRtfString = resources.GetString("messages.SerializableRtfString");
            this.messages.Size = new System.Drawing.Size(750, 233);
            this.messages.StylePriority.UseBackColor = false;
            this.messages.StylePriority.UseFont = false;
            // 
            // supervisorId
            // 
            this.supervisorId.BackColor = System.Drawing.Color.Beige;
            this.supervisorId.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.supervisorId.Location = new System.Drawing.Point(175, 105);
            this.supervisorId.Name = "supervisorId";
            this.supervisorId.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 2, 0, 0, 100F);
            this.supervisorId.Size = new System.Drawing.Size(575, 25);
            this.supervisorId.StylePriority.UseBackColor = false;
            this.supervisorId.StylePriority.UseFont = false;
            this.supervisorId.StylePriority.UsePadding = false;
            this.supervisorId.StylePriority.UseTextAlignment = false;
            this.supervisorId.Text = "supervisorId";
            this.supervisorId.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // supervisorIdLabel
            // 
            this.supervisorIdLabel.BackColor = System.Drawing.Color.CornflowerBlue;
            this.supervisorIdLabel.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.supervisorIdLabel.ForeColor = System.Drawing.Color.White;
            this.supervisorIdLabel.Location = new System.Drawing.Point(0, 105);
            this.supervisorIdLabel.Name = "supervisorIdLabel";
            this.supervisorIdLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 2, 0, 0, 100F);
            this.supervisorIdLabel.Size = new System.Drawing.Size(167, 25);
            this.supervisorIdLabel.StylePriority.UseBackColor = false;
            this.supervisorIdLabel.StylePriority.UseFont = false;
            this.supervisorIdLabel.StylePriority.UseForeColor = false;
            this.supervisorIdLabel.StylePriority.UsePadding = false;
            this.supervisorIdLabel.StylePriority.UseTextAlignment = false;
            this.supervisorIdLabel.Text = "cedula";
            this.supervisorIdLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // beginSession
            // 
            this.beginSession.BackColor = System.Drawing.Color.Beige;
            this.beginSession.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.beginSession.Location = new System.Drawing.Point(175, 195);
            this.beginSession.Name = "beginSession";
            this.beginSession.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 2, 0, 0, 100F);
            this.beginSession.Size = new System.Drawing.Size(575, 25);
            this.beginSession.StylePriority.UseBackColor = false;
            this.beginSession.StylePriority.UseFont = false;
            this.beginSession.StylePriority.UsePadding = false;
            this.beginSession.StylePriority.UseTextAlignment = false;
            this.beginSession.Text = "beginSession";
            this.beginSession.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // beginSessionLabel
            // 
            this.beginSessionLabel.BackColor = System.Drawing.Color.CornflowerBlue;
            this.beginSessionLabel.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.beginSessionLabel.ForeColor = System.Drawing.Color.White;
            this.beginSessionLabel.Location = new System.Drawing.Point(0, 195);
            this.beginSessionLabel.Name = "beginSessionLabel";
            this.beginSessionLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 2, 0, 0, 100F);
            this.beginSessionLabel.Size = new System.Drawing.Size(167, 25);
            this.beginSessionLabel.StylePriority.UseBackColor = false;
            this.beginSessionLabel.StylePriority.UseFont = false;
            this.beginSessionLabel.StylePriority.UseForeColor = false;
            this.beginSessionLabel.StylePriority.UsePadding = false;
            this.beginSessionLabel.StylePriority.UseTextAlignment = false;
            this.beginSessionLabel.Text = "Inicio de sesion";
            this.beginSessionLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1,
            this.xrLine1});
            this.PageHeader.Height = 26;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrPageInfo1.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.xrPageInfo1.Format = "{0:MM/dd/yyyy HH:mm}";
            this.xrPageInfo1.Location = new System.Drawing.Point(633, 3);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo1.Size = new System.Drawing.Size(115, 18);
            this.xrPageInfo1.StylePriority.UseFont = false;
            this.xrPageInfo1.StylePriority.UseForeColor = false;
            this.xrPageInfo1.StylePriority.UseTextAlignment = false;
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLine1
            // 
            this.xrLine1.BorderColor = System.Drawing.Color.CornflowerBlue;
            this.xrLine1.BorderWidth = 2;
            this.xrLine1.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.xrLine1.Location = new System.Drawing.Point(0, 17);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.Size = new System.Drawing.Size(750, 8);
            this.xrLine1.StylePriority.UseBorderColor = false;
            this.xrLine1.StylePriority.UseBorderWidth = false;
            this.xrLine1.StylePriority.UseForeColor = false;
            // 
            // xrControlStyle1
            // 
            this.xrControlStyle1.Name = "xrControlStyle1";
            this.xrControlStyle1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.Filter = "html | *.html";
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo2});
            this.PageFooter.Height = 26;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrPageInfo2.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.xrPageInfo2.Format = "Pagina {0} de {1}";
            this.xrPageInfo2.Location = new System.Drawing.Point(658, 0);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo2.Size = new System.Drawing.Size(90, 25);
            this.xrPageInfo2.StylePriority.UseFont = false;
            this.xrPageInfo2.StylePriority.UseForeColor = false;
            this.xrPageInfo2.StylePriority.UseTextAlignment = false;
            this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(536, 320);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // indicatorsLabel
            // 
            this.indicatorsLabel.BackColor = System.Drawing.Color.CornflowerBlue;
            this.indicatorsLabel.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.indicatorsLabel.ForeColor = System.Drawing.Color.White;
            this.indicatorsLabel.Location = new System.Drawing.Point(0, 267);
            this.indicatorsLabel.Name = "indicatorsLabel";
            this.indicatorsLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.indicatorsLabel.Size = new System.Drawing.Size(167, 25);
            this.indicatorsLabel.StylePriority.UseBackColor = false;
            this.indicatorsLabel.StylePriority.UseFont = false;
            this.indicatorsLabel.StylePriority.UseForeColor = false;
            this.indicatorsLabel.StylePriority.UsePadding = false;
            this.indicatorsLabel.StylePriority.UseTextAlignment = false;
            this.indicatorsLabel.Text = "Indicadores";
            this.indicatorsLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // ReportSupervisionClose
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.PageFooter});
            this.ExportOptions.PrintPreview.SaveMode = DevExpress.XtraPrinting.SaveMode.UsingDefaultPath;
            this.ExportOptions.PrintPreview.ShowOptionsBeforeExport = false;
            this.Margins = new System.Drawing.Printing.Margins(50, 50, 50, 50);
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.xrControlStyle1});
            this.Version = "8.2";
            ((System.ComponentModel.ISupportInitialize)(this.IndicatorsPhoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.messages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.XRLabel supervisorNameLabel;
        private DevExpress.XtraReports.UI.XRLabel supervisorType;
        private DevExpress.XtraReports.UI.XRLabel supervisorTypeLabel;
        private DevExpress.XtraReports.UI.XRLabel supervisorName;
        private DevExpress.XtraReports.UI.XRLabel endReport;
        private DevExpress.XtraReports.UI.XRLabel endReportLabel;
        private DevExpress.XtraReports.UI.XRLabel messagesLabel;
        private DevExpress.XtraReports.UI.XRControlStyle xrControlStyle1;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer1;
        private System.Windows.Forms.PictureBox IndicatorsPhoto;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private DevExpress.XtraReports.UI.XRLabel Title;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRRichText messages;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo2;
        private DevExpress.XtraReports.UI.XRLabel beginSessionLabel;
        private DevExpress.XtraReports.UI.XRLabel beginSession;
        private DevExpress.XtraReports.UI.XRLabel supervisorIdLabel;
        private DevExpress.XtraReports.UI.XRLabel supervisorId;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraReports.UI.XRLabel supervisorDepartmentTypes;
        private DevExpress.XtraReports.UI.XRLabel supervisorDepartmentTypesLabel;
        private DevExpress.XtraReports.UI.XRLabel indicatorsLabel;
    }
}
