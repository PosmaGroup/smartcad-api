using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Globalization;
using System.Drawing.Printing;
using System.Threading;
using System.Diagnostics;

using Smartmatic.SmartCad.Service;
using System.Reflection;

using System.Xml;
using System.Xml.Xsl;
using System.Net;
using System.ServiceModel;
using Microsoft.Win32;
using DevExpress.XtraBars;
using DevExpress.Data.Filtering;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using System.Text.RegularExpressions;
using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadControls.SyncBoxes;
using SmartCadCore.ClientData;
using SmartCadCore.Enums;
using SmartCadControls.Controls;
using SmartCadGuiCommon;
using SmartCadGuiCommon.Services;
using DevExpress.XtraLayout;
using SmartCadGuiCommon.Util;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using SmartCadControls;
using SmartCadControls.Filters;
using NAudio.Wave;
using SmartCadFirstLevel.Gui.SyncBoxes;
using SmartCadGuiCommon.Controls;
using SmartCadGuiCommon.Classes;
using SmartCadGui.Classes;
using SmartCadGuiCommon.Enums;

namespace SmartCadGuiCommon
{
    public partial class DefaultFrontClientFormDevX : XtraForm
    {
        #region Fields

        private ServerServiceClient serverServiceClient;
        private PhoneReportClientData globalPhoneReport;
        private OperatorStatusClientData defaultReadyStatus = null;
        private OperatorStatusClientData defaultBusyStatus = null;
        private OperatorStatusClientData defaultAbsentStatus = null;
        private bool isActiveCall = false;
        private bool incidentAdded = false;
        private bool callEntering = false;
        private ImageList activeStepImages;
        private ImageList inactiveStepImages;
        private FilterBase incidentsCurrentFilter;
        private BarButtonItem barButtonItem;
        private SkinEngine skinEngine;
        private const int GLOBAL_TIMER_PERIOD = 10000;
        public System.Threading.Timer globalTimer;
        private System.Threading.Timer answersUpdatedTimer;
        private EventArgs answersUpdatedTimerEventArgs;
        private Dictionary<int, IncidentTypeClientData> globalIncidentTypes;
        private Dictionary<int,DepartmentTypeClientData> gobalDepartmentTypes;
        private Dictionary<string, IncidentNotificationPriorityClientData> globalNotificationPriorities;
        private IList globalIncidents;
        private Dictionary<string, ApplicationPreferenceClientData> globalApplicationPreference;
        private System.Threading.Timer timerCheckServer;
        private FrontClientStateEnum frontClientState = FrontClientStateEnum.None;
        private object syncAssociatedInfo;
        private object syncTelephonyAction = new object();
        private IList updatedAnswers = new ArrayList();
        private XslCompiledTransform xslCompiledTransform;
        private ConfigurationClientData configurationClient;
        private string incomingCallTelephoneNumber;
        private GridControlSynBox incidentSyncBox;
        public bool doNotDisturbMessageShow;
        private int selectedIncidentCode = -1;
        private bool pickedUpCall = false;
        private bool hasDeletedQuestion = false;
        private System.Threading.Timer refreshIncidentReport;
        private bool operatorStatusFromApp = false;
        public string password;
        private object IEFooter;
        private string objectValue;
        private const string SourceDirectory = "Software\\Microsoft\\Internet Explorer\\PageSetup";
        private bool searchByText = false;
        private int beginLimit = 0;
        private Dictionary<int, IncidentTypeClientData> incidentTypeActionsUpdate;
        private Dictionary<int, IncidentTypeClientData> incidentTypeActionDelete;
        private Dictionary<int, IncidentTypeClientData> incidentTypeActionAdd;
        private Dictionary<int, QuestionClientData> questionActionsUpdate;
        private Dictionary<int, QuestionClientData> questionActionDelete;
        private Dictionary<int, QuestionClientData> questionActionAdd;
        private int counterToCompleteMinute;
        private const int ABSENCE_CHECK_CYCLE = 6;
        private double availableTimeInStatusWithPause = 0;

        private List<int> remoteControlOperators = new List<int>();
        private PublicLineClientData publicLineClient = null;
		private TelephoneStatusType telephoneStatus;
		BarButtonItem readyMenuItem = null;
		BarButtonItem busyMenuItem = null;
		private bool pressedButtonsFilter = false;
        private FrontClientFormDevX parentForm;
        System.Threading.Timer progressTimer;
        System.Threading.Timer checkStatusTimer;
		private bool stopped;
		private TimeSpan progress;
		private object sync = new object();
		ArrayList IncidentChanges = new ArrayList();
		ArrayList QuestionChanges = new ArrayList();
		private ArrayList DepartmentTypeChanges = new ArrayList();
        private string prevString = string.Empty;
        private bool prevStringFound = false;
        private bool changeRecommended = false;
        private bool deleteRecommended = false; 
        SoftPhoneForm popUpDialer;
        private bool isCallingBack = false;

		#endregion

        #region Properties

        public bool FinalizeEnabled 
		{
			get
			{
				return barButtonItemFinalize.Enabled;
			}
			set
			{
				barButtonItemFinalize.Enabled = value;
			}
		}

		public TelephoneStatusType TelephoneStatus
		{
			get
			{
				return telephoneStatus;
			}
			set
			{
				telephoneStatus = value;
				switch (telephoneStatus)
				{
					case TelephoneStatusType.None:
						this.barButtonItemTelephone.LargeGlyph = ResourceLoader.GetImage("TelephoneNone");
						barButtonItemTelephone.Caption = "";
						((FrontClientFormDevX)ParentForm).barButtonItemTelephone.Caption = "";
						this.barButtonItemTelephone.Enabled = false;
                        ((FrontClientFormDevX)ParentForm).barButtonItemVirtualCall.Enabled = true;
                       // ((FrontClientFormDevX)ParentForm).barButtonItemParkCall.Enabled = false;
						break;
					case TelephoneStatusType.Incoming:
                        Thread animate = new Thread(new ThreadStart(AnimateRingingImages));
                        animate.Name = "Animated ringing";
                        animate.Start();
						barButtonItemTelephone.Caption = ResourceLoader.GetString2("AnswerPhone");
						((FrontClientFormDevX)ParentForm).barButtonItemTelephone.Caption = ResourceLoader.GetString2("AnswerPhone");
						this.barButtonItemTelephone.Enabled = true;
                        //((FrontClientFormDevX)ParentForm).barButtonItemParkCall.Enabled = false;
						break;
					case TelephoneStatusType.Active:
						barButtonItemTelephone.LargeGlyph = ResourceLoader.GetImage("TelephoneActive");
						barButtonItemTelephone.Caption = ResourceLoader.GetString2("HangUp");
						((FrontClientFormDevX)ParentForm).barButtonItemTelephone.Caption = ResourceLoader.GetString2("HangUp");
                        CheckEnableParkCall();
						this.barButtonItemTelephone.Enabled = true;
						break;
					case TelephoneStatusType.Dropped:
						barButtonItemTelephone.LargeGlyph = ResourceLoader.GetImage("TelephoneDropped");
						barButtonItemTelephone.Caption = "";
						((FrontClientFormDevX)ParentForm).barButtonItemTelephone.Caption = "";
						this.barButtonItemTelephone.Enabled = true;
                        //((FrontClientFormDevX)ParentForm).barButtonItemParkCall.Enabled = false;
						break;
					case TelephoneStatusType.Ended:
						barButtonItemTelephone.LargeGlyph = ResourceLoader.GetImage("TelephoneEnded");
						barButtonItemTelephone.Caption = "";
						((FrontClientFormDevX)ParentForm).barButtonItemTelephone.Caption = "";
						this.barButtonItemTelephone.Enabled = true;
                        //((FrontClientFormDevX)ParentForm).barButtonItemParkCall.Enabled = false;
						break;
				}
			}
		}

        private void CheckEnableParkCall()
        {
            //if (this.FrontClientState == FrontClientStateEnum.UpdatingIncident &&
            //    !((FrontClientFormDevX)ParentForm).barButtonItemVirtualCall.Enabled &&
            //    !incidentClosed)
            //{
            //    //Commented for the release of SmartCAD 3.2, should be added when this function works.
            //    //Only has been added to the Genesys DLL.
            //    //((FrontClientFormDevX)ParentForm).barButtonItemParkCall.Enabled = TelephoneStatus == TelephoneStatusType.Active;
            //}
        }

        public FrontClientStateEnum FrontClientState
        {
            get
            {
                return this.frontClientState;
            }
            set
            {
                this.frontClientState = value;
                if (value == FrontClientStateEnum.WaitingForIncident)
                    SetFrontClientWaitingForCallState();
                else if(value == FrontClientStateEnum.RegisteringIncident)
                    SetFrontClientRegisteringCallState();
                else if(value == FrontClientStateEnum.UpdatingIncident)
                    SetFrontClientUpdatingCallState();
            }
        }

        public bool IsActiveCall
        {
            get
            {
                return this.isActiveCall;
            }
            set
            {
                this.isActiveCall = value;
            }
        }

        public IList GlobalIncidents
        {
            get
            {
                return globalIncidents;
            }
            set
            {
                globalIncidents = value;
            }
        }

        public PhoneReportClientData GlobalPhoneReport
        {
            get
            {
                return globalPhoneReport;
            }
            set
            { 
                globalPhoneReport = value;
            }
        }

        object syncGlobalIncidentTypes = new object();
        public Dictionary<int, IncidentTypeClientData> GlobalIncidentTypes
        {
            get
            {
                lock (syncGlobalIncidentTypes)
                {
                    return globalIncidentTypes;
                }
            }
            set
            {
                lock (sync)
                {
                    globalIncidentTypes = value;

                    foreach (IncidentTypeClientData itcd in incidentTypeActionAdd.Values)
                    {
                        if (globalIncidentTypes.ContainsKey(itcd.Code) == false)
                            globalIncidentTypes.Add(itcd.Code, itcd);
                    }
                    incidentTypeActionAdd.Clear();

                    foreach (IncidentTypeClientData itcd in incidentTypeActionsUpdate.Values)
                    {
                        if (globalIncidentTypes.ContainsKey(itcd.Code) == true)
                            globalIncidentTypes[itcd.Code] = itcd;
                    }
                    incidentTypeActionsUpdate.Clear();

                    foreach (IncidentTypeClientData itcd in incidentTypeActionDelete.Values)
                    {
                        if (globalIncidentTypes.ContainsKey(itcd.Code) == true)
                            globalIncidentTypes.Remove(itcd.Code);
                    }
                    incidentTypeActionDelete.Clear();

                    foreach (QuestionClientData qcd in questionActionAdd.Values)
                    {
                        AddQuestion(qcd);
                    }
                    questionActionAdd.Clear();

                    foreach (QuestionClientData qcd in questionActionsUpdate.Values)
                    {
                        UpdateQuestion(qcd);
                    }
                    questionActionsUpdate.Clear();

                    foreach (QuestionClientData qcd in questionActionDelete.Values)
                    {
                        DeleteQuestion(qcd);
                    }
                    questionActionDelete.Clear();

                    questionsControl.GlobalIncidentTypes = globalIncidentTypes;
                }
            }
        }

        public Dictionary<int, DepartmentTypeClientData> GlobalDepartmentTypes
        {
            get
            {
                return this.gobalDepartmentTypes;
            }
            set
            {
                this.gobalDepartmentTypes = value;
                this.departmentsInvolvedControl.departmentTypesControl.GlobalDepartmentTypes = gobalDepartmentTypes;
            }
        }

        public Dictionary<string, IncidentNotificationPriorityClientData> GlobalNotificationPriorities
        {
            get
            {
                return this.globalNotificationPriorities;
            }
            set
            {
                this.globalNotificationPriorities = value;
                this.departmentsInvolvedControl.departmentTypesControl.GlobalNotificationPriorities = globalNotificationPriorities;
            }
        }

        public Dictionary<string,ApplicationPreferenceClientData> GlobalApplicationPreference
        {
            get
            {
                return globalApplicationPreference;
            }
            set
            {
                globalApplicationPreference = value;
            }
        }

        public System.Threading.Timer TimerCheckServer
        {
            get
            {
                return timerCheckServer;
            }
            set
            {
                timerCheckServer = value;
            }
        }

        public ServerServiceClient ServerServiceClient
        {
            get
            {
                return serverServiceClient;
            }
            set
            {
                serverServiceClient = value;
            }
        }
        
        internal CallInformationControl CallReceiverControl
        {
            get
            {
                return this.callInformationControl;
            }
        }

        internal QuestionsControlDevX QuestionsControl
        {
            get
            {
                return this.questionsControl;
            }
        }

        public ConfigurationClientData ConfigurationClient
        {
            get
            {
                return configurationClient;
            }
            set
            {
                configurationClient = value;
            }
        }

        public void SetPassword(string passwd)
        {
            this.password = passwd;
        }

        #endregion

        #region Constructors

		public DefaultFrontClientFormDevX(FrontClientFormDevX form)
        {
            InitializeComponent();

			progressTimer = ApplicationUtil.RecurrentExecuteMethod(
				new SimpleDelegate(
				delegate
				{
					timer_Tick(null);
				}), null, null, Timeout.Infinite, Timeout.Infinite);

            checkStatusTimer = ApplicationUtil.RecurrentExecuteMethod(
                new SimpleDelegate(
                delegate
                {
                    IsNewStatusAllow();
                }), null, null, 60000, 60000); 

			this.parentForm = form;
            syncAssociatedInfo = new object();
            refreshIncidentReport = new System.Threading.Timer(new TimerCallback(OnRefreshIncidentReportTimer));

            ImageList imageList = new ImageList();
            imageList.Images.Add(ResourceLoader.GetImage("IconTreeFirstLevel"));
            //gridViewExPreviousIncidents.Images = imageList;
			
             //To load the images for the incoming call animation.
            animateImages.Add(ResourceLoader.GetImage("TelephoneIncoming1"));
            animateImages.Add(ResourceLoader.GetImage("TelephoneIncoming2"));
            animateImages.Add(ResourceLoader.GetImage("TelephoneIncoming3"));
            animateImages.Add(ResourceLoader.GetImage("TelephoneIncoming4"));
            animateImages.Add(ResourceLoader.GetImage("TelephoneIncoming3"));
            animateImages.Add(ResourceLoader.GetImage("TelephoneIncoming2"));

            incidentTypeActionsUpdate = new Dictionary<int, IncidentTypeClientData>();
            incidentTypeActionDelete = new Dictionary<int, IncidentTypeClientData>();
            incidentTypeActionAdd = new Dictionary<int, IncidentTypeClientData>();
            questionActionsUpdate = new Dictionary<int, QuestionClientData>();
            questionActionDelete = new Dictionary<int, QuestionClientData>();
            questionActionAdd = new Dictionary<int, QuestionClientData>();

            this.parentForm.FrontClientFormDevXCommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(serverService_CommittedChanges);


            XmlReaderSettings xmlReaderSettings = new XmlReaderSettings();
            xmlReaderSettings.ProhibitDtd = false;
            xslCompiledTransform = new XslCompiledTransform();
            xslCompiledTransform.Load(XmlReader.Create(SmartCadConfiguration.IncidentXslt, xmlReaderSettings));

            configurationClient = ServerServiceClient.GetInstance().GetConfiguration();

			LoadLanguages();            
        }

		#endregion

		private void LoadLanguages()
		{
			barButtonItemAddToIncident.Caption = ResourceLoader.GetString2("AddToIncident");
			barButtonItemFinalize.Caption = ResourceLoader.GetString2("Finalize");
			barButtonItemPrint.Caption = ResourceLoader.GetString2("Print");
			barButtonItemRefresh.Caption = ResourceLoader.GetString2("Refresh");
			barButtonItemSave.Caption = ResourceLoader.GetString2("Save");
			ribbonPageGroupUser.Text = ResourceLoader.GetString2("Status");
			ribbonPageGroupGeneralOptions.Text = ResourceLoader.GetString2("GeneralOptions");
            dockPanel1.Text = ResourceLoader.GetString2("StoredIncidents");
			ribbonPageGroupCallInfo.Text = ResourceLoader.GetString2("CallInfo");
			layoutControlGroupPreviousIncidents.Text = ResourceLoader.GetString2("IncidentList");
			layoutControlGroupCallsAndDispatchs.Text = ResourceLoader.GetString2("Information");
            this.Text = ResourceLoader.GetString2("DefaultFrontClientFormDevX");
			ribbonPage1.Text = ResourceLoader.GetString2("FirstLevel");
			this.barStaticItemChonometer.Caption = ResourceLoader.GetString2("Chronometer");
			this.barStaticItemTimeBar.Caption = ResourceLoader.GetString2("TimeBar");
            this.buttonExSameNumberIncidents.Text = ResourceLoader.GetString2("SameNumber");
            this.buttonExSameNumberIncidents.ToolTip = ResourceLoader.GetString2("SameNumberTooltip");
            this.buttonExOpenIncidentsFilter.Text = ResourceLoader.GetString2("Opens");
            this.buttonExOpenIncidentsFilter.ToolTip = ResourceLoader.GetString2("OpensTooltip");
            this.buttonExSimilarIncidentsFilter.Text = ResourceLoader.GetString2("Similar");
            this.buttonExSimilarIncidentsFilter.ToolTip = ResourceLoader.GetString2("SimilarTooltip");
            this.barButtonItemCallBack.Caption = ResourceLoader.GetString2("CallBack");

		}

		private void FrontClientForm_Load(object sender, EventArgs e)
        {
            TelephonyServiceClient.GetInstance().TelephonyAction += new EventHandler<TelephonyActionEventArgs>(FrontClientForm_TelephonyAction);

            SetSkin();

            this.callInformationControl.defaultFrontClientFormDevX = this;

			//Subscribing to events needed.
            departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.KeyDown += new KeyEventHandler(dataGridDepartmentTypes_KeyDown);

            questionsControl.IncidentTypesSelected += new EventHandler<EventArgs>(questionsControl_IncidentTypeSelected);
            callInformationControl.CallPickedUp += new EventHandler<EventArgs>(callReceiverControl1_CallPickedUp);
            callInformationControl.CallHangedUp += new EventHandler<EventArgs>(callReceiverControl1_CallHangedUp);
            questionsControl.AnswerUpdated += new EventHandler<EventArgs>(questionsControl_AnswerUpdated);
            this.departmentsInvolvedControl.departmentTypesControl.DepartmentTypesSelected += new EventHandler<EventArgs>(departmentTypesControl2_DepartmentTypesSelected);
            questionsControl.Enter += new EventHandler(questionsControl_Enter);
            this.departmentsInvolvedControl.Enter += new EventHandler(departmentsInvolvedControl_Enter);
          //  this.departmentsInvolvedControl.Enter += new EventHandler(departmentTypesControl2_Enter);
            callInformationControl.Enter +=new EventHandler(callReceiverControl1_Enter);
            callInformationControl.TextUpdated += new EventHandler<EventArgs>(callInformationControl_TextUpdated);
            proceduresControl.InternalControlClick += new EventHandler(tabControlProcedures_Click);
            KeyDown += new KeyEventHandler(FrontClientForm_KeyDown);
            repositoryItemProgressBar1.Step = 1;
            repositoryItemProgressBar1.Maximum = int.Parse(GlobalApplicationPreference["AlarmTimeLimit"].Value);
            beginLimit = repositoryItemProgressBar1.Maximum / 2;
            proceduresControl.ProceduresControlPreviewKeyDown += new PreviewKeyDownEventHandler(proceduresControl_ProceduresControlPreviewKeyDown);
            questionAnswerTextControl.QuestionAnswerTextControlPreviewKeyDown += new PreviewKeyDownEventHandler(questionAnswerTextControl_QuestionAnswerTextControlPreviewKeyDown);
            
            //Filling needed information...
            LoadStepImages();
            CreateOperatorStatusMenu();
            
            //Setting up the FrontClient and Nortel Configuration...
            this.FrontClientState = FrontClientStateEnum.WaitingForIncident;

			ArrayList parameters = new ArrayList();
            parameters.Add("OPEN");
            incidentsCurrentFilter = new IncidentsFilter(null, new ArrayList());

            buttonExOpenIncidentsFilter_Click(buttonExOpenIncidentsFilter, EventArgs.Empty);

            SetToolStripOperatorStatus(defaultReadyStatus);

			gridControlExPreviousIncidents.Type = typeof(GridIncidentData);
			incidentSyncBox = new GridControlSynBox(gridControlExPreviousIncidents);
			incidentSyncBox.Sync(globalIncidents);
			gridControlExPreviousIncidents.ViewTotalRows = true;
			gridControlExPreviousIncidents.EnableAutoFilter = true;
			gridViewExPreviousIncidents.ViewTotalRows = true;
			gridControlExPreviousIncidents.AllowDrop = false;


            answersUpdatedTimer = new System.Threading.Timer(new TimerCallback(OnAnswerUpdateTimer));

            CleanForm();

            questionsControl.removeIncident += new QuestionsControlDevX.RemoveIncident(questionsControl_removeIncident);
            ApplicationServerService.GisAction += new EventHandler<GisActionEventArgs>(FrontClientForm_GisAction);
			departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.ClearSelection();
			questionsControl.clientMode = FrontClientMode.FrontClient;
			barCheckItemIncompleteCall.Caption = ResourceLoader.GetString2("UnfinishedCall");

            questionsControl.listBoxIncidentTypes.KeyPress +=new KeyPressEventHandler(listBoxEx_KeyPress);
            questionsControl.listBoxSelectedIncidentTypes.KeyPress +=new KeyPressEventHandler(listBoxEx_KeyPress);

            //TelephonyServiceClient.GetInstance().SetIsReady(true);

            if (string.IsNullOrEmpty(ServerServiceClient.GetInstance().Configuration.Extension) == true)
            {
                MessageForm.Show(ResourceLoader.GetString2("ExtensionNotProperlyConfigured"), MessageFormType.Warning);
            }
        }

        void departmentsInvolvedControl_Enter(object sender, EventArgs e)
        {
            if (this.FrontClientState != FrontClientStateEnum.WaitingForIncident)
                ChangeCurrentStep(3, false);
        }

        private void questionsControl_removeIncident(IList selectedIncidentTypes)
        {

            IncidentTypeDepartmentTypeClientData department;
            IncidentTypeClientData incidentType;
            ArrayList updateIncident = new ArrayList();
            foreach (IncidentTypeClientData incident in selectedIncidentTypes)
                if (GlobalPhoneReport.IncidentTypesCodes!= null)
					GlobalPhoneReport.IncidentTypesCodes.Remove(incident.Code);

            for (int j = 0; j < departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.RowCount; j++)
				if (departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.GetRowCellValue(j, "Priority") == null)
					departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.SetRowCellValue(j, "CheckDepartmentType", false);
            
			if (globalPhoneReport.IncidentTypesCodes != null)
            {
                foreach (int incidentCode in GlobalPhoneReport.IncidentTypesCodes)
                {
                    for (int k = 0; k < this.QuestionsControl.SelectedIncidentTypes.Count; k++)
                    {
                        incidentType = (IncidentTypeClientData)this.QuestionsControl.SelectedIncidentTypes[k];
                        if (incidentType.Code == incidentCode)
                            updateIncident.Add(incidentType);
                    }
                }
            }
            foreach (IncidentTypeClientData incident in updateIncident)
            {
                for (int i = 0; i < incident.IncidentTypeDepartmentTypeClients.Count; i++)
                {
                    department = (IncidentTypeDepartmentTypeClientData)incident.IncidentTypeDepartmentTypeClients[i];
                    for (int j = 0; j < departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.RowCount; j++)
                        if (department.DepartmentType.Code == (((GridDepartmentTypeData)((IList)departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.DataSource)[j]).Tag as DepartmentTypeClientData).Code)
                            departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.SetRowCellValue(j,
								departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.Columns[0], true);
                }
            }
            //departmentsInvolvedControl.departmentTypesControl.gridViewEx1.so.SortByColumn(1, ListSortDirection.Descending);            
            this.questionsControl.UpdateTextBoxSelectedIncidentTypes(updateIncident);
        }      

        private void OnRefreshIncidentReport()
        {
            refreshIncidentReport.Change(1000, Timeout.Infinite);
        }

        private void OnRefreshIncidentReportTimer(object state)
        {
            refreshIncidentReport.Change(Timeout.Infinite, Timeout.Infinite);

            ThreadPool.QueueUserWorkItem(delegate
            {
                try
                {
                    RefreshIncidentReport();
                }
                catch (Exception ex)
                {
                    SmartLogger.Print(ex);
                }
            });
        }

        private void OnAnswerUpdateTimer(object state)
        {
            answersUpdatedTimer.Change(Timeout.Infinite, Timeout.Infinite);

            if (answersUpdatedTimerEventArgs != null)
            {
                FormUtil.InvokeRequired(this, delegate
                {
                    questionAnswerTextControl.RichTextArea.Clear();
                    QuestionsNaturalLanguage.Write(callInformationControl, questionsControl.SelectedIncidentTypes, questionsControl.QuestionsWithAnswers, questionAnswerTextControl.RichTextArea);

                    proceduresControl.browserProcedures.Clear();
                    Procedures2NaturalLanguage.Write(questionsControl.QuestionsWithAnswers, proceduresControl.browserProcedures);

                    UpdateSimilarIncidentsFilter();
                });
            }
        }

        private void UpdateSimilarIncidentsFilter()
        {
            FormUtil.InvokeRequired(buttonExSimilarIncidentsFilter, delegate
            {
                buttonExSimilarIncidentsFilter.BackColor = SystemColors.Control;
            });
            if (buttonExSimilarIncidentsFilter.LookAndFeel.SkinName.Equals("Black"))
                buttonExSimilarIncidentsFilter_Click(buttonExSimilarIncidentsFilter, new UpdateSimilarIncidentsEventArgs());
        }


        System.Media.SoundPlayer sp = new System.Media.SoundPlayer(SmartCadConfiguration.RingingWav);
        
        public void FrontClientForm_TelephonyAction(object sender, TelephonyActionEventArgs e)
        {
            if (!isCallingBack)
            {
                lock (syncTelephonyAction)
                {
                    if (e is CallRingingEventArgs)
                    {
                        #region CallRingingEventArgs
                        //esto no puede pasar... hay que evitarlo. Bataan. AA
                        //if (FrontClientState != FrontClientStateEnum.WaitingForIncident)
                        //{
                        //    TelephonyServiceClient.GetInstance().SetIsReady(false);
                        //    return; 
                        //}
                        callEntering = true;
                        // ((FrontClientFormDevX)ParentForm).barButtonItemPhoneReboot.Enabled = false;
                        FormUtil.InvokeRequired(this, delegate
                        {
                            this.barButtonItemStatus.DropDownEnabled = false;
                            ((FrontClientFormDevX)ParentForm).barButtonItemVirtualCall.Enabled = false;
                            this.Focus();
                            Win32.SetForegroundWindow(this.Handle);

                        });
                        #endregion
                    }

                    FormUtil.InvokeRequired(this, delegate
                    {
                        if (e is TelephonyServerConnectionEventArgs)
                        {
                            #region TelephonyServerConnectionEventArgs
                            TelephonyServerConnectionEventArgs eventArgs = e as TelephonyServerConnectionEventArgs;
                            if (eventArgs.Connected == false)
                                parentForm.barButtonItemVirtualCall.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                            else
                            {
                                parentForm.barButtonItemVirtualCall.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                                //((FrontClientFormDevX)ParentForm).barButtonItemPhoneReboot.Enabled = true;
                            }
                            #endregion
                        }
                        if (e is CallRingingEventArgs)
                        {
                            #region CallRingingEventArgs
                            sp.PlayLooping();
                            callInformationControl.WhenIncomingCallEventArgs((CallRingingEventArgs)e);
                            this.incomingCallTelephoneNumber = ((CallRingingEventArgs)e).ANI;

                            #endregion
                        }
                        else if (e is CallEstablishedEventArgs)
                        {
                            #region CallEstablishedEventArgs
                            sp.Stop();
                            callInformationControl.WhenEstablishedCallEventArgs((CallEstablishedEventArgs)e);
                            pickedUpCall = true;

                            ApplicationServiceClient.Current(UserApplicationClientData.Map).SendEnableButton(true, ButtonType.AddPoint);
                            ApplicationServiceClient.Current(UserApplicationClientData.Map).SendToActivateLayer(ShapeType.Incident);

                            #endregion
                        }
                        else if (e is CallReleasedEventArgs)
                        {
                            #region CallReleasedEventArgs
                            callInformationControl.WhenEndingCallEventArgs(e);
                            if (pickedUpCall == true)
                            {
                                readyMenuItem.Enabled = false;
                                pickedUpCall = false;
                            }
                            callEntering = false;

                            Thread.Sleep(300);
                            TelephonyServiceClient.GetInstance().SetIsReady(false);
                            #endregion
                        }
                        else if (e is CallAbandonedEventArgs)
                        {
                            #region CallAbandonedEventArgs
                            callInformationControl.WhenEndingCallEventArgs(e);
                            if (pickedUpCall == true)
                            {
                                readyMenuItem.Enabled = false;
                                pickedUpCall = false;
                            }
                            else
                            {
                                sp.Stop();
                                if (FrontClientState == FrontClientStateEnum.WaitingForIncident)
                                {
                                    readyMenuItem.Enabled = true;
                                    SetToolStripOperatorStatus(OperatorStatusClientData.Absent);
                                    serverServiceClient.SetOperatorStatus(OperatorStatusClientData.Absent);
                                    ((FrontClientFormDevX)ParentForm).barButtonItemVirtualCall.Enabled = true;
                                }
                            }

                            callEntering = false;

                            Thread.Sleep(300);
                            TelephonyServiceClient.GetInstance().SetIsReady(false);
                            #endregion
                        }
                        else if (e is ReadyStatusChangedEventArgs)
                        {
                            #region ReadyStatusChangedEventArgs
                            if (operatorStatusFromApp == false || ((ReadyStatusChangedEventArgs)e).IsReady == false)
                            {
                                ReadyStatusChangedEventArgs args = (ReadyStatusChangedEventArgs)e;
                                OperatorStatusClientData operatorStatus = null;

                                OperatorStatusClientData currentOperatorStatus = (OperatorStatusClientData)barButtonItemStatus.Tag;

                                if ((args.IsReady == false && FrontClientState == FrontClientStateEnum.WaitingForIncident && currentOperatorStatus.Equals(defaultAbsentStatus)) == false)
                                {
                                    if (args.IsReady == true && FrontClientState != FrontClientStateEnum.WaitingForIncident)
                                    {
                                        TelephonyServiceClient.GetInstance().SetIsReady(false);
                                        return;
                                    }
                                    if ((e as ReadyStatusChangedEventArgs).NotReadyReasonCode == ReadyStatusChangedEventArgs.CallNotAnswered)
                                    {
                                        callEntering = true;
                                    }

                                    if (args.IsReady == false && FrontClientState == FrontClientStateEnum.WaitingForIncident && callEntering == false)
                                    {
                                        TelephonyServiceClient.GetInstance().SetIsReady(true);
                                        return;
                                    }

                                    if (FrontClientState == FrontClientStateEnum.WaitingForIncident)
                                    {
                                        if (args.IsReady == false && FrontClientState == FrontClientStateEnum.WaitingForIncident && callEntering == true)
                                            operatorStatus = defaultAbsentStatus;
                                        else if (args.IsReady == false)
                                            operatorStatus = defaultBusyStatus;
                                        else
                                            operatorStatus = defaultReadyStatus;

                                        SetToolStripOperatorStatus(operatorStatus);
                                        serverServiceClient.SetOperatorStatus(operatorStatus);
                                    }
                                }
                            }
                            else
                                operatorStatusFromApp = false;
                            #endregion
                        }
                        else if (e is DoNotDisturbChangedEventArgs)
                        {
                            #region DoNotDisturbChangedEventArgs
                            if (pickedUpCall == false && FrontClientState == FrontClientStateEnum.WaitingForIncident && doNotDisturbMessageShow == false)
                            {
                                OperatorStatusClientData operatorStatus = defaultAbsentStatus;

                                SetToolStripOperatorStatus(operatorStatus);
                                serverServiceClient.SetOperatorStatus(operatorStatus);
                                TelephonyServiceClient.GetInstance().SetIsReady(false);
                            }
                            #endregion
                        }
                        else if (e is TerminalUnRegisteredEventArgs)
                        {
                            #region TerminalUnRegisteredEventArgs

                            isRegistered = false;
                            if (pickedUpCall == false)
                            {
                                OperatorStatusClientData operatorStatus = defaultAbsentStatus;

                                SetToolStripOperatorStatus(operatorStatus);
                                serverServiceClient.SetOperatorStatus(operatorStatus);
                                // TelephonyServiceClient.GetInstance().SetIsReady(false);
                            }

                            #endregion
                        }
                        else if (e is TelephonyErrorEventArgs)
                        {
                           // MessageForm.Show(((TelephonyErrorEventArgs)e).ErrorMessage, new Exception());
                        }
                        else if (e is CallRecordedEventArgs)
                        {
                            #region CallRecorded
                            //SEND THE AUDIO FILE (IF EXISTS) TO THE SERVER.
                            new Thread(new ThreadStart(delegate
                            {
                                try
                                {
                                    FileInfo[] files = FindAudioFiles();
                                    if (files != null && files.Length > 0)
                                    {
                                        foreach (FileInfo filePath in files)
                                        {
                                            WaveFileReader audioFile = new WaveFileReader(filePath.FullName);
                                            FileServerServiceClient fileServerServiceClient = FileServerServiceClient.CreateInstance();
                                            fileServerServiceClient.SendFile(audioFile, ((CallRecordedEventArgs)e).PhoneReportCustomCode + ".wav");
                                            fileServerServiceClient.Close();
                                            audioFile.Close();
                                            audioFile.Dispose();
                                            filePath.Delete();
                                        }
                                    }
                                    files = null;
                                }
                                catch { }
                            })).Start();
                            #endregion
                        }
                    });
                }
            }
        }

        private bool isRegistered=true;
         

        private void FrontClientForm_GisAction(object sender, GisActionEventArgs e)
        {
            try
            {
                if (e is SendGeoPointEventArgs && (this.frontClientState == FrontClientStateEnum.RegisteringIncident || this.frontClientState == FrontClientStateEnum.UpdatingIncident))
                {
                    GeoPoint geoPoint = (e as SendGeoPointEventArgs).Point;

                    this.questionsControl.IncidentAddress.Lon = geoPoint.Lon;
                    this.questionsControl.IncidentAddress.Lat = geoPoint.Lat;
                    this.questionsControl.IncidentAddress.IsSynchronized = !(geoPoint.Lon == 0 && geoPoint.Lat == 0);

                    UpdateSimilarIncidentsFilter();
                }
                else if (e is VerifyStateFrontClientEventArgs && this.frontClientState == FrontClientStateEnum.RegisteringIncident)
                {
                    ApplicationServiceClient.Current(UserApplicationClientData.Map).SendToActivateLayer(ShapeType.Incident);
                    ApplicationServiceClient.Current(UserApplicationClientData.Map).SendEnableButton(true, ButtonType.AddPoint);
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private bool IsNewStatusAllow()
        {
            if (bool.Parse(GlobalApplicationPreference["CheckLogInMode"].Value) == true)
            {
                //counterToCompleteMinute++;
                OperatorStatusClientData selectedOperatorStatus = barButtonItemStatus.Tag as OperatorStatusClientData;
                if (selectedOperatorStatus.IsPause())
                {
                    availableTimeInStatusWithPause = serverServiceClient.CalculateAmountTimeAvailableByStatusWithPause(selectedOperatorStatus.CustomCode);
                }
                if (availableTimeInStatusWithPause < 0 && selectedOperatorStatus.IsPause() == true)
                {
                    counterToCompleteMinute = 0;
                    availableTimeInStatusWithPause = 0;
                    OperatorStatusClientData operatorStatus = OperatorStatusClientData.Absent;
                    SetToolStripOperatorStatus(operatorStatus);
                    serverServiceClient.SetOperatorStatus(operatorStatus);

                    checkStatusTimer.Change(10000, 10000);

                    return false;
                }
                try
                {
                    int waitPeriod = (int)(availableTimeInStatusWithPause / 2) * 1000 + 100;
                    checkStatusTimer.Change(waitPeriod, waitPeriod);
                }
                catch
                {
                    checkStatusTimer.Change(10000, 10000);
                }
            }

            return true;
        }

        //SHORTCUTS...
        void FrontClientForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control == true && !(e.Modifiers == (Keys.Control | Keys.Alt)))
            {
                if ((e.KeyValue == 49 || e.KeyCode == Keys.NumPad1) && 
					this.frontClientState != FrontClientStateEnum.WaitingForIncident)//GOES TO STEP ONE(1)...
                    ChangeCurrentStep(1, true);
                else if ((e.KeyValue == 50 || e.KeyCode == Keys.NumPad2) && 
					this.frontClientState != FrontClientStateEnum.WaitingForIncident)//GOES TO STEP TWO(2)...
                    ChangeCurrentStep(2, true);
                else if ((e.KeyValue == 51 || e.KeyCode == Keys.NumPad3) && 
					this.frontClientState != FrontClientStateEnum.WaitingForIncident)//GOES TO STEP THREE(3)...
                    ChangeCurrentStep(3, true);
                else if ((e.KeyValue == 52 || e.KeyCode == Keys.NumPad4) && 
					this.frontClientState != FrontClientStateEnum.WaitingForIncident)//GOES TO STEP FOUR(4)...
                    ChangeCurrentStep(4, true);
                else if (e.KeyCode == Keys.A && 
					this.frontClientState == FrontClientStateEnum.RegisteringIncident)//TO CHECK OR UNCHECK THE IS ANONIMOUS CALL CHECBOX
                {
                    if (this.callInformationControl.IsAnonimousCallActivated == true)
                        this.callInformationControl.IsAnonimousCallActivated = false;
                    else
                        this.callInformationControl.IsAnonimousCallActivated = true;
                }
                else if ((e.KeyCode == Keys.Enter || e.KeyCode == Keys.Space))
                {
                    if (FinalizeEnabled == true)
                        barButtonItemFinalize_ItemClick(null, null);
                    else
                        e.SuppressKeyPress = true;
                }
                else if ((e.KeyCode == Keys.L) && (
					this.barCheckItemIncompleteCall.Enabled == true))//TO CHECK OR UNCHECK THE INCOMPLETE CALL CHECKBOX...
                {
					if (this.barCheckItemIncompleteCall.Checked == true)
						barCheckItemIncompleteCall.Checked = false;
                    else
						this.barCheckItemIncompleteCall.Checked = true;
                }
            }
            else if (e.Shift == true && ((e.KeyCode == Keys.Enter || e.KeyCode == Keys.Space) &&
                (this.callEntering == true && IsActiveCall == true)))//ENDS THE REPORT...
            {
                barButtonItemTelephone_ItemClick(null, null);
            }
            else if (e.KeyCode == Keys.F1)//CALLS ONLINE HELP...
            {
                ((FrontClientFormDevX)ParentForm).barButtonItemHelp.PerformClick();
            }
            else if ((this.callEntering == true && IsActiveCall == false) && 
				(e.KeyCode == Keys.Enter || e.KeyCode == Keys.Space))//PICKS THE CALL UP...
            {
				barButtonItemTelephone_ItemClick(null, null);
            }
            else if (e.KeyCode == Keys.F6)//SETS THE SIMILAR INCIDENT FILTER...
            {
                if(FrontClientState != FrontClientStateEnum.WaitingForIncident)
                    buttonExSimilarIncidentsFilter_Click(buttonExSimilarIncidentsFilter, null);
            }
            else if (e.KeyCode == Keys.F7)//SETS THE SAME NUMBER INCIDENT FILTER...
            {
                if (FrontClientState != FrontClientStateEnum.WaitingForIncident)
                    buttonExSameNumberIncidents_Click(buttonExSameNumberIncidents, null);
            }
            else if (e.KeyCode == Keys.Enter) 
            {
                if (this.questionsControl.ContainsFocus && 
					this.questionsControl.Active && 
					!this.questionsControl.IsPanelIncidentTypeActive)
                {
                    this.questionsControl.SetNextQuestion();
                }
            }
            else if (e.KeyCode == Keys.F8)//SETS THE OPEN INCIDENTS FILTER...
            {
                if (FrontClientState != FrontClientStateEnum.WaitingForIncident)
                    buttonExOpenIncidentsFilter_Click(buttonExOpenIncidentsFilter, null);
            }
            else if (e.KeyCode == Keys.F9)//CREATE A NEW INCIDENT WITH A NEW PHONE REPORTS...
            {
                if (FrontClientState != FrontClientStateEnum.WaitingForIncident)
					barButtonItemCreateNewIncident_ItemClick(null, null);
            }
            else if (e.KeyCode == Keys.F12)//ADDS THE PHONE REPORT TO A SELECTED INCIDENT...
            {
                if (FrontClientState == FrontClientStateEnum.RegisteringIncident)
                    barButtonItemAddToIncident_ItemClick(null, null);
            }
            else if (sender == null && e.Alt == true && !(e.Modifiers == (Keys.Control | Keys.Alt))
               && e.KeyCode == Keys.F4 && this.Disposing == false)
            {
                this.Close();
            }        
        }

        void tabControlProcedures_Click(object sender, EventArgs e)
        {
            if (this.FrontClientState != FrontClientStateEnum.WaitingForIncident)
                ChangeCurrentStep(4, false);
        }

        public void callReceiverControl1_Enter(object sender, EventArgs e)
        {
            if (this.FrontClientState != FrontClientStateEnum.WaitingForIncident)
                ChangeCurrentStep(1, false);
        }
        
        //void departmentTypesControl2_Enter(object sender, EventArgs e)
        //{
        //    if (this.FrontClientState != FrontClientStateEnum.WaitingForCall)
        //        ChangeCurrentStep(3, false);
        //}

        void questionsControl_Enter(object sender, EventArgs e)
        {
            if (this.FrontClientState != FrontClientStateEnum.WaitingForIncident)
                ChangeCurrentStep(2, false);
        }

        private List<Image> animateImages = new List<Image>();

        private void AnimateRingingImages()
        {
            int i = 0;
            while (TelephoneStatus == TelephoneStatusType.Incoming)
            {
                FormUtil.InvokeRequired(this, delegate
                {
                    barButtonItemTelephone.LargeGlyph = animateImages[i];
                });
                i++;
                i = i % animateImages.Count;
                Thread.Sleep(100);
            }
            TelephoneStatus = telephoneStatus;
        }

        private void LoadStepImages()
        {
            this.inactiveStepImages = new ImageList();
            this.activeStepImages = new ImageList();
            string imageName = "$Image.";
            for (int i = 1; i < 5; i++)
            {
                Image image = ResourceLoader.GetImage(imageName + i + "Encendido");
                if (activeStepImages.ImageSize.Height != image.Size.Height ||
                    activeStepImages.ImageSize.Width != image.Size.Width)
                    activeStepImages.ImageSize = image.Size;
                activeStepImages.Images.Add(image);
            }
            for (int i = 1; i < 5; i++)
            {
                Image image = ResourceLoader.GetImage(imageName + i + "Apagado");
                if (inactiveStepImages.ImageSize.Height != image.Size.Height ||
                    inactiveStepImages.ImageSize.Width != image.Size.Width)
                    inactiveStepImages.ImageSize = image.Size;
                inactiveStepImages.Images.Add(image);
            }
        }

        void departmentTypesControl2_DepartmentTypesSelected(object sender, EventArgs e)
        {
            DepartmentTypesControl control = (DepartmentTypesControl)sender;
            this.GlobalPhoneReport.ReportBaseDepartmentTypesClient = control.SelectedDepartmentTypesWithPriority;
        }

        void callInformationControl_TextUpdated(object sender, EventArgs e)
        {
            answersUpdatedTimerEventArgs = e;
            answersUpdatedTimer.Change(1000, Timeout.Infinite);
        }

        void questionsControl_AnswerUpdated(object sender, EventArgs e)
        {
            if (FrontClientState == FrontClientStateEnum.UpdatingIncident)
            {
                updatedAnswers = new ArrayList(questionsControl.QuestionsWithAnswers);
                foreach (ReportAnswerClientData phoneReportAnswer in updatedAnswers)
                    phoneReportAnswer.ReportCode = GlobalPhoneReport.Code;
            }
            else
            {
                this.GlobalPhoneReport.Answers = new ArrayList(questionsControl.QuestionsWithAnswers);
                foreach (ReportAnswerClientData phoneReportAnswer in GlobalPhoneReport.Answers)
                    phoneReportAnswer.ReportCode = GlobalPhoneReport.Code;
            }
            answersUpdatedTimerEventArgs = e;
            answersUpdatedTimer.Change(1000, Timeout.Infinite);
        }

        //returns true if there are invalid questions...
        private bool ValidateQuestions(RequirementTypeClientEnum requirementType)
        {
            bool toReturn = false;
            if (GlobalPhoneReport.IncidentTypesCodes != null)
            {
                int index = 0;
                while (toReturn != true && index < GlobalPhoneReport.IncidentTypesCodes.Count)
                {
                    IncidentTypeClientData incidentType = globalIncidentTypes[(int)GlobalPhoneReport.IncidentTypesCodes[index]];
                    foreach (IncidentTypeQuestionClientData incidentQuestion in incidentType.IncidentTypeQuestions)
                    {
                        if (incidentQuestion.Question != null)
                        {
                            if (incidentQuestion.Question.RequirementType == requirementType)
                            {
                                int questionIndex = 0;
                                bool found = false;
                                while ((questionIndex < this.questionsControl.QuestionsWithAnswers.Count) && (found != true))
                                {
                                    if (((ReportAnswerClientData)this.questionsControl.QuestionsWithAnswers[questionIndex]).QuestionCode.Equals(incidentQuestion.Question.Code) == true)
                                        found = true;
                                    questionIndex++;
                                }
                                if (found != true)
                                    toReturn = true;
                            }
                        }
                    }
                    index++;
                }
            }
            return toReturn;
        }

        private bool ValidateRecommendedQuestions()
        {
            return ValidateQuestions(RequirementTypeClientEnum.Recommended);
        }

        private bool ValidateRequiredQuestions()
        {
            return ValidateQuestions(RequirementTypeClientEnum.Required);
        }

        private bool ValidateIncidentTypes()
        {
            if (GlobalPhoneReport.IncidentTypesCodes != null && GlobalPhoneReport.IncidentTypesCodes.Count > 0)
                return false;
            else
                return true;
        }

        private bool ValidateDepartmentTypes()
        {
            bool toReturn = false;
            if (GlobalPhoneReport.ReportBaseDepartmentTypesClient != null)
            {
                int index = 0;
                while (toReturn != true && index < GlobalPhoneReport.ReportBaseDepartmentTypesClient.Count)
                {
                    ReportBaseDepartmentTypeClientData phoneReportDepartmentType =
                        GlobalPhoneReport.ReportBaseDepartmentTypesClient[index] as ReportBaseDepartmentTypeClientData;
                    if (phoneReportDepartmentType.PriorityCode == 0)
                    {
                        toReturn = true;
                    }
                    index++;
                }
            }
            else
            {
                GlobalPhoneReport.ReportBaseDepartmentTypesClient = new ArrayList();
            }
            return toReturn;
        }

        private bool ValidatePhoneReportCaller()
        {
            bool toReturn = false;
            if (callInformationControl.IsAnonimousCallActivated != true)
            {
                if (callInformationControl.LineName.Trim() == "")
                {
                    toReturn = true;
                }
            }
            return toReturn;
        }

        private void UpdateQuestionsControl(ArrayList Changes)
        {
            if ((Changes != null) && (Changes.Count > 0))
            {
                if (Changes[0] is IncidentTypeClientData)
                {
                    this.questionsControl.UpdateIncidentTClientData(Changes);
                }
                else if (Changes[0] is QuestionClientData)
                {
                    this.questionsControl.UpdateIncidentQuestions(Changes);
                }
            }
        }
        private void UpdatedDepartmentInvolvedControl()
        {            
            for (int i = 0; i < this.departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.RowCount; i++)
            {
                if ((this.departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.GetRowCellValue(i,"Prioridad")== null)||
					(this.departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.GetRowCellValue(i, "Prioridad").ToString() 
                        == string.Empty))
                {
                    this.departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.SetRowCellValue(i," ",false);
                }
            }
        }
        private bool popUpShow = true;
        private bool ValidateInterface()
        {
            bool toReturn = true;
            
            if (ValidateChangedIncidentype() == false)
            {
                if (ValidateIncidentTypes() == false)
                {
                    DialogResult result = MessageForm.Show(ResourceLoader.GetString2("DeletedOrModifiedIncidentTipes"), MessageFormType.Question);
                    if (result == DialogResult.No)
                    {
                        popUpShow = false;
                        this.UpdateQuestionsControl(IncidentChanges);
                        UpdatedDepartmentInvolvedControl();
                        this.departmentsInvolvedControl.departmentTypesControl.IncidentTypes = IncidentChanges;
                        IncidentChanges.Clear();                      
                        toReturn = false;
                        this.questionsControl.SetFrontClientChangedIncidentTypeState();
						//ChangeCurrentStep(2, true);
						this.questionsControl.Focus();
					}
                }
                else
                {
                    MessageForm.Show(ResourceLoader.GetString2("DeletedIncidentTypes"), MessageFormType.Information);
                    toReturn = false;
                    this.questionsControl.SetFrontClientChangedIncidentTypeState();
					//ChangeCurrentStep(2, true);
					this.questionsControl.Focus();
				}
            }
            else if (ValidateChangedQuestions() == false)
            {
                if (hasDeletedQuestion == true)
                {
                    DialogResult result = DialogResult.None;
                    if(deleteRecommended == true)
                        result = MessageForm.Show(ResourceLoader.GetString2("DeletedRequiredQuestionsToContinueFirstLevel"), MessageFormType.Information);
                    else
                        result = MessageForm.Show(ResourceLoader.GetString2("DeletedNotRequiredQuestionsToContinueFirstLevel"), MessageFormType.Information);

                    deleteRecommended = false;
                }
                else
                {
                    DialogResult result = DialogResult.None;
                    if(changeRecommended == true)
                        result = MessageForm.Show(ResourceLoader.GetString2("UpdatedRequiredQuestionsWishToContinueFirstLevel"), MessageFormType.Question);
                    else
                        result = MessageForm.Show(ResourceLoader.GetString2("UpdatedNotRequiredQuestionsWishToContinueFirstLevel"), MessageFormType.Question);
                    
                    if (result == DialogResult.Yes)
                    {
                        this.UpdateQuestionsControl(QuestionChanges);
                        QuestionChanges.Clear();
                        toReturn = false;
                        //ChangeCurrentStep(2, true);
                        this.questionsControl.Focus();
                    }
                    changeRecommended = false;
                }                
            }
            else if (ValidateRecommendedChangedQuestions() == false)
            {
                //DialogResult result = MessageForm.Show(ResourceLoader.GetString2("UpdatedNotRequiredQuestionsWishToContinueFirstLevel"), MessageFormType.Question);
                DialogResult result = DialogResult.None;
                if (changeRecommended == true)
                    result = MessageForm.Show(ResourceLoader.GetString2("UpdatedRequiredQuestionsWishToContinueFirstLevel"), MessageFormType.Question);
                else
                    result = MessageForm.Show(ResourceLoader.GetString2("UpdatedNotRequiredQuestionsWishToContinueFirstLevel"), MessageFormType.Question);
                    
                if (result == DialogResult.Yes)
                {
                    toReturn = false;
                    this.questionsControl.PressedButtonOk();
                    //ChangeCurrentStep(2, true);
                    this.questionsControl.Focus();
                }
                changeRecommended = false;
			}
            else if (ValidateAddedQuestions() == false)
            {
                DialogResult result = MessageForm.Show(ResourceLoader.GetString2("NewRequiredQuestions"), MessageFormType.Information);
                toReturn = false;
                this.questionsControl.PressedButtonOk();
				//ChangeCurrentStep(2, true);
				this.questionsControl.Focus();
			}
			else if (ValidateDepartmentTypeChanges() == false)
			{
				if (ValidateDepartmentTypes() == false)
				{
					DialogResult dialog = MessageForm.Show(ResourceLoader.GetString2("DepartmentTypeUpdated"), MessageFormType.Question);
					if (dialog == DialogResult.No)
					{
						popUpShow = false;
						toReturn = false;
					}
					else if (GetSelectedDepartment().Count() == 0)
					{
						MessageForm.Show(ResourceLoader.GetString2("DepartmentTypeUpdatedCheckSelection"), MessageFormType.Error);
						popUpShow = false;
						toReturn = false;
					}
				}
			}
           
            if (toReturn == true)
            {
                if (ValidateIncidentTypes() == true)
                {
                    toReturn = false;
                    MessageForm.Show(ResourceLoader.GetString2("ReportWithoutIncidentType"), MessageFormType.Error);
                    this.questionsControl.SetFrontClientChangedIncidentTypeState();
					ChangeCurrentStep(2, true);
					this.questionsControl.Focus();
				}
                else if (ValidateRequiredQuestions() == true)
                {
                    toReturn = false;
                    MessageForm.Show(ResourceLoader.GetString2("RemainingNotAnsweredRequiredQuestions"), MessageFormType.Error);
					ChangeCurrentStep(2, true);
					this.questionsControl.Focus();
					this.questionsControl.SelectNextNotAskedQuestion(new VistaButtonEx());
                }
                else if (ValidateRecommendedQuestions() == true && GlobalPhoneReport.Incomplete != true)
                {
                    toReturn = false;
                    MessageForm.Show(ResourceLoader.GetString2("RemainingNotAnsweredQuestions"), MessageFormType.Error);
					ChangeCurrentStep(2, true);
					this.questionsControl.Focus();
                    this.questionsControl.SelectNextNotAskedQuestion(new VistaButtonEx());
                }
                else if (ValidateDepartmentTypes() == true)
                {
                    toReturn = false;
                    MessageForm.Show(ResourceLoader.GetString2("DepartmentsWithoutPriority"), MessageFormType.Error);
                    ChangeCurrentStep(3, true);
                    this.departmentsInvolvedControl.Focus();
                    this.departmentsInvolvedControl.departmentTypesControl.SetFocus("radioButtonExPriority1");
                }
                else if (ValidatePhoneReportCaller() == true)
                {
                    toReturn = false;
					MessageForm.Show(ResourceLoader.GetString2("AnonimousCaller"), MessageFormType.Error); 
					ChangeCurrentStep(1, true);
					this.callInformationControl.Focus();
                }
               
            }
            if ((questionActionDelete.Count > 0)&&(toReturn == true))
            {
                questionsControl.ShowPanelSelected();             
            }
            return toReturn;
        }

		private bool ValidateDepartmentTypeChanges()
		{
			bool result = true;
			foreach (DepartmentTypeClientData item in DepartmentTypeChanges)
			{
				departmentsInvolvedControl.departmentTypesControl.DeleteDepartmentWithPriority(item);
				departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.BeginInit();
				departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.DeleteItem(new GridDepartmentTypeData(item));
				departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.EndInit();
			result = false;
			}
			DepartmentTypeChanges.Clear();
			return result;
		}

		private bool ValidateChangedQuestions()
        {
            bool validate = true;
            if (GlobalPhoneReport.Answers != null)
            {
                for (int i = 0; i < GlobalPhoneReport.Answers.Count; i++)
                {
                    ReportAnswerClientData pracd = GlobalPhoneReport.Answers[i] as ReportAnswerClientData;
                    if (questionActionDelete.ContainsKey(pracd.QuestionCode) == true)
                    {
                        if (validate == true)
                            validate = false;

                        if (questionActionDelete[pracd.QuestionCode].RequirementType.Value == RequirementTypeClientEnum.Recommended)
                        {
                            deleteRecommended = true;
                        }

                        QuestionClientData deletedQuestion = questionActionDelete[pracd.QuestionCode];
                        GlobalPhoneReport.Answers.RemoveAt(i);
                        questionsControl.DeleteQuestion(deletedQuestion);
                        hasDeletedQuestion = true;
                        i--;
                    }
                    else
                    {
                        if (questionActionsUpdate.ContainsKey(pracd.QuestionCode) == true)
                        {
                            if (questionActionsUpdate[pracd.QuestionCode].RequirementType.Value == RequirementTypeClientEnum.Recommended)
                            {
                                changeRecommended = true;
                            }
                            QuestionClientData updatedQuestion = questionActionsUpdate[pracd.QuestionCode];
                         
                            if (validate == true)
                                validate = false;

                            questionsControl.UpdateQuestion(updatedQuestion);
                        }
                    }
                }
            }
            return validate;
        }

        private bool ValidateRecommendedChangedQuestions()
        {
            bool validate = true;
            foreach (QuestionClientData qcd in questionActionsUpdate.Values)
            {
                if (qcd.RequirementType.Value == RequirementTypeClientEnum.Recommended)
                    changeRecommended = true;

                foreach (IncidentTypeQuestionClientData itqcd in qcd.IncidentTypes)
                {
                    for (int i = 0; i < questionsControl.SelectedIncidentTypes.Count; i++)
                    {
                        IncidentTypeClientData incidentTypeClientData = questionsControl.SelectedIncidentTypes[i] as IncidentTypeClientData;
                        if (incidentTypeClientData.Code == itqcd.IncidentType.Code)
                        {
                            if (validate == true)
                            {
                                validate = false;
                                break;
                            }

                            questionsControl.UpdateQuestion(qcd);
                        }
                    }
                    if (validate == false)
                        break;
                }
                if (validate == false)
                    break;
            }
            if (validate == false)
            {
                questionActionsUpdate.Clear();
            }
            
            return validate;
        }

        private bool ValidateAddedQuestions()
        {
            bool validate = true;
            foreach (QuestionClientData qcd in questionActionAdd.Values)
            {
                foreach (IncidentTypeQuestionClientData itqcd in qcd.IncidentTypes)
                {
                    for (int i = 0; i < questionsControl.SelectedIncidentTypes.Count; i++)
                    {
                        IncidentTypeClientData incidentTypeClientData = questionsControl.SelectedIncidentTypes[i] as IncidentTypeClientData;
                        if (incidentTypeClientData.Code == itqcd.IncidentType.Code)
                        {
                            if (validate == true)
                            {
                                validate = false;
                                break;
                            }
                        }
                    }
                    if (validate == false)
                        break;
                }
                if (validate == false)
                    break;
            }
            if (validate == false)
                questionActionAdd.Clear();
            return validate;
        }

        private bool ValidateChangedIncidentype()
        {
            bool validate = true;
            for (int i = 0; i < questionsControl.SelectedIncidentTypes.Count; i++)
            {
                IncidentTypeClientData incidentTypeClientData = questionsControl.SelectedIncidentTypes[i] as IncidentTypeClientData;
                if (GlobalIncidentTypes.ContainsKey(incidentTypeClientData.Code) == false)
                {
                    if (validate == true)
                        validate = false;

                    questionsControl.DeleteIncidentType(incidentTypeClientData);
                    CleanIncidentTypeInformationFromGlobalPhoneReport(incidentTypeClientData);
                }
                else
                {
                    IncidentTypeClientData updatedIncidentTypeClientData = GlobalIncidentTypes[incidentTypeClientData.Code] as IncidentTypeClientData;
                    if ((updatedIncidentTypeClientData.Version > incidentTypeClientData.Version)&&(popUpShow == true))
                    {
                        if (validate == true)
                            validate = false;

                        questionsControl.UpdateIncidentType(updatedIncidentTypeClientData);
                    }
                }
            }
            return validate;
        }

        //If the IncidentType has been deleted while using it. Before we save the phone report we need to 
        // delete the incident report and its answers from it.
        private void CleanIncidentTypeInformationFromGlobalPhoneReport(IncidentTypeClientData incidentTypeClientData)
        {
            GlobalPhoneReport.IncidentTypesCodes.Remove(incidentTypeClientData.Code);
            for (int k = 0; GlobalPhoneReport.Answers != null && k < GlobalPhoneReport.Answers.Count; k++)
            {
                ReportAnswerClientData pracd = GlobalPhoneReport.Answers[k] as ReportAnswerClientData;
                bool deleteAnswer = true;
                
                foreach (IncidentTypeClientData itcd in questionsControl.SelectedIncidentTypes)
                {
                    foreach (IncidentTypeQuestionClientData prqt in itcd.IncidentTypeQuestions)
                    {
                        if (prqt.Question != null && prqt.Question.Code == pracd.QuestionCode)
                        {
                            deleteAnswer = false;
                            break;
                        }
                    }
                    if (deleteAnswer == false)
                        break;
                }

                if (deleteAnswer == true)
                {
                    GlobalPhoneReport.Answers.RemoveAt(k);
                    k--;
                }
            }
        }

        void barButtonItemCallBack_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (GlobalPhoneReport != null)
            {
                if (popUpDialer == null)
                {
                    popUpDialer = new SoftPhoneForm();
                }
                isCallingBack = true;
                popUpDialer.Initialize(ResourceLoader.GetString2("CallBack"), GlobalPhoneReport.CustomCode, GlobalPhoneReport.PhoneReportLineClient.Telephone);
                popUpDialer.Show();
            }
        }

        void callReceiverControl1_CallHangedUp(object sender, EventArgs e)
        {
            if (!isCallingBack)
            {
                if (FrontClientState != FrontClientStateEnum.WaitingForIncident)
                {
                    if (incidentAdded != true)
                    {
                        FormUtil.InvokeRequired(this, delegate
                        {
                            this.barCheckItemIncompleteCall.Enabled = true;
                            this.barButtonItemCallBack.Enabled = true;
                            this.proceduresControl.xtraTabControlProcedures.Enabled = true;
                        });
                        if (GlobalPhoneReport != null)
                            GlobalPhoneReport.HangedUpCallTime = ServerServiceClient.GetInstance().GetTime();
                    }
                    else
                    {
                        GlobalPhoneReport.HangedUpCallTime = ServerServiceClient.GetInstance().GetTime();
                        FormUtil.InvokeRequired(this, delegate
                        {
                            FinalizeEnabled = true;
                        });
                    }
                    IsActiveCall = false;
                    this.callEntering = false;
                }
                FormUtil.InvokeRequired(this, delegate
                {
                    this.barButtonItemStatus.DropDownEnabled = true;

                });
            }
        }

        void callReceiverControl1_CallPickedUp(object sender, EventArgs e)
        {
            if (isCallingBack == false)
            {
                IsActiveCall = true;

                this.FrontClientState = FrontClientStateEnum.RegisteringIncident;

                IList list = ServerServiceClient.GetInstance().SearchClientObjects(
                    SmartCadHqls.GetCustomHql(
                    SmartCadHqls.GetPublicLineByTelephoneNumber,
                    incomingCallTelephoneNumber));

                if (list.Count > 0)
                {
                    publicLineClient = list[0] as PublicLineClientData;
                    if (publicLineClient.Public == false && publicLineClient.Updated.HasValue == true)
                    {
                        callInformationControl.CallerAddress = publicLineClient.CallerAddress;
                        callInformationControl.LineName = publicLineClient.CallerName;
                    }
                    else
                    {
                        callInformationControl.CallerAddress = publicLineClient.Address;
                        callInformationControl.LineName = publicLineClient.Name;
                    }
                }
                else
                {
                    publicLineClient = new PublicLineClientData();
                    publicLineClient.Telephone = incomingCallTelephoneNumber;
                }
                callInformationControl.LineTelephoneNumber = incomingCallTelephoneNumber;
                this.proceduresControl.xtraTabControlProcedures.Enabled = true;
                this.questionAnswerTextControl.RichTextArea.Enabled = true;
                ChangeCurrentStep(1, true);

                //begin the creation of the new phone report
                GenerateNewPhoneReport();

                //setting the operator's status to busy
                SetToolStripOperatorStatus(defaultBusyStatus);
                serverServiceClient.SetOperatorStatus(defaultBusyStatus);
            }
        }

        private void GeneratePublicLineInformation()
        {
            if (publicLineClient != null)
            {
                if (publicLineClient.Public == false)
                {
                    publicLineClient.CallerName = callInformationControl.LineName;
                    publicLineClient.CallerAddress = callInformationControl.CallerAddress;
                }
            }
        }

        private void ChangeCurrentStep(int stepNumber, bool needFocus)
        {
            switch (stepNumber)
            {
                case 0:

                    this.callInformationControl.Active = false;
                    this.questionsControl.Active = false;
                    this.departmentsInvolvedControl.Active = false;
                    this.questionAnswerTextControl.Active = false;
                    this.questionAnswerTextControl.RichTextArea.Enabled = false;
                    this.proceduresControl.Active = false;
                    this.proceduresControl.xtraTabControlProcedures.Enabled = false;

                    break;
                case 1:

                    this.callInformationControl.Active = true;
                    if(needFocus == true)
                        this.callInformationControl.Focus();
                    this.questionsControl.Active = false;
                    this.departmentsInvolvedControl.Active = false;
                    this.questionAnswerTextControl.Active = false;
                    this.proceduresControl.Active = false;
                    this.proceduresControl.xtraTabControlProcedures.Enabled = true;

                    break;
                case 2:

                    this.callInformationControl.Active = false;
                    this.questionsControl.Active = true;
                    if(needFocus == true)
                        this.questionsControl.Focus();
                    this.departmentsInvolvedControl.Active = false;
                    this.questionAnswerTextControl.Active = false;
                    this.proceduresControl.Active = false;
                    this.proceduresControl.xtraTabControlProcedures.Enabled = true;

                    break;
                case 3:

                    this.callInformationControl.Active = false;
                    this.questionsControl.Active = false;
                    this.departmentsInvolvedControl.Active = true;
                    if(needFocus == true)
                        this.departmentsInvolvedControl.Focus();
                    this.questionAnswerTextControl.Active = true;
                    this.proceduresControl.Active = false;
                    this.proceduresControl.xtraTabControlProcedures.Enabled = true;

                    break;
                case 4:

                    this.callInformationControl.Active = false;
                    this.questionsControl.Active = false;
                    this.departmentsInvolvedControl.Active = false;
                    this.questionAnswerTextControl.Active = false;
                    this.proceduresControl.Active = true;
                    this.proceduresControl.xtraTabControlProcedures.Enabled = true;
                    this.proceduresControl.xtraTabControlProcedures.Focus();
                    break;
            }
        }

        private void GenerateNewPhoneReport()
        {
            this.GlobalPhoneReport = new PhoneReportClientData();
            
            PhoneReportLineClientData line = new PhoneReportLineClientData();
            line.Telephone = callInformationControl.LineTelephoneNumber;
            
            line.Address = new AddressClientData();
            line.Address.Zone = callInformationControl.CallerAddress.Zone;
            line.Address.Street = callInformationControl.CallerAddress.Street;
            line.Address.Reference = callInformationControl.CallerAddress.Reference;
            line.Address.More = callInformationControl.CallerAddress.More;

            line.Name = this.callInformationControl.LineName;

            this.GlobalPhoneReport.PhoneReportLineClient = line;
            this.GlobalPhoneReport.PickedUpCallTime = ServerServiceClient.GetTime();
            this.GlobalPhoneReport.ReceivedCallTime = callInformationControl.ReceivedCallTime;
            this.GlobalPhoneReport.CustomCode = Guid.NewGuid().ToString();
            this.GlobalPhoneReport.OperatorLogin = ServerServiceClient.OperatorClient.Login;

            if (parentForm.RecordCalls && !TelephonyServiceClient.GetInstance().IsVirtual())
            {
                TelephonyServiceClient.GetInstance().SetPhoneReportCustomCode(globalPhoneReport.CustomCode);
            }
            
        }

        #region Operator status

        private void SetToolStripOperatorStatus(OperatorStatusClientData operatorStatus)
        {
            FormUtil.InvokeRequired(this,
                delegate
                {
                    barButtonItemStatus.Tag = operatorStatus;
                    barButtonItemStatus.Caption = operatorStatus.FriendlyName;
                    ((FrontClientFormDevX)ParentForm).barButtonItemStatus.Caption = operatorStatus.FriendlyName;
                    ((FrontClientFormDevX)ParentForm).barButtonItemStatus.Glyph = FormUtil.ImageFromArray(operatorStatus.Image);
                    barButtonItemStatus.Name = operatorStatus.Name;
                    barButtonItemStatus.Glyph = FormUtil.ImageFromArray(operatorStatus.Image); 
                });
        }

        private void CreateOperatorStatusItem(OperatorStatusClientData operatorStatus)
        {
            barButtonItem = new BarButtonItem(ribbonControl1.Manager, operatorStatus.FriendlyName);

            barButtonItem.Name = operatorStatus.Name;
            barButtonItem.Tag = operatorStatus;
            barButtonItem.Glyph = FormUtil.ImageFromArray(operatorStatus.Image);
            barButtonItem.ItemClick += new ItemClickEventHandler(barButtonItem_Click);

            if (operatorStatus.Name == OperatorStatusClientData.Ready.Name)
            {
                readyMenuItem = barButtonItem;
            }
            else if (operatorStatus.Name == OperatorStatusClientData.Busy.Name)
            {
                barButtonItem.Enabled = false;
                busyMenuItem = barButtonItem;
            }
           

            int indexToinsert = FindIndexOperatorStatusMenu(operatorStatus);
            if (operatorStatus.Name.Equals(OperatorStatusClientData.Training.Name) == false)
            {
                FormUtil.InvokeRequired(this, delegate
                {
                    if (popupMenu1.ItemLinks.Count == 2)
                        popupMenu1.ItemLinks.Add(barButtonItem, true);
                    else
                        popupMenu1.ItemLinks.Insert(indexToinsert, barButtonItem);
                });
            }
            if (operatorStatus.Name.Equals(OperatorStatusClientData.Busy.Name) || operatorStatus.Name.Equals(OperatorStatusClientData.Absent.Name))
                barButtonItem.Enabled = false;
        }

        private int FindIndexOperatorStatusMenu(OperatorStatusClientData operatorStatus)
        {
            if (operatorStatus.Name == OperatorStatusClientData.Ready.Name)
                return 0;
            if (operatorStatus.Name == OperatorStatusClientData.Busy.Name)
                return 1;

            int index = popupMenu1.ItemLinks.Count;
            for (int i = 3; i < popupMenu1.ItemLinks.Count ; i++)
            {
				BarButtonItem barButtonItem = popupMenu1.ItemLinks[i].Item as BarButtonItem;
				OperatorStatusClientData operatorStatusAux = barButtonItem.Tag as OperatorStatusClientData;
                if (operatorStatusAux != null)
                {
                    int res = Comparer<string>.Default.Compare(operatorStatus.FriendlyName, operatorStatusAux.FriendlyName);
                    if (res < 0) 
                    {
                        index = i;
                        break;
                    }
                }
            }
            return index;
        }

        private void UpdateOperatorStatusItem(OperatorStatusClientData operatorStatus)
        {
            
            OperatorStatusClientData operatorStatusAux = barButtonItemStatus.Tag as OperatorStatusClientData;
            if (operatorStatus.Name == OperatorStatusClientData.Ready.Name || operatorStatus.Name == OperatorStatusClientData.Busy.Name)
            {
                int index = 0;
                if (operatorStatus.Name == OperatorStatusClientData.Busy.Name)
                {
                    defaultBusyStatus = operatorStatus;
                    index = 1;
                }
                else
                {
                    defaultReadyStatus = operatorStatus;
                }
				BarButtonItem barButtonItem = popupMenu1.ItemLinks[index].Item as BarButtonItem;
                if (operatorStatusAux != null)
                {
                    FormUtil.InvokeRequired(this, delegate
                    {

                        barButtonItem.Name = operatorStatus.Name;
                        barButtonItem.Caption = operatorStatus.FriendlyName;
                        barButtonItem.Tag = operatorStatus;

                    });
                }
            }
            else
            {
                int inToDel = GetIndexToDelete(operatorStatus);
                FormUtil.InvokeRequired(this, delegate
                    {
                      popupMenu1.ItemLinks.RemoveAt(inToDel);
                   });
                CreateOperatorStatusItem(operatorStatus);
            }

            if (operatorStatusAux.Code == operatorStatus.Code)
                SetToolStripOperatorStatus(operatorStatus);
            else
                SetToolStripOperatorStatus(operatorStatusAux);
        }

        private int GetIndexToDelete(OperatorStatusClientData operatorStatus)
        {
            bool found = false;
            for (int i = 3; i < popupMenu1.ItemLinks.Count && found == false; i++)
            {
				BarButtonItem barButtonItem = popupMenu1.ItemLinks[i].Item as BarButtonItem;
				OperatorStatusClientData operatorStatusAux = barButtonItem.Tag as OperatorStatusClientData;
                if (operatorStatusAux != null && operatorStatusAux.Code == operatorStatus.Code)
                {
                    return i;
                }
            }
            return -1;
        }

        private void DeleteOperatorStatusItem(OperatorStatusClientData operatorStatus)
        {
            bool found = false;
            for (int i = 3; i < popupMenu1.ItemLinks.Count && found == false; i++)
            {
				BarButtonItem barButtonItem = popupMenu1.ItemLinks[i].Item as BarButtonItem;
				OperatorStatusClientData operatorStatusAux = barButtonItem.Tag as OperatorStatusClientData;
                if (operatorStatusAux != null && operatorStatusAux.Code == operatorStatus.Code)
                {
                    FormUtil.InvokeRequired(this, delegate
                    {
                        //check if is selected.
                        if (barButtonItemStatus.Name == operatorStatusAux.Name)
                        {
                            if (operatorStatusAux.NotReady == true)
                            {
                                SetToolStripOperatorStatus(defaultBusyStatus);
                            }
                            else
                            {
                                SetToolStripOperatorStatus(defaultReadyStatus);
                            }
                        }
                        popupMenu1.ItemLinks.RemoveAt(i);
                        //Delete separator
						if (popupMenu1.ItemLinks.Count <= 3) 
                        {
							popupMenu1.ItemLinks.RemoveAt(2);
                        }
                    });
                    found = true;
                }
            }
        }

        private void CreateOperatorStatusMenu()
        {
            IList operatorStatuses = serverServiceClient.SearchClientObjects(SmartCadHqls.GetOperatorStatus);

            defaultReadyStatus = operatorStatuses[0] as OperatorStatusClientData;
            defaultBusyStatus = operatorStatuses[1] as OperatorStatusClientData;

            List<OperatorStatusClientData> list = new List<OperatorStatusClientData>();

            //add all the operatorStatus that are nor equal to Busy or Ready
            foreach (OperatorStatusClientData operatorStatus in operatorStatuses)
            {
                if (operatorStatus.Name != OperatorStatusClientData.Busy.Name && operatorStatus.Name != OperatorStatusClientData.Ready.Name)
                {
                    if (operatorStatus.Name == OperatorStatusClientData.Absent.Name)
                    {
                        defaultAbsentStatus = operatorStatus;
                    }
                    list.Add(operatorStatus);
                }                
            }

            //sort this elements.
            list.Sort(delegate(OperatorStatusClientData x, OperatorStatusClientData y)
            {
                return Comparer<string>.Default.Compare(x.FriendlyName, y.FriendlyName);
            });

            //Add the 2 first elements..
            CreateOperatorStatusItem(defaultReadyStatus);
            CreateOperatorStatusItem(defaultBusyStatus);

            //Add the rest of the elements.
            foreach (OperatorStatusClientData operatorStatus in list)
            {
                CreateOperatorStatusItem(operatorStatus);
            }
            
            //SetToolStripOperatorStatus(defaultReadyStatus);
        }

        private void barButtonItem_Click(object sender, EventArgs e)
        {
			BarButtonItem barButtonItem = ((ItemClickEventArgs)e).Item as BarButtonItem;

            OperatorStatusClientData operatorStatus = barButtonItem.Tag as OperatorStatusClientData;
           
            if (operatorStatus == defaultReadyStatus)
            { 
                if (this.FrontClientState == FrontClientStateEnum.WaitingForIncident)
                {
                    operatorStatusFromApp = operatorStatus.NotReady != ((OperatorStatusClientData)barButtonItemStatus.Tag).NotReady;
                    SetToolStripOperatorStatus(operatorStatus);
                    serverServiceClient.SetOperatorStatus(operatorStatus);
                    //operatorStatusFromApp = true;
                    if (isRegistered)
                        TelephonyServiceClient.GetInstance().SetIsReady(true);
                }

                if (!isRegistered)
                {
                    parentForm.RecoverConnection();
                    isRegistered = true;
                }
            }
            else
            {
                operatorStatusFromApp = operatorStatus.NotReady != ((OperatorStatusClientData)barButtonItemStatus.Tag).NotReady;
                SetToolStripOperatorStatus(operatorStatus);

                if (IsNewStatusAllow())
                {
                    serverServiceClient.SetOperatorStatus(operatorStatus);
                    TelephonyServiceClient.GetInstance().SetIsReady(false);
                }
            }
        }

        #endregion

        void questionsControl_IncidentTypeSelected(object sender, EventArgs e)
        {
            QuestionsControlDevX questionCtrl = (QuestionsControlDevX)sender;
            IList selectedIncidentTypes = questionCtrl.SelectedIncidentTypes;
            if (HasIncidentTypesListChanged(selectedIncidentTypes) == true)
            {
                this.departmentsInvolvedControl.departmentTypesControl.FillDepartmentTypes();
                this.departmentsInvolvedControl.departmentTypesControl.CleanControl();
                this.GlobalPhoneReport.IncidentTypesCodes = new ArrayList();
                foreach (IncidentTypeClientData incidentType in selectedIncidentTypes)
                    this.GlobalPhoneReport.IncidentTypesCodes.Add(incidentType.Code);
				if (selectedIncidentTypes.Count == 1 && (selectedIncidentTypes[0] as IncidentTypeClientData) != null &&
					(selectedIncidentTypes[0] as IncidentTypeClientData).NoEmergency == true)
				{
					this.departmentsInvolvedControl.departmentTypesControl.CleanControl();
					this.departmentsInvolvedControl.Enabled = false;
				}
				else
				{
					this.departmentsInvolvedControl.Enabled = true;
				
				}
            }
            this.departmentsInvolvedControl.departmentTypesControl.IncidentTypes = selectedIncidentTypes;

            if (buttonExSimilarIncidentsFilter.LookAndFeel.SkinName.Equals("Black"))
            {
                UpdateSimilarIncidentsFilter();
            }
            proceduresControl.browserGenericProcedure.Clear();
            ProceduresNaturalLanguage.Write(selectedIncidentTypes, proceduresControl.browserGenericProcedure);

            proceduresControl.browserProcedures.Clear();
            questionAnswerTextControl.RichTextArea.Clear();

            proceduresControl.xtraTabControlProcedures.SelectedTabPage = proceduresControl.xtraTabControlProcedures.TabPages[0];

            questionActionAdd.Clear();
            questionActionsUpdate.Clear();
            questionActionDelete.Clear();
        }

        private bool HasIncidentTypesListChanged(IList newList)
        {
            bool result = false;
            if (GlobalPhoneReport.IncidentTypesCodes != null)
            {
                if (newList.Count != globalPhoneReport.IncidentTypesCodes.Count)
                    result = true;
                else if (result == false)
                {
                    for (int i = 0; i < newList.Count && result == false; i++)
                    {
                        IncidentTypeClientData newIncidentType = newList[i] as IncidentTypeClientData;
                        if (GlobalPhoneReport.IncidentTypesCodes.Contains(newIncidentType.Code) == false)
                            result = true;
                    }
                }
            }
            else
                result = true;

            return result;
        }

		public void serverService_CommittedChanges(object sender, CommittedObjectDataCollectionEventArgs args)
        {
            try
            {
                
                if (args != null && args.Objects != null && args.Objects.Count > 0)
                {
                    if (args.Objects[0] is IncidentClientData)//if it's an incident...
                    {
                        #region IncidentClientData
                        if (((IncidentClientData)args.Objects[0]).Status.Name.Equals(IncidentStatusClientData.Closed.Name) == false &&
							args.Action != CommittedDataAction.Delete)
                        {
                            SmartCadContext context = args.Context as SmartCadContext;
                            bool select = false;
                            if (context != null)
                            {
                                if (ServerServiceClient.GetInstance().OperatorClient != null &&
                                    ServerServiceClient.GetInstance().OperatorClient.Login == context.Operator)
                                {
                                    select = true;
                                }
                            }
                            gridControlExPreviousIncidents.AddOrUpdateItem(new GridIncidentData(args.Objects[0] as IncidentClientData),select);
                            pressedButtonsFilter = true;
                            gridViewExPreviousIncidents.RefreshData();
                            pressedButtonsFilter = false;
                            if (searchByText == true)
                            {
                                searchByText = false;
                            }
                            this.GlobalIncidents.Add(args.Objects[0] as IncidentClientData);
                        }
						else if (((IncidentClientData)args.Objects[0]).Status.Name.Equals(IncidentStatusClientData.Closed.Name))
						{
							gridControlExPreviousIncidents.DeleteItem(new GridIncidentData(args.Objects[0] as IncidentClientData));
							globalIncidents.Remove(args.Objects[0] as IncidentClientData);
						}
                        #endregion
                    }
                    else if (args.Objects[0] is PhoneReportClientData)// if it's a phone report... gotta check if it is already added...
                    {
                        #region PhoneReportClientData
                        PhoneReportClientData phoneReport = args.Objects[0] as PhoneReportClientData;
						IncidentClientData incidentData = new IncidentClientData();
						incidentData.CustomCode = phoneReport.IncidentCustomCode;

						int index = gridControlExPreviousIncidents.Items.IndexOf(new GridIncidentData(incidentData));
						if (phoneReport != null && index >= 0)
                            incidentData = ((GridIncidentData)gridControlExPreviousIncidents.Items[index]).Tag as IncidentClientData;

                        if (incidentData != null)
                        {
                            bool found = false;
                            for (int i = 0; i < incidentData.PhoneReports.Count && found != true; i++)
                            {
                                PhoneReportClientData phoneReportTemp = incidentData.PhoneReports[i] as PhoneReportClientData;
                                if (phoneReportTemp.CustomCode == phoneReport.CustomCode)
                                {
                                    found = true;
                                    phoneReportTemp = phoneReport;
                                }
                            }
                            if (found == false)
                            {
                                foreach (int incidentTypeCode in phoneReport.IncidentTypesCodes)
                                {
                                    bool alreadyAdded = false;
                                    IncidentTypeClientData itcd = GlobalIncidentTypes[incidentTypeCode];
                                    foreach (IncidentTypeClientData incidentType in incidentData.IncidentTypes)
                                    {
                                        if (incidentType.Code == itcd.Code)
                                            alreadyAdded = true;
                                    }

                                    if (alreadyAdded == false)
                                        incidentData.IncidentTypes.Add(itcd);
                                }
                                incidentData.PhoneReports.Add(phoneReport);
                                incidentSyncBox.Sync(new GridIncidentData(incidentData), args.Action);
                            }
                        }
                        #endregion
                    }
                    else if (args.Objects[0] is OperatorStatusClientData)
                    {
                        #region OperatorStatusClientData
                        OperatorStatusClientData operatorStatus = args.Objects[0] as OperatorStatusClientData;
                        if (args.Action == CommittedDataAction.Save)
                        {
                            CreateOperatorStatusItem(operatorStatus);
                        }
                        else if (args.Action == CommittedDataAction.Update)
                        {
                            UpdateOperatorStatusItem(operatorStatus);
                        }
                        else if (args.Action == CommittedDataAction.Delete)
                        {
                            DeleteOperatorStatusItem(operatorStatus);
                        }
                        #endregion
                    }
                    else if (args.Objects[0] is IncidentTypeClientData)
                    {
                        #region  IncidentTypeClientData
                        IncidentTypeClientData incidentTypeClientData = args.Objects[0] as IncidentTypeClientData;
                        if (args.Action == CommittedDataAction.Save)
                        {
                            //IList incidentTypeQuestion = serverServiceClient.SearchClientObjectsMaxRows(SmartCadHqls.GetCustomHql(SmartCadHqls.IncidentTypeQuestionByCodeWithDepartmentTypes, incidentTypeClientData.Code),true);
                            //IList incidentTypeDepartments = serverServiceClient.SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetIncidentTypeDepartmentTypeDataByIncidentTypeCode, incidentTypeClientData.Code));
                            //incidentTypeClientData.IncidentTypeQuestions = incidentTypeQuestion;
                            //incidentTypeClientData.IncidentTypeDepartmentTypeClients = incidentTypeDepartments;

                            AddIncindetType(incidentTypeClientData);
                        }
                        else if (args.Action == CommittedDataAction.Update)
                        {
                            popUpShow = true;
                            //IList incidentTypeQuestion = serverServiceClient.SearchClientObjectsMaxRows(SmartCadHqls.GetCustomHql(SmartCadHqls.IncidentTypeQuestionByCodeWithDepartmentTypes, incidentTypeClientData.Code), true);
                            //IList incidentTypeDepartments = serverServiceClient.SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetIncidentTypeDepartmentTypeDataByIncidentTypeCode, incidentTypeClientData.Code));
                            //incidentTypeClientData.IncidentTypeQuestions = incidentTypeQuestion;
                            //incidentTypeClientData.IncidentTypeDepartmentTypeClients = incidentTypeDepartments; 
							
							UpdateIncidentType(incidentTypeClientData);
                            for (int i = 0; i < IncidentChanges.Count; i++)
                            {
                                if ((IncidentChanges[i] as IncidentTypeClientData).Code == incidentTypeClientData.Code)
                                    IncidentChanges.RemoveAt(i);
                            }
                            IncidentChanges.Add(incidentTypeClientData);
                        }
                        else if (args.Action == CommittedDataAction.Delete)
                        {
                            DeleteIncidentType(incidentTypeClientData);
                        }
                        //Actualizar tipod de incident en datagrid.
                        FormUtil.InvokeRequired(this, delegate
                        {
                            gridControlExPreviousIncidents.BeginInit();
                        });
                        IList list = new ArrayList(gridControlExPreviousIncidents.Items);
                        foreach (GridIncidentData item in list)
                        {
                            IncidentClientData icd = item.Tag as IncidentClientData;
                            for (int i = 0; i < icd.IncidentTypes.Count; i++)
                            {
                                IncidentTypeClientData itcd = icd.IncidentTypes[i] as IncidentTypeClientData;
                                if (itcd.Code == incidentTypeClientData.Code)
                                {
                                    icd.IncidentTypes[i] = incidentTypeClientData;
                                    gridControlExPreviousIncidents.AddOrUpdateItem(item);
                                    break;
                                }
                            }
                        }
                        FormUtil.InvokeRequired(this, delegate
                        {
                            gridControlExPreviousIncidents.EndInit();
                        });
                        #endregion
                    }
                    else if (args.Objects[0] is QuestionClientData)                                                    
                    {
                        #region QuestionClientData
                        QuestionClientData questionClientDara = args.Objects[0] as QuestionClientData;
                        if (args.Action == CommittedDataAction.Save)
                        {
                            AddQuestion(questionClientDara);
                        }
                        else if (args.Action == CommittedDataAction.Update)
                        {
                            UpdateQuestion(questionClientDara);
                            for (int i = 0; i < QuestionChanges.Count; i++)
                            {
                                if ((QuestionChanges[i] as QuestionClientData).Code == questionClientDara.Code)
                                    QuestionChanges.RemoveAt(i);
                            }
                            QuestionChanges.Add(questionClientDara);
                        }
                        else if (args.Action == CommittedDataAction.Delete)
                        {
                            DeleteQuestion(questionClientDara);
                        }
                        #endregion
                    }
					else if (args.Objects[0] is ApplicationClientData)
                    {
                        #region ApplicationClientData
                        lock (sync)
                        {
                            ApplicationClientData acd = ((ApplicationClientData)args.Objects[0]);
                            if (acd.ToOperators.Contains(serverServiceClient.OperatorClient.Code))
                            {
                                if (acd.Message == "true")
                                {
                                    if (remoteControlOperators.Contains(acd.Operator.Code) == false)
                                        remoteControlOperators.Add(acd.Operator.Code);
                                    FormUtil.InvokeRequired(this,
                                        delegate
                                        {
                                            if (timerSendImage.Enabled == false && remoteControlOperators.Count > 0) { timerSendImage.Enabled = true; }
                                        });
                                }
                                else if (acd.Message == "false")
                                {
                                    FormUtil.InvokeRequired(this,
                                        delegate
                                        {
                                            if (timerSendImage.Enabled == true && remoteControlOperators.Count <= 1) { timerSendImage.Enabled = false; }
                                        });
                                    remoteControlOperators.Remove(acd.Operator.Code);
                                }
                            }
                        }
                        #endregion
                    }
                    else if (args.Objects[0] is ApplicationPreferenceClientData)
                    {
                        #region ApplicationPreferenceClientData
                        foreach (ApplicationPreferenceClientData preference in args.Objects)
                        {
                            if (args.Action == CommittedDataAction.Update)
                            {
                                if (preference.Name.Equals("ShowErrorDetails", StringComparison.CurrentCultureIgnoreCase) == true)
                                {
                                    MessageForm.CheckPreference = MessageForm.CheckPreference = (preference.Value.ToString().ToUpper() == "TRUE" ? true : false);
                                }
                                if (globalApplicationPreference.ContainsKey(preference.Name))
                                {
                                    globalApplicationPreference[preference.Name] = preference;
                                }
                                else
                                {
                                    globalApplicationPreference.Add(preference.Name, preference);
                                }
                            }
                        }
                        UpdateSimilarIncidentsFilter();
                        #endregion
					}
					else if (args.Objects[0] is DepartmentTypeClientData)
					{
						#region  DepartmentTypeClientData
						if (args.Action == CommittedDataAction.Delete)
						{
							DepartmentTypeClientData data = (DepartmentTypeClientData)args.Objects[0];
							GridDepartmentTypeData itemData = new GridDepartmentTypeData(data);
							if (GetSelectedDepartment().Contains(itemData))
								DepartmentTypeChanges.Add(data);
							else
							{
								FormUtil.InvokeRequired(this,
								   delegate
								   {
									   departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.BeginInit();
									   departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.DeleteItem(itemData);
									   departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.EndInit();
								   });
							}
						}
						else if (args.Action == CommittedDataAction.Save || args.Action == CommittedDataAction.Update)
						{
							GridDepartmentTypeData data = new GridDepartmentTypeData((DepartmentTypeClientData)args.Objects[0]);
							FormUtil.InvokeRequired(this,
								delegate
								{
									departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.BeginInit();
									departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.AddOrUpdateItem(data);
									departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.EndInit();
								});
						}
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

		private IEnumerable<GridDepartmentTypeData> GetSelectedDepartment()
		{
			return departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.Items.Cast<GridDepartmentTypeData>().Where(
									dep => dep.DepartmentTypeCheck);
		}

		/// <summary>
        /// This procedure get the list of the OperatorStatus that can be sorted.
        /// </summary>
        /// <returns>The list of the OperatorStatus.</returns>
        private List<OperatorStatusClientData> GetListOperatorStatusToOrder()
        {
            List<OperatorStatusClientData> list = new List<OperatorStatusClientData>();

            for (int i = 0; i < popupMenu1.ItemLinks.Count; i++)
            {
				BarButtonItem BarButtonItem = popupMenu1.ItemLinks[i].Item as BarButtonItem;
				OperatorStatusClientData operatorStatusAux = BarButtonItem.Tag as OperatorStatusClientData;
                if (operatorStatusAux != null)
                {
                    if (operatorStatusAux.Name != OperatorStatusClientData.Busy.Name && operatorStatusAux.Name != OperatorStatusClientData.Ready.Name)
                    {
                        list.Add(operatorStatusAux);
                    }
                }
            }
            return list;
        }

        #region IncidentType Actions
        private void DeleteIncidentType(IncidentTypeClientData incidentTypeClientData)
        {
            if (GlobalIncidents != null && GlobalIncidentTypes.ContainsKey(incidentTypeClientData.Code) == true)
            {
                GlobalIncidentTypes.Remove(incidentTypeClientData.Code);
                if (frontClientState == FrontClientStateEnum.WaitingForIncident)
                {
                    FormUtil.InvokeRequired(this, delegate
                    {
                        CleanForm(false);
                    });
                }
            }
            else if (incidentTypeActionDelete.ContainsKey(incidentTypeClientData.Code) == false)
            {
                incidentTypeActionDelete.Add(incidentTypeClientData.Code, incidentTypeClientData);
            }
        }

        private void UpdateIncidentType(IncidentTypeClientData incidentTypeClientData)
        {
            if (GlobalIncidents != null && GlobalIncidentTypes.ContainsKey(incidentTypeClientData.Code) == true)
            {
                GlobalIncidentTypes[incidentTypeClientData.Code] = incidentTypeClientData;
                if (frontClientState == FrontClientStateEnum.WaitingForIncident && callEntering == false)
                {
                    FormUtil.InvokeRequired(this, delegate
                    {
                        CleanForm(false);
                    });
                }
            }
            else
            {
                if (incidentTypeActionsUpdate.ContainsKey(incidentTypeClientData.Code) == true)
                {
                    incidentTypeActionsUpdate[incidentTypeClientData.Code] = incidentTypeClientData;
                }
                else
                {
                    incidentTypeActionsUpdate.Add(incidentTypeClientData.Code, incidentTypeClientData);
                }
            }
        }

        private void AddIncindetType(IncidentTypeClientData incidentTypeClientData)
        {
            if (GlobalIncidents != null)
                GlobalIncidentTypes.Add(incidentTypeClientData.Code, incidentTypeClientData);
            if (frontClientState == FrontClientStateEnum.WaitingForIncident && callEntering == false)
            {
                FormUtil.InvokeRequired(this, delegate
                {
                    CleanForm(false);
                });
            }
            else
            {
                if (incidentTypeActionAdd.ContainsKey(incidentTypeClientData.Code) == false)
                {
                    incidentTypeActionAdd.Add(incidentTypeClientData.Code, incidentTypeClientData);
                }
            }
        }
        #endregion

        #region Question Actions
        private void DeleteQuestion(QuestionClientData questionClientData)
        {
            if (GlobalIncidents != null)
            {
                foreach (IncidentTypeClientData itcd in GlobalIncidentTypes.Values)
                {
                    for (int i = 0; i < itcd.IncidentTypeQuestions.Count; i++)
                    {
                        IncidentTypeQuestionClientData itqcd = itcd.IncidentTypeQuestions[i] as IncidentTypeQuestionClientData;
                        if (itqcd.Question == null || itqcd.Question.Code == questionClientData.Code)
                        {
                            itcd.IncidentTypeQuestions.RemoveAt(i);
                            break;
                        }
                    }
                }
            }

            if (frontClientState == FrontClientStateEnum.WaitingForIncident)
            {
                FormUtil.InvokeRequired(this, delegate
                {
                    CleanForm(false);
                });
            }
            else
            {
                if (questionActionDelete.ContainsKey(questionClientData.Code) == false)
                {
                    questionActionDelete.Add(questionClientData.Code, questionClientData);
                }
            }
        }

        private void UpdateQuestion(QuestionClientData questionClientData)
        {
            if (GlobalIncidents != null)
            {
                foreach (IncidentTypeClientData itcd in GlobalIncidentTypes.Values)
                {
                    for (int i = 0; i < itcd.IncidentTypeQuestions.Count; i++)
                    {
                        IncidentTypeQuestionClientData itqcd = itcd.IncidentTypeQuestions[i] as IncidentTypeQuestionClientData;
                        if (itqcd.Question != null && itqcd.Question.Code == questionClientData.Code)
                        {
                            itcd.IncidentTypeQuestions.RemoveAt(i);
                            i--;
                        }
                    }
                }

                foreach (IncidentTypeQuestionClientData itqcd in questionClientData.IncidentTypes)
                {
                    if (GlobalIncidentTypes.ContainsKey(itqcd.IncidentType.Code) == true)
                    {
                        IncidentTypeClientData itcd = GlobalIncidentTypes[itqcd.IncidentType.Code] as IncidentTypeClientData;
                        itqcd.Question = questionClientData;
                        itcd.IncidentTypeQuestions.Add(itqcd);
                    }
                }
            }
            if (frontClientState == FrontClientStateEnum.WaitingForIncident && callEntering == false)
            {
                FormUtil.InvokeRequired(this, delegate
                {
                     CleanForm(false);
                });
            }
            else
            {
                //if (questionClientData.RequirementType == RequirementTypeClientEnum.Recommended && 
                  if(questionActionsUpdate.ContainsKey(questionClientData.Code) == false)
                {
                    questionActionsUpdate.Add(questionClientData.Code, questionClientData);
                }
                else
                {
                    //if (questionClientData.RequirementType == RequirementTypeClientEnum.Recommended &&
                      if(questionActionsUpdate[questionClientData.Code] != null)
                        questionActionsUpdate[questionClientData.Code] = questionClientData;
                }
            }

            if (questionActionAdd.ContainsKey(questionClientData.Code))
            {
                questionActionAdd[questionClientData.Code] = questionClientData;
                questionActionsUpdate.Remove(questionClientData.Code);
            }
        }

        private void AddQuestion(QuestionClientData questionClientData)
        {
            foreach (IncidentTypeQuestionClientData itqcd in questionClientData.IncidentTypes)
            {
                if (GlobalIncidents != null && GlobalIncidentTypes.ContainsKey(itqcd.IncidentType.Code) == true)
                {
                    IncidentTypeClientData itcd = GlobalIncidentTypes[itqcd.IncidentType.Code] as IncidentTypeClientData;
                    itqcd.Question = questionClientData;
                    itcd.IncidentTypeQuestions.Add(itqcd);
                }
            }
            if (frontClientState == FrontClientStateEnum.WaitingForIncident && callEntering == false)
            {
                FormUtil.InvokeRequired(this, delegate
                {
                    CleanForm(false);
                });
            }
            else
            {
                if (questionClientData.RequirementType == RequirementTypeClientEnum.Recommended &&
                    questionActionAdd.ContainsKey(questionClientData.Code) == false)
                {
                    questionActionAdd.Add(questionClientData.Code, questionClientData);
                }
                else
                {
                    if (questionClientData.RequirementType == RequirementTypeClientEnum.Recommended &&
                        questionActionAdd[questionClientData.Code] != null)
                        questionActionAdd[questionClientData.Code] = questionClientData;
                }
            }
        }
        #endregion

        /// <summary>
        /// This variable is used to know if the new incident that was created, it was created in status closed
        /// </summary>
        private bool incidentClosed = false;
        private void SaveOrUpdateIncident()
        {
			FormUtil.InvokeRequired(this, delegate
			{
			   if (incidentAdded != true)//Adding a phoneReport with the incident...
			   {
				   if (ValidateInterface() == true)
				   {
					   GenerateNewIncident();
					   this.FrontClientState = FrontClientStateEnum.UpdatingIncident;
                       ApplicationServiceClient.Current(UserApplicationClientData.Map).SendEnableButton(false, ButtonType.AddPoint);
                       hasDeletedQuestion = false; 
				   }
			   }//this happens only when updating the phoneReport...
			   else
			   {
				   if (updatedAnswers.Count > 0)
				   {
					   GlobalPhoneReport.Answers = updatedAnswers;
					   if (ValidateInterface() == true)
					   {
						   ServerServiceClient.GetInstance().UpdatePhoneReportClient(GlobalPhoneReport);
						   questionsControl.FrontClientState = FrontClientStateEnum.UpdatingIncident;
                           updatedAnswers.Clear();
					   }
					   else
					   {
						   questionsControl.FrontClientState = FrontClientStateEnum.UpdatingIncident;
                           updatedAnswers.Clear();
					   }
                       CheckEnableParkCall();
				   }
			   }
			});
        }

        //No borrar, se utilizara despues.
        private IList CheckPhoneReportUpdates(IList newAnswers)
        {
            IList answersToAdd = new ArrayList();

            if (GlobalPhoneReport != null)
            {
                if (newAnswers != null)
                {
                    foreach (ReportAnswerClientData phoneReportAnswer in newAnswers)
                    {
                        bool found = false;
                        int i = 0;
                        while (found == false && i < GlobalPhoneReport.Answers.Count)
                        {
                            ReportAnswerClientData tempPhoneReportAnswer = GlobalPhoneReport.Answers[i] as ReportAnswerClientData;
                            if (tempPhoneReportAnswer.QuestionCode == phoneReportAnswer.QuestionCode)
                            {
                                found = true;
                            }
                            i++;
                        }
                        if (found == false)
                        {
                            phoneReportAnswer.ReportCode = GlobalPhoneReport.Code;
                            answersToAdd.Add(phoneReportAnswer);
                        }
                    }
                }
            }
            return answersToAdd;
        }

        private bool IsEmergencyIncident(PhoneReportClientData newPhoneReport)
        {
            bool result = true;
            if (newPhoneReport.IncidentTypesCodes.Count == 1)
            {   
                IncidentTypeClientData incidentType = globalIncidentTypes[(int)newPhoneReport.IncidentTypesCodes[0]];
                if(incidentType.NoEmergency == true)
                    result = false;
            }
            return result;
        }

        private void GenerateNewIncident()
        {
            IncidentClientData incident = new IncidentClientData();
            incident.SourceIncidentApp = SourceIncidentAppEnum.FirstLevel;
            incident.PhoneReports = new ArrayList();
            incident.StartDate = DateTime.Now;
            incident.EndDate = DateTime.Now;
            ApplicationPreferenceClientData prefix = globalApplicationPreference["FrontClientIncidentCodePrefix"];
			ApplicationPreferenceClientData isSuffix = globalApplicationPreference["IsIncidentCodeSuffix"];
			incident.CustomCode = serverServiceClient.OperatorClient.Code + serverServiceClient.GetTime().ToString("yyMMddHHmmss");
			if (isSuffix.Value.Equals(false.ToString()))
				incident.CustomCode=prefix.Value + incident.CustomCode;
			else
				incident.CustomCode += prefix.Value;
			incident.Address = this.questionsControl.IncidentAddress;
            incident.IsEmergency = IsEmergencyIncident(GlobalPhoneReport);
            
            if (incident.IsEmergency == true)
                incident.Status = IncidentStatusClientData.Open;
            else
                incident.Status = IncidentStatusClientData.Closed;

            GeneratePhoneReportCaller();
            GeneratePublicLineInformation();
            foreach (ReportBaseDepartmentTypeClientData reportBaseDepartmentType in 
                this.GlobalPhoneReport.ReportBaseDepartmentTypesClient)
            {
                reportBaseDepartmentType.ReportBaseCode = this.GlobalPhoneReport.Code;
            }
            if (GlobalPhoneReport.ReportBaseDepartmentTypesClient == null || GlobalPhoneReport.ReportBaseDepartmentTypesClient.Count == 0)
                incident.Status = IncidentStatusClientData.Closed;

            //this variable is changed to know if incident was created in closed status
            incidentClosed = incident.Status == IncidentStatusClientData.Closed;
            GlobalPhoneReport.PhoneReportCallerClient.Anonymous = callInformationControl.IsAnonimousCallActivated;
            this.GlobalPhoneReport.IncidentCode = incident.Code;
            if (GlobalPhoneReport.Answers == null)
                GlobalPhoneReport.Answers = new ArrayList();
            incident.PhoneReports.Add(this.GlobalPhoneReport);
            ServerServiceClient.GetInstance().SaveOrUpdateClientData(publicLineClient);
            ServerServiceClient.GetInstance().SaveOrUpdateClientData(incident);
            updatedAnswers.Clear();
            
        }

        private void GeneratePhoneReportCaller()
        {
            PhoneReportCallerClientData phoneReportCaller = new PhoneReportCallerClientData();
            phoneReportCaller.Address = callInformationControl.CallerAddress;
            if (callInformationControl.Anonymous == false)
            {
                phoneReportCaller.Name = this.callInformationControl.LineName;
                phoneReportCaller.Anonymous = false;
            }
            else
            {
                phoneReportCaller.Name = string.Empty;
                phoneReportCaller.Anonymous = true;
            }
            phoneReportCaller.Telephone = this.callInformationControl.LineTelephoneNumber;
            phoneReportCaller.AdditionalNumber = this.callInformationControl.CallerNumber;
            this.GlobalPhoneReport.PhoneReportCallerClient = phoneReportCaller;
        }

        private void FillIncidentDetails(SelectedIncidentTaskResult selectedIncidentTask, bool sameIncident)
        {
            try
            {
                string strXml = ApplicationUtil.BuildHtml(selectedIncidentTask.Xml, xslCompiledTransform, "incident");

                FileStream fs = File.Open("incident_firstlevel.html", FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite);
                byte[] byteXML = Encoding.Unicode.GetBytes(strXml);
                fs.Write(byteXML, 0, byteXML.Length);
                fs.Flush();
                fs.Close();
                FormUtil.InvokeRequired(this,
               delegate
               {
                   this.layoutControlGroupIncidentDetails.Text =  ResourceLoader.GetString2("IncidentDetails");
				   this.layoutControlGroupIncidentDetails.Text += ResourceLoader.GetString2("UpdatedAt");
                   this.layoutControlGroupIncidentDetails.Text += serverServiceClient.GetTime().ToString("MM/dd/yyyy HH:mm", ApplicationUtil.GetCurrentCulture());
               });
             
                FormUtil.InvokeRequired(searchableWebBrowser, delegate
                {
                    searchableWebBrowser.Navigate(new FileInfo("incident_firstlevel.html").FullName, sameIncident);
                });
            }
            catch(Exception ex)
            {
                SmartLogger.Print(ex);
                //MessageForm.Show("No se pueden cargar los detalles del incidente. Intente nuevamente.", MessageFormType.Error);
            }
        }

        private void ClearAssociatedInfo()
        {
            FormUtil.InvokeRequired(this, delegate
            {
				this.callsAndDispatchsControl.gridControlExTotalDispatchs.ClearData();
				this.callsAndDispatchsControl.gridControlExTotalCalls.ClearData();
                
            });
        }

        private void FillCallsAndDispatchs(SelectedIncidentTaskResult selectedIncidentTask)
        {
            if (selectedIncidentTask != null)
            {
                FormUtil.InvokeRequired(this, delegate
                {
                    callsAndDispatchsControl.gridControlExTotalCalls.SetDataSource(selectedIncidentTask.TotalPhoneReports);
                    callsAndDispatchsControl.gridControlExTotalDispatchs.SetDataSource(selectedIncidentTask.TotalDispatchOrders);
                });               
            }
        }

        private string GetPhoneReportIncidentTypes(PhoneReportClientData phoneReport)
        {
            string incidentTypeStr = "";
            int counter = 0;
            int incTypeLength = phoneReport.IncidentTypesCodes.Count;
            foreach (int incidentTypeCode in phoneReport.IncidentTypesCodes)
            {
                incidentTypeStr += globalIncidentTypes[incidentTypeCode].CustomCode;
                counter++;
                if (counter < incTypeLength)
                    incidentTypeStr += ", ";

            }
            return incidentTypeStr;
        }

        private void toolStripMenuItemAbout_Click(object sender, EventArgs e)
        {
            AboutForm aboutForm = new AboutForm();
            aboutForm.ShowDialog();
        }

        private bool EnsureUpdatePhoneReport()
        {
            DialogResult dialogResult = MessageForm.Show(ResourceLoader.GetString2("ChangesOnReport"), MessageFormType.Question);

            return dialogResult == DialogResult.Yes;
        }

        private void CleanForm()
        {
            CleanForm(true);
        }

        private void CleanForm(bool clearSelection)
        {
            this.questionsControl.CleanControls();
            this.callInformationControl.CleanControl();
            this.departmentsInvolvedControl.departmentTypesControl.CleanControl();

            this.incidentAdded = false;
            callEntering = false;
            IsActiveCall = false;

            this.barCheckItemIncompleteCall.Checked = false;
            this.proceduresControl.browserGenericProcedure.Clear();
            this.proceduresControl.browserProcedures.Clear();

            this.questionAnswerTextControl.RichTextArea.Clear();

            //Update the progress bar.
            repositoryItemProgressBar1.Maximum = int.Parse(GlobalApplicationPreference["AlarmTimeLimit"].Value);
            beginLimit = repositoryItemProgressBar1.Maximum / 2;

            //Cleaning the search and filters...
            this.GlobalPhoneReport = null;
            ChangeCurrentStep(0, false);
        }

        private void buttonExSameNumberIncidents_Click(object sender, EventArgs e)
        {
            
            SimpleButtonEx pressedButton = (SimpleButtonEx)sender;
            gridControlExPreviousIncidents.Focus();

			if (pressedButton.LookAndFeel.SkinName.Equals("Black")==false)
            {
                searchByText = false;
                if (this.callInformationControl.LineTelephoneNumber.ToString().Trim() != "")
                {
                    pressedButton.LookAndFeel.UseDefaultLookAndFeel = false;
					pressedButton.LookAndFeel.SkinName = "Black";
					UnHighLightOtherButtons(pressedButton);

                    FilterButtonPressed();

					if (gridControlExPreviousIncidents.SelectedItems.Count == 0)
                        ClearTextBox();
                }
                else
                {
                    MessageForm.Show(ResourceLoader.GetString2("NoPhoneNumberOnStep1"), MessageFormType.Warning);
                }
            }
        }

        private void buttonExOpenIncidentsFilter_Click(object sender, EventArgs e)
        {
            SimpleButtonEx pressedButton = (SimpleButtonEx)sender;
            gridControlExPreviousIncidents.Focus();
			

			if (pressedButton != null && pressedButton.LookAndFeel.SkinName.Equals("Black")==false)
            {
                pressedButton.LookAndFeel.UseDefaultLookAndFeel = false;
                pressedButton.LookAndFeel.SkinName = "Black";
				UnHighLightOtherButtons(pressedButton); 
				searchByText = false;

                FilterButtonPressed();
                if (gridControlExPreviousIncidents.SelectedItems.Count == 0)
                    ClearTextBox();
            }
        }
       
        private void buttonExSimilarIncidentsFilter_Click(object sender, EventArgs e)
        {

			SimpleButtonEx pressedButton = (SimpleButtonEx)sender;
            
            if(e != null && (e is UpdateSimilarIncidentsEventArgs) == false)
                gridControlExPreviousIncidents.Focus();

            if (pressedButton.LookAndFeel.SkinName.Equals("Black") == false || (e is UpdateSimilarIncidentsEventArgs))
            {                
                searchByText = false;
				ArrayList parameters = new ArrayList();
                parameters.Add("SIMILAR_INCIDENTS");
                
                if (GlobalPhoneReport != null)
                {
                    pressedButton.LookAndFeel.UseDefaultLookAndFeel = false;
                    pressedButton.LookAndFeel.SkinName = "Black";
					UnHighLightOtherButtons(pressedButton); 

					parameters.Add(GlobalPhoneReport.PhoneReportLineClient.Telephone);
                    IList incidentTypeCodes = new ArrayList();
                    if (GlobalPhoneReport.IncidentTypesCodes != null)
                        foreach (int incidentTypeCode in GlobalPhoneReport.IncidentTypesCodes)
                            incidentTypeCodes.Add(incidentTypeCode);

                    parameters.Add(incidentTypeCodes);
                    parameters.Add(QuestionsControl.IncidentAddress);
                    
                    IList<string> inRatioIncidentCodes = new List<string>();
                    if (questionsControl.IncidentAddress.Lon != 0 && questionsControl.IncidentAddress.Lat != 0)
                        inRatioIncidentCodes = ServerServiceClient.GetInstance().GetIncidentCodeInRatio(new GeoPoint(questionsControl.IncidentAddress.Lon, questionsControl.IncidentAddress.Lat),
                            double.Parse(globalApplicationPreference["SimilarIncidentsRatio"].Value),
                            DistanceUnit.Meters);
                    
                    IList<string> syncIncidents = new List<string>();
                    foreach (GridIncidentData pair in gridControlExPreviousIncidents.Items)
                    {
                        IncidentClientData incidentData = (IncidentClientData)pair.Tag;
                        if (incidentData != null && inRatioIncidentCodes.Contains(incidentData.Code.ToString()) == true && incidentData.Address.IsSynchronized == true)
                            syncIncidents.Add(incidentData.Code.ToString());
                    }
                    
                    parameters.Add(syncIncidents);
                    parameters.Add(globalApplicationPreference);
                    parameters.Add(QuestionsControl.SelectedIncidentTypes);

					incidentsCurrentFilter = new IncidentsFilter(null,parameters);
                    FilterButtonPressed();

					if (gridControlExPreviousIncidents.SelectedItems.Count == 0)
                        ClearTextBox();					
                }
            }            
        }

        private void FilterButtonPressed()
        {
            gridViewExPreviousIncidents.ClearColumnsFilter();
            CriteriaOperator filter = null;
            if (buttonExOpenIncidentsFilter.LookAndFeel.SkinName.Equals("Black"))
            {
                filter = new BinaryOperator("Status", IncidentStatusClientData.Closed.CustomCode, BinaryOperatorType.NotEqual);
            }
            else if (buttonExSameNumberIncidents.LookAndFeel.SkinName.Equals("Black"))
            {
                filter = new BinaryOperator("LineNumber", "%," + callInformationControl.LineTelephoneNumber.ToString() + ",%", BinaryOperatorType.Like);
                
            }
            else if (buttonExSimilarIncidentsFilter.LookAndFeel.SkinName.Equals("Black"))
            {
                filter = new BinaryOperator("Visible", true, BinaryOperatorType.Equal);
            }
            gridViewExPreviousIncidents.ActiveFilter.NonColumnFilterCriteria = filter;
        }

		void gridViewExPreviousIncidents_CustomRowFilter(object sender, DevExpress.XtraGrid.Views.Base.RowFilterEventArgs e)
		{
            if (buttonExSimilarIncidentsFilter.LookAndFeel.SkinName.Equals("Black")){

                GridIncidentData item = gridViewExPreviousIncidents.GetRow(e.ListSourceRow) as GridIncidentData;
			    if (item != null)
			    {
					object clientData = ((GridIncidentData)gridViewExPreviousIncidents.GetRow(e.ListSourceRow)).Tag;
					item.Visible = incidentsCurrentFilter.Filter(clientData);
				}
			}
		}

		private void UnHighLightOtherButtons(SimpleButtonEx pressedButton)
		{
			foreach (LayoutControlItem item in layoutControlGroupPreviousIncidents.Items)
				if (item.Control is SimpleButtonEx && item.Control.Name.Equals(pressedButton.Name) == false)
					(item.Control as SimpleButtonEx).LookAndFeel.SkinName = "Blue";
		}

        private void ClearTextBox()
        {
            layoutControlGroupIncidentDetails.Text = ResourceLoader.GetString2("IncidentDetails");
            searchableWebBrowser.DocumentText = "<span style='HEIGHT:100%;FONT-SIZE: 10pt; FONT-FAMILY: Sans-Serif'>" + ResourceLoader.GetString2("NoIncidentSelected") + "</span>";
			callsAndDispatchsControl.richTextBoxPhoneReportDetails.DocumentText = "<span style='HEIGHT:100%;FONT-SIZE: 10pt; FONT-FAMILY: Sans-Serif'>" + ResourceLoader.GetString2("NoPhoneReportSelected") + "</span>";
			barButtonItemPrint.Enabled = false;
			barButtonItemSave.Enabled = false;
			barButtonItemRefresh.Enabled = false;
			callsAndDispatchsControl.gridControlExTotalDispatchs.ClearData();
			callsAndDispatchsControl.gridControlExTotalCalls.ClearData();
        }
                
        private void Page_Load(object sender, System.EventArgs e)
        {
            RegistryKey regkey;
            
            try
            {
                DirectoryInfo source = new DirectoryInfo(SourceDirectory);

                //Determine whether the source directory exists.
                if (!source.Exists)
                    regkey = Registry.CurrentUser;
                else
                    regkey = Registry.Users;

                regkey = regkey.OpenSubKey(@SourceDirectory, true);

                if (regkey != null)
                {
                    // Store the existing values.			
                    IEFooter = regkey.GetValue("footer");
                    // Hide the direction (URL) from Web page footer
                    objectValue = (string)IEFooter;
                    regkey.SetValue("footer", objectValue.Replace("&u", ""));
                }
            }
            catch
            { }
        }

        private bool IsVisibleAfterFilterText(string text)
        {
            bool result = false;
            if (incidentsCurrentFilter != null)
            {
                IncidentClientData selectedIncident = null;
                FormUtil.InvokeRequired(this,
                    delegate
                    {
                        if (gridControlExPreviousIncidents.SelectedItems.Count > 0)
                        {
                            selectedIncident = ((IncidentClientData)((GridIncidentData)gridControlExPreviousIncidents.SelectedItems[0]).Tag);
                        }
                    });
                if (selectedIncident != null)
                     result = (incidentsCurrentFilter as IncidentsFilter).FilterByText(selectedIncident, text);
            }
            return result;
        }

        private void SelectedIncidentHelp(SelectedIncidentTask selectedIncidentTask, GeoPoint point)
        {
            if (point != null)
            {
                if (point.Lon != 0 && point.Lat != 0)
                {
                    ApplicationServiceClient.Current(UserApplicationClientData.Map).DisplayGeoPointInGis(point);
                }
            }
            if (selectedIncidentTask != null)
            {
                ArrayList tasks = new ArrayList();
                tasks.Add(selectedIncidentTask);
                bool sameIncident = false;
                if (selectedIncidentCode == (int)selectedIncidentTask.Parameters[0])
                    sameIncident = true;
                selectedIncidentCode = (int)selectedIncidentTask.Parameters[0];
                try
                {
                    IList result = serverServiceClient.RunTasks(tasks);
                    if (result != null && result.Count > 0 && (result[0] as IList) != null)
                    {
                        IList tempList = (IList)result[0];
                        if (tempList != null && tempList.Count > 0)
                        {
                            SelectedIncidentTaskResult selectedIncidentLightData = tempList[0] as SelectedIncidentTaskResult;
                            if (selectedIncidentLightData != null)
                            {
                                FillIncidentDetails(selectedIncidentLightData, sameIncident);
                                FillCallsAndDispatchs(selectedIncidentLightData);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    SmartLogger.Print(ex);
                }
            }
        }

        private void RefreshIncidentReport()
        {
            if (selectedIncidentCode != -1)
            {
                ArrayList parameters = new ArrayList();
                parameters.Add(selectedIncidentCode);
                SelectedIncidentTask selectedIncidentTask = new SelectedIncidentTask(parameters);
                ArrayList tasks = new ArrayList();
                tasks.Add(selectedIncidentTask);
                try
                {
                    IList result = serverServiceClient.RunTasks(tasks);
                    if (result != null && result.Count > 0 && (result[0] as IList) != null)
                    {
                        IList tempList = (IList)result[0];
                        if (tempList != null && tempList.Count > 0)
                        {
                            SelectedIncidentTaskResult selectedIncidentLightData = tempList[0] as SelectedIncidentTaskResult;
                            if (selectedIncidentLightData != null)
                            {
                                FillIncidentDetails(selectedIncidentLightData, true);
                                FillCallsAndDispatchs(selectedIncidentLightData);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    SmartLogger.Print(ex);
                }

            }
        }

        private void SetSkin()
        {
            try
            {
                string skinFile = SmartCadConfiguration.SkinsFolder + "/Clasic.xml";

                skinEngine = SkinEngine.Load(skinFile);

                ProfessionalColorTable colorTable = new CustomToolStripColors(
                    skinEngine.Colors["ToolStripGradientBegin"],
                    skinEngine.Colors["ToolStripGradientMiddle"],
                    skinEngine.Colors["ToolStripGradientEnd"],

                    skinEngine.Colors["MenuStripGradientBegin"],
                    skinEngine.Colors["MenuStripGradientEnd"],

                    skinEngine.Colors["StatusStripGradientBegin"],
                    skinEngine.Colors["StatusStripGradientEnd"]
                );


                skinEngine.AddElement("Step1Control", this.callInformationControl);
                skinEngine.AddElement("Step2Control", this.questionsControl);
                skinEngine.AddElement("Step3LeftControl", this.departmentsInvolvedControl);
                skinEngine.AddElement("Step3RightControl", this.questionAnswerTextControl);
                skinEngine.AddElement("Step4Control", this.proceduresControl);

                skinEngine.AddCompleteElement(this);

                skinEngine.ApplyChanges();
            }
            catch (Exception ex)
            {
                MessageForm.Show(ResourceLoader.GetString2("$Message.Error"), ResourceLoader.GetString2("ErrorLoadingSkin"), ex);
            }
        }

		private void SetFrontClientWaitingForCallState()
        {
            FormUtil.InvokeRequired(this, delegate
            {
                this.barButtonItemCreateNewIncident.Caption = ResourceLoader.GetString2("ToolTipButtonCreateNewIncidentRegister");
                this.barButtonItemCreateNewIncident.ItemShortcut = null;
                ((FrontClientFormDevX)ParentForm).barButtonItemCreateNewIncident.Caption = ResourceLoader.GetString2("ToolTipButtonCreateNewIncidentRegister");
				this.callInformationControl.FrontClientState = FrontClientStateEnum.WaitingForIncident;
                this.questionsControl.FrontClientState = FrontClientStateEnum.WaitingForIncident;
                this.departmentsInvolvedControl.FrontClientState = FrontClientStateEnum.WaitingForIncident;

                barButtonItemCreateNewIncident.Enabled = false;
                barButtonItemAddToIncident.Enabled = false;
                FinalizeEnabled = false;
                TelephoneStatus = TelephoneStatusType.None;

				this.barCheckItemIncompleteCall.Enabled = false;
                this.barButtonItemCallBack.Enabled = false;
				this.buttonExOpenIncidentsFilter.Enabled = false;
				this.buttonExSameNumberIncidents.Enabled = false;
				this.buttonExSimilarIncidentsFilter.Enabled = false;
                TelephonyServiceClient.GetInstance().SetVirtualCall(false);
                this.questionAnswerTextControl.EnabledButtons = false;
                readyMenuItem.Enabled = true;
            });
        }

        private void SetFrontClientRegisteringCallState()
        {
            FormUtil.InvokeRequired(this, delegate
            {
				this.barButtonItemCreateNewIncident.Caption = ResourceLoader.GetString2("ToolTipButtonCreateNewIncidentRegister");
                this.barButtonItemCreateNewIncident.ItemShortcut = null;
				((FrontClientFormDevX)ParentForm).barButtonItemCreateNewIncident.Caption = ResourceLoader.GetString2("ToolTipButtonCreateNewIncidentRegister");
				this.callInformationControl.FrontClientState = FrontClientStateEnum.RegisteringIncident;
                this.questionsControl.FrontClientState = FrontClientStateEnum.RegisteringIncident;
                this.departmentsInvolvedControl.FrontClientState = FrontClientStateEnum.RegisteringIncident;

				Reset();
				Start();
                
				barButtonItemCreateNewIncident.Enabled= true;
                barButtonItemAddToIncident.Enabled = true;

				this.buttonExOpenIncidentsFilter.Enabled = true;
				this.buttonExSameNumberIncidents.Enabled = true;
				this.buttonExSimilarIncidentsFilter.Enabled = true;

                this.questionAnswerTextControl.EnabledButtons = true;
            }); 
        }

        private void SetFrontClientUpdatingCallState()
        {
            this.incidentAdded = true;			
            this.barButtonItemCreateNewIncident.Caption =  ResourceLoader.GetString2("ToolTipButtonCreateNewIncidentUpdate");
            this.barButtonItemCreateNewIncident.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.U));
			((FrontClientFormDevX)ParentForm).barButtonItemCreateNewIncident.Caption = ResourceLoader.GetString2("ToolTipButtonCreateNewIncidentUpdate"); 
			this.callInformationControl.FrontClientState = FrontClientStateEnum.UpdatingIncident;
            this.questionsControl.FrontClientState = FrontClientStateEnum.UpdatingIncident;
            this.departmentsInvolvedControl.FrontClientState = FrontClientStateEnum.UpdatingIncident;
			this.barCheckItemIncompleteCall.Enabled = false;
            this.barButtonItemCallBack.Enabled = false;

            Stop();
            barButtonItemAddToIncident.Enabled = false;

            if (IsActiveCall == false)
                FinalizeEnabled = true;

            ChangeCurrentStep(4, true);
            //If call is in updating and incident is not closed and there is an active call then
            //call parking is enabled
            CheckEnableParkCall();
        }

        private void questionAnswerTextControl_Enter(object sender, EventArgs e)
        {
            if (this.FrontClientState != FrontClientStateEnum.WaitingForIncident)
                ChangeCurrentStep(3, false);
        }

		private void textBoxExIncidentdetails_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
		{
			KeyEventArgs keyEvent = new KeyEventArgs(e.KeyData);
			FrontClientForm_KeyDown(null, keyEvent);
		}

		private void richTextBoxPhoneReportDetails_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            KeyEventArgs keyEvent = new KeyEventArgs(e.KeyData);
            FrontClientForm_KeyDown(null, keyEvent);
        }

        private void proceduresControl_ProceduresControlPreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            KeyEventArgs keyEvent = new KeyEventArgs(e.KeyData);
            FrontClientForm_KeyDown(sender, keyEvent);
        }

        private void questionAnswerTextControl_QuestionAnswerTextControlPreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            KeyEventArgs keyEvent = new KeyEventArgs(e.KeyData);
            FrontClientForm_KeyDown(null, keyEvent);
        }

        private void frontClientButton_MouseHover(object sender, EventArgs e)
        {
            
        }

        private void frontClientButton_MouseLeave(object sender, EventArgs e)
        {
            toolTipMain.Hide((DevExpress.XtraEditors.SimpleButton)sender);
        }

        private void contextMenuStripIncidents_Opening(object sender, CancelEventArgs e)
        {
            if (gridControlExPreviousIncidents.SelectedItems.Count > 0)
                barButtonItemRefresh.Enabled = true;
            else
            {
                e.Cancel = true;
				barButtonItemRefresh.Enabled = false;
            }
        }

        private void timerSendImage_Tick(object sender, EventArgs e)
        {
            OperatorClientData oper = new OperatorClientData();
            oper.Code = ServerServiceClient.GetInstance().OperatorClient.Code;

            ApplicationImageClientData aicd = new ApplicationImageClientData();
            aicd.Operator = oper;
            aicd.Application = UserApplicationClientData.FirstLevel.Name;
            aicd.ToApplications = new string[] { UserApplicationClientData.Supervision.Name };
            aicd.ToOperators = remoteControlOperators;

            byte[] image = ApplicationUtil.GetScreenShot(this.Handle);
            aicd.OriginalLength = image.Length;

            byte[] compressImage;
            MiniLZO.Compress(image, out compressImage);
            aicd.Image = compressImage;

            ServerServiceClient.GetInstance().SendClientData(aicd);
        }

		private void barButtonItemTelephone_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			if (TelephoneStatus == TelephoneStatusType.Incoming)
			{
				this.questionsControl.FixedTexBox();
				TelephonyServiceClient.GetInstance().Answer();
				//((FrontClientFormDevX)ParentForm).barButtonItemPhoneReboot.Enabled = false;
                
                buttonExSimilarIncidentsFilter.Enabled = true;
                buttonExSameNumberIncidents.Enabled = true;
			}
			else
			{
				TelephoneStatus = TelephoneStatusType.Ended;
				TelephonyServiceClient.GetInstance().Disconnect();
			}
		}

		private void barButtonItemCreateNewIncident_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
            //if (ValidateInterface())
            //{
                BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[] { this.GetType().GetMethod("SaveOrUpdateIncident", BindingFlags.Instance | BindingFlags.NonPublic) }, new object[][] { new object[] { } });
                processForm.CanThrowError = true;
                processForm.ShowDialog();
            //}
		}

		private void barButtonItemAddToIncident_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			if (ValidateInterface() == true)
            {
                if (gridControlExPreviousIncidents.SelectedItems.Count > 0)
                {
                    try
                    {
						IncidentClientData selectedIncident = ((GridIncidentData)gridControlExPreviousIncidents.SelectedItems[0]).Tag as IncidentClientData;
                        foreach (ReportBaseDepartmentTypeClientData reportBaseDepartmentType in GlobalPhoneReport.ReportBaseDepartmentTypesClient)
							reportBaseDepartmentType.ReportBaseCode = this.GlobalPhoneReport.Code;

                        GeneratePhoneReportCaller();
                        GeneratePublicLineInformation();
                        GlobalPhoneReport.PhoneReportCallerClient.Anonymous = callInformationControl.IsAnonimousCallActivated;
                        GlobalPhoneReport.IncidentCode = selectedIncident.Code;

                        ServerServiceClient.GetInstance().SaveOrUpdateClientData(publicLineClient);
                        ServerServiceClient.GetInstance().SaveOrUpdateClientData(GlobalPhoneReport);
                        ApplicationServiceClient.Current(UserApplicationClientData.Map).SendEnableButton(false, ButtonType.AddPoint);
                    
                        updatedAnswers.Clear();
                        this.FrontClientState = FrontClientStateEnum.UpdatingIncident;
                        gridViewExPreviousIncidents_SingleSelectionChanged(gridViewExPreviousIncidents, null);
               
                    }
                    catch (FaultException ex)
                    {
                        if (ex.Message == "UK_REPORT_BASE_INCIDENT_INDEX_CODE")
                            MessageForm.Show(ResourceLoader.GetString2("MoreOperatorsAddingToExistingReport"), MessageFormType.Error);
                        else
                            throw ex;
                    }
                }
                else
					MessageForm.Show(ResourceLoader.GetString2("IncidentNotSelected"), MessageFormType.Error);
            }
		}

        string lastGlobalPhoneReport_CustomCode;
		private void barButtonItemFinalize_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			//CHECKING FOR UNSAVED CHANGES IN PHONEREPORT...
            if (updatedAnswers.Count > 0 && EnsureUpdatePhoneReport() == true)
					GlobalPhoneReport.Answers = updatedAnswers;

            //This was commented to close issue 1580
            //if (ValidateChangedQuestions() == false)
            //    MessageForm.Show(ResourceLoader.GetString2("QuestionsWereDeleted"), MessageFormType.Information);
            
			//SETTING THE OPERATOR STATUS UP...
            OperatorStatusClientData currentOperatorStatus = barButtonItemStatus.Tag as OperatorStatusClientData;
            bool setReadyOperatorStatus = false;
            if (currentOperatorStatus == defaultBusyStatus)
            {
                SetToolStripOperatorStatus(defaultReadyStatus);
                serverServiceClient.SetOperatorStatus(defaultReadyStatus);
                setReadyOperatorStatus = true;
            }
			((FrontClientFormDevX)ParentForm).barButtonItemVirtualCall.Enabled = true;
            readyMenuItem.Enabled = true;
            
            //UPDATING THE PHONEREPORT END DATETIME...
            
            GlobalPhoneReport.FinishedCallTime = serverServiceClient.GetTime();
            ServerServiceClient.GetInstance().FinalizeCall(GlobalPhoneReport);
            //TelephonyServiceClient.GetInstance().SetIsReady(true);
           
            //SETTING THE STATE OF THE FRONT CLIENT...
            buttonExOpenIncidentsFilter_Click(buttonExOpenIncidentsFilter, EventArgs.Empty);
            CleanForm();
            departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.ClearSelection();
            this.FrontClientState = FrontClientStateEnum.WaitingForIncident;     
       
            //SEND CLEAR TO MAPS
            ApplicationServiceClient.Current(UserApplicationClientData.Map).InsertAddressDataInGis(
                new AddressClientData(string.Empty, string.Empty, string.Empty, string.Empty, false));

            questionActionsUpdate.Clear();
            questionActionDelete.Clear();
            questionActionAdd.Clear();
			//((FrontClientFormDevX)ParentForm).barButtonItemPhoneReboot.Enabled = true;
            pickedUpCall = false;

            buttonExSameNumberIncidents.Enabled = false;
            buttonExSimilarIncidentsFilter.Enabled = false;
            buttonExOpenIncidentsFilter_Click(buttonExOpenIncidentsFilter, null);

            gridViewExPreviousIncidents.ClearColumnsFilter();
            if (popUpDialer != null)
            {
                popUpDialer.SoftPhone.Stop();
                popUpDialer.Hide();
            }
            isCallingBack = false;

            TelephonyServiceClient.GetInstance().SetIsReady(true);
		}

        private FileInfo[] FindAudioFiles()
        {
            if (Directory.Exists(SmartCadConfiguration.AudioRecordingFolder) == false)
                Directory.CreateDirectory(SmartCadConfiguration.AudioRecordingFolder);
            var directory = new DirectoryInfo(SmartCadConfiguration.AudioRecordingFolder);
            var myFile = directory.GetFiles()
                         .Where<FileInfo>(f => f.Extension == ".wav" && f.Name.Contains("mixed")).ToArray();
            return myFile;
        }

        private void barButtonItemStatus_ItemPress(object sender, ItemClickEventArgs e)
        {
            BarButtonItem buttonItem = e.Item as BarButtonItem;
            OperatorStatusClientData operatorStatus = barButtonItem.Tag as OperatorStatusClientData;
            if (this.FrontClientState == FrontClientStateEnum.WaitingForIncident)
            {
                if (buttonItem != null && buttonItem.Tag != defaultBusyStatus && buttonItem.Tag != defaultReadyStatus)
                {
                    if (this.FrontClientState == FrontClientStateEnum.WaitingForIncident)
                    {
                        SetToolStripOperatorStatus(defaultReadyStatus);
                    }
                    else
                    {
                        SetToolStripOperatorStatus(defaultBusyStatus);
                    }
                }
                serverServiceClient.SetOperatorStatus(defaultReadyStatus);
                operatorStatus = defaultReadyStatus;
                TelephonyServiceClient.GetInstance().SetIsReady(true);
            }
        }

        private void barButtonItemRefresh_ItemClick(object sender, ItemClickEventArgs e)
		{
			if (gridControlExPreviousIncidents.SelectedItems.Count > 0)
                OnRefreshIncidentReport();
		}

        private void barButtonItemPrint_ItemClick(object sender, ItemClickEventArgs e)
        {
            searchableWebBrowser.ShowPrintDialog();
        }

		private void barButtonItemSave_ItemClick(object sender, ItemClickEventArgs e)
		{
            searchableWebBrowser.ShowSaveAsDialog();
		}

		private void gridControlExPreviousIncidents_CellToolTipNeeded(object sender, DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventArgs e)
		{
            if (e.Info == null)
            {
                string infoText = string.Empty;
                GridHitInfo info = gridViewExPreviousIncidents.CalcHitInfo(e.ControlMousePosition);
                if (info.RowHandle > -1 && info.RowHandle != GridControlEx.InvalidRowHandle)
                {
                    IncidentClientData incidentData = (IncidentClientData)((GridIncidentData)gridViewExPreviousIncidents.GetRow(info.RowHandle)).Tag;

                    if (incidentData != null)
                        foreach (IncidentTypeClientData incidentType in incidentData.IncidentTypes)
                            if (incidentType.NoEmergency != true)
                                infoText += incidentType.FriendlyName + ", ";

                    if (infoText.Trim() != "")
                         infoText = infoText.Substring(0, infoText.Length - 2);

                    e.Info = new DevExpress.Utils.ToolTipControlInfo(new DevExpress.XtraGrid.Views.Base.CellToolTipInfo(info.RowHandle, info.Column, "cell"), infoText);
                }
            }
		}

		private void dataGridDepartmentTypes_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.Shift == true && e.KeyCode == Keys.Space)
				FrontClientForm_KeyDown(sender, e);
		}

		private void gridControlExPreviousIncidents_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.Control == true && e.KeyCode == Keys.Tab && frontClientState != FrontClientStateEnum.WaitingForIncident)
            {
                if (incidentsCurrentFilter.Parameters[0] as string == "OPEN_INCIDENTS")
                    buttonExSimilarIncidentsFilter_Click(buttonExSimilarIncidentsFilter, EventArgs.Empty);
                else if (incidentsCurrentFilter.Parameters[0] as string == "SIMILAR_INCIDENTS")
                    buttonExSameNumberIncidents_Click(buttonExSameNumberIncidents, EventArgs.Empty);
                else if (incidentsCurrentFilter.Parameters[0] as string == "SAME_NUMBER_INCIDENTS")
                    buttonExOpenIncidentsFilter_Click(buttonExOpenIncidentsFilter, EventArgs.Empty);
            }
			else if (e.KeyCode == Keys.Tab && e.Shift)
			{
			}
			else if (e.KeyCode == Keys.Home)
			{
			}
			else if (e.KeyCode == Keys.End)
			{
			}
			else if (e.KeyCode == Keys.F5)
				barButtonItemRefresh.PerformClick();
			else
				FrontClientForm_KeyDown(null, e);
		}

		private void gridViewExPreviousIncidents_SelectionWillChange(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
		{
			barButtonItemSave.Enabled = false;
            barButtonItemPrint.Enabled = false;
            barButtonItemRefresh.Enabled = false;

            this.callsAndDispatchsControl.gridControlExTotalDispatchs.ClearData();
			this.callsAndDispatchsControl.gridControlExTotalCalls.ClearData();
            if (searchableWebBrowser.IsDisposed == false)
                searchableWebBrowser.DocumentText = "<span style='HEIGHT:100%;FONT-SIZE: 10pt; FONT-FAMILY: Sans-Serif'>" + ResourceLoader.GetString2("NoIncidentSelected") + "</span>";
			if (callsAndDispatchsControl.richTextBoxPhoneReportDetails.IsDisposed == false)
				callsAndDispatchsControl.richTextBoxPhoneReportDetails.DocumentText = "<span style='HEIGHT:100%;FONT-SIZE: 10pt; FONT-FAMILY: Sans-Serif'>" + ResourceLoader.GetString2("NoPhoneReportSelected") + "</span>";
   
            FormUtil.InvokeRequired(this,
               delegate
               {
                   this.layoutControlGroupIncidentDetails.Text = ResourceLoader.GetString2("IncidentDetails");
               });
		}


        private void gridViewExPreviousIncidents_SingleSelectionChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            FormUtil.InvokeRequired(this,
                delegate
                {

                    if (gridControlExPreviousIncidents.SelectedItems.Count > 0)
                    {
                        IncidentClientData selectedIncident = (IncidentClientData)((GridIncidentData)gridControlExPreviousIncidents.SelectedItems[0]).Tag;

                        ArrayList parameters = new ArrayList();
                        parameters.Add(selectedIncident.Code);
                        parameters.Add(selectedIncident.IncidentTypes);

                        SelectedIncidentTask selectedIncidentTask = new SelectedIncidentTask(parameters);
                        ArrayList tasks = new ArrayList();
                        tasks.Add(selectedIncidentTask);

                        GeoPoint point = null;
                        AddressClientData address = selectedIncident.Address;
                        if (address != null && address.Lon != 0 && address.Lat != 0)
                            point = new GeoPoint(address.Lon, address.Lat);

                        this.callsAndDispatchsControl.gridControlExTotalCalls.ClearData();
                        this.callsAndDispatchsControl.gridControlExTotalDispatchs.ClearData();

                        ThreadPool.QueueUserWorkItem(delegate(object state)
                        {
                            try
                            {
                                // prevSimilarIncidentSel = (this.dataGridExPreviousIncidents.SelectedItems[0].Tag as IncidentClientData);
                                SelectedIncidentHelp(selectedIncidentTask, point);
                            }
                            catch (Exception ex)
                            {
                                MessageForm.Show(ex.Message, ex);
                            }
                        });

                        barButtonItemSave.Enabled = true;
                        barButtonItemPrint.Enabled = true;
                        barButtonItemRefresh.Enabled = true;

                    }
                    else
                    {
                        barButtonItemSave.Enabled = false;
                        barButtonItemPrint.Enabled = false;
                        barButtonItemRefresh.Enabled = false;

                        this.callsAndDispatchsControl.gridControlExTotalDispatchs.ClearData();
                        this.callsAndDispatchsControl.gridControlExTotalCalls.ClearData();

                        //this.textBoxExIncidentdetails.Clear();
                        searchableWebBrowser.DocumentText = "<span style='HEIGHT:100%;FONT-SIZE: 10pt; FONT-FAMILY: Sans-Serif'>" + ResourceLoader.GetString2("NoIncidentSelected") + "</span>";
                        //this.richTextBoxPhoneReportDetails.Clear();
                        callsAndDispatchsControl.richTextBoxPhoneReportDetails.DocumentText = "<span style='HEIGHT:100%;FONT-SIZE: 10pt; FONT-FAMILY: Sans-Serif'>" + ResourceLoader.GetString2("NoPhoneReportSelected") + "</span>";
                    }
                });
        }

		private void barCheckItemIncompleteCall_CheckedChanged(object sender, ItemClickEventArgs e)
		{
			BarCheckItem checkEdit = sender as BarCheckItem;
			if ( checkEdit!= null)
                this.GlobalPhoneReport.Incomplete = checkEdit.Checked ;
		}

        private void DefaultFrontClientFormDevX_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
            }
            if (e.Cancel)
            {
                popUpDialer.Close();
                this.parentForm.FrontClientFormDevXCommittedChanges -=new EventHandler<CommittedObjectDataCollectionEventArgs>(serverService_CommittedChanges);
            }
        }

		private void timer_Tick(object state)
		{
			try
            {
                if (stopped == false)
                {
                    FormUtil.InvokeRequired(this, new MethodInvoker(SeccondAdded));
                }
			}
			catch (Exception e)
			{
			}
		}

		private void SeccondAdded()
		{
			progress = progress.Add(new TimeSpan(0, 0, 1));
			barEditItemChronometer.EditValue = progress.ToString();
			barEditItemProgress.EditValue = ((int)barEditItemProgress.EditValue) + 1;

            if ((int)barEditItemProgress.EditValue == 1)
            {
                ((RepositoryItemProgressBar)barEditItemProgress.Edit).StartColor = Color.Green;
                ((RepositoryItemProgressBar)barEditItemProgress.Edit).EndColor = Color.Green;
            }
            else if ((int)barEditItemProgress.EditValue == beginLimit)
            {
                ((RepositoryItemProgressBar)barEditItemProgress.Edit).StartColor = Color.Yellow;
                ((RepositoryItemProgressBar)barEditItemProgress.Edit).EndColor = Color.Yellow;
            }
            else if ((int)barEditItemProgress.EditValue == repositoryItemProgressBar1.Maximum)
            {
                ((RepositoryItemProgressBar)barEditItemProgress.Edit).StartColor = Color.Red;
                ((RepositoryItemProgressBar)barEditItemProgress.
                    
                    Edit).EndColor = Color.Red;
            }
		}

		public void Start()
		{
			stopped = false;
			progressTimer.Change(1000, 1000);
		}

		public void Stop()
		{
			stopped = true;
			progressTimer.Change(Timeout.Infinite, Timeout.Infinite);
		}

		public void Reset()
		{
			progress = new TimeSpan();
			barEditItemChronometer.EditValue = progress.ToString();
			barEditItemProgress.EditValue = 0;
		}

        private void listBoxEx_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((ModifierKeys & Keys.Control) == Keys.Control)
            {
                e.Handled=true;
            }
        }

        private void DefaultFrontClientFormDevX_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Modifiers == Keys.Control)
            {
                if (e.KeyCode == Keys.F)
                {
                    searchableWebBrowser.Focus();
                }
            }
        }

        internal void SetReady()
        {
            OperatorStatusClientData operatorStatus = defaultReadyStatus;
            SetToolStripOperatorStatus(operatorStatus);
            serverServiceClient.SetOperatorStatus(operatorStatus);
        }
    }
}
