using Microsoft.Win32;
using SmartCadControls;
using SmartCadControls.Controls;
using SmartCadCore.Enums;
using SmartCadFirstLevel.Gui;
using SmartCadGuiCommon.Controls;
using System.IO;

namespace SmartCadGuiCommon
{
    partial class DefaultFrontClientFormDevX
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private new System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();

                RegistryKey regkey;
               
                try
                {
                    DirectoryInfo source = new DirectoryInfo(SourceDirectory);

                    //Determine whether the source directory exists.
                    if (!source.Exists)
                        regkey = Registry.Users;
                    else
                        regkey = Registry.CurrentUser;

                    // Restore the Print Page Setup properties
                    regkey = regkey.OpenSubKey(@SourceDirectory, true);
                    if (regkey != null)
                    {
                        // Restore the footer values.			
                        regkey.SetValue("footer", IEFooter.ToString());
                    }
                }
                catch
                { }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DefaultFrontClientFormDevX));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.toolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.saveFileDialogMain = new System.Windows.Forms.SaveFileDialog();
            this.printDialogMain = new System.Windows.Forms.PrintDialog();
            this.buttonExSimilarIncidentsFilter = new SmartCadControls.Controls.SimpleButtonEx();
            this.layoutControlDockPanel = new DevExpress.XtraLayout.LayoutControl();
            this.searchableWebBrowser = new SmartCadControls.Controls.SearchableWebBrowser();
            this.gridControlExPreviousIncidents = new SmartCadControls.GridControlEx();
            this.gridViewExPreviousIncidents = new SmartCadControls.GridViewEx();
            this.callsAndDispatchsControl = new SmartCadGuiCommon.Controls.CallsAndDispatchsControl();
            this.buttonExSameNumberIncidents = new SmartCadControls.Controls.SimpleButtonEx();
            this.buttonExOpenIncidentsFilter = new SmartCadControls.Controls.SimpleButtonEx();
            this.layoutControlGroupDockPanel = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupPreviousIncidents = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemButtonSimilar = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemSameNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemOpenIncidents = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItemGridControlPreviousIncidents = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupIncidentDetails = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupCallsAndDispatchs = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemCallsAndDispatchs = new DevExpress.XtraLayout.LayoutControlItem();
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItemStatus = new DevExpress.XtraBars.BarButtonItem();
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barButtonItemPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSave = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItemProgress = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemProgressBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.barButtonItemTelephone = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemCreateNewIncident = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemAddToIncident = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemFinalize = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItemChronometer = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemTimeEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.barCheckItemIncompleteCall = new DevExpress.XtraBars.BarCheckItem();
            this.barButtonItemCallBack = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItemTimeBar = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChonometer = new DevExpress.XtraBars.BarStaticItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroupCallInfo = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupUser = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupGeneralOptions = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.callInformationControl = new SmartCadGuiCommon.Controls.CallInformationControl();
            this.proceduresControl = new SmartCadControls.Controls.ProceduresControl();
            this.questionAnswerTextControl = new SmartCadControls.Controls.QuestionAnswerTextControl();
            this.departmentsInvolvedControl = new SmartCadGuiCommon.Controls.DepartmentsInvolvedControl();
            this.timerSendImage = new System.Windows.Forms.Timer(this.components);
            this.barButtonGroupTelephone = new DevExpress.XtraBars.BarButtonGroup();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanel1 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.layoutControlMainWindow = new DevExpress.XtraLayout.LayoutControl();
            this.questionsControl = new SmartCadGuiCommon.Controls.QuestionsControlDevX();
            this.layoutControlGroupMainWindow = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemCallInformation = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemDepartmentsInvolved = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemQuestionAnswer = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemProcedures = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemQuestionsControl = new DevExpress.XtraLayout.LayoutControlItem();
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlDockPanel)).BeginInit();
            this.layoutControlDockPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExPreviousIncidents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExPreviousIncidents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupDockPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupPreviousIncidents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemButtonSimilar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSameNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOpenIncidents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGridControlPreviousIncidents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupIncidentDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupCallsAndDispatchs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCallsAndDispatchs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanel1.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlMainWindow)).BeginInit();
            this.layoutControlMainWindow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupMainWindow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCallInformation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartmentsInvolved)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemQuestionAnswer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemProcedures)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemQuestionsControl)).BeginInit();
            this.SuspendLayout();
            // 
            // printDialogMain
            // 
            this.printDialogMain.UseEXDialog = true;
            // 
            // buttonExSimilarIncidentsFilter
            // 
            this.buttonExSimilarIncidentsFilter.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExSimilarIncidentsFilter.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonExSimilarIncidentsFilter.Appearance.Options.UseFont = true;
            this.buttonExSimilarIncidentsFilter.Appearance.Options.UseForeColor = true;
            this.buttonExSimilarIncidentsFilter.Enabled = false;
            this.buttonExSimilarIncidentsFilter.FontFormat = SmartCadControls.Controls.FontFormat.GetFormat("ButtonFormat");
            this.buttonExSimilarIncidentsFilter.Image = ((System.Drawing.Image)(resources.GetObject("buttonExSimilarIncidentsFilter.Image")));
            this.buttonExSimilarIncidentsFilter.Location = new System.Drawing.Point(14, 35);
            this.buttonExSimilarIncidentsFilter.Name = "buttonExSimilarIncidentsFilter";
            this.buttonExSimilarIncidentsFilter.Size = new System.Drawing.Size(91, 28);
            this.buttonExSimilarIncidentsFilter.StyleController = this.layoutControlDockPanel;
            this.buttonExSimilarIncidentsFilter.TabIndex = 0;
            this.buttonExSimilarIncidentsFilter.Click += new System.EventHandler(this.buttonExSimilarIncidentsFilter_Click);
            // 
            // layoutControlDockPanel
            // 
            this.layoutControlDockPanel.AllowCustomizationMenu = false;
            this.layoutControlDockPanel.Controls.Add(this.searchableWebBrowser);
            this.layoutControlDockPanel.Controls.Add(this.gridControlExPreviousIncidents);
            this.layoutControlDockPanel.Controls.Add(this.callsAndDispatchsControl);
            this.layoutControlDockPanel.Controls.Add(this.buttonExSimilarIncidentsFilter);
            this.layoutControlDockPanel.Controls.Add(this.buttonExSameNumberIncidents);
            this.layoutControlDockPanel.Controls.Add(this.buttonExOpenIncidentsFilter);
            this.layoutControlDockPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlDockPanel.Location = new System.Drawing.Point(0, 0);
            this.layoutControlDockPanel.LookAndFeel.SkinName = "Blue";
            this.layoutControlDockPanel.LookAndFeel.UseDefaultLookAndFeel = false;
            this.layoutControlDockPanel.Name = "layoutControlDockPanel";
            this.layoutControlDockPanel.Root = this.layoutControlGroupDockPanel;
            this.layoutControlDockPanel.Size = new System.Drawing.Size(356, 748);
            this.layoutControlDockPanel.TabIndex = 0;
            this.layoutControlDockPanel.Text = "layoutControlDocPanel";
            // 
            // searchableWebBrowser
            // 
            this.searchableWebBrowser.AllowWebBrowserDrop = true;
            this.searchableWebBrowser.DocumentText = "<HTML></HTML>\0";
            this.searchableWebBrowser.IsWebBrowserContextMenuEnabled = true;
            this.searchableWebBrowser.Location = new System.Drawing.Point(7, 279);
            this.searchableWebBrowser.Margin = new System.Windows.Forms.Padding(0);
            this.searchableWebBrowser.Name = "searchableWebBrowser";
            this.searchableWebBrowser.Size = new System.Drawing.Size(342, 240);
            this.searchableWebBrowser.TabIndex = 11;
            this.searchableWebBrowser.WebBrowserShortcutsEnabled = true;
            this.searchableWebBrowser.XmlText = "<HTML></HTML>\0";
            // 
            // gridControlExPreviousIncidents
            // 
            this.gridControlExPreviousIncidents.EnableAutoFilter = true;
            this.gridControlExPreviousIncidents.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExPreviousIncidents.Location = new System.Drawing.Point(14, 67);
            this.gridControlExPreviousIncidents.MainView = this.gridViewExPreviousIncidents;
            this.gridControlExPreviousIncidents.Name = "gridControlExPreviousIncidents";
            this.gridControlExPreviousIncidents.Size = new System.Drawing.Size(328, 170);
            this.gridControlExPreviousIncidents.TabIndex = 3;
            this.gridControlExPreviousIncidents.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExPreviousIncidents});
            this.gridControlExPreviousIncidents.ViewTotalRows = true;
            this.gridControlExPreviousIncidents.CellToolTipNeeded += new DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventHandler(this.gridControlExPreviousIncidents_CellToolTipNeeded);
            this.gridControlExPreviousIncidents.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridControlExPreviousIncidents_KeyDown);
            // 
            // gridViewExPreviousIncidents
            // 
            this.gridViewExPreviousIncidents.AllowFocusedRowChanged = true;
            this.gridViewExPreviousIncidents.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExPreviousIncidents.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExPreviousIncidents.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExPreviousIncidents.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExPreviousIncidents.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewExPreviousIncidents.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewExPreviousIncidents.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExPreviousIncidents.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExPreviousIncidents.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExPreviousIncidents.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExPreviousIncidents.EnablePreviewLineForFocusedRow = false;
            this.gridViewExPreviousIncidents.GridControl = this.gridControlExPreviousIncidents;
            this.gridViewExPreviousIncidents.Name = "gridViewExPreviousIncidents";
            this.gridViewExPreviousIncidents.OptionsMenu.EnableFooterMenu = false;
            this.gridViewExPreviousIncidents.OptionsNavigation.UseTabKey = false;
            this.gridViewExPreviousIncidents.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExPreviousIncidents.OptionsView.ShowDetailButtons = false;
            this.gridViewExPreviousIncidents.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExPreviousIncidents.OptionsView.ShowFooter = true;
            this.gridViewExPreviousIncidents.ViewTotalRows = true;
            this.gridViewExPreviousIncidents.SelectionWillChange += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewExPreviousIncidents_SelectionWillChange);
            this.gridViewExPreviousIncidents.SingleSelectionChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewExPreviousIncidents_SingleSelectionChanged);
            this.gridViewExPreviousIncidents.CustomRowFilter += new DevExpress.XtraGrid.Views.Base.RowFilterEventHandler(this.gridViewExPreviousIncidents_CustomRowFilter);
            // 
            // callsAndDispatchsControl
            // 
            this.callsAndDispatchsControl.Location = new System.Drawing.Point(14, 561);
            this.callsAndDispatchsControl.Name = "callsAndDispatchsControl";
            this.callsAndDispatchsControl.Size = new System.Drawing.Size(328, 173);
            this.callsAndDispatchsControl.TabIndex = 6;
            // 
            // buttonExSameNumberIncidents
            // 
            this.buttonExSameNumberIncidents.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExSameNumberIncidents.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonExSameNumberIncidents.Appearance.Options.UseFont = true;
            this.buttonExSameNumberIncidents.Appearance.Options.UseForeColor = true;
            this.buttonExSameNumberIncidents.Enabled = false;
            this.buttonExSameNumberIncidents.FontFormat = SmartCadControls.Controls.FontFormat.GetFormat("ButtonFormat");
            this.buttonExSameNumberIncidents.Image = ((System.Drawing.Image)(resources.GetObject("buttonExSameNumberIncidents.Image")));
            this.buttonExSameNumberIncidents.Location = new System.Drawing.Point(109, 35);
            this.buttonExSameNumberIncidents.Name = "buttonExSameNumberIncidents";
            this.buttonExSameNumberIncidents.Size = new System.Drawing.Size(91, 28);
            this.buttonExSameNumberIncidents.StyleController = this.layoutControlDockPanel;
            this.buttonExSameNumberIncidents.TabIndex = 1;
            this.buttonExSameNumberIncidents.Click += new System.EventHandler(this.buttonExSameNumberIncidents_Click);
            // 
            // buttonExOpenIncidentsFilter
            // 
            this.buttonExOpenIncidentsFilter.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExOpenIncidentsFilter.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonExOpenIncidentsFilter.Appearance.Options.UseFont = true;
            this.buttonExOpenIncidentsFilter.Appearance.Options.UseForeColor = true;
            this.buttonExOpenIncidentsFilter.FontFormat = SmartCadControls.Controls.FontFormat.GetFormat("ButtonFormat");
            this.buttonExOpenIncidentsFilter.Image = ((System.Drawing.Image)(resources.GetObject("buttonExOpenIncidentsFilter.Image")));
            this.buttonExOpenIncidentsFilter.Location = new System.Drawing.Point(204, 35);
            this.buttonExOpenIncidentsFilter.Name = "buttonExOpenIncidentsFilter";
            this.buttonExOpenIncidentsFilter.Size = new System.Drawing.Size(91, 28);
            this.buttonExOpenIncidentsFilter.StyleController = this.layoutControlDockPanel;
            this.buttonExOpenIncidentsFilter.TabIndex = 2;
            this.buttonExOpenIncidentsFilter.Click += new System.EventHandler(this.buttonExOpenIncidentsFilter_Click);
            // 
            // layoutControlGroupDockPanel
            // 
            this.layoutControlGroupDockPanel.CustomizationFormText = "layoutControlGroupDockPanel";
            this.layoutControlGroupDockPanel.GroupBordersVisible = false;
            this.layoutControlGroupDockPanel.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupPreviousIncidents,
            this.layoutControlGroupIncidentDetails,
            this.layoutControlGroupCallsAndDispatchs});
            this.layoutControlGroupDockPanel.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupDockPanel.Name = "layoutControlGroupDockPanel";
            this.layoutControlGroupDockPanel.Size = new System.Drawing.Size(356, 748);
            this.layoutControlGroupDockPanel.Text = "layoutControlGroupDockPanel";
            this.layoutControlGroupDockPanel.TextVisible = false;
            // 
            // layoutControlGroupPreviousIncidents
            // 
            this.layoutControlGroupPreviousIncidents.CustomizationFormText = "layoutControlGroupPreviousIncidents";
            this.layoutControlGroupPreviousIncidents.ExpandButtonMode = DevExpress.Utils.Controls.ExpandButtonMode.Inverted;
            this.layoutControlGroupPreviousIncidents.ExpandButtonVisible = true;
            this.layoutControlGroupPreviousIncidents.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemButtonSimilar,
            this.layoutControlItemSameNumber,
            this.layoutControlItemOpenIncidents,
            this.emptySpaceItem1,
            this.layoutControlItemGridControlPreviousIncidents});
            this.layoutControlGroupPreviousIncidents.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupPreviousIncidents.Name = "layoutControlGroupPreviousIncidents";
            this.layoutControlGroupPreviousIncidents.Size = new System.Drawing.Size(356, 251);
            this.layoutControlGroupPreviousIncidents.Text = "layoutControlGroupPreviousIncidents";
            // 
            // layoutControlItemButtonSimilar
            // 
            this.layoutControlItemButtonSimilar.Control = this.buttonExSimilarIncidentsFilter;
            this.layoutControlItemButtonSimilar.CustomizationFormText = "layoutControlItemButtonSimilar";
            this.layoutControlItemButtonSimilar.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemButtonSimilar.MaxSize = new System.Drawing.Size(95, 32);
            this.layoutControlItemButtonSimilar.MinSize = new System.Drawing.Size(95, 32);
            this.layoutControlItemButtonSimilar.Name = "layoutControlItemButtonSimilar";
            this.layoutControlItemButtonSimilar.Size = new System.Drawing.Size(95, 32);
            this.layoutControlItemButtonSimilar.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemButtonSimilar.Text = "layoutControlItemButtonSimilar";
            this.layoutControlItemButtonSimilar.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemButtonSimilar.TextToControlDistance = 0;
            this.layoutControlItemButtonSimilar.TextVisible = false;
            // 
            // layoutControlItemSameNumber
            // 
            this.layoutControlItemSameNumber.Control = this.buttonExSameNumberIncidents;
            this.layoutControlItemSameNumber.CustomizationFormText = "layoutControlItemSameNumber";
            this.layoutControlItemSameNumber.Location = new System.Drawing.Point(95, 0);
            this.layoutControlItemSameNumber.MaxSize = new System.Drawing.Size(95, 32);
            this.layoutControlItemSameNumber.MinSize = new System.Drawing.Size(95, 32);
            this.layoutControlItemSameNumber.Name = "layoutControlItemSameNumber";
            this.layoutControlItemSameNumber.Size = new System.Drawing.Size(95, 32);
            this.layoutControlItemSameNumber.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemSameNumber.Text = "layoutControlItemSameNumber";
            this.layoutControlItemSameNumber.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemSameNumber.TextToControlDistance = 0;
            this.layoutControlItemSameNumber.TextVisible = false;
            // 
            // layoutControlItemOpenIncidents
            // 
            this.layoutControlItemOpenIncidents.Control = this.buttonExOpenIncidentsFilter;
            this.layoutControlItemOpenIncidents.CustomizationFormText = "layoutControlItemOpenIncidents";
            this.layoutControlItemOpenIncidents.Location = new System.Drawing.Point(190, 0);
            this.layoutControlItemOpenIncidents.MaxSize = new System.Drawing.Size(95, 32);
            this.layoutControlItemOpenIncidents.MinSize = new System.Drawing.Size(95, 32);
            this.layoutControlItemOpenIncidents.Name = "layoutControlItemOpenIncidents";
            this.layoutControlItemOpenIncidents.Size = new System.Drawing.Size(95, 32);
            this.layoutControlItemOpenIncidents.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemOpenIncidents.Text = "layoutControlItemOpenIncidents";
            this.layoutControlItemOpenIncidents.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemOpenIncidents.TextToControlDistance = 0;
            this.layoutControlItemOpenIncidents.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(285, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(47, 32);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItemGridControlPreviousIncidents
            // 
            this.layoutControlItemGridControlPreviousIncidents.Control = this.gridControlExPreviousIncidents;
            this.layoutControlItemGridControlPreviousIncidents.CustomizationFormText = "layoutControlItemGridControlPreviousIncidents";
            this.layoutControlItemGridControlPreviousIncidents.Location = new System.Drawing.Point(0, 32);
            this.layoutControlItemGridControlPreviousIncidents.Name = "layoutControlItemGridControlPreviousIncidents";
            this.layoutControlItemGridControlPreviousIncidents.Size = new System.Drawing.Size(332, 174);
            this.layoutControlItemGridControlPreviousIncidents.Text = "layoutControlItemGridControlPreviousIncidents";
            this.layoutControlItemGridControlPreviousIncidents.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemGridControlPreviousIncidents.TextToControlDistance = 0;
            this.layoutControlItemGridControlPreviousIncidents.TextVisible = false;
            // 
            // layoutControlGroupIncidentDetails
            // 
            this.layoutControlGroupIncidentDetails.CustomizationFormText = "layoutControlGroupIncidentDetails";
            this.layoutControlGroupIncidentDetails.ExpandButtonMode = DevExpress.Utils.Controls.ExpandButtonMode.Inverted;
            this.layoutControlGroupIncidentDetails.ExpandButtonVisible = true;
            this.layoutControlGroupIncidentDetails.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroupIncidentDetails.Location = new System.Drawing.Point(0, 251);
            this.layoutControlGroupIncidentDetails.Name = "layoutControlGroupIncidentDetails";
            this.layoutControlGroupIncidentDetails.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupIncidentDetails.Size = new System.Drawing.Size(356, 275);
            this.layoutControlGroupIncidentDetails.Text = "layoutControlGroupIncidentDetails";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.searchableWebBrowser;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(346, 244);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroupCallsAndDispatchs
            // 
            this.layoutControlGroupCallsAndDispatchs.CustomizationFormText = "layoutControlGroupCallsAndDispatchs";
            this.layoutControlGroupCallsAndDispatchs.ExpandButtonMode = DevExpress.Utils.Controls.ExpandButtonMode.Inverted;
            this.layoutControlGroupCallsAndDispatchs.ExpandButtonVisible = true;
            this.layoutControlGroupCallsAndDispatchs.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemCallsAndDispatchs});
            this.layoutControlGroupCallsAndDispatchs.Location = new System.Drawing.Point(0, 526);
            this.layoutControlGroupCallsAndDispatchs.Name = "layoutControlGroupCallsAndDispatchs";
            this.layoutControlGroupCallsAndDispatchs.Size = new System.Drawing.Size(356, 222);
            this.layoutControlGroupCallsAndDispatchs.Text = "layoutControlGroupCallsAndDispatchs";
            // 
            // layoutControlItemCallsAndDispatchs
            // 
            this.layoutControlItemCallsAndDispatchs.Control = this.callsAndDispatchsControl;
            this.layoutControlItemCallsAndDispatchs.CustomizationFormText = "layoutControlItemCallsAndDispatchs";
            this.layoutControlItemCallsAndDispatchs.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemCallsAndDispatchs.Name = "layoutControlItemCallsAndDispatchs";
            this.layoutControlItemCallsAndDispatchs.Size = new System.Drawing.Size(332, 177);
            this.layoutControlItemCallsAndDispatchs.Text = "layoutControlItemCallsAndDispatchs";
            this.layoutControlItemCallsAndDispatchs.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemCallsAndDispatchs.TextToControlDistance = 0;
            this.layoutControlItemCallsAndDispatchs.TextVisible = false;
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.Dock = System.Windows.Forms.DockStyle.None;
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.ExpandCollapseItem.Name = "";
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.barButtonItemStatus,
            this.barButtonItemPrint,
            this.barButtonItemSave,
            this.barButtonItemRefresh,
            this.barEditItemProgress,
            this.barButtonItemTelephone,
            this.barButtonItemCreateNewIncident,
            this.barButtonItemAddToIncident,
            this.barButtonItemFinalize,
            this.barEditItemChronometer,
            this.barCheckItemIncompleteCall,
            this.barButtonItemCallBack,
            this.barStaticItemTimeBar,
            this.barStaticItemChonometer});
            this.ribbonControl1.ItemsVertAlign = DevExpress.Utils.VertAlignment.Top;
            this.ribbonControl1.Location = new System.Drawing.Point(-150, 635);
            this.ribbonControl1.MaxItemId = 23;
            this.ribbonControl1.MdiMergeStyle = DevExpress.XtraBars.Ribbon.RibbonMdiMergeStyle.Always;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbonControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTimeEdit1,
            this.repositoryItemProgressBar1});
            this.ribbonControl1.ShowToolbarCustomizeItem = false;
            this.ribbonControl1.Size = new System.Drawing.Size(964, 119);
            this.ribbonControl1.Toolbar.ShowCustomizeItem = false;
            this.ribbonControl1.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // barButtonItemStatus
            // 
            this.barButtonItemStatus.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.barButtonItemStatus.Caption = "ItemStatus";
            this.barButtonItemStatus.DropDownControl = this.popupMenu1;
            this.barButtonItemStatus.Id = 3;
            this.barButtonItemStatus.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemStatus.Name = "barButtonItemStatus";
            this.barButtonItemStatus.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemStatus.ItemPress += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemStatus_ItemPress);
            // 
            // popupMenu1
            // 
            this.popupMenu1.Name = "popupMenu1";
            // 
            // barButtonItemPrint
            // 
            this.barButtonItemPrint.Caption = "ItemPrint";
            this.barButtonItemPrint.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.Glyph")));
            this.barButtonItemPrint.Id = 6;
            this.barButtonItemPrint.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemPrint.Name = "barButtonItemPrint";
            this.barButtonItemPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPrint_ItemClick);
            // 
            // barButtonItemSave
            // 
            this.barButtonItemSave.Caption = "ItemSave";
            this.barButtonItemSave.Glyph = global::SmartCadGuiCommon.PrintRibbonControllerResources.RibbonPrintPreview_Save;
            this.barButtonItemSave.Id = 7;
            this.barButtonItemSave.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemSave.Name = "barButtonItemSave";
            this.barButtonItemSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemSave_ItemClick);
            // 
            // barButtonItemRefresh
            // 
            this.barButtonItemRefresh.Caption = "ItemRefresh";
            this.barButtonItemRefresh.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemRefresh.Glyph")));
            this.barButtonItemRefresh.Id = 8;
            this.barButtonItemRefresh.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemRefresh.Name = "barButtonItemRefresh";
            this.barButtonItemRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemRefresh_ItemClick);
            // 
            // barEditItemProgress
            // 
            this.barEditItemProgress.CanOpenEdit = false;
            this.barEditItemProgress.Edit = this.repositoryItemProgressBar1;
            this.barEditItemProgress.Id = 11;
            this.barEditItemProgress.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barEditItemProgress.Name = "barEditItemProgress";
            this.barEditItemProgress.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barEditItemProgress.Width = 120;
            // 
            // repositoryItemProgressBar1
            // 
            this.repositoryItemProgressBar1.Appearance.BorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.repositoryItemProgressBar1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.repositoryItemProgressBar1.EndColor = System.Drawing.Color.Lime;
            this.repositoryItemProgressBar1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            this.repositoryItemProgressBar1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.repositoryItemProgressBar1.Name = "repositoryItemProgressBar1";
            this.repositoryItemProgressBar1.ProgressViewStyle = DevExpress.XtraEditors.Controls.ProgressViewStyle.Solid;
            this.repositoryItemProgressBar1.StartColor = System.Drawing.Color.Lime;
            this.repositoryItemProgressBar1.Step = 1;
            // 
            // barButtonItemTelephone
            // 
            this.barButtonItemTelephone.Caption = "ItemTelephone";
            this.barButtonItemTelephone.Id = 12;
            this.barButtonItemTelephone.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemTelephone.LargeGlyph")));
            this.barButtonItemTelephone.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemTelephone.Name = "barButtonItemTelephone";
            this.barButtonItemTelephone.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemTelephone.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemTelephone_ItemClick);
            // 
            // barButtonItemCreateNewIncident
            // 
            this.barButtonItemCreateNewIncident.Caption = "ItemCreateNewIncident";
            this.barButtonItemCreateNewIncident.Id = 13;
            this.barButtonItemCreateNewIncident.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemCreateNewIncident.LargeGlyph")));
            this.barButtonItemCreateNewIncident.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemCreateNewIncident.Name = "barButtonItemCreateNewIncident";
            this.barButtonItemCreateNewIncident.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemCreateNewIncident.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemCreateNewIncident_ItemClick);
            // 
            // barButtonItemAddToIncident
            // 
            this.barButtonItemAddToIncident.Caption = "ItemAddToIncident";
            this.barButtonItemAddToIncident.Id = 14;
            this.barButtonItemAddToIncident.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemAddToIncident.LargeGlyph")));
            this.barButtonItemAddToIncident.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemAddToIncident.Name = "barButtonItemAddToIncident";
            this.barButtonItemAddToIncident.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemAddToIncident.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemAddToIncident_ItemClick);
            // 
            // barButtonItemFinalize
            // 
            this.barButtonItemFinalize.Caption = "ItemFinalize";
            this.barButtonItemFinalize.Id = 15;
            this.barButtonItemFinalize.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemFinalize.LargeGlyph")));
            this.barButtonItemFinalize.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemFinalize.Name = "barButtonItemFinalize";
            this.barButtonItemFinalize.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemFinalize.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemFinalize_ItemClick);
            // 
            // barEditItemChronometer
            // 
            this.barEditItemChronometer.AutoHideEdit = false;
            this.barEditItemChronometer.CanOpenEdit = false;
            this.barEditItemChronometer.Edit = this.repositoryItemTimeEdit1;
            this.barEditItemChronometer.EditValue = "00:00:00";
            this.barEditItemChronometer.Id = 17;
            this.barEditItemChronometer.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barEditItemChronometer.Name = "barEditItemChronometer";
            this.barEditItemChronometer.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barEditItemChronometer.Width = 90;
            // 
            // repositoryItemTimeEdit1
            // 
            this.repositoryItemTimeEdit1.AllowFocused = false;
            this.repositoryItemTimeEdit1.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemTimeEdit1.Appearance.Font = new System.Drawing.Font("MS Reference Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.repositoryItemTimeEdit1.Appearance.Options.UseFont = true;
            this.repositoryItemTimeEdit1.AutoHeight = false;
            this.repositoryItemTimeEdit1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.repositoryItemTimeEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, false)});
            this.repositoryItemTimeEdit1.Mask.EditMask = "";
            this.repositoryItemTimeEdit1.Name = "repositoryItemTimeEdit1";
            // 
            // barCheckItemIncompleteCall
            // 
            this.barCheckItemIncompleteCall.Caption = "barCheckItemIncompleteCall";
            this.barCheckItemIncompleteCall.Id = 21;
            this.barCheckItemIncompleteCall.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barCheckItemIncompleteCall.LargeGlyph")));
            this.barCheckItemIncompleteCall.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barCheckItemIncompleteCall.Name = "barCheckItemIncompleteCall";
            this.barCheckItemIncompleteCall.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barCheckItemIncompleteCall.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.barCheckItemIncompleteCall_CheckedChanged);
            // 
            // barButtonItemCallBack
            // 
            this.barButtonItemCallBack.Caption = "ItemCallBack";
            this.barButtonItemCallBack.Id = 41;
            this.barButtonItemCallBack.LargeGlyph = global::SmartCadGuiCommon.Properties.ResourcesGui.Call;
            this.barButtonItemCallBack.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemCallBack.Name = "barButtonItemCallBack";
            this.barButtonItemCallBack.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barStaticItemTimeBar
            // 
            this.barStaticItemTimeBar.Caption = "TimeBar";
            this.barStaticItemTimeBar.Id = 25;
            this.barStaticItemTimeBar.LeftIndent = 25;
            this.barStaticItemTimeBar.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barStaticItemTimeBar.Name = "barStaticItemTimeBar";
            this.barStaticItemTimeBar.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // barStaticItemChonometer
            // 
            this.barStaticItemChonometer.Caption = "Chronometer";
            this.barStaticItemChonometer.Id = 26;
            this.barStaticItemChonometer.LeftIndent = 15;
            this.barStaticItemChonometer.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barStaticItemChonometer.Name = "barStaticItemChonometer";
            this.barStaticItemChonometer.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupCallInfo,
            this.ribbonPageGroupUser,
            this.ribbonPageGroupGeneralOptions});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "ribbonPage1";
            // 
            // ribbonPageGroupCallInfo
            // 
            this.ribbonPageGroupCallInfo.AllowMinimize = false;
            this.ribbonPageGroupCallInfo.AllowTextClipping = false;
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barButtonItemTelephone, true);
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barCheckItemIncompleteCall, true);
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barButtonItemCallBack);
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barButtonItemCreateNewIncident);
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barButtonItemAddToIncident);
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barButtonItemFinalize);
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barEditItemProgress, true, "", "", true);
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barStaticItemTimeBar, false, "", "", true);
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barEditItemChronometer, true, "", "", true);
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barStaticItemChonometer, false, "", "", true);
            this.ribbonPageGroupCallInfo.Name = "ribbonPageGroupCallInfo";
            this.ribbonPageGroupCallInfo.ShowCaptionButton = false;
            this.ribbonPageGroupCallInfo.Text = "ribbonPageGroupCallInfo";
            // 
            // ribbonPageGroupUser
            // 
            this.ribbonPageGroupUser.AllowMinimize = false;
            this.ribbonPageGroupUser.AllowTextClipping = false;
            this.ribbonPageGroupUser.ItemLinks.Add(this.barButtonItemStatus);
            this.ribbonPageGroupUser.Name = "ribbonPageGroupUser";
            this.ribbonPageGroupUser.ShowCaptionButton = false;
            this.ribbonPageGroupUser.Text = "ribbonPageGroupUser";
            // 
            // ribbonPageGroupGeneralOptions
            // 
            this.ribbonPageGroupGeneralOptions.AllowMinimize = false;
            this.ribbonPageGroupGeneralOptions.AllowTextClipping = false;
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemPrint);
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemSave);
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemRefresh);
            this.ribbonPageGroupGeneralOptions.Name = "ribbonPageGroupGeneralOptions";
            this.ribbonPageGroupGeneralOptions.ShowCaptionButton = false;
            this.ribbonPageGroupGeneralOptions.Text = "ribbonPageGroupGeneralOptions";
            // 
            // callInformationControl
            // 
            this.callInformationControl.Anonymous = false;
            this.callInformationControl.CallerNumber = "";
            this.callInformationControl.FrontClientState = SmartCadCore.Enums.FrontClientStateEnum.WaitingForIncident;
            this.callInformationControl.IsAnonimousCallActivated = false;
            this.callInformationControl.LineName = "";
            this.callInformationControl.LineTelephoneNumber = "";
            this.callInformationControl.Location = new System.Drawing.Point(5, 5);
            this.callInformationControl.Name = "callInformationControl";
            this.callInformationControl.Size = new System.Drawing.Size(908, 96);
            this.callInformationControl.TabIndex = 0;
            // 
            // proceduresControl
            // 
            this.proceduresControl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(240)))), ((int)(((byte)(244)))));
            this.proceduresControl.Location = new System.Drawing.Point(5, 589);
            this.proceduresControl.Name = "proceduresControl";
            this.proceduresControl.Size = new System.Drawing.Size(908, 186);
            this.proceduresControl.TabIndex = 4;
            // 
            // questionAnswerTextControl
            // 
            this.questionAnswerTextControl.Location = new System.Drawing.Point(532, 400);
            this.questionAnswerTextControl.Name = "questionAnswerTextControl";
            this.questionAnswerTextControl.Size = new System.Drawing.Size(381, 185);
            this.questionAnswerTextControl.TabIndex = 3;
            this.questionAnswerTextControl.Enter += new System.EventHandler(this.questionAnswerTextControl_Enter);
            // 
            // departmentsInvolvedControl
            // 
            this.departmentsInvolvedControl.FrontClientState = SmartCadCore.Enums.FrontClientStateEnum.WaitingForIncident;
            this.departmentsInvolvedControl.Location = new System.Drawing.Point(5, 400);
            this.departmentsInvolvedControl.MinimumSize = new System.Drawing.Size(400, 169);
            this.departmentsInvolvedControl.Name = "departmentsInvolvedControl";
            this.departmentsInvolvedControl.Size = new System.Drawing.Size(523, 185);
            this.departmentsInvolvedControl.TabIndex = 2;
            // 
            // timerSendImage
            // 
            this.timerSendImage.Interval = 300;
            this.timerSendImage.Tick += new System.EventHandler(this.timerSendImage_Tick);
            // 
            // barButtonGroupTelephone
            // 
            this.barButtonGroupTelephone.Caption = "GroupTelephone";
            this.barButtonGroupTelephone.Id = 18;
            this.barButtonGroupTelephone.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonGroupTelephone.Name = "barButtonGroupTelephone";
            this.barButtonGroupTelephone.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel1});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // dockPanel1
            // 
            this.dockPanel1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.dockPanel1.Appearance.Options.UseBackColor = true;
            this.dockPanel1.Controls.Add(this.dockPanel1_Container);
            this.dockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Right;
            this.dockPanel1.ID = new System.Guid("285c3b7c-aaf2-4589-bc72-0f71a2b202da");
            this.dockPanel1.Location = new System.Drawing.Point(918, 0);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.Options.AllowDockBottom = false;
            this.dockPanel1.Options.AllowDockFill = false;
            this.dockPanel1.Options.AllowDockLeft = false;
            this.dockPanel1.Options.AllowDockTop = false;
            this.dockPanel1.Options.AllowFloating = false;
            this.dockPanel1.Options.FloatOnDblClick = false;
            this.dockPanel1.Options.ShowCloseButton = false;
            this.dockPanel1.Options.ShowMaximizeButton = false;
            this.dockPanel1.OriginalSize = new System.Drawing.Size(362, 200);
            this.dockPanel1.Size = new System.Drawing.Size(362, 780);
            this.dockPanel1.Text = "dockPanel1";
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.layoutControlDockPanel);
            this.dockPanel1_Container.Location = new System.Drawing.Point(3, 29);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(356, 748);
            this.dockPanel1_Container.TabIndex = 5;
            // 
            // layoutControlMainWindow
            // 
            this.layoutControlMainWindow.AllowCustomizationMenu = false;
            this.layoutControlMainWindow.Controls.Add(this.questionsControl);
            this.layoutControlMainWindow.Controls.Add(this.callInformationControl);
            this.layoutControlMainWindow.Controls.Add(this.proceduresControl);
            this.layoutControlMainWindow.Controls.Add(this.departmentsInvolvedControl);
            this.layoutControlMainWindow.Controls.Add(this.questionAnswerTextControl);
            this.layoutControlMainWindow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlMainWindow.Location = new System.Drawing.Point(0, 0);
            this.layoutControlMainWindow.Name = "layoutControlMainWindow";
            this.layoutControlMainWindow.Root = this.layoutControlGroupMainWindow;
            this.layoutControlMainWindow.Size = new System.Drawing.Size(918, 780);
            this.layoutControlMainWindow.TabIndex = 7;
            this.layoutControlMainWindow.Text = "layoutControlMainWindow";
            // 
            // questionsControl
            // 
            this.questionsControl.ActiveAnswerTextColor = System.Drawing.Color.Empty;
            this.questionsControl.ActiveQuestionTextColor = System.Drawing.Color.Empty;
            this.questionsControl.AskedStatusColor = System.Drawing.Color.Empty;
            this.questionsControl.AskingStatusColor = System.Drawing.Color.Empty;
            this.questionsControl.CctvClientState = SmartCadCore.Enums.CctvClientStateEnum.WaitingForCctvIncident;
            this.questionsControl.DisabledButtonColor = System.Drawing.Color.Empty;
            this.questionsControl.FrontClientState = SmartCadCore.Enums.FrontClientStateEnum.WaitingForIncident;
            this.questionsControl.GlobalIncidentTypes = null;
            this.questionsControl.InactiveAnswerTextColor = System.Drawing.Color.Empty;
            this.questionsControl.InactiveQuestionTextColor = System.Drawing.Color.Empty;
            this.questionsControl.IncidentTypeLabelBorderColor = System.Drawing.Color.Empty;
            this.questionsControl.Location = new System.Drawing.Point(5, 105);
            this.questionsControl.MinimumSize = new System.Drawing.Size(583, 286);
            this.questionsControl.Name = "questionsControl";
            this.questionsControl.NamePressedButton = null;
            this.questionsControl.NotAskedStatusColor = System.Drawing.Color.Empty;
            this.questionsControl.QuestionsWithAnswers = ((System.Collections.IList)(resources.GetObject("questionsControl.QuestionsWithAnswers")));
            this.questionsControl.SelectedIncidentTypes = null;
            this.questionsControl.Size = new System.Drawing.Size(908, 291);
            this.questionsControl.TabIndex = 1;
            // 
            // layoutControlGroupMainWindow
            // 
            this.layoutControlGroupMainWindow.CustomizationFormText = "layoutControlGroupMainWindow";
            this.layoutControlGroupMainWindow.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemCallInformation,
            this.layoutControlItemDepartmentsInvolved,
            this.layoutControlItemQuestionAnswer,
            this.layoutControlItemProcedures,
            this.layoutControlItemQuestionsControl});
            this.layoutControlGroupMainWindow.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupMainWindow.Name = "Root";
            this.layoutControlGroupMainWindow.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupMainWindow.Size = new System.Drawing.Size(918, 780);
            this.layoutControlGroupMainWindow.Text = "Root";
            this.layoutControlGroupMainWindow.TextVisible = false;
            // 
            // layoutControlItemCallInformation
            // 
            this.layoutControlItemCallInformation.Control = this.callInformationControl;
            this.layoutControlItemCallInformation.CustomizationFormText = "layoutControlItemCallInformation";
            this.layoutControlItemCallInformation.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemCallInformation.MaxSize = new System.Drawing.Size(0, 100);
            this.layoutControlItemCallInformation.MinSize = new System.Drawing.Size(600, 100);
            this.layoutControlItemCallInformation.Name = "layoutControlItemCallInformation";
            this.layoutControlItemCallInformation.Size = new System.Drawing.Size(912, 100);
            this.layoutControlItemCallInformation.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemCallInformation.Text = "layoutControlItemCallInformation";
            this.layoutControlItemCallInformation.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemCallInformation.TextToControlDistance = 0;
            this.layoutControlItemCallInformation.TextVisible = false;
            // 
            // layoutControlItemDepartmentsInvolved
            // 
            this.layoutControlItemDepartmentsInvolved.Control = this.departmentsInvolvedControl;
            this.layoutControlItemDepartmentsInvolved.CustomizationFormText = "layoutControlItemDepartmentsInvolved";
            this.layoutControlItemDepartmentsInvolved.Location = new System.Drawing.Point(0, 395);
            this.layoutControlItemDepartmentsInvolved.MaxSize = new System.Drawing.Size(0, 189);
            this.layoutControlItemDepartmentsInvolved.MinSize = new System.Drawing.Size(393, 189);
            this.layoutControlItemDepartmentsInvolved.Name = "layoutControlItemDepartmentsInvolved";
            this.layoutControlItemDepartmentsInvolved.Size = new System.Drawing.Size(527, 189);
            this.layoutControlItemDepartmentsInvolved.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemDepartmentsInvolved.Text = "layoutControlItemDepartmentsInvolved";
            this.layoutControlItemDepartmentsInvolved.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemDepartmentsInvolved.TextToControlDistance = 0;
            this.layoutControlItemDepartmentsInvolved.TextVisible = false;
            // 
            // layoutControlItemQuestionAnswer
            // 
            this.layoutControlItemQuestionAnswer.Control = this.questionAnswerTextControl;
            this.layoutControlItemQuestionAnswer.CustomizationFormText = "layoutControlItemQuestionAnswer";
            this.layoutControlItemQuestionAnswer.Location = new System.Drawing.Point(527, 395);
            this.layoutControlItemQuestionAnswer.Name = "layoutControlItemQuestionAnswer";
            this.layoutControlItemQuestionAnswer.Size = new System.Drawing.Size(385, 189);
            this.layoutControlItemQuestionAnswer.Text = "layoutControlItemQuestionAnswer";
            this.layoutControlItemQuestionAnswer.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemQuestionAnswer.TextToControlDistance = 0;
            this.layoutControlItemQuestionAnswer.TextVisible = false;
            // 
            // layoutControlItemProcedures
            // 
            this.layoutControlItemProcedures.Control = this.proceduresControl;
            this.layoutControlItemProcedures.CustomizationFormText = "layoutControlItemProcedures";
            this.layoutControlItemProcedures.Location = new System.Drawing.Point(0, 584);
            this.layoutControlItemProcedures.Name = "layoutControlItemProcedures";
            this.layoutControlItemProcedures.Size = new System.Drawing.Size(912, 190);
            this.layoutControlItemProcedures.Text = "layoutControlItemProcedures";
            this.layoutControlItemProcedures.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemProcedures.TextToControlDistance = 0;
            this.layoutControlItemProcedures.TextVisible = false;
            // 
            // layoutControlItemQuestionsControl
            // 
            this.layoutControlItemQuestionsControl.Control = this.questionsControl;
            this.layoutControlItemQuestionsControl.CustomizationFormText = "layoutControlItemQuestionsControl";
            this.layoutControlItemQuestionsControl.Location = new System.Drawing.Point(0, 100);
            this.layoutControlItemQuestionsControl.MaxSize = new System.Drawing.Size(0, 295);
            this.layoutControlItemQuestionsControl.MinSize = new System.Drawing.Size(1, 295);
            this.layoutControlItemQuestionsControl.Name = "layoutControlItemQuestionsControl";
            this.layoutControlItemQuestionsControl.Size = new System.Drawing.Size(912, 295);
            this.layoutControlItemQuestionsControl.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemQuestionsControl.Text = "layoutControlItemQuestionsControl";
            this.layoutControlItemQuestionsControl.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemQuestionsControl.TextToControlDistance = 0;
            this.layoutControlItemQuestionsControl.TextVisible = false;
            // 
            // defaultLookAndFeel1
            // 
            this.defaultLookAndFeel1.LookAndFeel.SkinName = "Blue";
            // 
            // DefaultFrontClientFormDevX
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1280, 780);
            this.Controls.Add(this.layoutControlMainWindow);
            this.Controls.Add(this.dockPanel1);
            this.Controls.Add(this.ribbonControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DefaultFrontClientFormDevX";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SmartCAD - Mdulo de Atencin de Primer Nivel";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DefaultFrontClientFormDevX_FormClosing);
            this.Load += new System.EventHandler(this.FrontClientForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DefaultFrontClientFormDevX_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlDockPanel)).EndInit();
            this.layoutControlDockPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExPreviousIncidents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExPreviousIncidents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupDockPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupPreviousIncidents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemButtonSimilar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSameNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOpenIncidents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGridControlPreviousIncidents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupIncidentDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupCallsAndDispatchs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCallsAndDispatchs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanel1.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlMainWindow)).EndInit();
            this.layoutControlMainWindow.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupMainWindow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCallInformation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartmentsInvolved)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemQuestionAnswer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemProcedures)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemQuestionsControl)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolTip toolTipMain;
        private System.Windows.Forms.SaveFileDialog saveFileDialogMain;
        private System.Windows.Forms.PrintDialog printDialogMain;
        public CallInformationControl callInformationControl;
		private ProceduresControl proceduresControl;
        private QuestionAnswerTextControl questionAnswerTextControl;
		private DepartmentsInvolvedControl departmentsInvolvedControl;
		private System.Windows.Forms.Timer timerSendImage;
		private SimpleButtonEx buttonExSimilarIncidentsFilter;
		private SimpleButtonEx buttonExSameNumberIncidents;
		private SimpleButtonEx buttonExOpenIncidentsFilter;
		private DevExpress.XtraBars.PopupMenu popupMenu1;
		private DevExpress.XtraBars.BarButtonItem barButtonItemTelephone;
		private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
		public DevExpress.XtraBars.BarButtonItem barButtonItemStatus;
		private DevExpress.XtraBars.BarButtonItem barButtonItemPrint;
		private DevExpress.XtraBars.BarButtonItem barButtonItemSave;
		private DevExpress.XtraBars.BarButtonItem barButtonItemRefresh;
		public DevExpress.XtraBars.BarButtonItem barButtonItemCreateNewIncident;
		private DevExpress.XtraBars.BarButtonItem barButtonItemAddToIncident;
		private DevExpress.XtraBars.BarButtonItem barButtonItemFinalize;
		private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupUser;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupCallInfo;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupGeneralOptions;
		private DevExpress.XtraBars.BarButtonGroup barButtonGroupTelephone;
		private DevExpress.XtraLayout.LayoutControl layoutControlDockPanel;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupDockPanel;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemButtonSimilar;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemSameNumber;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemOpenIncidents;
		private DevExpress.XtraBars.Docking.DockManager dockManager1;
		private DevExpress.XtraBars.Docking.DockPanel dockPanel1;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
		private DevExpress.XtraLayout.LayoutControl layoutControlMainWindow;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupMainWindow;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemCallInformation;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDepartmentsInvolved;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemQuestionAnswer;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemProcedures;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupPreviousIncidents;
		private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupIncidentDetails;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupCallsAndDispatchs;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
		private QuestionsControlDevX questionsControl;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemQuestionsControl;
		private CallsAndDispatchsControl callsAndDispatchsControl;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemCallsAndDispatchs;
		private GridControlEx gridControlExPreviousIncidents;
		private GridViewEx gridViewExPreviousIncidents;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemGridControlPreviousIncidents;
		private DevExpress.XtraBars.BarCheckItem barCheckItemIncompleteCall;
        private DevExpress.XtraBars.BarButtonItem barButtonItemCallBack;
		private DevExpress.XtraBars.BarEditItem barEditItemProgress;
		private DevExpress.XtraBars.BarStaticItem barStaticItemTimeBar;
		private DevExpress.XtraBars.BarEditItem barEditItemChronometer;
		private DevExpress.XtraBars.BarStaticItem barStaticItemChonometer;
		private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBar1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit repositoryItemTimeEdit1;
        private SearchableWebBrowser searchableWebBrowser;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
    }
}
