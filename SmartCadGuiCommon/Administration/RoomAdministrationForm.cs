using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.ServiceModel;
using Smartmatic.SmartCad.Service;
using System.Reflection;
using System.Drawing.Imaging;
using System.IO;
using System.Drawing.Drawing2D;
using DevExpress.XtraEditors;
using SmartCadCore.ClientData;
using SmartCadCore.Enums;
using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadGuiCommon.Classes;

//Smartmatic.SmartCad.Gui.LoginForm.Icon
namespace SmartCadGuiCommon
{
    public partial class RoomAdministrationForm : XtraForm
    {
        #region Fields Globals
        private object syncCommitted = new object();
        private AdministrationRibbonForm parentForm;
        RoomClientData selectedRoom = null;
        private FormBehavior behaviorType = FormBehavior.Edit;
        private bool buttonOkPressed = false;
        private List<int> objectsToDelete = new List<int>();
        private Dictionary<int, Control> labelSeatsUsed = new Dictionary<int, Control>();
        private Dictionary<int, string> machineSeatsUsed = new Dictionary<int, string>();
        private Control selectedControl = null;
        private string VerySmall = ResourceLoader.GetString2("VerySmallBallSize");
        private string Small = ResourceLoader.GetString2("SmallBallSize");
        private string Medium = ResourceLoader.GetString2("MediumBallSize");
        private string Large = ResourceLoader.GetString2("LargeBallSize");
        private string Huge = ResourceLoader.GetString2("HugeBallSize");

        #endregion

        #region Properties

        public FormBehavior BehaviorType
        {
            get
            {
                return behaviorType;
            }
            set
            {
                behaviorType = value;
            }
        }

        public RoomClientData SelectedRoom
        {
            get
            {
                return this.selectedRoom;
            }
            set
            {
                Clear();

                this.selectedRoom = value;
                if (selectedRoom != null)
                {
                    this.textBoxName.Text = selectedRoom.Name;
                    this.textBoxExDescription.Text = selectedRoom.Description;
                    this.panelDesigner.BackgroundImage = GetThumbnailImage(selectedRoom.Image);
                    this.panelDesigner.BackgroundImageLayout = ImageLayout.Center;
                  
                    RenderRoom();
                    SetSeatsImage(selectedRoom.SeatsImage);
                    RoomStatusParameters_Change(null, null);
                }
            }
        }

        public bool ButtonOkPressed
        {
            get { return buttonOkPressed; }
        }

        #endregion

        #region Construtors

        public RoomAdministrationForm()
        {
            InitializeComponent();
            enumControls["PictureBox"] = 0;
            enumControls["Label"] = 0;

            comboBoxSizes.Items.AddRange(new string[] { VerySmall, Small, Medium, Large, Huge });
            this.buttonDeleteSeat.Image = ResourceLoader.GetImage("$Images.DeleteIcon");
            comboBoxSizes.SelectedItem = Medium;
            FillMachines();
            labelTools.Text = ResourceLoader.GetString2("ToolTipSeatRoom") + ":";
            labelToolsDelete.Text = ResourceLoader.GetString2("Delete") + ":";
        }

        public RoomAdministrationForm(AdministrationRibbonForm parentForm, RoomClientData room, FormBehavior behavior)
            : this()
        {
            this.parentForm = parentForm;
            this.parentForm.AdministrationCommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(RoomAdministrationForm_AdministrationCommittedChanges);
            this.FormClosing += new FormClosingEventHandler(RoomAdministrationForm_FormClosing);
            this.behaviorType = behavior;
            this.SelectedRoom = room;
        }


        #endregion

        #region Events Handlers

        void RoomAdministrationForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.parentForm != null)
            {
                this.parentForm.AdministrationCommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(RoomAdministrationForm_AdministrationCommittedChanges);
            }
        }

        void RoomAdministrationForm_AdministrationCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    lock (syncCommitted)
                    {
                        try
                        {
                            if (ServerServiceClient.GetInstance().OperatorClient == null)
                            { }
                        }
                        catch
                        { }

                        if (e.Objects[0] is RoomClientData)
                        {
                            #region OfficerClientData
                            RoomClientData room =
                                    e.Objects[0] as RoomClientData;

                            if (SelectedRoom != null && SelectedRoom.Equals(room) && ButtonOkPressed == false)
                            {
                                if (e.Action == CommittedDataAction.Update)
                                {
                                    MessageForm.Show(ResourceLoader.GetString2("UpdateFormRoomData"), MessageFormType.Warning);
                                    SelectedRoom = room;
                                }
                                else if (e.Action == CommittedDataAction.Delete)
                                {
                                    MessageForm.Show(ResourceLoader.GetString2("DeleteFormRoomData"), MessageFormType.Warning);
                                    FormUtil.InvokeRequired(this,
                                    delegate
                                    {
                                        this.Close();
                                    });
                                }
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private void RoomAdministrationForm_Load(object sender, EventArgs e)
        {
            this.Icon = ResourceLoader.GetIcon("$Icon.Room");
            switch (behaviorType)
            {
                case FormBehavior.Create:
                    Text = ResourceLoader.GetString2("RoomAdministrationFormCreateText");
                    buttonExOK.Text = ResourceLoader.GetString("RoomAdministrationFormCreateButtonOkText");
                    break;
                case FormBehavior.Edit:
                    Text = ResourceLoader.GetString2("RoomAdministrationFormEditText");
                    buttonExOK.Text = ResourceLoader.GetString("RoomAdministrationFormEditButtonOkText");
                    break;
                default:
                    break;
            }
            LoadLanguage();
        }

        private void LoadLanguage()
        {
            groupBoxProperties.Text = ResourceLoader.GetString2("RoomProperties");
            labelExName.Text = ResourceLoader.GetString2("LabelName");
            labelExDescription.Text = ResourceLoader.GetString2("LabelDescription");
            labelExMap.Text = ResourceLoader.GetString2("LabelPlane");
            labelComboBox.Text = ResourceLoader.GetString2("LabelSeatSize");

            groupBoxTools.Text = ResourceLoader.GetString2("Tools");
            labelTools.Text = ResourceLoader.GetString2("DragAndDropFigure");
            labelToolsDelete.Text = ResourceLoader.GetString2("LabelDelete");

            groupBoxRoomDesigner.Text = ResourceLoader.GetString2("DesignSeats");

            groupBoxControlParameters.Text = ResourceLoader.GetString2("SeatProperties");
            labelControlLabel.Text = ResourceLoader.GetString2("LabelNumber");
            labelControlMachine.Text = ResourceLoader.GetString2("LabelMachine");
            labelSearch.Text = ResourceLoader.GetString2("LabelSearch");

            buttonExCancel.Text = ResourceLoader.GetString2("Cancel");
        }

        private void groupBoxRoomDesigner_ControlAdded(object sender, ControlEventArgs e)
        {
            RoomStatusParameters_Change(null, null);           
        }

        private void groupBoxRoomDesigner_ControlRemoved(object sender, ControlEventArgs e)
        {
            RoomStatusParameters_Change(null, null);
        }

        private void buttonExOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (buttonExOK.Enabled == true)
                {
                    BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[1] { GetType().GetMethod("buttonOk_Click1", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[0] });
                    processForm.CanThrowError = true;
                    processForm.ShowDialog();
                }
            }
            catch (FaultException ex)
            {
                if (BehaviorType == FormBehavior.Edit)
                    SelectedRoom = (RoomClientData)ServerServiceClient.GetInstance().RefreshClient(SelectedRoom);

                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, MessageFormType.Information);
                buttonOkPressed = false;
               
				if (ex.Message.Contains(ResourceLoader.GetString2("Identifier").ToLower()))
				{
					textBoxName.Focus();
					textBoxName.SelectAll();
				}
                else if (ex.Message.ToLower().Contains(ResourceLoader.GetString2("UK_ROOM_NAME").ToLower()))
				{
					textBoxName.Focus();
					textBoxName.SelectAll();
				}                
            }
            catch (Exception ex)
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, ex);
                buttonOkPressed = false;
            }
        }

        private void buttonOk_Click1()
        {
            buttonOkPressed = true;
            if (behaviorType == FormBehavior.Create)
            {
                selectedRoom = new RoomClientData();
            }
            if (selectedRoom != null)
            {
                //ServerServiceClient.GetInstance().InitializeLazy(selectedRoom, selectedRoom.ListSeats);
                FormUtil.InvokeRequired(this, delegate
                {
                    selectedRoom.Name = this.textBoxName.Text;
                    selectedRoom.Description = this.textBoxExDescription.Text;
                    selectedRoom.RenderMethod = GetRenderMethod();
                    selectedRoom.Image = ImageToByteArray(panelDesigner.BackgroundImage, panelDesigner.BackgroundImage.RawFormat);
                    selectedRoom.SeatsImage = GetSeatsImage();

                    if (behaviorType == FormBehavior.Edit)
                    {
                        DeleteDirtyObjects();
                        UpdateListSeats();
                        
                        
                    }
                    else
                    {
                        selectedRoom.ListSeats = GetSeats(selectedRoom);
                        
                    }

                    selectedRoom.Seats = selectedRoom.ListSeats.Count;
                    
                });

                ServerServiceClient.GetInstance().SaveOrUpdateClientData(SelectedRoom);

            }
        }

        private void buttonEx1_Click(object sender, EventArgs e)
        {
            if (this.mapDialog.ShowDialog() == DialogResult.OK)
            {
                if (behaviorType == FormBehavior.Edit)
                {
                    DialogResult res = MessageForm.Show(ResourceLoader.GetString2("LostInformationForChangePlane"), MessageFormType.WarningQuestion);
                    if (res == DialogResult.OK || res == DialogResult.Yes)
                    {
                        DeleteAllSeats();
                    }
                    else if (res == DialogResult.Cancel || res == DialogResult.No)
                    {
                        return;
                    }

                }
                byte[] mg = File.ReadAllBytes(this.mapDialog.FileName);
                this.textBoxPath.Text = this.mapDialog.FileName;
                this.panelDesigner.BackgroundImage = GetThumbnailImage(mg);
                this.panelDesigner.BackgroundImageLayout = ImageLayout.Center;

            }
        }

        private void panelDesigner_MouseClick(object sender, MouseEventArgs e)
        {
            HideAllResizeBoxes();
            //sselectedControl = null;
            ShowProperties(false);
            textBoxExLabel.Text = string.Empty;
            comboBoxExMachine.SelectedItem = null; 
        }

        private void ShowProperties(bool b)
        {
            labelControlLabel.Enabled = b;
            labelControlMachine.Enabled = b;
            textBoxExLabel.Enabled = b;
            comboBoxExMachine.Enabled = b;
            //textBoxExFilter.Enabled = b;
            //buttonSearch.Enabled = b;
        }

        private void toolStripMenuItemDesignerDelete_Click(object sender, EventArgs e)
        {
            if (contextMenuStripDesigner.SourceControl != null)
            {
                RemoveControlToAnswerGroupBox(contextMenuStripDesigner.SourceControl);
            }
            else
            {
                Control control = null;
                foreach (string var in controls.Keys)
                {
                    if (controls[var].Focused && selectedControl != null)
                    {
                        control = controls[var];
                        RemoveControlToAnswerGroupBox(control);
                        break;
                    }
                }
            }

        }

        #endregion

        #region private methods

        private void FillMachines()
        {   
            foreach (ComputerInfo comp in ComputerInfoList.List)
            {
                comboBoxExMachine.Items.Add(comp.Name);
            }
        }

        private void Clear()
        {
            foreach(Control ctrl in panelDesigner.Controls)
            {
                RemoveControlToAnswerGroupBox(ctrl);
            }
            this.panelDesigner.Controls.Clear();
            this.createdControls = 0;
            this.textBoxExDescription.Clear();
            this.textBoxName.Clear();
            this.machineSeatsUsed.Clear();
            this.labelSeatsUsed.Clear();
            this.objectsToDelete.Clear();
            this.resizeList.Clear();
            this.controlsCodes.Clear();
            this.controls.Clear();
            this.comboBoxExMachine.SelectedItem = null;
            this.enumControls["PictureBox"] = 0;
            this.enumControls["Label"] = 0;
            this.textBoxExLabel.Leave -= new EventHandler(this.textBoxExLabel_TextChanged);
            textBoxExLabel.Clear();
            this.textBoxExLabel.Leave += new EventHandler(this.textBoxExLabel_TextChanged);
            ShowProperties(false);
        }

        private void RenderRoom()
        {
            Render method = Render.FromString(selectedRoom.RenderMethod);
            foreach (Group g in method.Groups)
            {
                foreach (ElementRoom f in g.Element)
                {
                    Control control = null;

                    if (f.GuiType == Element.GUITYPE_TEXT)
                    {
                        control = CreateControl(new Label());
                        control.Text = f.Text;
                        control.BackColor = Color.Transparent;
                    }
                    //else if (f.GuiType == Element.GUITYPE_PICTUREBOX)
                    //{
                    
                    //}
                    control.Width = int.Parse(f.Width);
                    control.Height = int.Parse(f.Height);
                    control.Location = new Point(int.Parse(f.X), int.Parse(f.Y));

                    AddControlToRoomDesignerGroupBox(control, false,f.Code);
                }
            }
            //now render the seats.
            //ServerServiceClient.GetInstance().InitializeLazy(selectedRoom, selectedRoom.ListSeats);
            foreach (RoomSeatClientData var in selectedRoom.ListSeats)
            {
                Control control = CreateControl(new PictureBox(), Int32.Parse(var.SeatName));
                control.Width = var.Width;
                control.Height = var.Height;
                control.Location = new Point(var.X, var.Y);
                machineSeatsUsed[Int32.Parse(var.SeatName)]= var.ComputerName;
                AddControlToRoomDesignerGroupBox(control, false, var.Code);
                if (resizeList.ContainsKey(control))
                {
                    resizeList[control].HideResizeBoxes();
                }
            }
        }

        private byte[] ImageToByteArray(Image image,ImageFormat format) 
        {
            byte[] Ret;

            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    image.Save(ms, ImageFormat.Jpeg);
                    Ret = ms.ToArray();
                }
            }
            catch (Exception) { throw; }

            return Ret; 
        }

        private Image GetThumbnailImage(byte[] data)
        {
            MemoryStream ms = new MemoryStream(data);
            Image image = Image.FromStream(ms);
            Size s = new Size(panelDesigner.Width, panelDesigner.Height);
            return  ApplicationUtil.ResizeImage(image, s, System.Drawing.SystemColors.Control);
        }

        

        private bool ThumbnailCallback()
        {
            return true;
        }

        private void UpdateListSeats()
        {
            ArrayList list = GetSeats(selectedRoom);
            foreach (RoomSeatClientData var in list)
            {
                if (var.Code != 0)
                {
                    int index = 0;
                    foreach (RoomSeatClientData var2 in selectedRoom.ListSeats) 
                    {
                        if (var2.Code == var.Code) 
                        {
                            break; 
                        }
                        index++;
                    }
                    RoomSeatClientData aux = selectedRoom.ListSeats[index] as RoomSeatClientData;
                    aux.ComputerName = var.ComputerName;
                    aux.SeatName = var.SeatName;
                    aux.Height = var.Height;
                    aux.Width = var.Width;
                    aux.X = var.X;
                    aux.Y = var.Y;
                }
                else
                {
                    selectedRoom.ListSeats.Add(var);
                }
            }
        }

        private ArrayList GetSeats(RoomClientData room)
        {
            ArrayList elements = new ArrayList();
            foreach (Control control in panelDesigner.Controls)
            {
                if (control.GetType().Name == typeof(PictureBox).Name)
                {
                    RoomSeatClientData seat = new RoomSeatClientData();
                    seat.Room = room;
                    seat.Height = control.Height;
                    seat.Width = control.Width;
                    seat.X = control.Location.X;
                    seat.Y = control.Location.Y;
                    seat.Code = controlsCodes[control.Text];
                    seat.SeatName = (string)control.Tag;
                    seat.ComputerName = machineSeatsUsed[Int32.Parse(seat.SeatName)];
                    elements.Add(seat);
                }
            }
            return elements;
        }

        private string GetRenderMethod()
        {
            Render render = new Render();
            ArrayList groups = new ArrayList();
            Group group = new Group();

            ArrayList elements = new ArrayList();
            foreach (Control control in panelDesigner.Controls)
            {
                ElementRoom element = new ElementRoom();

                if (control.GetType().Name == typeof(PictureBox).Name)
                    element.GuiType = Element.GUITYPE_PICTUREBOX;
                //else if (control.GetType().Name == typeof(Label).Name)
                //    element.GuiType = Element.GUITYPE_TEXT;

                if (string.IsNullOrEmpty(element.GuiType) == false)
                {
                    element.Code = controlsCodes[control.Text];
                    element.Height = control.Height.ToString();
                    element.Text = control.Text;
                    element.Width = control.Width.ToString();
                    element.X = control.Location.X.ToString();
                    element.Y = control.Location.Y.ToString();

                 //   if (element.GuiType == Element.GUITYPE_PICTUREBOX)
                  //      element.Image = (string)control.Tag;

                    elements.Add(element);
                }
            }
            group.Element = elements;
            groups.Add(group);
            render.Groups = groups;
            return Render.FromRender(render);
        }

        private void RoomStatusParameters_Change(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.textBoxName.Text) ||
                CountPictureBox() <= 0 || 
                ((selectedRoom == null || selectedRoom.Image == null) &&                
                string.IsNullOrEmpty(textBoxPath.Text)) ||
                machineSeatsUsed.ContainsValue("")
                )
                this.buttonExOK.Enabled = false;
            else
                this.buttonExOK.Enabled = true;
        }

        private int CountPictureBox()
        {
            int res = 0;
            foreach (Control cntrl in controls.Values) 
            {
                if (cntrl is PictureBox) 
                {
                    res++;
                }
            }
            return res;
        }

        private void DeleteAllSeats()
        {
            textBoxExLabel.Text = string.Empty;
            comboBoxExMachine.SelectedItem = null;

            foreach (SelectBorderControl con in resizeList.Values) 
            {
                con.HideResizeBoxes();
            }
            resizeList.Clear();
            panelDesigner.Controls.Clear();
            controls.Clear();
            controlsCodes.Clear();
            machineSeatsUsed.Clear();
            enumControls["PictureBox"] = 0;
            enumControls["Label"] = 0;
            createdControls = 0;

            ServerServiceClient server = ServerServiceClient.GetInstance();
            //server.InitializeLazy(selectedRoom, selectedRoom.ListSeats);
            for (int i = selectedRoom.ListSeats.Count - 1; i >= 0; i--) 
            {
                RoomSeatClientData seat = selectedRoom.ListSeats[i] as RoomSeatClientData;
                objectsToDelete.Add(seat.Code);
                seat = selectedRoom.ListSeats[i] as RoomSeatClientData;
                selectedRoom.ListSeats.RemoveAt(i);
                server.DeleteClientObject(seat);
            }
            RoomStatusParameters_Change(null, null);
        }

        private void DeleteDirtyObjects()
        {
            ServerServiceClient server = ServerServiceClient.GetInstance();
            //server.InitializeLazy(selectedRoom, selectedRoom.ListSeats);
            for (int i = selectedRoom.ListSeats.Count - 1; i >= 0; i--)
            {
                RoomSeatClientData seat = selectedRoom.ListSeats[i] as RoomSeatClientData;

                if (objectsToDelete.Contains(seat.Code)) 
                {
                    selectedRoom.ListSeats.RemoveAt(i);
                    server.DeleteClientObject(seat);
                }
            }
        }

        private void DeleteSeat(int code)
        {
            objectsToDelete.Add(code);
        }

        private bool CheckSameTextInControls(Control controlToCheck)
        {
            bool result = false;
            foreach (Control control in panelDesigner.Controls)
            {
                if (control.Name != controlToCheck.Name &&
                    control.Text == controlToCheck.Text )
                {
                    result = true;
                    break;
                }
            }
            return result;
        }

        private void HideAllResizeBoxes()
        {
            foreach (SelectBorderControl var in resizeList.Values)
            {
                var.HideResizeBoxes();
            }
        }

        #endregion

        #region Designer
        enum DesignerControls
        {
            PictureBox,
            Label,
        }

        int createdControls = 0;
        bool mouseDown = false;
        Point cursorOffSet = new Point();
        Cursor currentCursor = null;
        Control newControl = null;
        Dictionary<Control, SelectBorderControl> resizeList = new Dictionary<Control, SelectBorderControl>();
        Dictionary<string, Control> controls = new Dictionary<string, Control>();
        Dictionary<string, int> controlsCodes = new Dictionary<string, int>();
        Dictionary<string, int> enumControls = new Dictionary<string, int>();


        private Control CreateControl(Control controlBase)
        {
            return CreateControl(controlBase, -1);
        }

        private Control CreateControl(Control controlBase, int numberSeat)
        {
            Control control = (Control)Activator.CreateInstance(controlBase.GetType());
            if (numberSeat == -1)
            {
                numberSeat = 1;
                while (machineSeatsUsed.ContainsKey(numberSeat))
                    numberSeat++;
            }
            labelSeatsUsed[numberSeat] = control;
            machineSeatsUsed[numberSeat] = string.Empty;
            //enumControls[control.GetType().Name] = numberSeat;
            createdControls = enumControls[control.GetType().Name];
            
            control.Name = control.GetType().Name + numberSeat.ToString();
            control.Tag = numberSeat.ToString();
            control.Text = controlBase.Text + numberSeat.ToString();
            control.Location = controlBase.Location;
            control.MouseDown += new MouseEventHandler(control_MouseDown);
            control.MouseMove += new MouseEventHandler(control_MouseMove);
            control.MouseUp += new MouseEventHandler(control_MouseUp);
            control.Click += new EventHandler(control_Click);
            control.Validating += new CancelEventHandler(control_Validating);
            control.KeyDown += new KeyEventHandler(control_KeyDown);
            control.ContextMenuStrip = contextMenuStripDesigner;
            control.BackColor = System.Drawing.Color.Transparent;

            switch (control.GetType().Name)
            {
                case "PictureBox":
                    PictureBox newPictureBox = control as PictureBox;
                    newPictureBox.Image = this.pictureBox1.Image;
                    newPictureBox.Width = this.pictureBox1.Width;
                    newPictureBox.Height = this.pictureBox1.Height;
                    newPictureBox.AutoSize = false;
                    toolTipSeats.SetToolTip(control, "");
                    
                    newPictureBox.SizeMode = PictureBoxSizeMode.CenterImage;
                    control.Text = "pictureBox" + control.Text;
                    break;
                case "Label":
                    Label newLabel = control as Label;
                    newLabel.AutoSize = false;
                    break;
                case "Button":
                    Button newButton = control as Button;
                    break;
                default:
                    break;
            }
            return control;
        }

        void control_KeyDown(object sender, KeyEventArgs e)
        {
            if (sender is PictureBox)
            {
                if (e.Control && e.KeyCode == Keys.Delete)
                {
                    RemoveControlToAnswerGroupBox(sender as Control);
                }
                else if (e.KeyCode == Keys.Up)
                {
                    ((PictureBox)sender).Top = ((PictureBox)sender).Top + 1;
                }
                else if (e.KeyCode == Keys.Down)
                {
                    ((PictureBox)sender).Top = ((PictureBox)sender).Top - 1;
                }
                else if (e.KeyCode == Keys.Left)
                {
                    ((PictureBox)sender).Left = ((PictureBox)sender).Left + 1;
                }
                else if (e.KeyCode == Keys.Right)
                {
                    ((PictureBox)sender).Left = ((PictureBox)sender).Left - 1;
                }
            }
        }

        private void AddControlToRoomDesignerGroupBox(Control control, bool adjustLocation, int code)
        {
            HideAllResizeBoxes();
            controls.Add(control.Name, control);
            controlsCodes.Add(control.Text, code);
            this.panelDesigner.Controls.Add(control);
            if (adjustLocation == true)
                control.Location = new Point(control.Location.X - panelDesigner.Location.X, control.Location.Y - panelDesigner.Location.Y);
            panelDesigner.ControlAdded -= new ControlEventHandler(groupBoxRoomDesigner_ControlAdded);
            resizeList.Add(control, new SelectBorderControl(control, true));
            panelDesigner.ControlAdded += new ControlEventHandler(groupBoxRoomDesigner_ControlAdded);
            control.BringToFront();
            RoomStatusParameters_Change(null, null);
        }

        private void RemoveControlToAnswerGroupBox(Control control)
        {
            textBoxExLabel.Text = string.Empty;
            comboBoxExMachine.SelectedItem = null; 
            
            DeleteSeat(controlsCodes[control.Text]);
            panelDesigner.ControlRemoved -= new ControlEventHandler(groupBoxRoomDesigner_ControlRemoved);
            resizeList[control].HideResizeBoxes();
            resizeList.Remove(control);
            int t= int.Parse(control.Tag.ToString());
            if (machineSeatsUsed.ContainsKey(t))
            {
                machineSeatsUsed.Remove(t);
            }
            panelDesigner.Controls.Remove(control);
            selectedControl = null;
            controls.Remove(control.Name);
            controlsCodes.Remove(control.Text);
            panelDesigner.ControlRemoved += new ControlEventHandler(groupBoxRoomDesigner_ControlRemoved);
        }



        #region Mouse Event Control Base
        private void controlBase_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDown = true;

            newControl = CreateControl((Control)sender);
            this.groupBoxRoomDesigner.Controls.Add(newControl);
            cursorOffSet = e.Location;
            currentCursor = this.Cursor;
            Cursor = Cursors.SizeAll;
        }

        private void controlBase_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown == true)
            {
                Point clientPosition = newControl.Parent.PointToClient(System.Windows.Forms.Cursor.Position);
                newControl.Location = new Point(clientPosition.X - cursorOffSet.X, clientPosition.Y - cursorOffSet.Y);
            }
        }

        private void controlBase_MouseUp(object sender, MouseEventArgs e)
        {
            if (newControl == null)
            {
                mouseDown = false;
                Cursor = currentCursor;
                return;
            }
            if (panelDesigner.Location.X < newControl.Location.X &&
                panelDesigner.Location.X + panelDesigner.Size.Width > newControl.Location.X &&
                panelDesigner.Location.Y < newControl.Location.Y &&
                panelDesigner.Location.Y + panelDesigner.Size.Height > newControl.Location.Y)
            {
                AddControlToRoomDesignerGroupBox(newControl, true, 0);

                if (newControl is PictureBox)
                {
                    selectedControl = newControl;
                    ShowProperties(true);
                    SetPropertiesValues(newControl);
                    //propertyGridMain.SelectedObject = new PictureBoxProperties(newControl as Control);
                }
            }
            this.groupBoxRoomDesigner.Controls.Remove(newControl);
            mouseDown = false;
            Cursor = currentCursor;
            newControl = null;
        }

        private void SetPropertiesValues(Control newControl)
        {
            textBoxExLabel.Leave -= new EventHandler(this.textBoxExLabel_TextChanged);
            textBoxExLabel.Text = (string)newControl.Tag;
            textBoxExLabel.Leave += new EventHandler(this.textBoxExLabel_TextChanged);
            if (machineSeatsUsed.ContainsKey(Int32.Parse(textBoxExLabel.Text)) == false || machineSeatsUsed[Int32.Parse(textBoxExLabel.Text)] == string.Empty)
            {
                comboBoxExMachine.SelectedItem = null;
            }
            else
            {
                comboBoxExMachine.SelectedItem = machineSeatsUsed[Int32.Parse(textBoxExLabel.Text)];
            }
        }
        #endregion

        #region Mouse Events Control
        private void control_MouseDown(object sender, MouseEventArgs e)
        {
            HideAllResizeBoxes();
            mouseDown = true;
            cursorOffSet = e.Location;
            currentCursor = this.Cursor;
            Cursor = Cursors.SizeAll;
            Control control = sender as Control;
            textBoxExLabel.Text = (string)control.Tag;
            control.Focus();
            SelectBorderControl resizeControl = resizeList[control];
            panelDesigner.ControlRemoved -= new ControlEventHandler(groupBoxRoomDesigner_ControlRemoved);
            resizeControl.HideResizeBoxes();
            panelDesigner.ControlRemoved += new ControlEventHandler(groupBoxRoomDesigner_ControlRemoved);
        }

        private void control_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown == true)
            {
                Control control = sender as Control;
                MoveControlWithinBounds(control);
            }
            else
            {
                Control cntrl = sender as Control;
                if (cntrl != null)
                {
                    if (controls.ContainsKey(cntrl.Name))
                    {
                        string seat = machineSeatsUsed[int.Parse(cntrl.Tag.ToString())];

                        if (seat != null && seat != string.Empty)
                        {
                            toolTipSeats.SetToolTip(cntrl, ResourceLoader.GetString2("SeatMachine") + ": " + seat +
                                                            ", " + ResourceLoader.GetString2("SeatNumber") + ": " + cntrl.Tag.ToString());
                        }
                        else
                        {
                            toolTipSeats.SetToolTip(cntrl, ResourceLoader.GetString2("SeatNumber") + ": " + cntrl.Tag.ToString());                        
                        }
                    }
                }
            }
        }

        private void control_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
            this.Cursor = currentCursor;
            Control control = sender as Control;
            SelectBorderControl resizeControl = resizeList[control];
            panelDesigner.ControlAdded -= new ControlEventHandler(groupBoxRoomDesigner_ControlAdded);
            resizeControl.ShowResizeBoxes();
            panelDesigner.ControlAdded += new ControlEventHandler(groupBoxRoomDesigner_ControlAdded);
            control.Select();
            control.Focus();
        }

        private void control_Click(object sender, EventArgs e)
        {
            if (sender is Label)
            {
                (sender as Label).Select();
            }
            else if (sender is PictureBox)
            {
                selectedControl = sender as Control;
                ShowProperties(true);
                SetPropertiesValues(sender as Control);
            }
        }

        #endregion

        private void control_Validating(object sender, CancelEventArgs e)
        {
            Control control = sender as Control;
            e.Cancel = string.IsNullOrEmpty(control.Text) || CheckSameTextInControls(control);
        }

        private void MoveControlWithinBounds(Control control)
        {
            if (control.IsHandleCreated == true)
            {

                Point clientPosition = control.Parent.PointToClient(System.Windows.Forms.Cursor.Position);
                int x = clientPosition.X - cursorOffSet.X;
                int y = clientPosition.Y - cursorOffSet.Y;

                if (x > control.Parent.ClientRectangle.Right - control.Width)
                    x = control.Parent.ClientRectangle.Right - control.Width;

                if (y > control.Parent.ClientRectangle.Bottom - control.Height)
                    y = control.Parent.ClientRectangle.Bottom - control.Height;

                if (x < control.Parent.ClientRectangle.Left)
                    x = control.Parent.ClientRectangle.Left;

                if (y < control.Parent.ClientRectangle.Top)
                    y = control.Parent.ClientRectangle.Top;

                control.Location = new Point(x, y);
            }
        }
        #endregion

        private void comboBoxExSizes_SelectionChangeCommitted(object sender, EventArgs e)
        {
            Image aux = ResourceLoader.GetImage(GetSeatsImage());

            this.pictureBox1.Image = aux;
            this.pictureBox1.Width = aux.Width;
            this.pictureBox1.Height = aux.Height;
            this.pictureBox1.BackgroundImageLayout = ImageLayout.Center;

            SetPictureToAllControls(this.pictureBox1.Image);
        }

        public string GetSeatsImage()
        {
            string imageName = "BallLightImage_1";
                if (comboBoxSizes.SelectedItem != null)
                {
                    string seleted = (string)comboBoxSizes.SelectedItem;

                    if(seleted == Small)
                    {
                        imageName = "BallLightImage_2";
                    }
                    else if(seleted == Medium)
                    {
                        imageName = "BallLightImage_3";
                    }
                    else if(seleted == Large)
                    {
                        imageName = "BallLightImage_4";
                    }
                    else if(seleted == Huge)
                    {
                        imageName = "BallLightImage_5";
                    }
                }
                return imageName;
        }

        public void SetSeatsImage(string value)
        {
            switch (value)
            {
                case "BallLightImage_1":
                    comboBoxSizes.SelectedItem = VerySmall;
                    break;
                case "BallLightImage_2":
                    comboBoxSizes.SelectedItem = Small;
                    break;
                case "BallLightImage_3":
                    comboBoxSizes.SelectedItem = Medium;
                    break;
                case "BallLightImage_4":
                    comboBoxSizes.SelectedItem = Large;
                    break;
                case "BallLightImage_5":
                    comboBoxSizes.SelectedItem = Huge;
                    break;
                default:
                    comboBoxSizes.SelectedItem = VerySmall;
                    break;
            }
        }

        private void SetPictureToAllControls(Image image) 
        {
            foreach (Control cntrl in controls.Values) 
            {
                PictureBox var = cntrl as PictureBox;
                if (var != null) 
                {
                    if (resizeList.ContainsKey(cntrl))
                    {
                        resizeList[cntrl].HideResizeBoxes();
                    }
                    var.Width = this.pictureBox1.Width;
                    var.Height = this.pictureBox1.Height;
                    var.Image = image;
                    var.SizeMode = PictureBoxSizeMode.CenterImage;                    
                }
            }
        }

        private void textBoxExLabel_TextChanged(object sender, EventArgs e)
        {
            if (selectedControl != null)
            {
                if (string.IsNullOrEmpty(((TextBox)sender).Text.Trim()))
                {
                    MessageForm.Show(ResourceLoader.GetString2("FormatLabelSeatBad"), MessageFormType.Error);
                    
                    textBoxExLabel.Text = (string)selectedControl.Tag;
                }
                else
                {
                    int output;
                    bool good = Int32.TryParse(((TextBox)sender).Text, out output);
                    if (!good)
                    {
                        MessageForm.Show(ResourceLoader.GetString2("FormatLabelSeatBad"), MessageFormType.Error);

                        textBoxExLabel.Text = (string)selectedControl.Tag;
                    }
                    else
                    {
                        if (labelSeatsUsed.ContainsKey(output) && labelSeatsUsed[output] != selectedControl)
                        {
                            MessageForm.Show(ResourceLoader.GetString2("LabelSeatUsed"), MessageFormType.Error);
                            textBoxExLabel.Text = (string)selectedControl.Tag;
                            textBoxExLabel.SelectAll();
                        }
                        else
                        {
                            labelSeatsUsed.Remove(Int32.Parse((string)selectedControl.Tag));
                            machineSeatsUsed.Remove(Int32.Parse((string)selectedControl.Tag));

                            selectedControl.Tag = ((TextBox)sender).Text;
                            machineSeatsUsed[output] = comboBoxExMachine.Text;
                            labelSeatsUsed[output] = selectedControl;
                        }
                    }
                }
            }
        }

        private void comboBoxExMachine_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxExMachine.SelectedItem != null)
            {
                string newValue = comboBoxExMachine.SelectedItem.ToString();
                int seatNumber = Int32.Parse(textBoxExLabel.Text);
                if (machineSeatsUsed.ContainsValue(newValue) == true &&
                    (machineSeatsUsed.ContainsKey(seatNumber) == false ||
                    machineSeatsUsed[seatNumber] != newValue))
                {
                    string machineName = machineSeatsUsed[seatNumber];
                    MessageForm.Show(ResourceLoader.GetString2("UK_ROOM_SEAT_COMPUTER_NAME",comboBoxExMachine.Text), MessageFormType.Error);
                    if (machineName != string.Empty)
                        comboBoxExMachine.SelectedItem = machineName;
                    else
                        comboBoxExMachine.SelectedItem = null;
                }
                else
                {
                    machineSeatsUsed[seatNumber] = newValue;
                }
            }
            RoomStatusParameters_Change(null, null);
        }

        private void buttonEx1_MouseHover(object sender, System.EventArgs e)
        {
            toolTipRoomForm.SetToolTip(buttonEx1, ResourceLoader.GetString2("OpenBlueprint"));    
        }

        private void buttonDeleteSeat_Click(object sender, EventArgs e)
        {
            if (selectedControl != null)
            {
                RemoveControlToAnswerGroupBox(selectedControl);
            }
        }

        private void buttonDeleteSeat_MouseHover(object sender, EventArgs e)
        {
            toolTipDelete.SetToolTip(buttonDeleteSeat, ResourceLoader.GetString2("ToolTipDeleteSeat"));
        }


    }
}

