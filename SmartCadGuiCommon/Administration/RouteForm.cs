using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Collections;
using Smartmatic.SmartCad.Service;
using System.IO;
using Smartmatic.SmartCad.Map;
using System.Diagnostics;
using SmartCadCore.ClientData;
using SmartCadCore.Enums;
using SmartCadCore.Common;
using SmartCadGuiCommon.Services;
using SmartCadCore.Core;
using SmartCadControls.Controls;
using SmartCadControls;

namespace SmartCadGuiCommon
{
	public partial class RouteForm : RouteBaseForm
	{
		#region Fields
		BitmapViewerForm bitmapviewer;
		string fileName;
        IList originalPoints = new ArrayList();
        #endregion

		#region Constructors
		public RouteForm()
		{
			InitializeComponent();
		}

		public RouteForm(AdministrationRibbonForm form)
			: base(form)
		{
			InitializeComponent();
		}

		public RouteForm(AdministrationRibbonForm form, RouteClientData data, FormBehavior behaviour)
			: this(form)
		{
            InitializeGrid();
			FillDepartmetTypes();
			FillRouteType();
			PutIconByDefect();
            this.behavior = behaviour;
            this.SelectedData = data;
            ApplicationServerService.GisAction += serverServiceClient_GisAction;
		}

		private void FillRouteType()
		{
			comboBoxEditType.Properties.Items.Add(ResourceLoader.GetString2(RouteClientData.RouteType.Track.ToString()));
			comboBoxEditType.Properties.Items.Add(ResourceLoader.GetString2(RouteClientData.RouteType.Area.ToString()));
		}

		private void FillDepartmetTypes()
		{
			IList departments = ServerServiceClient.GetInstance().SearchClientObjects(typeof(DepartmentTypeClientData));
			foreach (DepartmentTypeClientData dep in departments)
			{
				comboBoxEditDepartment.Properties.Items.Add(dep);
			}
		}

		#endregion

		#region Properties

		public override RouteClientData SelectedData
		{
			get
			{
				return data;
			}
            set
            {
                data = value;
                if (data != null)
                {
                    data = (RouteClientData)ServerServiceClient.GetInstance().RefreshClient(data, true);
                    textEditName.Text = data.Name;
                    comboBoxEditDepartment.SelectedItem = data.Department;
                    textEditCustomCode.Text = data.CustomCode;
                    comboBoxEditType.SelectedIndex = (int)data.Type;
					textEditStopTime.Text = data.StopTime.ToString();
					textEditStopTimeTol.Text = data.StopTimeTol.ToString();
                    PutIconSaved(data);
                    if (data.RouteAddress != null)
					{
                        originalPoints = data.RouteAddress;
						bool hasStops = false;
						foreach (RouteAddressClientData address in data.RouteAddress)
						{
							gridControlExRoutePoints.AddOrUpdateItem(new GridControlDataRouteAddress(address));
                            if (string.IsNullOrEmpty(address.Name) == false)
                                hasStops = true;    
						}
						textEditStopTimeTol.Enabled = textEditStopTime.Enabled = hasStops;
					}
                    List<RouteAddressClientData> listRACD = data.RouteAddress.Cast<RouteAddressClientData>().ToList();
                    listRACD.Sort(delegate(RouteAddressClientData a, RouteAddressClientData b)
                    {
                        return a.PointNumber - b.PointNumber;
                    });
                    List<GeoPoint> finalList = listRACD.ConvertAll(delegate(RouteAddressClientData racd)
                    {
                        return new GeoPoint(racd.Lon, racd.Lat);
                    });
                    ApplicationServiceClient.Current(UserApplicationClientData.Map).SendGeoPointList(finalList,-1, data.Code, ShapeType.Route);
                }
                CheckButtons();
            }
		}

		protected override string UpdateMessage
		{
			get { return "UpdateFormRouteData"; }
		}

		protected override string DeleteMessage
		{
			get { return "DeleteFormRouteData"; }
		}

		protected override string CreateFormText
		{
			get { return "CreateRouteFormText"; }
		}

		protected override string EditFormText
		{
			get { return "EditRouteFormText"; }
		}

        public bool MapIsOpen { get; set; }
		#endregion

		#region Methods

		protected override void LoadLanguage()
		{
			layoutControlItemCustomCode.Text = ResourceLoader.GetString2("CustomCode") + ":";
			layoutControlItemDepartment.Text = ResourceLoader.GetString2("Department") + ":*";
			layoutControlItemName.Text = ResourceLoader.GetString2("Name") + ":*";
			layoutControlGroupGridControl.Text = ResourceLoader.GetString2("Coordinates") + ":*";
			layoutControlItemType.Text = ResourceLoader.GetString2("Type") + ":*";
			layoutControlItemStopTimeText.Text = ResourceLoader.GetString2("StopTimeText") + ":*";
			layoutControlItemStopTimeTolText.Text = ResourceLoader.GetString2("StopTimeTolText") + ":*";
            layoutControlGroupIcon.Text = ResourceLoader.GetString2("StopIcon");
			layoutControlGroupPreview.Text = ResourceLoader.GetString2("Preview");
			layoutControlGroupRouteDetails.Text = ResourceLoader.GetString2("RouteDetails");
			simpleButtonChooseIcon.Text = ResourceLoader.GetString2("SelectIcon");
            simpleButtonCancel.Text = ResourceLoader.GetString2("Cancel");
            simpleButtonOpenMap.ToolTip = ResourceLoader.GetString2("OpenMap");
            if (behavior == FormBehavior.Create)
				simpleButtonAccept.Text = ResourceLoader.GetString2("Create");
            else
                simpleButtonAccept.Text = ResourceLoader.GetString2("Accept");
		}

		protected override void CheckButtons()
		{
			if (comboBoxEditDepartment.SelectedIndex < 0 ||
				string.IsNullOrEmpty(textEditName.Text) ||
				(string.IsNullOrEmpty(textEditStopTime.Text) && textEditStopTime.Enabled == true  ) ||
				(string.IsNullOrEmpty(textEditStopTimeTol.Text) && textEditStopTimeTol.Enabled == true )||
				(behavior != FormBehavior.Edit && gridControlExRoutePoints.Items.Count == 0) ||
				comboBoxEditType.SelectedIndex < 0 ||
				(textEditStopTime.Enabled && string.IsNullOrEmpty(fileName)))
				simpleButtonAccept.Enabled = false;
			else
				simpleButtonAccept.Enabled = true;

            simpleButtonOpenMap.Enabled = !MapIsOpen && comboBoxEditDepartment.SelectedIndex != -1;
		}

		protected override void Save_Help()
		{
			buttonOkPressed = true;
			if (behavior == FormBehavior.Create)
				data = new RouteClientData();

			data.Name = textEditName.Text;
			data.Department = (DepartmentTypeClientData)comboBoxEditDepartment.SelectedItem;
			data.CustomCode = textEditCustomCode.Text;
			data.Type = (RouteClientData.RouteType)comboBoxEditType.SelectedIndex;
			data.RouteAddress = new ArrayList();
			data.Image = this.fileName;
            data.StopTime = string.IsNullOrEmpty(textEditStopTime.Text) ? 0 : int.Parse(textEditStopTime.Text);
            data.StopTimeTol = string.IsNullOrEmpty(textEditStopTimeTol.Text) ? 0 : int.Parse(textEditStopTimeTol.Text);
			if (gridControlExRoutePoints.DataSource != null)
				foreach (GridControlDataRouteAddress gridData in gridControlExRoutePoints.Items)
					data.RouteAddress.Add(gridData.Tag);
				
			ServerServiceClient.GetInstance().SaveOrUpdateClientData(data);
			buttonOkPressed = false;
		}

        private void PutIconByDefect()
        {
            bitmapviewer = new BitmapViewerForm();
            SetIcon(ResourceLoader.GetString("DefaultIconUnitType"));
        }

        private void PutIconSaved(RouteClientData route)
        {
            SetIcon(route.Image);
        }

        private void SetIcon(string iconName)
        {
            string iconFullName = Application.StartupPath + SmartCadConfiguration.MapsIconsFolder + iconName;
            if (File.Exists(iconFullName))
            {
                bitmapviewer.FileByDefect = iconName;
                fileName = bitmapviewer.FileByDefect;
                pictureEditIcon.Image = Image.FromFile(iconFullName);
                pictureEditIcon.Name = iconName;
            }
        }

		private void FixedBitmapIconBox()
		{
			for (int i = 0; i < this.bitmapviewer.bitmapViewer1.PnlPictures.Controls.Count; i++)
			{
				if ((this.bitmapviewer.bitmapViewer1.PnlPictures.Controls[i] as PictureBox).Name == this.pictureEditIcon.Name)
				{
					(this.bitmapviewer.bitmapViewer1.PnlPictures.Controls[i] as PictureBox).BorderStyle = BorderStyle.Fixed3D;
					(this.bitmapviewer.bitmapViewer1.PnlPictures.Controls[i] as PictureBox).DisplayRectangle.Inflate(15, 15);
					(this.bitmapviewer.bitmapViewer1.PnlPictures.Controls[i] as PictureBox).BackColor = SystemColors.GradientActiveCaption;
				}
				else
				{
					(this.bitmapviewer.bitmapViewer1.PnlPictures.Controls[i] as PictureBox).BorderStyle = BorderStyle.FixedSingle;
					(this.bitmapviewer.bitmapViewer1.PnlPictures.Controls[i] as PictureBox).DisplayRectangle.Inflate(0, 0);
					(this.bitmapviewer.bitmapViewer1.PnlPictures.Controls[i] as PictureBox).BackColor = SystemColors.Window;
				}
			}
		}

		private void InitializeGrid()
		{
			gridControlExRoutePoints.Type = typeof(GridControlDataRouteAddress);
			gridControlExRoutePoints.ViewTotalRows = true;
			gridControlExRoutePoints.EnableAutoFilter = true;
			gridViewExRoutePoints.OptionsBehavior.Editable = true;
			foreach (DevExpress.XtraGrid.Columns.GridColumn col in gridViewExRoutePoints.Columns)
				col.OptionsColumn.AllowEdit = col.FieldName.Equals("StopName") ? true : false;
			
		}
		
		private void simpleButtonAccept_Click(object sender, EventArgs e)
		{
			DialogResult result = DialogResult.None;
            if (behavior == FormBehavior.Edit)
            {
                if (RouteHasAssocioations(SelectedData) == true)
                {
                    result = MessageForm.Show(ResourceLoader.GetString2("RouteWithAssociations"), MessageFormType.Question);
                    if (result == DialogResult.No)
                        return;
                }
            }
			ButtonAccept_Click(sender, e);
		}

		private bool RouteHasAssocioations(RouteClientData SelectedData)
		{
			bool modifiedAddress = false;
			if (gridControlExRoutePoints.Items.Count > 0)
				modifiedAddress = ((RouteAddressClientData)((GridControlDataRouteAddress)gridControlExRoutePoints.Items[0]).Tag).Code == 0;
            if (SelectedData != null &&
                (SelectedData.Department.Equals(comboBoxEditDepartment.SelectedItem as DepartmentTypeClientData) == false ||
                ResourceLoader.GetString2(SelectedData.Type.ToString()).Equals(comboBoxEditType.SelectedItem) == false) ||
                modifiedAddress)
            {
                long result = (long)ServerServiceClient.GetInstance().SearchBasicObject(
                                    SmartCadHqls.GetCustomHql(SmartCadHqls.GetCountRouteUnitAssociations, SelectedData.Code));

                if (result > 0)
                    return true;
            }
            
            return false;
		}
		#endregion	

		#region Events
		private void textEditName_TextChanged(object sender, EventArgs e)
		{
			CheckButtons();
		}


		private void comboBoxEditType_SelectedIndexChanged(object sender, EventArgs e)
		{
			CheckButtons();
		}

		private void comboBoxEditDepartment_SelectedIndexChanged(object sender, EventArgs e)
		{
            if(MapIsOpen) SendActionsToMap();
			CheckButtons();
		}

        private void serverServiceClient_GisAction(object sender, GisActionEventArgs e)
        {
            try
            {
                if (e is SendGeoPointListEventArgs)
                {
                    #region GeoPointList
                    List<GeoPoint> points = (e as SendGeoPointListEventArgs).Points;
                    if (points != null && points.Count() > 0)
                    {
                        //if the grid points are the same originals points
                        if (originalPoints.Count > 0 && originalPoints.Contains(((GridControlDataRouteAddress)((IList)gridControlExRoutePoints.DataSource)[0]).Tag) == true)
                        {
                            gridControlExRoutePoints.ClearData();
                        }
                        int count = gridControlExRoutePoints.Items.Count;
                        foreach (GeoPoint p in points)
                        {
                            RouteAddressClientData racd = new RouteAddressClientData();
                            racd.PointNumber = (count++);
                            racd.RouteClient = SelectedData;
                            racd.Lat = p.Lat;
                            racd.Lon = p.Lon;
                            gridControlExRoutePoints.AddOrUpdateItem(new GridControlDataRouteAddress(racd));
                        }
                    }
                    else if ((e as SendGeoPointListEventArgs).Type == ShapeType.None)
                    {
                        gridControlExRoutePoints.ClearData();
                    }

                    FormUtil.InvokeRequired(this, delegate
                    {
                        CheckButtons();
                    });
                    #endregion

                }
                else if (e is SendACKEventArgs)
                {
                    if (((SendACKEventArgs)e).Connected)
                    {
                        FormUtil.InvokeRequired(this, () => simpleButtonOpenMap.Enabled = false);

                        MapIsOpen = true;
                        SendActionsToMap();
                    }
                    else
                    {
                        MapIsOpen = false;
                        FormUtil.InvokeRequired(this, () => simpleButtonOpenMap.Enabled = true);
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

		#endregion

        private void simpleButtonOpenMap_Click(object sender, EventArgs e)
        {
            try
            {
                SendActionsToMap();
            }
            catch (Exception ex)
            {
                MessageForm.Show(ex.Message, ex);
            }
        }

        private void SendActionsToMap()
        {
            if (MapIsOpen)
            {
                DepartmentTypeClientData deptType = comboBoxEditDepartment.SelectedItem as DepartmentTypeClientData;
                ApplicationServiceClient.Current(UserApplicationClientData.Map).SendEnableButton(true, ButtonType.AddRoute);
                ApplicationServiceClient.Current(UserApplicationClientData.Map).SendToActivateLayer(
                    ShapeType.Route,
                    comboBoxEditDepartment.SelectedItem != null ? ((DepartmentTypeClientData)comboBoxEditDepartment.SelectedItem).Code : -1,
                    -1);

                if (SelectedData != null && SelectedData.RouteAddress != null && SelectedData.RouteAddress.Count > 0)
                {
                    RouteAddressClientData address = (RouteAddressClientData)SelectedData.RouteAddress[0];
                    ApplicationServiceClient.Current(UserApplicationClientData.Map).DisplayGeoPointInGis(new GeoPoint(address.Lon, address.Lat));
                }
            }
            else if (simpleButtonOpenMap.Enabled)
            {
                RouteAddressClientData address = new RouteAddressClientData();

                if (SelectedData != null && SelectedData.RouteAddress != null && SelectedData.RouteAddress.Count > 0)
                {
                    address = (RouteAddressClientData)SelectedData.RouteAddress[0];
                }

                ApplicationUtil.LaunchApplicationWithArguments(
                    SmartCadConfiguration.SmartCadMapFilename,
                    new object[] { ServerServiceClient.GetInstance().OperatorClient.Login, 
                    AdministrationRibbonForm.Password, 
                    ApplicationUtil.CheckPrimaryScreenBounds(this.Left),
                    new SendEnableButtonEventArgs(ButtonType.AddRoute,true),
                    new SendActivateLayerEventArgs(ShapeType.Route),
                    new DisplayGeoPointEventArgs(new GeoPoint(address.Lon, address.Lat))
                    }
                );
            }
        }

		private void simpleButtonChooseIcon_Click(object sender, EventArgs e)
		{
			FixedBitmapIconBox();
			DialogResult result = bitmapviewer.ShowDialog();
			if (result == DialogResult.OK)
			{
				if (!string.IsNullOrEmpty(bitmapviewer.FileName))
					fileName = bitmapviewer.FileName;
				else
					fileName = bitmapviewer.FileByDefect;
				bitmapviewer.FileByDefect = fileName;
			}
			else if (result == DialogResult.Cancel)
			{
				fileName = bitmapviewer.FileByDefect;
			}
			this.pictureEditIcon.Name = fileName;
            if (fileName != null)
                pictureEditIcon.Image = Image.FromFile(bitmapviewer.bitmapViewer1.Directory + @"\" + fileName);
			CheckButtons();
		}

        private void RouteForm_Load(object sender, EventArgs e)
        {
            ApplicationServiceClient.Current(UserApplicationClientData.Map).SendACK(true);
            //if (behavior == FormBehavior.Edit && MapIsOpen)
            //{
            //    SendActionsToMap();
            //}
        }

        private void RouteForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.parentForm != null)
            {
                //this.parentForm.AdministrationCommittedChanges -= RouteForm_AdministrationCommittedChanges;
            }
            ApplicationServerService.GisAction -= serverServiceClient_GisAction;
            ApplicationServiceClient.Current(UserApplicationClientData.Map).SendACK(false);
        }

		private void gridViewExRoutePoints_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
		{
			if (e.Column.FieldName.Equals("StopName") && string.IsNullOrEmpty(e.Value.ToString()) == false)
			{
				textEditStopTimeTol.Enabled = true;
				textEditStopTime.Enabled = true;
			}
			else
			{
				textEditStopTimeTol.Enabled = false;
				textEditStopTime.Enabled = false;
				foreach (GridControlDataRouteAddress item in gridControlExRoutePoints.Items)
				{
					if (string.IsNullOrEmpty(item.StopName) == false)
					{
						textEditStopTimeTol.Enabled = true;
						textEditStopTime.Enabled = true;
						break;
					}
				}
			}
            CheckButtons();
		}

        void gridViewExRoutePoints_SingleSelectionChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle >= 0)
            {
                try
                {
                    GridControlDataRouteAddress coord = null;
                    FormUtil.InvokeRequired(this, () =>
                    {
                        IList list = gridControlExRoutePoints.SelectedItems;
                        if (list.Count > 0)
                        {
                            coord = list[0] as GridControlDataRouteAddress;
                        }
                    });

                    if (coord != null)
                    {
                        ApplicationServiceClient.Current(UserApplicationClientData.Map).DisplayGeoPointInGis(new GeoPoint(coord.Lon, coord.Lat));
                    }
                }
                catch
                {
                }
            }
        }
	}

	public class RouteBaseForm : XtraFormAdmistration<RouteClientData>
	{
		public RouteBaseForm()
		{
		}

		public RouteBaseForm(AdministrationRibbonForm form)
			: base(form)
		{
		}

		public RouteBaseForm(AdministrationRibbonForm form, RouteClientData data, FormBehavior behavior)
			: base(form, data, behavior)
		{
		}
	}

	public class GridControlDataRouteAddress : GridControlData
	{
		public GridControlDataRouteAddress(RouteAddressClientData address)
			: base(address)
		{

		}

		[GridControlRowInfoData(Searchable = true)]
		public int PointNumber 
		{
			get
			{
				return (Tag as RouteAddressClientData).PointNumber;
			}
		}

		[GridControlRowInfoData(Searchable = true, Editable = true)]
		public string StopName
		{
			get
			{
				return (Tag as RouteAddressClientData).Name;
			}
			set
			{
				(Tag as RouteAddressClientData).Name = value;
			}
		}

		[GridControlRowInfoData(Searchable = true)]
		public double Lat
		{
			get
			{
				return (Tag as RouteAddressClientData).Lat;
			}
		}

		[GridControlRowInfoData(Searchable = true)]
		public double Lon
		{
			get
			{
				return (Tag as RouteAddressClientData).Lon;
			}
		}

		public override bool Equals(object obj)
		{
			if (obj is GridControlDataRouteAddress == false)
				return false;
			
			GridControlDataRouteAddress gridData = obj as GridControlDataRouteAddress;

			return (gridData.Tag as RouteAddressClientData).Equals(Tag);				
		}
	}
}
