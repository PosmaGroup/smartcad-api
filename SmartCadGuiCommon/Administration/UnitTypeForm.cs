using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using SmartCadCore.Core;
using System.Reflection;
using System.Collections;
using System.IO;
using System.ServiceModel;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadCore.Enums;
using Smartmatic.SmartCad.Service;

namespace SmartCadGuiCommon
{
    public partial class UnitTypeForm : DevExpress.XtraEditors.XtraForm
	{
		#region Fields
		UnitTypeClientData selectedUnitType = null;
        FormBehavior behaviorType;
        BitmapViewerForm bitmapviewer;
        string fileName;
        private AdministrationRibbonForm parentForm;
        private object syncCommitted = new object();
		private bool buttonOkPressed = false;
		#endregion

		#region Properties
		public bool ButtonOkPressed
		{
			get { return buttonOkPressed; }
		}
		
		public FormBehavior BehaviorType
		{
			get
			{
				return behaviorType;
			}
			set
			{
				behaviorType = value;
				Clear();
			}
		}

		public UnitTypeClientData SelectedUnitType
		{
			get
			{
				return selectedUnitType;
			}
			set
			{
				Clear();

				selectedUnitType = value;
				if (selectedUnitType != null)
				{
					textBoxName.Text = selectedUnitType.Name;
					textBoxSeats.Text = selectedUnitType.Capacity.ToString();
					textBoxDescription.Text = selectedUnitType.Description;
                    textBoxVelocity.Text = selectedUnitType.Velocity.ToString();
                    PutIconSaved(selectedUnitType);

                    if (selectedUnitType.DepartmentType != null)
                    {
                        int index = -1;
                        index = comboBoxDepartmentType.Items.IndexOf(selectedUnitType.DepartmentType);
                        comboBoxDepartmentType.SelectedIndex = index;

                        if (selectedUnitType.DepartmentType.Dispatch == false)
                            layoutControlGroupIncidentType.Enabled = false;                        
                    }
         
                    if (layoutControlGroupIncidentType.Enabled == true)
                        FillAssignedIncidentTypes();
                    

				}
				UnitTypeParameters_Change(null, null);
			}
		}
		#endregion

		#region Constructor
		public UnitTypeForm()
        {
            InitializeComponent();
            FillIncidentTypes();
            FillDepartmentTypes();
            PutIconByDefect();
            LoadLanguage();
		}
		#endregion

		private void LoadLanguage() 
        {
           
            this.layoutControlGroupIcon.Text = ResourceLoader.GetString2("IconAssociated");
            this.layoutControlGroupIncidentType.Text = ResourceLoader.GetString2("IncidentTypeToAttend");
            this.layoutControlGroupPreview.Text = ResourceLoader.GetString2("Preview");
            this.layoutControlGroupUnit.Text = ResourceLoader.GetString2("UnitTypeInformation");

            this.layoutControlItemDepartment.Text = ResourceLoader.GetString2("Department") + ":*";
            this.layoutControlItemDes.Text = ResourceLoader.GetString2("Description") + ":";
            this.layoutControlItemSeats.Text = ResourceLoader.GetString2("SeatsNumber") + ":*";
            this.layoutControlItemUnitType.Text = ResourceLoader.GetString2("Name") + ":*";
            this.layoutControlItemVelocity.Text = ResourceLoader.GetString2("Velocity") + ":*";
            this.listBoxNotAssigneditem.Text = ResourceLoader.GetString2("NotAssociated");
            this.listBoxAssigneditem.Text = ResourceLoader.GetString2("Associated");
            this.buttonEx1.Text = ResourceLoader.GetString2("SelectIcon");            
        }

        private void UnitTypeForm_Load(object sender, EventArgs e)
        {
            this.Width = 520;
            this.Height = 605;
            this.Icon = ResourceLoader.GetIcon("$Icon.TipoUnidades");
            this.buttonCancel.Text = ResourceLoader.GetString2("$Message.Cancel");
            switch (behaviorType)
            {
                case FormBehavior.Create:
                    Text = ResourceLoader.GetString2("UnitTypeCreateFormText");
                    buttonOk.Text = ResourceLoader.GetString("UnitTypeFormCreateButtonOkText");
                    break;
                case FormBehavior.Edit:
                    Text = ResourceLoader.GetString2("UnitTypeEditFormText");
                    buttonOk.Text = ResourceLoader.GetString("UnitTypeFormEditButtonOkText");
                    break;
                default:
                    break;
            }
        }  

        public UnitTypeForm(AdministrationRibbonForm form, UnitTypeClientData unitType, FormBehavior type)
            :this()     
		{
			BehaviorType = type;
			SelectedUnitType = unitType;
            this.parentForm = form;
            this.parentForm.AdministrationCommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(UnitTypeForm_AdministrationCommittedChanges);
		}

        void UnitTypeForm_AdministrationCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    lock (syncCommitted)
                    {
                        try
                        {
                            if (ServerServiceClient.GetInstance().OperatorClient == null)
                            { }
                        }
                        catch
                        { }

                        if (e.Objects[0] is DepartmentTypeClientData)
                        {
                            #region DepartmentTypeClientData
                            DepartmentTypeClientData departmentType = e.Objects[0] as DepartmentTypeClientData;
                            if (e.Action == CommittedDataAction.Save)
                            {
                                FormUtil.InvokeRequired(this.comboBoxDepartmentType,
                                delegate
                                {
                                    comboBoxDepartmentType.BeginUpdate();
                                    comboBoxDepartmentType.Items.Add(departmentType);
                                    comboBoxDepartmentType.EndUpdate();
                                });
                            }
                            else if (e.Action == CommittedDataAction.Update)
                            {
                                FormUtil.InvokeRequired(this.comboBoxDepartmentType,
                                delegate
                                {
                                    int index = comboBoxDepartmentType.Items.IndexOf(departmentType);
                                    if (index != -1)
                                    {
                                        bool select = (((DepartmentTypeClientData)comboBoxDepartmentType.SelectedItem).Code == departmentType.Code);
                                        comboBoxDepartmentType.Items[index] = departmentType;
                                        if (select)
                                        {
                                            comboBoxDepartmentType.SelectedIndex = -1;
                                            comboBoxDepartmentType.SelectedIndex = index;
                                        }
                                    }                                    
                                });
                            }
                            #endregion
                        }
                        else if (e.Objects[0] is IncidentTypeClientData)
                        {
                            #region DepartmentTypeClientData
                            IncidentTypeClientData incidentType = e.Objects[0] as IncidentTypeClientData;
                            if (e.Action == CommittedDataAction.Save)
                            {
                                FormUtil.InvokeRequired(this.listBoxNotAssigned,
                                delegate
                                {
                                    listBoxNotAssigned.BeginUpdate();
                                    listBoxNotAssigned.Items.Add(incidentType);
                                    listBoxNotAssigned.EndUpdate();
                                });
                            }
                            else if (e.Action == CommittedDataAction.Update)
                            {
                                FormUtil.InvokeRequired(this,
                                delegate
                                {
                                    int index = listBoxNotAssigned.Items.IndexOf(incidentType);
                                    if (index != -1)
                                    {
                                        listBoxNotAssigned.Items[index] = incidentType;
                                    }
                                    index = listBoxAssigned.Items.IndexOf(incidentType);
                                    if (index != -1)
                                    {
                                        listBoxAssigned.Items[index] = incidentType;
                                    }
                                });
                            }
                            else if (e.Action == CommittedDataAction.Delete)
                            {
                                FormUtil.InvokeRequired(this,
                                delegate
                                {
                                    int index = listBoxNotAssigned.Items.IndexOf(incidentType);
                                    if (index != -1)
                                    {
                                        listBoxNotAssigned.Items.RemoveAt(index);
                                    }
                                    index = listBoxAssigned.Items.IndexOf(incidentType);
                                    if (index != -1)
                                    {
                                        listBoxAssigned.Items.RemoveAt(index);
                                    }
                                });
                            }
                            #endregion
                        }
                        else if (e.Objects[0] is UnitTypeClientData)
                        {
                            #region UnitTypeClientData
                            UnitTypeClientData unitType =
                                e.Objects[0] as UnitTypeClientData;

                            if (SelectedUnitType != null && SelectedUnitType.Equals(unitType) && ButtonOkPressed == false)
                            {
                                if (e.Action == CommittedDataAction.Update)
                                {
                                    MessageForm.Show(ResourceLoader.GetString2("UpdateFormUnitTypeData"), MessageFormType.Warning);
                                    SelectedUnitType = unitType;
                                }
                                else if (e.Action == CommittedDataAction.Delete)
                                {
                                    MessageForm.Show(ResourceLoader.GetString2("DeleteFormUnitTypeData"), MessageFormType.Warning);
                                    FormUtil.InvokeRequired(this,
                                    delegate
                                    {
                                        this.Close();
                                    });
                                }
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private void PutIconByDefect()
        {
            bitmapviewer = new BitmapViewerForm();
            SetIcon(ResourceLoader.GetString("DefaultIconUnitType"));
        }

        private void PutIconSaved(UnitTypeClientData unitType)
        {
            SetIcon(unitType.Image);
        }

        private void SetIcon(string iconName)
        {
            string iconFullName = Application.StartupPath + SmartCadConfiguration.MapsIconsFolder + iconName;
            if (File.Exists(iconFullName))
            {
                bitmapviewer.FileByDefect = iconName;
                fileName = bitmapviewer.FileByDefect;
                iconPictureBox.Load(iconFullName);
                iconPictureBox.Name = iconName;
            }
        }

        private void Clear()
        {
            textBoxName.Clear();
            textBoxSeats.Clear();
            textBoxDescription.Clear();
            comboBoxDepartmentType.SelectedIndex = -1;
            FillIncidentTypes();
            listBoxAssigned.Items.Clear();
        }

        private void UnitTypeParameters_Change(object sender, System.EventArgs e)
        {
          if ((textBoxName.Text.Trim() == string.Empty) || 
              (textBoxSeats.Text.Trim() == string.Empty) ||
              (comboBoxDepartmentType.SelectedIndex == -1) ||
              (iconPictureBox.Image == null) ||
              (textBoxVelocity.Text.Trim()== string.Empty))
                buttonOk.Enabled = false;
            else
                buttonOk.Enabled = true;
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            if ((int.Parse(textBoxSeats.Text.Trim()) == 0))
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ResourceLoader.GetString2("UnitTypeSeatsNumber"), MessageFormType.Error);
                textBoxSeats.Focus();
                textBoxSeats.SelectAll();
                return;
            }
            if ((double.Parse(textBoxVelocity.Text.Trim()) == 0))
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ResourceLoader.GetString2("UnitTypeVelocity"), MessageFormType.Error);
                textBoxVelocity.Focus();
                textBoxVelocity.SelectAll();
                return;
            }
            try
            {
                BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[1] { GetType().GetMethod("buttonOk_Click1", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[0] });
                processForm.CanThrowError = true;
                processForm.ShowDialog();
            }
            catch (FaultException ex)
            {
                if (BehaviorType == FormBehavior.Edit)
                    SelectedUnitType = (UnitTypeClientData) ServerServiceClient.GetInstance().RefreshClient(SelectedUnitType);
                
                DialogResult = DialogResult.None;
                GetErrorFocus(ex);
                MessageForm.Show(ex.Message, MessageFormType.Error);
                buttonOkPressed = true;
            }
            catch (Exception ex)
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, ex);
                buttonOkPressed = true;
            }
        }

        private void GetErrorFocus(FaultException ex)
        {
            if (ex.Message.Contains("nombre")) 
            {
                textBoxName.Focus();
            }
        }

        private void buttonOk_Click1()
        {
            buttonOkPressed = true;
            if (behaviorType == FormBehavior.Create)
                selectedUnitType = new UnitTypeClientData();
            if (selectedUnitType != null)
            {
                selectedUnitType.Name = textBoxName.Text;
                selectedUnitType.Capacity = int.Parse(textBoxSeats.Text);
                selectedUnitType.Description = (string.IsNullOrEmpty(textBoxDescription.Text)) ? (string.Empty) : (textBoxDescription.Text);
                FormUtil.InvokeRequired(comboBoxDepartmentType, delegate
                {
                    DepartmentTypeClientData dept = (DepartmentTypeClientData)comboBoxDepartmentType.SelectedItem;
                    selectedUnitType.DepartmentType = dept;
                });
                selectedUnitType.Color = System.Drawing.Color.White;
                selectedUnitType.Image = fileName;
                selectedUnitType.Velocity = double.Parse(textBoxVelocity.Text);
                selectedUnitType.IncidentTypes = new ArrayList(listBoxAssigned.Items);

                ServerServiceClient.GetInstance().SaveOrUpdateClientData(selectedUnitType);
            }
        }

        private void FillIncidentTypes()
        {
            listBoxNotAssigned.Items.Clear();
            foreach (IncidentTypeClientData type in ServerServiceClient.GetInstance().SearchClientObjectsMaxRows(
                SmartCadHqls.GetIncidentTypes, true))
            {
                listBoxNotAssigned.Items.Add(type);
            }

            if (listBoxNotAssigned.Items.Count > 0) {
                listBoxNotAssigned.Enabled = true;
                listBoxAssigned.Enabled = true;
                buttonAddAllIncidentType.Enabled = true;
                buttonAddIncidentType.Enabled = true;
                buttonRemoveAllIncidentType.Enabled = true;
                buttonRemoveIncidentType.Enabled = true;
            } 
        }

        private void FillDepartmentTypes()
        {
            comboBoxDepartmentType.Items.Clear();
            int maxlen = 0;
            foreach (DepartmentTypeClientData type in ServerServiceClient.GetInstance().SearchClientObjectsMaxRows(
                SmartCadHqls.GetDepartmentsType, true))
            {
                comboBoxDepartmentType.Items.Add(type);
                maxlen = Math.Max(maxlen, type.ToString().Length);
            }
            if (maxlen > 0)
            {
                comboBoxDepartmentType.DropDownWidth = maxlen * 6;
            }
        }

        private void FillAssignedIncidentTypes()
        {
            if (selectedUnitType.IncidentTypes != null)
            {
                listBoxAssigned.Items.Clear();
                foreach (IncidentTypeClientData type in selectedUnitType.IncidentTypes)
                {
                    listBoxAssigned.Items.Add(type);
                    listBoxNotAssigned.Items.Remove(type);
                }

                if (listBoxAssigned.Items.Count > 0)
                {
                    listBoxNotAssigned.Enabled = true;
                    listBoxAssigned.Enabled = true;
                    buttonAddAllIncidentType.Enabled = true;
                    buttonAddIncidentType.Enabled = true;
                    buttonRemoveAllIncidentType.Enabled = true;
                    buttonRemoveIncidentType.Enabled = true;
                }
            }
        }

        private void comboBoxDepartmentType_Change(object sender, EventArgs e)
        { 
            DepartmentTypeClientData departmentType = comboBoxDepartmentType.SelectedItem as DepartmentTypeClientData;

            if (departmentType != null)
            {
                if (departmentType.Dispatch == true)
                    layoutControlGroupIncidentType.Enabled = true;
                else
                {
                    layoutControlGroupIncidentType.Enabled = false;
                    buttonRemoveAllIncidentType_Click(null, null);
                }
            }

            UnitTypeParameters_Change(null, null);
        }

        private void buttonAddIncidentType_Click(object sender, EventArgs e)
        {
            ArrayList toDelete = new ArrayList();
            foreach (IncidentTypeClientData incident in listBoxNotAssigned.SelectedItems)
            {
                if (listBoxAssigned.Items.Count > 0)
                {
                        toDelete.Add(incident);
                        listBoxAssigned.Items.Add(incident);
                }
                else
                {
                    toDelete.Add(incident);
                    listBoxAssigned.Items.Add(incident);
                }
            }
            foreach (IncidentTypeClientData incident in toDelete)
            {
                listBoxNotAssigned.Items.Remove(incident);
            }
            UnitTypeParameters_Change(null, null);
        }

        private void buttonAddAllIncidentType_Click(object sender, EventArgs e)
        {
            ArrayList toDelete = new ArrayList();
            foreach (IncidentTypeClientData incident in listBoxNotAssigned.Items)
            {
                if (listBoxAssigned.Items.Count > 0)
                {
                        toDelete.Add(incident);
                        listBoxAssigned.Items.Add(incident);
                }
                else
                {
                    if (incident.Exclusive != true)
                    {
                        toDelete.Add(incident);
                        listBoxAssigned.Items.Add(incident);
                    }
                }
            }
            foreach (IncidentTypeClientData incident in toDelete)
            {
                listBoxNotAssigned.Items.Remove(incident);
            }
            UnitTypeParameters_Change(null, null);
        }

        private void buttonRemoveIncidentType_Click(object sender, EventArgs e)
        {
            ArrayList toDelete = new ArrayList();
            foreach (IncidentTypeClientData incident in listBoxAssigned.SelectedItems)
            {
                toDelete.Add(incident);
                listBoxNotAssigned.Items.Add(incident);
            }
            foreach (IncidentTypeClientData incident in toDelete)
            {
                listBoxAssigned.Items.Remove(incident);
            }
            UnitTypeParameters_Change(null, null);
        }

        private void buttonRemoveAllIncidentType_Click(object sender, EventArgs e)
        {
            ArrayList toDelete = new ArrayList();
            foreach (IncidentTypeClientData incident in listBoxAssigned.Items)
            {
                toDelete.Add(incident);
                listBoxNotAssigned.Items.Add(incident);
            }
            foreach (IncidentTypeClientData incident in toDelete)
            {
                listBoxAssigned.Items.Remove(incident);
            }
            UnitTypeParameters_Change(null, null);
        }

        private void listBoxNotAssigned_DoubleClick(object sender, EventArgs e)
        {
            buttonAddIncidentType_Click(sender, e);
        }

        private void listBoxAssigned_DoubleClick(object sender, EventArgs e)
        {
            buttonRemoveIncidentType_Click(sender, e);
        }

        private void buttonEx1_Click(object sender, EventArgs e)
        {
            FixedBitmapIconBox();
            DialogResult result = bitmapviewer.ShowDialog();
            if (result == DialogResult.OK) 
            {
                if (!string.IsNullOrEmpty(bitmapviewer.FileName))
                    fileName = bitmapviewer.FileName;
                else
                    fileName = bitmapviewer.FileByDefect;
                bitmapviewer.FileByDefect = fileName;
            }
            else if (result == DialogResult.Cancel) 
            {
                fileName = bitmapviewer.FileByDefect;                
            }
            this.iconPictureBox.Name = fileName;
            if (fileName != null)
                iconPictureBox.Load(bitmapviewer.bitmapViewer1.Directory + @"\" + fileName);
            UnitTypeParameters_Change(null, null);
        }

        private void FixedBitmapIconBox()
        {
            for (int i = 0; i < this.bitmapviewer.bitmapViewer1.PnlPictures.Controls.Count; i++)
            {
                if ((this.bitmapviewer.bitmapViewer1.PnlPictures.Controls[i] as PictureBox).Name == this.iconPictureBox.Name)
                {
                    (this.bitmapviewer.bitmapViewer1.PnlPictures.Controls[i] as PictureBox).BorderStyle = BorderStyle.Fixed3D;
                    (this.bitmapviewer.bitmapViewer1.PnlPictures.Controls[i] as PictureBox).DisplayRectangle.Inflate(15, 15);
                    (this.bitmapviewer.bitmapViewer1.PnlPictures.Controls[i] as PictureBox).BackColor = SystemColors.GradientActiveCaption;

                }
                else
                {
                    (this.bitmapviewer.bitmapViewer1.PnlPictures.Controls[i] as PictureBox).BorderStyle = BorderStyle.FixedSingle;
                    (this.bitmapviewer.bitmapViewer1.PnlPictures.Controls[i] as PictureBox).DisplayRectangle.Inflate(0, 0);
                    (this.bitmapviewer.bitmapViewer1.PnlPictures.Controls[i] as PictureBox).BackColor = SystemColors.Window;
                }
            }
        }

        private void listBoxNotAssigned_Enter(object sender, EventArgs e)
        {
            listBoxAssigned.ClearSelected();
        }

        private void listBoxAssigned_Enter(object sender, EventArgs e)
        {
            listBoxNotAssigned.ClearSelected();
        }         

        private void UnitTypeForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.parentForm != null)
            {
                this.parentForm.AdministrationCommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(UnitTypeForm_AdministrationCommittedChanges);
            }
        }      
    }
}