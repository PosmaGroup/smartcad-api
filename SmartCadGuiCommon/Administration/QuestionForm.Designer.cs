
using SmartCadControls;
using SmartCadControls.Controls;
using SmartCadCore.Common;
using SmartCadGuiCommon.Controls;
namespace SmartCadGuiCommon
{
    partial class QuestionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QuestionForm));
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem("Texto libre", 0);
            System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem("Seleccion simple", 2);
            System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem("Seleccion multiple", 1);
            this.contextMenuStripDesigner = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItemDesignerDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.richTextBoxExtendedProcedure = new RichTextBoxExtended();
            this.buttonRemoveAllIncidentType = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.buttonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.buttonOk = new DevExpress.XtraEditors.SimpleButton();
            this.checkBoxExPriority = new DevExpress.XtraEditors.CheckEdit();
            this.textBoxExMnemonic = new TextBoxEx();
            this.textBoxExName = new TextBoxEx();
            this.propertyGridControl1 = new DevExpress.XtraVerticalGrid.PropertyGridControl();
            this.repositoryItemTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.simpleButtonTrash = new DevExpress.XtraEditors.SimpleButton();
            this.imageList3 = new System.Windows.Forms.ImageList(this.components);
            this.listView1 = new System.Windows.Forms.ListView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.dragDropLayoutControl1 = new DragDropLayoutControl();
            this.buttonRemoveIncidentType = new DevExpress.XtraEditors.SimpleButton();
            this.buttonAddAllIncidentType = new DevExpress.XtraEditors.SimpleButton();
            this.buttonAddIncidentType = new DevExpress.XtraEditors.SimpleButton();
            this.listBoxAssigned = new System.Windows.Forms.ListBox();
            this.gridControlAnswers = new GridControlEx();
            this.gridViewAnswers = new GridViewEx();
            this.listBoxNotAssigned = new System.Windows.Forms.ListBox();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupDataQuestion = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemCustomCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemPriority = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupAnswersIncidents = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroupAnswers = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupAnswerType = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlSelectControl = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupAnswerDefinition = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupControlParameters = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupProcedures = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupProcedureAnswers = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupProcedureAsociated = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupIncidents = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupIncidentsType = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItemAsociated = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemNotAsociated = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.labelAnswerType = new System.Windows.Forms.Label();
            this.toolTipQuestion = new System.Windows.Forms.ToolTip(this.components);
            this.contextMenuStripDesigner.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxExPriority.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.propertyGridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAnswers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewAnswers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupDataQuestion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCustomCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPriority)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupAnswersIncidents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupAnswers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupAnswerType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlSelectControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupAnswerDefinition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupControlParameters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupProcedures)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupProcedureAnswers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupProcedureAsociated)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupIncidents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupIncidentsType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAsociated)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemNotAsociated)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            this.SuspendLayout();
            // 
            // contextMenuStripDesigner
            // 
            this.contextMenuStripDesigner.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemDesignerDelete});
            this.contextMenuStripDesigner.Name = "contextMenuStrip1";
            this.contextMenuStripDesigner.Size = new System.Drawing.Size(158, 48);
            // 
            // toolStripMenuItemDesignerDelete
            // 
            this.toolStripMenuItemDesignerDelete.Name = "toolStripMenuItemDesignerDelete";
            this.toolStripMenuItemDesignerDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Delete)));
            this.toolStripMenuItemDesignerDelete.Size = new System.Drawing.Size(157, 22);
            this.toolStripMenuItemDesignerDelete.Text = "Borrar";
            this.toolStripMenuItemDesignerDelete.Click += new System.EventHandler(this.toolStripMenuItemDesignerDelete_Click);
            // 
            // richTextBoxExtendedProcedure
            // 
            this.richTextBoxExtendedProcedure.AcceptsTab = false;
            this.richTextBoxExtendedProcedure.AutoScroll = true;
            this.richTextBoxExtendedProcedure.AutoWordSelection = true;
            this.richTextBoxExtendedProcedure.DetectURLs = true;
            this.richTextBoxExtendedProcedure.Enabled = false;
            this.richTextBoxExtendedProcedure.Location = new System.Drawing.Point(17, 350);
            this.richTextBoxExtendedProcedure.MaxLength = 16384;
            this.richTextBoxExtendedProcedure.Name = "richTextBoxExtendedProcedure";
            this.richTextBoxExtendedProcedure.ReadOnly = false;
            // 
            // 
            // 
            this.richTextBoxExtendedProcedure.RichTextBox.AutoWordSelection = true;
            this.richTextBoxExtendedProcedure.RichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBoxExtendedProcedure.RichTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.richTextBoxExtendedProcedure.RichTextBox.Location = new System.Drawing.Point(0, 25);
            this.richTextBoxExtendedProcedure.RichTextBox.MaxLength = 16384;
            this.richTextBoxExtendedProcedure.RichTextBox.Name = "rtb1";
            this.richTextBoxExtendedProcedure.RichTextBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.richTextBoxExtendedProcedure.RichTextBox.Size = new System.Drawing.Size(639, 154);
            this.richTextBoxExtendedProcedure.RichTextBox.TabIndex = 1;
            this.richTextBoxExtendedProcedure.RichTextBox.TextChanged += new System.EventHandler(this.richTextBoxExtendedProcedure_RichTextBox_TextChanged);
            this.richTextBoxExtendedProcedure.RichTextBox.GotFocus += new System.EventHandler(this.richTextBoxExtendedProcedure_RichTextBox_GotFocus);
            this.richTextBoxExtendedProcedure.RichTextBox.LostFocus += new System.EventHandler(this.RichTextBox_LostFocus);
            this.richTextBoxExtendedProcedure.ShowBold = false;
            this.richTextBoxExtendedProcedure.ShowCenterJustify = false;
            this.richTextBoxExtendedProcedure.ShowColors = false;
            this.richTextBoxExtendedProcedure.ShowCopy = false;
            this.richTextBoxExtendedProcedure.ShowCut = false;
            this.richTextBoxExtendedProcedure.ShowFont = false;
            this.richTextBoxExtendedProcedure.ShowFontSize = false;
            this.richTextBoxExtendedProcedure.ShowItalic = false;
            this.richTextBoxExtendedProcedure.ShowLeftJustify = false;
            this.richTextBoxExtendedProcedure.ShowOpen = false;
            this.richTextBoxExtendedProcedure.ShowPaste = false;
            this.richTextBoxExtendedProcedure.ShowRedo = false;
            this.richTextBoxExtendedProcedure.ShowRightJustify = false;
            this.richTextBoxExtendedProcedure.ShowSave = false;
            this.richTextBoxExtendedProcedure.ShowStamp = false;
            this.richTextBoxExtendedProcedure.ShowStrikeout = false;
            this.richTextBoxExtendedProcedure.ShowToolBarText = true;
            this.richTextBoxExtendedProcedure.ShowUnderline = false;
            this.richTextBoxExtendedProcedure.ShowUndo = false;
            this.richTextBoxExtendedProcedure.Size = new System.Drawing.Size(639, 179);
            this.richTextBoxExtendedProcedure.StampAction = StampActions.EditedBy;
            this.richTextBoxExtendedProcedure.StampColor = System.Drawing.Color.Blue;
            this.richTextBoxExtendedProcedure.TabIndex = 1;
            // 
            // 
            // 
            this.richTextBoxExtendedProcedure.Toolbar.Location = new System.Drawing.Point(0, 0);
            this.richTextBoxExtendedProcedure.Toolbar.Name = "toolStripMain";
            this.richTextBoxExtendedProcedure.Toolbar.Size = new System.Drawing.Size(639, 25);
            this.richTextBoxExtendedProcedure.Toolbar.TabIndex = 3;
            this.richTextBoxExtendedProcedure.Toolbar.TabStop = true;
            // 
            // buttonRemoveAllIncidentType
            // 
            this.buttonRemoveAllIncidentType.AccessibleDescription = "Algo";
            this.buttonRemoveAllIncidentType.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonRemoveAllIncidentType.Appearance.Options.UseForeColor = true;
            this.buttonRemoveAllIncidentType.Location = new System.Drawing.Point(311, 366);
            this.buttonRemoveAllIncidentType.Name = "buttonRemoveAllIncidentType";
            this.buttonRemoveAllIncidentType.Size = new System.Drawing.Size(36, 29);
            this.buttonRemoveAllIncidentType.StyleController = this.layoutControl1;
            this.buttonRemoveAllIncidentType.TabIndex = 4;
            this.buttonRemoveAllIncidentType.Text = "<<";
            this.toolTipQuestion.SetToolTip(this.buttonRemoveAllIncidentType, ResourceLoader.GetString2("RemoveAllIncidentTypes"));
            this.buttonRemoveAllIncidentType.Click += new System.EventHandler(this.buttonRemoveAllIncidentType_Click);
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowCustomizationMenu = false;
            this.layoutControl1.Controls.Add(this.buttonCancel);
            this.layoutControl1.Controls.Add(this.buttonOk);
            this.layoutControl1.Controls.Add(this.checkBoxExPriority);
            this.layoutControl1.Controls.Add(this.textBoxExMnemonic);
            this.layoutControl1.Controls.Add(this.textBoxExName);
            this.layoutControl1.Controls.Add(this.propertyGridControl1);
            this.layoutControl1.Controls.Add(this.simpleButtonTrash);
            this.layoutControl1.Controls.Add(this.listView1);
            this.layoutControl1.Controls.Add(this.dragDropLayoutControl1);
            this.layoutControl1.Controls.Add(this.buttonRemoveAllIncidentType);
            this.layoutControl1.Controls.Add(this.buttonRemoveIncidentType);
            this.layoutControl1.Controls.Add(this.buttonAddAllIncidentType);
            this.layoutControl1.Controls.Add(this.buttonAddIncidentType);
            this.layoutControl1.Controls.Add(this.listBoxAssigned);
            this.layoutControl1.Controls.Add(this.gridControlAnswers);
            this.layoutControl1.Controls.Add(this.richTextBoxExtendedProcedure);
            this.layoutControl1.Controls.Add(this.listBoxNotAssigned);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(473, 401, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(673, 582);
            this.layoutControl1.TabIndex = 10;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCancel.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonCancel.Appearance.Options.UseFont = true;
            this.buttonCancel.Appearance.Options.UseForeColor = true;
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(589, 548);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(82, 32);
            this.buttonCancel.StyleController = this.layoutControl1;
            this.buttonCancel.TabIndex = 7;
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOk.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonOk.Appearance.Options.UseFont = true;
            this.buttonOk.Appearance.Options.UseForeColor = true;
            this.buttonOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOk.Enabled = false;
            this.buttonOk.Location = new System.Drawing.Point(503, 548);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(82, 32);
            this.buttonOk.StyleController = this.layoutControl1;
            this.buttonOk.TabIndex = 6;
            this.buttonOk.Text = "Accept";
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // checkBoxExPriority
            // 
            this.checkBoxExPriority.Location = new System.Drawing.Point(259, 51);
            this.checkBoxExPriority.Name = "checkBoxExPriority";
            this.checkBoxExPriority.Properties.Caption = "Recomendada";
            this.checkBoxExPriority.Size = new System.Drawing.Size(407, 19);
            this.checkBoxExPriority.StyleController = this.layoutControl1;
            this.checkBoxExPriority.TabIndex = 4;
            // 
            // textBoxExMnemonic
            // 
            this.textBoxExMnemonic.AllowsLetters = true;
            this.textBoxExMnemonic.AllowsNumbers = true;
            this.textBoxExMnemonic.AllowsPunctuation = true;
            this.textBoxExMnemonic.AllowsSeparators = true;
            this.textBoxExMnemonic.AllowsSymbols = true;
            this.textBoxExMnemonic.AllowsWhiteSpaces = true;
            this.textBoxExMnemonic.ExtraAllowedChars = "";
            this.textBoxExMnemonic.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExMnemonic.Location = new System.Drawing.Point(112, 51);
            this.textBoxExMnemonic.MaxLength = 15;
            this.textBoxExMnemonic.Name = "textBoxExMnemonic";
            this.textBoxExMnemonic.NonAllowedCharacters = "";
            this.textBoxExMnemonic.RegularExpresion = "";
            this.textBoxExMnemonic.Size = new System.Drawing.Size(143, 27);
            this.textBoxExMnemonic.TabIndex = 3;
            this.textBoxExMnemonic.TextChanged += new System.EventHandler(this.QuestionStatusParameters_Change);
            // 
            // textBoxExName
            // 
            this.textBoxExName.AllowsLetters = true;
            this.textBoxExName.AllowsNumbers = true;
            this.textBoxExName.AllowsPunctuation = true;
            this.textBoxExName.AllowsSeparators = true;
            this.textBoxExName.AllowsSymbols = true;
            this.textBoxExName.AllowsWhiteSpaces = true;
            this.textBoxExName.ExtraAllowedChars = "";
            this.textBoxExName.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExName.Location = new System.Drawing.Point(112, 27);
            this.textBoxExName.MaxLength = 80;
            this.textBoxExName.Name = "textBoxExName";
            this.textBoxExName.NonAllowedCharacters = "";
            this.textBoxExName.RegularExpresion = "";
            this.textBoxExName.Size = new System.Drawing.Size(554, 20);
            this.textBoxExName.TabIndex = 1;
            this.textBoxExName.TextChanged += new System.EventHandler(this.QuestionStatusParameters_Change);
            this.textBoxExName.GotFocus += new System.EventHandler(this.textBoxName_GotFocus);
            // 
            // propertyGridControl1
            // 
            this.propertyGridControl1.DefaultEditors.AddRange(new DevExpress.XtraVerticalGrid.Rows.DefaultEditor[] {
            new DevExpress.XtraVerticalGrid.Rows.DefaultEditor(typeof(string), this.repositoryItemTextEdit)});
            this.propertyGridControl1.Location = new System.Drawing.Point(17, 402);
            this.propertyGridControl1.Name = "propertyGridControl1";
            this.propertyGridControl1.OptionsBehavior.ResizeHeaderPanel = false;
            this.propertyGridControl1.OptionsBehavior.ResizeRowHeaders = false;
            this.propertyGridControl1.OptionsView.ShowRootCategories = false;
            this.propertyGridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit});
            this.propertyGridControl1.Size = new System.Drawing.Size(639, 127);
            this.propertyGridControl1.TabIndex = 13;
            // 
            // repositoryItemTextEdit
            // 
            this.repositoryItemTextEdit.AutoHeight = false;
            this.repositoryItemTextEdit.MaxLength = 100;
            this.repositoryItemTextEdit.Name = "repositoryItemTextEdit";
            // 
            // simpleButtonTrash
            // 
            this.simpleButtonTrash.AllowDrop = true;
            this.simpleButtonTrash.ImageIndex = 0;
            this.simpleButtonTrash.ImageList = this.imageList3;
            this.simpleButtonTrash.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopRight;
            this.simpleButtonTrash.Location = new System.Drawing.Point(603, 162);
            this.simpleButtonTrash.Name = "simpleButtonTrash";
            this.simpleButtonTrash.Size = new System.Drawing.Size(53, 61);
            this.simpleButtonTrash.StyleController = this.layoutControl1;
            this.simpleButtonTrash.TabIndex = 11;
            this.simpleButtonTrash.Click += new System.EventHandler(this.simpleButtonTrash_Click);
            this.simpleButtonTrash.DragDrop += new System.Windows.Forms.DragEventHandler(this.simpleButtonTrash_DragDrop);
            this.simpleButtonTrash.DragEnter += new System.Windows.Forms.DragEventHandler(this.simpleButtonTrash_DragEnter);
            this.simpleButtonTrash.DragLeave += new System.EventHandler(this.simpleButtonTrash_DragLeave);
            // 
            // imageList3
            // 
            this.imageList3.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList3.ImageStream")));
            this.imageList3.TransparentColor = System.Drawing.Color.Magenta;
            this.imageList3.Images.SetKeyName(0, "");
            this.imageList3.Images.SetKeyName(1, "");
            // 
            // listView1
            // 
            this.listView1.Alignment = System.Windows.Forms.ListViewAlignment.Default;
            this.listView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            listViewItem1.Tag = "TextBox";
            listViewItem2.Tag = "RadioButton";
            listViewItem3.Tag = "CheckEdit";
            this.listView1.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1,
            listViewItem2,
            listViewItem3});
            this.listView1.Location = new System.Drawing.Point(172, 162);
            this.listView1.MultiSelect = false;
            this.listView1.Name = "listView1";
            this.listView1.Scrollable = false;
            this.listView1.Size = new System.Drawing.Size(427, 61);
            this.listView1.SmallImageList = this.imageList1;
            this.listView1.TabIndex = 11;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.SmallIcon;
            this.listView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listView1_MouseDown);
            this.listView1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.listView1_MouseMove);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "textBox.gif");
            this.imageList1.Images.SetKeyName(1, "checkBox.gif");
            this.imageList1.Images.SetKeyName(2, "radioButton.gif");
            // 
            // dragDropLayoutControl1
            // 
            this.dragDropLayoutControl1.Location = new System.Drawing.Point(17, 257);
            this.dragDropLayoutControl1.Name = "dragDropLayoutControl1";
            this.dragDropLayoutControl1.Size = new System.Drawing.Size(639, 111);
            this.dragDropLayoutControl1.TabIndex = 12;
            this.dragDropLayoutControl1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dragDropLayoutControl1_MouseClick);
            // 
            // buttonRemoveIncidentType
            // 
            this.buttonRemoveIncidentType.AccessibleDescription = "Algo";
            this.buttonRemoveIncidentType.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonRemoveIncidentType.Appearance.Options.UseForeColor = true;
            this.buttonRemoveIncidentType.Location = new System.Drawing.Point(311, 333);
            this.buttonRemoveIncidentType.Name = "buttonRemoveIncidentType";
            this.buttonRemoveIncidentType.Size = new System.Drawing.Size(36, 29);
            this.buttonRemoveIncidentType.StyleController = this.layoutControl1;
            this.buttonRemoveIncidentType.TabIndex = 3;
            this.buttonRemoveIncidentType.Text = "<";
            this.toolTipQuestion.SetToolTip(this.buttonRemoveIncidentType, ResourceLoader.GetString2("RemoveIncidentType"));
            this.buttonRemoveIncidentType.Click += new System.EventHandler(this.buttonRemoveIncidentType_Click);
            // 
            // buttonAddAllIncidentType
            // 
            this.buttonAddAllIncidentType.AccessibleDescription = "Algo";
            this.buttonAddAllIncidentType.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonAddAllIncidentType.Appearance.Options.UseForeColor = true;
            this.buttonAddAllIncidentType.Location = new System.Drawing.Point(311, 300);
            this.buttonAddAllIncidentType.Name = "buttonAddAllIncidentType";
            this.buttonAddAllIncidentType.Size = new System.Drawing.Size(36, 29);
            this.buttonAddAllIncidentType.StyleController = this.layoutControl1;
            this.buttonAddAllIncidentType.TabIndex = 2;
            this.buttonAddAllIncidentType.Text = ">>";
            this.toolTipQuestion.SetToolTip(this.buttonAddAllIncidentType, ResourceLoader.GetString2("AssociateAllIncidentTypes"));
            this.buttonAddAllIncidentType.Click += new System.EventHandler(this.buttonAddAllIncidentType_Click);
            // 
            // buttonAddIncidentType
            // 
            this.buttonAddIncidentType.AccessibleDescription = "Algo";
            this.buttonAddIncidentType.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonAddIncidentType.Appearance.Options.UseForeColor = true;
            this.buttonAddIncidentType.Location = new System.Drawing.Point(311, 267);
            this.buttonAddIncidentType.Name = "buttonAddIncidentType";
            this.buttonAddIncidentType.Size = new System.Drawing.Size(36, 29);
            this.buttonAddIncidentType.StyleController = this.layoutControl1;
            this.buttonAddIncidentType.TabIndex = 1;
            this.buttonAddIncidentType.Text = ">";
            this.toolTipQuestion.SetToolTip(this.buttonAddIncidentType, ResourceLoader.GetString2("AssociateIncidentType"));
            this.buttonAddIncidentType.Click += new System.EventHandler(this.buttonAddIncidentType_Click);
            // 
            // listBoxAssigned
            // 
            this.listBoxAssigned.FormattingEnabled = true;
            this.listBoxAssigned.HorizontalScrollbar = true;
            this.listBoxAssigned.IntegralHeight = false;
            this.listBoxAssigned.Location = new System.Drawing.Point(351, 178);
            this.listBoxAssigned.Name = "listBoxAssigned";
            this.listBoxAssigned.Size = new System.Drawing.Size(305, 351);
            this.listBoxAssigned.Sorted = true;
            this.listBoxAssigned.TabIndex = 5;
            this.listBoxAssigned.DoubleClick += new System.EventHandler(this.listBoxAssigned_DoubleClick);
            this.listBoxAssigned.Enter += new System.EventHandler(this.listBoxAssigned_Enter);
            // 
            // gridControlAnswers
            // 
            this.gridControlAnswers.EnableAutoFilter = false;
            this.gridControlAnswers.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlAnswers.Location = new System.Drawing.Point(17, 162);
            this.gridControlAnswers.MainView = this.gridViewAnswers;
            this.gridControlAnswers.Name = "gridControlAnswers";
            this.gridControlAnswers.Size = new System.Drawing.Size(639, 154);
            this.gridControlAnswers.TabIndex = 14;
            this.gridControlAnswers.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewAnswers});
            this.gridControlAnswers.ViewTotalRows = false;
            // 
            // gridViewAnswers
            // 
            this.gridViewAnswers.AllowFocusedRowChanged = true;
            this.gridViewAnswers.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewAnswers.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewAnswers.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewAnswers.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewAnswers.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewAnswers.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewAnswers.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewAnswers.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewAnswers.EnablePreviewLineForFocusedRow = false;
            this.gridViewAnswers.GridControl = this.gridControlAnswers;
            this.gridViewAnswers.Name = "gridViewAnswers";
            this.gridViewAnswers.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewAnswers.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewAnswers.ViewTotalRows = false;
            this.gridViewAnswers.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewAnswers_FocusedRowChanged);
            this.gridViewAnswers.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewAnswers_CellValueChanging);
            // 
            // listBoxNotAssigned
            // 
            this.listBoxNotAssigned.FormattingEnabled = true;
            this.listBoxNotAssigned.HorizontalScrollbar = true;
            this.listBoxNotAssigned.IntegralHeight = false;
            this.listBoxNotAssigned.Location = new System.Drawing.Point(17, 178);
            this.listBoxNotAssigned.Name = "listBoxNotAssigned";
            this.listBoxNotAssigned.Size = new System.Drawing.Size(290, 351);
            this.listBoxNotAssigned.Sorted = true;
            this.listBoxNotAssigned.TabIndex = 0;
            this.listBoxNotAssigned.DoubleClick += new System.EventHandler(this.listBoxNotAssigned_DoubleClick);
            this.listBoxNotAssigned.Enter += new System.EventHandler(this.listBoxNotAssigned_Enter);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupDataQuestion,
            this.layoutControlGroupAnswersIncidents,
            this.emptySpaceItem1,
            this.layoutControlItem9,
            this.layoutControlItem10});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(673, 582);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroupDataQuestion
            // 
            this.layoutControlGroupDataQuestion.CustomizationFormText = "Datos de la pregunta de tipificacion";
            this.layoutControlGroupDataQuestion.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemCustomCode,
            this.layoutControlItemPriority,
            this.layoutControlItemName});
            this.layoutControlGroupDataQuestion.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupDataQuestion.Name = "layoutControlGroupDataQuestion";
            this.layoutControlGroupDataQuestion.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupDataQuestion.Size = new System.Drawing.Size(673, 85);
            this.layoutControlGroupDataQuestion.Text = "Datos de la pregunta de tipificacion";
            // 
            // layoutControlItemCustomCode
            // 
            this.layoutControlItemCustomCode.Control = this.textBoxExMnemonic;
            this.layoutControlItemCustomCode.CustomizationFormText = "Mnemnico: *";
            this.layoutControlItemCustomCode.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItemCustomCode.MaxSize = new System.Drawing.Size(252, 31);
            this.layoutControlItemCustomCode.MinSize = new System.Drawing.Size(252, 31);
            this.layoutControlItemCustomCode.Name = "layoutControlItemCustomCode";
            this.layoutControlItemCustomCode.Size = new System.Drawing.Size(252, 31);
            this.layoutControlItemCustomCode.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemCustomCode.Text = "Mnemnico: *";
            this.layoutControlItemCustomCode.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItemCustomCode.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItemCustomCode.TextToControlDistance = 5;
            // 
            // layoutControlItemPriority
            // 
            this.layoutControlItemPriority.Control = this.checkBoxExPriority;
            this.layoutControlItemPriority.CustomizationFormText = "layoutControlItemPriority";
            this.layoutControlItemPriority.Location = new System.Drawing.Point(252, 24);
            this.layoutControlItemPriority.MinSize = new System.Drawing.Size(85, 30);
            this.layoutControlItemPriority.Name = "layoutControlItemPriority";
            this.layoutControlItemPriority.Size = new System.Drawing.Size(411, 31);
            this.layoutControlItemPriority.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemPriority.Text = "layoutControlItemPriority";
            this.layoutControlItemPriority.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemPriority.TextToControlDistance = 0;
            this.layoutControlItemPriority.TextVisible = false;
            // 
            // layoutControlItemName
            // 
            this.layoutControlItemName.Control = this.textBoxExName;
            this.layoutControlItemName.CustomizationFormText = "Nombre: *";
            this.layoutControlItemName.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemName.Name = "layoutControlItemName";
            this.layoutControlItemName.Size = new System.Drawing.Size(663, 24);
            this.layoutControlItemName.Text = "Nombre: *";
            this.layoutControlItemName.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItemName.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItemName.TextToControlDistance = 5;
            // 
            // layoutControlGroupAnswersIncidents
            // 
            this.layoutControlGroupAnswersIncidents.CustomizationFormText = "layoutControlGroup5";
            this.layoutControlGroupAnswersIncidents.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroupAnswersIncidents.Location = new System.Drawing.Point(0, 85);
            this.layoutControlGroupAnswersIncidents.Name = "layoutControlGroupAnswersIncidents";
            this.layoutControlGroupAnswersIncidents.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupAnswersIncidents.Size = new System.Drawing.Size(673, 461);
            this.layoutControlGroupAnswersIncidents.Text = "Respuestas e Incidentes";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroupAnswers;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(663, 431);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupAnswers,
            this.layoutControlGroupProcedures,
            this.layoutControlGroupIncidents});
            this.tabbedControlGroup1.Text = "tabbedControlGroup1";
            // 
            // layoutControlGroupAnswers
            // 
            this.layoutControlGroupAnswers.CustomizationFormText = "Respuestas";
            this.layoutControlGroupAnswers.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupAnswerType,
            this.layoutControlGroupAnswerDefinition,
            this.layoutControlGroupControlParameters});
            this.layoutControlGroupAnswers.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupAnswers.Name = "layoutControlGroupAnswers";
            this.layoutControlGroupAnswers.Size = new System.Drawing.Size(653, 401);
            this.layoutControlGroupAnswers.Text = "Respuestas";
            // 
            // layoutControlGroupAnswerType
            // 
            this.layoutControlGroupAnswerType.CustomizationFormText = "Tipo de respuestas *";
            this.layoutControlGroupAnswerType.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem13,
            this.layoutControlSelectControl});
            this.layoutControlGroupAnswerType.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupAnswerType.Name = "layoutControlGroupAnswerType";
            this.layoutControlGroupAnswerType.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupAnswerType.Size = new System.Drawing.Size(653, 95);
            this.layoutControlGroupAnswerType.Text = "Tipo de respuestas *";
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.simpleButtonTrash;
            this.layoutControlItem13.CustomizationFormText = "layoutControlItem13";
            this.layoutControlItem13.Location = new System.Drawing.Point(586, 0);
            this.layoutControlItem13.MaxSize = new System.Drawing.Size(57, 65);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(57, 65);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(57, 65);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Text = "layoutControlItem13";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextToControlDistance = 0;
            this.layoutControlItem13.TextVisible = false;
            // 
            // layoutControlSelectControl
            // 
            this.layoutControlSelectControl.Control = this.listView1;
            this.layoutControlSelectControl.CustomizationFormText = "layoutControlItem12";
            this.layoutControlSelectControl.Location = new System.Drawing.Point(0, 0);
            this.layoutControlSelectControl.MaxSize = new System.Drawing.Size(0, 65);
            this.layoutControlSelectControl.MinSize = new System.Drawing.Size(187, 65);
            this.layoutControlSelectControl.Name = "layoutControlSelectControl";
            this.layoutControlSelectControl.Size = new System.Drawing.Size(586, 65);
            this.layoutControlSelectControl.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlSelectControl.Text = "layoutControlSelectControl";
            this.layoutControlSelectControl.TextSize = new System.Drawing.Size(151, 13);
            // 
            // layoutControlGroupAnswerDefinition
            // 
            this.layoutControlGroupAnswerDefinition.CustomizationFormText = "Definicion de respuestas";
            this.layoutControlGroupAnswerDefinition.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem14});
            this.layoutControlGroupAnswerDefinition.Location = new System.Drawing.Point(0, 95);
            this.layoutControlGroupAnswerDefinition.Name = "layoutControlGroupAnswerDefinition";
            this.layoutControlGroupAnswerDefinition.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupAnswerDefinition.Size = new System.Drawing.Size(653, 145);
            this.layoutControlGroupAnswerDefinition.Text = "Definicion de respuestas";
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.dragDropLayoutControl1;
            this.layoutControlItem14.CustomizationFormText = "layoutControlItem14";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(643, 115);
            this.layoutControlItem14.Text = "layoutControlItem14";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem14.TextToControlDistance = 0;
            this.layoutControlItem14.TextVisible = false;
            // 
            // layoutControlGroupControlParameters
            // 
            this.layoutControlGroupControlParameters.CustomizationFormText = "Parametros de control";
            this.layoutControlGroupControlParameters.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem15});
            this.layoutControlGroupControlParameters.Location = new System.Drawing.Point(0, 240);
            this.layoutControlGroupControlParameters.Name = "layoutControlGroupControlParameters";
            this.layoutControlGroupControlParameters.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupControlParameters.Size = new System.Drawing.Size(653, 161);
            this.layoutControlGroupControlParameters.Text = "Parametros de control";
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.propertyGridControl1;
            this.layoutControlItem15.CustomizationFormText = "layoutControlItem15";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(643, 131);
            this.layoutControlItem15.Text = "layoutControlItem15";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextToControlDistance = 0;
            this.layoutControlItem15.TextVisible = false;
            // 
            // layoutControlGroupProcedures
            // 
            this.layoutControlGroupProcedures.CustomizationFormText = "Procedimientos";
            this.layoutControlGroupProcedures.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupProcedureAnswers,
            this.layoutControlGroupProcedureAsociated});
            this.layoutControlGroupProcedures.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupProcedures.Name = "layoutControlGroupProcedures";
            this.layoutControlGroupProcedures.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupProcedures.Size = new System.Drawing.Size(653, 401);
            this.layoutControlGroupProcedures.Text = "Procedimientos";
            this.layoutControlGroupProcedures.Shown += new System.EventHandler(this.layoutControlGroup3_Shown);
            // 
            // layoutControlGroupProcedureAnswers
            // 
            this.layoutControlGroupProcedureAnswers.CustomizationFormText = "Respuestas";
            this.layoutControlGroupProcedureAnswers.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem11});
            this.layoutControlGroupProcedureAnswers.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupProcedureAnswers.Name = "layoutControlGroupProcedureAnswers";
            this.layoutControlGroupProcedureAnswers.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupProcedureAnswers.Size = new System.Drawing.Size(653, 188);
            this.layoutControlGroupProcedureAnswers.Text = "Respuestas";
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.gridControlAnswers;
            this.layoutControlItem11.CustomizationFormText = "layoutControlItem11";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(643, 158);
            this.layoutControlItem11.Text = "layoutControlItem11";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextToControlDistance = 0;
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlGroupProcedureAsociated
            // 
            this.layoutControlGroupProcedureAsociated.CustomizationFormText = "Procedimientos asociados";
            this.layoutControlGroupProcedureAsociated.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroupProcedureAsociated.Location = new System.Drawing.Point(0, 188);
            this.layoutControlGroupProcedureAsociated.Name = "layoutControlGroupProcedureAsociated";
            this.layoutControlGroupProcedureAsociated.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupProcedureAsociated.Size = new System.Drawing.Size(653, 213);
            this.layoutControlGroupProcedureAsociated.Text = "Procedimientos asociados";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.richTextBoxExtendedProcedure;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(264, 31);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(643, 183);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlGroupIncidents
            // 
            this.layoutControlGroupIncidents.CustomizationFormText = "Asociar a incidentes";
            this.layoutControlGroupIncidents.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupIncidentsType});
            this.layoutControlGroupIncidents.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupIncidents.Name = "layoutControlGroupIncidents";
            this.layoutControlGroupIncidents.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupIncidents.Size = new System.Drawing.Size(653, 401);
            this.layoutControlGroupIncidents.Text = "Asociar a incidentes";
            // 
            // layoutControlGroupIncidentsType
            // 
            this.layoutControlGroupIncidentsType.CustomizationFormText = "Tipo de incidentes";
            this.layoutControlGroupIncidentsType.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem3,
            this.layoutControlItemAsociated,
            this.layoutControlItemNotAsociated,
            this.emptySpaceItem2,
            this.layoutControlItem8,
            this.layoutControlItem7,
            this.layoutControlItem6,
            this.layoutControlItem5});
            this.layoutControlGroupIncidentsType.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupIncidentsType.Name = "layoutControlGroupIncidentsType";
            this.layoutControlGroupIncidentsType.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupIncidentsType.Size = new System.Drawing.Size(653, 401);
            this.layoutControlGroupIncidentsType.Text = "Tipo de incidentes";
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(294, 0);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(32, 0);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(32, 10);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(40, 105);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItemAsociated
            // 
            this.layoutControlItemAsociated.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItemAsociated.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItemAsociated.Control = this.listBoxAssigned;
            this.layoutControlItemAsociated.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItemAsociated.Location = new System.Drawing.Point(334, 0);
            this.layoutControlItemAsociated.MinSize = new System.Drawing.Size(184, 31);
            this.layoutControlItemAsociated.Name = "layoutControlItemAsociated";
            this.layoutControlItemAsociated.Size = new System.Drawing.Size(309, 371);
            this.layoutControlItemAsociated.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemAsociated.Text = "layoutControlItemAsociated";
            this.layoutControlItemAsociated.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItemAsociated.TextSize = new System.Drawing.Size(151, 13);
            // 
            // layoutControlItemNotAsociated
            // 
            this.layoutControlItemNotAsociated.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItemNotAsociated.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItemNotAsociated.Control = this.listBoxNotAssigned;
            this.layoutControlItemNotAsociated.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItemNotAsociated.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemNotAsociated.MinSize = new System.Drawing.Size(184, 31);
            this.layoutControlItemNotAsociated.Name = "layoutControlItemNotAsociated";
            this.layoutControlItemNotAsociated.Size = new System.Drawing.Size(294, 371);
            this.layoutControlItemNotAsociated.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemNotAsociated.Text = "layoutControlItemNotAsociated";
            this.layoutControlItemNotAsociated.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItemNotAsociated.TextSize = new System.Drawing.Size(151, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(294, 237);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(32, 0);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(32, 10);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(40, 134);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.buttonRemoveAllIncidentType;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(294, 204);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(40, 33);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(40, 33);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(40, 33);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.buttonRemoveIncidentType;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(294, 171);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(40, 33);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(40, 33);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(40, 33);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.buttonAddAllIncidentType;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(294, 138);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(40, 33);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(40, 33);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(40, 33);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.buttonAddIncidentType;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(294, 105);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(40, 33);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(40, 33);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(40, 33);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 546);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(501, 36);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.buttonOk;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(501, 546);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.buttonCancel;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(587, 546);
            this.layoutControlItem10.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Text = "layoutControlItem10";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextToControlDistance = 0;
            this.layoutControlItem10.TextVisible = false;
            // 
            // labelAnswerType
            // 
            this.labelAnswerType.AutoSize = true;
            this.labelAnswerType.Location = new System.Drawing.Point(9, 15);
            this.labelAnswerType.Name = "labelAnswerType";
            this.labelAnswerType.Size = new System.Drawing.Size(100, 13);
            this.labelAnswerType.TabIndex = 8;
            this.labelAnswerType.Text = "Tipo de respuestas:";
            // 
            // QuestionForm
            // 
            this.AcceptButton = this.buttonOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(673, 582);
            this.Controls.Add(this.layoutControl1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(681, 561);
            this.Name = "QuestionForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.QuestionForm_Load);
            this.contextMenuStripDesigner.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxExPriority.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.propertyGridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAnswers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewAnswers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupDataQuestion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCustomCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPriority)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupAnswersIncidents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupAnswers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupAnswerType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlSelectControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupAnswerDefinition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupControlParameters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupProcedures)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupProcedureAnswers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupProcedureAsociated)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupIncidents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupIncidentsType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAsociated)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemNotAsociated)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton buttonCancel;
        private DevExpress.XtraEditors.SimpleButton buttonOk;
        private TextBoxEx textBoxExName;
        private TextBoxEx textBoxExMnemonic;
        private System.Windows.Forms.Label labelAnswerType;
        private RichTextBoxExtended richTextBoxExtendedProcedure;
        private DevExpress.XtraEditors.SimpleButton buttonRemoveAllIncidentType;
        private DevExpress.XtraEditors.SimpleButton buttonRemoveIncidentType;
        private DevExpress.XtraEditors.SimpleButton buttonAddAllIncidentType;
        private DevExpress.XtraEditors.SimpleButton buttonAddIncidentType;
        private System.Windows.Forms.ListBox listBoxAssigned;
        private System.Windows.Forms.ListBox listBoxNotAssigned;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripDesigner;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemDesignerDelete;
        private System.Windows.Forms.ToolTip toolTipQuestion;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.CheckEdit checkBoxExPriority;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupDataQuestion;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemCustomCode;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemPriority;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupAnswersIncidents;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupIncidents;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemNotAsociated;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemAsociated;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupProcedures;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupIncidentsType;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupProcedureAnswers;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupProcedureAsociated;
        private System.Windows.Forms.ListView listView1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupAnswers;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlSelectControl;
        private DevExpress.XtraEditors.SimpleButton simpleButtonTrash;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ImageList imageList3;
        private DragDropLayoutControl dragDropLayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupAnswerType;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupAnswerDefinition;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupControlParameters;
        private DevExpress.XtraVerticalGrid.PropertyGridControl propertyGridControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private GridControlEx gridControlAnswers;
        private GridViewEx gridViewAnswers;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit;
    }
}
