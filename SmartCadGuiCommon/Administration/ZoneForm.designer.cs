using SmartCadControls.Controls;

namespace SmartCadGuiCommon
{
    partial class ZoneForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ZoneForm));
            this.textBoxExName = new TextBoxEx();
            this.buttonAdd = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlZone = new DevExpress.XtraLayout.LayoutControl();
            this.buttonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.buttonRemoveAll = new DevExpress.XtraEditors.SimpleButton();
            this.buttonOk = new DevExpress.XtraEditors.SimpleButton();
            this.buttonAddAll = new DevExpress.XtraEditors.SimpleButton();
            this.buttonRemove = new DevExpress.XtraEditors.SimpleButton();
            this.comboBoxExStructTypes = new ComboBoxEx();
            this.listBoxNotAssigned = new System.Windows.Forms.ListBox();
            this.listBoxAssigned = new System.Windows.Forms.ListBox();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupInf = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupStruct = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemAvailables = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemUnAvailables = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemType = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlZone)).BeginInit();
            this.layoutControlZone.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupInf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupStruct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAvailables)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemUnAvailables)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxExName
            // 
            this.textBoxExName.AllowsLetters = true;
            this.textBoxExName.AllowsNumbers = true;
            this.textBoxExName.AllowsPunctuation = true;
            this.textBoxExName.AllowsSeparators = true;
            this.textBoxExName.AllowsSymbols = true;
            this.textBoxExName.AllowsWhiteSpaces = true;
            this.textBoxExName.ExtraAllowedChars = "";
            this.textBoxExName.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExName.Location = new System.Drawing.Point(165, 34);
            this.textBoxExName.MaxLength = 50;
            this.textBoxExName.Name = "textBoxExName";
            this.textBoxExName.NonAllowedCharacters = "";
            this.textBoxExName.RegularExpresion = "";
            this.textBoxExName.Size = new System.Drawing.Size(290, 20);
            this.textBoxExName.TabIndex = 0;
            this.textBoxExName.TextChanged += new System.EventHandler(this.ZoneStatusParameters_Change);
            // 
            // buttonAdd
            // 
            this.buttonAdd.AccessibleDescription = "Algo";
            this.buttonAdd.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.buttonAdd.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonAdd.Appearance.Options.UseFont = true;
            this.buttonAdd.Appearance.Options.UseForeColor = true;
            this.buttonAdd.Location = new System.Drawing.Point(216, 146);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(46, 34);
            this.buttonAdd.StyleController = this.layoutControlZone;
            this.buttonAdd.TabIndex = 2;
            this.buttonAdd.Text = ">";
            this.toolTip1.SetToolTip(this.buttonAdd, "AddStructure");
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // layoutControlZone
            // 
            this.layoutControlZone.AllowCustomizationMenu = false;
            this.layoutControlZone.Controls.Add(this.buttonCancel);
            this.layoutControlZone.Controls.Add(this.buttonRemoveAll);
            this.layoutControlZone.Controls.Add(this.buttonOk);
            this.layoutControlZone.Controls.Add(this.buttonAdd);
            this.layoutControlZone.Controls.Add(this.buttonAddAll);
            this.layoutControlZone.Controls.Add(this.buttonRemove);
            this.layoutControlZone.Controls.Add(this.textBoxExName);
            this.layoutControlZone.Controls.Add(this.comboBoxExStructTypes);
            this.layoutControlZone.Controls.Add(this.listBoxNotAssigned);
            this.layoutControlZone.Controls.Add(this.listBoxAssigned);
            this.layoutControlZone.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlZone.Location = new System.Drawing.Point(0, 0);
            this.layoutControlZone.Name = "layoutControlZone";
            this.layoutControlZone.OptionsCustomizationForm.ShowLayoutTreeView = false;
            this.layoutControlZone.Root = this.layoutControlGroup1;
            this.layoutControlZone.Size = new System.Drawing.Size(469, 372);
            this.layoutControlZone.TabIndex = 5;
            this.layoutControlZone.Text = "layoutControl1";
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.buttonCancel.Appearance.Options.UseFont = true;
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(385, 338);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(82, 32);
            this.buttonCancel.StyleController = this.layoutControlZone;
            this.buttonCancel.TabIndex = 3;
            this.buttonCancel.Text = "Cancelar  ";
            // 
            // buttonRemoveAll
            // 
            this.buttonRemoveAll.AccessibleDescription = "Algo";
            this.buttonRemoveAll.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.buttonRemoveAll.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonRemoveAll.Appearance.Options.UseFont = true;
            this.buttonRemoveAll.Appearance.Options.UseForeColor = true;
            this.buttonRemoveAll.Location = new System.Drawing.Point(216, 260);
            this.buttonRemoveAll.Name = "buttonRemoveAll";
            this.buttonRemoveAll.Size = new System.Drawing.Size(46, 34);
            this.buttonRemoveAll.StyleController = this.layoutControlZone;
            this.buttonRemoveAll.TabIndex = 5;
            this.buttonRemoveAll.Text = "<<";
            this.toolTip1.SetToolTip(this.buttonRemoveAll, "RemoveAllStructure");
            this.buttonRemoveAll.Click += new System.EventHandler(this.buttonRemoveAll_Click);
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.buttonOk.Appearance.Options.UseFont = true;
            this.buttonOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOk.Enabled = false;
            this.buttonOk.Location = new System.Drawing.Point(299, 338);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(82, 32);
            this.buttonOk.StyleController = this.layoutControlZone;
            this.buttonOk.TabIndex = 2;
            this.buttonOk.Text = "Accept";
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonAddAll
            // 
            this.buttonAddAll.AccessibleDescription = "Algo";
            this.buttonAddAll.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.buttonAddAll.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonAddAll.Appearance.Options.UseFont = true;
            this.buttonAddAll.Appearance.Options.UseForeColor = true;
            this.buttonAddAll.Location = new System.Drawing.Point(216, 184);
            this.buttonAddAll.Name = "buttonAddAll";
            this.buttonAddAll.Size = new System.Drawing.Size(46, 34);
            this.buttonAddAll.StyleController = this.layoutControlZone;
            this.buttonAddAll.TabIndex = 3;
            this.buttonAddAll.Text = ">>";
            this.toolTip1.SetToolTip(this.buttonAddAll, "AddAllStructure");
            this.buttonAddAll.Click += new System.EventHandler(this.buttonAddAll_Click);
            // 
            // buttonRemove
            // 
            this.buttonRemove.AccessibleDescription = "Algo";
            this.buttonRemove.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.buttonRemove.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonRemove.Appearance.Options.UseFont = true;
            this.buttonRemove.Appearance.Options.UseForeColor = true;
            this.buttonRemove.Location = new System.Drawing.Point(216, 222);
            this.buttonRemove.Name = "buttonRemove";
            this.buttonRemove.Size = new System.Drawing.Size(46, 34);
            this.buttonRemove.StyleController = this.layoutControlZone;
            this.buttonRemove.TabIndex = 4;
            this.buttonRemove.Text = "<";
            this.toolTip1.SetToolTip(this.buttonRemove, "RemoveStructure");
            this.buttonRemove.Click += new System.EventHandler(this.buttonRemove_Click);
            // 
            // comboBoxExStructTypes
            // 
            this.comboBoxExStructTypes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxExStructTypes.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.comboBoxExStructTypes.FormattingEnabled = true;
            this.comboBoxExStructTypes.Location = new System.Drawing.Point(165, 102);
            this.comboBoxExStructTypes.Name = "comboBoxExStructTypes";
            this.comboBoxExStructTypes.Size = new System.Drawing.Size(290, 21);
            this.comboBoxExStructTypes.TabIndex = 0;
            this.comboBoxExStructTypes.SelectedIndexChanged += new System.EventHandler(this.comboBoxExStructTypes_SelectedIndexChanged);
            // 
            // listBoxNotAssigned
            // 
            this.listBoxNotAssigned.AllowDrop = true;
            this.listBoxNotAssigned.BackColor = System.Drawing.SystemColors.Window;
            this.listBoxNotAssigned.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.listBoxNotAssigned.FormattingEnabled = true;
            this.listBoxNotAssigned.HorizontalScrollbar = true;
            this.listBoxNotAssigned.IntegralHeight = false;
            this.listBoxNotAssigned.Location = new System.Drawing.Point(14, 143);
            this.listBoxNotAssigned.Name = "listBoxNotAssigned";
            this.listBoxNotAssigned.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listBoxNotAssigned.Size = new System.Drawing.Size(198, 179);
            this.listBoxNotAssigned.Sorted = true;
            this.listBoxNotAssigned.TabIndex = 1;
            this.listBoxNotAssigned.DragDrop += new System.Windows.Forms.DragEventHandler(this.listBoxNotAssigned_DragDrop);
            this.listBoxNotAssigned.DoubleClick += new System.EventHandler(this.listBoxNotAssigned_DoubleClick);
            // 
            // listBoxAssigned
            // 
            this.listBoxAssigned.AllowDrop = true;
            this.listBoxAssigned.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.listBoxAssigned.FormattingEnabled = true;
            this.listBoxAssigned.HorizontalScrollbar = true;
            this.listBoxAssigned.IntegralHeight = false;
            this.listBoxAssigned.Location = new System.Drawing.Point(266, 143);
            this.listBoxAssigned.Name = "listBoxAssigned";
            this.listBoxAssigned.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listBoxAssigned.Size = new System.Drawing.Size(189, 179);
            this.listBoxAssigned.Sorted = true;
            this.listBoxAssigned.TabIndex = 6;
            this.listBoxAssigned.DragDrop += new System.Windows.Forms.DragEventHandler(this.listBoxAssigned_DragDrop);
            this.listBoxAssigned.DoubleClick += new System.EventHandler(this.listBoxAssigned_DoubleClick);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupInf,
            this.layoutControlGroupStruct,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.emptySpaceItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(469, 372);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroupInf
            // 
            this.layoutControlGroupInf.CustomizationFormText = "layoutControlGroupInf";
            this.layoutControlGroupInf.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemName});
            this.layoutControlGroupInf.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupInf.Name = "layoutControlGroupInf";
            this.layoutControlGroupInf.Size = new System.Drawing.Size(469, 68);
            this.layoutControlGroupInf.Text = "layoutControlGroupInf";
            // 
            // layoutControlItemName
            // 
            this.layoutControlItemName.Control = this.textBoxExName;
            this.layoutControlItemName.CustomizationFormText = "layoutControlItemName";
            this.layoutControlItemName.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemName.Name = "layoutControlItemName";
            this.layoutControlItemName.Size = new System.Drawing.Size(445, 24);
            this.layoutControlItemName.Text = "layoutControlItemName";
            this.layoutControlItemName.TextSize = new System.Drawing.Size(148, 13);
            // 
            // layoutControlGroupStruct
            // 
            this.layoutControlGroupStruct.CustomizationFormText = "layoutControlGroupStruct";
            this.layoutControlGroupStruct.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemAvailables,
            this.layoutControlItemUnAvailables,
            this.layoutControlItem4,
            this.layoutControlItem3,
            this.layoutControlItem2,
            this.layoutControlItem1,
            this.layoutControlItemType,
            this.emptySpaceItem2,
            this.emptySpaceItem3});
            this.layoutControlGroupStruct.Location = new System.Drawing.Point(0, 68);
            this.layoutControlGroupStruct.Name = "layoutControlGroupStruct";
            this.layoutControlGroupStruct.Size = new System.Drawing.Size(469, 268);
            this.layoutControlGroupStruct.Text = "layoutControlGroupStruct";
            // 
            // layoutControlItemAvailables
            // 
            this.layoutControlItemAvailables.Control = this.listBoxNotAssigned;
            this.layoutControlItemAvailables.CustomizationFormText = "layoutControlItemAvailables";
            this.layoutControlItemAvailables.Location = new System.Drawing.Point(0, 25);
            this.layoutControlItemAvailables.Name = "layoutControlItemAvailables";
            this.layoutControlItemAvailables.Size = new System.Drawing.Size(202, 199);
            this.layoutControlItemAvailables.Text = "layoutControlItemAvailables";
            this.layoutControlItemAvailables.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItemAvailables.TextSize = new System.Drawing.Size(148, 13);
            // 
            // layoutControlItemUnAvailables
            // 
            this.layoutControlItemUnAvailables.Control = this.listBoxAssigned;
            this.layoutControlItemUnAvailables.CustomizationFormText = "layoutControlItemUnAvailables";
            this.layoutControlItemUnAvailables.Location = new System.Drawing.Point(252, 25);
            this.layoutControlItemUnAvailables.Name = "layoutControlItemUnAvailables";
            this.layoutControlItemUnAvailables.Size = new System.Drawing.Size(193, 199);
            this.layoutControlItemUnAvailables.Text = "layoutControlItemUnAvailables";
            this.layoutControlItemUnAvailables.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItemUnAvailables.TextSize = new System.Drawing.Size(148, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.buttonRemoveAll;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(202, 158);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(50, 38);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(50, 38);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(50, 38);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.buttonRemove;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(202, 120);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(50, 38);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(50, 38);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(50, 38);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.buttonAddAll;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(202, 82);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(50, 38);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(50, 38);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(50, 38);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.buttonAdd;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(202, 44);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(50, 38);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(50, 38);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(50, 38);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItemType
            // 
            this.layoutControlItemType.Control = this.comboBoxExStructTypes;
            this.layoutControlItemType.CustomizationFormText = "layoutControlItemType";
            this.layoutControlItemType.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemType.Name = "layoutControlItemType";
            this.layoutControlItemType.Size = new System.Drawing.Size(445, 25);
            this.layoutControlItemType.Text = "layoutControlItemType";
            this.layoutControlItemType.TextSize = new System.Drawing.Size(148, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(202, 25);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(50, 19);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(202, 196);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(50, 28);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.buttonOk;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(297, 336);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.buttonCancel;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(383, 336);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 336);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(297, 36);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ZoneForm
            // 
            this.AcceptButton = this.buttonOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(469, 372);
            this.Controls.Add(this.layoutControlZone);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(477, 399);
            this.Name = "ZoneForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.ZoneForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlZone)).EndInit();
            this.layoutControlZone.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupInf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupStruct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAvailables)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemUnAvailables)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TextBoxEx textBoxExName;
        private System.Windows.Forms.ListBox listBoxNotAssigned;
        private System.Windows.Forms.ListBox listBoxAssigned;
        private DevExpress.XtraEditors.SimpleButton buttonCancel;
        private DevExpress.XtraEditors.SimpleButton buttonOk;
        private ComboBoxEx comboBoxExStructTypes;
        private System.Windows.Forms.ToolTip toolTip1;
        private DevExpress.XtraEditors.SimpleButton buttonAdd;
        private DevExpress.XtraEditors.SimpleButton buttonRemoveAll;
        private DevExpress.XtraEditors.SimpleButton buttonAddAll;
        private DevExpress.XtraEditors.SimpleButton buttonRemove;
        private DevExpress.XtraLayout.LayoutControl layoutControlZone;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupInf;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemType;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupStruct;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemAvailables;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemUnAvailables;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
    }
}