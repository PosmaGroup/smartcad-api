using SmartCadControls;
using SmartCadControls.Controls;
using SmartCadCore.Common;

namespace SmartCadGuiCommon
{
    partial class UnitOfficerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private new System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            DatagGridDefaultGroup datagGridDefaultGroup2 = new DatagGridDefaultGroup();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.buttonOk = new DevExpress.XtraEditors.SimpleButton();
            this.UnitOfficerFormConvertedLayout = new DevExpress.XtraLayout.LayoutControl();
            this.dataGridExAssing = new DataGridEx();
            this.comboBoxDepartmentStation = new System.Windows.Forms.ComboBox();
            this.dateTimePickerFrom = new DateTimePickerEx();
            this.dateTimePickerTo = new DateTimePickerEx();
            this.simpleButtonFinalize = new DevExpress.XtraEditors.SimpleButton();
            this.buttonImportUnitOfficerAssign = new DevExpress.XtraEditors.SimpleButton();
            this.comboBoxDepartmentZone = new System.Windows.Forms.ComboBox();
            this.comboBoxDepartmentType = new System.Windows.Forms.ComboBox();
            this.textBoxImportUnitOfficerAssign = new TextBoxEx();
            this.buttonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupAssigns = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupDepartment = new DevExpress.XtraLayout.LayoutControlGroup();
            this.comboBoxDepartmentStationitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.comboBoxDepartmentZoneitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.comboBoxDepartmentTypeitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupImport = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupPeriod = new DevExpress.XtraLayout.LayoutControlGroup();
            this.dateTimePickerFromitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.dateTimePickerToitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItemFinalize = new DevExpress.XtraLayout.LayoutControlItem();
            this.toolTipUnitOfficerAssign = new System.Windows.Forms.ToolTip(this.components);
            this.openFileDialogExcelDoc = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.UnitOfficerFormConvertedLayout)).BeginInit();
            this.UnitOfficerFormConvertedLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridExAssing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupAssigns)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupDepartment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxDepartmentStationitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxDepartmentZoneitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxDepartmentTypeitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupImport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateTimePickerFromitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateTimePickerToitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemFinalize)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonOk
            // 
            this.buttonOk.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOk.Appearance.Options.UseFont = true;
            this.buttonOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOk.Location = new System.Drawing.Point(496, 402);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(82, 32);
            this.buttonOk.StyleController = this.UnitOfficerFormConvertedLayout;
            this.buttonOk.TabIndex = 7;
            this.buttonOk.Text = "Accept";
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // UnitOfficerFormConvertedLayout
            // 
            this.UnitOfficerFormConvertedLayout.AllowCustomizationMenu = false;
            this.UnitOfficerFormConvertedLayout.Controls.Add(this.dataGridExAssing);
            this.UnitOfficerFormConvertedLayout.Controls.Add(this.comboBoxDepartmentStation);
            this.UnitOfficerFormConvertedLayout.Controls.Add(this.dateTimePickerFrom);
            this.UnitOfficerFormConvertedLayout.Controls.Add(this.dateTimePickerTo);
            this.UnitOfficerFormConvertedLayout.Controls.Add(this.simpleButtonFinalize);
            this.UnitOfficerFormConvertedLayout.Controls.Add(this.buttonImportUnitOfficerAssign);
            this.UnitOfficerFormConvertedLayout.Controls.Add(this.comboBoxDepartmentZone);
            this.UnitOfficerFormConvertedLayout.Controls.Add(this.comboBoxDepartmentType);
            this.UnitOfficerFormConvertedLayout.Controls.Add(this.textBoxImportUnitOfficerAssign);
            this.UnitOfficerFormConvertedLayout.Controls.Add(this.buttonCancel);
            this.UnitOfficerFormConvertedLayout.Controls.Add(this.buttonOk);
            this.UnitOfficerFormConvertedLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UnitOfficerFormConvertedLayout.Location = new System.Drawing.Point(0, 0);
            this.UnitOfficerFormConvertedLayout.Name = "UnitOfficerFormConvertedLayout";
            this.UnitOfficerFormConvertedLayout.OptionsCustomizationForm.ShowLayoutTreeView = false;
            this.UnitOfficerFormConvertedLayout.Root = this.layoutControlGroup1;
            this.UnitOfficerFormConvertedLayout.Size = new System.Drawing.Size(666, 436);
            this.UnitOfficerFormConvertedLayout.TabIndex = 9;
            // 
            // dataGridExAssing
            // 
            this.dataGridExAssing.AllowDrop = true;
            this.dataGridExAssing.AllowEditing = false;
            this.dataGridExAssing.AllowUserToAddRows = false;
            this.dataGridExAssing.AllowUserToDeleteRows = false;
            this.dataGridExAssing.AllowUserToOrderColumns = true;
            this.dataGridExAssing.AllowUserToResizeRows = false;
            this.dataGridExAssing.AllowUserToSortColumns = true;
            this.dataGridExAssing.BackgroundColor = System.Drawing.Color.White;
            this.dataGridExAssing.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridExAssing.ColumnHeadersBackColor = System.Drawing.Color.Empty;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Tahoma", 8.25F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridExAssing.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridExAssing.ColumnHeadersHeight = 20;
            this.dataGridExAssing.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Tahoma", 8.25F);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(31)))), ((int)(((byte)(53)))));
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridExAssing.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridExAssing.Editing = false;
            this.dataGridExAssing.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridExAssing.EnabledSelectionHandler = true;
            this.dataGridExAssing.EnableHeadersVisualStyles = false;
            this.dataGridExAssing.EnableSystemShortCuts = false;
            this.dataGridExAssing.Grouping = false;
            datagGridDefaultGroup2.Collapsed = false;
            datagGridDefaultGroup2.Column = null;
            datagGridDefaultGroup2.Height = 34;
            datagGridDefaultGroup2.ItemCount = 0;
            datagGridDefaultGroup2.Text = "";
            datagGridDefaultGroup2.Value = null;
            this.dataGridExAssing.GroupTemplate = datagGridDefaultGroup2;
            this.dataGridExAssing.Location = new System.Drawing.Point(7, 253);
            this.dataGridExAssing.MultiSelect = false;
            this.dataGridExAssing.Name = "dataGridExAssing";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Tahoma", 8.25F);
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridExAssing.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridExAssing.SelectedItemBackColor = System.Drawing.Color.Empty;
            this.dataGridExAssing.SelectedItemForeColor = System.Drawing.Color.Empty;
            this.dataGridExAssing.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridExAssing.Size = new System.Drawing.Size(652, 140);
            this.dataGridExAssing.SortedColumnColor = System.Drawing.Color.Empty;
            this.dataGridExAssing.StandardTab = true;
            this.dataGridExAssing.TabIndex = 6;
            this.dataGridExAssing.Type = null;
            this.dataGridExAssing.VirtualMode = true;
            this.dataGridExAssing.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridExAssing_CellContentClick);
            this.dataGridExAssing.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridExAssing_CellMouseClick);
            this.dataGridExAssing.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.ColumnHeaderClick);
            this.dataGridExAssing.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridExAssing_KeyDown);
            // 
            // comboBoxDepartmentStation
            // 
            this.comboBoxDepartmentStation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDepartmentStation.Enabled = false;
            this.comboBoxDepartmentStation.FormattingEnabled = true;
            this.comboBoxDepartmentStation.Location = new System.Drawing.Point(168, 138);
            this.comboBoxDepartmentStation.Name = "comboBoxDepartmentStation";
            this.comboBoxDepartmentStation.Size = new System.Drawing.Size(491, 21);
            this.comboBoxDepartmentStation.Sorted = true;
            this.comboBoxDepartmentStation.TabIndex = 4;
            this.comboBoxDepartmentStation.SelectionChangeCommitted += new System.EventHandler(this.comboBoxDepartmentStation_SelectionChangeCommitted);
            this.comboBoxDepartmentStation.Enter += new System.EventHandler(this.comboBoxDepartmentStation_Enter);
            this.comboBoxDepartmentStation.Leave += new System.EventHandler(this.comboBoxDepartmentStation_Leave);
            // 
            // dateTimePickerFrom
            // 
            this.dateTimePickerFrom.CustomFormat = "MM/dd/yyyy hh:mm tt";
            this.dateTimePickerFrom.Enabled = false;
            this.dateTimePickerFrom.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.dateTimePickerFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerFrom.Location = new System.Drawing.Point(168, 192);
            this.dateTimePickerFrom.Name = "dateTimePickerFrom";
            this.dateTimePickerFrom.Size = new System.Drawing.Size(138, 20);
            this.dateTimePickerFrom.TabIndex = 5;
            this.dateTimePickerFrom.Value = new System.DateTime(2007, 5, 17, 22, 55, 0, 0);
            this.dateTimePickerFrom.ValueChanged += new System.EventHandler(this.dateTimePickerTo_ValueChanged);
            this.dateTimePickerFrom.Leave += new System.EventHandler(this.dateTimePickerTo_ValueChanged);
            // 
            // dateTimePickerTo
            // 
            this.dateTimePickerTo.CustomFormat = "MM/dd/yyyy hh:mm tt";
            this.dateTimePickerTo.Enabled = false;
            this.dateTimePickerTo.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.dateTimePickerTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerTo.Location = new System.Drawing.Point(471, 192);
            this.dateTimePickerTo.Name = "dateTimePickerTo";
            this.dateTimePickerTo.Size = new System.Drawing.Size(143, 20);
            this.dateTimePickerTo.TabIndex = 6;
            this.dateTimePickerTo.Value = new System.DateTime(2007, 5, 17, 22, 55, 0, 0);
            this.dateTimePickerTo.ValueChanged += new System.EventHandler(this.dateTimePickerTo_ValueChanged);
            this.dateTimePickerTo.Leave += new System.EventHandler(this.dateTimePickerTo_ValueChanged);
            // 
            // simpleButtonFinalize
            // 
            this.simpleButtonFinalize.Enabled = false;
            this.simpleButtonFinalize.Location = new System.Drawing.Point(2, 402);
            this.simpleButtonFinalize.Name = "simpleButtonFinalize";
            this.simpleButtonFinalize.Size = new System.Drawing.Size(100, 22);
            this.simpleButtonFinalize.StyleController = this.UnitOfficerFormConvertedLayout;
            this.simpleButtonFinalize.TabIndex = 9;
            this.simpleButtonFinalize.Text = "simpleButton1";
            this.simpleButtonFinalize.Click += new System.EventHandler(this.simpleButtonFinalize_Click);
            // 
            // buttonImportUnitOfficerAssign
            // 
            this.buttonImportUnitOfficerAssign.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonImportUnitOfficerAssign.Appearance.Options.UseForeColor = true;
            this.buttonImportUnitOfficerAssign.Location = new System.Drawing.Point(616, 26);
            this.buttonImportUnitOfficerAssign.Name = "buttonImportUnitOfficerAssign";
            this.buttonImportUnitOfficerAssign.Size = new System.Drawing.Size(43, 29);
            this.buttonImportUnitOfficerAssign.StyleController = this.UnitOfficerFormConvertedLayout;
            this.buttonImportUnitOfficerAssign.TabIndex = 1;
            this.buttonImportUnitOfficerAssign.Text = ". . .";
            this.toolTipUnitOfficerAssign.SetToolTip(this.buttonImportUnitOfficerAssign, "Abrir archivo");
            this.buttonImportUnitOfficerAssign.Click += new System.EventHandler(this.buttonImportUnitOfficerAssign_Click);
            this.buttonImportUnitOfficerAssign.MouseLeave += new System.EventHandler(this.buttonImportUnitOfficerAssign_MouseLeave);
            this.buttonImportUnitOfficerAssign.MouseHover += new System.EventHandler(this.buttonImportUnitOfficerAssign_MouseHover);
            // 
            // comboBoxDepartmentZone
            // 
            this.comboBoxDepartmentZone.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDepartmentZone.Enabled = false;
            this.comboBoxDepartmentZone.FormattingEnabled = true;
            this.comboBoxDepartmentZone.Location = new System.Drawing.Point(168, 113);
            this.comboBoxDepartmentZone.Name = "comboBoxDepartmentZone";
            this.comboBoxDepartmentZone.Size = new System.Drawing.Size(491, 21);
            this.comboBoxDepartmentZone.Sorted = true;
            this.comboBoxDepartmentZone.TabIndex = 3;
            this.comboBoxDepartmentZone.SelectedIndexChanged += new System.EventHandler(this.comboBoxDepartmentZone_SelectedIndexChanged);
            this.comboBoxDepartmentZone.Enter += new System.EventHandler(this.comboBoxDepartmentZone_Enter);
            // 
            // comboBoxDepartmentType
            // 
            this.comboBoxDepartmentType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDepartmentType.Enabled = false;
            this.comboBoxDepartmentType.FormattingEnabled = true;
            this.comboBoxDepartmentType.Location = new System.Drawing.Point(168, 88);
            this.comboBoxDepartmentType.Name = "comboBoxDepartmentType";
            this.comboBoxDepartmentType.Size = new System.Drawing.Size(491, 21);
            this.comboBoxDepartmentType.Sorted = true;
            this.comboBoxDepartmentType.TabIndex = 2;
            this.comboBoxDepartmentType.SelectedIndexChanged += new System.EventHandler(this.comboBoxDepartmentType_SelectedIndexChanged);
            // 
            // textBoxImportUnitOfficerAssign
            // 
            this.textBoxImportUnitOfficerAssign.AllowsLetters = true;
            this.textBoxImportUnitOfficerAssign.AllowsNumbers = true;
            this.textBoxImportUnitOfficerAssign.AllowsPunctuation = true;
            this.textBoxImportUnitOfficerAssign.AllowsSeparators = true;
            this.textBoxImportUnitOfficerAssign.AllowsSymbols = true;
            this.textBoxImportUnitOfficerAssign.AllowsWhiteSpaces = true;
            this.textBoxImportUnitOfficerAssign.Enabled = false;
            this.textBoxImportUnitOfficerAssign.ExtraAllowedChars = "";
            this.textBoxImportUnitOfficerAssign.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxImportUnitOfficerAssign.Location = new System.Drawing.Point(7, 26);
            this.textBoxImportUnitOfficerAssign.Name = "textBoxImportUnitOfficerAssign";
            this.textBoxImportUnitOfficerAssign.NonAllowedCharacters = "";
            this.textBoxImportUnitOfficerAssign.RegularExpresion = "";
            this.textBoxImportUnitOfficerAssign.Size = new System.Drawing.Size(605, 20);
            this.textBoxImportUnitOfficerAssign.TabIndex = 0;
            this.textBoxImportUnitOfficerAssign.Leave += new System.EventHandler(this.textBoxImportUnitOfficerAssign_Leave);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCancel.Appearance.Options.UseFont = true;
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(582, 402);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(82, 32);
            this.buttonCancel.StyleController = this.UnitOfficerFormConvertedLayout;
            this.buttonCancel.TabIndex = 8;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.emptySpaceItem1,
            this.layoutControlGroupAssigns,
            this.layoutControlGroupDepartment,
            this.layoutControlGroupImport,
            this.layoutControlGroupPeriod,
            this.layoutControlItemFinalize});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(666, 436);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.buttonCancel;
            this.layoutControlItem9.CustomizationFormText = "buttonCancelitem";
            this.layoutControlItem9.Location = new System.Drawing.Point(580, 400);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem9.Name = "buttonCancelitem";
            this.layoutControlItem9.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "buttonCancelitem";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.buttonOk;
            this.layoutControlItem10.CustomizationFormText = "buttonOkitem";
            this.layoutControlItem10.Location = new System.Drawing.Point(494, 400);
            this.layoutControlItem10.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem10.Name = "buttonOkitem";
            this.layoutControlItem10.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Text = "buttonOkitem";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextToControlDistance = 0;
            this.layoutControlItem10.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(104, 400);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(390, 36);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupAssigns
            // 
            this.layoutControlGroupAssigns.AppearanceGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupAssigns.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroupAssigns.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupAssigns.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroupAssigns.CustomizationFormText = "layoutControlGroupAssigns";
            this.layoutControlGroupAssigns.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroupAssigns.Location = new System.Drawing.Point(0, 227);
            this.layoutControlGroupAssigns.Name = "layoutControlGroupAssigns";
            this.layoutControlGroupAssigns.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupAssigns.Size = new System.Drawing.Size(666, 173);
            this.layoutControlGroupAssigns.Text = "layoutControlGroupAssigns";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataGridExAssing;
            this.layoutControlItem1.CustomizationFormText = "dataGridExAssingitem";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "dataGridExAssingitem";
            this.layoutControlItem1.Size = new System.Drawing.Size(656, 144);
            this.layoutControlItem1.Text = "dataGridExAssingitem";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroupDepartment
            // 
            this.layoutControlGroupDepartment.AppearanceGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupDepartment.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroupDepartment.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupDepartment.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroupDepartment.CustomizationFormText = "layoutControlGroupDepartment";
            this.layoutControlGroupDepartment.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.comboBoxDepartmentStationitem,
            this.comboBoxDepartmentZoneitem,
            this.comboBoxDepartmentTypeitem});
            this.layoutControlGroupDepartment.Location = new System.Drawing.Point(0, 62);
            this.layoutControlGroupDepartment.Name = "layoutControlGroupDepartment";
            this.layoutControlGroupDepartment.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupDepartment.Size = new System.Drawing.Size(666, 104);
            this.layoutControlGroupDepartment.Text = "layoutControlGroupDepartment";
            // 
            // comboBoxDepartmentStationitem
            // 
            this.comboBoxDepartmentStationitem.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxDepartmentStationitem.AppearanceItemCaption.Options.UseFont = true;
            this.comboBoxDepartmentStationitem.Control = this.comboBoxDepartmentStation;
            this.comboBoxDepartmentStationitem.CustomizationFormText = "comboBoxDepartmentStationitem";
            this.comboBoxDepartmentStationitem.Location = new System.Drawing.Point(0, 50);
            this.comboBoxDepartmentStationitem.Name = "comboBoxDepartmentStationitem";
            this.comboBoxDepartmentStationitem.Size = new System.Drawing.Size(656, 25);
            this.comboBoxDepartmentStationitem.Text = "comboBoxDepartmentStationitem";
            this.comboBoxDepartmentStationitem.TextSize = new System.Drawing.Size(157, 13);
            // 
            // comboBoxDepartmentZoneitem
            // 
            this.comboBoxDepartmentZoneitem.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxDepartmentZoneitem.AppearanceItemCaption.Options.UseFont = true;
            this.comboBoxDepartmentZoneitem.Control = this.comboBoxDepartmentZone;
            this.comboBoxDepartmentZoneitem.CustomizationFormText = "comboBoxDepartmentZoneitem";
            this.comboBoxDepartmentZoneitem.Location = new System.Drawing.Point(0, 25);
            this.comboBoxDepartmentZoneitem.Name = "comboBoxDepartmentZoneitem";
            this.comboBoxDepartmentZoneitem.Size = new System.Drawing.Size(656, 25);
            this.comboBoxDepartmentZoneitem.Text = "comboBoxDepartmentZoneitem";
            this.comboBoxDepartmentZoneitem.TextSize = new System.Drawing.Size(157, 13);
            // 
            // comboBoxDepartmentTypeitem
            // 
            this.comboBoxDepartmentTypeitem.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxDepartmentTypeitem.AppearanceItemCaption.Options.UseFont = true;
            this.comboBoxDepartmentTypeitem.Control = this.comboBoxDepartmentType;
            this.comboBoxDepartmentTypeitem.CustomizationFormText = "comboBoxDepartmentTypeitem";
            this.comboBoxDepartmentTypeitem.Location = new System.Drawing.Point(0, 0);
            this.comboBoxDepartmentTypeitem.Name = "comboBoxDepartmentTypeitem";
            this.comboBoxDepartmentTypeitem.Size = new System.Drawing.Size(656, 25);
            this.comboBoxDepartmentTypeitem.Text = "comboBoxDepartmentTypeitem";
            this.comboBoxDepartmentTypeitem.TextSize = new System.Drawing.Size(157, 13);
            // 
            // layoutControlGroupImport
            // 
            this.layoutControlGroupImport.AppearanceGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupImport.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroupImport.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupImport.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroupImport.CustomizationFormText = "layoutControlGroupImport";
            this.layoutControlGroupImport.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.layoutControlItem8});
            this.layoutControlGroupImport.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupImport.Name = "layoutControlGroupImport";
            this.layoutControlGroupImport.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupImport.Size = new System.Drawing.Size(666, 62);
            this.layoutControlGroupImport.Text = "layoutControlGroupImport";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.buttonImportUnitOfficerAssign;
            this.layoutControlItem5.CustomizationFormText = "buttonImportUnitOfficerAssignitem";
            this.layoutControlItem5.Location = new System.Drawing.Point(609, 0);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(47, 33);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(47, 33);
            this.layoutControlItem5.Name = "buttonImportUnitOfficerAssignitem";
            this.layoutControlItem5.Size = new System.Drawing.Size(47, 33);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "buttonImportUnitOfficerAssignitem";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.textBoxImportUnitOfficerAssign;
            this.layoutControlItem8.CustomizationFormText = "textBoxImportUnitOfficerAssignitem";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem8.Name = "textBoxImportUnitOfficerAssignitem";
            this.layoutControlItem8.Size = new System.Drawing.Size(609, 33);
            this.layoutControlItem8.Text = "textBoxImportUnitOfficerAssignitem";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlGroupPeriod
            // 
            this.layoutControlGroupPeriod.AppearanceGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupPeriod.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroupPeriod.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupPeriod.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroupPeriod.CustomizationFormText = "layoutControlGroupPeriod";
            this.layoutControlGroupPeriod.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.dateTimePickerFromitem,
            this.dateTimePickerToitem,
            this.emptySpaceItem2});
            this.layoutControlGroupPeriod.Location = new System.Drawing.Point(0, 166);
            this.layoutControlGroupPeriod.Name = "layoutControlGroupPeriod";
            this.layoutControlGroupPeriod.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupPeriod.Size = new System.Drawing.Size(666, 61);
            this.layoutControlGroupPeriod.Text = "layoutControlGroupPeriod";
            // 
            // dateTimePickerFromitem
            // 
            this.dateTimePickerFromitem.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePickerFromitem.AppearanceItemCaption.Options.UseFont = true;
            this.dateTimePickerFromitem.Control = this.dateTimePickerFrom;
            this.dateTimePickerFromitem.CustomizationFormText = "dateTimePickerFromitem";
            this.dateTimePickerFromitem.Location = new System.Drawing.Point(0, 0);
            this.dateTimePickerFromitem.MaxSize = new System.Drawing.Size(303, 32);
            this.dateTimePickerFromitem.MinSize = new System.Drawing.Size(303, 32);
            this.dateTimePickerFromitem.Name = "dateTimePickerFromitem";
            this.dateTimePickerFromitem.Size = new System.Drawing.Size(303, 32);
            this.dateTimePickerFromitem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.dateTimePickerFromitem.Text = "dateTimeFromitem";
            this.dateTimePickerFromitem.TextSize = new System.Drawing.Size(157, 13);
            // 
            // dateTimePickerToitem
            // 
            this.dateTimePickerToitem.Control = this.dateTimePickerTo;
            this.dateTimePickerToitem.CustomizationFormText = "dateTimePickerToitem";
            this.dateTimePickerToitem.Location = new System.Drawing.Point(303, 0);
            this.dateTimePickerToitem.MaxSize = new System.Drawing.Size(308, 32);
            this.dateTimePickerToitem.MinSize = new System.Drawing.Size(308, 32);
            this.dateTimePickerToitem.Name = "dateTimePickerToitem";
            this.dateTimePickerToitem.Size = new System.Drawing.Size(308, 32);
            this.dateTimePickerToitem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.dateTimePickerToitem.Text = "dateTimeToitem";
            this.dateTimePickerToitem.TextSize = new System.Drawing.Size(157, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(611, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(45, 32);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItemFinalize
            // 
            this.layoutControlItemFinalize.Control = this.simpleButtonFinalize;
            this.layoutControlItemFinalize.CustomizationFormText = "layoutControlItemFinalize";
            this.layoutControlItemFinalize.Location = new System.Drawing.Point(0, 400);
            this.layoutControlItemFinalize.Name = "layoutControlItemFinalize";
            this.layoutControlItemFinalize.Size = new System.Drawing.Size(104, 36);
            this.layoutControlItemFinalize.Text = "layoutControlItemFinalize";
            this.layoutControlItemFinalize.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemFinalize.TextToControlDistance = 0;
            this.layoutControlItemFinalize.TextVisible = false;
            // 
            // openFileDialogExcelDoc
            // 
            this.openFileDialogExcelDoc.Filter = "Excel Files (*.xls)|*.xls";
            this.openFileDialogExcelDoc.Title = "Open File";
            // 
            // UnitOfficerForm
            // 
            this.AcceptButton = this.buttonOk;
            this.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(666, 436);
            this.Controls.Add(this.UnitOfficerFormConvertedLayout);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(682, 680);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(637, 0);
            this.Name = "UnitOfficerForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Asignacion de unidades";
            this.Load += new System.EventHandler(this.UnitOfficerForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.UnitOfficerFormConvertedLayout)).EndInit();
            this.UnitOfficerFormConvertedLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridExAssing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupAssigns)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupDepartment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxDepartmentStationitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxDepartmentZoneitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxDepartmentTypeitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupImport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateTimePickerFromitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateTimePickerToitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemFinalize)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton buttonOk;
		private DevExpress.XtraEditors.SimpleButton buttonCancel;
        private System.Windows.Forms.ToolTip toolTipUnitOfficerAssign;
        private DataGridEx dataGridExAssing;
        private System.Windows.Forms.OpenFileDialog openFileDialogExcelDoc;
        private DevExpress.XtraEditors.SimpleButton buttonImportUnitOfficerAssign;
        private System.Windows.Forms.ComboBox comboBoxDepartmentType;
        private System.Windows.Forms.ComboBox comboBoxDepartmentZone;
        private System.Windows.Forms.ComboBox comboBoxDepartmentStation;
        private TextBoxEx textBoxImportUnitOfficerAssign;
        private DateTimePickerEx dateTimePickerTo;
        private DateTimePickerEx dateTimePickerFrom;
        private DevExpress.XtraLayout.LayoutControl UnitOfficerFormConvertedLayout;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem comboBoxDepartmentStationitem;
        private DevExpress.XtraLayout.LayoutControlItem dateTimePickerFromitem;
        private DevExpress.XtraLayout.LayoutControlItem dateTimePickerToitem;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem comboBoxDepartmentZoneitem;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupAssigns;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupDepartment;
        private DevExpress.XtraLayout.LayoutControlItem comboBoxDepartmentTypeitem;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupImport;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupPeriod;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.SimpleButton simpleButtonFinalize;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemFinalize;

    }
}
