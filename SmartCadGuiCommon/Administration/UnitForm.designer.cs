using SmartCadControls.Controls;
using SmartCadCore.Common;

namespace SmartCadGuiCommon
{
    partial class UnitForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.UnitFormConvertedLayout = new DevExpress.XtraLayout.LayoutControl();
            this.comboBoxEditIdGPS = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEditDeviceType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textBoxSerial = new System.Windows.Forms.TextBox();
            this.textBoxIdUnit = new System.Windows.Forms.TextBox();
            this.listBoxNotAssigned = new System.Windows.Forms.ListBox();
            this.listBoxAssigned = new System.Windows.Forms.ListBox();
            this.buttonAddIncidentType = new DevExpress.XtraEditors.SimpleButton();
            this.buttonAddAllIncidentType = new DevExpress.XtraEditors.SimpleButton();
            this.buttonRemoveIncidentType = new DevExpress.XtraEditors.SimpleButton();
            this.buttonRemoveAllIncidentType = new DevExpress.XtraEditors.SimpleButton();
            this.textBoxVelocity = new TextBoxEx();
            this.textBoxDescription = new TextBoxEx();
            this.textBoxSeats = new TextBoxEx();
            this.textBoxBrand = new TextBoxEx();
            this.textBoxLicensePlate = new TextBoxEx();
            this.textBoxYear = new TextBoxEx();
            this.textBoxIDRadio = new TextBoxEx();
            this.textBoxModel = new TextBoxEx();
            this.comboBoxStationType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxZoneType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxUnitType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxDepartmentType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.buttonOk = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupUnitInf = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemBrand = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemPlate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemYear = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemModel = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemIdUnit = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemSerial = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemSeats = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemVelocity = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupDepartment = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemStation = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemZone = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemUnitType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemDepartment = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupIncidentType = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemNotAssigned = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemAssigned = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupUnitFeatures = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemDes = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItemIdRadio = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemDeviceType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemIdGPS = new DevExpress.XtraLayout.LayoutControlItem();
            this.toolTipUnits = new System.Windows.Forms.ToolTip(this.components);
            this.layoutConverter1 = new DevExpress.XtraLayout.Converter.LayoutConverter(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.UnitFormConvertedLayout)).BeginInit();
            this.UnitFormConvertedLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditIdGPS.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditDeviceType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxStationType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxZoneType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxUnitType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxDepartmentType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupUnitInf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBrand)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPlate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemModel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIdUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSerial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSeats)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemVelocity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupDepartment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemZone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemUnitType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupIncidentType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemNotAssigned)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAssigned)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupUnitFeatures)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIdRadio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDeviceType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIdGPS)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCancel.Appearance.Options.UseFont = true;
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(659, 494);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(82, 33);
            this.buttonCancel.StyleController = this.UnitFormConvertedLayout;
            this.buttonCancel.TabIndex = 5;
            // 
            // UnitFormConvertedLayout
            // 
            this.UnitFormConvertedLayout.AllowCustomizationMenu = false;
            this.UnitFormConvertedLayout.Controls.Add(this.comboBoxEditIdGPS);
            this.UnitFormConvertedLayout.Controls.Add(this.comboBoxEditDeviceType);
            this.UnitFormConvertedLayout.Controls.Add(this.textBoxSerial);
            this.UnitFormConvertedLayout.Controls.Add(this.textBoxIdUnit);
            this.UnitFormConvertedLayout.Controls.Add(this.listBoxNotAssigned);
            this.UnitFormConvertedLayout.Controls.Add(this.listBoxAssigned);
            this.UnitFormConvertedLayout.Controls.Add(this.buttonAddIncidentType);
            this.UnitFormConvertedLayout.Controls.Add(this.buttonAddAllIncidentType);
            this.UnitFormConvertedLayout.Controls.Add(this.buttonRemoveIncidentType);
            this.UnitFormConvertedLayout.Controls.Add(this.buttonRemoveAllIncidentType);
            this.UnitFormConvertedLayout.Controls.Add(this.textBoxVelocity);
            this.UnitFormConvertedLayout.Controls.Add(this.textBoxDescription);
            this.UnitFormConvertedLayout.Controls.Add(this.textBoxSeats);
            this.UnitFormConvertedLayout.Controls.Add(this.textBoxBrand);
            this.UnitFormConvertedLayout.Controls.Add(this.textBoxLicensePlate);
            this.UnitFormConvertedLayout.Controls.Add(this.textBoxYear);
            this.UnitFormConvertedLayout.Controls.Add(this.textBoxIDRadio);
            this.UnitFormConvertedLayout.Controls.Add(this.textBoxModel);
            this.UnitFormConvertedLayout.Controls.Add(this.comboBoxStationType);
            this.UnitFormConvertedLayout.Controls.Add(this.comboBoxZoneType);
            this.UnitFormConvertedLayout.Controls.Add(this.comboBoxUnitType);
            this.UnitFormConvertedLayout.Controls.Add(this.comboBoxDepartmentType);
            this.UnitFormConvertedLayout.Controls.Add(this.buttonCancel);
            this.UnitFormConvertedLayout.Controls.Add(this.buttonOk);
            this.UnitFormConvertedLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UnitFormConvertedLayout.Location = new System.Drawing.Point(0, 0);
            this.UnitFormConvertedLayout.Name = "UnitFormConvertedLayout";
            this.UnitFormConvertedLayout.OptionsCustomizationForm.ShowLayoutTreeView = false;
            this.UnitFormConvertedLayout.Root = this.layoutControlGroup1;
            this.UnitFormConvertedLayout.Size = new System.Drawing.Size(743, 529);
            this.UnitFormConvertedLayout.TabIndex = 7;
            // 
            // comboBoxEditIdGPS
            // 
            this.comboBoxEditIdGPS.Location = new System.Drawing.Point(493, 58);
            this.comboBoxEditIdGPS.Name = "comboBoxEditIdGPS";
            this.comboBoxEditIdGPS.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditIdGPS.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEditIdGPS.Size = new System.Drawing.Size(242, 20);
            this.comboBoxEditIdGPS.StyleController = this.UnitFormConvertedLayout;
            this.comboBoxEditIdGPS.TabIndex = 19;
            this.comboBoxEditIdGPS.SelectedIndexChanged += new System.EventHandler(this.comboBoxEditIdGPS_SelectedIndexChanged);
            // 
            // comboBoxEditDeviceType
            // 
            this.comboBoxEditDeviceType.Location = new System.Drawing.Point(493, 27);
            this.comboBoxEditDeviceType.Name = "comboBoxEditDeviceType";
            this.comboBoxEditDeviceType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditDeviceType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEditDeviceType.Size = new System.Drawing.Size(242, 20);
            this.comboBoxEditDeviceType.StyleController = this.UnitFormConvertedLayout;
            this.comboBoxEditDeviceType.TabIndex = 18;
            this.comboBoxEditDeviceType.SelectedIndexChanged += new System.EventHandler(this.comboBoxEditDeviceType_SelectedIndexChanged);
            // 
            // textBoxSerial
            // 
            this.textBoxSerial.Location = new System.Drawing.Point(151, 89);
            this.textBoxSerial.Name = "textBoxSerial";
            this.textBoxSerial.Size = new System.Drawing.Size(183, 25);
            this.textBoxSerial.TabIndex = 17;
            this.textBoxSerial.TextChanged += new System.EventHandler(this.UnitParameters_Change);
            // 
            // textBoxIdUnit
            // 
            this.textBoxIdUnit.Location = new System.Drawing.Point(151, 58);
            this.textBoxIdUnit.Name = "textBoxIdUnit";
            this.textBoxIdUnit.Size = new System.Drawing.Size(183, 25);
            this.textBoxIdUnit.TabIndex = 16;
            this.textBoxIdUnit.TextChanged += new System.EventHandler(this.UnitParameters_Change);
            // 
            // listBoxNotAssigned
            // 
            this.listBoxNotAssigned.Enabled = false;
            this.listBoxNotAssigned.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBoxNotAssigned.FormattingEnabled = true;
            this.listBoxNotAssigned.HorizontalScrollbar = true;
            this.listBoxNotAssigned.IntegralHeight = false;
            this.listBoxNotAssigned.Location = new System.Drawing.Point(350, 319);
            this.listBoxNotAssigned.Name = "listBoxNotAssigned";
            this.listBoxNotAssigned.Size = new System.Drawing.Size(162, 165);
            this.listBoxNotAssigned.Sorted = true;
            this.listBoxNotAssigned.TabIndex = 1;
            this.listBoxNotAssigned.DoubleClick += new System.EventHandler(this.listBoxNotAssigned_DoubleClick);
            this.listBoxNotAssigned.Enter += new System.EventHandler(this.listBoxNotAssigned_Enter);
            // 
            // listBoxAssigned
            // 
            this.listBoxAssigned.Enabled = false;
            this.listBoxAssigned.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBoxAssigned.FormattingEnabled = true;
            this.listBoxAssigned.HorizontalScrollbar = true;
            this.listBoxAssigned.IntegralHeight = false;
            this.listBoxAssigned.Location = new System.Drawing.Point(568, 319);
            this.listBoxAssigned.Name = "listBoxAssigned";
            this.listBoxAssigned.Size = new System.Drawing.Size(167, 165);
            this.listBoxAssigned.Sorted = true;
            this.listBoxAssigned.TabIndex = 7;
            this.listBoxAssigned.DoubleClick += new System.EventHandler(this.listBoxAssigned_DoubleClick);
            this.listBoxAssigned.Enter += new System.EventHandler(this.listBoxAssigned_Enter);
            // 
            // buttonAddIncidentType
            // 
            this.buttonAddIncidentType.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonAddIncidentType.Appearance.Options.UseForeColor = true;
            this.buttonAddIncidentType.Enabled = false;
            this.buttonAddIncidentType.Location = new System.Drawing.Point(518, 316);
            this.buttonAddIncidentType.Name = "buttonAddIncidentType";
            this.buttonAddIncidentType.Size = new System.Drawing.Size(44, 24);
            this.buttonAddIncidentType.StyleController = this.UnitFormConvertedLayout;
            this.buttonAddIncidentType.TabIndex = 2;
            this.buttonAddIncidentType.Text = ">";
            this.toolTipUnits.SetToolTip(this.buttonAddIncidentType, ResourceLoader.GetString2("AssociateIncidentType"));
            this.buttonAddIncidentType.Click += new System.EventHandler(this.buttonAddIncidentType_Click);
            // 
            // buttonAddAllIncidentType
            // 
            this.buttonAddAllIncidentType.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonAddAllIncidentType.Appearance.Options.UseForeColor = true;
            this.buttonAddAllIncidentType.Enabled = false;
            this.buttonAddAllIncidentType.Location = new System.Drawing.Point(518, 350);
            this.buttonAddAllIncidentType.Name = "buttonAddAllIncidentType";
            this.buttonAddAllIncidentType.Size = new System.Drawing.Size(44, 24);
            this.buttonAddAllIncidentType.StyleController = this.UnitFormConvertedLayout;
            this.buttonAddAllIncidentType.TabIndex = 3;
            this.buttonAddAllIncidentType.Text = ">>";
            this.toolTipUnits.SetToolTip(this.buttonAddAllIncidentType, ResourceLoader.GetString2("AssociateAllIncidentTypes"));
            this.buttonAddAllIncidentType.Click += new System.EventHandler(this.buttonAddAllIncidentType_Click);
            // 
            // buttonRemoveIncidentType
            // 
            this.buttonRemoveIncidentType.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonRemoveIncidentType.Appearance.Options.UseForeColor = true;
            this.buttonRemoveIncidentType.Enabled = false;
            this.buttonRemoveIncidentType.Location = new System.Drawing.Point(518, 384);
            this.buttonRemoveIncidentType.Name = "buttonRemoveIncidentType";
            this.buttonRemoveIncidentType.Size = new System.Drawing.Size(44, 24);
            this.buttonRemoveIncidentType.StyleController = this.UnitFormConvertedLayout;
            this.buttonRemoveIncidentType.TabIndex = 4;
            this.buttonRemoveIncidentType.Text = "<";
            this.toolTipUnits.SetToolTip(this.buttonRemoveIncidentType, ResourceLoader.GetString2("RemoveIncidentType"));
            this.buttonRemoveIncidentType.Click += new System.EventHandler(this.buttonRemoveIncidentType_Click);
            // 
            // buttonRemoveAllIncidentType
            // 
            this.buttonRemoveAllIncidentType.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonRemoveAllIncidentType.Appearance.Options.UseForeColor = true;
            this.buttonRemoveAllIncidentType.Enabled = false;
            this.buttonRemoveAllIncidentType.Location = new System.Drawing.Point(518, 418);
            this.buttonRemoveAllIncidentType.Name = "buttonRemoveAllIncidentType";
            this.buttonRemoveAllIncidentType.Size = new System.Drawing.Size(44, 24);
            this.buttonRemoveAllIncidentType.StyleController = this.UnitFormConvertedLayout;
            this.buttonRemoveAllIncidentType.TabIndex = 5;
            this.buttonRemoveAllIncidentType.Text = "<<";
            this.toolTipUnits.SetToolTip(this.buttonRemoveAllIncidentType, ResourceLoader.GetString2("RemoveAllIncidentTypes"));
            this.buttonRemoveAllIncidentType.Click += new System.EventHandler(this.buttonRemoveAllIncidentType_Click);
            // 
            // textBoxVelocity
            // 
            this.textBoxVelocity.AllowsLetters = false;
            this.textBoxVelocity.AllowsNumbers = true;
            this.textBoxVelocity.AllowsPunctuation = false;
            this.textBoxVelocity.AllowsSeparators = false;
            this.textBoxVelocity.AllowsSymbols = false;
            this.textBoxVelocity.AllowsWhiteSpaces = false;
            this.textBoxVelocity.ExtraAllowedChars = "";
            this.textBoxVelocity.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxVelocity.Location = new System.Drawing.Point(151, 244);
            this.textBoxVelocity.MaxLength = 3;
            this.textBoxVelocity.Name = "textBoxVelocity";
            this.textBoxVelocity.NonAllowedCharacters = "";
            this.textBoxVelocity.RegularExpresion = "";
            this.textBoxVelocity.Size = new System.Drawing.Size(183, 24);
            this.textBoxVelocity.TabIndex = 3;
            this.textBoxVelocity.TextChanged += new System.EventHandler(this.UnitParameters_Change);
            // 
            // textBoxDescription
            // 
            this.textBoxDescription.AcceptsReturn = true;
            this.textBoxDescription.AllowsLetters = true;
            this.textBoxDescription.AllowsNumbers = true;
            this.textBoxDescription.AllowsPunctuation = true;
            this.textBoxDescription.AllowsSeparators = true;
            this.textBoxDescription.AllowsSymbols = true;
            this.textBoxDescription.AllowsWhiteSpaces = true;
            this.textBoxDescription.ExtraAllowedChars = "";
            this.textBoxDescription.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxDescription.Location = new System.Drawing.Point(493, 120);
            this.textBoxDescription.MaxLength = 200;
            this.textBoxDescription.Multiline = true;
            this.textBoxDescription.Name = "textBoxDescription";
            this.textBoxDescription.NonAllowedCharacters = "";
            this.textBoxDescription.RegularExpresion = "";
            this.textBoxDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxDescription.Size = new System.Drawing.Size(242, 104);
            this.textBoxDescription.TabIndex = 5;
            this.textBoxDescription.TextChanged += new System.EventHandler(this.UnitParameters_Change);
            // 
            // textBoxSeats
            // 
            this.textBoxSeats.AllowsLetters = false;
            this.textBoxSeats.AllowsNumbers = true;
            this.textBoxSeats.AllowsPunctuation = false;
            this.textBoxSeats.AllowsSeparators = false;
            this.textBoxSeats.AllowsSymbols = false;
            this.textBoxSeats.AllowsWhiteSpaces = false;
            this.textBoxSeats.ExtraAllowedChars = "";
            this.textBoxSeats.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxSeats.Location = new System.Drawing.Point(151, 213);
            this.textBoxSeats.MaxLength = 2;
            this.textBoxSeats.Name = "textBoxSeats";
            this.textBoxSeats.NonAllowedCharacters = "";
            this.textBoxSeats.RegularExpresion = "";
            this.textBoxSeats.Size = new System.Drawing.Size(183, 25);
            this.textBoxSeats.TabIndex = 1;
            this.textBoxSeats.TextChanged += new System.EventHandler(this.UnitParameters_Change);
            // 
            // textBoxBrand
            // 
            this.textBoxBrand.AllowsLetters = true;
            this.textBoxBrand.AllowsNumbers = true;
            this.textBoxBrand.AllowsPunctuation = true;
            this.textBoxBrand.AllowsSeparators = true;
            this.textBoxBrand.AllowsSymbols = true;
            this.textBoxBrand.AllowsWhiteSpaces = true;
            this.textBoxBrand.ExtraAllowedChars = "";
            this.textBoxBrand.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxBrand.Location = new System.Drawing.Point(151, 120);
            this.textBoxBrand.MaxLength = 20;
            this.textBoxBrand.Name = "textBoxBrand";
            this.textBoxBrand.NonAllowedCharacters = "";
            this.textBoxBrand.RegularExpresion = "";
            this.textBoxBrand.Size = new System.Drawing.Size(183, 25);
            this.textBoxBrand.TabIndex = 3;
            this.textBoxBrand.TextChanged += new System.EventHandler(this.UnitParameters_Change);
            // 
            // textBoxLicensePlate
            // 
            this.textBoxLicensePlate.AllowsLetters = true;
            this.textBoxLicensePlate.AllowsNumbers = true;
            this.textBoxLicensePlate.AllowsPunctuation = false;
            this.textBoxLicensePlate.AllowsSeparators = false;
            this.textBoxLicensePlate.AllowsSymbols = false;
            this.textBoxLicensePlate.AllowsWhiteSpaces = false;
            this.textBoxLicensePlate.ExtraAllowedChars = "";
            this.textBoxLicensePlate.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxLicensePlate.Location = new System.Drawing.Point(151, 27);
            this.textBoxLicensePlate.MaxLength = 7;
            this.textBoxLicensePlate.Name = "textBoxLicensePlate";
            this.textBoxLicensePlate.NonAllowedCharacters = "";
            this.textBoxLicensePlate.RegularExpresion = "";
            this.textBoxLicensePlate.Size = new System.Drawing.Size(183, 25);
            this.textBoxLicensePlate.TabIndex = 1;
            this.textBoxLicensePlate.TextChanged += new System.EventHandler(this.UnitParameters_Change);
            // 
            // textBoxYear
            // 
            this.textBoxYear.AllowsLetters = false;
            this.textBoxYear.AllowsNumbers = true;
            this.textBoxYear.AllowsPunctuation = false;
            this.textBoxYear.AllowsSeparators = false;
            this.textBoxYear.AllowsSymbols = false;
            this.textBoxYear.AllowsWhiteSpaces = false;
            this.textBoxYear.ExtraAllowedChars = "";
            this.textBoxYear.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxYear.Location = new System.Drawing.Point(151, 182);
            this.textBoxYear.MaxLength = 4;
            this.textBoxYear.Name = "textBoxYear";
            this.textBoxYear.NonAllowedCharacters = "";
            this.textBoxYear.RegularExpresion = "";
            this.textBoxYear.Size = new System.Drawing.Size(183, 25);
            this.textBoxYear.TabIndex = 7;
            this.textBoxYear.TextChanged += new System.EventHandler(this.UnitParameters_Change);
            // 
            // textBoxIDRadio
            // 
            this.textBoxIDRadio.AllowsLetters = true;
            this.textBoxIDRadio.AllowsNumbers = true;
            this.textBoxIDRadio.AllowsPunctuation = true;
            this.textBoxIDRadio.AllowsSeparators = true;
            this.textBoxIDRadio.AllowsSymbols = true;
            this.textBoxIDRadio.AllowsWhiteSpaces = true;
            this.textBoxIDRadio.ExtraAllowedChars = "";
            this.textBoxIDRadio.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxIDRadio.Location = new System.Drawing.Point(493, 89);
            this.textBoxIDRadio.MaxLength = 30;
            this.textBoxIDRadio.Name = "textBoxIDRadio";
            this.textBoxIDRadio.NonAllowedCharacters = "";
            this.textBoxIDRadio.RegularExpresion = "";
            this.textBoxIDRadio.Size = new System.Drawing.Size(242, 25);
            this.textBoxIDRadio.TabIndex = 11;
            this.textBoxIDRadio.TextChanged += new System.EventHandler(this.UnitParameters_Change);
            // 
            // textBoxModel
            // 
            this.textBoxModel.AllowsLetters = true;
            this.textBoxModel.AllowsNumbers = true;
            this.textBoxModel.AllowsPunctuation = true;
            this.textBoxModel.AllowsSeparators = true;
            this.textBoxModel.AllowsSymbols = true;
            this.textBoxModel.AllowsWhiteSpaces = true;
            this.textBoxModel.ExtraAllowedChars = "";
            this.textBoxModel.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxModel.Location = new System.Drawing.Point(151, 151);
            this.textBoxModel.MaxLength = 20;
            this.textBoxModel.Name = "textBoxModel";
            this.textBoxModel.NonAllowedCharacters = "";
            this.textBoxModel.RegularExpresion = "";
            this.textBoxModel.Size = new System.Drawing.Size(183, 25);
            this.textBoxModel.TabIndex = 5;
            this.textBoxModel.TextChanged += new System.EventHandler(this.UnitParameters_Change);
            // 
            // comboBoxStationType
            // 
            this.comboBoxStationType.Location = new System.Drawing.Point(151, 355);
            this.comboBoxStationType.Name = "comboBoxStationType";
            this.comboBoxStationType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxStationType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxStationType.Size = new System.Drawing.Size(183, 20);
            this.comboBoxStationType.StyleController = this.UnitFormConvertedLayout;
            this.comboBoxStationType.TabIndex = 5;
            this.comboBoxStationType.SelectedIndexChanged += new System.EventHandler(this.UnitParameters_Change);
            this.comboBoxStationType.Enter += new System.EventHandler(this.comboBoxStationType_Enter);
            // 
            // comboBoxZoneType
            // 
            this.comboBoxZoneType.Location = new System.Drawing.Point(151, 329);
            this.comboBoxZoneType.Name = "comboBoxZoneType";
            this.comboBoxZoneType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxZoneType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxZoneType.Size = new System.Drawing.Size(183, 20);
            this.comboBoxZoneType.StyleController = this.UnitFormConvertedLayout;
            this.comboBoxZoneType.TabIndex = 3;
            this.comboBoxZoneType.SelectedIndexChanged += new System.EventHandler(this.comboBoxZoneType_SelectedIndexChanged);
            this.comboBoxZoneType.Enter += new System.EventHandler(this.comboBoxZoneType_Enter);
            // 
            // comboBoxUnitType
            // 
            this.comboBoxUnitType.Location = new System.Drawing.Point(151, 381);
            this.comboBoxUnitType.Name = "comboBoxUnitType";
            this.comboBoxUnitType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxUnitType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxUnitType.Properties.SelectedValueChanged += new System.EventHandler(this.comboBoxUnitType_SelectionChangeCommitted);
            this.comboBoxUnitType.Size = new System.Drawing.Size(183, 20);
            this.comboBoxUnitType.StyleController = this.UnitFormConvertedLayout;
            this.comboBoxUnitType.TabIndex = 7;
            this.comboBoxUnitType.SelectedIndexChanged += new System.EventHandler(this.unitType_selectedIndexChanged);
            this.comboBoxUnitType.Enter += new System.EventHandler(this.comboBoxUnitType_Enter);
            // 
            // comboBoxDepartmentType
            // 
            this.comboBoxDepartmentType.Location = new System.Drawing.Point(151, 303);
            this.comboBoxDepartmentType.Name = "comboBoxDepartmentType";
            this.comboBoxDepartmentType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxDepartmentType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxDepartmentType.Properties.SelectedValueChanged += new System.EventHandler(this.comboBoxDepartmentType_SelectionChangeCommitted);
            this.comboBoxDepartmentType.Size = new System.Drawing.Size(183, 20);
            this.comboBoxDepartmentType.StyleController = this.UnitFormConvertedLayout;
            this.comboBoxDepartmentType.TabIndex = 1;
            this.comboBoxDepartmentType.SelectedIndexChanged += new System.EventHandler(this.comboBoxDepartmentType_SelectedIndexChanged);
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOk.Appearance.Options.UseFont = true;
            this.buttonOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOk.Enabled = false;
            this.buttonOk.Location = new System.Drawing.Point(573, 494);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(82, 33);
            this.buttonOk.StyleController = this.UnitFormConvertedLayout;
            this.buttonOk.TabIndex = 4;
            this.buttonOk.Text = "Accept";
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupUnitInf,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.emptySpaceItem1,
            this.layoutControlGroupDepartment,
            this.layoutControlGroupIncidentType,
            this.layoutControlGroupUnitFeatures});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(743, 529);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroupUnitInf
            // 
            this.layoutControlGroupUnitInf.AppearanceGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupUnitInf.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroupUnitInf.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupUnitInf.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroupUnitInf.CustomizationFormText = "layoutControlGroupUnitInf";
            this.layoutControlGroupUnitInf.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemBrand,
            this.layoutControlItemPlate,
            this.layoutControlItemYear,
            this.layoutControlItemModel,
            this.layoutControlItemIdUnit,
            this.layoutControlItemSerial,
            this.layoutControlItemSeats,
            this.layoutControlItemVelocity});
            this.layoutControlGroupUnitInf.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupUnitInf.Name = "layoutControlGroupUnitInf";
            this.layoutControlGroupUnitInf.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupUnitInf.Size = new System.Drawing.Size(342, 276);
            this.layoutControlGroupUnitInf.Text = "layoutControlGroupUnitInf";
            // 
            // layoutControlItemBrand
            // 
            this.layoutControlItemBrand.Control = this.textBoxBrand;
            this.layoutControlItemBrand.CustomizationFormText = "layoutControlItemBrand";
            this.layoutControlItemBrand.Location = new System.Drawing.Point(0, 93);
            this.layoutControlItemBrand.MinSize = new System.Drawing.Size(171, 27);
            this.layoutControlItemBrand.Name = "layoutControlItemBrand";
            this.layoutControlItemBrand.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItemBrand.Size = new System.Drawing.Size(332, 31);
            this.layoutControlItemBrand.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemBrand.Text = "layoutControlItemBrand";
            this.layoutControlItemBrand.TextSize = new System.Drawing.Size(139, 13);
            // 
            // layoutControlItemPlate
            // 
            this.layoutControlItemPlate.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItemPlate.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItemPlate.Control = this.textBoxLicensePlate;
            this.layoutControlItemPlate.CustomizationFormText = "layoutControlItemPlate";
            this.layoutControlItemPlate.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemPlate.MinSize = new System.Drawing.Size(171, 27);
            this.layoutControlItemPlate.Name = "layoutControlItemPlate";
            this.layoutControlItemPlate.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItemPlate.Size = new System.Drawing.Size(332, 31);
            this.layoutControlItemPlate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemPlate.Text = "layoutControlItemPlate";
            this.layoutControlItemPlate.TextSize = new System.Drawing.Size(139, 13);
            // 
            // layoutControlItemYear
            // 
            this.layoutControlItemYear.Control = this.textBoxYear;
            this.layoutControlItemYear.CustomizationFormText = "layoutControlItemYear";
            this.layoutControlItemYear.Location = new System.Drawing.Point(0, 155);
            this.layoutControlItemYear.MinSize = new System.Drawing.Size(171, 27);
            this.layoutControlItemYear.Name = "layoutControlItemYear";
            this.layoutControlItemYear.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItemYear.Size = new System.Drawing.Size(332, 31);
            this.layoutControlItemYear.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemYear.Text = "layoutControlItemYear";
            this.layoutControlItemYear.TextSize = new System.Drawing.Size(139, 13);
            // 
            // layoutControlItemModel
            // 
            this.layoutControlItemModel.Control = this.textBoxModel;
            this.layoutControlItemModel.CustomizationFormText = "layoutControlItemModel";
            this.layoutControlItemModel.Location = new System.Drawing.Point(0, 124);
            this.layoutControlItemModel.MinSize = new System.Drawing.Size(171, 27);
            this.layoutControlItemModel.Name = "layoutControlItemModel";
            this.layoutControlItemModel.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItemModel.Size = new System.Drawing.Size(332, 31);
            this.layoutControlItemModel.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemModel.Text = "layoutControlItemModel";
            this.layoutControlItemModel.TextSize = new System.Drawing.Size(139, 13);
            // 
            // layoutControlItemIdUnit
            // 
            this.layoutControlItemIdUnit.Control = this.textBoxIdUnit;
            this.layoutControlItemIdUnit.CustomizationFormText = "layoutControlItemIdUnit";
            this.layoutControlItemIdUnit.Location = new System.Drawing.Point(0, 31);
            this.layoutControlItemIdUnit.MinSize = new System.Drawing.Size(171, 27);
            this.layoutControlItemIdUnit.Name = "layoutControlItemIdUnit";
            this.layoutControlItemIdUnit.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItemIdUnit.Size = new System.Drawing.Size(332, 31);
            this.layoutControlItemIdUnit.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemIdUnit.Text = "layoutControlItemIdUnit";
            this.layoutControlItemIdUnit.TextSize = new System.Drawing.Size(139, 13);
            // 
            // layoutControlItemSerial
            // 
            this.layoutControlItemSerial.Control = this.textBoxSerial;
            this.layoutControlItemSerial.CustomizationFormText = "layoutControlItemSerial";
            this.layoutControlItemSerial.Location = new System.Drawing.Point(0, 62);
            this.layoutControlItemSerial.MinSize = new System.Drawing.Size(171, 27);
            this.layoutControlItemSerial.Name = "layoutControlItemSerial";
            this.layoutControlItemSerial.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItemSerial.Size = new System.Drawing.Size(332, 31);
            this.layoutControlItemSerial.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemSerial.Text = "layoutControlItemSerial";
            this.layoutControlItemSerial.TextSize = new System.Drawing.Size(139, 13);
            // 
            // layoutControlItemSeats
            // 
            this.layoutControlItemSeats.Control = this.textBoxSeats;
            this.layoutControlItemSeats.CustomizationFormText = "textBoxSeatsitem";
            this.layoutControlItemSeats.Location = new System.Drawing.Point(0, 186);
            this.layoutControlItemSeats.MinSize = new System.Drawing.Size(171, 27);
            this.layoutControlItemSeats.Name = "layoutControlItemSeats";
            this.layoutControlItemSeats.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItemSeats.Size = new System.Drawing.Size(332, 31);
            this.layoutControlItemSeats.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemSeats.Text = "layoutControlItemSeats";
            this.layoutControlItemSeats.TextSize = new System.Drawing.Size(139, 13);
            // 
            // layoutControlItemVelocity
            // 
            this.layoutControlItemVelocity.Control = this.textBoxVelocity;
            this.layoutControlItemVelocity.CustomizationFormText = "layoutControlItemVelocity";
            this.layoutControlItemVelocity.Location = new System.Drawing.Point(0, 217);
            this.layoutControlItemVelocity.MinSize = new System.Drawing.Size(171, 27);
            this.layoutControlItemVelocity.Name = "layoutControlItemVelocity";
            this.layoutControlItemVelocity.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItemVelocity.Size = new System.Drawing.Size(332, 30);
            this.layoutControlItemVelocity.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemVelocity.Text = "layoutControlItemVelocity";
            this.layoutControlItemVelocity.TextSize = new System.Drawing.Size(139, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.buttonCancel;
            this.layoutControlItem1.CustomizationFormText = "buttonCancelitem";
            this.layoutControlItem1.Location = new System.Drawing.Point(657, 492);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(86, 37);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(86, 37);
            this.layoutControlItem1.Name = "buttonCancelitem";
            this.layoutControlItem1.Size = new System.Drawing.Size(86, 37);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "buttonCancelitem";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.buttonOk;
            this.layoutControlItem2.CustomizationFormText = "buttonOkitem";
            this.layoutControlItem2.Location = new System.Drawing.Point(571, 492);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(86, 37);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(86, 37);
            this.layoutControlItem2.Name = "buttonOkitem";
            this.layoutControlItem2.Size = new System.Drawing.Size(86, 37);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "buttonOkitem";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 492);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(110, 30);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(571, 37);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupDepartment
            // 
            this.layoutControlGroupDepartment.AppearanceGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupDepartment.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroupDepartment.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupDepartment.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroupDepartment.CustomizationFormText = "layoutControlGroupDepartment";
            this.layoutControlGroupDepartment.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemStation,
            this.layoutControlItemZone,
            this.layoutControlItemUnitType,
            this.layoutControlItemDepartment,
            this.emptySpaceItem5});
            this.layoutControlGroupDepartment.Location = new System.Drawing.Point(0, 276);
            this.layoutControlGroupDepartment.Name = "layoutControlGroupDepartment";
            this.layoutControlGroupDepartment.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupDepartment.Size = new System.Drawing.Size(342, 216);
            this.layoutControlGroupDepartment.Text = "layoutControlGroupDepartment";
            // 
            // layoutControlItemStation
            // 
            this.layoutControlItemStation.Control = this.comboBoxStationType;
            this.layoutControlItemStation.CustomizationFormText = "layoutControlItemStation";
            this.layoutControlItemStation.Location = new System.Drawing.Point(0, 52);
            this.layoutControlItemStation.Name = "layoutControlItemStation";
            this.layoutControlItemStation.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItemStation.Size = new System.Drawing.Size(332, 26);
            this.layoutControlItemStation.Text = "layoutControlItemStation";
            this.layoutControlItemStation.TextSize = new System.Drawing.Size(139, 13);
            // 
            // layoutControlItemZone
            // 
            this.layoutControlItemZone.Control = this.comboBoxZoneType;
            this.layoutControlItemZone.CustomizationFormText = "layoutControlItemZone";
            this.layoutControlItemZone.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItemZone.Name = "layoutControlItemZone";
            this.layoutControlItemZone.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItemZone.Size = new System.Drawing.Size(332, 26);
            this.layoutControlItemZone.Text = "layoutControlItemZone";
            this.layoutControlItemZone.TextSize = new System.Drawing.Size(139, 13);
            // 
            // layoutControlItemUnitType
            // 
            this.layoutControlItemUnitType.Control = this.comboBoxUnitType;
            this.layoutControlItemUnitType.CustomizationFormText = "layoutControlItemUnitType";
            this.layoutControlItemUnitType.Location = new System.Drawing.Point(0, 78);
            this.layoutControlItemUnitType.Name = "layoutControlItemUnitType";
            this.layoutControlItemUnitType.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItemUnitType.Size = new System.Drawing.Size(332, 26);
            this.layoutControlItemUnitType.Text = "layoutControlItemUnitType";
            this.layoutControlItemUnitType.TextSize = new System.Drawing.Size(139, 13);
            // 
            // layoutControlItemDepartment
            // 
            this.layoutControlItemDepartment.Control = this.comboBoxDepartmentType;
            this.layoutControlItemDepartment.CustomizationFormText = "layoutControlItemDepartment";
            this.layoutControlItemDepartment.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemDepartment.Name = "layoutControlItemDepartment";
            this.layoutControlItemDepartment.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItemDepartment.Size = new System.Drawing.Size(332, 26);
            this.layoutControlItemDepartment.Text = "layoutControlItemDepartment";
            this.layoutControlItemDepartment.TextSize = new System.Drawing.Size(139, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 104);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(110, 30);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(332, 83);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.Text = "emptySpaceItem5";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupIncidentType
            // 
            this.layoutControlGroupIncidentType.AppearanceGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupIncidentType.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroupIncidentType.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupIncidentType.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroupIncidentType.CustomizationFormText = "layoutControlGroupIncidentType";
            this.layoutControlGroupIncidentType.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemNotAssigned,
            this.layoutControlItemAssigned,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.emptySpaceItem2,
            this.emptySpaceItem3});
            this.layoutControlGroupIncidentType.Location = new System.Drawing.Point(342, 276);
            this.layoutControlGroupIncidentType.Name = "layoutControlGroupIncidentType";
            this.layoutControlGroupIncidentType.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupIncidentType.Size = new System.Drawing.Size(401, 216);
            this.layoutControlGroupIncidentType.Text = "layoutControlGroupIncidentType";
            // 
            // layoutControlItemNotAssigned
            // 
            this.layoutControlItemNotAssigned.Control = this.listBoxNotAssigned;
            this.layoutControlItemNotAssigned.CustomizationFormText = "layoutControlItemNotAssigned";
            this.layoutControlItemNotAssigned.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemNotAssigned.MinSize = new System.Drawing.Size(148, 52);
            this.layoutControlItemNotAssigned.Name = "layoutControlItemNotAssigned";
            this.layoutControlItemNotAssigned.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItemNotAssigned.Size = new System.Drawing.Size(168, 187);
            this.layoutControlItemNotAssigned.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemNotAssigned.Text = "layoutControlItemNotAssigned";
            this.layoutControlItemNotAssigned.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItemNotAssigned.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItemNotAssigned.TextSize = new System.Drawing.Size(141, 13);
            this.layoutControlItemNotAssigned.TextToControlDistance = 3;
            // 
            // layoutControlItemAssigned
            // 
            this.layoutControlItemAssigned.Control = this.listBoxAssigned;
            this.layoutControlItemAssigned.CustomizationFormText = "layoutControlItemAssigned";
            this.layoutControlItemAssigned.Location = new System.Drawing.Point(218, 0);
            this.layoutControlItemAssigned.MinSize = new System.Drawing.Size(148, 52);
            this.layoutControlItemAssigned.Name = "layoutControlItemAssigned";
            this.layoutControlItemAssigned.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItemAssigned.Size = new System.Drawing.Size(173, 187);
            this.layoutControlItemAssigned.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemAssigned.Text = "layoutControlItemAssigned";
            this.layoutControlItemAssigned.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItemAssigned.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItemAssigned.TextSize = new System.Drawing.Size(124, 13);
            this.layoutControlItemAssigned.TextToControlDistance = 3;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.buttonAddIncidentType;
            this.layoutControlItem6.CustomizationFormText = "buttonAddIncidentTypeitem";
            this.layoutControlItem6.Location = new System.Drawing.Point(168, 11);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(50, 34);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(50, 34);
            this.layoutControlItem6.Name = "buttonAddIncidentTypeitem";
            this.layoutControlItem6.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 5, 5);
            this.layoutControlItem6.Size = new System.Drawing.Size(50, 34);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "buttonAddIncidentTypeitem";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.buttonAddAllIncidentType;
            this.layoutControlItem7.CustomizationFormText = "buttonAddAllIncidentTypeitem";
            this.layoutControlItem7.Location = new System.Drawing.Point(168, 45);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(50, 34);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(50, 34);
            this.layoutControlItem7.Name = "buttonAddAllIncidentTypeitem";
            this.layoutControlItem7.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 5, 5);
            this.layoutControlItem7.Size = new System.Drawing.Size(50, 34);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "buttonAddAllIncidentTypeitem";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.buttonRemoveIncidentType;
            this.layoutControlItem8.CustomizationFormText = "buttonRemoveIncidentTypeitem";
            this.layoutControlItem8.Location = new System.Drawing.Point(168, 79);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(50, 34);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(50, 34);
            this.layoutControlItem8.Name = "buttonRemoveIncidentTypeitem";
            this.layoutControlItem8.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 5, 5);
            this.layoutControlItem8.Size = new System.Drawing.Size(50, 34);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "buttonRemoveIncidentTypeitem";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.buttonRemoveAllIncidentType;
            this.layoutControlItem9.CustomizationFormText = "buttonRemoveAllIncidentTypeitem";
            this.layoutControlItem9.Location = new System.Drawing.Point(168, 113);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(50, 34);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(50, 34);
            this.layoutControlItem9.Name = "buttonRemoveAllIncidentTypeitem";
            this.layoutControlItem9.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 5, 5);
            this.layoutControlItem9.Size = new System.Drawing.Size(50, 34);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "buttonRemoveAllIncidentTypeitem";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(168, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(50, 11);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(168, 147);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(50, 40);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupUnitFeatures
            // 
            this.layoutControlGroupUnitFeatures.AppearanceGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupUnitFeatures.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroupUnitFeatures.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupUnitFeatures.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroupUnitFeatures.CustomizationFormText = "layoutControlGroupUnitFeatures";
            this.layoutControlGroupUnitFeatures.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemDes,
            this.emptySpaceItem4,
            this.layoutControlItemIdRadio,
            this.layoutControlItemDeviceType,
            this.layoutControlItemIdGPS});
            this.layoutControlGroupUnitFeatures.Location = new System.Drawing.Point(342, 0);
            this.layoutControlGroupUnitFeatures.Name = "layoutControlGroupUnitFeatures";
            this.layoutControlGroupUnitFeatures.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupUnitFeatures.Size = new System.Drawing.Size(401, 276);
            this.layoutControlGroupUnitFeatures.Text = "layoutControlGroupUnitFeatures";
            // 
            // layoutControlItemDes
            // 
            this.layoutControlItemDes.Control = this.textBoxDescription;
            this.layoutControlItemDes.CustomizationFormText = "layoutControlItemDes";
            this.layoutControlItemDes.Location = new System.Drawing.Point(0, 93);
            this.layoutControlItemDes.MinSize = new System.Drawing.Size(1, 1);
            this.layoutControlItemDes.Name = "layoutControlItemDes";
            this.layoutControlItemDes.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItemDes.Size = new System.Drawing.Size(391, 110);
            this.layoutControlItemDes.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemDes.Text = "layoutControlItemDes";
            this.layoutControlItemDes.TextSize = new System.Drawing.Size(139, 13);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 203);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(391, 44);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItemIdRadio
            // 
            this.layoutControlItemIdRadio.Control = this.textBoxIDRadio;
            this.layoutControlItemIdRadio.CustomizationFormText = "layoutControlItemIdDevice";
            this.layoutControlItemIdRadio.Location = new System.Drawing.Point(0, 62);
            this.layoutControlItemIdRadio.MinSize = new System.Drawing.Size(1, 1);
            this.layoutControlItemIdRadio.Name = "layoutControlItemIdRadio";
            this.layoutControlItemIdRadio.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItemIdRadio.Size = new System.Drawing.Size(391, 31);
            this.layoutControlItemIdRadio.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemIdRadio.Text = "layoutControlItemIdRadio";
            this.layoutControlItemIdRadio.TextSize = new System.Drawing.Size(139, 13);
            // 
            // layoutControlItemDeviceType
            // 
            this.layoutControlItemDeviceType.Control = this.comboBoxEditDeviceType;
            this.layoutControlItemDeviceType.CustomizationFormText = "layoutControlItemDeviceType";
            this.layoutControlItemDeviceType.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemDeviceType.MinSize = new System.Drawing.Size(1, 1);
            this.layoutControlItemDeviceType.Name = "layoutControlItemDeviceType";
            this.layoutControlItemDeviceType.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItemDeviceType.Size = new System.Drawing.Size(391, 31);
            this.layoutControlItemDeviceType.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemDeviceType.Text = "layoutControlItemDeviceType";
            this.layoutControlItemDeviceType.TextSize = new System.Drawing.Size(139, 13);
            // 
            // layoutControlItemIdGPS
            // 
            this.layoutControlItemIdGPS.Control = this.comboBoxEditIdGPS;
            this.layoutControlItemIdGPS.CustomizationFormText = "layoutControlItemIdGPS";
            this.layoutControlItemIdGPS.Location = new System.Drawing.Point(0, 31);
            this.layoutControlItemIdGPS.MinSize = new System.Drawing.Size(1, 1);
            this.layoutControlItemIdGPS.Name = "layoutControlItemIdGPS";
            this.layoutControlItemIdGPS.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItemIdGPS.Size = new System.Drawing.Size(391, 31);
            this.layoutControlItemIdGPS.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemIdGPS.Text = "layoutControlItemIdGPS";
            this.layoutControlItemIdGPS.TextSize = new System.Drawing.Size(139, 13);
            // 
            // toolTipUnits
            // 
            this.toolTipUnits.AutoPopDelay = 5000;
            this.toolTipUnits.InitialDelay = 500;
            this.toolTipUnits.ReshowDelay = 100;
            // 
            // UnitForm
            // 
            this.AcceptButton = this.buttonOk;
            this.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(743, 529);
            this.Controls.Add(this.UnitFormConvertedLayout);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(0, 519);
            this.Name = "UnitForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "UnitForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.UnitForm_FormClosing);
            this.Load += new System.EventHandler(this.UnitForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.UnitFormConvertedLayout)).EndInit();
            this.UnitFormConvertedLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditIdGPS.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditDeviceType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxStationType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxZoneType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxUnitType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxDepartmentType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupUnitInf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBrand)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPlate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemModel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIdUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSerial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSeats)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemVelocity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupDepartment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemZone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemUnitType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupIncidentType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemNotAssigned)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAssigned)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupUnitFeatures)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIdRadio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDeviceType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIdGPS)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

		private DevExpress.XtraEditors.SimpleButton buttonCancel;
        private DevExpress.XtraEditors.SimpleButton buttonOk;
        private TextBoxEx textBoxSeats;
        private TextBoxEx textBoxYear;
        private TextBoxEx textBoxVelocity;
        private TextBoxEx textBoxLicensePlate;
        private System.Windows.Forms.ToolTip toolTipUnits;
        private TextBoxEx textBoxBrand;
        private System.Windows.Forms.ListBox listBoxNotAssigned;
        private System.Windows.Forms.ListBox listBoxAssigned;
		private DevExpress.XtraEditors.SimpleButton buttonRemoveAllIncidentType;
		private DevExpress.XtraEditors.SimpleButton buttonAddIncidentType;
		private DevExpress.XtraEditors.SimpleButton buttonRemoveIncidentType;
		private DevExpress.XtraEditors.SimpleButton buttonAddAllIncidentType;
        private TextBoxEx textBoxModel;
        private TextBoxEx textBoxIDRadio;
        private TextBoxEx textBoxDescription;
		private DevExpress.XtraEditors.ComboBoxEdit comboBoxStationType;
		private DevExpress.XtraEditors.ComboBoxEdit comboBoxZoneType;
		private DevExpress.XtraEditors.ComboBoxEdit comboBoxUnitType;
		private DevExpress.XtraEditors.ComboBoxEdit comboBoxDepartmentType;
        private DevExpress.XtraLayout.LayoutControl UnitFormConvertedLayout;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupIncidentType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemNotAssigned;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemAssigned;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupUnitFeatures;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemVelocity;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDes;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemSeats;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupUnitInf;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemBrand;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemPlate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemYear;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemIdRadio;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemModel;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupDepartment;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemStation;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemZone;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemUnitType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDepartment;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.Converter.LayoutConverter layoutConverter1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private System.Windows.Forms.TextBox textBoxSerial;
        private System.Windows.Forms.TextBox textBoxIdUnit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemIdUnit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemSerial;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditIdGPS;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditDeviceType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDeviceType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemIdGPS;
    }
}