using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Smartmatic.SmartCad.Service;
using System.IO;
using System.Reflection;
using System.Collections;
using System.ServiceModel;
using Iesi.Collections;
using SmartCadCore.ClientData;
using SmartCadCore.Enums;
using SmartCadCore.Core;
using SmartCadCore.Common;


namespace SmartCadGuiCommon
{
    public partial class IncidentTypeForm : DevExpress.XtraEditors.XtraForm
    {
        IncidentTypeClientData selectedIncidentType = null;
        FormBehavior behaviorType;
        private AdministrationRibbonForm parentForm;
        private object syncCommitted = new object();
    
        BitmapViewerForm bitmapviewer;
        string fileName;
        

        private IncidentTypeForm()
        {
            InitializeComponent();
            FillDepartmentTypes();
            PutIconByDefect();
        }

        public IncidentTypeForm(AdministrationRibbonForm form,
            IncidentTypeClientData incidentType, FormBehavior type)
            :this()
		{
			BehaviorType = type;
            LoadLanguage();
			SelectedIncidentType = incidentType;
            this.parentForm = form;
            form.AdministrationCommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(IncidentTypeForm_AdministrationCommittedChanges);
            this.FormClosing += new FormClosingEventHandler(IncidentTypeForm_FormClosing);
		}


        private void LoadLanguage() 
        {
            this.Icon = ResourceLoader.GetIcon("$Icon.Incidentes");
            this.buttonCancel.Text = ResourceLoader.GetString2("$Message.Cancel");
            switch (behaviorType)
            {
                case FormBehavior.Create:
                    Text = ResourceLoader.GetString2("IncidentTypeFormCreateText");
                    buttonOk.Text = ResourceLoader.GetString("IncidentTypeFormCreateButtonOkText");
                    break;
                case FormBehavior.Edit:
                    Text = ResourceLoader.GetString2("IncidentTypeFormEditText");
                    checkBoxNoEmergency.Enabled = false;
                    buttonOk.Text = ResourceLoader.GetString("IncidentTypeFormEditButtonOkText");
                    break;
                default:
                    break;
            }

            this.layoutControlGroupDepartment.Text = ResourceLoader.GetString2("RecommendedDepartments");
            this.layoutControlGroupFirstLevel.Text = ResourceLoader.GetString2("FirstLevelProcedures");
            this.layoutControlGroupIcon.Text = ResourceLoader.GetString2("IconAssociated");
            this.layoutControlGroupIncidentType.Text = ResourceLoader.GetString2("IncidentTypeInformation");
            this.layoutControlGroupPreview.Text = ResourceLoader.GetString2("Preview");
            
            this.checkBoxNoEmergency.Text = ResourceLoader.GetString2("IncidentTypeNoEmergency");
            this.layoutControlItemName.Text = ResourceLoader.GetString2("Name") + ":*";
            this.layoutControlItemMnemonico.Text = ResourceLoader.GetString2("IncidentTypeCustomCode") + ":*"; 
            buttonExIcon.Text = ResourceLoader.GetString2("SelectIcon");
        }

        void IncidentTypeForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (parentForm != null)
            {
                parentForm.AdministrationCommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(IncidentTypeForm_AdministrationCommittedChanges);
            }
        }

        void IncidentTypeForm_AdministrationCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    lock (syncCommitted)
                    {
                        try
                        {
                            if (ServerServiceClient.GetInstance().OperatorClient == null)
                            { }
                        }
                        catch
                        { }

                        if (e.Objects[0] is IncidentTypeClientData)
                        {
                            #region IncidentTypeClientData
                            IncidentTypeClientData incidentType =
                                    e.Objects[0] as IncidentTypeClientData;
                            if (SelectedIncidentType != null && SelectedIncidentType.Equals(incidentType) && ButtonOkPressed == false)
                            {
                                if (e.Action == CommittedDataAction.Update)
                                {
                                    MessageForm.Show(ResourceLoader.GetString2("UpdateFormIncidentTypeData"), MessageFormType.Warning);
                                    SelectedIncidentType = incidentType;
                                }
                                else if (e.Action == CommittedDataAction.Delete)
                                {
                                    MessageForm.Show(ResourceLoader.GetString2("DeleteFormIncidentTypeData"), MessageFormType.Warning);
                                    FormUtil.InvokeRequired(this,
                                    delegate
                                    {
                                        this.Close();
                                    });
                                }

                            }
                            #endregion
                        }
                        else if (e.Objects[0] is DepartmentTypeClientData)
                        {
                            #region DepartmentTypeClientData
                            DepartmentTypeClientData departmentType = e.Objects[0] as DepartmentTypeClientData;
                            if (e.Action == CommittedDataAction.Update)
                            {
                                FormUtil.InvokeRequired(checkedListBoxDepartmentTypes,
                                    delegate
                                    {
                                        int index = checkedListBoxDepartmentTypes.Items.IndexOf(departmentType);
                                        if (index != -1)
                                        {
                                            checkedListBoxDepartmentTypes.BeginUpdate();
                                            (checkedListBoxDepartmentTypes.Items[index] as DepartmentTypeClientData).Name = departmentType.Name;
                                            (checkedListBoxDepartmentTypes.Items[index] as DepartmentTypeClientData).Version = departmentType.Version;
                                            checkedListBoxDepartmentTypes.EndUpdate();
                                            checkedListBoxDepartmentTypes.Refresh();
                                        }
                                    });
                            }
                            else if (e.Action == CommittedDataAction.Save)
                            {
                                FormUtil.InvokeRequired(checkedListBoxDepartmentTypes,
                                    delegate
                                    {
                                        checkedListBoxDepartmentTypes.BeginUpdate();
                                        checkedListBoxDepartmentTypes.Items.Add(departmentType);
                                        checkedListBoxDepartmentTypes.EndUpdate();
                                        checkedListBoxDepartmentTypes.Refresh();
                                    });
                            }
                            else if (e.Action == CommittedDataAction.Delete)
                            {
                                FormUtil.InvokeRequired(checkedListBoxDepartmentTypes,
                                    delegate
                                    {
                                        DepartmentTypeClientData data = new DepartmentTypeClientData();
                                        data.Code = departmentType.Code;
                                        data.Name = departmentType.Name;

                                        checkedListBoxDepartmentTypes.BeginUpdate();
                                        checkedListBoxDepartmentTypes.Items.Remove(data);
                                        checkedListBoxDepartmentTypes.EndUpdate();
                                        checkedListBoxDepartmentTypes.Refresh();
                                    });
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        public FormBehavior BehaviorType
        {
            get
            {
                return behaviorType;
            }
            set
            {
            
                behaviorType = value;
                Clear();
            }
        }

        public IncidentTypeClientData SelectedIncidentType
        {
            get
            {
                return selectedIncidentType;
            }
            set
            {

                Clear();
                selectedIncidentType = value;
                if (selectedIncidentType != null)
                {
                    textBoxName.Text = selectedIncidentType.FriendlyName;
                    textBoxExMnemonic.Text = selectedIncidentType.CustomCode;
                    checkBoxNoEmergency.Checked = selectedIncidentType.NoEmergency;
                    CheckDepartmentTypes();
                   if (!string.IsNullOrEmpty(selectedIncidentType.Procedure) && ApplicationUtil.IsRTF(selectedIncidentType.Procedure) == true)
                    {
                       richTextBoxExtended1.RichTextBox.Rtf = selectedIncidentType.Procedure;
                    }
                    else if (!string.IsNullOrEmpty(selectedIncidentType.Procedure) && ApplicationUtil.IsRTF(selectedIncidentType.Procedure) == false)
                    {
                        richTextBoxExtended1.RichTextBox.Text = selectedIncidentType.Procedure;
                    }
                    PutIconSaved(selectedIncidentType);

                }
                this.richTextBoxExtended1.ShowRedo = false;
                this.richTextBoxExtended1.ShowUndo = false;
                IncidentTypeParameters_Change(null, null);
            }
        }

       
        
        private void Clear()
        {
            textBoxName.Clear();
            textBoxExMnemonic.Clear();
            checkBoxNoEmergency.Checked = false;
            ClearDepartmentTypes();
            richTextBoxExtended1.RichTextBox.Clear();
        }

        private void PutIconByDefect()
        {
            bitmapviewer = new BitmapViewerForm();
            SetIcon(ResourceLoader.GetString("DefaultIconIncidentType")+".bmp");
        }

        private void PutIconSaved(IncidentTypeClientData incident)
        {
            SetIcon(incident.IconName);
        }

        private void SetIcon(string iconName)
        {
            string iconFullName = Application.StartupPath + SmartCadConfiguration.MapsIconsFolder + iconName;
            if (File.Exists(iconFullName))
            {
                bitmapviewer.FileByDefect = iconName;
                fileName = bitmapviewer.FileByDefect;
                iconPictureBox.Load(iconFullName);
                iconPictureBox.Name = iconName;
            }
        }

        private void ClearDepartmentTypes()
        {
            for (int i = 0; i < checkedListBoxDepartmentTypes.Items.Count; i++)
            {
                checkedListBoxDepartmentTypes.SetItemChecked(i, false);
            }
        }

        private void IncidentTypeParameters_Change(object sender, System.EventArgs e)
        {
            if ((textBoxName.Text.Trim() == "") ||
                (textBoxExMnemonic.Text.Trim() == ""))
                buttonOk.Enabled = false;
            else
                buttonOk.Enabled = true;
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[1] { GetType().GetMethod("buttonOk_Click1", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[0] });
                processForm.CanThrowError = true;
                processForm.ShowDialog();
            }
            catch (FaultException ex)
            {
                if (behaviorType == FormBehavior.Edit)
                    SelectedIncidentType = (IncidentTypeClientData)ServerServiceClient.GetInstance().RefreshClient(SelectedIncidentType,true);
              
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, MessageFormType.Error);
                buttonOkPressed = false;
                GetErrorFocus(ex);
            }
            catch (Exception ex)
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, ex);
                buttonOkPressed = false;
            }
        }

        private void GetErrorFocus(FaultException ex)
        {
            if (ex.Message.Contains(ResourceLoader.GetString2("UK_INCIDENT_TYPE_FRIENDLY_NAME")))
            {
                if (behaviorType == FormBehavior.Create)
                {
                    textBoxName.Text = textBoxName.Text;
                }
                textBoxName.Focus();
            }
            if (ex.Message.Contains(ResourceLoader.GetString2("UK_INCIDENT_TYPE_CUSTOM_CODE")))
            {
                if (behaviorType == FormBehavior.Create)
                {
                    textBoxExMnemonic.Text = textBoxExMnemonic.Text;
                }
                textBoxExMnemonic.Focus();
            }

        }

        private bool buttonOkPressed = false;

        public bool ButtonOkPressed
        {
            get { return buttonOkPressed; }
        }

        private void buttonOk_Click1()
        {
            buttonOkPressed = true;
            if (behaviorType == FormBehavior.Create)
                selectedIncidentType = new IncidentTypeClientData();
            if (selectedIncidentType != null)
            {
                FormUtil.InvokeRequired(this,
                delegate
                {
                    selectedIncidentType.Name = textBoxName.Text;
                    selectedIncidentType.FriendlyName = textBoxName.Text;
                    selectedIncidentType.CustomCode = textBoxExMnemonic.Text;
                    selectedIncidentType.NoEmergency = checkBoxNoEmergency.Checked;
                    if (selectedIncidentType.NoEmergency == true)
                        selectedIncidentType.Exclusive = true;
                    selectedIncidentType.IconName = fileName;
                    ArrayList list = new ArrayList();
                    for (int i = 0; i < checkedListBoxDepartmentTypes.CheckedItems.Count; i++)
                    {
                        DepartmentTypeClientData dt = (DepartmentTypeClientData)checkedListBoxDepartmentTypes.CheckedItems[i];
                        IncidentTypeDepartmentTypeClientData itdt = new IncidentTypeDepartmentTypeClientData();
                        itdt.DepartmentType = dt;
                        itdt.IncidentType = selectedIncidentType; 
                        list.Add(itdt);
                    }
                    selectedIncidentType.IncidentTypeDepartmentTypeClients = new ArrayList(list);
                   if (richTextBoxExtended1.RichTextBox.Text != string.Empty)
                   {
                        selectedIncidentType.Procedure = richTextBoxExtended1.RichTextBox.Rtf;
                   }
                   else 
                   {
                        selectedIncidentType.Procedure = null;
                   }
                });
               
                selectedIncidentType.IconName = fileName;
                ServerServiceClient.GetInstance().SaveOrUpdateClientData(selectedIncidentType);
            }
        }

        private void FillDepartmentTypes()
        {
            checkedListBoxDepartmentTypes.Items.Clear();
            IList departmentTypes = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetDepartmentsTypeWithStationsAssociated);
            
            foreach (DepartmentTypeClientData dt in departmentTypes)
            {
                checkedListBoxDepartmentTypes.Items.Add(dt);
            }
            checkedListBoxDepartmentTypes.Sorted = true;
        }

        private void CheckDepartmentTypes()
        {
            /*ServerServiceClient.GetInstance().InitializeLazy(
                selectedIncidentType, 
                selectedIncidentType.IncidentTypeDepartmentTypeClients);*/
            foreach (IncidentTypeDepartmentTypeClientData dt in selectedIncidentType.IncidentTypeDepartmentTypeClients)
            {
                checkedListBoxDepartmentTypes.SetItemChecked(
                    checkedListBoxDepartmentTypes.Items.IndexOf(dt.DepartmentType), true);
            }
        }



        private void buttonEx1_Click(object sender, EventArgs e)
        {
            FixedBitmapIconBox();
            DialogResult result = bitmapviewer.ShowDialog();
            if (result == DialogResult.OK)
            {
                if (!string.IsNullOrEmpty(bitmapviewer.FileName))
                    fileName = bitmapviewer.FileName;
                else
                    fileName = bitmapviewer.FileByDefect;
                bitmapviewer.FileByDefect = fileName;
            }
            else if (result == DialogResult.Cancel)
            {
                fileName = bitmapviewer.FileByDefect;
            }
            this.iconPictureBox.Name = fileName;
            iconPictureBox.Load(bitmapviewer.bitmapViewer1.Directory + fileName);
        }

        private void FixedBitmapIconBox()
        {
            for (int i = 0; i < this.bitmapviewer.bitmapViewer1.PnlPictures.Controls.Count; i++)
            {
                if ((this.bitmapviewer.bitmapViewer1.PnlPictures.Controls[i] as PictureBox).Name == this.iconPictureBox.Name)
                {
                    (this.bitmapviewer.bitmapViewer1.PnlPictures.Controls[i] as PictureBox).BorderStyle = BorderStyle.Fixed3D;
                    (this.bitmapviewer.bitmapViewer1.PnlPictures.Controls[i] as PictureBox).DisplayRectangle.Inflate(15, 15);
                    (this.bitmapviewer.bitmapViewer1.PnlPictures.Controls[i] as PictureBox).BackColor = SystemColors.GradientActiveCaption;

                }
                else
                {
                    (this.bitmapviewer.bitmapViewer1.PnlPictures.Controls[i] as PictureBox).BorderStyle = BorderStyle.FixedSingle;
                    (this.bitmapviewer.bitmapViewer1.PnlPictures.Controls[i] as PictureBox).DisplayRectangle.Inflate(0, 0);
                    (this.bitmapviewer.bitmapViewer1.PnlPictures.Controls[i] as PictureBox).BackColor = SystemColors.Window;
                }
            }
        }
        private void checkBoxNoEmergency_CheckedChanged(object sender, EventArgs e)
        {
            if(textBoxExMnemonic.Enabled)
            {
                textBoxExMnemonic.Text = "NE";
                textBoxExMnemonic.Enabled = false;
                checkedListBoxDepartmentTypes.Enabled = false;
                buttonExIcon.Enabled = false;
                ClearDepartmentTypes();
            }
            else
            {
                textBoxExMnemonic.Text = "";
                textBoxExMnemonic.Enabled = true;
                checkedListBoxDepartmentTypes.Enabled = true;
                buttonExIcon.Enabled = true;
            }
        }

        private void richTextBoxExtended1_RichTextBox_GotFocus(object sender, EventArgs e) 
        {
            this.AcceptButton = null;
        
        }

        private void textBoxName_GotFocus(object sender, EventArgs e) 
        {
            this.AcceptButton = buttonOk;
        }

        private void IncidentTypeForm_Load(object sender, EventArgs e)
        {
            richTextBoxExtended1.ShowRedo = true;
            richTextBoxExtended1.ShowUndo = true;

          
        }


    }
}
