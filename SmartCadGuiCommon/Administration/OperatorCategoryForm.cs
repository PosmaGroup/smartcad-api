using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Smartmatic.SmartCad.Service;
using System.Reflection;
using System.ServiceModel;
using DevExpress.XtraEditors;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadCore.Core;

namespace Smartmatic.SmartCad.Gui
{
    public partial class OperatorCategoryForm : XtraForm
    {

        private OperatorCategoryClientData OperatorCategoryOnTraining;
        private OperatorCategoryClientData OperatorCategoryJunior;
        private OperatorCategoryClientData OperatorCategorySenior;

        public OperatorCategoryForm()
        {
            InitializeComponent();
            LoadLanguage();
            LoadOperatorCategories();
         
        }

        private bool buttonOkPressed = false;
        public bool ButtonOkPressed
        {
            set { buttonOkPressed = value; }
            get { return buttonOkPressed; }

        }

        private void LoadLanguage()
        {
            this.Icon = ResourceLoader.GetIcon("$Icon.OperatorCategory");
            layoutControlGroupCategory1.Text = ResourceLoader.GetString2("CategoryOne");
            layoutControlGroupCategory2.Text = ResourceLoader.GetString2("CategoryTwo");
            layoutControlGroupCategory3.Text = ResourceLoader.GetString2("categoryThree");
            layoutControlItemLevel1.Text = ResourceLoader.GetString2("Level") + ":";
            layoutControlItemLevel2.Text = ResourceLoader.GetString2("Level") + ":";
            layoutControlItemLevel3.Text = ResourceLoader.GetString2("Level") + ":";
            layoutControlItemName1.Text = ResourceLoader.GetString2("Name") + ":*";
            layoutControlItemName2.Text = ResourceLoader.GetString2("Name") + ":*";
            layoutControlItemName3.Text = ResourceLoader.GetString2("Name") + ":*";
            layoutControlItemDescription1.Text = ResourceLoader.GetString2("Description") + ":";
            layoutControlItemDescription2.Text = ResourceLoader.GetString2("Description") + ":";
            layoutControlItemDescription3.Text = ResourceLoader.GetString2("Description") + ":";
            buttonExCancel.Text = ResourceLoader.GetString2("Cancel");
            buttonExOk.Text = ResourceLoader.GetString2("Accept");
            this.Text = ResourceLoader.GetString2("OperatorCategories");
        }

        private void LoadOperatorCategories() 
        {
            IList operatorCatList = (IList)ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetOperatorCategories);

            foreach (OperatorCategoryClientData operCat in operatorCatList) 
            {
                FormUtil.InvokeRequired(this, delegate
                {
                    switch (operCat.Name)
                    {

                        case "OnTraining":
                            OperatorCategoryOnTraining = operCat;

                            textBoxExName1.Text = OperatorCategoryOnTraining.FriendlyName;
                            textBoxExLevel1.Text = OperatorCategoryOnTraining.Level.ToString();
                            textBoxExDescription1.Text = OperatorCategoryOnTraining.Description;
                            break;
                        case "Junior":
                            OperatorCategoryJunior = operCat;
                            textBoxExName2.Text = OperatorCategoryJunior.FriendlyName;
                            textBoxExLevel2.Text = OperatorCategoryJunior.Level.ToString();
                            textBoxExDescription2.Text = OperatorCategoryJunior.Description;
                            break;
                        case "Senior":
                            OperatorCategorySenior = operCat;
                            textBoxExName3.Text = OperatorCategorySenior.FriendlyName;
                            textBoxExLevel3.Text = OperatorCategorySenior.Level.ToString();
                            textBoxExDescription3.Text = OperatorCategorySenior.Description;
                            break;
                        default:
                            break;
                    }
                });
            }
            ButtonsActivation(null, null);
        }

        private void ButtonsActivation(object sender, System.EventArgs e)
        {
            if (textBoxExName1.Text.Trim() == "" || textBoxExName2.Text.Trim() == "" || textBoxExName3.Text.Trim() == "")
                buttonExOk.Enabled = false;
            else
                buttonExOk.Enabled = true;
        }

        private void buttonExOk_Click(object sender, EventArgs e)
        {
            try
            {
                BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[1] { GetType().GetMethod("buttonExOk_Click1", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[0] });
                processForm.CanThrowError = true;
                processForm.ShowDialog();
            }
            catch (FaultException ex)
            {
                LoadOperatorCategories();
                //  GetErrorFocus(ex);
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, MessageFormType.Error);
                buttonOkPressed = false;
            }
            catch (Exception ex)
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, ex);
                buttonOkPressed = false;
            }
        }


        private void buttonExOk_Click1()
        {
            buttonOkPressed = true;
            if (OperatorCategoryOnTraining != null)
            {
                OperatorCategoryOnTraining.FriendlyName = textBoxExName1.Text.Trim();
                OperatorCategoryOnTraining.Description = textBoxExDescription1.Text.Trim();

                ServerServiceClient.GetInstance().SaveOrUpdateClientData(OperatorCategoryOnTraining);
            }

            if (OperatorCategoryJunior != null)
            {
                OperatorCategoryJunior.FriendlyName = textBoxExName2.Text.Trim();
                OperatorCategoryJunior.Description = textBoxExDescription2.Text.Trim();

                ServerServiceClient.GetInstance().SaveOrUpdateClientData(OperatorCategoryJunior);
            }

            if (OperatorCategorySenior != null)
            {
                OperatorCategorySenior.FriendlyName = textBoxExName3.Text.Trim();
                OperatorCategorySenior.Description = textBoxExDescription3.Text.Trim();

                ServerServiceClient.GetInstance().SaveOrUpdateClientData(OperatorCategorySenior);
            }
        }

        private void OperatorCategoryForm_Load(object sender, EventArgs e)
        {
            this.Height = 609;
            this.Width = 472;
        }
    }
}
