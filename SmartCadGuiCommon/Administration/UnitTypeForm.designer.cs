using SmartCadControls.Controls;
using SmartCadCore.Common;

namespace SmartCadGuiCommon
{
    partial class UnitTypeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.UnitTypeFormConvertedLayout = new DevExpress.XtraLayout.LayoutControl();
            this.buttonEx1 = new DevExpress.XtraEditors.SimpleButton();
            this.iconPictureBox = new System.Windows.Forms.PictureBox();
            this.textBoxSeats = new TextBoxEx();
            this.textBoxVelocity = new TextBoxEx();
            this.comboBoxDepartmentType = new System.Windows.Forms.ComboBox();
            this.textBoxDescription = new TextBoxEx();
            this.textBoxName = new TextBoxEx();
            this.listBoxAssigned = new System.Windows.Forms.ListBox();
            this.listBoxNotAssigned = new System.Windows.Forms.ListBox();
            this.buttonAddIncidentType = new DevExpress.XtraEditors.SimpleButton();
            this.buttonAddAllIncidentType = new DevExpress.XtraEditors.SimpleButton();
            this.buttonRemoveIncidentType = new DevExpress.XtraEditors.SimpleButton();
            this.buttonRemoveAllIncidentType = new DevExpress.XtraEditors.SimpleButton();
            this.buttonOk = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupUnit = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemSeats = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemVelocity = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemDepartment = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemDes = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemUnitType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupIncidentType = new DevExpress.XtraLayout.LayoutControlGroup();
            this.listBoxNotAssigneditem = new DevExpress.XtraLayout.LayoutControlItem();
            this.listBoxAssigneditem = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.buttonAddIncidentTypeitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.buttonAddAllIncidentTypeitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.buttonRemoveIncidentTypeitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.buttonRemoveAllIncidentTypeitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupIcon = new DevExpress.XtraLayout.LayoutControlGroup();
            this.buttonEx1item = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupPreview = new DevExpress.XtraLayout.LayoutControlGroup();
            this.iconPictureBoxitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.button1 = new DevExpress.XtraEditors.SimpleButton();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.UnitTypeFormConvertedLayout)).BeginInit();
            this.UnitTypeFormConvertedLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iconPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSeats)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemVelocity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemUnitType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupIncidentType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxNotAssigneditem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxAssigneditem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonAddIncidentTypeitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonAddAllIncidentTypeitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonRemoveIncidentTypeitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonRemoveAllIncidentTypeitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEx1item)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupPreview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconPictureBoxitem)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCancel.Appearance.Options.UseFont = true;
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(428, 570);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(82, 32);
            this.buttonCancel.StyleController = this.UnitTypeFormConvertedLayout;
            this.buttonCancel.TabIndex = 4;
            // 
            // UnitTypeFormConvertedLayout
            // 
            this.UnitTypeFormConvertedLayout.AllowCustomizationMenu = false;
            this.UnitTypeFormConvertedLayout.Controls.Add(this.buttonEx1);
            this.UnitTypeFormConvertedLayout.Controls.Add(this.iconPictureBox);
            this.UnitTypeFormConvertedLayout.Controls.Add(this.textBoxSeats);
            this.UnitTypeFormConvertedLayout.Controls.Add(this.textBoxVelocity);
            this.UnitTypeFormConvertedLayout.Controls.Add(this.comboBoxDepartmentType);
            this.UnitTypeFormConvertedLayout.Controls.Add(this.textBoxDescription);
            this.UnitTypeFormConvertedLayout.Controls.Add(this.textBoxName);
            this.UnitTypeFormConvertedLayout.Controls.Add(this.listBoxAssigned);
            this.UnitTypeFormConvertedLayout.Controls.Add(this.listBoxNotAssigned);
            this.UnitTypeFormConvertedLayout.Controls.Add(this.buttonAddIncidentType);
            this.UnitTypeFormConvertedLayout.Controls.Add(this.buttonAddAllIncidentType);
            this.UnitTypeFormConvertedLayout.Controls.Add(this.buttonRemoveIncidentType);
            this.UnitTypeFormConvertedLayout.Controls.Add(this.buttonRemoveAllIncidentType);
            this.UnitTypeFormConvertedLayout.Controls.Add(this.buttonCancel);
            this.UnitTypeFormConvertedLayout.Controls.Add(this.buttonOk);
            this.UnitTypeFormConvertedLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UnitTypeFormConvertedLayout.Location = new System.Drawing.Point(0, 0);
            this.UnitTypeFormConvertedLayout.Name = "UnitTypeFormConvertedLayout";
            this.UnitTypeFormConvertedLayout.OptionsCustomizationForm.ShowLayoutTreeView = false;
            this.UnitTypeFormConvertedLayout.Root = this.layoutControlGroup1;
            this.UnitTypeFormConvertedLayout.Size = new System.Drawing.Size(512, 604);
            this.UnitTypeFormConvertedLayout.TabIndex = 6;
            // 
            // buttonEx1
            // 
            this.buttonEx1.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonEx1.Appearance.Options.UseFont = true;
            this.buttonEx1.Location = new System.Drawing.Point(7, 489);
            this.buttonEx1.Name = "buttonEx1";
            this.buttonEx1.Size = new System.Drawing.Size(91, 38);
            this.buttonEx1.StyleController = this.UnitTypeFormConvertedLayout;
            this.buttonEx1.TabIndex = 3;
            this.buttonEx1.Text = "buttonEx1";
            this.buttonEx1.Click += new System.EventHandler(this.buttonEx1_Click);
            // 
            // iconPictureBox
            // 
            this.iconPictureBox.Location = new System.Drawing.Point(107, 480);
            this.iconPictureBox.Name = "iconPictureBox";
            this.iconPictureBox.Size = new System.Drawing.Size(393, 76);
            this.iconPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.iconPictureBox.TabIndex = 0;
            this.iconPictureBox.TabStop = false;
            // 
            // textBoxSeats
            // 
            this.textBoxSeats.AllowsLetters = false;
            this.textBoxSeats.AllowsNumbers = true;
            this.textBoxSeats.AllowsPunctuation = false;
            this.textBoxSeats.AllowsSeparators = false;
            this.textBoxSeats.AllowsSymbols = false;
            this.textBoxSeats.AllowsWhiteSpaces = false;
            this.textBoxSeats.ExtraAllowedChars = "";
            this.textBoxSeats.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxSeats.Location = new System.Drawing.Point(155, 50);
            this.textBoxSeats.MaxLength = 2;
            this.textBoxSeats.Name = "textBoxSeats";
            this.textBoxSeats.NonAllowedCharacters = "";
            this.textBoxSeats.RegularExpresion = "";
            this.textBoxSeats.Size = new System.Drawing.Size(350, 20);
            this.textBoxSeats.TabIndex = 3;
            this.textBoxSeats.TextChanged += new System.EventHandler(this.UnitTypeParameters_Change);
            // 
            // textBoxVelocity
            // 
            this.textBoxVelocity.AllowsLetters = false;
            this.textBoxVelocity.AllowsNumbers = true;
            this.textBoxVelocity.AllowsPunctuation = false;
            this.textBoxVelocity.AllowsSeparators = false;
            this.textBoxVelocity.AllowsSymbols = false;
            this.textBoxVelocity.AllowsWhiteSpaces = false;
            this.textBoxVelocity.ExtraAllowedChars = "";
            this.textBoxVelocity.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxVelocity.Location = new System.Drawing.Point(155, 74);
            this.textBoxVelocity.MaxLength = 3;
            this.textBoxVelocity.Name = "textBoxVelocity";
            this.textBoxVelocity.NonAllowedCharacters = "";
            this.textBoxVelocity.RegularExpresion = "";
            this.textBoxVelocity.Size = new System.Drawing.Size(350, 20);
            this.textBoxVelocity.TabIndex = 5;
            this.textBoxVelocity.TextChanged += new System.EventHandler(this.UnitTypeParameters_Change);
            // 
            // comboBoxDepartmentType
            // 
            this.comboBoxDepartmentType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDepartmentType.FormattingEnabled = true;
            this.comboBoxDepartmentType.Location = new System.Drawing.Point(155, 194);
            this.comboBoxDepartmentType.Name = "comboBoxDepartmentType";
            this.comboBoxDepartmentType.Size = new System.Drawing.Size(350, 21);
            this.comboBoxDepartmentType.Sorted = true;
            this.comboBoxDepartmentType.TabIndex = 9;
            this.comboBoxDepartmentType.SelectionChangeCommitted += new System.EventHandler(this.comboBoxDepartmentType_Change);
            // 
            // textBoxDescription
            // 
            this.textBoxDescription.AcceptsReturn = true;
            this.textBoxDescription.AllowsLetters = true;
            this.textBoxDescription.AllowsNumbers = true;
            this.textBoxDescription.AllowsPunctuation = true;
            this.textBoxDescription.AllowsSeparators = true;
            this.textBoxDescription.AllowsSymbols = true;
            this.textBoxDescription.AllowsWhiteSpaces = true;
            this.textBoxDescription.ExtraAllowedChars = "";
            this.textBoxDescription.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxDescription.Location = new System.Drawing.Point(155, 98);
            this.textBoxDescription.MaxLength = 200;
            this.textBoxDescription.Multiline = true;
            this.textBoxDescription.Name = "textBoxDescription";
            this.textBoxDescription.NonAllowedCharacters = "";
            this.textBoxDescription.RegularExpresion = "";
            this.textBoxDescription.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxDescription.Size = new System.Drawing.Size(350, 92);
            this.textBoxDescription.TabIndex = 7;
            this.textBoxDescription.TextChanged += new System.EventHandler(this.UnitTypeParameters_Change);
            // 
            // textBoxName
            // 
            this.textBoxName.AllowsLetters = true;
            this.textBoxName.AllowsNumbers = true;
            this.textBoxName.AllowsPunctuation = true;
            this.textBoxName.AllowsSeparators = true;
            this.textBoxName.AllowsSymbols = true;
            this.textBoxName.AllowsWhiteSpaces = true;
            this.textBoxName.ExtraAllowedChars = "";
            this.textBoxName.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxName.Location = new System.Drawing.Point(155, 26);
            this.textBoxName.MaxLength = 40;
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.NonAllowedCharacters = "";
            this.textBoxName.RegularExpresion = "";
            this.textBoxName.Size = new System.Drawing.Size(350, 20);
            this.textBoxName.TabIndex = 1;
            this.textBoxName.TextChanged += new System.EventHandler(this.UnitTypeParameters_Change);
            // 
            // listBoxAssigned
            // 
            this.listBoxAssigned.Enabled = false;
            this.listBoxAssigned.FormattingEnabled = true;
            this.listBoxAssigned.HorizontalScrollbar = true;
            this.listBoxAssigned.IntegralHeight = false;
            this.listBoxAssigned.Location = new System.Drawing.Point(287, 265);
            this.listBoxAssigned.Name = "listBoxAssigned";
            this.listBoxAssigned.Size = new System.Drawing.Size(218, 156);
            this.listBoxAssigned.Sorted = true;
            this.listBoxAssigned.TabIndex = 7;
            this.listBoxAssigned.DoubleClick += new System.EventHandler(this.listBoxAssigned_DoubleClick);
            this.listBoxAssigned.Enter += new System.EventHandler(this.listBoxAssigned_Enter);
            // 
            // listBoxNotAssigned
            // 
            this.listBoxNotAssigned.Enabled = false;
            this.listBoxNotAssigned.FormattingEnabled = true;
            this.listBoxNotAssigned.HorizontalScrollbar = true;
            this.listBoxNotAssigned.IntegralHeight = false;
            this.listBoxNotAssigned.Location = new System.Drawing.Point(7, 265);
            this.listBoxNotAssigned.Name = "listBoxNotAssigned";
            this.listBoxNotAssigned.Size = new System.Drawing.Size(217, 156);
            this.listBoxNotAssigned.Sorted = true;
            this.listBoxNotAssigned.TabIndex = 1;
            this.listBoxNotAssigned.DoubleClick += new System.EventHandler(this.listBoxNotAssigned_DoubleClick);
            this.listBoxNotAssigned.Enter += new System.EventHandler(this.listBoxNotAssigned_Enter);
            // 
            // buttonAddIncidentType
            // 
            this.buttonAddIncidentType.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonAddIncidentType.Appearance.Options.UseForeColor = true;
            this.buttonAddIncidentType.Enabled = false;
            this.buttonAddIncidentType.Location = new System.Drawing.Point(228, 278);
            this.buttonAddIncidentType.Name = "buttonAddIncidentType";
            this.buttonAddIncidentType.Size = new System.Drawing.Size(55, 29);
            this.buttonAddIncidentType.StyleController = this.UnitTypeFormConvertedLayout;
            this.buttonAddIncidentType.TabIndex = 2;
            this.buttonAddIncidentType.Text = ">";
            this.toolTip1.SetToolTip(this.buttonAddIncidentType, ResourceLoader.GetString2("AssociateIncidentType"));
            this.buttonAddIncidentType.Click += new System.EventHandler(this.buttonAddIncidentType_Click);
            // 
            // buttonAddAllIncidentType
            // 
            this.buttonAddAllIncidentType.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonAddAllIncidentType.Appearance.Options.UseForeColor = true;
            this.buttonAddAllIncidentType.Enabled = false;
            this.buttonAddAllIncidentType.Location = new System.Drawing.Point(228, 311);
            this.buttonAddAllIncidentType.Name = "buttonAddAllIncidentType";
            this.buttonAddAllIncidentType.Size = new System.Drawing.Size(55, 29);
            this.buttonAddAllIncidentType.StyleController = this.UnitTypeFormConvertedLayout;
            this.buttonAddAllIncidentType.TabIndex = 3;
            this.buttonAddAllIncidentType.Text = ">>";
            this.toolTip1.SetToolTip(this.buttonAddAllIncidentType, ResourceLoader.GetString2("AssociateAllIncidentTypes"));
            this.buttonAddAllIncidentType.Click += new System.EventHandler(this.buttonAddAllIncidentType_Click);
            // 
            // buttonRemoveIncidentType
            // 
            this.buttonRemoveIncidentType.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonRemoveIncidentType.Appearance.Options.UseForeColor = true;
            this.buttonRemoveIncidentType.Enabled = false;
            this.buttonRemoveIncidentType.Location = new System.Drawing.Point(228, 344);
            this.buttonRemoveIncidentType.Name = "buttonRemoveIncidentType";
            this.buttonRemoveIncidentType.Size = new System.Drawing.Size(55, 29);
            this.buttonRemoveIncidentType.StyleController = this.UnitTypeFormConvertedLayout;
            this.buttonRemoveIncidentType.TabIndex = 4;
            this.buttonRemoveIncidentType.Text = "<";
            this.toolTip1.SetToolTip(this.buttonRemoveIncidentType, ResourceLoader.GetString2("RemoveIncidentType"));
            this.buttonRemoveIncidentType.Click += new System.EventHandler(this.buttonRemoveIncidentType_Click);
            // 
            // buttonRemoveAllIncidentType
            // 
            this.buttonRemoveAllIncidentType.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonRemoveAllIncidentType.Appearance.Options.UseForeColor = true;
            this.buttonRemoveAllIncidentType.Enabled = false;
            this.buttonRemoveAllIncidentType.Location = new System.Drawing.Point(228, 377);
            this.buttonRemoveAllIncidentType.Name = "buttonRemoveAllIncidentType";
            this.buttonRemoveAllIncidentType.Size = new System.Drawing.Size(55, 29);
            this.buttonRemoveAllIncidentType.StyleController = this.UnitTypeFormConvertedLayout;
            this.buttonRemoveAllIncidentType.TabIndex = 5;
            this.buttonRemoveAllIncidentType.Text = "<<";
            this.toolTip1.SetToolTip(this.buttonRemoveAllIncidentType, ResourceLoader.GetString2("RemoveAllIncidentTypes"));
            this.buttonRemoveAllIncidentType.Click += new System.EventHandler(this.buttonRemoveAllIncidentType_Click);
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOk.Appearance.Options.UseFont = true;
            this.buttonOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOk.Enabled = false;
            this.buttonOk.Location = new System.Drawing.Point(342, 570);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(82, 32);
            this.buttonOk.StyleController = this.UnitTypeFormConvertedLayout;
            this.buttonOk.TabIndex = 3;
            this.buttonOk.Text = "Accept";
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupUnit,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.emptySpaceItem1,
            this.layoutControlGroupIncidentType,
            this.layoutControlGroupIcon});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(512, 604);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroupUnit
            // 
            this.layoutControlGroupUnit.AppearanceGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupUnit.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroupUnit.CustomizationFormText = "layoutControlGroupUnit";
            this.layoutControlGroupUnit.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemSeats,
            this.layoutControlItemVelocity,
            this.layoutControlItemDepartment,
            this.layoutControlItemDes,
            this.layoutControlItemUnitType});
            this.layoutControlGroupUnit.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupUnit.Name = "layoutControlGroupUnit";
            this.layoutControlGroupUnit.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupUnit.Size = new System.Drawing.Size(512, 222);
            this.layoutControlGroupUnit.Text = "layoutControlGroupUnit";
            // 
            // layoutControlItemSeats
            // 
            this.layoutControlItemSeats.Control = this.textBoxSeats;
            this.layoutControlItemSeats.CustomizationFormText = "layoutControlItemSeats";
            this.layoutControlItemSeats.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItemSeats.Name = "layoutControlItemSeats";
            this.layoutControlItemSeats.Size = new System.Drawing.Size(502, 24);
            this.layoutControlItemSeats.Text = "layoutControlItemSeats";
            this.layoutControlItemSeats.TextSize = new System.Drawing.Size(144, 13);
            // 
            // layoutControlItemVelocity
            // 
            this.layoutControlItemVelocity.Control = this.textBoxVelocity;
            this.layoutControlItemVelocity.CustomizationFormText = "layoutControlItemSeats";
            this.layoutControlItemVelocity.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItemVelocity.Name = "layoutControlItemVelocity";
            this.layoutControlItemVelocity.Size = new System.Drawing.Size(502, 24);
            this.layoutControlItemVelocity.Text = "layoutControlItemSeats";
            this.layoutControlItemVelocity.TextSize = new System.Drawing.Size(144, 13);
            // 
            // layoutControlItemDepartment
            // 
            this.layoutControlItemDepartment.Control = this.comboBoxDepartmentType;
            this.layoutControlItemDepartment.CustomizationFormText = "layoutControlItemDepartment";
            this.layoutControlItemDepartment.Location = new System.Drawing.Point(0, 168);
            this.layoutControlItemDepartment.Name = "layoutControlItemDepartment";
            this.layoutControlItemDepartment.Size = new System.Drawing.Size(502, 25);
            this.layoutControlItemDepartment.Text = "layoutControlItemDepartment";
            this.layoutControlItemDepartment.TextSize = new System.Drawing.Size(144, 13);
            // 
            // layoutControlItemDes
            // 
            this.layoutControlItemDes.Control = this.textBoxDescription;
            this.layoutControlItemDes.CustomizationFormText = "layoutControlItemSeats";
            this.layoutControlItemDes.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItemDes.MinSize = new System.Drawing.Size(180, 31);
            this.layoutControlItemDes.Name = "layoutControlItemDes";
            this.layoutControlItemDes.Size = new System.Drawing.Size(502, 96);
            this.layoutControlItemDes.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemDes.Text = "layoutControlItemSeats";
            this.layoutControlItemDes.TextSize = new System.Drawing.Size(144, 13);
            // 
            // layoutControlItemUnitType
            // 
            this.layoutControlItemUnitType.Control = this.textBoxName;
            this.layoutControlItemUnitType.CustomizationFormText = "layoutControlItemUnitType";
            this.layoutControlItemUnitType.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemUnitType.Name = "layoutControlItemUnitType";
            this.layoutControlItemUnitType.Size = new System.Drawing.Size(502, 24);
            this.layoutControlItemUnitType.Text = "layoutControlItemUnitType";
            this.layoutControlItemUnitType.TextSize = new System.Drawing.Size(144, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.buttonCancel;
            this.layoutControlItem1.CustomizationFormText = "buttonCancelitem";
            this.layoutControlItem1.Location = new System.Drawing.Point(426, 568);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem1.Name = "buttonCancelitem";
            this.layoutControlItem1.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "buttonCancelitem";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.buttonOk;
            this.layoutControlItem2.CustomizationFormText = "buttonOkitem";
            this.layoutControlItem2.Location = new System.Drawing.Point(340, 568);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem2.Name = "buttonOkitem";
            this.layoutControlItem2.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "buttonOkitem";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 568);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(340, 36);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupIncidentType
            // 
            this.layoutControlGroupIncidentType.CustomizationFormText = "layoutControlGroupIncidentType";
            this.layoutControlGroupIncidentType.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.listBoxNotAssigneditem,
            this.listBoxAssigneditem,
            this.emptySpaceItem2,
            this.buttonAddIncidentTypeitem,
            this.buttonAddAllIncidentTypeitem,
            this.buttonRemoveIncidentTypeitem,
            this.buttonRemoveAllIncidentTypeitem,
            this.emptySpaceItem3});
            this.layoutControlGroupIncidentType.Location = new System.Drawing.Point(0, 215);
            this.layoutControlGroupIncidentType.Name = "layoutControlGroupIncidentType";
            this.layoutControlGroupIncidentType.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupIncidentType.Size = new System.Drawing.Size(512, 206);
            this.layoutControlGroupIncidentType.Text = "layoutControlGroupIncidentType";
            // 
            // listBoxNotAssigneditem
            // 
            this.listBoxNotAssigneditem.Control = this.listBoxNotAssigned;
            this.listBoxNotAssigneditem.CustomizationFormText = "No asociados";
            this.listBoxNotAssigneditem.Location = new System.Drawing.Point(0, 0);
            this.listBoxNotAssigneditem.MinSize = new System.Drawing.Size(123, 56);
            this.listBoxNotAssigneditem.Name = "listBoxNotAssigneditem";
            this.listBoxNotAssigneditem.Size = new System.Drawing.Size(221, 176);
            this.listBoxNotAssigneditem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.listBoxNotAssigneditem.Text = "listBoxNotAssigneditem";
            this.listBoxNotAssigneditem.TextLocation = DevExpress.Utils.Locations.Top;
            this.listBoxNotAssigneditem.TextSize = new System.Drawing.Size(144, 13);
            // 
            // listBoxAssigneditem
            // 
            this.listBoxAssigneditem.Control = this.listBoxAssigned;
            this.listBoxAssigneditem.CustomizationFormText = "Asociados";
            this.listBoxAssigneditem.Location = new System.Drawing.Point(280, 0);
            this.listBoxAssigneditem.MinSize = new System.Drawing.Size(123, 56);
            this.listBoxAssigneditem.Name = "listBoxAssigneditem";
            this.listBoxAssigneditem.Size = new System.Drawing.Size(222, 176);
            this.listBoxAssigneditem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.listBoxAssigneditem.Text = "listBoxAssigneditem";
            this.listBoxAssigneditem.TextLocation = DevExpress.Utils.Locations.Top;
            this.listBoxAssigneditem.TextSize = new System.Drawing.Size(144, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(221, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(59, 29);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // buttonAddIncidentTypeitem
            // 
            this.buttonAddIncidentTypeitem.Control = this.buttonAddIncidentType;
            this.buttonAddIncidentTypeitem.CustomizationFormText = "buttonAddIncidentTypeitem";
            this.buttonAddIncidentTypeitem.Location = new System.Drawing.Point(221, 29);
            this.buttonAddIncidentTypeitem.MaxSize = new System.Drawing.Size(59, 33);
            this.buttonAddIncidentTypeitem.MinSize = new System.Drawing.Size(59, 33);
            this.buttonAddIncidentTypeitem.Name = "buttonAddIncidentTypeitem";
            this.buttonAddIncidentTypeitem.Size = new System.Drawing.Size(59, 33);
            this.buttonAddIncidentTypeitem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.buttonAddIncidentTypeitem.Text = "buttonAddIncidentTypeitem";
            this.buttonAddIncidentTypeitem.TextSize = new System.Drawing.Size(0, 0);
            this.buttonAddIncidentTypeitem.TextToControlDistance = 0;
            this.buttonAddIncidentTypeitem.TextVisible = false;
            // 
            // buttonAddAllIncidentTypeitem
            // 
            this.buttonAddAllIncidentTypeitem.Control = this.buttonAddAllIncidentType;
            this.buttonAddAllIncidentTypeitem.CustomizationFormText = "buttonAddAllIncidentTypeitem";
            this.buttonAddAllIncidentTypeitem.Location = new System.Drawing.Point(221, 62);
            this.buttonAddAllIncidentTypeitem.MaxSize = new System.Drawing.Size(59, 33);
            this.buttonAddAllIncidentTypeitem.MinSize = new System.Drawing.Size(59, 33);
            this.buttonAddAllIncidentTypeitem.Name = "buttonAddAllIncidentTypeitem";
            this.buttonAddAllIncidentTypeitem.Size = new System.Drawing.Size(59, 33);
            this.buttonAddAllIncidentTypeitem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.buttonAddAllIncidentTypeitem.Text = "buttonAddAllIncidentTypeitem";
            this.buttonAddAllIncidentTypeitem.TextSize = new System.Drawing.Size(0, 0);
            this.buttonAddAllIncidentTypeitem.TextToControlDistance = 0;
            this.buttonAddAllIncidentTypeitem.TextVisible = false;
            // 
            // buttonRemoveIncidentTypeitem
            // 
            this.buttonRemoveIncidentTypeitem.Control = this.buttonRemoveIncidentType;
            this.buttonRemoveIncidentTypeitem.CustomizationFormText = "buttonRemoveIncidentTypeitem";
            this.buttonRemoveIncidentTypeitem.Location = new System.Drawing.Point(221, 95);
            this.buttonRemoveIncidentTypeitem.MaxSize = new System.Drawing.Size(59, 33);
            this.buttonRemoveIncidentTypeitem.MinSize = new System.Drawing.Size(59, 33);
            this.buttonRemoveIncidentTypeitem.Name = "buttonRemoveIncidentTypeitem";
            this.buttonRemoveIncidentTypeitem.Size = new System.Drawing.Size(59, 33);
            this.buttonRemoveIncidentTypeitem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.buttonRemoveIncidentTypeitem.Text = "buttonRemoveIncidentTypeitem";
            this.buttonRemoveIncidentTypeitem.TextSize = new System.Drawing.Size(0, 0);
            this.buttonRemoveIncidentTypeitem.TextToControlDistance = 0;
            this.buttonRemoveIncidentTypeitem.TextVisible = false;
            // 
            // buttonRemoveAllIncidentTypeitem
            // 
            this.buttonRemoveAllIncidentTypeitem.Control = this.buttonRemoveAllIncidentType;
            this.buttonRemoveAllIncidentTypeitem.CustomizationFormText = "buttonRemoveAllIncidentTypeitem";
            this.buttonRemoveAllIncidentTypeitem.Location = new System.Drawing.Point(221, 128);
            this.buttonRemoveAllIncidentTypeitem.MaxSize = new System.Drawing.Size(59, 33);
            this.buttonRemoveAllIncidentTypeitem.MinSize = new System.Drawing.Size(59, 33);
            this.buttonRemoveAllIncidentTypeitem.Name = "buttonRemoveAllIncidentTypeitem";
            this.buttonRemoveAllIncidentTypeitem.Size = new System.Drawing.Size(59, 33);
            this.buttonRemoveAllIncidentTypeitem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.buttonRemoveAllIncidentTypeitem.Text = "buttonRemoveAllIncidentTypeitem";
            this.buttonRemoveAllIncidentTypeitem.TextSize = new System.Drawing.Size(0, 0);
            this.buttonRemoveAllIncidentTypeitem.TextToControlDistance = 0;
            this.buttonRemoveAllIncidentTypeitem.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(221, 161);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(59, 15);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupIcon
            // 
            this.layoutControlGroupIcon.CustomizationFormText = "layoutControlGroupIcon";
            this.layoutControlGroupIcon.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.buttonEx1item,
            this.emptySpaceItem4,
            this.emptySpaceItem5,
            this.layoutControlGroupPreview});
            this.layoutControlGroupIcon.Location = new System.Drawing.Point(0, 428);
            this.layoutControlGroupIcon.Name = "layoutControlGroupIcon";
            this.layoutControlGroupIcon.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupIcon.Size = new System.Drawing.Size(512, 140);
            this.layoutControlGroupIcon.Text = "layoutControlGroupIcon";
            // 
            // buttonEx1item
            // 
            this.buttonEx1item.Control = this.buttonEx1;
            this.buttonEx1item.CustomizationFormText = "buttonEx1item";
            this.buttonEx1item.Location = new System.Drawing.Point(0, 34);
            this.buttonEx1item.MaxSize = new System.Drawing.Size(95, 42);
            this.buttonEx1item.MinSize = new System.Drawing.Size(95, 42);
            this.buttonEx1item.Name = "buttonEx1item";
            this.buttonEx1item.Size = new System.Drawing.Size(95, 42);
            this.buttonEx1item.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.buttonEx1item.Text = "buttonEx1item";
            this.buttonEx1item.TextSize = new System.Drawing.Size(0, 0);
            this.buttonEx1item.TextToControlDistance = 0;
            this.buttonEx1item.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(95, 34);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 76);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(95, 34);
            this.emptySpaceItem5.Text = "emptySpaceItem5";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupPreview
            // 
            this.layoutControlGroupPreview.CustomizationFormText = "layoutControlGroupPreview";
            this.layoutControlGroupPreview.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.iconPictureBoxitem});
            this.layoutControlGroupPreview.Location = new System.Drawing.Point(95, 0);
            this.layoutControlGroupPreview.Name = "layoutControlGroupPreview";
            this.layoutControlGroupPreview.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupPreview.Size = new System.Drawing.Size(407, 110);
            this.layoutControlGroupPreview.Text = "layoutControlGroupPreview";
            // 
            // iconPictureBoxitem
            // 
            this.iconPictureBoxitem.Control = this.iconPictureBox;
            this.iconPictureBoxitem.CustomizationFormText = "iconPictureBoxitem";
            this.iconPictureBoxitem.Location = new System.Drawing.Point(0, 0);
            this.iconPictureBoxitem.MinSize = new System.Drawing.Size(111, 31);
            this.iconPictureBoxitem.Name = "iconPictureBoxitem";
            this.iconPictureBoxitem.Size = new System.Drawing.Size(397, 80);
            this.iconPictureBoxitem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.iconPictureBoxitem.Text = "iconPictureBoxitem";
            this.iconPictureBoxitem.TextSize = new System.Drawing.Size(0, 0);
            this.iconPictureBoxitem.TextToControlDistance = 0;
            this.iconPictureBoxitem.TextVisible = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(258, 23);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(26, 23);
            this.button1.TabIndex = 4;
            // 
            // UnitTypeForm
            // 
            this.AcceptButton = this.buttonOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(512, 604);
            this.Controls.Add(this.UnitTypeFormConvertedLayout);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(520, 605);
            this.Name = "UnitTypeForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "UnitTypeForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.UnitTypeForm_FormClosing);
            this.Load += new System.EventHandler(this.UnitTypeForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.UnitTypeFormConvertedLayout)).EndInit();
            this.UnitTypeFormConvertedLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.iconPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSeats)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemVelocity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemUnitType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupIncidentType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxNotAssigneditem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxAssigneditem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonAddIncidentTypeitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonAddAllIncidentTypeitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonRemoveIncidentTypeitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonRemoveAllIncidentTypeitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEx1item)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupPreview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconPictureBoxitem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

		private DevExpress.XtraEditors.SimpleButton buttonCancel;
        private DevExpress.XtraEditors.SimpleButton buttonOk;
        private System.Windows.Forms.ListBox listBoxAssigned;
        private System.Windows.Forms.ListBox listBoxNotAssigned;
        private System.Windows.Forms.ComboBox comboBoxDepartmentType;
		private DevExpress.XtraEditors.SimpleButton button1;
        private TextBoxEx textBoxVelocity;
        private TextBoxEx textBoxSeats;
        private System.Windows.Forms.PictureBox iconPictureBox;
        private System.Windows.Forms.ToolTip toolTip1;
        private DevExpress.XtraEditors.SimpleButton buttonEx1;
        private TextBoxEx textBoxName;
        private TextBoxEx textBoxDescription;
		private DevExpress.XtraEditors.SimpleButton buttonAddIncidentType;
		private DevExpress.XtraEditors.SimpleButton buttonAddAllIncidentType;
		private DevExpress.XtraEditors.SimpleButton buttonRemoveIncidentType;
		private DevExpress.XtraEditors.SimpleButton buttonRemoveAllIncidentType;
        private DevExpress.XtraLayout.LayoutControl UnitTypeFormConvertedLayout;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupUnit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemSeats;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemVelocity;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDepartment;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDes;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemUnitType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem buttonEx1item;
        private DevExpress.XtraLayout.LayoutControlItem iconPictureBoxitem;
        private DevExpress.XtraLayout.LayoutControlItem listBoxAssigneditem;
        private DevExpress.XtraLayout.LayoutControlItem listBoxNotAssigneditem;
        private DevExpress.XtraLayout.LayoutControlItem buttonAddIncidentTypeitem;
        private DevExpress.XtraLayout.LayoutControlItem buttonAddAllIncidentTypeitem;
        private DevExpress.XtraLayout.LayoutControlItem buttonRemoveIncidentTypeitem;
        private DevExpress.XtraLayout.LayoutControlItem buttonRemoveAllIncidentTypeitem;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupIncidentType;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupIcon;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupPreview;
    }
}