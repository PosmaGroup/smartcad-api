using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;

using SmartCadCore.Core;
using System.ServiceModel;
using System.Reflection;
using Smartmatic.SmartCad.Service;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadCore.Enums;
using Iesi.Collections;

namespace SmartCadGuiCommon
{
    public partial class ZoneForm : DevExpress.XtraEditors.XtraForm
    {
        CctvZoneClientData selectedZone = null;
        FormBehavior behaviorType = FormBehavior.Edit;
        AdministrationRibbonForm parentForm;
        object syncCommitted = new object();

        public ZoneForm()
        {
            InitializeComponent();
            LoadLanguage();
            FillAllControls();
        }

        public ZoneForm(AdministrationRibbonForm form, CctvZoneClientData zone, FormBehavior type)
            :this()
		{
            FormClosing += new FormClosingEventHandler(ZoneForm_FormClosing);
            this.parentForm = form;
            this.parentForm.AdministrationCommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(ZoneForm_AdministrationCommittedChanges);

            BehaviorType = type;
			SelectedZone = zone;
		}

        void ZoneForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.parentForm != null)
            {
                this.parentForm.AdministrationCommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(ZoneForm_AdministrationCommittedChanges);
            }
        }

        void ZoneForm_AdministrationCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    lock (syncCommitted)
                    {
                        try
                        {
                            if (ServerServiceClient.GetInstance().OperatorClient == null)
                            { }
                        }
                        catch
                        { }

                        if (e.Objects[0] is CctvZoneClientData)
                        {
                            #region ZoneClientData
                            CctvZoneClientData zone =
                                e.Objects[0] as CctvZoneClientData;

                            if (SelectedZone != null && SelectedZone.Equals(zone) && ButtonOkPressed == false)
                            {
                                if (e.Action == CommittedDataAction.Update)
                                {
                                    MessageForm.Show(ResourceLoader.GetString2("UpdateFormCctvZoneData"), MessageFormType.Warning);
                                    SelectedZone = zone;
                                }
                                else if (e .Action == CommittedDataAction.Delete)
                                {
                                    MessageForm.Show(ResourceLoader.GetString2("DeleteFormCctvZoneData"), MessageFormType.Warning);
                                    FormUtil.InvokeRequired(this,
                                    delegate
                                    {
                                        this.Close();
                                    });
                                }

                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        public FormBehavior BehaviorType
        {
            get
            {
                return behaviorType;
            }
            set
            {
                behaviorType = value;
            }
        }

        public CctvZoneClientData SelectedZone
        {
            get
            {
                return selectedZone;
            }
            set
            {
                Clear();
                selectedZone = value;
                if (selectedZone != null)
                {
                    this.listBoxAssigned.Items.Clear();
                    this.listBoxNotAssigned.Items.Clear();
                    textBoxExName.Text = selectedZone.Name;
                    selectedZone.Structs = ServerServiceClient.GetInstance().SearchClientObjects(
                                            SmartCadHqls.GetCustomHql(
                                                SmartCadHqls.GetStructsByCctvZoneCode, selectedZone.Code), false);
                    foreach (StructClientData str in selectedZone.Structs)
                    {
                        listBoxAssigned.Items.Add(str);
                    }
                    FillListBoxNotAssigned();
                }
                ZoneStatusParameters_Change(null, null);
            }
        }


        private void LoadLanguage() 
        {            

            this.layoutControlGroupInf.Text = ResourceLoader.GetString2("ZoneCCTVInformation");
            this.layoutControlGroupStruct.Text = ResourceLoader.GetString2("Structs");
            this.layoutControlItemAvailables.Text = ResourceLoader.GetString2("StructsAvailables");
            this.layoutControlItemName.Text = ResourceLoader.GetString2("Name") + ":*";
            this.layoutControlItemType.Text = ResourceLoader.GetString2("StructType")+ ":";
            this.layoutControlItemUnAvailables.Text = ResourceLoader.GetString2("StructsUnAvailables");
                  
        }

        private void FillAllControls()
        {
            FillComboBoxStructTypes();
            FillListBoxNotAssigned();
        }

        private void FillComboBoxStructTypes()
        {
            IList structTypes = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetAllStructTypes);
            foreach (StructTypeClientData structType in structTypes)
            {
                comboBoxExStructTypes.Items.Add(structType);
            }
            comboBoxExStructTypes.Items.Add(ResourceLoader.GetString2("All"));
            comboBoxExStructTypes.SelectedItem = ResourceLoader.GetString2("All");
        }

        private void FillListBoxNotAssigned()
        {
            listBoxNotAssigned.Items.Clear();

            IList structs = new ArrayList();
            if (comboBoxExStructTypes.Text == ResourceLoader.GetString2("All"))
            {
                structs = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetAllFreeStructs);
            }
            else
            {
                structs = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetFreeStructsByType,comboBoxExStructTypes.Text));
            }
            foreach (StructClientData myStruct in structs)
            {
                if (listBoxAssigned.Items.Contains(myStruct) == false)
                    listBoxNotAssigned.Items.Add(myStruct);
            }
            if (listBoxNotAssigned.Items.Count > 0)
            {
                buttonAdd.Enabled = true;
            }
        }

        private void Clear()
        {
            textBoxExName.Clear();
            RemoveAll();
        }

        private void ZoneStatusParameters_Change(object sender, System.EventArgs e)
        {
            if ((textBoxExName.Text.Trim() == string.Empty))
                buttonOk.Enabled = false;
            else
                buttonOk.Enabled = true;
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            ArrayList toDelete = new ArrayList();
            foreach (StructClientData myStruct in listBoxNotAssigned.SelectedItems)
            {
                toDelete.Add(myStruct);
                listBoxAssigned.Items.Add(myStruct);
            }
            foreach (StructClientData myStruct in toDelete)
            {
                int index = -1;
                int i = 0;
                foreach (StructClientData myStruct2 in listBoxNotAssigned.Items)
                {
                    if (myStruct2.Code == myStruct.Code)
                    {
                        index = i;
                        break;
                    }
                    i++;
                }
                if (index != -1)
                    listBoxNotAssigned.Items.RemoveAt(index);
            }

            ZoneStatusParameters_Change(null, null);
        }

        private void AddAll()
        {
            foreach (StructClientData myStruct in listBoxNotAssigned.Items)
            {
                listBoxAssigned.Items.Add(myStruct);
            }
            listBoxNotAssigned.Items.Clear();
            ZoneStatusParameters_Change(null, null);
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            ArrayList toDelete = new ArrayList();
            foreach (StructClientData myStruct in listBoxAssigned.SelectedItems)
            {
                toDelete.Add(myStruct);
               if (comboBoxExStructTypes.Text == ResourceLoader.GetString2("All") ||
                    comboBoxExStructTypes.Text == myStruct.Type.Name)
                listBoxNotAssigned.Items.Add(myStruct);
            }
            foreach (StructClientData myStruct in toDelete)
            {
                int index = -1;
                int i = 0;
                foreach (StructClientData myStruct2 in listBoxAssigned.Items)
                {
                    if (myStruct.Code == myStruct2.Code)
                    {
                        index = i;
                        break;
                    }
                    i++;
                }
                if (index != -1)
                    listBoxAssigned.Items.RemoveAt(index);
            }
            ZoneStatusParameters_Change(null, null);
        }

        private void RemoveAll()
        {
            foreach (StructClientData myStruct in listBoxAssigned.Items)
            {
                listBoxNotAssigned.Items.Add(myStruct);
            }
            listBoxAssigned.Items.Clear();
            ZoneStatusParameters_Change(null, null);
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[1] { GetType().GetMethod("buttonOk_Click1", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[0] });
                processForm.CanThrowError = true;
                processForm.ShowDialog();
            }
            catch (FaultException ex)
            {
                if (behaviorType == FormBehavior.Edit)
                {
                    SelectedZone = (CctvZoneClientData)ServerServiceClient.GetInstance().RefreshClient(SelectedZone);
                }
                GetErrorFocus(ex);
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, MessageFormType.Information);
                buttonOkPressed = false;
            }
            catch (Exception ex)
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, ex);
                buttonOkPressed = false;
            }
        }


        private void GetErrorFocus(FaultException ex)
        {
            if (ex.Message.Contains(ResourceLoader.GetString2("UK_CCTV_ZONE_CUSTOM_CODE")))
            {
                if (behaviorType == FormBehavior.Create)
                    textBoxExName.Text = this.textBoxExName.Text;

                textBoxExName.Focus();
            }
        }
      
        private bool buttonOkPressed = false;

        public bool ButtonOkPressed
        {
            get { return buttonOkPressed; }
        }

        private void buttonOk_Click1()
        {
            buttonOkPressed = true;
            if (behaviorType == FormBehavior.Create)
            {
                selectedZone = new CctvZoneClientData();
            }
            if (selectedZone != null)
            {
                FormUtil.InvokeRequired(this,
                    delegate
                    {
                        selectedZone.Name = textBoxExName.Text;
                        IList newStructs = GetStructs();
                        if (selectedZone.Structs != null)
                        {
                            ISet structs = new HashedSet(selectedZone.Structs);
                            foreach (StructClientData myStruct in structs)
                            {
                                if (!newStructs.Contains(myStruct))
                                {
                                    selectedZone.Structs.Remove(myStruct);
                                }
                            }
                        }
                        selectedZone.Structs = newStructs;
                    });
                ServerServiceClient.GetInstance().SaveOrUpdateClientData(selectedZone);
            }
        }

        private IList GetStructs()
        {
            IList structs = new ArrayList();
            StructClientData myStruct = new StructClientData();

            foreach (StructClientData myStruct2 in listBoxAssigned.Items)
            {
                myStruct = myStruct2;
                myStruct.CctvZone = selectedZone;
                structs.Add(myStruct);
            }
            return structs;
        }

        private void comboBoxExStructTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillListBoxNotAssigned();
        }               

        private void listBoxNotAssigned_DoubleClick(object sender, EventArgs e)
        {
            buttonAdd_Click(null, null);
        }

        private void listBoxAssigned_DoubleClick(object sender, EventArgs e)
        {
            buttonRemove_Click(null, null);
        }

        private void listBoxNotAssigned_DragDrop(object sender, DragEventArgs e)
        {
            buttonAdd_Click(null, null);
        }

        private void listBoxAssigned_DragDrop(object sender, DragEventArgs e)
        {
            buttonRemove_Click(null, null);
        }

        private void buttonRemoveAll_Click(object sender, EventArgs e)
        {
            RemoveAll();
        }

        private void buttonAddAll_Click(object sender, EventArgs e)
        {
            AddAll();
        }

        private void ZoneForm_Load(object sender, EventArgs e)
        {
            this.Icon = ResourceLoader.GetIcon("$Icon.CctvZone");
            if (behaviorType == FormBehavior.Create)
            {
                Text = ResourceLoader.GetString2("ZoneFormCreateText");
                buttonOk.Text = ResourceLoader.GetString2("ZoneFormCreateButtonOkText");
                buttonCancel.Text = ResourceLoader.GetString2("ZoneFormCreateButtonCancelText");
            }
            else if (behaviorType == FormBehavior.Edit)
            {
                Text = ResourceLoader.GetString2("ZoneFormEditText");
                buttonOk.Text = ResourceLoader.GetString2("ZoneFormEditButtonOkText");
                buttonCancel.Text = ResourceLoader.GetString2("ZoneFormEditButtonCancelText");
            }
        }

    }
}