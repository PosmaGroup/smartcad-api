using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using SmartCadCore.Core;
using System.Reflection;
using Smartmatic.SmartCad.Service;
using System.ServiceModel;
using System.Collections;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using DevExpress.Data;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors.Controls;
using SmartCadCore.Enums;
using SmartCadControls;
using SmartCadGuiCommon.Administration;

namespace SmartCadGuiCommon
{

    public partial class DataloggerForm : DevExpress.XtraEditors.XtraForm
    {
        private FormBehavior behavior;
        private bool buttonOkPressed = false;
        private DataloggerClientData selectedDatalogger;
        private CctvZoneClientData cctvZoneSelected;
        private StructClientData structSelected;
        private AdministrationRibbonForm parentForm;
        private object syncCommitted = new object();
        private BindingList<SensorTelemetryGridDataAdm> deviceList;

        private class WrapperStructData
        {
            public StructClientData Struct { get; set; }

            public override string ToString()
            {
                return this.Struct.Name;
            }

            public WrapperStructData(StructClientData strc)
            {
                this.Struct = strc;
            }

            public override bool Equals(object obj)
            {
                WrapperStructData objAux = obj as WrapperStructData;
                if (objAux != null)
                {
                    return objAux.Struct.Code == this.Struct.Code;
                }
                return obj.Equals(this);
            }
        }

        private void ShowData()
        {
            deviceList.AllowNew = true;
            deviceList.AllowEdit = true;
            deviceList.AllowRemove = true;
            this.gridControlSensor.DataSource = deviceList;
        }

        public DataloggerClientData SelectedDatalogger
        {
            get
            {
                return selectedDatalogger;
            }
            set
            {
                selectedDatalogger = value;
                if (Behavior == FormBehavior.Edit)
                {
                    textBoxExxDataloggerName.Text = selectedDatalogger.Name;
                    textEditCustomCode.Text = selectedDatalogger.CustomCode.ToString();
                    if (selectedDatalogger.StructClientData != null)
                    {
                        comboBoxExCctvZone.SelectedItem = selectedDatalogger.StructClientData.CctvZone;
                        comboBoxExStructType.SelectedItem = selectedDatalogger.StructClientData.Type;
                        WrapperStructData aux = new WrapperStructData(selectedDatalogger.StructClientData);
                        comboBoxExStructName.SelectedItem = aux;
                    }
                }
                buttonEnable();
            }
        }
        public bool ButtonOkPressed
        {
            get
            {
                return buttonOkPressed;
            }
        }
        public FormBehavior Behavior
        {
            set
            {
                behavior = value;
            }
            get
            {
                return behavior;
            }
        }

        public DataloggerForm(AdministrationRibbonForm form, DataloggerClientData SensorTelemetry, FormBehavior behavior)
            : this()
        {
            FormClosing += new FormClosingEventHandler(SensorTelemetryForm_FormClosing);
            this.parentForm = form;
            this.parentForm.AdministrationCommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(SensorTelemetryForm_AdministrationCommittedChanges);


            //FillVariablesComboBox();
            Behavior = behavior;
            FillCctvZones();
            SelectedDatalogger = SensorTelemetry;
            FillSensors();
        }
        public DataloggerForm()
        {
            InitializeComponent();
            LoadLanguage();
            deviceList = new BindingList<SensorTelemetryGridDataAdm>();
            ShowData();
            CurrencyDataController.DisableThreadingProblemsDetection = true;

            this.gridViewSensor.InvalidValueException += new DevExpress.XtraEditors.Controls.InvalidValueExceptionEventHandler(gridViewSensor_InvalidValueException);
            this.gridViewSensor.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(gridViewSensor_ValidateRow);
            this.gridViewSensor.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(gridViewSensor_InvalidRowException);
            this.gridViewSensor.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(gridViewSensor_CellValueChanged);
            this.gridViewSensor.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(gridViewSensor_CellValueChanging);
        }

        void gridViewSensor_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column == gridColumnSensor && e.RowHandle == GridControlEx.NewItemRowHandle)
            { 
                
            }
        }

        void gridViewSensor_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column == gridColumnSensor && e.RowHandle == GridControlEx.NewItemRowHandle)
            {
                foreach (SensorTelemetryGridDataAdm gridData in deviceList)
                {
                    if (gridData.Sensor == e.Value.ToString())
                    {
                        gridViewSensor.SetFocusedRowCellValue(gridColumnVariable, gridData.Variable);
                    }
                }
            }
        }

        //void FillVariablesComboBox() 
        //{
        //    RepositoryItemLookUpEdit myLookup = new RepositoryItemLookUpEdit();
        //    myLookup.NullText = "";
        //    myLookup.DisplayMember = "Key";
        //    myLookup.ValueMember = "Value";
        //    myLookup.Columns.Add(new LookUpColumnInfo("Key", 0, "Variables"));
        //    List<ComboboxColumnHelper> list = new List<ComboboxColumnHelper>();
        //    list.Add(new ComboboxColumnHelper() { Key = "rain_mm", Value = "rain_mm" });
        //    list.Add(new ComboboxColumnHelper() { Key = "Battery", Value = "Battery" });
        //    list.Add(new ComboboxColumnHelper() { Key = "DT", Value = "DT" });
        //    myLookup.DataSource = list;
        //    gridColumnVariable.ColumnEdit = myLookup; 
        //}

        void gridViewSensor_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
        {
            e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
        }

        void gridViewSensor_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            if (gridViewSensor.FocusedRowHandle != GridControlEx.AutoFilterRowHandle)
            { 
                SensorTelemetryGridDataAdm gridData = (SensorTelemetryGridDataAdm)e.Row;

                if (gridData.Sensor == string.Empty)
                {
                    e.Valid = false;
                }
                if (gridData.Variable == string.Empty)
                {
                    e.Valid = false;
                }
                if ((gridData.Low == -1 && gridData.High == -1) || gridData.Low == gridData.High)
                {
                    e.Valid = false;
                    gridViewSensor.SetColumnError(gridColumnLow, "At least one value should be different");
                    gridViewSensor.SetColumnError(gridColumnHigh, "At least one value should be different");
                }
                else if (gridData.Low < -1)
                {
                    e.Valid = false;
                    gridViewSensor.SetColumnError(gridColumnLow, "The values should be higher or equal to -1");
                }
                else if (gridData.High < -1)
                {
                    e.Valid = false;
                    gridViewSensor.SetColumnError(gridColumnHigh, "The values should be higher or equal to -1");
                }
                else if (gridData.Low > gridData.High && gridData.Low > -1 && gridData.High > -1)
                {
                    e.Valid = false;
                    gridViewSensor.SetColumnError(gridColumnLow, "This value cannot be higher than the High value");
                }

                if (e.Valid)
                {
                    SensorTelemetryGridDataAdm sensorAux = null;
                    bool found = false;
                    foreach (SensorTelemetryGridDataAdm sensor in deviceList)
                    {
                        if (gridData.Equals(sensor.SensorTelemetryThreshold.SensorTelemetry))
                        {
                            sensorAux = sensor;
                            foreach (SensorTelemetryThresholdClientData threshold in sensor.SensorTelemetryThreshold.SensorTelemetry.Threshold)
                            {
                                if (gridData.Equals(new SensorTelemetryGridDataAdm(threshold)) == true)
                                {
                                    found = true;
                                }
                            }
                            break;
                        }
                    }
                    if (found == false)
                    {
                        gridData.SensorTelemetryThreshold.SensorTelemetry = sensorAux.SensorTelemetryThreshold.SensorTelemetry;
                        gridData.SensorTelemetryThreshold.SensorTelemetry.Threshold.Add(gridData.SensorTelemetryThreshold);
                    }
                    if (repositoryItemComboBoxSensor.Items.Contains(gridData.Sensor) == false)
                        repositoryItemComboBoxSensor.Items.Add(gridData.Sensor);
                }
            }
        }

        private void gridViewSensor_InvalidValueException(object sender, DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs e)
        {
            try
            {
                //throw new FaultException(ResourceLoader.GetString2("BoundsCharError"));
            }

            catch (FaultException ex)
            {
                if (behavior == FormBehavior.Edit)
                    SelectedDatalogger = (DataloggerClientData)ServerServiceClient.GetInstance().RefreshClient(selectedDatalogger);

                GetErrorFocus(ex);
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, MessageFormType.Information);
                buttonOkPressed = false;
            }
        }

        void SensorTelemetryForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.parentForm != null)
            {
                this.parentForm.AdministrationCommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(SensorTelemetryForm_AdministrationCommittedChanges);
            }
        }

        void SensorTelemetryForm_AdministrationCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    lock (syncCommitted)
                    {
                        try
                        {
                            if (ServerServiceClient.GetInstance().OperatorClient == null)
                            {
                            }
                        }
                        catch
                        {
                        }

                        if (e.Objects[0] is DataloggerClientData)
                        {
                            #region EvaluationClientData
                            DataloggerClientData SensorTelemetry =
                            e.Objects[0] as DataloggerClientData;

                            if (SelectedDatalogger != null && SelectedDatalogger.Equals(SensorTelemetry) && ButtonOkPressed == false)
                            {
                                if (e.Action == CommittedDataAction.Update)
                                {
                                    MessageForm.Show(ResourceLoader.GetString2("UpdateFormSensorTelemetryData"), MessageFormType.Warning);
                                    SelectedDatalogger = SensorTelemetry;
                                }
                                else
                                    if (e.Action == CommittedDataAction.Delete)
                                    {
                                        MessageForm.Show(ResourceLoader.GetString2("DeleteFormSensorTelemetryData"), MessageFormType.Warning);
                                        FormUtil.InvokeRequired(this,
                                        delegate
                                        {
                                            this.Close();
                                        });
                                    }
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private void LoadLanguage()
        {
            this.Icon = ResourceLoader.GetIcon("$Icon.SensorTelemetry");
            this.layoutControlGroupSensorTelemetryInf.Text = ResourceLoader.GetString2("DataloggerInformation");
            this.layoutControlGroupUbication.Text = ResourceLoader.GetString2("Ubication");
            this.textBoxExDataloggerNameitem.Text = ResourceLoader.GetString2("Name") + ":*";
            this.comboBoxExCctvZoneitem.Text = ResourceLoader.GetString2("CCTVZone") + ":";
            this.comboBoxExStructTypeitem.Text = ResourceLoader.GetString2("StructType") + ":";
            this.comboBoxExStructNameitem.Text = ResourceLoader.GetString2("Struct") + ":";
            this.gridColumnSensor.Caption = ResourceLoader.GetString2("Sensor");
            this.gridColumnVariable.Caption = ResourceLoader.GetString2("Variable");
            this.gridColumnHigh.Caption = ResourceLoader.GetString2("SensorTelemetryUpThreshold");
            this.gridColumnLow.Caption = ResourceLoader.GetString2("SensorTelemetryDownThreshold");
            this.layoutControlItemGridSensor.Text = ResourceLoader.GetString2("TelemetrySensorInformation");
            this.layoutControlItemCustomCode.Text = ResourceLoader.GetString2("CustomCode") + ":*";
        }


        private void buttonExAccept_Click(object sender, EventArgs e)
        {
            try
            {
                buttonOkPressed = true;
                if (behavior == FormBehavior.Create)
                {
                    selectedDatalogger = new DataloggerClientData();
                }                
                selectedDatalogger.Name = textBoxExxDataloggerName.Text;
                selectedDatalogger.CustomCode = int.Parse(textEditCustomCode.Text);
                selectedDatalogger.SensorTelemetrys = new List<SensorTelemetryClientData>();
                if (structSelected != null)
                {
                    selectedDatalogger.StructClientData = structSelected;
                    selectedDatalogger.StructClientData.Type = structSelected.Type;
                    selectedDatalogger.StructClientData.CctvZone = structSelected.CctvZone;
                }
                else
                {
                    selectedDatalogger.StructClientData = null;
                }
                if (deviceList.Count > 0)
                {
                    foreach (SensorTelemetryGridDataAdm gridData in deviceList)
                    {
                        if (selectedDatalogger.SensorTelemetrys.Contains(gridData.SensorTelemetryThreshold.SensorTelemetry) == false)
                            selectedDatalogger.SensorTelemetrys.Add(gridData.SensorTelemetryThreshold.SensorTelemetry);
                    }
                }
                ServerServiceClient.GetInstance().SaveOrUpdateClientData(selectedDatalogger);
            }
            catch (FaultException ex)
            {
                if (behavior == FormBehavior.Edit)
                    SelectedDatalogger = (DataloggerClientData)ServerServiceClient.GetInstance().RefreshClient(selectedDatalogger);

                GetErrorFocus(ex);
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, MessageFormType.Information);
                buttonOkPressed = false;
            }
            catch (Exception ex)
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.InnerException.Message, ex.InnerException);
                buttonOkPressed = false;
            }
        }
        private void buttonExCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void GetErrorFocus(FaultException ex)
        {
            if (ex.Message.Contains(ResourceLoader.GetString2("UK_SENSOR_CUSTOM_CODE")))
            {
                if (behavior == FormBehavior.Create)
                    textBoxExxDataloggerName.Text = textBoxExxDataloggerName.Text;
                textBoxExxDataloggerName.Focus();
            }
        }


        private void buttonEnable()
        {
            if (textBoxExxDataloggerName.Text.Trim() == string.Empty ||
                deviceList.Count < 0 ||
                int.Parse(textEditCustomCode.Text) <= 0)
                buttonExAccept.Enabled = false;
            else
                buttonExAccept.Enabled = true;
        }
        private void FillCctvZones()
        {
            foreach (CctvZoneClientData cctvZone in ServerServiceClient.GetInstance().SearchClientObjects(typeof(CctvZoneClientData)))
            {
                comboBoxExCctvZone.Items.Add(cctvZone);
            }

            comboBoxExCctvZone.Items.Insert(0, new CctvZoneClientData() { Name = string.Empty });
        }
        private void comboBoxExCctvZone_SelectedValueChanged(object sender, EventArgs e)
        {
            if (comboBoxExCctvZone.SelectedIndex > 0)
            {
                cctvZoneSelected = (CctvZoneClientData)(sender as ComboBox).SelectedItem;
                FillStructTypes();
            }
            else
                if (comboBoxExCctvZone.SelectedIndex == 0)
                {
                    comboBoxExStructType.SelectedIndex = -1;
                    comboBoxExStructType.Items.Clear();
                    comboBoxExStructName.SelectedIndex = -1;
                }
        }

        private void FillSensors()
        {
            if (selectedDatalogger != null)
            {
                repositoryItemComboBoxSensor.Items.Clear();
                foreach (SensorTelemetryClientData sen in selectedDatalogger.SensorTelemetrys)
                {
                    foreach (SensorTelemetryThresholdClientData threshol in sen.Threshold)
                    {
                        SensorTelemetryGridDataAdm sensorGrid = new SensorTelemetryGridDataAdm(threshol);
                        deviceList.Add(sensorGrid);
                        if (repositoryItemComboBoxSensor.Items.Contains(sensorGrid.Sensor) == false)
                            repositoryItemComboBoxSensor.Items.Add(sensorGrid.Sensor);
                    }
                }
            }
        }

        private void FillStructTypes()
        {
            comboBoxExStructType.SelectedIndex = -1;
            comboBoxExStructType.Items.Clear();
            IList structTypes = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetAllStructTypes);
            foreach (StructTypeClientData structType in structTypes)
            {
                comboBoxExStructType.Items.Add(structType);
            }
        }

        private void comboBoxExStructType_SelectedValueChanged(object sender, EventArgs e)
        {
            comboBoxExStructName.SelectedIndex = -1;
            comboBoxExStructName.Items.Clear();
            if (comboBoxExStructType.SelectedItem != null)
            {
                StructTypeClientData structType = (StructTypeClientData)(sender as ComboBox).SelectedItem;
                IList structs = ServerServiceClient.GetInstance().SearchClientObjects(
                SmartCadHqls.GetCustomHql(SmartCadHqls.GetStructByStructTypeCodeZoneCode, structType.Code, cctvZoneSelected.Code));
                foreach (StructClientData structdata in structs)
                {
                    comboBoxExStructName.Items.Add(new WrapperStructData(structdata));
                }
            }
        }
        private void comboBoxExStructName_SelectedValueChanged(object sender, EventArgs e)
        {
            ComboBox combo = sender as ComboBox;

            if (combo.SelectedItem != null)
                structSelected = ((WrapperStructData)(sender as ComboBox).SelectedItem).Struct;
            else
                structSelected = null;

            buttonEnable();
        }

        private void SensorTelemetryForm_Load(object sender, EventArgs e)
        {
            this.Width = 569;
            this.Height = 620;
            if (behavior == FormBehavior.Create)
            {
                this.Text = ResourceLoader.GetString2("DataloggerCreateText");
                buttonExAccept.Text = ResourceLoader.GetString2("SensorTelemetryFormCreateButtonOkText");
                buttonExCancel.Text = ResourceLoader.GetString2("$Message.Cancel");
            }
            else
                if (behavior == FormBehavior.Edit)
                {
                    this.Text = ResourceLoader.GetString2("DataloggerFormEditText");
                    buttonExAccept.Text = ResourceLoader.GetString2("SensorTelemetryFormEditButtonOkText");
                    buttonExCancel.Text = ResourceLoader.GetString2("$Message.Cancel");
                }
            gridControlSensor.EnableAutoFilter = true;
            gridColumnSensor.GroupIndex = 0;
        }

        private void repositoryItemButtonEditDelete_Click(object sender, EventArgs e)
        {
            if (gridViewSensor.GetFocusedRow() != null)
            {
                SensorTelemetryGridDataAdm gridData = (SensorTelemetryGridDataAdm)gridViewSensor.GetFocusedRow();
                DeleteSensorTelemetryThreshold(gridData);

                gridViewSensor.DeleteSelectedRows();

                buttonEnable();
            }
        }

        private void DeleteSensorTelemetryThreshold(SensorTelemetryGridDataAdm gridData)
        {
            if (gridData.SensorTelemetryThreshold.SensorTelemetry.Threshold.Count > 1 &&
                gridViewSensor.FocusedRowHandle != GridControlEx.NewItemRowHandle)
            {
                gridData.SensorTelemetryThreshold.SensorTelemetry.Threshold.Remove(gridData.SensorTelemetryThreshold);
            }
            else if (gridData.SensorTelemetryThreshold.SensorTelemetry.Threshold.Count == 1)
            {
                repositoryItemComboBoxSensor.Items.Remove(gridData.Sensor);
            }
        }

        private void gridViewSensor_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle == GridControlEx.NewItemRowHandle)
            {
                gridColumnSensor.OptionsColumn.AllowEdit = true;
                gridColumnVariable.OptionsColumn.AllowEdit = true;
            }
            else
            {
                gridColumnSensor.OptionsColumn.AllowEdit = false;
                gridColumnVariable.OptionsColumn.AllowEdit = false;
            }
        }

        private void textBoxExxDataloggerName_TextChanged(object sender, EventArgs e)
        {
            buttonEnable();
        }
    }
}
