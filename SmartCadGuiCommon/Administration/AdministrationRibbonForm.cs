﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Smartmatic.SmartCad.Service;
using DevExpress.Utils;
using System.Collections;
using DevExpress.XtraBars;
using System.ServiceModel;
using System.Reflection;
using DevExpress.XtraLayout.Utils;
using SmartCadCore.Common;
using SmartCadCore.ClientData;
using SmartCadControls;
using SmartCadCore.Core;
using SmartCadGuiCommon;
using SmartCadControls.Controls;
using SmartCadControls.SyncBoxes;
using SmartCadGuiCommon.Util;
using SmartCadGuiCommon.Controls;
using SmartCadCore.ClientData.Util;

namespace SmartCadGuiCommon
{
    public partial class AdministrationRibbonForm : Form
    {
        private DateTime syncServerDateTime;
        private System.Threading.Timer globalTimer;
        private ServerServiceClient serverServiceClient;
        private static string password;
        private readonly int GLOBAL_TIMER_PERIOD;
        private readonly TimeSpan ADD_TIMESPAN;
        private string labelUserCaption = "";
        private string labelConnectedCaption = "";
        Dictionary<string, ApplicationPreferenceClientData> globalApplicationPreferences;
        private GridControlSynBox syncBox;        
        private TimeSpan sinceLoggedIn;
        private IList accesses = null;
        private string userPassword;
        private ConfigurationClientData configuration;


        public GridControlSynBox SyncBox
        {
            get
            {
                return syncBox;
            }
        }

        public GridControlEx GridControl
        {
            get
            {
                return gridControlExAdministration;
            }
        }

        public GridViewEx GridView
        {
            get
            {
                return gridViewExAdministration;
            }
        }

        public ConfigurationClientData Configuration
        {
            get { return configuration; }
        }

        public string UserPassword
        {
            get
            {
                return userPassword;
            }
            set
            {
                userPassword = value;                
            }
        }
        public event EventHandler<CommittedObjectDataCollectionEventArgs> AdministrationCommittedChanges;
        
        public ServerServiceClient ServerServiceClient
        {
            get
            {
                return serverServiceClient;
            }
            set
            {
                serverServiceClient = value;
            }
        }

        public static string Password
        {
            get
            {
                return password;
            }

            set
            {
                password = value;
            }
        }

        public AdministrationRibbonForm()
        {
            InitializeComponent();
            this.labelLogo.BackColor = Color.FromArgb(191, 219, 255);
            LoadGlobalApplicationPreferences();
            #region ReadOnly values from ApplicationPreferences
            GLOBAL_TIMER_PERIOD = ApplicationGuiUtil.GLOBAL_TIME_PERIOD;
            ADD_TIMESPAN = new TimeSpan(0, 0, 0, 0, GLOBAL_TIMER_PERIOD);
            #endregion            
        }

        private void AdministrationRibbonForm_Load(object sender, EventArgs e)
        {
            LoadLanguage();
            configuration = ServerServiceClient.GetInstance().GetConfiguration();
            syncServerDateTime = ServerServiceClient.GetInstance().GetTime();
            labelDate.Caption = ApplicationUtil.GetFormattedDate(syncServerDateTime);
            labelConnected.Caption = labelConnectedCaption + "00:00";

            globalTimer = ApplicationUtil.RecurrentExecuteMethod(
                new SimpleDelegate(
                delegate
                {
                    timer_Tick(null, null);
                }), null, null, GLOBAL_TIMER_PERIOD, GLOBAL_TIMER_PERIOD);

            ServerServiceClient.CommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(ServerServiceClient_CommittedChanges);
            gridViewExAdministration.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(gridViewExAdministration_FocusedRowChanged);

            GridControlDataAdm.AdministrationForm = this;

            #region SyncBox Initialization
            syncBox = new GridControlSynBox(gridControlExAdministration);
            
            #endregion

            this.labelUser.Caption = labelUserCaption + this.serverServiceClient.OperatorClient.FirstName
                + " " + this.serverServiceClient.OperatorClient.LastName;
            //RibbonControl.SelectedPage = ribbonPageUserAccount;
            gridControlExAdministration.MouseDoubleClick += new MouseEventHandler(gridControlExAdministration_MouseDoubleClick);
            gridControlExAdministration.KeyPress += new KeyPressEventHandler(gridControlExAdministration_KeyPress);
            gridViewExAdministration.OptionsView.ShowDetailButtons = false;
            gridViewExAdministration.OptionsSelection.MultiSelect = false;
            gridViewExAdministration.OptionsView.ShowIndicator = true;

            LoadUserAccesses();

            ServerServiceClient.GetInstance().OperatorClient.Password = userPassword;
            ArrayList Applications = ServerServiceClient.GetInstance().GetActiveApplications() as ArrayList;            
            GridControlDataAdm.InitializeMenus(Applications);

        }

        private void LoadUserAccesses()
        {
            accesses = ServerServiceClient.SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetAllAccessByOperatorAndApplication, ServerServiceClient.OperatorClient.Code, UserApplicationClientData.Administration.Code));
        }

        void gridViewExAdministration_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            GridControlDataAdm.Current.Activate();
            RibbonControl.Refresh();
        }

        void gridViewExAdministration_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle <= 0)
            {
                gridViewExAdministration.ClearSelection();
            }
            GridControlDataAdm.Current.Activate();
            RibbonControl.Refresh();
        }

        void gridControlExAdministration_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                gridControlExAdministration_MouseDoubleClick(sender, null);
            }
        }

        public bool CheckAccessBarButtonItem(UserResourceClientData userResourceData, UserActionClientData action)
        {
            bool hasAccess = false;

            if (userResourceData != null && action != null)
            {
                foreach (UserAccessClientData userAccessData in accesses)
                {
                    if ((userAccessData.UserApplication.Equals(UserApplicationClientData.Administration)) &&
                           (userAccessData.UserPermission.UserResource.Equals(userResourceData)) &&
                           (userAccessData.UserPermission.UserAction.Equals(action)))
                    {
                        hasAccess = true;
                    }

                    if (hasAccess == true)
                        break;   

                }
            }

            return hasAccess;
        }

        void gridControlExAdministration_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int rowHandle = -1;
            if (e != null)
                rowHandle = gridViewExAdministration.CalcHitInfo(e.X, e.Y).RowHandle;
            else
                rowHandle = gridViewExAdministration.FocusedRowHandle;

            if (rowHandle > -1)
            {
                GridControlDataAdm.Current.Activate();
                GridControlDataAdm.Current.Edit();
                RibbonControl.Refresh();
            }
        }

        void ServerServiceClient_CommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            if (e.Objects != null && e.Objects.Count > 0)
            {
                Type gridControlType = null;
                FormUtil.InvokeRequired(gridControlExAdministration,
                    delegate
                    {
                        if (gridControlExAdministration.Type != null)
                        {
                            gridControlType = gridControlExAdministration.Type;
                        }
                    });
                if (gridControlType != null)
                {
                    if (AdministrationCommittedChanges != null)
                    {
                        AdministrationCommittedChanges(null, e);
                    } 
                    if (e.Objects[0] is DepartmentTypeClientData &&
                        gridControlType != typeof(GridControlDataAdmDepartmentType))
                    {
                        if (e.Action == CommittedDataAction.Update)
                        {
                            DepartmentTypeClientData deptType = e.Objects[0] as DepartmentTypeClientData;
                            if (gridControlType == typeof(GridControlDataAdmEvaluation))
                            {
                                UpdateDepartmentTypeInEvaluation(deptType);
                            }
                            else if (gridControlType == typeof(GridControlDataAdmPosition))
                            {
                                UpdateDepartmentTypeInPosition(deptType);
                            }
                            else if (gridControlType == typeof(GridControlDataAdmRank))
                            {
                                UpdateDepartmentTypeInRank(deptType);
                            }
                            else if (gridControlType == typeof(GridControlDataAdmOfficer))
                            {
                                UpdateDepartmentTypeInOfficers(deptType);
                            }
                            else if (gridControlType == typeof(GridControlDataAdmUnit))
                            {
                                UpdateDepartmentTypeInUnits(deptType);
                            }
                            else if (gridControlType == typeof(GridControlDataAdmUnitType))
                            {
                                UpdateDepartmentTypeInUnitTypes(deptType);
                            }
                            else if (gridControlType == typeof(GridControlDataAdmUnitOfficerAssignment))
                            {
                                UpdateDepartmentTypeZoneAndStationInUnitOfficerAssignment(deptType, null, null);
                            }
                        }
                    }
                    else if (e.Objects[0] is PositionClientData &&
                        gridControlType != typeof(GridControlDataAdmPosition))//Jerarquia
                    {
                        PositionClientData position = e.Objects[0] as PositionClientData;
                        if (e.Action == CommittedDataAction.Update || e.Action == CommittedDataAction.Delete)
                        {
                            if (gridControlType == typeof(GridControlDataAdmOfficer))
                            {
                                UpdatePositionInOfficers(position, e.Action);
                            }
                        }
                    }
                    else if (e.Objects[0] is DepartmentZoneClientData &&
                        gridControlType != typeof(GridControlDataAdmDepartmentZone))
                    {
                        if (e.Action == CommittedDataAction.Update)
                        {
                            DepartmentZoneClientData zone = e.Objects[0] as DepartmentZoneClientData;
                            if (gridControlType == typeof(GridControlDataAdmUnitOfficerAssignment))
                            {
                                UpdateDepartmentTypeZoneAndStationInUnitOfficerAssignment(null, zone, null);
                            }
                        }
                    }
                    else if (e.Objects[0] is DepartmentStationClientData &&
                        gridControlType != typeof(GridControlDataAdmDepartmentStation))
                    {
                        if (e.Action == CommittedDataAction.Update)
                        {
                            DepartmentStationClientData station = e.Objects[0] as DepartmentStationClientData;
                            if (gridControlType == typeof(GridControlDataAdmUnitOfficerAssignment))
                            {
                                UpdateDepartmentTypeZoneAndStationInUnitOfficerAssignment(null, null, station);
                            }
                        }
                    }
                    else if (e.Objects[0] is ApplicationPreferenceClientData)
                    {
                        for (int i = 0; i < e.Objects.Count; i++)
                        {
                            ApplicationPreferenceClientData clientData = (ApplicationPreferenceClientData)e.Objects[i];
                            if (clientData.Name.Equals("ShowErrorDetails", StringComparison.CurrentCultureIgnoreCase) == true)
                            {
                                MessageForm.CheckPreference = (clientData.Value.ToString().ToUpper() == "TRUE" ? true : false);
                            }
                            CheckActionCommittedChanges(e.Objects[i] as ClientData, e.Action, e.Context);
                        }
                    }
                    else
                    {
                        for (int i = 0; i < e.Objects.Count; i++)
                        {
                            ClientData clientData = (ClientData)e.Objects[i];
                            CheckActionCommittedChanges(e.Objects[i] as ClientData, e.Action, e.Context);
                        }
                    }
                }
            }
        }

        private void UpdateDepartmentTypeInEvaluation(DepartmentTypeClientData departmentType)
        {
            ArrayList gridList = new ArrayList();
            FormUtil.InvokeRequired(gridControlExAdministration,
                delegate
                {
                    if (gridControlExAdministration.Type != null && 
                        gridControlExAdministration.Type == typeof(GridControlDataAdmEvaluation) &&
                        gridControlExAdministration.DataSource != null)
                    {
                        gridList.AddRange(gridControlExAdministration.DataSource as IList);
                        EvaluationDepartmentTypeClientData temp = new EvaluationDepartmentTypeClientData();
                        int index = -1;
                        for (int i = 0; i < gridList.Count; i++)
                        {
                            temp.Department = departmentType;
                            GridControlDataAdmEvaluation gridData = gridList[i] as GridControlDataAdmEvaluation;
                            index = gridData.Evaluation.Departments.IndexOf(temp);
                            if (index != -1)
                            {
                                gridData.Evaluation.Departments[index].Department = departmentType;
                                syncBox.Sync(gridData, CommittedDataAction.Update);
                            }
                        }
                    }
                });            
        }

        private void UpdateDepartmentTypeInPosition(DepartmentTypeClientData departmentType)
        {
            ArrayList gridList = new ArrayList();
            FormUtil.InvokeRequired(gridControlExAdministration,
                delegate
                {
                    if (gridControlExAdministration.Type != null &&
                        gridControlExAdministration.Type == typeof(GridControlDataAdmPosition) &&
                        gridControlExAdministration.DataSource != null)
                    {
                        gridList.AddRange(gridControlExAdministration.DataSource as IList);
                        PositionClientData temp = new PositionClientData();
                        //int index = -1;
                        for (int i = 0; i < gridList.Count; i++)
                        {
                            GridControlDataAdmPosition gridData = gridList[i] as GridControlDataAdmPosition;
                            if (gridData.Position.DepartmentTypeCode == departmentType.Code)
                            {
                                gridData.Position.DepartmentTypeName = departmentType.Name;
                                syncBox.Sync(gridData, CommittedDataAction.Update);
                            }
                        }
                    }
                });
        }

        private void UpdatePositionInOfficers(PositionClientData position, CommittedDataAction action)
        {
            ArrayList gridList = new ArrayList();
            FormUtil.InvokeRequired(gridControlExAdministration,
                delegate
                {
                    if (gridControlExAdministration.Type != null &&
                        gridControlExAdministration.Type == typeof(GridControlDataAdmOfficer) &&
                        gridControlExAdministration.DataSource != null)
                    {
                        gridList.AddRange(gridControlExAdministration.DataSource as IList);
                        PositionClientData temp = new PositionClientData();
                        for (int i = 0; i < gridList.Count; i++)
                        {
                            GridControlDataAdmOfficer gridData = gridList[i] as GridControlDataAdmOfficer;
                            if (gridData.Officer.Position != null &&
                                gridData.Officer.Position.Code == position.Code)
                            {
                                if (action == CommittedDataAction.Delete)
                                {
                                    gridData.Officer.Position = null;
                                }
                                else
                                {
                                    gridData.Officer.Position = position;
                                }
                                syncBox.Sync(gridData, CommittedDataAction.Update);
                            }
                        }
                    }
                });
        }
        private void UpdateDepartmentTypeInRank(DepartmentTypeClientData departmentType)
        {
            ArrayList gridList = new ArrayList();
            FormUtil.InvokeRequired(gridControlExAdministration,
                delegate
                {
                    if (gridControlExAdministration.Type != null &&
                        gridControlExAdministration.Type == typeof(GridControlDataAdmRank) &&
                        gridControlExAdministration.DataSource != null)
                    {
                        gridList.AddRange(gridControlExAdministration.DataSource as IList);
                        RankClientData temp = new RankClientData();
                        //int index = -1;
                        for (int i = 0; i < gridList.Count; i++)
                        {
                            GridControlDataAdmRank gridData = gridList[i] as GridControlDataAdmRank;
                            if (gridData.Rank.DepartmentTypeCode == departmentType.Code)
                            {
                                gridData.Rank.DepartmentTypeName = departmentType.Name;
                                syncBox.Sync(gridData, CommittedDataAction.Update);
                            }
                        }
                    }
                });
        }

        private void UpdateDepartmentTypeZoneAndStationInUnitOfficerAssignment(
            DepartmentTypeClientData departmentType, 
            DepartmentZoneClientData deptZone, DepartmentStationClientData deptStation)
        {
            ArrayList gridList = new ArrayList();
            FormUtil.InvokeRequired(gridControlExAdministration,
                delegate
                {
                    if (gridControlExAdministration.Type != null &&
                        gridControlExAdministration.Type == typeof(GridControlDataAdmUnitOfficerAssignment) &&
                        gridControlExAdministration.DataSource != null)
                    {
                        gridList.AddRange(gridControlExAdministration.DataSource as IList);
                        for (int i = 0; i < gridList.Count; i++)
                        {
                            GridControlDataAdmUnitOfficerAssignment gridData = gridList[i] as GridControlDataAdmUnitOfficerAssignment;
                            if (departmentType != null || deptZone != null || deptStation != null)
                            {
                                if (departmentType != null && 
                                    gridData.Assignment.DepartmentStation.DepartmentZone.DepartmentType.Code == departmentType.Code)
                                {
                                    gridData.Assignment.DepartmentStation.DepartmentZone.DepartmentType = departmentType;
                                }
                                if (deptZone != null && gridData.Assignment.DepartmentStation.DepartmentZone.Code == deptZone.Code)
                                {
                                    gridData.Assignment.DepartmentStation.DepartmentZone = deptZone;
                                }
                                if (deptStation != null && gridData.Assignment.DepartmentStation.Code == deptStation.Code)
                                {
                                    gridData.Assignment.DepartmentStation = deptStation;
                                }

                                syncBox.Sync(gridData, CommittedDataAction.Update);
                            }
                        }
                    }
                });
        }

        private void UpdateDepartmentTypeInUnits(DepartmentTypeClientData departmentType)
        {
            ArrayList gridList = new ArrayList();
            FormUtil.InvokeRequired(gridControlExAdministration,
                delegate
                {
                    if (gridControlExAdministration.Type != null &&
                        gridControlExAdministration.Type == typeof(GridControlDataAdmUnit) &&
                        gridControlExAdministration.DataSource != null)
                    {
                        gridList.AddRange(gridControlExAdministration.DataSource as IList);
                        UnitClientData temp = new UnitClientData();
                        //int index = -1;
                        for (int i = 0; i < gridList.Count; i++)
                        {
                            GridControlDataAdmUnit gridData = gridList[i] as GridControlDataAdmUnit;
                            if (gridData.Unit.DepartmentStation.DepartmentZone.DepartmentType.Code == departmentType.Code)
                            {
                                gridData.Unit.DepartmentStation.DepartmentZone.DepartmentType = departmentType;
                                syncBox.Sync(gridData, CommittedDataAction.Update);
                            }
                        }
                    }
                });
        }

        private void UpdateDepartmentTypeInUnitTypes(DepartmentTypeClientData departmentType)
        {
            ArrayList gridList = new ArrayList();
            FormUtil.InvokeRequired(gridControlExAdministration,
                delegate
                {
                    if (gridControlExAdministration.Type != null &&
                        gridControlExAdministration.Type == typeof(GridControlDataAdmUnitType) &&
                        gridControlExAdministration.DataSource != null)
                    {
                        gridList.AddRange(gridControlExAdministration.DataSource as IList);
                        UnitTypeClientData temp = new UnitTypeClientData();
                        //int index = -1;
                        for (int i = 0; i < gridList.Count; i++)
                        {
                            GridControlDataAdmUnitType gridData = gridList[i] as GridControlDataAdmUnitType;
                            if (gridData.UnitType.DepartmentType.Code == departmentType.Code)
                            {
                                gridData.UnitType.DepartmentType = departmentType;
                                syncBox.Sync(gridData, CommittedDataAction.Update);
                            }
                        }
                    }
                });
        }

        private void UpdateDepartmentTypeInOfficers(DepartmentTypeClientData departmentType)
        {
            ArrayList gridList = new ArrayList();
            FormUtil.InvokeRequired(gridControlExAdministration,
                delegate
                {
                    if (gridControlExAdministration.Type != null &&
                        gridControlExAdministration.Type == typeof(GridControlDataAdmOfficer) &&
                        gridControlExAdministration.DataSource != null)
                    {
                        gridList.AddRange(gridControlExAdministration.DataSource as IList);
                        for (int i = 0; i < gridList.Count; i++)
                        {
                            GridControlDataAdmOfficer gridData =
                                gridList[i] as GridControlDataAdmOfficer;
                            if (gridData.Officer.DepartmentType.Code == departmentType.Code)
                            {
                                gridData.Officer.DepartmentType.Name = departmentType.Name;
                                syncBox.Sync(gridData, CommittedDataAction.Update);
                            }
                        }
                    }
                });
        }
        
        private void CheckActionCommittedChanges(ClientData clientData, CommittedDataAction action, 
            SmartCadContext context)
        {
            GridControlData gridData = null;
            try
            {
                gridData = (GridControlData)Activator.CreateInstance(gridControlExAdministration.Type, clientData);
            }
            catch
            { }
            if (gridData != null)
            {
                //syncBox.Sync(gridData, action);
                if (action == CommittedDataAction.Delete)
                {
                    gridControlExAdministration.DeleteItem(gridData);
                }
                else if (action == CommittedDataAction.Save)
                {
                    if (context == null || !context.Operator.Equals(ServerServiceClient.GetInstance().OperatorClient.Login))
                    {
                        gridControlExAdministration.AddOrUpdateItem(gridData, false);
                    }
                    else
                    {
                        gridControlExAdministration.AddOrUpdateItem(gridData, true);
                    }
                }
                else if (action == CommittedDataAction.Update)
                {
                    gridControlExAdministration.AddOrUpdateItem(gridData);
                }                
            }
        }

        public SuperToolTip BuildToolTip(string keyToolTip)
        {
            SuperToolTip toolTip = null;

            if (string.IsNullOrEmpty(keyToolTip))
                return new SuperToolTip();

            string data = ResourceLoader.GetString2(keyToolTip);
            string[] dataElements = ApplicationUtil.GetSplittedResult(data);
            if (dataElements.Length > 0)
            {
                toolTip = new DevExpress.Utils.SuperToolTip();
                ToolTipTitleItem titleItem = new ToolTipTitleItem();
                titleItem.Text = dataElements[0];
                toolTip.Items.Add(titleItem);
            }
            if (dataElements.Length > 1)
            {
                ToolTipItem toolTipItem = new ToolTipItem();
                toolTipItem.Text = dataElements[1];
                toolTip.Items.Add(toolTipItem);
            }
            return toolTip;
        }

        private void LoadLanguage()
        {
            this.Icon = ResourceLoader.GetIcon("$Icon.AdministrationReal");
            this.Text = ResourceLoader.GetString("AdministrationApplicationFormTitle");
            this.barButtonItemHelp.SuperTip = BuildToolTip("ToolTip_HelpOnline");        
            this.labelUserCaption = ResourceLoader.GetString("User") + ": ";
            //this.toolStripStatusLabelUserName.Text = ResourceLoader.GetString("usuario_LowerCase");
            this.labelConnectedCaption = ResourceLoader.GetString("Connected") + ":";
            this.labelLogo.Image = ResourceLoader.GetImage("$Image.AdministrationLogo");
        }

        private bool EnsureLogout()
        {
            DialogResult dialogResult = MessageForm.Show(ResourceLoader.GetString2("$Message.ExitApplication"), MessageFormType.Question);
            return dialogResult == DialogResult.Yes;
        }

        private void AdministrationRibbonForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (EnsureLogout())
            {
                this.serverServiceClient.CommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(ServerServiceClient_CommittedChanges);
                this.serverServiceClient.CloseSession();
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void barButtonItemExit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void barButtonItemHelp_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                ApplicationUtil.LaunchHelp(SmartCadConfiguration.DistFolder + "/Help/ADMINISTRATION Module.chm");
            }
            catch (Exception ex)
            {
                MessageForm.Show(ResourceLoader.GetString2("$Message.HelpNotInstalled"), ex);
            }
        }

        private void LoadGlobalApplicationPreferences()
        {
            try
            {
                globalApplicationPreferences = new Dictionary<string, ApplicationPreferenceClientData>();
                foreach (ApplicationPreferenceClientData applicationPreference in ServerServiceClient.GetInstance().SearchClientObjectsMaxRows(
                    SmartCadHqls.GetAllApplicationPreferences, true))
                {
                    this.globalApplicationPreferences.Add(applicationPreference.Name, applicationPreference);
                }
            }
            catch (Exception ex)
            {
                MessageForm.Show(ex.Message, ex);
            }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            syncServerDateTime = syncServerDateTime.AddMilliseconds((double)GLOBAL_TIMER_PERIOD);            
            sinceLoggedIn = sinceLoggedIn.Add(ADD_TIMESPAN);
            FormUtil.InvokeRequired(ribbonStatusBar1,
                delegate
                {
                    this.labelDate.Caption = ApplicationUtil.GetFormattedDate(ServerServiceClient.GetInstance().GetTime()); 
                    this.labelConnected.Caption = labelConnectedCaption + Math.Truncate(sinceLoggedIn.TotalHours).ToString().PadLeft(2, '0') + ":" + sinceLoggedIn.Minutes.ToString().PadLeft(2, '0');
                    ribbonStatusBar1.Refresh();
                });
        }

        private object RunQueryHelp(bool pagedSearch, bool InitializeCollections, Type objectType, string query, params object[] args)
        {
            string queryStr = query;
            if (args.Length > 0 )
            {
                queryStr = SmartCadHqls.GetCustomHql(query, args);                
            }
            IList list = null;
            if (pagedSearch == true)
            {
                list = ServerServiceClient.GetInstance().SearchClientObjectsMaxRows(queryStr, true);
            }
            else
            {
                list = ServerServiceClient.GetInstance().SearchClientObjects(queryStr,InitializeCollections);
            }
            return list;
        }

        private object RunQuery(bool pagedSearch, Type objectType, string query, params object[] args)
        {
            return RunQuery(pagedSearch, false, objectType, query, args);
        }

        public object RunQuery(bool pagedSearch, bool InitializeCollections, Type objectType, string query, params object[] args)
        {
            BackgroundProcessForm processForm = new BackgroundProcessForm(
                this, new MethodInfo[1] { GetType().GetMethod("RunQueryHelp", 
                    BindingFlags.NonPublic | BindingFlags.Instance) },
                    new object[1][] { new object[5] { pagedSearch, InitializeCollections, objectType, query, args } });
            processForm.CanThrowError = true;
            processForm.ShowDialog();
            return processForm.Results[0];
        }        

        private void DeleteObjects(string singleDeleteMessage, 
            string notSelectedObjects, IList selectedObjects)
        {
            bool TelephonyConfigActive = false;
            if (selectedObjects == null)
            {
                selectedObjects = gridControlExAdministration.SelectedItems;
            }            

            if (gridControlExAdministration.SelectedItems[0].GetType() == typeof(GridControlDataAdmTelephonyConfiguration))
            {
                IList LogUsers = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetAllUserSessions));
                if (LogUsers.Count > 0)
                {
                    foreach(SessionHistoryClientData UserInfo in LogUsers)
                    {
                        foreach (GridControlDataAdmTelephonyConfiguration SelectedUserInfo in gridControlExAdministration.SelectedItems)
                        {
                            if (UserInfo.ComputerName.ToUpper() == SelectedUserInfo.Computer.ToUpper() && UserInfo.UserApplication == "FirstLevel")
                            {
                                TelephonyConfigActive = true;
                            }
                        }
                    }
                }
            }

            int selectedItemsCount = selectedObjects.Count;

            if (selectedItemsCount > 0)
            {
                string message = singleDeleteMessage;

                if (MessageForm.Show(message, MessageFormType.Question) == DialogResult.Yes)
                {
                    if (TelephonyConfigActive == false)
                    {
                        ArrayList objectsToDelete = new ArrayList(selectedObjects);

                        try
                        {
                            GridControlDataAdmOfficer officer = null;

                            try
                            {
                                officer = (GridControlDataAdmOfficer)objectsToDelete[0];

                                if (ValidateOfficerHasAssignment(objectsToDelete) == false)
                                    return;
                            }
                            catch
                            {

                            }

                            if (objectsToDelete.Count > 0)
                            {
                                BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[1] { GetType().GetMethod("DeleteObjectsHelp", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[1] { objectsToDelete } });
                                processForm.CanThrowError = true;
                                processForm.ShowDialog();
                            }
                            else
                            {
                                MessageForm.Show(notSelectedObjects, MessageFormType.Information);
                            }

                        }
                        catch (FaultException ex)
                        {
                            if (ex.Code != null && ex.Code.Name != null
                                && ex.Code.Name.Equals(typeof(DatabaseObjectException).Name))
                            {
                                MessageForm.Show(ex.Message, MessageFormType.Information);
                            }
                            else if (ex.Code != null && ex.Code.Name != null
                                && ex.Code.Name.Equals(typeof(DatabaseDeletedObjectException).Name))
                            {
                                MessageForm.Show(notSelectedObjects, MessageFormType.Information);
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageForm.Show(ex.Message, ex);
                        }
                    }
                    else
                    {
                        MessageForm.Show(ResourceLoader.GetString2("DeleteTelephonyConfigurationActive"), MessageFormType.Error);
                    }                    
                }

      
            }
            TelephonyConfigActive = false;
        }

        private bool ValidateOfficerHasAssignment(ArrayList objectsToDelete)
        {
            try
            {
                int code = 0;

                
                
                    foreach (GridControlData data in objectsToDelete)
                    {
                        
                        code = data.Tag.Code;
                        break;
                    }

                    if (GetHaveOfficerCurrentAssings(code) || GetHaveOfficerCurrentAssingsNotStarted(code))
                        return true;
                        //if (MessageForm.Show(ResourceLoader.GetString2("OfficerAssignValidateDelete"), MessageFormType.Question) != DialogResult.Yes)
                            //return false;
            }
            catch { }
            return true;
        }

        private bool GetHaveOfficerCurrentAssings(int officerCode)
        {
            string DateNHibernateFormat = "yyyyMMdd HH:mm";
            string date = SmartCadDatabase.GetTimeFromBD().ToString(DateNHibernateFormat);
            string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.CountUnitOfficerCurrentAssignsByOfficerCode, officerCode, date);
            long list = (long)ServerServiceClient.GetInstance().SearchBasicObject(hql);
            if (list > 0)
            {
                long count = list;
                if (count > 0)
                {
                    return true;
                }
            }
            return false;
        }

        private bool GetHaveOfficerCurrentAssingsNotStarted(int officerCode)
        {
            string DateNHibernateFormat = "yyyyMMdd HH:mm";
            string date = SmartCadDatabase.GetTimeFromBD().ToString(DateNHibernateFormat);           
            string hql = SmartCadHqls.GetCustomHql(@"SELECT count(ObjectData.Code) FROM UnitOfficerAssignHistoryData ObjectData  WHERE  ObjectData.Officer.Code = '{0}'  AND  ('{1}' < ObjectData.Start)", officerCode, date);
            long list = (long)ServerServiceClient.GetInstance().SearchBasicObject(hql);
            if (list > 0)
            {
                long count = list;
                if (count > 0)
                {
                    return true;
                }
            }
            return false;
        }
     


        public void DeleteObjects(string singleDeleteMessage, string notSelectedObjects)
        {
            DeleteObjects(singleDeleteMessage, notSelectedObjects, null);            
        }

        private void DeleteObjectsHelp(IList selectedObjects)
        {
            if (selectedObjects.Count > 0)
            {
                
                if (selectedObjects[0] as GridControlData != null)
                {
                    foreach (GridControlData data in selectedObjects)
                    {
                        serverServiceClient.DeleteClientObject(data.Tag);
                    }
                }
                else
                {
                    foreach (ClientData data in selectedObjects)
                    {
                        serverServiceClient.DeleteClientObject(data);
                    }
                }

             }
        }

        private void RibbonControl_MinimizedChanged(object sender, EventArgs e)
        {
            if (this.RibbonControl.Minimized == true)
            {
                RibbonControl.Minimized = false;
            }
        }

		private void AdministrationRibbonForm_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.F1)//CALLS ONLINE HELP...
            {
                barButtonItemHelp_ItemClick(null, null);
                e.Handled = true;
            }
            else if (e.KeyData == Keys.Apps && e.Control == false &&
                e.Alt == false && e.Modifiers == Keys.None && e.Alt == false)
            {
                contextMenuStrip.Show(MousePosition);
                e.Handled = true;
            }
		}

        public void AssignButtonShorcutGroup()
        {
            contextMenuStrip.Items.Clear();
            foreach (BarButtonItem button in GridControlDataAdm.Current.ActionButtons)
            {
                //if (button.Enabled)
                //{
                    ToolStripMenuItem menutItem = new ToolStripMenuItem(button.Caption, button.Glyph);
                    menutItem.ShortcutKeys = button.ItemShortcut.Key;
                    menutItem.Tag = button;
                    
                    menutItem.Enabled = button.Enabled;
                    menutItem.Click += new EventHandler(menutItem_Click);
                    contextMenuStrip.Items.Add(menutItem);
                //}
            }
        }

        void menutItem_Click(object sender, EventArgs e)
        {
            ToolStripItem toolStripItem = sender as ToolStripItem;
            if (toolStripItem != null && toolStripItem.Tag != null)
            {
                (toolStripItem.Tag as BarButtonItem).PerformClick();
            }
        }

        private void gridViewExAdministration_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                contextMenuStrip.Show(MousePosition);
            }
        }
    }    


}
