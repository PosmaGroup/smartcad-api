using SmartCadControls;
namespace SmartCadGuiCommon
{
    partial class DepartmentStationXtraForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used. 
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DepartmentStationXtraForm));
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.DepartmentStationXtraFormConvertedLayout = new DevExpress.XtraLayout.LayoutControl();
            this.textEditTelephone = new DevExpress.XtraEditors.TextEdit();
            this.gridControlExCoordinates = new SmartCadControls.GridControlEx();
            this.gridViewExCoordinates = new SmartCadControls.GridViewEx();
            this.labelControlCoordinates = new DevExpress.XtraEditors.LabelControl();
            this.gridControlExUnits = new SmartCadControls.GridControlEx();
            this.gridViewExUnits = new SmartCadControls.GridViewEx();
            this.comboBoxEditDepartmentZones = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEditDepartmentTypes = new DevExpress.XtraEditors.ComboBoxEdit();
            this.simpleButtonMap = new DevExpress.XtraEditors.SimpleButton();
            this.textEditDepartmentStationName = new DevExpress.XtraEditors.TextEdit();
            this.gridControlExOfficers = new SmartCadControls.GridControlEx();
            this.gridViewExOfficers = new SmartCadControls.GridViewEx();
            this.textEditCustomCode = new DevExpress.XtraEditors.TextEdit();
            this.simpleButtonAccept = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemMaps = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupStations = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemDepartmentType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemZone = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemCustomCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTelephone = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupOfficers = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupUnits = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutConverter1 = new DevExpress.XtraLayout.Converter.LayoutConverter(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.DepartmentStationXtraFormConvertedLayout)).BeginInit();
            this.DepartmentStationXtraFormConvertedLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTelephone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExCoordinates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExCoordinates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExUnits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExUnits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditDepartmentZones.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditDepartmentTypes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDepartmentStationName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExOfficers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExOfficers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCustomCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemMaps)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupStations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartmentType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemZone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCustomCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTelephone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupOfficers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupUnits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButtonCancel.Appearance.Options.UseFont = true;
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(412, 521);
            this.simpleButtonCancel.Margin = new System.Windows.Forms.Padding(2);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(82, 32);
            this.simpleButtonCancel.StyleController = this.DepartmentStationXtraFormConvertedLayout;
            this.simpleButtonCancel.TabIndex = 10;
            this.simpleButtonCancel.Text = "Cancelar";
            // 
            // DepartmentStationXtraFormConvertedLayout
            // 
            this.DepartmentStationXtraFormConvertedLayout.AllowCustomizationMenu = false;
            this.DepartmentStationXtraFormConvertedLayout.Controls.Add(this.textEditTelephone);
            this.DepartmentStationXtraFormConvertedLayout.Controls.Add(this.gridControlExCoordinates);
            this.DepartmentStationXtraFormConvertedLayout.Controls.Add(this.labelControlCoordinates);
            this.DepartmentStationXtraFormConvertedLayout.Controls.Add(this.gridControlExUnits);
            this.DepartmentStationXtraFormConvertedLayout.Controls.Add(this.comboBoxEditDepartmentZones);
            this.DepartmentStationXtraFormConvertedLayout.Controls.Add(this.comboBoxEditDepartmentTypes);
            this.DepartmentStationXtraFormConvertedLayout.Controls.Add(this.simpleButtonMap);
            this.DepartmentStationXtraFormConvertedLayout.Controls.Add(this.textEditDepartmentStationName);
            this.DepartmentStationXtraFormConvertedLayout.Controls.Add(this.gridControlExOfficers);
            this.DepartmentStationXtraFormConvertedLayout.Controls.Add(this.textEditCustomCode);
            this.DepartmentStationXtraFormConvertedLayout.Controls.Add(this.simpleButtonCancel);
            this.DepartmentStationXtraFormConvertedLayout.Controls.Add(this.simpleButtonAccept);
            this.DepartmentStationXtraFormConvertedLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DepartmentStationXtraFormConvertedLayout.Location = new System.Drawing.Point(0, 0);
            this.DepartmentStationXtraFormConvertedLayout.Margin = new System.Windows.Forms.Padding(2);
            this.DepartmentStationXtraFormConvertedLayout.Name = "DepartmentStationXtraFormConvertedLayout";
            this.DepartmentStationXtraFormConvertedLayout.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(550, 328, 250, 350);
            this.DepartmentStationXtraFormConvertedLayout.OptionsCustomizationForm.ShowLayoutTreeView = false;
            this.DepartmentStationXtraFormConvertedLayout.Root = this.layoutControlGroup1;
            this.DepartmentStationXtraFormConvertedLayout.Size = new System.Drawing.Size(496, 565);
            this.DepartmentStationXtraFormConvertedLayout.TabIndex = 0;
            // 
            // textEditTelephone
            // 
            this.textEditTelephone.Location = new System.Drawing.Point(178, 87);
            this.textEditTelephone.Margin = new System.Windows.Forms.Padding(2);
            this.textEditTelephone.Name = "textEditTelephone";
            this.textEditTelephone.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEditTelephone.Properties.Appearance.Options.UseFont = true;
            this.textEditTelephone.Properties.MaxLength = 255;
            this.textEditTelephone.Size = new System.Drawing.Size(311, 19);
            this.textEditTelephone.StyleController = this.DepartmentStationXtraFormConvertedLayout;
            this.textEditTelephone.TabIndex = 1;
            // 
            // gridControlExCoordinates
            // 
            this.gridControlExCoordinates.EnableAutoFilter = false;
            this.gridControlExCoordinates.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExCoordinates.Location = new System.Drawing.Point(87, 178);
            this.gridControlExCoordinates.MainView = this.gridViewExCoordinates;
            this.gridControlExCoordinates.Name = "gridControlExCoordinates";
            this.gridControlExCoordinates.Size = new System.Drawing.Size(401, 98);
            this.gridControlExCoordinates.TabIndex = 5;
            this.gridControlExCoordinates.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExCoordinates});
            this.gridControlExCoordinates.ViewTotalRows = false;
            // 
            // gridViewExCoordinates
            // 
            this.gridViewExCoordinates.AllowFocusedRowChanged = true;
            this.gridViewExCoordinates.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExCoordinates.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExCoordinates.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExCoordinates.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExCoordinates.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExCoordinates.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExCoordinates.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExCoordinates.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExCoordinates.EnablePreviewLineForFocusedRow = false;
            this.gridViewExCoordinates.GridControl = this.gridControlExCoordinates;
            this.gridViewExCoordinates.Name = "gridViewExCoordinates";
            this.gridViewExCoordinates.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExCoordinates.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExCoordinates.ViewTotalRows = false;
            // 
            // labelControlCoordinates
            // 
            this.labelControlCoordinates.Location = new System.Drawing.Point(7, 177);
            this.labelControlCoordinates.Name = "labelControlCoordinates";
            this.labelControlCoordinates.Size = new System.Drawing.Size(68, 13);
            this.labelControlCoordinates.StyleController = this.DepartmentStationXtraFormConvertedLayout;
            this.labelControlCoordinates.TabIndex = 4;
            this.labelControlCoordinates.Text = "Coordenadas:";
            // 
            // gridControlExUnits
            // 
            this.gridControlExUnits.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(2);
            this.gridControlExUnits.EnableAutoFilter = false;
            this.gridControlExUnits.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExUnits.Location = new System.Drawing.Point(7, 429);
            this.gridControlExUnits.MainView = this.gridViewExUnits;
            this.gridControlExUnits.Margin = new System.Windows.Forms.Padding(2);
            this.gridControlExUnits.Name = "gridControlExUnits";
            this.gridControlExUnits.Size = new System.Drawing.Size(482, 83);
            this.gridControlExUnits.TabIndex = 7;
            this.gridControlExUnits.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExUnits});
            this.gridControlExUnits.ViewTotalRows = false;
            // 
            // gridViewExUnits
            // 
            this.gridViewExUnits.AllowFocusedRowChanged = true;
            this.gridViewExUnits.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExUnits.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExUnits.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExUnits.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExUnits.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExUnits.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExUnits.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExUnits.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExUnits.EnablePreviewLineForFocusedRow = false;
            this.gridViewExUnits.GridControl = this.gridControlExUnits;
            this.gridViewExUnits.Name = "gridViewExUnits";
            this.gridViewExUnits.OptionsBehavior.FocusLeaveOnTab = true;
            this.gridViewExUnits.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExUnits.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExUnits.ViewTotalRows = false;
            // 
            // comboBoxEditDepartmentZones
            // 
            this.comboBoxEditDepartmentZones.Location = new System.Drawing.Point(178, 147);
            this.comboBoxEditDepartmentZones.Name = "comboBoxEditDepartmentZones";
            this.comboBoxEditDepartmentZones.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxEditDepartmentZones.Properties.Appearance.Options.UseFont = true;
            this.comboBoxEditDepartmentZones.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditDepartmentZones.Properties.MaxLength = 255;
            this.comboBoxEditDepartmentZones.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEditDepartmentZones.Size = new System.Drawing.Size(311, 19);
            this.comboBoxEditDepartmentZones.StyleController = this.DepartmentStationXtraFormConvertedLayout;
            this.comboBoxEditDepartmentZones.TabIndex = 3;
            this.comboBoxEditDepartmentZones.SelectedIndexChanged += new System.EventHandler(this.comboBoxEditDepartmentZones_SelectedIndexChanged);
            // 
            // comboBoxEditDepartmentTypes
            // 
            this.comboBoxEditDepartmentTypes.Location = new System.Drawing.Point(178, 117);
            this.comboBoxEditDepartmentTypes.Name = "comboBoxEditDepartmentTypes";
            this.comboBoxEditDepartmentTypes.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxEditDepartmentTypes.Properties.Appearance.Options.UseFont = true;
            this.comboBoxEditDepartmentTypes.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditDepartmentTypes.Properties.MaxLength = 255;
            this.comboBoxEditDepartmentTypes.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEditDepartmentTypes.Size = new System.Drawing.Size(311, 19);
            this.comboBoxEditDepartmentTypes.StyleController = this.DepartmentStationXtraFormConvertedLayout;
            this.comboBoxEditDepartmentTypes.TabIndex = 2;
            this.comboBoxEditDepartmentTypes.SelectedIndexChanged += new System.EventHandler(this.comboBoxEditDepartmentTypes_SelectedIndexChanged);
            // 
            // simpleButtonMap
            // 
            this.simpleButtonMap.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButtonMap.Appearance.Options.UseFont = true;
            this.simpleButtonMap.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonMap.Image")));
            this.simpleButtonMap.Location = new System.Drawing.Point(2, 521);
            this.simpleButtonMap.Margin = new System.Windows.Forms.Padding(2);
            this.simpleButtonMap.Name = "simpleButtonMap";
            this.simpleButtonMap.Size = new System.Drawing.Size(43, 42);
            this.simpleButtonMap.StyleController = this.DepartmentStationXtraFormConvertedLayout;
            this.simpleButtonMap.TabIndex = 8;
            this.simpleButtonMap.Click += new System.EventHandler(this.simpleButtonMap_Click);
            // 
            // textEditDepartmentStationName
            // 
            this.textEditDepartmentStationName.Location = new System.Drawing.Point(178, 27);
            this.textEditDepartmentStationName.Margin = new System.Windows.Forms.Padding(2);
            this.textEditDepartmentStationName.Name = "textEditDepartmentStationName";
            this.textEditDepartmentStationName.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEditDepartmentStationName.Properties.Appearance.Options.UseFont = true;
            this.textEditDepartmentStationName.Properties.MaxLength = 255;
            this.textEditDepartmentStationName.Size = new System.Drawing.Size(311, 19);
            this.textEditDepartmentStationName.StyleController = this.DepartmentStationXtraFormConvertedLayout;
            this.textEditDepartmentStationName.TabIndex = 0;
            this.textEditDepartmentStationName.TextChanged += new System.EventHandler(this.EnableButtons);
            // 
            // gridControlExOfficers
            // 
            this.gridControlExOfficers.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(2);
            this.gridControlExOfficers.EnableAutoFilter = false;
            this.gridControlExOfficers.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExOfficers.Location = new System.Drawing.Point(7, 311);
            this.gridControlExOfficers.MainView = this.gridViewExOfficers;
            this.gridControlExOfficers.Margin = new System.Windows.Forms.Padding(2);
            this.gridControlExOfficers.Name = "gridControlExOfficers";
            this.gridControlExOfficers.Size = new System.Drawing.Size(482, 84);
            this.gridControlExOfficers.TabIndex = 6;
            this.gridControlExOfficers.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExOfficers});
            this.gridControlExOfficers.ViewTotalRows = false;
            // 
            // gridViewExOfficers
            // 
            this.gridViewExOfficers.AllowFocusedRowChanged = true;
            this.gridViewExOfficers.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExOfficers.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExOfficers.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExOfficers.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExOfficers.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExOfficers.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExOfficers.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExOfficers.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExOfficers.EnablePreviewLineForFocusedRow = false;
            this.gridViewExOfficers.GridControl = this.gridControlExOfficers;
            this.gridViewExOfficers.Name = "gridViewExOfficers";
            this.gridViewExOfficers.OptionsBehavior.FocusLeaveOnTab = true;
            this.gridViewExOfficers.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExOfficers.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExOfficers.ViewTotalRows = false;
            // 
            // textEditCustomCode
            // 
            this.textEditCustomCode.Location = new System.Drawing.Point(178, 57);
            this.textEditCustomCode.Name = "textEditCustomCode";
            this.textEditCustomCode.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEditCustomCode.Properties.Appearance.Options.UseFont = true;
            this.textEditCustomCode.Properties.MaxLength = 255;
            this.textEditCustomCode.Size = new System.Drawing.Size(311, 19);
            this.textEditCustomCode.StyleController = this.DepartmentStationXtraFormConvertedLayout;
            this.textEditCustomCode.TabIndex = 1;
            // 
            // simpleButtonAccept
            // 
            this.simpleButtonAccept.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonAccept.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButtonAccept.Appearance.Options.UseFont = true;
            this.simpleButtonAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButtonAccept.Location = new System.Drawing.Point(326, 521);
            this.simpleButtonAccept.Margin = new System.Windows.Forms.Padding(2);
            this.simpleButtonAccept.Name = "simpleButtonAccept";
            this.simpleButtonAccept.Size = new System.Drawing.Size(82, 32);
            this.simpleButtonAccept.StyleController = this.DepartmentStationXtraFormConvertedLayout;
            this.simpleButtonAccept.TabIndex = 9;
            this.simpleButtonAccept.Text = "Accept";
            this.simpleButtonAccept.Click += new System.EventHandler(this.simpleButtonAccept_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItemMaps,
            this.emptySpaceItem1,
            this.layoutControlGroupStations,
            this.layoutControlGroupOfficers,
            this.layoutControlGroupUnits});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(496, 565);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.simpleButtonCancel;
            this.layoutControlItem6.ControlAlignment = System.Drawing.ContentAlignment.BottomRight;
            this.layoutControlItem6.CustomizationFormText = "simpleButtonCancelitem";
            this.layoutControlItem6.Location = new System.Drawing.Point(410, 519);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem6.Name = "simpleButtonCancelitem";
            this.layoutControlItem6.Size = new System.Drawing.Size(86, 46);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "simpleButtonCancelitem";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.simpleButtonAccept;
            this.layoutControlItem7.ControlAlignment = System.Drawing.ContentAlignment.BottomRight;
            this.layoutControlItem7.CustomizationFormText = "simpleButtonAcceptitem";
            this.layoutControlItem7.Location = new System.Drawing.Point(324, 519);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem7.Name = "simpleButtonAcceptitem";
            this.layoutControlItem7.Size = new System.Drawing.Size(86, 46);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "simpleButtonAcceptitem";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItemMaps
            // 
            this.layoutControlItemMaps.Control = this.simpleButtonMap;
            this.layoutControlItemMaps.CustomizationFormText = "simpleButtonMapitem";
            this.layoutControlItemMaps.Location = new System.Drawing.Point(0, 519);
            this.layoutControlItemMaps.MaxSize = new System.Drawing.Size(47, 46);
            this.layoutControlItemMaps.MinSize = new System.Drawing.Size(47, 46);
            this.layoutControlItemMaps.Name = "layoutControlItemMaps";
            this.layoutControlItemMaps.Size = new System.Drawing.Size(47, 46);
            this.layoutControlItemMaps.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemMaps.Text = "layoutControlItemMaps";
            this.layoutControlItemMaps.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemMaps.TextToControlDistance = 0;
            this.layoutControlItemMaps.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(47, 519);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(110, 30);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(277, 46);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupStations
            // 
            this.layoutControlGroupStations.CustomizationFormText = "layoutControlGroupStations";
            this.layoutControlGroupStations.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemDepartmentType,
            this.layoutControlItemZone,
            this.layoutControlItemName,
            this.layoutControlItem5,
            this.emptySpaceItem4,
            this.layoutControlItem1,
            this.layoutControlItemCustomCode,
            this.layoutControlItemTelephone});
            this.layoutControlGroupStations.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupStations.Name = "layoutControlGroupStations";
            this.layoutControlGroupStations.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupStations.Size = new System.Drawing.Size(496, 284);
            this.layoutControlGroupStations.Text = "layoutControlGroupStations";
            // 
            // layoutControlItemDepartmentType
            // 
            this.layoutControlItemDepartmentType.Control = this.comboBoxEditDepartmentTypes;
            this.layoutControlItemDepartmentType.CustomizationFormText = "comboBoxEditDepartmentTypesItem";
            this.layoutControlItemDepartmentType.Location = new System.Drawing.Point(0, 90);
            this.layoutControlItemDepartmentType.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItemDepartmentType.MinSize = new System.Drawing.Size(214, 30);
            this.layoutControlItemDepartmentType.Name = "layoutControlItemDepartmentType";
            this.layoutControlItemDepartmentType.Size = new System.Drawing.Size(486, 30);
            this.layoutControlItemDepartmentType.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemDepartmentType.Text = "layoutControlItemDepartmentType";
            this.layoutControlItemDepartmentType.TextSize = new System.Drawing.Size(168, 13);
            // 
            // layoutControlItemZone
            // 
            this.layoutControlItemZone.Control = this.comboBoxEditDepartmentZones;
            this.layoutControlItemZone.CustomizationFormText = "comboBoxEditDepartmentZonesitem";
            this.layoutControlItemZone.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItemZone.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItemZone.MinSize = new System.Drawing.Size(214, 30);
            this.layoutControlItemZone.Name = "layoutControlItemZone";
            this.layoutControlItemZone.Size = new System.Drawing.Size(486, 30);
            this.layoutControlItemZone.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemZone.Text = "layoutControlItemZone";
            this.layoutControlItemZone.TextSize = new System.Drawing.Size(168, 13);
            // 
            // layoutControlItemName
            // 
            this.layoutControlItemName.Control = this.textEditDepartmentStationName;
            this.layoutControlItemName.CustomizationFormText = "textEditDepartmentStationNameitem";
            this.layoutControlItemName.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemName.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItemName.MinSize = new System.Drawing.Size(214, 30);
            this.layoutControlItemName.Name = "layoutControlItemName";
            this.layoutControlItemName.Size = new System.Drawing.Size(486, 30);
            this.layoutControlItemName.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemName.Text = "layoutControlItemName";
            this.layoutControlItemName.TextSize = new System.Drawing.Size(168, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.labelControlCoordinates;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 150);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(79, 17);
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 167);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(79, 62);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(79, 87);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridControlExCoordinates;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(79, 150);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem1.Size = new System.Drawing.Size(407, 104);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItemCustomCode
            // 
            this.layoutControlItemCustomCode.Control = this.textEditCustomCode;
            this.layoutControlItemCustomCode.CustomizationFormText = "textEditCustomCodeitem";
            this.layoutControlItemCustomCode.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItemCustomCode.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItemCustomCode.MinSize = new System.Drawing.Size(214, 30);
            this.layoutControlItemCustomCode.Name = "layoutControlItemCustomCode";
            this.layoutControlItemCustomCode.Size = new System.Drawing.Size(486, 30);
            this.layoutControlItemCustomCode.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemCustomCode.Text = "layoutControlItemCustomCode";
            this.layoutControlItemCustomCode.TextSize = new System.Drawing.Size(168, 13);
            // 
            // layoutControlItemTelephone
            // 
            this.layoutControlItemTelephone.Control = this.textEditTelephone;
            this.layoutControlItemTelephone.CustomizationFormText = "layoutControlItemTelephone";
            this.layoutControlItemTelephone.Location = new System.Drawing.Point(0, 60);
            this.layoutControlItemTelephone.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItemTelephone.MinSize = new System.Drawing.Size(225, 30);
            this.layoutControlItemTelephone.Name = "layoutControlItemTelephone";
            this.layoutControlItemTelephone.Size = new System.Drawing.Size(486, 30);
            this.layoutControlItemTelephone.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemTelephone.Text = "layoutControlItemTelephone";
            this.layoutControlItemTelephone.TextSize = new System.Drawing.Size(168, 13);
            // 
            // layoutControlGroupOfficers
            // 
            this.layoutControlGroupOfficers.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroupOfficers.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.layoutControlGroupOfficers.Location = new System.Drawing.Point(0, 284);
            this.layoutControlGroupOfficers.Name = "layoutControlGroupOfficers";
            this.layoutControlGroupOfficers.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupOfficers.Size = new System.Drawing.Size(496, 118);
            this.layoutControlGroupOfficers.Text = "layoutControlGroupOfficers";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.gridControlExOfficers;
            this.layoutControlItem3.CustomizationFormText = "gridControlExOfficersitem";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(448, 57);
            this.layoutControlItem3.Name = "gridControlExOfficersitem";
            this.layoutControlItem3.Size = new System.Drawing.Size(486, 88);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "gridControlExOfficersitem";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlGroupUnits
            // 
            this.layoutControlGroupUnits.CustomizationFormText = "layoutControlGroupUnits";
            this.layoutControlGroupUnits.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroupUnits.Location = new System.Drawing.Point(0, 402);
            this.layoutControlGroupUnits.Name = "layoutControlGroupUnits";
            this.layoutControlGroupUnits.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupUnits.Size = new System.Drawing.Size(496, 117);
            this.layoutControlGroupUnits.Text = "layoutControlGroupUnits";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridControlExUnits;
            this.layoutControlItem2.CustomizationFormText = "gridControlExUnitsitem";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(448, 49);
            this.layoutControlItem2.Name = "gridControlExUnitsitem";
            this.layoutControlItem2.Size = new System.Drawing.Size(486, 87);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "gridControlExUnitsitem";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // DepartmentStationXtraForm
            // 
            this.AcceptButton = this.simpleButtonAccept;
            this.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.CancelButton = this.simpleButtonCancel;
            this.ClientSize = new System.Drawing.Size(496, 565);
            this.Controls.Add(this.DepartmentStationXtraFormConvertedLayout);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(512, 565);
            this.Name = "DepartmentStationXtraForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DepartmentStationXtraForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DepartmentStationXtraForm_FormClosing);
            this.Load += new System.EventHandler(this.DepartmentStationXtraForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DepartmentStationXtraFormConvertedLayout)).EndInit();
            this.DepartmentStationXtraFormConvertedLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditTelephone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExCoordinates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExCoordinates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExUnits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExUnits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditDepartmentZones.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditDepartmentTypes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDepartmentStationName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExOfficers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExOfficers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCustomCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemMaps)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupStations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartmentType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemZone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCustomCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTelephone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupOfficers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupUnits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private DevExpress.XtraEditors.SimpleButton simpleButtonAccept;
        private DevExpress.XtraEditors.TextEdit textEditDepartmentStationName;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditDepartmentZones;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditDepartmentTypes;
        private DevExpress.XtraEditors.TextEdit textEditCustomCode;
        private GridControlEx gridControlExOfficers;
        private GridViewEx gridViewExOfficers;
        private GridControlEx gridControlExUnits;
        private GridViewEx gridViewExUnits;
        private DevExpress.XtraEditors.SimpleButton simpleButtonMap;
        private DevExpress.XtraLayout.LayoutControl DepartmentStationXtraFormConvertedLayout;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemZone;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDepartmentType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemCustomCode;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemMaps;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.Converter.LayoutConverter layoutConverter1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupStations;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupOfficers;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupUnits;
        private DevExpress.XtraEditors.LabelControl labelControlCoordinates;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private GridControlEx gridControlExCoordinates;
        private GridViewEx gridViewExCoordinates;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.TextEdit textEditTelephone;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTelephone;
    }
}
