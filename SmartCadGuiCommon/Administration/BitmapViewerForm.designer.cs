using DevExpress.XtraEditors;
using SmartCadControls.Controls;
namespace SmartCadGuiCommon
{
    partial class BitmapViewerForm: XtraForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonOK = new SimpleButtonEx();
            this.buttonCancel = new SimpleButtonEx();
            this.bitmapViewer1 = new BitmapViewer();
            this.SuspendLayout();
            // 
            // buttonOK
            // 
            this.buttonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOK.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonOK.ForeColor = System.Drawing.Color.Black;
            this.buttonOK.Location = new System.Drawing.Point(293, 298);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 1;
            this.buttonOK.Text = "Accept";
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCancel.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonCancel.ForeColor = System.Drawing.Color.Black;
            this.buttonCancel.Location = new System.Drawing.Point(374, 298);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 2;
            this.buttonCancel.Text = "Cancelar";
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // bitmapViewer1
            // 
            this.bitmapViewer1.AccessibleRole = System.Windows.Forms.AccessibleRole.ScrollBar;
            this.bitmapViewer1.AutoScroll = true;
            this.bitmapViewer1.BackColor = System.Drawing.SystemColors.Window;
            this.bitmapViewer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.bitmapViewer1.Dimension = 80;
            this.bitmapViewer1.Directory = "";
            this.bitmapViewer1.Location = new System.Drawing.Point(0, 0);
            this.bitmapViewer1.Name = "bitmapViewer1";
            this.bitmapViewer1.Size = new System.Drawing.Size(457, 286);
            this.bitmapViewer1.Spacing = 5;
            this.bitmapViewer1.TabIndex = 0;
            this.bitmapViewer1.Load += new System.EventHandler(this.bitmapViewer1_Load);
            this.bitmapViewer1.PictureSelected += new BitmapViewer.PictureSelectedDelegate(this.bitmapViewer1_PictureSelected);
            // 
            // BitmapViewerForm
            // 
            this.AcceptButton = this.buttonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(457, 331);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.bitmapViewer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BitmapViewerForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Elija un icono";
            this.ResumeLayout(false);

        }

        #endregion

        private SimpleButtonEx buttonOK;
        private SimpleButtonEx buttonCancel;
        public BitmapViewer bitmapViewer1;

    }
}