using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using Smartmatic.SmartCad.Service;
using System.Collections;
using System.ServiceModel;
using Iesi.Collections;
using DevExpress.XtraEditors;
using DevExpress.XtraLayout;
using Smartmatic.SmartCad.Controls;

using System.Xml;
using SmartCadCore.ClientData;
using SmartCadControls.Controls;
using SmartCadCore.Enums;
using SmartCadControls.SyncBoxes;
using SmartCadGuiCommon.Controls;
using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadGuiCommon.SyncBoxes;
using SmartCadControls.Interfaces;


namespace SmartCadGuiCommon
{
    public partial class QuestionForm : XtraForm, IDragManager
    {
        object syncCommitted = new object();
        AdministrationRibbonForm parentForm;
        QuestionClientData selectedQuestion = null;
        FormBehavior behaviorType = FormBehavior.Edit;
        int createdControls = 0;
        private GridControlSynBox answersSyncBox;
        Dictionary<string, LayoutControlItem> controlsDesignerParents;
        Dictionary<string, GridAnswerData> controlsDesignerGridData;
        Dictionary<string, int> controlsCreated;
        bool isquestionWhere = false;

        public QuestionForm()
        {
            InitializeComponent();

            this.gridControlAnswers.Type = typeof(GridAnswerData);
            this.gridControlAnswers.EnableAutoFilter = false;
            this.gridControlAnswers.ViewTotalRows = false;
            this.gridControlAnswers.AutoAdjustColumnWidth();

            answersSyncBox = new GridControlSynBox(gridControlAnswers);
            
            this.dragDropLayoutControl1.layoutControl.MouseClick +=new MouseEventHandler(designerLayoutControl_MouseClick);
            this.dragDropLayoutControl1.ControlCreated += dragDropLayoutControl1_ControlCreated;
            controlsDesignerParents = new Dictionary<string, LayoutControlItem>();
            controlsDesignerGridData = new Dictionary<string, GridAnswerData>();
            controlsCreated = new Dictionary<string, int>();

            controlsCreated["RadioButton"] = 0;
            controlsCreated["CheckEdit"] = 0;
            controlsCreated["TextBoxEx"] = 0;
            controlsCreated["SimpleButton"] = 0;
            FillAnswerGroups();
            FillIncidentTypes();
            LoadLanguage();        
            
        }

        private void dragDropLayoutControl1_ControlCreated(object sender, EventArgs e)
        {
            buttonOk.Enabled = true;
            if (currentItems.Count > 0)
            {
                controlsDesignerParents.Add(currentItems[0].Control.Name, currentItems[0]);
                CreateNewAnswerInGridControl(currentItems[0].Control);
                currentItems[0].Control.Click += new EventHandler(Control_Click);
                currentItems.Clear();
            }
           
        }

        private void LoadLanguage()
        {
            this.layoutControlGroupDataQuestion.Text = ResourceLoader.GetString2("GroupDataQuestion");
            this.layoutControlItemName.Text = ResourceLoader.GetString2("Name") + ":*";
            this.layoutControlItemCustomCode.Text = ResourceLoader.GetString2("CustomCodeQuestion") + ":*";
            this.checkBoxExPriority.Text = ResourceLoader.GetString2("Recommended");
            
            this.layoutControlGroupAnswersIncidents.Text = ResourceLoader.GetString2("AnswersIncidents");
            this.layoutControlGroupAnswers.Text = ResourceLoader.GetString2("Answers");
            this.layoutControlGroupAnswerType.Text = ResourceLoader.GetString2("AnswerType");
            this.layoutControlGroupAnswerDefinition.Text = ResourceLoader.GetString2("AnswerDefinition");
            this.layoutControlGroupControlParameters.Text = ResourceLoader.GetString2("ControlParameters");

            this.layoutControlGroupProcedures.Text = ResourceLoader.GetString2("QuestionFormProcedures");
            this.layoutControlGroupProcedureAnswers.Text = ResourceLoader.GetString2("Answers");
            this.layoutControlGroupProcedureAsociated.Text = ResourceLoader.GetString2("ProcedureAsociated");
            this.layoutControlSelectControl.Text = ResourceLoader.GetString2("SelectAnswerType") + ":";

            this.layoutControlGroupIncidents.Text = ResourceLoader.GetString2("QuestionAsociateIncidents");
            this.layoutControlGroupIncidentsType.Text = ResourceLoader.GetString2("IncidentTypes");
            this.layoutControlItemNotAsociated.Text = ResourceLoader.GetString2("NotAssociated");
            this.layoutControlItemAsociated.Text = ResourceLoader.GetString2("Associated");
            simpleButtonTrash.Text = ResourceLoader.GetString2("TrashText");   
            simpleButtonTrash.ToolTip = ResourceLoader.GetString2("TrashToolTip");   

            listView1.Items[0].Text = ResourceLoader.GetString2("TextBoxesName");
            listView1.Items[1].Text = ResourceLoader.GetString2("RadioButtonsName");
            listView1.Items[2].Text = ResourceLoader.GetString2("CheckBoxesName");
            
           
            
            this.buttonCancel.Text = ResourceLoader.GetString2("$Message.Cancel");
         
        }

        public QuestionForm(AdministrationRibbonForm form, QuestionClientData question, FormBehavior type)
            : this()
        {
            BehaviorType = type;
            SelectedQuestion = question;
            this.parentForm = form;
            form.AdministrationCommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(QuestionForm_AdministrationCommittedChanges);
            this.FormClosing += new FormClosingEventHandler(QuestionForm_FormClosing);

            if (dragDropLayoutControl1.layoutControl.Items.Count > 1)
            {
                buttonOk.Enabled = true;
            }
        }

        void QuestionForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.parentForm != null)
            {
                this.parentForm.AdministrationCommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(QuestionForm_AdministrationCommittedChanges);
            }
        }

        void QuestionForm_AdministrationCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    lock (syncCommitted)
                    {
                        try
                        {
                            if (ServerServiceClient.GetInstance().OperatorClient == null)
                            { }
                        }
                        catch
                        { }

                        if (e.Objects[0] is QuestionClientData)
                        {
                            #region OperatorStatusClientData
                            QuestionClientData question =
                                    e.Objects[0] as QuestionClientData;
                            if (SelectedQuestion != null && SelectedQuestion.Equals(question) && ButtonOkPressed == false)
                            {
                                if (e.Action == CommittedDataAction.Update)
                                {
                                    MessageForm.Show(ResourceLoader.GetString2("UpdateFormQuestionData"), MessageFormType.Warning);
                                    SelectedQuestion = question;
                                }
                                else if (e.Action == CommittedDataAction.Delete)
                                {
                                    MessageForm.Show(ResourceLoader.GetString2("DeleteFormQuestionData"), MessageFormType.Warning);
                                    FormUtil.InvokeRequired(this,
                                    delegate
                                    {
                                        this.Close();
                                    });
                                }

                            }
                            #endregion
                        }
                        else if (e.Objects[0] is IncidentTypeClientData)
                        {
                            #region DepartmentTypeClientData
                            IncidentTypeClientData incidentType = e.Objects[0] as IncidentTypeClientData;
                            if (e.Action == CommittedDataAction.Save)
                            {
                                FormUtil.InvokeRequired(this.listBoxNotAssigned,
                                delegate
                                {
                                    listBoxNotAssigned.BeginUpdate();
                                    listBoxNotAssigned.Items.Add(incidentType);
                                    listBoxNotAssigned.EndUpdate();
                                });
                            }
                            else if (e.Action == CommittedDataAction.Update)
                            {
                                FormUtil.InvokeRequired(this,
                                delegate
                                {
                                    int index = listBoxNotAssigned.Items.IndexOf(incidentType);
                                    if (index != -1)
                                    {
                                        listBoxNotAssigned.Items[index] = incidentType;
                                    }
                                    index = listBoxAssigned.Items.IndexOf(incidentType);
                                    if (index != -1)
                                    {
                                        listBoxAssigned.Items[index] = incidentType;
                                    }
                                });
                            }
                            else if (e.Action == CommittedDataAction.Delete)
                            {
                                FormUtil.InvokeRequired(this,
                                delegate
                                {
                                    int index = listBoxNotAssigned.Items.IndexOf(incidentType);
                                    if (index != -1)
                                    {
                                        listBoxNotAssigned.Items.RemoveAt(index);
                                    }
                                    index = listBoxAssigned.Items.IndexOf(incidentType);
                                    if (index != -1)
                                    {
                                        listBoxAssigned.Items.RemoveAt(index);
                                    }
                                });
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        public FormBehavior BehaviorType
        {
            get
            {
                return behaviorType;
            }
            set
            {
               
                behaviorType = value;
                FormUtil.InvokeRequired(this,
                delegate
                {
                    Clear();
                });
            }
        }

        public QuestionClientData SelectedQuestion
        {
            get
            {
                return selectedQuestion;
            }
            set
            {
                FormUtil.InvokeRequired(this,
                delegate
                {
                    Clear();
                });
                selectedQuestion = value;
                if (selectedQuestion != null)
                {

                    textBoxExName.Text = selectedQuestion.Text;
                    textBoxExMnemonic.Text = selectedQuestion.ShortText;
                    if (selectedQuestion.RequirementType.HasValue == true)
                        checkBoxExPriority.Checked = selectedQuestion.RequirementType.Value != RequirementTypeClientEnum.None;

                    if (selectedQuestion.RenderMethod != null)
                        RestoreLayoutControlFromXML(selectedQuestion.RenderMethod);
                    FillAssignedIncidentTypes();

                }
                else
                {
                    selectedQuestion = new QuestionClientData();
                    selectedQuestion.RequirementType = RequirementTypeClientEnum.Recommended;
                }
                if (listBoxAssigned.Items.Count == 0 && listBoxNotAssigned.Items.Count == 0)
                {

                    buttonAddAllIncidentType.Enabled = false;
                    buttonAddIncidentType.Enabled = false;
                    buttonRemoveAllIncidentType.Enabled = false;
                    buttonRemoveIncidentType.Enabled = false;
                }
                QuestionStatusParameters_Change(null, null);
            }
        }

        private void RestoreLayoutControlFromXML(string render)
        {
            MemoryStream ms = GetMemoryStreamWithXML(render);

            dragDropLayoutControl1.layoutControl.RestoreLayoutFromStream(ms);

            if (!isquestionWhere)
            {

                Dictionary<string, int> qpaData = new Dictionary<string, int>();

                foreach (QuestionPossibleAnswerClientData var in selectedQuestion.PossibleAnswers)
                {
                    if (!qpaData.ContainsKey(var.ControlName))
                    {
                        qpaData.Add(var.ControlName, var.Code);
                    }
                }

                InsertAnswersGridData(qpaData);
                SetAnswersProcedure();
            }
        }

        private void InsertAnswersGridData(Dictionary<string, int> qpaData)
        {
            foreach (BaseLayoutItem baseLayoutItem in dragDropLayoutControl1.layoutControl.Items)
            {
                if (baseLayoutItem is LayoutControlItem)
                {
                    LayoutControlItem item = (LayoutControlItem)baseLayoutItem;
                    if (item.Control != null && ((item.Control is SimpleButton) == false))
                    {
                        controlsDesignerParents.Add(item.Control.Name, item);
                       CreateAnswerInGridControl(item.Control, item.Text, qpaData[item.Control.Name]);
                        item.Control.Click += new EventHandler(Control_Click);
                    }
                }
            }
        }

        private void SetAnswersProcedure()
        {
            foreach (QuestionPossibleAnswerClientData var in selectedQuestion.PossibleAnswers)
            {
                if (var.Procedure != null)
                {
                    controlsDesignerGridData[var.ControlName].Answer.Procedure = var.Procedure;
                }
            }
        }

        private MemoryStream GetMemoryStreamWithXML(string render)
        {
            byte[] byteArray = Encoding.UTF8.GetBytes(render);
            MemoryStream ms = new MemoryStream(byteArray);
            ms.Position = 0;
            XmlDocument doc = new XmlDocument();
            doc.Load(ms);
            XmlNode firstChild = doc.FirstChild;
            foreach (XmlNode node in firstChild.ChildNodes)
            {
                if (node.Name == "Control")
                {
                    Control control = (Control)Activator.CreateInstance(Type.GetType(node.Attributes["type"].Value));
                    control.Name = node.Attributes["name"].Value;
                    control.Parent = this;
                    control.Text = "";
                    controlsCreated[control.GetType().Name]++;
					if (control is TextBoxEx)
					{
						(control as TextBoxEx).MaxLength = 255;
                        if (node.Attributes["multiline"] != null)
                            (control as TextBoxEx).Multiline = bool.Parse(node.Attributes["multiline"].Value);
					}
                    dragDropLayoutControl1.layoutControl.Controls.Add(control);
                }
            }
            ms.Position = 0;
            return ms;
		}

		void gridViewAnswers_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
		{
			if (e.Value != null && e.Value is string && ((string)e.Value).Length > 255)
			{
				gridViewAnswers.CellValueChanging -= new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(gridViewAnswers_CellValueChanging);
				gridViewAnswers.SetRowCellValue(e.RowHandle, e.Column, ((string)e.Value).Substring(0, 255));
				gridViewAnswers.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(gridViewAnswers_CellValueChanging);
			}
		}

        private void Clear()
        {
            textBoxExName.Clear();
            textBoxExMnemonic.Clear();
            checkBoxExPriority.Checked = false;
            gridControlAnswers.ClearData();
            dragDropLayoutControl1.layoutControl.Clear();
            propertyGridControl1.Rows.Clear();
            buttonRemoveAllIncidentType_Click(null, null);
            controlsDesignerParents.Clear();
            controlsDesignerGridData.Clear();
        }

        private void QuestionStatusParameters_Change(object sender, System.EventArgs e)
        {
            if (selectedQuestion != null && selectedQuestion.RequirementType.Value == RequirementTypeClientEnum.Required)
            {
                buttonOk.Enabled = false;
                return;
            }
            if ((string.IsNullOrEmpty(textBoxExName.Text)) ||
                (string.IsNullOrEmpty(textBoxExMnemonic.Text)) ||
                (controlsDesignerGridData.Count == 0) ||
                (CheckControlsOk() == false)
                )
                buttonOk.Enabled = false;
            else
                buttonOk.Enabled = true;
            
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[1] { GetType().GetMethod("buttonOk_Click1", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[0] });
                processForm.CanThrowError = true;
                processForm.ShowDialog();
            }
            catch (FaultException ex)
            {
                if (behaviorType == FormBehavior.Edit)
                {
                    SelectedQuestion = (QuestionClientData)ServerServiceClient.GetInstance().RefreshClient(SelectedQuestion, true);
                }
                DialogResult = DialogResult.None;
                GetErrorFocus(ex);
                MessageForm.Show(ex.Message, MessageFormType.Error);
                buttonOkPressed = false;
            }
            catch (Exception ex)
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, ex);
                buttonOkPressed = false;
            }
        }

        private void GetErrorFocus(FaultException ex)
        {
            if (ex.Message.ToLower().Contains(ResourceLoader.GetString2("QuestionMnemonic").ToLower())) 
            {
                if (behaviorType == FormBehavior.Create) 
                {
                    textBoxExMnemonic.Text = textBoxExMnemonic.Text;
                }
                textBoxExMnemonic.Focus();
            }
            if (ex.Message.ToLower().Contains(ResourceLoader.GetString2("$Message.Name").ToLower())) 
            {
                if (behaviorType == FormBehavior.Create)
                {
                    textBoxExName.Text = textBoxExName.Text;
                }
                textBoxExName.Focus();
            }
        }

        private bool buttonOkPressed = false;

        public bool ButtonOkPressed
        {
            get { return buttonOkPressed; }
        }

        private void buttonOk_Click1()
        {
            buttonOkPressed = true;
            if (behaviorType == FormBehavior.Create)
            {
                selectedQuestion = new QuestionClientData();
                selectedQuestion.CustomCode = Guid.NewGuid().ToString();
            }
            if (selectedQuestion != null)
            {
                FormUtil.InvokeRequired(this,
                    delegate
                    {
                        selectedQuestion.Text = textBoxExName.Text;
                        selectedQuestion.ShortText = textBoxExMnemonic.Text;
                        selectedQuestion.RequirementType = checkBoxExPriority.Checked ? RequirementTypeClientEnum.Recommended : RequirementTypeClientEnum.None;
                        selectedQuestion.IncidentTypes = GetIncidentTypes();
                        IList newPossibleAnswers = GetPossibleAnswers();
                        if (behaviorType == FormBehavior.Create)
                        {
                            ArrayList set = new ArrayList();
                            set.Add(UserApplicationClientData.Administration);
                            set.Add(UserApplicationClientData.FirstLevel);
                            selectedQuestion.Application = set;
                        }
                        selectedQuestion.PossibleAnswers = newPossibleAnswers;
                        selectedQuestion.RenderMethod = GetRenderMethod();
                    });   
            
                ServerServiceClient.GetInstance().SaveOrUpdateClientData(selectedQuestion);
            }
        }

        private IList GetIncidentTypes()
        {
            IList result = new ArrayList();
            if (selectedQuestion.IncidentTypes != null)
                ((ArrayList)result).AddRange(selectedQuestion.IncidentTypes);
            foreach (IncidentTypeClientData incidentType in listBoxAssigned.Items)
            {
                IncidentTypeQuestionClientData incidentTypeQuestion = new IncidentTypeQuestionClientData();
                incidentTypeQuestion.IncidentType = incidentType;
                incidentTypeQuestion.Question = SelectedQuestion;
                if(result.Contains(incidentTypeQuestion)==false)
					result.Add(incidentTypeQuestion);
            }
            if (selectedQuestion.IncidentTypes != null)
            {
                foreach (IncidentTypeQuestionClientData itq in selectedQuestion.IncidentTypes)
                {
                    if (listBoxAssigned.Items.Contains(itq.IncidentType) == false)
                    {
                        result.Remove(itq);
                    }
                }
            }
            return result;
        }

        private IList GetPossibleAnswers()
        {
            IList possibleAnswers = new ArrayList();
            QuestionPossibleAnswerClientData possibleAnswer;

            foreach (string var in controlsDesignerGridData.Keys)
            {
                LayoutControlItem item = controlsDesignerParents[var];
                possibleAnswer = controlsDesignerGridData[var].Answer;
                possibleAnswer.Description = item.Text;
                possibleAnswer.QuestionCode = selectedQuestion.Code;
                possibleAnswer.Procedure = controlsDesignerGridData[var].Answer.Procedure;
                if(possibleAnswers.Contains(possibleAnswer)==false)
					possibleAnswers.Add(possibleAnswer);
            }
            return possibleAnswers;
        }

        private string GetRenderMethod()
        {
            MemoryStream ms = new MemoryStream();
            this.dragDropLayoutControl1.layoutControl.SaveLayoutToStream(ms);
            ms.Position = 0;
            MemoryStream msTotal = InsertControlsInXML(ms);
            msTotal.Position = 0;
            StreamReader sr = new StreamReader(msTotal);
            string son = sr.ReadToEnd();
            return son;
        }

        private MemoryStream InsertControlsInXML(Stream s) 
        {

            XmlDocument doc = new XmlDocument();
            doc.Load(s);
            XmlNode firstChild = doc.FirstChild.ChildNodes[0];
            foreach (BaseLayoutItem baseLayoutItem in dragDropLayoutControl1.layoutControl.Items)
            {
                if (baseLayoutItem is LayoutControlItem)
                {
                    LayoutControlItem item = (LayoutControlItem)baseLayoutItem;
                    if (item.Control != null)
                    {
                        XmlNode node = doc.CreateNode(XmlNodeType.Element, "Control", "");
                        XmlAttribute control = doc.CreateAttribute("type");
                        control.Value = Assembly.CreateQualifiedName(item.Control.GetType().Assembly.FullName, item.Control.GetType().FullName);
                        XmlAttribute name = doc.CreateAttribute("name");
                        name.Value = item.Control.Name;
                        XmlAttribute itemName = doc.CreateAttribute("itemName");
                        itemName.Value = item.Name;
                        XmlAttribute itemTex = doc.CreateAttribute("itemText");
                        itemTex.Value = item.Text;
                        node.Attributes.Append(itemTex);
                        node.Attributes.Append(itemName);
                        node.Attributes.Append(control);
                        node.Attributes.Append(name);
                        if (item.Control is TextBoxEx)
                        {
                            XmlAttribute itemMultiline = doc.CreateAttribute("multiline");
                            itemMultiline.Value = ((TextBoxEx)item.Control).Multiline.ToString();
                            node.Attributes.Append(itemMultiline);
                        }
                        doc.FirstChild.InsertBefore(node, firstChild);
                    }
                }
            }
            MemoryStream ms = new MemoryStream();
            doc.Save(ms);
            return ms;
        }

        private void FillIncidentTypes()
        {
            listBoxNotAssigned.Items.Clear();
            foreach (IncidentTypeClientData type in ServerServiceClient.GetInstance().SearchClientObjects(typeof(IncidentTypeClientData)))
            {
                bool goes=true;
                //We have to check if all the incidentTypes goes to the list!.
                listBoxNotAssigned.Items.Add(type);
            }
        }


        private void FillAssignedIncidentTypes()
        {
            listBoxAssigned.Items.Clear();
            foreach (IncidentTypeQuestionClientData  type in selectedQuestion.IncidentTypes)
            {
                for (int i = 0; i < listBoxNotAssigned.Items.Count; i++)
                {
                    IncidentTypeClientData incident = listBoxNotAssigned.Items[i] as IncidentTypeClientData;
                    if (incident.Code == type.IncidentType.Code)
                    {
                        listBoxAssigned.Items.Add(incident);
                        listBoxNotAssigned.Items.RemoveAt(i);
                        break;
                    }
                }
            }
        }

        private void buttonAddIncidentType_Click(object sender, EventArgs e)
        {
            ArrayList toDelete = new ArrayList();
            foreach (IncidentTypeClientData incident in listBoxNotAssigned.SelectedItems)
            {
                toDelete.Add(incident);
                listBoxAssigned.Items.Add(incident);
            }
            foreach (IncidentTypeClientData incident in toDelete)
            {
                int index = -1;
                int i = 0;
                foreach (IncidentTypeClientData inc2 in listBoxNotAssigned.Items)
                {
                    if (inc2.Code == incident.Code)
                    {
                        index = i;
                        break;
                    }
                    i++;
                }
                if (index != -1)
                    listBoxNotAssigned.Items.RemoveAt(index);
            }
            QuestionStatusParameters_Change(null, null);
        }

        private void buttonAddAllIncidentType_Click(object sender, EventArgs e)
        {

            foreach (IncidentTypeClientData var in listBoxNotAssigned.Items)
            {
                listBoxAssigned.Items.Add(var);
            }
            
            listBoxNotAssigned.Items.Clear();
            QuestionStatusParameters_Change(null, null);
        }

        private void buttonRemoveIncidentType_Click(object sender, EventArgs e)
        {
            ArrayList toDelete = new ArrayList();
            foreach (IncidentTypeClientData incident in listBoxAssigned.SelectedItems)
            {
                toDelete.Add(incident);
                listBoxNotAssigned.Items.Add(incident);
            }
            foreach (IncidentTypeClientData incident in toDelete)
            {
                int index = -1;
                int i = 0;
                foreach (IncidentTypeClientData inc in listBoxAssigned.Items)
                {
                    if (inc.Code == incident.Code)
                    {
                        index = i;
                        break;
                    }
                    i++;
                }
                if (index != -1)
                    listBoxAssigned.Items.RemoveAt(index);
            }
            QuestionStatusParameters_Change(null, null);
        }

        private void buttonRemoveAllIncidentType_Click(object sender, EventArgs e)
        {

            foreach (IncidentTypeClientData var in listBoxAssigned.Items)
            {
                listBoxNotAssigned.Items.Add(var);
            }
            
            listBoxAssigned.Items.Clear();
            QuestionStatusParameters_Change(null, null);
        }

        private void richTextBoxExtendedProcedure_RichTextBox_TextChanged(object sender, EventArgs e)
        {
            if (gridControlAnswers.SelectedItems.Count > 0)
            {
                GridAnswerData possibleAnswer = ((GridAnswerData)gridControlAnswers.SelectedItems[0]);
                if (richTextBoxExtendedProcedure.RichTextBox.Text != string.Empty)
                {
                    possibleAnswer.Answer.Procedure = richTextBoxExtendedProcedure.RichTextBox.Rtf;
                    //gridControlAnswers.UpdateData(possibleAnswer);
                }
                else
                {
                    possibleAnswer.Answer.Procedure = string.Empty;
                }
            }
        }

        #region Designer
        

        private void FillAnswerGroups()
        {

        }

        void control_KeyDown(object sender, KeyEventArgs e)
        {
            if (sender is Label)
            {
                if (e.Control && e.KeyCode == Keys.Delete)
                {
                    //RemoveControlToAnswerGroupBox(sender as Control);
                }
            }
        }


        #region Mouse Events Control

        private void assignDefaultTextInControls(Control control)
        {
            createdControls = ++createdControls;
            switch (control.GetType().Name)
            {
                case "CheckBox":
                    control.Text = ResourceLoader.GetString2("CheckBoxesName") + createdControls.ToString();
                    break;
                case "Label":
                    control.Text = ResourceLoader.GetString2("LabelName") + createdControls.ToString();
                    break;
                case "TextBox":
                    control.Text = ResourceLoader.GetString2("TextBoxesName") + createdControls.ToString();
                    break;
                case "RadioButton":
                    control.Text = ResourceLoader.GetString2("RadioButtonsName") + createdControls.ToString();
                    break;
                case "Button":
                    control.Text = ResourceLoader.GetString2("ButtonsName") + createdControls.ToString();
                    break;
                default:
                    control.Text = ResourceLoader.GetString2("ControlsName") + createdControls.ToString();
                    break;
            }
        }

        private void control_Validating(object sender, CancelEventArgs e)
        {
            Control control = sender as Control;
            e.Cancel = string.IsNullOrEmpty(control.Text) || CheckSameTextInControls(control);
        }

        private bool CheckSameTextInControls(Control controlToCheck)
        {
            bool result = false;
           
            return result;
        }
        #endregion 



        private void toolStripMenuItemDesignerDelete_Click(object sender, EventArgs e)
        {
            if (contextMenuStripDesigner.SourceControl != null)
            {
                //RemoveControlToAnswerGroupBox(contextMenuStripDesigner.SourceControl);
            }
            else 
            {
                //Control control = null;
                //foreach (string var in controls.Keys)
                //{
                //    //if (controls[var].Focused && propertyGridMain.Visible)
                //    //{
                //    //    control = controls[var];
                //    //    RemoveControlToAnswerGroupBox(control);
                //    //    break;
                //    //}
                //}
            }
            QuestionStatusParameters_Change(null, null);
        }

        #endregion

        private void listBoxNotAssigned_DoubleClick(object sender, EventArgs e)
        {
            buttonAddIncidentType_Click(sender, e);
        }

        private void listBoxAssigned_SelectedIndexChanged(object sender, EventArgs e)
        {
            buttonRemoveIncidentType_Click(sender, e);
        }

        private void listBoxAssigned_DoubleClick(object sender, EventArgs e)
        {
            buttonRemoveIncidentType_Click(sender, e);
        }

        private void textBoxName_GotFocus(object sender, EventArgs e)
        {
            this.AcceptButton = buttonOk;
        }

        private void QuestionForm_Load(object sender, EventArgs e)
        {
            this.richTextBoxExtendedProcedure.ShowBold = true;
            this.richTextBoxExtendedProcedure.ShowCenterJustify = true;
            this.richTextBoxExtendedProcedure.ShowColors = true;
            this.richTextBoxExtendedProcedure.ShowCopy = false;
            this.richTextBoxExtendedProcedure.ShowCut = false;
            this.richTextBoxExtendedProcedure.ShowFont = true;
            this.richTextBoxExtendedProcedure.ShowFontSize = true;
            this.richTextBoxExtendedProcedure.ShowItalic = true;
            this.richTextBoxExtendedProcedure.ShowLeftJustify = true;
            this.richTextBoxExtendedProcedure.ShowOpen = true;
            this.richTextBoxExtendedProcedure.ShowPaste = false;
            this.richTextBoxExtendedProcedure.ShowRedo = true;
            this.richTextBoxExtendedProcedure.ShowRightJustify = true;
            this.richTextBoxExtendedProcedure.ShowSave = false;
            this.richTextBoxExtendedProcedure.ShowStamp = false;
            this.richTextBoxExtendedProcedure.ShowStrikeout = false;
            this.richTextBoxExtendedProcedure.ShowToolBarText = true;
            this.richTextBoxExtendedProcedure.ShowUnderline = true;
            this.richTextBoxExtendedProcedure.ShowUndo = true;

            //Disable the context menu from the textedit.
            repositoryItemTextEdit.ContextMenu = new ContextMenu();

            this.Icon = ResourceLoader.GetIcon("$Icon.Preguntas");
            if (behaviorType == FormBehavior.Create)
            {
                Text = ResourceLoader.GetString2("QuestionFormCreateText");
                buttonOk.Text = ResourceLoader.GetString("QuestionFormCreateButtonOkText");

            }
            else if (behaviorType == FormBehavior.Edit)
            {
                Text = ResourceLoader.GetString2("QuestionFormEditText");
                buttonOk.Text = ResourceLoader.GetString("QuestionFormEditButtonOkText");

                if (selectedQuestion != null && selectedQuestion.RequirementType.Value == RequirementTypeClientEnum.Required)
                {
                    textBoxExMnemonic.Enabled = false;
                    textBoxExName.Enabled = false;
                    propertyGridControl1.Enabled = false;
                    richTextBoxExtendedProcedure.RichTextBox.Enabled = false;
                    gridControlAnswers.Enabled = false;

                    listBoxNotAssigned.Enabled = false;
                    listBoxAssigned.Enabled = false;
                    buttonAddAllIncidentType.Enabled = false;
                    buttonAddIncidentType.Enabled = false;
                    buttonRemoveAllIncidentType.Enabled = false;
                    buttonRemoveIncidentType.Enabled = false;
                    checkBoxExPriority.Enabled = false;
                    int temp = buttonOk.Left;
                    buttonOk.Left = buttonCancel.Left;
                    buttonCancel.Left = temp;
                    buttonOk.Text = ResourceLoader.GetString("QuestionFormEditButtonCancelText");
                    buttonOk.Enabled = false;
                    buttonOk.Visible = false;
                    buttonCancel.Text = ResourceLoader.GetString("QuestionFormEditButtonOkText");
                    this.dragDropLayoutControl1.layoutControl.Enabled = false;
                    this.listView1.Enabled = false;
                }
            }
            
        }
       

        private void richTextBoxExtendedProcedure_RichTextBox_GotFocus(object sender, EventArgs e)
        {
            this.AcceptButton = null;

        }

        private void RichTextBox_LostFocus(object sender, EventArgs e)
        {
            this.AcceptButton = buttonOk;

        }

        private bool CheckControlsOk()
        {
            foreach (LayoutControlItem item in controlsDesignerParents.Values)
            {
                if (string.IsNullOrEmpty(item.Text))
                    return false;
            }
            return true;
        }

        private void listBoxNotAssigned_Enter(object sender, EventArgs e)
        {
            listBoxAssigned.ClearSelected();
        }
        
        private void listBoxAssigned_Enter(object sender, EventArgs e)
        {
            listBoxNotAssigned.ClearSelected();
        }



        LayoutControlItem dragItem = null;

        LayoutControlItem IDragManager.DragItem
        {
            get
            {
                return dragItem;
            }
            set
            {
                dragItem = value;
            }
        }

        void IDragManager.SetDragCursor(DragDropEffects e)
        {
            SetDragCursor(e);
        }

        private void SetDefaultCursor()
        {
            Cursor = Cursors.Default;
        }

        private void SetDragCursor(DragDropEffects e)
        {
            if (e == DragDropEffects.Move)
                Cursor = Cursors.AppStarting;
            if (e == DragDropEffects.Copy)
                Cursor = Cursors.AppStarting;
            if (e == DragDropEffects.None)
                Cursor = Cursors.No;
        }
        private LayoutControlItem GetDragNode(IDataObject data)
        {
            return data.GetData(typeof(LayoutControlItem)) as LayoutControlItem;
        }

        //listView
        private ListViewItem newItem = null;
        private void listView1_MouseDown(object sender, MouseEventArgs e)
        {
            newItem = listView1.GetItemAt(e.X, e.Y);
        }

        private void listView1_MouseMove(object sender, MouseEventArgs e)
        {
            if (newItem == null || e.Button != MouseButtons.Left)
                return;
            dragItem = new LayoutControlItem();
            ++createdControls;
            
            if ((string)newItem.Tag == "RadioButton")
            {
                dragItem.Name = ResourceLoader.GetString2("RadioButtonsName") + createdControls.ToString();
                dragItem.Control = new RadioButton();
                dragItem.Text = newItem.Text;
                dragItem.Control.Name = "RdBttn" + (++controlsCreated["RadioButton"]);
            }
            else if ((string)newItem.Tag == "CheckEdit")
            {
                dragItem.Name = ResourceLoader.GetString2("CheckBoxesName") + createdControls.ToString();
                dragItem.Control = new CheckEdit();
                dragItem.Text = newItem.Text;
                dragItem.Control.Name = "Chckdt" + (++controlsCreated["CheckEdit"]);
            }
            else
            {
                dragItem.Name = ResourceLoader.GetString2("TextBoxesName") + createdControls.ToString();
                dragItem.Control = new TextBoxEx();
                dragItem.Text = newItem.Text;
                dragItem.Control.Name = "TxtBxx" + (++controlsCreated["TextBoxEx"]);
				(dragItem.Control as TextBoxEx).MaxLength = 255;
            }
            
            dragItem.Control.Text = "";
            while (controlsDesignerParents.ContainsKey(dragItem.Control.Name))
            {
                char[] nameArray = dragItem.Control.Name.ToCharArray();
                int number = int.Parse(nameArray[nameArray.Length - 1].ToString()) + 1;
                dragItem.Control.Name = dragItem.Control.Name.Remove(nameArray.Length - 1);
                dragItem.Control.Name = dragItem.Control.Name + number.ToString();
            }
            //controlsDesignerParents.Add(dragItem.Control.Name, dragItem);
            //CreateNewAnswerInGridControl (dragItem.Control);

            //dragItem.Control.Click += new EventHandler(Control_Click);
            currentItems.Add(dragItem);
            listView1.DoDragDrop(dragItem, DragDropEffects.Copy);
            
            QuestionStatusParameters_Change(null, null);
            currentItems.Clear();
        }

        List<LayoutControlItem> currentItems = new List<LayoutControlItem>();

        private void CreateNewAnswerInGridControl(Control control)
        {
            QuestionPossibleAnswerClientData possibleAnswer = new QuestionPossibleAnswerClientData();
            possibleAnswer.Code = 0;
            possibleAnswer.Description = newItem.Text;
            possibleAnswer.ControlName = control.Name;
            GridAnswerData gdata = new GridAnswerData(control.Name,possibleAnswer);
            gridControlAnswers.AddOrUpdateItem(gdata);
            controlsDesignerGridData.Add(control.Name, gdata);
        }

        private void CreateAnswerInGridControl(Control control,string itemName,int codeQpa)
        {
            QuestionPossibleAnswerClientData possibleAnswer = new QuestionPossibleAnswerClientData();
            possibleAnswer.Code = codeQpa;
            possibleAnswer.QuestionCode = selectedQuestion.Code;
            possibleAnswer.Description = itemName;
            possibleAnswer.ControlName = control.Name;
            GridAnswerData gdata = new GridAnswerData(control.Name, possibleAnswer);
            gridControlAnswers.AddOrUpdateItem(gdata);
            controlsDesignerGridData.Add(control.Name, gdata);
        }

        void Control_Click(object sender, EventArgs e)
        {
            Control cntrl = sender as Control;
            if (cntrl != null)
            {
                SelectLayoutItem(controlsDesignerParents[cntrl.Name]);
            }
        }

        private void simpleButtonTrash_DragDrop(object sender, DragEventArgs e)
        {
            if (CanRecycleDragItem())
            {
                Control control = dragItem.Control;
                dragItem.Parent.Remove(dragItem);
                if (control != null)
                {
                    control.Parent = null;
                    control.Dispose();
                }
                dragItem = null;
                DeleteControlFromDesigner(control);
            }
            SetDefaultLabel();
            QuestionStatusParameters_Change(null, null);
        }

        private void DeleteControlFromDesigner(Control control)
        {
            controlsDesignerParents.Remove(control.Name);
            controlsDesignerGridData.Remove(control.Name);
            DeleteGridAnswerData(control);
            propertyGridControl1.Visible = false;
            propertyGridControl1.BeginUpdate();
            propertyGridControl1.Rows.Clear();
            propertyGridControl1.EndUpdate();
        }

        private void DeleteGridAnswerData(Control control)
        {
            GridAnswerData todel = new GridAnswerData(control.Name, new QuestionPossibleAnswerClientData());
            for (int i = gridControlAnswers.Items.Count - 1; i >= 0; i--)
            {
                GridAnswerData aux = gridControlAnswers.Items[i] as GridAnswerData;
                if (aux.Equals(todel))
                {
                    gridControlAnswers.Items.RemoveAt(i);
                    break;
                }
            }
        }

        private void simpleButtonTrash_DragEnter(object sender, DragEventArgs e)
        {
            if (CanRecycleDragItem())
            {
                simpleButtonTrash.ImageIndex = 1;
                e.Effect = DragDropEffects.Copy;
                Cursor = Cursors.Hand;
            }
        }

        private void simpleButtonTrash_DragLeave(object sender, EventArgs e)
        {
            SetDefaultLabel();
        }

        private void SetDefaultLabel()
        {
            simpleButtonTrash.ImageIndex = 0;
            SetDefaultCursor();
        }

        protected bool CanRecycleDragItem()
        {
            if (dragItem == null) return false;
            if (dragItem.Owner == null) return false;
            return true;
        }

        private void dragDropLayoutControl1_MouseClick(object sender, MouseEventArgs e)
        {

        }

        void designerLayoutControl_MouseClick(object sender, MouseEventArgs e)
        {
            LayoutControlItem ite = dragDropLayoutControl1.layoutControl.CalcHitInfo(new Point(e.X, e.Y)).Item as LayoutControlItem;
            if (ite != null && !isquestionWhere)
            {
                SelectLayoutItem(ite);
            }
        }

        private void SelectLayoutItem(LayoutControlItem ite)
        {
            dragDropLayoutControl1.layoutControl.BeginUpdate();
            for (int i = 0; i < dragDropLayoutControl1.layoutControl.Items.Count; i++)
            {
                dragDropLayoutControl1.layoutControl.Items[i].Selected = false;
            }
            ite.Selected = true;
            ite.Control.Focus();
            dragDropLayoutControl1.layoutControl.EndUpdate();
            UpdatePropertyGrid(ite);
            layoutControlGroup3_Shown(null, null);
        }

        private void UpdatePropertyGrid(LayoutControlItem ite)
        {
            propertyGridControl1.Visible = true;
            propertyGridControl1.BeginUpdate();
            propertyGridControl1.Rows.Clear();
            if (ite.Control is TextBox)
                propertyGridControl1.SelectedObject = new TextBoxExProperties(ite.Control, ite,controlsDesignerGridData[ite.Control.Name]);
            else
            {
                propertyGridControl1.SelectedObject = new ControlExProperties(ite.Control, ite, controlsDesignerGridData[ite.Control.Name]);
            }
            propertyGridControl1.EndUpdate();
        }

        private void gridViewAnswers_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (gridControlAnswers.SelectedItems.Count > 0)
            {
                this.richTextBoxExtendedProcedure.Enabled = true;

                GridAnswerData possibleAnswer = ((GridAnswerData)gridControlAnswers.SelectedItems[0]);

                if (possibleAnswer.Answer.Procedure != null && possibleAnswer.Answer.Procedure != string.Empty)
                {
                    if (ApplicationUtil.IsRTF(possibleAnswer.Answer.Procedure) == true)
                        richTextBoxExtendedProcedure.RichTextBox.Rtf = possibleAnswer.Answer.Procedure;
                    else
                        richTextBoxExtendedProcedure.RichTextBox.Text = possibleAnswer.Answer.Procedure;
                }
                else
                {
                    richTextBoxExtendedProcedure.RichTextBox.Text = "";
                }
            }
            else
            {
                this.richTextBoxExtendedProcedure.Enabled = false;
            }
        }

        private void layoutControlGroup3_Shown(object sender, EventArgs e)
        {
            gridViewAnswers_FocusedRowChanged(null, null);
            gridViewAnswers.BeginDataUpdate();
            gridViewAnswers.EndDataUpdate();
        }

		private void simpleButtonTrash_Click(object sender, EventArgs e)
		{
			foreach (LayoutControlItem item in dragDropLayoutControl1.layoutControl.Root.Items)
			{
				if (item.Selected == true)
				{
					dragItem = item;
					break;
				}
			}

			simpleButtonTrash_DragDrop(null, null);
		}
    }

}
