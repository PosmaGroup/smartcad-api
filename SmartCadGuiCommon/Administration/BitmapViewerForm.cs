using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using SmartCadCore.Core;
using SmartCadCore.Common;
using System.IO;
using SmartCadControls.Controls;

namespace SmartCadGuiCommon
{
    public partial class BitmapViewerForm : XtraForm
    {
        #region Members
        private string m_fileName;
        private string mfilebydefect;
        private Image m_image;
        #endregion

        #region Propierties

        public string FileName
        {
            get { return m_fileName; }
            set { m_fileName = value; }
        }

        public string FileByDefect
        {
            get { return mfilebydefect; }
            set { mfilebydefect = value; }
        }

        public Image Icon
        {
            get { return m_image; }
            set { m_image = value; }
        }


        #endregion

        public BitmapViewerForm()
        {
            InitializeComponent();
            bitmapViewer1.Directory = Application.StartupPath + SmartCadConfiguration.MapsIconsFolder;
        }

        private void bitmapViewer1_PictureSelected(object sender, PictureSelectedEventArgs e)
        {              
            //FileName = FileByDefect;
            if (!string.IsNullOrEmpty(e.FileName))
            {
                if (FileName != e.FileName && string.IsNullOrEmpty(FileName) == false)
                {
                    (this.bitmapViewer1.PnlPictures.Controls[FileName] as PictureBox).BorderStyle = BorderStyle.FixedSingle;
                    (this.bitmapViewer1.PnlPictures.Controls[FileName] as PictureBox).DisplayRectangle.Inflate(0, 0);
                    (this.bitmapViewer1.PnlPictures.Controls[FileName] as PictureBox).BackColor = SystemColors.Window;
                }
                FileName = e.FileName;
            }
            Icon = e.Image;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            FileName = FileByDefect;
        }

        private void bitmapViewer1_Load(object sender, EventArgs e)
        {
            this.buttonOK.Select();
            LoadLanguage();
            if (bitmapViewer1.images.Count > 0)
            {
                BitmapViewer.NamedImage namedImage = (BitmapViewer.NamedImage)bitmapViewer1.images[0];

                var b = bitmapViewer1.PnlPictures.Controls[0].Location;
                bitmapViewer1.PnlPictures.Controls[0].BackColor = SystemColors.GradientActiveCaption;
                bitmapViewer1.PnlPictures.Controls[0].Select();
                //b = bitmapViewer1.PnlPictures.Controls[0].Controls[0].Location;
                PictureBox pic = new PictureBox();
                pic.Name = namedImage.FileName;
                pic.Image = namedImage.Image;
                pic.Tag = namedImage.FileName;
                pic.Size = new Size(bitmapViewer1.Dimension, bitmapViewer1.Dimension);
                pic.Location = new Point(90, 5);
                pic.BorderStyle = BorderStyle.FixedSingle;
                // StretchImage mode gives us the "thumbnail" ability.
                pic.SizeMode = PictureBoxSizeMode.CenterImage;
                pic.BorderStyle = BorderStyle.Fixed3D;
                pic.DisplayRectangle.Inflate(15, 15);
                pic.BackColor = SystemColors.GradientActiveCaption;
                MouseEventArgs mouseEvent = new MouseEventArgs(System.Windows.Forms.MouseButtons.Left, 1, 90, 5, 0);
                bitmapViewer1.pic_Click(pic, mouseEvent, 0);
            }
        }


        private void LoadLanguage()
        {
            this.Text = ResourceLoader.GetString2(Name);
            this.buttonOK.Text = ResourceLoader.GetString2("Accept");
            this.buttonCancel.Text = ResourceLoader.GetString2("Cancel");
        }
    }
}