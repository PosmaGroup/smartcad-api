using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Smartmatic.SmartCad.Service;
using System.Collections;
using DevExpress.XtraEditors.Repository;
using SmartCadCore.ClientData;
using SmartCadCore.Enums;
using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadControls.Controls;
using SmartCadControls;


namespace SmartCadGuiCommon
{
    public partial class GPSForm: GPSBaseForm
    {
        private IList GPSTypes;
        private IList sensorsList;
        private RepositoryItemComboBox sensorCombo = new RepositoryItemComboBox();

        #region Constructors
        public GPSForm()
        {
            InitializeComponent();
        }

        public GPSForm(AdministrationRibbonForm form)
			: base(form)
		{
			InitializeComponent();
		}

		public GPSForm(AdministrationRibbonForm form, GPSClientData data, FormBehavior behaviour)
			: this(form)
		{
            this.behavior = behaviour;
            InitializeGrid();
            FillBrands();
            LoadSensors();
            SelectedData = data;
        }
        #endregion

        #region Properties
        public override GPSClientData SelectedData
        {
            get
            {
                return data;
            }
            set
            {
                data = value;

                if (data != null)
                {
                    textEditId.Text = data.Name;
                    int index = comboBoxEditBrand.Properties.Items.IndexOf(data.Type.Brand);
                    if (index != -1)
                        comboBoxEditBrand.SelectedIndex = index;
                    index = comboBoxEditModel.Properties.Items.IndexOf(data.Type);
                    if (index != -1)
                    comboBoxEditModel.SelectedIndex= index;

                    if (data.Sensors != null && data.Sensors.Count > 0)
                    {
                        SensorClientData sensor;
                        foreach (GPSPinSensorClientData sensorPin in data.Sensors)
                        {
                            for (int i = 0; i < gridControlExIOs.Items.Count; i++)
                            {
                                GridControlDataIOs gridData = gridControlExIOs.Items[i] as GridControlDataIOs;
                                if (gridData.Pin.Code == sensorPin.PinCode) 
                                {
                                    gridData.SensorPinCode = sensorPin.Code;
                                    gridData.SensorPinVersion = sensorPin.Version;
                                    sensor = new SensorClientData();
                                    sensor.Code = sensorPin.SensorCode;
                                    sensor.Name = sensorPin.SensorName;
                                    gridData.Sensor = sensor;
                                  //  gridViewExIOs.SetRowCellValue(i, "Sensor", sensorPin.SensorName); 
                                    break;
                                }  
                            }

                        }

                    }

                }

                CheckButtons();
            }
        }

        protected override string UpdateMessage
        {
            get { return "UpdateFormGPSData"; }
        }

        protected override string DeleteMessage
        {
            get { return "DeleteFormGPSData"; }
        }

        protected override string CreateFormText
        {
            get { return "CreateGPSFormText"; }
        }

        protected override string EditFormText
        {
            get { return "EditGPSFormText"; }
        }
        #endregion
                
        #region Methods

        protected override void LoadLanguage()
        {
            this.Icon = ResourceLoader.GetIcon("$Icon.GPS");
            layoutControlItemId.Text = ResourceLoader.GetString2("GPSName") + ":*";
            layoutControlItemBrand.Text = ResourceLoader.GetString2("Brand") + ":*";
            layoutControlItemModel.Text = ResourceLoader.GetString2("Model") + ":*";
            layoutControlGroupData.Text = ResourceLoader.GetString2("GPSInformation");
            layoutControlGroupIOs.Text = ResourceLoader.GetString2("Inputs_Outputs");
            simpleButtonCancel.Text = ResourceLoader.GetString2("Cancel");

            if (behavior == FormBehavior.Create)
                simpleButtonAccept.Text = ResourceLoader.GetString2("Create");
            else
                simpleButtonAccept.Text = ResourceLoader.GetString2("Accept");

        }

        private void InitializeGrid()
        {
           gridControlExIOs.Type = typeof(GridControlDataIOs);
           gridControlExIOs.ViewTotalRows = true;
           gridControlExIOs.EnableAutoFilter = true;
           sensorCombo.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
           gridViewExIOs.Columns["Sensor"].ColumnEdit = sensorCombo;
           gridViewExIOs.OptionsBehavior.Editable = true;
           gridViewExIOs.OptionsView.ColumnAutoWidth = true;
           gridViewExIOs.Columns["Sensor"].OptionsColumn.AllowEdit = true;
           gridViewExIOs.Columns["Name"].OptionsColumn.AllowEdit = false;
        }

        private void LoadSensors() 
        {
            sensorsList = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetSensors);
        }
        private void FillBrands()
        {
            GPSTypes = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetGPSTypes);
            if (GPSTypes != null)
            {
                foreach (GPSTypeClientData type in GPSTypes)
                {
                    if (comboBoxEditBrand.Properties.Items.Contains(type.Brand) == false)
                        comboBoxEditBrand.Properties.Items.Add(type.Brand);
                }
            }

        }

        protected override void CheckButtons()
        {
            if (textEditId.Text.Trim() == string.Empty || comboBoxEditBrand.SelectedIndex < 0 || comboBoxEditModel.SelectedIndex < 0)
                simpleButtonAccept.Enabled = false;
            else
                simpleButtonAccept.Enabled = true;
        }

        protected override void Save_Help()
        {
            buttonOkPressed = true;
            if (behavior == FormBehavior.Create)
                data = new GPSClientData();
            data.Name = textEditId.Text;
            data.Type = comboBoxEditModel.SelectedItem as GPSTypeClientData;
      
            GPSPinSensorClientData sensorPin;
            data.Sensors = new ArrayList();
            
            foreach (GridControlDataIOs gridData in gridControlExIOs.Items)
            {
                if (gridData.Sensor != null && gridData.Sensor.Code != 0) 
                {
                    sensorPin = new GPSPinSensorClientData();
                    sensorPin.Code = gridData.SensorPinCode;
                    sensorPin.SensorCode = gridData.Sensor.Code;
                    sensorPin.GPSCode = data.Code;
                    sensorPin.PinCode = gridData.Pin.Code;
                    sensorPin.SensorName = gridData.Sensor.Name;
                    sensorPin.Version = gridData.SensorPinVersion;
                    data.Sensors.Add(sensorPin);
                }
            }
          
            ServerServiceClient.GetInstance().SaveOrUpdateClientData(data);
            buttonOkPressed = false;
        }

        protected override void LoadData()
        {

        }

        protected override void GetErrorFocus(System.ServiceModel.FaultException ex)
        {
            if (ex.Message.Contains(ResourceLoader.GetString2("Name").ToLower()) == true)
                textEditId.Focus();
        }
        #endregion

        #region Events
        private void simpleButtonAccept_Click(object sender, EventArgs e)
        {
            ButtonAccept_Click(sender, e);
        }

        private void comboBoxEditBrand_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBoxEditModel.SelectedIndex = -1;
            comboBoxEditModel.Properties.Items.Clear();
            if (comboBoxEditBrand.SelectedIndex != -1)
            {
                foreach (GPSTypeClientData gpsType in GPSTypes)
                {
                    if (comboBoxEditBrand.Text.Trim() == gpsType.Brand.Trim())
                        comboBoxEditModel.Properties.Items.Add(gpsType);
                }
            }

            CheckButtons();
        }

       
        private void comboBoxEditModel_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxEditModel.SelectedIndex != -1)
            {
                BindingList<GridControlDataIOs> dataSource = new BindingList<GridControlDataIOs>();
                GPSTypeClientData gpsType = comboBoxEditModel.SelectedItem as GPSTypeClientData;

                foreach (GPSPinClientData pin in gpsType.Pines)
                    dataSource.Add(new GridControlDataIOs(pin));

                gridControlExIOs.BeginUpdate();
                gridControlExIOs.DataSource = dataSource;
                gridControlExIOs.EndUpdate();
            }
            else
            {
                gridControlExIOs.BeginUpdate();
                gridControlExIOs.DataSource = null;
                gridControlExIOs.EndUpdate();
            
            }
            CheckButtons();
        }

        /// <summary>
        /// Invoke the CheckButtons method.
        /// Method to subscribe to control events.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CheckButtonsEventHandler(object sender, EventArgs e)
        {
            CheckButtons();
        }
        #endregion

    
        private void gridViewExIOs_ShowingEditor(object sender, CancelEventArgs e)
        { 
            sensorCombo.Items.Clear();
            if (gridViewExIOs.FocusedRowHandle > -1)
            {
                sensorCombo.Items.Add(new SensorClientData());
                GridControlDataIOs gridData = gridControlExIOs.SelectedItems[0] as GridControlDataIOs;
                if (gridViewExIOs.FocusedColumn.FieldName == "Sensor")
                {
                    foreach (SensorClientData sensor in sensorsList)
                    {
                        if ((int)sensor.Type == (int)gridData.Pin.Type)
                            sensorCombo.Items.Add(sensor);
                    }
                }
            } 
        }
       

    }

    public class GPSBaseForm : XtraFormAdmistration<GPSClientData>
    {
        public GPSBaseForm()
        {
        }

        public GPSBaseForm(AdministrationRibbonForm form)
            : base(form)
        {
        }

        public GPSBaseForm(AdministrationRibbonForm form, GPSClientData data, FormBehavior behaviour)
            : base(form, data, behaviour)
        {
        }
    }

    public class GridControlDataIOs : GridControlData
    {
        public GridControlDataIOs(GPSPinClientData pin)
            : base(pin)
        {
          
        }

        public GPSPinClientData Pin
        {
            get
            {
                return Tag as GPSPinClientData;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string Name
        {
            get
            {
                return ResourceLoader.GetString2(Pin.Type.ToString()) + " " + Pin.Number;
            }
        }
                

        [GridControlRowInfoData(Searchable = true, RepositoryType = typeof(RepositoryItemComboBox))]
        public SensorClientData Sensor
        {
            get;
            set;
        }

        public int SensorPinCode 
        {
            get;
            set;        
        }

        public int SensorPinVersion
        {
            get;
            set;
        }

        
        public override bool Equals(object obj)
        {
            if (obj is GridControlDataIOs == false)
                return false;

            GridControlDataIOs gridData = obj as GridControlDataIOs;

            return (gridData.Tag as GPSPinClientData).Equals(Tag);
        }
    }
}