using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.Text.RegularExpressions;

using Smartmatic.SmartCad.Service;
using System.ServiceModel;
using System.Collections;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadCore.Enums;


namespace SmartCadGuiCommon
{
    public partial class OfficerForm : DevExpress.XtraEditors.XtraForm
    {
        OfficerClientData selectedOfficer = null;
        FormBehavior behaviorType = FormBehavior.Edit;
        bool wasChanged = false;
        private AdministrationRibbonForm parentForm;
        private object syncCommitted = new object();

        public OfficerForm()
        {
            InitializeComponent();
            LoadLanguage();
            FillDepartmentTypes();
        }

        public OfficerForm(AdministrationRibbonForm parentForm, OfficerClientData officer, FormBehavior type)
            :this()
		{
            this.parentForm = parentForm;
            if (this.parentForm != null)
            {
                this.parentForm.AdministrationCommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(OfficerForm_AdministrationCommittedChanges);
            }
            this.FormClosing += new FormClosingEventHandler(OfficerForm_FormClosing);
			BehaviorType = type;
			SelectedOfficer = officer;
		}

        void OfficerForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.parentForm != null)
            {
                this.parentForm.AdministrationCommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(OfficerForm_AdministrationCommittedChanges);
            }
        }

        void OfficerForm_AdministrationCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    lock (syncCommitted)
                    {
                        try
                        {
                            if (ServerServiceClient.GetInstance().OperatorClient == null)
                            { }
                        }
                        catch
                        { }

                        if (e.Objects[0] is RankClientData)
                        {
                            #region RankClientData
                            RankClientData rankData = e.Objects[0] as RankClientData;

                            if (e.Action == CommittedDataAction.Save)
                            {
                                FormUtil.InvokeRequired(this.comboBoxRank,
                               delegate
                               {
                                   comboBoxRank.Items.Add(rankData);
                               });
                            }
                            else if (e.Action == CommittedDataAction.Update)
                            {
                                FormUtil.InvokeRequired(this.comboBoxRank,
                               delegate
                               {
                                   bool select = (((RankClientData)comboBoxRank.SelectedItem).Code == rankData.Code);
                                   comboBoxRank.Items.Remove(rankData);
                                   comboBoxRank.Items.Add(rankData);
                                   if (select == true)
                                   {
                                       comboBoxRank.SelectedItem = rankData;
                                   }
                               });
                            }
                            else if (e.Action == CommittedDataAction.Delete)
                            {
                                FormUtil.InvokeRequired(this.comboBoxRank,
                               delegate
                               {
                                   bool select = (((RankClientData)comboBoxRank.SelectedItem).Code == rankData.Code);
                                   comboBoxRank.Items.Remove(rankData);
                                   if (select == true)
                                   {
                                       comboBoxRank.SelectedIndex = -1;
                                       if (selectedOfficer != null)
                                       {
                                           selectedOfficer.Rank = null;
                                       }
                                   }
                               });
                            }
                            #endregion
                        }
                        else if (e.Objects[0] is PositionClientData)
                        {
                            #region PositionClientData
                            PositionClientData positionData = e.Objects[0] as PositionClientData;
                            
                            if (e.Action == CommittedDataAction.Save)
                            {
                                FormUtil.InvokeRequired(this.comboBoxPosition,
                               delegate
                               {
                                   comboBoxPosition.Items.Add(positionData);
                               });
                            }
                            else if (e.Action == CommittedDataAction.Update)
                            {
                                FormUtil.InvokeRequired(this.comboBoxPosition,
                               delegate
                               {
                                   bool select = (((PositionClientData)comboBoxPosition.SelectedItem).Code == positionData.Code);
                                   comboBoxPosition.Items.Remove(positionData);
                                   comboBoxPosition.Items.Add(positionData);
                                   if (select == true)
                                   {
                                       comboBoxPosition.SelectedItem = positionData;
                                   }
                               });
                            }
                            else if (e.Action == CommittedDataAction.Delete)
                            {
                                FormUtil.InvokeRequired(this.comboBoxPosition,
                               delegate
                               {
                                   bool select = (((PositionClientData)comboBoxPosition.SelectedItem).Code == positionData.Code);
                                   comboBoxPosition.Items.Remove(positionData);
                                   if (select == true)
                                   {
                                       comboBoxPosition.SelectedIndex = -1;
                                       if (selectedOfficer != null)
                                       {
                                           selectedOfficer.Position = null;
                                       }
                                   }
                               });
                            }
                            #endregion
                        }
                        else if (e.Objects[0] is DepartmentTypeClientData)
                        {
                            #region DepartmentTypeClientData
                            DepartmentTypeClientData departmentType =
                                    e.Objects[0] as DepartmentTypeClientData;

                            DepartmentTypeClientData depTypeData = new DepartmentTypeClientData();
                            depTypeData.Code=departmentType.Code;
                            depTypeData = (DepartmentTypeClientData)ServerServiceClient.GetInstance().RefreshClient(depTypeData);

                            if (e.Action == CommittedDataAction.Save)
                            {
                                FormUtil.InvokeRequired(this.comboBoxDepartmentType,
                                delegate
                                {
                                    comboBoxDepartmentType.Items.Add(depTypeData);
                                });
                            }
                            else if (e.Action == CommittedDataAction.Update)
                            {
                                FormUtil.InvokeRequired(this.comboBoxDepartmentType,
                               delegate
                               {
                                   bool select = (((DepartmentTypeClientData)comboBoxDepartmentType.SelectedItem).Code == depTypeData.Code);
                                   comboBoxDepartmentType.Items.Remove(depTypeData);
                                   comboBoxDepartmentType.Items.Add(depTypeData);
                                   if (select == true)
                                   {
                                       comboBoxDepartmentType.SelectedItem = depTypeData;
                                   }
                               });
                            }
                            #endregion
                        }
                        else if (e.Objects[0] is DepartmentZoneClientData)
                        {
                            #region DepartmentZoneClientData
                            DepartmentZoneClientData departmentZone =
                                    e.Objects[0] as DepartmentZoneClientData;

                            DepartmentZoneClientData depZoneData = new DepartmentZoneClientData();
                            depZoneData.Code = departmentZone.Code;
                            depZoneData = (DepartmentZoneClientData)ServerServiceClient.GetInstance().RefreshClient(depZoneData);

                            if (e.Action == CommittedDataAction.Save)
                            {
                                FormUtil.InvokeRequired(this.comboBoxZoneType,
                               delegate
                               {
                                   comboBoxZoneType.Items.Add(depZoneData);
                               });
                            }
                            else if (e.Action == CommittedDataAction.Update)
                            {
                                FormUtil.InvokeRequired(this.comboBoxZoneType,
                               delegate
                               {
                                   bool select = (((DepartmentZoneClientData)comboBoxZoneType.SelectedItem).Code == depZoneData.Code);
                                   comboBoxZoneType.Items.Remove(depZoneData);
                                   comboBoxZoneType.Items.Add(depZoneData);
                                   if (select == true)
                                   {
                                       comboBoxZoneType.SelectedItem = depZoneData;
                                   }
                               });
                            }
                            #endregion
                        }
                        else if (e.Objects[0] is DepartmentStationClientData)
                        {
                            #region DepartmentStationClientData
                            DepartmentStationClientData departmentStation = e.Objects[0] as DepartmentStationClientData;

                            DepartmentStationClientData depStationData = new DepartmentStationClientData();
                            depStationData.Code = departmentStation.Code;
                            depStationData = (DepartmentStationClientData)ServerServiceClient.GetInstance().RefreshClient(depStationData);

                            if (e.Action == CommittedDataAction.Save)
                            {
                                FormUtil.InvokeRequired(this.comboBoxStationType,
                               delegate
                               {
                                   comboBoxStationType.Items.Add(depStationData);
                               });
                            }
                            else if (e.Action == CommittedDataAction.Update)
                            {
                                FormUtil.InvokeRequired(this.comboBoxZoneType,
                               delegate
                               {
                                   bool select = (((DepartmentStationClientData)comboBoxStationType.SelectedItem).Code == depStationData.Code);
                                   comboBoxStationType.Items.Remove(depStationData);
                                   comboBoxStationType.Items.Add(depStationData);
                                   if (select == true)
                                   {
                                       comboBoxStationType.SelectedItem = depStationData;
                                   }
                               });
                            }
                            #endregion
                        }
                        else if (e.Objects[0] is OfficerClientData)
                        {
                            #region OfficerClientData
                            OfficerClientData officer =
                                    e.Objects[0] as OfficerClientData;

                            if (SelectedOfficer != null && SelectedOfficer.Equals(officer) && ButtonOkPressed == false)
                            {
                                if (e.Action == CommittedDataAction.Update)
                                {
                                    MessageForm.Show(ResourceLoader.GetString2("UpdateFormOfficerData"), MessageFormType.Warning);
                                    SelectedOfficer = officer;
                                }
                                else if (e.Action == CommittedDataAction.Delete)
                                {
                                    MessageForm.Show(ResourceLoader.GetString2("DeleteFormOfficerData"), MessageFormType.Warning);
                                    FormUtil.InvokeRequired(this,
                                    delegate
                                    {
                                        this.Close();
                                    });
                                }
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        public FormBehavior BehaviorType
        {
            get
            {
                return behaviorType;
            }
            set
            {
               
                behaviorType = value;
                Clear();
            }
        }

        public OfficerClientData SelectedOfficer
        {
            get
            {
                return selectedOfficer;
            }
            set
            {

                Clear();
                selectedOfficer = value;
                if (selectedOfficer != null)
                {


                    if (textBoxId.IsValid(selectedOfficer.PersonID))
                    {
                        textBoxId.Text = selectedOfficer.PersonID;
                    }
                    else
                    {
                        string ErrorInLoadXLSOfficerID = ResourceLoader.GetString2("ErrorInLoadXLSOfficerID");
                        MessageForm.Show(ErrorInLoadXLSOfficerID, MessageFormType.Warning);
                        textBoxId.Text = "";
                    }
                    textBoxName.Text = selectedOfficer.FirstName;
                    textBoxLastName.Text = selectedOfficer.LastName;
                    chargeRankFromDepartment(selectedOfficer.DepartmentType.Name);
                    chargePositionFromDepartment(selectedOfficer.DepartmentType.Name);

                    if (selectedOfficer.Birthday.HasValue == true)
                        dateTimePicker1.Value = selectedOfficer.Birthday.Value;                    
                    if (selectedOfficer.Rank != null)
                    {                        
                        comboBoxRank.SelectedItem = selectedOfficer.Rank;
                    }
                    if (selectedOfficer.Position != null)
                    {
                        comboBoxPosition.SelectedItem = selectedOfficer.Position;
                    }

                    comboBoxDepartmentType.SelectedItem = selectedOfficer.DepartmentType;

                    if (selectedOfficer.DepartmentType != null && selectedOfficer.DepartmentStation!= null && selectedOfficer.DepartmentStation.DepartmentZone != null)
                    {
                        FillCmbBoxZone(selectedOfficer.DepartmentType);
                        FillCmbBoxStation(selectedOfficer.DepartmentStation.DepartmentZone);

                        comboBoxZoneType.SelectedItem = selectedOfficer.DepartmentStation.DepartmentZone;
                        comboBoxStationType.SelectedItem = selectedOfficer.DepartmentStation;
                    }
                    textBoxBadge.Text = selectedOfficer.Badge;
                    textBoxTelephone.Text = selectedOfficer.Telephone;
                }
                OfficerParameters_Change(null, null);
            }
        }

        private void LoadLanguage() 
        {
            this.Icon = ResourceLoader.GetIcon("$Icon.Oficiales");
           
            this.layoutControlGroupOfficer.Text = ResourceLoader.GetString2("OfficerInformation");
            this.layoutControlGroupDepartment.Text = ResourceLoader.GetString2("DepartmentTypeInformation");

            this.textBoxBadgeitem.Text = ResourceLoader.GetString2("OfficerBadge") + ":*";
            this.textBoxIditem.Text = ResourceLoader.GetString2("OfficerID") + ":*";
            this.textBoxLastNameitem.Text = ResourceLoader.GetString2("LastName") + ":*";
            this.textBoxNameitem.Text = ResourceLoader.GetString2("VariationFirstName") + ":*";
            this.textBoxTelephoneitem.Text = ResourceLoader.GetString2("Phone") + ":";
            this.comboBoxDepartmentTypeitem.Text = ResourceLoader.GetString2("Department") + ":*";
            this.comboBoxPositionitem.Text = ResourceLoader.GetString2("Position") + ":";
            this.comboBoxStationTypeitem.Text = ResourceLoader.GetString2("Station") + ":*";
            this.comboBoxZoneTypeitem.Text = ResourceLoader.GetString2("Zone") + ":*";
            this.comboBoxRankitem.Text = ResourceLoader.GetString2("Rank") + ":";
            this.dateTimePicker1item.Text = ResourceLoader.GetString2("DateofBirth") + ":*";
            this.buttonCancel.Text = ResourceLoader.GetString2("$Message.Cancel");
        }
       
        private void Clear()
        {
            textBoxId.Clear();
            textBoxName.Clear();
            textBoxLastName.Clear();
            dateTimePicker1.Value = DateTime.Today;
            comboBoxDepartmentType.SelectedIndex = -1;            
            textBoxBadge.Clear();
            textBoxTelephone.Clear();
            clearRankPosition();
        }

        private void OfficerParameters_Change(object sender, System.EventArgs e)
        {
            if ((string.IsNullOrEmpty(textBoxId.Text.Trim())) ||
                (string.IsNullOrEmpty(textBoxName.Text.Trim()) ) ||
                (string.IsNullOrEmpty(textBoxLastName.Text.Trim())) ||
                (ApplicationUtil.IsAgeValid(dateTimePicker1.Value, 18)) ||
                (comboBoxDepartmentType.SelectedIndex == -1) ||
                (comboBoxStationType.SelectedIndex == -1) ||
                (comboBoxZoneType.SelectedIndex == -1) ||
                (string.IsNullOrEmpty(textBoxBadge.Text.Trim())))
                buttonOk.Enabled = false;
            else
                buttonOk.Enabled = true;
            wasChanged = this.IsHandleCreated;
        }

        private void DateTimePicker1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 110)
            {
                e.SuppressKeyPress = true;
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            bool updateOfficer = true;
            if (behaviorType == FormBehavior.Edit)
            {
                DepartmentStationClientData newStation = (DepartmentStationClientData)comboBoxStationType.SelectedItem;
                if (CheckFutureAssings(selectedOfficer))
                    return;
            }
            if (wasChanged == false)
                return;
            Update:
            try
            {                
                BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[1] { GetType().GetMethod("buttonOk_Click1", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[0] });
                processForm.CanThrowError = true;
                processForm.ShowDialog();
            }
            catch (FaultException ex)
            {
                if (updateOfficer && wasChanged)
                {
                    updateOfficer = false;
                    goto Update;
                }
                if (behaviorType == FormBehavior.Edit)
                    SelectedOfficer = (OfficerClientData)ServerServiceClient.GetInstance().RefreshClient(SelectedOfficer);

                GetErrorFocus(ex);
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, MessageFormType.Error);
                buttonOkPressed = true;
            }
            catch (Exception ex)
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, ex);
                buttonOkPressed = true;
            }
        }

        private void GetErrorFocus(FaultException ex)
        {
            if (ex.Message.Contains(ResourceLoader.GetString2("PersonID").ToLower()))
            {
                if (behaviorType == FormBehavior.Create) 
                {
                    textBoxId.Text = textBoxId.Text;
                }
                textBoxId.Focus();
            }
            if (ex.Message.Contains(ResourceLoader.GetString2("SameBadges"))) 
            {
                if (behaviorType == FormBehavior.Create) 
                {
                    textBoxBadge.Text = textBoxBadge.Text;
                }
                textBoxBadge.Focus();
            }
        }

        private bool CheckFutureAssings(OfficerClientData officer)
        {
        
            if (HaveOfficerFutureAssings(officer.Code))
            {
                if (MessageForm.Show(ResourceLoader.GetString2("OfficerWithAssigns"), MessageFormType.WarningQuestion) == DialogResult.Yes)
                {
                    return false;
                }
                else {
                    DialogResult = DialogResult.None;
                    return true;
                }
             }

            return false;                    
        }

        private bool HaveOfficerFutureAssings(int officerCode)
        {

            string DateNHibernateFormat = "yyyyMMdd HH:mm";
            string date = ServerServiceClient.GetInstance().GetTime().ToString(DateNHibernateFormat);
            string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.CountUnitOfficerFutureAssignsByOfficerCode, officerCode, date);
            IList list = (IList)ServerServiceClient.GetInstance().SearchBasicObjects(hql);

            if (list.Count > 0)
            {
                long count = (long)list[0];
                if (count > 0)
                {
                    return true;
                }
            }
            return false;
        }

        private bool buttonOkPressed = false;

        public bool ButtonOkPressed
        {
            get { return buttonOkPressed; }
        }

        private void buttonOk_Click1()
        {
            buttonOkPressed = true;
            if (behaviorType == FormBehavior.Create)
                selectedOfficer = new OfficerClientData();
            if (selectedOfficer != null)
            {
                selectedOfficer.FirstName = textBoxName.Text;
                selectedOfficer.LastName = textBoxLastName.Text;
                selectedOfficer.Birthday = dateTimePicker1.Value;
                
                FormUtil.InvokeRequired(comboBoxDepartmentType, delegate
                {
                   
                    selectedOfficer.PersonID = textBoxId.Text;

                    if (comboBoxRank.SelectedIndex > 0)
                        selectedOfficer.Rank = (RankClientData)comboBoxRank.SelectedItem;
                    else
                        selectedOfficer.Rank = null;

                    if (comboBoxPosition.SelectedIndex > 0)
                        selectedOfficer.Position = (PositionClientData)comboBoxPosition.SelectedItem;
                    else
                        selectedOfficer.Position = null;

                    selectedOfficer.DepartmentType = (DepartmentTypeClientData)comboBoxDepartmentType.SelectedItem;
                    selectedOfficer.DepartmentStation = (DepartmentStationClientData)comboBoxStationType.SelectedItem;

                });
                selectedOfficer.Badge = textBoxBadge.Text;
                if (textBoxTelephone.Text != String.Empty)
                    selectedOfficer.Telephone = textBoxTelephone.Text;
                else
                    selectedOfficer.Telephone = null;
                ServerServiceClient.GetInstance().SaveOrUpdateClientData(selectedOfficer);
            }
        }

        private void FillDepartmentTypes()
        {
            comboBoxDepartmentType.Items.Clear();
            int maxlen = 0;
            foreach (DepartmentTypeClientData type in ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetDepartmentsType,true))
            {
                comboBoxDepartmentType.Items.Add(type);
                maxlen = Math.Max(maxlen, type.ToString().Length);
            }
            if (maxlen > 0)
            {
                comboBoxDepartmentType.DropDownWidth = maxlen * 6;
            }
        }

        private void clearRankPosition()
        {
            comboBoxPosition.Text = "";
            comboBoxPosition.Items.Clear();
            comboBoxRank.Text = "";
            comboBoxRank.Items.Clear();
        }

        private void ClearComboBoxZoneType()
        {
            if (comboBoxZoneType.SelectedIndex != -1)
            {
                comboBoxZoneType.Text = "";
            }
            comboBoxZoneType.Items.Clear();
        }

        private void ClearComboBoxStationType()
        {
            if (comboBoxStationType.SelectedIndex != -1)
            {
                comboBoxStationType.Text = "";
            }
            comboBoxStationType.Items.Clear();

        }

        private void comboBoxDepartmentType_SelectionChangeCommitted(object sender, EventArgs e)
        {
            clearRankPosition();
            ClearComboBoxStationType();
            ClearComboBoxZoneType();
            DepartmentTypeClientData departmentType = (DepartmentTypeClientData)comboBoxDepartmentType.SelectedItem;
            if (comboBoxDepartmentType.Focused == false)
            {
                chargeRankFromDepartment(departmentType.Name);
                chargePositionFromDepartment(departmentType.Name);
                FillCmbBoxZone(departmentType);
            }
            OfficerParameters_Change(null, null);
        }

        private void FillCmbBoxZone(DepartmentTypeClientData departmentType)
        {
            //ServerServiceClient.GetInstance().InitializeLazy(departmentType, departmentType.DepartmentZones);
            int maxlen = 0;
            foreach (DepartmentZoneClientData var in departmentType.DepartmentZones)
            {
                comboBoxZoneType.Items.Add(var);
                maxlen = Math.Max(maxlen, var.ToString().Length);
            }
            if(maxlen > 0)
            {
                comboBoxZoneType.DropDownWidth = maxlen * 6;
            }
        }


        private void FillCmbBoxStation(DepartmentZoneClientData departamentZone)
        {
            //ServerServiceClient.GetInstance().InitializeLazy(departamentZone, departamentZone.SetDepartmentStations);
            IList stations = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetDepartmentStationsByDepartmentZoneCode, departamentZone.Code));
            ClearComboBoxStationType();
            int maxlen = 0;
            foreach (DepartmentStationClientData var in stations)
            {
                comboBoxStationType.Items.Add(var);
                maxlen = Math.Max(maxlen, var.ToString().Length);
            }
            if(maxlen > 0)
            {
                comboBoxStationType.DropDownWidth = maxlen * 6;
            }
        }


        private void chargeRankFromDepartment(string departmentName)
        {
            if (departmentName != null)
            {
                string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetRankByDepartmentName, departmentName);
                int maxlen = 0;
                comboBoxRank.Items.Clear();
                comboBoxRank.Items.Add("");
                foreach (RankClientData type in ServerServiceClient.GetInstance().SearchClientObjects(hql,true))
                {
                    if (comboBoxRank.Items.Contains(type) == false)
                    {
                        comboBoxRank.Items.Add(type);
                        maxlen = Math.Max(maxlen, type.ToString().Length);
                    }
                }
                if (maxlen > 0)
                    comboBoxRank.DropDownWidth = maxlen * 6;
            }

        }

        private void chargePositionFromDepartment(string departmentName)
        {
            int maxlen = 0;
            comboBoxPosition.Items.Clear();
            comboBoxPosition.Items.Add("");
            foreach (PositionClientData type in ServerServiceClient.GetInstance().SearchClientObjects(
                SmartCadHqls.GetCustomHql(SmartCadHqls.GetPositionByDepartmentName, departmentName)))
            {
                if (comboBoxPosition.Items.Contains(type) == false)
                {
                    comboBoxPosition.Items.Add(type);
                    maxlen = Math.Max(maxlen, type.ToString().Length);
                }
            }
            if (maxlen > 0)
                comboBoxPosition.DropDownWidth = maxlen * 6;
        }

        private void OfficerForm_Load(object sender, EventArgs e)
        {
            switch (behaviorType)
            {
                case FormBehavior.Create:
                    Text = ResourceLoader.GetString2("OfficerFormCreateText");
                    buttonOk.Text = ResourceLoader.GetString("OfficerFormCreateButtonOkText");
                    break;
                case FormBehavior.Edit:
                    Text = ResourceLoader.GetString2("OfficerFormEditText");
                    buttonOk.Text = ResourceLoader.GetString("OfficerFormEditButtonOkText");
                    break;
                default:
                    break;
            }
        }

        private void comboBoxZoneType_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ClearComboBoxStationType();
            DepartmentZoneClientData departamentZone = comboBoxZoneType.SelectedItem as DepartmentZoneClientData;
            if (comboBoxZoneType.Focused == false)
            {
                FillCmbBoxStation(departamentZone);
            }
            OfficerParameters_Change(null, null);
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            if (SelectedOfficer != null) 
            {
                if (behaviorType == FormBehavior.Create)
                {
                    SelectedOfficer = null;
                }
                else 
                {
                    SelectedOfficer = (OfficerClientData)ServerServiceClient.GetInstance().RefreshClient(SelectedOfficer);
                }
            }
                
        }

        private void comboBoxZoneType_Enter(object sender, EventArgs e)
        {
            if (comboBoxZoneType.SelectedIndex == -1 && comboBoxDepartmentType.SelectedIndex != -1)
            {
                FillCmbBoxZone((DepartmentTypeClientData)comboBoxDepartmentType.SelectedItem);
            }
        }

        private void comboBoxStationType_Enter(object sender, EventArgs e)
        {
            if (comboBoxStationType.SelectedIndex == -1 && comboBoxZoneType.SelectedIndex != -1)
            {
                FillCmbBoxStation((DepartmentZoneClientData)comboBoxZoneType.SelectedItem);
            }
        }

        private void comboBoxPosition_Enter(object sender, EventArgs e)
        {
            if (comboBoxPosition.SelectedIndex == -1 && comboBoxDepartmentType.SelectedIndex != -1 && comboBoxPosition.Items.Count == 0)
            {
                chargePositionFromDepartment(((DepartmentTypeClientData)comboBoxDepartmentType.SelectedItem).Name);
            }
        }

        private void comboBoxRank_Enter(object sender, EventArgs e)
        {
            if (comboBoxRank.SelectedIndex == -1 && comboBoxDepartmentType.SelectedIndex != -1 && comboBoxRank.Items.Count == 0)
            {
                chargeRankFromDepartment(((DepartmentTypeClientData)comboBoxDepartmentType.SelectedItem).Name);
            }
        }
    }
}