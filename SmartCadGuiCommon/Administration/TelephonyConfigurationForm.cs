﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadCore.Core;
using System.ServiceModel;
using Smartmatic.SmartCad.Service;
using System.Reflection;
using SmartCadCore.Enums;

namespace SmartCadGuiCommon
{
    /// <summary>
    /// 
    /// </summary>
    public partial class TelephonyConfigurationForm : DevExpress.XtraEditors.XtraForm
    {
        #region Constructors
        public TelephonyConfigurationForm()
        {
            InitializeComponent();
        }

        public TelephonyConfigurationForm(AdministrationRibbonForm form)
            : this()
        {
        }

        public TelephonyConfigurationForm(AdministrationRibbonForm form, TelephonyConfigurationClientData data, FormBehavior behaviour)
            : this(form)
        {
            LoadLanguage();
            Data = data;
            ParentForm = form;
            Behavior = behaviour;
            LoadData();
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.textEditExtension.KeyDown +=new KeyEventHandler(textEditExtension_KeyDown);
            this.textEditExtension.KeyPress +=new KeyPressEventHandler(textEditExtension_KeyPress);
        }
        #endregion

        public TelephonyConfigurationClientData Data { get; set; }
        public AdministrationRibbonForm ParentForm { get; set; }
        public FormBehavior Behavior { get; set; }
        private bool nonNumberEntered = false;

        private void LoadLanguage()
        {
            Text = ResourceLoader.GetString2(this.Name);
            
            layoutControlGroupData.Text = ResourceLoader.GetString2("Data");
            layoutControlItemComputer.Text = ResourceLoader.GetString2("Computer") + ": *";
            layoutControlItemExtension.Text = ResourceLoader.GetString2("Extension") + ": *";
            layoutControlItemPassword.Text = ResourceLoader.GetString2("Password") + ":";
            layoutControlItemVirtual.Text = ResourceLoader.GetString2("Virtual") + ":";

            simpleButtonAccept.Text = ResourceLoader.GetString2("Accept");
            simpleButtonCancel.Text = ResourceLoader.GetString2("Cancel");
        }

        private void LoadData()
        {
            ConfigureEventListener();
            radioGroupVirtual.Properties.Items.Add(
                new DevExpress.XtraEditors.Controls.RadioGroupItem()
                {
                    Description = ResourceLoader.GetString2("Yes"),
                    Enabled = true,
                    Value = false
                });

            radioGroupVirtual.Properties.Items.Add(
                new DevExpress.XtraEditors.Controls.RadioGroupItem()
                {
                    Description = ResourceLoader.GetString2("No"),
                    Enabled = true,
                    Value = true
                });

            radioGroupVirtual.SelectedIndex = 1;

            comoboBoxEditComputer.Properties.Items.Clear();
            comoboBoxEditComputer.Properties.Items.AddRange(ComputerInfoList.List);
            //Parche para agregar el localhost. AA si se necesita pasar al ComputerInfoList.List ????
            comoboBoxEditComputer.Properties.Items.Add(new ComputerInfo(System.Net.Dns.GetHostName(),new List<string>()));

            
            if (Data == null)
            {
                Text = ResourceLoader.GetString2("TelephonyConfigurationFormCreate");
                simpleButtonAccept.Text = ResourceLoader.GetString("Create");
            }
            else
            {
                Text = ResourceLoader.GetString2("TelephonyConfigurationFormEdit");

                int index = comoboBoxEditComputer.Properties.Items.IndexOf(new ComputerInfo(Data.COMPUTER, new List<string>()));
                if (index > -1)
                    comoboBoxEditComputer.SelectedIndex = index;

                textEditExtension.Text = Data.Extension.ToString();
                textEditPassword.Text = Data.Password;
                radioGroupVirtual.SelectedIndex = Data.Virtual == true ? 0 : 1;
            }

            CheckAcceptButtonState(null, null);
        }

        private void simpleButtonAccept_Click(object sender, EventArgs e)
        {
            try
            {
                BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[1] { GetType().GetMethod("SaveOrUpdateData", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[0] });
                processForm.CanThrowError = true;
                processForm.ShowDialog();
            }
            catch (FaultException ex)
            {
                Data = (TelephonyConfigurationClientData)ServerServiceClient.GetInstance().RefreshClient(Data);
                GetErrorFocus(ex);
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, MessageFormType.Error);
            }
            catch (Exception ex)
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, ex);
            }
        }

        private void GetErrorFocus(FaultException ex)
        {
            if (ex.Message.Contains(ResourceLoader.GetString2("Computer").ToLower()))
            {
                comoboBoxEditComputer.Focus();
            }
            else if (ex.Message.Contains(ResourceLoader.GetString2("Extension").ToLower()))
            {
                textEditExtension.Focus();
            }
        }

        private void SaveOrUpdateData()
        {
            if (Data == null)
            {
                Data = new TelephonyConfigurationClientData();
            }

            FormUtil.InvokeRequired(this, delegate
            {
                Data.COMPUTER = ((ComputerInfo)comoboBoxEditComputer.SelectedItem).Name;
                Data.Extension = int.Parse(textEditExtension.Text);
                Data.Password = textEditPassword.Text;
                Data.Virtual = radioGroupVirtual.SelectedIndex == 0 ? true : false;
            });
            ServerServiceClient.GetInstance().SaveOrUpdateClientData(Data);
        }

        private void CheckAcceptButtonState(object sender, System.EventArgs e)
        {
            if ((string.IsNullOrEmpty(comoboBoxEditComputer.Text) || comoboBoxEditComputer.SelectedIndex < 0) || 
                (string.IsNullOrEmpty(textEditExtension.Text)))
            {
                simpleButtonAccept.Enabled = false;
            }
            else
            {
                simpleButtonAccept.Enabled = true;
            }
                 
        }

        private void textEditExtension_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            nonNumberEntered = false;
            if (e.KeyCode < Keys.D0 || e.KeyCode > Keys.D9)
            {
                if (e.KeyCode < Keys.NumPad0 || e.KeyCode > Keys.NumPad9)
                {
                    if (e.KeyCode != Keys.Back)
                    {                        
                        nonNumberEntered = true;
                    }
                }
            }
            if (Control.ModifierKeys == Keys.Shift)
            {
                nonNumberEntered = true;
            }
        }

        private void textEditExtension_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (nonNumberEntered == true)
            {
                e.Handled = true;
            }
        }       

        private void ConfigureEventListener()
        {
            comoboBoxEditComputer.SelectedIndexChanged += CheckAcceptButtonState;
            textEditExtension.TextChanged += CheckAcceptButtonState;
        }
    }
}