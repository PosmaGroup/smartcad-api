using SmartCadControls;
namespace SmartCadGuiCommon
{
	partial class RouteForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RouteForm));
            this.layoutControlRouteForm = new DevExpress.XtraLayout.LayoutControl();
            this.textEditStopTimeTol = new DevExpress.XtraEditors.TextEdit();
            this.textEditStopTime = new DevExpress.XtraEditors.TextEdit();
            this.simpleButtonChooseIcon = new DevExpress.XtraEditors.SimpleButton();
            this.pictureEditIcon = new DevExpress.XtraEditors.PictureEdit();
            this.comboBoxEditDepartment = new DevExpress.XtraEditors.ComboBoxEdit();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.gridControlExRoutePoints = new GridControlEx();
            this.gridViewExRoutePoints = new GridViewEx();
            this.simpleButtonAccept = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonOpenMap = new DevExpress.XtraEditors.SimpleButton();
            this.comboBoxEditType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEditCustomCode = new DevExpress.XtraEditors.TextEdit();
            this.textEditName = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroupRouteForm = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemOpenMap = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemAccept = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemCancel = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupRouteDetails = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemCustomCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemDepartment = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemType = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupIcon = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupPreview = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemPictureEditIcon = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemChooseIcon = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupGridControl = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemGridControl = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemStopTimeTolText = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItemStopTimeText = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlRouteForm)).BeginInit();
            this.layoutControlRouteForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditStopTimeTol.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditStopTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEditIcon.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditDepartment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExRoutePoints)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExRoutePoints)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCustomCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupRouteForm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOpenMap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAccept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupRouteDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCustomCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupPreview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPictureEditIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemChooseIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStopTimeTolText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStopTimeText)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControlRouteForm
            // 
            this.layoutControlRouteForm.AllowCustomizationMenu = false;
            this.layoutControlRouteForm.Controls.Add(this.textEditStopTimeTol);
            this.layoutControlRouteForm.Controls.Add(this.textEditStopTime);
            this.layoutControlRouteForm.Controls.Add(this.simpleButtonChooseIcon);
            this.layoutControlRouteForm.Controls.Add(this.pictureEditIcon);
            this.layoutControlRouteForm.Controls.Add(this.comboBoxEditDepartment);
            this.layoutControlRouteForm.Controls.Add(this.simpleButtonCancel);
            this.layoutControlRouteForm.Controls.Add(this.gridControlExRoutePoints);
            this.layoutControlRouteForm.Controls.Add(this.simpleButtonAccept);
            this.layoutControlRouteForm.Controls.Add(this.simpleButtonOpenMap);
            this.layoutControlRouteForm.Controls.Add(this.comboBoxEditType);
            this.layoutControlRouteForm.Controls.Add(this.textEditCustomCode);
            this.layoutControlRouteForm.Controls.Add(this.textEditName);
            this.layoutControlRouteForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlRouteForm.Location = new System.Drawing.Point(0, 0);
            this.layoutControlRouteForm.Name = "layoutControlRouteForm";
            this.layoutControlRouteForm.Root = this.layoutControlGroupRouteForm;
            this.layoutControlRouteForm.Size = new System.Drawing.Size(548, 627);
            this.layoutControlRouteForm.TabIndex = 0;
            this.layoutControlRouteForm.Text = "layoutControl1";
            // 
            // textEditStopTimeTol
            // 
            this.textEditStopTimeTol.Enabled = false;
            this.textEditStopTimeTol.Location = new System.Drawing.Point(179, 347);
            this.textEditStopTimeTol.Name = "textEditStopTimeTol";
            this.textEditStopTimeTol.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.textEditStopTimeTol.Properties.DisplayFormat.FormatString = "00";
            this.textEditStopTimeTol.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.textEditStopTimeTol.Properties.EditFormat.FormatString = "00";
            this.textEditStopTimeTol.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.textEditStopTimeTol.Properties.Mask.EditMask = "00";
            this.textEditStopTimeTol.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEditStopTimeTol.Properties.NullText = "1";
            this.textEditStopTimeTol.Size = new System.Drawing.Size(89, 20);
            this.textEditStopTimeTol.StyleController = this.layoutControlRouteForm;
            this.textEditStopTimeTol.TabIndex = 16;
            // 
            // textEditStopTime
            // 
            this.textEditStopTime.Enabled = false;
            this.textEditStopTime.Location = new System.Drawing.Point(165, 323);
            this.textEditStopTime.Name = "textEditStopTime";
            this.textEditStopTime.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.textEditStopTime.Properties.DisplayFormat.FormatString = "00";
            this.textEditStopTime.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.textEditStopTime.Properties.EditFormat.FormatString = "00";
            this.textEditStopTime.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.textEditStopTime.Properties.Mask.EditMask = "00";
            this.textEditStopTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEditStopTime.Properties.NullText = "1";
            this.textEditStopTime.Size = new System.Drawing.Size(103, 20);
            this.textEditStopTime.StyleController = this.layoutControlRouteForm;
            this.textEditStopTime.TabIndex = 15;
            // 
            // simpleButtonChooseIcon
            // 
            this.simpleButtonChooseIcon.Location = new System.Drawing.Point(7, 455);
            this.simpleButtonChooseIcon.Name = "simpleButtonChooseIcon";
            this.simpleButtonChooseIcon.Size = new System.Drawing.Size(87, 29);
            this.simpleButtonChooseIcon.StyleController = this.layoutControlRouteForm;
            this.simpleButtonChooseIcon.TabIndex = 14;
            this.simpleButtonChooseIcon.Text = "simpleButtonChooseIcon";
            this.simpleButtonChooseIcon.Click += new System.EventHandler(this.simpleButtonChooseIcon_Click);
            // 
            // pictureEditIcon
            // 
            this.pictureEditIcon.Location = new System.Drawing.Point(103, 426);
            this.pictureEditIcon.Name = "pictureEditIcon";
            this.pictureEditIcon.Size = new System.Drawing.Size(433, 140);
            this.pictureEditIcon.StyleController = this.layoutControlRouteForm;
            this.pictureEditIcon.TabIndex = 13;
            // 
            // comboBoxEditDepartment
            // 
            this.comboBoxEditDepartment.Location = new System.Drawing.Point(160, 54);
            this.comboBoxEditDepartment.Name = "comboBoxEditDepartment";
            this.comboBoxEditDepartment.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditDepartment.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEditDepartment.Size = new System.Drawing.Size(380, 20);
            this.comboBoxEditDepartment.StyleController = this.layoutControlRouteForm;
            this.comboBoxEditDepartment.TabIndex = 12;
            this.comboBoxEditDepartment.SelectedIndexChanged += new System.EventHandler(this.comboBoxEditDepartment_SelectedIndexChanged);
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(458, 591);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(88, 34);
            this.simpleButtonCancel.StyleController = this.layoutControlRouteForm;
            this.simpleButtonCancel.TabIndex = 10;
            this.simpleButtonCancel.Text = "Cancel";
            // 
            // gridControlExRoutePoints
            // 
            this.gridControlExRoutePoints.EnableAutoFilter = false;
            this.gridControlExRoutePoints.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExRoutePoints.Location = new System.Drawing.Point(8, 136);
            this.gridControlExRoutePoints.MainView = this.gridViewExRoutePoints;
            this.gridControlExRoutePoints.Name = "gridControlExRoutePoints";
            this.gridControlExRoutePoints.Size = new System.Drawing.Size(532, 182);
            this.gridControlExRoutePoints.TabIndex = 11;
            this.gridControlExRoutePoints.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExRoutePoints});
            this.gridControlExRoutePoints.ViewTotalRows = false;
            // 
            // gridViewExRoutePoints
            // 
            this.gridViewExRoutePoints.AllowFocusedRowChanged = true;
            this.gridViewExRoutePoints.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExRoutePoints.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExRoutePoints.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExRoutePoints.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExRoutePoints.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExRoutePoints.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExRoutePoints.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExRoutePoints.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExRoutePoints.EnablePreviewLineForFocusedRow = false;
            this.gridViewExRoutePoints.GridControl = this.gridControlExRoutePoints;
            this.gridViewExRoutePoints.Name = "gridViewExRoutePoints";
            this.gridViewExRoutePoints.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExRoutePoints.OptionsView.ShowAutoFilterRow = true;
            this.gridViewExRoutePoints.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExRoutePoints.ViewTotalRows = false;
            this.gridViewExRoutePoints.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewExRoutePoints_CellValueChanged);
            this.gridViewExRoutePoints.SingleSelectionChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(gridViewExRoutePoints_SingleSelectionChanged);
            // 
            // simpleButtonAccept
            // 
            this.simpleButtonAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButtonAccept.Location = new System.Drawing.Point(366, 591);
            this.simpleButtonAccept.Name = "simpleButtonAccept";
            this.simpleButtonAccept.Size = new System.Drawing.Size(88, 34);
            this.simpleButtonAccept.StyleController = this.layoutControlRouteForm;
            this.simpleButtonAccept.TabIndex = 9;
            this.simpleButtonAccept.Text = "Accept";
            this.simpleButtonAccept.Click += new System.EventHandler(this.simpleButtonAccept_Click);
            // 
            // simpleButtonOpenMap
            // 
            this.simpleButtonOpenMap.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonOpenMap.Image")));
            this.simpleButtonOpenMap.Location = new System.Drawing.Point(3, 581);
            this.simpleButtonOpenMap.Name = "simpleButtonOpenMap";
            this.simpleButtonOpenMap.Size = new System.Drawing.Size(43, 43);
            this.simpleButtonOpenMap.StyleController = this.layoutControlRouteForm;
            this.simpleButtonOpenMap.TabIndex = 8;
            this.simpleButtonOpenMap.Click += new System.EventHandler(this.simpleButtonOpenMap_Click);
            // 
            // comboBoxEditType
            // 
            this.comboBoxEditType.Location = new System.Drawing.Point(418, 80);
            this.comboBoxEditType.Name = "comboBoxEditType";
            this.comboBoxEditType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEditType.Size = new System.Drawing.Size(122, 20);
            this.comboBoxEditType.StyleController = this.layoutControlRouteForm;
            this.comboBoxEditType.TabIndex = 7;
            this.comboBoxEditType.SelectedIndexChanged += new System.EventHandler(this.comboBoxEditType_SelectedIndexChanged);
            // 
            // textEditCustomCode
            // 
            this.textEditCustomCode.Location = new System.Drawing.Point(160, 80);
            this.textEditCustomCode.Name = "textEditCustomCode";
            this.textEditCustomCode.Size = new System.Drawing.Size(100, 20);
            this.textEditCustomCode.StyleController = this.layoutControlRouteForm;
            this.textEditCustomCode.TabIndex = 6;
            // 
            // textEditName
            // 
            this.textEditName.Location = new System.Drawing.Point(160, 28);
            this.textEditName.Name = "textEditName";
            this.textEditName.Properties.MaxLength = 50;
            this.textEditName.Size = new System.Drawing.Size(380, 20);
            this.textEditName.StyleController = this.layoutControlRouteForm;
            this.textEditName.TabIndex = 4;
            this.textEditName.TextChanged += new System.EventHandler(this.textEditName_TextChanged);
            // 
            // layoutControlGroupRouteForm
            // 
            this.layoutControlGroupRouteForm.CustomizationFormText = "layoutControlGroupRouteForm";
            this.layoutControlGroupRouteForm.GroupBordersVisible = false;
            this.layoutControlGroupRouteForm.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemOpenMap,
            this.layoutControlItemAccept,
            this.layoutControlItemCancel,
            this.emptySpaceItem1,
            this.layoutControlGroupRouteDetails,
            this.emptySpaceItem2,
            this.layoutControlGroupIcon,
            this.layoutControlGroupGridControl});
            this.layoutControlGroupRouteForm.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupRouteForm.Name = "layoutControlGroupRouteForm";
            this.layoutControlGroupRouteForm.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupRouteForm.Size = new System.Drawing.Size(548, 627);
            this.layoutControlGroupRouteForm.Text = "layoutControlGroupRouteForm";
            this.layoutControlGroupRouteForm.TextVisible = false;
            // 
            // layoutControlItemOpenMap
            // 
            this.layoutControlItemOpenMap.Control = this.simpleButtonOpenMap;
            this.layoutControlItemOpenMap.CustomizationFormText = "layoutControlItemOpenMap";
            this.layoutControlItemOpenMap.Location = new System.Drawing.Point(0, 578);
            this.layoutControlItemOpenMap.MaxSize = new System.Drawing.Size(49, 49);
            this.layoutControlItemOpenMap.MinSize = new System.Drawing.Size(49, 49);
            this.layoutControlItemOpenMap.Name = "layoutControlItemOpenMap";
            this.layoutControlItemOpenMap.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItemOpenMap.Size = new System.Drawing.Size(49, 49);
            this.layoutControlItemOpenMap.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemOpenMap.Text = "layoutControlItemOpenMap";
            this.layoutControlItemOpenMap.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemOpenMap.TextToControlDistance = 0;
            this.layoutControlItemOpenMap.TextVisible = false;
            // 
            // layoutControlItemAccept
            // 
            this.layoutControlItemAccept.Control = this.simpleButtonAccept;
            this.layoutControlItemAccept.CustomizationFormText = "layoutControlItemAccept";
            this.layoutControlItemAccept.Location = new System.Drawing.Point(364, 589);
            this.layoutControlItemAccept.MaxSize = new System.Drawing.Size(92, 38);
            this.layoutControlItemAccept.MinSize = new System.Drawing.Size(92, 38);
            this.layoutControlItemAccept.Name = "layoutControlItemAccept";
            this.layoutControlItemAccept.Size = new System.Drawing.Size(92, 38);
            this.layoutControlItemAccept.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemAccept.Text = "layoutControlItemAccept";
            this.layoutControlItemAccept.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemAccept.TextToControlDistance = 0;
            this.layoutControlItemAccept.TextVisible = false;
            // 
            // layoutControlItemCancel
            // 
            this.layoutControlItemCancel.Control = this.simpleButtonCancel;
            this.layoutControlItemCancel.CustomizationFormText = "layoutControlItemCancel";
            this.layoutControlItemCancel.Location = new System.Drawing.Point(456, 589);
            this.layoutControlItemCancel.MaxSize = new System.Drawing.Size(92, 38);
            this.layoutControlItemCancel.MinSize = new System.Drawing.Size(92, 38);
            this.layoutControlItemCancel.Name = "layoutControlItemCancel";
            this.layoutControlItemCancel.Size = new System.Drawing.Size(92, 38);
            this.layoutControlItemCancel.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemCancel.Text = "layoutControlItemCancel";
            this.layoutControlItemCancel.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemCancel.TextToControlDistance = 0;
            this.layoutControlItemCancel.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(49, 589);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(315, 38);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupRouteDetails
            // 
            this.layoutControlGroupRouteDetails.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroupRouteDetails.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemCustomCode,
            this.layoutControlItemDepartment,
            this.layoutControlItemName,
            this.layoutControlItemType});
            this.layoutControlGroupRouteDetails.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupRouteDetails.Name = "layoutControlGroupRouteDetails";
            this.layoutControlGroupRouteDetails.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupRouteDetails.Size = new System.Drawing.Size(548, 108);
            this.layoutControlGroupRouteDetails.Text = "layoutControlGroupRouteDetails";
            // 
            // layoutControlItemCustomCode
            // 
            this.layoutControlItemCustomCode.Control = this.textEditCustomCode;
            this.layoutControlItemCustomCode.CustomizationFormText = "layoutControlItemCustomCode";
            this.layoutControlItemCustomCode.Location = new System.Drawing.Point(0, 52);
            this.layoutControlItemCustomCode.Name = "layoutControlItemCustomCode";
            this.layoutControlItemCustomCode.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItemCustomCode.Size = new System.Drawing.Size(258, 26);
            this.layoutControlItemCustomCode.Text = "layoutControlItemCustomCode";
            this.layoutControlItemCustomCode.TextSize = new System.Drawing.Size(148, 13);
            // 
            // layoutControlItemDepartment
            // 
            this.layoutControlItemDepartment.Control = this.comboBoxEditDepartment;
            this.layoutControlItemDepartment.CustomizationFormText = "layoutControlItemDepartment";
            this.layoutControlItemDepartment.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItemDepartment.Name = "layoutControlItemDepartment";
            this.layoutControlItemDepartment.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItemDepartment.Size = new System.Drawing.Size(538, 26);
            this.layoutControlItemDepartment.Text = "layoutControlItemDepartment";
            this.layoutControlItemDepartment.TextSize = new System.Drawing.Size(148, 13);
            // 
            // layoutControlItemName
            // 
            this.layoutControlItemName.Control = this.textEditName;
            this.layoutControlItemName.CustomizationFormText = "layoutControlItemName";
            this.layoutControlItemName.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemName.Name = "layoutControlItemName";
            this.layoutControlItemName.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItemName.Size = new System.Drawing.Size(538, 26);
            this.layoutControlItemName.Text = "layoutControlItemName";
            this.layoutControlItemName.TextSize = new System.Drawing.Size(148, 13);
            // 
            // layoutControlItemType
            // 
            this.layoutControlItemType.Control = this.comboBoxEditType;
            this.layoutControlItemType.CustomizationFormText = "layoutControlItemType";
            this.layoutControlItemType.Location = new System.Drawing.Point(258, 52);
            this.layoutControlItemType.Name = "layoutControlItemType";
            this.layoutControlItemType.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItemType.Size = new System.Drawing.Size(280, 26);
            this.layoutControlItemType.Text = "layoutControlItemType";
            this.layoutControlItemType.TextSize = new System.Drawing.Size(148, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(49, 578);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(499, 11);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupIcon
            // 
            this.layoutControlGroupIcon.CustomizationFormText = "layoutControlGroupIcon";
            this.layoutControlGroupIcon.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupPreview,
            this.layoutControlItemChooseIcon,
            this.emptySpaceItem3,
            this.emptySpaceItem4});
            this.layoutControlGroupIcon.Location = new System.Drawing.Point(0, 374);
            this.layoutControlGroupIcon.Name = "layoutControlGroupIcon";
            this.layoutControlGroupIcon.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupIcon.Size = new System.Drawing.Size(548, 204);
            this.layoutControlGroupIcon.Text = "layoutControlGroupIcon";
            // 
            // layoutControlGroupPreview
            // 
            this.layoutControlGroupPreview.CustomizationFormText = "layoutControlGroupPreview";
            this.layoutControlGroupPreview.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemPictureEditIcon});
            this.layoutControlGroupPreview.Location = new System.Drawing.Point(91, 0);
            this.layoutControlGroupPreview.Name = "layoutControlGroupPreview";
            this.layoutControlGroupPreview.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupPreview.Size = new System.Drawing.Size(447, 174);
            this.layoutControlGroupPreview.Text = "layoutControlGroupPreview";
            // 
            // layoutControlItemPictureEditIcon
            // 
            this.layoutControlItemPictureEditIcon.Control = this.pictureEditIcon;
            this.layoutControlItemPictureEditIcon.CustomizationFormText = "layoutControlItemPictureEditIcon";
            this.layoutControlItemPictureEditIcon.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemPictureEditIcon.Name = "layoutControlItemPictureEditIcon";
            this.layoutControlItemPictureEditIcon.Size = new System.Drawing.Size(437, 144);
            this.layoutControlItemPictureEditIcon.Text = "layoutControlItemPictureEditIcon";
            this.layoutControlItemPictureEditIcon.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemPictureEditIcon.TextToControlDistance = 0;
            this.layoutControlItemPictureEditIcon.TextVisible = false;
            // 
            // layoutControlItemChooseIcon
            // 
            this.layoutControlItemChooseIcon.Control = this.simpleButtonChooseIcon;
            this.layoutControlItemChooseIcon.CustomizationFormText = "layoutControlItemChooseIcon";
            this.layoutControlItemChooseIcon.Location = new System.Drawing.Point(0, 54);
            this.layoutControlItemChooseIcon.MaxSize = new System.Drawing.Size(91, 33);
            this.layoutControlItemChooseIcon.MinSize = new System.Drawing.Size(91, 33);
            this.layoutControlItemChooseIcon.Name = "layoutControlItemChooseIcon";
            this.layoutControlItemChooseIcon.Size = new System.Drawing.Size(91, 33);
            this.layoutControlItemChooseIcon.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemChooseIcon.Text = "layoutControlItemChooseIcon";
            this.layoutControlItemChooseIcon.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemChooseIcon.TextToControlDistance = 0;
            this.layoutControlItemChooseIcon.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(91, 54);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 87);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(91, 87);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupGridControl
            // 
            this.layoutControlGroupGridControl.CustomizationFormText = "layoutControlGroupGridControl";
            this.layoutControlGroupGridControl.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemGridControl,
            this.layoutControlItemStopTimeTolText,
            this.emptySpaceItem5,
            this.layoutControlItemStopTimeText});
            this.layoutControlGroupGridControl.Location = new System.Drawing.Point(0, 108);
            this.layoutControlGroupGridControl.Name = "layoutControlGroupGridControl";
            this.layoutControlGroupGridControl.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupGridControl.Size = new System.Drawing.Size(548, 266);
            this.layoutControlGroupGridControl.Text = "layoutControlGroupGridControl";
            // 
            // layoutControlItemGridControl
            // 
            this.layoutControlItemGridControl.Control = this.gridControlExRoutePoints;
            this.layoutControlItemGridControl.CustomizationFormText = "layoutControlItemGridControl";
            this.layoutControlItemGridControl.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemGridControl.Name = "layoutControlItemGridControl";
            this.layoutControlItemGridControl.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItemGridControl.Size = new System.Drawing.Size(538, 188);
            this.layoutControlItemGridControl.Text = "layoutControlItemGridControl";
            this.layoutControlItemGridControl.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItemGridControl.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemGridControl.TextToControlDistance = 0;
            this.layoutControlItemGridControl.TextVisible = false;
            // 
            // layoutControlItemStopTimeTolText
            // 
            this.layoutControlItemStopTimeTolText.Control = this.textEditStopTimeTol;
            this.layoutControlItemStopTimeTolText.CustomizationFormText = "layoutControlItemStopTimeTolText";
            this.layoutControlItemStopTimeTolText.Location = new System.Drawing.Point(0, 212);
            this.layoutControlItemStopTimeTolText.Name = "layoutControlItemStopTimeTolText";
            this.layoutControlItemStopTimeTolText.Size = new System.Drawing.Size(265, 24);
            this.layoutControlItemStopTimeTolText.Text = "layoutControlItemStopTimeTolText";
            this.layoutControlItemStopTimeTolText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItemStopTimeTolText.TextSize = new System.Drawing.Size(167, 13);
            this.layoutControlItemStopTimeTolText.TextToControlDistance = 5;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(265, 188);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(273, 48);
            this.emptySpaceItem5.Text = "emptySpaceItem5";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItemStopTimeText
            // 
            this.layoutControlItemStopTimeText.Control = this.textEditStopTime;
            this.layoutControlItemStopTimeText.CustomizationFormText = "layoutControlItemStopTimeText";
            this.layoutControlItemStopTimeText.Location = new System.Drawing.Point(0, 188);
            this.layoutControlItemStopTimeText.Name = "layoutControlItemStopTimeText";
            this.layoutControlItemStopTimeText.Size = new System.Drawing.Size(265, 24);
            this.layoutControlItemStopTimeText.Text = "layoutControlItemStopTimeText";
            this.layoutControlItemStopTimeText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItemStopTimeText.TextSize = new System.Drawing.Size(153, 13);
            this.layoutControlItemStopTimeText.TextToControlDistance = 5;
            // 
            // RouteForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(548, 627);
            this.Controls.Add(this.layoutControlRouteForm);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.LookAndFeel.SkinName = "Blue";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(443, 494);
            this.Name = "RouteForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RouteForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RouteForm_FormClosing);
            this.Load += new System.EventHandler(this.RouteForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlRouteForm)).EndInit();
            this.layoutControlRouteForm.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditStopTimeTol.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditStopTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEditIcon.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditDepartment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExRoutePoints)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExRoutePoints)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCustomCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupRouteForm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOpenMap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAccept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupRouteDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCustomCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupPreview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPictureEditIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemChooseIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStopTimeTolText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStopTimeText)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private DevExpress.XtraLayout.LayoutControl layoutControlRouteForm;
		private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
		private DevExpress.XtraEditors.SimpleButton simpleButtonAccept;
		private DevExpress.XtraEditors.SimpleButton simpleButtonOpenMap;
		private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditType;
		private DevExpress.XtraEditors.TextEdit textEditCustomCode;
		private DevExpress.XtraEditors.TextEdit textEditName;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupRouteForm;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemName;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDepartment;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemCustomCode;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemType;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemOpenMap;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemAccept;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemCancel;
		private GridControlEx gridControlExRoutePoints;
		private GridViewEx gridViewExRoutePoints;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupRouteDetails;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemGridControl;
		private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditDepartment;
		private DevExpress.XtraEditors.SimpleButton simpleButtonChooseIcon;
		private DevExpress.XtraEditors.PictureEdit pictureEditIcon;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupPreview;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemPictureEditIcon;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemChooseIcon;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupIcon;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupGridControl;
		private DevExpress.XtraEditors.TextEdit textEditStopTime;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemStopTimeText;
		private DevExpress.XtraEditors.TextEdit textEditStopTimeTol;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemStopTimeTolText;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
	}
}