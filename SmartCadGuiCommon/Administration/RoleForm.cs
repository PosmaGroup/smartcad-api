using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Reflection;
using System.Collections.Generic;
using Smartmatic.SmartCad.Service;
using System.ServiceModel;
using DevExpress.XtraEditors;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadGuiCommon;
using SmartCadCore.Core;
using SmartCadCore.Enums;
using SmartCadGuiCommon.Util;

namespace Smartmatic.SmartCad.Gui
{
	/// <summary>
	/// Summary description for RoleForm.
	/// </summary>
	public partial class RoleForm : XtraForm
	{
        private RoleProfileSyncBox2 profileSyncBox2;
        private AdministrationRibbonForm parentForm;
        private object syncCommitted = new object();
                
        public RoleForm(AdministrationRibbonForm form)
		{
            
            InitializeComponent();

            this.parentForm = form;

            profileSyncBox2 = new RoleProfileSyncBox2();
            profileSyncBox2.DataGrid = dataGridExProfiles;

            dataGridExProfiles.Type = typeof(GridRoleProfileData);
            GridRoleProfileData.DataGrid = dataGridExProfiles;
            dataGridExProfiles.AllowDrop = false;

			FillProfiles();
            dataGridExProfiles.CellContentClick += new DataGridViewCellEventHandler(dataGridExProfiles_CellContentClick);

            LoadLanguage();
            this.parentForm.AdministrationCommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(RoleForm_AdministrationCommittedChanges);

            ButtonsActivation();

		}

        void RoleForm_AdministrationCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    lock (syncCommitted)
                    {
                        try
                        {
                            if (ServerServiceClient.GetInstance().OperatorClient == null)
                            { }
                        }
                        catch
                        { }

                        if (e.Objects[0] is UserRoleClientData)
                        {
                            #region UserRoleClientData
                            UserRoleClientData role =
                                    e.Objects[0] as UserRoleClientData;
                            if (SelectedRole != null && SelectedRole.Equals(role) && ButtonOkPressed == false)
                            {
                                if (e.Action == CommittedDataAction.Update)
                                {
                                    MessageForm.Show(ResourceLoader.GetString2("UpdateFormUserRoleData"), MessageFormType.Warning);
                                    SelectedRole = role;
                                }
                                else if (e.Action == CommittedDataAction.Delete)
                                {
                                    MessageForm.Show(ResourceLoader.GetString2("DeleteFormUserRoleData"), MessageFormType.Warning);
                                    FormUtil.InvokeRequired(this,
                                    delegate
                                    {
                                        this.Close();
                                    });
                                }

                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private void LoadLanguage()
        {
            this.layoutControlGroupData.Text = ResourceLoader.GetString2("RoleData");
            this.layoutControlName.Text = ResourceLoader.GetString2("$Message.Name") + ":*";
            this.layoutControlDesc.Text = ResourceLoader.GetString2("$Message.Description") + ":";
            this.layoutControlGroupProfiles.Text = ResourceLoader.GetString2("TreeProfileText");
            this.layoutControlName.Text = ResourceLoader.GetString2("NameAsterisk");
            this.layoutControlDesc.Text = ResourceLoader.GetString2("LabelDescription");
            this.buttonExCancel.Text = ResourceLoader.GetString2("$Message.Cancel");
        }

        public RoleForm(AdministrationRibbonForm form, UserRoleClientData role, FormBehavior behavior)
            :this(form)
		{
			Behavior = behavior;
            SelectedRole = role;
        }
 
        private void dataGridExProfiles_LostFocus(object sender, EventArgs e)
        {
           dataGridExProfiles.SelectedItems.Clear();
            
        }

		private FormBehavior behavior;
		public FormBehavior Behavior
		{
			set
			{
				behavior = value;
               
			}
			get
			{
				return behavior;
			}
		}


		private UserRoleClientData selectedRole;
        public UserRoleClientData SelectedRole
		{
            get
			{
				return selectedRole;
			}
			set
			{
                UserRoleClientData roleClonned = null;
                if (value != null)
                {
                    roleClonned = value.Clone() as UserRoleClientData;
                }
				selectedRole = roleClonned;

				if (Behavior == FormBehavior.Edit)
				{
					textBoxName.Text = selectedRole.FriendlyName;
					textBoxDescription.Text = selectedRole.Description;
                    //ServerServiceClient.GetInstance().InitializeLazy(selectedRole, selectedRole.Profiles);

                    CheckProfileInDataGrid(selectedRole);                     
				                                      
				}
			}
		}

        private ArrayList selectedProfiles = new ArrayList();
        public ArrayList SelectedProfiles
		{
			set
			{
				selectedProfiles = value;
			}
			get
			{
                return selectedProfiles;
			}
		}


      


        private void ButtonsActivation()
        {
            //FormUtil.InvokeRequired(this,
            //    delegate
            //{

            if ((SelectedProfiles.Count == 0) || (textBoxName.Text.Trim() == ""))
            {

                buttonExOK.Enabled = false;

            }

            else
            {
                buttonExOK.Enabled = true;
            }


           //     });


        }

        private bool buttonOkPressed = false;

        public bool ButtonOkPressed
        {
            get { return buttonOkPressed; }
        }

        private void buttonExOK_Click1()
		{
            
            buttonOkPressed = true;

            if (behavior == FormBehavior.Create)
            {
                selectedRole = new UserRoleClientData();
                selectedRole.Name = textBoxName.Text.Trim();
                selectedRole.Immutable = false;
            }
            else { 
                if (!(bool)selectedRole.Immutable)
                   selectedRole.Name = textBoxName.Text.Trim();
            }          
          
            selectedRole.FriendlyName = textBoxName.Text.Trim();
			selectedRole.Description = textBoxDescription.Text.Trim();
           
            if (SelectedRole.Profiles != null)
			    SelectedRole.Profiles.Clear();

            SelectedRole.Profiles = SelectedProfiles;

            ServerServiceClient.GetInstance().SaveOrUpdateClientData(selectedRole);
           
		}

		private void buttonExOK_Click(object sender, System.EventArgs e)
		{
			try
			{
                BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[1] { GetType().GetMethod("buttonExOK_Click1", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[0] });
				processForm.CanThrowError = true;
				processForm.ShowDialog();	
			}
            catch (FaultException ex)
            {
                SelectedRole = (UserRoleClientData)ServerServiceClient.GetInstance().RefreshClient(SelectedRole);
                GetErrorFocus(ex);
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, MessageFormType.Error);
                buttonOkPressed = false;
            }
            catch (Exception ex)
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, ex);
                buttonOkPressed = false;
            }
		}

        private void GetErrorFocus(FaultException ex) 
        {
            if (ex.Message.Contains(ResourceLoader.GetString2("Name").ToLower())) 
            {
                if (behavior == FormBehavior.Create)
                    textBoxName.Text = textBoxName.Text;
                textBoxName.Focus();
            }
        }

        private void FillProfiles()
        {
            
            GridRoleProfileData item;
            dataGridExProfiles.ClearData();

            foreach (UserProfileClientData profile in ServerServiceClient.GetInstance().SearchClientObjects(typeof(UserProfileClientData)))
            {
                item = new GridRoleProfileData(profile);
                dataGridExProfiles.AddData(item);
                
            }

            if (dataGridExProfiles.RowCount >= 6)
            {
                dataGridExProfiles.Columns[2].Width -= 17;
            }

        }

        private void CheckProfileInDataGrid(UserRoleClientData currentRole)
        {
            ClearCheckedRows();

            string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetUserProfilesByRole, currentRole.Code);
            IList profiles = ServerServiceClient.GetInstance().SearchClientObjects(hql);
            currentRole.Profiles = profiles;

            foreach (UserProfileClientData profile in currentRole.Profiles)
            {
                for (int i = 0; i < dataGridExProfiles.Rows.Count; i++)
                {

                    GridRoleProfileData profileItem = ((GridRoleProfileData)dataGridExProfiles.GetData(i));
                    if (profileItem.Profile.Code == profile.Code)
                    {
                        DataGridViewCheckBoxCell checkBoxCell = (DataGridViewCheckBoxCell)dataGridExProfiles.Rows[i].Cells[0];
                        checkBoxCell.Value = true;
                        SelectedProfiles.Add(profile);

                        
                    }
                }
            }           
        }

        private void ClearCheckedRows() {
            
            for (int i = 0; i < dataGridExProfiles.Hash.Count; i++)
            {
                DataGridViewCheckBoxCell cell = (DataGridViewCheckBoxCell)dataGridExProfiles.Rows[i].Cells[0];
                cell.Value = false;               
            }
        }

       private void ClearLastSelectItem()
       {
            for (int i = 0; i < dataGridExProfiles.Rows.Count; i++)
            {
                dataGridExProfiles.Rows[i].Selected = false;
            }
       }

        private void dataGridExProfiles_CellContentClick(object sender, EventArgs e)
        {
            DataGridViewCellEventArgs e1 = ((DataGridViewCellEventArgs)e);
            if (e1.ColumnIndex == 0 && dataGridExProfiles.ReadOnly != true)
            {
                DataGridViewCheckBoxCell cellPressed = (DataGridViewCheckBoxCell)dataGridExProfiles.Rows[e1.RowIndex].Cells[e1.ColumnIndex];
                GridRoleProfileData profileItem = (GridRoleProfileData)dataGridExProfiles.GetData(e1.RowIndex);

                if (cellPressed.Value.ToString().Equals("False") && SelectedProfiles.Contains(profileItem.Profile) == false )
                {
                    cellPressed.Value = true;
                    SelectedProfiles.Add(profileItem.Profile);
                }
                else if (cellPressed.Value.ToString().Equals("True") && SelectedProfiles.Contains(profileItem.Profile) == true)
                {
                    cellPressed.Value = false;
                    SelectedProfiles.Remove(profileItem.Profile);
                }
            }
        }

        private void dataGridExProfiles_KeyDown(object sender, KeyEventArgs e)
        {
            
            if (e.KeyCode == Keys.Enter)
            {
                buttonExOK.PerformClick();
            }
        }

        private void RoleForm_Load(object sender, EventArgs e)
        {
            this.Icon = ResourceLoader.GetIcon("$Icon.Roles");
            if (behavior == FormBehavior.Create)
            {
                Text = ResourceLoader.GetString2("RoleFormCreateText");
                buttonExOK.Text = ResourceLoader.GetString("RoleFormCreateButtonOkText");
            }
            else if (behavior == FormBehavior.Edit)
            {
                Text = ResourceLoader.GetString2("RoleFormEditText");
                buttonExOK.Text = ResourceLoader.GetString("RoleFormEditButtonOkText");

                if (selectedRole != null && (bool)selectedRole.Immutable)
                {
                    textBoxDescription.Enabled = false;
                    textBoxName.Enabled = false;
                    dataGridExProfiles.ReadOnly = false;
                    //int temp = buttonExOK.Left;
                    //buttonExOK.Left = buttonExCancel.Left;
                    //buttonExCancel.Left = temp;
                    buttonExOK.Text = ResourceLoader.GetString("RoleFormEditButtonOkText");
                    buttonExOK.Enabled = false;
                    buttonExCancel.Text = ResourceLoader.GetString("RoleFormEditButtonCancelText");

                }
            }
               
        }

        private void RoleForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.parentForm != null)
            {
                this.parentForm.AdministrationCommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(RoleForm_AdministrationCommittedChanges);
            }
        }



        private void dataGridExProfiles_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            ButtonsActivation(); 
        }

        private void textBoxName_KeyUp(object sender, KeyEventArgs e)
        {
            ButtonsActivation();
        }

        private void dataGridExProfiles_KeyUp(object sender, KeyEventArgs e)
        {

            this.BeginInvoke((MethodInvoker)delegate
            {
                ButtonsActivation();
            });    
            
            
        }


	}
}
