using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using SmartCadCore.Core;
using System.ServiceModel;
using System.Reflection;
using Smartmatic.SmartCad.Service;
using SmartCadCore.Common;
using SmartCadCore.ClientData;
using SmartCadCore.Enums;

namespace SmartCadGuiCommon
{
    public partial class XtraFormAdmistration<T> : XtraForm where T : ClientData, new()
    {
        #region Fields
        protected T data;
        protected FormBehavior behavior;
        protected AdministrationRibbonForm parentForm;
        protected bool buttonOkPressed = false;
        protected object syncCommitted = new object();
        #endregion

        #region Constructors
        public XtraFormAdmistration()
        {
            InitializeComponent();
        }

        public XtraFormAdmistration(AdministrationRibbonForm form)
		{
            InitializeComponent();
            this.parentForm = form;
            this.Load += new EventHandler(XtraFormAdm_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(XtraFormAdm_FormClosing);
            
        }

        public XtraFormAdmistration(AdministrationRibbonForm form, T data, FormBehavior behaviour)
            :this(form)
        {}
        #endregion

        #region Properties
        public virtual T SelectedData
        {
            get { return null; }
            set { }
        }

        public FormBehavior Behavior
        {
            set
            {
                behavior = value;

            }
            get
            {
                return behavior;
            }
        }

        protected virtual string UpdateMessage
        {
            get { return null; }
        }

        protected virtual string DeleteMessage
        {
            get { return null; }
        }

        protected virtual string CreateFormText
        {
            get { return null; }
        }

        protected virtual string EditFormText
        {
            get { return null; }
        }
        #endregion

        #region Events
        /// <summary>
        /// Loads the form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void XtraFormAdm_Load(object sender, EventArgs e)
        {
            LoadLanguage();

            if (behavior == FormBehavior.Create)
            {
                Text = ResourceLoader.GetString2(CreateFormText);
            }
            else if (behavior == FormBehavior.Edit)
            {
                Text = ResourceLoader.GetString2(EditFormText);
            }

            this.parentForm.AdministrationCommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(XtraFormAdm_AdministrationCommittedChanges);
            this.FormClosing += new FormClosingEventHandler(XtraFormAdmistration_FormClosing);
        }

        /// <summary>
        /// Perform operation when the form is closing.
        /// </summary>
        /// <param name="sender">Form</param>
        /// <param name="e">Form closing event args.</param>
        void XtraFormAdmistration_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (parentForm != null)
                this.parentForm.AdministrationCommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(XtraFormAdm_AdministrationCommittedChanges);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void XtraFormAdm_AdministrationCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    lock (syncCommitted)
                    {
                        try
                        {
                            if (ServerServiceClient.GetInstance().OperatorClient == null)
                            { }
                        }
                        catch
                        { }

                        if (e.Objects[0] is T)
                        {
                            #region Data
                            T cData =
                                    e.Objects[0] as T;
                            if (SelectedData != null && SelectedData.Equals(data) && buttonOkPressed == false)
                            {
                                if (e.Action == CommittedDataAction.Update)
                                {
                                    MessageForm.Show(ResourceLoader.GetString2(UpdateMessage), MessageFormType.Warning);
                                    SelectedData = cData;
                                }
                                else if (e.Action == CommittedDataAction.Delete)
                                {
                                    MessageForm.Show(ResourceLoader.GetString2(DeleteMessage), MessageFormType.Warning);
                                    FormUtil.InvokeRequired(this,
                                    delegate
                                    {
                                        this.Close();
                                    });
                                }

                            }
                            #endregion
                        }
                        else
                        {
                            ExecuteCommitedChanges(e);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        /// <summary>
        /// Occurs when the form is closing.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void XtraFormAdm_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            this.parentForm.AdministrationCommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(XtraFormAdm_AdministrationCommittedChanges);
        }

        /// <summary>
        /// Create or updates the current fleet.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ButtonAccept_Click(object sender, EventArgs e)
        {
            try
            {
                BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[1] { GetType().GetMethod("Save_Help", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[0] });
                processForm.CanThrowError = true;
                processForm.ShowDialog();
            }
            catch (FaultException ex)
            {
                if (Behavior == FormBehavior.Edit)
                    SelectedData = (T)ServerServiceClient.GetInstance().RefreshClient(SelectedData);
                GetErrorFocus(ex);
                DialogResult = System.Windows.Forms.DialogResult.None;
                MessageForm.Show(ex.Message, MessageFormType.Warning);
                buttonOkPressed = false;
            }
            catch (Exception ex)
            {
                DialogResult = System.Windows.Forms.DialogResult.None;
                MessageForm.Show(ex.Message, ex);
                buttonOkPressed = false;
            }
        }

        /// <summary>
        /// This is where the actual data is going to be saved.
        /// </summary>
        protected virtual void Save_Help(){}

        /// <summary>
        /// Redirect the error to the field that caused it.
        /// </summary>
        protected virtual void GetErrorFocus(FaultException ex){}

        /// <summary>
        /// Load the specific language text for the form.
        /// </summary>
        protected virtual void LoadLanguage(){}

        /// <summary>
        /// Validate if the accept button has to be enabled.
        /// </summary>
        protected virtual void CheckButtons(){}

        /// <summary>
        /// Loads the data necessary for the form.
        /// </summary>
        protected virtual void LoadData() { }

        protected virtual void ExecuteCommitedChanges(CommittedObjectDataCollectionEventArgs e) { }
        #endregion
    }
}