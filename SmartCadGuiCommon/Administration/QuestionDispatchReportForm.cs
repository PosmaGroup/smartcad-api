using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Smartmatic.SmartCad.Service;
using SmartCadCore.ClientData;
using SmartCadCore.Enums;
using SmartCadCore.Common;
using SmartCadCore.Core;


namespace SmartCadGuiCommon
{
    public partial class QuestionDispatchReportForm : QuestionDispatchReportBaseForm
    {
        #region Constructors
        public QuestionDispatchReportForm()
        {
            InitializeComponent();
            designerQuestionDevX.ControlChanged += new EventHandler<EventArgs>(CheckButtonsEventHandler);
        }

        public QuestionDispatchReportForm(AdministrationRibbonForm form)
            : base(form)
        {
            InitializeComponent();
            designerQuestionDevX.ControlChanged += new EventHandler<EventArgs>(CheckButtonsEventHandler);
        }

        public QuestionDispatchReportForm(AdministrationRibbonForm form, QuestionDispatchReportClientData data, FormBehavior behaviour)
            : this(form)
        {
            this.SelectedData = data;
            this.behavior = behaviour;
        }
        #endregion

        #region Properties

        public override QuestionDispatchReportClientData SelectedData
        {
            get
            {
                return data;
            }
            set
            {
                data = value;
                if (data != null)
                {
                    textEditName.Text = data.Text;
                    designerQuestionDevX.SetDesignerXML(data.Render);

                    if (data.Required == true)
                    {
                        textEditName.Enabled = false;
                        designerQuestionDevX.Enabled = false;

                    }
                }
                CheckButtons();
            }
        }

        protected override string UpdateMessage
        {
            get { return "UpdateFormQuestionDispatchReportData"; }
        }

        protected override string DeleteMessage
        {
            get { return "DeleteFormQuestionDispatchReportData"; }
        }

        protected override string CreateFormText
        {
            get { return "CreateQuestionDispatchReportFormText"; }
        }

        protected override string EditFormText
        {
            get { return "EditQuestionDispatchReportFormText"; }
        }

        #endregion

        #region Methods

        protected override void LoadLanguage()
        {
            this.Icon = ResourceLoader.GetIcon("$Icon.QuestionDispatchReport");
            layoutControlGroupName.Text = ResourceLoader.GetString2("GroupDataQuestionDispatchReport");
            layoutControlItemName.Text = ResourceLoader.GetString2("Name")+ ":*";
            layoutControlGroupAnswers.Text = ResourceLoader.GetString2("Answers");
            simpleButtonCancel.Text = ResourceLoader.GetString2("Cancel");
            
            if (behavior == FormBehavior.Create)
                simpleButtonOk.Text = ResourceLoader.GetString2("Create");
            else
                simpleButtonOk.Text = ResourceLoader.GetString2("Accept");
        }

        protected override void CheckButtons()
        {
            if (textEditName.Text == "" ||
                designerQuestionDevX.Fill == false)
                simpleButtonOk.Enabled = false;
            else
                simpleButtonOk.Enabled = true;
        }
                
        protected override void Save_Help()
        {
            buttonOkPressed = true;
            if (behavior == FormBehavior.Create)
                data = new QuestionDispatchReportClientData();

            FormUtil.InvokeRequired(this,
                    delegate
                    {
                        data.Text = textEditName.Text;
                        data.Render = designerQuestionDevX.GetDesignerXML();
                    });

            ServerServiceClient.GetInstance().SaveOrUpdateClientData(data);

            buttonOkPressed = false;
        }

        private void simpleButtonAccept_Click(object sender, EventArgs e)
        {
            ButtonAccept_Click(sender, e);
        }

        /// <summary>
        /// Invoke the CheckButtons method.
        /// Method to subscribe to control events.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CheckButtonsEventHandler(object sender, EventArgs e)
        {
            CheckButtons();
        }
        #endregion	
    }

    public class QuestionDispatchReportBaseForm : XtraFormAdmistration<QuestionDispatchReportClientData>
    {
        public QuestionDispatchReportBaseForm()
        {
        }

        public QuestionDispatchReportBaseForm(AdministrationRibbonForm form)
            : base(form)
        {
        }

        public QuestionDispatchReportBaseForm(AdministrationRibbonForm form, QuestionDispatchReportClientData data, FormBehavior behaviour)
            : base(form, data, behaviour)
        {
        }
    }
}