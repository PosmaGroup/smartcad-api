using System;
using System.Reflection;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.IO;
using System.Net.Mail;
using Smartmatic.SmartCad.Service;
using System.ServiceModel;
using Iesi.Collections;
using System.Collections.Generic;
using DevExpress.XtraEditors;
using SmartCadGuiCommon;
using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadCore.ClientData;
using SmartCadCore.Enums;

namespace Smartmatic.SmartCad.Gui
{
    #region Class UserAccountForm Documentation
    /// <summary>
    /// TODO:
    /// </summary>
    /// <className>UserAccountForm</className>
    /// <author email="fsilva@smartmatic.com">Fernando Silva</author>
    /// <author email="atorres@smartmatic.com">Alden Torres</author>
    /// <date>2006/04/12</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
	public partial class UserAccountForm : XtraForm
	{
        #region Synchronization objects

        private object syncCommited = new object();

        #endregion

        #region Fields
        OperatorClientData actualUserAccount = null;
        OperatorClientData selectedUserAccount = null;
        private AdministrationRibbonForm parentForm;
        FormBehavior behaviorType = FormBehavior.Edit;
        int departmentSelected = 0;
        byte[] imageData;
        //OperatorCategoryData initialOperatorCategory;
        string textLabelDepartment = string.Empty;
        string textLabelAgentId = string.Empty;
        string textLabelAgentPassword = string.Empty;
        bool camerasRequired = false;
        bool departmentTypeIsRequired = false;
        #endregion

        public UserAccountForm(AdministrationRibbonForm parentForm)
		{
			InitializeComponent();
            this.Icon = ResourceLoader.GetIcon("$Icon.Usuarios");
            actualUserAccount = ServerServiceClient.GetInstance().OperatorClient;
            FillRoles();
            FillDepartments();
            FillCctvZones();
            removePictureButton.Image = ResourceLoader.GetImage("$Image.RemoveUserPicture");
            imageImportButton.Image = ResourceLoader.GetImage("$Image.ImportPhoto");
            
            LoadLanguage();
            this.parentForm = parentForm;
            if (this.parentForm != null)
                this.parentForm.AdministrationCommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(UserAccountForm_CommittedChanges);
		}

        private void LoadLanguage()
        {
            buttonCancel.Text = ResourceLoader.GetString2("$Message.Cancel");
            #region Datos del usuario

            this.layoutControlUserData.Text = ResourceLoader.GetString2("$Message.Description");
            this.layoutControlGroupUserData.Text = ResourceLoader.GetString2("UserData");
            this.layoutControlName.Text = ResourceLoader.GetString2("FirstName") + ":*";
            this.layoutControlLastName.Text = ResourceLoader.GetString2("LastName") + ":*";
            this.layoutControlPersonID.Text = ResourceLoader.GetString2("CapsPersonId") + ":*";
            this.layoutControlDateBirth.Text = ResourceLoader.GetString2("DateofBirth") + ":*";
            this.layoutControlPhone.Text = ResourceLoader.GetString2("Phone") + ":";
            this.layoutControlEmail.Text = ResourceLoader.GetString2("Mail") + ":";
            this.layoutControlAddress.Text = ResourceLoader.GetString2("Address") + ":";

            #endregion

            #region Datos de la cuenta

            this.layoutControlAccountDescription.Text = ResourceLoader.GetString2("Account");
            this.layoutControlGroupAccount.Text = ResourceLoader.GetString2("DescriptionAccount");
            this.layoutControlIdentifier.Text = ResourceLoader.GetString2("IdentifierM") + ":*";
            this.layoutControlRole.Text = ResourceLoader.GetString2("Role") + ":*";
            this.layoutControlAgentID.Text = ResourceLoader.GetString2("AgentIDM") + ":";
            this.layoutControlAgentPassword.Text = ResourceLoader.GetString2("AgentPassword") + ":";
            this.layoutControlDepartaments.Text = ResourceLoader.GetString2("Departments") + ":";
            this.layoutControlItemPassword.Text = ResourceLoader.GetString2("Password") + ":*";
            this.layoutControlItemRepeatPassword.Text = ResourceLoader.GetString2("RepeatPassword") + ":*";

            #endregion

            #region Camaras
            //CameraAccess
            this.layoutControlCameras.Text = ResourceLoader.GetString2("Cameras");
            this.layoutControlGroupCameras.Text = ResourceLoader.GetString2("AssignCameraActionToolTip");
            this.layoutControlCCTVZone.Text = ResourceLoader.GetString2("CCTVZone") + ":";
            this.layoutControlStructType.Text = ResourceLoader.GetString2("StructType")+":";
            this.layoutControlStruct.Text =  ResourceLoader.GetString2("Struct")+":";
            this.layoutControlCamerasAvailable.Text = ResourceLoader.GetString2("AvailablesCameras");
            this.layoutControlCameraAsociated.Text = ResourceLoader.GetString2("AssociatedCameras");

            #endregion

        }
		public UserAccountForm(AdministrationRibbonForm parentForm, OperatorClientData userAccountI, FormBehavior type)
            :this(parentForm)
		{
            BehaviorType = type;
            SelectedUserAccount = userAccountI;            
		}

		/// <summary>
		/// Get or set the behavior type of the control.
		/// </summary>
		/// <value>
		/// The BehaviorType of the control. The default value is Edit.
		/// </value>
		public FormBehavior BehaviorType
		{
			get
            {
                return behaviorType;
            }
			set
			{               
				behaviorType = value;
			}
		}

        public OperatorClientData SelectedUserAccount
		{
			get
            {
                return selectedUserAccount;
            }
            set
            {
                FormUtil.InvokeRequired(this,
                delegate
                {
                    Clear();
                    selectedUserAccount = value;
                    if (selectedUserAccount != null)
                    {
                        textBoxName.Text = selectedUserAccount.FirstName;
                        textBoxLastName.Text = selectedUserAccount.LastName;
                        textBoxIdNumber.Text = selectedUserAccount.PersonID;
                        textBoxEMail.Text = selectedUserAccount.Email;
                        if (selectedUserAccount.Birthday.HasValue == true)
                            dateTimePicker1.Value = selectedUserAccount.Birthday.Value;
                        textBoxAddress.Text = selectedUserAccount.Address;
                        textBoxPhone.Text = selectedUserAccount.Telephone;
                        textBoxLogin.Text = selectedUserAccount.Login;

                        UserRoleClientData tempRole = new UserRoleClientData();
                        tempRole.Code = selectedUserAccount.RoleCode;
                        tempRole.Name = selectedUserAccount.RoleName;
                        tempRole.FriendlyName = selectedUserAccount.RoleFriendlyName;

                        comboBoxRole.SelectedItem = tempRole;
                        FillAssignDepartments();
                        textBoxExAgentID.Text = selectedUserAccount.AgentId;
                        textBoxExAgentPassword.Text = selectedUserAccount.AgentPassword;
                        if (selectedUserAccount.Picture != null)
                        {
                            imageData = selectedUserAccount.Picture;
                            userAccountPictureBox.Image = GetThumbnailImage(imageData);
                            imageData = selectedUserAccount.Picture;
                            removePictureButton.Enabled = true;
                        }

                        if (selectedUserAccount.Windows == null || selectedUserAccount.Windows == false)
                        {
                            textBoxPassword.Enabled = true;
                            textBoxRepeatPassword.Enabled = true;
                            checkBoxExWindowsAuthentication.Checked = false;
                        }

                        textBoxPassword.Text = "''''''''";
                        textBoxRepeatPassword.Text = "''''''''";

                        if (selectedUserAccount.FirstName == ResourceLoader.GetString2("AdministratorName"))
                        {
                            textBoxName.Enabled = false;
                            textBoxLastName.Enabled = false;
                            textBoxIdNumber.Enabled = false;
                            textBoxEMail.Enabled = false;
                            dateTimePicker1.Enabled = false;
                            textBoxAddress.Enabled = false;
                            textBoxPhone.Enabled = false;
                            textBoxLogin.Enabled = false;
                            checkedListBoxDepartamentos.Enabled = false;
                            comboBoxRole.Enabled = false;
                            textBoxExAgentID.Enabled = false;
                            textBoxExAgentPassword.Enabled = false;

                            comboBoxRole.Location = new System.Drawing.Point(127, 104);
                            textBoxExAgentID.Location = new System.Drawing.Point(127, 133);
                            textBoxExAgentPassword.Location = new Point(127,162);

                            checkedListBoxDepartamentos.Location = new System.Drawing.Point(127, 171);

                            this.layoutControlItemPassword.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                            this.layoutControlItemRepeatPassword.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;

                            imageImportButton.Enabled = false;
                            userAccountPictureBox.Enabled = false;
                        }

                        if (ServerServiceClient.GetInstance().CheckDispatchOperator(selectedUserAccount.RoleCode) == false &&
                            ServerServiceClient.GetInstance().CheckDispatchOperator(selectedUserAccount.RoleCode) == false &&
                            ServerServiceClient.GetInstance().CheckGeneralSupervisor(selectedUserAccount.RoleCode) == false &&
                            ServerServiceClient.GetInstance().CheckUserAccess(selectedUserAccount.RoleCode, "UserApplicationData+Basic+Cctv") == false &&
                            ServerServiceClient.GetInstance().CheckApplicationSupervisor(selectedUserAccount.RoleCode, UserApplicationClientData.Dispatch.Name) == false)
                        {
                            this.tabbedControlGroup1.RemoveTabPage(this.layoutControlCameras);
                            camerasRequired = false;
                        }
                        else
                        {
                            buttonExRemoveAllCameras_Click(null, null);
                            listBoxExUnAvailablesCameras.Items.Clear();
                            try
                            {
                                //ServerServiceClient.GetInstance().InitializeLazy(selectedUserAccount, selectedUserAccount.cameras);

                                int i = selectedUserAccount.Cameras.Count;
                            }
                            catch
                            {
                                selectedUserAccount.Cameras = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetCamerasByOperatorCode, selectedUserAccount.Code));
                            }
                            foreach (OperatorDeviceClientData cam in selectedUserAccount.Cameras)
                            {
                                listBoxExUnAvailablesCameras.Items.Add(cam);
                                if (listBoxExAvailablesCameras.Items.Contains(cam) == true)
                                {
                                    listBoxExAvailablesCameras.Items.Remove(cam);
                                }
                            }
                        }
                    }
                    else
                    {
                        RemoveCameraTab();
                    }
                    UserAccountParameters_Change(null, null);
                    this.Refresh();
                });
            }
		}

        
		
        private void Clear()
		{
			textBoxLastName.Text = "";
			textBoxName.Text = "";
			textBoxIdNumber.Text = "";
			textBoxEMail.Text = "";
			textBoxAddress.Text = "";
			textBoxPhone.Text = "";
			textBoxLogin.Text = "";
			textBoxPassword.Text = "";
			textBoxRepeatPassword.Text = "";
            comboBoxRole.SelectedIndex = -1;
            checkedListBoxDepartamentos.ClearSelected();
			selectedUserAccount = null;
            SetDefaultImage();
		}

		private void UserAccountParameters_Change(object sender, System.EventArgs e)
		{

            //COMMENTED IN VERSION 3.2.8 bataan
            //    if((string.IsNullOrEmpty(textBoxLastName.Text.Trim())) ||
            //    (string.IsNullOrEmpty(textBoxName.Text.Trim())) ||
            //    (string.IsNullOrEmpty(textBoxIdNumber.Text.Trim())) ||
            //    (string.IsNullOrEmpty(textBoxLogin.Text.Trim())) ||
            //    (this.comboBoxRole.SelectedIndex == -1) ||
            //    (this.camerasRequired == true && listBoxExUnAvailablesCameras.Items.Count == 0) ||
            //    (ApplicationUtil.IsAgeValid(dateTimePicker1.Value, 18)) ||
            //    (checkBoxExWindowsAuthentication.Checked == false &&
            //    ((string.IsNullOrEmpty(textBoxPassword.Text.Trim())) ||
            //    (string.IsNullOrEmpty(textBoxRepeatPassword.Text.Trim())))) ||
            //        (textBoxExAgentID.Enabled == true &&
            //         string.IsNullOrEmpty(textBoxExAgentID.Text.Trim()) == true) ||
            //         (textBoxExAgentPassword.Enabled == true &&
            //         string.IsNullOrEmpty(textBoxExAgentPassword.Text.Trim()) == true) ||
            //        (this.comboBoxRole.SelectedItem != null &&
            //         (departmentTypeIsRequired == true && departmentSelected == 0)))
            //    buttonOk.Enabled = false;
            //else
            //    buttonOk.Enabled = true;

            buttonOk.Enabled = true;
		}

        private bool buttonOkPressed = false;

        public bool ButtonOkPressed
        {
            get { return buttonOkPressed; }
        }

		private void buttonOk_ClickHelpFunction1()
		{
            buttonOkPressed = true;
            if (behaviorType == FormBehavior.Create)
            {
                selectedUserAccount = new OperatorClientData();
                selectedUserAccount.CategoryList = new ArrayList();
                OperatorCategoryHistoryClientData ochd = new OperatorCategoryHistoryClientData();
                ochd.CategoryCode= OperatorCategoryClientData.OnTraining.Code;
                ochd.CategoryFriendlyName = OperatorCategoryClientData.OnTraining.FriendlyName;
                ochd.OperatorCode = selectedUserAccount.Code;
                ochd.StartDate = ServerServiceClient.GetInstance().GetTime();
                selectedUserAccount.CategoryList.Add(ochd);
            }
               
            if(selectedUserAccount!=null)
			{
				selectedUserAccount.FirstName = textBoxName.Text.Trim();
				selectedUserAccount.LastName = textBoxLastName.Text.Trim();
				selectedUserAccount.PersonID = textBoxIdNumber.Text.Trim();
                if (!string.IsNullOrEmpty(textBoxEMail.Text))
                    selectedUserAccount.Email = textBoxEMail.Text.Trim();
                else
                    selectedUserAccount.Email = null;
                if (!string.IsNullOrEmpty(textBoxAddress.Text))
                    selectedUserAccount.Address = textBoxAddress.Text.Trim();
                else
                    selectedUserAccount.Address = null;
				selectedUserAccount.Telephone = textBoxPhone.Text.Trim();
				selectedUserAccount.Login = textBoxLogin.Text.Trim();
                selectedUserAccount.Birthday = dateTimePicker1.Value;
                FormUtil.InvokeRequired(comboBoxRole, delegate
                {
                    UserRoleClientData tempRole = (UserRoleClientData)comboBoxRole.SelectedItem;
                    selectedUserAccount.RoleCode = tempRole.Code;
                    selectedUserAccount.RoleName = tempRole.Name;
                    selectedUserAccount.RoleFriendlyName = tempRole.FriendlyName;
                });
                selectedUserAccount.AgentId = textBoxExAgentID.Text;
                selectedUserAccount.AgentPassword = textBoxExAgentPassword.Text;
                selectedUserAccount.Windows = checkBoxExWindowsAuthentication.Checked;

                string pass = textBoxPassword.Text.Trim();
                if (pass != "''''''''")
                    selectedUserAccount.Password = CryptoUtil.Hash(pass);
                
                selectedUserAccount.Picture = imageData;

                FormUtil.InvokeRequired(checkedListBoxDepartamentos, delegate
                {
                    selectedUserAccount.DepartmentTypes = new ArrayList(checkedListBoxDepartamentos.CheckedItems);
                });

                GetCameras();
                ServerServiceClient.GetInstance().SaveOrUpdateClientData(selectedUserAccount);
			}
		}
		
		private void buttonOk_Click(object sender, System.EventArgs e)
		{
            if (checkBoxExWindowsAuthentication.Checked == false && textBoxPassword.Text.Length < OperatorClientData.MinimunPasswordLength)
			{
				DialogResult = DialogResult.None;
                MessageForm.Show(ResourceLoader.GetString2("Min6CharsPassword"), MessageFormType.Error);
				return;
			}
            else if (checkBoxExWindowsAuthentication.Checked == false &&  textBoxPassword.Text != textBoxRepeatPassword.Text)
			{
				DialogResult = DialogResult.None;
                MessageForm.Show(ResourceLoader.GetString2("PasswordDoesntMatchConfirmation"), MessageFormType.Error);
				return;
			}
            else if (string.IsNullOrEmpty(textBoxName.Text))
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ResourceLoader.GetString2("BlankFirstName"), MessageFormType.Error);
                tabbedControlGroup1.SelectedTabPage = layoutControlUserData;
                textBoxName.Focus();
                return;
            }
            else if (string.IsNullOrEmpty(textBoxLastName.Text))
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ResourceLoader.GetString2("BlankLastName"), MessageFormType.Error);
                tabbedControlGroup1.SelectedTabPage = layoutControlUserData;
                textBoxLastName.Focus();
                return;
            }
            else if (!string.IsNullOrEmpty(textBoxEMail.Text) && ValidatorUtil.ValidateEmail(textBoxEMail.Text.Trim()) == false )
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ResourceLoader.GetString2("BadEmailFormat"), MessageFormType.Error);
                tabbedControlGroup1.SelectedTabPage = layoutControlUserData;
                textBoxEMail.Focus();
                textBoxEMail.SelectAll();
                return ;
            }
            else if (string.IsNullOrEmpty(textBoxIdNumber.Text))
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ResourceLoader.GetString2("BlankUserID"), MessageFormType.Error);
                tabbedControlGroup1.SelectedTabPage = layoutControlUserData;
                textBoxIdNumber.Focus();
                return;
            }
            else if ((((TimeSpan)DateTime.Now.Subtract(dateTimePicker1.Value)).TotalDays / 365.25) < 18.00)
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ResourceLoader.GetString2("InvalidBirthday"), MessageFormType.Error);
                tabbedControlGroup1.SelectedTabPage = layoutControlUserData;
                dateTimePicker1.Focus();
                return;
            }
            else if (string.IsNullOrEmpty(textBoxLogin.Text))
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ResourceLoader.GetString2("InvalidLoginMessage"), MessageFormType.Error);
                tabbedControlGroup1.SelectedTabPage = layoutControlAccountDescription;
                textBoxLogin.Focus();
                return;
            }
            else if (string.IsNullOrEmpty(comboBoxRole.Text))
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ResourceLoader.GetString2("InvalidRole"), MessageFormType.Error);
                tabbedControlGroup1.SelectedTabPage = layoutControlAccountDescription;
                textBoxLogin.Focus();
                return;
            }
            else if (departmentTypeIsRequired == true && departmentSelected == 0)
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ResourceLoader.GetString2("InvalidDepartamentType"), MessageFormType.Error);
                tabbedControlGroup1.SelectedTabPage = layoutControlAccountDescription;
                checkedListBoxDepartamentos.Focus();
                return;
            }
            else if (this.camerasRequired == true && listBoxExUnAvailablesCameras.Items.Count == 0)
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ResourceLoader.GetString2("InvalidCameras"), MessageFormType.Error);
                tabbedControlGroup1.SelectedTabPage = layoutControlCameras;
                checkedListBoxDepartamentos.Focus();
                return;
            }

            try
            {
                BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[1] { GetType().GetMethod("buttonOk_ClickHelpFunction1", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[0] });
                processForm.CanThrowError = true;
                processForm.ShowDialog();

                SelectedUserAccount = (OperatorClientData)ServerServiceClient.GetInstance().RefreshClient(selectedUserAccount);
            }
            catch (FaultException ex)
            {
                string password = textBoxPassword.Text;
                string repeatPassword = textBoxRepeatPassword.Text;

                if (SelectedUserAccount.Code > 0)
                    SelectedUserAccount = (OperatorClientData)ServerServiceClient.GetInstance().RefreshClient(SelectedUserAccount);

                textBoxPassword.Text = password;
                textBoxRepeatPassword.Text = repeatPassword;
                GetErrorFocus(ex);
             
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, MessageFormType.Error);
                buttonOkPressed = false;
            }
            catch (Exception ex)
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, ex);
                buttonOkPressed = false;
            }
		}


        private void GetErrorFocus(FaultException ex)
        {
            if (ex.Message.Contains(ResourceLoader.GetString2("UK_USER_ACCOUNT_PERSON_ID")))
            {
                if (behaviorType == FormBehavior.Create)
                    textBoxIdNumber.Text = textBoxIdNumber.Text;

                //TabControl.SelectedTabPageIndex = 0;
                textBoxIdNumber.Focus();
            }
            else if (ex.Message.Contains(ResourceLoader.GetString2("UK_USER_ACCOUNT_LOGIN")))
            {
                if (behaviorType == FormBehavior.Create)
                    textBoxLogin.Text = textBoxLogin.Text;
				//TabControl.SelectedTabPageIndex = 1;
                textBoxLogin.Focus();
            }
            else if (ex.Message.Contains(ResourceLoader.GetString2("UK_USER_ACCOUNT_AGENTID")))
            {
                if (behaviorType == FormBehavior.Create)
                    textBoxExAgentID.Text = textBoxExAgentID.Text;
				//TabControl.SelectedTabPageIndex = 1;
                textBoxExAgentID.Focus();
            }

        }

        private void FillRoles()
        {
            comboBoxRole.Items.Clear();
            int maxlen = 0;
            foreach (UserRoleClientData role in ServerServiceClient.GetInstance().SearchClientObjects(typeof(UserRoleClientData)))
            {
                comboBoxRole.Items.Add(role);
                maxlen = Math.Max(role.ToString().Length, maxlen);
            }
            comboBoxRole.DropDownWidth = maxlen*7;
        }

        private void ComboBoxRole_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (comboBoxRole.SelectedItem != null)
            {
                bool isDispatch = false;
                UserRoleClientData userRole = (UserRoleClientData)comboBoxRole.SelectedItem;
                camerasRequired = false;
            
                int indexLabel = this.layoutControlAgentID.Text.IndexOf(" *");
                if (indexLabel > -1)
                    layoutControlAgentID.Text = layoutControlAgentID.Text.Substring(0, indexLabel);
                
                indexLabel = this.layoutControlAgentPassword.Text.IndexOf(" *");
                if (indexLabel > -1)
                    layoutControlAgentPassword.Text = layoutControlAgentPassword.Text.Substring(0, indexLabel);
                
                indexLabel = layoutControlDepartaments.Text.IndexOf(" *");
                if (indexLabel > -1)
                    layoutControlDepartaments.Text = layoutControlDepartaments.Text.Substring(0, indexLabel);


                if (userRole == UserRoleClientData.Developer
                    || ServerServiceClient.GetInstance().CheckGeneralSupervisor(userRole.Code))
                {
                    textBoxExAgentID.Enabled = true;
                    textBoxExAgentPassword.Enabled = true;
                    layoutControlAgentID.Text = layoutControlAgentID.Text + " *";
                    layoutControlAgentPassword.Text = layoutControlAgentPassword.Text + " *";

                    if (this.layoutControlCameras.IsInTabbedGroup == false)
                        AddTabCameras();

                    isDispatch = true;

                }
                else if (ServerServiceClient.GetInstance().CheckFirstLevelOperator(userRole.Code) 
                    || ServerServiceClient.GetInstance().CheckApplicationSupervisor(userRole.Code, UserApplicationClientData.FirstLevel.Name))
                {
                    textBoxExAgentID.Enabled = true;
                    textBoxExAgentPassword.Enabled = true;
                   layoutControlAgentID.Text = layoutControlAgentID.Text + " *";
                   layoutControlAgentPassword.Text = layoutControlAgentPassword.Text + " *";

                    RemoveCameraTab();

                }
                else if (ServerServiceClient.GetInstance().CheckUserAccess(userRole.Code, "UserApplicationData+Basic+Cctv"))
                {
                    textBoxExAgentID.Text = string.Empty;
                    textBoxExAgentID.Enabled = false;
                    textBoxExAgentPassword.Text = string.Empty;
                    textBoxExAgentPassword.Enabled = false;
                    if (this.layoutControlCameras.IsInTabbedGroup == false)
                        AddTabCameras();
                    camerasRequired = true;
                   
                }
                else if (ServerServiceClient.GetInstance().CheckDispatchOperator(userRole.Code)
                        || ServerServiceClient.GetInstance().CheckApplicationSupervisor(userRole.Code, UserApplicationClientData.Dispatch.Name))
                {
                    textBoxExAgentID.Text = string.Empty;
                    textBoxExAgentID.Enabled = false;
                    textBoxExAgentPassword.Text = string.Empty;
                    textBoxExAgentPassword.Enabled = false;
                    //layoutControlAgentID.Text = layoutControlAgentID.Text + " *";
                    //layoutControlAgentPassword.Text = layoutControlAgentPassword.Text + " *";
                    
                    if (this.layoutControlCameras.IsInTabbedGroup == false)
                         AddTabCameras();
               
                    isDispatch = true;
                }                
                else
                {
                    textBoxExAgentID.Text = string.Empty;
                    textBoxExAgentID.Enabled = false;
                    textBoxExAgentPassword.Text = string.Empty;
                    textBoxExAgentPassword.Enabled = false;
                    RemoveCameraTab();
                }


                if (isDispatch == true || CanSelectDepartmentTypes(userRole.Code))
                {
                    checkedListBoxDepartamentos.Enabled = true;
                    layoutControlDepartaments.Text = layoutControlDepartaments.Text + " *";

                    for (int i = 0; i < checkedListBoxDepartamentos.Items.Count; i++)
                    {
                        checkedListBoxDepartamentos.SetItemChecked(i, true);
                    }
                    departmentTypeIsRequired = true;
                }
                else
                {
                    checkedListBoxDepartamentos.ClearCheckedItems();
                    checkedListBoxDepartamentos.Enabled = false;
                    departmentTypeIsRequired = false;
                }


                if (camerasRequired == true)
                    this.layoutControlCameraAsociated.Text = ResourceLoader.GetString2("AssociatedCameras") + " *";
                

                textLabelAgentId = layoutControlAgentID.Text;
                textLabelAgentPassword = layoutControlAgentPassword.Text;
                textLabelDepartment = layoutControlDepartaments.Text;
                UserAccountParameters_Change(null, null);
            }
        }

        private bool CanSelectDepartmentTypes(int userRoleCode)
        {
            ServerServiceClient serverServiceClient = ServerServiceClient.GetInstance();
            
            string[] accesses = {"MapLayerDepartmentStations",
                                  "MapLayerDepartmentZones",
                                  "MapLayerUnits",
                                  "FleetControl",
                                  "HistoricTracks"};
            
            string hql = string.Format(@"select count(*) from UserRoleData role   
						  left join role.Profiles profile  
						  left join profile.Accesses access   
						  where role.Code = {0} and (", userRoleCode);

            for (int i = 0; i < accesses.Length; i++)
            {
                hql += string.Format("access.Name like '%{0}%' or ", accesses[i]);    
            }

            hql = hql.Substring(0, hql.Length - 3) + ")";

            
            long result = (long)ServerServiceClient.GetInstance().SearchBasicObject(hql);
                        
            if (result > 0)
                return true;

            return false;
        }


        private void RemoveCameraTab()
        {
            this.tabbedControlGroup1.RemoveTabPage(this.layoutControlCameras);
            camerasRequired = false;
        }

        private void AddTabCameras()
        {
            int oldOne = this.tabbedControlGroup1.SelectedTabPageIndex;
            this.tabbedControlGroup1.AddTabPage(this.layoutControlCameras);
            this.tabbedControlGroup1.SelectedTabPageIndex = oldOne;
        }

        private void FillDepartments()
        {
            IList departments = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetDepartmentsType);

            foreach (DepartmentTypeClientData department in departments)
            {
                checkedListBoxDepartamentos.Items.Add(department);
            }
        }

        private void FillAssignDepartments()
        {
            string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetDepartmentsTypeByOperatorCode, selectedUserAccount.Code);
            IList list = ServerServiceClient.GetInstance().SearchClientObjects(hql);

            selectedUserAccount.DepartmentTypes = list;
            for (int i = 0; i < checkedListBoxDepartamentos.Items.Count; i++)
            {
                checkedListBoxDepartamentos.SetItemChecked(i, false);
                DepartmentTypeClientData departmentType = checkedListBoxDepartamentos.Items[i] as DepartmentTypeClientData;
                foreach (DepartmentTypeClientData departmentTypeAux in selectedUserAccount.DepartmentTypes)
                {
                    if (departmentType.Name == departmentTypeAux.Name)
                    {
                        checkedListBoxDepartamentos.SetItemCheckState(i, CheckState.Checked);

                    }                               

                }
            }
        }

        private void comboBoxExDeparments_SelectedIndexChanged(object sender, EventArgs e)
        {
            UserAccountParameters_Change(null, null);
        }   
        
        void checkedListBoxDepartamentos_CheckedItem(object sender, ItemCheckEventArgs e)
        {
            if (e.NewValue == CheckState.Checked)
                ++departmentSelected;
            else if (e.NewValue == CheckState.Unchecked)
                --departmentSelected;
            
            UserAccountParameters_Change(null, null);
        }

        private void butonImageImport_Click(object sender, EventArgs e)
        {
            DialogResult resOpen = importUserAccountImage.ShowDialog();

            if (resOpen == DialogResult.OK)
            {
                imageData = File.ReadAllBytes(importUserAccountImage.FileName);
                userAccountPictureBox.Image = GetThumbnailImage(imageData);
                MemoryStream ms = new MemoryStream();
                userAccountPictureBox.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                if (ms.Length < 10000)
                {
                    imageData = ms.ToArray();
                    removePictureButton.Enabled = true;
                }
                else
                {
                    MessageForm.Show(ResourceLoader.GetString2("ImageSizeTooBig"), MessageFormType.Error);
                    imageImportButton.Focus();
                    SetDefaultImage();
                }
            }
        }

        private void SetDefaultImage()
        {
            userAccountPictureBox.Image = ResourceLoader.GetImage("$Image.DefaultPhoto");
            MemoryStream ms = new MemoryStream();
            userAccountPictureBox.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            imageData = ms.ToArray();
            userAccountPictureBox.Image = GetThumbnailImage(imageData);
        }

        private Image GetThumbnailImage(byte[] data)
        {
            MemoryStream ms = new MemoryStream(data);
            Image image = Image.FromStream(ms);
            Image thumbNailImage = image.GetThumbnailImage(userAccountPictureBox.Width, userAccountPictureBox.Height, new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallback), IntPtr.Zero);

            return thumbNailImage;
        }

        private bool ThumbnailCallback()
        {
            return true;
        }

        private void removePictureButton_Click(object sender, EventArgs e)
        {
            if (userAccountPictureBox.Image != null)
            {
                SetDefaultImage();
                removePictureButton.Enabled = false;
            }
        }

        #region CCTV Tab

        private void GetCameras()
        {
            if (this.tabbedControlGroup1.TabPages.Contains(layoutControlCameras) == true)
            {

                selectedUserAccount.Cameras = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorDevicesByOperatorCode, selectedUserAccount.Code)); 

                if (selectedUserAccount.Cameras == null)
                {
                    selectedUserAccount.Cameras = new ArrayList();
                }

                IList tempList = new ArrayList(selectedUserAccount.Cameras);
                foreach (OperatorDeviceClientData operCam in tempList)
                {
                    if (listBoxExUnAvailablesCameras.Items.Contains(operCam) == false)
                    {
                        operCam.End = DateTime.Now;
                        ServerServiceClient.GetInstance().DeleteClientObject(operCam);
                        selectedUserAccount.Cameras.Remove(operCam);
                    }
                }

                foreach (OperatorDeviceClientData operCam in listBoxExUnAvailablesCameras.Items)
                {
                    if (operCam.Code == 0)
                    {
                        OperatorDeviceClientData cam = new OperatorDeviceClientData();
                        cam.User = selectedUserAccount;
                        cam.Device = operCam.Device;
                        cam.Start = DateTime.Now;
                        selectedUserAccount.Cameras.Add(cam);
                    }
                }
            }
        }

        private void listBoxExAvailablesCameras_DoubleClick(object sender, EventArgs e)
        {
            buttonExAddCamera_Click(sender, e);
        }

        private void listBoxExAvailablesCameras_Enter(object sender, EventArgs e)
        {
            listBoxExUnAvailablesCameras.ClearSelected();
        }

        private void listBoxExUnAvailablesCameras_DoubleClick(object sender, EventArgs e)
        {
            buttonExRemoveCamera_Click(sender, e);
        }

        private void listBoxExUnAvailablesCameras_Enter(object sender, EventArgs e)
        {
            listBoxExAvailablesCameras.ClearSelected();
        }

        private void buttonExAddCamera_Click(object sender, EventArgs e)
        {
            ArrayList toDelete = new ArrayList();
            foreach (OperatorDeviceClientData camera in listBoxExAvailablesCameras.SelectedItems)
            {
                toDelete.Add(camera);
                listBoxExUnAvailablesCameras.Items.Add(camera);
            }
            foreach (OperatorDeviceClientData camera in toDelete)
            {
                listBoxExAvailablesCameras.Items.Remove(camera);
            }
            UserAccountParameters_Change(null, null);
        }

        private void buttonExAddAllCameras_Click(object sender, EventArgs e)
        {
            foreach (OperatorDeviceClientData camera in listBoxExAvailablesCameras.Items)
            {
                listBoxExUnAvailablesCameras.Items.Add(camera);
            }
            listBoxExAvailablesCameras.Items.Clear();
            UserAccountParameters_Change(null, null);
        }

        private void buttonExRemoveCamera_Click(object sender, EventArgs e)
        {
            ArrayList toDelete = new ArrayList();
            foreach (OperatorDeviceClientData camera in listBoxExUnAvailablesCameras.SelectedItems)
            {
                toDelete.Add(camera);
                if ((comboBoxCctvZone.Text == ResourceLoader.GetString2("SecurityForm.Zones.All")) ||
                    ((camera.Device as VideoDeviceClientData).StructClientData.CctvZone.Code == ((CctvZoneClientData)comboBoxCctvZone.SelectedItem).Code))
                {
                    if ((comboBoxExStructType.Text == ResourceLoader.GetString2("SecurityForm.StructTypes.All")) ||
                        ((camera.Device as VideoDeviceClientData).StructClientData.Type == (StructTypeClientData)comboBoxExStructType.SelectedItem))
                    {
                        if ((comboBoxExStruct.SelectedItem.ToString() == ResourceLoader.GetString2("SecurityForm.Structs.All")) ||
                            ((camera.Device as VideoDeviceClientData).StructClientData.Name == ((StructClientData)comboBoxExStruct.SelectedItem).Name))
                        {
                            if (listBoxExAvailablesCameras.Items.Contains(camera) == false)
                            {
                                listBoxExAvailablesCameras.Items.Add(camera);
                            }
                        }
                    }
                }
            }

            foreach (OperatorDeviceClientData camera in toDelete)
            {
                int index = -1;
                int i = 0;
                foreach (OperatorDeviceClientData cam in listBoxExUnAvailablesCameras.Items)
                {
                    if (cam.Device.Code == camera.Device.Code)
                    {
                        index = i;
                        break;
                    }
                    i++;
                }
                if (index != -1)
                    listBoxExUnAvailablesCameras.Items.RemoveAt(index);
            }

            UserAccountParameters_Change(null, null);
        }

        private void buttonExRemoveAllCameras_Click(object sender, EventArgs e)
        {
            foreach (OperatorDeviceClientData camera in listBoxExUnAvailablesCameras.Items)
            {
                if ((comboBoxCctvZone.Text == ResourceLoader.GetString2("SecurityForm.Zones.All")) ||
                    ((camera.Device as VideoDeviceClientData).StructClientData.CctvZone.Code == ((CctvZoneClientData)comboBoxCctvZone.SelectedItem).Code))
                {
                    if ((comboBoxExStructType.Text == ResourceLoader.GetString2("SecurityForm.StructTypes.All")) ||
                        ((camera.Device as VideoDeviceClientData).StructClientData.Type == (StructTypeClientData)comboBoxExStructType.SelectedItem))
                    {
                        if ((comboBoxExStruct.SelectedItem.ToString() == ResourceLoader.GetString2("SecurityForm.Structs.All")) ||
                            ((camera.Device as VideoDeviceClientData).StructClientData.Name == ((StructClientData)comboBoxExStruct.SelectedItem).Name))
                        {
                            if (listBoxExAvailablesCameras.Items.Contains(camera) == false)
                            {
                                listBoxExAvailablesCameras.Items.Add(camera);
                            }
                        }
                    }
                }
            }
            listBoxExUnAvailablesCameras.Items.Clear();
            UserAccountParameters_Change(null, null);
        }

        private void comboBoxCctvZone_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillStructTypes();
        }

        private void comboBoxExStructType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxExStructType.Items.Count > 0) FillStructs();
        }

        private void comboBoxExStruct_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillCamerasList();
        }

        private void FillCamerasList()
        {
            if (comboBoxCctvZone.SelectedItem != null && comboBoxExStruct.SelectedItem != null && comboBoxExStructType.SelectedItem != null)
            {
                string hql;
                listBoxExAvailablesCameras.Items.Clear();
                if (comboBoxExStruct.SelectedItem.ToString() == ResourceLoader.GetString2("SecurityForm.Structs.All"))
                {
                    if (comboBoxExStructType.Text == ResourceLoader.GetString2("SecurityForm.StructTypes.All"))
                    {
                        if (comboBoxCctvZone.SelectedItem.ToString() == ResourceLoader.GetString2("SecurityForm.Zones.All"))
                        {
                            hql = SmartCadHqls.GetAllCameras;
                        }
                        else
                        {
                            hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetCamerasByZoneName,
                                ((CctvZoneClientData)comboBoxCctvZone.SelectedItem).Name);
                        }
                    }
                    else
                    {
                        if (comboBoxCctvZone.SelectedItem.ToString() == ResourceLoader.GetString2("SecurityForm.Zones.All"))
                        {
                            hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetCamerasByStructTypeName,
                                ((StructTypeClientData)comboBoxExStructType.SelectedItem).Name);
                        }
                        else
                        {
                            hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetCamerasByStructTypeNameZoneName,
                                ((StructTypeClientData)comboBoxExStructType.SelectedItem).Name,
                                ((CctvZoneClientData)comboBoxCctvZone.SelectedItem).Name);
                        }
                    }
                }
                else
                {
                    hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetCamerasByStructName,
                        ((StructClientData)comboBoxExStruct.SelectedItem).Name);
                }
                ArrayList list = (ArrayList)ServerServiceClient.GetInstance().SearchClientObjects(hql);
                foreach (CameraClientData camera in list)
                {
                    OperatorDeviceClientData operCam = new OperatorDeviceClientData();
                    operCam.Device = camera;
                    if (FindCameraInList(listBoxExUnAvailablesCameras.Items,operCam) == false &&
                        FindCameraInList(listBoxExAvailablesCameras.Items, operCam) == false)
                    {
                        listBoxExAvailablesCameras.Items.Add(operCam);
                    }
                }
            }
        }

        private bool FindCameraInList(ListBox.ObjectCollection collection, OperatorDeviceClientData camera)
        {
            bool result = false;
            foreach (OperatorDeviceClientData ocd in collection)
            {
                if (ocd.Device.Code == camera.Device.Code)
                {
                    result = true;
                    break;
                }
            }
            return result;
        }

        private void FillCctvZones()
        {
            comboBoxCctvZone.Items.Clear();
            IList list = ServerServiceClient.GetInstance().SearchClientObjects(typeof(CctvZoneClientData));
            foreach (CctvZoneClientData cctvZone in list)
            {
                comboBoxCctvZone.Items.Add(cctvZone);
            }
            
            CctvZoneClientData all = new CctvZoneClientData();
            all.Name = ResourceLoader.GetString2("SecurityForm.Zones.All");
            comboBoxCctvZone.Items.Add(all);
            comboBoxCctvZone.SelectedItem = all;
        }
        
        private void FillStructTypes()
        {
            StructTypeClientData select = null;
            IList list = new ArrayList();
            if (comboBoxCctvZone.Text != ResourceLoader.GetString2("SecurityForm.Zones.All"))
            {
                list = ServerServiceClient.GetInstance().SearchClientObjects(
                    SmartCadHqls.GetCustomHql(SmartCadHqls.GetStructsByCctvZoneCode,
                    ((CctvZoneClientData)comboBoxCctvZone.SelectedItem).Code));
            }
            else
            {
                list = ServerServiceClient.GetInstance().SearchClientObjects(typeof(StructClientData));
            }

            if (comboBoxExStructType.SelectedItem != null)
                select = (StructTypeClientData)comboBoxExStructType.SelectedItem;

            comboBoxExStructType.Items.Clear();
            foreach (StructClientData str in list)
            {
                if (comboBoxExStructType.Items.Contains(str.Type) == false)
                comboBoxExStructType.Items.Add(str.Type);
            }

            StructTypeClientData all = new StructTypeClientData();
            all.Name = ResourceLoader.GetString2("SecurityForm.StructTypes.All");
            comboBoxExStructType.Items.Add(all);

            comboBoxExStructType.SelectedItem = select;
            if (comboBoxExStructType.SelectedItem == null)
                comboBoxExStructType.SelectedItem = all;

        }

        private void FillStructs()
        {
            comboBoxExStruct.Items.Clear();
            IList list = new ArrayList();
            if (comboBoxCctvZone.Text != ResourceLoader.GetString2("SecurityForm.Zones.All"))
            {
                list = ServerServiceClient.GetInstance().SearchClientObjects(
                    SmartCadHqls.GetCustomHql(SmartCadHqls.GetStructsByCctvZoneCode,
                    ((CctvZoneClientData)comboBoxCctvZone.SelectedItem).Code));
            }
            else
            {
                list = ServerServiceClient.GetInstance().SearchClientObjects(typeof(StructClientData));
            }

            foreach (StructClientData str in list)
            {
                if ((comboBoxExStructType.SelectedItem.ToString() == ResourceLoader.GetString2("SecurityForm.StructTypes.All")) ||
                    (str.Type.Equals((StructTypeClientData)comboBoxExStructType.SelectedItem)))
                {
                    comboBoxExStruct.Items.Add(str);
                }
            }

            StructClientData all = new StructClientData();
            all.Name = ResourceLoader.GetString2("SecurityForm.Structs.All");
            comboBoxExStruct.Items.Add(all);
            comboBoxExStruct.SelectedItem = all;
        }
        
        #endregion

        void UserAccountForm_CommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    lock (syncCommited)
                    {
                        try
                        {
                            if (ServerServiceClient.GetInstance().OperatorClient == null)
                            { }
                        }
                        catch
                        { }
                        if (e.Objects[0] is OperatorClientData)
                        {
                            #region OperatorData
                            OperatorClientData oper = e.Objects[0] as OperatorClientData;
                            if (SelectedUserAccount != null && SelectedUserAccount.Code == oper.Code && ButtonOkPressed == false)
                            {
                                if (e.Action == CommittedDataAction.Update)
                                {
                                    MessageForm.Show(ResourceLoader.GetString2("UpdateFormUserAccountData"), MessageFormType.Warning);
                                    SelectedUserAccount = oper;
                                }
                                else if (e.Action == CommittedDataAction.Delete)
                                {
                                    MessageForm.Show(ResourceLoader.GetString2("DeleteFormUserAccountData"), MessageFormType.Warning);
                                    FormUtil.InvokeRequired(this,
                                    delegate
                                    {
                                        this.Close();
                                    });
                                }
                            }
                            #endregion
                        }
                        else if (e.Objects[0] is DepartmentTypeClientData)
                        {
                            #region DepartmentTypeClientData
                            DepartmentTypeClientData dep = (DepartmentTypeClientData)e.Objects[0];

                            if (e.Action == CommittedDataAction.Save)
                            {
                                FormUtil.InvokeRequired(checkedListBoxDepartamentos,
                                delegate
                                {
                                    checkedListBoxDepartamentos.Items.Add(dep);
                                    checkedListBoxDepartamentos.Refresh();
                                });
                            }
                            else if (e.Action == CommittedDataAction.Delete)
                            {
                                FormUtil.InvokeRequired(checkedListBoxDepartamentos,
                                delegate
                                {
                                    for (int i=0; i<checkedListBoxDepartamentos.Items.Count; i++)
                                    {
                                        DepartmentTypeClientData dep2 = (DepartmentTypeClientData)checkedListBoxDepartamentos.Items[i];
                                        if (dep2.Code == dep.Code)
                                        {
                                            checkedListBoxDepartamentos.Items.RemoveAt(i);
                                            checkedListBoxDepartamentos.Refresh();
                                            break;
                                        }
                                    }
                                });
                            }
                            else if (e.Action == CommittedDataAction.Update)
                            {
                                FormUtil.InvokeRequired(checkedListBoxDepartamentos,
                                delegate
                                {
                                    int i = 0;
                                    bool check = false;
                                    foreach (DepartmentTypeClientData dep2 in checkedListBoxDepartamentos.Items)
                                    {
                                        if (dep2.Code == dep.Code)
                                        {
                                            check = (checkedListBoxDepartamentos.CheckedItems.Contains(dep2) == true);
                                            checkedListBoxDepartamentos.Items[i] = dep;
                                            checkedListBoxDepartamentos.SetItemChecked(i, check);
                                            break;
                                        }
                                        i++;
                                    }
                                });
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private void removePictureButton_MouseHover(object sender, EventArgs e)
        {
            this.toolTip1.SetToolTip(this.removePictureButton, ResourceLoader.GetString2("RemoveUserPictureToolTip"));
        }

        private void imageImportButton_MouseHover(object sender, EventArgs e)
        {
            this.toolTip1.SetToolTip(this.imageImportButton, ResourceLoader.GetString2("ImportUserPictureToolTip"));
        }

        private void UserAccountForm_Load(object sender, EventArgs e)
        {
            this.Icon = ResourceLoader.GetIcon("$Icon.Usuarios");

            if (behaviorType == FormBehavior.Create)
            {
                Text = ResourceLoader.GetString2("UserAccountFormCreateText");
                buttonOk.Text = ResourceLoader.GetString("UserAccountFormCreateButtonOkText");
            }
            else if (behaviorType == FormBehavior.Edit)
            {
                Text = ResourceLoader.GetString2("UserAccountFormEditText");
                buttonOk.Text = ResourceLoader.GetString("UserAccountFormEditButtonOkText");
            }
            textBoxName.Focus();
            textBoxName.Select();

            if (textLabelDepartment != string.Empty && textLabelAgentId != string.Empty)
            {
                layoutControlDepartaments.Text = textLabelDepartment;
                layoutControlAgentID.Text = textLabelAgentId;
                layoutControlAgentPassword.Text = textLabelAgentPassword;
            }
        }

        private void UserAccountForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.parentForm != null)
                this.parentForm.AdministrationCommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(UserAccountForm_CommittedChanges);
        }

    }

    

    
}
