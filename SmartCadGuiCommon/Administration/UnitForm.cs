using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Smartmatic.SmartCad.Service;
using SmartCadCore.Core;
using System.Reflection;
using System.Collections;
using System.ServiceModel;
using System.Globalization;
using SmartCadCore.ClientData;
using System.Linq;
using SmartCadCore.Common;
using SmartCadCore.Enums;
using SmartCadControls.Controls;
using SmartCadControls;

namespace SmartCadGuiCommon
{
    public partial class UnitForm : DevExpress.XtraEditors.XtraForm
    {
        UnitClientData selectedUnit = null;
        FormBehavior behaviorType = FormBehavior.Edit;
        private AdministrationRibbonForm parentForm;
        bool wasChanged = false;
        object syncCommitted = new object();

        public enum Device_Type 
        {
            GPS = 0,
            DVR = 1
        }

        public UnitForm()
        {
            InitializeComponent();
            LoadLanguage();
            FillDepartmentTypes();
            FillDevices();  
        }

        public UnitForm(AdministrationRibbonForm parentForm, UnitClientData unit, FormBehavior type)
            : this()
        {
            BehaviorType = type;
            SelectedUnit = unit;
            this.parentForm = parentForm;
            this.parentForm.AdministrationCommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(UnitForm_AdministrationCommittedChanges);
            
        }

        private void LoadLanguage()
        {
            this.Icon = ResourceLoader.GetIcon("$Icon.Unidades");

            this.layoutControlGroupDepartment.Text = ResourceLoader.GetString2("DepartmentTypeInformation");
            this.layoutControlGroupIncidentType.Text = ResourceLoader.GetString2("IncidentTypeToAttend");
            this.layoutControlGroupUnitFeatures.Text = ResourceLoader.GetString2("UnitFeatures");
            this.layoutControlGroupUnitInf.Text = ResourceLoader.GetString2("UnitInformation");
            this.layoutControlItemAssigned.Text = ResourceLoader.GetString2("Associated") + ":";
            this.layoutControlItemNotAssigned.Text = ResourceLoader.GetString2("NotAssociated") + ":";
            this.layoutControlItemBrand.Text = ResourceLoader.GetString2("Brand") + ":";
            this.layoutControlItemDepartment.Text = ResourceLoader.GetString2("Department") + ":*";
            this.layoutControlItemDes.Text = ResourceLoader.GetString2("Description") + ":";
            this.layoutControlItemIdRadio.Text = ResourceLoader.GetString2("PortableID") + ":";
            this.layoutControlItemIdGPS.Text = ResourceLoader.GetString2("GpsID") + ":";
            this.layoutControlItemModel.Text = ResourceLoader.GetString2("UnitModel") + ":";
            this.layoutControlItemPlate.Text = ResourceLoader.GetString2("UnitCustomCode") + ":*";
            this.layoutControlItemSeats.Text = ResourceLoader.GetString2("SeatsNumber") + ":*";
            this.layoutControlItemStation.Text = ResourceLoader.GetString2("Station") + ":*";
            this.layoutControlItemUnitType.Text = ResourceLoader.GetString2("UnitType") + ":*";
            this.layoutControlItemVelocity.Text = ResourceLoader.GetString2("Velocity") + ":*";
            this.layoutControlItemYear.Text = ResourceLoader.GetString2("YearUpper") + ":";
            this.layoutControlItemZone.Text = ResourceLoader.GetString2("Zone") + ":*";
            this.layoutControlItemDeviceType.Text = ResourceLoader.GetString2("DeviceType") + ":";
            this.layoutControlItemIdUnit.Text = ResourceLoader.GetString2("IdUnit") + ":";
            this.layoutControlItemSerial.Text = ResourceLoader.GetString2("Serial") + ":";
            this.layoutControlItemIdGPS.Text = ResourceLoader.GetString2("DeviceID") + ":";
            this.buttonCancel.Text = ResourceLoader.GetString2("$Message.Cancel");
        }

        private void FillDevices() 
        {
            comboBoxEditDeviceType.Properties.Items.Add(Device_Type.GPS.ToString());
            comboBoxEditDeviceType.Properties.Items.Add(Device_Type.DVR.ToString());            
        }

        void UnitForm_AdministrationCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    lock (syncCommitted)
                    {
                        try
                        {
                            if (ServerServiceClient.GetInstance().OperatorClient == null)
                            { }
                        }
                        catch
                        { }

                        if (e.Objects[0] is DepartmentZoneClientData)
                        {
                            #region DepartmentZoneClientData
                            DepartmentZoneClientData departmentZone = e.Objects[0] as DepartmentZoneClientData;
                            
                            DepartmentZoneClientData departmentZoneData = new DepartmentZoneClientData();
                            departmentZoneData.Code = departmentZone.Code;
                            departmentZoneData = (DepartmentZoneClientData)ServerServiceClient.GetInstance().RefreshClient(departmentZoneData);
                            
                            if (e.Action == CommittedDataAction.Save)
                            {
                                FormUtil.InvokeRequired(comboBoxZoneType,
                                delegate
                                {
                                    comboBoxZoneType.Properties.BeginUpdate();
                                    comboBoxZoneType.Properties.Items.Add(departmentZoneData);
									comboBoxZoneType.Properties.EndUpdate();
                                });
                            }
                            else if (e.Action == CommittedDataAction.Update)
                            {
                                FormUtil.InvokeRequired(comboBoxZoneType,
                                delegate
                                {
									int index = comboBoxZoneType.Properties.Items.IndexOf(departmentZoneData);
                                    if (index != -1)
                                    {
                                        comboBoxZoneType.SelectedIndexChanged -= new EventHandler(comboBoxZoneType_SelectedIndexChanged);
										comboBoxZoneType.Properties.Items[index] = departmentZoneData;
                                        comboBoxZoneType.SelectedIndexChanged += new EventHandler(comboBoxZoneType_SelectedIndexChanged);
                                    }
                                });
                            }
                            #endregion
                        }
                        else if (e.Objects[0] is DepartmentTypeClientData)
                        {
                            #region DepartmentTypeClientData
                            DepartmentTypeClientData departmentType = e.Objects[0] as DepartmentTypeClientData;

                            DepartmentTypeClientData departmentTypeData = new DepartmentTypeClientData();
                            departmentTypeData.Code = departmentType.Code;
                            departmentTypeData = (DepartmentTypeClientData)ServerServiceClient.GetInstance().RefreshClient(departmentTypeData);

                            if (e.Action == CommittedDataAction.Save)
                            {
                                FormUtil.InvokeRequired(this.comboBoxDepartmentType,
                                delegate
                                {
									comboBoxDepartmentType.Properties.BeginUpdate();
									comboBoxDepartmentType.Properties.Items.Add(departmentTypeData);
									comboBoxDepartmentType.Properties.EndUpdate();
                                });
                            }
                            else if (e.Action == CommittedDataAction.Update)
                            {
                                FormUtil.InvokeRequired(this.comboBoxDepartmentType,
                                delegate
                                {
									int index = comboBoxDepartmentType.Properties.Items.IndexOf(departmentTypeData);
                                    if (index != -1)
                                    {
                                        comboBoxDepartmentType.SelectedIndexChanged -= new EventHandler(comboBoxDepartmentType_SelectedIndexChanged);
										comboBoxDepartmentType.Properties.Items[index] = departmentTypeData;
                                        comboBoxDepartmentType.SelectedIndexChanged += new EventHandler(comboBoxDepartmentType_SelectedIndexChanged);
                                    }
                                });
                            }
                            #endregion
                        }
                        else if (e.Objects[0] is DepartmentStationClientData)
                        {
                            #region DepartmentStationClientData
                            DepartmentStationClientData departmentStation = e.Objects[0] as DepartmentStationClientData;

                            DepartmentStationClientData departmentStationData = new DepartmentStationClientData();
                            departmentStationData.Code = departmentStation.Code;
                            departmentStationData = (DepartmentStationClientData)ServerServiceClient.GetInstance().RefreshClient(departmentStationData);

                            if (e.Action == CommittedDataAction.Save)
                            {
                                FormUtil.InvokeRequired(this.comboBoxStationType,
                                delegate
                                {
									comboBoxStationType.Properties.BeginUpdate();
									comboBoxStationType.Properties.Items.Add(departmentStationData);
									comboBoxStationType.Properties.EndUpdate();
                                });
                            }
                            else if (e.Action == CommittedDataAction.Update)
                            {
                                FormUtil.InvokeRequired(this.comboBoxStationType,
                                delegate
                                {
									int index = comboBoxStationType.Properties.Items.IndexOf(departmentStationData);
                                    if (index != -1)
                                    {
										comboBoxStationType.Properties.Items[index] = departmentStationData;
                                    }
                                });
                            }
                            #endregion
                        }
                        else if (e.Objects[0] is UnitClientData)
                        {
                            #region UnitClientData
                            UnitClientData unit =
                                e.Objects[0] as UnitClientData;

                            if (SelectedUnit != null && SelectedUnit.Equals(unit) && ButtonOkPressed == false)
                            {
                                if (e.Action == CommittedDataAction.Update)
                                {
                                    MessageForm.Show(ResourceLoader.GetString2("UpdateFormUnitData"), MessageFormType.Warning);
                                    SelectedUnit = unit;
                                }
                                else if (e.Action == CommittedDataAction.Delete)
                                {
                                    MessageForm.Show(ResourceLoader.GetString2("DeleteFormUnitData"), MessageFormType.Warning);
                                    FormUtil.InvokeRequired(this,
                                    delegate
                                    {
                                        this.Close();
                                    });
                                }
                            }
                            #endregion
                        }
                        else if (e.Objects[0] is IncidentTypeClientData)
                        {
                            #region DepartmentTypeClientData
                            IncidentTypeClientData incidentType = e.Objects[0] as IncidentTypeClientData;
                            if (e.Action == CommittedDataAction.Save)
                            {
                                FormUtil.InvokeRequired(this.listBoxNotAssigned,
                                delegate
                                {
                                    listBoxNotAssigned.BeginUpdate();
                                    listBoxNotAssigned.Items.Add(incidentType);
                                    listBoxNotAssigned.EndUpdate();
                                });
                            }
                            else if (e.Action == CommittedDataAction.Update)
                            {
                                FormUtil.InvokeRequired(this,
                                delegate
                                {
                                    int index = listBoxNotAssigned.Items.IndexOf(incidentType);
                                    if (index != -1)
                                    {
                                        listBoxNotAssigned.Items[index] = incidentType;
                                    }
                                    index = listBoxAssigned.Items.IndexOf(incidentType);
                                    if (index != -1)
                                    {
                                        listBoxAssigned.Items[index] = incidentType;
                                    }
                                });
                            }
                            else if (e.Action == CommittedDataAction.Delete)
                            {
                                FormUtil.InvokeRequired(this,
                                delegate
                                {
                                    int index = listBoxNotAssigned.Items.IndexOf(incidentType);
                                    if (index != -1)
                                    {
                                        listBoxNotAssigned.Items.RemoveAt(index);
                                    }
                                    index = listBoxAssigned.Items.IndexOf(incidentType);
                                    if (index != -1)
                                    {
                                        listBoxAssigned.Items.RemoveAt(index);
                                    }
                                });
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        public FormBehavior BehaviorType
        {
            get
            {
                return behaviorType;
            }
            set
            {
               
                behaviorType = value;
                Clear();
            }
        }

        public UnitClientData SelectedUnit
        {
            get
            {
                return selectedUnit;
            }
            set
            {
                selectedUnit = value;
                setUnitData(selectedUnit);

            }
        }

        private void setUnitData(UnitClientData unit)
        {
            if (BehaviorType == FormBehavior.Edit) 
            {
                Clear();
                ClearDataUnit();               
            }
            if (unit != null)
            {
                int index = -1;

                if (unit.DepartmentStation != null && unit.DepartmentStation.DepartmentZone != null && unit.DepartmentType != null)
                {
                    comboBoxDepartmentType.SelectedItem = unit.DepartmentType;
                    FillCmbBoxZone(unit.DepartmentType);
                    FillCmbBoxStation(unit.DepartmentStation.DepartmentZone);
                    comboBoxZoneType.SelectedItem = unit.DepartmentStation.DepartmentZone;
                    comboBoxStationType.SelectedItem = unit.DepartmentStation;
                    FillUnitTypesFromDepartment(unit.DepartmentType);

                    index = comboBoxUnitType.Properties.Items.IndexOf(unit.Type);
                    comboBoxUnitType.SelectedIndex = index;
                 
                    
                }

                //if (textBoxLicensePlate.IsValid(unit.CustomCode))
                {
                    textBoxLicensePlate.Text = unit.CustomCode;
                }
                //else
                //{
                //    string ErrorInLoadXLSUnitCustomCode = ResourceLoader.GetString2("ErrorInLoadXLSUnitCustomCode");
                //    MessageForm.Show(ErrorInLoadXLSUnitCustomCode, MessageFormType.Warning);
                //    textBoxLicensePlate.Text = "";
                //}


                textBoxIdUnit.Text = unit.IdUnit;
                textBoxSerial.Text = unit.Serial;
                textBoxBrand.Text = unit.Brand;
                textBoxModel.Text = unit.Model;
           
                if (unit.Year.HasValue == true)
                    textBoxYear.Text = unit.Year.ToString();
                
                if (unit.GPSCode != 0) 
                {
                    comboBoxEditDeviceType.SelectedIndex = (int)unit.DeviceType;

                    index = -1;
                    GPSClientData gps = new GPSClientData();
                    gps.Code = unit.GPSCode;
                    index = comboBoxEditIdGPS.Properties.Items.IndexOf(gps);
                    if (index > -1)
                        comboBoxEditIdGPS.SelectedIndex = index;
                }

                textBoxIDRadio.Text = unit.IdRadio;
                textBoxSeats.Text = unit.Capacity.ToString();
                textBoxDescription.Text = string.IsNullOrEmpty(unit.Description) ? string.Empty : unit.Description;
                textBoxVelocity.Text = unit.Velocity.ToString();
                
                setIncidentAssingFromUnit(unit);
               
                
                //Se le coloca el estado que trae de base de datos.
                if (unit.Status != null)
                {
                    selectedUnit.Status = unit.Status;
                }
                else
                {
                    if (behaviorType == FormBehavior.Create) selectedUnit.Status = UnitStatusClientData.Available;
                }
            }
            UnitParameters_Change(null, null);
        }

      
        private void Clear()
        {
            comboBoxDepartmentType.SelectedIndex = -1;
            comboBoxUnitType.SelectedIndex = -1;
            comboBoxZoneType.SelectedItem = -1;
            comboBoxStationType.SelectedItem = -1;
            textBoxLicensePlate.Clear();
            textBoxBrand.Clear();
            textBoxModel.Clear();
            textBoxYear.Clear();
            comboBoxEditIdGPS.SelectedIndex = -1;
            textBoxIDRadio.Clear();
            textBoxSeats.Clear();
            textBoxDescription.Clear();
            textBoxVelocity.Clear();
            listBoxAssigned.Items.Clear();
            listBoxNotAssigned.Items.Clear();
        }

        private void UnitParameters_Change(object sender, System.EventArgs e)
        {
            if ((comboBoxDepartmentType.SelectedIndex == -1) ||
                (comboBoxUnitType.SelectedIndex == -1) ||
                (comboBoxStationType.SelectedIndex == -1) ||
                (comboBoxZoneType.SelectedIndex == -1) ||
                (string.IsNullOrEmpty(textBoxVelocity.Text.Trim())) ||
                (string.IsNullOrEmpty(textBoxLicensePlate.Text.Trim())) ||
                (string.IsNullOrEmpty(textBoxSeats.Text.Trim())) ||
                !(  string.IsNullOrEmpty(textBoxYear.Text.Trim()) ||
                    ApplicationUtil.ParseConstraints(textBoxYear.Text,1,int.MaxValue))  
                )
                buttonOk.Enabled = false;
            else
                buttonOk.Enabled = true;

            wasChanged =  this.IsHandleCreated;
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {            
            if ((double.Parse(textBoxVelocity.Text.Trim()) == 0))
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ResourceLoader.GetString2("UnitVelocity"), MessageFormType.Error);
                textBoxVelocity.Focus();
                textBoxVelocity.SelectAll();
                return;
            }
            if ((double.Parse(textBoxSeats.Text.Trim()) == 0))
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ResourceLoader.GetString2("UnitSeatsNumber"), MessageFormType.Error);
                textBoxSeats.Focus();
                textBoxSeats.SelectAll();
                return;
            }
            if (wasChanged == false)
            {
                //return;
            }
            //si el chequeo sale negativo existe un problema con la asignacion.
            //por lo tanto no puede ser modificada la unidad.
            if (CheckChangesUnitOfficerAssing() == false)
                return;
           
            try
            {
                if (buttonOk.Enabled == true)
                {
                    BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[1] { GetType().GetMethod("buttonOk_Click1", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[0] });
                    processForm.CanThrowError = true;
                    processForm.ShowDialog();
                }
            }
            catch (FaultException ex)
            {
                if(BehaviorType == FormBehavior.Edit)
                    SelectedUnit = (UnitClientData)ServerServiceClient.GetInstance().RefreshClient(SelectedUnit,true);

                GetErrorFocus(ex);
				wasChanged = false;
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, MessageFormType.Information);
                buttonOkPressed = false;
            }
            catch (Exception ex)
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, ex);
                buttonOkPressed = false;
            }
        }

        private bool CheckChangesUnitOfficerAssing()
        {
            if (behaviorType == FormBehavior.Edit)
            {
                DepartmentStationClientData station = comboBoxStationType.SelectedItem as DepartmentStationClientData;
                if (selectedUnit.DepartmentStation.Code != station.Code)
                {// si cambio el organismo,zona o estacion

                    if (UnitHasCurrentAssings(selectedUnit))
                    {
                        MessageForm.Show(ResourceLoader.GetString2("NoUpdateUnitOfficersAssigned"), MessageFormType.Information);
                        DialogResult = DialogResult.None;
                        return false;
                    }
                    if (!UnitHasFutureAssings(selectedUnit))
                        return true;

                    //Mensaje para indicar que se van a eliminar las asignacion actual y las futuras.
                    DialogResult res = MessageForm.Show(ResourceLoader.GetString2("UnitWithAssigns"), MessageFormType.Question);
                    if (res == DialogResult.Yes)
                    {//Borramos todo.
                        return true;
                    }
                    else
                    {//Como cancelo entonces no podra cambiar nada.
                        DialogResult = DialogResult.None;
                        return false;
                    }
                }
                return true;
            }
            else
                return true;
        }

        private bool UnitHasCurrentAssings(UnitClientData selectedUnit)
        {
            ServerServiceClient serverService = ServerServiceClient.GetInstance();
            string DateNHibernateFormat = "yyyyMMdd HH:mm";
            string date = serverService.GetTimeFromDB().ToString(DateNHibernateFormat);

			string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetCurrentUnitOfficerAssignsByUnitCodeCount, selectedUnit.Code, date);
            return (long)serverService.SearchBasicObject(hql) != 0;
        }

        private bool UnitHasFutureAssings(UnitClientData unit)
        {
            ServerServiceClient serverService = ServerServiceClient.GetInstance();
            string DateNHibernateFormat = "yyyyMMdd HH:mm";
            string date = serverService.GetTimeFromDB().ToString(DateNHibernateFormat);

            string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.CountFutureUnitAssigns, unit.Code, date);
            return (long)serverService.SearchBasicObject(hql) != 0;            
        }

        private void GetErrorFocus(FaultException ex)
        {
            if (ex.Message.ToLower().Contains(ResourceLoader.GetString2("UnitCustomCode").ToLower()))
            {
                if (behaviorType == FormBehavior.Create) 
                {
                    textBoxLicensePlate.Text = textBoxLicensePlate.Text;
                }
                this.textBoxLicensePlate.Focus();
            }
            if (ex.Message.ToLower().Contains("GPS".ToLower()))
            {
                comboBoxEditIdGPS.SelectedText = comboBoxEditIdGPS.SelectedText;

                this.comboBoxEditIdGPS.Focus();
            }
            if (ex.Message.ToLower().Contains(ResourceLoader.GetString2("Radio").ToLower()))
            {
                if (behaviorType == FormBehavior.Create) 
                {
                    textBoxIDRadio.Text = textBoxIDRadio.Text;
                }
                this.textBoxIDRadio.Focus();
            }
            if (ex.Message.ToLower().Contains(ResourceLoader.GetString2("Year").ToLower()))
            {
                this.textBoxYear.Focus();
            }    
        }

        private bool buttonOkPressed = false;

        public bool ButtonOkPressed
        {
            get { return buttonOkPressed; }
        }

        private void buttonOk_Click1()
        {
            buttonOkPressed = true;
            if (behaviorType == FormBehavior.Create)
            {
                selectedUnit = new UnitClientData();
                selectedUnit.CoordinatesDate = DateTime.Now;
            }
            if (selectedUnit != null)
            {
                FormUtil.InvokeRequired(this, delegate
                {
                    selectedUnit.DepartmentType = comboBoxDepartmentType.SelectedItem as DepartmentTypeClientData;
                    selectedUnit.Type = comboBoxUnitType.SelectedItem as UnitTypeClientData;
                    selectedUnit.DepartmentStation = comboBoxStationType.SelectedItem as DepartmentStationClientData;
                    selectedUnit.IncidentTypes = new ArrayList(listBoxAssigned.Items);
                    selectedUnit.Serial = textBoxSerial.Text.Trim();
                    selectedUnit.IdUnit = textBoxIdUnit.Text.Trim();
                    selectedUnit.CustomCode = textBoxLicensePlate.Text;
                    selectedUnit.Brand = textBoxBrand.Text;

                    if (textBoxModel.Text != String.Empty)
                        selectedUnit.Model = textBoxModel.Text;
                    else
                        selectedUnit.Model = string.Empty;
                    
                    if (textBoxYear.Text != String.Empty)
                        selectedUnit.Year = int.Parse(textBoxYear.Text);
                    else
                        selectedUnit.Year = null;
                    
                    if (comboBoxEditIdGPS.SelectedIndex > 0){
                        selectedUnit.GPSCode = (comboBoxEditIdGPS.SelectedItem as GPSClientData).Code;
                        selectedUnit.IdGPS = (comboBoxEditIdGPS.SelectedItem as GPSClientData).Name;
                    }
                    
                    selectedUnit.IdRadio = textBoxIDRadio.Text;
                    if (textBoxVelocity.Text != String.Empty) selectedUnit.Velocity = double.Parse(textBoxVelocity.Text);
                    selectedUnit.Capacity = int.Parse(textBoxSeats.Text);
                    if (textBoxDescription.Text != String.Empty)
                        selectedUnit.Description = textBoxDescription.Text;
                    else
                        selectedUnit.Description = "";

                    if (behaviorType == FormBehavior.Create)
                        selectedUnit.Status = UnitStatusClientData.Available;

                    selectedUnit.WorkShiftsRoutes = new ArrayList(); 
                });
                ServerServiceClient.GetInstance().SaveOrUpdateClientData(selectedUnit);
            }
        }

        private void FillUnitTypesFromDepartment(DepartmentTypeClientData department)
        {
            //relleno la lista de Tipos de unidades segun el unitData seleccionado
            comboBoxUnitType.Text = "";
			comboBoxUnitType.Properties.Items.Clear();
            int maxlen = 0;
            foreach (UnitTypeClientData type in ServerServiceClient.GetInstance().SearchClientObjects(
                    SmartCadHqls.GetCustomHql(SmartCadHqls.GetUnitTypeByDepartmentTypeCode, department.Code), true))
            {
				comboBoxUnitType.Properties.Items.Add(type);
                maxlen = Math.Max(maxlen, type.Name.Length);
            }
        }


        private void FillDepartmentTypes()
        {
			comboBoxDepartmentType.Properties.Items.Clear();
            int maxlen = 0;
            foreach (DepartmentTypeClientData type in ServerServiceClient.GetInstance().SearchClientObjects(typeof(DepartmentTypeClientData)))
            {
				comboBoxDepartmentType.Properties.Items.Add(type);
                maxlen = Math.Max(maxlen, type.Name.Length);
            }
            comboBoxDepartmentType.SelectedIndex = -1;
        }

        private void FillAssignedIncidentTypes(UnitClientData unit)
        {
            listBoxAssigned.Items.Clear();
            //ServerServiceClient.GetInstance().InitializeLazy(unit, unit.SetIncidentTypes);
            foreach (IncidentTypeClientData type in unit.IncidentTypes)
            {
                listBoxAssigned.Items.Add(type);
                listBoxNotAssigned.Items.Remove(type);
            }
        }
        /// <summary>
        /// Metodo que al hacer click sobre el boton de agregar un incidente
        /// lo agrega a la lista de incidentes, la particularidad de unidad
        /// es que a una unidad se pueden agregar cualquier incidente.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddIncidentType_Click(object sender, EventArgs e)
        {
            ArrayList toDelete = new ArrayList();
            foreach (IncidentTypeClientData incident in listBoxNotAssigned.SelectedItems)
            {
                toDelete.Add(incident);
                listBoxAssigned.Items.Add(incident);
            }
            foreach (IncidentTypeClientData incident in toDelete)
            {
                int i = 0;
                int index = -1;
                foreach (IncidentTypeClientData inc in listBoxNotAssigned.Items) 
                {
                    if (inc.Code == incident.Code) 
                    {
                        index = i;
                        break;
                    }
                    i++;
                }
                if(index >= 0)
                    listBoxNotAssigned.Items.RemoveAt(index);
            }
            
            UnitParameters_Change(null, null);
        }

        private void buttonAddAllIncidentType_Click(object sender, EventArgs e)
        {
            
            foreach (IncidentTypeClientData incident in listBoxNotAssigned.Items)
            {
                listBoxAssigned.Items.Add(incident);
            }
            listBoxNotAssigned.Items.Clear();
            UnitParameters_Change(null, null);
        }

        private void buttonRemoveIncidentType_Click(object sender, EventArgs e)
        {
            ArrayList toDelete = new ArrayList();
            foreach (IncidentTypeClientData incident in listBoxAssigned.SelectedItems)
            {
                toDelete.Add(incident);
                listBoxNotAssigned.Items.Add(incident);
            }
            foreach (IncidentTypeClientData incident in toDelete)
            {
                int i = 0;
                int index = -1;
                foreach (IncidentTypeClientData inc in listBoxAssigned.Items) 
                {
                    if (inc.Code == incident.Code) 
                    {
                        index = i;
                        break;
                    }
                    i++;
                }
                if(index >=0)
                    listBoxAssigned.Items.RemoveAt(index);
            }
            UnitParameters_Change(null, null);
        }

        private void buttonRemoveAllIncidentType_Click(object sender, EventArgs e)
        {
            foreach (IncidentTypeClientData incident in listBoxAssigned.Items)
            {
                listBoxNotAssigned.Items.Add(incident);
            }
            listBoxAssigned.Items.Clear();
            UnitParameters_Change(null, null);
        }

        private void comboBoxDepartmentType_SelectionChangeCommitted(object sender, EventArgs e)
        {
            comboBoxUnitType.Text = "";
			comboBoxUnitType.Properties.Items.Clear();
            textBoxSeats.Text = string.Empty;
            textBoxVelocity.Text = string.Empty;
            DepartmentTypeClientData departmentType = (DepartmentTypeClientData)comboBoxDepartmentType.SelectedItem;
            if (departmentType != null && comboBoxDepartmentType.Focused == false)
            {
               FillUnitTypesFromDepartment(departmentType);
            }
            cleanIncidentType();
            CleanComboBoxZoneType();
            CleanComboBoxStationType();

            UnitParameters_Change(null, null);


        }

        private void cleanIncidentType()
        {
            listBoxAssigned.Items.Clear();
            listBoxNotAssigned.Items.Clear();
            listBoxNotAssigned.Enabled = false;
            listBoxAssigned.Enabled = false;
            buttonAddAllIncidentType.Enabled = false;
            buttonAddIncidentType.Enabled = false;
            buttonRemoveAllIncidentType.Enabled = false;
            buttonRemoveIncidentType.Enabled = false;
        }

        private void comboBoxUnitType_SelectionChangeCommitted(object sender, EventArgs e)
        {

            //Dependiendo del departamento asignado obtengo el valor de la lista de 
            UnitTypeClientData unitType = comboBoxUnitType.SelectedItem as UnitTypeClientData;

            DepartmentTypeClientData departmentType = comboBoxDepartmentType.SelectedItem as DepartmentTypeClientData;

            if (departmentType != null && departmentType.Dispatch == true)
            {
                layoutControlGroupIncidentType.Enabled = true;

                if (comboBoxUnitType.Focused == true)
                    chargeIncidentTypesFromUnitType(unitType);
            }
            else
                layoutControlGroupIncidentType.Enabled = false;

        }

        private void chargeIncidentTypesFromUnitType(UnitTypeClientData unit)
        {
            cleanIncidentType();
            if (unit != null)
            {

                ChargeIncidentTypes();

                //Obtengo la lista de los incidentes asignados a el tipo de unidad
                if (unit.IncidentTypes != null)
                {
                    foreach (IncidentTypeClientData type in unit.IncidentTypes)
                    {
                        listBoxNotAssigned.Items.Remove(type);
                        listBoxAssigned.Items.Add(type);
                    }
                }
                if ((listBoxNotAssigned.Items.Count > 0) || (listBoxAssigned.Items.Count > 0))
                {
                    listBoxNotAssigned.Enabled = true;
                    listBoxAssigned.Enabled = true;
                    buttonAddAllIncidentType.Enabled = true;
                    buttonAddIncidentType.Enabled = true;
                    buttonRemoveAllIncidentType.Enabled = true;
                    buttonRemoveIncidentType.Enabled = true;
                }
            }
            UnitParameters_Change(null, null);
        }

        private void ChargeIncidentTypes()
        {
            //string hql = "FROM IncidentTypeClientData";
            IList list = ServerServiceClient.GetInstance().SearchClientObjects(typeof(IncidentTypeClientData));
            foreach (IncidentTypeClientData var in list)
            {
                listBoxNotAssigned.Items.Add(var);
            }
        }

        private void setIncidentAssingFromUnit(UnitClientData unit)
        {
            //Cargo los incidentes 
            ChargeIncidentTypes();

            if (unit != null)
            {
                if (unit.IncidentTypes != null)
                {
                    foreach (IncidentTypeClientData var in unit.IncidentTypes)
                    {
                        if (listBoxNotAssigned.Items.Contains(var))
                        {
                            listBoxNotAssigned.Items.Remove(var);
                        }
                        listBoxAssigned.Items.Add(var);
                    }
                }

                if ((listBoxNotAssigned.Items.Count > 0) || (listBoxAssigned.Items.Count > 0))
                {
                    listBoxNotAssigned.Enabled = true;
                    listBoxAssigned.Enabled = true;
                    buttonAddAllIncidentType.Enabled = true;
                    buttonAddIncidentType.Enabled = true;
                    buttonRemoveAllIncidentType.Enabled = true;
                    buttonRemoveIncidentType.Enabled = true;
                } 
            }
            UnitParameters_Change(null, null);
        }

        private void unitType_selectedIndexChanged(object sender, EventArgs e)
        {
            UnitTypeClientData unitType = comboBoxUnitType.SelectedItem as UnitTypeClientData;
            if (unitType != null)
            {
                int capacity = unitType.Capacity;
                double velocity = unitType.Velocity;
                textBoxSeats.Text = capacity.ToString();
                textBoxVelocity.Text = velocity.ToString();
            }

            UnitParameters_Change(sender, e);
        }

        private void comboBoxDepartmentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //si selecciono un departamento
            if (comboBoxDepartmentType.SelectedIndex != -1)
            {
                //Buscamos el elemento seleccionado para llenar la lista de zones
                DepartmentTypeClientData departmentType = comboBoxDepartmentType.SelectedItem as DepartmentTypeClientData;
                if (comboBoxDepartmentType.Focused == false)
                {
                    FillCmbBoxZone(departmentType);
                }
            }
            UnitParameters_Change(null, null);
            
        }

        

        private void FillCmbBoxZone(DepartmentTypeClientData departmentType)
        {
            CleanComboBoxZoneType();
            departmentType.DepartmentZones = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetDepartmentZonesByDepartmentType, departmentType.Code));

            int maxlen = 0;
            foreach (DepartmentZoneClientData var in departmentType.DepartmentZones)
            {
				comboBoxZoneType.Properties.Items.Add(var);
                maxlen = Math.Max(maxlen, var.Name.Length);
            }
        }

        private void comboBoxZoneType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //si selecciono un departamento
            if (comboBoxZoneType.SelectedIndex != -1)
            {
                //Buscamos la zona seleccionada para luego cargar la lista 
                DepartmentZoneClientData departamentZone = comboBoxZoneType.SelectedItem as DepartmentZoneClientData;
                //if (comboBoxZoneType.Focused == false)
                //{
                    FillCmbBoxStation(departamentZone);
                //}
            }
            UnitParameters_Change(null, null);
        }

        private void FillCmbBoxStation(DepartmentZoneClientData departamentZone)
        {
            CleanComboBoxStationType();
            departamentZone.DepartmentStations = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetDepartmentStationsByDepartmentZoneCode, departamentZone.Code));

            int maxlen = 0;
            foreach (DepartmentStationClientData var in departamentZone.DepartmentStations)
            {
				comboBoxStationType.Properties.Items.Add(var);
                maxlen = Math.Max(maxlen, var.Name.Length);
            }
        }

        private void CleanComboBoxZoneType()
        {
            if (comboBoxZoneType.SelectedIndex != -1)
            {
                comboBoxZoneType.Text = "";
            }
			comboBoxZoneType.Properties.Items.Clear();
        }    
    
        private void CleanComboBoxStationType()
        {
            if (comboBoxStationType.SelectedIndex != -1)
            {
                comboBoxStationType.Text = "";
            }
			comboBoxStationType.Properties.Items.Clear();
                
        }

        private void ClearDataUnit()
        {
            textBoxLicensePlate.Clear();
            textBoxBrand.Clear();
            textBoxModel.Clear();
            textBoxYear.Clear();
            comboBoxEditIdGPS.SelectedIndex = -1;
            textBoxIDRadio.Clear();
            textBoxSeats.Clear();
            textBoxDescription.Clear();
            textBoxVelocity.Clear();
        }

        private void listBoxNotAssigned_DoubleClick(object sender, EventArgs e)
        {
            buttonAddIncidentType_Click(sender, e);
        }

        private void listBoxAssigned_DoubleClick(object sender, EventArgs e)
        {
            buttonRemoveIncidentType_Click(sender, e);
        }

        private void comboBoxZoneType_Enter(object sender, EventArgs e)
        {
            if (comboBoxZoneType.SelectedIndex == -1 && comboBoxDepartmentType.SelectedIndex != -1)
            {
                FillCmbBoxZone((DepartmentTypeClientData)comboBoxDepartmentType.SelectedItem);
            }
        }

        private void comboBoxStationType_Enter(object sender, EventArgs e)
        {
            if (comboBoxStationType.SelectedIndex == -1 && comboBoxZoneType.SelectedIndex != -1)
            {
                FillCmbBoxStation((DepartmentZoneClientData)comboBoxZoneType.SelectedItem);
            }
        }

        private void comboBoxUnitType_Enter(object sender, EventArgs e)
        {
            if (comboBoxDepartmentType.SelectedIndex != -1 && comboBoxUnitType.SelectedIndex == -1)
            {
                FillUnitTypesFromDepartment((DepartmentTypeClientData)comboBoxDepartmentType.SelectedItem);
            }
        }

        private void listBoxNotAssigned_Enter(object sender, EventArgs e)
        {
            listBoxAssigned.ClearSelected();
        }

        private void listBoxAssigned_Enter(object sender, EventArgs e)
        {
            listBoxNotAssigned.ClearSelected();
        }

        private void UnitForm_Load(object sender, EventArgs e)
        {
            switch (behaviorType)
            {
                case FormBehavior.Create:
                    Text = ResourceLoader.GetString2("UnitCreateFormText");
                    buttonOk.Text = ResourceLoader.GetString("UnitFormCreateButtonOkText");
                    break;
                case FormBehavior.Edit:
                    Text = ResourceLoader.GetString2("UnitEditFormText");
                    buttonOk.Text = ResourceLoader.GetString("UnitFormEditButtonOkText");
                    break;
                default:
                    break;
            }
        }

        private void UnitForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (parentForm != null)
            {
                this.parentForm.AdministrationCommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(UnitForm_AdministrationCommittedChanges);
            }
        }

        private void comboBoxEditDeviceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBoxEditIdGPS.Properties.Items.Clear();
            comboBoxEditIdGPS.SelectedIndex = -1;
            if (comboBoxEditDeviceType.SelectedIndex > -1) 
            {
                comboBoxEditIdGPS.Properties.Items.Add(new GPSClientData());                
                if (comboBoxEditDeviceType.SelectedIndex == 0) 
                {
                    IList devices = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetGPSWithoutUnit,selectedUnit!=null?selectedUnit.Code:0));
                    comboBoxEditIdGPS.Properties.Items.AddRange(devices);
                }
            }
        }

        private void comboBoxEditIdGPS_SelectedIndexChanged(object sender, EventArgs e)
		{
			UnitParameters_Change(null, null);
		}
    }
}
