﻿using SmartCadControls.Controls;
namespace SmartCadGuiCommon
{
    partial class DesignerQuestionDevX
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DesignerQuestionDevX));
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem("TextBox", 0);
            System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem("RadioButton", 1);
            System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem(new string[] {
            "NumericTextBox"}, 3, System.Drawing.Color.Empty, System.Drawing.Color.White, new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0))));
            System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem("Check Box", 2);
            this.layoutControlMain = new DevExpress.XtraLayout.LayoutControl();
            this.simpleButtonTrash = new DevExpress.XtraEditors.SimpleButton();
            this.imageListTrash = new System.Windows.Forms.ImageList(this.components);
            this.propertyGridControl = new DevExpress.XtraVerticalGrid.PropertyGridControl();
            this.listViewControls = new System.Windows.Forms.ListView();
            this.imageListControls = new System.Windows.Forms.ImageList(this.components);
            this.dragDropLayoutControlAnswers = new DragDropLayoutControl();
            this.layoutControlGroupMain = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupControls = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemTools = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupAnswers = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemAnswers = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupPropertyGrid = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemPropertyGridControl = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlMain)).BeginInit();
            this.layoutControlMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.propertyGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupControls)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTools)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupAnswers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAnswers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupPropertyGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPropertyGridControl)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControlMain
            // 
            this.layoutControlMain.AllowCustomizationMenu = false;
            this.layoutControlMain.Appearance.DisabledLayoutGroupCaption.ForeColor = System.Drawing.Color.White;
            this.layoutControlMain.Appearance.DisabledLayoutGroupCaption.Options.UseForeColor = true;
            this.layoutControlMain.Controls.Add(this.simpleButtonTrash);
            this.layoutControlMain.Controls.Add(this.propertyGridControl);
            this.layoutControlMain.Controls.Add(this.listViewControls);
            this.layoutControlMain.Controls.Add(this.dragDropLayoutControlAnswers);
            this.layoutControlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlMain.Location = new System.Drawing.Point(0, 0);
            this.layoutControlMain.Name = "layoutControlMain";
            this.layoutControlMain.Root = this.layoutControlGroupMain;
            this.layoutControlMain.Size = new System.Drawing.Size(646, 360);
            this.layoutControlMain.TabIndex = 1;
            this.layoutControlMain.Text = "layoutControl1";
            // 
            // simpleButtonTrash
            // 
            this.simpleButtonTrash.AllowDrop = true;
            this.simpleButtonTrash.AutoWidthInLayoutControl = true;
            this.simpleButtonTrash.ImageIndex = 0;
            this.simpleButtonTrash.ImageList = this.imageListTrash;
            this.simpleButtonTrash.Location = new System.Drawing.Point(597, 27);
            this.simpleButtonTrash.Name = "simpleButtonTrash";
            this.simpleButtonTrash.Size = new System.Drawing.Size(42, 38);
            this.simpleButtonTrash.StyleController = this.layoutControlMain;
            this.simpleButtonTrash.TabIndex = 5;
            this.simpleButtonTrash.Click += new System.EventHandler(this.simpleButtonTrash_Click);
            this.simpleButtonTrash.DragDrop += new System.Windows.Forms.DragEventHandler(this.simpleButtonTrash_DragDrop);
            this.simpleButtonTrash.DragEnter += new System.Windows.Forms.DragEventHandler(this.simpleButtonTrash_DragEnter);
            this.simpleButtonTrash.DragLeave += new System.EventHandler(this.simpleButtonTrash_DragLeave);
            // 
            // imageListTrash
            // 
            this.imageListTrash.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListTrash.ImageStream")));
            this.imageListTrash.TransparentColor = System.Drawing.Color.Magenta;
            this.imageListTrash.Images.SetKeyName(0, "");
            this.imageListTrash.Images.SetKeyName(1, "");
            // 
            // propertyGridControl
            // 
            this.propertyGridControl.Location = new System.Drawing.Point(12, 236);
            this.propertyGridControl.Name = "propertyGridControl";
            this.propertyGridControl.OptionsBehavior.ResizeHeaderPanel = false;
            this.propertyGridControl.OptionsBehavior.ResizeRowHeaders = false;
            this.propertyGridControl.OptionsView.ShowRootCategories = false;
            this.propertyGridControl.Size = new System.Drawing.Size(622, 112);
            this.propertyGridControl.TabIndex = 6;
            // 
            // listViewControls
            // 
            this.listViewControls.Alignment = System.Windows.Forms.ListViewAlignment.Default;
            this.listViewControls.BorderStyle = System.Windows.Forms.BorderStyle.None;
            listViewItem1.StateImageIndex = 0;
            listViewItem1.Tag = "TextBox";
            listViewItem2.StateImageIndex = 0;
            listViewItem2.Tag = "RadioButton";
            listViewItem3.StateImageIndex = 0;
            listViewItem3.Tag = "Numeric";
            listViewItem4.StateImageIndex = 0;
            listViewItem4.Tag = "CheckEdit";
            this.listViewControls.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1,
            listViewItem2,
            listViewItem3,
            listViewItem4});
            this.listViewControls.LargeImageList = this.imageListControls;
            this.listViewControls.Location = new System.Drawing.Point(123, 27);
            this.listViewControls.MultiSelect = false;
            this.listViewControls.Name = "listViewControls";
            this.listViewControls.Scrollable = false;
            this.listViewControls.Size = new System.Drawing.Size(470, 38);
            this.listViewControls.SmallImageList = this.imageListControls;
            this.listViewControls.TabIndex = 4;
            this.listViewControls.UseCompatibleStateImageBehavior = false;
            this.listViewControls.View = System.Windows.Forms.View.SmallIcon;
            this.listViewControls.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listView1_MouseDown);
            this.listViewControls.MouseMove += new System.Windows.Forms.MouseEventHandler(this.listView1_MouseMove);
            // 
            // imageListControls
            // 
            this.imageListControls.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListControls.ImageStream")));
            this.imageListControls.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListControls.Images.SetKeyName(0, "textBox.gif");
            this.imageListControls.Images.SetKeyName(1, "radioButton.gif");
            this.imageListControls.Images.SetKeyName(2, "checkBox.gif");
            this.imageListControls.Images.SetKeyName(3, "numericBox.gif");
            // 
            // dragDropLayoutControlAnswers
            // 
            this.dragDropLayoutControlAnswers.Location = new System.Drawing.Point(7, 99);
            this.dragDropLayoutControlAnswers.Name = "dragDropLayoutControlAnswers";
            this.dragDropLayoutControlAnswers.Size = new System.Drawing.Size(632, 108);
            this.dragDropLayoutControlAnswers.TabIndex = 0;
            // 
            // layoutControlGroupMain
            // 
            this.layoutControlGroupMain.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroupMain.GroupBordersVisible = false;
            this.layoutControlGroupMain.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupControls,
            this.layoutControlGroupAnswers});
            this.layoutControlGroupMain.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupMain.Name = "layoutControlGroupMain";
            this.layoutControlGroupMain.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupMain.Size = new System.Drawing.Size(646, 360);
            this.layoutControlGroupMain.Text = "layoutControlGroupMain";
            this.layoutControlGroupMain.TextVisible = false;
            // 
            // layoutControlGroupControls
            // 
            this.layoutControlGroupControls.CustomizationFormText = "layoutControlGroupControls";
            this.layoutControlGroupControls.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemTools,
            this.layoutControlItem1});
            this.layoutControlGroupControls.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupControls.Name = "layoutControlGroupControls";
            this.layoutControlGroupControls.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupControls.Size = new System.Drawing.Size(646, 72);
            this.layoutControlGroupControls.Text = "layoutControlGroupControls";
            // 
            // layoutControlItemTools
            // 
            this.layoutControlItemTools.Control = this.listViewControls;
            this.layoutControlItemTools.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItemTools.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemTools.MinSize = new System.Drawing.Size(148, 31);
            this.layoutControlItemTools.Name = "layoutControlItemTools";
            this.layoutControlItemTools.Size = new System.Drawing.Size(590, 42);
            this.layoutControlItemTools.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemTools.Text = "layoutControlItemTools";
            this.layoutControlItemTools.TextSize = new System.Drawing.Size(112, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.simpleButtonTrash;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(590, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(46, 42);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroupAnswers
            // 
            this.layoutControlGroupAnswers.CustomizationFormText = "layoutControlGroupAnswers";
            this.layoutControlGroupAnswers.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemAnswers,
            this.layoutControlGroupPropertyGrid});
            this.layoutControlGroupAnswers.Location = new System.Drawing.Point(0, 72);
            this.layoutControlGroupAnswers.Name = "layoutControlGroupAnswers";
            this.layoutControlGroupAnswers.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupAnswers.Size = new System.Drawing.Size(646, 288);
            this.layoutControlGroupAnswers.Text = "layoutControlGroupAnswers";
            // 
            // layoutControlItemAnswers
            // 
            this.layoutControlItemAnswers.Control = this.dragDropLayoutControlAnswers;
            this.layoutControlItemAnswers.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItemAnswers.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemAnswers.MinSize = new System.Drawing.Size(1, 1);
            this.layoutControlItemAnswers.Name = "layoutControlItemAnswers";
            this.layoutControlItemAnswers.Size = new System.Drawing.Size(636, 112);
            this.layoutControlItemAnswers.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemAnswers.Text = "layoutControlItemAnswers";
            this.layoutControlItemAnswers.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemAnswers.TextToControlDistance = 0;
            this.layoutControlItemAnswers.TextVisible = false;
            // 
            // layoutControlGroupPropertyGrid
            // 
            this.layoutControlGroupPropertyGrid.CustomizationFormText = "layoutControlGroupPropertyGrid";
            this.layoutControlGroupPropertyGrid.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemPropertyGridControl});
            this.layoutControlGroupPropertyGrid.Location = new System.Drawing.Point(0, 112);
            this.layoutControlGroupPropertyGrid.Name = "layoutControlGroupPropertyGrid";
            this.layoutControlGroupPropertyGrid.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupPropertyGrid.Size = new System.Drawing.Size(636, 146);
            this.layoutControlGroupPropertyGrid.Text = "layoutControlGroupPropertyGrid";
            // 
            // layoutControlItemPropertyGridControl
            // 
            this.layoutControlItemPropertyGridControl.Control = this.propertyGridControl;
            this.layoutControlItemPropertyGridControl.CustomizationFormText = "layoutControlItemPropertyGridControl";
            this.layoutControlItemPropertyGridControl.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemPropertyGridControl.Name = "layoutControlItemPropertyGridControl";
            this.layoutControlItemPropertyGridControl.Size = new System.Drawing.Size(626, 116);
            this.layoutControlItemPropertyGridControl.Text = "layoutControlItemPropertyGridControl";
            this.layoutControlItemPropertyGridControl.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemPropertyGridControl.TextToControlDistance = 0;
            this.layoutControlItemPropertyGridControl.TextVisible = false;
            // 
            // DesignerQuestionDevX
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControlMain);
            this.Name = "DesignerQuestionDevX";
            this.Size = new System.Drawing.Size(646, 360);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlMain)).EndInit();
            this.layoutControlMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.propertyGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupControls)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTools)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupAnswers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAnswers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupPropertyGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPropertyGridControl)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DragDropLayoutControl dragDropLayoutControlAnswers;
        private DevExpress.XtraLayout.LayoutControl layoutControlMain;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupMain;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemAnswers;
        private System.Windows.Forms.ListView listViewControls;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTools;
        private System.Windows.Forms.ImageList imageListControls;
        private System.Windows.Forms.ImageList imageListTrash;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupControls;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupAnswers;
        private DevExpress.XtraVerticalGrid.PropertyGridControl propertyGridControl;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemPropertyGridControl;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupPropertyGrid;
        private DevExpress.XtraEditors.SimpleButton simpleButtonTrash;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
    }
}
