using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using SmartCadCore.ClientData;
using SmartCadCore.Core;
using Smartmatic.SmartCad.Service;
using System.Collections;
using System.ServiceModel;
using SmartCadCore.Common;
using SmartCadCore.Enums;

namespace SmartCadGuiCommon
{
    public partial class PositionXtraForm : DevExpress.XtraEditors.XtraForm
    {
        private FormBehavior behavior;
        private PositionClientData selectedPosition;
        private AdministrationRibbonForm parentForm;
        private object syncCommitted = new object();

        private PositionClientData position;
        private DepartmentTypeClientData dept;

        public PositionClientData SelectedPosition
        {
            get
            {
                return selectedPosition;
            }
            set
            {
                selectedPosition = value;
                if (Behavior == FormBehavior.Edit)
                {
                    FormUtil.InvokeRequired(this,
                        delegate
                        {
                            this.textEditPositionName.Text = selectedPosition.Name;
                            this.memoEditDescription.Text = selectedPosition.Description;
                            
                            if (selectedPosition.DepartmentTypeName != null)
                            {
                                foreach (DepartmentTypeClientData dep in comboBoxEditDepartmentTypes.Properties.Items)
                                {
                                    if (dep.Name == selectedPosition.DepartmentTypeName)
                                    {
                                        comboBoxEditDepartmentTypes.SelectedItem = dep;
                                        break;
                                    }
                                }
                            }
                        });
                }
            }
        }

        public FormBehavior Behavior
        {
            get
            {
                return behavior;
            }
            set
            {
                behavior = value;
            }
        }

        public PositionXtraForm(AdministrationRibbonForm parentForm, PositionClientData pposition, FormBehavior behavior)
        {
            InitializeComponent();
            LoadDepartments();
            this.Behavior = behavior;
            this.SelectedPosition = pposition;
            
            LoadLanguage();

            this.parentForm = parentForm;
            this.parentForm.AdministrationCommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(PositionXtraForm_AdministrationCommittedChanges);

            if (Behavior == FormBehavior.Create)
            {
                selectedPosition = new PositionClientData();
            }

            position = selectedPosition.Clone() as PositionClientData;

            EnableButtons(null, EventArgs.Empty);

        }

        public void LoadDepartments()
        {
            IList departments = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetDepartmentsType);
            foreach (DepartmentTypeClientData dep in departments)
            {
                comboBoxEditDepartmentTypes.Properties.Items.Add(dep);
            }
        }

        void PositionXtraForm_AdministrationCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    lock (syncCommitted)
                    {
                        try
                        {
                            if (ServerServiceClient.GetInstance().OperatorClient == null)
                            { }
                        }
                        catch
                        { }

                        if (e.Objects[0] is PositionClientData)
                        {
                            #region PositionClientData
                            PositionClientData position =
                                    e.Objects[0] as PositionClientData;
                            if (SelectedPosition != null && SelectedPosition.Equals(position))
                            {
                                if (e.Action == CommittedDataAction.Update)
                                {
                                    if (ButtonOkPressed == false)
                                        MessageForm.Show(ResourceLoader.GetString2("UpdateFormPositionData"), MessageFormType.Warning);
                                    
                                    SelectedPosition = position;
                                }
                                else if (e.Action == CommittedDataAction.Delete)
                                {
                                    MessageForm.Show(ResourceLoader.GetString2("DeleteFormPositionData"), MessageFormType.Warning);
                                    FormUtil.InvokeRequired(this,
                                    delegate
                                    {
                                        this.Close();
                                    });
                                }
                                
                            }
                            #endregion
                        }
                        else if (e.Objects[0] is DepartmentTypeClientData)
                        {
                            #region DepartmentTypeClientData
                            DepartmentTypeClientData departmentType = e.Objects[0] as DepartmentTypeClientData;
                            if (e.Action == CommittedDataAction.Save)
                            {
                                FormUtil.InvokeRequired(this.comboBoxEditDepartmentTypes,
                                delegate
                                {
                                    comboBoxEditDepartmentTypes.Properties.Items.Add(departmentType);
                                });
                            }
                            else if (e.Action == CommittedDataAction.Update)
                            {
                                FormUtil.InvokeRequired(this.comboBoxEditDepartmentTypes,
                               delegate
                               {
                                   bool select = (((DepartmentTypeClientData)comboBoxEditDepartmentTypes.SelectedItem).Code == departmentType.Code);
                                   int index = this.comboBoxEditDepartmentTypes.Properties.Items.IndexOf(departmentType);
                                   if (index != -1)
                                   {
                                       this.comboBoxEditDepartmentTypes.Properties.Items[index] = departmentType;
                                   }
                                   if (select == true)
                                   {
                                       comboBoxEditDepartmentTypes.SelectedIndex = -1;
                                       comboBoxEditDepartmentTypes.SelectedItem = departmentType;
                                   }
                               });
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

      

        private void LoadLanguage()
        {
            FormUtil.InvokeRequired(this,
                delegate
                {
                    if (Behavior == FormBehavior.Create)
                    {
                        this.Text = ResourceLoader.GetString2("FormCreatePosition");
                        this.simpleButtonAccept.Text = ResourceLoader.GetString2("Create");
                    }
                    else if (Behavior == FormBehavior.Edit)
                    {
                        this.Text = ResourceLoader.GetString2("FormEditPosition");
                        this.simpleButtonAccept.Text = ResourceLoader.GetString2("Accept");
                    }

                    this.layoutControlGroupPosition.Text = ResourceLoader.GetString2("PositionInformation");
                    this.layoutControlItemName.Text = ResourceLoader.GetString2("$Message.Name") + ": *";
                    this.layoutControlItemDepartment.Text = ResourceLoader.GetString2("DepartmentType") + ": *";
                    this.layoutControlItemDescription.Text = ResourceLoader.GetString2("Description") + ":";                    
                    this.simpleButtonCancel.Text = ResourceLoader.GetString2("$Message.Cancel");
                });
        }

        private bool buttonOkPressed = false;

        private bool ButtonOkPressed
        {
            get
            {
                return buttonOkPressed;
            }
        }

        private void simpleButtonAccept_Click(object sender, EventArgs e)
        {
            
            buttonOkPressed = true;

            dept = comboBoxEditDepartmentTypes.SelectedItem as DepartmentTypeClientData;

            if (Behavior == FormBehavior.Create)
            {
                
                selectedPosition.Name = this.textEditPositionName.Text;
                selectedPosition.Description = this.memoEditDescription.Text;

                if (dept != null)
                {
                    selectedPosition.DepartmentTypeName = dept.Name;
                    selectedPosition.DepartmentTypeCode = dept.Code;
                }
                try
                {
                    ServerServiceClient.GetInstance().SaveOrUpdateClientData(selectedPosition);
                }
                catch (FaultException ex)
                {
                    GetErrorFocus(ex);
                    DialogResult = DialogResult.None;
                    MessageForm.Show(ex.Message, ex);
                }
            }
            else if (Behavior == FormBehavior.Edit)
            {
                
                if (selectedPosition.DepartmentTypeCode != dept.Code)
                {
                    
                    long count = (long)ServerServiceClient.GetInstance().SearchBasicObject(SmartCadHqls.GetCustomHql(SmartCadHqls.CountOfficersWithPosition, selectedPosition.Code));

                    if (count > 0)
                    {
                        if (MessageForm.Show(ResourceLoader.GetString2("PositionAssignValidateDelete"), MessageFormType.Question) != DialogResult.Yes)
                        {
                            DialogResult = System.Windows.Forms.DialogResult.None;

                            buttonOkPressed = false;
                            return;
                        }
                    }

                    position.DepartmentTypeCode = dept.Code;
                    position.DepartmentTypeName = dept.Name;

                }

                position.Name = this.textEditPositionName.Text;
                position.Description = this.memoEditDescription.Text;

                
                try
                {
                    ServerServiceClient.GetInstance().SaveOrUpdateClientData(position);
                }
                catch (FaultException ex)
                {
                   
                    DialogResult = DialogResult.None;
                    MessageForm.Show(ex.Message, ex);
                    ServerServiceClient.GetInstance().AskUpdatedObjectToServer(selectedPosition,
                        null);
                    GetErrorFocus(ex);
                }
            }

            buttonOkPressed = false;

        }

        private void GetErrorFocus(FaultException ex)
        {
            if (ex.Message.Contains(ResourceLoader.GetString2("UK_POSITION_NAME")))
            {
                if (behavior == FormBehavior.Create)
                {
                    this.textEditPositionName.Text = this.textEditPositionName.Text;
                }
                textEditPositionName.Focus();
            }

        }

        private void PositionXtraForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.parentForm != null)
            {
                this.parentForm.AdministrationCommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(PositionXtraForm_AdministrationCommittedChanges);
            }
        }
        
        private void EnableButtons(object sender, EventArgs e)
        {
            FormUtil.InvokeRequired(this,
                delegate
                {
                    if (this.textEditPositionName.Text.Trim().Length > 0 &&
                        this.comboBoxEditDepartmentTypes.SelectedIndex > -1 )/*&&
                        this.memoEditDescription.Text.Trim().Length > 0)*/
                    {
                        this.simpleButtonAccept.Enabled = true;
                    }
                    else
                    {
                        this.simpleButtonAccept.Enabled = false;
                    }
                });
        }           
    }
}