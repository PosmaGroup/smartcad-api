using SmartCadControls.Controls;
namespace SmartCadGuiCommon
{
    partial class OfficerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.OfficerFormConvertedLayout = new DevExpress.XtraLayout.LayoutControl();
            this.textBoxBadge = new TextBoxEx();
            this.dateTimePicker1 = new DateTimePickerEx();
            this.textBoxTelephone = new TextBoxEx();
            this.comboBoxStationType = new System.Windows.Forms.ComboBox();
            this.textBoxLastName = new TextBoxEx();
            this.buttonOk = new DevExpress.XtraEditors.SimpleButton();
            this.comboBoxZoneType = new System.Windows.Forms.ComboBox();
            this.textBoxName = new TextBoxEx();
            this.textBoxId = new TextBoxEx();
            this.comboBoxPosition = new System.Windows.Forms.ComboBox();
            this.comboBoxRank = new System.Windows.Forms.ComboBox();
            this.comboBoxDepartmentType = new System.Windows.Forms.ComboBox();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupOfficer = new DevExpress.XtraLayout.LayoutControlGroup();
            this.textBoxNameitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.dateTimePicker1item = new DevExpress.XtraLayout.LayoutControlItem();
            this.textBoxIditem = new DevExpress.XtraLayout.LayoutControlItem();
            this.textBoxLastNameitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupDepartment = new DevExpress.XtraLayout.LayoutControlGroup();
            this.comboBoxDepartmentTypeitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.comboBoxZoneTypeitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.comboBoxStationTypeitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.comboBoxRankitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.comboBoxPositionitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.textBoxBadgeitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.textBoxTelephoneitem = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.OfficerFormConvertedLayout)).BeginInit();
            this.OfficerFormConvertedLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupOfficer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxNameitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateTimePicker1item)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxIditem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxLastNameitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupDepartment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxDepartmentTypeitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxZoneTypeitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxStationTypeitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxRankitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxPositionitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxBadgeitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxTelephoneitem)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCancel.Appearance.Options.UseFont = true;
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(373, 329);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(82, 32);
            this.buttonCancel.StyleController = this.OfficerFormConvertedLayout;
            this.buttonCancel.TabIndex = 3;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // OfficerFormConvertedLayout
            // 
            this.OfficerFormConvertedLayout.AllowCustomizationMenu = false;
            this.OfficerFormConvertedLayout.Controls.Add(this.textBoxBadge);
            this.OfficerFormConvertedLayout.Controls.Add(this.dateTimePicker1);
            this.OfficerFormConvertedLayout.Controls.Add(this.textBoxTelephone);
            this.OfficerFormConvertedLayout.Controls.Add(this.buttonCancel);
            this.OfficerFormConvertedLayout.Controls.Add(this.comboBoxStationType);
            this.OfficerFormConvertedLayout.Controls.Add(this.textBoxLastName);
            this.OfficerFormConvertedLayout.Controls.Add(this.buttonOk);
            this.OfficerFormConvertedLayout.Controls.Add(this.comboBoxZoneType);
            this.OfficerFormConvertedLayout.Controls.Add(this.textBoxName);
            this.OfficerFormConvertedLayout.Controls.Add(this.textBoxId);
            this.OfficerFormConvertedLayout.Controls.Add(this.comboBoxPosition);
            this.OfficerFormConvertedLayout.Controls.Add(this.comboBoxRank);
            this.OfficerFormConvertedLayout.Controls.Add(this.comboBoxDepartmentType);
            this.OfficerFormConvertedLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OfficerFormConvertedLayout.Location = new System.Drawing.Point(0, 0);
            this.OfficerFormConvertedLayout.Name = "OfficerFormConvertedLayout";
            this.OfficerFormConvertedLayout.OptionsCustomizationForm.ShowLayoutTreeView = false;
            this.OfficerFormConvertedLayout.Root = this.layoutControlGroup1;
            this.OfficerFormConvertedLayout.Size = new System.Drawing.Size(457, 370);
            this.OfficerFormConvertedLayout.TabIndex = 14;
            // 
            // textBoxBadge
            // 
            this.textBoxBadge.AllowsLetters = true;
            this.textBoxBadge.AllowsNumbers = true;
            this.textBoxBadge.AllowsPunctuation = false;
            this.textBoxBadge.AllowsSeparators = false;
            this.textBoxBadge.AllowsSymbols = false;
            this.textBoxBadge.AllowsWhiteSpaces = false;
            this.textBoxBadge.ExtraAllowedChars = "";
            this.textBoxBadge.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxBadge.Location = new System.Drawing.Point(159, 276);
            this.textBoxBadge.MaxLength = 20;
            this.textBoxBadge.Name = "textBoxBadge";
            this.textBoxBadge.NonAllowedCharacters = "";
            this.textBoxBadge.RegularExpresion = "";
            this.textBoxBadge.Size = new System.Drawing.Size(291, 20);
            this.textBoxBadge.TabIndex = 11;
            this.textBoxBadge.TextChanged += new System.EventHandler(this.OfficerParameters_Change);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.dateTimePicker1.Location = new System.Drawing.Point(159, 98);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.ShowUpDown = true;
            this.dateTimePicker1.Size = new System.Drawing.Size(291, 20);
            this.dateTimePicker1.TabIndex = 7;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.OfficerParameters_Change);
            this.dateTimePicker1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DateTimePicker1_KeyDown);
            // 
            // textBoxTelephone
            // 
            this.textBoxTelephone.AllowsLetters = false;
            this.textBoxTelephone.AllowsNumbers = true;
            this.textBoxTelephone.AllowsPunctuation = false;
            this.textBoxTelephone.AllowsSeparators = false;
            this.textBoxTelephone.AllowsSymbols = false;
            this.textBoxTelephone.AllowsWhiteSpaces = false;
            this.textBoxTelephone.ExtraAllowedChars = "()x/-";
            this.textBoxTelephone.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxTelephone.Location = new System.Drawing.Point(159, 300);
            this.textBoxTelephone.MaxLength = 17;
            this.textBoxTelephone.Name = "textBoxTelephone";
            this.textBoxTelephone.NonAllowedCharacters = "";
            this.textBoxTelephone.RegularExpresion = "";
            this.textBoxTelephone.Size = new System.Drawing.Size(291, 20);
            this.textBoxTelephone.TabIndex = 13;
            this.textBoxTelephone.TextChanged += new System.EventHandler(this.OfficerParameters_Change);
            // 
            // comboBoxStationType
            // 
            this.comboBoxStationType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxStationType.FormattingEnabled = true;
            this.comboBoxStationType.Location = new System.Drawing.Point(159, 201);
            this.comboBoxStationType.Name = "comboBoxStationType";
            this.comboBoxStationType.Size = new System.Drawing.Size(291, 21);
            this.comboBoxStationType.Sorted = true;
            this.comboBoxStationType.TabIndex = 5;
            this.comboBoxStationType.SelectedIndexChanged += new System.EventHandler(this.OfficerParameters_Change);
            this.comboBoxStationType.SelectionChangeCommitted += new System.EventHandler(this.OfficerParameters_Change);
            this.comboBoxStationType.Enter += new System.EventHandler(this.comboBoxStationType_Enter);
            // 
            // textBoxLastName
            // 
            this.textBoxLastName.AllowsLetters = true;
            this.textBoxLastName.AllowsNumbers = true;
            this.textBoxLastName.AllowsPunctuation = true;
            this.textBoxLastName.AllowsSeparators = true;
            this.textBoxLastName.AllowsSymbols = true;
            this.textBoxLastName.AllowsWhiteSpaces = true;
            this.textBoxLastName.ExtraAllowedChars = "";
            this.textBoxLastName.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxLastName.Location = new System.Drawing.Point(159, 50);
            this.textBoxLastName.MaxLength = 25;
            this.textBoxLastName.Name = "textBoxLastName";
            this.textBoxLastName.NonAllowedCharacters = "";
            this.textBoxLastName.RegularExpresion = "";
            this.textBoxLastName.Size = new System.Drawing.Size(291, 20);
            this.textBoxLastName.TabIndex = 3;
            this.textBoxLastName.TextChanged += new System.EventHandler(this.OfficerParameters_Change);
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOk.Appearance.Options.UseFont = true;
            this.buttonOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOk.Enabled = false;
            this.buttonOk.Location = new System.Drawing.Point(287, 329);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(82, 32);
            this.buttonOk.StyleController = this.OfficerFormConvertedLayout;
            this.buttonOk.TabIndex = 2;
            this.buttonOk.Text = "Accept";
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // comboBoxZoneType
            // 
            this.comboBoxZoneType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxZoneType.FormattingEnabled = true;
            this.comboBoxZoneType.Location = new System.Drawing.Point(159, 176);
            this.comboBoxZoneType.Name = "comboBoxZoneType";
            this.comboBoxZoneType.Size = new System.Drawing.Size(291, 21);
            this.comboBoxZoneType.Sorted = true;
            this.comboBoxZoneType.TabIndex = 3;
            this.comboBoxZoneType.SelectionChangeCommitted += new System.EventHandler(this.comboBoxZoneType_SelectionChangeCommitted);
            this.comboBoxZoneType.Enter += new System.EventHandler(this.comboBoxZoneType_Enter);
            // 
            // textBoxName
            // 
            this.textBoxName.AllowsLetters = true;
            this.textBoxName.AllowsNumbers = true;
            this.textBoxName.AllowsPunctuation = true;
            this.textBoxName.AllowsSeparators = true;
            this.textBoxName.AllowsSymbols = true;
            this.textBoxName.AllowsWhiteSpaces = true;
            this.textBoxName.ExtraAllowedChars = "";
            this.textBoxName.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxName.Location = new System.Drawing.Point(159, 26);
            this.textBoxName.MaxLength = 25;
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.NonAllowedCharacters = "";
            this.textBoxName.RegularExpresion = "";
            this.textBoxName.Size = new System.Drawing.Size(291, 20);
            this.textBoxName.TabIndex = 1;
            this.textBoxName.TextChanged += new System.EventHandler(this.OfficerParameters_Change);
            // 
            // textBoxId
            // 
            this.textBoxId.AllowsLetters = false;
            this.textBoxId.AllowsNumbers = true;
            this.textBoxId.AllowsPunctuation = false;
            this.textBoxId.AllowsSeparators = false;
            this.textBoxId.AllowsSymbols = false;
            this.textBoxId.AllowsWhiteSpaces = false;
            this.textBoxId.ExtraAllowedChars = "";
            this.textBoxId.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxId.Location = new System.Drawing.Point(159, 74);
            this.textBoxId.MaxLength = 12;
            this.textBoxId.Name = "textBoxId";
            this.textBoxId.NonAllowedCharacters = "";
            this.textBoxId.RegularExpresion = "";
            this.textBoxId.Size = new System.Drawing.Size(291, 20);
            this.textBoxId.TabIndex = 5;
            this.textBoxId.TextChanged += new System.EventHandler(this.OfficerParameters_Change);
            // 
            // comboBoxPosition
            // 
            this.comboBoxPosition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPosition.FormattingEnabled = true;
            this.comboBoxPosition.Location = new System.Drawing.Point(159, 251);
            this.comboBoxPosition.Name = "comboBoxPosition";
            this.comboBoxPosition.Size = new System.Drawing.Size(291, 21);
            this.comboBoxPosition.Sorted = true;
            this.comboBoxPosition.TabIndex = 9;
            this.comboBoxPosition.SelectedIndexChanged += new System.EventHandler(this.OfficerParameters_Change);
            this.comboBoxPosition.SelectionChangeCommitted += new System.EventHandler(this.OfficerParameters_Change);
            this.comboBoxPosition.Enter += new System.EventHandler(this.comboBoxPosition_Enter);
            // 
            // comboBoxRank
            // 
            this.comboBoxRank.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxRank.FormattingEnabled = true;
            this.comboBoxRank.Location = new System.Drawing.Point(159, 226);
            this.comboBoxRank.Name = "comboBoxRank";
            this.comboBoxRank.Size = new System.Drawing.Size(291, 21);
            this.comboBoxRank.Sorted = true;
            this.comboBoxRank.TabIndex = 7;
            this.comboBoxRank.SelectedIndexChanged += new System.EventHandler(this.OfficerParameters_Change);
            this.comboBoxRank.SelectionChangeCommitted += new System.EventHandler(this.OfficerParameters_Change);
            this.comboBoxRank.Enter += new System.EventHandler(this.comboBoxRank_Enter);
            // 
            // comboBoxDepartmentType
            // 
            this.comboBoxDepartmentType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDepartmentType.DropDownWidth = 179;
            this.comboBoxDepartmentType.FormattingEnabled = true;
            this.comboBoxDepartmentType.Location = new System.Drawing.Point(159, 151);
            this.comboBoxDepartmentType.Name = "comboBoxDepartmentType";
            this.comboBoxDepartmentType.Size = new System.Drawing.Size(291, 21);
            this.comboBoxDepartmentType.Sorted = true;
            this.comboBoxDepartmentType.TabIndex = 1;
            this.comboBoxDepartmentType.SelectedIndexChanged += new System.EventHandler(this.OfficerParameters_Change);
            this.comboBoxDepartmentType.SelectionChangeCommitted += new System.EventHandler(this.comboBoxDepartmentType_SelectionChangeCommitted);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem7,
            this.emptySpaceItem1,
            this.layoutControlGroupOfficer,
            this.layoutControlGroupDepartment});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(457, 370);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.buttonCancel;
            this.layoutControlItem4.CustomizationFormText = "buttonCancelitem";
            this.layoutControlItem4.Location = new System.Drawing.Point(371, 327);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem4.Name = "buttonCancelitem";
            this.layoutControlItem4.Size = new System.Drawing.Size(86, 43);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "buttonCancelitem";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.buttonOk;
            this.layoutControlItem7.CustomizationFormText = "buttonOkitem";
            this.layoutControlItem7.Location = new System.Drawing.Point(285, 327);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem7.Name = "buttonOkitem";
            this.layoutControlItem7.Size = new System.Drawing.Size(86, 43);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "buttonOkitem";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 327);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(110, 30);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(285, 43);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupOfficer
            // 
            this.layoutControlGroupOfficer.AppearanceGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupOfficer.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroupOfficer.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupOfficer.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroupOfficer.CustomizationFormText = "layoutControlGroupOfficer";
            this.layoutControlGroupOfficer.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.textBoxNameitem,
            this.dateTimePicker1item,
            this.textBoxIditem,
            this.textBoxLastNameitem});
            this.layoutControlGroupOfficer.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupOfficer.Name = "layoutControlGroupOfficer";
            this.layoutControlGroupOfficer.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupOfficer.Size = new System.Drawing.Size(457, 125);
            this.layoutControlGroupOfficer.Text = "layoutControlGroupOfficer";
            // 
            // textBoxNameitem
            // 
            this.textBoxNameitem.Control = this.textBoxName;
            this.textBoxNameitem.CustomizationFormText = "textBoxNameitem";
            this.textBoxNameitem.Location = new System.Drawing.Point(0, 0);
            this.textBoxNameitem.Name = "textBoxNameitem";
            this.textBoxNameitem.Size = new System.Drawing.Size(447, 24);
            this.textBoxNameitem.Text = "textBoxNameitem";
            this.textBoxNameitem.TextSize = new System.Drawing.Size(148, 13);
            // 
            // dateTimePicker1item
            // 
            this.dateTimePicker1item.Control = this.dateTimePicker1;
            this.dateTimePicker1item.CustomizationFormText = "dateTimePicker1item";
            this.dateTimePicker1item.Location = new System.Drawing.Point(0, 72);
            this.dateTimePicker1item.Name = "dateTimePicker1item";
            this.dateTimePicker1item.Size = new System.Drawing.Size(447, 24);
            this.dateTimePicker1item.Text = "dateTimePicker1item";
            this.dateTimePicker1item.TextSize = new System.Drawing.Size(148, 13);
            // 
            // textBoxIditem
            // 
            this.textBoxIditem.Control = this.textBoxId;
            this.textBoxIditem.CustomizationFormText = "textBoxIditem";
            this.textBoxIditem.Location = new System.Drawing.Point(0, 48);
            this.textBoxIditem.Name = "textBoxIditem";
            this.textBoxIditem.Size = new System.Drawing.Size(447, 24);
            this.textBoxIditem.Text = "textBoxIditem";
            this.textBoxIditem.TextSize = new System.Drawing.Size(148, 13);
            // 
            // textBoxLastNameitem
            // 
            this.textBoxLastNameitem.Control = this.textBoxLastName;
            this.textBoxLastNameitem.CustomizationFormText = "textBoxLastNameitem";
            this.textBoxLastNameitem.Location = new System.Drawing.Point(0, 24);
            this.textBoxLastNameitem.Name = "textBoxLastNameitem";
            this.textBoxLastNameitem.Size = new System.Drawing.Size(447, 24);
            this.textBoxLastNameitem.Text = "textBoxLastNameitem";
            this.textBoxLastNameitem.TextSize = new System.Drawing.Size(148, 13);
            // 
            // layoutControlGroupDepartment
            // 
            this.layoutControlGroupDepartment.AppearanceGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroupDepartment.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroupDepartment.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupDepartment.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroupDepartment.CustomizationFormText = "layoutControlGroupDepartment";
            this.layoutControlGroupDepartment.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.comboBoxDepartmentTypeitem,
            this.comboBoxZoneTypeitem,
            this.comboBoxStationTypeitem,
            this.comboBoxRankitem,
            this.comboBoxPositionitem,
            this.textBoxBadgeitem,
            this.textBoxTelephoneitem});
            this.layoutControlGroupDepartment.Location = new System.Drawing.Point(0, 125);
            this.layoutControlGroupDepartment.Name = "layoutControlGroupDepartment";
            this.layoutControlGroupDepartment.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupDepartment.Size = new System.Drawing.Size(457, 202);
            this.layoutControlGroupDepartment.Text = "layoutControlGroupDepartment";
            // 
            // comboBoxDepartmentTypeitem
            // 
            this.comboBoxDepartmentTypeitem.Control = this.comboBoxDepartmentType;
            this.comboBoxDepartmentTypeitem.CustomizationFormText = "comboBoxDepartmentTypeitem";
            this.comboBoxDepartmentTypeitem.Location = new System.Drawing.Point(0, 0);
            this.comboBoxDepartmentTypeitem.Name = "comboBoxDepartmentTypeitem";
            this.comboBoxDepartmentTypeitem.Size = new System.Drawing.Size(447, 25);
            this.comboBoxDepartmentTypeitem.Text = "comboBoxDepartmentTypeitem";
            this.comboBoxDepartmentTypeitem.TextSize = new System.Drawing.Size(148, 13);
            // 
            // comboBoxZoneTypeitem
            // 
            this.comboBoxZoneTypeitem.Control = this.comboBoxZoneType;
            this.comboBoxZoneTypeitem.CustomizationFormText = "comboBoxZoneTypeitem";
            this.comboBoxZoneTypeitem.Location = new System.Drawing.Point(0, 25);
            this.comboBoxZoneTypeitem.Name = "comboBoxZoneTypeitem";
            this.comboBoxZoneTypeitem.Size = new System.Drawing.Size(447, 25);
            this.comboBoxZoneTypeitem.Text = "comboBoxZoneTypeitem";
            this.comboBoxZoneTypeitem.TextSize = new System.Drawing.Size(148, 13);
            // 
            // comboBoxStationTypeitem
            // 
            this.comboBoxStationTypeitem.Control = this.comboBoxStationType;
            this.comboBoxStationTypeitem.CustomizationFormText = "comboBoxStationTypeitem";
            this.comboBoxStationTypeitem.Location = new System.Drawing.Point(0, 50);
            this.comboBoxStationTypeitem.Name = "comboBoxStationTypeitem";
            this.comboBoxStationTypeitem.Size = new System.Drawing.Size(447, 25);
            this.comboBoxStationTypeitem.Text = "comboBoxStationTypeitem";
            this.comboBoxStationTypeitem.TextSize = new System.Drawing.Size(148, 13);
            // 
            // comboBoxRankitem
            // 
            this.comboBoxRankitem.Control = this.comboBoxRank;
            this.comboBoxRankitem.CustomizationFormText = "comboBoxRankitem";
            this.comboBoxRankitem.Location = new System.Drawing.Point(0, 75);
            this.comboBoxRankitem.Name = "comboBoxRankitem";
            this.comboBoxRankitem.Size = new System.Drawing.Size(447, 25);
            this.comboBoxRankitem.Text = "comboBoxRankitem";
            this.comboBoxRankitem.TextSize = new System.Drawing.Size(148, 13);
            // 
            // comboBoxPositionitem
            // 
            this.comboBoxPositionitem.Control = this.comboBoxPosition;
            this.comboBoxPositionitem.CustomizationFormText = "comboBoxPositionitem";
            this.comboBoxPositionitem.Location = new System.Drawing.Point(0, 100);
            this.comboBoxPositionitem.Name = "comboBoxPositionitem";
            this.comboBoxPositionitem.Size = new System.Drawing.Size(447, 25);
            this.comboBoxPositionitem.Text = "comboBoxPositionitem";
            this.comboBoxPositionitem.TextSize = new System.Drawing.Size(148, 13);
            // 
            // textBoxBadgeitem
            // 
            this.textBoxBadgeitem.Control = this.textBoxBadge;
            this.textBoxBadgeitem.CustomizationFormText = "textBoxBadgeitem";
            this.textBoxBadgeitem.Location = new System.Drawing.Point(0, 125);
            this.textBoxBadgeitem.Name = "textBoxBadgeitem";
            this.textBoxBadgeitem.Size = new System.Drawing.Size(447, 24);
            this.textBoxBadgeitem.Text = "textBoxBadgeitem";
            this.textBoxBadgeitem.TextSize = new System.Drawing.Size(148, 13);
            // 
            // textBoxTelephoneitem
            // 
            this.textBoxTelephoneitem.Control = this.textBoxTelephone;
            this.textBoxTelephoneitem.CustomizationFormText = "textBoxTelephoneitem";
            this.textBoxTelephoneitem.Location = new System.Drawing.Point(0, 149);
            this.textBoxTelephoneitem.Name = "textBoxTelephoneitem";
            this.textBoxTelephoneitem.Size = new System.Drawing.Size(447, 24);
            this.textBoxTelephoneitem.Text = "textBoxTelephoneitem";
            this.textBoxTelephoneitem.TextSize = new System.Drawing.Size(148, 13);
            // 
            // OfficerForm
            // 
            this.AcceptButton = this.buttonOk;
            this.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(457, 370);
            this.Controls.Add(this.OfficerFormConvertedLayout);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1000, 469);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(465, 400);
            this.Name = "OfficerForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "OfficerForm";
            this.Load += new System.EventHandler(this.OfficerForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.OfficerFormConvertedLayout)).EndInit();
            this.OfficerFormConvertedLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupOfficer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxNameitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateTimePicker1item)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxIditem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxLastNameitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupDepartment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxDepartmentTypeitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxZoneTypeitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxStationTypeitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxRankitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxPositionitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxBadgeitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxTelephoneitem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxRank;
        private System.Windows.Forms.ComboBox comboBoxDepartmentType;
        private System.Windows.Forms.ComboBox comboBoxPosition;
        private System.Windows.Forms.ComboBox comboBoxStationType;
        private System.Windows.Forms.ComboBox comboBoxZoneType;
        private TextBoxEx textBoxId;
        private TextBoxEx textBoxTelephone;
        private TextBoxEx textBoxBadge;
        private TextBoxEx textBoxName;
        private TextBoxEx textBoxLastName;
        private DateTimePickerEx dateTimePicker1;
		private DevExpress.XtraEditors.SimpleButton buttonCancel;
        private DevExpress.XtraEditors.SimpleButton buttonOk;
        private DevExpress.XtraLayout.LayoutControl OfficerFormConvertedLayout;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem textBoxBadgeitem;
        private DevExpress.XtraLayout.LayoutControlItem dateTimePicker1item;
        private DevExpress.XtraLayout.LayoutControlItem textBoxTelephoneitem;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem comboBoxStationTypeitem;
        private DevExpress.XtraLayout.LayoutControlItem textBoxLastNameitem;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem comboBoxZoneTypeitem;
        private DevExpress.XtraLayout.LayoutControlItem textBoxNameitem;
        private DevExpress.XtraLayout.LayoutControlItem textBoxIditem;
        private DevExpress.XtraLayout.LayoutControlItem comboBoxPositionitem;
        private DevExpress.XtraLayout.LayoutControlItem comboBoxRankitem;
        private DevExpress.XtraLayout.LayoutControlItem comboBoxDepartmentTypeitem;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupOfficer;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupDepartment;
    }
}