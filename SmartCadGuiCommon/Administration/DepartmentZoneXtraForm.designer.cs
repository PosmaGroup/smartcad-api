using SmartCadControls;
namespace SmartCadGuiCommon
{
    partial class DepartmentZoneXtraForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DepartmentZoneXtraForm));
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlZone = new DevExpress.XtraLayout.LayoutControl();
            this.gridControlExCoordinates = new SmartCadControls.GridControlEx();
            this.gridViewExCoordinates = new SmartCadControls.GridViewEx();
            this.labelControlCoordinates = new DevExpress.XtraEditors.LabelControl();
            this.textEditDepartmentZoneName = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEditDepartmentTypes = new DevExpress.XtraEditors.ComboBoxEdit();
            this.simpleButtonAccept = new DevExpress.XtraEditors.SimpleButton();
            this.textEditCustomCode = new DevExpress.XtraEditors.TextEdit();
            this.gridControlExStations = new SmartCadControls.GridControlEx();
            this.gridViewExStations = new SmartCadControls.GridViewEx();
            this.simpleButtonMap = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupData = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemZoneName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemDepartment = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemCoordinates = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItemCustomCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupStations = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemStations = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlZone)).BeginInit();
            this.layoutControlZone.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExCoordinates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExCoordinates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDepartmentZoneName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditDepartmentTypes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCustomCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExStations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExStations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemZoneName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCoordinates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCustomCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupStations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.simpleButtonCancel.Appearance.Options.UseFont = true;
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(382, 409);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(88, 34);
            this.simpleButtonCancel.StyleController = this.layoutControlZone;
            this.simpleButtonCancel.TabIndex = 10;
            this.simpleButtonCancel.Text = "Cancelar";
            // 
            // layoutControlZone
            // 
            this.layoutControlZone.AllowCustomizationMenu = false;
            this.layoutControlZone.Controls.Add(this.gridControlExCoordinates);
            this.layoutControlZone.Controls.Add(this.labelControlCoordinates);
            this.layoutControlZone.Controls.Add(this.textEditDepartmentZoneName);
            this.layoutControlZone.Controls.Add(this.simpleButtonCancel);
            this.layoutControlZone.Controls.Add(this.comboBoxEditDepartmentTypes);
            this.layoutControlZone.Controls.Add(this.simpleButtonAccept);
            this.layoutControlZone.Controls.Add(this.textEditCustomCode);
            this.layoutControlZone.Controls.Add(this.gridControlExStations);
            this.layoutControlZone.Controls.Add(this.simpleButtonMap);
            this.layoutControlZone.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlZone.Location = new System.Drawing.Point(0, 0);
            this.layoutControlZone.Margin = new System.Windows.Forms.Padding(2);
            this.layoutControlZone.Name = "layoutControlZone";
            this.layoutControlZone.OptionsCustomizationForm.ShowLayoutTreeView = false;
            this.layoutControlZone.Root = this.layoutControlGroup1;
            this.layoutControlZone.Size = new System.Drawing.Size(472, 445);
            this.layoutControlZone.TabIndex = 12;
            this.layoutControlZone.Text = "layoutControl1";
            // 
            // gridControlExCoordinates
            // 
            this.gridControlExCoordinates.EnableAutoFilter = false;
            this.gridControlExCoordinates.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExCoordinates.Location = new System.Drawing.Point(86, 96);
            this.gridControlExCoordinates.MainView = this.gridViewExCoordinates;
            this.gridControlExCoordinates.Name = "gridControlExCoordinates";
            this.gridControlExCoordinates.Size = new System.Drawing.Size(379, 189);
            this.gridControlExCoordinates.TabIndex = 12;
            this.gridControlExCoordinates.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExCoordinates});
            this.gridControlExCoordinates.ViewTotalRows = false;
            // 
            // gridViewExCoordinates
            // 
            this.gridViewExCoordinates.AllowFocusedRowChanged = true;
            this.gridViewExCoordinates.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExCoordinates.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExCoordinates.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExCoordinates.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExCoordinates.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExCoordinates.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExCoordinates.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExCoordinates.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExCoordinates.EnablePreviewLineForFocusedRow = false;
            this.gridViewExCoordinates.GridControl = this.gridControlExCoordinates;
            this.gridViewExCoordinates.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gridViewExCoordinates.Name = "gridViewExCoordinates";
            this.gridViewExCoordinates.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExCoordinates.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExCoordinates.ViewTotalRows = false;
            // 
            // labelControlCoordinates
            // 
            this.labelControlCoordinates.Location = new System.Drawing.Point(7, 96);
            this.labelControlCoordinates.Name = "labelControlCoordinates";
            this.labelControlCoordinates.Size = new System.Drawing.Size(68, 13);
            this.labelControlCoordinates.StyleController = this.layoutControlZone;
            this.labelControlCoordinates.TabIndex = 11;
            this.labelControlCoordinates.Text = "Coordenadas:";
            // 
            // textEditDepartmentZoneName
            // 
            this.textEditDepartmentZoneName.Location = new System.Drawing.Point(158, 27);
            this.textEditDepartmentZoneName.Name = "textEditDepartmentZoneName";
            this.textEditDepartmentZoneName.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEditDepartmentZoneName.Properties.Appearance.Options.UseFont = true;
            this.textEditDepartmentZoneName.Size = new System.Drawing.Size(307, 19);
            this.textEditDepartmentZoneName.StyleController = this.layoutControlZone;
            this.textEditDepartmentZoneName.TabIndex = 1;
            this.textEditDepartmentZoneName.TextChanged += new System.EventHandler(this.EnableButtons);
            // 
            // comboBoxEditDepartmentTypes
            // 
            this.comboBoxEditDepartmentTypes.Location = new System.Drawing.Point(158, 73);
            this.comboBoxEditDepartmentTypes.Name = "comboBoxEditDepartmentTypes";
            this.comboBoxEditDepartmentTypes.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxEditDepartmentTypes.Properties.Appearance.Options.UseFont = true;
            this.comboBoxEditDepartmentTypes.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditDepartmentTypes.Size = new System.Drawing.Size(307, 19);
            this.comboBoxEditDepartmentTypes.StyleController = this.layoutControlZone;
            this.comboBoxEditDepartmentTypes.TabIndex = 3;
            this.comboBoxEditDepartmentTypes.SelectedIndexChanged += new System.EventHandler(this.comboBoxEditDepartmentTypes_SelectedIndexChanged);
            // 
            // simpleButtonAccept
            // 
            this.simpleButtonAccept.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.simpleButtonAccept.Appearance.Options.UseFont = true;
            this.simpleButtonAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButtonAccept.Location = new System.Drawing.Point(290, 409);
            this.simpleButtonAccept.Name = "simpleButtonAccept";
            this.simpleButtonAccept.Size = new System.Drawing.Size(88, 34);
            this.simpleButtonAccept.StyleController = this.layoutControlZone;
            this.simpleButtonAccept.TabIndex = 9;
            this.simpleButtonAccept.Text = "Accept";
            this.simpleButtonAccept.Click += new System.EventHandler(this.simpleButtonAccept_Click);
            // 
            // textEditCustomCode
            // 
            this.textEditCustomCode.Location = new System.Drawing.Point(158, 50);
            this.textEditCustomCode.Name = "textEditCustomCode";
            this.textEditCustomCode.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEditCustomCode.Properties.Appearance.Options.UseFont = true;
            this.textEditCustomCode.Size = new System.Drawing.Size(307, 19);
            this.textEditCustomCode.StyleController = this.layoutControlZone;
            this.textEditCustomCode.TabIndex = 5;
            // 
            // gridControlExStations
            // 
            this.gridControlExStations.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(2);
            this.gridControlExStations.EnableAutoFilter = false;
            this.gridControlExStations.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExStations.Location = new System.Drawing.Point(7, 319);
            this.gridControlExStations.MainView = this.gridViewExStations;
            this.gridControlExStations.Margin = new System.Windows.Forms.Padding(2);
            this.gridControlExStations.Name = "gridControlExStations";
            this.gridControlExStations.Size = new System.Drawing.Size(458, 70);
            this.gridControlExStations.TabIndex = 7;
            this.gridControlExStations.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExStations});
            this.gridControlExStations.ViewTotalRows = false;
            // 
            // gridViewExStations
            // 
            this.gridViewExStations.AllowFocusedRowChanged = true;
            this.gridViewExStations.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExStations.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExStations.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExStations.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExStations.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExStations.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExStations.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExStations.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExStations.EnablePreviewLineForFocusedRow = false;
            this.gridViewExStations.GridControl = this.gridControlExStations;
            this.gridViewExStations.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gridViewExStations.Name = "gridViewExStations";
            this.gridViewExStations.OptionsBehavior.FocusLeaveOnTab = true;
            this.gridViewExStations.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExStations.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExStations.ScrollStyle = DevExpress.XtraGrid.Views.Grid.ScrollStyleFlags.LiveVertScroll;
            this.gridViewExStations.ViewTotalRows = false;
            // 
            // simpleButtonMap
            // 
            this.simpleButtonMap.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.simpleButtonMap.Appearance.Options.UseFont = true;
            this.simpleButtonMap.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonMap.Image")));
            this.simpleButtonMap.Location = new System.Drawing.Point(3, 399);
            this.simpleButtonMap.Margin = new System.Windows.Forms.Padding(2);
            this.simpleButtonMap.Name = "simpleButtonMap";
            this.simpleButtonMap.Size = new System.Drawing.Size(43, 43);
            this.simpleButtonMap.StyleController = this.layoutControlZone;
            this.simpleButtonMap.TabIndex = 8;
            this.simpleButtonMap.Click += new System.EventHandler(this.simpleButtonMap_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupData,
            this.layoutControlGroupStations,
            this.layoutControlGroup3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(472, 445);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroupData
            // 
            this.layoutControlGroupData.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroupData.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemZoneName,
            this.layoutControlItemDepartment,
            this.layoutControlItem4,
            this.layoutControlItemCoordinates,
            this.emptySpaceItem2,
            this.layoutControlItemCustomCode});
            this.layoutControlGroupData.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupData.Name = "layoutControlGroupData";
            this.layoutControlGroupData.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupData.Size = new System.Drawing.Size(472, 292);
            this.layoutControlGroupData.Text = "layoutControlGroupData";
            // 
            // layoutControlItemZoneName
            // 
            this.layoutControlItemZoneName.Control = this.textEditDepartmentZoneName;
            this.layoutControlItemZoneName.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItemZoneName.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemZoneName.Name = "layoutControlItemZoneName";
            this.layoutControlItemZoneName.Size = new System.Drawing.Size(462, 23);
            this.layoutControlItemZoneName.Text = "layoutControlItemZoneName";
            this.layoutControlItemZoneName.TextSize = new System.Drawing.Size(148, 13);
            // 
            // layoutControlItemDepartment
            // 
            this.layoutControlItemDepartment.Control = this.comboBoxEditDepartmentTypes;
            this.layoutControlItemDepartment.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItemDepartment.Location = new System.Drawing.Point(0, 46);
            this.layoutControlItemDepartment.Name = "layoutControlItemDepartment";
            this.layoutControlItemDepartment.Size = new System.Drawing.Size(462, 23);
            this.layoutControlItemDepartment.Text = "layoutControlItemDepartment";
            this.layoutControlItemDepartment.TextSize = new System.Drawing.Size(148, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.labelControlCoordinates;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 69);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(79, 17);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItemCoordinates
            // 
            this.layoutControlItemCoordinates.Control = this.gridControlExCoordinates;
            this.layoutControlItemCoordinates.CustomizationFormText = "layoutControlItemCoordinates";
            this.layoutControlItemCoordinates.Location = new System.Drawing.Point(79, 69);
            this.layoutControlItemCoordinates.Name = "layoutControlItemCoordinates";
            this.layoutControlItemCoordinates.Size = new System.Drawing.Size(383, 193);
            this.layoutControlItemCoordinates.Text = "layoutControlItemCoordinates";
            this.layoutControlItemCoordinates.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemCoordinates.TextToControlDistance = 0;
            this.layoutControlItemCoordinates.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 86);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(79, 80);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(79, 176);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItemCustomCode
            // 
            this.layoutControlItemCustomCode.Control = this.textEditCustomCode;
            this.layoutControlItemCustomCode.CustomizationFormText = "layoutControlItemCustomCode";
            this.layoutControlItemCustomCode.Location = new System.Drawing.Point(0, 23);
            this.layoutControlItemCustomCode.Name = "layoutControlItemCustomCode";
            this.layoutControlItemCustomCode.Size = new System.Drawing.Size(462, 23);
            this.layoutControlItemCustomCode.Text = "layoutControlItemCustomCode";
            this.layoutControlItemCustomCode.TextSize = new System.Drawing.Size(148, 13);
            // 
            // layoutControlGroupStations
            // 
            this.layoutControlGroupStations.CustomizationFormText = "layoutControlGroupStations";
            this.layoutControlGroupStations.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemStations});
            this.layoutControlGroupStations.Location = new System.Drawing.Point(0, 292);
            this.layoutControlGroupStations.Name = "layoutControlGroupStations";
            this.layoutControlGroupStations.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupStations.Size = new System.Drawing.Size(472, 104);
            this.layoutControlGroupStations.Text = "layoutControlGroupStations";
            // 
            // layoutControlItemStations
            // 
            this.layoutControlItemStations.Control = this.gridControlExStations;
            this.layoutControlItemStations.CustomizationFormText = "layoutControlItemStations";
            this.layoutControlItemStations.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemStations.Name = "layoutControlItemStations";
            this.layoutControlItemStations.Size = new System.Drawing.Size(462, 74);
            this.layoutControlItemStations.Text = "layoutControlItemStations";
            this.layoutControlItemStations.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemStations.TextToControlDistance = 0;
            this.layoutControlItemStations.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.emptySpaceItem3,
            this.emptySpaceItem4});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 396);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(472, 49);
            this.layoutControlGroup3.Text = "layoutControlGroup3";
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(49, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.ShowInCustomizationForm = false;
            this.emptySpaceItem1.Size = new System.Drawing.Size(239, 49);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.simpleButtonMap;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(49, 49);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(49, 49);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem1.Size = new System.Drawing.Size(49, 49);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.simpleButtonAccept;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(288, 11);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(92, 38);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(92, 38);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(92, 38);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.simpleButtonCancel;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(380, 11);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(92, 38);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(92, 38);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(92, 38);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(288, 0);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(0, 11);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(10, 11);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(92, 11);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(380, 0);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 11);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 11);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(92, 11);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // DepartmentZoneXtraForm
            // 
            this.AcceptButton = this.simpleButtonAccept;
            this.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonCancel;
            this.ClientSize = new System.Drawing.Size(472, 445);
            this.Controls.Add(this.layoutControlZone);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(480, 150);
            this.Name = "DepartmentZoneXtraForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Zona";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DepartmentZoneXtraForm_FormClosing);
            this.Load += new System.EventHandler(this.DepartmentZoneXtraForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlZone)).EndInit();
            this.layoutControlZone.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExCoordinates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExCoordinates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDepartmentZoneName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditDepartmentTypes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCustomCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExStations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExStations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemZoneName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCoordinates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCustomCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupStations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private DevExpress.XtraEditors.SimpleButton simpleButtonAccept;
        private DevExpress.XtraEditors.TextEdit textEditDepartmentZoneName;
        private DevExpress.XtraEditors.TextEdit textEditCustomCode;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditDepartmentTypes;
        private GridControlEx gridControlExStations;
        private GridViewEx gridViewExStations;
        private DevExpress.XtraEditors.SimpleButton simpleButtonMap;
        private DevExpress.XtraLayout.LayoutControl layoutControlZone;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemZoneName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDepartment;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupData;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemCustomCode;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupStations;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemStations;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private GridControlEx gridControlExCoordinates;
        private GridViewEx gridViewExCoordinates;
        private DevExpress.XtraEditors.LabelControl labelControlCoordinates;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemCoordinates;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
    }
}