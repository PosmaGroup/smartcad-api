
using DevExpress.XtraEditors;
using SmartCadControls.Controls;
namespace SmartCadGuiCommon
{
    partial class RoomAdministrationForm : XtraForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RoomAdministrationForm));
            this.groupBoxProperties = new DevExpress.XtraEditors.GroupControl();
            this.comboBoxSizes = new ComboBoxEx();
            this.labelComboBox = new LabelEx();
            this.textBoxPath = new TextBoxEx();
            this.buttonEx1 = new DevExpress.XtraEditors.SimpleButton();
            this.textBoxExDescription = new TextBoxEx();
            this.textBoxName = new TextBoxEx();
            this.labelExMap = new LabelEx();
            this.labelExDescription = new LabelEx();
            this.labelExName = new LabelEx();
            this.groupBoxRoomDesigner = new DevExpress.XtraEditors.GroupControl();
            this.panelDesigner = new System.Windows.Forms.Panel();
            this.buttonExOK = new DevExpress.XtraEditors.SimpleButton();
            this.buttonExCancel = new DevExpress.XtraEditors.SimpleButton();
            this.groupBoxTools = new DevExpress.XtraEditors.GroupControl();
            this.labelToolsDelete = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.buttonDeleteSeat = new DevExpress.XtraEditors.SimpleButton();
            this.labelTools = new System.Windows.Forms.Label();
            this.contextMenuStripDesigner = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItemDesignerDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBoxControlParameters = new DevExpress.XtraEditors.GroupControl();
            this.labelSearch = new System.Windows.Forms.Label();
            this.textBoxExLabel = new TextBoxEx();
            this.buttonSearch = new DevExpress.XtraEditors.SimpleButton();
            this.comboBoxExMachine = new ComboBoxEx();
            this.textBoxExFilter = new TextBoxEx();
            this.labelControlMachine = new System.Windows.Forms.Label();
            this.labelControlLabel = new System.Windows.Forms.Label();
            this.mapDialog = new System.Windows.Forms.OpenFileDialog();
            this.toolTipRoomForm = new System.Windows.Forms.ToolTip(this.components);
            this.toolTipSeats = new System.Windows.Forms.ToolTip(this.components);
            this.toolTipDelete = new System.Windows.Forms.ToolTip(this.components);
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxProperties)).BeginInit();
            this.groupBoxProperties.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxRoomDesigner)).BeginInit();
            this.groupBoxRoomDesigner.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxTools)).BeginInit();
            this.groupBoxTools.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.contextMenuStripDesigner.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxControlParameters)).BeginInit();
            this.groupBoxControlParameters.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxProperties
            // 
            this.groupBoxProperties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.groupBoxProperties.Appearance.Options.UseForeColor = true;
            this.groupBoxProperties.Controls.Add(this.comboBoxSizes);
            this.groupBoxProperties.Controls.Add(this.labelComboBox);
            this.groupBoxProperties.Controls.Add(this.textBoxPath);
            this.groupBoxProperties.Controls.Add(this.buttonEx1);
            this.groupBoxProperties.Controls.Add(this.textBoxExDescription);
            this.groupBoxProperties.Controls.Add(this.textBoxName);
            this.groupBoxProperties.Controls.Add(this.labelExMap);
            this.groupBoxProperties.Controls.Add(this.labelExDescription);
            this.groupBoxProperties.Controls.Add(this.labelExName);
            this.groupBoxProperties.Location = new System.Drawing.Point(6, 7);
            this.groupBoxProperties.Name = "groupBoxProperties";
            this.groupBoxProperties.Size = new System.Drawing.Size(640, 189);
            this.groupBoxProperties.TabIndex = 0;
            this.groupBoxProperties.Text = "Datos de la sala";
            // 
            // comboBoxSizes
            // 
            this.comboBoxSizes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSizes.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.comboBoxSizes.FormattingEnabled = true;
            this.comboBoxSizes.Location = new System.Drawing.Point(123, 157);
            this.comboBoxSizes.Name = "comboBoxSizes";
            this.comboBoxSizes.Size = new System.Drawing.Size(207, 21);
            this.comboBoxSizes.TabIndex = 8;
            this.comboBoxSizes.SelectionChangeCommitted += new System.EventHandler(this.comboBoxExSizes_SelectionChangeCommitted);
            this.comboBoxSizes.SelectedValueChanged += new System.EventHandler(this.comboBoxExSizes_SelectionChangeCommitted);
            // 
            // labelComboBox
            // 
            this.labelComboBox.AutoSize = true;
            this.labelComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelComboBox.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelComboBox.Location = new System.Drawing.Point(14, 160);
            this.labelComboBox.Name = "labelComboBox";
            this.labelComboBox.Size = new System.Drawing.Size(108, 13);
            this.labelComboBox.TabIndex = 7;
            this.labelComboBox.Text = "Tamano del puesto: *";
            // 
            // textBoxPath
            // 
            this.textBoxPath.AllowsLetters = true;
            this.textBoxPath.AllowsNumbers = true;
            this.textBoxPath.AllowsPunctuation = true;
            this.textBoxPath.AllowsSeparators = true;
            this.textBoxPath.AllowsSymbols = true;
            this.textBoxPath.AllowsWhiteSpaces = true;
            this.textBoxPath.ExtraAllowedChars = "";
            this.textBoxPath.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxPath.Location = new System.Drawing.Point(122, 121);
            this.textBoxPath.Name = "textBoxPath";
            this.textBoxPath.NonAllowedCharacters = "";
            this.textBoxPath.ReadOnly = true;
            this.textBoxPath.RegularExpresion = "";
            this.textBoxPath.Size = new System.Drawing.Size(437, 20);
            this.textBoxPath.TabIndex = 5;
            this.textBoxPath.TabStop = false;
            // 
            // buttonEx1
            // 
            this.buttonEx1.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonEx1.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonEx1.Appearance.Options.UseFont = true;
            this.buttonEx1.Appearance.Options.UseForeColor = true;
            this.buttonEx1.Location = new System.Drawing.Point(581, 121);
            this.buttonEx1.Name = "buttonEx1";
            this.buttonEx1.Size = new System.Drawing.Size(35, 23);
            this.buttonEx1.TabIndex = 6;
            this.buttonEx1.Text = "....";
            this.toolTipRoomForm.SetToolTip(this.buttonEx1, "Abrir plano");
            this.buttonEx1.Click += new System.EventHandler(this.buttonEx1_Click);
            this.buttonEx1.MouseHover += new System.EventHandler(this.buttonEx1_MouseHover);
            // 
            // textBoxExDescription
            // 
            this.textBoxExDescription.AcceptsReturn = true;
            this.textBoxExDescription.AllowsLetters = true;
            this.textBoxExDescription.AllowsNumbers = true;
            this.textBoxExDescription.AllowsPunctuation = true;
            this.textBoxExDescription.AllowsSeparators = true;
            this.textBoxExDescription.AllowsSymbols = true;
            this.textBoxExDescription.AllowsWhiteSpaces = true;
            this.textBoxExDescription.ExtraAllowedChars = "";
            this.textBoxExDescription.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExDescription.Location = new System.Drawing.Point(122, 61);
            this.textBoxExDescription.MaxLength = 200;
            this.textBoxExDescription.Multiline = true;
            this.textBoxExDescription.Name = "textBoxExDescription";
            this.textBoxExDescription.NonAllowedCharacters = "";
            this.textBoxExDescription.RegularExpresion = "";
            this.textBoxExDescription.Size = new System.Drawing.Size(494, 48);
            this.textBoxExDescription.TabIndex = 3;
            this.textBoxExDescription.TextChanged += new System.EventHandler(this.RoomStatusParameters_Change);
            // 
            // textBoxName
            // 
            this.textBoxName.AllowsLetters = true;
            this.textBoxName.AllowsNumbers = true;
            this.textBoxName.AllowsPunctuation = true;
            this.textBoxName.AllowsSeparators = true;
            this.textBoxName.AllowsSymbols = true;
            this.textBoxName.AllowsWhiteSpaces = true;
            this.textBoxName.ExtraAllowedChars = "";
            this.textBoxName.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxName.Location = new System.Drawing.Point(122, 29);
            this.textBoxName.MaxLength = 40;
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.NonAllowedCharacters = "";
            this.textBoxName.RegularExpresion = "";
            this.textBoxName.Size = new System.Drawing.Size(241, 20);
            this.textBoxName.TabIndex = 1;
            this.textBoxName.TextChanged += new System.EventHandler(this.RoomStatusParameters_Change);
            // 
            // labelExMap
            // 
            this.labelExMap.AutoSize = true;
            this.labelExMap.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExMap.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExMap.Location = new System.Drawing.Point(16, 121);
            this.labelExMap.Name = "labelExMap";
            this.labelExMap.Size = new System.Drawing.Size(44, 13);
            this.labelExMap.TabIndex = 4;
            this.labelExMap.Text = "Plano: *";
            // 
            // labelExDescription
            // 
            this.labelExDescription.AutoSize = true;
            this.labelExDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExDescription.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExDescription.Location = new System.Drawing.Point(16, 61);
            this.labelExDescription.Name = "labelExDescription";
            this.labelExDescription.Size = new System.Drawing.Size(66, 13);
            this.labelExDescription.TabIndex = 2;
            this.labelExDescription.Text = "Descripcion:";
            // 
            // labelExName
            // 
            this.labelExName.AutoSize = true;
            this.labelExName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExName.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExName.Location = new System.Drawing.Point(16, 29);
            this.labelExName.Name = "labelExName";
            this.labelExName.Size = new System.Drawing.Size(54, 13);
            this.labelExName.TabIndex = 0;
            this.labelExName.Text = "Nombre: *";
            // 
            // groupBoxRoomDesigner
            // 
            this.groupBoxRoomDesigner.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.groupBoxRoomDesigner.Appearance.Options.UseForeColor = true;
            this.groupBoxRoomDesigner.Controls.Add(this.panelDesigner);
            this.groupBoxRoomDesigner.Location = new System.Drawing.Point(6, 275);
            this.groupBoxRoomDesigner.Name = "groupBoxRoomDesigner";
            this.groupBoxRoomDesigner.Size = new System.Drawing.Size(640, 352);
            this.groupBoxRoomDesigner.TabIndex = 2;
            this.groupBoxRoomDesigner.Text = "Ubicar puestos";
            // 
            // panelDesigner
            // 
            this.panelDesigner.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.panelDesigner.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelDesigner.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panelDesigner.Location = new System.Drawing.Point(4, 23);
            this.panelDesigner.Name = "panelDesigner";
            this.panelDesigner.Size = new System.Drawing.Size(631, 322);
            this.panelDesigner.TabIndex = 0;
            this.panelDesigner.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panelDesigner_MouseClick);
            // 
            // buttonExOK
            // 
            this.buttonExOK.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExOK.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonExOK.Appearance.Options.UseFont = true;
            this.buttonExOK.Appearance.Options.UseForeColor = true;
            this.buttonExOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonExOK.Enabled = false;
            this.buttonExOK.Location = new System.Drawing.Point(484, 694);
            this.buttonExOK.Name = "buttonExOK";
            this.buttonExOK.Size = new System.Drawing.Size(75, 23);
            this.buttonExOK.TabIndex = 0;
            this.buttonExOK.Text = "Accept";
            this.buttonExOK.Click += new System.EventHandler(this.buttonExOK_Click);
            // 
            // buttonExCancel
            // 
            this.buttonExCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExCancel.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonExCancel.Appearance.Options.UseFont = true;
            this.buttonExCancel.Appearance.Options.UseForeColor = true;
            this.buttonExCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonExCancel.Location = new System.Drawing.Point(565, 694);
            this.buttonExCancel.Name = "buttonExCancel";
            this.buttonExCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonExCancel.TabIndex = 1;
            this.buttonExCancel.Text = "Cancelar";
            // 
            // groupBoxTools
            // 
            this.groupBoxTools.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.groupBoxTools.Appearance.Options.UseForeColor = true;
            this.groupBoxTools.Controls.Add(this.labelToolsDelete);
            this.groupBoxTools.Controls.Add(this.pictureBox1);
            this.groupBoxTools.Controls.Add(this.buttonDeleteSeat);
            this.groupBoxTools.Controls.Add(this.labelTools);
            this.groupBoxTools.Location = new System.Drawing.Point(6, 202);
            this.groupBoxTools.Name = "groupBoxTools";
            this.groupBoxTools.Size = new System.Drawing.Size(640, 67);
            this.groupBoxTools.TabIndex = 1;
            this.groupBoxTools.Text = "Herramientas";
            // 
            // labelToolsDelete
            // 
            this.labelToolsDelete.AutoSize = true;
            this.labelToolsDelete.ForeColor = System.Drawing.Color.Black;
            this.labelToolsDelete.Location = new System.Drawing.Point(317, 35);
            this.labelToolsDelete.Name = "labelToolsDelete";
            this.labelToolsDelete.Size = new System.Drawing.Size(46, 13);
            this.labelToolsDelete.TabIndex = 2;
            this.labelToolsDelete.Text = "Eliminar:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = global::SmartCadGuiCommon.Properties.Resources.App;
            this.pictureBox1.Location = new System.Drawing.Point(258, 36);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(18, 16);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.controlBase_MouseMove);
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.controlBase_MouseDown);
            this.pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.controlBase_MouseUp);
            // 
            // buttonDeleteSeat
            // 
            this.buttonDeleteSeat.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonDeleteSeat.Appearance.Options.UseForeColor = true;
            this.buttonDeleteSeat.Location = new System.Drawing.Point(369, 30);
            this.buttonDeleteSeat.Name = "buttonDeleteSeat";
            this.buttonDeleteSeat.Size = new System.Drawing.Size(25, 25);
            this.buttonDeleteSeat.TabIndex = 1;
            this.buttonDeleteSeat.Click += new System.EventHandler(this.buttonDeleteSeat_Click);
            this.buttonDeleteSeat.MouseHover += new System.EventHandler(this.buttonDeleteSeat_MouseHover);
            // 
            // labelTools
            // 
            this.labelTools.AutoSize = true;
            this.labelTools.ForeColor = System.Drawing.Color.Black;
            this.labelTools.Location = new System.Drawing.Point(16, 35);
            this.labelTools.Name = "labelTools";
            this.labelTools.Size = new System.Drawing.Size(231, 13);
            this.labelTools.TabIndex = 0;
            this.labelTools.Text = "Arrastre y suelte la figura en el plano de la sala :";
            // 
            // contextMenuStripDesigner
            // 
            this.contextMenuStripDesigner.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemDesignerDelete});
            this.contextMenuStripDesigner.Name = "contextMenuStripDesigner";
            this.contextMenuStripDesigner.Size = new System.Drawing.Size(163, 26);
            // 
            // toolStripMenuItemDesignerDelete
            // 
            this.toolStripMenuItemDesignerDelete.Name = "toolStripMenuItemDesignerDelete";
            this.toolStripMenuItemDesignerDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Delete)));
            this.toolStripMenuItemDesignerDelete.Size = new System.Drawing.Size(162, 22);
            this.toolStripMenuItemDesignerDelete.Text = "Borrar";
            this.toolStripMenuItemDesignerDelete.Click += new System.EventHandler(this.toolStripMenuItemDesignerDelete_Click);
            // 
            // groupBoxControlParameters
            // 
            this.groupBoxControlParameters.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.groupBoxControlParameters.Appearance.Options.UseForeColor = true;
            this.groupBoxControlParameters.Controls.Add(this.labelSearch);
            this.groupBoxControlParameters.Controls.Add(this.textBoxExLabel);
            this.groupBoxControlParameters.Controls.Add(this.buttonSearch);
            this.groupBoxControlParameters.Controls.Add(this.comboBoxExMachine);
            this.groupBoxControlParameters.Controls.Add(this.textBoxExFilter);
            this.groupBoxControlParameters.Controls.Add(this.labelControlMachine);
            this.groupBoxControlParameters.Controls.Add(this.labelControlLabel);
            this.groupBoxControlParameters.Location = new System.Drawing.Point(6, 633);
            this.groupBoxControlParameters.Name = "groupBoxControlParameters";
            this.groupBoxControlParameters.Size = new System.Drawing.Size(640, 50);
            this.groupBoxControlParameters.TabIndex = 3;
            this.groupBoxControlParameters.Text = "Propiedades de los puestos";
            // 
            // labelSearch
            // 
            this.labelSearch.AutoSize = true;
            this.labelSearch.ForeColor = System.Drawing.Color.Black;
            this.labelSearch.Location = new System.Drawing.Point(408, 28);
            this.labelSearch.Name = "labelSearch";
            this.labelSearch.Size = new System.Drawing.Size(46, 13);
            this.labelSearch.TabIndex = 4;
            this.labelSearch.Text = "Buscar :";
            this.labelSearch.Visible = false;
            // 
            // textBoxExLabel
            // 
            this.textBoxExLabel.AllowsLetters = false;
            this.textBoxExLabel.AllowsNumbers = true;
            this.textBoxExLabel.AllowsPunctuation = false;
            this.textBoxExLabel.AllowsSeparators = false;
            this.textBoxExLabel.AllowsSymbols = false;
            this.textBoxExLabel.AllowsWhiteSpaces = false;
            this.textBoxExLabel.Enabled = false;
            this.textBoxExLabel.ExtraAllowedChars = "";
            this.textBoxExLabel.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExLabel.Location = new System.Drawing.Point(76, 24);
            this.textBoxExLabel.MaxLength = 3;
            this.textBoxExLabel.Name = "textBoxExLabel";
            this.textBoxExLabel.NonAllowedCharacters = "";
            this.textBoxExLabel.RegularExpresion = "";
            this.textBoxExLabel.Size = new System.Drawing.Size(63, 20);
            this.textBoxExLabel.TabIndex = 1;
            this.textBoxExLabel.Leave += new System.EventHandler(this.textBoxExLabel_TextChanged);
            // 
            // buttonSearch
            // 
            this.buttonSearch.Enabled = false;
            this.buttonSearch.Image = ((System.Drawing.Image)(resources.GetObject("buttonSearch.Image")));
            this.buttonSearch.Location = new System.Drawing.Point(571, 20);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(25, 25);
            this.buttonSearch.TabIndex = 6;
            this.buttonSearch.Visible = false;
            // 
            // comboBoxExMachine
            // 
            this.comboBoxExMachine.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxExMachine.Enabled = false;
            this.comboBoxExMachine.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.comboBoxExMachine.FormattingEnabled = true;
            this.comboBoxExMachine.Location = new System.Drawing.Point(209, 24);
            this.comboBoxExMachine.Name = "comboBoxExMachine";
            this.comboBoxExMachine.Size = new System.Drawing.Size(193, 21);
            this.comboBoxExMachine.TabIndex = 3;
            this.comboBoxExMachine.SelectedIndexChanged += new System.EventHandler(this.comboBoxExMachine_SelectedIndexChanged);
            // 
            // textBoxExFilter
            // 
            this.textBoxExFilter.AllowsLetters = true;
            this.textBoxExFilter.AllowsNumbers = true;
            this.textBoxExFilter.AllowsPunctuation = true;
            this.textBoxExFilter.AllowsSeparators = true;
            this.textBoxExFilter.AllowsSymbols = true;
            this.textBoxExFilter.AllowsWhiteSpaces = true;
            this.textBoxExFilter.Enabled = false;
            this.textBoxExFilter.ExtraAllowedChars = "";
            this.textBoxExFilter.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExFilter.Location = new System.Drawing.Point(460, 24);
            this.textBoxExFilter.MaxLength = 40;
            this.textBoxExFilter.Name = "textBoxExFilter";
            this.textBoxExFilter.NonAllowedCharacters = "";
            this.textBoxExFilter.RegularExpresion = "";
            this.textBoxExFilter.Size = new System.Drawing.Size(105, 20);
            this.textBoxExFilter.TabIndex = 5;
            this.textBoxExFilter.Visible = false;
            // 
            // labelControlMachine
            // 
            this.labelControlMachine.AutoSize = true;
            this.labelControlMachine.BackColor = System.Drawing.Color.Transparent;
            this.labelControlMachine.Enabled = false;
            this.labelControlMachine.ForeColor = System.Drawing.Color.Black;
            this.labelControlMachine.Location = new System.Drawing.Point(145, 28);
            this.labelControlMachine.Name = "labelControlMachine";
            this.labelControlMachine.Size = new System.Drawing.Size(58, 13);
            this.labelControlMachine.TabIndex = 2;
            this.labelControlMachine.Text = "Maquina: *";
            // 
            // labelControlLabel
            // 
            this.labelControlLabel.AutoSize = true;
            this.labelControlLabel.BackColor = System.Drawing.Color.Transparent;
            this.labelControlLabel.Enabled = false;
            this.labelControlLabel.ForeColor = System.Drawing.Color.Black;
            this.labelControlLabel.Location = new System.Drawing.Point(15, 28);
            this.labelControlLabel.Name = "labelControlLabel";
            this.labelControlLabel.Size = new System.Drawing.Size(54, 13);
            this.labelControlLabel.TabIndex = 0;
            this.labelControlLabel.Text = "Numero: *";
            // 
            // mapDialog
            // 
            this.mapDialog.Filter = "JPEG|*.jpg|GIF|*.gif|BITMAPS|*.bmp|PNG|*.png";
            this.mapDialog.Title = "Open Layout";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.buttonExCancel);
            this.panelControl1.Controls.Add(this.buttonExOK);
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(655, 728);
            this.panelControl1.TabIndex = 6;
            // 
            // RoomAdministrationForm
            // 
            this.AcceptButton = this.buttonExOK;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.CancelButton = this.buttonExCancel;
            this.ClientSize = new System.Drawing.Size(652, 727);
            this.Controls.Add(this.groupBoxProperties);
            this.Controls.Add(this.groupBoxTools);
            this.Controls.Add(this.groupBoxControlParameters);
            this.Controls.Add(this.groupBoxRoomDesigner);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RoomAdministrationForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.RoomAdministrationForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxProperties)).EndInit();
            this.groupBoxProperties.ResumeLayout(false);
            this.groupBoxProperties.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxRoomDesigner)).EndInit();
            this.groupBoxRoomDesigner.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxTools)).EndInit();
            this.groupBoxTools.ResumeLayout(false);
            this.groupBoxTools.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.contextMenuStripDesigner.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxControlParameters)).EndInit();
            this.groupBoxControlParameters.ResumeLayout(false);
            this.groupBoxControlParameters.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

       
        #endregion

        private DevExpress.XtraEditors.GroupControl groupBoxProperties;
        private LabelEx labelExName;
        private LabelEx labelExMap;
        private LabelEx labelExDescription;
        private DevExpress.XtraEditors.GroupControl groupBoxRoomDesigner;
        private DevExpress.XtraEditors.SimpleButton buttonExOK;
        private DevExpress.XtraEditors.SimpleButton buttonExCancel;
        private DevExpress.XtraEditors.GroupControl groupBoxTools;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripDesigner;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemDesignerDelete;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.GroupControl groupBoxControlParameters;
        private TextBoxEx textBoxExDescription;
        private TextBoxEx textBoxName;
        private TextBoxEx textBoxPath;
        private System.Windows.Forms.OpenFileDialog mapDialog;
        private System.Windows.Forms.ToolTip toolTipRoomForm;
        private ComboBoxEx comboBoxSizes;
        private LabelEx labelComboBox;
        private System.Windows.Forms.Label labelControlMachine;
        private System.Windows.Forms.Label labelControlLabel;
        private ComboBoxEx comboBoxExMachine;
        private TextBoxEx textBoxExFilter;
        private TextBoxEx textBoxExLabel;
        private DevExpress.XtraEditors.SimpleButton buttonSearch;
        private System.Windows.Forms.Panel panelDesigner;
        private System.Windows.Forms.Label labelTools;
        private DevExpress.XtraEditors.SimpleButton buttonDeleteSeat;
        private System.Windows.Forms.Label labelSearch;
        private System.Windows.Forms.ToolTip toolTipSeats;
        private System.Windows.Forms.ToolTip toolTipDelete;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton buttonEx1;
        private System.Windows.Forms.Label labelToolsDelete;
    }
}