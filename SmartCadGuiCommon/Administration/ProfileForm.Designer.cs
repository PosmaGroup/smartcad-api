using System;
using System.Collections.Generic;
using System.Text;
using DevExpress.XtraEditors;
using SmartCadControls.Controls;

namespace Smartmatic.SmartCad.Gui
{
    public partial class ProfileForm : XtraForm
    {
        private DevExpress.XtraEditors.SimpleButton buttonExOK;
		private DevExpress.XtraEditors.SimpleButton buttonExCancel;
        private TextBoxEx textBoxDescription;
        private TextBoxEx textBoxName;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.textBoxDescription = new TextBoxEx();
            this.textBoxName = new TextBoxEx();
            this.buttonExOK = new DevExpress.XtraEditors.SimpleButton();
            this.ProfileFormConvertedLayout = new DevExpress.XtraLayout.LayoutControl();
            this.buttonExCancel = new DevExpress.XtraEditors.SimpleButton();
            this.treeViewAccess = new Smartmatic.SmartCad.Gui.TriStateTreeView();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupAccess = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlAccess = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupData = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlDesc = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlName = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutConverter1 = new DevExpress.XtraLayout.Converter.LayoutConverter(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ProfileFormConvertedLayout)).BeginInit();
            this.ProfileFormConvertedLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupAccess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlAccess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxDescription
            // 
            this.textBoxDescription.AcceptsReturn = true;
            this.textBoxDescription.AllowsLetters = true;
            this.textBoxDescription.AllowsNumbers = true;
            this.textBoxDescription.AllowsPunctuation = true;
            this.textBoxDescription.AllowsSeparators = true;
            this.textBoxDescription.AllowsSymbols = true;
            this.textBoxDescription.AllowsWhiteSpaces = true;
            this.textBoxDescription.ExtraAllowedChars = "";
            this.textBoxDescription.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxDescription.Location = new System.Drawing.Point(69, 51);
            this.textBoxDescription.MaxLength = 200;
            this.textBoxDescription.Multiline = true;
            this.textBoxDescription.Name = "textBoxDescription";
            this.textBoxDescription.NonAllowedCharacters = "";
            this.textBoxDescription.RegularExpresion = "";
            this.textBoxDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxDescription.Size = new System.Drawing.Size(356, 90);
            this.textBoxDescription.TabIndex = 3;
            // 
            // textBoxName
            // 
            this.textBoxName.AllowsLetters = true;
            this.textBoxName.AllowsNumbers = true;
            this.textBoxName.AllowsPunctuation = true;
            this.textBoxName.AllowsSeparators = true;
            this.textBoxName.AllowsSymbols = true;
            this.textBoxName.AllowsWhiteSpaces = true;
            this.textBoxName.ExtraAllowedChars = "";
            this.textBoxName.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxName.Location = new System.Drawing.Point(69, 27);
            this.textBoxName.MaxLength = 40;
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.NonAllowedCharacters = "";
            this.textBoxName.RegularExpresion = "";
            this.textBoxName.Size = new System.Drawing.Size(356, 20);
            this.textBoxName.TabIndex = 1;
            this.textBoxName.TextChanged += new System.EventHandler(this.ButtonsActivation);
            // 
            // buttonExOK
            // 
            this.buttonExOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonExOK.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExOK.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonExOK.Appearance.Options.UseFont = true;
            this.buttonExOK.Appearance.Options.UseForeColor = true;
            this.buttonExOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonExOK.Enabled = false;
            this.buttonExOK.Location = new System.Drawing.Point(262, 497);
            this.buttonExOK.Name = "buttonExOK";
            this.buttonExOK.Size = new System.Drawing.Size(82, 32);
            this.buttonExOK.StyleController = this.ProfileFormConvertedLayout;
            this.buttonExOK.TabIndex = 2;
            this.buttonExOK.Text = "Accept";
            this.buttonExOK.Click += new System.EventHandler(this.buttonExOK_Click);
            // 
            // ProfileFormConvertedLayout
            // 
            this.ProfileFormConvertedLayout.AllowCustomizationMenu = false;
            this.ProfileFormConvertedLayout.Controls.Add(this.buttonExOK);
            this.ProfileFormConvertedLayout.Controls.Add(this.buttonExCancel);
            this.ProfileFormConvertedLayout.Controls.Add(this.treeViewAccess);
            this.ProfileFormConvertedLayout.Controls.Add(this.textBoxDescription);
            this.ProfileFormConvertedLayout.Controls.Add(this.textBoxName);
            this.ProfileFormConvertedLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ProfileFormConvertedLayout.Location = new System.Drawing.Point(0, 0);
            this.ProfileFormConvertedLayout.Name = "ProfileFormConvertedLayout";
            this.ProfileFormConvertedLayout.OptionsCustomizationForm.ShowLayoutTreeView = false;
            this.ProfileFormConvertedLayout.Root = this.layoutControlGroup1;
            this.ProfileFormConvertedLayout.Size = new System.Drawing.Size(432, 531);
            this.ProfileFormConvertedLayout.TabIndex = 5;
            // 
            // buttonExCancel
            // 
            this.buttonExCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonExCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExCancel.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonExCancel.Appearance.Options.UseFont = true;
            this.buttonExCancel.Appearance.Options.UseForeColor = true;
            this.buttonExCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonExCancel.Location = new System.Drawing.Point(348, 497);
            this.buttonExCancel.Name = "buttonExCancel";
            this.buttonExCancel.Size = new System.Drawing.Size(82, 32);
            this.buttonExCancel.StyleController = this.ProfileFormConvertedLayout;
            this.buttonExCancel.TabIndex = 3;
            this.buttonExCancel.Text = "Cancelar";
            // 
            // treeViewAccess
            // 
            this.treeViewAccess.FullRowSelect = true;
            this.treeViewAccess.ImageIndex = 1;
            this.treeViewAccess.Location = new System.Drawing.Point(69, 175);
            this.treeViewAccess.Name = "treeViewAccess";
            this.treeViewAccess.SelectedImageIndex = 1;
            this.treeViewAccess.Size = new System.Drawing.Size(356, 313);
            this.treeViewAccess.TabIndex = 1;
            this.treeViewAccess.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.treeViewAccess_AfterCheck);
            this.treeViewAccess.Enter += new System.EventHandler(this.treeViewAccess_Enter);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlGroupAccess,
            this.layoutControlGroupData,
            this.emptySpaceItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(432, 531);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.buttonExOK;
            this.layoutControlItem1.CustomizationFormText = "buttonExOKitem";
            this.layoutControlItem1.Location = new System.Drawing.Point(260, 495);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem1.Name = "buttonExOKitem";
            this.layoutControlItem1.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "buttonExOKitem";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.buttonExCancel;
            this.layoutControlItem2.CustomizationFormText = "buttonExCancelitem";
            this.layoutControlItem2.Location = new System.Drawing.Point(346, 495);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem2.Name = "buttonExCancelitem";
            this.layoutControlItem2.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "buttonExCancelitem";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlGroupAccess
            // 
            this.layoutControlGroupAccess.CustomizationFormText = "Accesos";
            this.layoutControlGroupAccess.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlAccess});
            this.layoutControlGroupAccess.Location = new System.Drawing.Point(0, 148);
            this.layoutControlGroupAccess.Name = "layoutControlGroupAccess";
            this.layoutControlGroupAccess.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupAccess.Size = new System.Drawing.Size(432, 347);
            this.layoutControlGroupAccess.Text = "Accesos";
            // 
            // layoutControlAccess
            // 
            this.layoutControlAccess.Control = this.treeViewAccess;
            this.layoutControlAccess.CustomizationFormText = "Accesos:";
            this.layoutControlAccess.Location = new System.Drawing.Point(0, 0);
            this.layoutControlAccess.Name = "layoutControlAccess";
            this.layoutControlAccess.Size = new System.Drawing.Size(422, 317);
            this.layoutControlAccess.Text = "Accesos:";
            this.layoutControlAccess.TextSize = new System.Drawing.Size(58, 13);
            // 
            // layoutControlGroupData
            // 
            this.layoutControlGroupData.CustomizationFormText = "Datos del perfil";
            this.layoutControlGroupData.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlDesc,
            this.layoutControlName});
            this.layoutControlGroupData.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupData.Name = "layoutControlGroupData";
            this.layoutControlGroupData.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupData.Size = new System.Drawing.Size(432, 148);
            this.layoutControlGroupData.Text = "Datos del perfil";
            // 
            // layoutControlDesc
            // 
            this.layoutControlDesc.Control = this.textBoxDescription;
            this.layoutControlDesc.CustomizationFormText = "Descripcion:";
            this.layoutControlDesc.Location = new System.Drawing.Point(0, 24);
            this.layoutControlDesc.MinSize = new System.Drawing.Size(94, 31);
            this.layoutControlDesc.Name = "layoutControlDesc";
            this.layoutControlDesc.Size = new System.Drawing.Size(422, 94);
            this.layoutControlDesc.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlDesc.Text = "Descripcion:";
            this.layoutControlDesc.TextSize = new System.Drawing.Size(58, 13);
            // 
            // layoutControlName
            // 
            this.layoutControlName.Control = this.textBoxName;
            this.layoutControlName.CustomizationFormText = "Nombre: *";
            this.layoutControlName.Location = new System.Drawing.Point(0, 0);
            this.layoutControlName.Name = "layoutControlName";
            this.layoutControlName.Size = new System.Drawing.Size(422, 24);
            this.layoutControlName.Text = "Nombre: *";
            this.layoutControlName.TextSize = new System.Drawing.Size(58, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 495);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(110, 30);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(260, 36);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ProfileForm
            // 
            this.AcceptButton = this.buttonExOK;
            this.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonExCancel;
            this.ClientSize = new System.Drawing.Size(432, 531);
            this.Controls.Add(this.ProfileFormConvertedLayout);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(440, 558);
            this.Name = "ProfileForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ProfileForm_FormClosing);
            this.Load += new System.EventHandler(this.ProfileForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ProfileFormConvertedLayout)).EndInit();
            this.ProfileFormConvertedLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupAccess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlAccess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion

        private Smartmatic.SmartCad.Gui.TriStateTreeView treeViewAccess;
        private new System.ComponentModel.IContainer components;
        private DevExpress.XtraLayout.LayoutControl ProfileFormConvertedLayout;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupAccess;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlAccess;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupData;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlDesc;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlName;
        private DevExpress.XtraLayout.Converter.LayoutConverter layoutConverter1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;

    }
}
