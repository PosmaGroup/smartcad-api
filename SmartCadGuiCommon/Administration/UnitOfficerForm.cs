using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

using Smartmatic.SmartCad.Service;
using System.Collections;
using SmartCadCore.Core;
using System.Reflection;
using System.ServiceModel;
using SmartCadControls.SyncBoxes;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadCore.Enums;
using SmartCadControls;

namespace SmartCadGuiCommon
{
    public partial class UnitOfficerForm : DevExpress.XtraEditors.XtraForm
    {

        #region WrapperUnit

        internal class UnitWrapper 
        {
            private UnitClientData unit;
            int freeSeats;

            public UnitWrapper(UnitClientData unit,int seats)
            {
                this.unit = unit;
                this.freeSeats = seats;
            }

            public override string ToString()
            {
                return unit.CustomCode + " " + unit.Type.Name + " (" + freeSeats + "/" + unit.Capacity + ")";
            }

            public UnitClientData GetUnit() 
            {
                return this.unit;
            }
        }

        #endregion

        DepartmentStationAssignHistoryClientData selectedDepartmentStationAssign = null;
        FormBehavior behaviorType = FormBehavior.Edit;
        Dictionary<int, int> freeSeats;
        DepartmentStationClientData station;
        List<int> officersIDs;
        Timer timer;

        public bool deletePreviousAssign = false;

        bool DepartamentStationOK;
        bool DepartamentZoneOK;
        bool DepartamentTypeOK;

        const int ADD_UNIT_BUTTON_INDEX = 6;
        const int ADD_OFFICER_BUTTON_INDEX = 4;
        const int INDEX_COLUNM_IMAGE = 0;

        private const string DateNHibernateFormatSeconds = "yyyyMMdd HH:mm:ss";
        private const string DateNHibernateFormat = "yyyyMMdd HH:mm";
        private AdministrationRibbonForm parentForm;
        private object syncCommitted = new object();

        public UnitOfficerForm()
        {
            InitializeComponent();

            LoadLanguage();
            FillDepartmentTypes();
            freeSeats = new Dictionary<int, int>();
            officersIDs = new List<int>();
            InitializeDataGrid();
            buttonOk.Enabled = false;
            ClearErrorsDepartamentData();
        }

        private void LoadLanguage() 
        {
            this.Icon = ResourceLoader.GetIcon("$Icon.Asignacion");

            this.layoutControlGroupAssigns.Text = ResourceLoader.GetString2("Assigns") + "*";
            this.layoutControlGroupDepartment.Text = ResourceLoader.GetString2("DepartmentTypeInformation");
            this.layoutControlGroupImport.Text = ResourceLoader.GetString2("ImportAssign");
            this.layoutControlGroupPeriod.Text = ResourceLoader.GetString2("Period");
            
            this.comboBoxDepartmentStationitem.Text = ResourceLoader.GetString2("Station") + ":*";
            this.comboBoxDepartmentTypeitem.Text = ResourceLoader.GetString2("Department") + ":*";
            this.comboBoxDepartmentZoneitem.Text = ResourceLoader.GetString2("Zone") + ":*";
            this.dateTimePickerFromitem.Text = ResourceLoader.GetString2("From") + ":*";
            this.dateTimePickerToitem.Text = ResourceLoader.GetString2("Until") + ":*";

            this.simpleButtonFinalize.Text = ResourceLoader.GetString2("Finalize");
            this.buttonCancel.Text = ResourceLoader.GetString2("$Message.Cancel");
        }
        private void ClearErrorsDepartamentData()
        {
            DepartamentStationOK = true;
            DepartamentZoneOK = true;
            DepartamentTypeOK = true;
        }

        public UnitOfficerForm(AdministrationRibbonForm parentForm,
            DepartmentStationAssignHistoryClientData departmentStationAssign, FormBehavior type)
            : this()
        {
            BehaviorType = type;
            SelectedDepartmentStationAssign = departmentStationAssign;

            if (BehaviorType == FormBehavior.Edit && selectedDepartmentStationAssign.StartDate > ServerServiceClient.GetInstance().GetTime())
            {
                timer = new Timer();
                timer.Tick += new EventHandler(timer_Tick);
                timer.Interval = 1000;
                timer.Start();
            }
            
            this.parentForm = parentForm;
            if (this.parentForm != null)
            {
                this.parentForm.AdministrationCommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(UnitOfficerForm_AdministrationCommittedChanges);
            }
            this.FormClosing += new FormClosingEventHandler(UnitOfficerForm_FormClosing);
        }

        void timer_Tick(object sender, EventArgs e)
        {
            if (BehaviorType == FormBehavior.Edit && selectedDepartmentStationAssign.StartDate < ServerServiceClient.GetInstance().GetTime())
            {
                simpleButtonFinalize.Enabled = true;
                buttonImportUnitOfficerAssign.Enabled = false;
                timer.Stop();
            }
        }

        void UnitOfficerForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.parentForm != null)
            {
                this.parentForm.AdministrationCommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(UnitOfficerForm_AdministrationCommittedChanges);
            }
            if (timer != null)
            {
                if (timer.Enabled == true)
                    timer.Stop();

                try
                {
                    timer.Dispose();
                }
                catch { }
            }
        }

        void UnitOfficerForm_AdministrationCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    lock (syncCommitted)
                    {
                        try
                        {
                            if (ServerServiceClient.GetInstance().OperatorClient == null)
                            { }
                        }
                        catch
                        { }

                        if (e.Objects[0] is UnitClientData)
                        {
                            #region UnitClientData
                            UnitClientData unit =
                                    e.Objects[0] as UnitClientData;
                            #endregion
                        }
                        else if (e.Objects[0] is DepartmentStationAssignHistoryClientData)
                        {
                            #region DepartmentStationAssignHistoryClientData
                            DepartmentStationAssignHistoryClientData history =
                                    e.Objects[0] as DepartmentStationAssignHistoryClientData;
                            
                            #endregion
                        }
                        else if (e.Objects[0] is DepartmentTypeClientData)
                        {
                            #region DepartmentTypeClientData
                            DepartmentTypeClientData departmentType =
                                    e.Objects[0] as DepartmentTypeClientData;

                            DepartmentTypeClientData depTypeData = new DepartmentTypeClientData();
                            depTypeData.Code = departmentType.Code;
                            depTypeData = (DepartmentTypeClientData)ServerServiceClient.GetInstance().RefreshClient(depTypeData);

                            if (e.Action == CommittedDataAction.Save)
                            {
                                FormUtil.InvokeRequired(this.comboBoxDepartmentType,
                                delegate
                                {
                                    comboBoxDepartmentType.Items.Add(depTypeData);
                                });
                            }
                            else if (e.Action == CommittedDataAction.Update)
                            {
                                FormUtil.InvokeRequired(this.comboBoxDepartmentType,
                               delegate
                               {
                                   int index = comboBoxDepartmentType.Items.IndexOf(depTypeData);
                                   if (index != -1)
                                   {
                                       comboBoxDepartmentType.SelectedIndexChanged -=new EventHandler(comboBoxDepartmentType_SelectedIndexChanged);
                                       comboBoxDepartmentType.Items[index] = depTypeData;
                                       comboBoxDepartmentType.SelectedIndexChanged += new EventHandler(comboBoxDepartmentType_SelectedIndexChanged);
                                   }
                               });
                            }
                            #endregion
                        }
                        else if (e.Objects[0] is DepartmentZoneClientData)
                        {
                            #region DepartmentZoneClientData
                            DepartmentZoneClientData departmentZone =
                                    e.Objects[0] as DepartmentZoneClientData;

                            DepartmentZoneClientData depZoneData = new DepartmentZoneClientData();
                            depZoneData.Code = departmentZone.Code;
                            depZoneData = (DepartmentZoneClientData)ServerServiceClient.GetInstance().RefreshClient(depZoneData);

                            if (e.Action == CommittedDataAction.Save)
                            {
                                FormUtil.InvokeRequired(this.comboBoxDepartmentZone,
                               delegate
                               {
                                   comboBoxDepartmentZone.Items.Add(depZoneData);
                               });
                            }
                            else if (e.Action == CommittedDataAction.Update)
                            {
                                FormUtil.InvokeRequired(this.comboBoxDepartmentZone,
                               delegate
                               {
                                   int index = comboBoxDepartmentZone.Items.IndexOf(depZoneData);
                                   if (index != -1)
                                   {
                                       comboBoxDepartmentZone.SelectedIndexChanged -= new EventHandler(comboBoxDepartmentZone_SelectedIndexChanged);
                                       comboBoxDepartmentZone.Items[index] = depZoneData;
                                       comboBoxDepartmentZone.SelectedIndexChanged += new EventHandler(comboBoxDepartmentZone_SelectedIndexChanged);
                                   }
                               });
                            }
                            #endregion
                        }
                        else if (e.Objects[0] is DepartmentStationClientData)
                        {
                            #region DepartmentStationClientData
                            DepartmentStationClientData departmentStation = e.Objects[0] as DepartmentStationClientData;

                            DepartmentStationClientData depStationData = new DepartmentStationClientData();
                            depStationData.Code = departmentStation.Code;
                            depStationData = (DepartmentStationClientData)ServerServiceClient.GetInstance().RefreshClient(depStationData);

                            if (e.Action == CommittedDataAction.Save)
                            {
                                FormUtil.InvokeRequired(this.comboBoxDepartmentStation,
                               delegate
                               {
                                   comboBoxDepartmentStation.Items.Add(depStationData);
                               });
                            }
                            else if (e.Action == CommittedDataAction.Update)
                            {
                                FormUtil.InvokeRequired(this.comboBoxDepartmentStation,
                               delegate
                               {
                                   int index  = comboBoxDepartmentStation.Items.IndexOf(depStationData);
                                   if (index!=-1)
                                   {
                                       comboBoxDepartmentStation.SelectedIndexChanged -= new EventHandler(comboBoxDepartmentStation_SelectedIndexChanged);
                                       comboBoxDepartmentStation.Items[index] = depStationData;
                                       comboBoxDepartmentStation.SelectedIndexChanged += new EventHandler(comboBoxDepartmentStation_SelectedIndexChanged);
                                   }
                               });
                            }
                            #endregion
                        }
                        else if (e.Objects[0] is OfficerClientData)
                        {
                            #region OfficerClientData
                            OfficerClientData officer =
                                    e.Objects[0] as OfficerClientData;
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        public FormBehavior BehaviorType
        {
            get
            {
                return behaviorType;
            }
            set
            {               
                behaviorType = value;
                Clear();
            }
        }

        public DepartmentStationAssignHistoryClientData SelectedDepartmentStationAssign
        {
            get
            {
                return selectedDepartmentStationAssign;
            }
            set
            {
                selectedDepartmentStationAssign = value;
                if (selectedDepartmentStationAssign != null)
                {
                    comboBoxDepartmentType.SelectedItem = selectedDepartmentStationAssign.DepartmentStation.DepartmentZone.DepartmentType;
                    comboBoxDepartmentZone.SelectedItem = selectedDepartmentStationAssign.DepartmentStation.DepartmentZone;
                    comboBoxDepartmentStation.SelectedItem = selectedDepartmentStationAssign.DepartmentStation;

                    //Colocar solo read

                    comboBoxDepartmentType.Enabled = false;
                    comboBoxDepartmentZone.Enabled = false;
                    comboBoxDepartmentStation.Enabled = false;

                    dateTimePickerFrom.Value = selectedDepartmentStationAssign.StartDate;
                    dateTimePickerTo.Value = selectedDepartmentStationAssign.EndDate;

                    if (selectedDepartmentStationAssign.StartDate < ServerServiceClient.GetInstance().GetTime())
                    {
                        simpleButtonFinalize.Enabled = true;
                        buttonImportUnitOfficerAssign.Enabled = false;
                    }
                }
            }
        }

        private void Clear()
        {
            comboBoxDepartmentType.SelectedIndex = -1;
            comboBoxDepartmentZone.Items.Clear();
            comboBoxDepartmentStation.Items.Clear();

            comboBoxDepartmentZone.Enabled = false;
            comboBoxDepartmentStation.Enabled = false;
            
            dateTimePickerTo.Value = DateTime.Now;
            dateTimePickerFrom.Value = DateTime.Now;
        }

        private void UnitParameters_Change(object sender, System.EventArgs e)
        {
            IList list = (IList)dataGridExAssing.GetDataGridObjects(string.Empty);
            bool assingOK = true;
            foreach (GridUnitOfficerAsignData var in list)
            {
                if (var.Error != ErrorsInRow.AssignOk)
                {
                    assingOK = false;
                    break;
                }
            }
            if (
                dateTimePickerFrom.Value >= dateTimePickerTo.Value ||
                comboBoxDepartmentStation.SelectedIndex == -1 || 
                list.Count <= 0 ||
                assingOK == false
                )
                buttonOk.Enabled = false;
            else
                buttonOk.Enabled = true;
        }

        private void InitializeDataGrid()
        {
            dataGridExAssing.Type = typeof(GridUnitOfficerAsignData);
            GridUnitOfficerAsignData.DataGrid = dataGridExAssing;        
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (buttonOk.Enabled == true)
                {
                    bool AllOK = PreProcessAssing();
                    if (AllOK)
                    {
                        BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[1] { GetType().GetMethod("buttonOk_Click1", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[0] });
                        processForm.CanThrowError = true;
                        processForm.ShowDialog();
                    }
                }
            }
            catch (FaultException ex)
            {
                DialogResult = DialogResult.None;

                if (ex.Message == "UK_DEPARTMENT_STATION_ASSIGN")
                {
                    ReProcessSaveOrUpdate();
                }

                MessageForm.Show(ex.Message, MessageFormType.Error);
                buttonOkPressed = false;
            }
            catch (Exception ex)
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, ex);
                buttonOkPressed = false;
            }
        }

        private void ReProcessSaveOrUpdate()
        {
            CheckDepartmentStationAssignHistoryClientData();
        }

        private bool PreProcessAssing()
        {
            //primero vemos las fechas. 
            bool datesOK = CheckHistoryForCurrentDates((DepartmentStationClientData)comboBoxDepartmentStation.SelectedItem);
            if (!datesOK)
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ResourceLoader.GetString2("UniqueUnitOfficerAssign"), MessageFormType.Error);
                return false;
            }

            //finalmente vemos cuantas asignaciones estan malas.
            //luego vemos si mostramos el mensaje o no.
            int k = 0;
            IList list = dataGridExAssing.GetDataGridObjects(string.Empty);
            foreach (GridUnitOfficerAsignData var in list)
            {
                if (var.Error != ErrorsInRow.AssignOk)
                {// con esto sabremos al final si dar un mensaje o no.
                    k++;
                }
            }
            int n = list.Count;
            int diff = n - k;
            if (diff == n)
            {//no hay error en nada
                return true;
            }
            else
            {
                string errorMessage = ResourceLoader.GetString2("ErrorsOnUnitOfficerAssign");
                
                int countOverload = 0;
                foreach (GridUnitOfficerAsignData var in list)
                {
                    if (var.Error == ErrorsInRow.UnitOveloaded)
                    {// con esto sabremos cuantos errores son de sobrecarga
                        countOverload++;
                    }
                }

                if(countOverload == k)
                {
                    errorMessage = ResourceLoader.GetString2("OverSeatLimitsUnitOfficerAssign");
                }

                DialogResult res = MessageForm.Show(errorMessage, MessageFormType.WarningQuestion);
                if (res == DialogResult.OK || res == DialogResult.Yes)
                {
                    return true;
                }
                else
                {
                    DialogResult = DialogResult.None;
                    return false;
                }
            }
        }

        private bool buttonOkPressed = false;

        public bool ButtonOkPressed
        {
            get { return buttonOkPressed; }
        }

        private void buttonOk_Click1()
        {
            buttonOkPressed = true;
            if (behaviorType == FormBehavior.Create)
                selectedDepartmentStationAssign = new DepartmentStationAssignHistoryClientData();
            if (selectedDepartmentStationAssign != null)
            {
                int n = 0; //numero de elementos en la lista

                FormUtil.InvokeRequired(this, delegate
                {
                    selectedDepartmentStationAssign.DepartmentStation = (DepartmentStationClientData)comboBoxDepartmentStation.SelectedItem;
                    selectedDepartmentStationAssign.StartDate = dateTimePickerFrom.Value;
                    selectedDepartmentStationAssign.EndDate = dateTimePickerTo.Value;

                    if (selectedDepartmentStationAssign.StartDate < ServerServiceClient.GetInstance().GetTime())
                        throw new Exception(ResourceLoader.GetString2("UnitOfficerAssignWithBadDate"));

                    IList list = dataGridExAssing.GetDataGridObjects(string.Empty);
                    n = list.Count;//numero de elementos en la lista.
                    ArrayList asigns = new ArrayList();
                    foreach (GridUnitOfficerAsignData var in list)
                    {
                        if (var.Error == ErrorsInRow.AssignOk)
                        {
                            var.UnitOfficerAssing.Code = 0;
                            var.UnitOfficerAssing.Start = dateTimePickerFrom.Value;
                            var.UnitOfficerAssing.End = dateTimePickerTo.Value;
                            asigns.Add(var.UnitOfficerAssing);
                        }

                    }
                    selectedDepartmentStationAssign.UnitOfficerAssign = asigns;
                    
                });

                ServerServiceClient.GetInstance().SaveOrUpdateClientData(selectedDepartmentStationAssign);
            }
        }

        private void FillDepartmentTypes()
        {
            comboBoxDepartmentType.Items.Clear();
            foreach (DepartmentTypeClientData type in ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetDepartmentsType, true))
            {
                comboBoxDepartmentType.Items.Add(type);
            }
            comboBoxDepartmentType.SelectedIndex = -1;
        }

        private void comboBoxDepartmentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxDepartmentType.SelectedIndex != -1)
            {
                this.DepartamentTypeOK = true;
                comboBoxDepartmentZone.Items.Clear();
                comboBoxDepartmentStation.Items.Clear();
                if (!comboBoxDepartmentType.Focused)
                {
                    FillDepartmentZones(((DepartmentTypeClientData)comboBoxDepartmentType.SelectedItem));
                }
            }
            else
                this.DepartamentTypeOK = true;
            UnitParameters_Change(null, null);

        }

        private void FillDepartmentZones(DepartmentTypeClientData departmentType)
        {
            comboBoxDepartmentStation.Items.Clear();
            comboBoxDepartmentZone.Items.Clear();
            departmentType.DepartmentZones = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetDepartmentZonesByDepartmentType, departmentType.Code),true); 
            foreach (DepartmentZoneClientData zone in departmentType.DepartmentZones)
            {
                comboBoxDepartmentZone.Items.Add(zone);
            }
            comboBoxDepartmentZone.SelectedIndex = -1;
        }

        private void comboBoxDepartmentZone_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxDepartmentZone.SelectedIndex != -1)
            {
                this.DepartamentZoneOK = true;
                comboBoxDepartmentStation.Items.Clear();
                if (!comboBoxDepartmentZone.Focused)
                {
                    FillDepartmentStations(((DepartmentZoneClientData)comboBoxDepartmentZone.SelectedItem));
                }
            }
            else
                this.DepartamentZoneOK = true;
            UnitParameters_Change(null, null);

        }

        private void FillDepartmentStations(DepartmentZoneClientData departmentZone)
        {

            comboBoxDepartmentStation.Items.Clear();
            //ServerServiceClient.GetInstance().InitializeLazy(departmentZone, departmentZone.DepartmentStations);
            foreach (DepartmentStationClientData station in departmentZone.DepartmentStations)
            {
                comboBoxDepartmentStation.Items.Add(station);
            }
            comboBoxDepartmentStation.SelectedIndex = -1;
        }

        private void comboBoxDepartmentStation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxDepartmentStation.SelectedIndex != -1)
            {
                DepartmentStationClientData departmentStation = (DepartmentStationClientData)comboBoxDepartmentStation.SelectedItem;
                this.station = departmentStation;
                this.DepartamentStationOK = true;
            }
            else
                this.DepartamentStationOK = true;

        }

        private void dateTimePickerFrom_ValueChanged(object sender, EventArgs e)
        {
            DateTime now  = ServerServiceClient.GetInstance().GetTime();
            if (dateTimePickerFrom.Enabled && dateTimePickerFrom.Value < now)
                dateTimePickerFrom.Value = now;

            if (dateTimePickerTo.Value < dateTimePickerFrom.Value)
            {
                //MessageForm.Show("Las fecha de inicio no puede ser mayor a la final", MessageFormType.Warning);
                dateTimePickerTo.Value = dateTimePickerFrom.Value;
            }
            UnitParameters_Change(null, null);
        }

        private bool CheckHistoryForCurrentDates(DepartmentStationClientData departmentStation)
        {
            string dateFrom = dateTimePickerTo.Value.ToString(DateNHibernateFormat);
            string dateTo = dateTimePickerFrom.Value.ToString(DateNHibernateFormat);

            string code = selectedDepartmentStationAssign == null ? "0" : selectedDepartmentStationAssign.Code + "";

            string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.IntersectDepartmentStationAssign, departmentStation.Code,selectedDepartmentStationAssign.Code,dateFrom,dateTo);

            ArrayList list = (ArrayList)ServerServiceClient.GetInstance().SearchBasicObjects(hql);
            if (list.Count > 0 )
            {
                long count = (long)list[0];

                if (count > 0)
                {
                    return false;
                }
            }
            return true;
        }
        
        private void UnitOfficerForm_Load(object sender, EventArgs e)
        {
            if (BehaviorType == FormBehavior.Create)
            {
                dateTimePickerTo.Value = DateTime.Now;
                dateTimePickerFrom.Value = DateTime.Now - TimeSpan.FromHours(6);
            }
            if (BehaviorType == FormBehavior.Edit)
            {
                LoadRowsInDataGrid();
            }

            switch (behaviorType)
            {
                case FormBehavior.Create:
                    Text = ResourceLoader.GetString2("UnitOfficerCreateFormText");
                    buttonOk.Text = ResourceLoader.GetString("UnitOfficerFormCreateButtonOkText");
                    break;
                case FormBehavior.Edit:
                    Text = ResourceLoader.GetString2("UnitOfficerEditFormText");
                    buttonOk.Text = ResourceLoader.GetString("UnitOfficerFormEditButtonOkText");
                   
                    break;
                default:
                    break;
            }
          
        }

        private void LoadRowsInDataGrid()
        {
            DateTime now = ServerServiceClient.GetInstance().GetTime();
            this.station = selectedDepartmentStationAssign.DepartmentStation;
            if (dateTimePickerTo.Value <= now && now < dateTimePickerFrom.Value)
            {
                dateTimePickerFrom.Enabled = false;
                dateTimePickerTo.Enabled = false;
            }

            //ServerServiceClient.GetInstance().InitializeLazy(selectedDepartmentStationAssign, selectedDepartmentStationAssign.UnitOfficerAssign);
            foreach (UnitOfficerAssignHistoryClientData var in selectedDepartmentStationAssign.UnitOfficerAssign)
            {
                GridUnitOfficerAsignData row = new GridUnitOfficerAsignData(var);
                dataGridExAssing.AddData(row);
                int index = dataGridExAssing.Rows.GetLastRow(DataGridViewElementStates.None);
                DataGridViewDisableButtonCell cellOfficer = (DataGridViewDisableButtonCell)dataGridExAssing.Rows[index].Cells[ADD_OFFICER_BUTTON_INDEX];
                DataGridViewDisableButtonCell cellUnit = (DataGridViewDisableButtonCell)dataGridExAssing.Rows[index].Cells[ADD_UNIT_BUTTON_INDEX];
                cellOfficer.Enabled = cellUnit.Enabled = false;
                DataGridViewImageCell cellImage = (DataGridViewImageCell)dataGridExAssing.Rows[index].Cells[INDEX_COLUNM_IMAGE];
                row.Icon = ResourceLoader.GetImage("UnitOfficerAssignOK");
                dataGridExAssing.UpdateData(row);
            }
        }

        private bool CheckOfficerAssign(OfficerClientData officerData, DateTime start, DateTime end)
        {
            bool result = false;
            //ServerServiceClient.GetInstance().InitializeLazy(officerData, officerData.UnitsAssigned);
            for (int i = 0; i < officerData.UnitsAssigned.Count && result == false; i++)
            {
                UnitOfficerAssignHistoryClientData unitOfficerAssign = (UnitOfficerAssignHistoryClientData)officerData.UnitsAssigned[i];
                if (unitOfficerAssign.Officer == officerData)
                {
                    if ((start >= unitOfficerAssign.Start && start <= unitOfficerAssign.End) ||
                        (end >= unitOfficerAssign.Start && end <= unitOfficerAssign.End))
                    {
                        result = true;
                    }
                }
            }
            return result;
        }

        private void comboBoxDepartmentStation_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (comboBoxDepartmentStation.SelectedIndex != -1)
            {
                DepartmentStationClientData departmentStation = (DepartmentStationClientData)comboBoxDepartmentStation.SelectedItem;
                this.station = departmentStation;
                this.DepartamentStationOK = true;
                if (!comboBoxDepartmentStation.Focused)
                {
                    //actaulizamos las asignaciones.
                    InitializeSeats(departmentStation);
                    UpdateAssignsInDataGrid();
                    //vemos si coincide con otra asignacion hecha sobre la misma estacion.
                    bool overwriteAssings = CheckDepartmentStationAssignHistoryClientData();
                    if (!overwriteAssings)
                    { //implica que el usuario se quiere quedar con la vieja asignacion,
                        //en este caso nos vamos de este metodo
                        return;
                    }
                }
                else
                {
                    CheckUnitOfficerAssignDataGrid();
                }
                dateTimePickerFrom_ValueChanged(sender, e);
                UnitParameters_Change(null, null);
                //buttonOk.Enabled = true;
            }
        }

        private void InitializeSeats(DepartmentStationClientData dep)
        {
            if (dep == null)
                return;

			IList units = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetUnitsByDepartmentStationCode,dep.Code), true);

            foreach (UnitClientData var in units)
            {
                if (freeSeats.ContainsKey(var.Code))
                    freeSeats[var.Code] = var.Capacity;
                else
                    freeSeats.Add(var.Code, var.Capacity);
            }
        }

        private void dateTimePickerTo_ValueChanged(object sender, EventArgs e)
        {
            dateTimePickerFrom_ValueChanged(sender, e);
        }

        private void dataGridExAssing_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && (e.ColumnIndex == ADD_UNIT_BUTTON_INDEX || e.ColumnIndex == ADD_OFFICER_BUTTON_INDEX))
            {
                ButtonCellsPressed(e.RowIndex, e.ColumnIndex);
            }
        }

        private void CheckDataGridCellButtonsEnabled()
        {
            for(int i=0;i<dataGridExAssing.RowCount;i++)
            {
                GridUnitOfficerAsignData row = (GridUnitOfficerAsignData)dataGridExAssing.GetData(i);
                if (row.Error.Equals(ErrorsInRow.OfficerNotInSystem) || row.Error.Equals(ErrorsInRow.UnitNotInSystem)==false)
                {
                    DataGridViewDisableButtonCell cellUnit = (DataGridViewDisableButtonCell)dataGridExAssing.Rows[i].Cells[ADD_UNIT_BUTTON_INDEX];
                    DataGridViewDisableButtonCell cellOfficer = (DataGridViewDisableButtonCell)dataGridExAssing.Rows[i].Cells[ADD_OFFICER_BUTTON_INDEX];
                    cellOfficer.Enabled = cellUnit.Enabled = false;
                }
            }
        }



        private void ButtonCellsPressed(int x, int y)
        {
            bool cancelForm = false;
            DataGridViewDisableButtonCell cell = (DataGridViewDisableButtonCell)dataGridExAssing.Rows[x].Cells[y];
            if (y == ADD_UNIT_BUTTON_INDEX && cell.Enabled)
            {
                GridUnitOfficerAsignData row = (GridUnitOfficerAsignData)dataGridExAssing.GetData(x);
                if (comboBoxDepartmentStation.SelectedItem != null)
                {
                    row.UnitOfficerAssing.Unit.DepartmentStation = (DepartmentStationClientData)comboBoxDepartmentStation.SelectedItem;
                    if (comboBoxDepartmentZone.SelectedItem != null)
                    {
                        row.UnitOfficerAssing.Unit.DepartmentStation.DepartmentZone = (DepartmentZoneClientData)comboBoxDepartmentZone.SelectedItem;
                        if (comboBoxDepartmentType.SelectedItem != null)
                            row.UnitOfficerAssing.Unit.DepartmentType = (DepartmentTypeClientData)comboBoxDepartmentType.SelectedItem;
                    }
                }
                UnitForm unitForm = new UnitForm(this.parentForm, row.UnitOfficerAssing.Unit, FormBehavior.Create);

                if (DialogResult.OK == unitForm.ShowDialog())
                {
                    //GridUnitOfficerAsignData row = (GridUnitOfficerAsignData)dataGridExAssing.GetData(x);
                    row.Error = ErrorsInRow.UnitNotInDepartamentStation;
                    row.UnitOfficerAssing.Unit = unitForm.SelectedUnit;
                    dataGridExAssing.UpdateData(row);
                    if (row.Error != ErrorsInRow.BadDepartamentStation)
                        InitializeSeats((DepartmentStationClientData)comboBoxDepartmentStation.SelectedItem);
                }
                else
                    cancelForm = true;
            }
            if (y == ADD_OFFICER_BUTTON_INDEX && cell.Enabled)
            {
                GridUnitOfficerAsignData row = (GridUnitOfficerAsignData)dataGridExAssing.GetData(x);
                if (comboBoxDepartmentStation.SelectedItem != null)
                {
                    row.UnitOfficerAssing.Officer.DepartmentStation = (DepartmentStationClientData)comboBoxDepartmentStation.SelectedItem;
                    if (comboBoxDepartmentZone.SelectedItem != null)
                    {
                        row.UnitOfficerAssing.Officer.DepartmentStation.DepartmentZone = (DepartmentZoneClientData)comboBoxDepartmentZone.SelectedItem;
                        if (comboBoxDepartmentType.SelectedItem != null)
                            row.UnitOfficerAssing.Officer.DepartmentType = (DepartmentTypeClientData)comboBoxDepartmentType.SelectedItem;
                    }
                }
                OfficerForm officerForm = new OfficerForm(this.parentForm, row.UnitOfficerAssing.Officer, FormBehavior.Create);
                if (DialogResult.OK == officerForm.ShowDialog())
                {
                    //GridUnitOfficerAsignData row = (GridUnitOfficerAsignData)dataGridExAssing.GetData(x);
                    row.UnitOfficerAssing.Officer = officerForm.SelectedOfficer;
                    dataGridExAssing.UpdateData(row);
                    if (row.Error != ErrorsInRow.BadDepartamentStation)
                        InitializeSeats((DepartmentStationClientData)comboBoxDepartmentStation.SelectedItem);
                }
                else
                    cancelForm = true;
            }
            if (cell.Enabled && cancelForm == false)
            {
                UpdateAssignsInDataGrid();
                UnitParameters_Change(null, null);
                cell.Enabled = false;
            }
        }

        private void dataGridExAssing_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && e.Clicks == 1 && e.RowIndex >= 0 && ( e.ColumnIndex == ADD_UNIT_BUTTON_INDEX || e.ColumnIndex == ADD_OFFICER_BUTTON_INDEX))
            {
                ButtonCellsPressed(e.RowIndex, e.ColumnIndex);
            }
        }

        private void buttonImportUnitOfficerAssign_Click(object sender, EventArgs e)
        {
            
            DialogResult resOpen = openFileDialogExcelDoc.ShowDialog();
            
            if (DialogResult.OK == resOpen)
            {
                if (behaviorType == FormBehavior.Edit)
                {
                    DialogResult res = MessageForm.Show(ResourceLoader.GetString2("ReplaceExistingAssignsWithNewFile"), MessageFormType.WarningQuestion);
                    if (res == DialogResult.Yes || res == DialogResult.OK)
                    {
                        buttonOkPressed = true;
                        if(!deletePreviousAssign)
                            ServerServiceClient.GetInstance().DeleteClientObject(selectedDepartmentStationAssign);
                        this.deletePreviousAssign = true;
                    }
                    else
                        return;
                }
                Clear();
                ClearErrorsDepartamentData();
                string fileName = openFileDialogExcelDoc.FileName;
                this.textBoxImportUnitOfficerAssign.Text = fileName;
                textBoxImportUnitOfficerAssign.Tag = fileName;
                dataGridExAssing.ClearData();
                freeSeats.Clear();
                comboBoxDepartmentType.SelectedIndex = -1;
                FileInfo fi = new FileInfo(fileName);
                bool isopen = ApplicationUtil.CheckFileOpenByAnother(fileName);
                if (fi.Extension.EndsWith("xls") && !isopen)
                    LoadDataBaseAssign(fileName);
                else if (isopen)
                    MessageForm.Show(ResourceLoader.GetString2("FileAlreadyOpenByOtherUser"), MessageFormType.Error);
                else
                    MessageForm.Show(ResourceLoader.GetString2("InvalidFile"), MessageFormType.Error);
            }
            buttonOk.Focus();
            //   toolTipUnitOfficerAssign.SetToolTip(buttonImportUnitOfficerAssign, "Abrir archivo");
        }


        private void buttonImportUnitOfficerAssign_MouseHover(object sender, EventArgs e) {

            toolTipUnitOfficerAssign.SetToolTip(buttonImportUnitOfficerAssign, ResourceLoader.GetString2("OpenFile"));
               
        }

        private void buttonImportUnitOfficerAssign_MouseLeave(object sender, EventArgs e) {
            toolTipUnitOfficerAssign.Hide(buttonImportUnitOfficerAssign);
  
        }

        /// <summary>
        /// Busca en los combo box el string pasado y si lo consigue 
        /// lo selecciona.
        /// </summary>
        /// <typeparam name="T">Tipo de datos que contiene el combo</typeparam>
        /// <param name="combo">ComboBox</param>
        /// <param name="name">Nombre a buscar</param>
        /// <param name="errorMessage">Mensaje de error</param>
        private bool FindSelectElemCombo<T>(ComboBox comboBox, string name) where T : INamedObjectClientData
        {
            string nameOffAccent = ApplicationUtil.GetStringWithoutAccent(name).ToLower();
            bool find = false;
            foreach(T var in comboBox.Items)
            {
                string notAcc = ApplicationUtil.GetStringWithoutAccent(var.Name).ToLower();
                if (notAcc == nameOffAccent)
                {
                    comboBox.SelectedItem = var;
                    comboBox.Enabled = false;
                    find = true;
                    break;
                }
            }
            if (!find)
            {
                buttonOk.Enabled = false;
            }
            return find;
        }

        private DepartmentStationAssignHistoryClientData GetAssignHistoryDataFromExcelFile(string file)
        {
            return UnitOfficerAssignDataBase.GetAssignHistoryDataFromExcelFile(file);
        }

        private void LoadDataBaseAssign(string fileName)
        {
            ClearErrorsDepartamentData();
            BackgroundProcessForm processForm = null;
            try
            {
                processForm = new BackgroundProcessForm(this, new MethodInfo[1] { GetType().GetMethod("GetAssignHistoryDataFromExcelFile", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[1] { fileName } });
                processForm.CanThrowError = true;
                processForm.ShowDialog();
            }
            catch (FaultException ex)
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, MessageFormType.Error);
                return;
            }
            catch (Exception ex)
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, ex);
                return;
            }

            selectedDepartmentStationAssign = (DepartmentStationAssignHistoryClientData)processForm.Results[0];

            string departament = selectedDepartmentStationAssign.DepartmentStation.DepartmentZone.DepartmentType.Name;
            string zone = selectedDepartmentStationAssign.DepartmentStation.DepartmentZone.Name;
            string station = selectedDepartmentStationAssign.DepartmentStation.Name;

            bool findDepartamentType = FindSelectElemCombo<DepartmentTypeClientData>(comboBoxDepartmentType, departament);
            bool findDepartamentZone = FindSelectElemCombo<DepartmentZoneClientData>(comboBoxDepartmentZone, zone);
            bool findDepartamentStation = FindSelectElemCombo<DepartmentStationClientData>(comboBoxDepartmentStation, station);

            ProcessDepartamentErrors(findDepartamentType, findDepartamentZone, findDepartamentStation);

            dateTimePickerFrom.Value = selectedDepartmentStationAssign.StartDate;
            if ((selectedDepartmentStationAssign.StartDate >= selectedDepartmentStationAssign.EndDate) || (selectedDepartmentStationAssign.StartDate <= ServerServiceClient.GetInstance().GetTime()))
            {
                dateTimePickerFrom.Value = dateTimePickerTo.Value = DateTime.Now;
                MessageForm.Show(ResourceLoader.GetString2("InvalidDateUnitOfficerAssign"), MessageFormType.Error);
                dateTimePickerFrom.Enabled = true;
                dateTimePickerTo.Enabled = true;
            }
            else
            {
                dateTimePickerFrom.Enabled = false;
                dateTimePickerTo.Enabled = false;    
            }
            bool overwriteAssings;
            if (dateTimePickerTo.Enabled)
            {
                dateTimePickerTo.Value = dateTimePickerFrom.Value;
                overwriteAssings = true;
            }
            else
            {
                dateTimePickerTo.Value = selectedDepartmentStationAssign.EndDate;
                overwriteAssings = CheckDepartmentStationAssignHistoryClientData();
            }
            if (!overwriteAssings)
            { //implica que el usuario se quiere quedar con la vieja asignacion,
              //se borra todo lo creado hasta ahora y se sale de este metodo.
                Clear();
                return;
            }

            dataGridExAssing.ClearData();
            freeSeats.Clear();
            officersIDs.Clear();
            FillAssignsInDataGrid();
            UnitParameters_Change(null, null);
            
        }

        private bool CheckDepartmentStationAssignHistoryClientData()
        {
            DepartmentStationAssignHistoryClientData repeted = SearchRepetedAssings(selectedDepartmentStationAssign);
            if (repeted != null)
            {//si ya hay una solicitud pendiente entonces hay que borrar la existente.
                DialogResult res = MessageForm.Show(ResourceLoader.GetString2("ExistingSameDateAssign"), MessageFormType.WarningQuestion);
                if (res == DialogResult.Yes || res == DialogResult.OK)
                {
                    ServerServiceClient.GetInstance().DeleteClientObject(repeted);
                    return true;
                }
                else
                {// si el usuario no desea eliminarla entonces nos vamos de este metodo.
                    return false;
                }
            }
            return true;
        }

        private DepartmentStationAssignHistoryClientData SearchRepetedAssings(DepartmentStationAssignHistoryClientData depassing)
        {
            if (comboBoxDepartmentStation.SelectedItem != null)
            {
                DepartmentStationClientData dep = (DepartmentStationClientData)comboBoxDepartmentStation.SelectedItem;
                string dateFrom = depassing.StartDate.ToString(DateNHibernateFormatSeconds);
                string dateTo = depassing.EndDate.ToString(DateNHibernateFormatSeconds);
                
                string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.SeekRepetedAssings, dep.Code, dateFrom, dateTo);

                IList list = ServerServiceClient.GetInstance().SearchClientObjects(hql,true);
                if (list.Count > 0)
                {
                    return (DepartmentStationAssignHistoryClientData)list[0];
                }
                return null;
            }
            return null;
        }

        private void FillAssignsInDataGrid()
        {
            foreach (UnitOfficerAssignHistoryClientData var in selectedDepartmentStationAssign.UnitOfficerAssign)
            {
                OfficerClientData officer = CheckOfficerInFile(var.Officer, comboBoxDepartmentStation.SelectedIndex);
                UnitClientData unit = CheckUnitInFile(var.Unit, comboBoxDepartmentStation.SelectedIndex);
                if (unit != null)
                    unit = (UnitClientData)ServerServiceClient.GetInstance().RefreshClient(unit, true);
                int id = AddRowToDataGrid(var, officer, unit);
                UpdateRowInDataGrid(id, var, officer, unit);
            }
        }

        private void UpdateAssignsInDataGrid()
        {
            int id = 0;
            if (selectedDepartmentStationAssign == null || selectedDepartmentStationAssign.UnitOfficerAssign == null)
                return;
            officersIDs.Clear();
            foreach (UnitOfficerAssignHistoryClientData var in selectedDepartmentStationAssign.UnitOfficerAssign)
            {
                OfficerClientData officer = CheckOfficerInFile(var.Officer, comboBoxDepartmentStation.SelectedIndex);
                UnitClientData unit = CheckUnitInFile(var.Unit, comboBoxDepartmentStation.SelectedIndex);
                UpdateRowInDataGrid(id++, var, officer, unit);
            }
            UnitParameters_Change(null, null);
        }

        private void ProcessDepartamentErrors(bool findDepartamentType, bool findDepartamentZone, bool findDepartamentStation)
        {
            bool showMessage = false;

            if (!findDepartamentType)
            {
                this.DepartamentTypeOK = false;
                this.comboBoxDepartmentType.Enabled = true;
                this.comboBoxDepartmentZone.Enabled = true;
                this.comboBoxDepartmentStation.Enabled = true;
                showMessage = true;
            }
            if (!findDepartamentZone)
            {
                this.DepartamentZoneOK = false;
                this.comboBoxDepartmentZone.Enabled = true;
                this.comboBoxDepartmentStation.Enabled = true;
                showMessage = true;
            }
            if (!findDepartamentStation)
            {
                this.DepartamentStationOK = false;
                this.comboBoxDepartmentStation.Enabled = true;
                showMessage = true;
            }

            if (showMessage)
            {
                if (!findDepartamentType)
                    MessageForm.Show(ResourceLoader.GetString2("InvalidDepartamentType"), MessageFormType.Warning);
                else if (!findDepartamentZone)
                    MessageForm.Show(ResourceLoader.GetString2("InvalidDepartamentZone"), MessageFormType.Warning);
                else
                    MessageForm.Show(ResourceLoader.GetString2("InvalidDepartamentStation"), MessageFormType.Warning);
            }
        }


        private int AddRowToDataGrid(UnitOfficerAssignHistoryClientData var, OfficerClientData officer, UnitClientData unit)
        {
            GridUnitOfficerAsignData row = new GridUnitOfficerAsignData(var);
            dataGridExAssing.AddData(row);
            return dataGridExAssing.Rows.GetLastRow(DataGridViewElementStates.None);
        }

        private void UpdateRowInDataGrid(int index,UnitOfficerAssignHistoryClientData var,OfficerClientData officer, UnitClientData unit)
        {
            GridUnitOfficerAsignData row = (GridUnitOfficerAsignData)dataGridExAssing.GetData(index);
            DataGridViewDisableButtonCell cellOfficer = (DataGridViewDisableButtonCell)dataGridExAssing.Rows[index].Cells[ADD_OFFICER_BUTTON_INDEX];
            DataGridViewDisableButtonCell cellUnit = (DataGridViewDisableButtonCell)dataGridExAssing.Rows[index].Cells[ADD_UNIT_BUTTON_INDEX];
            DataGridViewImageCell cellImage = (DataGridViewImageCell)dataGridExAssing.Rows[index].Cells[INDEX_COLUNM_IMAGE];
            string ret = GetResultDepartament();
            int offdepOK=-1;
            int unitdepOK=-1;
            cellOfficer.ToolTipText = "";
            cellUnit.ToolTipText = "";
            
            row.Error = ErrorsInRow.BadDepartamentStation;
            row.Icon = ResourceLoader.GetImage("BadStation");

            //Colocamos los tooltips y errores necessarios para el oficial
            offdepOK = SetOfficerStuffs(var, officer, row, cellOfficer, cellImage, ret, offdepOK);
            //igual para la unidad
            unitdepOK = SetUnitStuffs(var, unit, row, cellUnit, cellImage, ret, unitdepOK);

            if (row.Error != ErrorsInRow.BadDepartamentStation)
            {
                if (officer == null)
                {
                    //cellImage.ToolTipText = ResourceLoader.GetString2("BadUnitOfficerAssign");
                    cellImage.ToolTipText = ResourceLoader.GetString2("OfficerNotInSystem");
                    row.Icon = ResourceLoader.GetImage("BadStation");
                }
                else if (unit == null)
                {
                    row.Icon = ResourceLoader.GetImage("BadStation");
                    cellImage.ToolTipText = ResourceLoader.GetString2("UnitNotInSystem");
                }
                else if (offdepOK == 1 && unitdepOK == 1)
                    cellImage.ToolTipText = ResourceLoader.GetString2("BadUnitDepartmentAndOfficerDepartment");
                else if (offdepOK == 1 && unitdepOK == 2)
                    cellImage.ToolTipText = ResourceLoader.GetString2("BadUnitZoneAndOfficerDepartment");
                else if (offdepOK == 1 && unitdepOK == 3)
                    cellImage.ToolTipText = ResourceLoader.GetString2("BadUnitStationAndOfficerDepartment");
                else if (offdepOK == 2 && unitdepOK == 1)
                    cellImage.ToolTipText = ResourceLoader.GetString2("BadUnitDepartmentAndOfficerZone");
                else if (offdepOK == 2 && unitdepOK == 2)
                    cellImage.ToolTipText = ResourceLoader.GetString2("BadUnitZoneAndOfficerZone");
                else if (offdepOK == 2 && unitdepOK == 3)
                    cellImage.ToolTipText = ResourceLoader.GetString2("BadUnitStationAndOfficerZone");
                else if (offdepOK == 3 && unitdepOK == 1)
                    cellImage.ToolTipText = ResourceLoader.GetString2("BadUnitDepartmentAndOfficerStation");
                else if (offdepOK == 3 && unitdepOK == 2)
                    cellImage.ToolTipText = ResourceLoader.GetString2("BadUnitZoneAndOfficerStation");
                else if (offdepOK == 3 && unitdepOK == 3)
                    cellImage.ToolTipText = ResourceLoader.GetString2("BadUnitStationAndOfficerStation");
            }

            //vemos si la asignacion esta correcta.

            if(offdepOK == 0 && unitdepOK == 0)
            {//implica que la asignacion esta perfect.
                //Vemos si el oficial no esta repetido

                if (!officersIDs.Contains(officer.Code))
                {
                    officersIDs.Add(officer.Code);
                    //Vemos los asientos.
                    row.Error = ErrorsInRow.AssignOk;
                    row.Icon = ResourceLoader.GetImage("UnitOfficerAssignOK");
                    cellImage.ToolTipText = "";
                    AccomadateSeats(unit, row, cellImage);
                }
                else
                { //implica que el oficial esta repetido en una asignacion buena.
                    row.Error = ErrorsInRow.OfficerRepeted;
                    row.Icon = ResourceLoader.GetImage("BadStation");
                    cellImage.ToolTipText = ResourceLoader.GetString2("DuplicatedOfficer");
                }
            }
            
            row.UnitOfficerAssing = var;
            dataGridExAssing.UpdateData(row);
        }

        private void AccomadateSeats(UnitClientData unit, GridUnitOfficerAsignData row, DataGridViewImageCell imageCell)
        {
            if (freeSeats.ContainsKey(unit.Code) == false)
            {
                freeSeats.Add(unit.Code, unit.Capacity);
            }

            if (freeSeats[unit.Code] <= 0)
            {
                row.Error = ErrorsInRow.UnitOveloaded;
                imageCell.ToolTipText = ResourceLoader.GetString2("UnitOfficerAssignOverloaded");
                row.Icon = ResourceLoader.GetImage("UnitOverloaded");
            }
            else
            {
                freeSeats[unit.Code]--;
            }

        }

        private int SetUnitStuffs(UnitOfficerAssignHistoryClientData var, UnitClientData unit, GridUnitOfficerAsignData row, DataGridViewDisableButtonCell cellUnit, DataGridViewImageCell cellImage, string ret, int unitdepOK)
        {
            if (unit != null)
            {
                var.Unit = unit;
                cellUnit.Enabled = false;
                if (!row.Error.Equals(ErrorsInRow.OfficerNotInSystem))
                {
                
                    if (ret.Length > 0)
                    {
                        cellImage.ToolTipText = cellUnit.ToolTipText = string.Format(ResourceLoader.GetString2("InvalidDataOnUnitOfficerAssign"), ret);
                        row.Error = ErrorsInRow.BadDepartamentStation;
                        row.Icon = ResourceLoader.GetImage("BadStation");
                    }
                    else
                    {
                        int code = ((DepartmentTypeClientData)comboBoxDepartmentType.SelectedItem).Code;
                        if (code == unit.DepartmentType.Code)
                        {
                            code = ((DepartmentZoneClientData)comboBoxDepartmentZone.SelectedItem).Code;
                            if (code == unit.DepartmentStation.DepartmentZone.Code)
                            {
                                code = ((DepartmentStationClientData)comboBoxDepartmentStation.SelectedItem).Code;
                                if (code == unit.DepartmentStation.Code)
                                {
                                    unitdepOK = 0;
                                }
                                else
                                {
                                    cellImage.ToolTipText = ResourceLoader.GetString2("InvalidUnitStation");
                                    row.Error = ErrorsInRow.UnitNotInDepartamentStation;
                                    row.Icon = ResourceLoader.GetImage("UnitNotInStation");
                                    unitdepOK = 3;
                                }
                            }
                            else
                            {

                                cellImage.ToolTipText = ResourceLoader.GetString2("InvalidUnitZone");
                                row.Error = ErrorsInRow.UnitNotInDepartamentStation;
                                row.Icon = ResourceLoader.GetImage("UnitNotInStation");
                                unitdepOK = 2;
                            }
                    
                        }
                        else
                        {
                            cellImage.ToolTipText = ResourceLoader.GetString2("InvalidUnitDepartment");
                            row.Error = ErrorsInRow.UnitNotInDepartamentStation;
                            row.Icon = ResourceLoader.GetImage("UnitNotInStation");
                            unitdepOK = 1;
                        }
                    }
                }
            }
            else
            {
                cellImage.ToolTipText = cellUnit.ToolTipText = ResourceLoader.GetString2("UnitNotInSystem");
                row.Error = ErrorsInRow.UnitNotInSystem;
                row.Icon = ResourceLoader.GetImage("UnitNotInSystem");
            }
            return unitdepOK;
        }

        private int SetOfficerStuffs(UnitOfficerAssignHistoryClientData var, OfficerClientData officer, GridUnitOfficerAsignData row, DataGridViewDisableButtonCell cellOfficer, DataGridViewImageCell cellImage, string ret, int offdepOK)
        {
            if (officer != null)
            {
                var.Officer = officer;
                cellOfficer.Enabled = false;
                if (ret.Length > 0)
                {
                    cellImage.ToolTipText = cellOfficer.ToolTipText = string.Format(ResourceLoader.GetString2("InvalidDataOnUnitOfficerAssign"), ret);
                    row.Error = ErrorsInRow.BadDepartamentStation;
                    row.Icon = ResourceLoader.GetImage("BadStation");
                }
                else
                {
                    int code = ((DepartmentTypeClientData)comboBoxDepartmentType.SelectedItem).Code;
                    if (code == officer.DepartmentType.Code)
                    {
                        code = ((DepartmentZoneClientData)comboBoxDepartmentZone.SelectedItem).Code;
                        if (code == officer.DepartmentStation.DepartmentZone.Code)
                        {
                            code = ((DepartmentStationClientData)comboBoxDepartmentStation.SelectedItem).Code;
                            if (code == officer.DepartmentStation.Code)
                            {
                                offdepOK = 0;
                            }
                            else
                            {
                                cellImage.ToolTipText = ResourceLoader.GetString2("InvalidOfficerStation"); 
                                row.Error = ErrorsInRow.OfficerNotInDepartamentStation;
                                row.Icon = ResourceLoader.GetImage("OfficerNotInStation");
                                offdepOK = 3;
                            }
                        }
                        else
                        {

                            cellImage.ToolTipText = ResourceLoader.GetString2("InvalidOfficerZone");
                            row.Error = ErrorsInRow.OfficerNotInDepartamentStation;
                            row.Icon = ResourceLoader.GetImage("OfficerNotInStation");
                            offdepOK = 2;
                        }
                
                    }
                    else
                    {
                        cellImage.ToolTipText = ResourceLoader.GetString2("InvalidOfficerDepartment");
                        row.Error = ErrorsInRow.OfficerNotInDepartamentStation;
                        row.Icon = ResourceLoader.GetImage("OfficerNotInStation");
                        offdepOK = 1;
                    }
                }
            }
            else
            {
                cellImage.ToolTipText = cellOfficer.ToolTipText = ResourceLoader.GetString2("OfficerNotInSystem");
                row.Error = ErrorsInRow.OfficerNotInSystem;
                row.Icon = ResourceLoader.GetImage("OfficerNotInSystem");
            }
            return offdepOK;
        }

        private string GetResultDepartament()
        {
            StringBuilder sb = new StringBuilder();
            bool before = false;
            if (!DepartamentTypeOK)
            {
                sb.Append(ResourceLoader.GetString2("FromDepartment"));
                before = true;
            }
            if (!DepartamentZoneOK)
            {
                if(before)
                    sb.Append(ResourceLoader.GetString2("FromZone"));
                else
                    sb.Append(ResourceLoader.GetString2("FromTheZone"));
                before = true;
            }
            if (!DepartamentStationOK)
            {
                if (before)
                    sb.Append(ResourceLoader.GetString2("FromStation"));
                else
                    sb.Append(ResourceLoader.GetString2("FromTheStation"));
            }
            return sb.ToString();
        }

        private UnitClientData CheckUnitInFile(UnitClientData unitData,int index)
        {
            string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetUnitByCustomCode, unitData.CustomCode);
            IList list = ServerServiceClient.GetInstance().SearchClientObjects(hql,true);
            if (list.Count > 0)
            {
                return (UnitClientData)list[0];
            }
            return null;
        }

        private OfficerClientData CheckOfficerInFile(OfficerClientData officerData, int index)
        {
            string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetOfficersByPersonID, officerData.PersonID);
            IList list = ServerServiceClient.GetInstance().SearchClientObjects(hql,true);
            if (list.Count > 0)
            {
                return (OfficerClientData)list[0];
            }
            return null;
        }

        private void textBoxImportUnitOfficerAssign_Leave(object sender, EventArgs e)
        {
            
            if (!string.IsNullOrEmpty(textBoxImportUnitOfficerAssign.Text))
            {
                FileInfo file = new FileInfo(textBoxImportUnitOfficerAssign.Text.Trim());
                if (file.Exists)
                {
                    string previous = (string)textBoxImportUnitOfficerAssign.Tag;
                    if (previous != textBoxImportUnitOfficerAssign.Text.Trim())
                        LoadDataBaseAssign(textBoxImportUnitOfficerAssign.Text.Trim());
                    textBoxImportUnitOfficerAssign.Tag = textBoxImportUnitOfficerAssign.Text.Trim();
                }
                else
                    textBoxImportUnitOfficerAssign.Text = (string)textBoxImportUnitOfficerAssign.Tag;
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            if (this.deletePreviousAssign)
                selectedDepartmentStationAssign = null;
            this.Close();

        }


        private void dataGridExAssing_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                buttonOk_Click(null, null);
                this.DialogResult = DialogResult.OK;
                this.Close();
            }

        }

        private void ColumnHeaderClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            CheckDataGridCellButtonsEnabled();
        }

        private void comboBoxDepartmentZone_Enter(object sender, EventArgs e)
        {
            if (comboBoxDepartmentType.SelectedIndex != -1 && comboBoxDepartmentZone.SelectedIndex == -1)
            {
                FillDepartmentZones(((DepartmentTypeClientData)comboBoxDepartmentType.SelectedItem));
            }
        }

        private void comboBoxDepartmentStation_Enter(object sender, EventArgs e)
        {
            if (comboBoxDepartmentZone.SelectedIndex != -1 && comboBoxDepartmentStation.SelectedIndex == -1)
            {
                FillDepartmentStations(((DepartmentZoneClientData)comboBoxDepartmentZone.SelectedItem));
            }
        }
        private void comboBoxDepartmentStation_Leave(object sender, EventArgs e)
        {
            if (comboBoxDepartmentStation.SelectedIndex != -1 )
            {
                CheckUnitOfficerAssignDataGrid();
            }
        }

        private void CheckUnitOfficerAssignDataGrid()
        {
            for (int id = 0; id < selectedDepartmentStationAssign.UnitOfficerAssign.Count; id++)
            {
                UnitOfficerAssignHistoryClientData var = (selectedDepartmentStationAssign.UnitOfficerAssign[id] as UnitOfficerAssignHistoryClientData);
                OfficerClientData officer = var.Officer;
                UnitClientData unit = var.Unit;
                GridUnitOfficerAsignData row = (GridUnitOfficerAsignData)dataGridExAssing.GetData(id);
                officersIDs.Clear();
                if (!row.Error.Equals(ErrorsInRow.OfficerNotInSystem) && !row.Error.Equals(ErrorsInRow.UnitNotInSystem))
                    UpdateRowInDataGrid(id, var, officer, unit);
            }
        }

        /// <summary>
        /// Boton para finalizar las asignaciones de oficiales a unidades.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButtonFinalize_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageForm.Show(ResourceLoader.GetString2("UnitOfficerAssignFinalize"), MessageFormType.Question);
            if (result == DialogResult.Yes)
            {
                UpdateAssignsInDataGrid();
                if (ServerServiceClient.GetInstance().GetTime() < selectedDepartmentStationAssign.EndDate)
                {
                    foreach (UnitOfficerAssignHistoryClientData data in selectedDepartmentStationAssign.UnitOfficerAssign)
                    {
                        data.End = ServerServiceClient.GetInstance().GetTime();
                    }

                    selectedDepartmentStationAssign.EndDate = ServerServiceClient.GetInstance().GetTime();
                    ServerServiceClient.GetInstance().SaveOrUpdateClientData(selectedDepartmentStationAssign);
                }
                else
                {
                    MessageForm.Show(ResourceLoader.GetString2("UnitOfficerAssignFinalized"), MessageFormType.Warning);
                }
                this.Close();
            }
        }
    }
}
