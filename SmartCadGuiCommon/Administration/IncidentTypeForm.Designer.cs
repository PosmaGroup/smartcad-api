using SmartCadControls.Controls;
using SmartCadGuiCommon.Controls;
namespace SmartCadGuiCommon
{
    partial class IncidentTypeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private new System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlIncidentType = new DevExpress.XtraLayout.LayoutControl();
            this.iconPictureBox = new System.Windows.Forms.PictureBox();
            this.buttonExIcon = new DevExpress.XtraEditors.SimpleButton();
            this.richTextBoxExtended1 = new RichTextBoxExtended();
            this.checkedListBoxDepartmentTypes = new CheckedListBoxEx();
            this.checkBoxNoEmergency = new System.Windows.Forms.CheckBox();
            this.buttonOk = new DevExpress.XtraEditors.SimpleButton();
            this.textBoxName = new TextBoxEx();
            this.textBoxExMnemonic = new TextBoxEx();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupIncidentType = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemMnemonico = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemEmergency = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupDepartment = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemDepartment = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupFirstLevel = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupIcon = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupPreview = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.openFileDialogMain = new System.Windows.Forms.OpenFileDialog();
            this.toolTipIncidentType = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlIncidentType)).BeginInit();
            this.layoutControlIncidentType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iconPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupIncidentType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemMnemonico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemEmergency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupDepartment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupFirstLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupPreview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCancel.Appearance.Options.UseFont = true;
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(388, 544);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(82, 32);
            this.buttonCancel.StyleController = this.layoutControlIncidentType;
            this.buttonCancel.TabIndex = 6;
            // 
            // layoutControlIncidentType
            // 
            this.layoutControlIncidentType.AllowCustomizationMenu = false;
            this.layoutControlIncidentType.Controls.Add(this.iconPictureBox);
            this.layoutControlIncidentType.Controls.Add(this.buttonExIcon);
            this.layoutControlIncidentType.Controls.Add(this.richTextBoxExtended1);
            this.layoutControlIncidentType.Controls.Add(this.checkedListBoxDepartmentTypes);
            this.layoutControlIncidentType.Controls.Add(this.buttonCancel);
            this.layoutControlIncidentType.Controls.Add(this.checkBoxNoEmergency);
            this.layoutControlIncidentType.Controls.Add(this.buttonOk);
            this.layoutControlIncidentType.Controls.Add(this.textBoxName);
            this.layoutControlIncidentType.Controls.Add(this.textBoxExMnemonic);
            this.layoutControlIncidentType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlIncidentType.Location = new System.Drawing.Point(0, 0);
            this.layoutControlIncidentType.Name = "layoutControlIncidentType";
            this.layoutControlIncidentType.OptionsCustomizationForm.ShowLayoutTreeView = false;
            this.layoutControlIncidentType.Root = this.layoutControlGroup1;
            this.layoutControlIncidentType.Size = new System.Drawing.Size(472, 578);
            this.layoutControlIncidentType.TabIndex = 8;
            this.layoutControlIncidentType.Text = "layoutControl1";
            // 
            // iconPictureBox
            // 
            this.iconPictureBox.Location = new System.Drawing.Point(114, 474);
            this.iconPictureBox.Name = "iconPictureBox";
            this.iconPictureBox.Size = new System.Drawing.Size(339, 49);
            this.iconPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.iconPictureBox.TabIndex = 5;
            this.iconPictureBox.TabStop = false;
            // 
            // buttonExIcon
            // 
            this.buttonExIcon.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExIcon.Appearance.Options.UseFont = true;
            this.buttonExIcon.Location = new System.Drawing.Point(7, 469);
            this.buttonExIcon.Name = "buttonExIcon";
            this.buttonExIcon.Size = new System.Drawing.Size(91, 38);
            this.buttonExIcon.StyleController = this.layoutControlIncidentType;
            this.buttonExIcon.TabIndex = 1;
            this.buttonExIcon.Text = "Escoger icono";
            this.buttonExIcon.Click += new System.EventHandler(this.buttonEx1_Click);
            // 
            // richTextBoxExtended1
            // 
            this.richTextBoxExtended1.AcceptsTab = false;
            this.richTextBoxExtended1.AutoWordSelection = true;
            this.richTextBoxExtended1.DetectURLs = true;
            this.richTextBoxExtended1.Location = new System.Drawing.Point(7, 221);
            this.richTextBoxExtended1.MaxLength = 7500;
            this.richTextBoxExtended1.Name = "richTextBoxExtended1";
            this.richTextBoxExtended1.ReadOnly = false;
            // 
            // 
            // 
            this.richTextBoxExtended1.RichTextBox.AutoWordSelection = true;
            this.richTextBoxExtended1.RichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBoxExtended1.RichTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.richTextBoxExtended1.RichTextBox.Location = new System.Drawing.Point(0, 25);
            this.richTextBoxExtended1.RichTextBox.MaxLength = 7500;
            this.richTextBoxExtended1.RichTextBox.Name = "rtb1";
            this.richTextBoxExtended1.RichTextBox.Size = new System.Drawing.Size(458, 164);
            this.richTextBoxExtended1.RichTextBox.TabIndex = 1;
            this.richTextBoxExtended1.RichTextBox.GotFocus += new System.EventHandler(this.richTextBoxExtended1_RichTextBox_GotFocus);
            this.richTextBoxExtended1.ShowBold = true;
            this.richTextBoxExtended1.ShowCenterJustify = true;
            this.richTextBoxExtended1.ShowColors = true;
            this.richTextBoxExtended1.ShowCopy = false;
            this.richTextBoxExtended1.ShowCut = false;
            this.richTextBoxExtended1.ShowFont = true;
            this.richTextBoxExtended1.ShowFontSize = true;
            this.richTextBoxExtended1.ShowItalic = true;
            this.richTextBoxExtended1.ShowLeftJustify = true;
            this.richTextBoxExtended1.ShowOpen = true;
            this.richTextBoxExtended1.ShowPaste = false;
            this.richTextBoxExtended1.ShowRedo = true;
            this.richTextBoxExtended1.ShowRightJustify = true;
            this.richTextBoxExtended1.ShowSave = false;
            this.richTextBoxExtended1.ShowStamp = false;
            this.richTextBoxExtended1.ShowStrikeout = false;
            this.richTextBoxExtended1.ShowToolBarText = true;
            this.richTextBoxExtended1.ShowUnderline = true;
            this.richTextBoxExtended1.ShowUndo = true;
            this.richTextBoxExtended1.Size = new System.Drawing.Size(458, 189);
            this.richTextBoxExtended1.StampAction = StampActions.EditedBy;
            this.richTextBoxExtended1.StampColor = System.Drawing.Color.Blue;
            this.richTextBoxExtended1.TabIndex = 0;
            // 
            // 
            // 
            this.richTextBoxExtended1.Toolbar.Location = new System.Drawing.Point(0, 0);
            this.richTextBoxExtended1.Toolbar.Name = "tb1";
            this.richTextBoxExtended1.Toolbar.Size = new System.Drawing.Size(458, 25);
            this.richTextBoxExtended1.Toolbar.TabIndex = 0;
            this.richTextBoxExtended1.Toolbar.TabStop = true;
            // 
            // checkedListBoxDepartmentTypes
            // 
            this.checkedListBoxDepartmentTypes.CheckOnClick = true;
            this.checkedListBoxDepartmentTypes.FormattingEnabled = true;
            this.checkedListBoxDepartmentTypes.IntegralHeight = false;
            this.checkedListBoxDepartmentTypes.Location = new System.Drawing.Point(7, 110);
            this.checkedListBoxDepartmentTypes.Name = "checkedListBoxDepartmentTypes";
            this.checkedListBoxDepartmentTypes.Size = new System.Drawing.Size(458, 78);
            this.checkedListBoxDepartmentTypes.TabIndex = 0;
            this.checkedListBoxDepartmentTypes.GotFocus += new System.EventHandler(this.textBoxName_GotFocus);
            // 
            // checkBoxNoEmergency
            // 
            this.checkBoxNoEmergency.ForeColor = System.Drawing.SystemColors.ControlText;
            this.checkBoxNoEmergency.Location = new System.Drawing.Point(239, 50);
            this.checkBoxNoEmergency.Name = "checkBoxNoEmergency";
            this.checkBoxNoEmergency.Size = new System.Drawing.Size(226, 20);
            this.checkBoxNoEmergency.TabIndex = 4;
            this.checkBoxNoEmergency.Text = "No  emergencia";
            this.checkBoxNoEmergency.UseVisualStyleBackColor = true;
            this.checkBoxNoEmergency.CheckedChanged += new System.EventHandler(this.checkBoxNoEmergency_CheckedChanged);
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOk.Appearance.Options.UseFont = true;
            this.buttonOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOk.Enabled = false;
            this.buttonOk.Location = new System.Drawing.Point(302, 544);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(82, 32);
            this.buttonOk.StyleController = this.layoutControlIncidentType;
            this.buttonOk.TabIndex = 5;
            this.buttonOk.Text = "Accept";
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // textBoxName
            // 
            this.textBoxName.AllowsLetters = true;
            this.textBoxName.AllowsNumbers = true;
            this.textBoxName.AllowsPunctuation = true;
            this.textBoxName.AllowsSeparators = true;
            this.textBoxName.AllowsSymbols = true;
            this.textBoxName.AllowsWhiteSpaces = true;
            this.textBoxName.ExtraAllowedChars = "";
            this.textBoxName.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxName.Location = new System.Drawing.Point(147, 26);
            this.textBoxName.MaxLength = 40;
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.NonAllowedCharacters = "";
            this.textBoxName.RegularExpresion = "";
            this.textBoxName.Size = new System.Drawing.Size(318, 20);
            this.textBoxName.TabIndex = 1;
            this.textBoxName.TextChanged += new System.EventHandler(this.IncidentTypeParameters_Change);
            this.textBoxName.GotFocus += new System.EventHandler(this.textBoxName_GotFocus);
            // 
            // textBoxExMnemonic
            // 
            this.textBoxExMnemonic.AllowsLetters = true;
            this.textBoxExMnemonic.AllowsNumbers = true;
            this.textBoxExMnemonic.AllowsPunctuation = true;
            this.textBoxExMnemonic.AllowsSeparators = true;
            this.textBoxExMnemonic.AllowsSymbols = true;
            this.textBoxExMnemonic.AllowsWhiteSpaces = false;
            this.textBoxExMnemonic.ExtraAllowedChars = "";
            this.textBoxExMnemonic.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExMnemonic.Location = new System.Drawing.Point(147, 50);
            this.textBoxExMnemonic.MaxLength = 3;
            this.textBoxExMnemonic.Name = "textBoxExMnemonic";
            this.textBoxExMnemonic.NonAllowedCharacters = "";
            this.textBoxExMnemonic.RegularExpresion = "";
            this.textBoxExMnemonic.Size = new System.Drawing.Size(88, 27);
            this.textBoxExMnemonic.TabIndex = 3;
            this.textBoxExMnemonic.TextChanged += new System.EventHandler(this.IncidentTypeParameters_Change);
            this.textBoxExMnemonic.GotFocus += new System.EventHandler(this.textBoxName_GotFocus);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupIncidentType,
            this.layoutControlGroupDepartment,
            this.layoutControlGroupFirstLevel,
            this.layoutControlGroupIcon,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.emptySpaceItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(472, 578);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroupIncidentType
            // 
            this.layoutControlGroupIncidentType.AppearanceGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupIncidentType.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroupIncidentType.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupIncidentType.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroupIncidentType.CustomizationFormText = "layoutControlGroupIncidentType";
            this.layoutControlGroupIncidentType.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemName,
            this.layoutControlItemMnemonico,
            this.layoutControlItemEmergency});
            this.layoutControlGroupIncidentType.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupIncidentType.Name = "layoutControlGroupIncidentType";
            this.layoutControlGroupIncidentType.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupIncidentType.Size = new System.Drawing.Size(472, 84);
            this.layoutControlGroupIncidentType.Text = "layoutControlGroupIncidentType";
            // 
            // layoutControlItemName
            // 
            this.layoutControlItemName.Control = this.textBoxName;
            this.layoutControlItemName.CustomizationFormText = "layoutControlItemMnemonico";
            this.layoutControlItemName.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemName.Name = "layoutControlItemName";
            this.layoutControlItemName.Size = new System.Drawing.Size(462, 24);
            this.layoutControlItemName.Text = "layoutControlItemName";
            this.layoutControlItemName.TextSize = new System.Drawing.Size(136, 13);
            // 
            // layoutControlItemMnemonico
            // 
            this.layoutControlItemMnemonico.Control = this.textBoxExMnemonic;
            this.layoutControlItemMnemonico.CustomizationFormText = "layoutControlItemName";
            this.layoutControlItemMnemonico.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItemMnemonico.MaxSize = new System.Drawing.Size(232, 31);
            this.layoutControlItemMnemonico.MinSize = new System.Drawing.Size(232, 31);
            this.layoutControlItemMnemonico.Name = "layoutControlItemMnemonico";
            this.layoutControlItemMnemonico.Size = new System.Drawing.Size(232, 31);
            this.layoutControlItemMnemonico.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemMnemonico.Text = "layoutControlItemMnemonico";
            this.layoutControlItemMnemonico.TextSize = new System.Drawing.Size(136, 13);
            // 
            // layoutControlItemEmergency
            // 
            this.layoutControlItemEmergency.Control = this.checkBoxNoEmergency;
            this.layoutControlItemEmergency.CustomizationFormText = "layoutControlItemEmergency";
            this.layoutControlItemEmergency.Location = new System.Drawing.Point(232, 24);
            this.layoutControlItemEmergency.Name = "layoutControlItemEmergency";
            this.layoutControlItemEmergency.Size = new System.Drawing.Size(230, 31);
            this.layoutControlItemEmergency.Text = "layoutControlItemEmergency";
            this.layoutControlItemEmergency.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemEmergency.TextToControlDistance = 0;
            this.layoutControlItemEmergency.TextVisible = false;
            // 
            // layoutControlGroupDepartment
            // 
            this.layoutControlGroupDepartment.AppearanceGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupDepartment.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroupDepartment.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupDepartment.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroupDepartment.CustomizationFormText = "layoutControlGroupDepartment";
            this.layoutControlGroupDepartment.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemDepartment});
            this.layoutControlGroupDepartment.Location = new System.Drawing.Point(0, 84);
            this.layoutControlGroupDepartment.Name = "layoutControlGroupDepartment";
            this.layoutControlGroupDepartment.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupDepartment.Size = new System.Drawing.Size(472, 111);
            this.layoutControlGroupDepartment.Text = "layoutControlGroupDepartment";
            // 
            // layoutControlItemDepartment
            // 
            this.layoutControlItemDepartment.Control = this.checkedListBoxDepartmentTypes;
            this.layoutControlItemDepartment.CustomizationFormText = "layoutControlItemDepartment";
            this.layoutControlItemDepartment.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemDepartment.Name = "layoutControlItemDepartment";
            this.layoutControlItemDepartment.Size = new System.Drawing.Size(462, 82);
            this.layoutControlItemDepartment.Text = "layoutControlItemDepartment";
            this.layoutControlItemDepartment.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemDepartment.TextToControlDistance = 0;
            this.layoutControlItemDepartment.TextVisible = false;
            // 
            // layoutControlGroupFirstLevel
            // 
            this.layoutControlGroupFirstLevel.AppearanceGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupFirstLevel.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroupFirstLevel.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupFirstLevel.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroupFirstLevel.CustomizationFormText = "layoutControlGroupFirstLevel";
            this.layoutControlGroupFirstLevel.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroupFirstLevel.Location = new System.Drawing.Point(0, 195);
            this.layoutControlGroupFirstLevel.Name = "layoutControlGroupFirstLevel";
            this.layoutControlGroupFirstLevel.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupFirstLevel.Size = new System.Drawing.Size(472, 222);
            this.layoutControlGroupFirstLevel.Text = "layoutControlGroupFirstLevel";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.richTextBoxExtended1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(462, 193);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroupIcon
            // 
            this.layoutControlGroupIcon.AppearanceGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupIcon.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroupIcon.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupIcon.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroupIcon.CustomizationFormText = "layoutControlGroupIcon";
            this.layoutControlGroupIcon.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlGroupPreview,
            this.emptySpaceItem2,
            this.emptySpaceItem3});
            this.layoutControlGroupIcon.Location = new System.Drawing.Point(0, 417);
            this.layoutControlGroupIcon.Name = "layoutControlGroupIcon";
            this.layoutControlGroupIcon.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupIcon.Size = new System.Drawing.Size(472, 125);
            this.layoutControlGroupIcon.Text = "layoutControlGroupIcon";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.buttonExIcon;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(95, 42);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(95, 42);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(95, 42);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlGroupPreview
            // 
            this.layoutControlGroupPreview.AppearanceGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupPreview.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroupPreview.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupPreview.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroupPreview.CustomizationFormText = "layoutControlGroupPreview";
            this.layoutControlGroupPreview.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.layoutControlGroupPreview.Location = new System.Drawing.Point(95, 0);
            this.layoutControlGroupPreview.Name = "layoutControlGroupPreview";
            this.layoutControlGroupPreview.Size = new System.Drawing.Size(367, 96);
            this.layoutControlGroupPreview.Text = "layoutControlGroupPreview";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.iconPictureBox;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(343, 53);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(95, 26);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 68);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(95, 28);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.buttonOk;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(300, 542);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.buttonCancel;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(386, 542);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 542);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(300, 36);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // openFileDialogMain
            // 
            this.openFileDialogMain.Filter = "Audio Files  |*.wma; *.mp2;*.mp3;*.wav";
            this.openFileDialogMain.Title = "Abrir Archivo";
            // 
            // IncidentTypeForm
            // 
            this.AcceptButton = this.buttonOk;
            this.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(472, 578);
            this.Controls.Add(this.layoutControlIncidentType);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(480, 605);
            this.Name = "IncidentTypeForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "IncidentTypeForm";
            this.Load += new System.EventHandler(this.IncidentTypeForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlIncidentType)).EndInit();
            this.layoutControlIncidentType.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.iconPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupIncidentType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemMnemonico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemEmergency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupDepartment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupFirstLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupPreview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton buttonCancel;
        private DevExpress.XtraEditors.SimpleButton buttonOk;
        private System.Windows.Forms.CheckBox checkBoxNoEmergency;
        private TextBoxEx textBoxExMnemonic;
        private TextBoxEx textBoxName;
        private CheckedListBoxEx checkedListBoxDepartmentTypes;
        private RichTextBoxExtended richTextBoxExtended1;
        private System.Windows.Forms.OpenFileDialog openFileDialogMain;
        private System.Windows.Forms.PictureBox iconPictureBox;
        private DevExpress.XtraEditors.SimpleButton buttonExIcon;
        private System.Windows.Forms.ToolTip toolTipIncidentType;
        private DevExpress.XtraLayout.LayoutControl layoutControlIncidentType;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupIncidentType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemMnemonico;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemEmergency;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupDepartment;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDepartment;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupFirstLevel;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupIcon;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupPreview;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
    }
}
