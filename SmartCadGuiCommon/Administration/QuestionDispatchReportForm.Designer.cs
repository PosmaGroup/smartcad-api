namespace SmartCadGuiCommon
{
    partial class QuestionDispatchReportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControlMain = new DevExpress.XtraLayout.LayoutControl();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.textEditName = new DevExpress.XtraEditors.TextEdit();
            this.simpleButtonOk = new DevExpress.XtraEditors.SimpleButton();
            this.designerQuestionDevX = new DesignerQuestionDevX();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupName = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupAnswers = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemDesigner = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemButtonOk = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemButtonCancel = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlMain)).BeginInit();
            this.layoutControlMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupAnswers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDesigner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemButtonOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemButtonCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControlMain
            // 
            this.layoutControlMain.AllowCustomizationMenu = false;
            this.layoutControlMain.Controls.Add(this.simpleButtonCancel);
            this.layoutControlMain.Controls.Add(this.textEditName);
            this.layoutControlMain.Controls.Add(this.simpleButtonOk);
            this.layoutControlMain.Controls.Add(this.designerQuestionDevX);
            this.layoutControlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlMain.Location = new System.Drawing.Point(0, 0);
            this.layoutControlMain.Name = "layoutControlMain";
            this.layoutControlMain.Root = this.layoutControlGroup1;
            this.layoutControlMain.Size = new System.Drawing.Size(672, 481);
            this.layoutControlMain.TabIndex = 0;
            this.layoutControlMain.Text = "layoutControl1";
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(588, 447);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(82, 32);
            this.simpleButtonCancel.StyleController = this.layoutControlMain;
            this.simpleButtonCancel.TabIndex = 7;
            this.simpleButtonCancel.Text = "simpleButton2";
            // 
            // designerQuestionDevX
            // 
            this.designerQuestionDevX.Location = new System.Drawing.Point(7, 81);
            this.designerQuestionDevX.Name = "designerQuestionDevX";
            this.designerQuestionDevX.Padding = new System.Windows.Forms.Padding(2);
            this.designerQuestionDevX.Size = new System.Drawing.Size(658, 357);
            this.designerQuestionDevX.TabIndex = 5;
            // 
            // textEditName
            // 
            this.textEditName.Location = new System.Drawing.Point(125, 27);
            this.textEditName.Name = "textEditName";
            this.textEditName.Size = new System.Drawing.Size(540, 20);
            this.textEditName.StyleController = this.layoutControlMain;
            this.textEditName.TabIndex = 4;
            this.textEditName.EditValueChanged += new System.EventHandler(this.CheckButtonsEventHandler);
            // 
            // simpleButtonOk
            // 
            this.simpleButtonOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButtonOk.Location = new System.Drawing.Point(502, 447);
            this.simpleButtonOk.MinimumSize = new System.Drawing.Size(75, 25);
            this.simpleButtonOk.Name = "simpleButtonOk";
            this.simpleButtonOk.Size = new System.Drawing.Size(82, 32);
            this.simpleButtonOk.StyleController = this.layoutControlMain;
            this.simpleButtonOk.TabIndex = 6;
            this.simpleButtonOk.Text = "simpleButton1";
            this.simpleButtonOk.Click += new System.EventHandler(this.simpleButtonAccept_Click);
            // 
            // designerQuestionDevX
            // 
            this.designerQuestionDevX.Location = new System.Drawing.Point(24, 112);
            this.designerQuestionDevX.Name = "designerQuestionDevX";
            this.designerQuestionDevX.Size = new System.Drawing.Size(624, 309);
            this.designerQuestionDevX.TabIndex = 5;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupName,
            this.layoutControlGroupAnswers,
            this.layoutControlItemButtonOk,
            this.layoutControlItemButtonCancel,
            this.emptySpaceItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(672, 481);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroupName
            // 
            this.layoutControlGroupName.CustomizationFormText = "layoutControlGroupName";
            this.layoutControlGroupName.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemName});
            this.layoutControlGroupName.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupName.Name = "layoutControlGroupName";
            this.layoutControlGroupName.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupName.Size = new System.Drawing.Size(672, 54);
            this.layoutControlGroupName.Text = "layoutControlGroupName";
            // 
            // layoutControlItemName
            // 
            this.layoutControlItemName.Control = this.textEditName;
            this.layoutControlItemName.CustomizationFormText = "layoutControlItemName";
            this.layoutControlItemName.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemName.Name = "layoutControlItemName";
            this.layoutControlItemName.Size = new System.Drawing.Size(662, 24);
            this.layoutControlItemName.Text = "layoutControlItemName";
            this.layoutControlItemName.TextSize = new System.Drawing.Size(114, 13);
            // 
            // layoutControlGroupAnswers
            // 
            this.layoutControlGroupAnswers.CustomizationFormText = "layoutControlGroupAnswers";
            this.layoutControlGroupAnswers.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemDesigner});
            this.layoutControlGroupAnswers.Location = new System.Drawing.Point(0, 54);
            this.layoutControlGroupAnswers.Name = "layoutControlGroupAnswers";
            this.layoutControlGroupAnswers.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupAnswers.Size = new System.Drawing.Size(672, 391);
            this.layoutControlGroupAnswers.Text = "layoutControlGroupAnswers";
            // 
            // layoutControlItemDesigner
            // 
            this.layoutControlItemDesigner.Control = this.designerQuestionDevX;
            this.layoutControlItemDesigner.CustomizationFormText = "layoutControlItemDesigner";
            this.layoutControlItemDesigner.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemDesigner.Name = "layoutControlItemDesigner";
            this.layoutControlItemDesigner.Size = new System.Drawing.Size(662, 361);
            this.layoutControlItemDesigner.Text = "layoutControlItemDesigner";
            this.layoutControlItemDesigner.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemDesigner.TextToControlDistance = 0;
            this.layoutControlItemDesigner.TextVisible = false;
            // 
            // layoutControlItemButtonOk
            // 
            this.layoutControlItemButtonOk.Control = this.simpleButtonOk;
            this.layoutControlItemButtonOk.CustomizationFormText = "layoutControlItemButtonOk";
            this.layoutControlItemButtonOk.Location = new System.Drawing.Point(500, 445);
            this.layoutControlItemButtonOk.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItemButtonOk.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItemButtonOk.Name = "layoutControlItemButtonOk";
            this.layoutControlItemButtonOk.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItemButtonOk.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemButtonOk.Text = "layoutControlItemButtonOk";
            this.layoutControlItemButtonOk.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemButtonOk.TextToControlDistance = 0;
            this.layoutControlItemButtonOk.TextVisible = false;
            // 
            // layoutControlItemButtonCancel
            // 
            this.layoutControlItemButtonCancel.Control = this.simpleButtonCancel;
            this.layoutControlItemButtonCancel.CustomizationFormText = "layoutControlItemButtonCancel";
            this.layoutControlItemButtonCancel.Location = new System.Drawing.Point(586, 445);
            this.layoutControlItemButtonCancel.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItemButtonCancel.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItemButtonCancel.Name = "layoutControlItemButtonCancel";
            this.layoutControlItemButtonCancel.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItemButtonCancel.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemButtonCancel.Text = "layoutControlItemButtonCancel";
            this.layoutControlItemButtonCancel.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemButtonCancel.TextToControlDistance = 0;
            this.layoutControlItemButtonCancel.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 445);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(500, 36);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // QuestionDispatchReportForm
            // 
            this.AcceptButton = this.simpleButtonOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonCancel;
            this.ClientSize = new System.Drawing.Size(672, 481);
            this.Controls.Add(this.layoutControlMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(680, 515);
            this.Name = "QuestionDispatchReportForm";
            this.Text = "QuestionsDispatchReportForm";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlMain)).EndInit();
            this.layoutControlMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupAnswers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDesigner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemButtonOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemButtonCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControlMain;
        private DevExpress.XtraEditors.TextEdit textEditName;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemName;
        private DesignerQuestionDevX designerQuestionDevX;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupAnswers;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDesigner;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private DevExpress.XtraEditors.SimpleButton simpleButtonOk;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemButtonCancel;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemButtonOk;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;

    }
}