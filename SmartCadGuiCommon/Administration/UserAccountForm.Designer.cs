﻿using System;
using System.Collections.Generic;
using System.Text;
using DevExpress.XtraEditors;
using SmartCadControls.Controls;

namespace Smartmatic.SmartCad.Gui
{
    partial class UserAccountForm : XtraForm
    {
        private DevExpress.XtraEditors.SimpleButton buttonCancel;
        private DevExpress.XtraEditors.SimpleButton buttonOk;
        private LabelEx labelRepeatPassword;
        private LabelEx labelPassword;
        private TextBoxEx textBoxPassword;
        private TextBoxEx textBoxRepeatPassword;
        private System.Windows.Forms.OpenFileDialog importUserAccountImage;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserAccountForm));
            this.buttonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.textBoxRepeatPassword = new TextBoxEx();
            this.textBoxPassword = new TextBoxEx();
            this.buttonOk = new DevExpress.XtraEditors.SimpleButton();
            this.textBoxExAgentPassword = new TextBoxEx();
            this.textBoxLastName = new TextBoxEx();
            this.dateTimePicker1 = new DateTimePickerEx();
            this.textBoxAddress = new TextBoxEx();
            this.textBoxPhone = new TextBoxEx();
            this.textBoxEMail = new TextBoxEx();
            this.textBoxIdNumber = new TextBoxEx();
            this.textBoxName = new TextBoxEx();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.userAccountPictureBox = new System.Windows.Forms.PictureBox();
            this.removePictureButton = new DevExpress.XtraEditors.SimpleButton();
            this.imageImportButton = new DevExpress.XtraEditors.SimpleButton();
            this.comboBoxExStruct = new ComboBoxEx();
            this.buttonExAddCamera = new DevExpress.XtraEditors.SimpleButton();
            this.listBoxExUnAvailablesCameras = new ListBoxEx();
            this.comboBoxCctvZone = new ComboBoxEx();
            this.buttonExRemoveAllCameras = new DevExpress.XtraEditors.SimpleButton();
            this.buttonExAddAllCameras = new DevExpress.XtraEditors.SimpleButton();
            this.buttonExRemoveCamera = new DevExpress.XtraEditors.SimpleButton();
            this.listBoxExAvailablesCameras = new ListBoxEx();
            this.comboBoxExStructType = new ComboBoxEx();
            this.textBoxLogin = new TextBoxEx();
            this.comboBoxRole = new ComboBoxEx();
            this.textBoxExAgentID = new TextBoxEx();
            this.checkedListBoxDepartamentos = new CheckedListBoxEx();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlAccountDescription = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupAccount = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlIdentifier = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlRole = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlAgentID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlDepartaments = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemPassword = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemRepeatPassword = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlAgentPassword = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlUserData = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupUserData = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlLastName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlPersonID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlDateBirth = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlPhone = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlEmail = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlAddress = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlCameras = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupCameras = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlCCTVZone = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlStruct = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlStructType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlCamerasAvailable = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlCameraAsociated = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.importUserAccountImage = new System.Windows.Forms.OpenFileDialog();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.checkBoxExWindowsAuthentication = new CheckBoxEx();
            this.labelRepeatPassword = new LabelEx();
            this.labelPassword = new LabelEx();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userAccountPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlAccountDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlIdentifier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlRole)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlAgentID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlDepartaments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemRepeatPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlAgentPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlUserData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupUserData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlLastName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlPersonID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlDateBirth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCameras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupCameras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCCTVZone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlStruct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlStructType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCamerasAvailable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCameraAsociated)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCancel.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonCancel.Appearance.Options.UseFont = true;
            this.buttonCancel.Appearance.Options.UseForeColor = true;
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(369, 358);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(82, 32);
            this.buttonCancel.StyleController = this.layoutControl1;
            this.buttonCancel.TabIndex = 3;
            this.buttonCancel.Text = "Cancelar  ";
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowCustomizationMenu = false;
            this.layoutControl1.Controls.Add(this.buttonCancel);
            this.layoutControl1.Controls.Add(this.textBoxRepeatPassword);
            this.layoutControl1.Controls.Add(this.textBoxPassword);
            this.layoutControl1.Controls.Add(this.buttonOk);
            this.layoutControl1.Controls.Add(this.textBoxExAgentPassword);
            this.layoutControl1.Controls.Add(this.textBoxLogin);
            this.layoutControl1.Controls.Add(this.comboBoxRole);
            this.layoutControl1.Controls.Add(this.textBoxExAgentID);
            this.layoutControl1.Controls.Add(this.checkedListBoxDepartamentos);
            this.layoutControl1.Controls.Add(this.textBoxLastName);
            this.layoutControl1.Controls.Add(this.dateTimePicker1);
            this.layoutControl1.Controls.Add(this.textBoxAddress);
            this.layoutControl1.Controls.Add(this.textBoxPhone);
            this.layoutControl1.Controls.Add(this.textBoxEMail);
            this.layoutControl1.Controls.Add(this.textBoxIdNumber);
            this.layoutControl1.Controls.Add(this.textBoxName);
            this.layoutControl1.Controls.Add(this.panelControl1);
            this.layoutControl1.Controls.Add(this.comboBoxExStruct);
            this.layoutControl1.Controls.Add(this.buttonExAddCamera);
            this.layoutControl1.Controls.Add(this.listBoxExUnAvailablesCameras);
            this.layoutControl1.Controls.Add(this.comboBoxCctvZone);
            this.layoutControl1.Controls.Add(this.buttonExRemoveAllCameras);
            this.layoutControl1.Controls.Add(this.buttonExAddAllCameras);
            this.layoutControl1.Controls.Add(this.buttonExRemoveCamera);
            this.layoutControl1.Controls.Add(this.listBoxExAvailablesCameras);
            this.layoutControl1.Controls.Add(this.comboBoxExStructType);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(621, 216, 250, 350);
            this.layoutControl1.OptionsCustomizationForm.EnableUndoManager = false;
            this.layoutControl1.OptionsCustomizationForm.ShowLayoutTreeView = false;
            this.layoutControl1.OptionsCustomizationForm.ShowLoadButton = false;
            this.layoutControl1.OptionsCustomizationForm.ShowRedoButton = false;
            this.layoutControl1.OptionsCustomizationForm.ShowSaveButton = false;
            this.layoutControl1.OptionsCustomizationForm.ShowUndoButton = false;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(453, 392);
            this.layoutControl1.TabIndex = 5;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // textBoxRepeatPassword
            // 
            this.textBoxRepeatPassword.AllowsLetters = true;
            this.textBoxRepeatPassword.AllowsNumbers = true;
            this.textBoxRepeatPassword.AllowsPunctuation = true;
            this.textBoxRepeatPassword.AllowsSeparators = true;
            this.textBoxRepeatPassword.AllowsSymbols = true;
            this.textBoxRepeatPassword.AllowsWhiteSpaces = true;
            this.textBoxRepeatPassword.Enabled = false;
            this.textBoxRepeatPassword.ExtraAllowedChars = "";
            this.textBoxRepeatPassword.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxRepeatPassword.Location = new System.Drawing.Point(198, 114);
            this.textBoxRepeatPassword.MaxLength = 25;
            this.textBoxRepeatPassword.Name = "textBoxRepeatPassword";
            this.textBoxRepeatPassword.NonAllowedCharacters = "\'";
            this.textBoxRepeatPassword.PasswordChar = '*';
            this.textBoxRepeatPassword.RegularExpresion = "";
            this.textBoxRepeatPassword.Size = new System.Drawing.Size(229, 20);
            this.textBoxRepeatPassword.TabIndex = 5;
            this.textBoxRepeatPassword.TextChanged += new System.EventHandler(this.UserAccountParameters_Change);
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.AllowsLetters = true;
            this.textBoxPassword.AllowsNumbers = true;
            this.textBoxPassword.AllowsPunctuation = true;
            this.textBoxPassword.AllowsSeparators = true;
            this.textBoxPassword.AllowsSymbols = true;
            this.textBoxPassword.AllowsWhiteSpaces = true;
            this.textBoxPassword.ExtraAllowedChars = "";
            this.textBoxPassword.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxPassword.Location = new System.Drawing.Point(198, 90);
            this.textBoxPassword.MaxLength = 25;
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.NonAllowedCharacters = "\'";
            this.textBoxPassword.PasswordChar = '*';
            this.textBoxPassword.RegularExpresion = "";
            this.textBoxPassword.Size = new System.Drawing.Size(229, 20);
            this.textBoxPassword.TabIndex = 3;
            this.textBoxPassword.TextChanged += new System.EventHandler(this.UserAccountParameters_Change);
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOk.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonOk.Appearance.Options.UseFont = true;
            this.buttonOk.Appearance.Options.UseForeColor = true;
            this.buttonOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOk.Enabled = false;
            this.buttonOk.Location = new System.Drawing.Point(283, 358);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(82, 32);
            this.buttonOk.StyleController = this.layoutControl1;
            this.buttonOk.TabIndex = 2;
            this.buttonOk.Text = "Accept";
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // textBoxExAgentPassword
            // 
            this.textBoxExAgentPassword.AllowsLetters = true;
            this.textBoxExAgentPassword.AllowsNumbers = true;
            this.textBoxExAgentPassword.AllowsPunctuation = true;
            this.textBoxExAgentPassword.AllowsSeparators = true;
            this.textBoxExAgentPassword.AllowsSymbols = true;
            this.textBoxExAgentPassword.AllowsWhiteSpaces = false;
            this.textBoxExAgentPassword.ExtraAllowedChars = "";
            this.textBoxExAgentPassword.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExAgentPassword.Location = new System.Drawing.Point(198, 187);
            this.textBoxExAgentPassword.Name = "textBoxExAgentPassword";
            this.textBoxExAgentPassword.NonAllowedCharacters = "\'";
            this.textBoxExAgentPassword.PasswordChar = '*';
            this.textBoxExAgentPassword.RegularExpresion = "";
            this.textBoxExAgentPassword.Size = new System.Drawing.Size(229, 20);
            this.textBoxExAgentPassword.TabIndex = 9;
            // 
            // textBoxLastName
            // 
            this.textBoxLastName.AllowsLetters = true;
            this.textBoxLastName.AllowsNumbers = true;
            this.textBoxLastName.AllowsPunctuation = true;
            this.textBoxLastName.AllowsSeparators = true;
            this.textBoxLastName.AllowsSymbols = true;
            this.textBoxLastName.AllowsWhiteSpaces = true;
            this.textBoxLastName.ExtraAllowedChars = "";
            this.textBoxLastName.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxLastName.Location = new System.Drawing.Point(198, 90);
            this.textBoxLastName.MaxLength = 25;
            this.textBoxLastName.Name = "textBoxLastName";
            this.textBoxLastName.NonAllowedCharacters = "";
            this.textBoxLastName.RegularExpresion = "";
            this.textBoxLastName.Size = new System.Drawing.Size(108, 20);
            this.textBoxLastName.TabIndex = 2;
            this.textBoxLastName.TextChanged += new System.EventHandler(this.UserAccountParameters_Change);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(198, 138);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.ShowUpDown = true;
            this.dateTimePicker1.Size = new System.Drawing.Size(108, 20);
            this.dateTimePicker1.TabIndex = 4;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.UserAccountParameters_Change);
            // 
            // textBoxAddress
            // 
            this.textBoxAddress.AcceptsReturn = true;
            this.textBoxAddress.AllowsLetters = true;
            this.textBoxAddress.AllowsNumbers = true;
            this.textBoxAddress.AllowsPunctuation = true;
            this.textBoxAddress.AllowsSeparators = true;
            this.textBoxAddress.AllowsSymbols = true;
            this.textBoxAddress.AllowsWhiteSpaces = true;
            this.textBoxAddress.ExtraAllowedChars = "";
            this.textBoxAddress.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxAddress.Location = new System.Drawing.Point(198, 252);
            this.textBoxAddress.MaxLength = 200;
            this.textBoxAddress.Multiline = true;
            this.textBoxAddress.Name = "textBoxAddress";
            this.textBoxAddress.NonAllowedCharacters = "";
            this.textBoxAddress.RegularExpresion = "";
            this.textBoxAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxAddress.Size = new System.Drawing.Size(229, 52);
            this.textBoxAddress.TabIndex = 7;
            // 
            // textBoxPhone
            // 
            this.textBoxPhone.AllowsLetters = false;
            this.textBoxPhone.AllowsNumbers = true;
            this.textBoxPhone.AllowsPunctuation = false;
            this.textBoxPhone.AllowsSeparators = false;
            this.textBoxPhone.AllowsSymbols = false;
            this.textBoxPhone.AllowsWhiteSpaces = false;
            this.textBoxPhone.ExtraAllowedChars = "";
            this.textBoxPhone.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxPhone.Location = new System.Drawing.Point(198, 162);
            this.textBoxPhone.MaxLength = 17;
            this.textBoxPhone.Name = "textBoxPhone";
            this.textBoxPhone.NonAllowedCharacters = "\'";
            this.textBoxPhone.RegularExpresion = "";
            this.textBoxPhone.Size = new System.Drawing.Size(108, 20);
            this.textBoxPhone.TabIndex = 5;
            // 
            // textBoxEMail
            // 
            this.textBoxEMail.AllowsLetters = true;
            this.textBoxEMail.AllowsNumbers = true;
            this.textBoxEMail.AllowsPunctuation = false;
            this.textBoxEMail.AllowsSeparators = false;
            this.textBoxEMail.AllowsSymbols = false;
            this.textBoxEMail.AllowsWhiteSpaces = false;
            this.textBoxEMail.ExtraAllowedChars = "_.@-";
            this.textBoxEMail.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxEMail.Location = new System.Drawing.Point(198, 186);
            this.textBoxEMail.MaxLength = 40;
            this.textBoxEMail.Name = "textBoxEMail";
            this.textBoxEMail.NonAllowedCharacters = " \'\"´`=!|%&¬/()¡?^[]+*¨{},:><";
            this.textBoxEMail.RegularExpresion = "^(([^<>()[\\]\\\\.,;:\\s@\\\"\"]+(\\.[^<>()[\\]\\\\.,;:\\s@\\\"\"]+)*)|(\\\"\".+\\\"\"))((\\[[0-9]{1,3}" +
                "\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
            this.textBoxEMail.Size = new System.Drawing.Size(108, 20);
            this.textBoxEMail.TabIndex = 6;
            // 
            // textBoxIdNumber
            // 
            this.textBoxIdNumber.AllowsLetters = false;
            this.textBoxIdNumber.AllowsNumbers = true;
            this.textBoxIdNumber.AllowsPunctuation = false;
            this.textBoxIdNumber.AllowsSeparators = false;
            this.textBoxIdNumber.AllowsSymbols = false;
            this.textBoxIdNumber.AllowsWhiteSpaces = false;
            this.textBoxIdNumber.ExtraAllowedChars = "";
            this.textBoxIdNumber.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxIdNumber.Location = new System.Drawing.Point(198, 114);
            this.textBoxIdNumber.MaxLength = 12;
            this.textBoxIdNumber.Name = "textBoxIdNumber";
            this.textBoxIdNumber.NonAllowedCharacters = "\'";
            this.textBoxIdNumber.RegularExpresion = "";
            this.textBoxIdNumber.Size = new System.Drawing.Size(108, 20);
            this.textBoxIdNumber.TabIndex = 3;
            this.textBoxIdNumber.TextChanged += new System.EventHandler(this.UserAccountParameters_Change);
            // 
            // textBoxName
            // 
            this.textBoxName.AllowsLetters = true;
            this.textBoxName.AllowsNumbers = true;
            this.textBoxName.AllowsPunctuation = true;
            this.textBoxName.AllowsSeparators = true;
            this.textBoxName.AllowsSymbols = true;
            this.textBoxName.AllowsWhiteSpaces = true;
            this.textBoxName.ExtraAllowedChars = "";
            this.textBoxName.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxName.Location = new System.Drawing.Point(198, 66);
            this.textBoxName.MaxLength = 25;
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.NonAllowedCharacters = "";
            this.textBoxName.RegularExpresion = "";
            this.textBoxName.Size = new System.Drawing.Size(108, 20);
            this.textBoxName.TabIndex = 1;
            this.textBoxName.TextChanged += new System.EventHandler(this.UserAccountParameters_Change);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.userAccountPictureBox);
            this.panelControl1.Controls.Add(this.removePictureButton);
            this.panelControl1.Controls.Add(this.imageImportButton);
            this.panelControl1.Location = new System.Drawing.Point(310, 66);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(117, 182);
            this.panelControl1.TabIndex = 7;
            // 
            // userAccountPictureBox
            // 
            this.userAccountPictureBox.Location = new System.Drawing.Point(5, 5);
            this.userAccountPictureBox.Name = "userAccountPictureBox";
            this.userAccountPictureBox.Size = new System.Drawing.Size(100, 119);
            this.userAccountPictureBox.TabIndex = 4;
            this.userAccountPictureBox.TabStop = false;
            // 
            // removePictureButton
            // 
            this.removePictureButton.Enabled = false;
            this.removePictureButton.Location = new System.Drawing.Point(5, 130);
            this.removePictureButton.Name = "removePictureButton";
            this.removePictureButton.Size = new System.Drawing.Size(21, 21);
            this.removePictureButton.TabIndex = 9;
            this.removePictureButton.Click += new System.EventHandler(this.removePictureButton_Click);
            this.removePictureButton.MouseHover += new System.EventHandler(this.removePictureButton_MouseHover);
            // 
            // imageImportButton
            // 
            this.imageImportButton.Location = new System.Drawing.Point(84, 130);
            this.imageImportButton.Name = "imageImportButton";
            this.imageImportButton.Size = new System.Drawing.Size(21, 21);
            this.imageImportButton.TabIndex = 8;
            this.imageImportButton.Click += new System.EventHandler(this.butonImageImport_Click);
            this.imageImportButton.MouseHover += new System.EventHandler(this.imageImportButton_MouseHover);
            // 
            // comboBoxExStruct
            // 
            this.comboBoxExStruct.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxExStruct.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.comboBoxExStruct.FormattingEnabled = true;
            this.comboBoxExStruct.Location = new System.Drawing.Point(198, 116);
            this.comboBoxExStruct.Name = "comboBoxExStruct";
            this.comboBoxExStruct.Size = new System.Drawing.Size(229, 21);
            this.comboBoxExStruct.TabIndex = 2;
            this.comboBoxExStruct.SelectedIndexChanged += new System.EventHandler(this.comboBoxExStruct_SelectedIndexChanged);
            // 
            // buttonExAddCamera
            // 
            this.buttonExAddCamera.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExAddCamera.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonExAddCamera.Appearance.Options.UseFont = true;
            this.buttonExAddCamera.Appearance.Options.UseForeColor = true;
            this.buttonExAddCamera.Location = new System.Drawing.Point(207, 176);
            this.buttonExAddCamera.Name = "buttonExAddCamera";
            this.buttonExAddCamera.Size = new System.Drawing.Size(41, 29);
            this.buttonExAddCamera.StyleController = this.layoutControl1;
            this.buttonExAddCamera.TabIndex = 4;
            this.buttonExAddCamera.Text = ">";
            this.buttonExAddCamera.Click += new System.EventHandler(this.buttonExAddCamera_Click);
            // 
            // listBoxExUnAvailablesCameras
            // 
            this.listBoxExUnAvailablesCameras.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.listBoxExUnAvailablesCameras.FormattingEnabled = true;
            this.listBoxExUnAvailablesCameras.IntegralHeight = false;
            this.listBoxExUnAvailablesCameras.Location = new System.Drawing.Point(252, 157);
            this.listBoxExUnAvailablesCameras.Name = "listBoxExUnAvailablesCameras";
            this.listBoxExUnAvailablesCameras.Size = new System.Drawing.Size(175, 173);
            this.listBoxExUnAvailablesCameras.TabIndex = 8;
            this.listBoxExUnAvailablesCameras.DoubleClick += new System.EventHandler(this.listBoxExUnAvailablesCameras_DoubleClick);
            this.listBoxExUnAvailablesCameras.Enter += new System.EventHandler(this.listBoxExUnAvailablesCameras_Enter);
            // 
            // comboBoxCctvZone
            // 
            this.comboBoxCctvZone.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCctvZone.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.comboBoxCctvZone.FormattingEnabled = true;
            this.comboBoxCctvZone.Location = new System.Drawing.Point(198, 66);
            this.comboBoxCctvZone.Name = "comboBoxCctvZone";
            this.comboBoxCctvZone.Size = new System.Drawing.Size(229, 21);
            this.comboBoxCctvZone.Sorted = true;
            this.comboBoxCctvZone.TabIndex = 0;
            this.comboBoxCctvZone.SelectedIndexChanged += new System.EventHandler(this.comboBoxCctvZone_SelectedIndexChanged);
            // 
            // buttonExRemoveAllCameras
            // 
            this.buttonExRemoveAllCameras.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExRemoveAllCameras.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonExRemoveAllCameras.Appearance.Options.UseFont = true;
            this.buttonExRemoveAllCameras.Appearance.Options.UseForeColor = true;
            this.buttonExRemoveAllCameras.Location = new System.Drawing.Point(207, 275);
            this.buttonExRemoveAllCameras.Name = "buttonExRemoveAllCameras";
            this.buttonExRemoveAllCameras.Size = new System.Drawing.Size(41, 29);
            this.buttonExRemoveAllCameras.StyleController = this.layoutControl1;
            this.buttonExRemoveAllCameras.TabIndex = 7;
            this.buttonExRemoveAllCameras.Text = "<<";
            this.buttonExRemoveAllCameras.Click += new System.EventHandler(this.buttonExRemoveAllCameras_Click);
            // 
            // buttonExAddAllCameras
            // 
            this.buttonExAddAllCameras.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExAddAllCameras.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonExAddAllCameras.Appearance.Options.UseFont = true;
            this.buttonExAddAllCameras.Appearance.Options.UseForeColor = true;
            this.buttonExAddAllCameras.Location = new System.Drawing.Point(207, 209);
            this.buttonExAddAllCameras.Name = "buttonExAddAllCameras";
            this.buttonExAddAllCameras.Size = new System.Drawing.Size(41, 29);
            this.buttonExAddAllCameras.StyleController = this.layoutControl1;
            this.buttonExAddAllCameras.TabIndex = 5;
            this.buttonExAddAllCameras.Text = ">>";
            this.buttonExAddAllCameras.Click += new System.EventHandler(this.buttonExAddAllCameras_Click);
            // 
            // buttonExRemoveCamera
            // 
            this.buttonExRemoveCamera.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExRemoveCamera.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonExRemoveCamera.Appearance.Options.UseFont = true;
            this.buttonExRemoveCamera.Appearance.Options.UseForeColor = true;
            this.buttonExRemoveCamera.Location = new System.Drawing.Point(207, 242);
            this.buttonExRemoveCamera.Name = "buttonExRemoveCamera";
            this.buttonExRemoveCamera.Size = new System.Drawing.Size(41, 29);
            this.buttonExRemoveCamera.StyleController = this.layoutControl1;
            this.buttonExRemoveCamera.TabIndex = 6;
            this.buttonExRemoveCamera.Text = "<";
            this.buttonExRemoveCamera.Click += new System.EventHandler(this.buttonExRemoveCamera_Click);
            // 
            // listBoxExAvailablesCameras
            // 
            this.listBoxExAvailablesCameras.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.listBoxExAvailablesCameras.FormattingEnabled = true;
            this.listBoxExAvailablesCameras.IntegralHeight = false;
            this.listBoxExAvailablesCameras.Location = new System.Drawing.Point(26, 157);
            this.listBoxExAvailablesCameras.Name = "listBoxExAvailablesCameras";
            this.listBoxExAvailablesCameras.Size = new System.Drawing.Size(177, 173);
            this.listBoxExAvailablesCameras.TabIndex = 3;
            this.listBoxExAvailablesCameras.DoubleClick += new System.EventHandler(this.listBoxExAvailablesCameras_DoubleClick);
            this.listBoxExAvailablesCameras.Enter += new System.EventHandler(this.listBoxExAvailablesCameras_Enter);
            // 
            // comboBoxExStructType
            // 
            this.comboBoxExStructType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxExStructType.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.comboBoxExStructType.FormattingEnabled = true;
            this.comboBoxExStructType.Location = new System.Drawing.Point(198, 91);
            this.comboBoxExStructType.Name = "comboBoxExStructType";
            this.comboBoxExStructType.Size = new System.Drawing.Size(229, 21);
            this.comboBoxExStructType.TabIndex = 1;
            this.comboBoxExStructType.SelectedIndexChanged += new System.EventHandler(this.comboBoxExStructType_SelectedIndexChanged);
            // 
            // textBoxLogin
            // 
            this.textBoxLogin.AllowsLetters = true;
            this.textBoxLogin.AllowsNumbers = true;
            this.textBoxLogin.AllowsPunctuation = true;
            this.textBoxLogin.AllowsSeparators = true;
            this.textBoxLogin.AllowsSymbols = true;
            this.textBoxLogin.AllowsWhiteSpaces = false;
            this.textBoxLogin.ExtraAllowedChars = "._-";
            this.textBoxLogin.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxLogin.Location = new System.Drawing.Point(198, 66);
            this.textBoxLogin.MaxLength = 50;
            this.textBoxLogin.Name = "textBoxLogin";
            this.textBoxLogin.NonAllowedCharacters = "";
            this.textBoxLogin.RegularExpresion = "";
            this.textBoxLogin.Size = new System.Drawing.Size(229, 20);
            this.textBoxLogin.TabIndex = 0;
            this.textBoxLogin.TextChanged += new System.EventHandler(this.UserAccountParameters_Change);
            // 
            // comboBoxRole
            // 
            this.comboBoxRole.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxRole.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.comboBoxRole.FormattingEnabled = true;
            this.comboBoxRole.Location = new System.Drawing.Point(198, 138);
            this.comboBoxRole.Name = "comboBoxRole";
            this.comboBoxRole.Size = new System.Drawing.Size(229, 21);
            this.comboBoxRole.Sorted = true;
            this.comboBoxRole.TabIndex = 1;
            this.comboBoxRole.SelectedIndexChanged += new System.EventHandler(this.ComboBoxRole_SelectedIndexChanged);
            // 
            // textBoxExAgentID
            // 
            this.textBoxExAgentID.AllowsLetters = false;
            this.textBoxExAgentID.AllowsNumbers = true;
            this.textBoxExAgentID.AllowsPunctuation = false;
            this.textBoxExAgentID.AllowsSeparators = false;
            this.textBoxExAgentID.AllowsSymbols = false;
            this.textBoxExAgentID.AllowsWhiteSpaces = false;
            this.textBoxExAgentID.Enabled = false;
            this.textBoxExAgentID.ExtraAllowedChars = "";
            this.textBoxExAgentID.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExAgentID.Location = new System.Drawing.Point(198, 163);
            this.textBoxExAgentID.MaxLength = 4;
            this.textBoxExAgentID.Name = "textBoxExAgentID";
            this.textBoxExAgentID.NonAllowedCharacters = "\'";
            this.textBoxExAgentID.RegularExpresion = "";
            this.textBoxExAgentID.Size = new System.Drawing.Size(229, 20);
            this.textBoxExAgentID.TabIndex = 2;
            this.textBoxExAgentID.TextChanged += new System.EventHandler(this.UserAccountParameters_Change);
            // 
            // checkedListBoxDepartamentos
            // 
            this.checkedListBoxDepartamentos.CheckOnClick = true;
            this.checkedListBoxDepartamentos.Enabled = false;
            this.checkedListBoxDepartamentos.FormattingEnabled = true;
            this.checkedListBoxDepartamentos.HorizontalScrollbar = true;
            this.checkedListBoxDepartamentos.IntegralHeight = false;
            this.checkedListBoxDepartamentos.Location = new System.Drawing.Point(198, 211);
            this.checkedListBoxDepartamentos.Name = "checkedListBoxDepartamentos";
            this.checkedListBoxDepartamentos.Size = new System.Drawing.Size(229, 119);
            this.checkedListBoxDepartamentos.TabIndex = 3;
            this.checkedListBoxDepartamentos.CheckedItem += new System.Windows.Forms.ItemCheckEventHandler(this.checkedListBoxDepartamentos_CheckedItem);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.emptySpaceItem6});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(453, 392);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlAccountDescription;
            this.tabbedControlGroup1.SelectedTabPageIndex = 1;
            this.tabbedControlGroup1.ShowInCustomizationForm = false;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(453, 356);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlUserData,
            this.layoutControlAccountDescription,
            this.layoutControlCameras});
            this.tabbedControlGroup1.Text = "tabbedControlGroup1";
            // 
            // layoutControlAccountDescription
            // 
            this.layoutControlAccountDescription.CustomizationFormText = "Account";
            this.layoutControlAccountDescription.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupAccount});
            this.layoutControlAccountDescription.Location = new System.Drawing.Point(0, 0);
            this.layoutControlAccountDescription.Name = "layoutControlAccountDescription";
            this.layoutControlAccountDescription.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlAccountDescription.Size = new System.Drawing.Size(429, 312);
            this.layoutControlAccountDescription.Text = "AccountDesc";
            // 
            // layoutControlGroupAccount
            // 
            this.layoutControlGroupAccount.CustomizationFormText = "layoutControlGroup4";
            this.layoutControlGroupAccount.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlIdentifier,
            this.layoutControlRole,
            this.layoutControlAgentID,
            this.layoutControlDepartaments,
            this.layoutControlItemPassword,
            this.layoutControlItemRepeatPassword,
            this.layoutControlAgentPassword});
            this.layoutControlGroupAccount.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupAccount.Name = "layoutControlGroupAccount";
            this.layoutControlGroupAccount.Size = new System.Drawing.Size(429, 312);
            this.layoutControlGroupAccount.Text = "layoutControlGroupAccount";
            // 
            // layoutControlIdentifier
            // 
            this.layoutControlIdentifier.Control = this.textBoxLogin;
            this.layoutControlIdentifier.CustomizationFormText = "layoutControlIdenti";
            this.layoutControlIdentifier.Location = new System.Drawing.Point(0, 0);
            this.layoutControlIdentifier.Name = "layoutControlIdentifier";
            this.layoutControlIdentifier.Size = new System.Drawing.Size(405, 24);
            this.layoutControlIdentifier.Text = "layoutControlIdentifier";
            this.layoutControlIdentifier.TextSize = new System.Drawing.Size(168, 13);
            // 
            // layoutControlRole
            // 
            this.layoutControlRole.Control = this.comboBoxRole;
            this.layoutControlRole.CustomizationFormText = "layoutControlRole";
            this.layoutControlRole.Location = new System.Drawing.Point(0, 72);
            this.layoutControlRole.Name = "layoutControlRole";
            this.layoutControlRole.Size = new System.Drawing.Size(405, 25);
            this.layoutControlRole.Text = "layoutControlRole";
            this.layoutControlRole.TextSize = new System.Drawing.Size(168, 13);
            // 
            // layoutControlAgentID
            // 
            this.layoutControlAgentID.Control = this.textBoxExAgentID;
            this.layoutControlAgentID.CustomizationFormText = "layoutControlAgentID";
            this.layoutControlAgentID.Location = new System.Drawing.Point(0, 97);
            this.layoutControlAgentID.Name = "layoutControlAgentID";
            this.layoutControlAgentID.Size = new System.Drawing.Size(405, 24);
            this.layoutControlAgentID.Text = "layoutControlAgentID";
            this.layoutControlAgentID.TextSize = new System.Drawing.Size(168, 13);
            // 
            // layoutControlDepartaments
            // 
            this.layoutControlDepartaments.Control = this.checkedListBoxDepartamentos;
            this.layoutControlDepartaments.CustomizationFormText = "layoutControlDepartaments";
            this.layoutControlDepartaments.Location = new System.Drawing.Point(0, 145);
            this.layoutControlDepartaments.Name = "layoutControlDepartaments";
            this.layoutControlDepartaments.Size = new System.Drawing.Size(405, 123);
            this.layoutControlDepartaments.Text = "layoutControlDepartaments";
            this.layoutControlDepartaments.TextSize = new System.Drawing.Size(168, 13);
            // 
            // layoutControlItemPassword
            // 
            this.layoutControlItemPassword.Control = this.textBoxPassword;
            this.layoutControlItemPassword.CustomizationFormText = "layoutControlItemPassword";
            this.layoutControlItemPassword.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItemPassword.Name = "layoutControlItemPassword";
            this.layoutControlItemPassword.Size = new System.Drawing.Size(405, 24);
            this.layoutControlItemPassword.Text = "layoutControlItemPassword";
            this.layoutControlItemPassword.TextSize = new System.Drawing.Size(168, 13);
            this.layoutControlItemPassword.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItemRepeatPassword
            // 
            this.layoutControlItemRepeatPassword.Control = this.textBoxRepeatPassword;
            this.layoutControlItemRepeatPassword.CustomizationFormText = "layoutControlItemRepeatPassword";
            this.layoutControlItemRepeatPassword.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItemRepeatPassword.Name = "layoutControlItemRepeatPassword";
            this.layoutControlItemRepeatPassword.Size = new System.Drawing.Size(405, 24);
            this.layoutControlItemRepeatPassword.Text = "layoutControlItemRepeatPassword";
            this.layoutControlItemRepeatPassword.TextSize = new System.Drawing.Size(168, 13);
            this.layoutControlItemRepeatPassword.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlAgentPassword
            // 
            this.layoutControlAgentPassword.Control = this.textBoxExAgentPassword;
            this.layoutControlAgentPassword.CustomizationFormText = "layoutControlAgentPassword";
            this.layoutControlAgentPassword.Location = new System.Drawing.Point(0, 121);
            this.layoutControlAgentPassword.Name = "layoutControlAgentPassword";
            this.layoutControlAgentPassword.Size = new System.Drawing.Size(405, 24);
            this.layoutControlAgentPassword.Text = "layoutControlAgentPassword";
            this.layoutControlAgentPassword.TextSize = new System.Drawing.Size(168, 13);
            // 
            // layoutControlUserData
            // 
            this.layoutControlUserData.CustomizationFormText = "layoutControlGroup5";
            this.layoutControlUserData.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupUserData});
            this.layoutControlUserData.Location = new System.Drawing.Point(0, 0);
            this.layoutControlUserData.Name = "layoutControlUserData";
            this.layoutControlUserData.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlUserData.Size = new System.Drawing.Size(429, 312);
            this.layoutControlUserData.Text = "layoutControlUserData";
            // 
            // layoutControlGroupUserData
            // 
            this.layoutControlGroupUserData.CustomizationFormText = "layoutControlGroupUserData";
            this.layoutControlGroupUserData.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlName,
            this.layoutControlLastName,
            this.layoutControlPersonID,
            this.layoutControlItem7,
            this.layoutControlDateBirth,
            this.layoutControlPhone,
            this.layoutControlEmail,
            this.layoutControlAddress,
            this.emptySpaceItem5});
            this.layoutControlGroupUserData.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupUserData.Name = "layoutControlGroupUserData";
            this.layoutControlGroupUserData.Size = new System.Drawing.Size(429, 312);
            this.layoutControlGroupUserData.Text = "layoutControlGroupUserData";
            // 
            // layoutControlName
            // 
            this.layoutControlName.Control = this.textBoxName;
            this.layoutControlName.CustomizationFormText = "layoutControlName";
            this.layoutControlName.Location = new System.Drawing.Point(0, 0);
            this.layoutControlName.Name = "layoutControlName";
            this.layoutControlName.Size = new System.Drawing.Size(284, 24);
            this.layoutControlName.Text = "layoutControlName";
            this.layoutControlName.TextSize = new System.Drawing.Size(168, 13);
            // 
            // layoutControlLastName
            // 
            this.layoutControlLastName.Control = this.textBoxLastName;
            this.layoutControlLastName.CustomizationFormText = "layoutControlLastName";
            this.layoutControlLastName.Location = new System.Drawing.Point(0, 24);
            this.layoutControlLastName.Name = "layoutControlLastName";
            this.layoutControlLastName.Size = new System.Drawing.Size(284, 24);
            this.layoutControlLastName.Text = "layoutControlLastName";
            this.layoutControlLastName.TextSize = new System.Drawing.Size(168, 13);
            // 
            // layoutControlPersonID
            // 
            this.layoutControlPersonID.Control = this.textBoxIdNumber;
            this.layoutControlPersonID.CustomizationFormText = "layoutControlPersonID";
            this.layoutControlPersonID.Location = new System.Drawing.Point(0, 48);
            this.layoutControlPersonID.Name = "layoutControlPersonID";
            this.layoutControlPersonID.Size = new System.Drawing.Size(284, 24);
            this.layoutControlPersonID.Text = "layoutControlPersonID";
            this.layoutControlPersonID.TextSize = new System.Drawing.Size(168, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.panelControl1;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(284, 0);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(121, 186);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(121, 186);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(121, 186);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlDateBirth
            // 
            this.layoutControlDateBirth.Control = this.dateTimePicker1;
            this.layoutControlDateBirth.CustomizationFormText = "layoutControlDateBirth";
            this.layoutControlDateBirth.Location = new System.Drawing.Point(0, 72);
            this.layoutControlDateBirth.Name = "layoutControlDateBirth";
            this.layoutControlDateBirth.Size = new System.Drawing.Size(284, 24);
            this.layoutControlDateBirth.Text = "layoutControlDateBirth";
            this.layoutControlDateBirth.TextSize = new System.Drawing.Size(168, 13);
            // 
            // layoutControlPhone
            // 
            this.layoutControlPhone.Control = this.textBoxPhone;
            this.layoutControlPhone.CustomizationFormText = "layoutControlPhone";
            this.layoutControlPhone.Location = new System.Drawing.Point(0, 96);
            this.layoutControlPhone.Name = "layoutControlPhone";
            this.layoutControlPhone.Size = new System.Drawing.Size(284, 24);
            this.layoutControlPhone.Text = "layoutControlPhone";
            this.layoutControlPhone.TextSize = new System.Drawing.Size(168, 13);
            // 
            // layoutControlEmail
            // 
            this.layoutControlEmail.Control = this.textBoxEMail;
            this.layoutControlEmail.CustomizationFormText = "layoutControlEmail";
            this.layoutControlEmail.Location = new System.Drawing.Point(0, 120);
            this.layoutControlEmail.Name = "layoutControlEmail";
            this.layoutControlEmail.Size = new System.Drawing.Size(284, 66);
            this.layoutControlEmail.Text = "layoutControlEmail";
            this.layoutControlEmail.TextSize = new System.Drawing.Size(168, 13);
            // 
            // layoutControlAddress
            // 
            this.layoutControlAddress.Control = this.textBoxAddress;
            this.layoutControlAddress.CustomizationFormText = "layoutControlAddress";
            this.layoutControlAddress.Location = new System.Drawing.Point(0, 186);
            this.layoutControlAddress.MinSize = new System.Drawing.Size(169, 31);
            this.layoutControlAddress.Name = "layoutControlAddress";
            this.layoutControlAddress.Size = new System.Drawing.Size(405, 56);
            this.layoutControlAddress.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlAddress.Text = "layoutControlAddress";
            this.layoutControlAddress.TextSize = new System.Drawing.Size(168, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 242);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(405, 26);
            this.emptySpaceItem5.Text = "emptySpaceItem5";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlCameras
            // 
            this.layoutControlCameras.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlCameras.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupCameras});
            this.layoutControlCameras.Location = new System.Drawing.Point(0, 0);
            this.layoutControlCameras.Name = "layoutControlCameras";
            this.layoutControlCameras.Size = new System.Drawing.Size(429, 312);
            this.layoutControlCameras.Text = "layoutControlCameras";
            // 
            // layoutControlGroupCameras
            // 
            this.layoutControlGroupCameras.CustomizationFormText = "layoutControlGroupCameras";
            this.layoutControlGroupCameras.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlCCTVZone,
            this.layoutControlStruct,
            this.layoutControlStructType,
            this.layoutControlCamerasAvailable,
            this.layoutControlCameraAsociated,
            this.emptySpaceItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.emptySpaceItem1});
            this.layoutControlGroupCameras.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupCameras.Name = "layoutControlGroupCameras";
            this.layoutControlGroupCameras.Size = new System.Drawing.Size(429, 312);
            this.layoutControlGroupCameras.Text = "layoutControlGroupCameras";
            // 
            // layoutControlCCTVZone
            // 
            this.layoutControlCCTVZone.Control = this.comboBoxCctvZone;
            this.layoutControlCCTVZone.CustomizationFormText = "layoutControlCCTVZone";
            this.layoutControlCCTVZone.Location = new System.Drawing.Point(0, 0);
            this.layoutControlCCTVZone.Name = "layoutControlCCTVZone";
            this.layoutControlCCTVZone.Size = new System.Drawing.Size(405, 25);
            this.layoutControlCCTVZone.Text = "layoutControlCCTVZone";
            this.layoutControlCCTVZone.TextSize = new System.Drawing.Size(168, 13);
            // 
            // layoutControlStruct
            // 
            this.layoutControlStruct.Control = this.comboBoxExStruct;
            this.layoutControlStruct.CustomizationFormText = "layoutControlStruct";
            this.layoutControlStruct.Location = new System.Drawing.Point(0, 50);
            this.layoutControlStruct.Name = "layoutControlStruct";
            this.layoutControlStruct.Size = new System.Drawing.Size(405, 25);
            this.layoutControlStruct.Text = "layoutControlStruct";
            this.layoutControlStruct.TextSize = new System.Drawing.Size(168, 13);
            // 
            // layoutControlStructType
            // 
            this.layoutControlStructType.Control = this.comboBoxExStructType;
            this.layoutControlStructType.CustomizationFormText = "layoutControlStructType";
            this.layoutControlStructType.Location = new System.Drawing.Point(0, 25);
            this.layoutControlStructType.Name = "layoutControlStructType";
            this.layoutControlStructType.Size = new System.Drawing.Size(405, 25);
            this.layoutControlStructType.Text = "layoutControlStructType";
            this.layoutControlStructType.TextSize = new System.Drawing.Size(168, 13);
            // 
            // layoutControlCamerasAvailable
            // 
            this.layoutControlCamerasAvailable.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlCamerasAvailable.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlCamerasAvailable.Control = this.listBoxExAvailablesCameras;
            this.layoutControlCamerasAvailable.CustomizationFormText = "layoutControlItem1";
            this.layoutControlCamerasAvailable.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.layoutControlCamerasAvailable.Location = new System.Drawing.Point(0, 75);
            this.layoutControlCamerasAvailable.Name = "layoutControlCamerasAvailable";
            this.layoutControlCamerasAvailable.Size = new System.Drawing.Size(181, 193);
            this.layoutControlCamerasAvailable.Text = "layoutControlCamerasAvailable";
            this.layoutControlCamerasAvailable.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlCamerasAvailable.TextSize = new System.Drawing.Size(168, 13);
            // 
            // layoutControlCameraAsociated
            // 
            this.layoutControlCameraAsociated.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlCameraAsociated.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlCameraAsociated.Control = this.listBoxExUnAvailablesCameras;
            this.layoutControlCameraAsociated.CustomizationFormText = "layoutControlItem2";
            this.layoutControlCameraAsociated.Location = new System.Drawing.Point(226, 75);
            this.layoutControlCameraAsociated.MinSize = new System.Drawing.Size(144, 56);
            this.layoutControlCameraAsociated.Name = "layoutControlCameraAsociated";
            this.layoutControlCameraAsociated.Size = new System.Drawing.Size(179, 193);
            this.layoutControlCameraAsociated.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlCameraAsociated.Text = "layoutControlCameraAsociated";
            this.layoutControlCameraAsociated.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlCameraAsociated.TextSize = new System.Drawing.Size(168, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(181, 75);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(45, 35);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.buttonExAddCamera;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(181, 110);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(45, 33);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(45, 33);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(45, 33);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.buttonExAddAllCameras;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(181, 143);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(45, 33);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(45, 33);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(45, 33);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.buttonExRemoveCamera;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(181, 176);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(45, 33);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(45, 33);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(45, 33);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.buttonExRemoveAllCameras;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(181, 209);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(45, 33);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(45, 33);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(45, 33);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(181, 242);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(45, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(45, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(45, 26);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.buttonOk;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(281, 356);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.ShowInCustomizationForm = false;
            this.layoutControlItem8.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.buttonCancel;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(367, 356);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.ShowInCustomizationForm = false;
            this.layoutControlItem9.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 356);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(110, 30);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.ShowInCustomizationForm = false;
            this.emptySpaceItem6.Size = new System.Drawing.Size(281, 36);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.Text = "emptySpaceItem6";
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // importUserAccountImage
            // 
            this.importUserAccountImage.Filter = "Image Files (*.jpg)|*.jpg";
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem3.Location = new System.Drawing.Point(181, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem4";
            this.emptySpaceItem3.Size = new System.Drawing.Size(33, 122);
            this.emptySpaceItem3.Text = "emptySpaceItem4";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 236);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(400, 70);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem4});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup2.Size = new System.Drawing.Size(406, 330);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "layoutControlGroup6";
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup6.Size = new System.Drawing.Size(406, 56);
            this.layoutControlGroup6.Text = "layoutControlGroup6";
            // 
            // checkBoxExWindowsAuthentication
            // 
            this.checkBoxExWindowsAuthentication.AutoSize = true;
            this.checkBoxExWindowsAuthentication.Checked = true;
            this.checkBoxExWindowsAuthentication.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxExWindowsAuthentication.Enabled = false;
            this.checkBoxExWindowsAuthentication.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.checkBoxExWindowsAuthentication.Location = new System.Drawing.Point(129, 166);
            this.checkBoxExWindowsAuthentication.Name = "checkBoxExWindowsAuthentication";
            this.checkBoxExWindowsAuthentication.Size = new System.Drawing.Size(150, 17);
            this.checkBoxExWindowsAuthentication.TabIndex = 10;
            this.checkBoxExWindowsAuthentication.Text = "Autenticacion de windows";
            this.checkBoxExWindowsAuthentication.UseVisualStyleBackColor = true;
            // 
            // labelRepeatPassword
            // 
            this.labelRepeatPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRepeatPassword.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelRepeatPassword.Location = new System.Drawing.Point(7, 76);
            this.labelRepeatPassword.Name = "labelRepeatPassword";
            this.labelRepeatPassword.Size = new System.Drawing.Size(112, 21);
            this.labelRepeatPassword.TabIndex = 4;
            this.labelRepeatPassword.Text = "Repetir contrasena:";
            this.labelRepeatPassword.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelPassword
            // 
            this.labelPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPassword.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelPassword.Location = new System.Drawing.Point(7, 48);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(104, 21);
            this.labelPassword.TabIndex = 2;
            this.labelPassword.Text = "Contrasena:";
            this.labelPassword.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.textBoxPassword;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItemPassword";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem1.Name = "layoutControlItemPassword";
            this.layoutControlItem1.Size = new System.Drawing.Size(405, 24);
            this.layoutControlItem1.Text = "layoutControlItemPassword";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(168, 13);
            this.layoutControlItem1.TextToControlDistance = 5;
            this.layoutControlItem1.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // UserAccountForm
            // 
            this.AcceptButton = this.buttonOk;
            this.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(453, 392);
            this.Controls.Add(this.layoutControl1);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(461, 390);
            this.Name = "UserAccountForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Editar Usuario";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.UserAccountForm_FormClosing);
            this.Load += new System.EventHandler(this.UserAccountForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.userAccountPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlAccountDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlIdentifier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlRole)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlAgentID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlDepartaments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemRepeatPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlAgentPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlUserData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupUserData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlLastName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlPersonID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlDateBirth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCameras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupCameras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCCTVZone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlStruct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlStructType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCamerasAvailable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCameraAsociated)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.ResumeLayout(false);

        }

       
        #endregion

		private CheckBoxEx checkBoxExWindowsAuthentication;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.ComponentModel.IContainer components;
		private DevExpress.XtraEditors.SimpleButton buttonExAddCamera;
		private DevExpress.XtraEditors.SimpleButton buttonExAddAllCameras;
		private DevExpress.XtraEditors.SimpleButton buttonExRemoveCamera;
		private ComboBoxEx comboBoxExStructType;
		private DevExpress.XtraEditors.SimpleButton buttonExRemoveAllCameras;
        private ComboBoxEx comboBoxExStruct;
        private ComboBoxEx comboBoxCctvZone;
		private ListBoxEx listBoxExAvailablesCameras;
        private ListBoxEx listBoxExUnAvailablesCameras;
		private TextBoxEx textBoxLogin;
		private TextBoxEx textBoxExAgentID;
		private ComboBoxEx comboBoxRole;
        private CheckedListBoxEx checkedListBoxDepartamentos;
		private DevExpress.XtraEditors.SimpleButton removePictureButton;
		private DevExpress.XtraEditors.SimpleButton imageImportButton;
		private TextBoxEx textBoxLastName;
		private System.Windows.Forms.PictureBox userAccountPictureBox;
		private TextBoxEx textBoxName;
        private DateTimePickerEx dateTimePicker1;
		private TextBoxEx textBoxIdNumber;
		private TextBoxEx textBoxPhone;
		private TextBoxEx textBoxAddress;
        private TextBoxEx textBoxEMail;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlCameras;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlAccountDescription;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupAccount;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlIdentifier;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlRole;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlAgentID;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlDepartaments;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlStructType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlStruct;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlCamerasAvailable;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlCameraAsociated;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlCCTVZone;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlUserData;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupUserData;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlLastName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlPersonID;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlDateBirth;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlPhone;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlEmail;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlAddress;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemPassword;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemRepeatPassword;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupCameras;
        private TextBoxEx textBoxExAgentPassword;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlAgentPassword;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
    }
}
