using SmartCadControls;
using SmartCadControls.Controls;
namespace SmartCadGuiCommon
{
    partial class EvaluationsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EvaluationsForm));
            this.textBoxExDesc = new TextBoxEx();
            this.comboBoxExScale = new ComboBoxEx();
            this.comboBoxExOperatorCat = new ComboBoxEx();
            this.label2 = new LabelEx();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.checkBoxExPon = new CheckBoxEx();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonExCancel = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlEvaluation = new DevExpress.XtraLayout.LayoutControl();
            this.gridControlExQuestions = new GridControlEx();
            this.gridViewExQuestions = new GridViewEx();
            this.gridViewEx2 = new GridViewEx();
            this.buttonExAccept = new DevExpress.XtraEditors.SimpleButton();
            this.checkedListBoxDepartaments = new CheckedListBoxEx();
            this.checkedListBoxApplications = new CheckedListBoxEx();
            this.textBoxExEvaluationName = new TextBoxEx();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroupEvaluation = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroupEvaluation = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupEvalInformation = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemDesc = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemScale = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupApplyTo = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemOpeCat = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemApplications = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemDepartments = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupQuestion = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemPon = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTotalLabel = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTotal = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItemQuestions = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlEvaluation)).BeginInit();
            this.layoutControlEvaluation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExQuestions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExQuestions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEx2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroupEvaluation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupEvaluation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupEvalInformation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemScale)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupApplyTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOpeCat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemApplications)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupQuestion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemQuestions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxExDesc
            // 
            this.textBoxExDesc.AllowsLetters = true;
            this.textBoxExDesc.AllowsNumbers = true;
            this.textBoxExDesc.AllowsPunctuation = true;
            this.textBoxExDesc.AllowsSeparators = true;
            this.textBoxExDesc.AllowsSymbols = true;
            this.textBoxExDesc.AllowsWhiteSpaces = true;
            this.textBoxExDesc.Enabled = false;
            this.textBoxExDesc.ExtraAllowedChars = "";
            this.textBoxExDesc.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExDesc.Location = new System.Drawing.Point(157, 100);
            this.textBoxExDesc.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxExDesc.MaxLength = 60;
            this.textBoxExDesc.Multiline = true;
            this.textBoxExDesc.Name = "textBoxExDesc";
            this.textBoxExDesc.NonAllowedCharacters = "";
            this.textBoxExDesc.RegularExpresion = "";
            this.textBoxExDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxExDesc.Size = new System.Drawing.Size(266, 81);
            this.textBoxExDesc.TabIndex = 2;
            this.textBoxExDesc.TextChanged += new System.EventHandler(this.EvaluationParameters_Change);
            // 
            // comboBoxExScale
            // 
            this.comboBoxExScale.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxExScale.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.comboBoxExScale.FormattingEnabled = true;
            this.comboBoxExScale.Location = new System.Drawing.Point(157, 75);
            this.comboBoxExScale.Margin = new System.Windows.Forms.Padding(2);
            this.comboBoxExScale.Name = "comboBoxExScale";
            this.comboBoxExScale.Size = new System.Drawing.Size(266, 21);
            this.comboBoxExScale.TabIndex = 1;
            this.comboBoxExScale.SelectedIndexChanged += new System.EventHandler(this.comboBoxExScale_SelectedIndexChanged);
            // 
            // comboBoxExOperatorCat
            // 
            this.comboBoxExOperatorCat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxExOperatorCat.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.comboBoxExOperatorCat.FormattingEnabled = true;
            this.comboBoxExOperatorCat.Location = new System.Drawing.Point(157, 214);
            this.comboBoxExOperatorCat.Margin = new System.Windows.Forms.Padding(2);
            this.comboBoxExOperatorCat.Name = "comboBoxExOperatorCat";
            this.comboBoxExOperatorCat.Size = new System.Drawing.Size(266, 21);
            this.comboBoxExOperatorCat.TabIndex = 0;
            this.comboBoxExOperatorCat.SelectedIndexChanged += new System.EventHandler(this.EvaluationParameters_Change);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.label2.Location = new System.Drawing.Point(363, 451);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 27);
            this.label2.TabIndex = 3;
            this.label2.Text = "0";
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton2});
            this.toolStrip1.Location = new System.Drawing.Point(10, 30);
            this.toolStrip1.Margin = new System.Windows.Forms.Padding(2);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(415, 28);
            this.toolStrip1.TabIndex = 3;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 25);
            this.toolStripButton1.Text = "toolStripButton1";
            this.toolStripButton1.ToolTipText = "Agregar pregunta";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click_1);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(23, 25);
            this.toolStripButton2.Text = "toolStripButton2";
            this.toolStripButton2.ToolTipText = "Eliminar pregunta";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // checkBoxExPon
            // 
            this.checkBoxExPon.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.checkBoxExPon.Location = new System.Drawing.Point(7, 451);
            this.checkBoxExPon.Margin = new System.Windows.Forms.Padding(2);
            this.checkBoxExPon.Name = "checkBoxExPon";
            this.checkBoxExPon.Size = new System.Drawing.Size(304, 27);
            this.checkBoxExPon.TabIndex = 0;
            this.checkBoxExPon.Text = "Ponderacion equitativa";
            this.checkBoxExPon.UseVisualStyleBackColor = true;
            this.checkBoxExPon.CheckedChanged += new System.EventHandler(this.checkBoxExPon_CheckedChanged);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(315, 451);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 27);
            this.label1.TabIndex = 2;
            this.label1.Text = "Total:";
            // 
            // buttonExCancel
            // 
            this.buttonExCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.buttonExCancel.Appearance.Options.UseFont = true;
            this.buttonExCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonExCancel.Location = new System.Drawing.Point(351, 487);
            this.buttonExCancel.Margin = new System.Windows.Forms.Padding(2);
            this.buttonExCancel.Name = "buttonExCancel";
            this.buttonExCancel.Size = new System.Drawing.Size(82, 32);
            this.buttonExCancel.StyleController = this.layoutControlEvaluation;
            this.buttonExCancel.TabIndex = 2;
            this.buttonExCancel.Text = "Cancelar";
            // 
            // layoutControlEvaluation
            // 
            this.layoutControlEvaluation.AllowCustomizationMenu = false;
            this.layoutControlEvaluation.Controls.Add(this.gridControlExQuestions);
            this.layoutControlEvaluation.Controls.Add(this.buttonExCancel);
            this.layoutControlEvaluation.Controls.Add(this.buttonExAccept);
            this.layoutControlEvaluation.Controls.Add(this.toolStrip1);
            this.layoutControlEvaluation.Controls.Add(this.label2);
            this.layoutControlEvaluation.Controls.Add(this.label1);
            this.layoutControlEvaluation.Controls.Add(this.checkBoxExPon);
            this.layoutControlEvaluation.Controls.Add(this.checkedListBoxDepartaments);
            this.layoutControlEvaluation.Controls.Add(this.textBoxExDesc);
            this.layoutControlEvaluation.Controls.Add(this.checkedListBoxApplications);
            this.layoutControlEvaluation.Controls.Add(this.comboBoxExScale);
            this.layoutControlEvaluation.Controls.Add(this.comboBoxExOperatorCat);
            this.layoutControlEvaluation.Controls.Add(this.textBoxExEvaluationName);
            this.layoutControlEvaluation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlEvaluation.Location = new System.Drawing.Point(0, 0);
            this.layoutControlEvaluation.Name = "layoutControlEvaluation";
            this.layoutControlEvaluation.OptionsCustomizationForm.ShowLayoutTreeView = false;
            this.layoutControlEvaluation.Root = this.layoutControlGroup1;
            this.layoutControlEvaluation.Size = new System.Drawing.Size(435, 521);
            this.layoutControlEvaluation.TabIndex = 4;
            this.layoutControlEvaluation.Text = "layoutControl1";
            // 
            // gridControlExQuestions
            // 
            this.gridControlExQuestions.EnableAutoFilter = true;
            this.gridControlExQuestions.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExQuestions.Location = new System.Drawing.Point(7, 60);
            this.gridControlExQuestions.MainView = this.gridViewExQuestions;
            this.gridControlExQuestions.Name = "gridControlExQuestions";
            this.gridControlExQuestions.Size = new System.Drawing.Size(421, 387);
            this.gridControlExQuestions.TabIndex = 4;
            this.gridControlExQuestions.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExQuestions,
            this.gridViewEx2});
            this.gridControlExQuestions.ViewTotalRows = false;
            // 
            // gridViewExQuestions
            // 
            this.gridViewExQuestions.AllowFocusedRowChanged = true;
            this.gridViewExQuestions.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExQuestions.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExQuestions.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExQuestions.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExQuestions.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExQuestions.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExQuestions.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExQuestions.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExQuestions.EnablePreviewLineForFocusedRow = false;
            this.gridViewExQuestions.GridControl = this.gridControlExQuestions;
            this.gridViewExQuestions.Name = "gridViewExQuestions";
            this.gridViewExQuestions.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExQuestions.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExQuestions.OptionsView.ShowGroupPanel = false;
            this.gridViewExQuestions.ViewTotalRows = false;
            this.gridViewExQuestions.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewExQuestions_CellValueChanged);
            this.gridViewExQuestions.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewExQuestions_CellValueChanging);
            // 
            // gridViewEx2
            // 
            this.gridViewEx2.AllowFocusedRowChanged = true;
            this.gridViewEx2.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewEx2.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewEx2.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewEx2.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewEx2.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewEx2.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewEx2.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewEx2.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewEx2.EnablePreviewLineForFocusedRow = false;
            this.gridViewEx2.GridControl = this.gridControlExQuestions;
            this.gridViewEx2.Name = "gridViewEx2";
            this.gridViewEx2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewEx2.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewEx2.ViewTotalRows = false;
            // 
            // buttonExAccept
            // 
            this.buttonExAccept.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.buttonExAccept.Appearance.Options.UseFont = true;
            this.buttonExAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonExAccept.Location = new System.Drawing.Point(265, 487);
            this.buttonExAccept.Margin = new System.Windows.Forms.Padding(2);
            this.buttonExAccept.Name = "buttonExAccept";
            this.buttonExAccept.Size = new System.Drawing.Size(82, 32);
            this.buttonExAccept.StyleController = this.layoutControlEvaluation;
            this.buttonExAccept.TabIndex = 1;
            this.buttonExAccept.Text = "Accept";
            this.buttonExAccept.Click += new System.EventHandler(this.buttonExAccept_Click);
            // 
            // checkedListBoxDepartaments
            // 
            this.checkedListBoxDepartaments.CheckOnClick = true;
            this.checkedListBoxDepartaments.Enabled = false;
            this.checkedListBoxDepartaments.FormattingEnabled = true;
            this.checkedListBoxDepartaments.IntegralHeight = false;
            this.checkedListBoxDepartaments.Location = new System.Drawing.Point(157, 339);
            this.checkedListBoxDepartaments.Margin = new System.Windows.Forms.Padding(2);
            this.checkedListBoxDepartaments.Name = "checkedListBoxDepartaments";
            this.checkedListBoxDepartaments.Size = new System.Drawing.Size(266, 134);
            this.checkedListBoxDepartaments.TabIndex = 2;
            this.checkedListBoxDepartaments.SelectedIndexChanged += new System.EventHandler(this.EvaluationParameters_Change);
            // 
            // checkedListBoxApplications
            // 
            this.checkedListBoxApplications.CheckOnClick = true;
            this.checkedListBoxApplications.FormattingEnabled = true;
            this.checkedListBoxApplications.IntegralHeight = false;
            this.checkedListBoxApplications.Location = new System.Drawing.Point(157, 239);
            this.checkedListBoxApplications.Margin = new System.Windows.Forms.Padding(2);
            this.checkedListBoxApplications.Name = "checkedListBoxApplications";
            this.checkedListBoxApplications.Size = new System.Drawing.Size(266, 96);
            this.checkedListBoxApplications.TabIndex = 1;
            this.checkedListBoxApplications.SelectedIndexChanged += new System.EventHandler(this.checkedListBoxApplications_SelectedIndexChanged);
            // 
            // textBoxExEvaluationName
            // 
            this.textBoxExEvaluationName.AllowsLetters = true;
            this.textBoxExEvaluationName.AllowsNumbers = true;
            this.textBoxExEvaluationName.AllowsPunctuation = true;
            this.textBoxExEvaluationName.AllowsSeparators = true;
            this.textBoxExEvaluationName.AllowsSymbols = true;
            this.textBoxExEvaluationName.AllowsWhiteSpaces = true;
            this.textBoxExEvaluationName.ExtraAllowedChars = "";
            this.textBoxExEvaluationName.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExEvaluationName.Location = new System.Drawing.Point(157, 51);
            this.textBoxExEvaluationName.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxExEvaluationName.MaxLength = 40;
            this.textBoxExEvaluationName.Name = "textBoxExEvaluationName";
            this.textBoxExEvaluationName.NonAllowedCharacters = "";
            this.textBoxExEvaluationName.RegularExpresion = "";
            this.textBoxExEvaluationName.Size = new System.Drawing.Size(266, 20);
            this.textBoxExEvaluationName.TabIndex = 0;
            this.textBoxExEvaluationName.TextChanged += new System.EventHandler(this.EvaluationParameters_Change);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroupEvaluation,
            this.layoutControlItem12,
            this.layoutControlItem13,
            this.emptySpaceItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(435, 521);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // tabbedControlGroupEvaluation
            // 
            this.tabbedControlGroupEvaluation.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroupEvaluation.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroupEvaluation.Name = "tabbedControlGroupEvaluation";
            this.tabbedControlGroupEvaluation.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.tabbedControlGroupEvaluation.SelectedTabPage = this.layoutControlGroupQuestion;
            this.tabbedControlGroupEvaluation.SelectedTabPageIndex = 1;
            this.tabbedControlGroupEvaluation.Size = new System.Drawing.Size(435, 485);
            this.tabbedControlGroupEvaluation.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupEvaluation,
            this.layoutControlGroupQuestion});
            this.tabbedControlGroupEvaluation.Text = "tabbedControlGroupEvaluation";
            // 
            // layoutControlGroupEvaluation
            // 
            this.layoutControlGroupEvaluation.AppearanceGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupEvaluation.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroupEvaluation.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupEvaluation.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroupEvaluation.AppearanceTabPage.Header.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupEvaluation.AppearanceTabPage.Header.Options.UseFont = true;
            this.layoutControlGroupEvaluation.AppearanceTabPage.HeaderActive.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroupEvaluation.AppearanceTabPage.HeaderActive.Options.UseFont = true;
            this.layoutControlGroupEvaluation.AppearanceTabPage.HeaderDisabled.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroupEvaluation.AppearanceTabPage.HeaderDisabled.Options.UseFont = true;
            this.layoutControlGroupEvaluation.AppearanceTabPage.HeaderHotTracked.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroupEvaluation.AppearanceTabPage.HeaderHotTracked.Options.UseFont = true;
            this.layoutControlGroupEvaluation.AppearanceTabPage.PageClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroupEvaluation.AppearanceTabPage.PageClient.Options.UseFont = true;
            this.layoutControlGroupEvaluation.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroupEvaluation.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupEvalInformation,
            this.layoutControlGroupApplyTo});
            this.layoutControlGroupEvaluation.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupEvaluation.Name = "layoutControlGroupEvaluation";
            this.layoutControlGroupEvaluation.Size = new System.Drawing.Size(425, 455);
            this.layoutControlGroupEvaluation.Text = "layoutControlGroupEvaluation";
            // 
            // layoutControlGroupEvalInformation
            // 
            this.layoutControlGroupEvalInformation.AppearanceGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupEvalInformation.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroupEvalInformation.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupEvalInformation.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroupEvalInformation.CustomizationFormText = "layoutControlGroupEvalInformation";
            this.layoutControlGroupEvalInformation.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemDesc,
            this.layoutControlItemScale,
            this.layoutControlItemName});
            this.layoutControlGroupEvalInformation.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupEvalInformation.Name = "layoutControlGroupEvalInformation";
            this.layoutControlGroupEvalInformation.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupEvalInformation.Size = new System.Drawing.Size(425, 163);
            this.layoutControlGroupEvalInformation.Text = "layoutControlGroupEvalInformation";
            // 
            // layoutControlItemDesc
            // 
            this.layoutControlItemDesc.Control = this.textBoxExDesc;
            this.layoutControlItemDesc.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItemDesc.Location = new System.Drawing.Point(0, 49);
            this.layoutControlItemDesc.MinSize = new System.Drawing.Size(177, 31);
            this.layoutControlItemDesc.Name = "layoutControlItemDesc";
            this.layoutControlItemDesc.Size = new System.Drawing.Size(415, 85);
            this.layoutControlItemDesc.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemDesc.Text = "layoutControlItemDesc";
            this.layoutControlItemDesc.TextSize = new System.Drawing.Size(141, 13);
            // 
            // layoutControlItemScale
            // 
            this.layoutControlItemScale.Control = this.comboBoxExScale;
            this.layoutControlItemScale.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItemScale.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItemScale.Name = "layoutControlItemScale";
            this.layoutControlItemScale.Size = new System.Drawing.Size(415, 25);
            this.layoutControlItemScale.Text = "layoutControlItemScale";
            this.layoutControlItemScale.TextSize = new System.Drawing.Size(141, 13);
            // 
            // layoutControlItemName
            // 
            this.layoutControlItemName.Control = this.textBoxExEvaluationName;
            this.layoutControlItemName.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItemName.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemName.Name = "layoutControlItemName";
            this.layoutControlItemName.Size = new System.Drawing.Size(415, 24);
            this.layoutControlItemName.Text = "layoutControlItemName";
            this.layoutControlItemName.TextSize = new System.Drawing.Size(141, 13);
            // 
            // layoutControlGroupApplyTo
            // 
            this.layoutControlGroupApplyTo.AppearanceGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupApplyTo.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroupApplyTo.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupApplyTo.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroupApplyTo.CustomizationFormText = "layoutControlGroupApplyTo";
            this.layoutControlGroupApplyTo.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemOpeCat,
            this.layoutControlItemApplications,
            this.layoutControlItemDepartments});
            this.layoutControlGroupApplyTo.Location = new System.Drawing.Point(0, 163);
            this.layoutControlGroupApplyTo.Name = "layoutControlGroupApplyTo";
            this.layoutControlGroupApplyTo.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupApplyTo.Size = new System.Drawing.Size(425, 292);
            this.layoutControlGroupApplyTo.Text = "layoutControlGroupApplyTo";
            // 
            // layoutControlItemOpeCat
            // 
            this.layoutControlItemOpeCat.Control = this.comboBoxExOperatorCat;
            this.layoutControlItemOpeCat.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItemOpeCat.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemOpeCat.Name = "layoutControlItemOpeCat";
            this.layoutControlItemOpeCat.Size = new System.Drawing.Size(415, 25);
            this.layoutControlItemOpeCat.Text = "layoutControlItemOpeCat";
            this.layoutControlItemOpeCat.TextSize = new System.Drawing.Size(141, 13);
            // 
            // layoutControlItemApplications
            // 
            this.layoutControlItemApplications.Control = this.checkedListBoxApplications;
            this.layoutControlItemApplications.CustomizationFormText = "layoutControlItemApplications";
            this.layoutControlItemApplications.Location = new System.Drawing.Point(0, 25);
            this.layoutControlItemApplications.Name = "layoutControlItemApplications";
            this.layoutControlItemApplications.Size = new System.Drawing.Size(415, 100);
            this.layoutControlItemApplications.Text = "layoutControlItemApplications";
            this.layoutControlItemApplications.TextSize = new System.Drawing.Size(141, 13);
            // 
            // layoutControlItemDepartments
            // 
            this.layoutControlItemDepartments.Control = this.checkedListBoxDepartaments;
            this.layoutControlItemDepartments.CustomizationFormText = "layoutControlItemDepartments";
            this.layoutControlItemDepartments.Location = new System.Drawing.Point(0, 125);
            this.layoutControlItemDepartments.Name = "layoutControlItemDepartments";
            this.layoutControlItemDepartments.Size = new System.Drawing.Size(415, 138);
            this.layoutControlItemDepartments.Text = "layoutControlItemDepartments";
            this.layoutControlItemDepartments.TextSize = new System.Drawing.Size(141, 13);
            // 
            // layoutControlGroupQuestion
            // 
            this.layoutControlGroupQuestion.AppearanceGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroupQuestion.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroupQuestion.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroupQuestion.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroupQuestion.AppearanceTabPage.Header.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroupQuestion.AppearanceTabPage.Header.Options.UseFont = true;
            this.layoutControlGroupQuestion.AppearanceTabPage.HeaderActive.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroupQuestion.AppearanceTabPage.HeaderActive.Options.UseFont = true;
            this.layoutControlGroupQuestion.AppearanceTabPage.HeaderDisabled.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroupQuestion.AppearanceTabPage.HeaderDisabled.Options.UseFont = true;
            this.layoutControlGroupQuestion.AppearanceTabPage.HeaderHotTracked.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroupQuestion.AppearanceTabPage.HeaderHotTracked.Options.UseFont = true;
            this.layoutControlGroupQuestion.AppearanceTabPage.PageClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroupQuestion.AppearanceTabPage.PageClient.Options.UseFont = true;
            this.layoutControlGroupQuestion.CustomizationFormText = "layoutControlGroupQuestion";
            this.layoutControlGroupQuestion.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem7,
            this.layoutControlItemPon,
            this.layoutControlItemTotalLabel,
            this.layoutControlItemTotal,
            this.emptySpaceItem2,
            this.layoutControlItemQuestions});
            this.layoutControlGroupQuestion.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupQuestion.Name = "layoutControlGroupQuestion";
            this.layoutControlGroupQuestion.Size = new System.Drawing.Size(425, 455);
            this.layoutControlGroupQuestion.Text = "layoutControlGroupQuestion";
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.toolStrip1;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(0, 33);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(111, 33);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 0);
            this.layoutControlItem7.Size = new System.Drawing.Size(425, 33);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItemPon
            // 
            this.layoutControlItemPon.Control = this.checkBoxExPon;
            this.layoutControlItemPon.CustomizationFormText = "layoutControlItemPon";
            this.layoutControlItemPon.Location = new System.Drawing.Point(0, 424);
            this.layoutControlItemPon.MaxSize = new System.Drawing.Size(308, 31);
            this.layoutControlItemPon.MinSize = new System.Drawing.Size(308, 31);
            this.layoutControlItemPon.Name = "layoutControlItemPon";
            this.layoutControlItemPon.Size = new System.Drawing.Size(308, 31);
            this.layoutControlItemPon.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemPon.Text = "layoutControlItemPon";
            this.layoutControlItemPon.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemPon.TextToControlDistance = 0;
            this.layoutControlItemPon.TextVisible = false;
            // 
            // layoutControlItemTotalLabel
            // 
            this.layoutControlItemTotalLabel.Control = this.label1;
            this.layoutControlItemTotalLabel.CustomizationFormText = "layoutControlItemTotalLabel";
            this.layoutControlItemTotalLabel.Location = new System.Drawing.Point(308, 424);
            this.layoutControlItemTotalLabel.MaxSize = new System.Drawing.Size(48, 31);
            this.layoutControlItemTotalLabel.MinSize = new System.Drawing.Size(48, 31);
            this.layoutControlItemTotalLabel.Name = "layoutControlItemTotalLabel";
            this.layoutControlItemTotalLabel.Size = new System.Drawing.Size(48, 31);
            this.layoutControlItemTotalLabel.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemTotalLabel.Text = "layoutControlItemTotalLabel";
            this.layoutControlItemTotalLabel.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemTotalLabel.TextToControlDistance = 0;
            this.layoutControlItemTotalLabel.TextVisible = false;
            // 
            // layoutControlItemTotal
            // 
            this.layoutControlItemTotal.Control = this.label2;
            this.layoutControlItemTotal.CustomizationFormText = "layoutControlItemTotal";
            this.layoutControlItemTotal.Location = new System.Drawing.Point(356, 424);
            this.layoutControlItemTotal.MaxSize = new System.Drawing.Size(46, 31);
            this.layoutControlItemTotal.MinSize = new System.Drawing.Size(46, 31);
            this.layoutControlItemTotal.Name = "layoutControlItemTotal";
            this.layoutControlItemTotal.Size = new System.Drawing.Size(46, 31);
            this.layoutControlItemTotal.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemTotal.Text = "layoutControlItemTotal";
            this.layoutControlItemTotal.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemTotal.TextToControlDistance = 0;
            this.layoutControlItemTotal.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(402, 424);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(23, 31);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItemQuestions
            // 
            this.layoutControlItemQuestions.Control = this.gridControlExQuestions;
            this.layoutControlItemQuestions.CustomizationFormText = "layoutControlItemQuestions";
            this.layoutControlItemQuestions.Location = new System.Drawing.Point(0, 33);
            this.layoutControlItemQuestions.Name = "layoutControlItemQuestions";
            this.layoutControlItemQuestions.Size = new System.Drawing.Size(425, 391);
            this.layoutControlItemQuestions.Text = "layoutControlItemQuestions";
            this.layoutControlItemQuestions.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemQuestions.TextToControlDistance = 0;
            this.layoutControlItemQuestions.TextVisible = false;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.buttonExAccept;
            this.layoutControlItem12.CustomizationFormText = "layoutControlItem12";
            this.layoutControlItem12.Location = new System.Drawing.Point(263, 485);
            this.layoutControlItem12.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Text = "layoutControlItem12";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextToControlDistance = 0;
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.buttonExCancel;
            this.layoutControlItem13.CustomizationFormText = "layoutControlItem13";
            this.layoutControlItem13.Location = new System.Drawing.Point(349, 485);
            this.layoutControlItem13.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Text = "layoutControlItem13";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextToControlDistance = 0;
            this.layoutControlItem13.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 485);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(263, 36);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // EvaluationsForm
            // 
            this.AcceptButton = this.buttonExAccept;
            this.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonExCancel;
            this.ClientSize = new System.Drawing.Size(435, 521);
            this.Controls.Add(this.layoutControlEvaluation);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(443, 548);
            this.Name = "EvaluationsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Modulo de administracion - Propiedades de la evaluacion";
            this.Load += new System.EventHandler(this.EvaluationsForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.EvaluationsForm_KeyDown);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlEvaluation)).EndInit();
            this.layoutControlEvaluation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExQuestions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExQuestions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEx2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroupEvaluation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupEvaluation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupEvalInformation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemScale)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupApplyTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOpeCat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemApplications)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupQuestion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemQuestions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private CheckBoxEx checkBoxExPon;
        private DevExpress.XtraEditors.SimpleButton buttonExCancel;
        private ComboBoxEx comboBoxExScale;
        private CheckedListBoxEx checkedListBoxDepartaments;
        private CheckedListBoxEx checkedListBoxApplications;
        private ComboBoxEx comboBoxExOperatorCat;
        private DevExpress.XtraEditors.SimpleButton buttonExAccept;
        private System.Windows.Forms.Label label1;
        private TextBoxEx textBoxExDesc;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private LabelEx label2;
        private DevExpress.XtraLayout.LayoutControl layoutControlEvaluation;
        private TextBoxEx textBoxExEvaluationName;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroupEvaluation;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupQuestion;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupEvaluation;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupEvalInformation;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDesc;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemScale;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemName;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupApplyTo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemOpeCat;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemApplications;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDepartments;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemPon;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotalLabel;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotal;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private GridControlEx gridControlExQuestions;
        private GridViewEx gridViewExQuestions;
        private GridViewEx gridViewEx2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemQuestions;
    }
}