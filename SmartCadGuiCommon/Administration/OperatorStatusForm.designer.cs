using SmartCadControls.Controls;
namespace Smartmatic.SmartCad.Gui
{
    partial class OperatorStatusForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.textBoxExTolerance = new TextBoxEx();
            this.textBoxName = new TextBoxEx();
            this.textBoxExPorcentage = new TextBoxEx();
            this.buttonOk = new DevExpress.XtraEditors.SimpleButton();
            this.labelExStatusColor = new LabelEx();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupData = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemColor = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItemPorcentage = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTolerance = new DevExpress.XtraLayout.LayoutControlItem();
            this.checkBoxAvailable = new System.Windows.Forms.CheckBox();
            this.toolTipOperatorStatus = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemColor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPorcentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTolerance)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCancel.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonCancel.Appearance.Options.UseFont = true;
            this.buttonCancel.Appearance.Options.UseForeColor = true;
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(408, 112);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(82, 33);
            this.buttonCancel.StyleController = this.layoutControl1;
            this.buttonCancel.TabIndex = 2;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowCustomizationMenu = false;
            this.layoutControl1.Controls.Add(this.textBoxExTolerance);
            this.layoutControl1.Controls.Add(this.textBoxName);
            this.layoutControl1.Controls.Add(this.buttonCancel);
            this.layoutControl1.Controls.Add(this.textBoxExPorcentage);
            this.layoutControl1.Controls.Add(this.buttonOk);
            this.layoutControl1.Controls.Add(this.labelExStatusColor);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.ShowLayoutTreeView = false;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(492, 153);
            this.layoutControl1.TabIndex = 4;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // textBoxExTolerance
            // 
            this.textBoxExTolerance.AllowsLetters = false;
            this.textBoxExTolerance.AllowsNumbers = true;
            this.textBoxExTolerance.AllowsPunctuation = false;
            this.textBoxExTolerance.AllowsSeparators = false;
            this.textBoxExTolerance.AllowsSymbols = false;
            this.textBoxExTolerance.AllowsWhiteSpaces = false;
            this.textBoxExTolerance.ExtraAllowedChars = "";
            this.textBoxExTolerance.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExTolerance.Location = new System.Drawing.Point(392, 83);
            this.textBoxExTolerance.MaxLength = 3;
            this.textBoxExTolerance.Name = "textBoxExTolerance";
            this.textBoxExTolerance.NonAllowedCharacters = "";
            this.textBoxExTolerance.RegularExpresion = "";
            this.textBoxExTolerance.Size = new System.Drawing.Size(93, 20);
            this.textBoxExTolerance.TabIndex = 2;
            this.textBoxExTolerance.TextChanged += new System.EventHandler(this.OperatorStatusParameters_Change);
            // 
            // textBoxName
            // 
            this.textBoxName.AllowsLetters = true;
            this.textBoxName.AllowsNumbers = true;
            this.textBoxName.AllowsPunctuation = true;
            this.textBoxName.AllowsSeparators = true;
            this.textBoxName.AllowsSymbols = true;
            this.textBoxName.AllowsWhiteSpaces = true;
            this.textBoxName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxName.ExtraAllowedChars = "";
            this.textBoxName.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxName.Location = new System.Drawing.Point(153, 27);
            this.textBoxName.MaxLength = 19;
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.NonAllowedCharacters = "";
            this.textBoxName.RegularExpresion = "";
            this.textBoxName.Size = new System.Drawing.Size(332, 20);
            this.textBoxName.TabIndex = 0;
            this.textBoxName.TextChanged += new System.EventHandler(this.OperatorStatusParameters_Change);
            // 
            // textBoxExPorcentage
            // 
            this.textBoxExPorcentage.AllowsLetters = false;
            this.textBoxExPorcentage.AllowsNumbers = true;
            this.textBoxExPorcentage.AllowsPunctuation = false;
            this.textBoxExPorcentage.AllowsSeparators = false;
            this.textBoxExPorcentage.AllowsSymbols = false;
            this.textBoxExPorcentage.AllowsWhiteSpaces = false;
            this.textBoxExPorcentage.ExtraAllowedChars = "";
            this.textBoxExPorcentage.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExPorcentage.Location = new System.Drawing.Point(153, 83);
            this.textBoxExPorcentage.MaxLength = 3;
            this.textBoxExPorcentage.Name = "textBoxExPorcentage";
            this.textBoxExPorcentage.NonAllowedCharacters = "";
            this.textBoxExPorcentage.RegularExpresion = "";
            this.textBoxExPorcentage.Size = new System.Drawing.Size(89, 20);
            this.textBoxExPorcentage.TabIndex = 1;
            this.textBoxExPorcentage.TextChanged += new System.EventHandler(this.OperatorStatusParameters_Change);
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOk.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonOk.Appearance.Options.UseFont = true;
            this.buttonOk.Appearance.Options.UseForeColor = true;
            this.buttonOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOk.Enabled = false;
            this.buttonOk.Location = new System.Drawing.Point(322, 112);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(82, 33);
            this.buttonOk.StyleController = this.layoutControl1;
            this.buttonOk.TabIndex = 1;
            this.buttonOk.Text = "Accept";
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // labelExStatusColor
            // 
            this.labelExStatusColor.BackColor = System.Drawing.Color.Red;
            this.labelExStatusColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelExStatusColor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExStatusColor.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExStatusColor.Location = new System.Drawing.Point(153, 51);
            this.labelExStatusColor.Name = "labelExStatusColor";
            this.labelExStatusColor.Size = new System.Drawing.Size(37, 28);
            this.labelExStatusColor.TabIndex = 3;
            this.labelExStatusColor.Click += new System.EventHandler(this.labelExStatusColor_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.emptySpaceItem1,
            this.layoutControlGroupData});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(492, 153);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.buttonOk;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(320, 110);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(86, 37);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(86, 37);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(86, 43);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.buttonCancel;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(406, 110);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(86, 37);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(86, 37);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(86, 43);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 110);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 37);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(110, 37);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(320, 43);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupData
            // 
            this.layoutControlGroupData.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroupData.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemName,
            this.layoutControlItemColor,
            this.emptySpaceItem2,
            this.layoutControlItemPorcentage,
            this.layoutControlItemTolerance});
            this.layoutControlGroupData.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupData.Name = "layoutControlGroupData";
            this.layoutControlGroupData.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupData.Size = new System.Drawing.Size(492, 110);
            this.layoutControlGroupData.Text = "layoutControlGroupData";
            // 
            // layoutControlItemName
            // 
            this.layoutControlItemName.Control = this.textBoxName;
            this.layoutControlItemName.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItemName.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemName.Name = "layoutControlItemName";
            this.layoutControlItemName.Size = new System.Drawing.Size(482, 24);
            this.layoutControlItemName.Text = "layoutControlItemName";
            this.layoutControlItemName.TextSize = new System.Drawing.Size(142, 13);
            // 
            // layoutControlItemColor
            // 
            this.layoutControlItemColor.Control = this.labelExStatusColor;
            this.layoutControlItemColor.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItemColor.FillControlToClientArea = false;
            this.layoutControlItemColor.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItemColor.MaxSize = new System.Drawing.Size(187, 32);
            this.layoutControlItemColor.MinSize = new System.Drawing.Size(187, 32);
            this.layoutControlItemColor.Name = "layoutControlItemColor";
            this.layoutControlItemColor.Size = new System.Drawing.Size(187, 32);
            this.layoutControlItemColor.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemColor.Text = "layoutControlItemColor";
            this.layoutControlItemColor.TextSize = new System.Drawing.Size(142, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(187, 24);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(187, 32);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(187, 32);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(295, 32);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItemPorcentage
            // 
            this.layoutControlItemPorcentage.Control = this.textBoxExPorcentage;
            this.layoutControlItemPorcentage.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItemPorcentage.Location = new System.Drawing.Point(0, 56);
            this.layoutControlItemPorcentage.Name = "layoutControlItemPorcentage";
            this.layoutControlItemPorcentage.Size = new System.Drawing.Size(239, 24);
            this.layoutControlItemPorcentage.Text = "layoutControlItemPorcentage";
            this.layoutControlItemPorcentage.TextSize = new System.Drawing.Size(142, 13);
            // 
            // layoutControlItemTolerance
            // 
            this.layoutControlItemTolerance.Control = this.textBoxExTolerance;
            this.layoutControlItemTolerance.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItemTolerance.Location = new System.Drawing.Point(239, 56);
            this.layoutControlItemTolerance.Name = "layoutControlItemTolerance";
            this.layoutControlItemTolerance.Size = new System.Drawing.Size(243, 24);
            this.layoutControlItemTolerance.Text = "layoutControlItemTolerance";
            this.layoutControlItemTolerance.TextSize = new System.Drawing.Size(142, 13);
            // 
            // checkBoxAvailable
            // 
            this.checkBoxAvailable.AutoSize = true;
            this.checkBoxAvailable.Enabled = false;
            this.checkBoxAvailable.ForeColor = System.Drawing.SystemColors.ControlText;
            this.checkBoxAvailable.Location = new System.Drawing.Point(120, 56);
            this.checkBoxAvailable.Name = "checkBoxAvailable";
            this.checkBoxAvailable.Size = new System.Drawing.Size(75, 17);
            this.checkBoxAvailable.TabIndex = 2;
            this.checkBoxAvailable.Text = "Disponible";
            this.checkBoxAvailable.UseVisualStyleBackColor = true;
            // 
            // OperatorStatusForm
            // 
            this.AcceptButton = this.buttonOk;
            this.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(492, 153);
            this.Controls.Add(this.layoutControl1);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1500, 500);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(433, 191);
            this.Name = "OperatorStatusForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "OperatorStatusForm";
            this.Load += new System.EventHandler(this.OperatorStatusForm_Load);
            this.SizeChanged += new System.EventHandler(this.OperatorStatusForm_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemColor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPorcentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTolerance)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton buttonCancel;
        private DevExpress.XtraEditors.SimpleButton buttonOk;
        private TextBoxEx textBoxName;
        private System.Windows.Forms.CheckBox checkBoxAvailable;
        private System.Windows.Forms.ToolTip toolTipOperatorStatus;
        private LabelEx labelExStatusColor;
        private TextBoxEx textBoxExTolerance;
        private TextBoxEx textBoxExPorcentage;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupData;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemColor;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemPorcentage;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTolerance;
    }
}