using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using SmartCadCore.ClientData;
using SmartCadCore.Core;
using Smartmatic.SmartCad.Service;
using System.Collections;
using System.Reflection;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors.Controls;
using System.Text.RegularExpressions;
using DevExpress.Utils;
using SmartCadCore.Common;
using SmartCadCore.Enums;
using SmartCadControls.Controls;
using SmartCadControls;
using SmartCadGuiCommon.Controls.Administration;

namespace SmartCadGuiCommon
{
    public partial class DepartmentTypeXtraForm : DevExpress.XtraEditors.XtraForm
    {
        private FormBehavior behavior;
        private DepartmentTypeClientData selectedDepartmentType;
        private AdministrationRibbonForm parentForm;
        private object syncCommitted = new object();
        private RepositoryItemComboBox priorityCombo = new RepositoryItemComboBox();
        private RepositoryItemSpinEdit maxValueSpinEdit = new RepositoryItemSpinEdit();
        private bool internalUpdate = false;
        
        public DepartmentTypeClientData SelectedDepartmentType
        {
            get
            {
                return selectedDepartmentType;
            }
            set
            {
                selectedDepartmentType = value;
                if (Behavior == FormBehavior.Edit)
                {
                    FormUtil.InvokeRequired(this,
                        delegate
                        {
                            this.textEditDepartmentName.Text = selectedDepartmentType.Name;

                            checkEditEnabledDispatch.Checked = selectedDepartmentType.Dispatch;

                            if (selectedDepartmentType.Dispatch == false)
                            {
                                textEditPattern.Enabled = false;
                                comboBoxEditFrequency.Enabled = false;
                            }

                            this.comboBoxEditFrequency.SelectedIndex = (int)selectedDepartmentType.RestartFrequency;
                            if (selectedDepartmentType.Pattern != null)
                                this.textEditPattern.Text = selectedDepartmentType.Pattern;

                            if (selectedDepartmentType.Color != null)
                                colorEdit.Color = selectedDepartmentType.Color;


                            if (selectedDepartmentType.Alerts != null && selectedDepartmentType.Alerts.Count > 0)
                            {
                                foreach (DepartmentTypeAlertClientData depAlert in selectedDepartmentType.Alerts)
                                {
                                    foreach (GridControlDataAlert gridData in gridControlExAlerts.Items)
                                    {
                                        if (depAlert.AlertCode == gridData.Code)
                                        {
                                            gridData.DepartmentAlertCode = depAlert.Code;
                                            gridData.Checked = true;
                                            gridData.MaxValue = depAlert.MaxValue;
                                            gridData.Priority = ResourceLoader.GetString2(depAlert.Priority.ToString());
                                            gridData.PriorityValue = (int)depAlert.Priority;
                                            gridData.DepartmentAlertVersion = depAlert.Version;
                                            break;
                                        }
                                    }

                                }

                            }


                        });
                }
                else
                {
                    this.comboBoxEditFrequency.SelectedIndex = 0;
                }
                internalUpdate = false;
                
            }
        }


        public FormBehavior Behavior
        {
            get
            {
                return behavior;
            }
            set
            {
                behavior = value;
            }
        }

        public DepartmentTypeXtraForm(AdministrationRibbonForm parentForm, DepartmentTypeClientData departmentType, FormBehavior behavior)
        {
            InitializeComponent();
            LoadLanguage();
            InitializeDataGrid();
            FillAlerts();
            FillFrequencyCombo();
            this.parentForm = parentForm;
            this.parentForm.AdministrationCommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(DepartmentTypeXtraForm_AdministrationCommittedChanges);
            this.Behavior = behavior;
            this.SelectedDepartmentType = departmentType;
            EnableButtons(null, EventArgs.Empty);
            gridViewExAlerts.Columns["MaxValue"].Visible = false;
        }
        

        void DepartmentTypeXtraForm_AdministrationCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    lock (syncCommitted)
                    {
                        try
                        {
                            if (ServerServiceClient.GetInstance().OperatorClient == null)
                            { }
                        }
                        catch
                        { }

                        if (e.Objects[0] is DepartmentTypeClientData)
                        {
                            #region DepartmentTypeClientData
                            DepartmentTypeClientData department =
                                    e.Objects[0] as DepartmentTypeClientData;
                            if (SelectedDepartmentType.Equals(department))
                            {
                                if (e.Action == CommittedDataAction.Update)
                                {
                                    if (!internalUpdate)
                                    {
                                        MessageForm.Show(ResourceLoader.GetString2("DepartmentTypeWasUpdated") + ": " + SelectedDepartmentType.ToString(), MessageFormType.Warning);
                                    }
                                    SelectedDepartmentType = department;
                                }
                            }
                            else if (e.Action == CommittedDataAction.Delete)
                            {
                                MessageForm.Show(ResourceLoader.GetString2("DeleteFormDepartmentTypeData") + ": " + SelectedDepartmentType.ToString(), MessageFormType.Warning);
                                FormUtil.InvokeRequired(this,
                                delegate
                                {
                                    this.Close();
                                });
                            }
                            #endregion
                        }
                        
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private void SetTooltipInformation()
        {
            string tittle = ResourceLoader.GetString2("Legend");
            string text = "<B>YY|YYYY:</B> " + ResourceLoader.GetString2("YearUpper") + "\r\n" +
                          "<B>MM:</B>  " + ResourceLoader.GetString2("Month") + "\r\n" +
                          "<B>DD:</B>  " + ResourceLoader.GetString2("Day") + "\r\n" +
                          "<B>hh:</B>  " + ResourceLoader.GetString2("HoursUnit") + "\r\n" +
                          "<B>mm:</B>  " + ResourceLoader.GetString2("MinutesUnit") + "\r\n" +
                          "<B>ss:</B>  " + ResourceLoader.GetString2("SecondsUnit") + "\r\n" +
                          "<B>o:</B>  " + ResourceLoader.GetString2("OperatorCode") + "\r\n" +
                          "<B>'" + ResourceLoader.GetString2("Text").ToLower() + "':</B> " + ResourceLoader.GetString2("AlphanumericText") + "\r\n" +
                          "<B>#:</B>  " + ResourceLoader.GetString2("Consecutive");
            string footer = ResourceLoader.GetString2("ExampleShort") + " 'PM'oDDMMYYYYhhmmss#";

            pictureEditHelp.SuperTip = GetSuperToolTip(tittle, text, footer);
        }

        private SuperToolTip GetSuperToolTip(string tooltiptext, string supertooltipcontents, string tooltipfooter)
        {
            ToolTipTitleItem tttitem = new ToolTipTitleItem();
            ToolTipItem ttitem = new ToolTipItem();
            ToolTipSeparatorItem separator = new ToolTipSeparatorItem();
            tttitem.Text = tooltiptext;
            ttitem.LeftIndent = 6;
            ttitem.AllowHtmlText = DefaultBoolean.True;
            ttitem.Text = supertooltipcontents;
            SuperToolTip supertt = new SuperToolTip();
            supertt.Items.Add(tttitem);
            supertt.Items.Add(separator);
            supertt.Items.Add(ttitem);

            if (tooltipfooter != null)
            {
                ToolTipTitleItem tttitemfooter = new ToolTipTitleItem();
                tttitemfooter.Text = tooltipfooter;
                ToolTipSeparatorItem separator2 = new ToolTipSeparatorItem();
                supertt.Items.Add(separator2);
                supertt.Items.Add(tttitemfooter);
            }

            return supertt;
        }

        private void InitializeDataGrid() 
        {
            gridControlExAlerts.Type = typeof(GridControlDataAlert);
            gridControlExAlerts.EnableAutoFilter = true;
            gridControlExAlerts.ViewTotalRows = true;
            gridViewExAlerts.ViewTotalRows = true;
            gridViewExAlerts.OptionsBehavior.Editable = true;
            gridViewExAlerts.OptionsView.ColumnAutoWidth = true;
            gridViewExAlerts.Columns["Checked"].OptionsColumn.ShowCaption = false;
            gridViewExAlerts.Columns["Checked"].Width = 15;
            gridViewExAlerts.Columns["Checked"].OptionsColumn.AllowEdit = true;
            gridViewExAlerts.Columns["Checked"].VisibleIndex = 0;
            gridViewExAlerts.Columns["Checked"].OptionsColumn.AllowMove = false;
            gridViewExAlerts.Columns["MaxValue"].OptionsColumn.AllowEdit = true;
            gridViewExAlerts.Columns["Priority"].OptionsColumn.AllowEdit = true;
            gridViewExAlerts.Columns["Name"].OptionsColumn.AllowEdit = false;
            gridViewExAlerts.Columns["Measure"].OptionsColumn.AllowEdit = false;

            priorityCombo.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            gridViewExAlerts.Columns["Priority"].ColumnEdit = priorityCombo;
            FillPriorityCombo();
            maxValueSpinEdit.MaxValue = 1000;
            maxValueSpinEdit.MinValue = 0;
            maxValueSpinEdit.EditMask = "D";
            gridViewExAlerts.Columns["MaxValue"].ColumnEdit = maxValueSpinEdit;
            
        }

        private void FillFrequencyCombo() 
        {
            comboBoxEditFrequency.Properties.Items.Add(ResourceLoader.GetString2(DepartmentTypeClientData.Frequency.None.ToString()));
            comboBoxEditFrequency.Properties.Items.Add(ResourceLoader.GetString2(DepartmentTypeClientData.Frequency.Daily.ToString()));
            comboBoxEditFrequency.Properties.Items.Add(ResourceLoader.GetString2(DepartmentTypeClientData.Frequency.Weekly.ToString()));
            comboBoxEditFrequency.Properties.Items.Add(ResourceLoader.GetString2(DepartmentTypeClientData.Frequency.Fortnightly.ToString()));
            comboBoxEditFrequency.Properties.Items.Add(ResourceLoader.GetString2(DepartmentTypeClientData.Frequency.Monthly.ToString()));
            comboBoxEditFrequency.Properties.Items.Add(ResourceLoader.GetString2(DepartmentTypeClientData.Frequency.Annual.ToString())); 
       
        }

        private void FillPriorityCombo() 
        {
            priorityCombo.Items.Add(ResourceLoader.GetString2(UnitAlertClientData.AlertPriority.Low.ToString()));
            priorityCombo.Items.Add(ResourceLoader.GetString2(UnitAlertClientData.AlertPriority.Medium.ToString()));
            priorityCombo.Items.Add(ResourceLoader.GetString2(UnitAlertClientData.AlertPriority.High.ToString()));
                   
        }

        private void LoadLanguage()
        {
            layoutControlGroupDepartment.Text = ResourceLoader.GetString2("DepartmentTypeInformation");
            layoutControlGroupAlerts.Text = ResourceLoader.GetString2("Alerts");
            this.layoutControlItemName.Text = ResourceLoader.GetString2("$Message.Name") + ":* ";
            this.layoutControlItemColor.Text = ResourceLoader.GetString2("$Message.Color") + ": ";
            layoutControlItemPattern.Text = ResourceLoader.GetString2("Pattern") + ":*";
            layoutControlItemFrequency.Text = ResourceLoader.GetString2("Frequency") + ":";
            layoutControlGroupFeatures.Text = ResourceLoader.GetString2("DepartmentTypeFeatures");
            layoutControlItemExample.Text = ResourceLoader.GetString2("ExampleShort");
            layoutControlGroupIncidentNotificationIdentify.Text = ResourceLoader.GetString2("IncidentNotificationIndentify");
            this.checkEditEnabledDispatch.Properties.Caption = ResourceLoader.GetString2("DispatchEnabled");
        }

        private void FillAlerts() 
        {
            BindingList<GridControlDataAlert> dataSourceAlert = new BindingList<GridControlDataAlert>();
            IList list = ServerServiceClient.GetInstance().SearchClientObjects(@"SELECT alert FROM AlertData alert");

            foreach (AlertClientData alert in list)
                  dataSourceAlert.Add(new GridControlDataAlert(alert));
            
            gridControlExAlerts.BeginInit();
            gridControlExAlerts.DataSource = dataSourceAlert;
            gridControlExAlerts.EndInit();

        }

        private bool ValidateAlerts() 
        {
            bool result = true;

            foreach (GridControlDataAlert gridData in gridControlExAlerts.Items)
            {
                if (gridData.Checked == true && (gridData.Priority == null || gridData.Priority == string.Empty))
                {
                    result = false;
                    break;
                }
           
            }

            return result;
        }

        private bool ValidatePattern()
        {
            int count;
            string[] patterns = { "YYYY", "YY", "MM", "DD", "hh", "mm", "ss", "o", "#", "'" };
            bool fullYear = false; 

            Regex exp = new Regex("^(YY|MM|DD|hh|mm|ss|o|\'\\w+\')*#(YY|MM|DD|hh|mm|ss|o|\'\\w+\')*$");
            if (textEditPattern.Text.Trim() != string.Empty)
            {
                if (exp.IsMatch(textEditPattern.Text.Trim()) == true)
                {
                    foreach (string st in patterns)
                    {
                        count = FindOccurrences(textEditPattern.Text.Trim(), st);

                        if (st == "'")
                        {
                            if (count > 2)
                                return false;
                        }
                        else if (st == "AA" || st == "YY") 
                        {
                            if (fullYear == true && count > 2)
                                return false;
                            else if (fullYear == false && count > 1)
                                return false;
                        }
                        else
                        {
                            if ((st == "AAAA" || st == "YYYY") && count == 1) 
                                fullYear = true;

                            if (count > 1)
                                return false;
                        }
                    }
                }
                else
                    return false;
            }

            return true;
        }


        private int FindOccurrences(string st, string pattern) 
        {
            int count = 0;
            int index = 0;
            while (true)
            {
                index = st.IndexOf(pattern);
                if (index != -1)
                {
                    st = st.Substring(index + pattern.Length);
                    count++;
                }
                else
                    return count;
            }
           
        }


        private void simpleButtonAccept_Click(object sender, EventArgs e)
        {
            if (ValidateAlerts() == false)
            {           
                MessageForm.Show(ResourceLoader.GetString2("AlertWithoutPriority"), MessageFormType.Error);
                DialogResult = DialogResult.None;
                return;
            }
            if (ValidatePattern() == false)
            {
                if (textEditPattern.Text.Contains("#") == true)
                    MessageForm.Show(ResourceLoader.GetString2("InvalidPattern"), MessageFormType.Error);
                else
                    MessageForm.Show(ResourceLoader.GetString2("InvalidPatternWithoutConsecutive"), MessageFormType.Error);

                textEditPattern.Focus();
                DialogResult = DialogResult.None;
                return;
            }
            
            DepartmentTypeAlertClientData departmentAlert;

            if (Behavior == FormBehavior.Create)
            {
                selectedDepartmentType = new DepartmentTypeClientData();
                selectedDepartmentType.Name = this.textEditDepartmentName.Text;
                selectedDepartmentType.Color = colorEdit.Color;
                selectedDepartmentType.Dispatch = checkEditEnabledDispatch.Checked;

                if (checkEditEnabledDispatch.Checked == true)
                {
                    if (this.textEditPattern.Text.Trim() != string.Empty)
                        selectedDepartmentType.Pattern = this.textEditPattern.Text.Trim();
                    if (comboBoxEditFrequency.SelectedIndex != -1)
                        selectedDepartmentType.RestartFrequency = (DepartmentTypeClientData.Frequency)this.comboBoxEditFrequency.SelectedIndex;
                }
                else 
                {
                    selectedDepartmentType.Pattern = null;
                    selectedDepartmentType.RestartFrequency = DepartmentTypeClientData.Frequency.None;
                }
                    selectedDepartmentType.Alerts = new ArrayList();
                foreach (GridControlDataAlert gridData in gridControlExAlerts.Items)
                    if (gridData.Checked == true)
                    {
                        departmentAlert = new DepartmentTypeAlertClientData();
                        departmentAlert.AlertCode = gridData.Code;
                        departmentAlert.MaxValue = gridData.MaxValue;
                        departmentAlert.Priority = (DepartmentTypeAlertClientData.AlertPriority)gridData.PriorityValue;
                        departmentAlert.Code = gridData.DepartmentAlertCode;
                        departmentAlert.DepartmentTypeCode = 0;
                        departmentAlert.Version = gridData.DepartmentAlertVersion;
                        selectedDepartmentType.Alerts.Add(departmentAlert);
                    }
                try
                {
                    ServerServiceClient.GetInstance().SaveOrUpdateClientData(selectedDepartmentType);
                }
                catch (Exception ex)
                {
                    DialogResult = DialogResult.None;
                    MessageForm.Show(ex.Message, ex);
                    GetErrorFocus(ex);
                }
            }
            else if (Behavior == FormBehavior.Edit)
            {
                DepartmentTypeClientData department = selectedDepartmentType.Clone() as DepartmentTypeClientData;
                department.Name = this.textEditDepartmentName.Text;
                department.Color = colorEdit.Color;
                department.Dispatch = checkEditEnabledDispatch.Checked;

                if (checkEditEnabledDispatch.Checked == true)
                {
                    if (this.textEditPattern.Text.Trim() != string.Empty)
                        department.Pattern = this.textEditPattern.Text.Trim();

                    if (comboBoxEditFrequency.SelectedIndex != -1)
                        department.RestartFrequency = (DepartmentTypeClientData.Frequency)this.comboBoxEditFrequency.SelectedIndex;
                }
                else
                {
                    department.Pattern = null;
                    department.RestartFrequency = DepartmentTypeClientData.Frequency.None;
                }
                
                department.Alerts = new ArrayList();
				foreach (GridControlDataAlert gridData in gridControlExAlerts.Items)
                    if (gridData.Checked == true)
                    {
                        departmentAlert = new DepartmentTypeAlertClientData();
                        departmentAlert.AlertCode = gridData.Code;
                        departmentAlert.MaxValue = gridData.MaxValue;
                        departmentAlert.Priority = (DepartmentTypeAlertClientData.AlertPriority)gridData.PriorityValue;
                        departmentAlert.Code = gridData.DepartmentAlertCode;
                        departmentAlert.DepartmentTypeCode = selectedDepartmentType.Code;
                        departmentAlert.Version = gridData.DepartmentAlertVersion;
                        department.Alerts.Add(departmentAlert);
                    }

				try
                {
                    ServerServiceClient.GetInstance().SaveOrUpdateClientData(department);
                }
                catch (Exception ex)
                {
                    DialogResult = DialogResult.None;
                    internalUpdate = true;
                    MessageForm.Show(ex.Message, ex);
                    ServerServiceClient.GetInstance().AskUpdatedObjectToServer(selectedDepartmentType, null);
                    GetErrorFocus(ex);
                }
            }
        }

        private void GetErrorFocus(Exception ex)
        {
            if (ex.Message.Contains(ResourceLoader.GetString2("UK_DEPARTMENT_TYPE_NAME")))
            {
                if (behavior == FormBehavior.Create)
                {
                    this.textEditDepartmentName.Text = this.textEditDepartmentName.Text;
                }
                this.textEditDepartmentName.Focus();
            }

        }

		private void DepartmentTypeXtraForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.parentForm != null)
            {
                this.parentForm.AdministrationCommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(DepartmentTypeXtraForm_AdministrationCommittedChanges);
            }
        }

        private void EnableButtons(object sender, EventArgs e)
        {
            FormUtil.InvokeRequired(this,
                delegate
                {
                    if (this.textEditDepartmentName.Text.Trim().Length > 0 && 
                        (checkEditEnabledDispatch.Checked == false || (textEditPattern.Text != string.Empty && checkEditEnabledDispatch.Checked == true)))
                    {
                        this.simpleButtonAccept.Enabled = true;
                    }
                    else
                    {
                        this.simpleButtonAccept.Enabled = false;
                    }                    
                });
        }

        private void DepartmentTypeXtraForm_Load(object sender, EventArgs e)
        {
            labelControlExample.Text = string.Empty;
           
            this.Icon = ResourceLoader.GetIcon("$Icon.DepartmentType");
            if (Behavior == FormBehavior.Create)
            {
                this.Text = ResourceLoader.GetString2("FormCreateDepartmentType");
                this.simpleButtonAccept.Text = ResourceLoader.GetString2("Create");
            }
            else if (Behavior == FormBehavior.Edit)
            {
                this.Text = ResourceLoader.GetString2("FormEditDepartmentType");
                this.simpleButtonAccept.Text = ResourceLoader.GetString2("Accept");
            }

            this.simpleButtonCancel.Text = ResourceLoader.GetString2("$Message.Cancel");
            priorityCombo.SelectedIndexChanged += new EventHandler(priorityCombo_SelectedIndexChanged);
            SetTooltipInformation();

            if (this.Behavior == FormBehavior.Edit)
            {
                if (textEditPattern.Text.Trim() != string.Empty)
                {
                    labelControlExample.Text = GenerateNotificationCustomCode(textEditPattern.Text.Trim());
                }
            }
        }

        void priorityCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (gridViewExAlerts.FocusedRowHandle > -1) 
            {
                GridControlDataAlert gridData = gridControlExAlerts.SelectedItems[0] as GridControlDataAlert;
                
                gridData.PriorityValue = (int)((ComboBoxEdit)sender).SelectedIndex; 
                
            }
       
        }

        private void gridViewExAlerts_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            //if (e.RowHandle > -1)
            //{
            //    if (e.Column.FieldName == "Priority" || e.Column.FieldName == "MaxValue")
            //    {
            //        ((GridControlDataAlert)gridViewExAlerts.GetRow(e.RowHandle)).Checked = true;
            //    }
            //    else if (e.Column.FieldName == "Checked") {
            //        if ((bool)e.Value == false)
            //        {
            //            ((GridControlDataAlert)gridViewExAlerts.GetRow(e.RowHandle)).Priority = string.Empty;
            //            ((GridControlDataAlert)gridViewExAlerts.GetRow(e.RowHandle)).PriorityValue =  0;
            //            ((GridControlDataAlert)gridViewExAlerts.GetRow(e.RowHandle)).MaxValue = 0;                        
            //        }

            //    }
            //}
          
        }

        private void gridViewExAlerts_ShowingEditor(object sender, CancelEventArgs e)
        {
            if (gridViewExAlerts.FocusedRowHandle > -1)
            {
                GridControlDataAlert gridData = gridControlExAlerts.SelectedItems[0] as GridControlDataAlert;
                if (string.IsNullOrEmpty(gridData.Measure))
                {
                    if (gridViewExAlerts.FocusedColumn.FieldName == "MaxValue")
                        e.Cancel = true;
                }
            }
            
            
           
        }

        private void comboBoxEditFrequency_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableButtons(null, null);
        }

        private void textEditPattern_TextChanged(object sender, EventArgs e)
        {
            if (textEditPattern.Text.Trim() != string.Empty)
            {
                layoutControlItemExample.Text = ResourceLoader.GetString2("ExampleShort");
                labelControlExample.Text = GenerateNotificationCustomCode(textEditPattern.Text.Trim());
            }

            EnableButtons(null, null);
        }

        private string GenerateNotificationCustomCode(string pattern)
        {
            string customCode = string.Empty;
            ArrayList parsingPattern = new ArrayList();
            Random r = new Random();

            int counter = r.Next(100);
            int operatorCode = r.Next(100);
            
            //Obtengo un arreglo con cada seccion del patron
            parsingPattern = GetParsingPattern(pattern);

            foreach (string st in parsingPattern)
            {
                if (st == "o")
                    customCode += operatorCode;
                else if (st == "#")
                    customCode += counter.ToString();
                else if (st.Contains("'") == true)
                    customCode += st.Replace("'", "");
                else if (st == "YY")
                    customCode += DateTime.Now.ToString("yy");
                else if (st == "YYYY")
                    customCode += DateTime.Now.ToString("yyyy");
                else if (st == "MM")
                    customCode += DateTime.Now.ToString("MM");
                else if (st == "DD")
                    customCode += DateTime.Now.ToString("dd");
                else if (st == "hh")
                    customCode += DateTime.Now.ToString("hh");
                else if (st == "ss")
                    customCode += DateTime.Now.ToString("ss");
                else if (st == "mm")
                    customCode += DateTime.Now.ToString("mm");
            }


            return customCode;
        }


        private ArrayList GetParsingPattern(string pattern)
        {
            ArrayList result = new ArrayList();
            string st = string.Empty;
            bool skip = false;

            for (int i = 0; i < pattern.Length; i++)
            {
                if (pattern[i].ToString() == "'")
                    skip = !skip;

                st += pattern[i].ToString();

                if ((pattern.Length - 1 == i || pattern[i] != pattern[i + 1]) && skip == false)
                {
                    result.Add(st);
                    st = string.Empty;
                }
            }

            return result;
        }

        private void checkEditEnabledDispatch_CheckedChanged(object sender, EventArgs e)
        {
            comboBoxEditFrequency.Enabled = checkEditEnabledDispatch.Checked;
            textEditPattern.Enabled = checkEditEnabledDispatch.Checked;
            textEditPattern.Text = string.Empty;
            EnableButtons(sender,e);
        
        }

    }

}