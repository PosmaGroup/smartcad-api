using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using Smartmatic.SmartCad.Service;
using SmartCadCore.ClientData;
using SmartCadControls.Controls;
using SmartCadControls;

namespace Smartmatic.SmartCad.Gui
{
     public class GridControlDataCctvSecurityOperator: GridControlData
    {

         public GridControlDataCctvSecurityOperator(OperatorClientData oper)
        :base(oper){
        
        }
                 

        public OperatorClientData Operator
        {
            get { return this.Tag as OperatorClientData; }
        }

        public int Code
        {
            get { return Operator.Code; }
        }

        [GridControlRowInfoData(Visible = true, Width = 155)]
        public string FirstName
        {
            get { return Operator.FirstName; }
        }

        [GridControlRowInfoData(Visible = true, Width = 155)]
        public string LastName
        {
            get { return Operator.LastName; }
        }

        [GridControlRowInfoData(Visible = true, Width = 155)]
        public string Role
        {
            get { return Operator.RoleFriendlyName; }
        }

        
        public override bool Equals(object obj)
        {
            bool result = false;
            GridControlDataCctvSecurityOperator var = obj as GridControlDataCctvSecurityOperator;
            if (var != null)
            {
                if (this.Code == var.Code)
                    result = true;
            }
            return result;
        }


    }
   
}
