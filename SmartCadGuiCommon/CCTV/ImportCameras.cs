﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraLayout.Utils;
using Smartmatic.SmartCad.Service;
using System.Collections;
using System.ServiceModel;
using System.Windows.Forms;
using SmartCadGuiCommon;
using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadCore.ClientData;
using Smartmatic.SmartCad.Vms;


namespace Smartmatic.SmartCad.Gui.Classes
{
    class ImportCameras
    {
        #region Fields
        private string login;
        private string password;
        private string serverIp;
        private string portVms;
        private bool delete = false;
        private int deletes = 0;
        private bool failed = false;
        private int fail = 0;
        private int newcam  = 0;
        private ImportManager importManager;

        #endregion

        //public void Initialize(string login, string password, string serverIp, int portVms)
        //{
        //    this.login = login;
        //    this.password = password;
        //    this.serverIp = serverIp;
        //    this.portVms = portVms;
        //}

        public void Initialize(string login, string password, string serverIp, string port)
        {
            this.login = login;
            this.password = password;
            this.serverIp = serverIp;
            this.portVms = port;
        }

        public void ExportFromVMS()
        {
            ProgressForm form = new ProgressForm(new TaskFunctionsDelegate(Export), new object[0] {  }, ResourceLoader.GetString2("ImportingCameras"));
            form.ButtonCancelVisibility = LayoutVisibility.Always;
            form.CanThrowError = true;
            form.ShowDialog();
            form.Dispose();
        }

        private void Export(ChangeTaskDelegate changeTaskFunction, ChangeValueDelegate changeValueFunction, object[] arguments)
        {
            string serverIpPort = serverIp + ":" + portVms;

            changeTaskFunction(ResourceLoader.GetString2("ConnectingVMSServer"));
            changeValueFunction(10);
            importManager = ImportManager.GetInstance(ServerServiceClient.GetInstance().GetConfiguration().VmsDllName);
            importManager.Initialize(serverIpPort, login, password);
            int aux = importManager.Export();

            if (importManager.Cameras.Count != 0)
            {
                if (aux == 1)
                {
                    changeTaskFunction(ResourceLoader.GetString2("ImportingCameras"));
                    changeValueFunction(60);

                    IList list = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetAllCameras);
                    IList conList = ServerServiceClient.GetInstance().SearchClientObjects(typeof(ConnectionTypeClientData));
                    IList canList = ServerServiceClient.GetInstance().SearchClientObjects(typeof(CameraTypeClientData));

                    foreach (string cam in importManager.Cameras)
                    {
                        string[] data = cam.Split('\\');

                        bool tempflag = true;
                        if (data.Count() > 1)
                        {
                            foreach (CameraClientData camera in list)
                            {
                                if (data[1] == camera.CustomId)
                                {
                                    tempflag = false;
                                    break;
                                }
                            }
                        }
                        try
                        {
                            if (tempflag)
                            {
                                //string[] words = data[0].Split('(');
                                //string ip = words[1].Substring(0, words[words.Length - 1].Length).Split(')')[0];
                                string ip = data[0];
                                CameraClientData cameranew = new CameraClientData();
                                cameranew.Type = (canList[1] as CameraTypeClientData);
                                cameranew.ConnectionType = (conList[3] as ConnectionTypeClientData);
                                cameranew.Name = data[0];
                                cameranew.CustomId = data[1];
                                cameranew.Ip = ip;
                                cameranew.IpServer = serverIp;
                                cameranew.Port = "0";
                                cameranew.Login = "0";
                                cameranew.Password = "0";
                                cameranew.FrameRate = "0";
                                cameranew.StreamType = "0";
                                cameranew.Resolution = "0";
                                ServerServiceClient.GetInstance().SaveOrUpdateClientData(cameranew);
                                newcam++;
                            }
                        }
                        catch (Exception e)
                        {
                        }
                    }
                    list = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetAllCameras);
                    changeTaskFunction(ResourceLoader.GetString2("RefreshCamerasimported"));
                    changeValueFunction(90);
                    foreach (CameraClientData camera in list)
                    {
                        bool tempflag = true;
                        foreach (string cam in importManager.Cameras)
                        {
                            string[] data = cam.Split('\\');
                            if (data[1] == camera.CustomId)
                            {
                                tempflag = false;
                                break;
                            }
                        }
                        if (tempflag)
                        {
                            try
                            {
                                ServerServiceClient.GetInstance().DeleteClientObject(camera);
                                delete = true;
                                deletes++;
                            }
                            catch (Exception ex)
                            {
                                failed = true;
                                fail++;
                            }
                        }

                    }
                    changeValueFunction(100);

                    string temp = string.Empty;
                    if (newcam > 0)
                        temp = ResourceLoader.GetString2("CamerasNewCount", newcam.ToString()) + "\n";

                    if (delete)
                        temp = temp + ResourceLoader.GetString2("CamerasDelete", deletes.ToString()) + "\n";

                    if (failed)
                        temp = temp + ResourceLoader.GetString2("CamerasNotDelete", fail.ToString()) + "\n";
                    if (!temp.Equals(string.Empty))
                        MessageForm.Show(temp, MessageFormType.Information);
                }
                else
                {
                    MessageForm.Show(ResourceLoader.GetString2("NotVMSConfigured"), MessageFormType.Error);
                }
            }
            else { importManager.Disconnect(); }
        }


    }
}
