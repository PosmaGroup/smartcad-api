﻿using SmartCadGuiCommon.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadGuiCommon.CCTV.Nodes
{
    public class CctvTemplateNode : CctvNode
    {
        //The Name prop represents the file name
        public string RenderMethod { get; set; }
        public TemplatePanelControl PanelControl { get; set; }
        public List<CctvCameraNode> Cameras = new List<CctvCameraNode>();

        public CctvTemplateNode()
        {
            Name = "";
        }

        public CctvTemplateNode(string fileName, string text, string renderMethod)
            : base(fileName, text)
        {
            RenderMethod = renderMethod;
        }

        public override bool Equals(object obj)
        {
            if (obj is CctvTemplateNode)
            {
                return this.Name == ((CctvTemplateNode)obj).Name;
            }
            return false;
        }
    }
}
