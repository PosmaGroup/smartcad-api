﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadGuiCommon.CCTV.Nodes
{
    public class CctvNode : Object
    {
        public string Name { get; set; }
        public string Text { get; set; }

        public CctvNode()
        {
        }

        public CctvNode(string name, string text)
        {
            Name = name;
            Text = text;
        }

        public override string ToString()
        {
            return Text;
        }
    }
}
