﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using DevExpress.XtraTreeList;
using Smartmatic.SmartCad.Service;
using DevExpress.XtraLayout;
using System.Threading;
using System.Collections;
using System.IO;
using System.Text.RegularExpressions;
using System.Text;
using System.ComponentModel;
using DevExpress.XtraEditors.Repository;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
//using SmartCadControls.Controls;
using SmartCadCore.Core;
using SmartCadControls;
using Smartmatic.SmartCad.Vms;
using SmartCadControls.Controls;
using SmartCadGuiCommon.Classes;
using SmartCadGuiCommon.CCTV.Nodes;
using SmartCadGuiCommon.Controls;
using SmartCadControls.Interfaces;
using SlimDX.DirectInput;
using VideoOS.Platform;

namespace SmartCadGuiCommon
{
    public partial class CameraVisualizeForm : DevExpress.XtraEditors.XtraForm, IDragManager
    {
        #region Fields

        private IList userCameras = new ArrayList();
        public DevExpress.XtraLayout.LayoutControlItem DragItem { get; set; }
        string[] cameraTemplates;
        private ServerServiceClient serverServiceClient;
        public System.Threading.Timer joyTimer;
        private ConfigurationClientData config = null;
        internal bool joyActive = false;
        private Joystick Joy;
        private JoystickManager JoyMan;
        private CctvFrontClientFormDevX cctvFrontClientFormDevX;

        #endregion

        #region Properties
        public ServerServiceClient ServerServiceClient
        {
            get
            {
                return serverServiceClient;
            }
            set
            {
                serverServiceClient = value;
            }
        }

        public CameraPanelControl SelectedCamera
        {
            get;
            set;
        }

        public CctvTemplateNode SelectedTemplate
        {
            get;
            set;
        }
        #endregion

        #region Constructor
        public CameraVisualizeForm()
        {
            InitializeComponent();
            InitGrid();

            config = ServerServiceClient.GetInstance().GetConfiguration();

            cctvFrontClientFormDevX = new CctvFrontClientFormDevX();
        }
        #endregion

        /// <summary>
        /// Initialize Grid Control EX of Alarms
        /// </summary>
        private void InitGrid()
        {
            gridControlCameras.EnableAutoFilter = true;
            gridControlCameras.AllowDrop = true;
            gridViewCameras.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            gridViewCameras.OptionsView.ShowAutoFilterRow = true;
            gridViewCameras.OptionsCustomization.AllowFilter = false;

            gridControlTemplates.EnableAutoFilter = true;
            //gridViewTemplates.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            //gridViewTemplates.OptionsView.ShowAutoFilterRow = true;
            //gridViewTemplates.OptionsCustomization.AllowFilter = false;   
        }

        private void CameraVisualizeForm_Load(object sender, EventArgs e)
        {
            LoadLanguage();

            #region Joystick
            ConnectJoy();

            if (Joy != null)
            {
                joyActive = true;
            }
            JoyMan = new JoystickManager();
            JoyMan.Init();
            JoyMan.JoystickAddedEvent += new EventHandler(JoyMan_JoystickAddedEvent);
            JoyMan.JoystickRemovedEvent += new EventHandler(JoyMan_JoystickRemovedEvent);

            #endregion
        }

        /// <summary>
        /// Initialize Captions and Text messages for each controls in the form
        /// gets from the resources
        /// </summary>
        private void LoadLanguage()
        {
            Text = ResourceLoader.GetString2("CameraVisualizeForm");

            barButtonItemPrint.Caption = ResourceLoader.GetString2("Print");
            barButtonItemRefresh.Caption = ResourceLoader.GetString2("Refresh");
            barButtonItemSave.Caption = ResourceLoader.GetString2("Save");
            ribbonPageGroupGeneralOptions.Text = ResourceLoader.GetString2("GeneralOptions");
            ribbonPage1.Text = ResourceLoader.GetString2("Cctv");

            repositoryItemButtonUndo.Buttons[0].ToolTip = ResourceLoader.GetString2("Undo");
            repositoryItemButtonSave.Buttons[0].ToolTip = ResourceLoader.GetString2("Save");
            repositoryItemButtonEdit.Buttons[0].ToolTip = ResourceLoader.GetString2("Edit");
            repositoryItemButtonCancel.Buttons[0].ToolTip = ResourceLoader.GetString2("Delete");
        }

        private void ConnectJoy()
        {

            DirectInput input = new DirectInput();
            IList<SlimDX.DirectInput.DeviceInstance> deviceList = input.GetDevices(SlimDX.DirectInput.DeviceType.Joystick, DeviceEnumerationFlags.AttachedOnly);

            //DeviceList deviceList = Manager.GetDevices(DeviceType.Joystick, EnumDevicesFlags.AttachedOnly);

            SlimDX.DirectInput.DeviceInstance deviceInstance = null;
            for (int i = 0; i < deviceList.Count; i++)
            {
                //deviceList.MoveNext();
                deviceInstance = (SlimDX.DirectInput.DeviceInstance)deviceList[i];
                Joy = new Joystick(input, deviceInstance.ProductGuid);
                SlimDX.DirectInput.JoystickState state = Joy.GetCurrentState();
                break;
            }

            if (deviceInstance != null && deviceInstance.InstanceName != "")
            {
                joyTimer = ApplicationUtil.RecurrentExecuteMethod(
                   new SimpleDelegate(
                   delegate
                   {
                       timerJoystick_Tick(null, null);
                   }), null, null, 200, 200);
            }
        }

        void JoyMan_JoystickRemovedEvent(object sender, EventArgs e)
        {
            Joy.Dispose();
            joyActive = false;
        }

        void JoyMan_JoystickAddedEvent(object sender, EventArgs e)
        {
            ConnectJoy();
            joyActive = true;

        }

        /// <summary>
        /// Load Initial data of camera
        /// cameras and templates
        /// </summary>
        public void LoadInitialData()
        {
            #region Load Cameras
            LoadCameras();
            #endregion

            #region Load Camera Templates

            gridControlTemplates.BeginUpdate();
            gridControlTemplates.DataSource = ((CctvFrontClientFormDevX)this.MdiParent).Templates;
            gridControlTemplates.EndUpdate();

            gridViewTemplates.NewItemRowText = ResourceLoader.GetString2("Click here to add a new Template");

            #endregion
        }

        public void LoadCameras()
        {
            BindingList<CctvCameraNode> source = ((CctvFrontClientFormDevX)this.MdiParent).Cameras;
            gridControlCameras.BeginUpdate();
            gridControlCameras.DataSource = source;
            gridControlCameras.EndUpdate();

            //if (source.Count > 0)
            //{
            //    VmsControlEx vmsControl = VmsControlEx.GetInstance(config.VmsDllName);
            //    vmsControl.Initialize("", config.VmsLogin, config.VmsPassword, config.VmsServerIp);
            //    vmsControl.Connect();
            //}
        }

        private void timerJoystick_Tick(object sender, EventArgs e)
        {
            //try
            //{
            //    if (joyActive && SelectedCamera != null)
            //    {
            //        ArrayList joystickAxisList = Joy.JoystickAxisList as ArrayList;
            //        int X = 0;
            //        int Y = 0;
            //        int Z = 0;
            //        foreach (JoystickAxisInput input in joystickAxisList)
            //        {
            //            int value = input.Value;
            //            #region AxisX
            //            if (input.ToString() == "AxisX")
            //            {
            //                X = value;
            //            }
            //            #endregion
            //            #region AxisY
            //            if (input.ToString() == "AxisY")
            //            {
            //                Y = value;
            //            }
            //            #endregion
            //            #region AxisZ
            //            if (input.ToString() == "AxisZ")
            //            {
            //                Z = value;
            //            }
            //            #endregion
            //        }
            //        SelectedCamera.PTZMove(X, Y, Z);
            //    }
            //}
            //catch { }
        }

        void CameraVisualizeForm_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
            }
            else
            {
                JoyMan.JoystickAddedEvent -= JoyMan_JoystickAddedEvent;
                JoyMan.JoystickRemovedEvent -= JoyMan_JoystickRemovedEvent;
                if (joyTimer != null)
                    joyTimer.Dispose();
                JoyMan.Close();
                gridViewTemplates.CustomRowCellEdit -= gridViewTemplates_CustomRowCellEdit;
            }
        }

        void IDragManager.SetDragCursor(DragDropEffects effect)
        {
            SetDragCursor(effect);
        }

        private void SetDragCursor(DragDropEffects e)
        {
            if (e == DragDropEffects.Move)
                Cursor = Cursors.AppStarting;
            if (e == DragDropEffects.Copy)
                Cursor = Cursors.AppStarting;
            if (e == DragDropEffects.None)
                Cursor = Cursors.No;
        }

        #region Templates 
        int currentTemplateRowHandle = -1;
        private void gridViewTemplates_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            e.Column.ColumnEdit = null;

            if (e.RowHandle != GridControlEx.NewItemRowHandle && e.RowHandle == ((GridViewEx)sender).GetSelectedRows()[0])
            {
                switch (e.Column.FieldName)
                {
                    case "Undo":
                        e.RepositoryItem = repositoryItemButtonUndo;
                        break;
                    case "Save":
                        e.RepositoryItem = repositoryItemButtonSave;
                        break;
                    case "Edit":
                        e.RepositoryItem = repositoryItemButtonEdit;
                        break;
                    case "Cancel":
                        e.RepositoryItem = repositoryItemButtonCancel;
                        break;
                    default:
                        break;
                }
            }
        }

        private void gridViewTemplates_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                if (gridViewTemplates.FocusedRowHandle > -1)
                {

                    
                    CctvTemplateNode template = gridViewTemplates.GetFocusedRow() as CctvTemplateNode;
                    ((CctvFrontClientFormDevX)this.MdiParent).barButtonItemStartRegIncident.Enabled = false;

                    if (SelectedCamera != null)
                    {
                        
                        SelectedCamera.Controls.Clear();
                        SelectedCamera.CameraPanelControlTemplates();
                        cctvFrontClientFormDevX.CheckUnpin();
                    }
                        if (SelectedTemplate != null && template !=SelectedTemplate)
                    {
                        
                        SelectedTemplate.PanelControl.DisposePanelControl();
                        SelectedTemplate.PanelControl.dragDropLayoutControl1.Dispose();
                        SelectedTemplate.PanelControl.dragDropLayoutControl1.layoutControl.Dispose();
                        layoutControlDisplayArea.Controls.Clear();
                        cctvFrontClientFormDevX.CheckUnpin();
                    
                    }
                        if (template != null && layoutControlDisplayArea != null &&
                            (SelectedTemplate != template || SelectedTemplate.PanelControl.HasChanged()))
                    {
                        layoutControlDisplayArea.Clear();
                        layoutControlDisplayArea.BeginUpdate();
                        
                        template.PanelControl = new TemplatePanelControl(template, PlayModes.Live, config);
                        
                        template.PanelControl.SelectionChanged += PanelControl_SelectionChanged;
                        
                       // LayoutControlItem dragItem = new LayoutControlItem();

                        LayoutControlItem dragItem = layoutControlDisplayArea.AddItem("", template.PanelControl);
                        dragItem.Name = template.Name;
                        dragItem.TextVisible = false;
                        dragItem.Padding = new DevExpress.XtraLayout.Utils.Padding(2);
                 //       dragItem.Control = template.PanelControl.dragDropLayoutControl1;
              //          dragItem.SizeConstraintsType = SizeConstraintsType.Default;

                        
                        //layoutControlDisplayArea.Controls.Clear();
                        //layoutControlDisplayArea.HiddenItems.Clear();
                        //layoutControlDisplayArea.AddItem(dragItem);
                        //layoutControlDisplayArea.BestFit();

                        layoutControlDisplayArea.EndUpdate();

                        layoutControlDisplayArea.Invalidate();
                        layoutControlDisplayArea.ResumeLayout();

                    }
                    SelectedTemplate = template;
                    SelectedCamera = null;
                }
            }
            catch { }
        }

        void PanelControl_SelectionChanged(CameraPanelControl camera)
        {
            SelectCamera(camera);
        }

        /// <summary>
        /// Set the current selected camera. Use "camera" == null to clear selection.
        /// </summary>
        /// <param name="camera"></param>
        void SelectCamera(CameraPanelControl camera)
        {
            if (SelectedCamera != null)
            {
                if (camera == null)
                {
                    SelectedCamera.DeSelect();
                    ((CctvFrontClientFormDevX)this.MdiParent).barButtonItemStartRegIncident.Enabled = false;
                }
                else
                {
                    ((CctvFrontClientFormDevX)this.MdiParent).barButtonItemStartRegIncident.Enabled = true;
                    SelectedCamera.DeSelect();
                }
            }
            else
            {
                if (camera != null)
                    ((CctvFrontClientFormDevX)this.MdiParent).barButtonItemStartRegIncident.Enabled = true;
            }
            SelectedCamera = camera;
        }

        private void gridViewTemplates_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle == GridControlEx.NewItemRowHandle)
            {
                gridColumnName.OptionsColumn.AllowEdit = true;
                gridColumnUndo.Visible = false;
                gridColumnEdit.Visible = false;
                gridColumnSave.Visible = false;
                gridColumnCancel.Visible = false;
            }
            else
            {
                if (editing && currentTemplateRowHandle != e.FocusedRowHandle)
                {
                    repositoryItemButtonEdit_Click(null, null);
                }
                gridColumnName.OptionsColumn.AllowEdit = false;
                gridColumnUndo.Visible = true;
                gridColumnUndo.VisibleIndex = 2;
                gridColumnEdit.Visible = true;
                gridColumnEdit.VisibleIndex = 4;
                gridColumnSave.Visible = true;
                gridColumnSave.VisibleIndex = 3;
                gridColumnCancel.Visible = true;
                gridColumnCancel.VisibleIndex = 5;
            }

            currentTemplateRowHandle = e.FocusedRowHandle;
        }

        private void gridViewTemplates_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            try
            {
                CctvTemplateNode template = e.Row as CctvTemplateNode;

                if (string.IsNullOrEmpty(template.Name))
                {
                    if (SelectedTemplate != null)
                    {
                        SelectedTemplate.PanelControl.DisposePanelControl();
                    }
                    else
                    {
                        SelectedCamera.Controls.Clear();
                        SelectedCamera.CameraPanelControlTemplates();
                    }
                    
                    layoutControlDisplayArea.SuspendLayout();
                    layoutControlDisplayArea.Clear();
                    layoutControlDisplayArea.ResumeLayout();

                    template.Name = SmartCadConfiguration.CameraTemplates + template.Text + ".xml";
                    StreamWriter writer = new StreamWriter(template.Name, false);
                    if (template.PanelControl != null) writer.Write(template.PanelControl.GetRenderMethod());
                    writer.Close();
                    writer.Dispose();
                }
            }
            catch (Exception ex) { SmartLogger.Print(ex); }
        }

        bool editing = false;
        private void repositoryItemButtonEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (editing)
                {
                    gridColumnName.OptionsColumn.AllowEdit = false;
                    repositoryItemButtonSave.Buttons[0].Enabled = false;
                    repositoryItemButtonCancel.Buttons[0].Enabled = true;
                    repositoryItemButtonUndo.Buttons[0].Enabled = false;

                    SelectedTemplate.PanelControl.EndEdit();
                    gridViewTemplates_DoubleClick(null, null);
                }
                else
                {
                    CctvTemplateNode selectedTemplate = gridViewTemplates.GetFocusedRow() as CctvTemplateNode;

                //    if (SelectedTemplate != null) SelectedTemplate.PanelControl.ClearConnectionsEditButton();

                    if (SelectedTemplate == null ||
                        SelectedTemplate.Name != selectedTemplate.Name || SelectedTemplate.PanelControl.HasChanged())
                    {
                        
                        gridViewTemplates_DoubleClick(null, null);
                    }

                    gridColumnName.OptionsColumn.AllowEdit = true;
                    repositoryItemButtonSave.Buttons[0].Enabled = true;
                    repositoryItemButtonCancel.Buttons[0].Enabled = false;
                    repositoryItemButtonUndo.Buttons[0].Enabled = true;

                    SelectedTemplate.PanelControl.StartEdit();
                }
            }
            catch { }
            editing = !editing;
        }

        private void repositoryItemButtonCancel_Click(object sender, EventArgs e)
        {
            if (repositoryItemButtonCancel.Buttons[0].Enabled == true)
            {
                try
                {
                    CctvTemplateNode selectedTemplate = gridViewTemplates.GetFocusedRow() as CctvTemplateNode;

                    if (File.Exists(selectedTemplate.Name))
                    {
                        if (MessageForm.Show("Are you sure you want to delete this Template?", MessageFormType.Question)
                            == DialogResult.Yes)
                        {
                            File.Delete(selectedTemplate.Name);
                            gridViewTemplates.DeleteSelectedRows();

                            ClearDisplayArea();

                            ((CctvFrontClientFormDevX)this.MdiParent).DeleteTemplate(selectedTemplate);
                        }
                    }
                }
                catch { }
            }
        }

        public void ClearDisplayArea()
        {
            layoutControlDisplayArea.SuspendLayout();
            layoutControlDisplayArea.Clear();
            layoutControlDisplayArea.ResumeLayout();
        }

        public void SaveTemplate()
        {

             CctvTemplateNode template = gridViewTemplates.GetFocusedRow() as CctvTemplateNode;

            
            if (SelectedTemplate != null & template != null ) { 
                 if (SelectedTemplate.PanelControl.HasChanged() || template.Text != template.Name.Substring(template.Name.LastIndexOf('\\') + 1, template.Text.Length))
                 {
                     if (!string.IsNullOrEmpty(template.Name))//deleting the old file
                     {
                         FileInfo prevFile = new FileInfo(template.Name);
                         if (prevFile.Exists)
                             prevFile.Delete();
                     }

                     template.Name = SmartCadConfiguration.CameraTemplates + template.Text + ".xml";
                     template.RenderMethod = template.PanelControl.GetRenderMethod();
                     StreamWriter writer = new StreamWriter(template.Name, false);
                     writer.Write(template.RenderMethod);
                     writer.Close();
                     writer.Dispose();

                    // template.Cameras.Clear();
                         foreach (string cameraName in template.PanelControl.GetCamerasName())
                         {
                             foreach (CctvCameraNode camera in gridViewCameras.DataSource as BindingList<CctvCameraNode>)
                             {
                                 if (camera.Name == cameraName)
                                 {
                                     template.Cameras.Add(new CctvCameraNode(camera.Camera));
                                     break;
                                 }
                             }
                         }
                     //Saves the new layout
                     ((CctvFrontClientFormDevX)this.MdiParent).UpdateTemplate(SelectedTemplate);
                     if (SelectedTemplate != null) SelectedTemplate.PanelControl.DisposePanelControl();
                 }
             }

        }

        private void repositoryItemButtonSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (repositoryItemButtonSave.Buttons[0].Enabled == true)
                {
                    CctvTemplateNode template = gridViewTemplates.GetFocusedRow() as CctvTemplateNode;
                    if (SelectedTemplate.PanelControl.HasChanged() || template.Text != template.Name.Substring(template.Name.LastIndexOf('\\') + 1, template.Text.Length))
                    {
                        if (!string.IsNullOrEmpty(template.Name))//deleting the old file
                        {
                            FileInfo prevFile = new FileInfo(template.Name);
                            if (prevFile.Exists)
                                prevFile.Delete();
                        }

                        template.Name = SmartCadConfiguration.CameraTemplates + template.Text + ".xml";
                        template.RenderMethod = template.PanelControl.GetRenderMethod();
                        StreamWriter writer = new StreamWriter(template.Name, false);
                        writer.Write(template.RenderMethod);
                        writer.Close();
                        writer.Dispose();

                        
                       // template.Cameras.Clear();
                        foreach (string cameraName in template.PanelControl.GetCamerasName())
                        {
                            foreach (CctvCameraNode camera in gridViewCameras.DataSource as BindingList<CctvCameraNode>)
                            {
                                if (camera.Name == cameraName)
                                {
                                    template.Cameras.Add(new CctvCameraNode(camera.Camera));
                                    break;
                                }
                            }
                        }

                        //Saves the new layout
                        ((CctvFrontClientFormDevX)this.MdiParent).UpdateTemplate(SelectedTemplate);

                        if (SelectedTemplate != null) SelectedTemplate.PanelControl.DisposePanelControl();
                       
                    }
                    repositoryItemButtonEdit_Click(null, null);
                }
            }
            catch { }
        }

        void repositoryItemButtonUndo_Click(object sender, System.EventArgs e)
        {
            if (repositoryItemButtonUndo.Buttons[0].Enabled == true)
            {
                SelectedTemplate.PanelControl.dragDropLayoutControl1.layoutControl.UndoManager.Undo();
            }
        }

        #endregion

        public void StartPanel()
        {
            if (gridViewCameras.FocusedRowHandle > -1)
            {
                CctvCameraNode camera = gridViewCameras.GetFocusedRow() as CctvCameraNode;
                if (SelectedCamera != null)
                {
                    SelectedCamera.Controls.Clear();
                    SelectedCamera.CameraPanelControlTemplates();
                    cctvFrontClientFormDevX.CheckUnpin();
                }

                if (SelectedTemplate != null)
                {
                    SelectedTemplate.PanelControl.DisposePanelControl();
                    SelectedTemplate.PanelControl.dragDropLayoutControl1.Dispose();
                    SelectedTemplate.PanelControl.dragDropLayoutControl1.layoutControl.Dispose();
                    layoutControlDisplayArea.Controls.Clear();
                    cctvFrontClientFormDevX.CheckUnpin();

                }

                    if (camera != null)
                {
                    LayoutControlItem dragItem = new LayoutControlItem();
                    dragItem.Name = camera.Name;
                    dragItem.Text = "";
                    dragItem.TextVisible = false;
                    dragItem.Padding = new DevExpress.XtraLayout.Utils.Padding(2);
                    CameraPanelControl panel = new CameraPanelControl(camera.Camera, PlayModes.Live, config);
                    SelectedCamera = panel;
                    panel.SelectedChanged += PanelControl_SelectionChanged;
                    //panel.Disposed += new EventHandler(PanelControl_Disposed);
                    dragItem.Control = panel;

                    if (editing && SelectedTemplate != null)
                    {
                        SelectedTemplate.PanelControl.dragDropLayoutControl1.layoutControl.Clear();
                        SelectedTemplate.PanelControl.dragDropLayoutControl1.layoutControl.AddItem(dragItem);
                    }
                    else
                    {
                        editing = false;

                        layoutControlDisplayArea.BeginUpdate();
                        layoutControlDisplayArea.Clear();
                        layoutControlDisplayArea.AddItem(dragItem);
                        layoutControlDisplayArea.EndUpdate();

                        SelectedTemplate = null;
                    }
                }
            }
        }


        public void StartTemplate()
        {
            CctvTemplateNode template = gridViewTemplates.GetFocusedRow() as CctvTemplateNode;

                
            if (SelectedCamera != null)
            {
                SelectedCamera.Controls.Clear();
                SelectedCamera.CameraPanelControlTemplates();
                cctvFrontClientFormDevX.CheckUnpin();
            }

            if (SelectedTemplate != null)
            {
                SelectedTemplate.PanelControl.DisposePanelControl();
                SelectedTemplate.PanelControl.dragDropLayoutControl1.Dispose();
                SelectedTemplate.PanelControl.dragDropLayoutControl1.layoutControl.Dispose();
                layoutControlDisplayArea.Controls.Clear();
                cctvFrontClientFormDevX.CheckUnpin();

            }
            if (template != null && layoutControlDisplayArea != null)
            {
                layoutControlDisplayArea.Clear();
                layoutControlDisplayArea.BeginUpdate();

                template.PanelControl = new TemplatePanelControl(template, PlayModes.Live, config);

                template.PanelControl.SelectionChanged += PanelControl_SelectionChanged;

                LayoutControlItem dragItem = layoutControlDisplayArea.AddItem("", template.PanelControl);
                dragItem.Name = template.Name;
                dragItem.TextVisible = false;
                dragItem.Padding = new DevExpress.XtraLayout.Utils.Padding(2);


                
                layoutControlDisplayArea.BestFit();
                layoutControlDisplayArea.EndUpdate();
                layoutControlDisplayArea.Invalidate();
                layoutControlDisplayArea.ResumeLayout();

            }
            SelectedTemplate = template;
            SelectedCamera = null;
        }


        #region Cameras
        private void gridViewCameras_DoubleClick(object sender, EventArgs e)
        {
            if (gridViewCameras.FocusedRowHandle > -1)
            {
                CctvCameraNode camera = gridViewCameras.GetFocusedRow() as CctvCameraNode;
                ((CctvFrontClientFormDevX)this.MdiParent).barButtonItemStartRegIncident.Enabled = false;
                if (SelectedCamera != null)  
                {
                    
                    SelectedCamera.Controls.Clear();
                    SelectedCamera.CameraPanelControlTemplates();
                    cctvFrontClientFormDevX.CheckUnpin();
                }

                if (SelectedTemplate != null)
                {
                    
                    SelectedTemplate.PanelControl.DisposePanelControl();
                    SelectedTemplate.PanelControl.dragDropLayoutControl1.Dispose();
                    SelectedTemplate.PanelControl.dragDropLayoutControl1.layoutControl.Dispose();
                    layoutControlDisplayArea.Controls.Clear();
                    cctvFrontClientFormDevX.CheckUnpin();

                }

                if (camera != null)
                {
                    LayoutControlItem dragItem = new LayoutControlItem();
                    dragItem.Name = camera.Name;
                    dragItem.Text = "";
                    dragItem.TextVisible = false;
                    dragItem.Padding = new DevExpress.XtraLayout.Utils.Padding(2);
                    CameraPanelControl panel = new CameraPanelControl(camera.Camera, PlayModes.Live, config);
                    SelectedCamera = panel;
                    panel.SelectedChanged += PanelControl_SelectionChanged;
                    //panel.Disposed += new EventHandler(PanelControl_Disposed);
                    dragItem.Control = panel;

                    if (editing && SelectedTemplate != null)
                    {
                        SelectedTemplate.PanelControl.dragDropLayoutControl1.layoutControl.Clear();
                        SelectedTemplate.PanelControl.dragDropLayoutControl1.layoutControl.AddItem(dragItem);
                    }
                    else
                    {
                        editing = false;
                     
                        layoutControlDisplayArea.BeginUpdate();
                        layoutControlDisplayArea.Clear();
                        layoutControlDisplayArea.AddItem(dragItem);
                        layoutControlDisplayArea.EndUpdate();

                        SelectedTemplate = null;
                    }
                }
            }
        }
        
        private void gridControlCameras_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left && editing)
            {
                if (gridViewCameras.FocusedRowHandle > -1
                    && SelectedTemplate.PanelControl.ContainsCamera(((CctvCameraNode)gridViewCameras.GetFocusedRow()).Name) == false)
                {
                    CctvCameraNode camera = gridViewCameras.GetFocusedRow() as CctvCameraNode;

                    LayoutControlItem dragItem = new LayoutControlItem();
                    dragItem.Name = camera.Name;
                    dragItem.Text = camera.Text;
                    dragItem.Tag = camera.Camera;
                    dragItem.TextVisible = false;
                    dragItem.Padding = new DevExpress.XtraLayout.Utils.Padding(2);
                    DragItem = dragItem;

                    gridControlCameras.DoDragDrop(dragItem, DragDropEffects.Copy);
                    this.Cursor = Cursors.Default;
                }
            }
        }
        #endregion

       // INTENTO DE ELIMINAR CAMARAS DE LOS TEMPLATES 
        private void gridControlCameras_DragDrop(object sender, DragEventArgs e)
        {
            if (DragItem != null && DragItem.Owner!=null)
            {
                Control control = DragItem.Control;
                DragItem.Parent.Remove(DragItem);
                if (control != null)
                {
                    control.Parent = null;
                    control.Dispose();
                }
                DragItem = null;
            }
        }

        public void cleaCameraVisualizeForm()
        {
            if (this.SelectedCamera != null) this.SelectedCamera.Dispose();
            if (this.SelectedTemplate != null) this.SelectedTemplate.PanelControl.Dispose();
        }


    }







}
