using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Reflection;
using Smartmatic.SmartCad.Service;
using System.ServiceModel;
using System.Collections;
using SmartCadCore.Enums;
using SmartCadCore.ClientData;
using SmartCadGuiCommon;
using SmartCadCore.Core;
using SmartCadCore.Common;

namespace Smartmatic.SmartCad.Gui
{
    public partial class CameraForm : DevExpress.XtraEditors.XtraForm
    {
        private FormBehavior behavior;       
        private bool buttonOkPressed = false;
        private CameraClientData selectedCamera;
        private CctvZoneClientData cctvZoneSelected;
        private CameraTypeClientData cameraTypeSelected;
        private ConnectionTypeClientData connTypeSelected;
        private StructClientData structSelected;
        private AdministrationRibbonForm parentForm;
        private object syncCommitted = new object();

        private class WrapperStructData 
        {
            public StructClientData Struct { get; set; }

            public override string ToString()
            {
                return this.Struct.Name;
            }

            public WrapperStructData(StructClientData strc)
            {
                this.Struct = strc;
            }

            public override bool Equals(object obj)
            {
                WrapperStructData objAux = obj as WrapperStructData;
                if (objAux != null)
                {
                    return objAux.Struct.Code == this.Struct.Code;
                }
                return obj.Equals(this);
            }

        }
                        
        public CameraClientData SelectedCamera
        {
            get
            {
                return selectedCamera;
            }
            set
            {
                selectedCamera = value;               
                if (Behavior == FormBehavior.Edit)
                {
                    textBoxExCameraName.Text = selectedCamera.Name;
                    ipAddressControlCamera.Value = 
                        GetIpAddresFromObject(selectedCamera.Ip);                                                                               
                    ipAddressControlServer.Value = 
                        GetIpAddresFromObject(selectedCamera.IpServer);                                           
                    textBoxExPort.Text = selectedCamera.Port;                    
                    comboBoxExCameraType.SelectedItem = selectedCamera.Type;
                    textBoxExFrameRate.Text = selectedCamera.FrameRate;                   
                    textBoxExRes.Text = selectedCamera.Resolution;                   
                    textBoxExStreamType.Text = selectedCamera.StreamType;
                    comboBoxExConnType.SelectedItem = selectedCamera.ConnectionType;
                    textBoxExId.Text = selectedCamera.Login;
                    textBoxExPassword.Text = selectedCamera.Password;
                    if (selectedCamera.StructClientData != null)
                    {
                        comboBoxExCctvZone.SelectedItem = selectedCamera.StructClientData.CctvZone;
                        comboBoxExStructType.SelectedItem = selectedCamera.StructClientData.Type;
                        WrapperStructData aux = new WrapperStructData(selectedCamera.StructClientData);
                        comboBoxExStructName.SelectedItem = aux;
                    }

                }
                buttonEnable();
            }
        }
        public bool ButtonOkPressed
        {
            get { return buttonOkPressed; }
        }
        public FormBehavior Behavior
        {
            set
            {
                behavior = value;

            }
            get
            {
                return behavior;
            }
        }

        public CameraForm(AdministrationRibbonForm form, CameraClientData camera, FormBehavior behavior)
            : this()
        {
            FormClosing += new FormClosingEventHandler(CameraForm_FormClosing);
            this.parentForm = form;
            this.parentForm.AdministrationCommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(CameraForm_AdministrationCommittedChanges);

            if (parentForm.Configuration.VmsServerIp != string.Empty)
            {
                int height = this.Height
                    - IPAddressControlitem.Height
                    - textBoxExPortitem.Height
                    - comboBoxExCameraTypeitem.Height
                    - textBoxExFrameRateitem.Height
                    - textBoxExResitem.Height
                    - textBoxExStreamTypeitem.Height
                    - comboBoxExConnTypeitem.Height
                    - layoutControlGroupId.Height;

                IPAddressControlitem.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                textBoxExPortitem.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                comboBoxExCameraTypeitem.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                textBoxExFrameRateitem.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                textBoxExResitem.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                textBoxExStreamTypeitem.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                comboBoxExConnTypeitem.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                layoutControlGroupId.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                ipAddressClientControlItem.Control.Enabled = false;

                this.MinimumSize = new System.Drawing.Size(this.Width, height);
                this.MaximumSize = new System.Drawing.Size(this.Width, height);
            }

            Behavior = behavior;
            FillCctvZones();
            FillCameraType();
            FillCameraConnType();
            SelectedCamera = camera;
        }
        public CameraForm()
        {
            InitializeComponent();
            LoadLanguage();
        }

        void CameraForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.parentForm != null)
            {
                this.parentForm.AdministrationCommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(CameraForm_AdministrationCommittedChanges);
            }
        }

        void CameraForm_AdministrationCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    lock (syncCommitted)
                    {
                        try
                        {
                            if (ServerServiceClient.GetInstance().OperatorClient == null)
                            { }
                        }
                        catch
                        { }

                        if (e.Objects[0] is CameraClientData)
                        {
                            #region EvaluationClientData
                            CameraClientData camera =
                                e.Objects[0] as CameraClientData;

                            if (SelectedCamera != null && SelectedCamera.Equals(camera) && ButtonOkPressed == false)
                            {
                                if (e.Action == CommittedDataAction.Update)
                                {
                                    MessageForm.Show(ResourceLoader.GetString2("UpdateFormCameraData"), MessageFormType.Warning);
                                    SelectedCamera = camera;
                                }
                                else if (e.Action == CommittedDataAction.Delete)
                                {
                                    MessageForm.Show(ResourceLoader.GetString2("DeleteFormCameraData"), MessageFormType.Warning);
                                    FormUtil.InvokeRequired(this,
                                    delegate
                                    {
                                        this.Close();
                                    });
                                }

                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private void LoadLanguage() 
        {
            this.Icon = ResourceLoader.GetIcon("$Icon.Camera");
          
            this.layoutControlGroupCameraInf.Text = ResourceLoader.GetString2("CameraInformation");
            this.layoutControlGroupId.Text = ResourceLoader.GetString2("Identification");
            this.layoutControlGroupUbication.Text = ResourceLoader.GetString2("Ubication");

            this.textBoxExCameraNameitem.Text = ResourceLoader.GetString2("Name") + ":*";
            this.textBoxExFrameRateitem.Text = ResourceLoader.GetString2("FrameRate") + ":*";
            this.textBoxExIditem.Text = ResourceLoader.GetString2("CameraLogin") + ":*";
            this.textBoxExPassworditem.Text = ResourceLoader.GetString2("Password") + ":*";
            this.textBoxExPortitem.Text = ResourceLoader.GetString2("Port") + ":*";
            this.ipAddressClientControlItem.Text = ResourceLoader.GetString2("IPClient") + ":*";
            this.IPAddressControlitem.Text = ResourceLoader.GetString2("IPServer") + ":*";
            this.textBoxExResitem.Text = ResourceLoader.GetString2("Resolution") + ":*";
            this.textBoxExStreamTypeitem.Text = ResourceLoader.GetString2("Compression") + ":*";
            this.comboBoxExCameraTypeitem.Text = ResourceLoader.GetString2("DeviceModel") + ":*";
            this.comboBoxExConnTypeitem.Text = ResourceLoader.GetString2("ConnectionType") + ":*";
            this.comboBoxExCctvZoneitem.Text = ResourceLoader.GetString2("CCTVZone") + ":";
            this.comboBoxExStructTypeitem.Text = ResourceLoader.GetString2("StructType")+ ":";
            this.comboBoxExStructNameitem.Text = ResourceLoader.GetString2("Struct") + ":";
        }


        private void buttonExAccept_Click(object sender, EventArgs e)
        {
            try
            {
                buttonOkPressed = true;
                if (behavior == FormBehavior.Create)
                {
                    selectedCamera = new CameraClientData();
                }
                selectedCamera.Name = textBoxExCameraName.Text;
                selectedCamera.Ip =
                    GetIpAddresFromControl(ipAddressControlCamera.Value);
                selectedCamera.IpServer =
                    GetIpAddresFromControl(ipAddressControlServer.Value);
                selectedCamera.Port = textBoxExPort.Text;
                selectedCamera.Type = cameraTypeSelected;
                selectedCamera.FrameRate = textBoxExFrameRate.Text;
                selectedCamera.Resolution = textBoxExRes.Text;
                selectedCamera.StreamType = textBoxExStreamType.Text;
                selectedCamera.ConnectionType = connTypeSelected;
                selectedCamera.Login = textBoxExId.Text;
                selectedCamera.Password = textBoxExPassword.Text;
                if (structSelected != null)
                {
                    selectedCamera.StructClientData = structSelected;
                    selectedCamera.StructClientData.Type = structSelected.Type;
                    selectedCamera.StructClientData.CctvZone = structSelected.CctvZone;
                }
                else
                {
                    selectedCamera.StructClientData = null;
                }
                ServerServiceClient.GetInstance().SaveOrUpdateClientData(selectedCamera);
            }
            catch (FaultException ex)
            {
                if (behavior == FormBehavior.Edit)
                    SelectedCamera = (CameraClientData)ServerServiceClient.GetInstance().RefreshClient(selectedCamera);

                GetErrorFocus(ex);
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, MessageFormType.Information);
                buttonOkPressed = false;
            }
            catch (Exception ex)
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.InnerException.Message, ex.InnerException);
                buttonOkPressed = false;
            }
        }      
        private void buttonExCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void GetErrorFocus(FaultException ex)
        {
            if (ex.Message.Contains(ResourceLoader.GetString2("UK_CAMERA_CUSTOM_CODE")))
            {
                if (behavior == FormBehavior.Create)
                   textBoxExCameraName.Text = textBoxExCameraName.Text;
                
                textBoxExCameraName.Focus();
            }else if (ex.Message.Contains(ResourceLoader.GetString2("UK_CAMERA_IP")))
            {
                if (behavior == FormBehavior.Create)
                    ipAddressControlCamera.Value = ipAddressControlCamera.Value;
                
                ipAddressControlCamera.Focus();
            }
            else if (ex.Message.Contains(ResourceLoader.GetString2("UK_CAMERA_LOGIN")))
            {
                if (behavior == FormBehavior.Create)
                   textBoxExId.Text = textBoxExId.Text;
                textBoxExId.Focus();
            }

        }


        private void buttonEnable()
        {
            if ((textBoxExCameraName.Text.Trim() == string.Empty) || 
                (comboBoxExCameraType.SelectedItem == null) ||
                (textBoxExStreamType.Text.Trim() == string.Empty) || 
                (comboBoxExConnType.SelectedItem == null) ||
                (textBoxExId.Text.Trim() == string.Empty) || 
                (textBoxExPassword.Text.Trim() == string.Empty) ||
                ipAddressControlCamera.AnyTextBoxEmpty() ||
                ipAddressControlServer.AnyTextBoxEmpty()) 
                buttonExAccept.Enabled = false;
            else
                buttonExAccept.Enabled = true;
        }

        private void FillCameraType()
        {
            IList list = ServerServiceClient.GetInstance().SearchClientObjects(typeof(CameraTypeClientData));
            foreach (CameraTypeClientData cameraType in list)
            {
                comboBoxExCameraType.Items.Add(cameraType);
            }
        }
        private void comboBoxExCameraType_SelectedValueChanged(object sender, EventArgs e)
        {
            cameraTypeSelected = (CameraTypeClientData)(sender as ComboBox).SelectedItem;
            buttonEnable();
        }

        private void FillCameraConnType()
        {
            foreach (ConnectionTypeClientData connType in ServerServiceClient.GetInstance().SearchClientObjects(typeof(ConnectionTypeClientData)))
            {
                comboBoxExConnType.Items.Add(connType);
            }
        }      
        private void comboBoxExConnType_SelectedValueChanged(object sender, EventArgs e)
        {
            connTypeSelected = (ConnectionTypeClientData)(sender as ComboBox).SelectedItem;
            buttonEnable();
        }

        private void FillCctvZones()
        {
            foreach (CctvZoneClientData cctvZone in ServerServiceClient.GetInstance().SearchClientObjects(typeof(CctvZoneClientData)))
            {
                comboBoxExCctvZone.Items.Add(cctvZone);
            }

            comboBoxExCctvZone.Items.Insert(0, new CctvZoneClientData() { Name = string.Empty });
        }
        private void comboBoxExCctvZone_SelectedValueChanged(object sender, EventArgs e)
        {
            if (comboBoxExCctvZone.SelectedIndex > 0)
            {
                cctvZoneSelected = (CctvZoneClientData)(sender as ComboBox).SelectedItem;
                FillStructTypes();
            }
            else if(comboBoxExCctvZone.SelectedIndex == 0)
            {
                comboBoxExStructType.SelectedIndex = -1;
                comboBoxExStructType.Items.Clear();
                comboBoxExStructName.SelectedIndex = -1;
            }
        }
        
        private void FillStructTypes()
        {
            comboBoxExStructType.SelectedIndex = -1;
            comboBoxExStructType.Items.Clear();
            IList structTypes = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetAllStructTypes);
            foreach (StructTypeClientData structType in structTypes)
            {
                comboBoxExStructType.Items.Add(structType);
            }
        }

        private void comboBoxExStructType_SelectedValueChanged(object sender, EventArgs e)
        {
            comboBoxExStructName.SelectedIndex = -1;
            comboBoxExStructName.Items.Clear();
            if (comboBoxExStructType.SelectedItem != null)
            {
                StructTypeClientData structType = (StructTypeClientData)(sender as ComboBox).SelectedItem;
                IList structs = ServerServiceClient.GetInstance().SearchClientObjects(
                                SmartCadHqls.GetCustomHql(SmartCadHqls.GetStructByStructTypeCodeZoneCode, structType.Code,cctvZoneSelected.Code));
                foreach (StructClientData structdata in structs)
                {
                    comboBoxExStructName.Items.Add(new WrapperStructData(structdata));
                }
            }
        }
        private void comboBoxExStructName_SelectedValueChanged(object sender, EventArgs e)
        {
            ComboBox combo = sender as ComboBox;

            if (combo.SelectedItem != null)
                structSelected = ((WrapperStructData)(sender as ComboBox).SelectedItem).Struct;
            else
                structSelected = null;
        }

        private IPAddressStruct GetIpAddresFromObject(string ip)
        {
            string[] ipNumbers = ip.Split('.');
            IPAddressStruct ipAddres = new IPAddressStruct(
                byte.Parse(ipNumbers[0]),
                byte.Parse(ipNumbers[1]),
                byte.Parse(ipNumbers[2]),
                byte.Parse(ipNumbers[3]));

            return ipAddres;
        }
        private string GetIpAddresFromControl(IPAddressStruct control)
        {
            string ip =
                 control.Byte1 + "." +
                 control.Byte2 + "." +
                 control.Byte3 + "." +
                 control.Byte4;
            return ip;
        }

        private void textBoxExCameraName_TextChanged(object sender, EventArgs e)
        {
            buttonEnable();
        }
        private void textBoxExStreamType_TextChanged(object sender, EventArgs e)
        {
            buttonEnable();
        }
        private void textBoxExId_TextChanged(object sender, EventArgs e)
        {
            buttonEnable();
        }
        private void textBoxExPassword_TextChanged(object sender, EventArgs e)
        {
            buttonEnable();
        }

        private void ipAddressControlCamera_ValueChange(object sender, EventArgs e)
        {
            buttonEnable();
        }

        private void ipAddressControlServer_ValueChange(object sender, EventArgs e)
        {
            buttonEnable();
        }

        private void groupBoxId_Enter(object sender, EventArgs e)
        {

        }

        private void CameraForm_Load(object sender, EventArgs e)
        {
            if (behavior == FormBehavior.Create)
            {
                Text = ResourceLoader.GetString2("CameraFormCreateText");
                buttonExAccept.Text = ResourceLoader.GetString("CameraFormCreateButtonOkText");
            }
            else if (behavior == FormBehavior.Edit)
            {
                Text = ResourceLoader.GetString2("CameraFormEditText");
                buttonExAccept.Text = ResourceLoader.GetString("CameraFormEditButtonOkText");
            }
        }

        private void textBoxExFrameRate_TextChanged(object sender, EventArgs e)
        {
            buttonEnable();
        }

        private void textBoxExRes_TextChanged(object sender, EventArgs e)
        {
            buttonEnable();
        }

        private void textBoxExPort_TextChanged(object sender, EventArgs e)
        {
            buttonEnable();
        }
       
    }
}
