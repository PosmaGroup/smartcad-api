using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Smartmatic.SmartCad.Service;
using System.Collections;
using DevExpress.XtraLayout;
using System.Threading;
using SmartCadCore.ClientData;
using SmartCadCore.Enums;
using SmartCadControls.SyncBoxes;
using SmartCadControls.Controls;
using SmartCadCore.Core;
using SmartCadControls;
using Smartmatic.SmartCad.Vms;
using SmartCadControls.Interfaces;

namespace Smartmatic.SmartCad.Gui
{
    public partial class CamerasTemplateForm : DevExpress.XtraEditors.XtraForm, IDragManager
    {
        public FormBehavior Behavior { get; set; }
        public CamerasTemplateClientData SelectedTemplate { get; set; }
        private GridControlSynBox camerasSyncBox;

        public CamerasTemplateForm()
        {
            InitializeComponent();
            dragDropLayoutControl1.ControlCreated += dragDropLayoutControl1_ControlCreated;
        }

        public CamerasTemplateForm(CamerasTemplateClientData template, FormBehavior behavior)
            :this()
		{
			Behavior = behavior;
            SelectedTemplate = template ;
            if (SelectedTemplate != null)
            {
                //GridControlDataCctvSecurityOperator oper = new GridControlDataCctvSecurityOperator(selectedUser);
                //this.gridControlExOperators.SelectData(oper);
                //this.gridViewExOperators.MakeRowVisible(this.gridViewExOperators.FocusedRowHandle, false);                
                //this.gridViewExOperators.TopRowIndex = this.gridViewExOperators.FocusedRowHandle;                              
            }
            
        }

        void dragDropLayoutControl1_ControlCreated(object sender, EventArgs e)
        {
            dragDropLayoutControl1.layoutControl.ShowCustomizationForm();
            dragDropLayoutControl1.layoutControl.CustomizationForm.Hide();
        }

        private void CamerasLayoutForm_Load(object sender, EventArgs e)
        {
            LoadCameras();
            LoadLanguage();
        }

        private void LoadCameras()
        {
            gridControlExCameras.Type = typeof(GridCameraData);
            camerasSyncBox = new GridControlSynBox(gridControlExCameras);
            gridControlExCameras.ViewTotalRows = true;
            gridControlExCameras.EnableAutoFilter = true;
            //gridControlExCameras.ViewTotalRows = true;
            gridControlExCameras.AllowDrop = true;

            IList cameras = ServerServiceClient.GetInstance().SearchClientObjectsMaxRows(SmartCadHqls.GetAllCameras, true);
            camerasSyncBox.Sync(cameras);
        }

        private void LoadLanguage()
        {
        }

        private void gridControlExCameras_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left && gridControlExCameras.SelectedItems.Count > 0)
            {
                ConfigurationClientData config = ServerServiceClient.GetInstance().GetConfiguration();
                GridCameraData camera = gridControlExCameras.SelectedItems[0] as GridCameraData;

                LayoutControlItem dragItem = new LayoutControlItem();
                dragItem.Name = camera.Name;
                dragItem.Control = VmsControlEx.GetInstance(config.VmsDllName);
                dragItem.Text = camera.Name;
                dragItem.Control.Name = "Panel" +new Random(DateTime.Now.Millisecond).Next().ToString();
                dragItem.TextLocation = DevExpress.Utils.Locations.Bottom;

                OperatorClientData oper = ServerServiceClient.GetInstance().OperatorClient;

                DragItem = dragItem;
                gridControlExCameras.DoDragDrop(dragItem, DragDropEffects.Copy);

                Thread t = new Thread(() =>
                {
                    bool connected = false;
                    if (oper != null)
                    {
                        ((VmsControlEx)dragItem.Control).Initialize(camera.Name, oper.Login, oper.Password, config.VmsServerIp);
                        if (((VmsControlEx)dragItem.Control).Connect(0) == 1)
                        {
                            connected = true;
                        }
                    }
                    if (!connected)
                    {
                        ((VmsControlEx)dragItem.Control).Initialize(camera.Name, config.VmsLogin, config.VmsPassword, config.VmsServerIp);
                        ((VmsControlEx)dragItem.Control).Connect(0);
                    }
                });
                t.Start();
            }
        }

        public DevExpress.XtraLayout.LayoutControlItem DragItem { get; set; }

        void IDragManager.SetDragCursor(DragDropEffects effect)
        {
            SetDragCursor(effect);
        }

        private void SetDragCursor(DragDropEffects e)
        {
            if (e == DragDropEffects.Move)
                Cursor = Cursors.AppStarting;
            if (e == DragDropEffects.Copy)
                Cursor = Cursors.AppStarting;
            if (e == DragDropEffects.None)
                Cursor = Cursors.No;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            dragDropLayoutControl1.layoutControl.CustomizationForm.Close();
            dragDropLayoutControl1.layoutControl.AllowDrop = false;
        }
    }

    public class GridCameraData : GridControlData
    {
        private CameraClientData camera;


        public GridCameraData(CameraClientData camera)
            : base(camera)
        {
            this.camera = camera;
        }

        [GridControlRowInfoData(Searchable = true)]
        public string Name
        {
            get
            {
                return camera.Name;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string Zone
        {
            get
            {
                return (camera.StructClientData!=null) ? camera.StructClientData.CctvZone.Name : "";
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string Struct
        {
            get
            {
                return (camera.StructClientData!=null) ? camera.StructClientData.Name: "";
            }
        }

        public override bool Equals(object obj)
        {
            if (obj is GridCameraData == false)
                return false;
            GridCameraData data = obj as GridCameraData;
            if (data.camera.Code == camera.Code)
                return true;
            return false;
        }
    }

}