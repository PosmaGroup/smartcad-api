using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.Threading;
using DevExpress.XtraEditors;
using Smartmatic.SmartCad.Service;
using SmartCadCore.ClientData;
using SmartCadControls.Controls;
using SmartCadCore.Common;
using Smartmatic.SmartCad.Vms;
using SmartCadGuiCommon.Controls;
using SmartCadGuiCommon.CCTV.Nodes;
using SmartCadGuiCommon.Classes;

namespace SmartCadGuiCommon
{
    public partial class CameraCctvAppForm : XtraForm
    {

        private string cameraType = string.Empty;
        private string ip = string.Empty;
        private string login = string.Empty;
        private string password = string.Empty;
        private string resolution = string.Empty;
        private string streamType = string.Empty;
        private string frameRate = string.Empty;
        private CameraClientData selectedCameraClientData;
        private ConfigurationClientData configuration;
        private bool isLoad = false;
        private CctvNode selectedRow = null;
        public static System.Windows.Forms.Control.ControlCollection controlGlobal;
        IPlayBackPanel currentPanel;
        public PlayModes Mode { get; set; }
        public string CameraType
        {
            get
            {
                return cameraType;
            }
            set
            {
                cameraType = value;
            }
        }
        public string Ip
        {
            get
            {
                return ip;
            }
            set
            {
                ip = value;
            }
        }
        public string Login
        {
            get
            {
                return login;
            }
            set
            {
                login = value;
            }
        }
        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                password = value;
            }
        }
        public string Resolution
        {
            get
            {
                return resolution;
            }
            set
            {
                resolution = value;
            }
        }
        public string StreamType
        {
            get
            {
                return streamType;
            }
            set
            {
                streamType = value;
            }
        }
        public string FrameRate
        {
            get
            {
                return frameRate;
            }
            set
            {
                frameRate = value;
            }
        }
        public ConfigurationClientData Configuration
        {
            get
            {
                return configuration;
            }
            set
            {
                configuration = value;
            }
        }
        public CameraCctvAppForm()
        {
            InitializeComponent();
        }
        private IPlayBackPanel CurrentPanel
        {
            get { return currentPanel; }
            set
            {
                currentPanel = value;
                this.Controls.Clear();
                this.Controls.Add(currentPanel as Control);
                ((Control)currentPanel).Dock = DockStyle.Fill;
                controlGlobal = this.Controls as System.Windows.Forms.Control.ControlCollection; 
                // currentPanel.DateTimeValueChanged += CurrentPanel_DateTimeValueChanged;
                ((Control)currentPanel).Disposed += CurrentPanel_Disposed;
            }
        }
        void CurrentPanel_Disposed(object sender, EventArgs e)
        {
            currentPanel = null;
            //simpleButtonGoto.Enabled = false;
            //simpleButtonGoto.Enabled = true;
        }

        public CameraCctvAppForm(CameraClientData cameraClientData, PlayModes mode)
            : this()
        {
            
            configuration = ServerServiceClient.GetInstance().GetConfiguration();


            if (configuration.VmsServerIp != "")
            {
                CurrentPanel = new CameraPanelControl( cameraClientData, mode , configuration, false);

                if (CurrentPanel == null)
                {
                    MessageForm.Show(ResourceLoader.GetString2("NocameraAccess", selectedCameraClientData.Name), MessageFormType.Warning);
                    this.Close();
                }
            }
            else
            {
                MessageForm.Show(ResourceLoader.GetString2("NotVMSConfigured"), MessageFormType.Error);
                this.Close();
            }

            


            //System.GC.Collect();
            //selectedCameraClientData = cameraClientData;


            //this.CreateControl();
            //configuration = ServerServiceClient.GetInstance().GetConfiguration();

            //if (configuration.VmsDllName != "") //LM: VMS configured
            //{
            //Mode = mode;
            //if (configuration.VmsServerIp != "")
            //{
            //    //this.Controls.Remove(vmsControlEx1);
            //    if (!this.IsHandleCreated)
            //    {
            //        this.CreateHandle();
            //        this.CreateControl();
            //    }

            //    //this.CreateControl();
            //    vmsControlEx1 = VmsControlEx.GetInstance(configuration.VmsDllName);
            //    vmsControlEx1.Dock = DockStyle.Fill;
            //    this.Controls.Add(vmsControlEx1);

            //    vmsControlEx1.Initialize(selectedCameraClientData.CustomId, configuration.VmsLogin, configuration.VmsPassword, 
            //        configuration.VmsServerIp + ":" + configuration.VmsPort);

            //    vmsControlEx1.Playmode(Mode);
            //    // vmsControlEx1.Click += new EventHandler(vmsControl_Click);
            //    //vmsControlEx1.DateTimeValueChanged += vmsControl_DateTimeValueChanged;

            //    this.Invoke((MethodInvoker)delegate
            //    {
            //    if (vmsControlEx1.Connect() != 1)
            //    {
            //        //vmsControlEx1.Connect();
            //            MessageForm.Show(ResourceLoader.GetString2("NocameraAccess", selectedCameraClientData.Name), MessageFormType.Warning);
            //            this.Close();
            //        }
            //    });
            //    //if (Connected != null)
            //    //    Connected(null, null);

            //    //Thread t = new Thread(() =>
            //    //{
            //    //    if (vmsControlEx1.Connect() != 1)
            //    //    {
            //    //        MessageForm.Show(ResourceLoader.GetString2("NocameraAccess", selectedCameraClientData.Name), MessageFormType.Warning);
            //    //        this.Close();
            //    //    }
            //    //});
            //    //t.Start();
            //}
            //else
            //{
            //    MessageForm.Show(ResourceLoader.GetString2("NotVMSConfigured"), MessageFormType.Error);
            //    this.Close();
            //}
            //}
        }

        private void CameraForm_Load(object sender, EventArgs e)
        {
        }

        private void CameraCctvAppForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.FormClosing -= new System.Windows.Forms.FormClosingEventHandler(this.CameraCctvAppForm_FormClosing);
            this.Close();
        }

        private void CameraCctvAppForm_Activated(object sender, EventArgs e)
        {
            if (isLoad == true)
            {
                CamerasRibbonForm parentForm = this.MdiParent as CamerasRibbonForm;
                if (parentForm != null)
                {
                    parentForm.VMSControl = this.vmsControlEx1;
                    parentForm.tmrUpdateStick.Enabled = true;
                    parentForm.groupControlPanTiltZoom.Enabled = this.vmsControlEx1.PTZ;
                    parentForm.groupControlAudioIN.Enabled = true;//this.vmsControlEx1.audio;
                    parentForm.simpleButtonSpeak.Enabled = true;//this.vmsControlEx1.speaker;
                    DevExpress.XtraEditors.Controls.ImageListBoxItem item = new DevExpress.XtraEditors.Controls.ImageListBoxItem();
                    if (this.vmsControlEx1.GetPlaymode())
                    {
                        parentForm.panelContainer1.ActiveChild = parentForm.dockPanelControls;
                        parentForm.muteCheckBox.Checked = this.vmsControlEx1.GetMute();
                        parentForm.trackBarControlVolume.Value = this.vmsControlEx1.GetVolume();
                    }
                    else
                    {
                        parentForm.panelContainer1.ActiveChild = parentForm.dockPanelPlayback;
                        parentForm.trackBarControlPlayback.Value = this.vmsControlEx1.GetVolume();
                        parentForm.checkBoxMutePlayback.Checked = this.vmsControlEx1.GetMute();
                        double speed = this.vmsControlEx1.GetSpeedPlayback();
                        foreach (KeyValuePair<int, float> pair in parentForm.speed)
                        {
                            if (pair.Value == speed)
                            {
                                parentForm.zoomTrackBarControlSpeed.Value = pair.Key;
                                break;
                            }
                        }

                        if (this.vmsControlEx1.GetPlayDirection() == 0)
                        {
                            parentForm.checkButtonPlay.Checked = true;
                            parentForm.checkButtonBack.Checked = false;
                        }
                        else
                        {
                            parentForm.checkButtonPlay.Checked = false;
                            parentForm.checkButtonBack.Checked = true;
                        }
                    }
                    if (this.vmsControlEx1.inputs.Count > 0)
                    {
                        parentForm.groupControlInput.Enabled = true;
                        foreach (IOData data in this.vmsControlEx1.inputs)
                        {
                            item = new DevExpress.XtraEditors.Controls.ImageListBoxItem();
                            item.Value = data.Name;
                            parentForm.imageListBoxControlInputs.Items.Add(item, data.Status);
                        }

                    }
                    if (this.vmsControlEx1.outputs.Count > 0)
                    {
                        parentForm.groupControlOutput.Enabled = true;
                        foreach (IOData data in this.vmsControlEx1.outputs)
                        {
                            item = new DevExpress.XtraEditors.Controls.ImageListBoxItem();
                            item.Value = data.Name;
                            parentForm.imageListBoxControlOutputs.Items.Add(item, data.Status);
                        }
                    }
                    parentForm.eventsuscribe();
                }
            }
        }

        private void CameraCctvAppForm_Deactivate(object sender, EventArgs e)

        {
            if (isLoad == true)
            {
                CamerasRibbonForm parentForm = this.MdiParent as CamerasRibbonForm;
                if (parentForm != null)
                {
                    parentForm.VMSControl = null;
                    parentForm.tmrUpdateStick.Enabled = false;
                    parentForm.groupControlPanTiltZoom.Enabled = false;
                    parentForm.groupControlAudioIN.Enabled = false;
                    parentForm.groupControlInput.Enabled = false;
                    parentForm.groupControlOutput.Enabled = false;
                    parentForm.imageListBoxControlInputs.Items.Clear();
                    parentForm.imageListBoxControlOutputs.Items.Clear();
                }
            }
        }

    }
}