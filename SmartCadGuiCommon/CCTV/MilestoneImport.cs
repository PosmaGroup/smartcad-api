﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VideoOS.Sdk.SystemInformation;
using System.Collections;
using Smartmatic.SmartCad.Vms;

namespace SmartCadGuiCommon
{
    class MilestoneImport : ImportManager
    {

        #region fields
        private string login;
        private string password;
        private string serverIp;
        private AuthenticationType authenticationType;
        private SystemInfo sysinfo;
        IList list = new ArrayList();
        #endregion

        public MilestoneImport()
        :base()
        {

        }

        /// <summary>
        /// Initialize the class
        /// </summary>
        /// <param name="serverIp">Ip for the VMS server</param>
        /// <param name="login">User Login</param>
        /// <param name="password">User Password</param>
        public override void Initialize(string serverIp, string login, string password)
        {
            this.login = login;
            this.password = password;
            this.authenticationType = AuthenticationType.Basic;
            this.serverIp = serverIp;
        }

        /// <summary>
        /// Connect to the server to import the cameras
        /// </summary>
        /// <returns>0 Not connected, 1 Connected</returns>
        public override int Export()
        {
            string urlManagementService = string.Format("http://{0}", serverIp);
            try
            {
                //sysinfo = new SystemInfo(urlManagementService, login, password, authenticationType);
                //sysinfo.Connect();

                sysinfo = new SystemInfo();
                sysinfo.Connect(urlManagementService, login, password, authenticationType);
                CameraCollection cameras = sysinfo.Cameras;
                if (cameras != null)
                {
                    foreach (Camera cam in cameras)
                    {
                        list.Add(cam.Name + "\\" + cam.Id);
                    }
                    Cameras = list;
                }
                return 1;
            }
            catch
            {
                return 0;
            }

        }



        public override void Disconnect()
        {
            try
            {
                this.sysinfo.Disconnect();
                sysinfo = null;
            }
            catch
            {
            }
        }

    }
}
