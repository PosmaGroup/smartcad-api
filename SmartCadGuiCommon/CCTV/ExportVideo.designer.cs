
namespace SmartCadGuiCommon
{
    partial class ExportVideo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExportVideo));
            this.groupControlExportStatus = new DevExpress.XtraEditors.GroupControl();
            this.dateEditEndTime = new DevExpress.XtraEditors.DateEdit();
            this.dateEditStartTime = new DevExpress.XtraEditors.DateEdit();
            this.lblSourceName = new DevExpress.XtraEditors.LabelControl();
            this.lblSource = new DevExpress.XtraEditors.LabelControl();
            this.lblEndTime = new DevExpress.XtraEditors.LabelControl();
            this.lblStartTime = new DevExpress.XtraEditors.LabelControl();
            this.lblExportFormat = new DevExpress.XtraEditors.LabelControl();
            this.lblTimestamp = new DevExpress.XtraEditors.LabelControl();
            this.groupControlExportType = new DevExpress.XtraEditors.GroupControl();
            this.radioGroupTimeStamp = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupExportFormat = new DevExpress.XtraEditors.RadioGroup();
            this.simpleButtonExport = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonClose = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlExportStatus)).BeginInit();
            this.groupControlExportStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEndTime.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEndTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStartTime.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStartTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlExportType)).BeginInit();
            this.groupControlExportType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupTimeStamp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupExportFormat.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControlExportStatus
            // 
            this.groupControlExportStatus.Controls.Add(this.dateEditEndTime);
            this.groupControlExportStatus.Controls.Add(this.dateEditStartTime);
            this.groupControlExportStatus.Controls.Add(this.lblSourceName);
            this.groupControlExportStatus.Controls.Add(this.lblSource);
            this.groupControlExportStatus.Controls.Add(this.lblEndTime);
            this.groupControlExportStatus.Controls.Add(this.lblStartTime);
            this.groupControlExportStatus.Location = new System.Drawing.Point(5, 5);
            this.groupControlExportStatus.Name = "groupControlExportStatus";
            this.groupControlExportStatus.Size = new System.Drawing.Size(266, 127);
            this.groupControlExportStatus.TabIndex = 0;
            this.groupControlExportStatus.Text = "Export Status";
            // 
            // dateEditEndTime
            // 
            this.dateEditEndTime.EditValue = null;
            this.dateEditEndTime.Location = new System.Drawing.Point(75, 50);
            this.dateEditEndTime.Name = "dateEditEndTime";
            this.dateEditEndTime.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dateEditEndTime.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, false)});
            this.dateEditEndTime.Properties.DisplayFormat.FormatString = "MM/dd/yyyy HH:mm:ss.fff";
            this.dateEditEndTime.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.dateEditEndTime.Properties.EditFormat.FormatString = "MM/dd/yyyy HH:mm:ss.fff";
            this.dateEditEndTime.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.dateEditEndTime.Properties.Mask.EditMask = "MM/dd/yyyy HH:mm:ss.fff";
            this.dateEditEndTime.Properties.ShowDropDown = DevExpress.XtraEditors.Controls.ShowDropDown.Never;
            this.dateEditEndTime.Properties.ShowPopupShadow = false;
            this.dateEditEndTime.Properties.ShowToday = false;
            this.dateEditEndTime.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditEndTime.Size = new System.Drawing.Size(184, 20);
            this.dateEditEndTime.TabIndex = 19;
            this.dateEditEndTime.DateTimeChanged += new System.EventHandler(this.dateEditEndTime_DateTimeChanged);
            // 
            // dateEditStartTime
            // 
            this.dateEditStartTime.EditValue = null;
            this.dateEditStartTime.Location = new System.Drawing.Point(75, 26);
            this.dateEditStartTime.Name = "dateEditStartTime";
            this.dateEditStartTime.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dateEditStartTime.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, null, false)});
            this.dateEditStartTime.Properties.DisplayFormat.FormatString = "MM/dd/yyyy HH:mm:ss.fff";
            this.dateEditStartTime.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.dateEditStartTime.Properties.EditFormat.FormatString = "MM/dd/yyyy HH:mm:ss.fff";
            this.dateEditStartTime.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.dateEditStartTime.Properties.Mask.EditMask = "MM/dd/yyyy HH:mm:ss.fff";
            this.dateEditStartTime.Properties.ShowDropDown = DevExpress.XtraEditors.Controls.ShowDropDown.Never;
            this.dateEditStartTime.Properties.ShowPopupShadow = false;
            this.dateEditStartTime.Properties.ShowToday = false;
            this.dateEditStartTime.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditStartTime.Size = new System.Drawing.Size(184, 20);
            this.dateEditStartTime.TabIndex = 18;
            this.dateEditStartTime.DateTimeChanged += new System.EventHandler(this.dateEditStartTime_DateTimeChanged);
            // 
            // lblSourceName
            // 
            this.lblSourceName.Appearance.BackColor = System.Drawing.Color.White;
            this.lblSourceName.Appearance.BorderColor = System.Drawing.Color.LightSteelBlue;
            this.lblSourceName.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblSourceName.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.lblSourceName.Location = new System.Drawing.Point(75, 76);
            this.lblSourceName.Name = "lblSourceName";
            this.lblSourceName.Size = new System.Drawing.Size(184, 46);
            this.lblSourceName.TabIndex = 7;
            this.lblSourceName.Text = "Source";
            // 
            // lblSource
            // 
            this.lblSource.Location = new System.Drawing.Point(9, 76);
            this.lblSource.Name = "lblSource";
            this.lblSource.Size = new System.Drawing.Size(37, 13);
            this.lblSource.TabIndex = 3;
            this.lblSource.Text = "Source:";
            // 
            // lblEndTime
            // 
            this.lblEndTime.Location = new System.Drawing.Point(9, 53);
            this.lblEndTime.Name = "lblEndTime";
            this.lblEndTime.Size = new System.Drawing.Size(47, 13);
            this.lblEndTime.TabIndex = 2;
            this.lblEndTime.Text = "End Time:";
            // 
            // lblStartTime
            // 
            this.lblStartTime.Location = new System.Drawing.Point(9, 27);
            this.lblStartTime.Name = "lblStartTime";
            this.lblStartTime.Size = new System.Drawing.Size(53, 13);
            this.lblStartTime.TabIndex = 1;
            this.lblStartTime.Text = "Start Time:";
            // 
            // lblExportFormat
            // 
            this.lblExportFormat.Location = new System.Drawing.Point(10, 32);
            this.lblExportFormat.Name = "lblExportFormat";
            this.lblExportFormat.Size = new System.Drawing.Size(73, 13);
            this.lblExportFormat.TabIndex = 1;
            this.lblExportFormat.Text = "Export Format:";
            // 
            // lblTimestamp
            // 
            this.lblTimestamp.Location = new System.Drawing.Point(9, 61);
            this.lblTimestamp.Name = "lblTimestamp";
            this.lblTimestamp.Size = new System.Drawing.Size(77, 13);
            this.lblTimestamp.TabIndex = 2;
            this.lblTimestamp.Text = "Add Timestamp:";
            // 
            // groupControlExportType
            // 
            this.groupControlExportType.Controls.Add(this.radioGroupTimeStamp);
            this.groupControlExportType.Controls.Add(this.radioGroupExportFormat);
            this.groupControlExportType.Controls.Add(this.lblTimestamp);
            this.groupControlExportType.Controls.Add(this.lblExportFormat);
            this.groupControlExportType.Location = new System.Drawing.Point(5, 138);
            this.groupControlExportType.Name = "groupControlExportType";
            this.groupControlExportType.Size = new System.Drawing.Size(266, 85);
            this.groupControlExportType.TabIndex = 1;
            this.groupControlExportType.Text = "Export Type";
            // 
            // radioGroupTimeStamp
            // 
            this.radioGroupTimeStamp.Location = new System.Drawing.Point(145, 55);
            this.radioGroupTimeStamp.Name = "radioGroupTimeStamp";
            this.radioGroupTimeStamp.Size = new System.Drawing.Size(100, 23);
            this.radioGroupTimeStamp.TabIndex = 4;
            // 
            // radioGroupExportFormat
            // 
            this.radioGroupExportFormat.Location = new System.Drawing.Point(145, 26);
            this.radioGroupExportFormat.Name = "radioGroupExportFormat";
            this.radioGroupExportFormat.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "AVI"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "JPEG")});
            this.radioGroupExportFormat.Size = new System.Drawing.Size(100, 23);
            this.radioGroupExportFormat.TabIndex = 3;
            // 
            // simpleButtonExport
            // 
            this.simpleButtonExport.Location = new System.Drawing.Point(115, 240);
            this.simpleButtonExport.Name = "simpleButtonExport";
            this.simpleButtonExport.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonExport.TabIndex = 4;
            this.simpleButtonExport.Text = "Export";
            this.simpleButtonExport.Click += new System.EventHandler(this.simpleButtonExport_Click);
            // 
            // simpleButtonClose
            // 
            this.simpleButtonClose.Location = new System.Drawing.Point(196, 239);
            this.simpleButtonClose.Name = "simpleButtonClose";
            this.simpleButtonClose.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonClose.TabIndex = 5;
            this.simpleButtonClose.Text = "Close";
            this.simpleButtonClose.Click += new System.EventHandler(this.simpleButtonClose_Click);
            // 
            // ExportVideo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(279, 274);
            this.Controls.Add(this.simpleButtonClose);
            this.Controls.Add(this.simpleButtonExport);
            this.Controls.Add(this.groupControlExportType);
            this.Controls.Add(this.groupControlExportStatus);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ExportVideo";
            this.Text = "van ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ExportVideo_FormClosing);
            this.Load += new System.EventHandler(this.ExportVideo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControlExportStatus)).EndInit();
            this.groupControlExportStatus.ResumeLayout(false);
            this.groupControlExportStatus.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEndTime.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEndTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStartTime.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStartTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlExportType)).EndInit();
            this.groupControlExportType.ResumeLayout(false);
            this.groupControlExportType.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupTimeStamp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupExportFormat.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControlExportStatus;
        private DevExpress.XtraEditors.LabelControl lblSourceName;
        private DevExpress.XtraEditors.LabelControl lblSource;
        private DevExpress.XtraEditors.LabelControl lblEndTime;
        private DevExpress.XtraEditors.LabelControl lblStartTime;
        private DevExpress.XtraEditors.DateEdit dateEditEndTime;
        private DevExpress.XtraEditors.DateEdit dateEditStartTime;
        private DevExpress.XtraEditors.LabelControl lblExportFormat;
        private DevExpress.XtraEditors.LabelControl lblTimestamp;
        private DevExpress.XtraEditors.GroupControl groupControlExportType;
        private DevExpress.XtraEditors.RadioGroup radioGroupExportFormat;
        private DevExpress.XtraEditors.RadioGroup radioGroupTimeStamp;
        private DevExpress.XtraEditors.SimpleButton simpleButtonExport;
        private DevExpress.XtraEditors.SimpleButton simpleButtonClose;



    }
}