using SmartCadControls;
using SmartCadControls.Controls;
using SmartCadGuiCommon;
using Smartmatic.SmartCad.Controls;
namespace Smartmatic.SmartCad.Gui
{
    partial class CameraForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CameraForm));
            this.textBoxExRes = new TextBoxEx();
            this.textBoxExFrameRate = new TextBoxEx();
            this.textBoxExPort = new TextBoxEx();
            this.ipAddressControlServer = new IPAddressControl();
            this.ipAddressControlCamera = new IPAddressControl();
            this.comboBoxExConnType = new ComboBoxEx();
            this.comboBoxExCameraType = new ComboBoxEx();
            this.textBoxExStreamType = new TextBoxEx();
            this.textBoxExCameraName = new TextBoxEx();
            this.textBoxExPassword = new TextBoxEx();
            this.textBoxExId = new TextBoxEx();
            this.comboBoxExStructName = new ComboBoxEx();
            this.comboBoxExStructType = new ComboBoxEx();
            this.comboBoxExCctvZone = new ComboBoxEx();
            this.buttonExAccept = new DevExpress.XtraEditors.SimpleButton();
            this.CameraFormConvertedLayout = new DevExpress.XtraLayout.LayoutControl();
            this.buttonExCancel = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupCameraInf = new DevExpress.XtraLayout.LayoutControlGroup();
            this.textBoxExCameraNameitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.ipAddressClientControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.IPAddressControlitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.textBoxExPortitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.comboBoxExCameraTypeitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.textBoxExFrameRateitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.textBoxExResitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.textBoxExStreamTypeitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.comboBoxExConnTypeitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupId = new DevExpress.XtraLayout.LayoutControlGroup();
            this.textBoxExPassworditem = new DevExpress.XtraLayout.LayoutControlItem();
            this.textBoxExIditem = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupUbication = new DevExpress.XtraLayout.LayoutControlGroup();
            this.comboBoxExCctvZoneitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.comboBoxExStructTypeitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.comboBoxExStructNameitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutConverter1 = new DevExpress.XtraLayout.Converter.LayoutConverter(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.CameraFormConvertedLayout)).BeginInit();
            this.CameraFormConvertedLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupCameraInf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxExCameraNameitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ipAddressClientControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IPAddressControlitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxExPortitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxExCameraTypeitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxExFrameRateitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxExResitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxExStreamTypeitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxExConnTypeitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxExPassworditem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxExIditem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupUbication)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxExCctvZoneitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxExStructTypeitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxExStructNameitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxExRes
            // 
            this.textBoxExRes.AllowsLetters = false;
            this.textBoxExRes.AllowsNumbers = true;
            this.textBoxExRes.AllowsPunctuation = false;
            this.textBoxExRes.AllowsSeparators = false;
            this.textBoxExRes.AllowsSymbols = false;
            this.textBoxExRes.AllowsWhiteSpaces = false;
            this.textBoxExRes.ExtraAllowedChars = "xX";
            this.textBoxExRes.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExRes.Location = new System.Drawing.Point(169, 209);
            this.textBoxExRes.MaxLength = 20;
            this.textBoxExRes.Name = "textBoxExRes";
            this.textBoxExRes.NonAllowedCharacters = "";
            this.textBoxExRes.RegularExpresion = "";
            this.textBoxExRes.Size = new System.Drawing.Size(266, 20);
            this.textBoxExRes.TabIndex = 6;
            this.textBoxExRes.TextChanged += new System.EventHandler(this.textBoxExRes_TextChanged);
            // 
            // textBoxExFrameRate
            // 
            this.textBoxExFrameRate.AllowsLetters = false;
            this.textBoxExFrameRate.AllowsNumbers = true;
            this.textBoxExFrameRate.AllowsPunctuation = false;
            this.textBoxExFrameRate.AllowsSeparators = false;
            this.textBoxExFrameRate.AllowsSymbols = false;
            this.textBoxExFrameRate.AllowsWhiteSpaces = false;
            this.textBoxExFrameRate.ExtraAllowedChars = "";
            this.textBoxExFrameRate.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExFrameRate.Location = new System.Drawing.Point(169, 185);
            this.textBoxExFrameRate.MaxLength = 20;
            this.textBoxExFrameRate.Name = "textBoxExFrameRate";
            this.textBoxExFrameRate.NonAllowedCharacters = "";
            this.textBoxExFrameRate.RegularExpresion = "";
            this.textBoxExFrameRate.Size = new System.Drawing.Size(266, 20);
            this.textBoxExFrameRate.TabIndex = 5;
            this.textBoxExFrameRate.TextChanged += new System.EventHandler(this.textBoxExFrameRate_TextChanged);
            // 
            // textBoxExPort
            // 
            this.textBoxExPort.AllowsLetters = true;
            this.textBoxExPort.AllowsNumbers = true;
            this.textBoxExPort.AllowsPunctuation = false;
            this.textBoxExPort.AllowsSeparators = false;
            this.textBoxExPort.AllowsSymbols = false;
            this.textBoxExPort.AllowsWhiteSpaces = false;
            this.textBoxExPort.ExtraAllowedChars = "";
            this.textBoxExPort.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExPort.Location = new System.Drawing.Point(169, 136);
            this.textBoxExPort.MaxLength = 20;
            this.textBoxExPort.Name = "textBoxExPort";
            this.textBoxExPort.NonAllowedCharacters = "";
            this.textBoxExPort.RegularExpresion = "";
            this.textBoxExPort.Size = new System.Drawing.Size(266, 20);
            this.textBoxExPort.TabIndex = 3;
            this.textBoxExPort.TextChanged += new System.EventHandler(this.textBoxExPort_TextChanged);
            // 
            // ipAddressControlServer
            // 
            this.ipAddressControlServer.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.ipAddressControlServer.Location = new System.Drawing.Point(169, 103);
            this.ipAddressControlServer.Name = "ipAddressControlServer";
            this.ipAddressControlServer.Size = new System.Drawing.Size(266, 29);
            this.ipAddressControlServer.TabIndex = 2;
            this.ipAddressControlServer.Value = new IPAddressStruct(((byte)(0)), ((byte)(0)), ((byte)(0)), ((byte)(0)));
            this.ipAddressControlServer.ValueChange += new ValueChangeEventHandler(this.ipAddressControlServer_ValueChange);
            // 
            // ipAddressControlCamera
            // 
            this.ipAddressControlCamera.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.ipAddressControlCamera.Location = new System.Drawing.Point(169, 68);
            this.ipAddressControlCamera.Name = "ipAddressControlCamera";
            this.ipAddressControlCamera.Size = new System.Drawing.Size(266, 31);
            this.ipAddressControlCamera.TabIndex = 1;
            this.ipAddressControlCamera.Value = new IPAddressStruct(((byte)(0)), ((byte)(0)), ((byte)(0)), ((byte)(0)));
            this.ipAddressControlCamera.ValueChange += new ValueChangeEventHandler(this.ipAddressControlCamera_ValueChange);
            // 
            // comboBoxExConnType
            // 
            this.comboBoxExConnType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxExConnType.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.comboBoxExConnType.FormattingEnabled = true;
            this.comboBoxExConnType.Location = new System.Drawing.Point(169, 257);
            this.comboBoxExConnType.Name = "comboBoxExConnType";
            this.comboBoxExConnType.Size = new System.Drawing.Size(266, 21);
            this.comboBoxExConnType.Sorted = true;
            this.comboBoxExConnType.TabIndex = 8;
            this.comboBoxExConnType.SelectedValueChanged += new System.EventHandler(this.comboBoxExConnType_SelectedValueChanged);
            // 
            // comboBoxExCameraType
            // 
            this.comboBoxExCameraType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxExCameraType.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.comboBoxExCameraType.FormattingEnabled = true;
            this.comboBoxExCameraType.Location = new System.Drawing.Point(169, 160);
            this.comboBoxExCameraType.Name = "comboBoxExCameraType";
            this.comboBoxExCameraType.Size = new System.Drawing.Size(266, 21);
            this.comboBoxExCameraType.Sorted = true;
            this.comboBoxExCameraType.TabIndex = 4;
            this.comboBoxExCameraType.SelectedValueChanged += new System.EventHandler(this.comboBoxExCameraType_SelectedValueChanged);
            // 
            // textBoxExStreamType
            // 
            this.textBoxExStreamType.AllowsLetters = true;
            this.textBoxExStreamType.AllowsNumbers = true;
            this.textBoxExStreamType.AllowsPunctuation = true;
            this.textBoxExStreamType.AllowsSeparators = true;
            this.textBoxExStreamType.AllowsSymbols = true;
            this.textBoxExStreamType.AllowsWhiteSpaces = true;
            this.textBoxExStreamType.ExtraAllowedChars = "";
            this.textBoxExStreamType.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExStreamType.Location = new System.Drawing.Point(169, 233);
            this.textBoxExStreamType.MaxLength = 20;
            this.textBoxExStreamType.Name = "textBoxExStreamType";
            this.textBoxExStreamType.NonAllowedCharacters = "";
            this.textBoxExStreamType.RegularExpresion = "";
            this.textBoxExStreamType.Size = new System.Drawing.Size(266, 20);
            this.textBoxExStreamType.TabIndex = 7;
            this.textBoxExStreamType.TextChanged += new System.EventHandler(this.textBoxExStreamType_TextChanged);
            // 
            // textBoxExCameraName
            // 
            this.textBoxExCameraName.AllowsLetters = true;
            this.textBoxExCameraName.AllowsNumbers = true;
            this.textBoxExCameraName.AllowsPunctuation = true;
            this.textBoxExCameraName.AllowsSeparators = true;
            this.textBoxExCameraName.AllowsSymbols = true;
            this.textBoxExCameraName.AllowsWhiteSpaces = true;
            this.textBoxExCameraName.ExtraAllowedChars = "";
            this.textBoxExCameraName.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExCameraName.Location = new System.Drawing.Point(169, 44);
            this.textBoxExCameraName.MaxLength = 0;
            this.textBoxExCameraName.Name = "textBoxExCameraName";
            this.textBoxExCameraName.NonAllowedCharacters = "";
            this.textBoxExCameraName.RegularExpresion = "";
            this.textBoxExCameraName.Size = new System.Drawing.Size(266, 20);
            this.textBoxExCameraName.TabIndex = 0;
            this.textBoxExCameraName.TextChanged += new System.EventHandler(this.textBoxExCameraName_TextChanged);
            // 
            // textBoxExPassword
            // 
            this.textBoxExPassword.AllowsLetters = true;
            this.textBoxExPassword.AllowsNumbers = true;
            this.textBoxExPassword.AllowsPunctuation = true;
            this.textBoxExPassword.AllowsSeparators = true;
            this.textBoxExPassword.AllowsSymbols = true;
            this.textBoxExPassword.AllowsWhiteSpaces = true;
            this.textBoxExPassword.ExtraAllowedChars = "";
            this.textBoxExPassword.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExPassword.Location = new System.Drawing.Point(169, 350);
            this.textBoxExPassword.MaxLength = 40;
            this.textBoxExPassword.Name = "textBoxExPassword";
            this.textBoxExPassword.NonAllowedCharacters = "";
            this.textBoxExPassword.PasswordChar = '*';
            this.textBoxExPassword.RegularExpresion = "";
            this.textBoxExPassword.Size = new System.Drawing.Size(266, 20);
            this.textBoxExPassword.TabIndex = 10;
            this.textBoxExPassword.TextChanged += new System.EventHandler(this.textBoxExPassword_TextChanged);
            // 
            // textBoxExId
            // 
            this.textBoxExId.AllowsLetters = true;
            this.textBoxExId.AllowsNumbers = true;
            this.textBoxExId.AllowsPunctuation = true;
            this.textBoxExId.AllowsSeparators = true;
            this.textBoxExId.AllowsSymbols = true;
            this.textBoxExId.AllowsWhiteSpaces = true;
            this.textBoxExId.ExtraAllowedChars = "";
            this.textBoxExId.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExId.Location = new System.Drawing.Point(169, 326);
            this.textBoxExId.MaxLength = 40;
            this.textBoxExId.Name = "textBoxExId";
            this.textBoxExId.NonAllowedCharacters = "";
            this.textBoxExId.RegularExpresion = "";
            this.textBoxExId.Size = new System.Drawing.Size(266, 20);
            this.textBoxExId.TabIndex = 9;
            this.textBoxExId.TextChanged += new System.EventHandler(this.textBoxExId_TextChanged);
            // 
            // comboBoxExStructName
            // 
            this.comboBoxExStructName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxExStructName.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.comboBoxExStructName.FormattingEnabled = true;
            this.comboBoxExStructName.Location = new System.Drawing.Point(169, 468);
            this.comboBoxExStructName.Name = "comboBoxExStructName";
            this.comboBoxExStructName.Size = new System.Drawing.Size(266, 21);
            this.comboBoxExStructName.Sorted = true;
            this.comboBoxExStructName.TabIndex = 13;
            this.comboBoxExStructName.SelectedValueChanged += new System.EventHandler(this.comboBoxExStructName_SelectedValueChanged);
            // 
            // comboBoxExStructType
            // 
            this.comboBoxExStructType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxExStructType.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.comboBoxExStructType.FormattingEnabled = true;
            this.comboBoxExStructType.Location = new System.Drawing.Point(169, 443);
            this.comboBoxExStructType.Name = "comboBoxExStructType";
            this.comboBoxExStructType.Size = new System.Drawing.Size(266, 21);
            this.comboBoxExStructType.Sorted = true;
            this.comboBoxExStructType.TabIndex = 12;
            this.comboBoxExStructType.SelectedValueChanged += new System.EventHandler(this.comboBoxExStructType_SelectedValueChanged);
            // 
            // comboBoxExCctvZone
            // 
            this.comboBoxExCctvZone.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxExCctvZone.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.comboBoxExCctvZone.FormattingEnabled = true;
            this.comboBoxExCctvZone.Location = new System.Drawing.Point(169, 418);
            this.comboBoxExCctvZone.Name = "comboBoxExCctvZone";
            this.comboBoxExCctvZone.Size = new System.Drawing.Size(266, 21);
            this.comboBoxExCctvZone.Sorted = true;
            this.comboBoxExCctvZone.TabIndex = 11;
            this.comboBoxExCctvZone.SelectedValueChanged += new System.EventHandler(this.comboBoxExCctvZone_SelectedValueChanged);
            // 
            // buttonExAccept
            // 
            this.buttonExAccept.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.buttonExAccept.Appearance.Options.UseFont = true;
            this.buttonExAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonExAccept.Location = new System.Drawing.Point(279, 505);
            this.buttonExAccept.Name = "buttonExAccept";
            this.buttonExAccept.Size = new System.Drawing.Size(82, 32);
            this.buttonExAccept.StyleController = this.CameraFormConvertedLayout;
            this.buttonExAccept.TabIndex = 14;
            this.buttonExAccept.Text = "Accept";
            this.buttonExAccept.Click += new System.EventHandler(this.buttonExAccept_Click);
            // 
            // CameraFormConvertedLayout
            // 
            this.CameraFormConvertedLayout.AllowCustomizationMenu = false;
            this.CameraFormConvertedLayout.Controls.Add(this.buttonExCancel);
            this.CameraFormConvertedLayout.Controls.Add(this.comboBoxExStructName);
            this.CameraFormConvertedLayout.Controls.Add(this.buttonExAccept);
            this.CameraFormConvertedLayout.Controls.Add(this.textBoxExPassword);
            this.CameraFormConvertedLayout.Controls.Add(this.comboBoxExStructType);
            this.CameraFormConvertedLayout.Controls.Add(this.textBoxExRes);
            this.CameraFormConvertedLayout.Controls.Add(this.comboBoxExCctvZone);
            this.CameraFormConvertedLayout.Controls.Add(this.textBoxExId);
            this.CameraFormConvertedLayout.Controls.Add(this.textBoxExFrameRate);
            this.CameraFormConvertedLayout.Controls.Add(this.textBoxExCameraName);
            this.CameraFormConvertedLayout.Controls.Add(this.textBoxExPort);
            this.CameraFormConvertedLayout.Controls.Add(this.textBoxExStreamType);
            this.CameraFormConvertedLayout.Controls.Add(this.ipAddressControlServer);
            this.CameraFormConvertedLayout.Controls.Add(this.comboBoxExCameraType);
            this.CameraFormConvertedLayout.Controls.Add(this.ipAddressControlCamera);
            this.CameraFormConvertedLayout.Controls.Add(this.comboBoxExConnType);
            this.CameraFormConvertedLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CameraFormConvertedLayout.Location = new System.Drawing.Point(0, 0);
            this.CameraFormConvertedLayout.Name = "CameraFormConvertedLayout";
            this.CameraFormConvertedLayout.OptionsCustomizationForm.ShowLayoutTreeView = false;
            this.CameraFormConvertedLayout.Root = this.layoutControlGroup1;
            this.CameraFormConvertedLayout.Size = new System.Drawing.Size(459, 549);
            this.CameraFormConvertedLayout.TabIndex = 20;
            // 
            // buttonExCancel
            // 
            this.buttonExCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.buttonExCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.buttonExCancel.Appearance.Options.UseFont = true;
            this.buttonExCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonExCancel.Location = new System.Drawing.Point(365, 505);
            this.buttonExCancel.Name = "buttonExCancel";
            this.buttonExCancel.Size = new System.Drawing.Size(82, 32);
            this.buttonExCancel.StyleController = this.CameraFormConvertedLayout;
            this.buttonExCancel.TabIndex = 15;
            this.buttonExCancel.Text = "Cancel";
            this.buttonExCancel.Click += new System.EventHandler(this.buttonExCancel_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.layoutControlGroupCameraInf,
            this.layoutControlGroupId,
            this.layoutControlGroupUbication,
            this.layoutControlItem3,
            this.layoutControlItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(459, 549);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 493);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(267, 36);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupCameraInf
            // 
            this.layoutControlGroupCameraInf.CustomizationFormText = "layoutControlGroupCameraInf";
            this.layoutControlGroupCameraInf.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.textBoxExCameraNameitem,
            this.ipAddressClientControlItem,
            this.IPAddressControlitem,
            this.textBoxExPortitem,
            this.comboBoxExCameraTypeitem,
            this.textBoxExFrameRateitem,
            this.textBoxExResitem,
            this.textBoxExStreamTypeitem,
            this.comboBoxExConnTypeitem});
            this.layoutControlGroupCameraInf.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupCameraInf.Name = "layoutControlGroupCameraInf";
            this.layoutControlGroupCameraInf.Size = new System.Drawing.Size(439, 282);
            this.layoutControlGroupCameraInf.Text = "layoutControlGroupCameraInf";
            // 
            // textBoxExCameraNameitem
            // 
            this.textBoxExCameraNameitem.Control = this.textBoxExCameraName;
            this.textBoxExCameraNameitem.CustomizationFormText = "textBoxExCameraNameitem";
            this.textBoxExCameraNameitem.Location = new System.Drawing.Point(0, 0);
            this.textBoxExCameraNameitem.Name = "textBoxExCameraNameitem";
            this.textBoxExCameraNameitem.Size = new System.Drawing.Size(415, 24);
            this.textBoxExCameraNameitem.Text = "textBoxExCameraNameitem";
            this.textBoxExCameraNameitem.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ipAddressClientControlItem
            // 
            this.ipAddressClientControlItem.Control = this.ipAddressControlCamera;
            this.ipAddressClientControlItem.CustomizationFormText = "ipAddressClientControlItem";
            this.ipAddressClientControlItem.Location = new System.Drawing.Point(0, 24);
            this.ipAddressClientControlItem.Name = "ipAddressClientControlItem";
            this.ipAddressClientControlItem.Size = new System.Drawing.Size(415, 35);
            this.ipAddressClientControlItem.Text = "ipAddressClientControlItem";
            this.ipAddressClientControlItem.TextSize = new System.Drawing.Size(142, 13);
            // 
            // IPAddressControlitem
            // 
            this.IPAddressControlitem.Control = this.ipAddressControlServer;
            this.IPAddressControlitem.CustomizationFormText = "IPAddressControlitem";
            this.IPAddressControlitem.Location = new System.Drawing.Point(0, 59);
            this.IPAddressControlitem.MaxSize = new System.Drawing.Size(0, 33);
            this.IPAddressControlitem.MinSize = new System.Drawing.Size(258, 33);
            this.IPAddressControlitem.Name = "IPAddressControlitem";
            this.IPAddressControlitem.Size = new System.Drawing.Size(415, 33);
            this.IPAddressControlitem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.IPAddressControlitem.Text = "IPAddressControlitem";
            this.IPAddressControlitem.TextSize = new System.Drawing.Size(142, 13);
            // 
            // textBoxExPortitem
            // 
            this.textBoxExPortitem.Control = this.textBoxExPort;
            this.textBoxExPortitem.CustomizationFormText = "textBoxExPortitem";
            this.textBoxExPortitem.Location = new System.Drawing.Point(0, 92);
            this.textBoxExPortitem.Name = "textBoxExPortitem";
            this.textBoxExPortitem.Size = new System.Drawing.Size(415, 24);
            this.textBoxExPortitem.Text = "textBoxExPortitem";
            this.textBoxExPortitem.TextSize = new System.Drawing.Size(142, 13);
            // 
            // comboBoxExCameraTypeitem
            // 
            this.comboBoxExCameraTypeitem.Control = this.comboBoxExCameraType;
            this.comboBoxExCameraTypeitem.CustomizationFormText = "comboBoxExCameraTypeitem";
            this.comboBoxExCameraTypeitem.Location = new System.Drawing.Point(0, 116);
            this.comboBoxExCameraTypeitem.Name = "comboBoxExCameraTypeitem";
            this.comboBoxExCameraTypeitem.Size = new System.Drawing.Size(415, 25);
            this.comboBoxExCameraTypeitem.Text = "comboBoxExCameraTypeitem";
            this.comboBoxExCameraTypeitem.TextSize = new System.Drawing.Size(142, 13);
            // 
            // textBoxExFrameRateitem
            // 
            this.textBoxExFrameRateitem.Control = this.textBoxExFrameRate;
            this.textBoxExFrameRateitem.CustomizationFormText = "textBoxExFrameRateitem";
            this.textBoxExFrameRateitem.Location = new System.Drawing.Point(0, 141);
            this.textBoxExFrameRateitem.Name = "textBoxExFrameRateitem";
            this.textBoxExFrameRateitem.Size = new System.Drawing.Size(415, 24);
            this.textBoxExFrameRateitem.Text = "textBoxExFrameRateitem";
            this.textBoxExFrameRateitem.TextSize = new System.Drawing.Size(142, 13);
            // 
            // textBoxExResitem
            // 
            this.textBoxExResitem.Control = this.textBoxExRes;
            this.textBoxExResitem.CustomizationFormText = "textBoxExResitem";
            this.textBoxExResitem.Location = new System.Drawing.Point(0, 165);
            this.textBoxExResitem.Name = "textBoxExResitem";
            this.textBoxExResitem.Size = new System.Drawing.Size(415, 24);
            this.textBoxExResitem.Text = "textBoxExResitem";
            this.textBoxExResitem.TextSize = new System.Drawing.Size(142, 13);
            // 
            // textBoxExStreamTypeitem
            // 
            this.textBoxExStreamTypeitem.Control = this.textBoxExStreamType;
            this.textBoxExStreamTypeitem.CustomizationFormText = "textBoxExStreamTypeitem";
            this.textBoxExStreamTypeitem.Location = new System.Drawing.Point(0, 189);
            this.textBoxExStreamTypeitem.Name = "textBoxExStreamTypeitem";
            this.textBoxExStreamTypeitem.Size = new System.Drawing.Size(415, 24);
            this.textBoxExStreamTypeitem.Text = "textBoxExStreamTypeitem";
            this.textBoxExStreamTypeitem.TextSize = new System.Drawing.Size(142, 13);
            // 
            // comboBoxExConnTypeitem
            // 
            this.comboBoxExConnTypeitem.Control = this.comboBoxExConnType;
            this.comboBoxExConnTypeitem.CustomizationFormText = "comboBoxExConnTypeitem";
            this.comboBoxExConnTypeitem.Location = new System.Drawing.Point(0, 213);
            this.comboBoxExConnTypeitem.Name = "comboBoxExConnTypeitem";
            this.comboBoxExConnTypeitem.Size = new System.Drawing.Size(415, 25);
            this.comboBoxExConnTypeitem.Text = "comboBoxExConnTypeitem";
            this.comboBoxExConnTypeitem.TextSize = new System.Drawing.Size(142, 13);
            // 
            // layoutControlGroupId
            // 
            this.layoutControlGroupId.CustomizationFormText = "layoutControlGroupId";
            this.layoutControlGroupId.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.textBoxExPassworditem,
            this.textBoxExIditem});
            this.layoutControlGroupId.Location = new System.Drawing.Point(0, 282);
            this.layoutControlGroupId.Name = "layoutControlGroupId";
            this.layoutControlGroupId.Size = new System.Drawing.Size(439, 92);
            this.layoutControlGroupId.Text = "layoutControlGroupId";
            // 
            // textBoxExPassworditem
            // 
            this.textBoxExPassworditem.Control = this.textBoxExPassword;
            this.textBoxExPassworditem.CustomizationFormText = "textBoxExPassworditem";
            this.textBoxExPassworditem.Location = new System.Drawing.Point(0, 24);
            this.textBoxExPassworditem.Name = "textBoxExPassworditem";
            this.textBoxExPassworditem.Size = new System.Drawing.Size(415, 24);
            this.textBoxExPassworditem.Text = "textBoxExPassworditem";
            this.textBoxExPassworditem.TextSize = new System.Drawing.Size(142, 13);
            // 
            // textBoxExIditem
            // 
            this.textBoxExIditem.Control = this.textBoxExId;
            this.textBoxExIditem.CustomizationFormText = "textBoxExIditem";
            this.textBoxExIditem.Location = new System.Drawing.Point(0, 0);
            this.textBoxExIditem.Name = "textBoxExIditem";
            this.textBoxExIditem.Size = new System.Drawing.Size(415, 24);
            this.textBoxExIditem.Text = "textBoxExIditem";
            this.textBoxExIditem.TextSize = new System.Drawing.Size(142, 13);
            // 
            // layoutControlGroupUbication
            // 
            this.layoutControlGroupUbication.CustomizationFormText = "layoutControlGroupUbication";
            this.layoutControlGroupUbication.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.comboBoxExCctvZoneitem,
            this.comboBoxExStructTypeitem,
            this.comboBoxExStructNameitem});
            this.layoutControlGroupUbication.Location = new System.Drawing.Point(0, 374);
            this.layoutControlGroupUbication.Name = "layoutControlGroupUbication";
            this.layoutControlGroupUbication.Size = new System.Drawing.Size(439, 119);
            this.layoutControlGroupUbication.Text = "layoutControlGroupUbication";
            // 
            // comboBoxExCctvZoneitem
            // 
            this.comboBoxExCctvZoneitem.Control = this.comboBoxExCctvZone;
            this.comboBoxExCctvZoneitem.CustomizationFormText = "comboBoxExCctvZoneitem";
            this.comboBoxExCctvZoneitem.Location = new System.Drawing.Point(0, 0);
            this.comboBoxExCctvZoneitem.Name = "comboBoxExCctvZoneitem";
            this.comboBoxExCctvZoneitem.Size = new System.Drawing.Size(415, 25);
            this.comboBoxExCctvZoneitem.Text = "comboBoxExCctvZoneitem";
            this.comboBoxExCctvZoneitem.TextSize = new System.Drawing.Size(142, 13);
            // 
            // comboBoxExStructTypeitem
            // 
            this.comboBoxExStructTypeitem.Control = this.comboBoxExStructType;
            this.comboBoxExStructTypeitem.CustomizationFormText = "comboBoxExStructTypeitem";
            this.comboBoxExStructTypeitem.Location = new System.Drawing.Point(0, 25);
            this.comboBoxExStructTypeitem.Name = "comboBoxExStructTypeitem";
            this.comboBoxExStructTypeitem.Size = new System.Drawing.Size(415, 25);
            this.comboBoxExStructTypeitem.Text = "comboBoxExStructTypeitem";
            this.comboBoxExStructTypeitem.TextSize = new System.Drawing.Size(142, 13);
            // 
            // comboBoxExStructNameitem
            // 
            this.comboBoxExStructNameitem.Control = this.comboBoxExStructName;
            this.comboBoxExStructNameitem.CustomizationFormText = "comboBoxExStructNameitem";
            this.comboBoxExStructNameitem.Location = new System.Drawing.Point(0, 50);
            this.comboBoxExStructNameitem.Name = "comboBoxExStructNameitem";
            this.comboBoxExStructNameitem.Size = new System.Drawing.Size(415, 25);
            this.comboBoxExStructNameitem.Text = "comboBoxExStructNameitem";
            this.comboBoxExStructNameitem.TextSize = new System.Drawing.Size(142, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.buttonExAccept;
            this.layoutControlItem3.CustomizationFormText = "buttonExAcceptitem";
            this.layoutControlItem3.Location = new System.Drawing.Point(267, 493);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem3.Name = "buttonExAcceptitem";
            this.layoutControlItem3.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "buttonExAcceptitem";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.buttonExCancel;
            this.layoutControlItem1.CustomizationFormText = "buttonExCancelitem";
            this.layoutControlItem1.Location = new System.Drawing.Point(353, 493);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem1.Name = "buttonExCancelitem";
            this.layoutControlItem1.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "buttonExCancelitem";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // CameraForm
            // 
            this.AcceptButton = this.buttonExAccept;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.CancelButton = this.buttonExCancel;
            this.ClientSize = new System.Drawing.Size(459, 549);
            this.Controls.Add(this.CameraFormConvertedLayout);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(475, 587);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(475, 587);
            this.Name = "CameraForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.CameraForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.CameraFormConvertedLayout)).EndInit();
            this.CameraFormConvertedLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupCameraInf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxExCameraNameitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ipAddressClientControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IPAddressControlitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxExPortitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxExCameraTypeitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxExFrameRateitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxExResitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxExStreamTypeitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxExConnTypeitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxExPassworditem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxExIditem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupUbication)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxExCctvZoneitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxExStructTypeitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxExStructNameitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TextBoxEx textBoxExStreamType;
        private TextBoxEx textBoxExCameraName;
        private TextBoxEx textBoxExPassword;
        private TextBoxEx textBoxExId;
        private DevExpress.XtraEditors.SimpleButton buttonExAccept;
        private DevExpress.XtraEditors.SimpleButton buttonExCancel;
        private ComboBoxEx comboBoxExConnType;
        private ComboBoxEx comboBoxExCameraType;
        private ComboBoxEx comboBoxExStructName;
        private ComboBoxEx comboBoxExStructType;
        private ComboBoxEx comboBoxExCctvZone;
        private IPAddressControl ipAddressControlCamera;
        private IPAddressControl ipAddressControlServer;
        private TextBoxEx textBoxExPort;
        private TextBoxEx textBoxExFrameRate;
        private TextBoxEx textBoxExRes;
        private DevExpress.XtraLayout.LayoutControl CameraFormConvertedLayout;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem comboBoxExStructNameitem;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem textBoxExPassworditem;
        private DevExpress.XtraLayout.LayoutControlItem comboBoxExStructTypeitem;
        private DevExpress.XtraLayout.LayoutControlItem textBoxExResitem;
        private DevExpress.XtraLayout.LayoutControlItem comboBoxExCctvZoneitem;
        private DevExpress.XtraLayout.LayoutControlItem textBoxExIditem;
        private DevExpress.XtraLayout.LayoutControlItem textBoxExFrameRateitem;
        private DevExpress.XtraLayout.LayoutControlItem textBoxExCameraNameitem;
        private DevExpress.XtraLayout.LayoutControlItem textBoxExPortitem;
        private DevExpress.XtraLayout.LayoutControlItem textBoxExStreamTypeitem;
        private DevExpress.XtraLayout.LayoutControlItem IPAddressControlitem;
        private DevExpress.XtraLayout.LayoutControlItem comboBoxExCameraTypeitem;
        private DevExpress.XtraLayout.LayoutControlItem ipAddressClientControlItem;
        private DevExpress.XtraLayout.LayoutControlItem comboBoxExConnTypeitem;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.Converter.LayoutConverter layoutConverter1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupCameraInf;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupId;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupUbication;
    }
}
