﻿using SmartCadControls;

namespace SmartCadGuiCommon
{
    partial class PlayBackForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PlayBackForm));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            this.barButtonItemStartRegIncident = new DevExpress.XtraBars.BarButtonItem();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.groupControlTemplates = new DevExpress.XtraEditors.GroupControl();
            this.gridControlTemplates = new GridControlEx();
            this.gridViewTemplates = new GridViewEx();
            this.gridColumnName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox3 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemSpinEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemButtonUndo = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemButtonSave = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemButtonCancel = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemButtonEdit = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemButtonUnping = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.groupControlPlayback = new DevExpress.XtraEditors.GroupControl();
            this.simpleButtonGoto = new DevExpress.XtraEditors.SimpleButton();
            this.DateEditEndDate = new DevExpress.XtraEditors.DateEdit();
            this.DateEditStartDate = new DevExpress.XtraEditors.DateEdit();
            this.lbEndDate = new System.Windows.Forms.Label();
            this.lbStartDate = new System.Windows.Forms.Label();
            this.groupBoxCameras = new DevExpress.XtraEditors.GroupControl();
            this.gridControlCameras = new GridControlEx();
            this.gridViewCameras = new GridViewEx();
            this.gridColumnCameraName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemComboBoxDevices = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.groupControlExport = new DevExpress.XtraEditors.GroupControl();
            this.simpleButtonExport = new DevExpress.XtraEditors.SimpleButton();
            this.groupControlAudioControl = new DevExpress.XtraEditors.GroupControl();
            this.checkButtonMute = new DevExpress.XtraEditors.CheckButton();
            this.labelControlPlaybackSpeed = new DevExpress.XtraEditors.LabelControl();
            this.labelVideoTime = new System.Windows.Forms.Label();
            this.checkButtonBack = new DevExpress.XtraEditors.CheckButton();
            this.trackBarControlVolume = new DevExpress.XtraEditors.TrackBarControl();
            this.checkButtonPlay = new DevExpress.XtraEditors.CheckButton();
            this.simpleButtonStop = new DevExpress.XtraEditors.SimpleButton();
            this.toolTipController1 = new DevExpress.Utils.ToolTipController(this.components);
            this.zoomTrackBarControlSpeed = new DevExpress.XtraEditors.ZoomTrackBarControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tmrUpdateStick = new System.Windows.Forms.Timer(this.components);
            this.xtraTabbedMdiManager1 = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            this.imageListBoxControlOutputs = new DevExpress.XtraEditors.ImageListBoxControl();
            this.imageListBoxControlInputs = new DevExpress.XtraEditors.ImageListBoxControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItemPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSave = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemCancelTelemetryAlarm = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroupGeneralOptions = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupAlarm = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlTemplates)).BeginInit();
            this.groupControlTemplates.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTemplates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTemplates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonUndo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonUnping)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlPlayback)).BeginInit();
            this.groupControlPlayback.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditEndDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditEndDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditStartDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditStartDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxCameras)).BeginInit();
            this.groupBoxCameras.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCameras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCameras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBoxDevices)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlExport)).BeginInit();
            this.groupControlExport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlAudioControl)).BeginInit();
            this.groupControlAudioControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControlVolume)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControlVolume.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zoomTrackBarControlSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zoomTrackBarControlSpeed.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageListBoxControlOutputs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageListBoxControlInputs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            this.SuspendLayout();
            // 
            // barButtonItemStartRegIncident
            // 
            this.barButtonItemStartRegIncident.Caption = "ItemStartRegIncident";
            this.barButtonItemStartRegIncident.Id = 12;
            this.barButtonItemStartRegIncident.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemStartRegIncident.Name = "barButtonItemStartRegIncident";
            this.barButtonItemStartRegIncident.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // layoutControl1
            // 
            this.layoutControl1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.layoutControl1.Controls.Add(this.groupControlTemplates);
            this.layoutControl1.Controls.Add(this.groupControlPlayback);
            this.layoutControl1.Controls.Add(this.groupBoxCameras);
            this.layoutControl1.Controls.Add(this.groupControlExport);
            this.layoutControl1.Controls.Add(this.groupControlAudioControl);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(488, 438, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(255, 671);
            this.layoutControl1.TabIndex = 6;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // groupControlTemplates
            // 
            this.groupControlTemplates.Controls.Add(this.gridControlTemplates);
            this.groupControlTemplates.Location = new System.Drawing.Point(4, 176);
            this.groupControlTemplates.Name = "groupControlTemplates";
            this.groupControlTemplates.Size = new System.Drawing.Size(247, 173);
            this.groupControlTemplates.TabIndex = 17;
            this.groupControlTemplates.Text = "Templates";
            // 
            // gridControlTemplates
            // 
            this.gridControlTemplates.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlTemplates.EnableAutoFilter = false;
            this.gridControlTemplates.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlTemplates.Location = new System.Drawing.Point(2, 22);
            this.gridControlTemplates.MainView = this.gridViewTemplates;
            this.gridControlTemplates.Name = "gridControlTemplates";
            this.gridControlTemplates.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox3,
            this.repositoryItemSpinEdit2,
            this.repositoryItemComboBox2,
            this.repositoryItemButtonUndo,
            this.repositoryItemButtonSave,
            this.repositoryItemButtonCancel,
            this.repositoryItemButtonEdit,
            this.repositoryItemButtonUnping});
            this.gridControlTemplates.Size = new System.Drawing.Size(243, 149);
            this.gridControlTemplates.TabIndex = 0;
            this.gridControlTemplates.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewTemplates});
            this.gridControlTemplates.ViewTotalRows = false;
            this.gridControlTemplates.Click += new System.EventHandler(this.gridControlTemplates_Click);
            this.gridControlTemplates.Enter += new System.EventHandler(this.gridControlTemplates_Enter);
            // 
            // gridViewTemplates
            // 
            this.gridViewTemplates.AllowFocusedRowChanged = true;
            this.gridViewTemplates.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewTemplates.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewTemplates.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewTemplates.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewTemplates.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.White;
            this.gridViewTemplates.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewTemplates.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewTemplates.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewTemplates.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnName});
            this.gridViewTemplates.EnablePreviewLineForFocusedRow = false;
            this.gridViewTemplates.GridControl = this.gridControlTemplates;
            this.gridViewTemplates.Name = "gridViewTemplates";
            this.gridViewTemplates.OptionsCustomization.AllowFilter = false;
            this.gridViewTemplates.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewTemplates.OptionsView.ShowColumnHeaders = false;
            this.gridViewTemplates.OptionsView.ShowDetailButtons = false;
            this.gridViewTemplates.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewTemplates.OptionsView.ShowGroupPanel = false;
            this.gridViewTemplates.OptionsView.ShowIndicator = false;
            this.gridViewTemplates.OptionsView.ShowPreviewLines = false;
            this.gridViewTemplates.OptionsView.ShowVertLines = false;
            this.gridViewTemplates.ViewTotalRows = false;
            this.gridViewTemplates.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewTemplates_FocusedRowChanged);
            // 
            // gridColumnName
            // 
            this.gridColumnName.FieldName = "Text";
            this.gridColumnName.Name = "gridColumnName";
            this.gridColumnName.OptionsColumn.AllowEdit = false;
            this.gridColumnName.Visible = true;
            this.gridColumnName.VisibleIndex = 0;
            this.gridColumnName.Width = 149;
            // 
            // repositoryItemComboBox3
            // 
            this.repositoryItemComboBox3.AutoHeight = false;
            this.repositoryItemComboBox3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox3.Items.AddRange(new object[] {
            "TCP",
            "UDP"});
            this.repositoryItemComboBox3.Name = "repositoryItemComboBox3";
            this.repositoryItemComboBox3.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // repositoryItemSpinEdit2
            // 
            this.repositoryItemSpinEdit2.AutoHeight = false;
            this.repositoryItemSpinEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit2.IsFloatValue = false;
            this.repositoryItemSpinEdit2.Mask.EditMask = "\\d{5}";
            this.repositoryItemSpinEdit2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.repositoryItemSpinEdit2.MaxValue = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.repositoryItemSpinEdit2.MinValue = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.repositoryItemSpinEdit2.Name = "repositoryItemSpinEdit2";
            this.repositoryItemSpinEdit2.ValidateOnEnterKey = true;
            // 
            // repositoryItemComboBox2
            // 
            this.repositoryItemComboBox2.AutoHeight = false;
            this.repositoryItemComboBox2.Name = "repositoryItemComboBox2";
            this.repositoryItemComboBox2.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // repositoryItemButtonUndo
            // 
            this.repositoryItemButtonUndo.Appearance.Options.UseImage = true;
            this.repositoryItemButtonUndo.AutoHeight = false;
            this.repositoryItemButtonUndo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, false, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonUndo.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.repositoryItemButtonUndo.Name = "repositoryItemButtonUndo";
            this.repositoryItemButtonUndo.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // repositoryItemButtonSave
            // 
            this.repositoryItemButtonSave.AutoHeight = false;
            this.repositoryItemButtonSave.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, false, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonSave.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, null, true)});
            this.repositoryItemButtonSave.Name = "repositoryItemButtonSave";
            this.repositoryItemButtonSave.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // repositoryItemButtonCancel
            // 
            this.repositoryItemButtonCancel.AutoHeight = false;
            this.repositoryItemButtonCancel.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonCancel.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject3, "", null, null, true)});
            this.repositoryItemButtonCancel.Name = "repositoryItemButtonCancel";
            this.repositoryItemButtonCancel.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // repositoryItemButtonEdit
            // 
            this.repositoryItemButtonEdit.AutoHeight = false;
            this.repositoryItemButtonEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonEdit.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject4, "", null, null, true)});
            this.repositoryItemButtonEdit.Name = "repositoryItemButtonEdit";
            this.repositoryItemButtonEdit.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // repositoryItemButtonUnping
            // 
            this.repositoryItemButtonUnping.AutoHeight = false;
            this.repositoryItemButtonUnping.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, false, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonUnping.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, "", null, null, true)});
            this.repositoryItemButtonUnping.Name = "repositoryItemButtonUnping";
            this.repositoryItemButtonUnping.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // groupControlPlayback
            // 
            this.groupControlPlayback.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.groupControlPlayback.Appearance.Options.UseBackColor = true;
            this.groupControlPlayback.Controls.Add(this.simpleButtonGoto);
            this.groupControlPlayback.Controls.Add(this.DateEditEndDate);
            this.groupControlPlayback.Controls.Add(this.DateEditStartDate);
            this.groupControlPlayback.Controls.Add(this.lbEndDate);
            this.groupControlPlayback.Controls.Add(this.lbStartDate);
            this.groupControlPlayback.Location = new System.Drawing.Point(4, 353);
            this.groupControlPlayback.Name = "groupControlPlayback";
            this.groupControlPlayback.Size = new System.Drawing.Size(247, 120);
            this.groupControlPlayback.TabIndex = 2;
            // 
            // simpleButtonGoto
            // 
            this.simpleButtonGoto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonGoto.Enabled = false;
            this.simpleButtonGoto.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonGoto.Image")));
            this.simpleButtonGoto.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonGoto.Location = new System.Drawing.Point(208, 84);
            this.simpleButtonGoto.Name = "simpleButtonGoto";
            this.simpleButtonGoto.Size = new System.Drawing.Size(25, 25);
            this.simpleButtonGoto.TabIndex = 46;
            this.simpleButtonGoto.EnabledChanged += new System.EventHandler(this.simpleButtonGoto_EnabledChanged);
            this.simpleButtonGoto.Click += new System.EventHandler(this.simpleButtonGoto_Click);
            // 
            // DateEditEndDate
            // 
            this.DateEditEndDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DateEditEndDate.EditValue = null;
            this.DateEditEndDate.Location = new System.Drawing.Point(87, 58);
            this.DateEditEndDate.Name = "DateEditEndDate";
            this.DateEditEndDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.DateEditEndDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject6, "", null, null, false)});
            this.DateEditEndDate.Properties.DisplayFormat.FormatString = "MM/dd/yyyy HH:mm:ss.fff";
            this.DateEditEndDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.DateEditEndDate.Properties.EditFormat.FormatString = "MM/dd/yyyy HH:mm:ss.fff";
            this.DateEditEndDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.DateEditEndDate.Properties.Mask.EditMask = "MM/dd/yyyy HH:mm:ss.fff";
            this.DateEditEndDate.Properties.ShowDropDown = DevExpress.XtraEditors.Controls.ShowDropDown.Never;
            this.DateEditEndDate.Properties.ShowPopupShadow = false;
            this.DateEditEndDate.Properties.ShowToday = false;
            this.DateEditEndDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DateEditEndDate.Size = new System.Drawing.Size(146, 20);
            this.DateEditEndDate.TabIndex = 45;
            // 
            // DateEditStartDate
            // 
            this.DateEditStartDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DateEditStartDate.EditValue = null;
            this.DateEditStartDate.Location = new System.Drawing.Point(87, 32);
            this.DateEditStartDate.Name = "DateEditStartDate";
            this.DateEditStartDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.DateEditStartDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject7, "", null, null, false)});
            this.DateEditStartDate.Properties.DisplayFormat.FormatString = "MM/dd/yyyy HH:mm:ss.fff";
            this.DateEditStartDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.DateEditStartDate.Properties.EditFormat.FormatString = "MM/dd/yyyy HH:mm:ss.fff";
            this.DateEditStartDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.DateEditStartDate.Properties.Mask.EditMask = "MM/dd/yyyy HH:mm:ss.fff";
            this.DateEditStartDate.Properties.ShowDropDown = DevExpress.XtraEditors.Controls.ShowDropDown.Never;
            this.DateEditStartDate.Properties.ShowPopupShadow = false;
            this.DateEditStartDate.Properties.ShowToday = false;
            this.DateEditStartDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DateEditStartDate.Size = new System.Drawing.Size(146, 20);
            this.DateEditStartDate.TabIndex = 44;
            // 
            // lbEndDate
            // 
            this.lbEndDate.AutoSize = true;
            this.lbEndDate.Location = new System.Drawing.Point(5, 61);
            this.lbEndDate.Name = "lbEndDate";
            this.lbEndDate.Size = new System.Drawing.Size(55, 13);
            this.lbEndDate.TabIndex = 1;
            this.lbEndDate.Text = "End Date:";
            // 
            // lbStartDate
            // 
            this.lbStartDate.AutoSize = true;
            this.lbStartDate.Location = new System.Drawing.Point(5, 35);
            this.lbStartDate.Name = "lbStartDate";
            this.lbStartDate.Size = new System.Drawing.Size(61, 13);
            this.lbStartDate.TabIndex = 0;
            this.lbStartDate.Text = "Start Date:";
            // 
            // groupBoxCameras
            // 
            this.groupBoxCameras.Controls.Add(this.gridControlCameras);
            this.groupBoxCameras.Location = new System.Drawing.Point(4, 4);
            this.groupBoxCameras.Name = "groupBoxCameras";
            this.groupBoxCameras.Size = new System.Drawing.Size(247, 168);
            this.groupBoxCameras.TabIndex = 16;
            this.groupBoxCameras.Text = "Cameras";
            // 
            // gridControlCameras
            // 
            this.gridControlCameras.AllowDrop = true;
            this.gridControlCameras.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlCameras.EnableAutoFilter = false;
            this.gridControlCameras.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlCameras.Location = new System.Drawing.Point(2, 22);
            this.gridControlCameras.MainView = this.gridViewCameras;
            this.gridControlCameras.Name = "gridControlCameras";
            this.gridControlCameras.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox1,
            this.repositoryItemSpinEdit1,
            this.repositoryItemComboBoxDevices,
            this.repositoryItemButtonEdit1});
            this.gridControlCameras.Size = new System.Drawing.Size(243, 144);
            this.gridControlCameras.TabIndex = 0;
            this.gridControlCameras.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewCameras});
            this.gridControlCameras.ViewTotalRows = false;
            this.gridControlCameras.Enter += new System.EventHandler(this.gridControlCameras_Enter);
            // 
            // gridViewCameras
            // 
            this.gridViewCameras.AllowFocusedRowChanged = true;
            this.gridViewCameras.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewCameras.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewCameras.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewCameras.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewCameras.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.White;
            this.gridViewCameras.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewCameras.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewCameras.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewCameras.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnCameraName});
            this.gridViewCameras.EnablePreviewLineForFocusedRow = false;
            this.gridViewCameras.GridControl = this.gridControlCameras;
            this.gridViewCameras.Name = "gridViewCameras";
            this.gridViewCameras.OptionsCustomization.AllowFilter = false;
            this.gridViewCameras.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewCameras.OptionsView.ShowColumnHeaders = false;
            this.gridViewCameras.OptionsView.ShowDetailButtons = false;
            this.gridViewCameras.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewCameras.OptionsView.ShowGroupPanel = false;
            this.gridViewCameras.OptionsView.ShowIndicator = false;
            this.gridViewCameras.ViewTotalRows = false;
            this.gridViewCameras.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewCameras_FocusedRowChanged);
            // 
            // gridColumnCameraName
            // 
            this.gridColumnCameraName.FieldName = "Text";
            this.gridColumnCameraName.Name = "gridColumnCameraName";
            this.gridColumnCameraName.OptionsColumn.AllowEdit = false;
            this.gridColumnCameraName.Visible = true;
            this.gridColumnCameraName.VisibleIndex = 0;
            this.gridColumnCameraName.Width = 149;
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Items.AddRange(new object[] {
            "TCP",
            "UDP"});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            this.repositoryItemComboBox1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit1.IsFloatValue = false;
            this.repositoryItemSpinEdit1.Mask.EditMask = "\\d{5}";
            this.repositoryItemSpinEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.repositoryItemSpinEdit1.MaxValue = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.MinValue = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            this.repositoryItemSpinEdit1.ValidateOnEnterKey = true;
            // 
            // repositoryItemComboBoxDevices
            // 
            this.repositoryItemComboBoxDevices.AutoHeight = false;
            this.repositoryItemComboBoxDevices.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBoxDevices.Name = "repositoryItemComboBoxDevices";
            this.repositoryItemComboBoxDevices.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonEdit1.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject8, "", null, null, true)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // groupControlExport
            // 
            this.groupControlExport.Controls.Add(this.simpleButtonExport);
            this.groupControlExport.Location = new System.Drawing.Point(4, 610);
            this.groupControlExport.Name = "groupControlExport";
            this.groupControlExport.Size = new System.Drawing.Size(247, 57);
            this.groupControlExport.TabIndex = 16;
            // 
            // simpleButtonExport
            // 
            this.simpleButtonExport.Enabled = false;
            this.simpleButtonExport.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonExport.Location = new System.Drawing.Point(87, 27);
            this.simpleButtonExport.Name = "simpleButtonExport";
            this.simpleButtonExport.Size = new System.Drawing.Size(61, 25);
            this.simpleButtonExport.TabIndex = 50;
            this.simpleButtonExport.Text = "Export";
            this.simpleButtonExport.Click += new System.EventHandler(this.simpleButtonExport_Click);
            // 
            // groupControlAudioControl
            // 
            this.groupControlAudioControl.Controls.Add(this.checkButtonMute);
            this.groupControlAudioControl.Controls.Add(this.labelControlPlaybackSpeed);
            this.groupControlAudioControl.Controls.Add(this.labelVideoTime);
            this.groupControlAudioControl.Controls.Add(this.checkButtonBack);
            this.groupControlAudioControl.Controls.Add(this.trackBarControlVolume);
            this.groupControlAudioControl.Controls.Add(this.checkButtonPlay);
            this.groupControlAudioControl.Controls.Add(this.simpleButtonStop);
            this.groupControlAudioControl.Controls.Add(this.zoomTrackBarControlSpeed);
            this.groupControlAudioControl.Location = new System.Drawing.Point(4, 477);
            this.groupControlAudioControl.Name = "groupControlAudioControl";
            this.groupControlAudioControl.Size = new System.Drawing.Size(247, 129);
            this.groupControlAudioControl.TabIndex = 11;
            // 
            // checkButtonMute
            // 
            this.checkButtonMute.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.checkButtonMute.Enabled = false;
            this.checkButtonMute.Image = global::SmartCadGuiCommon.Properties.Resources.soundSmall;
            this.checkButtonMute.Location = new System.Drawing.Point(212, 99);
            this.checkButtonMute.Name = "checkButtonMute";
            this.checkButtonMute.Size = new System.Drawing.Size(25, 25);
            this.checkButtonMute.TabIndex = 60;
            this.checkButtonMute.CheckedChanged += new System.EventHandler(this.checkButtonMute_CheckedChanged);
            // 
            // labelControlPlaybackSpeed
            // 
            this.labelControlPlaybackSpeed.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControlPlaybackSpeed.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControlPlaybackSpeed.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControlPlaybackSpeed.Location = new System.Drawing.Point(52, 89);
            this.labelControlPlaybackSpeed.Name = "labelControlPlaybackSpeed";
            this.labelControlPlaybackSpeed.Size = new System.Drawing.Size(107, 13);
            this.labelControlPlaybackSpeed.TabIndex = 59;
            this.labelControlPlaybackSpeed.Text = "Speed: 1X";
            // 
            // labelVideoTime
            // 
            this.labelVideoTime.AutoSize = true;
            this.labelVideoTime.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelVideoTime.Location = new System.Drawing.Point(33, 31);
            this.labelVideoTime.Name = "labelVideoTime";
            this.labelVideoTime.Size = new System.Drawing.Size(154, 18);
            this.labelVideoTime.TabIndex = 56;
            this.labelVideoTime.Text = "05/02/2012 15:25:16";
            // 
            // checkButtonBack
            // 
            this.checkButtonBack.Enabled = false;
            this.checkButtonBack.Image = ((System.Drawing.Image)(resources.GetObject("checkButtonBack.Image")));
            this.checkButtonBack.Location = new System.Drawing.Point(63, 62);
            this.checkButtonBack.Name = "checkButtonBack";
            this.checkButtonBack.Size = new System.Drawing.Size(25, 25);
            this.checkButtonBack.TabIndex = 22;
            this.checkButtonBack.Click += new System.EventHandler(this.simpleButtonBack_Click);
            // 
            // trackBarControlVolume
            // 
            this.trackBarControlVolume.EditValue = 10;
            this.trackBarControlVolume.Enabled = false;
            this.trackBarControlVolume.Location = new System.Drawing.Point(210, 18);
            this.trackBarControlVolume.Name = "trackBarControlVolume";
            this.trackBarControlVolume.Properties.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trackBarControlVolume.Properties.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBarControlVolume.Size = new System.Drawing.Size(45, 84);
            this.trackBarControlVolume.TabIndex = 14;
            this.trackBarControlVolume.Value = 10;
            this.trackBarControlVolume.EditValueChanged += new System.EventHandler(this.trackBarControlPlayback_EditValueChanged);
            // 
            // checkButtonPlay
            // 
            this.checkButtonPlay.Enabled = false;
            this.checkButtonPlay.Image = global::SmartCadGuiCommon.Properties.Resources.Play;
            this.checkButtonPlay.Location = new System.Drawing.Point(125, 62);
            this.checkButtonPlay.Name = "checkButtonPlay";
            this.checkButtonPlay.Size = new System.Drawing.Size(25, 25);
            this.checkButtonPlay.TabIndex = 54;
            this.checkButtonPlay.Click += new System.EventHandler(this.simpleButtonPlay_Click);
            // 
            // simpleButtonStop
            // 
            this.simpleButtonStop.Enabled = false;
            this.simpleButtonStop.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonStop.Image")));
            this.simpleButtonStop.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonStop.Location = new System.Drawing.Point(94, 62);
            this.simpleButtonStop.Name = "simpleButtonStop";
            this.simpleButtonStop.Size = new System.Drawing.Size(25, 25);
            this.simpleButtonStop.TabIndex = 47;
            this.simpleButtonStop.ToolTipController = this.toolTipController1;
            this.simpleButtonStop.Click += new System.EventHandler(this.simpleButtonStop_Click);
            // 
            // zoomTrackBarControlSpeed
            // 
            this.zoomTrackBarControlSpeed.EditValue = 4;
            this.zoomTrackBarControlSpeed.Enabled = false;
            this.zoomTrackBarControlSpeed.Location = new System.Drawing.Point(27, 103);
            this.zoomTrackBarControlSpeed.Name = "zoomTrackBarControlSpeed";
            this.zoomTrackBarControlSpeed.Properties.LargeChange = 1;
            this.zoomTrackBarControlSpeed.Properties.Maximum = 8;
            this.zoomTrackBarControlSpeed.Properties.ScrollThumbStyle = DevExpress.XtraEditors.Repository.ScrollThumbStyle.ArrowDownRight;
            this.zoomTrackBarControlSpeed.Size = new System.Drawing.Size(160, 20);
            this.zoomTrackBarControlSpeed.TabIndex = 58;
            this.zoomTrackBarControlSpeed.Value = 4;
            this.zoomTrackBarControlSpeed.EditValueChanged += new System.EventHandler(this.zoomTrackBarControlSpeed_EditValueChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem4,
            this.layoutControlItem5});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(255, 671);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.groupControlPlayback;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 349);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(0, 124);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(104, 124);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(251, 124);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.groupControlAudioControl;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 473);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(0, 133);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(104, 133);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(251, 133);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.groupControlExport;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 606);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 61);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(251, 61);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(251, 61);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.groupBoxCameras;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(251, 172);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.groupControlTemplates;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 172);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(251, 177);
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // tmrUpdateStick
            // 
            this.tmrUpdateStick.Interval = 1;
            // 
            // xtraTabbedMdiManager1
            // 
            this.xtraTabbedMdiManager1.MdiParent = null;
            // 
            // imageListBoxControlOutputs
            // 
            this.imageListBoxControlOutputs.Location = new System.Drawing.Point(261, 603);
            this.imageListBoxControlOutputs.Name = "imageListBoxControlOutputs";
            this.imageListBoxControlOutputs.Size = new System.Drawing.Size(10, 10);
            this.imageListBoxControlOutputs.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.imageListBoxControlOutputs.TabIndex = 8;
            this.imageListBoxControlOutputs.Visible = false;
            // 
            // imageListBoxControlInputs
            // 
            this.imageListBoxControlInputs.Location = new System.Drawing.Point(277, 603);
            this.imageListBoxControlInputs.Name = "imageListBoxControlInputs";
            this.imageListBoxControlInputs.Size = new System.Drawing.Size(10, 10);
            this.imageListBoxControlInputs.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.imageListBoxControlInputs.TabIndex = 7;
            this.imageListBoxControlInputs.Visible = false;
            // 
            // panelControl1
            // 
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(255, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(945, 671);
            this.panelControl1.TabIndex = 10;
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ApplicationButtonAnimationLength = 1;
            this.ribbonControl1.ExpandCollapseItem.AllowRightClickInMenu = false;
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.ExpandCollapseItem.Name = "";
            this.ribbonControl1.GroupAnimationLength = 1;
            this.ribbonControl1.ItemAnimationLength = 1;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.barButtonItemPrint,
            this.barButtonItemSave,
            this.barButtonItemRefresh,
            this.barButtonItemCancelTelemetryAlarm});
            this.ribbonControl1.Location = new System.Drawing.Point(255, 0);
            this.ribbonControl1.MaxItemId = 45;
            this.ribbonControl1.MdiMergeStyle = DevExpress.XtraBars.Ribbon.RibbonMdiMergeStyle.Always;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.PageAnimationLength = 1;
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbonControl1.ShowExpandCollapseButton = DevExpress.Utils.DefaultBoolean.True;
            this.ribbonControl1.ShowToolbarCustomizeItem = false;
            this.ribbonControl1.Size = new System.Drawing.Size(945, 120);
            this.ribbonControl1.Toolbar.ShowCustomizeItem = false;
            this.ribbonControl1.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // barButtonItemPrint
            // 
            this.barButtonItemPrint.Caption = "ItemPrint";
            this.barButtonItemPrint.Enabled = false;
            this.barButtonItemPrint.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.Glyph")));
            this.barButtonItemPrint.Id = 6;
            this.barButtonItemPrint.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemPrint.Name = "barButtonItemPrint";
            this.barButtonItemPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPrint_ItemClick);
            // 
            // barButtonItemSave
            // 
            this.barButtonItemSave.Caption = "ItemSave";
            this.barButtonItemSave.Enabled = false;
            this.barButtonItemSave.Glyph = global::SmartCadGuiCommon.PrintRibbonControllerResources.RibbonPrintPreview_Save;
            this.barButtonItemSave.Id = 7;
            this.barButtonItemSave.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemSave.Name = "barButtonItemSave";
            this.barButtonItemSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemSave_ItemClick);
            // 
            // barButtonItemRefresh
            // 
            this.barButtonItemRefresh.Caption = "ItemRefresh";
            this.barButtonItemRefresh.Enabled = false;
            this.barButtonItemRefresh.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemRefresh.Glyph")));
            this.barButtonItemRefresh.Id = 8;
            this.barButtonItemRefresh.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemRefresh.Name = "barButtonItemRefresh";
            // 
            // barButtonItemCancelTelemetryAlarm
            // 
            this.barButtonItemCancelTelemetryAlarm.Caption = "barButtonItemCancelAlarm";
            this.barButtonItemCancelTelemetryAlarm.Enabled = false;
            this.barButtonItemCancelTelemetryAlarm.Id = 41;
            this.barButtonItemCancelTelemetryAlarm.LargeGlyph = global::SmartCadGuiCommon.Properties.Resources.CancelAlarm1;
            this.barButtonItemCancelTelemetryAlarm.Name = "barButtonItemCancelTelemetryAlarm";
            //this.barButtonItemCancelTelemetryAlarm.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemCancelTelemetryAlarm_ItemClick);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupGeneralOptions});
            //this.ribbonPageGroupAlarm});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "ribbonPage1";
            // 
            // ribbonPageGroupGeneralOptions
            // 
            this.ribbonPageGroupGeneralOptions.AllowMinimize = false;
            this.ribbonPageGroupGeneralOptions.AllowTextClipping = false;
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemPrint);
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemSave);
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemRefresh);
            this.ribbonPageGroupGeneralOptions.Name = "ribbonPageGroupGeneralOptions";
            this.ribbonPageGroupGeneralOptions.ShowCaptionButton = false;
            this.ribbonPageGroupGeneralOptions.Text = "ribbonPageGroupGeneralOptions";
            // 
            // ribbonPageGroupAlarm
            //
            this.ribbonPageGroupAlarm.AllowMinimize = false;
            this.ribbonPageGroupAlarm.AllowTextClipping = false;
            //this.ribbonPageGroupAlarm.ItemLinks.Add(this.barButtonItemCancelTelemetryAlarm);
            this.ribbonPageGroupAlarm.Name = "ribbonPageGroupAlarm";
            this.ribbonPageGroupAlarm.ShowCaptionButton = false;
            this.ribbonPageGroupAlarm.Text = "ribbonPageGroupAlarm";
            // 
            // PlayBackForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1200, 671);
            this.Controls.Add(this.ribbonControl1);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.imageListBoxControlOutputs);
            this.Controls.Add(this.imageListBoxControlInputs);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MinimumSize = new System.Drawing.Size(792, 671);
            this.Name = "PlayBackForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CamerasRibbonForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PlayBackForm_FormClosing);
            this.Load += new System.EventHandler(this.PlayBackForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControlTemplates)).EndInit();
            this.groupControlTemplates.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTemplates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTemplates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonUndo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonUnping)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlPlayback)).EndInit();
            this.groupControlPlayback.ResumeLayout(false);
            this.groupControlPlayback.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditEndDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditEndDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditStartDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditStartDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxCameras)).EndInit();
            this.groupBoxCameras.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCameras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCameras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBoxDevices)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlExport)).EndInit();
            this.groupControlExport.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControlAudioControl)).EndInit();
            this.groupControlAudioControl.ResumeLayout(false);
            this.groupControlAudioControl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControlVolume.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControlVolume)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zoomTrackBarControlSpeed.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zoomTrackBarControlSpeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageListBoxControlOutputs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageListBoxControlInputs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraBars.BarButtonItem barButtonItemStartRegIncident;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        public DevExpress.XtraEditors.GroupControl groupControlPlayback;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        public DevExpress.XtraEditors.GroupControl groupControlAudioControl;
        private System.Windows.Forms.Label lbEndDate;
        private System.Windows.Forms.Label lbStartDate;
        public DevExpress.XtraEditors.DateEdit DateEditEndDate;
        public DevExpress.XtraEditors.DateEdit DateEditStartDate;
        private DevExpress.XtraEditors.SimpleButton simpleButtonGoto;
        private DevExpress.Utils.ToolTipController toolTipController1;
        public DevExpress.XtraEditors.TrackBarControl trackBarControlVolume;
        public DevExpress.XtraEditors.GroupControl groupControlExport;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.SimpleButton simpleButtonExport;
        public System.Windows.Forms.Timer tmrUpdateStick;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager xtraTabbedMdiManager1;
        public DevExpress.XtraEditors.ImageListBoxControl imageListBoxControlOutputs;
        public DevExpress.XtraEditors.ImageListBoxControl imageListBoxControlInputs;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.GroupControl groupControlTemplates;
        private GridControlEx gridControlTemplates;
        private GridViewEx gridViewTemplates;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonUndo;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonSave;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonCancel;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox3;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox2;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonUnping;
        private DevExpress.XtraEditors.GroupControl groupBoxCameras;
        private GridControlEx gridControlCameras;
        private GridViewEx gridViewCameras;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBoxDevices;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        public DevExpress.XtraEditors.CheckButton checkButtonBack;
        public DevExpress.XtraEditors.CheckButton checkButtonPlay;
        private DevExpress.XtraEditors.SimpleButton simpleButtonStop;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private System.Windows.Forms.Label labelVideoTime;
        private DevExpress.XtraEditors.LabelControl labelControlPlaybackSpeed;
        public DevExpress.XtraEditors.ZoomTrackBarControl zoomTrackBarControlSpeed;
        public DevExpress.XtraEditors.CheckButton checkButtonMute;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnCameraName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnName;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.BarButtonItem barButtonItemPrint;
        private DevExpress.XtraBars.BarButtonItem barButtonItemSave;
        private DevExpress.XtraBars.BarButtonItem barButtonItemRefresh;
        public DevExpress.XtraBars.BarButtonItem barButtonItemCancelTelemetryAlarm;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupGeneralOptions;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupAlarm;

    }
}