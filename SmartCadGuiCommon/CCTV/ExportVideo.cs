using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.Threading;
using DevExpress.XtraEditors;
using Smartmatic.SmartCad.Vms;
using Smartmatic.SmartCad.Service;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadGuiCommon.Classes;

namespace SmartCadGuiCommon
{
    public partial class ExportVideo : XtraForm
    {
        private ConfigurationClientData configuration;
        private OperatorClientData oper;
        private IPlayBackPanel Control;

        public ExportVideo(IPlayBackPanel control, DateTime startDate, DateTime endDate)
        {
            InitializeComponent();
            Control = control;
            this.dateEditStartTime.DateTime = startDate;
            this.dateEditEndTime.DateTime = endDate;
            configuration = ServerServiceClient.GetInstance().GetConfiguration();
            oper = ServerServiceClient.GetInstance().OperatorClient;
        }

        private void ExportVideo_Load(object sender, EventArgs e)
        {
            this.radioGroupTimeStamp.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, ResourceLoader.GetString2("Yes")),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, ResourceLoader.GetString2("No"))});
            LoadLanguage();
        }

        private void LoadLanguage()
        {
            this.Text = ResourceLoader.GetString2("ExportVideo");
            this.groupControlExportStatus.Text = ResourceLoader.GetString2("ExportStatus");
            this.lblStartTime.Text = ResourceLoader.GetString2("ExportStartTime");
            this.lblEndTime.Text = ResourceLoader.GetString2("ExportEndTime");
            this.lblSource.Text = ResourceLoader.GetString2("Exportsource");

            this.groupControlExportType.Text = ResourceLoader.GetString2("ExportType");
            this.lblExportFormat.Text = ResourceLoader.GetString2("ExportFormat");
            this.lblTimestamp.Text = ResourceLoader.GetString2("ExportTimestamp");

            this.simpleButtonExport.Text = ResourceLoader.GetString2("ExportButton");
            this.simpleButtonClose.Text = ResourceLoader.GetString2("Cancel");
            this.lblSourceName.Text = Control.Name;
        }

        private void ExportVideo_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.FormClosing -= new System.Windows.Forms.FormClosingEventHandler(this.ExportVideo_FormClosing);
            this.Close();
        }

        private void simpleButtonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dateEditStartTime_DateTimeChanged(object sender, EventArgs e)
        {
            if (this.dateEditStartTime.DateTime >= this.dateEditEndTime.DateTime)
                this.dateEditEndTime.DateTime = this.dateEditStartTime.DateTime.AddSeconds(1);
        }

        private void dateEditEndTime_DateTimeChanged(object sender, EventArgs e)
        {
            if (this.dateEditStartTime.DateTime >= this.dateEditEndTime.DateTime)
                this.dateEditEndTime.DateTime = this.dateEditStartTime.DateTime.AddSeconds(1);  
        }

        private void simpleButtonExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog fDialog = new SaveFileDialog();
            fDialog.Filter = "AVI Video (*.avi)|*.avi|JPEG Image (*.jpg)|*.jpg";
            fDialog.FilterIndex = 1;
            if (fDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK && fDialog.FileName != "")
            {
                ///aqui invoco la exportacion
                
                Control.ExportVideo(this.dateEditStartTime.DateTime, this.dateEditEndTime.DateTime, fDialog.FileName,
                    this.radioGroupExportFormat.SelectedIndex, this.radioGroupTimeStamp.SelectedIndex);

                this.Close();

           //     Control.CameraPanelControlTemplates();
           //     Control.ClearVMSControlPlayback();
                

            }
        }
    }
}