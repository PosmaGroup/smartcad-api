using SmartCadControls;
namespace SmartCadGuiCommon
{
    partial class CameraVisualizeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CameraVisualizeForm));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            this.layoutControlGroupMainWindow = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControlDisplayArea = new DevExpress.XtraLayout.LayoutControl();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.groupControlTemplates = new DevExpress.XtraEditors.GroupControl();
            this.gridControlTemplates = new GridControlEx();
            this.gridViewTemplates = new GridViewEx();
            this.gridColumnName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnUndo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnSave = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnEdit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnCancel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox3 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemSpinEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemButtonUndo = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemButtonSave = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemButtonEdit = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemButtonCancel = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemButtonUnping = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.groupBoxCameras = new DevExpress.XtraEditors.GroupControl();
            this.gridControlCameras = new GridControlEx();
            this.gridViewCameras = new GridViewEx();
            this.gridColumnCameraName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemComboBoxDevices = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.toolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItemPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSave = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroupGeneralOptions = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupMainWindow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlDisplayArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlTemplates)).BeginInit();
            this.groupControlTemplates.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTemplates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTemplates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonUndo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonUnping)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxCameras)).BeginInit();
            this.groupBoxCameras.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCameras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCameras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBoxDevices)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControlGroupMainWindow
            // 
            this.layoutControlGroupMainWindow.CustomizationFormText = "layoutControlGroupMainWindow";
            this.layoutControlGroupMainWindow.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupMainWindow.Name = "Root";
            this.layoutControlGroupMainWindow.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroupMainWindow.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupMainWindow.Size = new System.Drawing.Size(752, 770);
            this.layoutControlGroupMainWindow.Text = "Root";
            this.layoutControlGroupMainWindow.TextVisible = false;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroupMainWindow";
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(752, 770);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroupMainWindow";
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup2.Size = new System.Drawing.Size(752, 770);
            this.layoutControlGroup2.Text = "Root";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.layoutControlDisplayArea);
            this.layoutControl1.Controls.Add(this.groupControlTemplates);
            this.layoutControl1.Controls.Add(this.groupBoxCameras);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(587, 301, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup3;
            this.layoutControl1.Size = new System.Drawing.Size(1120, 641);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // layoutControlDisplayArea
            // 
            this.layoutControlDisplayArea.Location = new System.Drawing.Point(218, 4);
            this.layoutControlDisplayArea.Name = "layoutControlDisplayArea";
            this.layoutControlDisplayArea.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1599, 262, 250, 350);
            this.layoutControlDisplayArea.Root = this.Root;
            this.layoutControlDisplayArea.Size = new System.Drawing.Size(898, 633);
            this.layoutControlDisplayArea.TabIndex = 24;
            this.layoutControlDisplayArea.Text = "layoutControl2";
            // 
            // Root
            // 
            this.Root.CustomizationFormText = "Root";
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.Location = new System.Drawing.Point(0, 0);
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(898, 633);
            this.Root.Text = "Root";
            this.Root.TextVisible = false;
            // 
            // groupControlTemplates
            // 
            this.groupControlTemplates.Controls.Add(this.gridControlTemplates);
            this.groupControlTemplates.Location = new System.Drawing.Point(4, 326);
            this.groupControlTemplates.Name = "groupControlTemplates";
            this.groupControlTemplates.Size = new System.Drawing.Size(210, 311);
            this.groupControlTemplates.TabIndex = 15;
            this.groupControlTemplates.Text = "Templates";
            // 
            // gridControlTemplates
            // 
            this.gridControlTemplates.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlTemplates.EnableAutoFilter = false;
            this.gridControlTemplates.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlTemplates.Location = new System.Drawing.Point(2, 22);
            this.gridControlTemplates.MainView = this.gridViewTemplates;
            this.gridControlTemplates.Name = "gridControlTemplates";
            this.gridControlTemplates.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox3,
            this.repositoryItemSpinEdit2,
            this.repositoryItemComboBox2,
            this.repositoryItemButtonUndo,
            this.repositoryItemButtonSave,
            this.repositoryItemButtonEdit,
            this.repositoryItemButtonCancel,
            this.repositoryItemButtonUnping});
            this.gridControlTemplates.Size = new System.Drawing.Size(206, 287);
            this.gridControlTemplates.TabIndex = 0;
            this.gridControlTemplates.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewTemplates});
            this.gridControlTemplates.ViewTotalRows = false;
            // 
            // gridViewTemplates
            // 
            this.gridViewTemplates.AllowFocusedRowChanged = true;
            this.gridViewTemplates.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewTemplates.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewTemplates.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewTemplates.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewTemplates.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewTemplates.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewTemplates.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewTemplates.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewTemplates.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnName,
            this.gridColumnUndo,
            this.gridColumnSave,
            this.gridColumnEdit,
            this.gridColumnCancel});
            this.gridViewTemplates.EnablePreviewLineForFocusedRow = false;
            this.gridViewTemplates.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None;
            this.gridViewTemplates.GridControl = this.gridControlTemplates;
            this.gridViewTemplates.Name = "gridViewTemplates";
            this.gridViewTemplates.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewTemplates.OptionsCustomization.AllowColumnResizing = false;
            this.gridViewTemplates.OptionsCustomization.AllowFilter = false;
            this.gridViewTemplates.OptionsCustomization.AllowGroup = false;
            this.gridViewTemplates.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewTemplates.OptionsCustomization.AllowSort = false;
            this.gridViewTemplates.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewTemplates.OptionsView.AnimationType = DevExpress.XtraGrid.Views.Base.GridAnimationType.NeverAnimate;
            this.gridViewTemplates.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top;
            this.gridViewTemplates.OptionsView.ShowColumnHeaders = false;
            this.gridViewTemplates.OptionsView.ShowDetailButtons = false;
            this.gridViewTemplates.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewTemplates.OptionsView.ShowGroupPanel = false;
            this.gridViewTemplates.OptionsView.ShowPreviewLines = false;
            this.gridViewTemplates.OptionsView.ShowVertLines = false;
            this.gridViewTemplates.ViewTotalRows = false;
            this.gridViewTemplates.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewTemplates_CustomRowCellEdit);
            this.gridViewTemplates.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewTemplates_FocusedRowChanged);
            this.gridViewTemplates.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridViewTemplates_RowUpdated);
            this.gridViewTemplates.DoubleClick += new System.EventHandler(this.gridViewTemplates_DoubleClick);
            // 
            // gridColumnName
            // 
            this.gridColumnName.FieldName = "Text";
            this.gridColumnName.Name = "gridColumnName";
            this.gridColumnName.OptionsColumn.AllowEdit = false;
            this.gridColumnName.Visible = true;
            this.gridColumnName.VisibleIndex = 0;
            this.gridColumnName.Width = 180;
            // 
            // gridColumnUndo
            // 
            this.gridColumnUndo.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumnUndo.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnUndo.Caption = "Undo";
            this.gridColumnUndo.FieldName = "Undo";
            this.gridColumnUndo.MaxWidth = 20;
            this.gridColumnUndo.Name = "gridColumnUndo";
            this.gridColumnUndo.OptionsColumn.AllowMove = false;
            this.gridColumnUndo.OptionsColumn.AllowShowHide = false;
            this.gridColumnUndo.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnUndo.OptionsColumn.ShowCaption = false;
            this.gridColumnUndo.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumnUndo.OptionsColumn.ShowInExpressionEditor = false;
            this.gridColumnUndo.Visible = true;
            this.gridColumnUndo.VisibleIndex = 1;
            this.gridColumnUndo.Width = 20;
            // 
            // gridColumnSave
            // 
            this.gridColumnSave.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumnSave.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnSave.Caption = "Save";
            this.gridColumnSave.FieldName = "Save";
            this.gridColumnSave.MaxWidth = 20;
            this.gridColumnSave.Name = "gridColumnSave";
            this.gridColumnSave.Visible = true;
            this.gridColumnSave.VisibleIndex = 2;
            this.gridColumnSave.Width = 20;
            // 
            // gridColumnEdit
            // 
            this.gridColumnEdit.FieldName = "Edit";
            this.gridColumnEdit.MaxWidth = 20;
            this.gridColumnEdit.Name = "gridColumnEdit";
            this.gridColumnEdit.Visible = true;
            this.gridColumnEdit.VisibleIndex = 3;
            this.gridColumnEdit.Width = 20;
            // 
            // gridColumnCancel
            // 
            this.gridColumnCancel.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumnCancel.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnCancel.Caption = "Cancel";
            this.gridColumnCancel.FieldName = "Cancel";
            this.gridColumnCancel.MaxWidth = 20;
            this.gridColumnCancel.Name = "gridColumnCancel";
            this.gridColumnCancel.Visible = true;
            this.gridColumnCancel.VisibleIndex = 4;
            this.gridColumnCancel.Width = 20;
            // 
            // repositoryItemComboBox3
            // 
            this.repositoryItemComboBox3.AutoHeight = false;
            this.repositoryItemComboBox3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox3.Items.AddRange(new object[] {
            "TCP",
            "UDP"});
            this.repositoryItemComboBox3.Name = "repositoryItemComboBox3";
            this.repositoryItemComboBox3.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // repositoryItemSpinEdit2
            // 
            this.repositoryItemSpinEdit2.AutoHeight = false;
            this.repositoryItemSpinEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit2.IsFloatValue = false;
            this.repositoryItemSpinEdit2.Mask.EditMask = "\\d{5}";
            this.repositoryItemSpinEdit2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.repositoryItemSpinEdit2.MaxValue = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.repositoryItemSpinEdit2.MinValue = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.repositoryItemSpinEdit2.Name = "repositoryItemSpinEdit2";
            this.repositoryItemSpinEdit2.ValidateOnEnterKey = true;
            // 
            // repositoryItemComboBox2
            // 
            this.repositoryItemComboBox2.AutoHeight = false;
            this.repositoryItemComboBox2.Name = "repositoryItemComboBox2";
            this.repositoryItemComboBox2.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // repositoryItemButtonUndo
            // 
            this.repositoryItemButtonUndo.Appearance.Options.UseImage = true;
            this.repositoryItemButtonUndo.AutoHeight = false;
            this.repositoryItemButtonUndo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, false, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonUndo.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.repositoryItemButtonUndo.Name = "repositoryItemButtonUndo";
            this.repositoryItemButtonUndo.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonUndo.Click += new System.EventHandler(this.repositoryItemButtonUndo_Click);
            // 
            // repositoryItemButtonSave
            // 
            this.repositoryItemButtonSave.AutoHeight = false;
            this.repositoryItemButtonSave.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, false, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonSave.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, null, true)});
            this.repositoryItemButtonSave.Name = "repositoryItemButtonSave";
            this.repositoryItemButtonSave.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonSave.Click += new System.EventHandler(this.repositoryItemButtonSave_Click);
            // 
            // repositoryItemButtonEdit
            // 
            this.repositoryItemButtonEdit.AutoHeight = false;
            this.repositoryItemButtonEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonEdit.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject3, "", null, null, true)});
            this.repositoryItemButtonEdit.Name = "repositoryItemButtonEdit";
            this.repositoryItemButtonEdit.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit.Click += new System.EventHandler(this.repositoryItemButtonEdit_Click);
            // 
            // repositoryItemButtonCancel
            // 
            this.repositoryItemButtonCancel.AutoHeight = false;
            this.repositoryItemButtonCancel.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonCancel.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject4, "", null, null, true)});
            this.repositoryItemButtonCancel.Name = "repositoryItemButtonCancel";
            this.repositoryItemButtonCancel.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonCancel.Click += new System.EventHandler(this.repositoryItemButtonCancel_Click);
            // 
            // repositoryItemButtonUnping
            // 
            this.repositoryItemButtonUnping.AutoHeight = false;
            this.repositoryItemButtonUnping.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, false, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonUnping.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, "", null, null, true)});
            this.repositoryItemButtonUnping.Name = "repositoryItemButtonUnping";
            this.repositoryItemButtonUnping.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // groupBoxCameras
            // 
            this.groupBoxCameras.Controls.Add(this.gridControlCameras);
            this.groupBoxCameras.Location = new System.Drawing.Point(4, 4);
            this.groupBoxCameras.Name = "groupBoxCameras";
            this.groupBoxCameras.Size = new System.Drawing.Size(210, 318);
            this.groupBoxCameras.TabIndex = 14;
            this.groupBoxCameras.Text = "Cameras";
            // 
            // gridControlCameras
            // 
            this.gridControlCameras.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlCameras.EnableAutoFilter = false;
            this.gridControlCameras.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlCameras.Location = new System.Drawing.Point(2, 22);
            this.gridControlCameras.MainView = this.gridViewCameras;
            this.gridControlCameras.Name = "gridControlCameras";
            this.gridControlCameras.Size = new System.Drawing.Size(206, 294);
            this.gridControlCameras.TabIndex = 0;
            this.gridControlCameras.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewCameras});
            this.gridControlCameras.ViewTotalRows = false;
            this.gridControlCameras.DragDrop += new System.Windows.Forms.DragEventHandler(this.gridControlCameras_DragDrop);
            this.gridControlCameras.MouseMove += new System.Windows.Forms.MouseEventHandler(this.gridControlCameras_MouseMove);
            // 
            // gridViewCameras
            // 
            this.gridViewCameras.AllowFocusedRowChanged = true;
            this.gridViewCameras.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewCameras.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewCameras.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewCameras.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewCameras.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewCameras.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewCameras.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewCameras.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewCameras.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnCameraName});
            this.gridViewCameras.EnablePreviewLineForFocusedRow = false;
            this.gridViewCameras.GridControl = this.gridControlCameras;
            this.gridViewCameras.Name = "gridViewCameras";
            this.gridViewCameras.OptionsCustomization.AllowFilter = false;
            this.gridViewCameras.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewCameras.OptionsView.ShowColumnHeaders = false;
            this.gridViewCameras.OptionsView.ShowDetailButtons = false;
            this.gridViewCameras.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewCameras.OptionsView.ShowGroupPanel = false;
            this.gridViewCameras.OptionsView.ShowIndicator = false;
            this.gridViewCameras.ViewTotalRows = false;
            this.gridViewCameras.DoubleClick += new System.EventHandler(this.gridViewCameras_DoubleClick);
            // 
            // gridColumnCameraName
            // 
            this.gridColumnCameraName.Caption = "Name";
            this.gridColumnCameraName.FieldName = "Text";
            this.gridColumnCameraName.Name = "gridColumnCameraName";
            this.gridColumnCameraName.OptionsColumn.AllowEdit = false;
            this.gridColumnCameraName.Visible = true;
            this.gridColumnCameraName.VisibleIndex = 0;
            this.gridColumnCameraName.Width = 149;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Root";
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.layoutControlItem1,
            this.layoutControlItem2});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "Root";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup3.Size = new System.Drawing.Size(1120, 641);
            this.layoutControlGroup3.Text = "Root";
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.groupBoxCameras;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(214, 322);
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.groupControlTemplates;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 322);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(214, 315);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.layoutControlDisplayArea;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(214, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(902, 637);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Items.AddRange(new object[] {
            "TCP",
            "UDP"});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            this.repositoryItemComboBox1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit1.IsFloatValue = false;
            this.repositoryItemSpinEdit1.Mask.EditMask = "\\d{5}";
            this.repositoryItemSpinEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.repositoryItemSpinEdit1.MaxValue = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.MinValue = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            this.repositoryItemSpinEdit1.ValidateOnEnterKey = true;
            // 
            // repositoryItemComboBoxDevices
            // 
            this.repositoryItemComboBoxDevices.AutoHeight = false;
            this.repositoryItemComboBoxDevices.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBoxDevices.Name = "repositoryItemComboBoxDevices";
            this.repositoryItemComboBoxDevices.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonEdit1.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject6, "", null, null, true)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "layout-icon.png");
            this.imageCollection1.Images.SetKeyName(1, "camaras 16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "layout-4-icon.png");
            this.imageCollection1.Images.SetKeyName(3, "lista de camaras.png");
            // 
            // defaultLookAndFeel1
            // 
            this.defaultLookAndFeel1.LookAndFeel.SkinName = "Blue";
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ApplicationButtonAnimationLength = 1;
            this.ribbonControl1.ExpandCollapseItem.AllowRightClickInMenu = false;
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.ExpandCollapseItem.Name = "";
            this.ribbonControl1.GroupAnimationLength = 1;
            this.ribbonControl1.ItemAnimationLength = 1;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.barButtonItemPrint,
            this.barButtonItemSave,
            this.barButtonItemRefresh});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 25;
            this.ribbonControl1.MdiMergeStyle = DevExpress.XtraBars.Ribbon.RibbonMdiMergeStyle.Always;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.PageAnimationLength = 1;
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbonControl1.ShowExpandCollapseButton = DevExpress.Utils.DefaultBoolean.True;
            this.ribbonControl1.ShowToolbarCustomizeItem = false;
            this.ribbonControl1.Size = new System.Drawing.Size(1120, 119);
            this.ribbonControl1.Toolbar.ShowCustomizeItem = false;
            this.ribbonControl1.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // barButtonItemPrint
            // 
            this.barButtonItemPrint.Caption = "ItemPrint";
            this.barButtonItemPrint.Enabled = false;
            this.barButtonItemPrint.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.Glyph")));
            this.barButtonItemPrint.Id = 6;
            this.barButtonItemPrint.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemPrint.Name = "barButtonItemPrint";
            // 
            // barButtonItemSave
            // 
            this.barButtonItemSave.Caption = "ItemSave";
            this.barButtonItemSave.Enabled = false;
            this.barButtonItemSave.Glyph = global::SmartCadGuiCommon.PrintRibbonControllerResources.RibbonPrintPreview_Save;
            this.barButtonItemSave.Id = 7;
            this.barButtonItemSave.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemSave.Name = "barButtonItemSave";
            // 
            // barButtonItemRefresh
            // 
            this.barButtonItemRefresh.Caption = "ItemRefresh";
            this.barButtonItemRefresh.Enabled = false;
            this.barButtonItemRefresh.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemRefresh.Glyph")));
            this.barButtonItemRefresh.Id = 8;
            this.barButtonItemRefresh.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemRefresh.Name = "barButtonItemRefresh";
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupGeneralOptions});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "ribbonPage1";
            // 
            // ribbonPageGroupGeneralOptions
            // 
            this.ribbonPageGroupGeneralOptions.AllowMinimize = false;
            this.ribbonPageGroupGeneralOptions.AllowTextClipping = false;
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemPrint);
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemSave);
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemRefresh);
            this.ribbonPageGroupGeneralOptions.Name = "ribbonPageGroupGeneralOptions";
            this.ribbonPageGroupGeneralOptions.ShowCaptionButton = false;
            this.ribbonPageGroupGeneralOptions.Text = "ribbonPageGroupGeneralOptions";
            // 
            // CameraVisualizeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1120, 641);
            this.Controls.Add(this.ribbonControl1);
            this.Controls.Add(this.layoutControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CameraVisualizeForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CameraVisualizeForm_FormClosing);
            this.Load += new System.EventHandler(this.CameraVisualizeForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupMainWindow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlDisplayArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlTemplates)).EndInit();
            this.groupControlTemplates.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTemplates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTemplates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonUndo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonUnping)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxCameras)).EndInit();
            this.groupBoxCameras.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCameras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCameras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBoxDevices)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupMainWindow;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private System.Windows.Forms.ToolTip toolTipMain;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private System.Windows.Forms.Timer timer1;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.GroupControl groupControlTemplates;
        private GridControlEx gridControlTemplates;
        private GridViewEx gridViewTemplates;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnName;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnUndo;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnCancel;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnSave;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonUndo;
        private DevExpress.XtraEditors.GroupControl groupBoxCameras;
        private GridControlEx gridControlCameras;
        private GridViewEx gridViewCameras;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnCameraName;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBoxDevices;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonSave;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonCancel;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonUnping;
        private DevExpress.XtraLayout.LayoutControl layoutControlDisplayArea;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.BarButtonItem barButtonItemPrint;
        private DevExpress.XtraBars.BarButtonItem barButtonItemSave;
        private DevExpress.XtraBars.BarButtonItem barButtonItemRefresh;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupGeneralOptions;
    }
}