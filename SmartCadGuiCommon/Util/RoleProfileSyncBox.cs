using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Threading;
using System.Collections;
using Smartmatic.SmartCad.Gui;
using SmartCadControls.SyncBoxes;
using SmartCadControls;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadControls.Filters;
using SmartCadCore.Core;

namespace SmartCadGuiCommon.Util
{
    public class GridRoleProfileData : DataGridExData
    {
        private UserProfileClientData profile;
        public static DataGridEx DataGrid = null;
        private bool check;

        private Color backColor;
        //private string recommended;

        public UserProfileClientData Profile
        {
            get
            {
                return profile;
            }
            set
            {
                profile = value;
            }
        }

        public GridRoleProfileData(UserProfileClientData profile)
            : base(profile)
        {
            this.profile = profile;
        }

        public override Color BackColor
        {
            get
            {
                return backColor;
            }
            set
            {
                backColor = value;
            }
        }

        public override string Key
        {
            get
            {
                return profile.Code.ToString();
            }
            set
            {
            }
        }

        [DataGridExColumn(HeaderText = "", Column = typeof(DataGridViewCheckBoxColumn), Width = 40)]
        public bool ProfileCheck
        {
            get
            {
                return check;
            }
            set
            {
                check = value;
            }
        }

        [DataGridExColumn(HeaderText = "Role", Width = 150)]
        public string ProfileName
        {
            get
            {
                return profile.FriendlyName;
            }
        }

        [DataGridExColumn(HeaderText = "Description", Width = 317)]
        public string ProfileDescription
        {
            get
            {
                return profile.Description;
            }
        }
    }
      
    
     
    public class RoleProfileSyncBox2 : SyncBox2
    {
        public override DataGridExData BuildGridData(object obj)
        {
            UserProfileClientData profile = obj as UserProfileClientData;

            GridRoleProfileData gridProfile = new GridRoleProfileData(profile);

            return gridProfile;
        }

        public void Sync(object objectData, CommittedDataAction action, FilterBase filter, bool updatedFromDispatch)
        {
            if (updatedFromDispatch == true)
            {
                base.Sync(objectData, action, filter);
            }
            else
            {
                Sync(objectData, action, filter);
            }
        }

        public override void Sync(object objectData, CommittedDataAction action, FilterBase filter)
        {
            UserProfileClientData profile = objectData as UserProfileClientData;

            switch (action)
            {
                case CommittedDataAction.Update:
                    {
                        DataGridExData gridData = BuildGridData(objectData);
                        bool visible = true;
                        if (filter != null)
                            visible = filter.Filter(objectData);

                        if (DataGrid.ContainsData(gridData))
                        {
                            FormUtil.InvokeRequired(DataGrid, delegate
                            {
                                DataGrid.UpdateData(gridData, visible);
                                DataGrid.ClearSelection();
                                DataGrid.SelectData(gridData);
                            });
                        }
                        else
                        {
                            gridData = null;
                            base.Sync(objectData, action, filter);
                        }
                    }
                    break;
                default:
                    base.Sync(objectData, action, filter);
                    break;
            }
        }
    }
}
