using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadGuiCommon;
using SmartCadGuiCommon.Services;
using SmartCadGuiCommon.Util;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;


namespace Smartmatic.SmartCad.Gui
{
    class SearchAddressButtonHandler : ButtonHandler
    {
        public override void OnClick(object sender, EventArgs e)
        {
			DevExpress.XtraEditors.SimpleButton senderButton = (DevExpress.XtraEditors.SimpleButton)sender;
			DevExpress.XtraLayout.LayoutControl behindPanel= senderButton.Parent as DevExpress.XtraLayout.LayoutControl;
			DefaultFrontClientFormDevX frontClientForm = behindPanel.Parent.Parent.Parent.Parent.Parent.Parent.Parent as DefaultFrontClientFormDevX;
            AddressClientData address = frontClientForm.QuestionsControl.IncidentAddress;
            
            if (ApplicationServiceClient.Current(UserApplicationClientData.Map).Ping() == false)
            {
                MessageForm.Show(ResourceLoader.GetString2("MapsNotOpenByUser"), MessageFormType.Information);
            }
            else
            {
                ApplicationServiceClient.Current(UserApplicationClientData.Map).InsertAddressDataInGis(address);
            }
        }
    }
}