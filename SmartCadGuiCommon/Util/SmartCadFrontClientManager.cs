using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Diagnostics;
using SmartCadCore.Core;
using SmartCadCore.Common;

namespace SmartCadGuiCommon.Util
{
    public class SmartCadFrontClientManager
    {
        public void StartMonitor(int id)
        {
            try
            {
                Process proc = Process.GetProcessById(id);

                proc.EnableRaisingEvents = true;
                proc.Exited += new EventHandler(proc_Exited);
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private void proc_Exited(object sender, EventArgs e)
        {
            Process.GetCurrentProcess().Kill();
        }
    }
}
