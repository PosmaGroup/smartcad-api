﻿using SmartCadCore.ClientData;
using Smartmatic.SmartCad.Vms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadGuiCommon.Util
{
    public  class VmsSingletonUtil
    {
        public  VmsControlEx vmsControl { get; set; }


        public  VmsSingletonUtil()
        {         
        }

        public  VmsControlEx GetInstance(ConfigurationClientData config)
        {
            if(vmsControl == null)
            {
                vmsControl = VmsControlEx.GetInstance(config.VmsDllName);
            }
            return vmsControl;
        }
    }
}
