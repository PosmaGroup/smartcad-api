using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

using SmartCadCore.Core;

namespace SmartCadGuiCommon.Util
{
    public class CustomToolStripColors : ProfessionalColorTable
    {
        public CustomToolStripColors()
        {
        }

        public CustomToolStripColors(Color toolStripGradientBegin, Color toolStripGradientMiddle, Color toolStripGradientEnd,
            Color menuStripGradientBegin, Color menuStripGradientEnd,
            Color statusStripGradientBegin, Color statusStripGradientEnd)
        {
            this.toolStripGradientBegin = toolStripGradientBegin;
            this.toolStripGradientMiddle = toolStripGradientMiddle;
            this.toolStripGradientEnd = toolStripGradientEnd;

            this.menuStripGradientBegin = menuStripGradientBegin;
            this.menuStripGradientEnd = menuStripGradientEnd;

            this.statusStripGradientBegin = statusStripGradientBegin;
            this.statusStripGradientEnd = statusStripGradientEnd;
        }

        private Color toolStripGradientBegin;

        public override Color ToolStripGradientBegin
        {
            get
            {
                return toolStripGradientBegin;
            }
        }

        private Color toolStripGradientMiddle;

        public override Color ToolStripGradientMiddle
        {
            get
            {
                return toolStripGradientMiddle;
            }
        }

        private Color toolStripGradientEnd;

        public override Color ToolStripGradientEnd
        {
            get
            {
                return toolStripGradientEnd;
            }
        }

        private Color menuStripGradientBegin;

        public override Color MenuStripGradientBegin
        {
            get
            {
                return menuStripGradientBegin;
            }
        }

        private Color menuStripGradientEnd;

        public override Color MenuStripGradientEnd
        {
            get
            {
                return menuStripGradientEnd;
            }
        }

        private Color statusStripGradientBegin;

        public override Color StatusStripGradientBegin
        {
            get
            {
                return statusStripGradientBegin;
            }
        }

        private Color statusStripGradientEnd;

        public override Color StatusStripGradientEnd
        {
            get
            {
                return statusStripGradientEnd;
            }
        }
    }
}
