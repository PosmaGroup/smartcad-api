using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Smartmatic.SmartCad.Controls;
using SmartCadGuiCommon.Util;
using SmartCadCore.ClientData;
using SmartCadGuiCommon;

namespace Smartmatic.SmartCad.Gui
{
    public class WhereButtonHandler : ButtonHandler
    {
        public override void OnClick(object sender, EventArgs e)
        {
			DevExpress.XtraEditors.SimpleButton senderButton = (DevExpress.XtraEditors.SimpleButton)sender;
			DevExpress.XtraLayout.LayoutControl behindPanel = senderButton.Parent as DevExpress.XtraLayout.LayoutControl;
			
            if(behindPanel.Parent.Parent.Parent.Parent.Parent.Parent.Parent is DefaultFrontClientFormDevX)
            {
                DefaultFrontClientFormDevX frontClientForm = behindPanel.Parent.Parent.Parent.Parent.Parent.Parent.Parent as DefaultFrontClientFormDevX;
                AddressClientData address = new AddressClientData();
                address.Zone = frontClientForm.CallReceiverControl.CallerAddress.Zone;
                address.Street = frontClientForm.CallReceiverControl.CallerAddress.Street;
                address.Reference = frontClientForm.CallReceiverControl.CallerAddress.Reference;
                address.More = frontClientForm.CallReceiverControl.CallerAddress.More;
                address.Lat = frontClientForm.CallReceiverControl.CallerAddress.Lat;
                address.Lon = frontClientForm.CallReceiverControl.CallerAddress.Lon;
            
                frontClientForm.QuestionsControl.SetIncidentAddress(address);
            }

            ///Modularizado sin twitter
            //else if (behindPanel.Parent.Parent.Parent.Parent.Parent.Parent.Parent is DefaultTwitterFrontClientForm)
            //{
            //    DefaultTwitterFrontClientForm frontClientForm = behindPanel.Parent.Parent.Parent.Parent.Parent.Parent.Parent as DefaultTwitterFrontClientForm;
            //    AddressClientData address = new AddressClientData();
            //    address.Zone = frontClientForm.incidentInformationControl.CallerAddress.Zone;
            //    address.Street = frontClientForm.incidentInformationControl.CallerAddress.Street;
            //    address.Reference = frontClientForm.incidentInformationControl.CallerAddress.Reference;
            //    address.More = frontClientForm.incidentInformationControl.CallerAddress.More;
            //    address.Lat = frontClientForm.incidentInformationControl.CallerAddress.Lat;
            //    address.Lon = frontClientForm.incidentInformationControl.CallerAddress.Lon;

            //    frontClientForm.QuestionsTwitterControl.SetIncidentAddress(address);
            //}

            
        }
    }
}
