using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Collections;
using Smartmatic.SmartCad.Service;
using DevExpress.Data.Filtering;
using DevExpress.XtraEditors.Controls;
using System.ServiceModel;
using System.Reflection;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using System.Linq;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadCore.Core;

namespace SmartCadGuiCommon
{
    public partial class OperatorAssignRecurrenceForm : DevExpress.XtraEditors.XtraForm
    {

        private bool isBatchAssignment = false;
        private WorkShiftVariationClientData workShiftSelected;
        private WorkShiftScheduleVariationClientData scheduleSelected;
        private int itemsCheck;
        private int operCode;
        private Dictionary<int, Dictionary<int, List<WorkShiftScheduleVariationClientData>>> schedulesAvailablesBySupervisor = new Dictionary<int, Dictionary<int, List<WorkShiftScheduleVariationClientData>>>();
        private Dictionary<int, IList> supervisorSchedules = new Dictionary<int, IList>();


        public OperatorAssignRecurrenceForm()
        {
            InitializeComponent();
            InitializeDataGrid();
            LoadLanguage();
            FillDaysOfWeek();
        }

        public OperatorAssignRecurrenceForm(List<OperatorAssignClientData> assignList, int operatorCode, WorkShiftVariationClientData workShift, WorkShiftScheduleVariationClientData schedule, bool isBatch, Dictionary<int, IList> supervisorSchedules)
        :this()
        {
            isBatchAssignment = isBatch;
            workShiftSelected = workShift;
            scheduleSelected = schedule;
            operCode = operatorCode;
            this.supervisorSchedules = supervisorSchedules;
            LoadSupervisorsSchedulesAvailables(assignList);
            SeekCoincidences(assignList, operCode);
           
        }

        private void InitializeDataGrid() 
        {
            gridControlExAssignment.Type = typeof(GridControlDataOperatorAssign);
            gridControlExAssignment.ViewTotalRows = true;
            gridControlExAssignment.EnableAutoFilter = true;
            gridViewExAssignment.OptionsBehavior.Editable = true;
            gridViewExAssignment.Columns["Checked"].OptionsColumn.ShowCaption = false;
            gridViewExAssignment.Columns["Checked"].Width = 15;
            gridViewExAssignment.Columns["Checked"].OptionsColumn.AllowEdit = true;
            gridViewExAssignment.Columns["Checked"].VisibleIndex = 0;  
            gridViewExAssignment.Columns["Checked"].OptionsColumn.AllowMove = false;
            gridViewExAssignment.Columns["FirstName"].OptionsColumn.AllowEdit = false;
            gridViewExAssignment.Columns["LastName"].OptionsColumn.AllowEdit = false;
            gridViewExAssignment.Columns["Date"].OptionsColumn.AllowEdit = false;
            gridViewExAssignment.Columns["StartTime"].OptionsColumn.AllowEdit = false;
            gridViewExAssignment.Columns["EndTime"].OptionsColumn.AllowEdit = false;
            gridViewExAssignment.OptionsView.ColumnAutoWidth = true;

        }


        private void LoadLanguage() 
        {
            radioButtonAll.Text = ResourceLoader.GetString2("All");
            radioButtonDay.Text = ResourceLoader.GetString2("Day");
            radioButtonDaysOfWeek.Text = ResourceLoader.GetString2("DayWeek");
            simpleButtonAccept.Text = ResourceLoader.GetString2("Create");
            simpleButtonCancel.Text = ResourceLoader.GetString2("Cancel");

            this.Text = ResourceLoader.GetString2("AssignmentByRecurrence");
            layoutControlGroupAssignment.Text = ResourceLoader.GetString2("Recurrences");
            
        }

        private void SeekCoincidences(List<OperatorAssignClientData> assignList, int operCode)
        {
            IList dataSourceAssignments = new ArrayList();
            OperatorAssignClientData newOperatorAssign;
            IList conflicts = new ArrayList();
            DateTime currentDate = ServerServiceClient.GetInstance().GetTimeFromDB();
            if (isBatchAssignment == false)
            {
                string hql = "SELECT assign FROM OperatorAssignData assign WHERE (";
                foreach (OperatorAssignClientData operAs in assignList)
                {
                    hql += string.Format("(convert(varchar, assign.StartDate, 114) between '{0}' and '{1}' OR " +
                                            "convert(varchar, assign.EndDate, 114) between '{0}' and '{1}' OR " +
                                            "'{0}' between convert(varchar, assign.StartDate, 114) and convert(varchar, assign.EndDate, 114) OR " +
                                            "'{1}' between convert(varchar, assign.StartDate, 114) and convert(varchar, assign.EndDate, 114)) OR", operAs.StartDate.ToString("HH:mm:ss.fff"), operAs.EndDate.ToString("HH:mm:ss.fff"));
                }

                hql = hql.Substring(0, hql.Length - 2);

                hql += ") AND assign.SupervisedOperator.Code = " + operCode;

                conflicts = ServerServiceClient.GetInstance().SearchClientObjects(hql);

            }

            foreach (WorkShiftScheduleVariationClientData schedule in workShiftSelected.Schedules)
            {
                if (schedule.Start.Date != scheduleSelected.Start.Date)
                {
                    if (schedule.Start > currentDate){
                        foreach (OperatorAssignClientData assign in assignList)
                        {
                            if (schedule.Start.TimeOfDay <= assign.StartDate.TimeOfDay && schedule.End.TimeOfDay >= assign.EndDate.TimeOfDay &&
                               //(schedule.End.TimeOfDay == assign.EndDate.TimeOfDay || (schedule.End.TimeOfDay > assign.EndDate.TimeOfDay && assign.EndDate.Hour != 0 && assign.EndDate.Minute != 0)) &&
                                schedulesAvailablesBySupervisor[assign.SupervisorCode].ContainsKey(schedule.Code) == true)
                            {
                                newOperatorAssign = new OperatorAssignClientData();
                                newOperatorAssign.SupervisorCode = assign.SupervisorCode;
                                newOperatorAssign.SupervisedOperatorCode = operCode;
                                newOperatorAssign.StartDate = schedule.Start.Date.Add(assign.StartDate.TimeOfDay);
                                newOperatorAssign.EndDate = schedule.End.Date.Add(assign.EndDate.TimeOfDay);
                                newOperatorAssign.SupervisedScheduleVariation = schedule;
                                newOperatorAssign.SupFirstName = assign.SupFirstName;
                                newOperatorAssign.SupLastName = assign.SupLastName;
                                if (ContainsAssign(conflicts, newOperatorAssign) == false)
                                    dataSourceAssignments.Add(newOperatorAssign);
                            }
                        }
                    }
                }

            }

            gridControlExAssignment.SetDataSource(dataSourceAssignments);
            
        }

        private void LoadSupervisorsSchedulesAvailables(IList assignList)
        {
            DateTime start = new DateTime();
            DateTime end = new DateTime();
            DateTime oldStart = new DateTime();
            DateTime oldEnd = new DateTime();
            Dictionary<int, List<WorkShiftScheduleVariationClientData>> item;
            List<WorkShiftScheduleVariationClientData> schedulesAvailables = new List<WorkShiftScheduleVariationClientData>();
            IList supSchedules = new ArrayList();
        
            foreach (OperatorAssignClientData operAssign in assignList)
            {
                if (supervisorSchedules.ContainsKey(operAssign.SupervisorCode) == true)
                    supSchedules = supervisorSchedules[operAssign.SupervisorCode];

                item = new Dictionary<int, List<WorkShiftScheduleVariationClientData>>();
                
                foreach (WorkShiftScheduleVariationClientData wsSchedule in workShiftSelected.Schedules)
                {
                    start = wsSchedule.Start.Date.Add(operAssign.StartDate.TimeOfDay);
                    end = wsSchedule.End.Date.Add(operAssign.EndDate.TimeOfDay);

                    foreach (WorkShiftScheduleVariationClientData supSchedule in supSchedules)
                    {
                        if (supSchedule.Start != oldEnd || (supSchedule.Start.Hour == 0 && supSchedule.Start.Minute == 0 && oldEnd.Hour == 0 && oldEnd.Minute == 0))
                        {
                            oldStart = supSchedule.Start;
                            schedulesAvailables = new List<WorkShiftScheduleVariationClientData>();
                        }

                        if (oldStart <= start && end <= supSchedule.End)
                        {
                            schedulesAvailables.Add(supSchedule);
                            item.Add(wsSchedule.Code, schedulesAvailables);
                            break;
                        }
                        else
                        {
                            oldStart = supSchedule.Start;
                            oldEnd = supSchedule.End;
                            schedulesAvailables.Add(supSchedule);
                        }
                    }

                }

                schedulesAvailablesBySupervisor.Add(operAssign.SupervisorCode, item);
                
            }
        }
        
        private bool ContainsAssign(IList list, OperatorAssignClientData assign) 
        {
            foreach (OperatorAssignClientData operAs in list)
            {
                if (operAs.SupervisedOperatorCode == assign.SupervisedOperatorCode &&
                    operAs.StartDate.Date == assign.StartDate.Date &&
                    operAs.EndDate.Date == assign.EndDate.Date)
                    return true;
                
            }
            return false;
        
        }

        
        private void FillDaysOfWeek()
        {
            this.checkedComboBoxEditDaysOfWeek.Properties.Items.AddRange(new string[] {
                                                                    ResourceLoader.GetString2("Monday"),
                                                                    ResourceLoader.GetString2("Tuesday"),
                                                                    ResourceLoader.GetString2("Wednesday"),
                                                                    ResourceLoader.GetString2("Thursday"),
                                                                    ResourceLoader.GetString2("Friday"),
                                                                    ResourceLoader.GetString2("Saturday"),
                                                                    ResourceLoader.GetString2("Sunday")
            });
        
        }

        private void radioButton_CheckedChanged(object sender, EventArgs e)
        {
          
            if (radioButtonAll.Checked == true)
            {
                checkedComboBoxEditDaysOfWeek.Enabled = false;
                gridViewExAssignment.ActiveFilterString = string.Empty;
            }
            else if (radioButtonDay.Checked == true)
            {
                checkedComboBoxEditDaysOfWeek.Enabled = false;
                gridViewExAssignment.ActiveFilterString = "[DayOfWeek] =" + (int)scheduleSelected.Start.DayOfWeek;
            }
            else if (radioButtonDaysOfWeek.Checked == true)
            {
                checkedComboBoxEditDaysOfWeek.Enabled = true;

                string filter = string.Empty;
                ArrayList checkedItems = GetCheckedItems();

                if (checkedItems.Count > 0)
                {
                    for (int i = 0; i < checkedItems.Count; i++)
                    {
                        filter += " OR [DayOfWeek] = " + checkedItems[i];
                    }

                    filter = filter.Substring(4);
                }

                gridViewExAssignment.ActiveFilterString = filter;
            }
        }

       
        private ArrayList GetCheckedItems() 
        {
            ArrayList result = new ArrayList();

            for (int i = 0; i < checkedComboBoxEditDaysOfWeek.Properties.Items.Count; i++)
            {
                CheckedListBoxItem item = checkedComboBoxEditDaysOfWeek.Properties.Items[i];
                if (item.CheckState == CheckState.Checked)
                {
                    if (i == 6)
                        result.Add(i - 6);
                    else
                        result.Add(i + 1);
                }
            }

            return result; 
        }

        private void checkedComboBoxEditDaysOfWeek_EditValueChanged(object sender, EventArgs e)
        {
            radioButton_CheckedChanged(null, null);
        }

        private void checkEditSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            gridViewExAssignment.CellValueChanging -=new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(gridViewExAssignment_CellValueChanging);
            
            gridViewExAssignment.BeginUpdate();
            foreach (GridControlDataOperatorAssign gridData in gridControlExAssignment.Items)
            {
               gridData.Checked = checkEditSelectAll.Checked; 
            }

            if (checkEditSelectAll.Checked == true)
            {
                itemsCheck = gridControlExAssignment.Items.Count;
                if (itemsCheck > 0)
                    simpleButtonAccept.Enabled = true;
                else
                    simpleButtonAccept.Enabled = false;
            }
            else
            {
                simpleButtonAccept.Enabled = false;
                itemsCheck = 0;
            }
            
            gridViewExAssignment.EndUpdate();

            gridViewExAssignment.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(gridViewExAssignment_CellValueChanging);
            
        }

        private void gridViewExAssignment_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            checkEditSelectAll.CheckedChanged -=new EventHandler(checkEditSelectAll_CheckedChanged);

            if ((bool)e.Value == true)
                itemsCheck++;
            else
                itemsCheck--;

            if (itemsCheck > 0)
                simpleButtonAccept.Enabled = true;
            else
                simpleButtonAccept.Enabled = false;


            if (itemsCheck > 0 && itemsCheck == gridControlExAssignment.Items.Count)
                checkEditSelectAll.CheckState = CheckState.Checked;
            else if (itemsCheck == 0)
                checkEditSelectAll.CheckState = CheckState.Unchecked;
            else
                checkEditSelectAll.CheckState = CheckState.Indeterminate;


            checkEditSelectAll.CheckedChanged += new EventHandler(checkEditSelectAll_CheckedChanged);

        }

        private void simpleButtonAccept_Click(object sender, EventArgs e)
        {
            try
            {
                BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[1] { GetType().GetMethod("simpleButtonAccept_Click_1", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[0] });
                processForm.CanThrowError = true;
                processForm.ShowDialog();
            }
            catch (FaultException ex)
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, MessageFormType.Error);
            }
            catch (Exception ex)
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, ex);

            }

        }


        private void simpleButtonAccept_Click_1()
        {
            IList assignsToSave = new ArrayList();
            IList assignsConflicts = new ArrayList();
            IList assignsToDelete = new ArrayList();
            IList conflictsWithVacation = new ArrayList();
            DialogResult dialogResult;
            bool runSave = true;

            OperatorAssignClientData operAssign = new OperatorAssignClientData();
            List<WorkShiftScheduleVariationClientData> supervisorSchedules = new List<WorkShiftScheduleVariationClientData>();
            OperatorAssignClientData tag = new OperatorAssignClientData();

            List<GridControlDataOperatorAssign> assigns = GetCheckedVisibleRows();

            if (isBatchAssignment == true)
            {
                foreach (OperatorClientData oper in workShiftSelected.Operators)
                {
                    if (oper.IsSupervisor == false)
                    {
                        foreach (GridControlDataOperatorAssign gridData in assigns)
                        {
                            tag = gridData.Tag as OperatorAssignClientData;
                            if (schedulesAvailablesBySupervisor.ContainsKey(tag.SupervisorCode) == true &&
                                schedulesAvailablesBySupervisor[tag.SupervisorCode].ContainsKey(tag.SupervisedScheduleVariation.Code))
                            {
                                supervisorSchedules = schedulesAvailablesBySupervisor[tag.SupervisorCode][tag.SupervisedScheduleVariation.Code];
                            }
                            else
                            {
                                supervisorSchedules = new List<WorkShiftScheduleVariationClientData>();
                            }
                            foreach (WorkShiftScheduleVariationClientData supSchedule in supervisorSchedules)
                            {
                                operAssign = tag.Clone();
                                operAssign.SupervisedOperatorCode = oper.Code;
                                operAssign.SupervisorScheduleVariation = supSchedule;

                                if (supSchedule.Start >= operAssign.StartDate)
                                    operAssign.StartDate = supSchedule.Start;
                                if (supSchedule.End <= operAssign.EndDate)
                                    operAssign.EndDate = supSchedule.End;

                                assignsToSave.Add(operAssign);

                            }
                        }
                    }
                }

                assignsConflicts = SearchAssignsConflicts(workShiftSelected.Operators, assigns);
            }
            else 
            {
                foreach (GridControlDataOperatorAssign gridData in assigns)
                {
                    tag = gridData.Tag as OperatorAssignClientData;
                    if (schedulesAvailablesBySupervisor.ContainsKey(tag.SupervisorCode) == true &&
                        schedulesAvailablesBySupervisor[tag.SupervisorCode].ContainsKey(tag.SupervisedScheduleVariation.Code))
                    {
                        supervisorSchedules = schedulesAvailablesBySupervisor[tag.SupervisorCode][tag.SupervisedScheduleVariation.Code];
                    }
                    else
                    {
                        supervisorSchedules = new List<WorkShiftScheduleVariationClientData>();
                    }
                    foreach (WorkShiftScheduleVariationClientData supSchedule in supervisorSchedules)
                    {
                        operAssign = tag.Clone();
                        operAssign.SupervisedOperatorCode = operCode;
                        operAssign.SupervisorScheduleVariation = supSchedule;

                        if (supSchedule.Start >= operAssign.StartDate)
                            operAssign.StartDate = supSchedule.Start;
                        if (supSchedule.End <= operAssign.EndDate)
                            operAssign.EndDate = supSchedule.End;

                        assignsToSave.Add(operAssign);

                    }
                }                      
            }

            if (assignsConflicts.Count > 0)
            {
                if (isBatchAssignment == false)
                    //dialogResult = MessageForm.Show("El operador ya tienen asignaciones para los intervalos seleccionados, si continua  se sobrescribiran estas asignaciones. Desea continuar?", MessageFormType.WarningQuestion);
                    dialogResult = MessageForm.Show(ResourceLoader.GetString2("MessageBoxErrorWorkShiftAssignment"), MessageFormType.WarningQuestion);
                else
                    //dialogResult = MessageForm.Show("Algunos operadores del turno ya tienen asignaciones para los intervalos seleccionados, si continua  se sobrescribiran estas asignaciones. Desea continuar?", MessageFormType.WarningQuestion);
                    dialogResult = MessageForm.Show(ResourceLoader.GetString2("MessageBoxErrorWorkShiftAssignmentSome"), MessageFormType.WarningQuestion);

                if (dialogResult == DialogResult.Yes)
                {
                    runSave = true;
                    object[] list = ResolveAssignsConflicts(assignsToSave, assignsConflicts);

                    assignsToSave = (IList)list[0];
                    assignsToDelete = (IList)list[1];
                }
                else
                {
                    runSave = false;
                    this.DialogResult = DialogResult.None;
                }
            }

            if (runSave == true)
            {

                if (assignsToSave.Count > 0)
                {
                    conflictsWithVacation = SearchAssignsConflictsWithVacation(assignsToSave);

                    if (conflictsWithVacation.Count > 0)
                    {
                        if (isBatchAssignment == false)
                            dialogResult = MessageForm.Show("El supervisor y/o operador que esta intentando asignar tiene un permiso/vacacion para el intervalo indicado", MessageFormType.Warning);
                        else
                            dialogResult = MessageForm.Show("Algunos operadores del turno tienen un permiso/vacacion para el intervalo seleccionado, si continua las asignaciones de estos operadores no se crearan. Desea continuar?", MessageFormType.WarningQuestion);
                       
                        if (dialogResult == DialogResult.Yes)
                        {
                            runSave = true;
                            assignsToSave = RemoveConflictsWithVacations(assignsToSave, conflictsWithVacation);
                        }
                        else
                            runSave = false;
                    }

                    if (runSave == true)
                    {
                        if (assignsToDelete.Count > 0)
                            ServerServiceClient.GetInstance().DeleteClientObjectCollection(assignsToDelete);

                        ServerServiceClient.GetInstance().SaveOrUpdateClientData(assignsToSave);
                    }
                }
            }
        }

        private IList RemoveConflictsWithVacations(IList assignsToSave, IList conflicts)
        {
            if (conflicts != null && conflicts.Count > 0)
            {
                IList assignsToRemove = new ArrayList();

                for (int i = 0; i < assignsToSave.Count; i++)
                {
                    OperatorAssignClientData assign = (OperatorAssignClientData)assignsToSave[i];
                    foreach (object[] item in conflicts)
                    {
                        if ((assign.SupervisedOperatorCode == (int)item[3] || assign.SupervisorCode == (int)item[3]) &&
                                (assign.StartDate > (DateTime)item[1] && assign.StartDate < (DateTime)item[2] ||
                                  assign.EndDate > (DateTime)item[1] && assign.EndDate < (DateTime)item[2] ||
                                  assign.StartDate <= (DateTime)item[1] && assign.EndDate >= (DateTime)item[2]))
                        {
                            assignsToRemove.Add(i);
                            break;
                        }
                    }
                }

                for (int i = assignsToRemove.Count - 1; i >= 0; i--)
                    assignsToSave.RemoveAt((int)assignsToRemove[i]);

            }

            return assignsToSave;
        }



        private IList SearchAssignsConflictsWithVacation(IList assignList)
        {
            string hql = string.Format(@"SELECT schedules.Code, schedules.Start, schedules.End, operators.Code FROM WorkShiftScheduleVariationData schedules
                          left join schedules.WorkShiftVariation.Operators operators 
                         WHERE schedules.WorkShiftVariation.Type = {0} AND schedules.WorkShiftVariation.ObjectType = 0 AND (", (int)WorkShiftVariationClientData.WorkShiftType.TimeOff);

            foreach (OperatorAssignClientData assign in assignList)
            {
                hql += string.Format("((schedules.Start between '{0}' and '{1}' OR " +
                                        "schedules.End between '{0}' and '{1}' OR " +
                                        "'{0}' between schedules.Start and schedules.End OR " +
                                        "'{1}' between schedules.Start and schedules.End) AND operators.Operator.Code IN ({2},{3})) OR", ApplicationUtil.GetDataBaseFormattedDate(assign.StartDate), ApplicationUtil.GetDataBaseFormattedDate(assign.EndDate), assign.SupervisedOperatorCode, assign.SupervisorCode);
            }

            hql = hql.Substring(0, hql.Length - 2) + ") order by schedules.Start";

            IList conflicts = (IList)ServerServiceClient.GetInstance().SearchBasicObjects(hql);

            return conflicts;

        }

        private IList SearchAssignsConflicts(IList operList, IList assignList)
        {

            string hql = "SELECT oad FROM OperatorAssignData oad WHERE (";
            OperatorAssignClientData operA = new OperatorAssignClientData();
            foreach (GridControlDataOperatorAssign oad in assignList)
            {
                operA = oad.Tag as OperatorAssignClientData;
                hql += string.Format("(oad.StartDate between '{0}' and '{1}' OR " +
                                        "oad.EndDate between '{0}' and '{1}' OR " +
                                        "'{0}' between oad.StartDate and oad.EndDate OR " +
                                        "'{1}' between oad.StartDate and oad.EndDate) OR", ApplicationUtil.GetDataBaseFormattedDate(operA.StartDate), ApplicationUtil.GetDataBaseFormattedDate(operA.EndDate));
            }

            if (assignList.Count > 0)
                hql = hql.Substring(0, hql.Length - 2);

            if (assignList.Count > 0)
                hql += ") AND oad.SupervisedOperator.Code IN (";
            else
                hql += "oad.SupervisedOperator.Code IN (";

            foreach (OperatorClientData oper in operList)
                hql += string.Format("{0},", oper.Code);

            hql = hql.Substring(0, hql.Length - 1);

            hql += ")";

            if (assignList.Count <= 0)
                hql += ")";


            IList conflicts = ServerServiceClient.GetInstance().SearchClientObjects(hql);

            return conflicts;

        }

        private List<GridControlDataOperatorAssign> GetCheckedVisibleRows() 
        {
             List<GridControlDataOperatorAssign> result = new List<GridControlDataOperatorAssign>();
           
             if (radioButtonAll.Checked == true)
                 result = ((BindingList<GridControlDataOperatorAssign>)gridViewExAssignment.DataSource).Where(operAssign => operAssign.Checked == true).ToList();
             else if (radioButtonDay.Checked == true)
                 result = ((BindingList<GridControlDataOperatorAssign>)gridViewExAssignment.DataSource).Where(operAssign => operAssign.DayOfWeek == (int)scheduleSelected.Start.DayOfWeek && operAssign.Checked == true).ToList();
             else
             {
                 ArrayList checkedItems = GetCheckedItems();
                 result = ((BindingList<GridControlDataOperatorAssign>)gridViewExAssignment.DataSource).Where(operAssign => checkedItems.Contains(operAssign.DayOfWeek) == true && operAssign.Checked == true).ToList();

             } 

             return result;
        }


        private object[] ResolveAssignsConflicts(IList newAssings, IList conflicts)
        {
            object[] result = new object[2];
            IList assignsToSave = new ArrayList();
            IList assignsToDelete = new ArrayList();
            OperatorAssignClientData operAssign;


            foreach (OperatorAssignClientData newAssign in newAssings)
            {
                foreach (OperatorAssignClientData oldAssign in conflicts)
                {

                    if (oldAssign.SupervisedOperatorCode == newAssign.SupervisedOperatorCode &&
                                oldAssign.SupervisedScheduleVariation.Code == newAssign.SupervisedScheduleVariation.Code)
                    {

                        if (newAssign.StartDate <= oldAssign.StartDate && newAssign.EndDate < oldAssign.EndDate)
                        {
                            oldAssign.StartDate = newAssign.EndDate;
                           assignsToSave.Add(oldAssign);
                        }
                        else if (newAssign.StartDate > oldAssign.StartDate && newAssign.EndDate >= oldAssign.EndDate)
                        {
                            oldAssign.EndDate = newAssign.StartDate;
                            assignsToSave.Add(oldAssign);
                        }
                        else if (newAssign.StartDate > oldAssign.StartDate && newAssign.EndDate < oldAssign.EndDate)
                        {
                            //Creo una nueva asignacion similar a la vieja, pero modificandole la fecha fin.
                            operAssign = new OperatorAssignClientData();
                            operAssign.SupervisedOperatorCode = oldAssign.SupervisedOperatorCode;
                            operAssign.SupervisedScheduleVariation = oldAssign.SupervisedScheduleVariation;
                            operAssign.StartDate = oldAssign.StartDate;
                            operAssign.EndDate = newAssign.StartDate;
                            operAssign.SupervisorCode = oldAssign.SupervisorCode;

                            //Modifico la fecha fin de la asignacion vieja
                            oldAssign.StartDate = newAssign.EndDate;

                            assignsToSave.Add(operAssign);
                            assignsToSave.Add(oldAssign);

                        }
                        else if (newAssign.StartDate <= oldAssign.StartDate && newAssign.EndDate >= oldAssign.EndDate)
                            assignsToDelete.Add(oldAssign);


                       
                    }
                }
                //Esto se debe hacer para todos los casos
                assignsToSave.Add(newAssign);
            }

            result[0] = assignsToSave;
            result[1] = assignsToDelete;


            return result;
        }


    }
}