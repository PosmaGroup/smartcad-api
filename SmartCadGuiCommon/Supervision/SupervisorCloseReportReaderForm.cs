using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Smartmatic.SmartCad.Service;
using DevExpress.XtraEditors;
using System.Drawing.Imaging;
using System.IO;
using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadCore.ClientData;

using SmartCadControls;
using SmartCadGuiCommon.Controls;


namespace SmartCadGuiCommon
{
    public partial class SupervisorCloseReportReaderForm : XtraForm
    {
        List<SupervisorCloseReportReaderGridData> gridDataElements;
        SupervisorType selectedSupervisorType = SupervisorType.General;
        string supervisedApplicationName = string.Empty;
        string selectedDepartamentName = string.Empty;
        DateTime selectedReportEndSession = new DateTime();
        int selectedReportCode = -1;
        private object syncCommited = new object();

        public SupervisorCloseReportReaderForm(SupervisorType supervisorType, string supervisedApplication, string selectedDepartament)
        {
            InitializeComponent();
            LoadLanguage();
            InitializeDataGrid();
            selectedSupervisorType = supervisorType;
            supervisedApplicationName = supervisedApplication;
            selectedDepartamentName = selectedDepartament;
            LoadData();
            this.Activated += new EventHandler(SupervisorCloseReportReaderForm_Activated);
            this.FormClosing += new FormClosingEventHandler(SupervisorCloseReportReaderForm_FormClosing);
        }

        void SupervisorCloseReportReaderForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.MdiParent != null)
            {
                (this.MdiParent as SupervisionForm).SupervisionCommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(SupervisorCloseReportReaderForm_SupervisionCommittedChanges);
            }
        }

        void SupervisorCloseReportReaderForm_Activated(object sender, EventArgs e)
        {
            if (this.MdiParent != null)
                (this.MdiParent as SupervisionForm).RibbonControl.SelectedPage = (this.MdiParent as SupervisionForm).RibbonPageOperation;
    
        }

        private void InitializeDataGrid()
        {
            gridControlCloseReports.Type = typeof(SupervisorCloseReportReaderGridData);
            gridControlCloseReports.EnableAutoFilter = true;
            gridControlCloseReports.ViewTotalRows = true;
        }
        public SupervisorType SelectedSupervisorType
        {
            get
            {
                return selectedSupervisorType;
            }
        }

        public string SupervisedApplicationName
        {
            get
            {
                return supervisedApplicationName;
            }
        }

        public string SelectedDepartamentName
        {
            get
            {
                return selectedDepartamentName;
            }
        }

        private void LoadLanguage()
        {
            this.Text = ResourceLoader.GetString2("SupervisorCloseReportReaderForm");
          //  this.gridColumnSupervisorType.Caption = ResourceLoader.GetString2("SupervisorType");
          //  this.gridColumnFullName.Caption = ResourceLoader.GetString2("FullName");
          //  this.gridColumnStart.Caption = ResourceLoader.GetString2("StartShift");
          //  this.gridColumnEnd.Caption = ResourceLoader.GetString2("EndShift");
            this.layoutControlGroupCloseMessages.Text = ResourceLoader.GetString2("ClosedSystemMessages");
            this.layoutControlGroupCloseReports.Text = ResourceLoader.GetString2("Reports");
            this.layoutControlGroupDashBoard.Text = ResourceLoader.GetString2("Indicators");
            this.indicatorDashBoardControl1.TitleFirstGroup = ResourceLoader.GetString2("Calls");
            this.indicatorDashBoardControl1.TitleSecondGroup = ResourceLoader.GetString2("FirstLevel");
            this.indicatorDashBoardControl1.TitleThirdGroup = ResourceLoader.GetString2("Dispatch");
            this.RibbonPageOperation.Text = ResourceLoader.GetString2("Operation");
            this.ribbonPageGroupFinalReport.Text = ResourceLoader.GetString2("CloseReport");
            this.ribbonPageGroupOptions.Text = ResourceLoader.GetString2("GeneralOptions");
            this.barButtonItemPrint.Caption = ResourceLoader.GetString2("Print");
            this.barButtonItemSave.Caption = ResourceLoader.GetString2("Save");
            this.barButtonItemSend.Caption = ResourceLoader.GetString2("Send");
            this.barButtonItemUpdate.Caption = ResourceLoader.GetString2("Update");
            this.barButtonItemCut.Caption = ResourceLoader.GetString2("Cut");
            this.barButtonItemCopy.Caption = ResourceLoader.GetString2("Copy");
            this.barButtonItemPaste.Caption = ResourceLoader.GetString2("Paste");
            this.barButtonItemCopy.SuperTip = SupervisionForm.BuildToolTip("ToolTip_Copy");

        }

        private void barButtonItemSend_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string fileName = ResourceLoader.GetString2("SupervisionCloseReport") + " " + ((SupervisorCloseReportReaderGridData)gridView1.GetFocusedRow()).FullName + " " + selectedReportEndSession.ToString("ddMMyyyy");
            string fileDirectory = Application.UserAppDataPath;
            if (!Directory.Exists(fileDirectory))
            {
                Directory.CreateDirectory(fileDirectory);
            }
            string filePath = fileDirectory + Path.DirectorySeparatorChar  + fileName + ".pdf";
            if (File.Exists(filePath)) 
            {
                File.Delete(filePath);
            }
            ReportSupervisionClose report = BuildReport();
            report.ExportToPdf(filePath);

            MAPI mapi = new MAPI();
            mapi.AddAttachment(filePath);
            mapi.SendMailPopup("Closing Report", "");
        }

        private void barButtonItemPrint_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ReportSupervisionClose report = BuildReport();
            

            DialogResult res = report.PrintDialog();
        }


        private void barButtonItemSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                ReportSupervisionClose report = BuildReport();
                saveFileDialog.AddExtension = false;
                saveFileDialog.FilterIndex = 0;
                saveFileDialog.Filter = "PDF|*.pdf|HTML|*.html|JPEG|*.jpg";
                saveFileDialog.SupportMultiDottedExtensions = false;
                saveFileDialog.Title = ResourceLoader.GetString2("SaveAsFile");
                saveFileDialog.FileName = ResourceLoader.GetString2("SupervisionCloseReport") + " " + ((SupervisorCloseReportReaderGridData)gridView1.GetFocusedRow()).FullName + " " + selectedReportEndSession.ToString("ddMMyyyy");
                DialogResult res = saveFileDialog.ShowDialog();
                if (res == DialogResult.OK || res == DialogResult.Yes)
                {
                    if (saveFileDialog.FilterIndex == 1)
                    {
                        report.ExportToPdf(saveFileDialog.FileName);
                    }
                    else if (saveFileDialog.FilterIndex == 2)
                    {
                        report.ExportToHtml(saveFileDialog.FileName);
                    }
                    else
                    {
                        report.ExportToImage(saveFileDialog.FileName, ImageFormat.Jpeg);
                    }
                }
            }
            catch { }
        }

        private ReportSupervisionClose BuildReport()
        {
            ReportSupervisionClose report = new ReportSupervisionClose(((SupervisorCloseReportReaderGridData)gridView1.GetFocusedRow()).Data, BuildGridImage());
            return report;
        }

        private Image BuildGridImage()
        {
            
            Bitmap bmpScreenshot = new Bitmap(indicatorDashBoardControl1.Width, indicatorDashBoardControl1.Height, PixelFormat.Format32bppArgb);
            Graphics gfxScreenshot = Graphics.FromImage(bmpScreenshot);
            int tittlebarheigh = (ParentForm as SupervisionForm).Height - (ParentForm as SupervisionForm).ClientSize.Height;
            int X = ParentForm.Location.X + indicatorDashBoardControl1.Location.X + 7;
            int Y = ParentForm.Location.Y + (ParentForm as SupervisionForm).RibbonControl.Size.Height + indicatorDashBoardControl1.Location.Y + tittlebarheigh + 20;
            gfxScreenshot.CopyFromScreen(X, Y, 0, 0, new Size(indicatorDashBoardControl1.Size.Width + 10,indicatorDashBoardControl1.Size.Height + 100), CopyPixelOperation.SourceCopy);
              
            return bmpScreenshot;
        }

        private void PrintPreviewBarItemCopy_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            richTextBoxCloseMessages.Copy();
        }

        private void SupervisorCloseReportReaderForm_Load(object sender, System.EventArgs e)
        {
            if (this.MdiParent != null)
            {
              (this.MdiParent as SupervisionForm).SupervisionCommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(SupervisorCloseReportReaderForm_SupervisionCommittedChanges); 
            }
        }
        
        private void richTextBoxCloseMessages_SelectionChanged(object sender, EventArgs e)
        {
            if (richTextBoxCloseMessages.SelectionLength > 0)
                barButtonItemCopy.Enabled = true;
            else
                barButtonItemCopy.Enabled = false;
        }

        void SupervisorCloseReportReaderForm_SupervisionCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    lock (syncCommited)
                    {
                        try
                        {
                            if (ServerServiceClient.GetInstance().OperatorClient == null)
                            { }
                        }
                        catch
                        { }

                        if (e.Objects[0] is SupervisorCloseReportClientData)
                        {
                            if (e.Action == CommittedDataAction.Update)
                            {
                                SupervisorCloseReportClientData report = e.Objects[0] as SupervisorCloseReportClientData;

                                SupervisorCloseReportReaderGridData reportGridData = new SupervisorCloseReportReaderGridData(report);

                                int index = gridDataElements.IndexOf(reportGridData);

                                if (index > -1)
                                    gridDataElements[index] = reportGridData;
                                else
                                {
                                    if ((((int)selectedSupervisorType == report.SupervisorType) 
                                        || (supervisedApplicationName == "FirstLevel" && report.SupervisorType == 1) 
                                        || (supervisedApplicationName == "Dispatch" && report.SupervisorType == 2)) 
                                        && report.Finished == true)
                                        gridDataElements.Add(reportGridData);
                                }


                                FormUtil.InvokeRequired(this, delegate
                                {
                                    gridControlCloseReports.DataSource = gridDataElements;
                                    gridView1.RefreshData();
                                                                       
                                });
                            }
                        }
                        else if (e.Objects[0] is SessionHistoryClientData)
                        {
                            SessionHistoryClientData session = e.Objects[0] as SessionHistoryClientData;
                            if (session.EndDateLogin != null)
                            {
                                foreach (SupervisorCloseReportReaderGridData grid in gridDataElements)
                                {
                                    if (grid.Data.SessionCode == session.Code)
                                    {
                                        grid.Data.End = session.EndDateLogin.Value;
                                        FormUtil.InvokeRequired(this, delegate
                                        {
                                            gridControlCloseReports.DataSource = gridDataElements;
                                            gridView1.RefreshData();
                                        });
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }

        }

        private void LoadData()
        {
            gridView1.Columns["SupervisorType"].Visible = selectedSupervisorType == SupervisorType.General;

            if (gridView1.Columns["SupervisorType"].Visible == false)
            {
                gridView1.OptionsView.ColumnAutoWidth = true;
                gridView1.Columns["SupervisorType"].GroupIndex = -1;
                gridView1.Columns["FullName"].GroupIndex = 0;
            }
            else {
                gridView1.Columns["SupervisorType"].GroupIndex = 0;
                gridView1.Columns["FullName"].GroupIndex = 1;
            }

            IList result = ServerServiceClient.GetInstance().SearchClientObjects(
                          SmartCadHqls.GetCustomHql(SmartCadHqls.GetApplicationPreferenceByName, "HoursCloseReport"));
            if (result.Count > 0)
            {
                ApplicationPreferenceClientData hoursToSustract = (ApplicationPreferenceClientData)result[0];

                TimeSpan hour = new TimeSpan(int.Parse(hoursToSustract.Value), 0, 0);

                string date = ApplicationUtil.GetDataBaseFormattedDate(ServerServiceClient.GetInstance().GetTime().Date.Subtract(hour));

                IList list = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetCloseReportsByDate, date));
                gridDataElements = new List<SupervisorCloseReportReaderGridData>();
                foreach (SupervisorCloseReportClientData data in list)
                {
                    if (selectedSupervisorType == 0 && ((SupervisorType)data.SupervisorType) == 0) //Supervisor General
                    {
                        gridDataElements.Add(new SupervisorCloseReportReaderGridData(data));
                    }
                    else if (supervisedApplicationName == ((SupervisorType)data.SupervisorType).ToString())
                    {
                        if (data.DepartamentTypes.Count == 0 || data.DepartamentTypes.Contains(selectedDepartamentName))
                            gridDataElements.Add(new SupervisorCloseReportReaderGridData(data));
                    }
                }
                if (gridDataElements.Count > 0)
                {
                    gridControlCloseReports.BeginInit();
                    gridControlCloseReports.DataSource = gridDataElements;
                    gridControlCloseReports.EndInit();
                }
            }
        }

        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            richTextBoxCloseMessages.Clear();
            if (e.FocusedRowHandle > -1)
            {
                SupervisorCloseReportReaderGridData gridData = (SupervisorCloseReportReaderGridData)gridView1.GetFocusedRow();
                selectedReportEndSession = gridData.Data.End;
                selectedReportCode = gridData.Data.Code;

                barButtonItemPrint.Enabled = true;
                barButtonItemSave.Enabled = true;
                barButtonItemSend.Enabled = true;

                if (selectedReportEndSession == null || selectedReportEndSession == DateTime.MinValue)
                {
                    selectedReportEndSession = ServerServiceClient.GetInstance().GetTime();               

                }
                
                System.Threading.Thread thread = new System.Threading.Thread(new System.Threading.ThreadStart(LoadIndicators));
                thread.Start();

                int index = gridDataElements.IndexOf(gridData);

                foreach (SupervisorCloseReportMessageClientData message in gridDataElements[index].Data.Messages)
                {
                    PrintMessage(message.Time, message.Message);
                }
            }
            else {
                barButtonItemPrint.Enabled = false;
                barButtonItemSave.Enabled = false;
                barButtonItemSend.Enabled = false;

                indicatorDashBoardControl1.ClearData();
            }
        }

        private void LoadIndicators()
        {
            FormUtil.InvokeRequired(this,
                    delegate
                    {
                        indicatorDashBoardControl1.IsGeneralSupervisor = ((SupervisionForm)this.MdiParent).IsGeneralSupervisor;

                        bool isFirstLevel = UserApplicationClientData.FirstLevel.Name == ((SupervisionForm)this.MdiParent).SupervisedApplicationName;
                        indicatorDashBoardControl1.IsFirstLevel = isFirstLevel;

                        indicatorDashBoardControl1.DashBoardType = IndicatorDashBoardControl.DashBoardClassType.SYSTEM;

                        IList indicatorClassDashboards = ServerServiceClient.GetInstance().SearchClientObjects(
                        SmartCadHqls.GetCustomHql("SELECT indicators FROM SupervisorCloseReportData data left join data.Indicators indicators where data.Code = {0}", selectedReportCode));

                        if (indicatorClassDashboards.Count > 0)
                        {
                            indicatorDashBoardControl1.SetPreferences(indicatorClassDashboards, true);

                            IList results = ServerServiceClient.GetInstance().SearchClientObjects(
                                SmartCadHqls.GetCustomHql(
                                @"SELECT data 
                              FROM IndicatorResultData data left join fetch
                                   data.Result result,
                                   SupervisorCloseReportData report left join
                                   report.Indicators indicators
                              WHERE report.Code = {1} and
                                    data.Indicator.Code = indicators.Indicator.Code and
                                    result.IndicatorClass.Code = indicators.IndicatorClass.Code and
                                    data.Time = (select max(res.Time) 
                                                 from IndicatorResultData res 
                                                 where res.Indicator.Code = data.Indicator.Code and
                                                       res.Time < '{0}') ORDER BY data.Indicator.Code",
                                ApplicationUtil.GetDataBaseFormattedDate(selectedReportEndSession),
                                selectedReportCode));

                            foreach (IndicatorResultClientData result in results)
                            {
                                if (result.IndicatorTypeName.Equals(IndicatorTypeClientData.Calls.Name))
                                    indicatorDashBoardControl1.AddOrUpdateItem(result, IndicatorDashBoardControl.IndicatorType.CALLS, true);
                                else if (result.IndicatorTypeName.Equals(IndicatorTypeClientData.FirstLevel.Name))
                                    indicatorDashBoardControl1.AddOrUpdateItem(result, IndicatorDashBoardControl.IndicatorType.FIRSTLEVEL, true);
                                else if (result.IndicatorTypeName.Equals(IndicatorTypeClientData.Dispatch.Name))
                                    indicatorDashBoardControl1.AddOrUpdateItem(result, IndicatorDashBoardControl.IndicatorType.DISPATCH, true);
                            }
                        }
                        else
                        {
                            indicatorDashBoardControl1.SetPreferences(new ArrayList());
                        }
                    });
        }

        private void PrintMessage(DateTime date, string message)
        {
            int initIndex = richTextBoxCloseMessages.Text.Length;
            richTextBoxCloseMessages.AppendText(date.ToString("MM/dd/yyyy HH:mm") + ": ");
            richTextBoxCloseMessages.Select(initIndex, richTextBoxCloseMessages.Text.Length);
            richTextBoxCloseMessages.SelectionFont = new Font(richTextBoxCloseMessages.Font.FontFamily, richTextBoxCloseMessages.Font.Size, FontStyle.Bold);
            richTextBoxCloseMessages.AppendText(Environment.NewLine);           
            richTextBoxCloseMessages.AppendText(message.Trim());
            richTextBoxCloseMessages.AppendText(Environment.NewLine);
            richTextBoxCloseMessages.AppendText(Environment.NewLine);
        }
    }

    class SupervisorCloseReportReaderGridData
    {
        SupervisorCloseReportClientData data;

        public SupervisorCloseReportReaderGridData(SupervisorCloseReportClientData data)
        {
            this.data = data;
        }

        public int Code
        {
            get { return data.Code; }
        }

        [GridControlRowInfoData(Visible = true, Width = 190, ColumnOrder = DevExpress.Data.ColumnSortOrder.Ascending)]
        public string SupervisorType
        {
            get { return data.Supervisor; }
        }

        [GridControlRowInfoData(Visible = true, Width = 190)]
        public string FullName
        {
            get { return data.SessionFullName; }
        }


        [GridControlRowInfoData(Visible = true, Width = 130)]
        public string StartSession
        {
            get { return data.Start.ToString("MM/dd/yyyy HH:mm"); }
        }

        [GridControlRowInfoData(Visible = true, Width = 130)]
        public string EndSession
        {
            get
            {
                if (data.End.Equals(DateTime.MinValue) == true)
                    return string.Empty;
                else
                    return data.End.ToString("MM/dd/yyyy HH:mm");

            }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            SupervisorCloseReportReaderGridData var = obj as SupervisorCloseReportReaderGridData;
            if (var != null)
            {
                if (this.Code == var.Code)
                    result = true;
            }
            return result;
        }

        public SupervisorCloseReportClientData Data
        {
            get { return data; }
        }
    }
}