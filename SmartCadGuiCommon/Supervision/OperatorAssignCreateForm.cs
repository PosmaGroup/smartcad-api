using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraScheduler;
using System.Collections;
using Smartmatic.SmartCad.Service;
using DevExpress.XtraEditors.Repository;
using System.ServiceModel;
using System.Reflection;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadControls.Controls;
using SmartCadControls;

namespace SmartCadGuiCommon
{
    public partial class OperatorAssignCreateForm : DevExpress.XtraEditors.XtraForm
    {
        private Appointment selectedAppointment;
        private bool isBatchAssignment = false;
        private IList dataSourceSupervisors;
        private OperatorClientData appOperator = new OperatorClientData();
        private WorkShiftScheduleVariationClientData appSchedule = new WorkShiftScheduleVariationClientData();
        private WorkShiftVariationClientData appWorkShift = new WorkShiftVariationClientData();
        private DateTime maxValue = new DateTime();
        private DateTime minValue = new DateTime();
        private CreationMode mode;
        private List<OperatorAssignClientData> assignsToCheckForRecurrence = new List<OperatorAssignClientData>();
        private Dictionary<int, Dictionary<int, List<WorkShiftScheduleVariationClientData>>> schedulesAvailablesBySupervisor = new Dictionary<int, Dictionary<int, List<WorkShiftScheduleVariationClientData>>>();
        private Dictionary<int, IList> supervisorSchedules;
        private bool runSave = true;
        DateTime currentDate = new DateTime();
        private OperatorAssignForm parentForm;
        private bool frmShowRecurrence = false;

        private enum CreationMode 
        {
            BY_RANGE = 0,
            COMPLETE = 1            
        }

        public OperatorAssignCreateForm()
        {
            InitializeComponent();
            InitializeDataGrids();
            LoadLanguage();
        

        }


        public OperatorAssignCreateForm(Appointment app, IList supervisors, WorkShiftVariationClientData wsv, bool isBatch, OperatorAssignForm parentForm)
        :this()
        {
            this.parentForm = parentForm;
            isBatchAssignment = isBatch;
            appWorkShift = wsv;
            SelectedAppointment = app;
            dataSourceSupervisors = supervisors;
            LoadSupervisorSchedules();
         //   FillSupervisors(app.Start, app.End);
            radioButtonByRange.Checked = true;
            
          
        }

        public Appointment SelectedAppointment 
        {
            get {
                return selectedAppointment;
            }
            set {
                selectedAppointment = value;
               
                
                if (selectedAppointment != null)
                {
                    appOperator.Code = (int)selectedAppointment.CustomFields["OperatorCode"];
                    appOperator = (OperatorClientData)ServerServiceClient.GetInstance().RefreshClient(appOperator);
                    appSchedule.Code = (int)selectedAppointment.CustomFields["ScheduleCode"];
                    appSchedule = (WorkShiftScheduleVariationClientData)ServerServiceClient.GetInstance().RefreshClient(appSchedule);
                    
                    maxValue = selectedAppointment.End;
                    minValue = selectedAppointment.Start;

                    if (isBatchAssignment == true)
                    {
                        textEditName.Text = appWorkShift.Name;
                        layoutControlItemWorkShift.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                    }
                    else
                        textEditName.Text = appOperator.FirstName + " " + appOperator.LastName;

                    textEditWorkShift.Text = appWorkShift.Name;

                    labelControlDate.Text = String.Format("{0:dddd, MMMM d, yyyy}", selectedAppointment.Start) + ". " + ResourceLoader.GetString2("From") + " " +
                                        selectedAppointment.Start.ToString("HH:mm") + " " + ResourceLoader.GetString2("To") + " " + selectedAppointment.End.ToString("HH:mm");

                    timeEditStart.Time = selectedAppointment.Start;
                    timeEditEnd.Time = selectedAppointment.End;
                  
                }
                
            }
        
        }


        private void OperatorAssignCreateForm_Load(object sender, EventArgs e)
        {
         if (this.parentForm != null)
             this.parentForm.OperatorAssignCommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(OperatorAssignCreateForm_OperatorAssignCommittedChanges);        
       
        }

        void OperatorAssignCreateForm_OperatorAssignCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            if (e.Objects[0] is WorkShiftVariationClientData)
            {
                WorkShiftVariationClientData var = e.Objects[0] as WorkShiftVariationClientData;

                if (var.Code == appWorkShift.Code)
                {
           
                    FormUtil.InvokeRequired(this, delegate
                    {
                        MessageForm.Show(ResourceLoader.GetString2("WorkShiftWasChangedFormWillBeClose"), MessageFormType.Warning);
                        this.Close();
                    });
                }
            }
            else if (e.Objects[0] is OperatorClientData)
            {
                OperatorClientData oper = e.Objects[0] as OperatorClientData;
                if (appOperator.Code == oper.Code || appWorkShift.Operators.Contains(oper) == true)
                {
                    FormUtil.InvokeRequired(this, delegate
                    {
                        MessageForm.Show(ResourceLoader.GetString2("OperatorWasChangedFormWillBeClose"), MessageFormType.Warning);
                        this.Close();
                    });
                
                }            
            }
        }


        private void OperatorAssignCreateForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.parentForm != null)
                this.parentForm.OperatorAssignCommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(OperatorAssignCreateForm_OperatorAssignCommittedChanges);        
       
        }



        private void LoadSupervisorSchedules()
        {
            supervisorSchedules = new Dictionary<int, IList>();
            IList schedules;

            foreach (GridControlDataSupervisor gridData in dataSourceSupervisors)
            {
				schedules = 
					(IList)ServerServiceClient.GetInstance().SearchClientObjects(
						SmartCadHqls.GetCustomHql(
							SmartCadHqls.GetWorkShitftSchedulesWithOperators,
							gridData.Code, 
							(int)WorkShiftVariationClientData.WorkShiftType.TimeOff, 
							ApplicationUtil.GetDataBaseFormattedDate(ServerServiceClient.GetInstance().GetTimeFromDB())));

                supervisorSchedules.Add(gridData.Code, schedules);
            }
        }

        private void LoadLanguage() 
        {
            layoutControlItemName.Text = ResourceLoader.GetString2("Name") + ":";
            layoutControlItemWorkShift.Text = ResourceLoader.GetString2("Schedule") + ":";
            layoutControlItemDate.Text = ResourceLoader.GetString2("Date") + ":";
            layoutControlGroupInf.Text = ResourceLoader.GetString2("Data");
            layoutControlGroupAssigment.Text = ResourceLoader.GetString2("AssignedSupervisors");
         
            layoutControlGroupSupervisors.Text = ResourceLoader.GetString2("Supervisors");
            radioButtonByRange.Text = ResourceLoader.GetString2("ByRange");
            radioButtonByWorkShift.Text = ResourceLoader.GetString2("ByWorkShift");
            layoutControlItemEnd.Text = ResourceLoader.GetString2("EndTime") + ":";
            layoutControlItemStart.Text = ResourceLoader.GetString2("StartTime") + ":";
            simpleButtonAssign.Text = ResourceLoader.GetString2("Assign");

            layoutControlGroupOperators.Text = ResourceLoader.GetString2("AssignedOperators");
           
            simpleButtonCancel.Text = ResourceLoader.GetString2("Cancel");
            simpleButtonClean.Text = ResourceLoader.GetString2("Clear");
            simpleButtonCreate.Text = ResourceLoader.GetString2("Create"); 
            simpleButtonDelete.Text = ResourceLoader.GetString2("Delete");
            simpleButtonRecurrence.Text = ResourceLoader.GetString2("CreateRecurrence");
            this.Text = ResourceLoader.GetString2("Assignment");
                    
        }


        private void FillSupervisors(DateTime start, DateTime end)
        {
            if (dataSourceSupervisors != null)
            {
                IList availables = new ArrayList();
                bool doBreak = false;
                IList schedules;
                DateTime oldStart = new DateTime();
                DateTime oldEnd = new DateTime();
                Dictionary<int, List<WorkShiftScheduleVariationClientData>> item = new Dictionary<int, List<WorkShiftScheduleVariationClientData>>();
                List<WorkShiftScheduleVariationClientData> schedulesAvailables = new List<WorkShiftScheduleVariationClientData>();
                schedulesAvailablesBySupervisor = new Dictionary<int, Dictionary<int, List<WorkShiftScheduleVariationClientData>>>();
                
                foreach (GridControlDataSupervisor gridData in dataSourceSupervisors)
                {
                    item = new Dictionary<int, List<WorkShiftScheduleVariationClientData>>();
                    OperatorClientData sup = gridData.Tag as OperatorClientData;
                    doBreak = false;

                    schedules = supervisorSchedules[sup.Code];
                    
                    if (mode == CreationMode.BY_RANGE)
                    {
                        foreach (WorkShiftScheduleVariationClientData sc in schedules)
                        {
                            if (sc.Start.Date > start.Date)
                                break;

                            if (sc.Start != oldEnd)
                            {
                                oldStart = sc.Start;
                                schedulesAvailables = new List<WorkShiftScheduleVariationClientData>();
                            }

                            if (oldStart <= start && end <= sc.End)
                            {
                                availables.Add(gridData);
                                schedulesAvailables.Add(sc);
                                item.Add(appSchedule.Code, schedulesAvailables);
                                schedulesAvailablesBySupervisor.Add(sup.Code, item);
                                doBreak = true;
                                break;
                            }
                            else
                            {
                                oldStart = sc.Start;
                                oldEnd = sc.End;
                                schedulesAvailables.Add(sc);
                            }

                        }
                    }
                    else
                    {
                        bool isAvailable = false;
                      
                        foreach (WorkShiftScheduleVariationClientData wsSchedule in appWorkShift.Schedules)
                        {
                            isAvailable = false;
                            foreach (WorkShiftScheduleVariationClientData supSchedule in schedules)
                            {
                                if (supSchedule.Start != oldEnd || (supSchedule.Start.Hour == 0 && supSchedule.Start.Minute == 0 && oldEnd.Hour == 0 && oldEnd.Minute == 0))
                                {
                                    oldStart = supSchedule.Start;
                                    schedulesAvailables =  new List<WorkShiftScheduleVariationClientData>();    
                                }

                                if (oldStart <= wsSchedule.Start && wsSchedule.End <= supSchedule.End)
                                {
                                    isAvailable = true;
                                    schedulesAvailables.Add(supSchedule);
                                    item.Add(wsSchedule.Code, schedulesAvailables);
                                    break;
                                }
                                else
                                {
                                    oldStart = supSchedule.Start;
                                    oldEnd = supSchedule.End;
                                    schedulesAvailables.Add(supSchedule);
                                }
                            }

                            if (isAvailable == false)
                                break;
                        }

                        if (isAvailable == true)
                        {
                            availables.Add(gridData);
                            schedulesAvailablesBySupervisor.Add(sup.Code, item);
                        }
                    }

                }

                gridControlExSupervisors.BeginUpdate();
                gridControlExSupervisors.DataSource = availables;
                gridControlExSupervisors.EndUpdate();

            }


        }

        private void timeEditEnd_EditValueChanged(object sender, EventArgs e)
        {
            timeEditEnd.EditValueChanged -= new EventHandler(timeEditEnd_EditValueChanged);
            DateTime value = timeEditEnd.Time;
            if (value.Date > timeEditStart.Time.Date && value.Hour != 0)
                timeEditEnd.EditValue = timeEditStart.Time.Date.Add(value.TimeOfDay);

            if (value.TimeOfDay < timeEditStart.Time.TimeOfDay && (value.Hour != 0 || value.Minute != 0))
            {
                timeEditEnd.EditValue = timeEditStart.Time;
            }
            else if (value.TimeOfDay > maxValue.TimeOfDay && (maxValue.Hour != 0 || maxValue.Minute != 0))
            {
                timeEditEnd.EditValue = maxValue;
            }

          
            FillSupervisors(timeEditStart.Time, timeEditEnd.Time);

            timeEditEnd.EditValueChanged += new EventHandler(timeEditEnd_EditValueChanged);
            
        }

        private void timeEditStart_EditValueChanged(object sender, EventArgs e)
        {
            timeEditStart.EditValueChanged -= new EventHandler(timeEditStart_EditValueChanged);
            DateTime value = timeEditStart.Time;

            if (value.TimeOfDay < minValue.TimeOfDay)
            {
                timeEditStart.EditValue = minValue;
            }
            else if (value.TimeOfDay > timeEditEnd.Time.TimeOfDay  && (timeEditEnd.Time.Hour != 0 || timeEditEnd.Time.Minute != 0)) 
            {
                timeEditStart.EditValue = timeEditEnd.Time;
            }

         
            FillSupervisors(timeEditStart.Time, timeEditEnd.Time);
            timeEditStart.EditValueChanged += new EventHandler(timeEditStart_EditValueChanged);
        }


        void timeEditEnd_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            //timeEditEnd.EditValueChanging -= new DevExpress.XtraEditors.Controls.ChangingEventHandler(timeEditEnd_EditValueChanging);
            //DateTime value = DateTime.Parse(e.NewValue.ToString(), System.Globalization.CultureInfo.CreateSpecificCulture("en-US").DateTimeFormat);
            //DateTime oldValue = DateTime.Parse(e.OldValue.ToString(), System.Globalization.CultureInfo.CreateSpecificCulture("en-US").DateTimeFormat);
         
            //if (value.TimeOfDay < selectedAppointment.Start.TimeOfDay) 
            //{
            //    e.Cancel = true;
            //    timeEditEnd.EditValue = timeEditStart.Time;
            //}
            //else if (value.TimeOfDay > selectedAppointment.End.TimeOfDay)
            //{
            //    e.Cancel = true;
            //    timeEditEnd.EditValue = maxValue;
            //}

            //timeEditEnd.Refresh();
            //FillSupervisors(timeEditStart.Time, timeEditEnd.Time);
           
            //timeEditEnd.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(timeEditEnd_EditValueChanging);
            
        }



        void timeEditStart_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            //timeEditStart.EditValueChanging -= new DevExpress.XtraEditors.Controls.ChangingEventHandler(timeEditStart_EditValueChanging);
            //DateTime value = DateTime.Parse(e.NewValue.ToString(), System.Globalization.CultureInfo.CreateSpecificCulture("en-US").DateTimeFormat);
            //DateTime oldValue = DateTime.Parse(e.OldValue.ToString(), System.Globalization.CultureInfo.CreateSpecificCulture("en-US").DateTimeFormat);
            
            //if (value.TimeOfDay < selectedAppointment.Start.TimeOfDay)
            //{
            //    e.Cancel = true;
            //    timeEditStart.EditValue = minValue;                
            //}
            //else if (value.TimeOfDay > timeEditEnd.Time.TimeOfDay)
            //{
            //    e.Cancel = true;
            //    timeEditStart.EditValue = timeEditEnd.Time;
            //}
            
            //timeEditStart.Refresh();
            //FillSupervisors(timeEditStart.Time, timeEditEnd.Time);
            //timeEditStart.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(timeEditStart_EditValueChanging);

        }




        private void InitializeDataGrids()
        {
            gridControlExOperators.Type = typeof(GridControlDataOperatorEx);
            gridControlExOperators.ViewTotalRows = true;
            gridControlExOperators.EnableAutoFilter = true;
            gridViewExOperators.ViewTotalRows = true;
            gridViewExOperators.GroupFormat = "{1}";


            gridControlExSupervisors.Type = typeof(GridControlDataSupervisor);
            gridControlExSupervisors.ViewTotalRows = true;
            gridControlExSupervisors.EnableAutoFilter = true;
            gridViewExSupervisors.ViewTotalRows = true;

            gridControlExAssigment.Type = typeof(GridControlDataOperatorAssign);
            gridControlExAssigment.ViewTotalRows = true;
            gridControlExAssigment.EnableAutoFilter = true;
            gridControlExAssigment.ViewTotalRows = true;
            gridViewExAssigment.Columns["Checked"].Visible = false;
            gridViewExAssigment.OptionsView.ColumnAutoWidth = true;
        
        }

        private void simpleButtonAssign_Click(object sender, EventArgs e)
        {
            currentDate = ServerServiceClient.GetInstance().GetTimeFromDB();
            if (timeEditStart.Time >= timeEditEnd.Time)
            {
                MessageForm.Show(ResourceLoader.GetString2("ErrorGreaterEndDate"), MessageFormType.Information);
                return;
            }
            else if (minValue.Date == currentDate.Date)
            {
                if (timeEditStart.Time.TimeOfDay <= currentDate.TimeOfDay && mode == CreationMode.BY_RANGE)
                {
                    MessageForm.Show(ResourceLoader.GetString2("ErrorGreaterStartDate"), MessageFormType.Information);
                    return;
                }
            }
            if (gridControlExSupervisors.SelectedItems.Count > 0)
            {

                bool showError = false;
               
                List<OperatorAssignClientData> assigns = new List<OperatorAssignClientData>();
        
                if (mode == CreationMode.BY_RANGE)
                {
                    assigns.Add(CreateOperatorAssign(appSchedule, false));
                    
                }
                else if (mode == CreationMode.COMPLETE) 
                {
                    foreach (WorkShiftScheduleVariationClientData schedule in appWorkShift.Schedules)
                    {
                        assigns.Add(CreateOperatorAssign(schedule, true));
                    }
                }


                ArrayList assignList = new ArrayList();
                
                //Esto se hace para verificar las asignaciones del operador ya creadas
                if (mode == CreationMode.BY_RANGE && isBatchAssignment == false) 
                      assignList.AddRange(appOperator.Supervisors);
               
                //Agrego a la lista las asignaciones que estan en el grid                
                foreach (GridControlDataOperatorAssign data in gridControlExAssigment.Items)
                    assignList.Add(data.OperatorAssign);

                foreach (OperatorAssignClientData operAssign in assigns)
                {                    
                    //Verifico que no existan asignaciones que se solapen con la que estoy creando.
                    if (ContainsOperatorAssign(operAssign, assignList) == false)
                    {
                        
                        assignsToCheckForRecurrence.Add(operAssign);
                        gridControlExAssigment.AddOrUpdateItem(new GridControlDataOperatorAssign(operAssign));
                    }
                    else
                        showError = true;
                }

                
                if (showError == true)
                    MessageForm.Show(ResourceLoader.GetString2("ErrorAssignments"), MessageFormType.Information);

                CheckEnabledButtons();
            }

        }

        //Crea un OperatorAssign a partir de un schedule
        private OperatorAssignClientData CreateOperatorAssign(WorkShiftScheduleVariationClientData schedule, bool isComplete) 
        {
            GridControlDataSupervisor supGridData = gridControlExSupervisors.SelectedItems[0] as GridControlDataSupervisor;
            OperatorAssignClientData assign = new OperatorAssignClientData();
            if (isComplete == true)
            {
                if (schedule.Start >= currentDate)
                    assign.StartDate = schedule.Start;
                else if (schedule.Start < currentDate && schedule.End > currentDate)
                    assign.StartDate = currentDate;
                
                assign.EndDate = schedule.End;
            }
            else 
            {
                assign.StartDate = schedule.Start.Date.Add(timeEditStart.Time.TimeOfDay);
                if (schedule.End.Date > schedule.Start.Date && (timeEditEnd.Time.Hour != 0 || timeEditEnd.Time.Minute != 0))
                    assign.EndDate = schedule.Start.Date.Add(timeEditEnd.Time.TimeOfDay);
                else
                    assign.EndDate = schedule.End.Date.Add(timeEditEnd.Time.TimeOfDay);
            }
            assign.SupervisedScheduleVariation = schedule;
            //assign.SupervisedOperatorCode = operCode;
            assign.SupervisorCode = supGridData.Code;
            assign.SupFirstName = supGridData.FirstName;
            assign.SupLastName = supGridData.LastName;

            return assign;            
        }

        //Verifica si la lista de asignaciones pasada como parametro contiene o se solapa con la asignacion pasada
        //como parametro.
        private bool ContainsOperatorAssign(OperatorAssignClientData assign, ArrayList list) 
        {
            foreach (OperatorAssignClientData operAssign in list)
            {
                if (assign.SupervisedScheduleVariation.Code == operAssign.SupervisedScheduleVariation.Code &&
                         !(assign.StartDate >= operAssign.EndDate || assign.EndDate <= operAssign.StartDate))
                {
                    return true;
                }

            }
            return false;
        
        }

        private void simpleButtonClean_Click(object sender, EventArgs e)
        {
            gridControlExAssigment.BeginUpdate();
            gridControlExAssigment.DataSource = null;
            gridControlExAssigment.EndUpdate();
            assignsToCheckForRecurrence.Clear();
            CheckEnabledButtons();
        }

        private void simpleButtonDelete_Click(object sender, EventArgs e)
        {
            if (gridControlExAssigment.SelectedItems.Count > 0) 
            {
                assignsToCheckForRecurrence.Remove(((GridControlDataOperatorAssign)gridControlExAssigment.SelectedItems[0]).Tag as OperatorAssignClientData);
                gridControlExAssigment.DeleteItem((GridControlDataOperatorAssign)gridControlExAssigment.SelectedItems[0]);
                CheckEnabledButtons();
            }

        }

        private void gridViewExSupervisors_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (gridViewExSupervisors.FocusedRowHandle > -1)
            {
                this.simpleButtonAssign.Enabled = true;
                if (gridControlExSupervisors.SelectedItems.Count > 0)
                {
                    GridControlDataSupervisor gridData = gridControlExSupervisors.SelectedItems[0] as GridControlDataSupervisor;
                    BindingList<GridControlDataOperatorEx> operatorsDataSource = new BindingList<GridControlDataOperatorEx>();
                    IList wsvList =
                            ServerServiceClient.GetInstance().SearchClientObjects(
                                SmartCadHqls.GetCustomHql(
                                    SmartCadHqls.GetFutureWorkShiftVariationWithOperatorsBySupervisor,
                                    (int)WorkShiftVariationClientData.WorkShiftType.TimeOff,
                                    gridData.Code,
                                    ApplicationUtil.GetDataBaseFormattedDate(DateTime.Now.Date)));


                    if (wsvList != null)
                    {
                        foreach (WorkShiftVariationClientData wsv in wsvList)
                        {
                            foreach (OperatorClientData oper in wsv.Operators)
                            {
                                operatorsDataSource.Add(new GridControlDataOperatorEx(oper, wsv, null));
                            }
                        }

                        if (operatorsDataSource.Count > 0)
                        {
                            gridControlExOperators.BeginUpdate();
                            gridControlExOperators.DataSource = operatorsDataSource;
                            gridControlExOperators.EndUpdate();
                        }
                        else
                        {
                            gridControlExOperators.BeginUpdate();
                            gridControlExOperators.DataSource = null;
                            gridControlExOperators.EndUpdate();
                        }
                    }
                }
            }
            else 
            {
                this.simpleButtonAssign.Enabled = false;
                gridControlExOperators.BeginUpdate();
                gridControlExOperators.DataSource = null;
                gridControlExOperators.EndUpdate();
                
            }
        }

        private void simpleButtonCreate_Click(object sender, EventArgs e)
        {           
            try
            {
                BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[1] { GetType().GetMethod("simpleButtonCreate_Click_1", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[0] });
                processForm.CanThrowError = true;
                processForm.ShowDialog();
            }
            catch (FaultException ex)
            {                          
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, MessageFormType.Error);
            }
            catch (Exception ex)
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, ex);
             
            }
       
        }

        private void simpleButtonCreate_Click_1()
        {

            IList assignsToSave = new ArrayList();
            IList assignsConflicts = new ArrayList();
            IList assignsToDelete = new ArrayList();
            IList conflictsWithVacation = new ArrayList();
            DialogResult dialogResult;
            List<WorkShiftScheduleVariationClientData> supervisorSchedules = new List<WorkShiftScheduleVariationClientData>();
            OperatorAssignClientData tag;
           

            OperatorAssignClientData operAssign = new OperatorAssignClientData();

            if (isBatchAssignment == false && mode == CreationMode.BY_RANGE)
            {
                foreach (GridControlDataOperatorAssign gridData in gridControlExAssigment.Items)
                {
                    tag = gridData.Tag as OperatorAssignClientData;
                    supervisorSchedules = schedulesAvailablesBySupervisor[tag.SupervisorCode][tag.SupervisedScheduleVariation.Code];
                    foreach (WorkShiftScheduleVariationClientData supSchedule in supervisorSchedules)
                    {
                        operAssign = tag.Clone();
                        operAssign.SupervisedOperatorCode = appOperator.Code;
                        operAssign.SupervisorScheduleVariation = supSchedule;

                        if (supSchedule.Start >= operAssign.StartDate)
                            operAssign.StartDate = supSchedule.Start;
                        if (supSchedule.End <= operAssign.EndDate)
                            operAssign.EndDate = supSchedule.End;

                        assignsToSave.Add(operAssign);
                
                    }
                }
            }
            else
            {

                if (isBatchAssignment == true)
                {
                    foreach (OperatorClientData oper in appWorkShift.Operators)
                    {
                        if (oper.IsSupervisor == false)
                        {
                            foreach (GridControlDataOperatorAssign gridData in gridControlExAssigment.Items)
                            {
                                tag = gridData.Tag as OperatorAssignClientData;
                                supervisorSchedules = schedulesAvailablesBySupervisor[tag.SupervisorCode][tag.SupervisedScheduleVariation.Code];
                                foreach (WorkShiftScheduleVariationClientData supSchedule in supervisorSchedules)
                                {
                                    operAssign = tag.Clone();
                                    operAssign.SupervisedOperatorCode = oper.Code;
                                    operAssign.SupervisorScheduleVariation = supSchedule;

                                    if (supSchedule.Start >= operAssign.StartDate)
                                        operAssign.StartDate = supSchedule.Start;
                                    if (supSchedule.End <= operAssign.EndDate)
                                        operAssign.EndDate = supSchedule.End;

                                    assignsToSave.Add(operAssign);
                            
                                }

                             }
                        }
                    }

                    assignsConflicts = SearchAssignsConflicts(appWorkShift.Operators, gridControlExAssigment.Items);
                }
                else //Is complete
                {
                    foreach (GridControlDataOperatorAssign gridData in gridControlExAssigment.Items)
                    {
                        tag = gridData.Tag as OperatorAssignClientData;
                        supervisorSchedules = schedulesAvailablesBySupervisor[tag.SupervisorCode][tag.SupervisedScheduleVariation.Code];
                        foreach (WorkShiftScheduleVariationClientData supSchedule in supervisorSchedules)
                        {
                            operAssign = tag.Clone();
                            operAssign.SupervisedOperatorCode = appOperator.Code;
                            operAssign.SupervisorScheduleVariation = supSchedule;

                            if (supSchedule.Start >= operAssign.StartDate)
                                operAssign.StartDate = supSchedule.Start;
                           if (supSchedule.End <= operAssign.EndDate)
                                operAssign.EndDate = supSchedule.End;

                            assignsToSave.Add(operAssign);

                        }                    
                    }
                                        
                    assignsConflicts = SearchAssignsConflicts(appOperator, gridControlExAssigment.Items);

                }
            }

            if (assignsConflicts.Count > 0)
            {
                if (frmShowRecurrence != true)
                {
                    if (isBatchAssignment == false && frmShowRecurrence == true)
                        //dialogResult = MessageForm.Show("El operador ya tienen asignaciones para los intervalos seleccionados, si continua  se sobrescribiran estas asignaciones. Desea continuar?", MessageFormType.WarningQuestion);
                        dialogResult = MessageForm.Show(ResourceLoader.GetString2("MessageBoxErrorWorkShiftAssignment"), MessageFormType.WarningQuestion);
                    else
                        //dialogResult = MessageForm.Show("Algunos operadores del turno ya tienen asignaciones para los intervalos seleccionados, si continua  se sobrescribiran estas asignaciones. Desea continuar?", MessageFormType.WarningQuestion);                    
                        dialogResult = MessageForm.Show(ResourceLoader.GetString2("MessageBoxErrorWorkShiftAssignmentSome"), MessageFormType.WarningQuestion);
                }
                else
                    dialogResult = DialogResult.No;

                if (dialogResult == DialogResult.Yes)
                {
                    runSave = true;
                    object[] list = ResolveAssignsConflicts(assignsToSave, assignsConflicts);

                    assignsToSave = (IList)list[0];
                    assignsToDelete = (IList)list[1];
                }
                else
                    runSave = false;
            }

            if (runSave == true)
            {

                if (assignsToSave.Count > 0)
                {
                    conflictsWithVacation = SearchAssignsConflictsWithVacation(assignsToSave);

                    if (conflictsWithVacation.Count > 0)
                    {
                        if (mode == CreationMode.BY_RANGE)
                        {
                            if (isBatchAssignment == false)
                                dialogResult = MessageForm.Show(ResourceLoader.GetString2("UserAssignedForVacationsInterval"), MessageFormType.Warning);
                            else
                                dialogResult = MessageForm.Show(ResourceLoader.GetString2("UsersAssignedForVacationsInterval"), MessageFormType.Warning);
                        }
                        else
                        {
                            if (isBatchAssignment == false)
                                dialogResult = MessageForm.Show(ResourceLoader.GetString2("UserAssignedForVacationsMultipleInterval"), MessageFormType.Warning);
                            else                                                           
                                dialogResult = MessageForm.Show(ResourceLoader.GetString2("UsersAssignedForVacationsMultipleInterval"), MessageFormType.Warning);
                        
                        }
                        if (dialogResult == DialogResult.Yes)
                        {
                            runSave = true;
                            assignsToSave = RemoveConflictsWithVacations(assignsToSave, conflictsWithVacation);
                        }
                        else
                            runSave = false;
                    }

                    if (runSave == true)
                    {
                        if (assignsToDelete.Count > 0)
                            ServerServiceClient.GetInstance().DeleteClientObjectCollection(assignsToDelete);

                        ServerServiceClient.GetInstance().SaveOrUpdateClientData(assignsToSave);
                    }
                }
            }

        }

        private IList RemoveConflictsWithVacations(IList assignsToSave, IList conflicts)
        {            
            if (conflicts != null && conflicts.Count > 0)
            {
                IList assignsToRemove = new ArrayList();

                for (int i = 0; i < assignsToSave.Count; i++)
                {
                    OperatorAssignClientData assign = (OperatorAssignClientData)assignsToSave[i];
                    foreach (object[] item in conflicts)
                    {
                        if ((assign.SupervisedOperatorCode == (int)item[3] || assign.SupervisorCode == (int)item[3]) &&
                                (assign.StartDate > (DateTime)item[1] && assign.StartDate < (DateTime)item[2] ||
                                  assign.EndDate > (DateTime)item[1] && assign.EndDate < (DateTime)item[2] ||
                                  assign.StartDate <= (DateTime)item[1] && assign.EndDate >= (DateTime)item[2]))
                        {
                            assignsToRemove.Add(i);
                            break;
                        }
                    }
                }

                for (int i = assignsToRemove.Count - 1; i >= 0; i--)
                    assignsToSave.RemoveAt((int)assignsToRemove[i]);

            }

            return assignsToSave;
        }

      

        private IList SearchAssignsConflictsWithVacation(IList assignList) 
        {
            string hql = string.Format(@"SELECT schedules.Code, schedules.Start, schedules.End, operators.Code FROM WorkShiftScheduleVariationData schedules
                          left join schedules.WorkShiftVariation.Operators operators 
                         WHERE schedules.WorkShiftVariation.Type = {0} AND schedules.WorkShiftVariation.ObjectType = 0 AND (", (int)WorkShiftVariationClientData.WorkShiftType.TimeOff);
            
            foreach (OperatorAssignClientData assign in assignList)
            {
                hql += string.Format("(('{0}' < schedules.Start and schedules.Start < '{1}' OR " +
                                     "'{0}' < schedules.End and schedules.End < '{1}' OR " +
                                     "schedules.Start < '{0}' and '{0}' < schedules.End OR " +
                                     "schedules.Start < '{1}' and '{1}' < schedules.End OR " +
                                     "schedules.Start = '{0}' and schedules.End = '{1}') AND operators.Operator.Code IN ({2},{3})) OR", ApplicationUtil.GetDataBaseFormattedDate(assign.StartDate), ApplicationUtil.GetDataBaseFormattedDate(assign.EndDate), assign.SupervisedOperatorCode, assign.SupervisorCode);
          

            }

            hql = hql.Substring(0, hql.Length - 2) + ") order by schedules.Start";

            IList conflicts = (IList)ServerServiceClient.GetInstance().SearchBasicObjects(hql);

            return conflicts; 
        
        }


        private object[] ResolveAssignsConflicts(IList newAssings, IList conflicts) 
        {
            object[] result = new object[2];
            IList assignsToSave = new ArrayList();
            IList assignsToDelete = new ArrayList();
            OperatorAssignClientData operAssign;

            foreach (OperatorAssignClientData newAssign in newAssings)
            {
                foreach (OperatorAssignClientData oldAssign in conflicts)
                {

                    if (oldAssign.SupervisedOperatorCode == newAssign.SupervisedOperatorCode &&
                                oldAssign.SupervisedScheduleVariation.Code == newAssign.SupervisedScheduleVariation.Code)
                    {

                        if (newAssign.StartDate <= oldAssign.StartDate && newAssign.EndDate < oldAssign.EndDate)
                        {
                            oldAssign.StartDate = newAssign.EndDate;
                            assignsToSave.Add(oldAssign);
                        }
                        else if (newAssign.StartDate > oldAssign.StartDate && newAssign.EndDate >= oldAssign.EndDate)
                        {
                            oldAssign.EndDate = newAssign.StartDate;
                            assignsToSave.Add(oldAssign);

                        }
                        else if (newAssign.StartDate > oldAssign.StartDate && newAssign.EndDate < oldAssign.EndDate)
                        {
                            //Creo una nueva asignacion similar a la vieja, pero modificandole la fecha fin.
                            operAssign = new OperatorAssignClientData();
                            operAssign.SupervisedOperatorCode = oldAssign.SupervisedOperatorCode;
                            operAssign.SupervisedScheduleVariation = oldAssign.SupervisedScheduleVariation;
                            operAssign.SupervisorScheduleVariation = oldAssign.SupervisorScheduleVariation;
                            operAssign.StartDate = oldAssign.StartDate;
                            operAssign.EndDate = newAssign.StartDate;
                            operAssign.SupervisorCode = oldAssign.SupervisorCode;

                            //Modifico la fecha fin de la asignacion vieja
                            oldAssign.StartDate = newAssign.EndDate;
                            assignsToSave.Add(operAssign);
                            assignsToSave.Add(oldAssign);

                        }
                        else if (newAssign.StartDate <= oldAssign.StartDate && newAssign.EndDate >= oldAssign.EndDate)
                            assignsToDelete.Add(oldAssign);

                    }

                }
                //Esto se debe hacer para todos los casos
                assignsToSave.Add(newAssign);
            }

            result[0] = assignsToSave;
            result[1] = assignsToDelete;


            return result;
        }

        //private IList SearchAssignsConflicts(IList list) 
        //{
        //     string hql = "select oad from OperatorAssignData oad where ";

        //    foreach (OperatorAssignClientData oad in list)
        //        hql += string.Format("((oad.StartDate between '{0}' and '{1}' or " +
        //                                "oad.EndDate between '{0}' and '{1}' or "+
        //                                "'{0}' between oad.StartDate and oad.EndDate or "+
        //                                "'{1}' between oad.StartDate and oad.EndDate) and "+
        //                                "oad.SupervisedOperator.Code = {2}) or",
        //                                ApplicationUtil.GetDataBaseFormattedDate(oad.StartDate), ApplicationUtil.GetDataBaseFormattedDate(oad.EndDate),oad.SupervisedOperatorCode);
           
        //    if (list.Count > 0)
        //        hql = hql.Substring(0, hql.Length - 2);

        //    IList collideAssigments = ServerServiceClient.GetInstance().SearchClientObjects(hql);

        //    return list;
        
        //}

        private IList SearchAssignsConflicts(IList operList, IList assignList)
        {
            
            string hql = "SELECT oad FROM OperatorAssignData oad WHERE (";
            OperatorAssignClientData operA = new OperatorAssignClientData();
            foreach (GridControlDataOperatorAssign oad in assignList)
            {
                operA = oad.Tag as OperatorAssignClientData;
                hql += string.Format("(oad.StartDate between '{0}' and '{1}' OR " +
                                        "oad.EndDate between '{0}' and '{1}' OR " +
                                        "'{0}' between oad.StartDate and oad.EndDate OR " +
                                        "'{1}' between oad.StartDate and oad.EndDate) OR", ApplicationUtil.GetDataBaseFormattedDate(operA.StartDate), ApplicationUtil.GetDataBaseFormattedDate(operA.EndDate));
            }

            hql = hql.Substring(0, hql.Length - 2);

            hql += ") AND oad.SupervisedOperator.Code IN (";

            foreach (OperatorClientData oper in operList)
                hql += string.Format("{0},", oper.Code);

            hql = hql.Substring(0, hql.Length - 1);

            hql += ")";


            IList conflicts = ServerServiceClient.GetInstance().SearchClientObjects(hql);

            return conflicts;

        }


        private IList SearchAssignsConflicts(OperatorClientData oper, IList assignList)
        {

            string hql = "SELECT oad FROM OperatorAssignData oad WHERE (";
            OperatorAssignClientData operA = new OperatorAssignClientData();
            foreach (GridControlDataOperatorAssign oad in assignList)
            {
                operA = oad.Tag as OperatorAssignClientData;
                hql += string.Format("(oad.StartDate between '{0}' and '{1}' OR " +
                                        "oad.EndDate between '{0}' and '{1}' OR " +
                                        "'{0}' between oad.StartDate and oad.EndDate OR " +
                                        "'{1}' between oad.StartDate and oad.EndDate) OR", ApplicationUtil.GetDataBaseFormattedDate(operA.StartDate), ApplicationUtil.GetDataBaseFormattedDate(operA.EndDate));
            }

            hql = hql.Substring(0, hql.Length - 2);

            hql += ") AND oad.SupervisedOperator.Code =" + oper.Code;


            IList conflicts = ServerServiceClient.GetInstance().SearchClientObjects(hql);

            return conflicts;

        }

        private void simpleButtonRecurrence_Click(object sender, EventArgs e)
        {
            if (runSave == true)
            {
                OperatorAssignRecurrenceForm form;

                if (appOperator != null)
                    form = new OperatorAssignRecurrenceForm(assignsToCheckForRecurrence, appOperator.Code, appWorkShift, appSchedule, isBatchAssignment, supervisorSchedules);
                else
                    form = new OperatorAssignRecurrenceForm(assignsToCheckForRecurrence, 0, appWorkShift, appSchedule, isBatchAssignment, supervisorSchedules);

                if (form.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    frmShowRecurrence = true;
                    simpleButtonCreate_Click(null, null);
                    this.Close();
                }
            }
          

        }

        private void radioButton_CheckedChanged(object sender, EventArgs e)
        {
            timeEditStart.EditValueChanging -=new DevExpress.XtraEditors.Controls.ChangingEventHandler(timeEditStart_EditValueChanging);
            timeEditEnd.EditValueChanging -=new DevExpress.XtraEditors.Controls.ChangingEventHandler(timeEditEnd_EditValueChanging);
            
            timeEditStart.EditValue = minValue;
            timeEditEnd.EditValue = maxValue;

            simpleButtonClean.PerformClick();
            
            if (radioButtonByRange.Checked == true) 
            {              
                mode = CreationMode.BY_RANGE;
                timeEditStart.Enabled = true;
                timeEditEnd.Enabled = true;
            }
            else if (radioButtonByWorkShift.Checked == true) 
            {
                mode = CreationMode.COMPLETE;
                timeEditStart.Enabled = false;
                timeEditEnd.Enabled = false;
                simpleButtonRecurrence.Enabled = false;
            }
            
            FillSupervisors(timeEditStart.Time, timeEditEnd.Time);

            timeEditStart.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(timeEditStart_EditValueChanging);
            timeEditEnd.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(timeEditEnd_EditValueChanging);
          
        }

        private void CheckEnabledButtons() 
        {
            if (gridControlExAssigment.Items.Count > 0)
            {
                simpleButtonClean.Enabled = true;
                simpleButtonCreate.Enabled = true;
                if (radioButtonByWorkShift.Checked == false)
                    simpleButtonRecurrence.Enabled = true;
                else
                    simpleButtonRecurrence.Enabled = false;
            }
            else
            {
                simpleButtonClean.Enabled = false;
                simpleButtonCreate.Enabled = false;
                simpleButtonRecurrence.Enabled = false;
            }        
        }


        private void gridViewExAssigment_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle > -1)
                simpleButtonDelete.Enabled = true;
            else
                simpleButtonDelete.Enabled = false;
            
        }

   





    }


    public class GridControlDataOperatorAssign : GridControlData
    {

        private bool check;

        public GridControlDataOperatorAssign(OperatorAssignClientData operAssign)
            : base(operAssign)
        {

        }

        public OperatorAssignClientData OperatorAssign
        {
            get { return this.Tag as OperatorAssignClientData; }
        }

        public int Code
        {
            get { return OperatorAssign.Code; }
        }


        [GridControlRowInfoData(Visible = true, RepositoryType = typeof(RepositoryItemCheckEdit), Searchable = false)]
        public bool Checked
        {
            get { return check; }
            set { check = value; }
        }

        [GridControlRowInfoData(Visible = true)]
        public string FirstName
        {
            get { return OperatorAssign.SupFirstName; }
        }

        [GridControlRowInfoData(Visible = true)]
        public string LastName
        {
            get { return OperatorAssign.SupLastName; }
        }

        [GridControlRowInfoData(Visible = true)]
        public string Date
        {
            get
            {
                return OperatorAssign.StartDate.ToString("MM/dd/yyyy");
            }
        }

        [GridControlRowInfoData(Visible = false)]
        public int DayOfWeek
        {
            get
            {
                return (int)OperatorAssign.StartDate.DayOfWeek;
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string StartTime
        {
            get
            {
                return OperatorAssign.StartDate.ToString("HH:mm");
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string EndTime
        {
            get
            {
                return OperatorAssign.EndDate.ToString("HH:mm");
            }
        }


        public override bool Equals(object obj)
        {
            bool result = false;
            GridControlDataOperatorAssign var = obj as GridControlDataOperatorAssign;
            if (var != null)
            {
                OperatorAssignClientData oad = (OperatorAssignClientData)var.OperatorAssign;
                if (this.OperatorAssign.Equals(oad) == true)
                    result = true;

            }
            return result;

        }


    }
}