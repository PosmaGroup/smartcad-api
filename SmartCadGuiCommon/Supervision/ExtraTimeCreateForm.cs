using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Reflection;
using System.ServiceModel;
using Smartmatic.SmartCad.Service;
using DevExpress.XtraGrid.Columns;
using SmartCadCore.ClientData;
using SmartCadCore.Enums;
using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadGuiCommon.SyncBoxes;

namespace SmartCadGuiCommon
{
    public partial class ExtraTimeCreateForm : DevExpress.XtraEditors.XtraForm
    {
        #region fields
        FormBehavior behaviorType = FormBehavior.Edit;
        WorkShiftVariationClientData selectedVariation = null;
        private bool? isTimeOff;
        private IList toDelete = new ArrayList();
        #endregion

        public ExtraTimeCreateForm()
        {
            InitializeComponent();
        }
        public ExtraTimeCreateForm(WorkShiftVariationClientData variation, FormBehavior behaviorType,  bool? isTimeOff)
        :this()
        {
            this.BehaviorType = behaviorType;
            this.isTimeOff = isTimeOff;
			if (isTimeOff.HasValue == true)
			{
				FillMotives();
				FillTypes();
			}
            this.SelectedVariation = variation;
	    }

        public FormBehavior BehaviorType
        {
            get
            {
                return behaviorType;
            }
            set
            {
                behaviorType = value;
            }
        }

        public WorkShiftVariationClientData SelectedVariation
        {
            get
            {
                return selectedVariation;
            }
            set
            {
                Clear();
                selectedVariation = value;
               
                if (selectedVariation != null)
                {
                    textBoxExName.Text = selectedVariation.Name;
                    textBoxExDes.Text = selectedVariation.Description;
                    foreach (MotiveVariationClientData motive in comboBoxExMotive.Properties.Items)
                    {
                        if (motive.Code == selectedVariation.MotiveCode)
                        {
                            comboBoxExMotive.SelectedItem = motive;
                            break;
                        }
                    }


                    comboBoxEditType.SelectedIndex = (int)selectedVariation.Type;

                    if (selectedVariation.Schedules == null)
                    {
                        IList list = (IList)ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetWorkShiftScheduleVariationsByWSVCode, selectedVariation.Code));
                        selectedVariation.Schedules = new List<WorkShiftScheduleVariationClientData>();
                        foreach (WorkShiftScheduleVariationClientData sc in list)
                        {
                            selectedVariation.Schedules.Add(sc);
                        }
                    }
                    if (selectedVariation.Schedules != null)
                    {
                        IList gridDatas = new ArrayList();
                        foreach (WorkShiftScheduleVariationClientData schedule in selectedVariation.Schedules)
                        {
                            VariationScheduleGridData gridData = new VariationScheduleGridData();
                            gridData.BeginTime = ((DateTime)schedule.Start);
                            gridData.EndTime = ((DateTime)schedule.End);
                            gridData.Date = ((DateTime)schedule.Start);
                            gridData.Code = schedule.Code;
                            gridData.Version = schedule.Version;
                            gridDatas.Add(gridData);
                        }
                        gridControlSelected.DataSource = gridDatas;
                    }
                }
                ExtraTimeParameters_Change(null, null);
            }
        }

        private void Clear()
        {
            gridControlSelected.DataSource = null;
            toDelete.Clear();
        }

        private void LoadLanguage() 
        {
            /****************  DataGrids **************/

            gridColumnScheduleDay.Caption = ResourceLoader.GetString2("Day");
            gridColumnScheduleDate.Caption = ResourceLoader.GetString2("VariationDate");
            gridColumnScheduleStart.Caption = ResourceLoader.GetString2("VariationStartTime");
            gridColumnScheduleEnd.Caption = ResourceLoader.GetString2("VariationEndTime");

            /******************* Labels **********************/
            this.layoutControlItemName.Text = ResourceLoader.GetString2("LabelName");
            this.layoutControlItemDesc.Text = ResourceLoader.GetString2("LabelDescription");
            this.layoutControlItemMotive.Text = ResourceLoader.GetString2("LabelMotive");
            this.layoutControlItemSelection.Text = ResourceLoader.GetString2("Intervals") + ": *";
            this.layoutControlItemType.Text = ResourceLoader.GetString2("Type") + ": *";

            /********************* Buttons *****************************/

            ButtonAdd.Text = ResourceLoader.GetString2("Add");
            ButtonDelete.Text = ResourceLoader.GetString2("Delete");
            ButtonEmpty.Text = ResourceLoader.GetString2("Clear");
            ButtonCancel.Text = ResourceLoader.GetString2("$Message.Cancel");

            /********************* CheckBox **************************/


            /******************** GroupBox ****************************/
            this.layoutControlGroupExtraTimeInfo.Text = ResourceLoader.GetString2("Data");
            if (isTimeOff != null && isTimeOff.Value)
                this.layoutControlGroupDates.Text = ResourceLoader.GetString2("VacationDetails");
            else
                this.layoutControlGroupDates.Text = ResourceLoader.GetString2("WorkShiftDetails");
        }

        private void FillMotives()
        {
            IList gridDatas = new ArrayList();
            IList motives = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetVariationMotives);
            if (motives != null)
            {
                foreach (MotiveVariationClientData motiv in motives)
                {
                    if (motiv.Type == !isTimeOff)
                        comboBoxExMotive.Properties.Items.Add(motiv);
                }
            }
        }

        private void FillTypes(){

            comboBoxEditType.Properties.Items.Add(ResourceLoader.GetString2("TypeWorkShift"));
            comboBoxEditType.Properties.Items.Add(ResourceLoader.GetString2("ExtraTime"));
        }

        /// <summary>
        /// Validate that remove schedules has assignings.
        /// </summary>
        /// <returns>True if its validate, otherwise false</returns>
        private bool ValidateScheduleVariations()
        {
            bool result = true;
            ArrayList codes = new ArrayList();

            //Filter those supervisors or operators remove.
            if (selectedVariation != null && selectedVariation.Operators != null)
            {
                foreach (WorkShiftScheduleVariationClientData clientData in selectedVariation.Schedules)
                {
                    bool found = false;
                    foreach (VariationScheduleGridData var in ((IList)gridControlSelected.DataSource))
                    {
                        if (var.Code == clientData.Code)
                            found = true;
                    }
                    if (found == false)
                        codes.Add(clientData.Code);
                }
            }

            if (codes.Count > 0)
            {
                StringBuilder builder = new StringBuilder();
                builder.Append("(");
                foreach (int code in codes)
                {
                    builder.Append(code);
                    builder.Append(",");
                }
                builder.Remove(builder.Length - 1, 1);
                builder.Append(")");

                string hql = "select count(*) from OperatorAssignData assign where assign.SupervisedScheduleVariation.Code in {0}";
                long count = (long)ServerServiceClient.GetInstance().SearchBasicObject(SmartCadHqls.GetCustomHql(hql, builder.ToString()));
                if (count > 0)
                {
                    DialogResult dialog = MessageForm.Show(ResourceLoader.GetString2("IntervalWithAssign"), MessageFormType.WarningQuestion);
                    if (dialog == DialogResult.No)
                    {
                        DialogResult = DialogResult.None;
                        SelectedVariation = selectedVariation;
                        result = false;
                    }
                }
            }
            return result;
        }
       
        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidateScheduleVariations() == true)
                {
                    BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[1] { GetType().GetMethod("buttonOk_Click1", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[0] });
                    processForm.CanThrowError = true;
                    processForm.ShowDialog();
                }
            }
            catch (FaultException ex)
            {
                if (behaviorType == FormBehavior.Edit)
                {
                    SelectedVariation = (WorkShiftVariationClientData)ServerServiceClient.GetInstance().SearchClientObject(SmartCadHqls.GetCustomHql(SmartCadHqls.GetWorkShiftVariationWithSchedulesByCode, selectedVariation.Code));;
                }
                DialogResult = DialogResult.None;
                if (ex.Message == ResourceLoader.GetString2("UK_WORK_SHIFT_VARIATION_NAME"))
                {
					if (isTimeOff.HasValue == true)
					{
						long result = (long)ServerServiceClient.GetInstance().SearchBasicObject(SmartCadHqls.GetCustomHql(SmartCadHqls.GetWorkShiftVariationWithEndedSchedulesOperators, selectedVariation.Name, ApplicationUtil.GetDataBaseFormattedDate(ServerServiceClient.GetInstance().GetTime())));
						if (result > 0)
							if (isTimeOff.Value == false)
								MessageForm.Show(ResourceLoader.GetString2("UK_WORK_SHIFT_VARIATION_NAME_ENDED", ResourceLoader.GetString2("WorkShift").ToLower()), MessageFormType.Error);
							else
								MessageForm.Show(ResourceLoader.GetString2("UK_WORK_SHIFT_VARIATION_NAME_ENDED", ResourceLoader.GetString2("FreeTimeFormGroupText").ToLower()), MessageFormType.Error);
						else
							if (isTimeOff.Value == false)
								MessageForm.Show(ResourceLoader.GetString2("UK_WORK_SHIFT_VARIATION_NAME", ResourceLoader.GetString2("WorkShift").ToLower()), MessageFormType.Error);
							else
								MessageForm.Show(ResourceLoader.GetString2("UK_WORK_SHIFT_VARIATION_NAME", ResourceLoader.GetString2("FreeTimeFormGroupText").ToLower()), MessageFormType.Error);
					}
					else
					{
						MessageForm.Show(ResourceLoader.GetString2("UK_WORK_SHIFT_VARIATION_NAME", ResourceLoader.GetString2("WorkShift").ToLower()), MessageFormType.Error);
					}
                }
                else
                    MessageForm.Show(ex.Message, MessageFormType.Error);
            }
            catch (Exception ex)
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, MessageFormType.Error);
            }
        }

        private void buttonOk_Click1()
        {
            if (behaviorType == FormBehavior.Create)
            {
                selectedVariation = new WorkShiftVariationClientData();
                selectedVariation.Schedules = new List<WorkShiftScheduleVariationClientData>();
                selectedVariation.Operators = new ArrayList();
            }
            if (selectedVariation != null)
            {
                FormUtil.InvokeRequired(this,
                    delegate
                    {
                        selectedVariation.Name = textBoxExName.Text;
                        selectedVariation.Description = textBoxExDes.Text;
                        if (comboBoxExMotive.SelectedIndex == -1)
                        {
                            foreach (MotiveVariationClientData motive in comboBoxExMotive.Properties.Items)
                            {
                                if (motive.Name == comboBoxExMotive.Text)
                                {
                                    comboBoxExMotive.SelectedItem = motive;
                                }
                            }
                        }
                        if (isTimeOff.HasValue)
                        {
                            if (isTimeOff.Value == true)
                            {
                                selectedVariation.Type = WorkShiftVariationClientData.WorkShiftType.TimeOff;

                                if (comboBoxExMotive.SelectedItem != null)
                                    selectedVariation.MotiveCode = ((MotiveVariationClientData)comboBoxExMotive.SelectedItem).Code;
                                else
                                    selectedVariation.MotiveCode = 0;
                            }
                            else
                            {
                                selectedVariation.Type = (WorkShiftVariationClientData.WorkShiftType)comboBoxEditType.SelectedIndex;

                                if (selectedVariation.Type == WorkShiftVariationClientData.WorkShiftType.ExtraTime)
                                    if (comboBoxExMotive.SelectedItem != null)
                                        selectedVariation.MotiveCode = ((MotiveVariationClientData)comboBoxExMotive.SelectedItem).Code;
                                    else
                                        selectedVariation.MotiveCode = 0;
                                else
                                    selectedVariation.MotiveCode = 0;
                            }
                        }
                        else
                        {
                            selectedVariation.Type = WorkShiftVariationClientData.WorkShiftType.WorkShift; ;
                            selectedVariation.ObjectType = WorkShiftVariationClientData.ObjectRelatedType.Units;
                        }

                        selectedVariation.Schedules.Clear();
                        if (gridControlSelected.DataSource != null && ((IList)gridControlSelected.DataSource).Count > 0)
                        {
                            foreach (VariationScheduleGridData var in ((IList)gridControlSelected.DataSource))
                            {
                                WorkShiftScheduleVariationClientData wsVar = new WorkShiftScheduleVariationClientData();
                                DateTime date = new DateTime(var.Date.Year, var.Date.Month, var.Date.Day);
                                wsVar.Start = date.Add(var.BeginTime.TimeOfDay);
                                //if (var.EndTime.TimeOfDay.Hours == 0 && var.EndTime.TimeOfDay.Minutes == 0)
                                //    wsVar.End = date.AddDays(1).AddSeconds(-1);
                                //else
                                wsVar.End = date.Add(var.EndTime.TimeOfDay);
                                wsVar.WorkShiftVariationCode = selectedVariation.Code;
                                wsVar.Code = var.Code;
                                wsVar.Version = var.Version;

                                selectedVariation.Schedules.Add(wsVar);
                            }
                        }
                    });
                if (selectedVariation.Type == WorkShiftVariationClientData.WorkShiftType.TimeOff)
                {
                    bool timeOff = false;
                    foreach (WorkShiftScheduleVariationClientData wssv in selectedVariation.Schedules)
                    {
                        if (wssv.End > DateTime.Now)
                        {
                            timeOff = true;
                            break;
                        }

                    }
                    if (timeOff == false)
                    {
                        MessageForm.Show(ResourceLoader.GetString2("ExtraTimeCreateForm.IntervalEndError"), MessageFormType.Error);
                        FormUtil.InvokeRequired(this,
                    delegate
                    {
                        this.Close();
                    });
                    }
                }
                ServerServiceClient.GetInstance().SaveOrUpdateClientData(selectedVariation);
            }
        }

        private void ExtraTimeParameters_Change(object sender, System.EventArgs e)
        {

            if (string.IsNullOrEmpty(textBoxExName.Text.Trim()) || gridControlSelected.DataSource == null || ((IList)gridControlSelected.DataSource).Count == 0 || (comboBoxEditType.SelectedIndex == -1 && isTimeOff == false))
                buttonOk.Enabled = false;
            else
            {
                if (comboBoxExMotive.Enabled == true && comboBoxExMotive.SelectedItem != null)
                    buttonOk.Enabled = true;
                else if (comboBoxExMotive.Enabled == false)
                    buttonOk.Enabled = true;
                else
                    buttonOk.Enabled = false;
            }
            if (gridControlSelected.DataSource == null || ((IList)gridControlSelected.DataSource).Count == 0)
            {
                ButtonEmpty.Enabled = false;
                ButtonDelete.Enabled = false;
            }
            else
            {
                ButtonEmpty.Enabled = true;
                ButtonDelete.Enabled = true;
            }
            
        }

        private void textBoxExName_TextChanged(object sender, EventArgs e)
        {
            ExtraTimeParameters_Change(sender, e);
        }

        private void comboBoxExMotive_SelectedIndexChanged(object sender, EventArgs e)
        {
            ExtraTimeParameters_Change(sender, e);
        }

        private void ButtonEmpty_Click(object sender, EventArgs e)
        {
            gridControlSelected.DataSource = null;
            ExtraTimeParameters_Change(sender, e);
        }

        private void ButtonAdd_Click(object sender, EventArgs e)
        {
            IList intervals = dateIntervalsControl1.GetDateIntervals();
            if (ButtonAdd.Tag != null && ((int)ButtonAdd.Tag) == 3)
            {
                MessageForm.Show(ResourceLoader.GetString2("ExtraTimeCreateForm.MinBeginTimeError"), MessageFormType.Error);
                dateIntervalsControl1.Focus();
            }
            else if (ButtonAdd.Tag != null && ((int)ButtonAdd.Tag) == 2)
            {
                MessageForm.Show(ResourceLoader.GetString2("ExtraTimeCreateForm.BeginTimeError"), MessageFormType.Error);
                dateIntervalsControl1.Focus();
            }
            else if (ButtonAdd.Tag != null && ((int)ButtonAdd.Tag) == 4)
            {
                MessageForm.Show(ResourceLoader.GetString2("ExtraTimeCreateForm.StartError"), MessageFormType.Error);
                dateIntervalsControl1.Focus();
            }
            else if (intervals.Count == 0)
            {
                MessageForm.Show(ResourceLoader.GetString2("ExtraTimeCreateForm.IntervalEmptyError"), MessageFormType.Error);
                dateIntervalsControl1.Focus();
            }
            else
            {

                if (gridControlSelected.DataSource == null)
                    gridControlSelected.DataSource = new ArrayList();
                IList schedules = (IList)gridControlSelected.DataSource;
                int count = schedules.Count;

                foreach (VariationScheduleGridData gridData in intervals)
                {
                    if (Overlap(gridData, schedules) == false)
                    {
                        DateTime date = new DateTime(gridData.Date.Year,
                                                     gridData.Date.Month,
                                                     gridData.Date.Day,
                                                     gridData.BeginTime.Hour,
                                                     gridData.BeginTime.Minute,
                                                     gridData.BeginTime.Second);

                        if (date >= DateTime.Now)
                            schedules.Add(gridData);

                    }
                }
                gridControlSelected.DataSource = new ArrayList();
                gridControlSelected.DataSource = schedules;
                dateIntervalsControl1.ClearSelection();
                //gridControlSelected.RefreshDataSource();
                if (count == schedules.Count)
                {
                    MessageForm.Show(ResourceLoader.GetString2("ExtraTimeCreateForm.IntervalEqualsError"), MessageFormType.Error);
                    dateIntervalsControl1.Focus();
                }


                ExtraTimeParameters_Change(null, null);
            }
        }

        private bool Overlap(VariationScheduleGridData gridData, IList schedules)
        {
            DateTime start = gridData.BeginTime;
            DateTime end = gridData.EndTime;
            if (schedules != null)
            {
                foreach (VariationScheduleGridData schedule in schedules)
                {
                    if (start.Date == schedule.Date.Date &&
                            !((end <= schedule.BeginTime && !(end.Hour == 0 && end.Minute == 0)) || (start >= schedule.EndTime && !(schedule.EndTime.Hour == 0 && schedule.EndTime.Minute == 0)))
                       )
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private void ButtonDelete_Click(object sender, EventArgs e)
        {
            if (gridViewSelection.GetFocusedRow() != null)
            {
                ((IList)gridControlSelected.DataSource).Remove(gridViewSelection.GetFocusedRow() as VariationScheduleGridData);
                gridViewSelection.RefreshData();
            }
            ExtraTimeParameters_Change(null, null);
        }

        private void dateIntervalsControl1_Parameters_Change(object sender, int result)
        {
            ButtonAdd.Tag = (int)result;
            if ((int)result == 1)
            {
                ButtonAdd.Enabled = false;
            }
            else
            {
                ButtonAdd.Enabled = true;
            }
        }

        private void gridView_DragObjectOver(object sender, DevExpress.XtraGrid.Views.Base.DragObjectOverEventArgs e)
        {
            GridColumn column = e.DragObject as GridColumn;
            if (column != null && e.DropInfo.Index < 0)
            {
                e.DropInfo.Valid = false;
            }

        }

        private void comboBoxEditType_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBoxExMotive.Enabled = (comboBoxEditType.SelectedIndex == 1);
            ExtraTimeParameters_Change(null, null);
        }

        private void ExtraTimeCreateForm_Load(object sender, EventArgs e)
        {
			if (isTimeOff.HasValue == true)
			{
				if (isTimeOff.Value == false)
				{
					switch (behaviorType)
					{
						case FormBehavior.Create:
							Text = ResourceLoader.GetString2("WorkShiftCreateFormText");
							buttonOk.Text = ResourceLoader.GetString2("VariationCreateButtonOkText");
							break;
						case FormBehavior.Edit:
							Text = ResourceLoader.GetString2("WorkShiftCreateFormEditText");
							buttonOk.Text = ResourceLoader.GetString2("VariationEditButtonOkText");
							break;
						default:
							break;

					}
				}
				else
				{
					switch (behaviorType)
					{
						case FormBehavior.Create:
							Text = ResourceLoader.GetString2("VacationCreateFormText");
							buttonOk.Text = ResourceLoader.GetString2("VariationCreateButtonOkText");
							break;
						case FormBehavior.Edit:
							Text = ResourceLoader.GetString2("VacationCreateFormEditText");
							buttonOk.Text = ResourceLoader.GetString2("VariationEditButtonOkText");
							break;
						default:
							break;
					}

					this.Size = new Size(951, 515);
					this.MinimumSize = new Size(951, 515);
					layoutControlItemType.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
					comboBoxExMotive.Enabled = true;

				}
			}
			else
			{
                switch (behaviorType)
                {
                    case FormBehavior.Create:
                        Text = ResourceLoader.GetString2("WorkShiftAdminCreateFormText");
                        buttonOk.Text = ResourceLoader.GetString2("VariationCreateButtonOkText");
                        break;
                    case FormBehavior.Edit:
                        Text = ResourceLoader.GetString2("WorkShiftAdminCreateFormEditText");
                        buttonOk.Text = ResourceLoader.GetString2("VariationEditButtonOkText");
                        break;
                    default:
                        break;

                }
				layoutControlItemMotive.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
				layoutControlItemType.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
			}
            LoadLanguage();
        }
    }    
}
