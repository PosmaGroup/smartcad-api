﻿namespace SmartCadGuiCommon
{
    partial class IndicatorOperatorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IndicatorOperatorForm));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.groupControlIndicator = new DevExpress.XtraEditors.GroupControl();
            this.gridControlIndicatorOperator = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnOperator = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnThreshold = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemPictureEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.gridColumnTrend = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlIndicator)).BeginInit();
            this.groupControlIndicator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlIndicatorOperator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.groupControlIndicator);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(491, 567);
            this.panelControl1.TabIndex = 1;
            // 
            // groupControlIndicator
            // 
            this.groupControlIndicator.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.groupControlIndicator.Controls.Add(this.gridControlIndicatorOperator);
            this.groupControlIndicator.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControlIndicator.Location = new System.Drawing.Point(2, 2);
            this.groupControlIndicator.Name = "groupControlIndicator";
            this.groupControlIndicator.Size = new System.Drawing.Size(487, 563);
            this.groupControlIndicator.TabIndex = 2;
            this.groupControlIndicator.Text = "groupControl";
            // 
            // gridControlIndicatorOperator
            // 
            this.gridControlIndicatorOperator.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlIndicatorOperator.Location = new System.Drawing.Point(2, 20);
            this.gridControlIndicatorOperator.MainView = this.gridView1;
            this.gridControlIndicatorOperator.Name = "gridControlIndicatorOperator";
            this.gridControlIndicatorOperator.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPictureEdit1});
            this.gridControlIndicatorOperator.Size = new System.Drawing.Size(483, 541);
            this.gridControlIndicatorOperator.TabIndex = 0;
            this.gridControlIndicatorOperator.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnOperator,
            this.gridColumnValue,
            this.gridColumnThreshold,
            this.gridColumnTrend});
            this.gridView1.GridControl = this.gridControlIndicatorOperator;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsCustomization.AllowFilter = false;
            this.gridView1.OptionsCustomization.AllowGroup = false;
            this.gridView1.OptionsMenu.EnableColumnMenu = false;
            this.gridView1.OptionsMenu.EnableFooterMenu = false;
            this.gridView1.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowDetailButtons = false;
            this.gridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            // 
            // gridColumnOperator
            // 
            this.gridColumnOperator.Caption = "gridColumn1";
            this.gridColumnOperator.FieldName = "Operator";
            this.gridColumnOperator.Name = "gridColumnOperator";
            this.gridColumnOperator.OptionsColumn.AllowEdit = false;
            this.gridColumnOperator.Visible = true;
            this.gridColumnOperator.VisibleIndex = 0;
            this.gridColumnOperator.Width = 330;
            // 
            // gridColumnValue
            // 
            this.gridColumnValue.Caption = "gridColumn2";
            this.gridColumnValue.FieldName = "Value";
            this.gridColumnValue.Name = "gridColumnValue";
            this.gridColumnValue.OptionsColumn.AllowEdit = false;
            this.gridColumnValue.Visible = true;
            this.gridColumnValue.VisibleIndex = 1;
            this.gridColumnValue.Width = 271;
            // 
            // gridColumnThreshold
            // 
            this.gridColumnThreshold.Caption = "gridColumn3";
            this.gridColumnThreshold.ColumnEdit = this.repositoryItemPictureEdit1;
            this.gridColumnThreshold.FieldName = "Threshold";
            this.gridColumnThreshold.Name = "gridColumnThreshold";
            this.gridColumnThreshold.OptionsColumn.AllowEdit = false;
            this.gridColumnThreshold.Visible = true;
            this.gridColumnThreshold.VisibleIndex = 2;
            this.gridColumnThreshold.Width = 357;
            // 
            // repositoryItemPictureEdit1
            // 
            this.repositoryItemPictureEdit1.Name = "repositoryItemPictureEdit1";
            // 
            // gridColumnTrend
            // 
            this.gridColumnTrend.Caption = "gridColumn4";
            this.gridColumnTrend.ColumnEdit = this.repositoryItemPictureEdit1;
            this.gridColumnTrend.FieldName = "Trend";
            this.gridColumnTrend.Name = "gridColumnTrend";
            this.gridColumnTrend.OptionsColumn.AllowEdit = false;
            this.gridColumnTrend.Visible = true;
            this.gridColumnTrend.VisibleIndex = 3;
            this.gridColumnTrend.Width = 365;
            // 
            // IndicatorOperatorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(491, 567);
            this.Controls.Add(this.panelControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "IndicatorOperatorForm";
            this.Text = "IndicatorOperatorForm";
            this.Load += new System.EventHandler(this.IndicatorOperatorForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControlIndicator)).EndInit();
            this.groupControlIndicator.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlIndicatorOperator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.GroupControl groupControlIndicator;
        private DevExpress.XtraGrid.GridControl gridControlIndicatorOperator;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnOperator;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnValue;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnThreshold;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnTrend;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit1;
    }
}