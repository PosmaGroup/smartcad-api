namespace SmartCadGuiCommon
{
    partial class AppointmentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButtonOk = new DevExpress.XtraEditors.SimpleButton();
            this.textBoxDescription = new DevExpress.XtraEditors.MemoEdit();
            this.labelDescription = new DevExpress.XtraEditors.LabelControl();
            this.textType = new DevExpress.XtraEditors.LabelControl();
            this.labelImage = new DevExpress.XtraEditors.LabelControl();
            this.textTimeEnd = new DevExpress.XtraEditors.LabelControl();
            this.textDateEnd = new DevExpress.XtraEditors.LabelControl();
            this.labelDateEnd = new DevExpress.XtraEditors.LabelControl();
            this.textTimeStart = new DevExpress.XtraEditors.LabelControl();
            this.textDateStart = new DevExpress.XtraEditors.LabelControl();
            this.labelDateStart = new DevExpress.XtraEditors.LabelControl();
            this.textName = new DevExpress.XtraEditors.LabelControl();
            this.labelName = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxDescription.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.simpleButtonOk);
            this.panelControl1.Controls.Add(this.textBoxDescription);
            this.panelControl1.Controls.Add(this.labelDescription);
            this.panelControl1.Controls.Add(this.textType);
            this.panelControl1.Controls.Add(this.labelImage);
            this.panelControl1.Controls.Add(this.textTimeEnd);
            this.panelControl1.Controls.Add(this.textDateEnd);
            this.panelControl1.Controls.Add(this.labelDateEnd);
            this.panelControl1.Controls.Add(this.textTimeStart);
            this.panelControl1.Controls.Add(this.textDateStart);
            this.panelControl1.Controls.Add(this.labelDateStart);
            this.panelControl1.Controls.Add(this.textName);
            this.panelControl1.Controls.Add(this.labelName);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(267, 300);
            this.panelControl1.TabIndex = 3;
            // 
            // simpleButtonOk
            // 
            this.simpleButtonOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButtonOk.Location = new System.Drawing.Point(185, 272);
            this.simpleButtonOk.Name = "simpleButtonOk";
            this.simpleButtonOk.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonOk.TabIndex = 13;
            this.simpleButtonOk.Text = "Accept";
            // 
            // textBoxDescription
            // 
            this.textBoxDescription.Enabled = false;
            this.textBoxDescription.Location = new System.Drawing.Point(5, 144);
            this.textBoxDescription.Name = "textBoxDescription";
            this.textBoxDescription.Size = new System.Drawing.Size(255, 119);
            this.textBoxDescription.TabIndex = 11;
            // 
            // labelDescription
            // 
            this.labelDescription.Location = new System.Drawing.Point(12, 125);
            this.labelDescription.Name = "labelDescription";
            this.labelDescription.Size = new System.Drawing.Size(61, 13);
            this.labelDescription.TabIndex = 10;
            this.labelDescription.Text = "Descripcion :";
            // 
            // textType
            // 
            this.textType.AllowHtmlString = true;
            this.textType.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.textType.Location = new System.Drawing.Point(86, 40);
            this.textType.Name = "textType";
            this.textType.Size = new System.Drawing.Size(69, 14);
            this.textType.TabIndex = 9;
            this.textType.Text = "labelControl10";
            // 
            // labelImage
            // 
            this.labelImage.Location = new System.Drawing.Point(12, 40);
            this.labelImage.Name = "labelImage";
            this.labelImage.Size = new System.Drawing.Size(27, 13);
            this.labelImage.TabIndex = 8;
            this.labelImage.Text = "Tipo :";
            // 
            // textTimeEnd
            // 
            this.textTimeEnd.Location = new System.Drawing.Point(155, 97);
            this.textTimeEnd.Name = "textTimeEnd";
            this.textTimeEnd.Size = new System.Drawing.Size(63, 13);
            this.textTimeEnd.TabIndex = 7;
            this.textTimeEnd.Text = "labelControl8";
            // 
            // textDateEnd
            // 
            this.textDateEnd.Location = new System.Drawing.Point(86, 97);
            this.textDateEnd.Name = "textDateEnd";
            this.textDateEnd.Size = new System.Drawing.Size(63, 13);
            this.textDateEnd.TabIndex = 6;
            this.textDateEnd.Text = "labelControl7";
            // 
            // labelDateEnd
            // 
            this.labelDateEnd.Location = new System.Drawing.Point(12, 97);
            this.labelDateEnd.Name = "labelDateEnd";
            this.labelDateEnd.Size = new System.Drawing.Size(21, 13);
            this.labelDateEnd.TabIndex = 5;
            this.labelDateEnd.Text = "Fin :";
            // 
            // textTimeStart
            // 
            this.textTimeStart.Location = new System.Drawing.Point(155, 68);
            this.textTimeStart.Name = "textTimeStart";
            this.textTimeStart.Size = new System.Drawing.Size(63, 13);
            this.textTimeStart.TabIndex = 4;
            this.textTimeStart.Text = "labelControl5";
            // 
            // textDateStart
            // 
            this.textDateStart.Location = new System.Drawing.Point(86, 68);
            this.textDateStart.Name = "textDateStart";
            this.textDateStart.Size = new System.Drawing.Size(63, 13);
            this.textDateStart.TabIndex = 3;
            this.textDateStart.Text = "labelControl4";
            // 
            // labelDateStart
            // 
            this.labelDateStart.Location = new System.Drawing.Point(12, 68);
            this.labelDateStart.Name = "labelDateStart";
            this.labelDateStart.Size = new System.Drawing.Size(53, 13);
            this.labelDateStart.TabIndex = 2;
            this.labelDateStart.Text = "Comienzo :";
            // 
            // textName
            // 
            this.textName.Location = new System.Drawing.Point(86, 12);
            this.textName.Name = "textName";
            this.textName.Size = new System.Drawing.Size(63, 13);
            this.textName.TabIndex = 1;
            this.textName.Text = "labelControl2";
            // 
            // labelName
            // 
            this.labelName.Location = new System.Drawing.Point(12, 12);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(44, 13);
            this.labelName.TabIndex = 0;
            this.labelName.Text = "Nombre :";
            // 
            // AppointmentForm
            // 
            this.AcceptButton = this.simpleButtonOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(267, 300);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AppointmentForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.AppointmentForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxDescription.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl textTimeEnd;
        private DevExpress.XtraEditors.LabelControl textDateEnd;
        private DevExpress.XtraEditors.LabelControl labelDateEnd;
        private DevExpress.XtraEditors.LabelControl textTimeStart;
        private DevExpress.XtraEditors.LabelControl textDateStart;
        private DevExpress.XtraEditors.LabelControl labelDateStart;
        private DevExpress.XtraEditors.LabelControl textName;
        private DevExpress.XtraEditors.LabelControl labelName;
        private DevExpress.XtraEditors.LabelControl labelImage;
        private DevExpress.XtraEditors.MemoEdit textBoxDescription;
        private DevExpress.XtraEditors.LabelControl labelDescription;
        private DevExpress.XtraEditors.LabelControl textType;
        private DevExpress.XtraEditors.SimpleButton simpleButtonOk;

    }
}