using SmartCadControls;
namespace SmartCadGuiCommon
{
    partial class TrainingCourseHistoryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TrainingCourseHistoryForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            DatagGridDefaultGroup datagGridDefaultGroup1 = new DatagGridDefaultGroup();
            this.toolStripMain = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonEval = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonSearch = new System.Windows.Forms.ToolStripButton();
            this.toolStripTextBoxSearch = new System.Windows.Forms.ToolStripTextBox();
            this.dataGridExCourses = new DataGridEx();
            this.toolStripMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridExCourses)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStripMain
            // 
            this.toolStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonEval,
            this.toolStripButtonSearch,
            this.toolStripTextBoxSearch});
            this.toolStripMain.Location = new System.Drawing.Point(0, 0);
            this.toolStripMain.Name = "toolStripMain";
            this.toolStripMain.Size = new System.Drawing.Size(439, 25);
            this.toolStripMain.TabIndex = 0;
            this.toolStripMain.Text = "toolStrip1";
            // 
            // toolStripButtonEval
            // 
            this.toolStripButtonEval.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonEval.Enabled = false;
            this.toolStripButtonEval.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonEval.Image")));
            this.toolStripButtonEval.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonEval.Name = "toolStripButtonEval";
            this.toolStripButtonEval.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonEval.Text = "toolStripButtonEval";
            this.toolStripButtonEval.ToolTipText = "Ver evaluacion";
            this.toolStripButtonEval.Click += new System.EventHandler(this.toolStripButtonEval_Click);
            // 
            // toolStripButtonSearch
            // 
            this.toolStripButtonSearch.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonSearch.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonSearch.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonSearch.Image")));
            this.toolStripButtonSearch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonSearch.Name = "toolStripButtonSearch";
            this.toolStripButtonSearch.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonSearch.Click += new System.EventHandler(this.toolStripButtonSearch_Click);
            // 
            // toolStripTextBoxSearch
            // 
            this.toolStripTextBoxSearch.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripTextBoxSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.toolStripTextBoxSearch.Name = "toolStripTextBoxSearch";
            this.toolStripTextBoxSearch.Size = new System.Drawing.Size(100, 25);
            // 
            // dataGridExCourses
            // 
            this.dataGridExCourses.AllowDrop = true;
            this.dataGridExCourses.AllowEditing = false;
            this.dataGridExCourses.AllowUserToAddRows = false;
            this.dataGridExCourses.AllowUserToDeleteRows = false;
            this.dataGridExCourses.AllowUserToOrderColumns = true;
            this.dataGridExCourses.AllowUserToResizeRows = false;
            this.dataGridExCourses.AllowUserToSortColumns = true;
            this.dataGridExCourses.BackgroundColor = System.Drawing.Color.White;
            this.dataGridExCourses.ColumnHeadersBackColor = System.Drawing.SystemColors.ButtonFace;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ButtonFace;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridExCourses.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridExCourses.ColumnHeadersHeight = 22;
            this.dataGridExCourses.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridExCourses.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridExCourses.Editing = false;
            this.dataGridExCourses.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridExCourses.EnabledSelectionHandler = true;
            this.dataGridExCourses.EnableHeadersVisualStyles = false;
            this.dataGridExCourses.EnableSystemShortCuts = false;
            this.dataGridExCourses.Grouping = false;
            datagGridDefaultGroup1.Collapsed = false;
            datagGridDefaultGroup1.Column = null;
            datagGridDefaultGroup1.Height = 34;
            datagGridDefaultGroup1.ItemCount = 0;
            datagGridDefaultGroup1.Text = "";
            datagGridDefaultGroup1.Value = null;
            this.dataGridExCourses.GroupTemplate = datagGridDefaultGroup1;
            this.dataGridExCourses.Location = new System.Drawing.Point(0, 25);
            this.dataGridExCourses.MultiSelect = false;
            this.dataGridExCourses.Name = "dataGridExCourses";
            this.dataGridExCourses.SelectedItemBackColor = System.Drawing.Color.Empty;
            this.dataGridExCourses.SelectedItemForeColor = System.Drawing.Color.Empty;
            this.dataGridExCourses.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridExCourses.Size = new System.Drawing.Size(439, 255);
            this.dataGridExCourses.SortedColumnColor = System.Drawing.Color.Empty;
            this.dataGridExCourses.TabIndex = 1;
            this.dataGridExCourses.Type = null;
            this.dataGridExCourses.VirtualMode = true;
            this.dataGridExCourses.SelectionWillChange += new System.EventHandler(this.dataGridExCourses_SelectionWillChange);
            this.dataGridExCourses.DoubleClick += new System.EventHandler(this.dataGridExCourses_DoubleClick);
            // 
            // TrainingCourseHistoryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(439, 280);
            this.Controls.Add(this.dataGridExCourses);
            this.Controls.Add(this.toolStripMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TrainingCourseHistoryForm";
            this.Text = "Historico de cursos de entrenamiento";
            this.toolStripMain.ResumeLayout(false);
            this.toolStripMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridExCourses)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStripMain;
        private System.Windows.Forms.ToolStripButton toolStripButtonEval;
        private System.Windows.Forms.ToolStripButton toolStripButtonSearch;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBoxSearch;
        private DataGridEx dataGridExCourses;
    }
}