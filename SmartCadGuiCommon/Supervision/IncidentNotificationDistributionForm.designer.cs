using SmartCadControls;
using SmartCadControls.Controls;
namespace SmartCadGuiCommon
{
    partial class IncidentNotificationDistributionForm
    {

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IncidentNotificationDistributionForm));
            this.contextMenuStripNotficationsToDistribute = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.asignarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.simpleButtonAssign = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.gridControlExNotificationDistribution = new GridControlEx();
            this.gridViewNotificationDistribution = new GridViewEx();
            this.gridControlExNotificationInProgress = new GridControlEx();
            this.contextMenuStripNotificationsInProgress = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.quitarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gridViewNotificationInProgress = new GridViewEx();
            this.simpleButtonRemove = new DevExpress.XtraEditors.SimpleButton();
            this.gridControlExOperators = new GridControlEx();
            this.gridViewOperators = new GridViewEx();
            this.webBrowserExIncidentDetailsLeft = new SearchableWebBrowser();
            this.webBrowserExIncidentDetailsRight = new SearchableWebBrowser();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupIncidentDetailsRight = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupIncidentDetailsLeft = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.simpleSeparator1 = new DevExpress.XtraLayout.SimpleSeparator();
            this.splitterItem2 = new DevExpress.XtraLayout.SplitterItem();
            this.layoutControlGroupNotifications = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupOperators = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupNotificationsInProcess = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.toolTipGeneral = new System.Windows.Forms.ToolTip(this.components);
            this.printDialog = new System.Windows.Forms.PrintDialog();
            this.RibbonControl = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.barButtonItemSave = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemUpdate = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSend = new DevExpress.XtraBars.BarButtonItem();
            this.RibbonPageOperation = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPage();
            this.ribbonPageGroupOptions = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.contextMenuStripNotficationsToDistribute.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExNotificationDistribution)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewNotificationDistribution)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExNotificationInProgress)).BeginInit();
            this.contextMenuStripNotificationsInProgress.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewNotificationInProgress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExOperators)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewOperators)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupIncidentDetailsRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupIncidentDetailsLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupNotifications)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupOperators)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupNotificationsInProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RibbonControl)).BeginInit();
            this.SuspendLayout();
            // 
            // contextMenuStripNotficationsToDistribute
            // 
            this.contextMenuStripNotficationsToDistribute.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.asignarToolStripMenuItem});
            this.contextMenuStripNotficationsToDistribute.Name = "contextMenuStripNotficationsToDistribute";
            this.contextMenuStripNotficationsToDistribute.Size = new System.Drawing.Size(122, 26);
            this.contextMenuStripNotficationsToDistribute.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStripNotficationsToDistribute_Opening);
            // 
            // asignarToolStripMenuItem
            // 
            this.asignarToolStripMenuItem.Name = "asignarToolStripMenuItem";
            this.asignarToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.asignarToolStripMenuItem.Text = "Asignar";
            this.asignarToolStripMenuItem.Click += new System.EventHandler(this.AssignIncidentNotificationToOperator_Handler);
            // 
            // simpleButtonAssign
            // 
            this.simpleButtonAssign.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonAssign.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.simpleButtonAssign.Appearance.Options.UseFont = true;
            this.simpleButtonAssign.Enabled = false;
            this.simpleButtonAssign.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonAssign.Image")));
            this.simpleButtonAssign.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.simpleButtonAssign.Location = new System.Drawing.Point(532, 349);
            this.simpleButtonAssign.Margin = new System.Windows.Forms.Padding(2);
            this.simpleButtonAssign.Name = "simpleButtonAssign";
            this.simpleButtonAssign.Size = new System.Drawing.Size(81, 31);
            this.simpleButtonAssign.StyleController = this.layoutControl1;
            this.simpleButtonAssign.TabIndex = 10;
            this.simpleButtonAssign.Text = "Asignar";
            this.simpleButtonAssign.Click += new System.EventHandler(this.AssignIncidentNotificationToOperator_Handler);
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowCustomizationMenu = false;
            this.layoutControl1.AutoScroll = false;
            this.layoutControl1.Controls.Add(this.simpleButtonAssign);
            this.layoutControl1.Controls.Add(this.gridControlExNotificationDistribution);
            this.layoutControl1.Controls.Add(this.gridControlExNotificationInProgress);
            this.layoutControl1.Controls.Add(this.simpleButtonRemove);
            this.layoutControl1.Controls.Add(this.gridControlExOperators);
            this.layoutControl1.Controls.Add(this.webBrowserExIncidentDetailsLeft);
            this.layoutControl1.Controls.Add(this.webBrowserExIncidentDetailsRight);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Padding = new System.Windows.Forms.Padding(1);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1265, 787);
            this.layoutControl1.TabIndex = 9;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // gridControlExNotificationDistribution
            // 
            this.gridControlExNotificationDistribution.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControlExNotificationDistribution.ContextMenuStrip = this.contextMenuStripNotficationsToDistribute;
            this.gridControlExNotificationDistribution.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(2);
            this.gridControlExNotificationDistribution.EnableAutoFilter = false;
            this.gridControlExNotificationDistribution.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExNotificationDistribution.Location = new System.Drawing.Point(10, 30);
            this.gridControlExNotificationDistribution.MainView = this.gridViewNotificationDistribution;
            this.gridControlExNotificationDistribution.Margin = new System.Windows.Forms.Padding(2);
            this.gridControlExNotificationDistribution.MaximumSize = new System.Drawing.Size(605, 0);
            this.gridControlExNotificationDistribution.MinimumSize = new System.Drawing.Size(605, 200);
            this.gridControlExNotificationDistribution.Name = "gridControlExNotificationDistribution";
            this.gridControlExNotificationDistribution.Size = new System.Drawing.Size(605, 315);
            this.gridControlExNotificationDistribution.TabIndex = 9;
            this.gridControlExNotificationDistribution.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewNotificationDistribution});
            this.gridControlExNotificationDistribution.ViewTotalRows = false;
            // 
            // gridViewNotificationDistribution
            // 
            this.gridViewNotificationDistribution.AllowFocusedRowChanged = true;
            this.gridViewNotificationDistribution.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewNotificationDistribution.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewNotificationDistribution.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewNotificationDistribution.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewNotificationDistribution.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewNotificationDistribution.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewNotificationDistribution.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewNotificationDistribution.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewNotificationDistribution.EnablePreviewLineForFocusedRow = false;
            this.gridViewNotificationDistribution.GridControl = this.gridControlExNotificationDistribution;
            this.gridViewNotificationDistribution.Name = "gridViewNotificationDistribution";
            this.gridViewNotificationDistribution.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewNotificationDistribution.OptionsView.ColumnAutoWidth = false;
            this.gridViewNotificationDistribution.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewNotificationDistribution.ViewTotalRows = false;
            this.gridViewNotificationDistribution.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridViewNotificationDistribution_MouseDown);
            // 
            // gridControlExNotificationInProgress
            // 
            this.gridControlExNotificationInProgress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControlExNotificationInProgress.ContextMenuStrip = this.contextMenuStripNotificationsInProgress;
            this.gridControlExNotificationInProgress.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(2);
            this.gridControlExNotificationInProgress.EnableAutoFilter = false;
            this.gridControlExNotificationInProgress.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExNotificationInProgress.Location = new System.Drawing.Point(629, 308);
            this.gridControlExNotificationInProgress.MainView = this.gridViewNotificationInProgress;
            this.gridControlExNotificationInProgress.Margin = new System.Windows.Forms.Padding(2);
            this.gridControlExNotificationInProgress.MinimumSize = new System.Drawing.Size(616, 200);
            this.gridControlExNotificationInProgress.Name = "gridControlExNotificationInProgress";
            this.gridControlExNotificationInProgress.Size = new System.Drawing.Size(626, 219);
            this.gridControlExNotificationInProgress.TabIndex = 13;
            this.gridControlExNotificationInProgress.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewNotificationInProgress});
            this.gridControlExNotificationInProgress.ViewTotalRows = false;
            // 
            // contextMenuStripNotificationsInProgress
            // 
            this.contextMenuStripNotificationsInProgress.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.quitarToolStripMenuItem});
            this.contextMenuStripNotificationsInProgress.Name = "contextMenuStripNotificationsInProgress";
            this.contextMenuStripNotificationsInProgress.Size = new System.Drawing.Size(116, 26);
            this.contextMenuStripNotificationsInProgress.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStripNotificationsInProgress_Opening);
            // 
            // quitarToolStripMenuItem
            // 
            this.quitarToolStripMenuItem.Name = "quitarToolStripMenuItem";
            this.quitarToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.quitarToolStripMenuItem.Text = "Quitar";
            this.quitarToolStripMenuItem.Click += new System.EventHandler(this.AssignIncidentNotificationToManualSupervisor);
            // 
            // gridViewNotificationInProgress
            // 
            this.gridViewNotificationInProgress.AllowFocusedRowChanged = true;
            this.gridViewNotificationInProgress.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewNotificationInProgress.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewNotificationInProgress.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewNotificationInProgress.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewNotificationInProgress.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewNotificationInProgress.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewNotificationInProgress.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewNotificationInProgress.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewNotificationInProgress.EnablePreviewLineForFocusedRow = false;
            this.gridViewNotificationInProgress.GridControl = this.gridControlExNotificationInProgress;
            this.gridViewNotificationInProgress.Name = "gridViewNotificationInProgress";
            this.gridViewNotificationInProgress.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewNotificationInProgress.OptionsView.ColumnAutoWidth = false;
            this.gridViewNotificationInProgress.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewNotificationInProgress.ViewTotalRows = false;
            this.gridViewNotificationInProgress.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridViewNotificationInProgress_MouseDown);
            // 
            // simpleButtonRemove
            // 
            this.simpleButtonRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonRemove.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.simpleButtonRemove.Appearance.Options.UseFont = true;
            this.simpleButtonRemove.Enabled = false;
            this.simpleButtonRemove.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonRemove.Image")));
            this.simpleButtonRemove.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.simpleButtonRemove.Location = new System.Drawing.Point(1174, 531);
            this.simpleButtonRemove.Margin = new System.Windows.Forms.Padding(2);
            this.simpleButtonRemove.Name = "simpleButtonRemove";
            this.simpleButtonRemove.Size = new System.Drawing.Size(81, 31);
            this.simpleButtonRemove.StyleController = this.layoutControl1;
            this.simpleButtonRemove.TabIndex = 15;
            this.simpleButtonRemove.Text = "Remover";
            this.simpleButtonRemove.Click += new System.EventHandler(this.AssignIncidentNotificationToManualSupervisor);
            // 
            // gridControlExOperators
            // 
            this.gridControlExOperators.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControlExOperators.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(2);
            this.gridControlExOperators.EnableAutoFilter = false;
            this.gridControlExOperators.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExOperators.Location = new System.Drawing.Point(629, 30);
            this.gridControlExOperators.MainView = this.gridViewOperators;
            this.gridControlExOperators.Margin = new System.Windows.Forms.Padding(2);
            this.gridControlExOperators.Name = "gridControlExOperators";
            this.gridControlExOperators.Size = new System.Drawing.Size(626, 244);
            this.gridControlExOperators.TabIndex = 12;
            this.gridControlExOperators.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewOperators});
            this.gridControlExOperators.ViewTotalRows = false;
            // 
            // gridViewOperators
            // 
            this.gridViewOperators.AllowFocusedRowChanged = true;
            this.gridViewOperators.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewOperators.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewOperators.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewOperators.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewOperators.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewOperators.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewOperators.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewOperators.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewOperators.EnablePreviewLineForFocusedRow = false;
            this.gridViewOperators.GridControl = this.gridControlExOperators;
            this.gridViewOperators.Name = "gridViewOperators";
            this.gridViewOperators.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewOperators.OptionsView.ColumnAutoWidth = false;
            this.gridViewOperators.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewOperators.ViewTotalRows = false;
            // 
            // webBrowserExIncidentDetailsLeft
            // 
            this.webBrowserExIncidentDetailsLeft.IsWebBrowserContextMenuEnabled = false;
            this.webBrowserExIncidentDetailsLeft.Location = new System.Drawing.Point(10, 419);
            this.webBrowserExIncidentDetailsLeft.Margin = new System.Windows.Forms.Padding(2);
            this.webBrowserExIncidentDetailsLeft.MinimumSize = new System.Drawing.Size(15, 16);
            this.webBrowserExIncidentDetailsLeft.Name = "webBrowserExIncidentDetailsLeft";
            this.webBrowserExIncidentDetailsLeft.Size = new System.Drawing.Size(603, 358);
            this.webBrowserExIncidentDetailsLeft.TabIndex = 7;
            this.webBrowserExIncidentDetailsLeft.XmlText = "<HTML></HTML>\0";
            // 
            // webBrowserExIncidentDetailsRight
            // 
            this.webBrowserExIncidentDetailsRight.IsWebBrowserContextMenuEnabled = false;
            this.webBrowserExIncidentDetailsRight.Location = new System.Drawing.Point(625, 597);
            this.webBrowserExIncidentDetailsRight.Margin = new System.Windows.Forms.Padding(2);
            this.webBrowserExIncidentDetailsRight.MinimumSize = new System.Drawing.Size(15, 16);
            this.webBrowserExIncidentDetailsRight.Name = "webBrowserExIncidentDetailsRight";
            this.webBrowserExIncidentDetailsRight.Size = new System.Drawing.Size(634, 184);
            this.webBrowserExIncidentDetailsRight.TabIndex = 0;
            this.webBrowserExIncidentDetailsRight.XmlText = "<HTML></HTML>\0";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupIncidentDetailsRight,
            this.layoutControlGroupIncidentDetailsLeft,
            this.splitterItem1,
            this.simpleSeparator1,
            this.splitterItem2,
            this.layoutControlGroupNotifications,
            this.layoutControlGroupOperators,
            this.layoutControlGroupNotificationsInProcess});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(1265, 787);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroupIncidentDetailsRight
            // 
            this.layoutControlGroupIncidentDetailsRight.AppearanceGroup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.layoutControlGroupIncidentDetailsRight.AppearanceGroup.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.layoutControlGroupIncidentDetailsRight.AppearanceGroup.Options.UseBackColor = true;
            this.layoutControlGroupIncidentDetailsRight.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroupIncidentDetailsRight.ExpandButtonMode = DevExpress.Utils.Controls.ExpandButtonMode.Inverted;
            this.layoutControlGroupIncidentDetailsRight.ExpandButtonVisible = true;
            this.layoutControlGroupIncidentDetailsRight.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6});
            this.layoutControlGroupIncidentDetailsRight.Location = new System.Drawing.Point(619, 571);
            this.layoutControlGroupIncidentDetailsRight.Name = "layoutControlGroupIncidentDetailsRight";
            this.layoutControlGroupIncidentDetailsRight.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupIncidentDetailsRight.Size = new System.Drawing.Size(640, 210);
            this.layoutControlGroupIncidentDetailsRight.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroupIncidentDetailsRight.Text = "layoutControlGroupIncidentDetailsRight";
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.webBrowserExIncidentDetailsRight;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem6.Size = new System.Drawing.Size(634, 184);
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlGroupIncidentDetailsLeft
            // 
            this.layoutControlGroupIncidentDetailsLeft.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroupIncidentDetailsLeft.ExpandButtonMode = DevExpress.Utils.Controls.ExpandButtonMode.Inverted;
            this.layoutControlGroupIncidentDetailsLeft.ExpandButtonVisible = true;
            this.layoutControlGroupIncidentDetailsLeft.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.layoutControlGroupIncidentDetailsLeft.Location = new System.Drawing.Point(0, 389);
            this.layoutControlGroupIncidentDetailsLeft.Name = "layoutControlGroupIncidentDetailsLeft";
            this.layoutControlGroupIncidentDetailsLeft.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupIncidentDetailsLeft.Size = new System.Drawing.Size(617, 392);
            this.layoutControlGroupIncidentDetailsLeft.Text = "layoutControlGroupIncidentDetailsLeft";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.webBrowserExIncidentDetailsLeft;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(607, 362);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.CustomizationFormText = "splitterItem1";
            this.splitterItem1.Location = new System.Drawing.Point(0, 384);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(617, 5);
            // 
            // simpleSeparator1
            // 
            this.simpleSeparator1.AllowHotTrack = false;
            this.simpleSeparator1.CustomizationFormText = "simpleSeparator1";
            this.simpleSeparator1.Location = new System.Drawing.Point(617, 0);
            this.simpleSeparator1.Name = "simpleSeparator1";
            this.simpleSeparator1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.simpleSeparator1.Size = new System.Drawing.Size(2, 781);
            this.simpleSeparator1.Text = "simpleSeparator1";
            // 
            // splitterItem2
            // 
            this.splitterItem2.AllowHotTrack = true;
            this.splitterItem2.CustomizationFormText = "splitterItem2";
            this.splitterItem2.Location = new System.Drawing.Point(619, 566);
            this.splitterItem2.Name = "splitterItem2";
            this.splitterItem2.Size = new System.Drawing.Size(640, 5);
            // 
            // layoutControlGroupNotifications
            // 
            this.layoutControlGroupNotifications.CustomizationFormText = "layoutControlGroupNotifications";
            this.layoutControlGroupNotifications.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem2,
            this.layoutControlItem2,
            this.layoutControlItem1});
            this.layoutControlGroupNotifications.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupNotifications.Name = "layoutControlGroupNotifications";
            this.layoutControlGroupNotifications.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupNotifications.Size = new System.Drawing.Size(617, 384);
            this.layoutControlGroupNotifications.Text = "layoutControlGroupNotifications";
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 319);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(522, 35);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.simpleButtonAssign;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(522, 319);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(85, 35);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(85, 35);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(85, 35);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridControlExNotificationDistribution;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(607, 319);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroupOperators
            // 
            this.layoutControlGroupOperators.CustomizationFormText = "layoutControlGroupOperators";
            this.layoutControlGroupOperators.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4});
            this.layoutControlGroupOperators.Location = new System.Drawing.Point(619, 0);
            this.layoutControlGroupOperators.Name = "layoutControlGroupOperators";
            this.layoutControlGroupOperators.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupOperators.Size = new System.Drawing.Size(640, 278);
            this.layoutControlGroupOperators.Text = "layoutControlGroupOperators";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.gridControlExOperators;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(630, 248);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlGroupNotificationsInProcess
            // 
            this.layoutControlGroupNotificationsInProcess.CustomizationFormText = "layoutControlGroupNotificationsInProcess";
            this.layoutControlGroupNotificationsInProcess.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem7,
            this.emptySpaceItem1,
            this.layoutControlItem5});
            this.layoutControlGroupNotificationsInProcess.Location = new System.Drawing.Point(619, 278);
            this.layoutControlGroupNotificationsInProcess.Name = "layoutControlGroupNotificationsInProcess";
            this.layoutControlGroupNotificationsInProcess.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupNotificationsInProcess.Size = new System.Drawing.Size(640, 288);
            this.layoutControlGroupNotificationsInProcess.Text = "layoutControlGroupNotificationsInProcess";
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.gridControlExNotificationInProgress;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(630, 223);
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 223);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(545, 35);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.simpleButtonRemove;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(545, 223);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(85, 35);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(85, 35);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(85, 35);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // printDialog
            // 
            this.printDialog.UseEXDialog = true;
            // 
            // RibbonControl
            // 
            this.RibbonControl.Dock = System.Windows.Forms.DockStyle.None;
            this.RibbonControl.ExpandCollapseItem.Id = 0;
            this.RibbonControl.ExpandCollapseItem.Name = "";
            this.RibbonControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RibbonControl.Images = this.imageList;
            this.RibbonControl.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.RibbonControl.ExpandCollapseItem,
            this.barButtonItemSave,
            this.barButtonItemPrint,
            this.barButtonItemUpdate,
            this.barButtonItemSend});
            this.RibbonControl.ItemsVertAlign = DevExpress.Utils.VertAlignment.Top;
            this.RibbonControl.LargeImages = this.imageList;
            this.RibbonControl.Location = new System.Drawing.Point(681, 334);
            this.RibbonControl.Margin = new System.Windows.Forms.Padding(2);
            this.RibbonControl.MaxItemId = 251;
            this.RibbonControl.MdiMergeStyle = DevExpress.XtraBars.Ribbon.RibbonMdiMergeStyle.Always;
            this.RibbonControl.Name = "RibbonControl";
            this.RibbonControl.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.RibbonPageOperation});
            this.RibbonControl.ShowToolbarCustomizeItem = false;
            this.RibbonControl.Size = new System.Drawing.Size(150, 120);
            this.RibbonControl.Toolbar.ShowCustomizeItem = false;
            this.RibbonControl.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "logout.gif");
            // 
            // barButtonItemSave
            // 
            this.barButtonItemSave.Caption = "Guardar";
            this.barButtonItemSave.Glyph = global::SmartCadGuiCommon.PrintRibbonControllerResources.RibbonPrintPreview_Save;
            this.barButtonItemSave.Id = 230;
            this.barButtonItemSave.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S));
            this.barButtonItemSave.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemSave.Name = "barButtonItemSave";
            this.barButtonItemSave.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemSave_ItemClick);
            // 
            // barButtonItemPrint
            // 
            this.barButtonItemPrint.Caption = "Imprimir";
            this.barButtonItemPrint.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.Glyph")));
            this.barButtonItemPrint.Id = 231;
            this.barButtonItemPrint.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P));
            this.barButtonItemPrint.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemPrint.Name = "barButtonItemPrint";
            this.barButtonItemPrint.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPrint_ItemClick);
            // 
            // barButtonItemUpdate
            // 
            this.barButtonItemUpdate.Caption = "Actualizar";
            this.barButtonItemUpdate.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemUpdate.Glyph")));
            this.barButtonItemUpdate.Id = 248;
            this.barButtonItemUpdate.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F5);
            this.barButtonItemUpdate.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemUpdate.Name = "barButtonItemUpdate";
            this.barButtonItemUpdate.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemUpdate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemUpdate_ItemClick);
            // 
            // barButtonItemSend
            // 
            this.barButtonItemSend.Caption = "Enviar";
            this.barButtonItemSend.Enabled = false;
            this.barButtonItemSend.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemSend.Glyph")));
            this.barButtonItemSend.Id = 249;
            this.barButtonItemSend.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Enter));
            this.barButtonItemSend.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemSend.Name = "barButtonItemSend";
            this.barButtonItemSend.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // RibbonPageOperation
            // 
            this.RibbonPageOperation.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupOptions});
            this.RibbonPageOperation.Name = "RibbonPageOperation";
            this.RibbonPageOperation.Text = "Operacin";
            // 
            // ribbonPageGroupOptions
            // 
            this.ribbonPageGroupOptions.AllowMinimize = false;
            this.ribbonPageGroupOptions.AllowTextClipping = false;
            this.ribbonPageGroupOptions.ItemLinks.Add(this.barButtonItemPrint);
            this.ribbonPageGroupOptions.ItemLinks.Add(this.barButtonItemSave);
            this.ribbonPageGroupOptions.ItemLinks.Add(this.barButtonItemSend);
            this.ribbonPageGroupOptions.ItemLinks.Add(this.barButtonItemUpdate);
            this.ribbonPageGroupOptions.Name = "ribbonPageGroupOptions";
            this.ribbonPageGroupOptions.ShowCaptionButton = false;
            this.ribbonPageGroupOptions.Text = "Opciones generales";
            // 
            // IncidentNotificationDistributionForm
            // 
            this.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1265, 787);
            this.Controls.Add(this.RibbonControl);
            this.Controls.Add(this.layoutControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "IncidentNotificationDistributionForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Distribucion de solicitudes de despacho";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.IncidentNotificationDistributionForm_FormClosing);
            this.Load += new System.EventHandler(this.IncidentNotificationDistributionForm_Load);
            this.contextMenuStripNotficationsToDistribute.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExNotificationDistribution)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewNotificationDistribution)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExNotificationInProgress)).EndInit();
            this.contextMenuStripNotificationsInProgress.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridViewNotificationInProgress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExOperators)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewOperators)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupIncidentDetailsRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupIncidentDetailsLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupNotifications)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupOperators)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupNotificationsInProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RibbonControl)).EndInit();
            this.ResumeLayout(false);

        }


        #endregion

        private SearchableWebBrowser webBrowserExIncidentDetailsRight;
        private System.Windows.Forms.ToolTip toolTipGeneral;
        private System.ComponentModel.IContainer components;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripNotficationsToDistribute;
        private System.Windows.Forms.ToolStripMenuItem asignarToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripNotificationsInProgress;
        private System.Windows.Forms.ToolStripMenuItem quitarToolStripMenuItem;
        private System.Windows.Forms.PrintDialog printDialog;
        private SearchableWebBrowser webBrowserExIncidentDetailsLeft;
        private GridControlEx gridControlExOperators;
        private GridViewEx gridViewOperators;
        private GridControlEx gridControlExNotificationDistribution;
        private GridViewEx gridViewNotificationDistribution;
        private GridControlEx gridControlExNotificationInProgress;
        private GridViewEx gridViewNotificationInProgress;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.SimpleButton simpleButtonRemove;
        private DevExpress.XtraEditors.SimpleButton simpleButtonAssign;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupIncidentDetailsRight;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupIncidentDetailsLeft;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private DevExpress.XtraLayout.SplitterItem splitterItem2;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupNotifications;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupOperators;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupNotificationsInProcess;
        internal DevExpress.XtraBars.Ribbon.RibbonControl RibbonControl;
        private System.Windows.Forms.ImageList imageList;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemSave;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemPrint;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemUpdate;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemSend;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPage RibbonPageOperation;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupOptions;

    }
}