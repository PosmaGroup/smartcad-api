using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.ServiceModel;
using Smartmatic.SmartCad.Service;
using System.Reflection;
using System.Collections;
using DevExpress.XtraEditors;
using SmartCadCore.ClientData;
using SmartCadCore.Enums;

using SmartCadCore.Common;
using SmartCadControls;
using SmartCadGuiCommon.Filters;
using SmartCadGuiCommon.SyncBoxes;



namespace SmartCadGuiCommon
{
    public partial class TrainingCourseEvaluationForm : XtraForm
    {
        private int totalAtt = 0;
        private OperatorTrainingCourseFilter operatorTrainingCourseFilter;

        public TrainingCourseEvaluationForm()
        {
            InitializeComponent();
            InitializeDataGrid();
        }
                
        public TrainingCourseEvaluationForm(TrainingCourseScheduleClientData schedule, FormBehavior behavior)
            : this()
        {
           SelectedCourseSchedule = schedule;
           Behavior = behavior;
        }

        private FormBehavior behavior;
        public FormBehavior Behavior
        {
            set
            {
                behavior = value;

            }
            get
            {
                return behavior;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            if (Behavior == FormBehavior.View) {

                this.Controls.Remove(this.buttonExOk);
                this.Controls.Remove(this.buttonExCancel);
                this.Size = new Size(this.Size.Width, this.Size.Height - 30);

                dataGridExEvaluation.ReadOnly = true;
            }    
        }

        private void InitializeDataGrid() 
        {
            dataGridExEvaluation.Type = typeof(GridOperatorTrainingCourseData);
            GridOperatorTrainingCourseData.DataGrid = dataGridExEvaluation;
            dataGridExEvaluation.ColumnHeadersHeight = 22;
            dataGridExEvaluation.AllowDrop = false;
            dataGridExEvaluation.AllowEditing = true;
        }

        private TrainingCourseScheduleClientData selectedCourseSchedule;
        public TrainingCourseScheduleClientData SelectedCourseSchedule
        {
            get
            {
                return selectedCourseSchedule;
            }
            set
            {
                selectedCourseSchedule = value;

                if (selectedCourseSchedule != null)
                {
                    textBoxExBase.Text = selectedCourseSchedule.TrainingCourse.Qualification.ToString();
                    textBoxExMinApp.Text = selectedCourseSchedule.TrainingCourse.ApprovalQualification.ToString();
                    textBoxExMinAtt.Text = selectedCourseSchedule.MinimumAttendance.ToString();
                    textBoxExTotalAtt.Text = CalculateTotalAttendance();
                    textBoxCourseName.Text = selectedCourseSchedule.TrainingCourseName;
                    FillOperatorTrainingCourse();
                    CalculateTotals();

                }

            }
        }

        private void FillOperatorTrainingCourse() {

            foreach (OperatorTrainingCourseClientData operTraining in selectedCourseSchedule.TrainingCourseOperators) 
            {
                GridOperatorTrainingCourseData gridData = new GridOperatorTrainingCourseData(operTraining);
                dataGridExEvaluation.AddData(gridData);            
            }
            
        }

        private string CalculateTotalAttendance()
        {
            int totalAttendance = 0;
           

            totalAtt = selectedCourseSchedule.Parts.Count;
            return totalAttendance.ToString();

        }

        private void dataGridExEvaluation_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            //if (e.ColumnIndex == 1) {
            //    DataGridViewCell cell = dataGridExEvaluation.Rows[e.RowIndex].Cells[e.ColumnIndex];
                
            
            //}else if(e.ColumnIndex == 2)
            

            CalculateTotals();
        }

        private void CalculateTotals()
        {
            int pending = 0;
            int total = 0;
            foreach (GridOperatorTrainingCourseData data in dataGridExEvaluation.GetDataGridObjects()) {

                if (data.Action == GridOperatorTrainingCourseData.ActionOnData.Pending)
                    pending += 1;
                else
                    total += 1;

            }

            textBoxExPending.Text = pending.ToString();
            textBoxExTotal.Text = total.ToString();
        
        }

        private void buttonExOk_Click(object sender, EventArgs e)
        {
            try
            {
                BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[1] { GetType().GetMethod("buttonExOK_Click1", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[0] });
                processForm.CanThrowError = true;
                processForm.ShowDialog();

            }
            catch (FaultException ex)
            {
                SelectedCourseSchedule = (TrainingCourseScheduleClientData)ServerServiceClient.GetInstance().RefreshClient(selectedCourseSchedule);
                //GetErrorFocus(ex);
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, MessageFormType.Error);
                //   buttonOkPressed = false;
            }
            catch (Exception ex)
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, ex);
                //  buttonOkPressed = false;
            }
        }

        private void buttonExOK_Click1()
        {
            foreach (DataGridExData data in dataGridExEvaluation.GetDataGridObjects())
            {
                OperatorTrainingCourseClientData operTraining = data.Tag as OperatorTrainingCourseClientData;

                ServerServiceClient.GetInstance().SaveOrUpdateClientData(operTraining);
            }
        
        }

        private void dataGridExEvaluation_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is DataGridViewTextBoxEditingControl) {
                ((DataGridViewTextBoxEditingControl)e.Control).KeyDown -= new KeyEventHandler(TrainingCourseEvaluationForm_KeyDown);
                ((DataGridViewTextBoxEditingControl)e.Control).KeyDown += new KeyEventHandler(TrainingCourseEvaluationForm_KeyDown);
            }
        }

        void TrainingCourseEvaluationForm_KeyDown(object sender, KeyEventArgs e)
        {
            int newNumber = 0;
            if (e.KeyValue > 47 && e.KeyValue < 58)
            {
                newNumber = e.KeyValue - 48;
                if (dataGridExEvaluation.CurrentCell.ColumnIndex == 1)
                {

                    double valueQ = Convert.ToDouble(((DataGridViewTextBoxEditingControl)sender).Text + newNumber);
                    if (valueQ > selectedCourseSchedule.TrainingCourse.Qualification)
                        e.SuppressKeyPress = true;
                }
                else if (dataGridExEvaluation.CurrentCell.ColumnIndex == 2)
                {
                    int valueAtt = Convert.ToInt32(((DataGridViewTextBoxEditingControl)sender).Text + newNumber);
                    if (valueAtt > totalAtt)
                        e.SuppressKeyPress = true;
                }
            }
            else if (e.KeyValue > 95 && e.KeyValue < 105)
            {
                newNumber = e.KeyValue - 96;
                if (dataGridExEvaluation.CurrentCell.ColumnIndex == 1)
                {

                    double valueQ = Convert.ToDouble(((DataGridViewTextBoxEditingControl)sender).Text + newNumber);
                    if (valueQ > selectedCourseSchedule.TrainingCourse.Qualification)
                        e.SuppressKeyPress = true;
                }
                else if (dataGridExEvaluation.CurrentCell.ColumnIndex == 2)
                {
                    int valueAtt = Convert.ToInt32(((DataGridViewTextBoxEditingControl)sender).Text + newNumber);
                    if (valueAtt > totalAtt)
                        e.SuppressKeyPress = true;
                }
            }           
            
        }

        private void toolStripButtonSearch_Click(object sender, EventArgs e)
        {
            operatorTrainingCourseFilter = new OperatorTrainingCourseFilter(dataGridExEvaluation, new ArrayList());
            if (string.IsNullOrEmpty(this.toolStripTextBoxSearch.Text.Trim()) == false)
            {
                operatorTrainingCourseFilter.FilterByText(this.toolStripTextBoxSearch.Text.Trim());
            }
            else
                operatorTrainingCourseFilter.Filter(false);
     
        }

        private void TrainingCourseEvaluationForm_Load(object sender, EventArgs e)
        {
            LoadLanguage();
        }

        private void LoadLanguage()
        {
            groupBoxCourseData.Text = ResourceLoader.GetString2("TrainingCourseProperties");
            labelCourseName.Text = ResourceLoader.GetString2("LabelName");

            groupBoxEvaluationCriteria.Text = ResourceLoader.GetString2("EvaluationCriteria");
            labelExQualification.Text = ResourceLoader.GetString2("BaseQualification");
            labelExMinApp.Text = ResourceLoader.GetString2("MinimumQualification");
            labelExTotalAtt.Text = ResourceLoader.GetString2("TotalAttendance");
            labelExMinAtt.Text = ResourceLoader.GetString2("MinimumAttendance");

            groupBoxEvaluation.Text = ResourceLoader.GetString2("Evaluation");
            labelExTotal.Text = ResourceLoader.GetString2("EvaluatedOperators");
            labelExPending.Text = ResourceLoader.GetString2("PendingOperators");
        }
    }
}