

using SmartCadControls.Controls;
using SmartCadCore.Common;
namespace SmartCadGuiCommon
{
    partial class EvaluationRenderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EvaluationRenderForm));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.groupControlQuestions = new DevExpress.XtraEditors.GroupControl();
            this.panelRender = new DevExpress.XtraEditors.XtraScrollableControl();
            this.groupControlEvaluationInfo = new DevExpress.XtraEditors.GroupControl();
            this.labelExTitleDate = new LabelEx();
            this.labelExTitleTime = new LabelEx();
            this.labelTitleEvaluator = new LabelEx();
            this.labelExEvalName = new LabelEx();
            this.labelExEvaluator = new LabelEx();
            this.labelExNameEvaluation = new LabelEx();
            this.labelDate = new LabelEx();
            this.labelTime = new LabelEx();
            this.buttonOK = new DevExpress.XtraEditors.SimpleButton();
            this.buttonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.groupControlInfo = new DevExpress.XtraEditors.GroupControl();
            this.labelExCat = new LabelEx();
            this.labelExTitleRole = new LabelEx();
            this.labelExName = new LabelEx();
            this.labelOperatorName = new LabelEx();
            this.labelRole = new LabelEx();
            this.labelCategory = new LabelEx();
            this.labelQualification = new LabelEx();
            this.qualification = new LabelEx();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlQuestions)).BeginInit();
            this.groupControlQuestions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlEvaluationInfo)).BeginInit();
            this.groupControlEvaluationInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlInfo)).BeginInit();
            this.groupControlInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.groupControlQuestions);
            this.panelControl1.Controls.Add(this.groupControlEvaluationInfo);
            this.panelControl1.Controls.Add(this.buttonOK);
            this.panelControl1.Controls.Add(this.buttonCancel);
            this.panelControl1.Controls.Add(this.groupControlInfo);
            this.panelControl1.Controls.Add(this.labelQualification);
            this.panelControl1.Controls.Add(this.qualification);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(538, 745);
            this.panelControl1.TabIndex = 6;
            // 
            // groupControlQuestions
            // 
            this.groupControlQuestions.Controls.Add(this.panelRender);
            this.groupControlQuestions.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControlQuestions.Location = new System.Drawing.Point(2, 218);
            this.groupControlQuestions.Name = "groupControlQuestions";
            this.groupControlQuestions.Size = new System.Drawing.Size(534, 481);
            this.groupControlQuestions.TabIndex = 3;
            //this.groupControlQuestions.Text = "Evaluacion";
            this.groupControlQuestions.Text = ResourceLoader.GetString2("EvaluationText");
            // 
            // panelRender
            // 
            this.panelRender.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelRender.Location = new System.Drawing.Point(2, 22);
            this.panelRender.Name = "panelRender";
            this.panelRender.Size = new System.Drawing.Size(530, 457);
            this.panelRender.TabIndex = 0;
            // 
            // groupControlEvaluationInfo
            // 
            this.groupControlEvaluationInfo.Controls.Add(this.labelExTitleDate);
            this.groupControlEvaluationInfo.Controls.Add(this.labelExTitleTime);
            this.groupControlEvaluationInfo.Controls.Add(this.labelTitleEvaluator);
            this.groupControlEvaluationInfo.Controls.Add(this.labelExEvalName);
            this.groupControlEvaluationInfo.Controls.Add(this.labelExEvaluator);
            this.groupControlEvaluationInfo.Controls.Add(this.labelExNameEvaluation);
            this.groupControlEvaluationInfo.Controls.Add(this.labelDate);
            this.groupControlEvaluationInfo.Controls.Add(this.labelTime);
            this.groupControlEvaluationInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControlEvaluationInfo.Location = new System.Drawing.Point(2, 118);
            this.groupControlEvaluationInfo.Name = "groupControlEvaluationInfo";
            this.groupControlEvaluationInfo.Size = new System.Drawing.Size(534, 100);
            this.groupControlEvaluationInfo.TabIndex = 2;
            this.groupControlEvaluationInfo.Text = "groupControl1";
            // 
            // labelExTitleDate
            // 
            this.labelExTitleDate.AutoSize = true;
            this.labelExTitleDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExTitleDate.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExTitleDate.Location = new System.Drawing.Point(268, 40);
            this.labelExTitleDate.Name = "labelExTitleDate";
            this.labelExTitleDate.Size = new System.Drawing.Size(46, 13);
            this.labelExTitleDate.TabIndex = 20;
            this.labelExTitleDate.Text = "Fecha:";
            // 
            // labelExTitleTime
            // 
            this.labelExTitleTime.AutoSize = true;
            this.labelExTitleTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExTitleTime.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExTitleTime.Location = new System.Drawing.Point(268, 66);
            this.labelExTitleTime.Name = "labelExTitleTime";
            this.labelExTitleTime.Size = new System.Drawing.Size(52, 13);
            this.labelExTitleTime.TabIndex = 19;
            this.labelExTitleTime.Text = "Tiempo:";
            // 
            // labelTitleEvaluator
            // 
            this.labelTitleEvaluator.AutoSize = true;
            this.labelTitleEvaluator.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitleEvaluator.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelTitleEvaluator.Location = new System.Drawing.Point(25, 66);
            this.labelTitleEvaluator.Name = "labelTitleEvaluator";
            this.labelTitleEvaluator.Size = new System.Drawing.Size(68, 13);
            this.labelTitleEvaluator.TabIndex = 18;
            this.labelTitleEvaluator.Text = "Evaluador:";
            // 
            // labelExEvalName
            // 
            this.labelExEvalName.AutoSize = true;
            this.labelExEvalName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExEvalName.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExEvalName.Location = new System.Drawing.Point(25, 38);
            this.labelExEvalName.Name = "labelExEvalName";
            this.labelExEvalName.Size = new System.Drawing.Size(54, 13);
            this.labelExEvalName.TabIndex = 17;
            this.labelExEvalName.Text = "Nombre:";
            // 
            // labelExEvaluator
            // 
            this.labelExEvaluator.AutoSize = true;
            this.labelExEvaluator.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExEvaluator.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExEvaluator.Location = new System.Drawing.Point(108, 66);
            this.labelExEvaluator.Name = "labelExEvaluator";
            this.labelExEvaluator.Size = new System.Drawing.Size(47, 13);
            this.labelExEvaluator.TabIndex = 16;
            this.labelExEvaluator.Text = "labelEx2";
            // 
            // labelExNameEvaluation
            // 
            this.labelExNameEvaluation.AutoSize = true;
            this.labelExNameEvaluation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExNameEvaluation.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExNameEvaluation.Location = new System.Drawing.Point(108, 38);
            this.labelExNameEvaluation.Name = "labelExNameEvaluation";
            this.labelExNameEvaluation.Size = new System.Drawing.Size(47, 13);
            this.labelExNameEvaluation.TabIndex = 15;
            this.labelExNameEvaluation.Text = "labelEx1";
            // 
            // labelDate
            // 
            this.labelDate.AutoSize = true;
            this.labelDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDate.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelDate.Location = new System.Drawing.Point(335, 38);
            this.labelDate.Name = "labelDate";
            this.labelDate.Size = new System.Drawing.Size(47, 13);
            this.labelDate.TabIndex = 13;
            this.labelDate.Text = "labelEx8";
            // 
            // labelTime
            // 
            this.labelTime.AutoSize = true;
            this.labelTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTime.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelTime.Location = new System.Drawing.Point(335, 66);
            this.labelTime.Name = "labelTime";
            this.labelTime.Size = new System.Drawing.Size(53, 13);
            this.labelTime.TabIndex = 14;
            this.labelTime.Text = "labelEx10";
            // 
            // buttonOK
            // 
            this.buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOK.Enabled = false;
            this.buttonOK.Location = new System.Drawing.Point(370, 712);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 18;
            this.buttonOK.Text = "Accept";
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(451, 712);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 17;
            //this.buttonCancel.Text = "Cancelar";
            this.buttonCancel.Text = ResourceLoader.GetString2("$Message.Cancel");
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // groupControlInfo
            // 
            this.groupControlInfo.Controls.Add(this.labelExCat);
            this.groupControlInfo.Controls.Add(this.labelExTitleRole);
            this.groupControlInfo.Controls.Add(this.labelExName);
            this.groupControlInfo.Controls.Add(this.labelOperatorName);
            this.groupControlInfo.Controls.Add(this.labelRole);
            this.groupControlInfo.Controls.Add(this.labelCategory);
            this.groupControlInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControlInfo.Location = new System.Drawing.Point(2, 2);
            this.groupControlInfo.Name = "groupControlInfo";
            this.groupControlInfo.Size = new System.Drawing.Size(534, 116);
            this.groupControlInfo.TabIndex = 0;
            //this.groupControlInfo.Text = "Datos del operador";
            this.groupControlInfo.Text = ResourceLoader.GetString2("OperatorDataText");
                                                                    
            // labelExCat
            // 
            this.labelExCat.AutoSize = true;
            this.labelExCat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExCat.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExCat.Location = new System.Drawing.Point(25, 83);
            this.labelExCat.Name = "labelExCat";
            this.labelExCat.Size = new System.Drawing.Size(65, 13);
            this.labelExCat.TabIndex = 15;
            this.labelExCat.Text = "Categoria:";
            // 
            // labelExTitleRole
            // 
            this.labelExTitleRole.AutoSize = true;
            this.labelExTitleRole.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExTitleRole.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExTitleRole.Location = new System.Drawing.Point(25, 57);
            this.labelExTitleRole.Name = "labelExTitleRole";
            this.labelExTitleRole.Size = new System.Drawing.Size(30, 13);
            this.labelExTitleRole.TabIndex = 14;
            this.labelExTitleRole.Text = "Rol:";
            // 
            // labelExName
            // 
            this.labelExName.AutoSize = true;
            this.labelExName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExName.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExName.Location = new System.Drawing.Point(25, 31);
            this.labelExName.Name = "labelExName";
            this.labelExName.Size = new System.Drawing.Size(54, 13);
            this.labelExName.TabIndex = 13;
            this.labelExName.Text = "Nombre:";
            // 
            // labelOperatorName
            // 
            this.labelOperatorName.AutoSize = true;
            this.labelOperatorName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOperatorName.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelOperatorName.Location = new System.Drawing.Point(108, 31);
            this.labelOperatorName.Name = "labelOperatorName";
            this.labelOperatorName.Size = new System.Drawing.Size(47, 13);
            this.labelOperatorName.TabIndex = 10;
            this.labelOperatorName.Text = "labelEx2";
            // 
            // labelRole
            // 
            this.labelRole.AutoSize = true;
            this.labelRole.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRole.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelRole.Location = new System.Drawing.Point(108, 57);
            this.labelRole.Name = "labelRole";
            this.labelRole.Size = new System.Drawing.Size(47, 13);
            this.labelRole.TabIndex = 11;
            this.labelRole.Text = "labelEx4";
            // 
            // labelCategory
            // 
            this.labelCategory.AutoSize = true;
            this.labelCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCategory.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelCategory.Location = new System.Drawing.Point(108, 83);
            this.labelCategory.Name = "labelCategory";
            this.labelCategory.Size = new System.Drawing.Size(47, 13);
            this.labelCategory.TabIndex = 12;
            this.labelCategory.Text = "labelEx6";
            // 
            // labelQualification
            // 
            this.labelQualification.AutoSize = true;
            this.labelQualification.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQualification.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelQualification.Location = new System.Drawing.Point(23, 712);
            this.labelQualification.Name = "labelQualification";
            this.labelQualification.Size = new System.Drawing.Size(0, 13);
            this.labelQualification.TabIndex = 9;
            // 
            // qualification
            // 
            this.qualification.AutoSize = true;
            this.qualification.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qualification.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.qualification.Location = new System.Drawing.Point(63, 712);
            this.qualification.Name = "qualification";
            this.qualification.Size = new System.Drawing.Size(0, 13);
            this.qualification.TabIndex = 8;
            // 
            // EvaluationRenderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(538, 745);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EvaluationRenderForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "EvaluationRenderForm";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlQuestions)).EndInit();
            this.groupControlQuestions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControlEvaluationInfo)).EndInit();
            this.groupControlEvaluationInfo.ResumeLayout(false);
            this.groupControlEvaluationInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlInfo)).EndInit();
            this.groupControlInfo.ResumeLayout(false);
            this.groupControlInfo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton buttonOK;
        private DevExpress.XtraEditors.SimpleButton buttonCancel;
        private DevExpress.XtraEditors.GroupControl groupControlQuestions;
        private DevExpress.XtraEditors.XtraScrollableControl panelRender;
        private DevExpress.XtraEditors.GroupControl groupControlInfo;
        private LabelEx labelOperatorName;
        private LabelEx labelTime;
        private LabelEx labelRole;
        private LabelEx labelDate;
        private LabelEx labelCategory;
        private LabelEx labelQualification;
        private LabelEx qualification;
        private DevExpress.XtraEditors.GroupControl groupControlEvaluationInfo;
        private LabelEx labelExEvaluator;
        private LabelEx labelExNameEvaluation;
        private LabelEx labelExCat;
        private LabelEx labelExTitleRole;
        private LabelEx labelExName;
        private LabelEx labelExTitleDate;
        private LabelEx labelExTitleTime;
        private LabelEx labelTitleEvaluator;
        private LabelEx labelExEvalName;

    }
}