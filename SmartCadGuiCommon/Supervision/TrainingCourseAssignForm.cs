 using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Smartmatic.SmartCad.Service;
using System.ServiceModel;
using System.Reflection;
using DevExpress.XtraEditors;
using SmartCadCore.ClientData;
using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadGuiCommon.SyncBoxes;




namespace SmartCadGuiCommon
{
    public partial class TrainingCourseAssignForm : XtraForm
    {
        private object syncCommited = new object();
        private Dictionary<int, IList> operatorsAssigned = new Dictionary<int, IList>();
        private bool buttonPressed = false;
        private DepartmentTypeClientData department;
        private string applicationName = string.Empty;
        private Dictionary<int, OperatorClientData> operatorsByApplication = new Dictionary<int, OperatorClientData>();
        private Dictionary<int, IList> operatorsAssignedOtherApp = new Dictionary<int, IList>();
        private ArrayList operatorsTrainingToDeleteConflict = new ArrayList();

        public TrainingCourseAssignForm()
        {
            InitializeComponent();
            LoadLanguage();
            InitializeDataGrid();
        }

        public TrainingCourseAssignForm(TrainingCourseClientData course, TrainingCourseScheduleClientData schedule, string appName, DepartmentTypeClientData dep)
            : this()
        {

            ServerServiceClient.GetInstance().CommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(TrainingCourseAssignForm_CommittedChanges);
            applicationName = appName;
            if (dep!=null)
                department = dep;

           
            SelectedCourse = course;
            SelectedCourseSchedule = schedule;
            
            FillNotAssignedOperators();
        
        }

        private Dictionary<int, OperatorClientData> GetOperatorsByApplication()
        {
            IList list = new ArrayList();
            Dictionary<int, OperatorClientData> result = new Dictionary<int, OperatorClientData>();

            if (applicationName == UserApplicationClientData.FirstLevel.Name)
                list = (IList)ServerServiceClient.GetInstance().SearchBasicObjects(SmartCadHqls.GetFirstLevelOperatorsCodeFirstAndLastNameWithoutSupervisor);
            else if (applicationName == UserApplicationClientData.Dispatch.Name)
                list = (IList)ServerServiceClient.GetInstance().SearchBasicObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetDispatchOperatorsCodeFirstAndLastNameWithoutSupervisor, department.Code));
            
            foreach (object[] operObject in list)
            {
                OperatorClientData oper = new OperatorClientData();
                oper.Code = (int)operObject[0];
                oper.FirstName = operObject[1] as string;
                oper.LastName = operObject[2] as string;

                result.Add(oper.Code, oper);
            }

            return result;

        }

        private void LoadLanguage()
        {
            this.Text = ResourceLoader.GetString2("TrainingCourseAssignForm");

            this.layoutControlGroupTrainingCourse.Text = ResourceLoader.GetString2("TrainingCourseInformation");
            this.textBoxExCourseitem.Text = ResourceLoader.GetString2("Name") + ":";
            this.gridControlSchedulesitem.Text = ResourceLoader.GetString2("Schedules") + ":";

            this.layoutControlGroupOperators.Text = ResourceLoader.GetString2("TrainingCourseOperators");
            this.layoutControlItemAssigned.Text = ResourceLoader.GetString2("ParticipantsAssigned");
            this.layoutControlItemNotAssigned.Text = ResourceLoader.GetString2("ParticipantsNotAssigned");

            buttonExOk.Text = ResourceLoader.GetString2("Accept");
            buttonExCancel.Text = ResourceLoader.GetString2("Cancel");
            buttonExApply.Text = ResourceLoader.GetString2("Apply");
        }

        private void InitializeDataGrid()
        {
			gridControlSchedules.Type = typeof(GridTrainingCourseScheduleData);
            gridControlSchedules.EnableAutoFilter = true;
            gridControlSchedules.ViewTotalRows = true;
			gridViewSchedules.OptionsView.ShowAutoFilterRow = true;
			gridControlSchedules.AllowDrop = false;
			gridViewSchedules.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(gridViewSchedules_SelectionChanged);
        }

    
        protected override void OnFormClosed(FormClosedEventArgs e)
        {
            base.OnFormClosed(e);
            ServerServiceClient.GetInstance().CommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(TrainingCourseAssignForm_CommittedChanges);
        }


        private bool CheckOperatorAssigned(OperatorClientData ope)
        {
            foreach (KeyValuePair<int, IList> item in operatorsAssigned)
            {
                IList list = item.Value;

                if (list != null)
                {
                    foreach (OperatorTrainingCourseClientData otc in list)
                    {
                        if (otc.OperatorCode == ope.Code)
                            return true;

                    }
                }
    
            }

            return false;
          
        }

        private void EnableTranferButtons(bool enable) {

            buttonAddAllOperators.Enabled = enable;
            buttonAddOperator.Enabled = enable;
            buttonRemoveAllOperators.Enabled = enable;
            buttonRemoveOperator.Enabled = enable;        
        }

        void gridViewSchedules_SelectionChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (gridControlSchedules.SelectedItems.Count > 0)
            {
				selectedCourseSchedule = ((GridTrainingCourseScheduleData)gridControlSchedules.SelectedItems[0]).TrainingCourseScheduleClient;
                listBoxAssigned.Items.Clear();
                bool enable = selectedCourseSchedule.RealStart > ServerServiceClient.GetInstance().GetTime();
                EnableTranferButtons(enable);

                operatorsByApplication = GetOperatorsByApplication();

                foreach (TrainingCourseScheduleClientData schedule in selectedCourse.Schedules)
                {
                    IList trainingCourseOperators = new ArrayList();
                    //IList trainingCourseOperatorsOtherApp = new ArrayList();

                    if (operatorsAssigned.ContainsKey(schedule.Code) == false)
                    {
                        if (schedule.TrainingCourseOperators != null)
                        {
                            foreach (OperatorTrainingCourseClientData operTraining in schedule.TrainingCourseOperators)
                            {
                                if (operatorsByApplication.ContainsKey(operTraining.OperatorCode) == true)
                                    trainingCourseOperators.Add(operTraining);
                                //else
                                //    trainingCourseOperatorsOtherApp.Add(operTraining);
                            }
                            //operatorsAssignedOtherApp.Add(schedule.Code, trainingCourseOperatorsOtherApp);
                        }
                        operatorsAssigned.Add(schedule.Code, trainingCourseOperators);
                    }
                }


                IList listOperatorAssign = operatorsAssigned[selectedCourseSchedule.Code];

                foreach (OperatorTrainingCourseClientData operCourse in listOperatorAssign) 
                {
                    OperatorClientData operClient = new OperatorClientData();
                    operClient.Code = operCourse.OperatorCode;
                    operClient.FirstName = operCourse.OperatorFirstName;
                    operClient.LastName = operCourse.OperatorLastName;

                    listBoxAssigned.Items.Add(operClient);
                }
                
                //foreach (OperatorTrainingCourseClientData operCourse in listOperatorAssign)
                //{
                //    if (operCourse.CourseScheduleCode == selectedCourseSchedule.Code)
                //    {
                //        OperatorClientData operClient = new OperatorClientData();
                //        operClient.Code = operCourse.OperatorCode;

                //        operClient = (OperatorClientData)ServerServiceClient.GetInstance().RefreshClient(operClient);

                //        if (operClient != null)
                //        {
                //            if ((UserApplicationClientData.FirstLevel.Name == applicationName) 
                //                || (UserApplicationClientData.Dispatch.Name == applicationName && operClient.DepartmentTypes != null && operClient.DepartmentTypes.Contains(department))
                //                || (operClient.DepartmentTypes == null))
                //                listBoxAssigned.Items.Add(operClient);
                //        }
                //    }
                //}
                //FillOperators();
            }
            else
            {
                EnableTranferButtons(false);
            }
            EnableDisable_AcceptButton();
        }

        private TrainingCourseClientData selectedCourse;
        public TrainingCourseClientData SelectedCourse
        {
            get
            {
                return selectedCourse;
            }
            set
            {
                selectedCourse = value;

                if (selectedCourse != null)
                {
                    DateTime now = ServerServiceClient.GetInstance().GetTime();
                    textBoxExCourse.Text = selectedCourse.Name;

                    BindingList<GridTrainingCourseScheduleData> dataSource = new BindingList<GridTrainingCourseScheduleData>();
                  
                    operatorsByApplication = GetOperatorsByApplication();

                    foreach (TrainingCourseScheduleClientData schedule in selectedCourse.Schedules)
                    {
                        if (schedule.RealStart > now)
                        {
                            IList trainingCourseOperators = new ArrayList();
                            IList trainingCourseOperatorsOtherApp = new ArrayList();

                            if (operatorsAssigned.ContainsKey(schedule.Code) == false)
                            {
                                if (schedule.TrainingCourseOperators != null)
                                {
                                    foreach (OperatorTrainingCourseClientData operTraining in schedule.TrainingCourseOperators)
                                    {
                                        if (operatorsByApplication.ContainsKey(operTraining.OperatorCode) == true)
                                            trainingCourseOperators.Add(operTraining);
                                        else
                                            trainingCourseOperatorsOtherApp.Add(operTraining);
                                    }
                                    operatorsAssigned.Add(schedule.Code, trainingCourseOperators);
                                    operatorsAssignedOtherApp.Add(schedule.Code, trainingCourseOperatorsOtherApp);
                                }
                            }

                            dataSource.Add(new GridTrainingCourseScheduleData(schedule));
                        }
                    }

                    gridViewSchedules.BeginUpdate();
                    gridControlSchedules.DataSource = dataSource;
                    gridViewSchedules.EndUpdate();
                }
            }
        }

        private TrainingCourseScheduleClientData selectedCourseSchedule;
        public TrainingCourseScheduleClientData SelectedCourseSchedule
        {
            get
            {
                return selectedCourseSchedule;
            }
            set
            {
                selectedCourseSchedule = value;

                if (selectedCourseSchedule != null) 
                {
                    GridTrainingCourseScheduleData gridData = new GridTrainingCourseScheduleData(selectedCourseSchedule);
                    gridControlSchedules.SelectData(gridData);                  
                }
            }
        }



        private void FillNotAssignedOperators()
        {
            listBoxNotAssigned.Items.Clear();

            foreach (KeyValuePair<int, OperatorClientData> item in operatorsByApplication)
            {
                if (CheckOperatorAssigned(item.Value) == false && listBoxAssigned.Items.Contains(item.Value) == false)
                    listBoxNotAssigned.Items.Add(item.Value);
            }
        }

               

        private void buttonExOk_Click(object sender, EventArgs e)
        {
            buttonPressed = true;
            try
            {
                if (buttonExApply.Enabled == true)
                {
                    BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[1] { GetType().GetMethod("buttonExOK_Click1", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[0] });
                    processForm.CanThrowError = true;
                    processForm.ShowDialog();
                }
            }
            catch (FaultException ex)
            {
                buttonPressed = false;
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, MessageFormType.Error);
            }
            catch (Exception ex)
            {
                buttonPressed = false;
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, ex);
            }
          
        }




        private void buttonExOK_Click1()
        {
            ArrayList operatorsTrainingToSave = new ArrayList();
            ArrayList operatorsTrainingToDelete = new ArrayList();            
            int SelectedScheduleCode = 0;
            DateTime scheduleEndDate;
            DateTime scheduleStartDate;
            string isActiveOperators = "";
            bool isAccepted = true;

            foreach (GridTrainingCourseScheduleData gridData in gridControlSchedules.Items)
            {
                TrainingCourseScheduleClientData schedule = gridData.TrainingCourseScheduleClient as TrainingCourseScheduleClientData;
                SelectedScheduleCode = schedule.Code;
                scheduleEndDate = schedule.RealEnd;
                scheduleStartDate = schedule.RealStart;
                if (operatorsAssigned.ContainsKey(schedule.Code) == true)
                    operatorsTrainingToSave.AddRange(new ArrayList(operatorsAssigned[schedule.Code]));

                if (operatorsAssignedOtherApp.ContainsKey(schedule.Code) == true)
                    operatorsTrainingToSave.AddRange(operatorsAssignedOtherApp[schedule.Code]);

                IList list = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorsTrainingCourseByScheduleCode, schedule.Code));

                foreach (OperatorTrainingCourseClientData oper in list)
                {
                    if (operatorsTrainingToSave.Contains(oper) == false)
                        operatorsTrainingToDelete.Add(oper);
                }               

                foreach (OperatorTrainingCourseClientData oper in operatorsAssigned[schedule.Code])
                {
                    IList listCourse = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorsTrainingCourseByOperatorCode, oper.OperatorCode));
                    if (listCourse.Count > 0)
                    {
                        foreach (OperatorTrainingCourseClientData selectedSchedule in listCourse)
                        {
                            IList listCourseSchedule = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetTrainingCourseScheduleByCourseCode, selectedSchedule.CourseCode));
                            if (listCourseSchedule.Count > 0)
                            {
                                foreach (TrainingCourseScheduleClientData listSchedule in listCourseSchedule)
                                {
                                    if (listSchedule.Code != SelectedScheduleCode)
                                    {
                                        bool overlapSched = intersects(listSchedule.RealStart, listSchedule.RealEnd, scheduleStartDate, scheduleEndDate);
                                        bool overlapSchedule = (listSchedule.RealStart > scheduleStartDate && listSchedule.RealStart < scheduleEndDate) || (listSchedule.RealEnd > scheduleStartDate && listSchedule.RealEnd < scheduleEndDate);
                                        if (overlapSched == true)
                                        {
                                            if (operatorsTrainingToSave.Contains(oper))
                                            {
                                                isActiveOperators += oper.OperatorFirstName + " " + oper.OperatorLastName + ", ";
                                                operatorsTrainingToSave.Remove(oper);
                                            }                                            

                                            ArrayList toDelete = new ArrayList();
                                            foreach (OperatorClientData operDelete in listBoxAssigned.Items)
                                            {
                                                if (operDelete.Code == oper.OperatorCode)
                                                {
                                                    if (!operatorsTrainingToDeleteConflict.Contains(operDelete))
                                                        operatorsTrainingToDeleteConflict.Add(operDelete);
                                                }
                                            }
                                        }
                                    }                                    
                                }
                                
                            }
                        }
                    }
                }                              
            }

            if (isActiveOperators != "")
            {
                DialogResult result = MessageForm.Show(ResourceLoader.GetString2("OperatorHasTrainingCourseScheduled", isActiveOperators.Substring(0, isActiveOperators.Length - 2)), MessageFormType.Information);
                if (result == DialogResult.OK)
                {
                    isAccepted = true;                    
                }
                else
                    isAccepted = false;
            }  

            if (isAccepted == true)
            {
                if (operatorsTrainingToDelete != null && operatorsTrainingToDelete.Count > 0)
                    ServerServiceClient.GetInstance().DeleteClientObjectCollection(operatorsTrainingToDelete);

                if (operatorsTrainingToSave != null && operatorsTrainingToSave.Count > 0)
                    ServerServiceClient.GetInstance().SaveOrUpdateClientData(operatorsTrainingToSave);
            }            
        }

        private bool intersects(DateTime r1start, DateTime r1end, DateTime r2start, DateTime r2end)
        {
            return (r1start == r2start)
                || (r1start > r2start ? r1start <= r2end : r2start <= r1end);
        }

        private void buttonExApply_Click(object sender, EventArgs e)
        {            
            buttonExOk_Click(null, null);
            buttonExApply.Enabled = false;
            buttonRemoveOperatorConflict(operatorsTrainingToDeleteConflict);
        }

        void TrainingCourseAssignForm_CommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    lock (syncCommited)
                    {
                        if (e.Objects[0] is TrainingCourseClientData)
                        {
                            TrainingCourseClientData course = (TrainingCourseClientData)e.Objects[0];
                            if (e.Action == CommittedDataAction.Delete && course.Code == selectedCourse.Code && buttonPressed == false)
                            {
                                MessageForm.Show(ResourceLoader.GetString2("DeleteFormTrainingCourseAssignTrainingCourse"), MessageFormType.Information);
                                DialogResult = DialogResult.Cancel;
                                this.Close();
                            }
                        }
                        else if (e.Objects[0] is TrainingCourseScheduleClientData)
                        {
                            TrainingCourseScheduleClientData schedule = (TrainingCourseScheduleClientData)e.Objects[0];
                            if (e.Action == CommittedDataAction.Delete && schedule.Code == selectedCourseSchedule.Code && buttonPressed == false)
                            {
                                MessageForm.Show(ResourceLoader.GetString2("DeleteFormTrainingCourseAssignTrainingCourseSchedule"), MessageFormType.Information);
                                DialogResult = DialogResult.Cancel;
                                this.Close();
                            }
                        }
                        else if (e.Objects[0] is OperatorTrainingCourseClientData)
                        {
                            bool messageShowed = false;
                            foreach (OperatorTrainingCourseClientData otcd in e.Objects)
                            {
                                IList operatorsTrainingCourse = new ArrayList();
                                IList operatorsTrainingCourseOtherApp = new ArrayList();

                                if (operatorsAssigned.ContainsKey(otcd.CourseScheduleCode) == true)
                                    operatorsTrainingCourse = operatorsAssigned[otcd.CourseScheduleCode];

                                if (operatorsAssignedOtherApp.ContainsKey(otcd.CourseScheduleCode) == true)
                                    operatorsTrainingCourseOtherApp = operatorsAssignedOtherApp[otcd.CourseScheduleCode];


                                if (operatorsByApplication.ContainsKey(otcd.OperatorCode) == true)
                                {
                                    if (e.Action != CommittedDataAction.Delete)
                                    {
                                        int index = operatorsTrainingCourse.IndexOf(otcd);
                                        if (index == -1)
                                            operatorsTrainingCourse.Add(otcd);
                                        else
                                            operatorsTrainingCourse[index] = otcd;
                                    }
                                    else
                                    {
                                        operatorsTrainingCourse.Remove(otcd);
                                    }

                                }
                                else
                                {
                                    if (e.Action != CommittedDataAction.Delete)
                                    {
                                        int index = operatorsTrainingCourseOtherApp.IndexOf(otcd);
                                        if (index == -1)
                                            operatorsTrainingCourseOtherApp.Add(otcd);
                                        else
                                            operatorsTrainingCourseOtherApp[index] = otcd;
                                    }
                                    else
                                    {
                                        operatorsTrainingCourseOtherApp.Remove(otcd);
                                    }

                                }

                                if (selectedCourseSchedule.Code == otcd.CourseScheduleCode && buttonPressed == false)
                                {
                                    if (messageShowed == false)
                                    {
                                        messageShowed = true;
                                        MessageForm.Show(ResourceLoader.GetString2("UpdateFormTrainingCourseAssign"), MessageFormType.Information);
                                    }
                                }
                            }


                            FormUtil.InvokeRequired(gridControlSchedules,
                                delegate
                                {
                                    gridViewSchedules_SelectionChanged(
                                        gridViewSchedules,
                                        new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs(
                                            gridViewSchedules.FocusedRowHandle,
                                            gridViewSchedules.FocusedRowHandle));
                                });

                        }
                        else if(e.Objects[0] is OperatorClientData)
                        {
                            OperatorClientData oper = e.Objects[0] as OperatorClientData;

                            if (e.Action == CommittedDataAction.Delete) 
                            {
                                FormUtil.InvokeRequired(this, delegate
                                {
                                    if (operatorsByApplication.ContainsKey(oper.Code) == true)
                                        operatorsByApplication.Remove(oper.Code);

                                    if (listBoxNotAssigned.Items.Contains(oper) == true)
                                        listBoxNotAssigned.Items.Remove(oper);

                                    if (listBoxAssigned.Items.Contains(oper) == true)
                                        listBoxAssigned.Items.Remove(oper);
                                                                                                           
                                });

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }

        }

        private void buttonAddOperator_Click(object sender, EventArgs e)
        {
            ArrayList toDelete = new ArrayList();

            foreach (OperatorClientData oper in listBoxNotAssigned.SelectedItems)
            {
                if (buttonExApply.Enabled == false)
                    buttonExApply.Enabled = true;
                toDelete.Add(oper);
                listBoxAssigned.Items.Add(oper);

                OperatorTrainingCourseClientData operTraining = new OperatorTrainingCourseClientData();
                operTraining.OperatorCode = oper.Code;
                operTraining.OperatorFirstName = oper.FirstName;
                operTraining.OperatorLastName = oper.LastName;
                operTraining.CourseScheduleCode = selectedCourseSchedule.Code;

                if (operatorsAssigned.ContainsKey(selectedCourseSchedule.Code) == true)
                {
                    IList list = operatorsAssigned[selectedCourseSchedule.Code];
                    list.Add(operTraining);
                    operatorsAssigned[selectedCourseSchedule.Code] = list;
                
                }

            }

            foreach (OperatorClientData oper in toDelete)
            {
                listBoxNotAssigned.Items.Remove(oper);
            }

            EnableDisable_AcceptButton();
        }


        private void buttonRemoveOperator_Click(object sender, EventArgs e)
        {
            ArrayList toDelete = new ArrayList();

            foreach (OperatorClientData oper in listBoxAssigned.SelectedItems)
            {
                if (buttonExApply.Enabled == false)
                    buttonExApply.Enabled = true;
                toDelete.Add(oper);
                listBoxNotAssigned.Items.Add(oper);

                IList list = operatorsAssigned[selectedCourseSchedule.Code];

                for (int i = 0; i < list.Count; i++)
                {
                    OperatorTrainingCourseClientData operTraining = list[i] as OperatorTrainingCourseClientData;
                    if (oper.Code == operTraining.OperatorCode)
                    {
                        list.Remove(operTraining);
                        break;
                    }
                }

                operatorsAssigned[selectedCourseSchedule.Code] = list;

            }

            foreach (OperatorClientData oper in toDelete)
            {
                listBoxAssigned.Items.Remove(oper);
            }
            EnableDisable_AcceptButton();
        }

        private void buttonRemoveOperatorConflict(IList Operators)
        {
            ArrayList toDelete = new ArrayList();

            foreach (OperatorClientData oper in Operators)
            {
                if (buttonExOk.Enabled == false)
                    buttonExOk.Enabled = true;
                toDelete.Add(oper);
                listBoxNotAssigned.Items.Add(oper);

                IList list = operatorsAssigned[selectedCourseSchedule.Code];

                for (int i = 0; i < list.Count; i++)
                {
                    OperatorTrainingCourseClientData operTraining = list[i] as OperatorTrainingCourseClientData;
                    if (oper.Code == operTraining.OperatorCode)
                    {
                        list.Remove(operTraining);
                        break;
                    }
                }

                operatorsAssigned[selectedCourseSchedule.Code] = list;

            }

            foreach (OperatorClientData oper in toDelete)
            {
                listBoxAssigned.Items.Remove(oper);
            }
            EnableDisable_AcceptButton();
            operatorsTrainingToDeleteConflict.Clear();
        }

        private void buttonAddAllOperators_Click(object sender, EventArgs e)
        {
            foreach (OperatorClientData oper in listBoxNotAssigned.Items)
            {
                if (buttonExApply.Enabled == false)
                    buttonExApply.Enabled = true;
                listBoxAssigned.Items.Add(oper);

                OperatorTrainingCourseClientData operTraining = new OperatorTrainingCourseClientData();
                operTraining.OperatorCode = oper.Code;
                operTraining.OperatorFirstName = oper.FirstName;
                operTraining.OperatorLastName = oper.LastName;
                operTraining.CourseScheduleCode = selectedCourseSchedule.Code;

                if (operatorsAssigned.ContainsKey(selectedCourseSchedule.Code) == true)
                {
                    IList list = operatorsAssigned[selectedCourseSchedule.Code];
                    list.Add(operTraining);
                    operatorsAssigned[selectedCourseSchedule.Code] = list;
                }
            }
            listBoxNotAssigned.Items.Clear();
            EnableDisable_AcceptButton();
        }

        private void buttonRemoveAllOperators_Click(object sender, EventArgs e)
        {

            ArrayList toDelete = new ArrayList();
            foreach (OperatorClientData oper in listBoxAssigned.Items)
            {
                if (buttonExApply.Enabled == false)
                    buttonExApply.Enabled = true;
                listBoxNotAssigned.Items.Add(oper);
                toDelete.Add(oper);
                      
                IList list = operatorsAssigned[selectedCourseSchedule.Code];

                for (int i = 0; i < list.Count; i++)
                {
                    OperatorTrainingCourseClientData operTraining = list[i] as OperatorTrainingCourseClientData;
                    if (oper.Code == operTraining.OperatorCode)
                    {
                        list.Remove(operTraining);
                        break;
                    }
                }

                operatorsAssigned[selectedCourseSchedule.Code] = list;

            }

            foreach (OperatorClientData oper in toDelete)
            {
                listBoxAssigned.Items.Remove(oper);
            }

            EnableDisable_AcceptButton();
        }

        private void listBoxNotAssigned_Enter(object sender, EventArgs e)
        {
            listBoxAssigned.ClearSelected();
            EnableDisable_AcceptButton();
        }


        private void listBoxAssigned_Enter(object sender, EventArgs e)
        {
            listBoxNotAssigned.ClearSelected();
            EnableDisable_AcceptButton();
        }

        private void listBoxNotAssigned_DoubleClick(object sender, EventArgs e)
        {
            if (this.buttonAddOperator.Enabled == true)
            {
                buttonAddOperator_Click(sender, e);
                EnableDisable_AcceptButton();
            }
        }


        private void listBoxAssigned_DoubleClick(object sender, EventArgs e)
        {
            if (this.buttonRemoveOperator.Enabled == true)
            {
                buttonRemoveOperator_Click(sender, e);
                EnableDisable_AcceptButton();
            }
        }

        private void EnableDisable_AcceptButton()
        {
            // Check listBoxAssigned
            if (listBoxAssigned.Items.Count > 0) buttonExOk.Enabled = true;
            else buttonExOk.Enabled = false;
        }
    }
}
