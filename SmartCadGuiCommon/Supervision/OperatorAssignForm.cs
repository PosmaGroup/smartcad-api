using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Collections;
using Smartmatic.SmartCad.Service;
using System.Linq;
using DevExpress.XtraGrid.Views.Base;
using SmartCadCore.ClientData;
using SmartCadControls.SyncBoxes;
using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadControls;
using SmartCadControls.Controls;
using SmartCadGuiCommon.Controls;

namespace SmartCadGuiCommon
{
    public partial class OperatorAssignForm : XtraForm
    {
        private string supervisedApplicationName;
        private DepartmentTypeClientData selectedDepartment;
        private OperatorFilter filter = OperatorFilter.All;
        private object syncCommited = new object();
        private GridControlSynBox supervisorsSyncBox;
        private Dictionary<int, WorkShiftVariationClientData> listWorkShifts = new Dictionary<int, WorkShiftVariationClientData>();
        Timer timerUpdateInformation;
        private int timeToDelete = 0;
        IList groupRowExpanded = new ArrayList();
        IList groupRowCollapsed = new ArrayList();

        public event EventHandler<CommittedObjectDataCollectionEventArgs> OperatorAssignCommittedChanges;

        public enum OperatorFilter
        {
            None = 0,
            Partial = 1,
            Total = 2,
            All = 3
        }

        public OperatorAssignForm()
        {
            InitializeComponent();
            InitializeDataGrids();
            LoadLanguage();
        }
                
        public OperatorAssignForm(string supervisedApplicationName, DepartmentTypeClientData department, int timeToDeleteSchedules)
            : this()
        {
            this.supervisedApplicationName = supervisedApplicationName;
            SelectedDepartment = department;
            FillOperatorsAndAppointments();
            radioButtonNotSupervised.Checked = true;
            this.timeToDelete = timeToDeleteSchedules;
        }

        private void OperatorAssignForm_Load(object sender, EventArgs e)
        {
            gridViewExOperators.GroupRowExpanded += new RowEventHandler(gridViewExOperators_GroupRowExpanded);
            gridViewExOperators.GroupRowCollapsed += new RowEventHandler(gridViewExOperators_GroupRowCollapsed);
            ServerServiceClient.GetInstance().CommittedChanges +=new EventHandler<CommittedObjectDataCollectionEventArgs>(OperatorAssignForm_CommittedChanges);
            schedulerControl.SelectedAppointmentChanged += new OperatorAssignSchedulerControl.SelectedAppointment_Changed(schedulerControl_SelectedAppointmentChanged);
            schedulerControl.CustomMenu_Click += new OperatorAssignSchedulerControl.CustomMenuClick(schedulerControl_CustomMenu_Click);
            supervisorsSyncBox = new GridControlSynBox(gridControlExSupervisors);

            timerUpdateInformation = new Timer();
            timerUpdateInformation.Tick +=new EventHandler(timerUpdateInformation_Tick);
            timerUpdateInformation.Interval = 10000;
            timerUpdateInformation.Start();
        }
       

        public DepartmentTypeClientData SelectedDepartment
        {
            get
            {
                return this.selectedDepartment;
            }
            set
            {
                if (value == null)
                {
                    selectedDepartment = new DepartmentTypeClientData();
                }
                else
                {
                    selectedDepartment = value as DepartmentTypeClientData;
                }
            }
        }

        private void InitializeDataGrids()
        {
            gridControlExOperators.Type = typeof(GridControlDataOperatorEx);
            gridControlExOperators.ViewTotalRows = true;
            gridControlExOperators.EnableAutoFilter = true;
            gridViewExOperators.ViewTotalRows = true;
            gridViewExOperators.GroupFormat = "{1}";

            gridControlExAssignedOperators.Type = typeof(GridControlDataOperatorEx);
            gridControlExAssignedOperators.ViewTotalRows = true;
            gridControlExAssignedOperators.EnableAutoFilter = true;
            gridViewExAssignedOperators.ViewTotalRows = true;
            gridViewExAssignedOperators.GroupFormat = "{1}";

            gridControlExSupervisors.Type = typeof(GridControlDataSupervisor);
            gridControlExSupervisors.ViewTotalRows = true;
            gridControlExSupervisors.EnableAutoFilter = true;
            gridViewExSupervisors.ViewTotalRows = true;
            gridViewExSupervisors.Columns["FirstName"].SortIndex = 0;

        }

        private void LoadLanguage()
        {
            RibbonPageOperation.Text = ResourceLoader.GetString2("Operation");
            ribbonPageGroupAssignment.Text = ResourceLoader.GetString2("AssignmentByWorkShift");
            this.barButtonItemAssign.Caption = ResourceLoader.GetString2("Assign");
            this.barButtonItemRemove.Caption = ResourceLoader.GetString2("Delete");
            this.barButtonItemOpen.Caption = ResourceLoader.GetString2("Details");
            this.Text = ResourceLoader.GetString2("Assignment");
            layoutControlGroupOperators.Text = ResourceLoader.GetString2("Operators");
            layoutControlGroupSupervisors.Text = ResourceLoader.GetString2("Supervisors");
            layoutControlGroupAssignedOperators.Text = ResourceLoader.GetString2("AssignedOperators");

            radioButtonAll.Text = ResourceLoader.GetString2("All");
            radioButtonNotSupervised.Text = ResourceLoader.GetString2("NotSupervised");
            radioButtonPartially.Text = ResourceLoader.GetString2("PartiallySupervised");
            radioButtonSupervised.Text = ResourceLoader.GetString2("Supervised");
        }

        void gridViewExOperators_GroupRowCollapsed(object sender, RowEventArgs e)
        {
            groupRowExpanded.Remove(e.RowHandle);

            if (groupRowCollapsed.Contains(e.RowHandle) == false)
                groupRowCollapsed.Add(e.RowHandle);
   
        }
      
        void gridViewExOperators_GroupRowExpanded(object sender, RowEventArgs e)
        {
            if (groupRowExpanded.Contains(e.RowHandle) == false)
                groupRowExpanded.Add(e.RowHandle);

            groupRowCollapsed.Remove(e.RowHandle);
        }

        void timerUpdateInformation_Tick(object sender, EventArgs e)
        {
            FormUtil.InvokeRequired(this, new MethodInvoker(UpdateCriteria));
        }


        void UpdateCriteria()
        {
            DateTime currentDate = ServerServiceClient.GetInstance().GetTimeFromDB();

            //Elimino los operadores cuyo workShift ya termino
            RemoveExpiredWorkShift(currentDate);

            //Elimino los appointments cuyo workShift ya termino
            schedulerControl.RemoveExpiredAppointments(currentDate, timeToDelete);                                                                                                        
                   
        }

        private void RemoveExpiredWorkShift(DateTime currentDate)
        {

            int count = 0;
            List<WorkShiftVariationClientData> wsToDelete = new List<WorkShiftVariationClientData>();

            foreach (WorkShiftVariationClientData ws in listWorkShifts.Values)
            {
                count = ws.Schedules.Cast<WorkShiftScheduleVariationClientData>().Count(sc => sc.End.AddMinutes(timeToDelete) > currentDate);

                if (count == 0)
                    wsToDelete.Add(ws);
            }

            if (wsToDelete.Count > 0)
            {
                //Elimino el workShift de la lista q esta en memoria
                foreach (WorkShiftVariationClientData ws in wsToDelete)
                    listWorkShifts.Remove(ws.Code);

                //Elimino los operadores del workShift que termino
                if (gridControlExOperators.Items.Count > 0)
                {
                    IList operators = ((BindingList<GridControlDataOperatorEx>)gridControlExOperators.DataSource).Where(gridData => wsToDelete.Contains(new WorkShiftVariationClientData() { Code = gridData.WorkShiftCode })).ToList();

                    foreach (GridControlDataOperatorEx gridData in operators)
                    {
                        gridControlExOperators.DeleteItem(gridData);
                    }
                }

                //Elimino o actualizo los supervisores del workShift que termino
                if (gridControlExSupervisors.Items.Count > 0)
                {
                    foreach (WorkShiftVariationClientData ws in wsToDelete)
                    {
                        IList supervisors = ((BindingList<GridControlDataSupervisor>)gridControlExSupervisors.DataSource).Where(gridData => gridData.WorkShifts != null && gridData.WorkShifts.Contains(ws)).ToList();

                        foreach (GridControlDataSupervisor gridData in supervisors)
                        {
                            if (gridData.WorkShifts.Count == 1)
                                gridControlExSupervisors.DeleteItem(gridData);
                            else
                            {
                                gridData.WorkShifts.Remove(ws);
                                gridControlExSupervisors.AddOrUpdateItem(gridData);
                            }
                        }
                    }

                }

            }
        }

        void OperatorAssignForm_CommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            
            if (e.Objects != null && e.Objects.Count > 0)
            {
           
                lock (syncCommited)
                {
                    try
                    {
                        if (ServerServiceClient.GetInstance().OperatorClient == null)
                        { }
                    }
                    catch
                    { }
                    if (e.Objects[0] is ApplicationPreferenceClientData)
                    {
                        #region ApplicationPreferenceClientData
                        ApplicationPreferenceClientData preference = e.Objects[0] as ApplicationPreferenceClientData;
                        if (e.Action == CommittedDataAction.Update)
                        {
                            if (preference.Name.Equals("TimeToDeleteWorkShift"))
                                timeToDelete = int.Parse(preference.Value);
                            
                        }
                        #endregion
                    }
                    else if (e.Objects[0] is OperatorCategoryClientData)
                    {
                        #region OperatorCategoryClientData
                        
                        FormUtil.InvokeRequired(this, delegate
                        {
                            if (gridControlExOperators.DataSource != null)
                            {
                                IList operators = GetOperatorsOnGrid(gridControlExOperators);
                                IList operatorsToUpdate = new ArrayList();
                                foreach (GridControlData grid in operators)
                                {
                                    OperatorClientData ope = grid.Tag as OperatorClientData;
                                    foreach (OperatorCategoryClientData opeCategory in e.Objects)
                                    {
                                        if (ope.OperatorCategoryCode == opeCategory.Code)
                                        {
                                            ope.OperatorCategory = opeCategory.FriendlyName;
                                            operatorsToUpdate.Add(ope.Code);
                                        }
                                    }
                                }
                                if (operatorsToUpdate.Count > 0)
                                {
                                    operatorsToUpdate = SearchOperators(operatorsToUpdate);
                                    foreach (OperatorClientData oper in operatorsToUpdate)
                                    {
                                        gridControlExOperators.BeginUpdate();
                                        gridControlExOperators.DataSource = UpdateOperatorsGridControl(gridControlExOperators, oper, CommittedDataAction.Update);
                                        gridControlExOperators.EndUpdate();
                                    }
                                }
                            }

                            if (gridControlExAssignedOperators.DataSource != null)
                            {
                                IList operators = GetOperatorsOnGrid(gridControlExOperators);
                                IList operatorsToUpdate = new ArrayList();
                                foreach (GridControlData grid in operators)
                                {
                                    OperatorClientData ope = grid.Tag as OperatorClientData;
                                    foreach (OperatorCategoryClientData opeCategory in e.Objects)
                                    {
                                        if (ope.OperatorCategoryCode == opeCategory.Code)
                                        {
                                            ope.OperatorCategory = opeCategory.FriendlyName;
                                            operatorsToUpdate.Add(ope.Code);
                                        }
                                    }
                                }
                                if (operatorsToUpdate.Count > 0)
                                {
                                    operatorsToUpdate = SearchOperators(operatorsToUpdate);
                                    foreach (OperatorClientData oper in operatorsToUpdate)
                                    {
                                        gridControlExAssignedOperators.BeginUpdate();
                                        gridControlExAssignedOperators.DataSource = UpdateOperatorsGridControl(gridControlExAssignedOperators, oper, CommittedDataAction.Update);
                                        gridControlExAssignedOperators.EndUpdate();
                                    }
                                }
                            }
                        });
                        #endregion
                    }
                    else if (e.Objects[0] is OperatorClientData)
                    {
                        #region OperatorClientData
                        OperatorClientData oper = e.Objects[0] as OperatorClientData;

                        if (e.Action != CommittedDataAction.Save)
                        {
                            if (OperatorAssignCommittedChanges != null)
                                OperatorAssignCommittedChanges(null, e);


                            if (oper.WorkShifts != null && oper.WorkShifts.Count > 0)
                            {

                                if (ServerServiceClient.GetInstance().CheckOperatorClientAccess(oper, supervisedApplicationName) == false)
                                {
                                    supervisorsSyncBox.Sync(new GridControlDataSupervisor(oper), CommittedDataAction.Delete);

                                    FormUtil.InvokeRequired(this, delegate
                                    {
                                        if (gridControlExOperators.DataSource != null)
                                        {
                                            gridControlExOperators.BeginUpdate();
                                            gridControlExOperators.DataSource = UpdateOperatorsGridControl(gridControlExOperators, oper, CommittedDataAction.Delete);
                                            gridControlExOperators.EndUpdate();
                                        }

                                        if (gridControlExAssignedOperators.DataSource != null)
                                        {
                                            gridControlExAssignedOperators.BeginUpdate();
                                            gridControlExAssignedOperators.DataSource = UpdateOperatorsGridControl(gridControlExAssignedOperators, oper, CommittedDataAction.Delete);
                                            gridControlExAssignedOperators.EndUpdate();
                                        }

                                        schedulerControl.OperatorUpdate(oper, CommittedDataAction.Delete, false);

                                    });

                                }
                                else
                                {
                                    bool changeRole = false;
                                    if (oper.IsSupervisor == true)
                                    {

                                        List<WorkShiftVariationClientData> wsList = new List<WorkShiftVariationClientData>(oper.WorkShifts.OfType<WorkShiftVariationClientData>());
                                        if (gridControlExSupervisors.Items.Contains(new GridControlDataSupervisor(oper)) == true)
                                        {
                                            if (ServerServiceClient.GetInstance().CheckGeneralSupervisor(oper.RoleCode) == false)
                                            {
                                                //Add or update operator in supervisor's grid 
                                                supervisorsSyncBox.Sync(new GridControlDataSupervisor(oper, wsList), e.Action);
                                            }
                                            else
                                            {
                                                supervisorsSyncBox.Sync(new GridControlDataSupervisor(oper, wsList), CommittedDataAction.Delete);
                                            }
                                        }
                                        else
                                        {
                                            changeRole = true;

                                            //Add or update operator in supervisor's grid 
                                            if (ServerServiceClient.GetInstance().CheckGeneralSupervisor(oper.RoleCode) == false)
                                            {
                                                supervisorsSyncBox.Sync(new GridControlDataSupervisor(oper, wsList), e.Action);
                                            }

                                            if (ContainsOperator(oper) == true)
                                            {
                                                FormUtil.InvokeRequired(this, delegate
                                                {
                                                    if (gridControlExOperators.DataSource != null)
                                                    {
                                                        gridControlExOperators.BeginUpdate();
                                                        gridControlExOperators.DataSource = UpdateOperatorsGridControl(gridControlExOperators, oper, CommittedDataAction.Delete);
                                                        gridControlExOperators.EndUpdate();
                                                    }
                                                });
                                            }
                                        }

                                    }
                                    else
                                    {
                                        if (ContainsOperator(oper) == true)
                                        {
                                            FormUtil.InvokeRequired(this, delegate
                                            {
                                                if (gridControlExOperators.DataSource != null)
                                                {
                                                    gridControlExOperators.BeginUpdate();
                                                    gridControlExOperators.DataSource = UpdateOperatorsGridControl(gridControlExOperators, oper, e.Action);
                                                    gridControlExOperators.EndUpdate();
                                                }

                                                if (gridControlExAssignedOperators.DataSource != null)
                                                {
                                                    gridControlExAssignedOperators.BeginUpdate();
                                                    gridControlExAssignedOperators.DataSource = UpdateOperatorsGridControl(gridControlExAssignedOperators, oper, e.Action);
                                                    gridControlExAssignedOperators.EndUpdate();
                                                }

                                            });
                                        }
                                        else
                                        {
                                            changeRole = true;
                                            supervisorsSyncBox.Sync(new GridControlDataSupervisor(oper), CommittedDataAction.Delete);

                                            FormUtil.InvokeRequired(this, delegate
                                            {
                                                if (gridControlExOperators.DataSource != null)
                                                {
                                                    gridControlExOperators.BeginUpdate();
                                                    gridControlExOperators.DataSource = UpdateOperatorsGridControl(gridControlExOperators, oper, CommittedDataAction.Save);
                                                    gridControlExOperators.EndUpdate();
                                                }

                                            });

                                        }
                                    }

                                    FormUtil.InvokeRequired(this, delegate
                                    {
                                        schedulerControl.OperatorUpdate(oper, e.Action, changeRole);
                                    });
                                }
                            }
                        }

                        #endregion


                    }
                    else if (e.Objects[0] is OperatorAssignClientData)
                    {
                        #region OperatorAssignClientData

                        OperatorClientData oper;
                        BindingList<GridControlDataOperatorEx> operatorsDataSource = new BindingList<GridControlDataOperatorEx>();
                        BindingList<GridControlDataSupervisor> supervisorsDataSource = new BindingList<GridControlDataSupervisor>();
                        List<OperatorAssignClientData> assignsBySchedule;
                        int isSupervised = 0;


                        FormUtil.InvokeRequired(this, delegate
                        {
                            schedulerControl.SchedulerBeginUpdate();
                            foreach (OperatorAssignClientData operAssign in e.Objects)
                            {
                                operatorsDataSource = ((BindingList<GridControlDataOperatorEx>)gridControlExOperators.DataSource);

                                if (operatorsDataSource != null)
                                {

                                    for (int i = 0; i < operatorsDataSource.Count; i++)
                                    {
                                        GridControlDataOperatorEx gridData = operatorsDataSource[i] as GridControlDataOperatorEx;

                                        if (gridData.Code == operAssign.SupervisedOperatorCode && gridData.WorkShiftCode == operAssign.SupervisedScheduleVariation.WorkShiftVariationCode)
                                        {
                                            oper = new OperatorClientData();
                                            oper = gridData.Tag as OperatorClientData;

                                            if (oper.Supervisors == null)
                                                oper.Supervisors = new ArrayList();

                                            int index = oper.Supervisors.IndexOf(operAssign);

                                            if (index > -1)
                                            {
                                                if (e.Action == CommittedDataAction.Delete)
                                                    oper.Supervisors.RemoveAt(index);
                                                else
                                                    oper.Supervisors[index] = operAssign;
                                            }
                                            else
                                            {
                                                oper.Supervisors.Add(operAssign);
                                            }

                                            assignsBySchedule = GetAssignListBySchedule(oper.Supervisors, operAssign.SupervisedScheduleVariation.Code);
                                            isSupervised = HowIsSupervised(assignsBySchedule, operAssign.SupervisedScheduleVariation);

                                            operatorsDataSource[i] = new GridControlDataOperatorEx(oper, isSupervised, operAssign.SupervisedScheduleVariation.Code, gridData.WorkShiftName, gridData.WorkShiftCode, gridData.IsSupervisedList);

                                            schedulerControl.AssignmentUpdate(operAssign, isSupervised, e.Action, false);


                                            break;
                                        }

                                    }

                                }

                                supervisorsDataSource = ((BindingList<GridControlDataSupervisor>)gridControlExSupervisors.DataSource);

                                if (supervisorsDataSource != null)
                                {
                                    for (int i = 0; i < supervisorsDataSource.Count; i++)
                                    {
                                        GridControlDataSupervisor sGridData = supervisorsDataSource[i] as GridControlDataSupervisor;
                                        if (sGridData.Code == operAssign.SupervisorCode)
                                        {
                                            oper = new OperatorClientData();
                                            oper = sGridData.Tag as OperatorClientData;

                                            if (oper.Operators == null)
                                                oper.Operators = new ArrayList();

                                            int index = oper.Operators.IndexOf(operAssign);


                                            if (index > -1)
                                            {
                                                if (e.Action == CommittedDataAction.Delete)
                                                    oper.Operators.RemoveAt(index);
                                                else
                                                    oper.Operators[index] = operAssign;
                                            }
                                            else
                                            {
                                                oper.Operators.Add(operAssign);
                                            }


                                            assignsBySchedule = GetAssignListBySchedule(oper.Operators, operAssign.SupervisedScheduleVariation.Code);
                                            if (assignsBySchedule.Count > 0)
                                                isSupervised = 2;
                                            else
                                                isSupervised = 0;

                                            supervisorsDataSource[i] = new GridControlDataSupervisor(oper, sGridData.WorkShifts);

                                            schedulerControl.AssignmentUpdate(operAssign, isSupervised, e.Action, true);

                                            break;
                                        }

                                    }

                                }
                            }

                            schedulerControl.SchedulerEndUpdate();
                        });

                        FormUtil.InvokeRequired(this, delegate
                        {
                            layoutControlGroupScheduler.Text = ResourceLoader.GetString2("ProgramFrom") + ":";
                            if (gridControlExSupervisors.SelectedItems.Count > 0 && gridControlExAssignedOperators.DataSource != null)
                                gridViewExSupervisors_SelectionWillChange(gridViewExSupervisors, new FocusedRowChangedEventArgs(gridViewExSupervisors.FocusedRowHandle, gridViewExSupervisors.FocusedRowHandle));

                            if (gridControlExOperators.SelectedItems.Count > 0 && gridControlExOperators.DataSource != null)
                                gridViewExOperators_SelectionWillChange(gridViewExOperators, new FocusedRowChangedEventArgs(gridViewExOperators.FocusedRowHandle, gridViewExOperators.FocusedRowHandle));

                        });


                        // radioButtonFilterOperators_CheckedChanged(null, null);

                        #endregion
                    }
                    else if (e.Objects[0] is WorkShiftVariationClientData)
                    {
                        #region WorkShiftVariationClientData

                        WorkShiftVariationClientData var = e.Objects[0] as WorkShiftVariationClientData;

                        if (var.Type != WorkShiftVariationClientData.WorkShiftType.TimeOff)
                        {
                            bool isGroupRow = false;
                            gridViewExSupervisors.SelectionWillChange -= new FocusedRowChangedEventHandler(gridViewExSupervisors_SelectionWillChange);
                            gridViewExOperators.SelectionWillChange -= new FocusedRowChangedEventHandler(gridViewExOperators_SelectionWillChange);
                            GridControlDataOperatorEx gridOperatorsSelectedItem = null;
                            GridControlDataSupervisor gridSupervisorsSelectedItem = null;

                            if (gridControlExOperators.DataSource != null && gridControlExOperators.SelectedItems.Count > 0)
                            {
                                if (gridViewExOperators.FocusedRowHandle < 0)
                                    isGroupRow = true;

                                gridOperatorsSelectedItem = gridControlExOperators.SelectedItems[0] as GridControlDataOperatorEx;
                            }
                            if (gridControlExSupervisors.SelectedItems.Count > 0 && gridControlExSupervisors.DataSource != null)
                                gridSupervisorsSelectedItem = gridControlExSupervisors.SelectedItems[0] as GridControlDataSupervisor;

                            if (OperatorAssignCommittedChanges != null)
                                OperatorAssignCommittedChanges(null, e);

                            FormUtil.InvokeRequired(this, delegate
                            {
                                BindingList<GridControlDataOperatorEx> operatorsDataSource = (BindingList<GridControlDataOperatorEx>)gridControlExOperators.DataSource;
                                BindingList<GridControlDataSupervisor> supervisorsDataSource = (BindingList<GridControlDataSupervisor>)gridControlExSupervisors.DataSource;

                                //Elimino los operadores de grid
                                IList operatorsToDeleteIndexes = new ArrayList();
                                for (int i = 0; i < operatorsDataSource.Count; i++)
                                {
                                    if (((GridControlDataOperatorEx)operatorsDataSource[i]).WorkShiftCode == var.Code)
                                        operatorsToDeleteIndexes.Add(i);
                                }

                                for (int i = operatorsToDeleteIndexes.Count - 1; i >= 0; i--)
                                    operatorsDataSource.RemoveAt((int)operatorsToDeleteIndexes[i]);


                                //Elimino los operadores de grid
                                IList supervisorsToDeleteIndexes = new ArrayList();
                                List<WorkShiftVariationClientData> workShifts = null;
                                for (int i = 0; i < supervisorsDataSource.Count; i++)
                                {
                                    workShifts = ((GridControlDataSupervisor)supervisorsDataSource[i]).WorkShifts;

                                    if (workShifts != null && workShifts.Contains(var) == true)
                                    {
                                        if (workShifts.Count == 1)
                                            supervisorsToDeleteIndexes.Add(i);
                                        else if (workShifts.Count > 1)
                                        {
                                            workShifts.Remove(var);
                                            supervisorsDataSource[i] = new GridControlDataSupervisor(supervisorsDataSource[i].Operator, workShifts);
                                        }
                                    }

                                }

                                for (int i = supervisorsToDeleteIndexes.Count - 1; i >= 0; i--)
                                    supervisorsDataSource.RemoveAt((int)supervisorsToDeleteIndexes[i]);



                                //Elimino los appointments
                                schedulerControl.RemoveAppointmentByWorkShift(var.Code);
                                listWorkShifts.Remove(var.Code);

                                if (e.Action == CommittedDataAction.Update)
                                {
                                    var.Operators = new ArrayList();
                                    var.Operators = GetOperators(var.Code);

                                    List<OperatorAssignClientData> assignList;
                                    Dictionary<int, int> list;
                                    int isSupervised;
                                    schedulerControl.FillListOperators(var.Operators);

                                    foreach (OperatorClientData oper in var.Operators)
                                    {
                                        schedulerControl.SchedulerBeginUpdate();

                                        if (oper.IsSupervisor == false)
                                        {
                                            list = new Dictionary<int, int>();
                                            foreach (WorkShiftScheduleVariationClientData schedule in var.Schedules)
                                            {
                                                assignList = GetAssignListBySchedule(oper.Supervisors, schedule.Code);
                                                isSupervised = HowIsSupervised(assignList, schedule);
                                                list.Add(schedule.Code, isSupervised);
                                                schedulerControl.CreateAppointmentByWorkShift(oper, var, schedule, assignList, isSupervised, false);
                                            }

                                            operatorsDataSource.Add(new GridControlDataOperatorEx(oper, var, list));
                                        }
                                        else
                                        {
                                            foreach (WorkShiftScheduleVariationClientData schedule in var.Schedules)
                                            {
                                                assignList = GetAssignListBySchedule(oper.Operators, schedule.Code);
                                                if (assignList.Count > 0)
                                                    isSupervised = 2;
                                                else
                                                    isSupervised = 0;
                                                schedulerControl.CreateAppointmentByWorkShift(oper, var, schedule, assignList, isSupervised, true);
                                            }

                                            int indexSuper = supervisorsDataSource.IndexOf(new GridControlDataSupervisor(oper));
                                            List<WorkShiftVariationClientData> wsList = new List<WorkShiftVariationClientData>();

                                            if (indexSuper < 0)
                                            {
                                                wsList.Add(var);
                                                supervisorsDataSource.Add(new GridControlDataSupervisor(oper, wsList));
                                            }
                                            else
                                            {
                                                wsList = supervisorsDataSource[indexSuper].WorkShifts;
                                                wsList.Add(var);
                                                supervisorsDataSource[indexSuper] = new GridControlDataSupervisor(oper, wsList);
                                            }

                                        }

                                        schedulerControl.SchedulerEndUpdate();

                                    }

                                    listWorkShifts.Add(var.Code, var);
                                }


                            });


                            gridViewExSupervisors.SelectionWillChange += new FocusedRowChangedEventHandler(gridViewExSupervisors_SelectionWillChange);
                            gridViewExOperators.SelectionWillChange += new FocusedRowChangedEventHandler(gridViewExOperators_SelectionWillChange);


                            FormUtil.InvokeRequired(this, delegate
                            {
                                //Expande los grupos que estaba expandidos antes del cambio
                                ExpandGroups();

                                int indexSuper = ((BindingList<GridControlDataSupervisor>)gridViewExSupervisors.DataSource).IndexOf(gridSupervisorsSelectedItem);
                                if (indexSuper >= 0)
                                {
                                    gridViewExSupervisors.FocusedRowHandle = GridControlEx.AutoFilterRowHandle;
                                    gridViewExSupervisors.FocusedRowHandle = gridViewExSupervisors.GetRowHandle(indexSuper);
                                }
                                else
                                {
                                    gridViewExSupervisors.FocusedRowHandle = GridControlEx.AutoFilterRowHandle;
                                }

                                int indexOper = ((BindingList<GridControlDataOperatorEx>)gridViewExOperators.DataSource).IndexOf(gridOperatorsSelectedItem);
                                if (indexOper >= 0)
                                {
                                    int visibleIndex = 0;
                                    if (isGroupRow == true)
                                    {
                                        gridViewExOperators.SelectionWillChange -= new FocusedRowChangedEventHandler(gridViewExOperators_SelectionWillChange);
                                        gridViewExOperators.FocusedRowHandle = gridViewExOperators.GetRowHandle(indexOper);
                                        gridViewExOperators.SelectionWillChange += new FocusedRowChangedEventHandler(gridViewExOperators_SelectionWillChange);

                                        visibleIndex = gridViewExOperators.GetVisibleIndex(gridViewExOperators.GetRowHandle(indexOper));
                                        gridViewExOperators.FocusedRowHandle = gridViewExOperators.GetVisibleRowHandle(visibleIndex - 1);
                                    }
                                    else
                                    {
                                        gridViewExOperators.FocusedRowHandle = gridViewExOperators.GetRowHandle(indexOper);
                                    }
                                }
                                else
                                    gridViewExOperators.FocusedRowHandle = GridControlEx.AutoFilterRowHandle;

                                CollapseGroups();

                            });



                        }

                        #endregion
                    }                


                }
            }
        }

        private void ExpandGroups() 
        {
            foreach (int rowHandle in groupRowExpanded)
            {
                this.gridViewExOperators.ExpandGroupRow(rowHandle);    
            }
        
        }

        private void CollapseGroups()
        {
            foreach (int rowHandle in groupRowCollapsed)
            {
                this.gridViewExOperators.CollapseGroupRow(rowHandle);
            }

        }

        //private void RemoveOperator(OperatorClientData oper)
        //{
        //    supervisorsSyncBox.Sync(new GridControlDataSupervisor(oper), CommittedDataAction.Delete); 
        //    SyncOperator(oper, CommittedDataAction.Delete);

        //    FormUtil.InvokeRequired(this, delegate
        //    {
        //        schedulerControl.RemoveAppointmentByOperator(oper.Code);
        //    });
        //}

        //private void SyncOperator(OperatorClientData oper, CommittedDataAction action)
        //{
        //    FormUtil.InvokeRequired(this, delegate
        //        {
        //            if (action == CommittedDataAction.Save)
        //            {                    
        //                BindingList<GridControlDataOperatorEx> operatorsDataSource = (BindingList<GridControlDataOperatorEx>)gridControlExOperators.DataSource;
        //                foreach (WorkShiftVariationClientData wsv in oper.WorkShifts)
        //                {
        //                    Dictionary<int, int> list = new Dictionary<int, int>();
        //                    foreach (WorkShiftScheduleVariationClientData schedule in wsv.Schedules)
        //                    {
        //                        List<OperatorAssignClientData> assignList = GetAssignListBySchedule(oper.Supervisors, schedule.Code);
        //                        int isSupervised = HowIsSupervised(assignList, schedule);
        //                        list.Add(schedule.Code, isSupervised);
        //                    }
        //                    operatorsDataSource.Add(new GridControlDataOperatorEx(oper, wsv, list));
        //                }

        //            }
        //            else
        //            {
        //                if (gridControlExOperators.DataSource != null)
        //                {
        //                    gridControlExOperators.BeginUpdate();
        //                    gridControlExOperators.DataSource = UpdateOperatorsGridControl(gridControlExOperators, oper, action);
        //                    gridControlExOperators.EndUpdate();
        //                }

        //                if (gridControlExAssignedOperators.DataSource != null)
        //                {
        //                    gridControlExAssignedOperators.BeginUpdate();
        //                    gridControlExAssignedOperators.DataSource = UpdateOperatorsGridControl(gridControlExAssignedOperators, oper, action);
        //                    gridControlExAssignedOperators.EndUpdate();
        //                }
        //            }
        //        });
        //}

        private bool ContainsOperator(OperatorClientData oper)
        {
            foreach (GridControlDataOperatorEx gcdpe in gridControlExOperators.Items)
            {
                if (gcdpe.Code == oper.Code)
                    return true;
            }
            return false;
        }

        private IList SearchOperators(IList operators)
        {
            IList result = new ArrayList();
            StringBuilder builder = new StringBuilder();
            builder.Append("(");

            foreach (int operCode in operators)
            {
                builder.Append(operCode);
                builder.Append(",");
            }

            builder.Remove(builder.Length - 1, 1);
            builder.Append(")");

            string hql = @"SELECT oper FROM OperatorData oper WHERE oper.Code IN {0}";

            result = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(hql, builder.ToString()));

            return result;
        }

        private IList GetOperators(int wsCode)
        {
            IList operators = new ArrayList();


            if (supervisedApplicationName == UserApplicationClientData.FirstLevel.Name)
            {
                operators = ServerServiceClient.GetInstance().SearchClientObjects(
                                             SmartCadHqls.GetCustomHql(
                                             @"SELECT ope 
                                               FROM OperatorData ope
                                                 left join fetch ope.Operators assigns
                                                 inner join ope.WorkShifts ws
                                                 inner join ope.Role role
                                                 inner join role.Profiles profile
                                                 inner join profile.Accesses access
                                             WHERE (access.Name like '%FirstLevelSupervisor%' 
                                                  OR access.Name like '%UserApplicationData+Basic+FirstLevel%')
                                                  AND ws.WorkShift.Code = {0}
                                                  AND ope.Code not in (SELECT distinct opeAux.Code 
                                                                       FROM OperatorData opeAux
                                                                             inner join opeAux.Role roleAux
                                                                             inner join roleAux.Profiles profileAux
                                                                             inner join profileAux.Accesses accessAux
                                                                       WHERE accessAux.Name like '%GeneralSupervisorName%')", wsCode));

            }
            else if (supervisedApplicationName == UserApplicationClientData.Dispatch.Name)
            {
                operators = ServerServiceClient.GetInstance().SearchClientObjects(
                                             SmartCadHqls.GetCustomHql(
                                             @"SELECT ope FROM OperatorData ope
                                                 left join fetch ope.Operators assigns
                                                 inner join ope.WorkShifts ws
                                                 inner join ope.DepartmentTypes dep
                                                 inner join ope.Role role
                                                 inner join role.Profiles profile
                                                 inner join profile.Accesses access
                                             WHERE (access.Name like '%DispatchSupervisor%'
                                                    OR access.Name like '%UserApplicationData+Basic+Dispatch%')
                                                 AND dep.Code = {0} and ws.WorkShift.Code = {1}
                                                 AND ope.Code not in (SELECT distinct opeAux.Code FROM OperatorData opeAux
                                                                             inner join opeAux.Role roleAux
                                                                             inner join roleAux.Profiles profileAux
                                                                             inner join profileAux.Accesses accessAux
                                                                      WHERE accessAux.Name like '%GeneralSupervisorName%')",
                                              selectedDepartment.Code, wsCode));
            }



            return operators;

        }

//        private ArrayList GetOperators(int wsCode)  
//        {
//            ArrayList operators = new ArrayList();

//            operators = (ArrayList)GetOperatorsWorkShift(wsCode);

//            operators.AddRange(GetSupervisorsWorkShift(wsCode));

//            return operators;
//        }

//        private IList GetOperatorsWorkShift(int wsCode) 
//        {
//            string hql = string.Empty;
//            IList operators = null;

//            if (supervisedApplicationName == UserApplicationClientData.FirstLevel.Name)
//            {
//                hql = @"SELECT ope FROM OperatorData ope
//                    left join fetch ope.Supervisors assigns
//                    left join ope.WorkShifts ws
//                    left join ope.DepartmentTypes dep 
//                    left join ope.Role.Profiles profiles 
//                    left join profiles.Accesses access 
//                    WHERE ws.WorkShift.Code = {0} and access.Name like '%UserApplicationData+Basic+FirstLevel%'";

//                operators = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(hql, wsCode));

//            }
//            else if (supervisedApplicationName == UserApplicationClientData.Dispatch.Name)
//            {
//                hql = @"SELECT ope FROM OperatorData ope
//                        left join fetch ope.Supervisors assigns
//                        left join ope.WorkShifts ws
//                        left join ope.DepartmentTypes dep 
//                        left join ope.Role.Profiles profiles 
//                        left join profiles.Accesses access 
//                        WHERE ws.WorkShift.Code = {0} and access.Name like '%UserApplicationData+Basic+Dispatch%' 
//                        and dep.Code = {1}";

//                operators = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(hql, wsCode, selectedDepartment.Code));

//            }          

//            return operators;
        
//        }

//        private IList GetSupervisorsWorkShift(int wsCode)
//        {
//            IList supers = null;
            

//            if (supervisedApplicationName == UserApplicationClientData.FirstLevel.Name)
//            {
//                supers = ServerServiceClient.GetInstance().SearchClientObjects(
//                                             SmartCadHqls.GetCustomHql(
//                                             "select ope from OperatorData ope " +
//                                                 "left join fetch ope.Operators assigns " +
//                                                 "left join ope.WorkShifts ws " +
//                                                  "left join ope.Role role " +
//                                                 "left join role.Profiles profile " +
//                                                 "left join profile.Accesses access " +
//                                             "where (access.Name like '%FirstLevelSupervisor%' OR access.Name like '%UserApplicationData+Basic+FirstLevel%') " +
//                                                 "and ws.WorkShift.Code = {0} " +
//                                                 "and ope.Code not in (select distinct opeAux.Code from OperatorData opeAux " +
//                                                                             "left join opeAux.Role roleAux " +
//                                                                             "left join roleAux.Profiles profileAux " +
//                                                                             "left join profileAux.Accesses accessAux " +
//                                                                       "where accessAux.Name like '%GeneralSupervisorName%')", wsCode));

//            }
//            else if (supervisedApplicationName == UserApplicationClientData.Dispatch.Name)
//            {
//                supers = ServerServiceClient.GetInstance().SearchClientObjects(
//                                             SmartCadHqls.GetCustomHql(
//                                             "select ope from OperatorData ope " +
//                                                 "left join fetch ope.Operators assigns " +
//                                                 "left join ope.WorkShifts ws " +
//                                                 "left join ope.DepartmentTypes dep " +
//                                                 "left join ope.Role role " +
//                                                 "left join role.Profiles profile " +
//                                                 "left join profile.Accesses access " +
//                                             "where access.Name like '%DispatchSupervisor%' " +
//                                                 "and dep.Code = {0} and ws.WorkShift.Code = {1} " +
//                                                 "and ope.Code not in (select distinct opeAux.Code from OperatorData opeAux " +
//                                                                             "left join opeAux.Role roleAux " +
//                                                                             "left join roleAux.Profiles profileAux " +
//                                                                             "left join profileAux.Accesses accessAux " +
//                                                                       "where accessAux.Name like '%GeneralSupervisorName%')",
//                                              selectedDepartment.Code, wsCode));
//            }

            

//            return supers;

//        }

        private BindingList<GridControlDataOperatorEx> UpdateOperatorsGridControl(GridControlEx gridControl, OperatorClientData oper, CommittedDataAction action)
        {
            BindingList<GridControlDataOperatorEx> dataSource = new BindingList<GridControlDataOperatorEx>();
            GridControlDataOperatorEx data = null;
            GridControlDataOperatorEx gridData;
            ArrayList indexes = new ArrayList();
            Dictionary<int, int> list = null;
            
            dataSource = ((BindingList<GridControlDataOperatorEx>)gridControl.DataSource);
            
            if (action == CommittedDataAction.Save)
            {
                foreach (WorkShiftVariationClientData ws in oper.WorkShifts)
                {                    
                    list = new Dictionary<int, int>();
                    foreach (WorkShiftScheduleVariationClientData schedule in ws.Schedules)
                        list.Add(schedule.Code, 0);                       
                    
                    dataSource.Add(new GridControlDataOperatorEx(oper, ws, list));
                }
            }
            else
            {

                for (int i = 0; i < dataSource.Count; i++)
                {
                    gridData = dataSource[i] as GridControlDataOperatorEx;

                    if (gridData.Code == oper.Code)
                        indexes.Add(i);
                }

                if (action == CommittedDataAction.Update)
                {
                    for (int i = 0; i < indexes.Count; i++)
                    {
                        data = new GridControlDataOperatorEx(oper);
                        gridData = dataSource[(int)indexes[i]];
                        data.IsNotSupervised = gridData.IsNotSupervised;
                        data.IsPartiallySupervised = gridData.IsPartiallySupervised;
                        data.IsTotallySupervised = gridData.IsTotallySupervised;
                        data.WorkShiftName = gridData.WorkShiftName;
                        data.WorkShiftCode = gridData.WorkShiftCode;
                        data.IsSupervisedList = gridData.IsSupervisedList;
                        dataSource[(int)indexes[i]] = data;
                    }
                }
                else if (action == CommittedDataAction.Delete)
                {
                    for (int i = 0; i < indexes.Count; i++)
                        dataSource.RemoveAt((int)indexes[i]);
                }
            }
            return dataSource;
        }

        private void FillOperatorsAndAppointments()
        {
            IList workShifts = null;
            List<OperatorAssignClientData> assignList = new List<OperatorAssignClientData>();
            int isSupervised;
            Dictionary<int, int> list;
            BindingList<GridControlDataOperatorEx> operatorsDataSource = new BindingList<GridControlDataOperatorEx>();
            DateTime currentDate = ServerServiceClient.GetInstance().GetTimeFromDB();
            Dictionary<OperatorClientData, List<WorkShiftVariationClientData>> superWorkShift = new Dictionary<OperatorClientData, List<WorkShiftVariationClientData>>();
            List<WorkShiftVariationClientData> wsList;

            Dictionary<string, IList> querys = new Dictionary<string, IList>();
            querys["WorkShifts"] = new ArrayList();

            querys["WorkShifts"].Add(SmartCadHqls.GetCustomHql(SmartCadHqls.GetWorkShiftWithNotExpiredSchedules,
                                                                (int)WorkShiftVariationClientData.WorkShiftType.TimeOff,
                                                                ApplicationUtil.GetDataBaseFormattedDate(currentDate.AddMinutes(timeToDelete * -1))));

            querys["WorkShifts"].Add(SmartCadHqls.GetCustomHql(SmartCadHqls.GetWorkShiftWithOperatorsAssigns,
                                                                 (int)WorkShiftVariationClientData.WorkShiftType.TimeOff,
                                                                 ApplicationUtil.GetDataBaseFormattedDate(currentDate.Date)));

            querys["WorkShifts"].Add(SmartCadHqls.GetCustomHql(SmartCadHqls.GetWorkShiftWithSupervisorsAssigns,
                                                                (int)WorkShiftVariationClientData.WorkShiftType.TimeOff,
                                                                ApplicationUtil.GetDataBaseFormattedDate(currentDate.Date)));

            querys["WorkShifts"].Add(SmartCadHqls.GetCustomHql(SmartCadHqls.GetWorkShiftWithOperatorsCategoryList,
                                                                (int)WorkShiftVariationClientData.WorkShiftType.TimeOff));


            if (supervisedApplicationName == UserApplicationClientData.FirstLevel.Name)
            {
                querys["WorkShifts"].Add(SmartCadHqls.GetCustomHql(SmartCadHqls.GetWorkShiftWithFirstLevelOperatorsAndSupervisors,
                                                                 (int)WorkShiftVariationClientData.WorkShiftType.TimeOff));

                //querys["WorkShifts"].Add(SmartCadHqls.GetCustomHql(SmartCadHqls.GetWorkShiftForOperatorAssignsFormCaseFirstLevel,
                //                                            (int)WorkShiftVariationClientData.WorkShiftType.TimeOff,
                //                                             ApplicationUtil.GetDataBaseFormattedDate(currentDate.AddMinutes(timeToDelete * -1))));

            }
            else if (supervisedApplicationName == UserApplicationClientData.Dispatch.Name)
            {
                querys["WorkShifts"].Add(SmartCadHqls.GetCustomHql(SmartCadHqls.GetWorkShiftWithDispatchOperatorsAndSupervisors,
                                                            (int)WorkShiftVariationClientData.WorkShiftType.TimeOff, selectedDepartment.Code));

                querys["WorkShifts"].Add(SmartCadHqls.GetCustomHql(SmartCadHqls.GetWorkShiftForOperatorAssignsFormCaseDispatch,
                                                            (int)WorkShiftVariationClientData.WorkShiftType.TimeOff,
                                                             ApplicationUtil.GetDataBaseFormattedDate(currentDate.AddMinutes(timeToDelete * -1)), selectedDepartment.Code));

            }

                        
            querys = ServerServiceClient.GetInstance().SearchClientObjectsMaxRowsMultiQuery(true, querys);

            workShifts = querys["WorkShifts"];

            //workShifts = ServerServiceClient.GetInstance().SearchClientObjects(
            // SmartCadHqls.GetCustomHql(
            //     SmartCadHqls.GetWorkShiftVariationWithFirstLevelOperatorsWithStrictAccess,
            //     (int)WorkShiftVariationClientData.WorkShiftType.TimeOff));

            //else if (supervisedApplicationName == UserApplicationClientData.Dispatch.Name)
            //{
            //    workShifts = ServerServiceClient.GetInstance().SearchClientObjects(
            //    SmartCadHqls.GetCustomHql(
            //        SmartCadHqls.GetWorkShiftVariationWithDispatchOperatorsWithStrictAccess,
            //        (int)WorkShiftVariationClientData.WorkShiftType.TimeOff,                    
            //        selectedDepartment.Code));
            //}


            foreach (WorkShiftVariationClientData wsv in workShifts)
            {
                if (wsv.Operators != null && wsv.Schedules != null)
                {
                    //wsv.Schedules = 
                    //    (IList)ServerServiceClient.GetInstance().SearchClientObjects(
                    //        SmartCadHqls.GetCustomHql(
                    //            SmartCadHqls.GetNotStartedWorkShiftSchedules,
                    //            wsv.Code,
                    //            ApplicationUtil.GetDataBaseFormattedDate(currentDate.AddMinutes(timeToDelete * -1))));
                    //if (wsv.Schedules != null && wsv.Schedules.Count == 0)
                    //    continue;
                    schedulerControl.FillListOperators(wsv.Operators);
                    listWorkShifts.Add(wsv.Code, wsv);
                    
                    foreach (OperatorClientData oper in wsv.Operators)
                    {
                        if (oper.IsSupervisor == false)
                        {
                            //oper.Supervisors = new ArrayList();
                            //oper.Supervisors = 
                            //    (IList)ServerServiceClient.GetInstance().SearchClientObjects(
                            //        SmartCadHqls.GetCustomHql(
                            //            SmartCadHqls.GetNotStartedOperatorAssignsForGinvenWorkShiftVariationAndOperator,
                            //            oper.Code,
                            //            wsv.Code, 
                            //            ApplicationUtil.GetDataBaseFormattedDate(currentDate.Date)));
                            list = new Dictionary<int, int>();
                            foreach (WorkShiftScheduleVariationClientData schedule in wsv.Schedules)
                            {
                                assignList = GetAssignListBySchedule(oper.Supervisors, schedule.Code);
                                isSupervised = HowIsSupervised(assignList, schedule);
                                list.Add(schedule.Code, isSupervised);
                                schedulerControl.CreateAppointments(oper, wsv, schedule, assignList, isSupervised, false);
                            }

                            operatorsDataSource.Add(new GridControlDataOperatorEx(oper, wsv, list));
                        }
                        else
                        {
                            //oper.Operators = new ArrayList();
                            //oper.Operators = 
                            //    (IList)ServerServiceClient.GetInstance().SearchClientObjects(
                            //        SmartCadHqls.GetCustomHql(
                            //            SmartCadHqls.GetNotStartedOperatorAssignsForGinvenWorkShiftVariationAndSupervisor,
                            //            oper.Code,
                            //            wsv.Code, 
                            //            ApplicationUtil.GetDataBaseFormattedDate(currentDate.Date)));
                            foreach (WorkShiftScheduleVariationClientData schedule in wsv.Schedules)
                            {
                                assignList = GetAssignListBySchedule(oper.Operators, schedule.Code);
                                if (assignList.Count > 0)
                                    isSupervised = 2;
                                else
                                    isSupervised = 0;
                                schedulerControl.CreateAppointments(oper, wsv, schedule, assignList, isSupervised, true);
                            }

                            if (superWorkShift.ContainsKey(oper) == false)
                            {
                                wsList = new List<WorkShiftVariationClientData>();
                                wsList.Add(wsv);
                                superWorkShift.Add(oper, wsList);
                            }
                            else
                            {
                                wsList = superWorkShift[oper];
                                wsList.Add(wsv);
                                superWorkShift[oper] = wsList;
                            }
                        }
                    }

                }

                schedulerControl.LoadAppointments();

                FillSupervisorsGrid(superWorkShift);

                gridControlExOperators.BeginUpdate();
                gridControlExOperators.DataSource = operatorsDataSource;
                gridControlExOperators.EndUpdate();

            }
        }

        private void FillSupervisorsGrid(Dictionary<OperatorClientData, List<WorkShiftVariationClientData>> supervisors) 
        {
            gridViewExSupervisors.SelectionWillChange -=new FocusedRowChangedEventHandler(gridViewExSupervisors_SelectionWillChange);
            BindingList<GridControlDataSupervisor> superDataSource = new BindingList<GridControlDataSupervisor>();
         
            foreach (OperatorClientData oper in supervisors.Keys)
                superDataSource.Add(new GridControlDataSupervisor(oper, supervisors[oper]));
            
            gridControlExSupervisors.BeginUpdate();
            gridControlExSupervisors.DataSource = superDataSource;
            gridControlExSupervisors.EndUpdate();
            gridViewExSupervisors.SelectionWillChange += new FocusedRowChangedEventHandler(gridViewExSupervisors_SelectionWillChange);
          

        }
            
        //Retorna 0 si no est supervisado, 1 si est parcalmente supervisado y 2 si est totalmente supervisado.
        private int HowIsSupervised(List<OperatorAssignClientData> assigns, WorkShiftScheduleVariationClientData schedule)
        {
            double assignTime = (double)GetOperatorAssignsTotalTime(assigns);
            double scheduleTime = schedule.End.Subtract(schedule.Start).TotalMilliseconds;

            if (assignTime == scheduleTime)
                return 2;
            else if (assignTime > 0 && assignTime < scheduleTime)
                return 1;
            else
                return 0;

        }

        public static double GetOperatorAssignsTotalTime(List<OperatorAssignClientData> assigns)
        {
            double result = 0.0;
            foreach (OperatorAssignClientData opAss in assigns)
            {
                result += opAss.EndDate.Subtract(opAss.StartDate).TotalMilliseconds;
            }
            return result;
        }

        private List<OperatorAssignClientData> GetAssignListBySchedule(IList operatorAssigns, int scheduleCode) 
        {
            List<OperatorAssignClientData> assignList = new List<OperatorAssignClientData>();

            if (operatorAssigns != null)
            {
                foreach (OperatorAssignClientData assign in operatorAssigns)
                {
                    if (assign.SupervisedScheduleVariation.Code == scheduleCode && assignList.Contains(assign) == false)
                        assignList.Add(assign);
                }
            }
            return assignList;
        
        }

        private void OperatorAssignForm_Activated(object sender, EventArgs e)
        {
             if (this.MdiParent != null)
                (this.MdiParent as SupervisionForm).RibbonControl.SelectedPage = (this.MdiParent as SupervisionForm).RibbonPageOperation;
        }

        private void gridViewExOperators_SelectionWillChange(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle != GridControlEx.InvalidRowHandle)
            {
                if (e.FocusedRowHandle > -1)
                {
                    GridControlDataOperatorEx gridData = gridControlExOperators.SelectedItems[0] as GridControlDataOperatorEx;
                    schedulerControl.IsOperatorView = true;
                    schedulerControl.IsOperatorSelected = true;
                    if (gridData != null)
                    {
                        schedulerControl.UpdateScheduler(gridData.Code, gridData.WorkShiftCode, (int)filter);
                        layoutControlGroupScheduler.Text = ResourceLoader.GetString2("ProgramFrom") + ": " + gridData.FirstName + " " + gridData.LastName;
                        if (schedulerControl.SelectedAppointment != null && (int)schedulerControl.SelectedAppointment.CustomFields["OperatorCode"] == gridData.Code)
                        {
                            barButtonItemAssign.Enabled = true;
                            schedulerControl.EnabledAssign = true; 
                        }
                        else
                        {
                            schedulerControl.EnabledAssign = false;
                            barButtonItemAssign.Enabled = false;
                        }
                    }
                   // gridViewExSupervisors.SelectionWillChange -= new FocusedRowChangedEventHandler(gridViewExSupervisors_SelectionWillChange);
                    gridViewExSupervisors.FocusedRowHandle = GridControlEx.AutoFilterRowHandle;
                   // gridViewExSupervisors.SelectionWillChange += new FocusedRowChangedEventHandler(gridViewExSupervisors_SelectionWillChange);
                    barButtonItemRemove.Enabled = true;
                    schedulerControl.EnabledRemove = true;

                    schedulerControl.GoToFirstAppointment(gridData.Code, gridData.WorkShiftCode);

                    if (gridData.IsNotSupervised != null && gridData.IsNotSupervised > 0 && radioButtonNotSupervised.Checked == true)
                    {
                        barButtonItemRemove.Enabled = false;
                    }
                    
                }
                else if (e.FocusedRowHandle == GridControlEx.AutoFilterRowHandle)
                {
                    barButtonItemAssign.Enabled = false;
                    barButtonItemRemove.Enabled = false;
                    schedulerControl.EnabledAssign = false;
                    schedulerControl.EnabledRemove = false;
                    schedulerControl.CleanScheduler();
                    layoutControlGroupScheduler.Text = ResourceLoader.GetString2("Program");
                }
                else
                {
                    GridControlDataOperatorEx gridDataWs = gridViewExOperators.GetRow(e.FocusedRowHandle) as GridControlDataOperatorEx;
                    schedulerControl.IsOperatorView = true;
                    schedulerControl.IsOperatorSelected = false;
                    if (gridDataWs != null)
                    {
                        schedulerControl.UpdateScheduler(0, gridDataWs.WorkShiftCode, (int)filter);
                        layoutControlGroupScheduler.Text = ResourceLoader.GetString2("ProgramFrom") + ": " + gridDataWs.WorkShiftName;
                    }
                    if (schedulerControl.SelectedAppointment != null)
                    {
                        barButtonItemAssign.Enabled = true;
                        schedulerControl.EnabledAssign = true;
                    }
                    else
                    {
                        schedulerControl.EnabledAssign = false;
                        barButtonItemAssign.Enabled = false;
                    }
                    gridViewExSupervisors.SelectionWillChange -= new FocusedRowChangedEventHandler(gridViewExSupervisors_SelectionWillChange);
                    gridViewExSupervisors.FocusedRowHandle = GridControlEx.AutoFilterRowHandle;
                    gridViewExSupervisors.SelectionWillChange += new FocusedRowChangedEventHandler(gridViewExSupervisors_SelectionWillChange);
                    barButtonItemRemove.Enabled = false;
                    schedulerControl.EnabledRemove = false;

                    schedulerControl.GoToFirstAppointment(gridDataWs.WorkShiftCode, false);

                }
            }

        }

        public IList GetOperatorsOnGrid(GridControlEx grid)
        {
            IList operators = null;
            FormUtil.InvokeRequired(grid,
                delegate
                {
                    operators = new ArrayList(grid.Items);
                });
            return operators;
        }

        private void gridViewExSupervisors_SelectionWillChange(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {

            if (gridViewExSupervisors.FocusedRowHandle > -1)
            {
                GridControlDataSupervisor gridData = gridControlExSupervisors.SelectedItems[0] as GridControlDataSupervisor;
                BindingList<GridControlDataOperatorEx> operatorsDataSource = new BindingList<GridControlDataOperatorEx>();

                schedulerControl.IsOperatorView = false;
                schedulerControl.UpdateScheduler(gridData.Code);
                layoutControlGroupScheduler.Text = ResourceLoader.GetString2("ProgramFrom") + ": " + gridData.FirstName + " " + gridData.LastName;

                IList wsvList =
                    ServerServiceClient.GetInstance().SearchClientObjects(
                    SmartCadHqls.GetCustomHql(SmartCadHqls.GetFutureWorkShiftVariationWithOperatorsBySupervisor,
                        (int)WorkShiftVariationClientData.WorkShiftType.TimeOff,
                        gridData.Code,
                        ApplicationUtil.GetDataBaseFormattedDate(DateTime.Now)));


                if (wsvList != null && wsvList.Count > 0)
                {
                    foreach (WorkShiftVariationClientData ws in wsvList)
                    {
                        foreach (OperatorClientData oper in ws.Operators)
                            operatorsDataSource.Add(new GridControlDataOperatorEx(oper, ws, null));
                    }
                }
                gridControlExAssignedOperators.BeginUpdate();
                gridControlExAssignedOperators.DataSource = operatorsDataSource;
                gridControlExAssignedOperators.EndUpdate();
      
                gridViewExOperators.SelectionWillChange -=new FocusedRowChangedEventHandler(gridViewExOperators_SelectionWillChange);
                gridViewExOperators.FocusedRowHandle = GridControlEx.AutoFilterRowHandle;
                gridViewExOperators.SelectionWillChange += new FocusedRowChangedEventHandler(gridViewExOperators_SelectionWillChange);
                barButtonItemRemove.Enabled = true;
                schedulerControl.EnabledRemove = true;
                barButtonItemAssign.Enabled = false;
                schedulerControl.EnabledAssign = false;
                if (schedulerControl.SelectedAppointment != null && (int)schedulerControl.SelectedAppointment.CustomFields["OperatorCode"] == gridData.Code)
                    barButtonItemOpen.Enabled = true;
                else
                    barButtonItemOpen.Enabled = false;
                schedulerControl.IsOperatorView = false;
                schedulerControl.UpdateScheduler(gridData.Code, 0, 0);

                schedulerControl.GoToFirstAppointment(gridData.Code, true);
            }
            else
            {
                //schedulerControl.CleanScheduler();
                layoutControlGroupScheduler.Text = ResourceLoader.GetString2("Program");
                barButtonItemRemove.Enabled = false;
                schedulerControl.EnabledRemove = false;
                gridControlExAssignedOperators.BeginUpdate();
                gridControlExAssignedOperators.DataSource = null;
                gridControlExAssignedOperators.EndUpdate();

            }

        }

        private void barButtonItemAssign_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (schedulerControl.SelectedAppointment != null) 
            {
                int wsvCode;
                WorkShiftVariationClientData wsv = new WorkShiftVariationClientData();
                if (gridControlExOperators.SelectedItems.Count > 0)
                {   
                    wsvCode = (gridViewExOperators.GetRow(gridViewExOperators.FocusedRowHandle) as GridControlDataOperatorEx).WorkShiftCode;                
                
                    wsv = listWorkShifts[wsvCode];
                }

                OperatorAssignCreateForm form = new OperatorAssignCreateForm();
                
                if (gridViewExOperators.FocusedRowHandle > -1)
                    form = new OperatorAssignCreateForm(schedulerControl.SelectedAppointment, gridControlExSupervisors.Items, wsv,false, this);
                else if (gridViewExOperators.FocusedRowHandle < 0 && gridViewExOperators.FocusedRowHandle != GridControlEx.AutoFilterRowHandle)
                    form = new OperatorAssignCreateForm(schedulerControl.SelectedAppointment, gridControlExSupervisors.Items, wsv,true, this);
               
                form.ShowDialog();
    
            }
        }

        private void barButtonItemRemove_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OperatorAssignRemoveForm form;
            OperatorClientData oper = new OperatorClientData();

            if (gridViewExOperators.FocusedRowHandle > -1)
                  oper = ((GridControlDataOperatorEx)gridControlExOperators.SelectedItems[0]).Tag as OperatorClientData;
            else if (gridViewExSupervisors.FocusedRowHandle > -1)
                  oper = ((GridControlDataSupervisor)gridControlExSupervisors.SelectedItems[0]).Tag as OperatorClientData;
                     
            
            form = new OperatorAssignRemoveForm(oper, schedulerControl.SelectedAppointment);
            form.ShowDialog();
            
        }


        void schedulerControl_SelectedAppointmentChanged(object sender, DevExpress.XtraScheduler.Appointment newApp)
        {
            if (schedulerControl.SelectedAppointment != null)
            {
                if (gridControlExOperators.SelectedItems.Count > 0)
                {
                    if (((int)schedulerControl.SelectedAppointment.CustomFields["OperatorCode"] == ((GridControlDataOperatorEx)gridControlExOperators.SelectedItems[0]).Code || gridViewExOperators.FocusedRowHandle < 0) && (gridViewExOperators.FocusedRowHandle != GridControlEx.AutoFilterRowHandle))
                    {
                        barButtonItemAssign.Enabled = true;
                        schedulerControl.EnabledAssign = true;
                    }
                    else
                    {
                        schedulerControl.EnabledAssign = false;
                        barButtonItemAssign.Enabled = false;
                    }
                }             

                barButtonItemOpen.Enabled = true;
            }
            else
            {
                barButtonItemOpen.Enabled = false;
                barButtonItemAssign.Enabled = false;
                schedulerControl.EnabledAssign = false;
            }
        }

        void schedulerControl_CustomMenu_Click(object sender, DevExpress.Utils.Menu.DXMenuItem item)
        {
            if ((OperatorAssignSchedulerControl.CustomMenuItem)item.Tag == OperatorAssignSchedulerControl.CustomMenuItem.DETAIL)
                barButtonItemOpen_ItemClick(null, null);
            else if ((OperatorAssignSchedulerControl.CustomMenuItem)item.Tag == OperatorAssignSchedulerControl.CustomMenuItem.ASSIGN)
                barButtonItemAssign_ItemClick(null, null);
            else if ((OperatorAssignSchedulerControl.CustomMenuItem)item.Tag == OperatorAssignSchedulerControl.CustomMenuItem.DELETE)
                barButtonItemRemove_ItemClick(null, null);

        }

        private void barButtonItemOpen_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (schedulerControl.SelectedAppointment != null){
                OperatorAssignDetailForm form = new OperatorAssignDetailForm(schedulerControl.SelectedAppointment);

                form.ShowDialog();
            }
        }

        private void radioButtonFilterOperators_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonAll.Checked == true)
            {
                filter = OperatorFilter.All;
                gridViewExOperators.ActiveFilterString = string.Empty;
            }
            else if (radioButtonNotSupervised.Checked == true)
            {
                filter = OperatorFilter.None;
                gridViewExOperators.ActiveFilterString = "[IsNotSupervised] > 0";
            }
            else if (radioButtonPartially.Checked == true)
            {
                filter = OperatorFilter.Partial;
                gridViewExOperators.ActiveFilterString = "[IsPartiallySupervised] > 0";
            }
            else if (this.radioButtonSupervised.Checked == true)
            {
                filter = OperatorFilter.Total;
                gridViewExOperators.ActiveFilterString = "[IsTotallySupervised] > 0";
            }

            if (gridViewExOperators.DataRowCount == 0)
            {
                schedulerControl.CleanScheduler();
                layoutControlGroupScheduler.Text = ResourceLoader.GetString2("Program");
            }
            if (gridControlExOperators.SelectedItems.Count > 0)
                gridViewExOperators_SelectionWillChange(null, new FocusedRowChangedEventArgs(gridViewExOperators.FocusedRowHandle, gridViewExOperators.FocusedRowHandle));
        }

        private void OperatorAssignForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.MdiParent != null)
                ServerServiceClient.GetInstance().CommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(OperatorAssignForm_CommittedChanges);
        
               if (timerUpdateInformation != null)
                {
                    timerUpdateInformation.Stop();
                    timerUpdateInformation.Dispose();
                }
        }
    }


           

    #region GridControlData
    public class GridControlDataSupervisor: GridControlData
    {

        private List<WorkShiftVariationClientData> workShifts = new List<WorkShiftVariationClientData>();

        public GridControlDataSupervisor(OperatorClientData data)
        :base(data){
           
        }

        public GridControlDataSupervisor(OperatorClientData data, List<WorkShiftVariationClientData> wsvList)
            : base(data)
        {
            workShifts = wsvList;
        }
        
        public OperatorClientData Operator
        {
            get { return this.Tag as OperatorClientData; }
        }

        public List<WorkShiftVariationClientData> WorkShifts
        {
            get { return workShifts; }
            set { workShifts = value;}
        }

        public int Code
        {
            get { return Operator.Code; }
        }

        [GridControlRowInfoData(Visible = true, Width = 150)]
        public string FirstName
        {
            get { return Operator.FirstName; }
        }

        [GridControlRowInfoData(Visible = true, Width = 150)]
        public string LastName
        {
            get { return Operator.LastName; }
        }

        [GridControlRowInfoData(Visible = true, Width = 150)]
        public string WorkShiftName
        {
            get
            {
                string wsName = string.Empty;
                foreach (WorkShiftVariationClientData ws in workShifts)
                    wsName += ws.Name + ", "; 
                

                if (wsName != string.Empty)
                    return wsName.Substring(0, wsName.Length - 2);
                else
                    return wsName;
            }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            GridControlDataSupervisor var = obj as GridControlDataSupervisor;
            if (var != null)
            {
                if (this.Code == var.Code)
                    result = true;
            }
            return result;
        }

    }

    public class GridControlDataOperatorEx: GridControlData
    {
        
        string wsvName;
        int wsvCode;
        int isTotallySupervised;
        int isPartiallySupervised;
        int isNotSupervised;
        Dictionary<int, int> list = new Dictionary<int, int>();

        public GridControlDataOperatorEx(OperatorClientData oper)
        :base(oper){
        
        }

        public GridControlDataOperatorEx(OperatorClientData oper, int newIsSupervised, int scheduleCode, string wsName, int wsCode, Dictionary<int, int> listSupervised)
            : base(oper)
        {

            this.WorkShiftName = wsName;
            this.WorkShiftCode = wsCode;
           // int oldIsSupervised = listSupervised[schedule.Code];

            listSupervised[scheduleCode] = newIsSupervised;
            if (listSupervised != null)
            {
                List<int> values = listSupervised.Values.ToList();

                for (int i = 0; i < values.Count; i++)
                {
                    if ((int)values[i] == 0)
                        isNotSupervised++;
                    else if ((int)values[i] == 1)
                        isPartiallySupervised++;
                    else if ((int)values[i] == 2)
                        isTotallySupervised++;
                }
            }

                     
            IsSupervisedList = listSupervised;
        }


        public GridControlDataOperatorEx(OperatorClientData oper, WorkShiftVariationClientData wsv, Dictionary<int,int> listSupervised)
        :base(oper)
        {
            this.WorkShiftName = wsv.Name;
            this.WorkShiftCode = wsv.Code;
            this.list = listSupervised;

            if (listSupervised != null)
            {
                 List<int> values = listSupervised.Values.ToList();
       
                for (int i = 0; i < values.Count; i++)
                {
                    if ((int)values[i] == 0)
                        isNotSupervised++;
                    else if ((int)values[i] == 1)
                        isPartiallySupervised++;
                    else if ((int)values[i] == 2)
                        isTotallySupervised++;
                }
            }
        }

        public OperatorClientData Operator
        {
            get { return this.Tag as OperatorClientData; }
        }

        public int Code
        {
            get { return Operator.Code; }
        }

        [GridControlRowInfoData(Visible = true, Width = 150)]
        public string FirstName
        {
            get { return Operator.FirstName; }
        }

        [GridControlRowInfoData(Visible = true, Width = 150)]
        public string LastName
        {
            get { return Operator.LastName; }
        }

        [GridControlRowInfoData(Visible = false, GroupIndex = 0, DisplayFormat="{1}")]
        public string WorkShiftName
        {
            get
            {                
                return wsvName;
            }
            set 
            {
                wsvName = value;
            }
        }

        [GridControlRowInfoData(Visible = true, Width = 150)]
        public string Category
        {
            get
            {
                return Operator.OperatorCategory;
            }
        }

        public int WorkShiftCode
        {
            get
            {
                return wsvCode;
            }
            set
            {
                wsvCode = value;
            }
        }

        [GridControlRowInfoData(Visible = false)]
        public int IsTotallySupervised
        {
            get
            {
                return isTotallySupervised;
            }

            set
            {
                isTotallySupervised = value;
            }
        }

        [GridControlRowInfoData(Visible = false)]
        public int IsPartiallySupervised
        {
            get
            {
                return isPartiallySupervised;
            }

            set
            {
                isPartiallySupervised = value;
            }
        }

        [GridControlRowInfoData(Visible = false)]
        public int IsNotSupervised
        {
            get
            {
                return isNotSupervised;
            }

            set
            {
                isNotSupervised = value;
            }
        }


        public Dictionary<int, int> IsSupervisedList
        {
            get
            {
                return list;
            }

            set
            {
                list = value;
            }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            GridControlDataOperatorEx var = obj as GridControlDataOperatorEx;
            if (var != null)
            {
                if (this.Code == var.Code && 
                    this.wsvCode== var.wsvCode)
                    result = true;
            }
            return result;
        }


    }
    #endregion
}
