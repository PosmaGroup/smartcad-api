using SmartCadControls;
using SmartCadControls.Controls;
namespace SmartCadGuiCommon
{
    partial class TrainingCourseAssignForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TrainingCourseAssignForm));
            this.buttonExApply = new DevExpress.XtraEditors.SimpleButton();
            this.TrainingCourseAssignFormConvertedLayout = new DevExpress.XtraLayout.LayoutControl();
            this.buttonRemoveAllOperators = new DevExpress.XtraEditors.SimpleButton();
            this.buttonAddAllOperators = new DevExpress.XtraEditors.SimpleButton();
            this.gridControlSchedules = new GridControlEx();
            this.gridViewSchedules = new GridViewEx();
            this.listBoxAssigned = new ListBoxEx();
            this.buttonAddOperator = new DevExpress.XtraEditors.SimpleButton();
            this.textBoxExCourse = new TextBoxEx();
            this.listBoxNotAssigned = new ListBoxEx();
            this.buttonRemoveOperator = new DevExpress.XtraEditors.SimpleButton();
            this.buttonExCancel = new DevExpress.XtraEditors.SimpleButton();
            this.buttonExOk = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupTrainingCourse = new DevExpress.XtraLayout.LayoutControlGroup();
            this.textBoxExCourseitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.gridControlSchedulesitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupOperators = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItemAssigned = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemNotAssigned = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutConverter1 = new DevExpress.XtraLayout.Converter.LayoutConverter(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.TrainingCourseAssignFormConvertedLayout)).BeginInit();
            this.TrainingCourseAssignFormConvertedLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSchedules)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSchedules)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupTrainingCourse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxExCourseitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSchedulesitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupOperators)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAssigned)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemNotAssigned)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonExApply
            // 
            this.buttonExApply.Enabled = false;
            this.buttonExApply.Location = new System.Drawing.Point(453, 417);
            this.buttonExApply.Name = "buttonExApply";
            this.buttonExApply.Size = new System.Drawing.Size(82, 32);
            this.buttonExApply.StyleController = this.TrainingCourseAssignFormConvertedLayout;
            this.buttonExApply.TabIndex = 8;
            this.buttonExApply.Text = "Aplicar";
            this.buttonExApply.Click += new System.EventHandler(this.buttonExApply_Click);
            // 
            // TrainingCourseAssignFormConvertedLayout
            // 
            this.TrainingCourseAssignFormConvertedLayout.AllowCustomizationMenu = false;
            this.TrainingCourseAssignFormConvertedLayout.Controls.Add(this.buttonRemoveAllOperators);
            this.TrainingCourseAssignFormConvertedLayout.Controls.Add(this.buttonAddAllOperators);
            this.TrainingCourseAssignFormConvertedLayout.Controls.Add(this.gridControlSchedules);
            this.TrainingCourseAssignFormConvertedLayout.Controls.Add(this.listBoxAssigned);
            this.TrainingCourseAssignFormConvertedLayout.Controls.Add(this.buttonAddOperator);
            this.TrainingCourseAssignFormConvertedLayout.Controls.Add(this.textBoxExCourse);
            this.TrainingCourseAssignFormConvertedLayout.Controls.Add(this.listBoxNotAssigned);
            this.TrainingCourseAssignFormConvertedLayout.Controls.Add(this.buttonRemoveOperator);
            this.TrainingCourseAssignFormConvertedLayout.Controls.Add(this.buttonExApply);
            this.TrainingCourseAssignFormConvertedLayout.Controls.Add(this.buttonExCancel);
            this.TrainingCourseAssignFormConvertedLayout.Controls.Add(this.buttonExOk);
            this.TrainingCourseAssignFormConvertedLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TrainingCourseAssignFormConvertedLayout.Location = new System.Drawing.Point(0, 0);
            this.TrainingCourseAssignFormConvertedLayout.Name = "TrainingCourseAssignFormConvertedLayout";
            this.TrainingCourseAssignFormConvertedLayout.Root = this.layoutControlGroup1;
            this.TrainingCourseAssignFormConvertedLayout.Size = new System.Drawing.Size(547, 461);
            this.TrainingCourseAssignFormConvertedLayout.TabIndex = 9;
            // 
            // buttonRemoveAllOperators
            // 
            this.buttonRemoveAllOperators.Location = new System.Drawing.Point(251, 362);
            this.buttonRemoveAllOperators.Name = "buttonRemoveAllOperators";
            this.buttonRemoveAllOperators.Size = new System.Drawing.Size(47, 29);
            this.buttonRemoveAllOperators.StyleController = this.TrainingCourseAssignFormConvertedLayout;
            toolTipTitleItem1.Text = "Remove all participants";
            superToolTip1.Items.Add(toolTipTitleItem1);
            this.buttonRemoveAllOperators.SuperTip = superToolTip1;
            this.buttonRemoveAllOperators.TabIndex = 12;
            this.buttonRemoveAllOperators.Text = "<<";
            this.buttonRemoveAllOperators.Click += new System.EventHandler(this.buttonRemoveAllOperators_Click);
            // 
            // buttonAddAllOperators
            // 
            this.buttonAddAllOperators.Location = new System.Drawing.Point(251, 296);
            this.buttonAddAllOperators.Name = "buttonAddAllOperators";
            this.buttonAddAllOperators.Size = new System.Drawing.Size(47, 29);
            this.buttonAddAllOperators.StyleController = this.TrainingCourseAssignFormConvertedLayout;
            toolTipTitleItem2.Text = "Add all participants";
            superToolTip2.Items.Add(toolTipTitleItem2);
            this.buttonAddAllOperators.SuperTip = superToolTip2;
            this.buttonAddAllOperators.TabIndex = 11;
            this.buttonAddAllOperators.Text = ">>";
            this.buttonAddAllOperators.Click += new System.EventHandler(this.buttonAddAllOperators_Click);
            // 
            // gridControlSchedules
            // 
            this.gridControlSchedules.EnableAutoFilter = false;
            this.gridControlSchedules.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlSchedules.Location = new System.Drawing.Point(175, 68);
            this.gridControlSchedules.MainView = this.gridViewSchedules;
            this.gridControlSchedules.Name = "gridControlSchedules";
            this.gridControlSchedules.Size = new System.Drawing.Size(348, 137);
            this.gridControlSchedules.TabIndex = 6;
            this.gridControlSchedules.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewSchedules});
            this.gridControlSchedules.ViewTotalRows = false;
            // 
            // gridViewSchedules
            // 
            this.gridViewSchedules.AllowFocusedRowChanged = true;
            this.gridViewSchedules.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewSchedules.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewSchedules.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewSchedules.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewSchedules.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewSchedules.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewSchedules.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewSchedules.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewSchedules.EnablePreviewLineForFocusedRow = false;
            this.gridViewSchedules.GridControl = this.gridControlSchedules;
            this.gridViewSchedules.Name = "gridViewSchedules";
            this.gridViewSchedules.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewSchedules.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewSchedules.ViewTotalRows = false;
            // 
            // listBoxAssigned
            // 
            this.listBoxAssigned.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.listBoxAssigned.FormattingEnabled = true;
            this.listBoxAssigned.IntegralHeight = false;
            this.listBoxAssigned.Location = new System.Drawing.Point(302, 269);
            this.listBoxAssigned.Name = "listBoxAssigned";
            this.listBoxAssigned.Size = new System.Drawing.Size(221, 132);
            this.listBoxAssigned.Sorted = true;
            this.listBoxAssigned.TabIndex = 6;
            this.listBoxAssigned.DoubleClick += new System.EventHandler(this.listBoxAssigned_DoubleClick);
            this.listBoxAssigned.Enter += new System.EventHandler(this.listBoxAssigned_Enter);
            // 
            // buttonAddOperator
            // 
            this.buttonAddOperator.Location = new System.Drawing.Point(251, 263);
            this.buttonAddOperator.Name = "buttonAddOperator";
            this.buttonAddOperator.Size = new System.Drawing.Size(47, 29);
            this.buttonAddOperator.StyleController = this.TrainingCourseAssignFormConvertedLayout;
            toolTipTitleItem3.Text = "Add participants";
            superToolTip3.Items.Add(toolTipTitleItem3);
            this.buttonAddOperator.SuperTip = superToolTip3;
            this.buttonAddOperator.TabIndex = 9;
            this.buttonAddOperator.Text = ">";
            this.buttonAddOperator.Click += new System.EventHandler(this.buttonAddOperator_Click);
            // 
            // textBoxExCourse
            // 
            this.textBoxExCourse.AllowsLetters = true;
            this.textBoxExCourse.AllowsNumbers = true;
            this.textBoxExCourse.AllowsPunctuation = true;
            this.textBoxExCourse.AllowsSeparators = true;
            this.textBoxExCourse.AllowsSymbols = true;
            this.textBoxExCourse.AllowsWhiteSpaces = true;
            this.textBoxExCourse.BackColor = System.Drawing.Color.White;
            this.textBoxExCourse.ExtraAllowedChars = "";
            this.textBoxExCourse.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExCourse.Location = new System.Drawing.Point(175, 44);
            this.textBoxExCourse.Name = "textBoxExCourse";
            this.textBoxExCourse.NonAllowedCharacters = "";
            this.textBoxExCourse.ReadOnly = true;
            this.textBoxExCourse.RegularExpresion = "";
            this.textBoxExCourse.Size = new System.Drawing.Size(348, 20);
            this.textBoxExCourse.TabIndex = 4;
            // 
            // listBoxNotAssigned
            // 
            this.listBoxNotAssigned.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.listBoxNotAssigned.FormattingEnabled = true;
            this.listBoxNotAssigned.IntegralHeight = false;
            this.listBoxNotAssigned.Location = new System.Drawing.Point(24, 269);
            this.listBoxNotAssigned.Name = "listBoxNotAssigned";
            this.listBoxNotAssigned.Size = new System.Drawing.Size(223, 132);
            this.listBoxNotAssigned.Sorted = true;
            this.listBoxNotAssigned.TabIndex = 1;
            this.listBoxNotAssigned.DoubleClick += new System.EventHandler(this.listBoxNotAssigned_DoubleClick);
            this.listBoxNotAssigned.Enter += new System.EventHandler(this.listBoxNotAssigned_Enter);
            // 
            // buttonRemoveOperator
            // 
            this.buttonRemoveOperator.Location = new System.Drawing.Point(251, 329);
            this.buttonRemoveOperator.Name = "buttonRemoveOperator";
            this.buttonRemoveOperator.Size = new System.Drawing.Size(47, 29);
            this.buttonRemoveOperator.StyleController = this.TrainingCourseAssignFormConvertedLayout;
            toolTipTitleItem4.Text = "Remove participant";
            superToolTip4.Items.Add(toolTipTitleItem4);
            this.buttonRemoveOperator.SuperTip = superToolTip4;
            this.buttonRemoveOperator.TabIndex = 10;
            this.buttonRemoveOperator.Text = "<";
            this.buttonRemoveOperator.Click += new System.EventHandler(this.buttonRemoveOperator_Click);
            // 
            // buttonExCancel
            // 
            this.buttonExCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonExCancel.Location = new System.Drawing.Point(367, 417);
            this.buttonExCancel.Name = "buttonExCancel";
            this.buttonExCancel.Size = new System.Drawing.Size(82, 32);
            this.buttonExCancel.StyleController = this.TrainingCourseAssignFormConvertedLayout;
            this.buttonExCancel.TabIndex = 7;
            this.buttonExCancel.Text = "Cancelar";
            // 
            // buttonExOk
            // 
            this.buttonExOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonExOk.Location = new System.Drawing.Point(281, 417);
            this.buttonExOk.Name = "buttonExOk";
            this.buttonExOk.Size = new System.Drawing.Size(82, 32);
            this.buttonExOk.StyleController = this.TrainingCourseAssignFormConvertedLayout;
            this.buttonExOk.TabIndex = 6;
            this.buttonExOk.Text = "Accept";
            this.buttonExOk.Click += new System.EventHandler(this.buttonExOk_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlGroupTrainingCourse,
            this.layoutControlGroupOperators,
            this.emptySpaceItem3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(547, 461);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.buttonExApply;
            this.layoutControlItem6.CustomizationFormText = "buttonExApplyitem";
            this.layoutControlItem6.Location = new System.Drawing.Point(441, 405);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem6.Name = "buttonExApplyitem";
            this.layoutControlItem6.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "buttonExApplyitem";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.buttonExCancel;
            this.layoutControlItem7.CustomizationFormText = "buttonExCancelitem";
            this.layoutControlItem7.Location = new System.Drawing.Point(355, 405);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem7.Name = "buttonExCancelitem";
            this.layoutControlItem7.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "buttonExCancelitem";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.buttonExOk;
            this.layoutControlItem8.CustomizationFormText = "buttonExOkitem";
            this.layoutControlItem8.Location = new System.Drawing.Point(269, 405);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem8.Name = "buttonExOkitem";
            this.layoutControlItem8.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "buttonExOkitem";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlGroupTrainingCourse
            // 
            this.layoutControlGroupTrainingCourse.CustomizationFormText = "layoutControlGroupTrainingCourse";
            this.layoutControlGroupTrainingCourse.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.textBoxExCourseitem,
            this.gridControlSchedulesitem});
            this.layoutControlGroupTrainingCourse.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupTrainingCourse.Name = "layoutControlGroupTrainingCourse";
            this.layoutControlGroupTrainingCourse.Size = new System.Drawing.Size(527, 209);
            this.layoutControlGroupTrainingCourse.Text = "layoutControlGroupTrainingCourse";
            // 
            // textBoxExCourseitem
            // 
            this.textBoxExCourseitem.Control = this.textBoxExCourse;
            this.textBoxExCourseitem.CustomizationFormText = "textBoxExCourseitem";
            this.textBoxExCourseitem.Location = new System.Drawing.Point(0, 0);
            this.textBoxExCourseitem.Name = "textBoxExCourseitem";
            this.textBoxExCourseitem.Size = new System.Drawing.Size(503, 24);
            this.textBoxExCourseitem.Text = "textBoxExCourseitem";
            this.textBoxExCourseitem.TextSize = new System.Drawing.Size(147, 13);
            // 
            // gridControlSchedulesitem
            // 
            this.gridControlSchedulesitem.Control = this.gridControlSchedules;
            this.gridControlSchedulesitem.CustomizationFormText = "gridControlSchedulesitem";
            this.gridControlSchedulesitem.Location = new System.Drawing.Point(0, 24);
            this.gridControlSchedulesitem.Name = "gridControlSchedulesitem";
            this.gridControlSchedulesitem.Size = new System.Drawing.Size(503, 141);
            this.gridControlSchedulesitem.Text = "gridControlSchedulesitem";
            this.gridControlSchedulesitem.TextSize = new System.Drawing.Size(147, 13);
            // 
            // layoutControlGroupOperators
            // 
            this.layoutControlGroupOperators.CustomizationFormText = "layoutControlGroupOperators";
            this.layoutControlGroupOperators.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItemAssigned,
            this.layoutControlItemNotAssigned,
            this.layoutControlItem2,
            this.layoutControlItem4,
            this.layoutControlItem3,
            this.layoutControlItem5});
            this.layoutControlGroupOperators.Location = new System.Drawing.Point(0, 209);
            this.layoutControlGroupOperators.Name = "layoutControlGroupOperators";
            this.layoutControlGroupOperators.Size = new System.Drawing.Size(527, 196);
            this.layoutControlGroupOperators.Text = "layoutControlGroupOperators";
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(227, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(51, 10);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(227, 142);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(51, 10);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItemAssigned
            // 
            this.layoutControlItemAssigned.Control = this.listBoxAssigned;
            this.layoutControlItemAssigned.CustomizationFormText = "listBoxAssigneditem";
            this.layoutControlItemAssigned.Location = new System.Drawing.Point(278, 0);
            this.layoutControlItemAssigned.Name = "listBoxAssigneditem";
            this.layoutControlItemAssigned.Size = new System.Drawing.Size(225, 152);
            this.layoutControlItemAssigned.Text = "listBoxAssigneditem";
            this.layoutControlItemAssigned.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItemAssigned.TextSize = new System.Drawing.Size(147, 13);
            // 
            // layoutControlItemNotAssigned
            // 
            this.layoutControlItemNotAssigned.Control = this.listBoxNotAssigned;
            this.layoutControlItemNotAssigned.CustomizationFormText = "layoutControlItemNotAssigned";
            this.layoutControlItemNotAssigned.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemNotAssigned.Name = "layoutControlItemNotAssigned";
            this.layoutControlItemNotAssigned.Size = new System.Drawing.Size(227, 152);
            this.layoutControlItemNotAssigned.Text = "layoutControlItemNotAssigned";
            this.layoutControlItemNotAssigned.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItemNotAssigned.TextSize = new System.Drawing.Size(147, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.buttonAddOperator;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(227, 10);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(51, 33);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(51, 33);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(51, 33);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.buttonAddAllOperators;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(227, 43);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(51, 33);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(51, 33);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(51, 33);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.buttonRemoveOperator;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(227, 76);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(51, 33);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(51, 33);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(51, 33);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.buttonRemoveAllOperators;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(227, 109);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(51, 33);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(51, 33);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(51, 33);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 405);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(269, 36);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // TrainingCourseAssignForm
            // 
            this.AcceptButton = this.buttonExOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonExCancel;
            this.ClientSize = new System.Drawing.Size(547, 461);
            this.Controls.Add(this.TrainingCourseAssignFormConvertedLayout);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(555, 495);
            this.Name = "TrainingCourseAssignForm";
            this.Text = "Asignacion de operadores";
            ((System.ComponentModel.ISupportInitialize)(this.TrainingCourseAssignFormConvertedLayout)).EndInit();
            this.TrainingCourseAssignFormConvertedLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSchedules)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSchedules)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupTrainingCourse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxExCourseitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSchedulesitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupOperators)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAssigned)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemNotAssigned)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton buttonExApply;
        private DevExpress.XtraEditors.SimpleButton buttonExCancel;
        private DevExpress.XtraEditors.SimpleButton buttonExOk;
        private ListBoxEx listBoxAssigned;
        private ListBoxEx listBoxNotAssigned;
        private TextBoxEx textBoxExCourse;
        private GridControlEx gridControlSchedules;
        private GridViewEx gridViewSchedules;
        private DevExpress.XtraEditors.SimpleButton buttonRemoveAllOperators;
        private DevExpress.XtraEditors.SimpleButton buttonAddAllOperators;
        private DevExpress.XtraEditors.SimpleButton buttonRemoveOperator;
        private DevExpress.XtraEditors.SimpleButton buttonAddOperator;
        private DevExpress.XtraLayout.LayoutControl TrainingCourseAssignFormConvertedLayout;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem gridControlSchedulesitem;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemAssigned;
        private DevExpress.XtraLayout.LayoutControlItem textBoxExCourseitem;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemNotAssigned;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.Converter.LayoutConverter layoutConverter1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupTrainingCourse;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupOperators;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
    }
}