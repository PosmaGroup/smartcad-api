using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Smartmatic.SmartCad.Service;
using System.ServiceModel;
using System.Reflection;
using DevExpress.XtraEditors;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadCore.Core;


namespace SmartCadGuiCommon
{
    public partial class OperatorObservationForm : XtraForm
    {

        public int supervisorCode;
        public int operatorCode;
        public bool buttonOkPressed;
		public bool firstLevel;



        public OperatorObservationForm()
        {
            InitializeComponent();
            LoadLanguage();
        }


        public OperatorObservationForm(int sprCode,int prtrCode)
            : this()
        {
            this.supervisorCode = sprCode;
            this.operatorCode = prtrCode;
        }


        private void OperatorObservationForm_Load(object sender, EventArgs e)
        {
            IList list = ServerServiceClient.GetInstance().SearchClientObjects(typeof(ObservationTypeClientData));
			string userAccessName = string.Empty;
			if (firstLevel == false)
				userAccessName = "UserApplicationData+Basic+Dispatch";
			else
				userAccessName = "UserApplicationData+Basic+FirstLevel";
			foreach (ObservationTypeClientData obs in list) 
            {
				if(obs.UserAccessName.Contains(userAccessName)==true)
					this.comboBoxObservationTypes.Properties.Items.Add(obs);
            }
            OperatorObservationParametersChange(null, null); 
        }


        private void LoadLanguage()
        {
            this.layoutControlItemObservation.Text = ResourceLoader.GetString2("Observation") + ": *";
            this.layoutControlItemType.Text = ResourceLoader.GetString2("ObservationType") + ": *";
            buttonCancel.Text = ResourceLoader.GetString2("Cancel");
            buttonOK.Text = ResourceLoader.GetString2("Accept");
            this.Text = ResourceLoader.GetString2("AddObservation");

        }

        private void OperatorObservationParametersChange(object sender, EventArgs e) 
        {
            if (this.comboBoxObservationTypes.SelectedIndex == -1 ||
                string.IsNullOrEmpty(this.textBoxObservation.Text))
                this.buttonOK.Enabled = false;
            else
                this.buttonOK.Enabled = true;
        }

        private void comboBoxObservationTypes_SelectionChangeCommitted(object sender, EventArgs e)
        {
			OperatorObservationParametersChange(null, null);
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (buttonOK.Enabled == true)
                {
                    BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[1] { GetType().GetMethod("buttonOk_ClickHelp", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[0] });
                    processForm.CanThrowError = true;
                    processForm.ShowDialog();
                }
            }
            catch (FaultException ex)
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, MessageFormType.Information);
                buttonOkPressed = false;
            }
        }

        private void buttonOk_ClickHelp() 
        {
            OperatorObservationClientData client = new OperatorObservationClientData();
            client.Date = ServerServiceClient.GetInstance().GetTime();
            FormUtil.InvokeRequired(this, delegate
            {
                client.NameObservationType = (comboBoxObservationTypes.SelectedItem as ObservationTypeClientData).Name;
                client.Observation = textBoxObservation.Text;
            });
            client.SupervisorCode = supervisorCode;
            client.OperatorCode = operatorCode;
            ServerServiceClient.GetInstance().SaveOrUpdateClientData(client);
        }
    }
}