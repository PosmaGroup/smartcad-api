using SmartCadControls;
namespace SmartCadGuiCommon
{
    partial class OperatorAssignCreateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OperatorAssignCreateForm));
            this.layoutControlOperatorAssignCreateForm = new DevExpress.XtraLayout.LayoutControl();
            this.simpleButtonDelete = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonClean = new DevExpress.XtraEditors.SimpleButton();
            this.gridControlExAssigment = new GridControlEx();
            this.gridViewExAssigment = new GridViewEx();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonCreate = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonRecurrence = new DevExpress.XtraEditors.SimpleButton();
            this.gridControlExOperators = new GridControlEx();
            this.gridViewExOperators = new GridViewEx();
            this.simpleButtonAssign = new DevExpress.XtraEditors.SimpleButton();
            this.gridControlExSupervisors = new GridControlEx();
            this.gridViewExSupervisors = new GridViewEx();
            this.timeEditEnd = new DevExpress.XtraEditors.TimeEdit();
            this.timeEditStart = new DevExpress.XtraEditors.TimeEdit();
            this.radioButtonByWorkShift = new System.Windows.Forms.RadioButton();
            this.radioButtonByRange = new System.Windows.Forms.RadioButton();
            this.labelControlDate = new DevExpress.XtraEditors.LabelControl();
            this.textEditName = new DevExpress.XtraEditors.TextEdit();
            this.textEditWorkShift = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupOperators = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupSupervisors = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemStart = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemEnd = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupAssigment = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.layoutControlGroupInf = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemWorkShift = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemDate = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlOperatorAssignCreateForm)).BeginInit();
            this.layoutControlOperatorAssignCreateForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExAssigment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExAssigment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExOperators)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExOperators)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExSupervisors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExSupervisors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeEditEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeEditStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditWorkShift.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupOperators)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupSupervisors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupAssigment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupInf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWorkShift)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDate)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControlOperatorAssignCreateForm
            // 
            this.layoutControlOperatorAssignCreateForm.AllowCustomizationMenu = false;
            this.layoutControlOperatorAssignCreateForm.Controls.Add(this.simpleButtonDelete);
            this.layoutControlOperatorAssignCreateForm.Controls.Add(this.simpleButtonClean);
            this.layoutControlOperatorAssignCreateForm.Controls.Add(this.gridControlExAssigment);
            this.layoutControlOperatorAssignCreateForm.Controls.Add(this.simpleButtonCancel);
            this.layoutControlOperatorAssignCreateForm.Controls.Add(this.simpleButtonCreate);
            this.layoutControlOperatorAssignCreateForm.Controls.Add(this.simpleButtonRecurrence);
            this.layoutControlOperatorAssignCreateForm.Controls.Add(this.gridControlExOperators);
            this.layoutControlOperatorAssignCreateForm.Controls.Add(this.simpleButtonAssign);
            this.layoutControlOperatorAssignCreateForm.Controls.Add(this.gridControlExSupervisors);
            this.layoutControlOperatorAssignCreateForm.Controls.Add(this.timeEditEnd);
            this.layoutControlOperatorAssignCreateForm.Controls.Add(this.timeEditStart);
            this.layoutControlOperatorAssignCreateForm.Controls.Add(this.radioButtonByWorkShift);
            this.layoutControlOperatorAssignCreateForm.Controls.Add(this.radioButtonByRange);
            this.layoutControlOperatorAssignCreateForm.Controls.Add(this.labelControlDate);
            this.layoutControlOperatorAssignCreateForm.Controls.Add(this.textEditName);
            this.layoutControlOperatorAssignCreateForm.Controls.Add(this.textEditWorkShift);
            this.layoutControlOperatorAssignCreateForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlOperatorAssignCreateForm.Location = new System.Drawing.Point(0, 0);
            this.layoutControlOperatorAssignCreateForm.Name = "layoutControlOperatorAssignCreateForm";
            this.layoutControlOperatorAssignCreateForm.Root = this.layoutControlGroup1;
            this.layoutControlOperatorAssignCreateForm.Size = new System.Drawing.Size(892, 586);
            this.layoutControlOperatorAssignCreateForm.TabIndex = 0;
            this.layoutControlOperatorAssignCreateForm.Text = "layoutControl1";
            // 
            // simpleButtonDelete
            // 
            this.simpleButtonDelete.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButtonDelete.Appearance.Options.UseFont = true;
            this.simpleButtonDelete.Enabled = false;
            this.simpleButtonDelete.Location = new System.Drawing.Point(809, 510);
            this.simpleButtonDelete.Name = "simpleButtonDelete";
            this.simpleButtonDelete.Size = new System.Drawing.Size(76, 26);
            this.simpleButtonDelete.StyleController = this.layoutControlOperatorAssignCreateForm;
            this.simpleButtonDelete.TabIndex = 19;
            this.simpleButtonDelete.Text = "simpleButtonDelete";
            this.simpleButtonDelete.Click += new System.EventHandler(this.simpleButtonDelete_Click);
            // 
            // simpleButtonClean
            // 
            this.simpleButtonClean.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButtonClean.Appearance.Options.UseFont = true;
            this.simpleButtonClean.Enabled = false;
            this.simpleButtonClean.Location = new System.Drawing.Point(729, 510);
            this.simpleButtonClean.Name = "simpleButtonClean";
            this.simpleButtonClean.Size = new System.Drawing.Size(76, 26);
            this.simpleButtonClean.StyleController = this.layoutControlOperatorAssignCreateForm;
            this.simpleButtonClean.TabIndex = 18;
            this.simpleButtonClean.Text = "simpleButtonClean";
            this.simpleButtonClean.Click += new System.EventHandler(this.simpleButtonClean_Click);
            // 
            // gridControlExAssigment
            // 
            this.gridControlExAssigment.EnableAutoFilter = true;
            this.gridControlExAssigment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExAssigment.Location = new System.Drawing.Point(443, 26);
            this.gridControlExAssigment.MainView = this.gridViewExAssigment;
            this.gridControlExAssigment.Name = "gridControlExAssigment";
            this.gridControlExAssigment.Size = new System.Drawing.Size(442, 480);
            this.gridControlExAssigment.TabIndex = 17;
            this.gridControlExAssigment.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExAssigment});
            this.gridControlExAssigment.ViewTotalRows = true;
            // 
            // gridViewExAssigment
            // 
            this.gridViewExAssigment.AllowFocusedRowChanged = true;
            this.gridViewExAssigment.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExAssigment.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExAssigment.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExAssigment.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExAssigment.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewExAssigment.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewExAssigment.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExAssigment.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExAssigment.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExAssigment.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExAssigment.EnablePreviewLineForFocusedRow = false;
            this.gridViewExAssigment.GridControl = this.gridControlExAssigment;
            this.gridViewExAssigment.Name = "gridViewExAssigment";
            this.gridViewExAssigment.OptionsDetail.EnableMasterViewMode = false;
            this.gridViewExAssigment.OptionsMenu.EnableFooterMenu = false;
            this.gridViewExAssigment.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExAssigment.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExAssigment.OptionsView.ShowFooter = true;
            this.gridViewExAssigment.OptionsView.ShowGroupPanel = false;
            this.gridViewExAssigment.ViewTotalRows = true;
            this.gridViewExAssigment.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewExAssigment_FocusedRowChanged);
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButtonCancel.Appearance.Options.UseFont = true;
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(814, 558);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(76, 26);
            this.simpleButtonCancel.StyleController = this.layoutControlOperatorAssignCreateForm;
            this.simpleButtonCancel.TabIndex = 16;
            this.simpleButtonCancel.Text = "simpleButtonCancel";
            // 
            // simpleButtonCreate
            // 
            this.simpleButtonCreate.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButtonCreate.Appearance.Options.UseFont = true;
            this.simpleButtonCreate.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButtonCreate.Enabled = false;
            this.simpleButtonCreate.Location = new System.Drawing.Point(734, 558);
            this.simpleButtonCreate.Name = "simpleButtonCreate";
            this.simpleButtonCreate.Size = new System.Drawing.Size(76, 26);
            this.simpleButtonCreate.StyleController = this.layoutControlOperatorAssignCreateForm;
            this.simpleButtonCreate.TabIndex = 15;
            this.simpleButtonCreate.Text = "simpleButtonCreate";
            this.simpleButtonCreate.Click += new System.EventHandler(this.simpleButtonCreate_Click);
            // 
            // simpleButtonRecurrence
            // 
            this.simpleButtonRecurrence.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButtonRecurrence.Appearance.Options.UseFont = true;
            this.simpleButtonRecurrence.Enabled = false;
            this.simpleButtonRecurrence.Location = new System.Drawing.Point(654, 558);
            this.simpleButtonRecurrence.Name = "simpleButtonRecurrence";
            this.simpleButtonRecurrence.Size = new System.Drawing.Size(76, 26);
            this.simpleButtonRecurrence.StyleController = this.layoutControlOperatorAssignCreateForm;
            this.simpleButtonRecurrence.TabIndex = 14;
            this.simpleButtonRecurrence.Text = "simpleButtonRecurrence";
            this.simpleButtonRecurrence.Click += new System.EventHandler(this.simpleButtonRecurrence_Click);
            // 
            // gridControlExOperators
            // 
            this.gridControlExOperators.EnableAutoFilter = true;
            this.gridControlExOperators.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExOperators.Location = new System.Drawing.Point(7, 411);
            this.gridControlExOperators.MainView = this.gridViewExOperators;
            this.gridControlExOperators.Name = "gridControlExOperators";
            this.gridControlExOperators.Size = new System.Drawing.Size(417, 138);
            this.gridControlExOperators.TabIndex = 13;
            this.gridControlExOperators.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExOperators});
            this.gridControlExOperators.ViewTotalRows = true;
            // 
            // gridViewExOperators
            // 
            this.gridViewExOperators.AllowFocusedRowChanged = true;
            this.gridViewExOperators.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExOperators.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExOperators.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExOperators.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExOperators.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewExOperators.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewExOperators.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExOperators.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExOperators.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExOperators.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExOperators.EnablePreviewLineForFocusedRow = false;
            this.gridViewExOperators.GridControl = this.gridControlExOperators;
            this.gridViewExOperators.Name = "gridViewExOperators";
            this.gridViewExOperators.OptionsMenu.EnableFooterMenu = false;
            this.gridViewExOperators.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExOperators.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExOperators.OptionsView.ShowFooter = true;
            this.gridViewExOperators.OptionsView.ShowGroupPanel = false;
            this.gridViewExOperators.ViewTotalRows = true;
            // 
            // simpleButtonAssign
            // 
            this.simpleButtonAssign.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButtonAssign.Appearance.Options.UseFont = true;
            this.simpleButtonAssign.Enabled = false;
            this.simpleButtonAssign.Location = new System.Drawing.Point(348, 352);
            this.simpleButtonAssign.Name = "simpleButtonAssign";
            this.simpleButtonAssign.Size = new System.Drawing.Size(76, 26);
            this.simpleButtonAssign.StyleController = this.layoutControlOperatorAssignCreateForm;
            this.simpleButtonAssign.TabIndex = 12;
            this.simpleButtonAssign.Text = "simpleButtonAssign";
            this.simpleButtonAssign.Click += new System.EventHandler(this.simpleButtonAssign_Click);
            // 
            // gridControlExSupervisors
            // 
            this.gridControlExSupervisors.EnableAutoFilter = true;
            this.gridControlExSupervisors.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExSupervisors.Location = new System.Drawing.Point(7, 181);
            this.gridControlExSupervisors.MainView = this.gridViewExSupervisors;
            this.gridControlExSupervisors.Name = "gridControlExSupervisors";
            this.gridControlExSupervisors.Size = new System.Drawing.Size(417, 167);
            this.gridControlExSupervisors.TabIndex = 11;
            this.gridControlExSupervisors.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExSupervisors});
            this.gridControlExSupervisors.ViewTotalRows = true;
            // 
            // gridViewExSupervisors
            // 
            this.gridViewExSupervisors.AllowFocusedRowChanged = true;
            this.gridViewExSupervisors.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExSupervisors.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExSupervisors.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExSupervisors.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExSupervisors.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewExSupervisors.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewExSupervisors.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExSupervisors.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExSupervisors.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExSupervisors.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExSupervisors.EnablePreviewLineForFocusedRow = false;
            this.gridViewExSupervisors.GridControl = this.gridControlExSupervisors;
            this.gridViewExSupervisors.Name = "gridViewExSupervisors";
            this.gridViewExSupervisors.OptionsDetail.EnableMasterViewMode = false;
            this.gridViewExSupervisors.OptionsMenu.EnableFooterMenu = false;
            this.gridViewExSupervisors.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExSupervisors.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExSupervisors.OptionsView.ShowFooter = true;
            this.gridViewExSupervisors.OptionsView.ShowGroupPanel = false;
            this.gridViewExSupervisors.ViewTotalRows = true;
            this.gridViewExSupervisors.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewExSupervisors_FocusedRowChanged);
            // 
            // timeEditEnd
            // 
            this.timeEditEnd.EditValue = new System.DateTime(2009, 7, 17, 0, 0, 0, 0);
            this.timeEditEnd.Location = new System.Drawing.Point(285, 157);
            this.timeEditEnd.Name = "timeEditEnd";
            this.timeEditEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.timeEditEnd.Properties.DisplayFormat.FormatString = "HH:mm";
            this.timeEditEnd.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.timeEditEnd.Properties.EditFormat.FormatString = "HH:mm";
            this.timeEditEnd.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.timeEditEnd.Properties.Mask.EditMask = "HH:mm";
            this.timeEditEnd.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.timeEditEnd.Size = new System.Drawing.Size(60, 20);
            this.timeEditEnd.StyleController = this.layoutControlOperatorAssignCreateForm;
            this.timeEditEnd.TabIndex = 10;
            this.timeEditEnd.EditValueChanged += new System.EventHandler(this.timeEditEnd_EditValueChanged);
            // 
            // timeEditStart
            // 
            this.timeEditStart.EditValue = new System.DateTime(2009, 7, 17, 0, 0, 0, 0);
            this.timeEditStart.Location = new System.Drawing.Point(115, 157);
            this.timeEditStart.Name = "timeEditStart";
            this.timeEditStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.timeEditStart.Properties.DisplayFormat.FormatString = "HH:mm";
            this.timeEditStart.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.timeEditStart.Properties.EditFormat.FormatString = "HH:mm";
            this.timeEditStart.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.timeEditStart.Properties.Mask.EditMask = "HH:mm";
            this.timeEditStart.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.timeEditStart.Size = new System.Drawing.Size(61, 20);
            this.timeEditStart.StyleController = this.layoutControlOperatorAssignCreateForm;
            this.timeEditStart.TabIndex = 9;
            this.timeEditStart.EditValueChanged += new System.EventHandler(this.timeEditStart_EditValueChanged);
            // 
            // radioButtonByWorkShift
            // 
            this.radioButtonByWorkShift.Location = new System.Drawing.Point(116, 127);
            this.radioButtonByWorkShift.Name = "radioButtonByWorkShift";
            this.radioButtonByWorkShift.Size = new System.Drawing.Size(116, 26);
            this.radioButtonByWorkShift.TabIndex = 8;
            this.radioButtonByWorkShift.TabStop = true;
            this.radioButtonByWorkShift.Text = "radioButtonByWorkShift";
            this.radioButtonByWorkShift.UseVisualStyleBackColor = true;
            this.radioButtonByWorkShift.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // radioButtonByRange
            // 
            this.radioButtonByRange.Location = new System.Drawing.Point(7, 127);
            this.radioButtonByRange.Name = "radioButtonByRange";
            this.radioButtonByRange.Size = new System.Drawing.Size(105, 26);
            this.radioButtonByRange.TabIndex = 7;
            this.radioButtonByRange.TabStop = true;
            this.radioButtonByRange.Text = "radioButtonByRange";
            this.radioButtonByRange.UseVisualStyleBackColor = true;
            this.radioButtonByRange.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // labelControlDate
            // 
            this.labelControlDate.Location = new System.Drawing.Point(139, 74);
            this.labelControlDate.Name = "labelControlDate";
            this.labelControlDate.Size = new System.Drawing.Size(285, 20);
            this.labelControlDate.StyleController = this.layoutControlOperatorAssignCreateForm;
            this.labelControlDate.TabIndex = 6;
            this.labelControlDate.Text = "labelControlDate";
            // 
            // textEditName
            // 
            this.textEditName.Enabled = false;
            this.textEditName.Location = new System.Drawing.Point(139, 26);
            this.textEditName.Name = "textEditName";
            this.textEditName.Size = new System.Drawing.Size(285, 20);
            this.textEditName.StyleController = this.layoutControlOperatorAssignCreateForm;
            this.textEditName.TabIndex = 5;
            // 
            // textEditWorkShift
            // 
            this.textEditWorkShift.Enabled = false;
            this.textEditWorkShift.Location = new System.Drawing.Point(139, 50);
            this.textEditWorkShift.Name = "textEditWorkShift";
            this.textEditWorkShift.Size = new System.Drawing.Size(285, 20);
            this.textEditWorkShift.StyleController = this.layoutControlOperatorAssignCreateForm;
            this.textEditWorkShift.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.emptySpaceItem2,
            this.layoutControlGroupOperators,
            this.layoutControlGroupSupervisors,
            this.layoutControlGroupAssigment,
            this.splitterItem1,
            this.layoutControlGroupInf});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(892, 586);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.simpleButtonRecurrence;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(652, 556);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(80, 30);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(80, 30);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(80, 30);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.simpleButtonCreate;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(732, 556);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(80, 30);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(80, 30);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(80, 30);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.simpleButtonCancel;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(812, 556);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(80, 30);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(80, 30);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(80, 30);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 556);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(652, 30);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupOperators
            // 
            this.layoutControlGroupOperators.CustomizationFormText = "layoutControlGroupOperators";
            this.layoutControlGroupOperators.ExpandButtonMode = DevExpress.Utils.Controls.ExpandButtonMode.Inverted;
            this.layoutControlGroupOperators.ExpandButtonVisible = true;
            this.layoutControlGroupOperators.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroupOperators.Location = new System.Drawing.Point(0, 385);
            this.layoutControlGroupOperators.Name = "layoutControlGroupOperators";
            this.layoutControlGroupOperators.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupOperators.Size = new System.Drawing.Size(431, 171);
            this.layoutControlGroupOperators.Text = "layoutControlGroupOperators";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridControlExOperators;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(105, 25);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(421, 142);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroupSupervisors
            // 
            this.layoutControlGroupSupervisors.CustomizationFormText = "layoutControlGroupSupervisors";
            this.layoutControlGroupSupervisors.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItemStart,
            this.layoutControlItemEnd,
            this.layoutControlItem4,
            this.emptySpaceItem1,
            this.layoutControlItem5,
            this.emptySpaceItem4,
            this.emptySpaceItem5});
            this.layoutControlGroupSupervisors.Location = new System.Drawing.Point(0, 101);
            this.layoutControlGroupSupervisors.Name = "layoutControlGroupSupervisors";
            this.layoutControlGroupSupervisors.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupSupervisors.Size = new System.Drawing.Size(431, 284);
            this.layoutControlGroupSupervisors.Text = "layoutControlGroupSupervisors";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.radioButtonByRange;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(109, 30);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(109, 30);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(109, 30);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.radioButtonByWorkShift;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(109, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(120, 30);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(120, 30);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(120, 30);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItemStart
            // 
            this.layoutControlItemStart.Control = this.timeEditStart;
            this.layoutControlItemStart.CustomizationFormText = "layoutControlItemStart";
            this.layoutControlItemStart.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItemStart.Name = "layoutControlItemStart";
            this.layoutControlItemStart.Size = new System.Drawing.Size(173, 24);
            this.layoutControlItemStart.Text = "layoutControlItemStart";
            this.layoutControlItemStart.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItemStart.TextSize = new System.Drawing.Size(103, 13);
            this.layoutControlItemStart.TextToControlDistance = 5;
            // 
            // layoutControlItemEnd
            // 
            this.layoutControlItemEnd.Control = this.timeEditEnd;
            this.layoutControlItemEnd.CustomizationFormText = "layoutControlItemEnd";
            this.layoutControlItemEnd.Location = new System.Drawing.Point(173, 30);
            this.layoutControlItemEnd.Name = "layoutControlItemEnd";
            this.layoutControlItemEnd.Size = new System.Drawing.Size(169, 24);
            this.layoutControlItemEnd.Text = "layoutControlItemEnd";
            this.layoutControlItemEnd.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItemEnd.TextSize = new System.Drawing.Size(100, 13);
            this.layoutControlItemEnd.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.gridControlExSupervisors;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 54);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(105, 25);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(421, 171);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 225);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(341, 30);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.simpleButtonAssign;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(341, 225);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(80, 30);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(80, 30);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(80, 30);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(342, 30);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(79, 24);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(229, 0);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(192, 30);
            this.emptySpaceItem5.Text = "emptySpaceItem5";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupAssigment
            // 
            this.layoutControlGroupAssigment.CustomizationFormText = "layoutControlGroupAssigment";
            this.layoutControlGroupAssigment.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem9,
            this.emptySpaceItem3,
            this.layoutControlItem10,
            this.layoutControlItem11});
            this.layoutControlGroupAssigment.Location = new System.Drawing.Point(436, 0);
            this.layoutControlGroupAssigment.Name = "layoutControlGroupAssigment";
            this.layoutControlGroupAssigment.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupAssigment.Size = new System.Drawing.Size(456, 556);
            this.layoutControlGroupAssigment.Text = "layoutControlGroupAssigment";
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.gridControlExAssigment;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(105, 25);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(446, 484);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 484);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(286, 43);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.simpleButtonClean;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(286, 484);
            this.layoutControlItem10.MaxSize = new System.Drawing.Size(80, 43);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(80, 43);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 15);
            this.layoutControlItem10.Size = new System.Drawing.Size(80, 43);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Text = "layoutControlItem10";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextToControlDistance = 0;
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.simpleButtonDelete;
            this.layoutControlItem11.CustomizationFormText = "layoutControlItem11";
            this.layoutControlItem11.Location = new System.Drawing.Point(366, 484);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(80, 43);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(80, 43);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 15);
            this.layoutControlItem11.Size = new System.Drawing.Size(80, 43);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "layoutControlItem11";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextToControlDistance = 0;
            this.layoutControlItem11.TextVisible = false;
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.CustomizationFormText = "splitterItem1";
            this.splitterItem1.Location = new System.Drawing.Point(431, 0);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(5, 556);
            // 
            // layoutControlGroupInf
            // 
            this.layoutControlGroupInf.CustomizationFormText = "layoutControlGroupInf";
            this.layoutControlGroupInf.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemName,
            this.layoutControlItemWorkShift,
            this.layoutControlItemDate});
            this.layoutControlGroupInf.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupInf.Name = "layoutControlGroupInf";
            this.layoutControlGroupInf.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupInf.Size = new System.Drawing.Size(431, 101);
            this.layoutControlGroupInf.Text = "layoutControlGroupInf";
            // 
            // layoutControlItemName
            // 
            this.layoutControlItemName.Control = this.textEditName;
            this.layoutControlItemName.CustomizationFormText = "layoutControlItemName";
            this.layoutControlItemName.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemName.Name = "layoutControlItemName";
            this.layoutControlItemName.Size = new System.Drawing.Size(421, 24);
            this.layoutControlItemName.Text = "layoutControlItemName";
            this.layoutControlItemName.TextSize = new System.Drawing.Size(128, 13);
            // 
            // layoutControlItemWorkShift
            // 
            this.layoutControlItemWorkShift.Control = this.textEditWorkShift;
            this.layoutControlItemWorkShift.CustomizationFormText = "layoutControlItemWorkShift";
            this.layoutControlItemWorkShift.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItemWorkShift.Name = "layoutControlItemWorkShift";
            this.layoutControlItemWorkShift.Size = new System.Drawing.Size(421, 24);
            this.layoutControlItemWorkShift.Text = "layoutControlItemWorkShift";
            this.layoutControlItemWorkShift.TextSize = new System.Drawing.Size(128, 13);
            // 
            // layoutControlItemDate
            // 
            this.layoutControlItemDate.Control = this.labelControlDate;
            this.layoutControlItemDate.CustomizationFormText = "layoutControlItemDate";
            this.layoutControlItemDate.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItemDate.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItemDate.MinSize = new System.Drawing.Size(200, 24);
            this.layoutControlItemDate.Name = "layoutControlItemDate";
            this.layoutControlItemDate.Size = new System.Drawing.Size(421, 24);
            this.layoutControlItemDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemDate.Text = "layoutControlItemDate";
            this.layoutControlItemDate.TextSize = new System.Drawing.Size(128, 13);
            // 
            // OperatorAssignCreateForm
            // 
            this.AcceptButton = this.simpleButtonCreate;
            this.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonCancel;
            this.ClientSize = new System.Drawing.Size(892, 586);
            this.Controls.Add(this.layoutControlOperatorAssignCreateForm);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(900, 620);
            this.Name = "OperatorAssignCreateForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "OperatorAssignCreateForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OperatorAssignCreateForm_FormClosing);
            this.Load += new System.EventHandler(this.OperatorAssignCreateForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlOperatorAssignCreateForm)).EndInit();
            this.layoutControlOperatorAssignCreateForm.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExAssigment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExAssigment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExOperators)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExOperators)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExSupervisors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExSupervisors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeEditEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeEditStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditWorkShift.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupOperators)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupSupervisors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupAssigment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupInf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWorkShift)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDate)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControlOperatorAssignCreateForm;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.LabelControl labelControlDate;
        private DevExpress.XtraEditors.TextEdit textEditName;
        private DevExpress.XtraEditors.TextEdit textEditWorkShift;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemWorkShift;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDate;
        private System.Windows.Forms.RadioButton radioButtonByWorkShift;
        private System.Windows.Forms.RadioButton radioButtonByRange;
        private DevExpress.XtraEditors.TimeEdit timeEditEnd;
        private DevExpress.XtraEditors.TimeEdit timeEditStart;
        private DevExpress.XtraEditors.SimpleButton simpleButtonAssign;
        private GridControlEx gridControlExSupervisors;
        private GridViewEx gridViewExSupervisors;
        private GridControlEx gridControlExOperators;
        private GridViewEx gridViewExOperators;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCreate;
        private DevExpress.XtraEditors.SimpleButton simpleButtonRecurrence;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private GridControlEx gridControlExAssigment;
        private GridViewEx gridViewExAssigment;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.SimpleButton simpleButtonDelete;
        private DevExpress.XtraEditors.SimpleButton simpleButtonClean;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupOperators;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupSupervisors;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemStart;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemEnd;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupAssigment;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupInf;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
    }
}