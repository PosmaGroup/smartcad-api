﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraScheduler;
using Smartmatic.SmartCad.Service;
using System.Collections;
using DevExpress.XtraEditors;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadControls;
using SmartCadCore.Core;

namespace SmartCadGuiCommon
{
    public partial class AppointmentFormEx : XtraForm
    {
        #region AppointmentFormExGridData -- Class Data

        public class AppointmentFormExGridData
        {

            public AppointmentFormExGridData(OperatorClientData ope, string supervisor)
            {
                this.Operator = ope;
                this.FirstName = ope.FirstName;
                this.LastName = ope.LastName;
				if (ope.IsSupervisor)
					this.TypeOperator = ResourceLoader.GetString2("Supervisor");
				else
					this.TypeOperator = ResourceLoader.GetString2("Operators");
				//else if (ope.DepartmentTypes.Count > 0)
				//    this.TypeOperator = ResourceLoader.GetString2("Dispatch");
				//else
				//    this.TypeOperator = ResourceLoader.GetString2("FirstLevel");

                this.Category = ope.OperatorCategory;
                this.Supervisor = supervisor;
            }

            public OperatorClientData Operator { get; set; }

			[GridControlRowInfoData(Searchable = true,GroupIndex = 0,Visible = false)]
            public string TypeOperator { get; set; }

			[GridControlRowInfoData(Searchable = true)]
			public string FirstName { get; set; }

			[GridControlRowInfoData(Searchable = true)]
			public string LastName { get; set; }

			[GridControlRowInfoData(Searchable = true)]
			public string Category { get; set; }
			
			
			[GridControlRowInfoData(Searchable = true)]
			public string Supervisor { get; set; }
        } 
        #endregion


        List<AppointmentFormExGridData> dataSource;

        public AppointmentFormEx()
        {
            InitializeComponent();

            dataSource = new List<AppointmentFormExGridData>();
        }

        public AppointmentFormEx(Appointment appointment, List<OperatorClientData> listOpe): this()
        {
            this.textName.Text = appointment.Subject;
            this.textType.Text = (string)appointment.CustomFields["AType"];
            this.Text = (string)appointment.CustomFields["AType"];

			int wsvc = (int)appointment.CustomFields["WorkShiftVariationScheduleCode"];
			List<OperatorAssignClientData> Supervisors = GetAllSupervisorsInVariation(wsvc);
			CreateListOperators(listOpe, Supervisors); 
			if (appointment.LabelId == 0)
            {
                this.Icon = ResourceLoader.GetIcon("$Icon.ExtraTime");
                layoutControlGroup2.Text = ResourceLoader.GetString2("WorkShift");
            }
            else if (appointment.LabelId == 1)
            {
                this.Icon = ResourceLoader.GetIcon("$Icon.ExtraTime");
                layoutControlGroup2.Text = ResourceLoader.GetString2("ExtraTime");
            }
            else if (appointment.LabelId == 2) 
            {
                this.Icon = ResourceLoader.GetIcon("$Icon.VacationOrPermission");
                layoutControlGroup2.Text = ResourceLoader.GetString2("FreeTimeFormText");
                gridViewOperators.Columns["Supervisor"].Visible = false;            
            }

            this.textBoxDescription.Text = appointment.Description;
            this.textDateStart.Text = appointment.Start.ToShortDateString();
            this.textTimeStart.Text = appointment.Start.ToString("HH:mm");
            this.textDateEnd.Text = appointment.End.ToShortDateString();
            this.textTimeEnd.Text = appointment.End.ToString("HH:mm");

            LoadLenguage();
        }

        private void LoadLenguage()
        {
            
            this.Text = ResourceLoader.GetString2("Details");
            layoutControlGroup3.Text = ResourceLoader.GetString2("Personal");
            labelName.Text = ResourceLoader.GetString2("$Message.Name") + ":";
            labelImage.Text = ResourceLoader.GetString2("$Message.Type") + ":";
            labelDateStart.Text = ResourceLoader.GetString2("StartDate") + ":";
            labelDateEnd.Text = ResourceLoader.GetString2("EndDate") + ":";
            labelDescription.Text = ResourceLoader.GetString2("Description") + ":";
            simpleButtonOk.Text = ResourceLoader.GetString2("$Message.Accept");
        }

		private void CreateListOperators(List<OperatorClientData> listOpe, List<OperatorAssignClientData> Supervisors)
        {
			gridControlOperators.Type = typeof(AppointmentFormExGridData);
			foreach (OperatorClientData var in listOpe)
			{
				string supName = string.Empty;
				try
				{
					OperatorAssignClientData sup = Supervisors.Where<OperatorAssignClientData>(obj => obj.SupervisedOperatorCode == var.Code).ElementAt<OperatorAssignClientData>(0);
					supName = sup.SupFirstName+ " " + sup.SupLastName;
				}
				catch
				{

				}
				dataSource.Add(new AppointmentFormExGridData(var, supName));
			}
            this.gridControlOperators.BeginUpdate();
            this.gridControlOperators.DataSource = dataSource;
            this.gridControlOperators.EndUpdate();
            this.gridViewOperators.ExpandAllGroups();
			this.gridViewOperators.OptionsView.ShowAutoFilterRow = true;
			this.gridControlOperators.EnableAutoFilter = true;
			this.gridControlOperators.ViewTotalRows = true;
        }

        private List<OperatorAssignClientData> GetAllSupervisorsInVariation(int wsvc)
        {
            string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorsAssignByWorkShiftVariationSchedule, wsvc);
			List<OperatorAssignClientData> list = new List<OperatorAssignClientData>(((IList)ServerServiceClient.GetInstance().SearchClientObjects(hql)).Cast<OperatorAssignClientData>());
            
            return list;
        }


    }
}
