using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.ClientData;

namespace SmartCadGuiCommon
{
    [Serializable]
    public class SupervisedApplicationChangedEventArgs : EventArgs
    {
        private string applicationName;
        private DepartmentTypeClientData departmentType;

        public DepartmentTypeClientData DepartmentType
        {
            get
            {
                return departmentType;
            }
        }

        public string ApplicationName
        {
            get
            {
                return applicationName;
            }
        }

        public SupervisedApplicationChangedEventArgs(string application, DepartmentTypeClientData department)
            : base()
        {
            this.applicationName = application;
            this.departmentType = department;
        }
    }
}
