using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Smartmatic.SmartCad.Service;
using System.ServiceModel;
using System.Reflection;
using System.Text.RegularExpressions;
using DevExpress.XtraEditors;
using SmartCadCore.ClientData;
using SmartCadCore.Enums;
using SmartCadCore.Common;

namespace SmartCadGuiCommon
{
    public partial class TrainingCreateForm : XtraForm
    {
        public TrainingCreateForm()
        {
            InitializeComponent();
       
        }

        public TrainingCreateForm(TrainingCourseClientData course, FormBehavior behavior)
            : this()
        {
            Behavior = behavior;
            SelectedCourse = course;
            LoadLanguage();
        }

        public TrainingCreateForm(TrainingCourseClientData course) : this()
        {
            SelectedCourse = course;
            LoadLanguage();
        }

        private void LoadLanguage()
        {
            this.Icon = ResourceLoader.GetIcon("$Icon.TrainingCourse");
            switch (behavior)
            {
                case FormBehavior.Create:
                    Text = ResourceLoader.GetString2("TrainingCreateFormCreateText");
                    buttonExOk.Text = ResourceLoader.GetString("TrainingCreateFormCreateButtonOkText");
                    this.Icon = ResourceLoader.GetIcon("$Icon.Create");
                    break;
                case FormBehavior.Edit:
                    Text = ResourceLoader.GetString2("TrainingCreateFormEditText");
                    buttonExOk.Text = ResourceLoader.GetString("TrainingCreateFormEditButtonOkText");
                    this.Icon = ResourceLoader.GetIcon("$Icon.Update");
                    break;
                default:
                    break;
            }

            this.layoutControlGroupTrainingCourse.Text = ResourceLoader.GetString2("TrainingCourseInformation");
            this.textBoxExNameitem.Text = ResourceLoader.GetString2("Name") + ": *";
            this.textBoxExObjitem.Text = ResourceLoader.GetString2("Objectives") + ": *";
            this.textBoxExContentitem.Text = ResourceLoader.GetString2("Content") + ": *";
            this.textBoxExContactitem.Text = ResourceLoader.GetString2("ContactPerson") + ": *";
            this.textBoxExPhoneitem.Text = ResourceLoader.GetString2("ContactNumber") + ": *";
            this.textBoxExLinkitem.Text = ResourceLoader.GetString2("Link");
            this.buttonExCancel.Text = ResourceLoader.GetString2("Cancel");
        }

        public void SetReadOnly(bool readOnly) 
        {
            textBoxExName.ReadOnly = readOnly;
            textBoxExContent.ReadOnly = readOnly;
            textBoxExObj.ReadOnly = readOnly;
            textBoxExPhone.ReadOnly = readOnly;
            textBoxExContact.ReadOnly = readOnly;
            textBoxExLink.ReadOnly = readOnly;
            Text = ResourceLoader.GetString2("TrainingCreateFormEditText");
            if (readOnly)
            {
                behavior = FormBehavior.Edit;
                this.buttonExCancel.Text = ResourceLoader.GetString("TrainingCreateFormEditButtonOkText");
                this.layoutControlItemOk.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            }
        }

        private FormBehavior behavior;
        public FormBehavior Behavior
        {
            set
            {
                behavior = value;

            }
            get
            {
                return behavior;
            }
        }

        private bool buttonOkPressed = false;

        public bool ButtonOkPressed
        {
            get { return buttonOkPressed; }
        }

        private TrainingCourseClientData selectedCourse;
        public TrainingCourseClientData SelectedCourse
        {
            get
            {
                return selectedCourse;
            }
            set
            {
                selectedCourse = value;

                if (selectedCourse != null && selectedCourse.Code != 0)
                {
                    textBoxExName.Text = selectedCourse.Name;
                    textBoxExContent.Text = selectedCourse.Content;
                    textBoxExObj.Text = selectedCourse.Objective;
                    textBoxExPhone.Text = selectedCourse.PhoneContactPerson;
                    textBoxExContact.Text = selectedCourse.ContactPerson;
                    textBoxExLink.Text = selectedCourse.Link;
                }

                ButtonsActivation(null, null);
            }
        }

     

        private void ButtonsActivation(object sender, System.EventArgs e)
        {
            if (textBoxExName.Text.Trim() == "" || textBoxExContent.Text.Trim() == "" || textBoxExObj.Text.Trim() == "" ||
                textBoxExContact.Text.Trim() == "" || textBoxExPhone.Text.Trim() == "")
                buttonExOk.Enabled = false;
            else
                buttonExOk.Enabled = true;
        }

        private void buttonExOk_Click(object sender, EventArgs e)
        {
            //if (ValidateURL(textBoxExLink.Text.Trim()) == false)
            //{
            //    DialogResult = DialogResult.None;
            //    MessageForm.Show(ResourceLoader.GetString2("NoValidLink"), MessageFormType.Error);
            //    return;
            //}       
            try
            {
                BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[1] { GetType().GetMethod("buttonExOK_Click1", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[0] });
                processForm.CanThrowError = true;
                processForm.ShowDialog();
            }
            catch (FaultException ex)
            {
                buttonOkPressed = false;
                GetErrorFocus(ex);
                SelectedCourse = (TrainingCourseClientData)ServerServiceClient.GetInstance().RefreshClient(selectedCourse);
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, MessageFormType.Error);
            }
            catch (Exception ex)
            {
                buttonOkPressed = false;
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, ex);
            }
        }


        private void buttonExOK_Click1()
        {
            buttonOkPressed = true;
            if (behavior == FormBehavior.Create)
            {
                selectedCourse = new TrainingCourseClientData();
            }
         
            selectedCourse.Name = textBoxExName.Text.Trim();
            selectedCourse.Content = textBoxExContent.Text.Trim();
            selectedCourse.Objective = textBoxExObj.Text.Trim();
            selectedCourse.PhoneContactPerson = textBoxExPhone.Text.Trim();
            selectedCourse.Link = textBoxExLink.Text.Trim();
            selectedCourse.ContactPerson = textBoxExContact.Text.Trim();

            ServerServiceClient.GetInstance().SaveOrUpdateClientData(selectedCourse);
        }

        private void GetErrorFocus(FaultException ex)
        {
            if (ex.Message.Contains(ResourceLoader.GetString2("UK_TRAINING_COURSE")))
            {
                if (behavior == FormBehavior.Create)
                    textBoxExName.Text = textBoxExName.Text;

                textBoxExName.Focus();
            }
            
        }

        private bool ValidateURL(string url) 
        { 
            bool result = true;
            if (url != string.Empty)
            {
                Regex exp = new Regex("^(?:http|https|ftp)://[a-zA-Z0-9.-]+(?::d{1,5})?(?:[A-Za-z0-9.;:@&=+$,?/]|%u[0-9A-Fa-f]{4}|%[0-9A-Fa-f]{2})*$");

                result = exp.IsMatch(url);
            }
            return result;
        }
    }
}