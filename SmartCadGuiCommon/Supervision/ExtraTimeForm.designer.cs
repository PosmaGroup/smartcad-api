using SmartCadControls;
namespace SmartCadGuiCommon
{
    partial class ExtraTimeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExtraTimeForm));
            this.gridControlOperators = new GridControlEx();
            this.gridViewOperators = new GridViewEx();
            this.gridControlDetails = new GridControlEx();
            this.gridViewDetails = new GridViewEx();
            this.gridControlExtraTime = new GridControlEx();
            this.gridViewExtraTime = new GridViewEx();
            this.RibbonControl = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.barButtonItemCreateExtraTime = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemDeleteExtraTime = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemModifyExtraTime = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemCreateVacations = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemDeleteVacations = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemModifyVacations = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemConsultVacations = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemConsultExtraTime = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemPersonalAssignVacations = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemPersonalAssignExtraTime = new DevExpress.XtraBars.BarButtonItem();
            this.RibbonPageOperation = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPage();
            this.ribbonPageGroupExtraTime = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupVacationAndAbsence = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.layoutControlExtraForm = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupExtraTime = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupDetails = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupOperators = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlOperators)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewOperators)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExtraTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExtraTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RibbonControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlExtraForm)).BeginInit();
            this.layoutControlExtraForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupExtraTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupOperators)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControlOperators
            // 
            this.gridControlOperators.EnableAutoFilter = true;
            this.gridControlOperators.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlOperators.Location = new System.Drawing.Point(641, 481);
            this.gridControlOperators.LookAndFeel.SkinName = "Blue";
            this.gridControlOperators.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gridControlOperators.MainView = this.gridViewOperators;
            this.gridControlOperators.Name = "gridControlOperators";
            this.gridControlOperators.Size = new System.Drawing.Size(614, 291);
            this.gridControlOperators.TabIndex = 0;
            this.gridControlOperators.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewOperators});
            this.gridControlOperators.ViewTotalRows = true;
            // 
            // gridViewOperators
            // 
            this.gridViewOperators.AllowFocusedRowChanged = true;
            this.gridViewOperators.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewOperators.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewOperators.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewOperators.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewOperators.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewOperators.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewOperators.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewOperators.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewOperators.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewOperators.EnablePreviewLineForFocusedRow = false;
            this.gridViewOperators.GridControl = this.gridControlOperators;
            this.gridViewOperators.Name = "gridViewOperators";
            this.gridViewOperators.OptionsBehavior.Editable = false;
            this.gridViewOperators.OptionsCustomization.AllowFilter = false;
            this.gridViewOperators.OptionsCustomization.AllowGroup = false;
            this.gridViewOperators.OptionsMenu.EnableColumnMenu = false;
            this.gridViewOperators.OptionsMenu.EnableFooterMenu = false;
            this.gridViewOperators.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridViewOperators.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewOperators.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewOperators.OptionsView.ShowFooter = true;
            this.gridViewOperators.OptionsView.ShowGroupPanel = false;
            this.gridViewOperators.OptionsView.ShowIndicator = false;
            this.gridViewOperators.ViewTotalRows = true;
            // 
            // gridControlDetails
            // 
            this.gridControlDetails.EnableAutoFilter = false;
            this.gridControlDetails.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlDetails.Location = new System.Drawing.Point(15, 481);
            this.gridControlDetails.LookAndFeel.SkinName = "Blue";
            this.gridControlDetails.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gridControlDetails.MainView = this.gridViewDetails;
            this.gridControlDetails.Name = "gridControlDetails";
            this.gridControlDetails.Size = new System.Drawing.Size(612, 291);
            this.gridControlDetails.TabIndex = 0;
            this.gridControlDetails.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDetails});
            this.gridControlDetails.ViewTotalRows = true;
            // 
            // gridViewDetails
            // 
            this.gridViewDetails.AllowFocusedRowChanged = true;
            this.gridViewDetails.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewDetails.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewDetails.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewDetails.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewDetails.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewDetails.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewDetails.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewDetails.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewDetails.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewDetails.EnablePreviewLineForFocusedRow = false;
            this.gridViewDetails.GridControl = this.gridControlDetails;
            this.gridViewDetails.Name = "gridViewDetails";
            this.gridViewDetails.OptionsCustomization.AllowFilter = false;
            this.gridViewDetails.OptionsMenu.EnableColumnMenu = false;
            this.gridViewDetails.OptionsMenu.EnableFooterMenu = false;
            this.gridViewDetails.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridViewDetails.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewDetails.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewDetails.OptionsView.ShowFooter = true;
            this.gridViewDetails.OptionsView.ShowGroupPanel = false;
            this.gridViewDetails.OptionsView.ShowIndicator = false;
            this.gridViewDetails.ViewTotalRows = true;
            // 
            // gridControlExtraTime
            // 
            this.gridControlExtraTime.EnableAutoFilter = true;
            this.gridControlExtraTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExtraTime.Location = new System.Drawing.Point(10, 30);
            this.gridControlExtraTime.LookAndFeel.SkinName = "Blue";
            this.gridControlExtraTime.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gridControlExtraTime.MainView = this.gridViewExtraTime;
            this.gridControlExtraTime.Name = "gridControlExtraTime";
            this.gridControlExtraTime.Size = new System.Drawing.Size(1250, 422);
            this.gridControlExtraTime.TabIndex = 4;
            this.gridControlExtraTime.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExtraTime});
            this.gridControlExtraTime.ViewTotalRows = true;
            // 
            // gridViewExtraTime
            // 
            this.gridViewExtraTime.AllowFocusedRowChanged = true;
            this.gridViewExtraTime.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExtraTime.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExtraTime.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExtraTime.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExtraTime.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewExtraTime.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewExtraTime.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExtraTime.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExtraTime.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExtraTime.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExtraTime.EnablePreviewLineForFocusedRow = false;
            this.gridViewExtraTime.GridControl = this.gridControlExtraTime;
            this.gridViewExtraTime.GroupFormat = "[#image]{1} {2}";
            this.gridViewExtraTime.Name = "gridViewExtraTime";
            this.gridViewExtraTime.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridViewExtraTime.OptionsBehavior.Editable = false;
            this.gridViewExtraTime.OptionsCustomization.AllowFilter = false;
            this.gridViewExtraTime.OptionsMenu.EnableColumnMenu = false;
            this.gridViewExtraTime.OptionsMenu.EnableFooterMenu = false;
            this.gridViewExtraTime.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridViewExtraTime.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewExtraTime.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExtraTime.OptionsView.ShowAutoFilterRow = true;
            this.gridViewExtraTime.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExtraTime.OptionsView.ShowFooter = true;
            this.gridViewExtraTime.OptionsView.ShowGroupPanel = false;
            this.gridViewExtraTime.OptionsView.ShowIndicator = false;
            this.gridViewExtraTime.ViewTotalRows = true;
            this.gridViewExtraTime.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewExtraTime_FocusedRowChanged);
            this.gridViewExtraTime.DoubleClick += new System.EventHandler(this.gridViewExtraTime_DoubleClick);
            // 
            // RibbonControl
            // 
            this.RibbonControl.Dock = System.Windows.Forms.DockStyle.None;
            this.RibbonControl.ExpandCollapseItem.Id = 0;
            this.RibbonControl.ExpandCollapseItem.Name = "";
            this.RibbonControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RibbonControl.Images = this.imageList;
            this.RibbonControl.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.RibbonControl.ExpandCollapseItem,
            this.barButtonItemCreateExtraTime,
            this.barButtonItemDeleteExtraTime,
            this.barButtonItemModifyExtraTime,
            this.barButtonItemCreateVacations,
            this.barButtonItemDeleteVacations,
            this.barButtonItemModifyVacations,
            this.barButtonItemConsultVacations,
            this.barButtonItemConsultExtraTime,
            this.barButtonItemPersonalAssignVacations,
            this.barButtonItemPersonalAssignExtraTime});
            this.RibbonControl.ItemsVertAlign = DevExpress.Utils.VertAlignment.Top;
            this.RibbonControl.LargeImages = this.imageList;
            this.RibbonControl.Location = new System.Drawing.Point(552, 12);
            this.RibbonControl.MaxItemId = 263;
            this.RibbonControl.MdiMergeStyle = DevExpress.XtraBars.Ribbon.RibbonMdiMergeStyle.Always;
            this.RibbonControl.Name = "RibbonControl";
            this.RibbonControl.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.RibbonPageOperation});
            this.RibbonControl.ShowToolbarCustomizeItem = false;
            this.RibbonControl.Size = new System.Drawing.Size(309, 120);
            this.RibbonControl.Toolbar.ShowCustomizeItem = false;
            this.RibbonControl.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "logout.gif");
            // 
            // barButtonItemCreateExtraTime
            // 
            this.barButtonItemCreateExtraTime.Caption = "Crear";
            this.barButtonItemCreateExtraTime.Enabled = false;
            this.barButtonItemCreateExtraTime.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemCreateExtraTime.Glyph")));
            this.barButtonItemCreateExtraTime.Id = 251;
            this.barButtonItemCreateExtraTime.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemCreateExtraTime.Name = "barButtonItemCreateExtraTime";
            this.barButtonItemCreateExtraTime.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemCreateExtraTime.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemCreateExtraTime_ItemClick);
            // 
            // barButtonItemDeleteExtraTime
            // 
            this.barButtonItemDeleteExtraTime.Caption = "Eliminar";
            this.barButtonItemDeleteExtraTime.Enabled = false;
            this.barButtonItemDeleteExtraTime.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemDeleteExtraTime.Glyph")));
            this.barButtonItemDeleteExtraTime.Id = 252;
            this.barButtonItemDeleteExtraTime.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemDeleteExtraTime.Name = "barButtonItemDeleteExtraTime";
            this.barButtonItemDeleteExtraTime.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemDeleteExtraTime.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemDeleteExtraTime_ItemClick);
            // 
            // barButtonItemModifyExtraTime
            // 
            this.barButtonItemModifyExtraTime.Caption = "Modificar";
            this.barButtonItemModifyExtraTime.Enabled = false;
            this.barButtonItemModifyExtraTime.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemModifyExtraTime.Glyph")));
            this.barButtonItemModifyExtraTime.Id = 253;
            this.barButtonItemModifyExtraTime.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemModifyExtraTime.Name = "barButtonItemModifyExtraTime";
            this.barButtonItemModifyExtraTime.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemModifyExtraTime.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemModifyExtraTime_ItemClick);
            // 
            // barButtonItemCreateVacations
            // 
            this.barButtonItemCreateVacations.Caption = "Crear";
            this.barButtonItemCreateVacations.Enabled = false;
            this.barButtonItemCreateVacations.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemCreateVacations.Glyph")));
            this.barButtonItemCreateVacations.Id = 255;
            this.barButtonItemCreateVacations.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemCreateVacations.Name = "barButtonItemCreateVacations";
            this.barButtonItemCreateVacations.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemCreateVacations.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemCreateVacations_ItemClick);
            // 
            // barButtonItemDeleteVacations
            // 
            this.barButtonItemDeleteVacations.Caption = "Eliminar";
            this.barButtonItemDeleteVacations.Enabled = false;
            this.barButtonItemDeleteVacations.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemDeleteVacations.Glyph")));
            this.barButtonItemDeleteVacations.Id = 256;
            this.barButtonItemDeleteVacations.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemDeleteVacations.Name = "barButtonItemDeleteVacations";
            this.barButtonItemDeleteVacations.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemDeleteVacations.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemDeleteVacations_ItemClick);
            // 
            // barButtonItemModifyVacations
            // 
            this.barButtonItemModifyVacations.Caption = "Modificar";
            this.barButtonItemModifyVacations.Enabled = false;
            this.barButtonItemModifyVacations.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemModifyVacations.Glyph")));
            this.barButtonItemModifyVacations.Id = 257;
            this.barButtonItemModifyVacations.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemModifyVacations.Name = "barButtonItemModifyVacations";
            this.barButtonItemModifyVacations.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemModifyVacations.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemModifyVacations_ItemClick);
            // 
            // barButtonItemConsultVacations
            // 
            this.barButtonItemConsultVacations.Caption = "Consulta";
            this.barButtonItemConsultVacations.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemConsultVacations.Glyph")));
            this.barButtonItemConsultVacations.Id = 259;
            this.barButtonItemConsultVacations.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemConsultVacations.Name = "barButtonItemConsultVacations";
            this.barButtonItemConsultVacations.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemConsultVacations.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemConsultVacations_ItemClick);
            // 
            // barButtonItemConsultExtraTime
            // 
            this.barButtonItemConsultExtraTime.Caption = "Consulta";
            this.barButtonItemConsultExtraTime.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemConsultExtraTime.Glyph")));
            this.barButtonItemConsultExtraTime.Id = 260;
            this.barButtonItemConsultExtraTime.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemConsultExtraTime.Name = "barButtonItemConsultExtraTime";
            this.barButtonItemConsultExtraTime.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemConsultExtraTime.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemConsultExtraTime_ItemClick);
            // 
            // barButtonItemPersonalAssignVacations
            // 
            this.barButtonItemPersonalAssignVacations.Caption = "Personal";
            this.barButtonItemPersonalAssignVacations.Enabled = false;
            this.barButtonItemPersonalAssignVacations.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPersonalAssignVacations.Glyph")));
            this.barButtonItemPersonalAssignVacations.Id = 261;
            this.barButtonItemPersonalAssignVacations.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemPersonalAssignVacations.Name = "barButtonItemPersonalAssignVacations";
            this.barButtonItemPersonalAssignVacations.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemPersonalAssignVacations.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPersonalAssign_ItemClick);
            // 
            // barButtonItemPersonalAssignExtraTime
            // 
            this.barButtonItemPersonalAssignExtraTime.Caption = "Personal";
            this.barButtonItemPersonalAssignExtraTime.Enabled = false;
            this.barButtonItemPersonalAssignExtraTime.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPersonalAssignExtraTime.Glyph")));
            this.barButtonItemPersonalAssignExtraTime.Id = 262;
            this.barButtonItemPersonalAssignExtraTime.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemPersonalAssignExtraTime.Name = "barButtonItemPersonalAssignExtraTime";
            this.barButtonItemPersonalAssignExtraTime.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemPersonalAssignExtraTime.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPersonalAssign_ItemClick);
            // 
            // RibbonPageOperation
            // 
            this.RibbonPageOperation.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupExtraTime,
            this.ribbonPageGroupVacationAndAbsence});
            this.RibbonPageOperation.Name = "RibbonPageOperation";
            this.RibbonPageOperation.Text = "Operacin";
            // 
            // ribbonPageGroupExtraTime
            // 
            this.ribbonPageGroupExtraTime.AllowMinimize = false;
            this.ribbonPageGroupExtraTime.AllowTextClipping = false;
            this.ribbonPageGroupExtraTime.ItemLinks.Add(this.barButtonItemCreateExtraTime);
            this.ribbonPageGroupExtraTime.ItemLinks.Add(this.barButtonItemDeleteExtraTime);
            this.ribbonPageGroupExtraTime.ItemLinks.Add(this.barButtonItemModifyExtraTime);
            this.ribbonPageGroupExtraTime.ItemLinks.Add(this.barButtonItemPersonalAssignExtraTime);
            this.ribbonPageGroupExtraTime.ItemLinks.Add(this.barButtonItemConsultExtraTime);
            this.ribbonPageGroupExtraTime.Name = "ribbonPageGroupExtraTime";
            this.ribbonPageGroupExtraTime.ShowCaptionButton = false;
            this.ribbonPageGroupExtraTime.Text = "Tiempo extra";
            // 
            // ribbonPageGroupVacationAndAbsence
            // 
            this.ribbonPageGroupVacationAndAbsence.AllowMinimize = false;
            this.ribbonPageGroupVacationAndAbsence.AllowTextClipping = false;
            this.ribbonPageGroupVacationAndAbsence.ItemLinks.Add(this.barButtonItemCreateVacations);
            this.ribbonPageGroupVacationAndAbsence.ItemLinks.Add(this.barButtonItemDeleteVacations);
            this.ribbonPageGroupVacationAndAbsence.ItemLinks.Add(this.barButtonItemModifyVacations);
            this.ribbonPageGroupVacationAndAbsence.ItemLinks.Add(this.barButtonItemPersonalAssignVacations);
            this.ribbonPageGroupVacationAndAbsence.ItemLinks.Add(this.barButtonItemConsultVacations);
            this.ribbonPageGroupVacationAndAbsence.Name = "ribbonPageGroupVacationAndAbsence";
            this.ribbonPageGroupVacationAndAbsence.ShowCaptionButton = false;
            this.ribbonPageGroupVacationAndAbsence.Text = "Permisos y vacaciones";
            // 
            // layoutControlExtraForm
            // 
            this.layoutControlExtraForm.AllowCustomizationMenu = false;
            this.layoutControlExtraForm.Controls.Add(this.gridControlOperators);
            this.layoutControlExtraForm.Controls.Add(this.gridControlDetails);
            this.layoutControlExtraForm.Controls.Add(this.gridControlExtraTime);
            this.layoutControlExtraForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlExtraForm.Location = new System.Drawing.Point(0, 0);
            this.layoutControlExtraForm.Name = "layoutControlExtraForm";
            this.layoutControlExtraForm.Root = this.layoutControlGroup1;
            this.layoutControlExtraForm.Size = new System.Drawing.Size(1270, 787);
            this.layoutControlExtraForm.TabIndex = 7;
            this.layoutControlExtraForm.Text = "layoutControl1";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupExtraTime});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(1270, 787);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroupExtraTime
            // 
            this.layoutControlGroupExtraTime.CustomizationFormText = "layoutControlGroupExtraTime";
            this.layoutControlGroupExtraTime.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlGroupDetails,
            this.layoutControlGroupOperators});
            this.layoutControlGroupExtraTime.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupExtraTime.Name = "layoutControlGroupExtraTime";
            this.layoutControlGroupExtraTime.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupExtraTime.Size = new System.Drawing.Size(1264, 781);
            this.layoutControlGroupExtraTime.Text = "layoutControlGroupExtraTime";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridControlExtraTime;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1254, 426);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroupDetails
            // 
            this.layoutControlGroupDetails.CustomizationFormText = "layoutControlGroupDetails";
            this.layoutControlGroupDetails.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroupDetails.Location = new System.Drawing.Point(0, 426);
            this.layoutControlGroupDetails.Name = "layoutControlGroupDetails";
            this.layoutControlGroupDetails.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupDetails.Size = new System.Drawing.Size(626, 325);
            this.layoutControlGroupDetails.Text = "layoutControlGroupDetails";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridControlDetails;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(616, 295);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlGroupOperators
            // 
            this.layoutControlGroupOperators.CustomizationFormText = "layoutControlGroupOperators";
            this.layoutControlGroupOperators.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.layoutControlGroupOperators.Location = new System.Drawing.Point(626, 426);
            this.layoutControlGroupOperators.Name = "layoutControlGroupOperators";
            this.layoutControlGroupOperators.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupOperators.Size = new System.Drawing.Size(628, 325);
            this.layoutControlGroupOperators.Text = "layoutControlGroupOperators";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.gridControlOperators;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(618, 295);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // ExtraTimeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1270, 787);
            this.Controls.Add(this.RibbonControl);
            this.Controls.Add(this.layoutControlExtraForm);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ExtraTimeForm";
            this.Text = "Tiempos extras";
            this.Activated += new System.EventHandler(this.ExtraTimeForm_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ExtraTimeForm_FormClosing);
            this.Load += new System.EventHandler(this.ExtraTimeForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlOperators)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewOperators)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExtraTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExtraTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RibbonControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlExtraForm)).EndInit();
            this.layoutControlExtraForm.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupExtraTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupOperators)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private GridControlEx gridControlOperators;
        private GridViewEx gridViewOperators;
        private GridControlEx gridControlDetails;
        private GridViewEx gridViewDetails;
        private GridControlEx gridControlExtraTime;
        private GridViewEx gridViewExtraTime;
        internal DevExpress.XtraBars.Ribbon.RibbonControl RibbonControl;
        private System.Windows.Forms.ImageList imageList;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemCreateExtraTime;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemDeleteExtraTime;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemModifyExtraTime;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemCreateVacations;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemDeleteVacations;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemModifyVacations;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPage RibbonPageOperation;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupExtraTime;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupVacationAndAbsence;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemConsultVacations;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemConsultExtraTime;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemPersonalAssignVacations;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemPersonalAssignExtraTime;
        private DevExpress.XtraLayout.LayoutControl layoutControlExtraForm;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupExtraTime;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupDetails;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupOperators;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
 
    }
}
