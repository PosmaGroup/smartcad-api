using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Smartmatic.SmartCad.Service;
using System.Collections;
using System.Reflection;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadCore.Core;


namespace SmartCadGuiCommon
{
    public partial class OperatorCategoryChangeForm : DevExpress.XtraEditors.XtraForm
    {

        private OperatorCategoryHistoryClientData selected;
        private OperatorCategoryHistoryClientData previous;
        private DateTime since;
        private string FullName;
        //bool buttonOkPressed;

        public OperatorCategoryChangeForm(OperatorClientData ope)
        {
            InitializeComponent();
            LoadLanguage();
            FullName = ope.FirstName + " " + ope.LastName;
            selected = new OperatorCategoryHistoryClientData();
            this.since = ServerServiceClient.GetInstance().GetTime();
            this.labelDate.Text = since.ToShortDateString();
            string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorCategories);
            IList list = ServerServiceClient.GetInstance().SearchClientObjects(hql);
            SetNextCategory(ope, list);
            //buttonOkPressed = false;
            this.Text = ResourceLoader.GetString2("ModifyCategory");
            selected.StartDate = since;
            selected.OperatorCode = ope.Code;
        }

        private void LoadLanguage()
        {
            this.layoutControlItemCategory.Text = ResourceLoader.GetString2("NewCategory") + ":";
            this.layoutControlItemSince.Text = ResourceLoader.GetString2("Since") + ":";
            this.layoutControlGroupDescription.Text = ResourceLoader.GetString2("Description");
            this.layoutControlGroupInfo.Text = ResourceLoader.GetString2("CategoryDetails");
            buttonOK.Text = ResourceLoader.GetString2("Accept");
            buttonCancel.Text = ResourceLoader.GetString2("Cancel");

        }

        private void SetNextCategory(OperatorClientData ope, IList list)
        {
            int level = ope.CategoryList.Count + 1;
            OperatorCategoryClientData cat = null;
           
            switch (level)
            {
                case 2:
                    foreach (OperatorCategoryHistoryClientData category in ope.CategoryList)
                    {
                        if (category.CategoryCode == 1)
                            previous = category;
                    }
                    cat = list[1] as OperatorCategoryClientData;
                    SetThingsNextCategory(cat);
                    break;
                case 3:
                    foreach (OperatorCategoryHistoryClientData category in ope.CategoryList)
                    {
                        if (category.CategoryCode == 2)
                            previous = category;
                    }
                    cat = list[2] as OperatorCategoryClientData;
                    SetThingsNextCategory(cat);
                    break;
                case 4:
                    MessageForm.Show(string.Format(ResourceLoader.GetString2("OperatorIsMaxCategory"), ope.FirstName, ope.LastName), MessageFormType.Information);
                    this.Close();
                    break;
            }

           
        }
            
    

        private void SetThingsNextCategory(OperatorCategoryClientData cat) 
        {
            this.labelNextCategory.Text = cat.FriendlyName;
            selected.CategoryCode = cat.Code;
            this.textBoxDesc.Text = cat.Description;
            this.buttonOK.Enabled = true;
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            try
            {
                string message = ResourceLoader.GetString2("MessageConfirmChangeCategory", FullName);
                DialogResult res = MessageForm.Show( message, MessageFormType.WarningQuestion);
                if (res == DialogResult.OK || res == DialogResult.Yes)
                {
                    BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[1] { GetType().GetMethod("buttonOk_Click1", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[0] });
                    processForm.CanThrowError = true;
                    processForm.ShowDialog();
                }
                else
                {
                    DialogResult = DialogResult.None;
                    //buttonOkPressed = false;
                }
            }
            catch (Exception ex)
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, ex);
                //buttonOkPressed = false;
            }
        }

        private void buttonOk_Click1() 
        {
            if (previous != null)
            {
                previous.EndDate = since;
                ServerServiceClient.GetInstance().SaveOrUpdateClientData(this.previous);
            }
            ServerServiceClient.GetInstance().SaveOrUpdateClientData(this.selected);
        }

    }
}