
namespace SmartCadGuiCommon
{
    partial class SupervisionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SupervisionForm));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem9 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem9 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip10 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem10 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem10 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip11 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem11 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem11 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip12 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem12 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem12 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip13 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem13 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem13 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip14 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem14 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem14 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip15 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem15 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem15 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip16 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem16 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem16 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip17 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem17 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem17 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip18 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem18 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem18 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip19 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem19 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem19 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip20 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem20 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem20 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip21 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem21 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem21 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip22 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem22 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem22 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip23 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem23 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem23 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip24 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem24 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem24 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip25 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem25 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem25 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip26 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem26 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem26 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip27 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem27 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem27 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip28 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem28 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem28 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip29 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem29 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem29 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip30 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem30 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem30 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip31 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem31 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem31 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip32 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem32 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem32 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip33 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem33 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem33 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip34 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem34 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem34 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip35 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem35 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem35 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip36 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem36 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem36 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip37 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem37 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem37 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip38 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem38 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem38 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip39 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem39 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem39 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip40 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem40 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem40 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip41 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem41 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem41 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip42 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem42 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem42 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip43 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem43 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem43 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip44 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem44 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem44 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip45 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem45 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem45 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip46 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem46 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem46 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip47 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem47 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem47 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip48 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem48 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem48 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip49 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem49 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem49 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip50 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem50 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem50 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip51 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem51 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem51 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip52 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem52 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem52 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip53 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem53 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem53 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip54 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem54 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem54 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip55 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem55 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem55 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip56 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem56 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem56 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip57 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem57 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem57 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip58 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem58 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem58 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip59 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem59 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem59 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip60 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem60 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem60 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip61 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem61 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem61 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip62 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem62 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem62 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip63 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem63 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem63 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip64 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem64 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem64 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip65 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem65 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem65 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip66 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem66 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem66 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip67 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem67 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem67 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip68 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem68 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem68 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip69 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem69 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem69 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip70 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem70 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem70 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip71 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem71 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip72 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem72 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip73 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem73 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem71 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip74 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem74 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem72 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip75 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem75 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem73 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip76 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem76 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip77 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem77 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem74 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip78 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem78 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem75 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip79 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem79 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem76 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip80 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem80 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem77 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip81 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem81 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem78 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip82 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem82 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem79 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip83 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem83 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem80 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip84 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem84 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem81 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip85 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem85 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem82 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip86 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem86 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip87 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem87 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip88 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem88 = new DevExpress.Utils.ToolTipTitleItem();
            this.contextMenuStripOperators = new System.Windows.Forms.ContextMenuStrip();
            this.verOcultarCronogramaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asignarCursoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.agregarObservacionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ubicacinEnLaSalaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.conversacinToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.monitorearEstacinToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.monitorearPantallaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.monitorearLlamadaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator16 = new System.Windows.Forms.ToolStripSeparator();
            this.camposToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.nombreToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.apellidosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.estacinToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.categoraToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.estatusToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.RibbonControl = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.imageList = new System.Windows.Forms.ImageList();
            this.CommandBarItem46 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.CommandBarItem48 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.CommandBarItem49 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.CommandBarItem50 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.CommandBarItem51 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.CommandBarItem52 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.CommandBarItem53 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.CommandBarItem54 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.CommandBarItem55 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.CommandBarItem56 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.CommandBarItem57 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.CommandBarItem58 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.CommandBarItem59 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.CommandBarItem60 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.CommandBarItem61 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.CommandBarItem62 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.CommandBarItem63 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.CommandBarItem64 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.CommandBarItem65 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.CommandBarItem66 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.CommandBarItem67 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.CommandBarItem68 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.CommandBarItem69 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.CommandBarItem70 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.CommandBarItem71 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.CommandColorBarItem3 = new DevExpress.XtraReports.UserDesigner.CommandColorBarItem();
            this.CommandColorBarItem4 = new DevExpress.XtraReports.UserDesigner.CommandColorBarItem();
            this.CommandBarItem72 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.CommandBarItem73 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.CommandBarItem74 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.CommandBarItem75 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.CommandBarItem76 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.CommandBarItem77 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.CommandBarItem78 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.CommandBarItem79 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.CommandBarItem80 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.CommandBarItem81 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.CommandBarItem82 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.commandBarItemIndicators = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.CommandBarItem86 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.CommandBarItem90 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.BarDockPanelsListItem2 = new DevExpress.XtraReports.UserDesigner.BarDockPanelsListItem();
            this.XrDesignBarButtonGroup4 = new DevExpress.XtraReports.UserDesigner.XRDesignBarButtonGroup();
            this.XrDesignBarButtonGroup5 = new DevExpress.XtraReports.UserDesigner.XRDesignBarButtonGroup();
            this.barButtonItemCopy = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.barButtonItemCut = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItemPersonalfile = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItemPersonalFileObs = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItemModifyCategory = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem6 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.barItemCreateCourses = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.barItemDeleteCourses = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem9 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem10 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem11 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem12 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem13 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem14 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.barButtonItemPaste = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem19 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem20 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem21 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem22 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem23 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem24 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.barButtonOperatorTrainingAssign = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.barButtonTrainingCoursesSchedule = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem28 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem29 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem30 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem31 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem32 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem33 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem34 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem35 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem36 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem37 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem38 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem39 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem40 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem41 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem42 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.barItemNotificationDistribution = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewStaticItem1 = new DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem();
            this.PrintPreviewStaticItem2 = new DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem();
            this.PrintPreviewStaticItem3 = new DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem();
            this.CommandBar = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemOperatorActivities = new DevExpress.XtraBars.BarButtonItem();
            this.BarEditItem4 = new DevExpress.XtraBars.BarEditItem();
            this.BarEditItem6 = new DevExpress.XtraBars.BarEditItem();
            this.BarButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItemSendMonitor = new DevExpress.XtraBars.BarButtonItem();
            this.BarListItem2 = new DevExpress.XtraBars.BarListItem();
            this.BarButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonGroup1 = new DevExpress.XtraBars.BarButtonGroup();
            this.BarCheckItem2 = new DevExpress.XtraBars.BarCheckItem();
            this.BarCheckItem3 = new DevExpress.XtraBars.BarCheckItem();
            this.BarCheckItem4 = new DevExpress.XtraBars.BarCheckItem();
            this.BarButtonItemSelectRoom = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemForecast = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemAddForecast = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemDeleteForecast = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemUpdateForecast = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemDuplicate = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemReplace = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemFinalize = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItemHistoricGraphic = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItemHistoryDetails = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem17 = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem18 = new DevExpress.XtraBars.BarButtonItem();
            this.BarEditItem7 = new DevExpress.XtraBars.BarEditItem();
            this.barButtonItemEvaluateOperator = new DevExpress.XtraBars.BarButtonItem();
            this.BarSubItem16 = new DevExpress.XtraBars.BarSubItem();
            this.BarSubItem17 = new DevExpress.XtraBars.BarSubItem();
            this.BarStaticItem2 = new DevExpress.XtraBars.BarStaticItem();
            this.BarStaticItem3 = new DevExpress.XtraBars.BarStaticItem();
            this.BarStaticItem4 = new DevExpress.XtraBars.BarStaticItem();
            this.BarButtonItem25 = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem26 = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem27 = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem28 = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem29 = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem30 = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem31 = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem32 = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem33 = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem34 = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem35 = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem36 = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem37 = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem38 = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem39 = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem40 = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem41 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemExit = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonGroup2 = new DevExpress.XtraBars.BarButtonGroup();
            this.barButtonItemOperatorAssign = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemConfigureIndicators = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemOperAffectIndicators = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemCourses = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemModifyCourses = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemReport = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemEndReport = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemRedo = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemUndo = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemPreferences = new DevExpress.XtraBars.BarButtonItem();
            this.barMdiChildrenListItemSelect = new DevExpress.XtraBars.BarMdiChildrenListItem();
            this.barButtonItemUpdate = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSend = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSave = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemHelp = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemRefreshGraphic = new DevExpress.XtraBars.BarButtonItem();
            this.barCheckItemFirstLevel = new DevExpress.XtraBars.BarCheckItem();
            this.barButtonItemDispatch = new DevExpress.XtraBars.BarButtonItem();
            this.popupMenuDispatch = new DevExpress.XtraBars.PopupMenu();
            this.barButtonItemCloseReportsReader = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemExtraTime = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemCreateExtraTime = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemDeleteExtraTime = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemModifyExtraTime = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemVacationsAndAbsences = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemCreateVacations = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemDeleteVacations = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemModifyVacations = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItemSelectRoom = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemComboBoxSelectRoom = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.barCheckItemViewSelected = new DevExpress.XtraBars.BarCheckItem();
            this.barCheckItemViewAll = new DevExpress.XtraBars.BarCheckItem();
            this.barButtonItemConsultVacations = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemConsultExtraTime = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonGroupGeneralOptionsMonitor = new DevExpress.XtraBars.BarButtonGroup();
            this.barSubItemChartControl = new DevExpress.XtraBars.BarSubItem();
            this.barCheckItemUpTreshold = new DevExpress.XtraBars.BarCheckItem();
            this.barCheckItemDownTreshold = new DevExpress.XtraBars.BarCheckItem();
            this.barCheckItemTrend = new DevExpress.XtraBars.BarCheckItem();
            this.barButtonItemOperatorPerformance = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItemIndicatorSelection = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemRadioGroup3 = new DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup();
            this.barButtonItemPersonalAssignExtraTime = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemPersonalAssignVacations = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItemLogo = new DevExpress.XtraBars.BarStaticItem();
            this.barButtonItemChat = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemRemoteControl = new DevExpress.XtraBars.BarButtonItem();
            this.barCheckItemTakeControl = new DevExpress.XtraBars.BarCheckItem();
            this.barCheckItemScaledView = new DevExpress.XtraBars.BarCheckItem();
            this.barButtonItemReConnect = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemAssign = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemRemove = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemOpen = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemLogo = new DevExpress.XtraBars.BarButtonItem();
            this.RibbonPageMonitoring = new DevExpress.XtraReports.UserDesigner.XRDesignRibbonPage();
            this.ribbonPageGroupPerformance = new DevExpress.XtraReports.UserDesigner.XRDesignRibbonPageGroup();
            this.ribbonPageGroupOperator = new DevExpress.XtraReports.UserDesigner.XRDesignRibbonPageGroup();
            this.ribbonPageGroupGeneralOptMonitor = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.RibbonPageOperation = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPage();
            this.ribbonPageGroupPersonalFile = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup();
            this.ribbonPageGroupExtraTime = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupAssignment = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupVacationAndAbsence = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupTraining = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup();
            this.ribbonPageGroupDistribution = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup();
            this.ribbonPageGroupFinalReport = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup();
            this.ribbonPageGroupOptions = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.RibbonPageTools = new DevExpress.XtraReports.UserDesigner.XRHtmlRibbonPage();
            this.ribbonPageGroupPreferences = new DevExpress.XtraReports.UserDesigner.XRDesignRibbonPageGroup();
            this.ribbonPageGroupAccessProfile = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.repositoryItemPictureEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.repositoryItemTimeEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemComboBoxWorkshift = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemDateEdit = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemPopupContainerEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.repositoryItemRadioGroup1 = new DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup();
            this.repositoryItemRadioGroup2 = new DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.ribbonStatusBar1 = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.labelUser = new DevExpress.XtraBars.BarStaticItem();
            this.labelConnected = new DevExpress.XtraBars.BarStaticItem();
            this.labelDate = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem5 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem6 = new DevExpress.XtraBars.BarStaticItem();
            this.contextMenuStripDesigner = new System.Windows.Forms.ContextMenuStrip();
            this.xtraTabbedMdiManager = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.barButtonRefreshChart = new DevExpress.XtraBars.BarButtonItem();
            this.labelLogo = new System.Windows.Forms.Label();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroupCallInfo = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.barButtonItemStartRegIncident = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemCreateNewIncident = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemCancelIncident = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageGroupApplications = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemHistory = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageGroupGeneralOptions = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageGroupTools = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.barButtonItemOpenMap = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem7 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage2 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.barButtonItem8 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem9 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem10 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.barButtonItem11 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem12 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.barButtonItem13 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem14 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem15 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageGroup5 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.barButtonItem16 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem19 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem20 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage3 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup6 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.barButtonItem21 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem22 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem23 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageGroup7 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.barButtonItem24 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem42 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageGroup8 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.barButtonItem43 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem44 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem45 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageGroup9 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.barButtonItem46 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem47 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem48 = new DevExpress.XtraBars.BarButtonItem();
            this.contextMenuStripOperators.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RibbonControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenuDispatch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBoxSelectRoom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBoxWorkshift)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager)).BeginInit();
            this.SuspendLayout();
            // 
            // contextMenuStripOperators
            // 
            this.contextMenuStripOperators.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.verOcultarCronogramaToolStripMenuItem,
            this.asignarCursoToolStripMenuItem1,
            this.agregarObservacionesToolStripMenuItem,
            this.ubicacinEnLaSalaToolStripMenuItem,
            this.conversacinToolStripMenuItem,
            this.toolStripSeparator14,
            this.monitorearEstacinToolStripMenuItem,
            this.monitorearPantallaToolStripMenuItem1,
            this.monitorearLlamadaToolStripMenuItem1,
            this.toolStripSeparator16,
            this.camposToolStripMenuItem1});
            this.contextMenuStripOperators.Name = "contextMenuStripOperators";
            this.contextMenuStripOperators.Size = new System.Drawing.Size(210, 214);
            // 
            // verOcultarCronogramaToolStripMenuItem
            // 
            this.verOcultarCronogramaToolStripMenuItem.Name = "verOcultarCronogramaToolStripMenuItem";
            this.verOcultarCronogramaToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.verOcultarCronogramaToolStripMenuItem.Text = "Ver / Ocultar cronograma";
            // 
            // asignarCursoToolStripMenuItem1
            // 
            this.asignarCursoToolStripMenuItem1.Name = "asignarCursoToolStripMenuItem1";
            this.asignarCursoToolStripMenuItem1.Size = new System.Drawing.Size(209, 22);
            this.asignarCursoToolStripMenuItem1.Text = "Asignar curso";
            // 
            // agregarObservacionesToolStripMenuItem
            // 
            this.agregarObservacionesToolStripMenuItem.Name = "agregarObservacionesToolStripMenuItem";
            this.agregarObservacionesToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.agregarObservacionesToolStripMenuItem.Text = "Agregar observaciones";
            // 
            // ubicacinEnLaSalaToolStripMenuItem
            // 
            this.ubicacinEnLaSalaToolStripMenuItem.Name = "ubicacinEnLaSalaToolStripMenuItem";
            this.ubicacinEnLaSalaToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.ubicacinEnLaSalaToolStripMenuItem.Text = "Ubicacin en la sala";
            // 
            // conversacinToolStripMenuItem
            // 
            this.conversacinToolStripMenuItem.Name = "conversacinToolStripMenuItem";
            this.conversacinToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.conversacinToolStripMenuItem.Text = "Conversacin";
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(206, 6);
            // 
            // monitorearEstacinToolStripMenuItem
            // 
            this.monitorearEstacinToolStripMenuItem.Name = "monitorearEstacinToolStripMenuItem";
            this.monitorearEstacinToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.monitorearEstacinToolStripMenuItem.Text = "Monitorear estacin";
            // 
            // monitorearPantallaToolStripMenuItem1
            // 
            this.monitorearPantallaToolStripMenuItem1.Name = "monitorearPantallaToolStripMenuItem1";
            this.monitorearPantallaToolStripMenuItem1.Size = new System.Drawing.Size(209, 22);
            this.monitorearPantallaToolStripMenuItem1.Text = "Monitorear pantalla";
            // 
            // monitorearLlamadaToolStripMenuItem1
            // 
            this.monitorearLlamadaToolStripMenuItem1.Name = "monitorearLlamadaToolStripMenuItem1";
            this.monitorearLlamadaToolStripMenuItem1.Size = new System.Drawing.Size(209, 22);
            this.monitorearLlamadaToolStripMenuItem1.Text = "Monitorear llamada";
            // 
            // toolStripSeparator16
            // 
            this.toolStripSeparator16.Name = "toolStripSeparator16";
            this.toolStripSeparator16.Size = new System.Drawing.Size(206, 6);
            // 
            // camposToolStripMenuItem1
            // 
            this.camposToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nombreToolStripMenuItem1,
            this.apellidosToolStripMenuItem,
            this.estacinToolStripMenuItem2,
            this.categoraToolStripMenuItem1,
            this.estatusToolStripMenuItem2});
            this.camposToolStripMenuItem1.Name = "camposToolStripMenuItem1";
            this.camposToolStripMenuItem1.Size = new System.Drawing.Size(209, 22);
            this.camposToolStripMenuItem1.Text = "Campos";
            // 
            // nombreToolStripMenuItem1
            // 
            this.nombreToolStripMenuItem1.Checked = true;
            this.nombreToolStripMenuItem1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.nombreToolStripMenuItem1.Name = "nombreToolStripMenuItem1";
            this.nombreToolStripMenuItem1.Size = new System.Drawing.Size(123, 22);
            this.nombreToolStripMenuItem1.Text = "Nombre";
            // 
            // apellidosToolStripMenuItem
            // 
            this.apellidosToolStripMenuItem.Checked = true;
            this.apellidosToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.apellidosToolStripMenuItem.Name = "apellidosToolStripMenuItem";
            this.apellidosToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
            this.apellidosToolStripMenuItem.Text = "Apellidos";
            // 
            // estacinToolStripMenuItem2
            // 
            this.estacinToolStripMenuItem2.Checked = true;
            this.estacinToolStripMenuItem2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.estacinToolStripMenuItem2.Name = "estacinToolStripMenuItem2";
            this.estacinToolStripMenuItem2.Size = new System.Drawing.Size(123, 22);
            this.estacinToolStripMenuItem2.Text = "Estacin";
            // 
            // categoraToolStripMenuItem1
            // 
            this.categoraToolStripMenuItem1.Checked = true;
            this.categoraToolStripMenuItem1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.categoraToolStripMenuItem1.Name = "categoraToolStripMenuItem1";
            this.categoraToolStripMenuItem1.Size = new System.Drawing.Size(123, 22);
            this.categoraToolStripMenuItem1.Text = "Categora";
            // 
            // estatusToolStripMenuItem2
            // 
            this.estatusToolStripMenuItem2.Checked = true;
            this.estatusToolStripMenuItem2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.estatusToolStripMenuItem2.Name = "estatusToolStripMenuItem2";
            this.estatusToolStripMenuItem2.Size = new System.Drawing.Size(123, 22);
            this.estatusToolStripMenuItem2.Text = "Estatus";
            // 
            // RibbonControl
            // 
            this.RibbonControl.AutoSizeItems = true;
            this.RibbonControl.ExpandCollapseItem.Id = 0;
            this.RibbonControl.ExpandCollapseItem.Name = "";
            this.RibbonControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RibbonControl.Images = this.imageList;
            this.RibbonControl.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.RibbonControl.ExpandCollapseItem,
            this.CommandBarItem46,
            this.CommandBarItem48,
            this.CommandBarItem49,
            this.CommandBarItem50,
            this.CommandBarItem51,
            this.CommandBarItem52,
            this.CommandBarItem53,
            this.CommandBarItem54,
            this.CommandBarItem55,
            this.CommandBarItem56,
            this.CommandBarItem57,
            this.CommandBarItem58,
            this.CommandBarItem59,
            this.CommandBarItem60,
            this.CommandBarItem61,
            this.CommandBarItem62,
            this.CommandBarItem63,
            this.CommandBarItem64,
            this.CommandBarItem65,
            this.CommandBarItem66,
            this.CommandBarItem67,
            this.CommandBarItem68,
            this.CommandBarItem69,
            this.CommandBarItem70,
            this.CommandBarItem71,
            this.CommandColorBarItem3,
            this.CommandColorBarItem4,
            this.CommandBarItem72,
            this.CommandBarItem73,
            this.CommandBarItem74,
            this.CommandBarItem75,
            this.CommandBarItem76,
            this.CommandBarItem77,
            this.CommandBarItem78,
            this.CommandBarItem79,
            this.CommandBarItem80,
            this.CommandBarItem81,
            this.CommandBarItem82,
            this.commandBarItemIndicators,
            this.CommandBarItem86,
            this.CommandBarItem90,
            this.BarDockPanelsListItem2,
            this.XrDesignBarButtonGroup4,
            this.XrDesignBarButtonGroup5,
            this.barButtonItemCopy,
            this.barButtonItemCut,
            this.printPreviewBarItemPersonalfile,
            this.printPreviewBarItemPersonalFileObs,
            this.printPreviewBarItemModifyCategory,
            this.PrintPreviewBarItem6,
            this.barItemCreateCourses,
            this.barItemDeleteCourses,
            this.PrintPreviewBarItem9,
            this.PrintPreviewBarItem10,
            this.PrintPreviewBarItem11,
            this.PrintPreviewBarItem12,
            this.PrintPreviewBarItem13,
            this.PrintPreviewBarItem14,
            this.barButtonItemPaste,
            this.PrintPreviewBarItem19,
            this.PrintPreviewBarItem20,
            this.PrintPreviewBarItem21,
            this.PrintPreviewBarItem22,
            this.PrintPreviewBarItem23,
            this.PrintPreviewBarItem24,
            this.barButtonOperatorTrainingAssign,
            this.barButtonTrainingCoursesSchedule,
            this.PrintPreviewBarItem28,
            this.PrintPreviewBarItem29,
            this.PrintPreviewBarItem30,
            this.PrintPreviewBarItem31,
            this.PrintPreviewBarItem32,
            this.PrintPreviewBarItem33,
            this.PrintPreviewBarItem34,
            this.PrintPreviewBarItem35,
            this.PrintPreviewBarItem36,
            this.PrintPreviewBarItem37,
            this.PrintPreviewBarItem38,
            this.PrintPreviewBarItem39,
            this.PrintPreviewBarItem40,
            this.PrintPreviewBarItem41,
            this.PrintPreviewBarItem42,
            this.barItemNotificationDistribution,
            this.PrintPreviewStaticItem1,
            this.PrintPreviewStaticItem2,
            this.PrintPreviewStaticItem3,
            this.CommandBar,
            this.barButtonItemOperatorActivities,
            this.BarEditItem4,
            this.BarEditItem6,
            this.BarButtonItem3,
            this.BarButtonItemSendMonitor,
            this.BarListItem2,
            this.BarButtonItem5,
            this.BarButtonGroup1,
            this.BarCheckItem2,
            this.BarCheckItem3,
            this.BarCheckItem4,
            this.BarButtonItemSelectRoom,
            this.barButtonItemForecast,
            this.barButtonItemAddForecast,
            this.barButtonItemDeleteForecast,
            this.barButtonItemUpdateForecast,
            this.barButtonItemDuplicate,
            this.barButtonItemReplace,
            this.barButtonItemFinalize,
            this.BarButtonItemHistoricGraphic,
            this.BarButtonItemHistoryDetails,
            this.BarButtonItem17,
            this.BarButtonItem18,
            this.BarEditItem7,
            this.barButtonItemEvaluateOperator,
            this.BarSubItem16,
            this.BarSubItem17,
            this.BarStaticItem2,
            this.BarStaticItem3,
            this.BarStaticItem4,
            this.BarButtonItem25,
            this.BarButtonItem26,
            this.BarButtonItem27,
            this.BarButtonItem28,
            this.BarButtonItem29,
            this.BarButtonItem30,
            this.BarButtonItem31,
            this.BarButtonItem32,
            this.BarButtonItem33,
            this.BarButtonItem34,
            this.BarButtonItem35,
            this.BarButtonItem36,
            this.BarButtonItem37,
            this.BarButtonItem38,
            this.BarButtonItem39,
            this.BarButtonItem40,
            this.BarButtonItem41,
            this.barButtonItemExit,
            this.barButtonGroup2,
            this.barButtonItemOperatorAssign,
            this.barButtonItemConfigureIndicators,
            this.barButtonItemOperAffectIndicators,
            this.barButtonItemCourses,
            this.barButtonItemModifyCourses,
            this.barButtonItemReport,
            this.barButtonItemEndReport,
            this.barButtonItemRedo,
            this.barButtonItemUndo,
            this.barButtonItemPreferences,
            this.barMdiChildrenListItemSelect,
            this.barButtonItemUpdate,
            this.barButtonItemSend,
            this.barButtonItemSave,
            this.barButtonItemPrint,
            this.barButtonItemHelp,
            this.barButtonItemRefreshGraphic,
            this.barCheckItemFirstLevel,
            this.barButtonItemDispatch,
            this.barButtonItemCloseReportsReader,
            this.barButtonItemExtraTime,
            this.barButtonItemCreateExtraTime,
            this.barButtonItemDeleteExtraTime,
            this.barButtonItemModifyExtraTime,
            this.barButtonItemVacationsAndAbsences,
            this.barButtonItemCreateVacations,
            this.barButtonItemDeleteVacations,
            this.barButtonItemModifyVacations,
            this.barEditItemSelectRoom,
            this.barCheckItemViewSelected,
            this.barCheckItemViewAll,
            this.barButtonItemConsultVacations,
            this.barButtonItemConsultExtraTime,
            this.barButtonGroupGeneralOptionsMonitor,
            this.barSubItemChartControl,
            this.barCheckItemUpTreshold,
            this.barCheckItemDownTreshold,
            this.barCheckItemTrend,
            this.barButtonItemOperatorPerformance,
            this.barEditItemIndicatorSelection,
            this.barButtonItemPersonalAssignExtraTime,
            this.barButtonItemPersonalAssignVacations,
            this.barStaticItemLogo,
            this.barButtonItemChat,
            this.barButtonItemRemoteControl,
            this.barCheckItemTakeControl,
            this.barCheckItemScaledView,
            this.barButtonItemReConnect,
            this.barButtonItemAssign,
            this.barButtonItemRemove,
            this.barButtonItemOpen,
            this.barButtonItemLogo});
            this.RibbonControl.ItemsVertAlign = DevExpress.Utils.VertAlignment.Top;
            this.RibbonControl.LargeImages = this.imageList;
            this.RibbonControl.Location = new System.Drawing.Point(0, 0);
            this.RibbonControl.MaxItemId = 313;
            this.RibbonControl.MdiMergeStyle = DevExpress.XtraBars.Ribbon.RibbonMdiMergeStyle.Always;
            this.RibbonControl.Name = "RibbonControl";
            this.RibbonControl.PageHeaderItemLinks.Add(this.barButtonItemHelp);
            this.RibbonControl.PageHeaderItemLinks.Add(this.barButtonItemLogo);
            this.RibbonControl.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.RibbonPageMonitoring,
            this.RibbonPageOperation,
            this.RibbonPageTools});
            this.RibbonControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPictureEdit1,
            this.repositoryItemTimeEdit1,
            this.repositoryItemCheckEdit1,
            this.repositoryItemCheckEdit2,
            this.repositoryItemComboBoxWorkshift,
            this.repositoryItemDateEdit,
            this.repositoryItemComboBox1,
            this.repositoryItemPopupContainerEdit1,
            this.repositoryItemComboBoxSelectRoom,
            this.repositoryItemRadioGroup1,
            this.repositoryItemRadioGroup2,
            this.repositoryItemCheckEdit3,
            this.repositoryItemRadioGroup3});
            this.RibbonControl.ShowToolbarCustomizeItem = false;
            this.RibbonControl.Size = new System.Drawing.Size(1280, 119);
            this.RibbonControl.StatusBar = this.ribbonStatusBar1;
            this.RibbonControl.Toolbar.ShowCustomizeItem = false;
            this.RibbonControl.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            this.RibbonControl.TransparentEditors = true;
            this.RibbonControl.Click += new System.EventHandler(this.RibbonControl_Click);
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "logout.gif");
            // 
            // CommandBarItem46
            // 
            this.CommandBarItem46.Caption = "Align to Grid";
            this.CommandBarItem46.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.AlignToGrid;
            this.CommandBarItem46.Enabled = false;
            this.CommandBarItem46.Id = 0;
            this.CommandBarItem46.Name = "CommandBarItem46";
            superToolTip1.FixedTooltipWidth = true;
            toolTipTitleItem1.Text = "Align to Grid";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Align the positions of the selected controls to the grid.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            superToolTip1.MaxWidth = 210;
            this.CommandBarItem46.SuperTip = superToolTip1;
            // 
            // CommandBarItem48
            // 
            this.CommandBarItem48.Caption = "Align Centers";
            this.CommandBarItem48.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.AlignVerticalCenters;
            this.CommandBarItem48.Enabled = false;
            this.CommandBarItem48.Id = 2;
            this.CommandBarItem48.Name = "CommandBarItem48";
            superToolTip2.FixedTooltipWidth = true;
            toolTipTitleItem2.Text = "Align Centers";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Align the centers of the selected controls vertically.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            superToolTip2.MaxWidth = 210;
            this.CommandBarItem48.SuperTip = superToolTip2;
            // 
            // CommandBarItem49
            // 
            this.CommandBarItem49.Caption = "Align Rights";
            this.CommandBarItem49.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.AlignRight;
            this.CommandBarItem49.Enabled = false;
            this.CommandBarItem49.Id = 3;
            this.CommandBarItem49.Name = "CommandBarItem49";
            superToolTip3.FixedTooltipWidth = true;
            toolTipTitleItem3.Text = "Align Rights";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Right align the selected controls.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            superToolTip3.MaxWidth = 210;
            this.CommandBarItem49.SuperTip = superToolTip3;
            // 
            // CommandBarItem50
            // 
            this.CommandBarItem50.Caption = "Align Tops";
            this.CommandBarItem50.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.AlignTop;
            this.CommandBarItem50.Enabled = false;
            this.CommandBarItem50.Id = 4;
            this.CommandBarItem50.Name = "CommandBarItem50";
            superToolTip4.FixedTooltipWidth = true;
            toolTipTitleItem4.Text = "Align Tops";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Align the tops of the selected controls.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            superToolTip4.MaxWidth = 210;
            this.CommandBarItem50.SuperTip = superToolTip4;
            // 
            // CommandBarItem51
            // 
            this.CommandBarItem51.Caption = "Align Middles";
            this.CommandBarItem51.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.AlignHorizontalCenters;
            this.CommandBarItem51.Enabled = false;
            this.CommandBarItem51.Id = 5;
            this.CommandBarItem51.Name = "CommandBarItem51";
            superToolTip5.FixedTooltipWidth = true;
            toolTipTitleItem5.Text = "Align Middles";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Align the centers of the selected controls horizontally.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            superToolTip5.MaxWidth = 210;
            this.CommandBarItem51.SuperTip = superToolTip5;
            // 
            // CommandBarItem52
            // 
            this.CommandBarItem52.Caption = "Align Bottoms";
            this.CommandBarItem52.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.AlignBottom;
            this.CommandBarItem52.Enabled = false;
            this.CommandBarItem52.Id = 6;
            this.CommandBarItem52.Name = "CommandBarItem52";
            superToolTip6.FixedTooltipWidth = true;
            toolTipTitleItem6.Text = "Align Bottoms";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Align the bottoms of the selected controls.";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            superToolTip6.MaxWidth = 210;
            this.CommandBarItem52.SuperTip = superToolTip6;
            // 
            // CommandBarItem53
            // 
            this.CommandBarItem53.Caption = "Make Same Width";
            this.CommandBarItem53.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.SizeToControlWidth;
            this.CommandBarItem53.Enabled = false;
            this.CommandBarItem53.Id = 7;
            this.CommandBarItem53.Name = "CommandBarItem53";
            superToolTip7.FixedTooltipWidth = true;
            toolTipTitleItem7.Text = "Make Same Width";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Make the selected controls have the same width.";
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            superToolTip7.MaxWidth = 210;
            this.CommandBarItem53.SuperTip = superToolTip7;
            // 
            // CommandBarItem54
            // 
            this.CommandBarItem54.Caption = "Size to Grid";
            this.CommandBarItem54.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.SizeToGrid;
            this.CommandBarItem54.Enabled = false;
            this.CommandBarItem54.Id = 8;
            this.CommandBarItem54.Name = "CommandBarItem54";
            superToolTip8.FixedTooltipWidth = true;
            toolTipTitleItem8.Text = "Size to Grid";
            toolTipItem8.LeftIndent = 6;
            toolTipItem8.Text = "Size the selected controls to the grid.";
            superToolTip8.Items.Add(toolTipTitleItem8);
            superToolTip8.Items.Add(toolTipItem8);
            superToolTip8.MaxWidth = 210;
            this.CommandBarItem54.SuperTip = superToolTip8;
            // 
            // CommandBarItem55
            // 
            this.CommandBarItem55.Caption = "Make Same Height";
            this.CommandBarItem55.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.SizeToControlHeight;
            this.CommandBarItem55.Enabled = false;
            this.CommandBarItem55.Id = 9;
            this.CommandBarItem55.Name = "CommandBarItem55";
            superToolTip9.FixedTooltipWidth = true;
            toolTipTitleItem9.Text = "Make Same Height";
            toolTipItem9.LeftIndent = 6;
            toolTipItem9.Text = "Make the selected controls have the same height.";
            superToolTip9.Items.Add(toolTipTitleItem9);
            superToolTip9.Items.Add(toolTipItem9);
            superToolTip9.MaxWidth = 210;
            this.CommandBarItem55.SuperTip = superToolTip9;
            // 
            // CommandBarItem56
            // 
            this.CommandBarItem56.Caption = "Make Same Size";
            this.CommandBarItem56.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.SizeToControl;
            this.CommandBarItem56.Enabled = false;
            this.CommandBarItem56.Id = 10;
            this.CommandBarItem56.Name = "CommandBarItem56";
            superToolTip10.FixedTooltipWidth = true;
            toolTipTitleItem10.Text = "Make Same Size";
            toolTipItem10.LeftIndent = 6;
            toolTipItem10.Text = "Make the selected controls have the same size.";
            superToolTip10.Items.Add(toolTipTitleItem10);
            superToolTip10.Items.Add(toolTipItem10);
            superToolTip10.MaxWidth = 210;
            this.CommandBarItem56.SuperTip = superToolTip10;
            // 
            // CommandBarItem57
            // 
            this.CommandBarItem57.Caption = "Make Horizontal Spacing Equal";
            this.CommandBarItem57.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.HorizSpaceMakeEqual;
            this.CommandBarItem57.Enabled = false;
            this.CommandBarItem57.Id = 11;
            this.CommandBarItem57.Name = "CommandBarItem57";
            superToolTip11.FixedTooltipWidth = true;
            toolTipTitleItem11.Text = "Make Horizontal Spacing Equal";
            toolTipItem11.LeftIndent = 6;
            toolTipItem11.Text = "Make the horizontal spacing between the selected controls equal.";
            superToolTip11.Items.Add(toolTipTitleItem11);
            superToolTip11.Items.Add(toolTipItem11);
            superToolTip11.MaxWidth = 210;
            this.CommandBarItem57.SuperTip = superToolTip11;
            // 
            // CommandBarItem58
            // 
            this.CommandBarItem58.Caption = "Increase Horizontal Spacing";
            this.CommandBarItem58.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.HorizSpaceIncrease;
            this.CommandBarItem58.Enabled = false;
            this.CommandBarItem58.Id = 12;
            this.CommandBarItem58.Name = "CommandBarItem58";
            superToolTip12.FixedTooltipWidth = true;
            toolTipTitleItem12.Text = "Increase Horizontal Spacing";
            toolTipItem12.LeftIndent = 6;
            toolTipItem12.Text = "Increase the horizontal spacing between the selected controls.";
            superToolTip12.Items.Add(toolTipTitleItem12);
            superToolTip12.Items.Add(toolTipItem12);
            superToolTip12.MaxWidth = 210;
            this.CommandBarItem58.SuperTip = superToolTip12;
            // 
            // CommandBarItem59
            // 
            this.CommandBarItem59.Caption = "Decrease Horizontal Spacing";
            this.CommandBarItem59.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.HorizSpaceDecrease;
            this.CommandBarItem59.Enabled = false;
            this.CommandBarItem59.Id = 13;
            this.CommandBarItem59.Name = "CommandBarItem59";
            superToolTip13.FixedTooltipWidth = true;
            toolTipTitleItem13.Text = "Decrease Horizontal Spacing";
            toolTipItem13.LeftIndent = 6;
            toolTipItem13.Text = "Decrease the horizontal spacing between the selected controls.";
            superToolTip13.Items.Add(toolTipTitleItem13);
            superToolTip13.Items.Add(toolTipItem13);
            superToolTip13.MaxWidth = 210;
            this.CommandBarItem59.SuperTip = superToolTip13;
            // 
            // CommandBarItem60
            // 
            this.CommandBarItem60.Caption = "Remove Horizontal Spacing";
            this.CommandBarItem60.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.HorizSpaceConcatenate;
            this.CommandBarItem60.Enabled = false;
            this.CommandBarItem60.Id = 14;
            this.CommandBarItem60.Name = "CommandBarItem60";
            superToolTip14.FixedTooltipWidth = true;
            toolTipTitleItem14.Text = "Remove Horizontal Spacing";
            toolTipItem14.LeftIndent = 6;
            toolTipItem14.Text = "Remove the horizontal spacing between the selected controls.";
            superToolTip14.Items.Add(toolTipTitleItem14);
            superToolTip14.Items.Add(toolTipItem14);
            superToolTip14.MaxWidth = 210;
            this.CommandBarItem60.SuperTip = superToolTip14;
            // 
            // CommandBarItem61
            // 
            this.CommandBarItem61.Caption = "Make Vertical Spacing Equal";
            this.CommandBarItem61.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.VertSpaceMakeEqual;
            this.CommandBarItem61.Enabled = false;
            this.CommandBarItem61.Id = 15;
            this.CommandBarItem61.Name = "CommandBarItem61";
            superToolTip15.FixedTooltipWidth = true;
            toolTipTitleItem15.Text = "Make Vertical Spacing Equal";
            toolTipItem15.LeftIndent = 6;
            toolTipItem15.Text = "Make the vertical spacing between the selected controls equal.";
            superToolTip15.Items.Add(toolTipTitleItem15);
            superToolTip15.Items.Add(toolTipItem15);
            superToolTip15.MaxWidth = 210;
            this.CommandBarItem61.SuperTip = superToolTip15;
            // 
            // CommandBarItem62
            // 
            this.CommandBarItem62.Caption = "Increase Vertical Spacing";
            this.CommandBarItem62.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.VertSpaceIncrease;
            this.CommandBarItem62.Enabled = false;
            this.CommandBarItem62.Id = 16;
            this.CommandBarItem62.Name = "CommandBarItem62";
            superToolTip16.FixedTooltipWidth = true;
            toolTipTitleItem16.Text = "Increase Vertical Spacing";
            toolTipItem16.LeftIndent = 6;
            toolTipItem16.Text = "Increase the vertical spacing between the selected controls.";
            superToolTip16.Items.Add(toolTipTitleItem16);
            superToolTip16.Items.Add(toolTipItem16);
            superToolTip16.MaxWidth = 210;
            this.CommandBarItem62.SuperTip = superToolTip16;
            // 
            // CommandBarItem63
            // 
            this.CommandBarItem63.Caption = "Decrease Vertical Spacing";
            this.CommandBarItem63.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.VertSpaceDecrease;
            this.CommandBarItem63.Enabled = false;
            this.CommandBarItem63.Id = 17;
            this.CommandBarItem63.Name = "CommandBarItem63";
            superToolTip17.FixedTooltipWidth = true;
            toolTipTitleItem17.Text = "Decrease Vertical Spacing";
            toolTipItem17.LeftIndent = 6;
            toolTipItem17.Text = "Decrease the vertical spacing between the selected controls.";
            superToolTip17.Items.Add(toolTipTitleItem17);
            superToolTip17.Items.Add(toolTipItem17);
            superToolTip17.MaxWidth = 210;
            this.CommandBarItem63.SuperTip = superToolTip17;
            // 
            // CommandBarItem64
            // 
            this.CommandBarItem64.Caption = "Remove Vertical Spacing";
            this.CommandBarItem64.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.VertSpaceConcatenate;
            this.CommandBarItem64.Enabled = false;
            this.CommandBarItem64.Id = 18;
            this.CommandBarItem64.Name = "CommandBarItem64";
            superToolTip18.FixedTooltipWidth = true;
            toolTipTitleItem18.Text = "Remove Vertical Spacing";
            toolTipItem18.LeftIndent = 6;
            toolTipItem18.Text = "Remove the vertical spacing between the selected controls.";
            superToolTip18.Items.Add(toolTipTitleItem18);
            superToolTip18.Items.Add(toolTipItem18);
            superToolTip18.MaxWidth = 210;
            this.CommandBarItem64.SuperTip = superToolTip18;
            // 
            // CommandBarItem65
            // 
            this.CommandBarItem65.Caption = "Center Horizontally";
            this.CommandBarItem65.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.CenterHorizontally;
            this.CommandBarItem65.Enabled = false;
            this.CommandBarItem65.Id = 19;
            this.CommandBarItem65.Name = "CommandBarItem65";
            superToolTip19.FixedTooltipWidth = true;
            toolTipTitleItem19.Text = "Center Horizontally";
            toolTipItem19.LeftIndent = 6;
            toolTipItem19.Text = "Horizontally center the selected controls within a band.";
            superToolTip19.Items.Add(toolTipTitleItem19);
            superToolTip19.Items.Add(toolTipItem19);
            superToolTip19.MaxWidth = 210;
            this.CommandBarItem65.SuperTip = superToolTip19;
            // 
            // CommandBarItem66
            // 
            this.CommandBarItem66.Caption = "Center Vetically";
            this.CommandBarItem66.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.CenterVertically;
            this.CommandBarItem66.Enabled = false;
            this.CommandBarItem66.Id = 20;
            this.CommandBarItem66.Name = "CommandBarItem66";
            superToolTip20.FixedTooltipWidth = true;
            toolTipTitleItem20.Text = "Center Vetically";
            toolTipItem20.LeftIndent = 6;
            toolTipItem20.Text = "Vertically center the selected controls within a band.";
            superToolTip20.Items.Add(toolTipTitleItem20);
            superToolTip20.Items.Add(toolTipItem20);
            superToolTip20.MaxWidth = 210;
            this.CommandBarItem66.SuperTip = superToolTip20;
            // 
            // CommandBarItem67
            // 
            this.CommandBarItem67.Caption = "Bring to Front";
            this.CommandBarItem67.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.BringToFront;
            this.CommandBarItem67.Enabled = false;
            this.CommandBarItem67.Id = 21;
            this.CommandBarItem67.Name = "CommandBarItem67";
            superToolTip21.FixedTooltipWidth = true;
            toolTipTitleItem21.Text = "Bring to Front";
            toolTipItem21.LeftIndent = 6;
            toolTipItem21.Text = "Bring the selected controls to the front.";
            superToolTip21.Items.Add(toolTipTitleItem21);
            superToolTip21.Items.Add(toolTipItem21);
            superToolTip21.MaxWidth = 210;
            this.CommandBarItem67.SuperTip = superToolTip21;
            // 
            // CommandBarItem68
            // 
            this.CommandBarItem68.Caption = "Send to Back";
            this.CommandBarItem68.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.SendToBack;
            this.CommandBarItem68.Enabled = false;
            this.CommandBarItem68.Id = 22;
            this.CommandBarItem68.Name = "CommandBarItem68";
            superToolTip22.FixedTooltipWidth = true;
            toolTipTitleItem22.Text = "Send to Back";
            toolTipItem22.LeftIndent = 6;
            toolTipItem22.Text = "Move the selected controls to the back.";
            superToolTip22.Items.Add(toolTipTitleItem22);
            superToolTip22.Items.Add(toolTipItem22);
            superToolTip22.MaxWidth = 210;
            this.CommandBarItem68.SuperTip = superToolTip22;
            // 
            // CommandBarItem69
            // 
            this.CommandBarItem69.Caption = "Bold";
            this.CommandBarItem69.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.FontBold;
            this.CommandBarItem69.Enabled = false;
            this.CommandBarItem69.Id = 23;
            this.CommandBarItem69.Name = "CommandBarItem69";
            superToolTip23.FixedTooltipWidth = true;
            toolTipTitleItem23.Text = "Bold (Ctrl+B)";
            toolTipItem23.LeftIndent = 6;
            toolTipItem23.Text = "Make the selected text bold.";
            superToolTip23.Items.Add(toolTipTitleItem23);
            superToolTip23.Items.Add(toolTipItem23);
            superToolTip23.MaxWidth = 210;
            this.CommandBarItem69.SuperTip = superToolTip23;
            // 
            // CommandBarItem70
            // 
            this.CommandBarItem70.Caption = "Italic";
            this.CommandBarItem70.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.FontItalic;
            this.CommandBarItem70.Enabled = false;
            this.CommandBarItem70.Id = 24;
            this.CommandBarItem70.Name = "CommandBarItem70";
            superToolTip24.FixedTooltipWidth = true;
            toolTipTitleItem24.Text = "Italic (Ctrl+I)";
            toolTipItem24.LeftIndent = 6;
            toolTipItem24.Text = "Italicize the text.";
            superToolTip24.Items.Add(toolTipTitleItem24);
            superToolTip24.Items.Add(toolTipItem24);
            superToolTip24.MaxWidth = 210;
            this.CommandBarItem70.SuperTip = superToolTip24;
            // 
            // CommandBarItem71
            // 
            this.CommandBarItem71.Caption = "Underline";
            this.CommandBarItem71.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.FontUnderline;
            this.CommandBarItem71.Enabled = false;
            this.CommandBarItem71.Id = 25;
            this.CommandBarItem71.Name = "CommandBarItem71";
            superToolTip25.FixedTooltipWidth = true;
            toolTipTitleItem25.Text = "Underline (Ctrl+U)";
            toolTipItem25.LeftIndent = 6;
            toolTipItem25.Text = "Underline the selected text.";
            superToolTip25.Items.Add(toolTipTitleItem25);
            superToolTip25.Items.Add(toolTipItem25);
            superToolTip25.MaxWidth = 210;
            this.CommandBarItem71.SuperTip = superToolTip25;
            // 
            // CommandColorBarItem3
            // 
            this.CommandColorBarItem3.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.CommandColorBarItem3.Caption = "Foreground Color";
            this.CommandColorBarItem3.CloseSubMenuOnClick = false;
            this.CommandColorBarItem3.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.ForeColor;
            this.CommandColorBarItem3.Enabled = false;
            this.CommandColorBarItem3.Id = 26;
            this.CommandColorBarItem3.Name = "CommandColorBarItem3";
            superToolTip26.FixedTooltipWidth = true;
            toolTipTitleItem26.Text = "Foreground Color";
            toolTipItem26.LeftIndent = 6;
            toolTipItem26.Text = "Change the text foreground color.";
            superToolTip26.Items.Add(toolTipTitleItem26);
            superToolTip26.Items.Add(toolTipItem26);
            superToolTip26.MaxWidth = 210;
            this.CommandColorBarItem3.SuperTip = superToolTip26;
            // 
            // CommandColorBarItem4
            // 
            this.CommandColorBarItem4.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.CommandColorBarItem4.Caption = "Background Color";
            this.CommandColorBarItem4.CloseSubMenuOnClick = false;
            this.CommandColorBarItem4.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.BackColor;
            this.CommandColorBarItem4.Enabled = false;
            this.CommandColorBarItem4.Id = 27;
            this.CommandColorBarItem4.Name = "CommandColorBarItem4";
            superToolTip27.FixedTooltipWidth = true;
            toolTipTitleItem27.Text = "Background Color";
            toolTipItem27.LeftIndent = 6;
            toolTipItem27.Text = "Change the text background color.";
            superToolTip27.Items.Add(toolTipTitleItem27);
            superToolTip27.Items.Add(toolTipItem27);
            superToolTip27.MaxWidth = 210;
            this.CommandColorBarItem4.SuperTip = superToolTip27;
            // 
            // CommandBarItem72
            // 
            this.CommandBarItem72.Caption = "Align Text Left";
            this.CommandBarItem72.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.JustifyLeft;
            this.CommandBarItem72.Enabled = false;
            this.CommandBarItem72.Id = 28;
            this.CommandBarItem72.Name = "CommandBarItem72";
            superToolTip28.FixedTooltipWidth = true;
            toolTipTitleItem28.Text = "Align Text Left";
            toolTipItem28.LeftIndent = 6;
            toolTipItem28.Text = "Align text to the left.";
            superToolTip28.Items.Add(toolTipTitleItem28);
            superToolTip28.Items.Add(toolTipItem28);
            superToolTip28.MaxWidth = 210;
            this.CommandBarItem72.SuperTip = superToolTip28;
            // 
            // CommandBarItem73
            // 
            this.CommandBarItem73.Caption = "Center Text";
            this.CommandBarItem73.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.JustifyCenter;
            this.CommandBarItem73.Enabled = false;
            this.CommandBarItem73.Id = 29;
            this.CommandBarItem73.Name = "CommandBarItem73";
            superToolTip29.FixedTooltipWidth = true;
            toolTipTitleItem29.Text = "Center Text";
            toolTipItem29.LeftIndent = 6;
            toolTipItem29.Text = "Center text.";
            superToolTip29.Items.Add(toolTipTitleItem29);
            superToolTip29.Items.Add(toolTipItem29);
            superToolTip29.MaxWidth = 210;
            this.CommandBarItem73.SuperTip = superToolTip29;
            // 
            // CommandBarItem74
            // 
            this.CommandBarItem74.Caption = "Align Text Right";
            this.CommandBarItem74.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.JustifyRight;
            this.CommandBarItem74.Enabled = false;
            this.CommandBarItem74.Id = 30;
            this.CommandBarItem74.Name = "CommandBarItem74";
            superToolTip30.FixedTooltipWidth = true;
            toolTipTitleItem30.Text = "Align Text Right";
            toolTipItem30.LeftIndent = 6;
            toolTipItem30.Text = "Align text to the right.";
            superToolTip30.Items.Add(toolTipTitleItem30);
            superToolTip30.Items.Add(toolTipItem30);
            superToolTip30.MaxWidth = 210;
            this.CommandBarItem74.SuperTip = superToolTip30;
            // 
            // CommandBarItem75
            // 
            this.CommandBarItem75.Caption = "Justify";
            this.CommandBarItem75.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.JustifyJustify;
            this.CommandBarItem75.Enabled = false;
            this.CommandBarItem75.Id = 31;
            this.CommandBarItem75.Name = "CommandBarItem75";
            superToolTip31.FixedTooltipWidth = true;
            toolTipTitleItem31.Text = "Justify";
            toolTipItem31.LeftIndent = 6;
            toolTipItem31.Text = "Align text to both the left and right sides, adding extra space between words as " +
    "necessary.";
            superToolTip31.Items.Add(toolTipTitleItem31);
            superToolTip31.Items.Add(toolTipItem31);
            superToolTip31.MaxWidth = 210;
            this.CommandBarItem75.SuperTip = superToolTip31;
            // 
            // CommandBarItem76
            // 
            this.CommandBarItem76.ActAsDropDown = true;
            this.CommandBarItem76.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.CommandBarItem76.Caption = "Llamada";
            this.CommandBarItem76.Id = 32;
            this.CommandBarItem76.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P));
            this.CommandBarItem76.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("CommandBarItem76.LargeGlyph")));
            this.CommandBarItem76.Name = "CommandBarItem76";
            superToolTip32.FixedTooltipWidth = true;
            toolTipTitleItem32.Text = "(Ctrl+P) Monitoreo de llamada \r\n";
            toolTipItem32.LeftIndent = 6;
            toolTipItem32.Text = "Permite al supervisor escuchar la llamada del operador seleccionado.";
            superToolTip32.Items.Add(toolTipTitleItem32);
            superToolTip32.Items.Add(toolTipItem32);
            superToolTip32.MaxWidth = 210;
            this.CommandBarItem76.SuperTip = superToolTip32;
            // 
            // CommandBarItem77
            // 
            this.CommandBarItem77.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.CommandBarItem77.Caption = "Pantalla...";
            this.CommandBarItem77.Id = 33;
            this.CommandBarItem77.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("CommandBarItem77.LargeGlyph")));
            this.CommandBarItem77.Name = "CommandBarItem77";
            superToolTip33.FixedTooltipWidth = true;
            toolTipTitleItem33.Text = "Monitoreo de la Llamada y la Pantalla.";
            toolTipItem33.LeftIndent = 6;
            toolTipItem33.Text = "Permite al Supervisor el monitore";
            superToolTip33.Items.Add(toolTipTitleItem33);
            superToolTip33.Items.Add(toolTipItem33);
            superToolTip33.MaxWidth = 210;
            this.CommandBarItem77.SuperTip = superToolTip33;
            // 
            // CommandBarItem78
            // 
            this.CommandBarItem78.Caption = "New Report";
            this.CommandBarItem78.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.NewReport;
            this.CommandBarItem78.Id = 34;
            this.CommandBarItem78.Name = "CommandBarItem78";
            superToolTip34.FixedTooltipWidth = true;
            toolTipTitleItem34.Text = "New Blank Report (Ctrl+N)";
            toolTipItem34.LeftIndent = 6;
            toolTipItem34.Text = "Create a new blank report so that you can insert fields and controls and design a" +
    " report.";
            superToolTip34.Items.Add(toolTipTitleItem34);
            superToolTip34.Items.Add(toolTipItem34);
            superToolTip34.MaxWidth = 210;
            this.CommandBarItem78.SuperTip = superToolTip34;
            // 
            // CommandBarItem79
            // 
            this.CommandBarItem79.Caption = "New Report with Wizard...";
            this.CommandBarItem79.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.NewReportWizard;
            this.CommandBarItem79.Id = 35;
            this.CommandBarItem79.Name = "CommandBarItem79";
            superToolTip35.FixedTooltipWidth = true;
            toolTipTitleItem35.Text = "New Report with Wizard (Ctrl+W)";
            toolTipItem35.LeftIndent = 6;
            toolTipItem35.Text = "Launch the report wizard which helps you to create simple, customized reports.";
            superToolTip35.Items.Add(toolTipTitleItem35);
            superToolTip35.Items.Add(toolTipItem35);
            superToolTip35.MaxWidth = 210;
            this.CommandBarItem79.SuperTip = superToolTip35;
            // 
            // CommandBarItem80
            // 
            this.CommandBarItem80.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.CommandBarItem80.Caption = "Panta...";
            this.CommandBarItem80.Id = 36;
            this.CommandBarItem80.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P));
            this.CommandBarItem80.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("CommandBarItem80.LargeGlyph")));
            this.CommandBarItem80.Name = "CommandBarItem80";
            superToolTip36.FixedTooltipWidth = true;
            toolTipTitleItem36.Text = "(Ctrl+P) Monitoreo de Pantalla";
            toolTipItem36.LeftIndent = 6;
            toolTipItem36.Text = "Permite al supervisor monitorear la pantalla del operador seleccionado.";
            superToolTip36.Items.Add(toolTipTitleItem36);
            superToolTip36.Items.Add(toolTipItem36);
            superToolTip36.MaxWidth = 210;
            this.CommandBarItem80.SuperTip = superToolTip36;
            // 
            // CommandBarItem81
            // 
            this.CommandBarItem81.Caption = "Save";
            this.CommandBarItem81.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.SaveFile;
            this.CommandBarItem81.Enabled = false;
            this.CommandBarItem81.Id = 37;
            this.CommandBarItem81.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S));
            this.CommandBarItem81.Name = "CommandBarItem81";
            superToolTip37.FixedTooltipWidth = true;
            toolTipTitleItem37.Text = "Save Report (Ctrl+S)";
            toolTipItem37.LeftIndent = 6;
            toolTipItem37.Text = "Save a report.";
            superToolTip37.Items.Add(toolTipTitleItem37);
            superToolTip37.Items.Add(toolTipItem37);
            superToolTip37.MaxWidth = 210;
            this.CommandBarItem81.SuperTip = superToolTip37;
            // 
            // CommandBarItem82
            // 
            this.CommandBarItem82.Caption = "Save As...";
            this.CommandBarItem82.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.SaveFileAs;
            this.CommandBarItem82.Enabled = false;
            this.CommandBarItem82.Id = 38;
            this.CommandBarItem82.Name = "CommandBarItem82";
            superToolTip38.FixedTooltipWidth = true;
            toolTipTitleItem38.Text = "Save Report As";
            toolTipItem38.LeftIndent = 6;
            toolTipItem38.Text = "Save a report with a new name.";
            superToolTip38.Items.Add(toolTipTitleItem38);
            superToolTip38.Items.Add(toolTipItem38);
            superToolTip38.MaxWidth = 210;
            this.CommandBarItem82.SuperTip = superToolTip38;
            // 
            // commandBarItemIndicators
            // 
            this.commandBarItemIndicators.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left;
            this.commandBarItemIndicators.Caption = "Indicadores";
            this.commandBarItemIndicators.Id = 39;
            this.commandBarItemIndicators.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.I));
            this.commandBarItemIndicators.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("commandBarItemIndicators.LargeGlyph")));
            this.commandBarItemIndicators.Name = "commandBarItemIndicators";
            this.commandBarItemIndicators.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.commandBarItemIndicators_ItemClick);
            // 
            // CommandBarItem86
            // 
            this.CommandBarItem86.Id = 42;
            this.CommandBarItem86.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("CommandBarItem86.LargeGlyph")));
            this.CommandBarItem86.Name = "CommandBarItem86";
            superToolTip39.FixedTooltipWidth = true;
            toolTipTitleItem39.Text = "(Ctrl+E)\r\nAdjuntar a un correo";
            toolTipItem39.LeftIndent = 6;
            toolTipItem39.Text = "Adjunta las actividades del operador al cliente de correo establecido en la mquin" +
    "a loca.";
            superToolTip39.Items.Add(toolTipTitleItem39);
            superToolTip39.Items.Add(toolTipItem39);
            superToolTip39.MaxWidth = 210;
            this.CommandBarItem86.SuperTip = superToolTip39;
            // 
            // CommandBarItem90
            // 
            this.CommandBarItem90.Caption = "Ubicar";
            this.CommandBarItem90.Id = 46;
            this.CommandBarItem90.Name = "CommandBarItem90";
            superToolTip40.FixedTooltipWidth = true;
            toolTipTitleItem40.Text = "(Ctrl+U) Ubicar";
            toolTipItem40.LeftIndent = 6;
            toolTipItem40.Text = "Permite ubicar en el plano de la sala al operador seleccionado.";
            superToolTip40.Items.Add(toolTipTitleItem40);
            superToolTip40.Items.Add(toolTipItem40);
            superToolTip40.MaxWidth = 210;
            this.CommandBarItem90.SuperTip = superToolTip40;
            // 
            // BarDockPanelsListItem2
            // 
            this.BarDockPanelsListItem2.Caption = "Ubicar";
            this.BarDockPanelsListItem2.Id = 49;
            this.BarDockPanelsListItem2.Name = "BarDockPanelsListItem2";
            this.BarDockPanelsListItem2.ShowCustomizationItem = false;
            this.BarDockPanelsListItem2.ShowDockPanels = true;
            this.BarDockPanelsListItem2.ShowToolbars = false;
            superToolTip41.FixedTooltipWidth = true;
            toolTipTitleItem41.Text = "(Ctrl+U) Ubicar";
            toolTipItem41.LeftIndent = 6;
            toolTipItem41.Text = "Permite ubicar al operador en su respectiva sala.";
            superToolTip41.Items.Add(toolTipTitleItem41);
            superToolTip41.Items.Add(toolTipItem41);
            superToolTip41.MaxWidth = 210;
            this.BarDockPanelsListItem2.SuperTip = superToolTip41;
            // 
            // XrDesignBarButtonGroup4
            // 
            this.XrDesignBarButtonGroup4.Id = 58;
            this.XrDesignBarButtonGroup4.ItemLinks.Add(this.CommandBarItem46);
            this.XrDesignBarButtonGroup4.ItemLinks.Add(this.CommandBarItem48);
            this.XrDesignBarButtonGroup4.ItemLinks.Add(this.CommandBarItem49);
            this.XrDesignBarButtonGroup4.ItemLinks.Add(this.CommandBarItem46);
            this.XrDesignBarButtonGroup4.ItemLinks.Add(this.CommandBarItem48);
            this.XrDesignBarButtonGroup4.ItemLinks.Add(this.CommandBarItem49);
            this.XrDesignBarButtonGroup4.Name = "XrDesignBarButtonGroup4";
            // 
            // XrDesignBarButtonGroup5
            // 
            this.XrDesignBarButtonGroup5.Id = 59;
            this.XrDesignBarButtonGroup5.ItemLinks.Add(this.CommandBarItem50);
            this.XrDesignBarButtonGroup5.ItemLinks.Add(this.CommandBarItem51);
            this.XrDesignBarButtonGroup5.ItemLinks.Add(this.CommandBarItem52);
            this.XrDesignBarButtonGroup5.ItemLinks.Add(this.CommandBarItem50);
            this.XrDesignBarButtonGroup5.ItemLinks.Add(this.CommandBarItem51);
            this.XrDesignBarButtonGroup5.ItemLinks.Add(this.CommandBarItem52);
            this.XrDesignBarButtonGroup5.Name = "XrDesignBarButtonGroup5";
            // 
            // barButtonItemCopy
            // 
            this.barButtonItemCopy.Enabled = false;
            this.barButtonItemCopy.Glyph = global::SmartCadGuiCommon.XRDesignRibbonControllerResources.RibbonUserDesigner_Copy;
            this.barButtonItemCopy.Id = 64;
            this.barButtonItemCopy.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C));
            this.barButtonItemCopy.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemCopy.Name = "barButtonItemCopy";
            this.barButtonItemCopy.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barButtonItemCut
            // 
            this.barButtonItemCut.Enabled = false;
            this.barButtonItemCut.Glyph = global::SmartCadGuiCommon.XRDesignRibbonControllerResources.RibbonUserDesigner_Cut;
            this.barButtonItemCut.Id = 65;
            this.barButtonItemCut.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X));
            this.barButtonItemCut.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemCut.Name = "barButtonItemCut";
            this.barButtonItemCut.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // printPreviewBarItemPersonalfile
            // 
            this.printPreviewBarItemPersonalfile.Caption = "Ver expediente";
            this.printPreviewBarItemPersonalfile.Id = 66;
            this.printPreviewBarItemPersonalfile.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E));
            this.printPreviewBarItemPersonalfile.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("printPreviewBarItemPersonalfile.LargeGlyph")));
            this.printPreviewBarItemPersonalfile.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.printPreviewBarItemPersonalfile.Name = "printPreviewBarItemPersonalfile";
            this.printPreviewBarItemPersonalfile.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.PrintPreviewBarItem3_ItemClick);
            // 
            // printPreviewBarItemPersonalFileObs
            // 
            this.printPreviewBarItemPersonalFileObs.Caption = "Agregar observacin";
            this.printPreviewBarItemPersonalFileObs.Enabled = false;
            this.printPreviewBarItemPersonalFileObs.Glyph = ((System.Drawing.Image)(resources.GetObject("printPreviewBarItemPersonalFileObs.Glyph")));
            this.printPreviewBarItemPersonalFileObs.Id = 67;
            this.printPreviewBarItemPersonalFileObs.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.printPreviewBarItemPersonalFileObs.Name = "printPreviewBarItemPersonalFileObs";
            this.printPreviewBarItemPersonalFileObs.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.printPreviewBarItemPersonalFileObs.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.PrintPreviewBarItemPersonalFileObs_ItemClick);
            // 
            // printPreviewBarItemModifyCategory
            // 
            this.printPreviewBarItemModifyCategory.Caption = "Modificar categora";
            this.printPreviewBarItemModifyCategory.Enabled = false;
            this.printPreviewBarItemModifyCategory.Glyph = ((System.Drawing.Image)(resources.GetObject("printPreviewBarItemModifyCategory.Glyph")));
            this.printPreviewBarItemModifyCategory.Id = 68;
            this.printPreviewBarItemModifyCategory.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("printPreviewBarItemModifyCategory.LargeGlyph")));
            this.printPreviewBarItemModifyCategory.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.printPreviewBarItemModifyCategory.Name = "printPreviewBarItemModifyCategory";
            this.printPreviewBarItemModifyCategory.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.printPreviewBarItemModifyCategory.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.PrintPreviewBarItemModifyCategory_ItemClick);
            // 
            // PrintPreviewBarItem6
            // 
            this.PrintPreviewBarItem6.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.PrintPreviewBarItem6.Caption = "Custom Margins...";
            this.PrintPreviewBarItem6.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PageSetup;
            this.PrintPreviewBarItem6.Enabled = false;
            this.PrintPreviewBarItem6.Id = 69;
            this.PrintPreviewBarItem6.Name = "PrintPreviewBarItem6";
            superToolTip42.FixedTooltipWidth = true;
            toolTipTitleItem42.Text = "Page Setup";
            toolTipItem42.LeftIndent = 6;
            toolTipItem42.Text = "Show the Page Setup dialog.";
            superToolTip42.Items.Add(toolTipTitleItem42);
            superToolTip42.Items.Add(toolTipItem42);
            superToolTip42.MaxWidth = 210;
            this.PrintPreviewBarItem6.SuperTip = superToolTip42;
            // 
            // barItemCreateCourses
            // 
            this.barItemCreateCourses.Caption = "Crear";
            this.barItemCreateCourses.Enabled = false;
            this.barItemCreateCourses.Glyph = ((System.Drawing.Image)(resources.GetObject("barItemCreateCourses.Glyph")));
            this.barItemCreateCourses.Id = 70;
            this.barItemCreateCourses.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barItemCreateCourses.Name = "barItemCreateCourses";
            this.barItemCreateCourses.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barItemCreateCourses.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.PrintPreviewBarItemCreateCourses_ItemClick);
            // 
            // barItemDeleteCourses
            // 
            this.barItemDeleteCourses.Caption = "Eliminar";
            this.barItemDeleteCourses.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Scale;
            this.barItemDeleteCourses.Enabled = false;
            this.barItemDeleteCourses.Glyph = ((System.Drawing.Image)(resources.GetObject("barItemDeleteCourses.Glyph")));
            this.barItemDeleteCourses.Id = 71;
            this.barItemDeleteCourses.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barItemDeleteCourses.Name = "barItemDeleteCourses";
            this.barItemDeleteCourses.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // PrintPreviewBarItem9
            // 
            this.PrintPreviewBarItem9.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.PrintPreviewBarItem9.Caption = "Pointer";
            this.PrintPreviewBarItem9.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Pointer;
            this.PrintPreviewBarItem9.Enabled = false;
            this.PrintPreviewBarItem9.GroupIndex = 1;
            this.PrintPreviewBarItem9.Id = 72;
            this.PrintPreviewBarItem9.Name = "PrintPreviewBarItem9";
            this.PrintPreviewBarItem9.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            superToolTip43.FixedTooltipWidth = true;
            toolTipTitleItem43.Text = "Mouse Pointer";
            toolTipItem43.LeftIndent = 6;
            toolTipItem43.Text = "Show the mouse pointer.";
            superToolTip43.Items.Add(toolTipTitleItem43);
            superToolTip43.Items.Add(toolTipItem43);
            superToolTip43.MaxWidth = 210;
            this.PrintPreviewBarItem9.SuperTip = superToolTip43;
            // 
            // PrintPreviewBarItem10
            // 
            this.PrintPreviewBarItem10.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.PrintPreviewBarItem10.Caption = "Hand Tool";
            this.PrintPreviewBarItem10.Command = DevExpress.XtraPrinting.PrintingSystemCommand.HandTool;
            this.PrintPreviewBarItem10.Enabled = false;
            this.PrintPreviewBarItem10.GroupIndex = 1;
            this.PrintPreviewBarItem10.Id = 73;
            this.PrintPreviewBarItem10.Name = "PrintPreviewBarItem10";
            this.PrintPreviewBarItem10.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            superToolTip44.FixedTooltipWidth = true;
            toolTipTitleItem44.Text = "Hand Tool";
            toolTipItem44.LeftIndent = 6;
            toolTipItem44.Text = "Invoke the Hand tool to manually scroll through pages.";
            superToolTip44.Items.Add(toolTipTitleItem44);
            superToolTip44.Items.Add(toolTipItem44);
            superToolTip44.MaxWidth = 210;
            this.PrintPreviewBarItem10.SuperTip = superToolTip44;
            // 
            // PrintPreviewBarItem11
            // 
            this.PrintPreviewBarItem11.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.PrintPreviewBarItem11.Caption = "Magnifier";
            this.PrintPreviewBarItem11.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Magnifier;
            this.PrintPreviewBarItem11.Enabled = false;
            this.PrintPreviewBarItem11.GroupIndex = 1;
            this.PrintPreviewBarItem11.Id = 74;
            this.PrintPreviewBarItem11.Name = "PrintPreviewBarItem11";
            this.PrintPreviewBarItem11.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            superToolTip45.FixedTooltipWidth = true;
            toolTipTitleItem45.Text = "Magnifier";
            toolTipItem45.LeftIndent = 6;
            toolTipItem45.Text = "Invoke the Magnifier tool.\r\n\r\nClicking once on a document zooms it so that a sing" +
    "le page becomes entirely visible, while clicking another time zooms it to 100% o" +
    "f the normal size.";
            superToolTip45.Items.Add(toolTipTitleItem45);
            superToolTip45.Items.Add(toolTipItem45);
            superToolTip45.MaxWidth = 210;
            this.PrintPreviewBarItem11.SuperTip = superToolTip45;
            // 
            // PrintPreviewBarItem12
            // 
            this.PrintPreviewBarItem12.Caption = "Zoom Out";
            this.PrintPreviewBarItem12.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ZoomOut;
            this.PrintPreviewBarItem12.Enabled = false;
            this.PrintPreviewBarItem12.Id = 75;
            this.PrintPreviewBarItem12.Name = "PrintPreviewBarItem12";
            superToolTip46.FixedTooltipWidth = true;
            toolTipTitleItem46.Text = "Zoom Out";
            toolTipItem46.LeftIndent = 6;
            toolTipItem46.Text = "Zoom out to see more of the page at a reduced size.";
            superToolTip46.Items.Add(toolTipTitleItem46);
            superToolTip46.Items.Add(toolTipItem46);
            superToolTip46.MaxWidth = 210;
            this.PrintPreviewBarItem12.SuperTip = superToolTip46;
            // 
            // PrintPreviewBarItem13
            // 
            this.PrintPreviewBarItem13.Caption = "Zoom In";
            this.PrintPreviewBarItem13.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ZoomIn;
            this.PrintPreviewBarItem13.Enabled = false;
            this.PrintPreviewBarItem13.Id = 76;
            this.PrintPreviewBarItem13.Name = "PrintPreviewBarItem13";
            superToolTip47.FixedTooltipWidth = true;
            toolTipTitleItem47.Text = "Zoom In";
            toolTipItem47.LeftIndent = 6;
            toolTipItem47.Text = "Zoom in to get a close-up view of the document.";
            superToolTip47.Items.Add(toolTipTitleItem47);
            superToolTip47.Items.Add(toolTipItem47);
            superToolTip47.MaxWidth = 210;
            this.PrintPreviewBarItem13.SuperTip = superToolTip47;
            // 
            // PrintPreviewBarItem14
            // 
            this.PrintPreviewBarItem14.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.PrintPreviewBarItem14.Caption = "Zoom";
            this.PrintPreviewBarItem14.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Zoom;
            this.PrintPreviewBarItem14.Enabled = false;
            this.PrintPreviewBarItem14.Id = 77;
            this.PrintPreviewBarItem14.Name = "PrintPreviewBarItem14";
            superToolTip48.FixedTooltipWidth = true;
            toolTipTitleItem48.Text = "Zoom";
            toolTipItem48.LeftIndent = 6;
            toolTipItem48.Text = "Change the zoom level of the document preview.";
            superToolTip48.Items.Add(toolTipTitleItem48);
            superToolTip48.Items.Add(toolTipItem48);
            superToolTip48.MaxWidth = 210;
            this.PrintPreviewBarItem14.SuperTip = superToolTip48;
            // 
            // barButtonItemPaste
            // 
            this.barButtonItemPaste.Enabled = false;
            this.barButtonItemPaste.Glyph = global::SmartCadGuiCommon.XRDesignRibbonControllerResources.RibbonUserDesigner_Paste;
            this.barButtonItemPaste.Id = 79;
            this.barButtonItemPaste.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V));
            this.barButtonItemPaste.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemPaste.Name = "barButtonItemPaste";
            this.barButtonItemPaste.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // PrintPreviewBarItem19
            // 
            this.PrintPreviewBarItem19.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.PrintPreviewBarItem19.Caption = "Many Pages";
            this.PrintPreviewBarItem19.Command = DevExpress.XtraPrinting.PrintingSystemCommand.MultiplePages;
            this.PrintPreviewBarItem19.Enabled = false;
            this.PrintPreviewBarItem19.Id = 82;
            this.PrintPreviewBarItem19.Name = "PrintPreviewBarItem19";
            superToolTip49.FixedTooltipWidth = true;
            toolTipTitleItem49.Text = "View Many Pages";
            toolTipItem49.LeftIndent = 6;
            toolTipItem49.Text = "Choose the page layout to arrange the document pages in preview.";
            superToolTip49.Items.Add(toolTipTitleItem49);
            superToolTip49.Items.Add(toolTipItem49);
            superToolTip49.MaxWidth = 210;
            this.PrintPreviewBarItem19.SuperTip = superToolTip49;
            // 
            // PrintPreviewBarItem20
            // 
            this.PrintPreviewBarItem20.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.PrintPreviewBarItem20.Caption = "Page Color";
            this.PrintPreviewBarItem20.Command = DevExpress.XtraPrinting.PrintingSystemCommand.FillBackground;
            this.PrintPreviewBarItem20.Enabled = false;
            this.PrintPreviewBarItem20.Id = 83;
            this.PrintPreviewBarItem20.Name = "PrintPreviewBarItem20";
            superToolTip50.FixedTooltipWidth = true;
            toolTipTitleItem50.Text = "Background Color";
            toolTipItem50.LeftIndent = 6;
            toolTipItem50.Text = "Choose a color for the background of the document pages.";
            superToolTip50.Items.Add(toolTipTitleItem50);
            superToolTip50.Items.Add(toolTipItem50);
            superToolTip50.MaxWidth = 210;
            this.PrintPreviewBarItem20.SuperTip = superToolTip50;
            // 
            // PrintPreviewBarItem21
            // 
            this.PrintPreviewBarItem21.Caption = "Watermark";
            this.PrintPreviewBarItem21.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Watermark;
            this.PrintPreviewBarItem21.Enabled = false;
            this.PrintPreviewBarItem21.Id = 84;
            this.PrintPreviewBarItem21.Name = "PrintPreviewBarItem21";
            superToolTip51.FixedTooltipWidth = true;
            toolTipTitleItem51.Text = "Watermark";
            toolTipItem51.LeftIndent = 6;
            toolTipItem51.Text = "Insert ghosted text or image behind the content of a page.\r\n\r\nThis is often used " +
    "to indicate that a document is to be treated specially.";
            superToolTip51.Items.Add(toolTipTitleItem51);
            superToolTip51.Items.Add(toolTipItem51);
            superToolTip51.MaxWidth = 210;
            this.PrintPreviewBarItem21.SuperTip = superToolTip51;
            // 
            // PrintPreviewBarItem22
            // 
            this.PrintPreviewBarItem22.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.PrintPreviewBarItem22.Caption = "Export To";
            this.PrintPreviewBarItem22.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportFile;
            this.PrintPreviewBarItem22.Enabled = false;
            this.PrintPreviewBarItem22.Id = 85;
            this.PrintPreviewBarItem22.Name = "PrintPreviewBarItem22";
            superToolTip52.FixedTooltipWidth = true;
            toolTipTitleItem52.Text = "Export To...";
            toolTipItem52.LeftIndent = 6;
            toolTipItem52.Text = "Export the current document in one of the available formats, and save it to the f" +
    "ile on a disk.";
            superToolTip52.Items.Add(toolTipTitleItem52);
            superToolTip52.Items.Add(toolTipItem52);
            superToolTip52.MaxWidth = 210;
            this.PrintPreviewBarItem22.SuperTip = superToolTip52;
            // 
            // PrintPreviewBarItem23
            // 
            this.PrintPreviewBarItem23.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.PrintPreviewBarItem23.Caption = "E-Mail As";
            this.PrintPreviewBarItem23.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendFile;
            this.PrintPreviewBarItem23.Enabled = false;
            this.PrintPreviewBarItem23.Id = 86;
            this.PrintPreviewBarItem23.Name = "PrintPreviewBarItem23";
            superToolTip53.FixedTooltipWidth = true;
            toolTipTitleItem53.Text = "E-Mail As...";
            toolTipItem53.LeftIndent = 6;
            toolTipItem53.Text = "Export the current document in one of the available formats, and attach it to the" +
    " e-mail.";
            superToolTip53.Items.Add(toolTipTitleItem53);
            superToolTip53.Items.Add(toolTipItem53);
            superToolTip53.MaxWidth = 210;
            this.PrintPreviewBarItem23.SuperTip = superToolTip53;
            // 
            // PrintPreviewBarItem24
            // 
            this.PrintPreviewBarItem24.Caption = "Close Print Preview";
            this.PrintPreviewBarItem24.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ClosePreview;
            this.PrintPreviewBarItem24.Id = 87;
            this.PrintPreviewBarItem24.Name = "PrintPreviewBarItem24";
            superToolTip54.FixedTooltipWidth = true;
            toolTipTitleItem54.Text = "Close Print Preview";
            toolTipItem54.LeftIndent = 6;
            toolTipItem54.Text = "Close Print Preview of the document.";
            superToolTip54.Items.Add(toolTipTitleItem54);
            superToolTip54.Items.Add(toolTipItem54);
            superToolTip54.MaxWidth = 210;
            this.PrintPreviewBarItem24.SuperTip = superToolTip54;
            // 
            // barButtonOperatorTrainingAssign
            // 
            this.barButtonOperatorTrainingAssign.Caption = "Participantes";
            this.barButtonOperatorTrainingAssign.Enabled = false;
            this.barButtonOperatorTrainingAssign.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonOperatorTrainingAssign.Glyph")));
            this.barButtonOperatorTrainingAssign.Id = 88;
            this.barButtonOperatorTrainingAssign.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonOperatorTrainingAssign.Name = "barButtonOperatorTrainingAssign";
            this.barButtonOperatorTrainingAssign.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonOperatorTrainingAssign.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BarButtonOperatorTrainingAssign_ItemClick);
            // 
            // barButtonTrainingCoursesSchedule
            // 
            this.barButtonTrainingCoursesSchedule.Caption = "Horarios";
            this.barButtonTrainingCoursesSchedule.Enabled = false;
            this.barButtonTrainingCoursesSchedule.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonTrainingCoursesSchedule.Glyph")));
            this.barButtonTrainingCoursesSchedule.Id = 90;
            this.barButtonTrainingCoursesSchedule.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonTrainingCoursesSchedule.Name = "barButtonTrainingCoursesSchedule";
            this.barButtonTrainingCoursesSchedule.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonTrainingCoursesSchedule.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BarButtonTrainingCoursesSchedule_ItemClick);
            // 
            // PrintPreviewBarItem28
            // 
            this.PrintPreviewBarItem28.Caption = "PDF File";
            this.PrintPreviewBarItem28.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendPdf;
            this.PrintPreviewBarItem28.Description = "Adobe Portable Document Format";
            this.PrintPreviewBarItem28.Enabled = false;
            this.PrintPreviewBarItem28.Id = 91;
            this.PrintPreviewBarItem28.Name = "PrintPreviewBarItem28";
            superToolTip55.FixedTooltipWidth = true;
            toolTipTitleItem55.Text = "E-Mail As PDF";
            toolTipItem55.LeftIndent = 6;
            toolTipItem55.Text = "Export the document to PDF and attach it to the e-mail.";
            superToolTip55.Items.Add(toolTipTitleItem55);
            superToolTip55.Items.Add(toolTipItem55);
            superToolTip55.MaxWidth = 210;
            this.PrintPreviewBarItem28.SuperTip = superToolTip55;
            // 
            // PrintPreviewBarItem29
            // 
            this.PrintPreviewBarItem29.Caption = "Text File";
            this.PrintPreviewBarItem29.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendTxt;
            this.PrintPreviewBarItem29.Description = "Plain Text";
            this.PrintPreviewBarItem29.Enabled = false;
            this.PrintPreviewBarItem29.Id = 92;
            this.PrintPreviewBarItem29.Name = "PrintPreviewBarItem29";
            superToolTip56.FixedTooltipWidth = true;
            toolTipTitleItem56.Text = "E-Mail As Text";
            toolTipItem56.LeftIndent = 6;
            toolTipItem56.Text = "Export the document to Text and attach it to the e-mail.";
            superToolTip56.Items.Add(toolTipTitleItem56);
            superToolTip56.Items.Add(toolTipItem56);
            superToolTip56.MaxWidth = 210;
            this.PrintPreviewBarItem29.SuperTip = superToolTip56;
            // 
            // PrintPreviewBarItem30
            // 
            this.PrintPreviewBarItem30.Caption = "CSV File";
            this.PrintPreviewBarItem30.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendCsv;
            this.PrintPreviewBarItem30.Description = "Comma-Separated Values Text";
            this.PrintPreviewBarItem30.Enabled = false;
            this.PrintPreviewBarItem30.Id = 93;
            this.PrintPreviewBarItem30.Name = "PrintPreviewBarItem30";
            superToolTip57.FixedTooltipWidth = true;
            toolTipTitleItem57.Text = "E-Mail As CSV";
            toolTipItem57.LeftIndent = 6;
            toolTipItem57.Text = "Export the document to CSV and attach it to the e-mail.";
            superToolTip57.Items.Add(toolTipTitleItem57);
            superToolTip57.Items.Add(toolTipItem57);
            superToolTip57.MaxWidth = 210;
            this.PrintPreviewBarItem30.SuperTip = superToolTip57;
            // 
            // PrintPreviewBarItem31
            // 
            this.PrintPreviewBarItem31.Caption = "MHT File";
            this.PrintPreviewBarItem31.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendMht;
            this.PrintPreviewBarItem31.Description = "Single File Web Page";
            this.PrintPreviewBarItem31.Enabled = false;
            this.PrintPreviewBarItem31.Id = 94;
            this.PrintPreviewBarItem31.Name = "PrintPreviewBarItem31";
            superToolTip58.FixedTooltipWidth = true;
            toolTipTitleItem58.Text = "E-Mail As MHT";
            toolTipItem58.LeftIndent = 6;
            toolTipItem58.Text = "Export the document to MHT and attach it to the e-mail.";
            superToolTip58.Items.Add(toolTipTitleItem58);
            superToolTip58.Items.Add(toolTipItem58);
            superToolTip58.MaxWidth = 210;
            this.PrintPreviewBarItem31.SuperTip = superToolTip58;
            // 
            // PrintPreviewBarItem32
            // 
            this.PrintPreviewBarItem32.Caption = "Excel File";
            this.PrintPreviewBarItem32.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendXls;
            this.PrintPreviewBarItem32.Description = "Microsoft Excel Workbook";
            this.PrintPreviewBarItem32.Enabled = false;
            this.PrintPreviewBarItem32.Id = 95;
            this.PrintPreviewBarItem32.Name = "PrintPreviewBarItem32";
            superToolTip59.FixedTooltipWidth = true;
            toolTipTitleItem59.Text = "E-Mail As XLS";
            toolTipItem59.LeftIndent = 6;
            toolTipItem59.Text = "Export the document to XLS and attach it to the e-mail.";
            superToolTip59.Items.Add(toolTipTitleItem59);
            superToolTip59.Items.Add(toolTipItem59);
            superToolTip59.MaxWidth = 210;
            this.PrintPreviewBarItem32.SuperTip = superToolTip59;
            // 
            // PrintPreviewBarItem33
            // 
            this.PrintPreviewBarItem33.Caption = "RTF File";
            this.PrintPreviewBarItem33.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendRtf;
            this.PrintPreviewBarItem33.Description = "Rich Text Format";
            this.PrintPreviewBarItem33.Enabled = false;
            this.PrintPreviewBarItem33.Id = 96;
            this.PrintPreviewBarItem33.Name = "PrintPreviewBarItem33";
            superToolTip60.FixedTooltipWidth = true;
            toolTipTitleItem60.Text = "E-Mail As RTF";
            toolTipItem60.LeftIndent = 6;
            toolTipItem60.Text = "Export the document to RTF and attach it to the e-mail.";
            superToolTip60.Items.Add(toolTipTitleItem60);
            superToolTip60.Items.Add(toolTipItem60);
            superToolTip60.MaxWidth = 210;
            this.PrintPreviewBarItem33.SuperTip = superToolTip60;
            // 
            // PrintPreviewBarItem34
            // 
            this.PrintPreviewBarItem34.Caption = "Image File";
            this.PrintPreviewBarItem34.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendGraphic;
            this.PrintPreviewBarItem34.Description = "BMP, GIF, JPEG, PNG, TIFF, EMF, WMF";
            this.PrintPreviewBarItem34.Enabled = false;
            this.PrintPreviewBarItem34.Id = 97;
            this.PrintPreviewBarItem34.Name = "PrintPreviewBarItem34";
            superToolTip61.FixedTooltipWidth = true;
            toolTipTitleItem61.Text = "E-Mail As Image";
            toolTipItem61.LeftIndent = 6;
            toolTipItem61.Text = "Export the document to Image and attach it to the e-mail.";
            superToolTip61.Items.Add(toolTipTitleItem61);
            superToolTip61.Items.Add(toolTipItem61);
            superToolTip61.MaxWidth = 210;
            this.PrintPreviewBarItem34.SuperTip = superToolTip61;
            // 
            // PrintPreviewBarItem35
            // 
            this.PrintPreviewBarItem35.Caption = "PDF File";
            this.PrintPreviewBarItem35.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportPdf;
            this.PrintPreviewBarItem35.Description = "Adobe Portable Document Format";
            this.PrintPreviewBarItem35.Enabled = false;
            this.PrintPreviewBarItem35.Id = 98;
            this.PrintPreviewBarItem35.Name = "PrintPreviewBarItem35";
            superToolTip62.FixedTooltipWidth = true;
            toolTipTitleItem62.Text = "Export to PDF";
            toolTipItem62.LeftIndent = 6;
            toolTipItem62.Text = "Export the document to PDF and save it to the file on a disk.";
            superToolTip62.Items.Add(toolTipTitleItem62);
            superToolTip62.Items.Add(toolTipItem62);
            superToolTip62.MaxWidth = 210;
            this.PrintPreviewBarItem35.SuperTip = superToolTip62;
            // 
            // PrintPreviewBarItem36
            // 
            this.PrintPreviewBarItem36.Caption = "HTML File";
            this.PrintPreviewBarItem36.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportHtm;
            this.PrintPreviewBarItem36.Description = "Web Page";
            this.PrintPreviewBarItem36.Enabled = false;
            this.PrintPreviewBarItem36.Id = 99;
            this.PrintPreviewBarItem36.Name = "PrintPreviewBarItem36";
            superToolTip63.FixedTooltipWidth = true;
            toolTipTitleItem63.Text = "Export to HTML";
            toolTipItem63.LeftIndent = 6;
            toolTipItem63.Text = "Export the document to HTML and save it to the file on a disk.";
            superToolTip63.Items.Add(toolTipTitleItem63);
            superToolTip63.Items.Add(toolTipItem63);
            superToolTip63.MaxWidth = 210;
            this.PrintPreviewBarItem36.SuperTip = superToolTip63;
            // 
            // PrintPreviewBarItem37
            // 
            this.PrintPreviewBarItem37.Caption = "Text File";
            this.PrintPreviewBarItem37.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportTxt;
            this.PrintPreviewBarItem37.Description = "Plain Text";
            this.PrintPreviewBarItem37.Enabled = false;
            this.PrintPreviewBarItem37.Id = 100;
            this.PrintPreviewBarItem37.Name = "PrintPreviewBarItem37";
            superToolTip64.FixedTooltipWidth = true;
            toolTipTitleItem64.Text = "Export to Text";
            toolTipItem64.LeftIndent = 6;
            toolTipItem64.Text = "Export the document to Text and save it to the file on a disk.";
            superToolTip64.Items.Add(toolTipTitleItem64);
            superToolTip64.Items.Add(toolTipItem64);
            superToolTip64.MaxWidth = 210;
            this.PrintPreviewBarItem37.SuperTip = superToolTip64;
            // 
            // PrintPreviewBarItem38
            // 
            this.PrintPreviewBarItem38.Caption = "CSV File";
            this.PrintPreviewBarItem38.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportCsv;
            this.PrintPreviewBarItem38.Description = "Comma-Separated Values Text";
            this.PrintPreviewBarItem38.Enabled = false;
            this.PrintPreviewBarItem38.Id = 101;
            this.PrintPreviewBarItem38.Name = "PrintPreviewBarItem38";
            superToolTip65.FixedTooltipWidth = true;
            toolTipTitleItem65.Text = "Export to CSV";
            toolTipItem65.LeftIndent = 6;
            toolTipItem65.Text = "Export the document to CSV and save it to the file on a disk.";
            superToolTip65.Items.Add(toolTipTitleItem65);
            superToolTip65.Items.Add(toolTipItem65);
            superToolTip65.MaxWidth = 210;
            this.PrintPreviewBarItem38.SuperTip = superToolTip65;
            // 
            // PrintPreviewBarItem39
            // 
            this.PrintPreviewBarItem39.Caption = "MHT File";
            this.PrintPreviewBarItem39.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportMht;
            this.PrintPreviewBarItem39.Description = "Single File Web Page";
            this.PrintPreviewBarItem39.Enabled = false;
            this.PrintPreviewBarItem39.Id = 102;
            this.PrintPreviewBarItem39.Name = "PrintPreviewBarItem39";
            superToolTip66.FixedTooltipWidth = true;
            toolTipTitleItem66.Text = "Export to MHT";
            toolTipItem66.LeftIndent = 6;
            toolTipItem66.Text = "Export the document to MHT and save it to the file on a disk.";
            superToolTip66.Items.Add(toolTipTitleItem66);
            superToolTip66.Items.Add(toolTipItem66);
            superToolTip66.MaxWidth = 210;
            this.PrintPreviewBarItem39.SuperTip = superToolTip66;
            // 
            // PrintPreviewBarItem40
            // 
            this.PrintPreviewBarItem40.Caption = "Excel File";
            this.PrintPreviewBarItem40.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportXls;
            this.PrintPreviewBarItem40.Description = "Microsoft Excel Workbook";
            this.PrintPreviewBarItem40.Enabled = false;
            this.PrintPreviewBarItem40.Id = 103;
            this.PrintPreviewBarItem40.Name = "PrintPreviewBarItem40";
            superToolTip67.FixedTooltipWidth = true;
            toolTipTitleItem67.Text = "Export to XLS";
            toolTipItem67.LeftIndent = 6;
            toolTipItem67.Text = "Export the document to XLS and save it to the file on a disk.";
            superToolTip67.Items.Add(toolTipTitleItem67);
            superToolTip67.Items.Add(toolTipItem67);
            superToolTip67.MaxWidth = 210;
            this.PrintPreviewBarItem40.SuperTip = superToolTip67;
            // 
            // PrintPreviewBarItem41
            // 
            this.PrintPreviewBarItem41.Caption = "RTF File";
            this.PrintPreviewBarItem41.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportRtf;
            this.PrintPreviewBarItem41.Description = "Rich Text Format";
            this.PrintPreviewBarItem41.Enabled = false;
            this.PrintPreviewBarItem41.Id = 104;
            this.PrintPreviewBarItem41.Name = "PrintPreviewBarItem41";
            superToolTip68.FixedTooltipWidth = true;
            toolTipTitleItem68.Text = "Export to RTF";
            toolTipItem68.LeftIndent = 6;
            toolTipItem68.Text = "Export the document to RTF and save it to the file on a disk.";
            superToolTip68.Items.Add(toolTipTitleItem68);
            superToolTip68.Items.Add(toolTipItem68);
            superToolTip68.MaxWidth = 210;
            this.PrintPreviewBarItem41.SuperTip = superToolTip68;
            // 
            // PrintPreviewBarItem42
            // 
            this.PrintPreviewBarItem42.Caption = "Image File";
            this.PrintPreviewBarItem42.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportGraphic;
            this.PrintPreviewBarItem42.Description = "BMP, GIF, JPEG, PNG, TIFF, EMF, WMF";
            this.PrintPreviewBarItem42.Enabled = false;
            this.PrintPreviewBarItem42.Id = 105;
            this.PrintPreviewBarItem42.Name = "PrintPreviewBarItem42";
            superToolTip69.FixedTooltipWidth = true;
            toolTipTitleItem69.Text = "Export to Image";
            toolTipItem69.LeftIndent = 6;
            toolTipItem69.Text = "Export the document to Image and save it to the file on a disk.";
            superToolTip69.Items.Add(toolTipTitleItem69);
            superToolTip69.Items.Add(toolTipItem69);
            superToolTip69.MaxWidth = 210;
            this.PrintPreviewBarItem42.SuperTip = superToolTip69;
            // 
            // barItemNotificationDistribution
            // 
            this.barItemNotificationDistribution.Caption = "Distribucin de solicitudes";
            this.barItemNotificationDistribution.Glyph = ((System.Drawing.Image)(resources.GetObject("barItemNotificationDistribution.Glyph")));
            this.barItemNotificationDistribution.Id = 106;
            this.barItemNotificationDistribution.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.B));
            this.barItemNotificationDistribution.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barItemNotificationDistribution.LargeGlyph")));
            this.barItemNotificationDistribution.Name = "barItemNotificationDistribution";
            this.barItemNotificationDistribution.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.PrintPreviewBarNotificationDistribution_ItemClick);
            // 
            // PrintPreviewStaticItem1
            // 
            this.PrintPreviewStaticItem1.Caption = "Current Page: none";
            this.PrintPreviewStaticItem1.Id = 108;
            this.PrintPreviewStaticItem1.LeftIndent = 1;
            this.PrintPreviewStaticItem1.Name = "PrintPreviewStaticItem1";
            this.PrintPreviewStaticItem1.RightIndent = 1;
            this.PrintPreviewStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            this.PrintPreviewStaticItem1.Type = "CurrentPageNo";
            // 
            // PrintPreviewStaticItem2
            // 
            this.PrintPreviewStaticItem2.Caption = "Total Pages: 0";
            this.PrintPreviewStaticItem2.Id = 109;
            this.PrintPreviewStaticItem2.LeftIndent = 1;
            this.PrintPreviewStaticItem2.Name = "PrintPreviewStaticItem2";
            this.PrintPreviewStaticItem2.RightIndent = 1;
            this.PrintPreviewStaticItem2.TextAlignment = System.Drawing.StringAlignment.Near;
            this.PrintPreviewStaticItem2.Type = "TotalPageNo";
            // 
            // PrintPreviewStaticItem3
            // 
            this.PrintPreviewStaticItem3.Caption = "Zoom Factor: 100%";
            this.PrintPreviewStaticItem3.Id = 110;
            this.PrintPreviewStaticItem3.LeftIndent = 1;
            this.PrintPreviewStaticItem3.Name = "PrintPreviewStaticItem3";
            this.PrintPreviewStaticItem3.RightIndent = 1;
            this.PrintPreviewStaticItem3.TextAlignment = System.Drawing.StringAlignment.Near;
            this.PrintPreviewStaticItem3.Type = "ZoomFactor";
            // 
            // CommandBar
            // 
            this.CommandBar.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.CommandBar.Caption = "TESTE";
            this.CommandBar.Glyph = ((System.Drawing.Image)(resources.GetObject("CommandBar.Glyph")));
            this.CommandBar.Id = 117;
            this.CommandBar.Name = "CommandBar";
            // 
            // barButtonItemOperatorActivities
            // 
            this.barButtonItemOperatorActivities.Caption = "Ver actividades";
            this.barButtonItemOperatorActivities.Id = 119;
            this.barButtonItemOperatorActivities.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A));
            this.barButtonItemOperatorActivities.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemOperatorActivities.LargeGlyph")));
            this.barButtonItemOperatorActivities.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemOperatorActivities.Name = "barButtonItemOperatorActivities";
            toolTipTitleItem70.Text = "Actividades de los Operadores";
            toolTipItem70.LeftIndent = 6;
            toolTipItem70.Text = "Permite visualizar las actividades de los operadores conectados.";
            superToolTip70.Items.Add(toolTipTitleItem70);
            superToolTip70.Items.Add(toolTipItem70);
            this.barButtonItemOperatorActivities.SuperTip = superToolTip70;
            this.barButtonItemOperatorActivities.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BarButtonItem2_ItemClick);
            // 
            // BarEditItem4
            // 
            this.BarEditItem4.Caption = "BarEditItem4";
            this.BarEditItem4.Edit = null;
            this.BarEditItem4.Id = 122;
            this.BarEditItem4.Name = "BarEditItem4";
            // 
            // BarEditItem6
            // 
            this.BarEditItem6.Caption = "Fecha";
            this.BarEditItem6.Edit = null;
            this.BarEditItem6.Enabled = false;
            this.BarEditItem6.Id = 124;
            this.BarEditItem6.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("BarEditItem6.LargeGlyph")));
            this.BarEditItem6.Name = "BarEditItem6";
            toolTipTitleItem71.Text = "Fecha en la que se desea ver las actividades de los operadores";
            superToolTip71.Items.Add(toolTipTitleItem71);
            this.BarEditItem6.SuperTip = superToolTip71;
            this.BarEditItem6.VisibleWhenVertical = true;
            // 
            // BarButtonItem3
            // 
            this.BarButtonItem3.Caption = "Imprimir";
            this.BarButtonItem3.Id = 126;
            this.BarButtonItem3.Name = "BarButtonItem3";
            this.BarButtonItem3.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            toolTipTitleItem72.Text = "Imprimir";
            superToolTip72.Items.Add(toolTipTitleItem72);
            this.BarButtonItem3.SuperTip = superToolTip72;
            // 
            // BarButtonItemSendMonitor
            // 
            this.BarButtonItemSendMonitor.DropDownEnabled = false;
            this.BarButtonItemSendMonitor.Enabled = false;
            this.BarButtonItemSendMonitor.Glyph = ((System.Drawing.Image)(resources.GetObject("BarButtonItemSendMonitor.Glyph")));
            this.BarButtonItemSendMonitor.Id = 127;
            this.BarButtonItemSendMonitor.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.BarButtonItemSendMonitor.Name = "BarButtonItemSendMonitor";
            this.BarButtonItemSendMonitor.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // BarListItem2
            // 
            this.BarListItem2.Caption = "BarListItem2";
            this.BarListItem2.Id = 133;
            this.BarListItem2.Name = "BarListItem2";
            // 
            // BarButtonItem5
            // 
            this.BarButtonItem5.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.BarButtonItem5.Caption = "Ubicar";
            this.BarButtonItem5.Id = 139;
            this.BarButtonItem5.Name = "BarButtonItem5";
            toolTipTitleItem73.Text = "(Ctrl+U) Ubicar";
            toolTipItem71.LeftIndent = 6;
            toolTipItem71.Text = "Permite ubicar un operar en la sala.";
            superToolTip73.Items.Add(toolTipTitleItem73);
            superToolTip73.Items.Add(toolTipItem71);
            this.BarButtonItem5.SuperTip = superToolTip73;
            // 
            // BarButtonGroup1
            // 
            this.BarButtonGroup1.Caption = "BarButtonGroup1";
            this.BarButtonGroup1.Id = 141;
            this.BarButtonGroup1.ItemLinks.Add(this.BarCheckItem2);
            this.BarButtonGroup1.ItemLinks.Add(this.BarCheckItem3);
            this.BarButtonGroup1.ItemLinks.Add(this.BarCheckItem4);
            this.BarButtonGroup1.Name = "BarButtonGroup1";
            // 
            // BarCheckItem2
            // 
            this.BarCheckItem2.Caption = "Todos";
            this.BarCheckItem2.Id = 142;
            this.BarCheckItem2.MergeType = DevExpress.XtraBars.BarMenuMerge.MergeItems;
            this.BarCheckItem2.Name = "BarCheckItem2";
            // 
            // BarCheckItem3
            // 
            this.BarCheckItem3.Caption = "Conectados";
            this.BarCheckItem3.Enabled = false;
            this.BarCheckItem3.Id = 143;
            this.BarCheckItem3.Name = "BarCheckItem3";
            // 
            // BarCheckItem4
            // 
            this.BarCheckItem4.Caption = "BarCheckItem4";
            this.BarCheckItem4.Id = 144;
            this.BarCheckItem4.Name = "BarCheckItem4";
            // 
            // BarButtonItemSelectRoom
            // 
            this.BarButtonItemSelectRoom.Caption = "Ver sala";
            this.BarButtonItemSelectRoom.Id = 145;
            this.BarButtonItemSelectRoom.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.K));
            this.BarButtonItemSelectRoom.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("BarButtonItemSelectRoom.LargeGlyph")));
            this.BarButtonItemSelectRoom.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.BarButtonItemSelectRoom.Name = "BarButtonItemSelectRoom";
            this.BarButtonItemSelectRoom.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BarButtonItemSelectRoom_ItemClick);
            // 
            // barButtonItemForecast
            // 
            this.barButtonItemForecast.Caption = "Pronsticos";
            this.barButtonItemForecast.Id = 150;
            this.barButtonItemForecast.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O));
            this.barButtonItemForecast.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemForecast.LargeGlyph")));
            this.barButtonItemForecast.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemForecast.Name = "barButtonItemForecast";
            this.barButtonItemForecast.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemForecast_ItemClick);
            // 
            // barButtonItemAddForecast
            // 
            this.barButtonItemAddForecast.Caption = "Crear";
            this.barButtonItemAddForecast.Enabled = false;
            this.barButtonItemAddForecast.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemAddForecast.Glyph")));
            this.barButtonItemAddForecast.Id = 151;
            this.barButtonItemAddForecast.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemAddForecast.Name = "barButtonItemAddForecast";
            this.barButtonItemAddForecast.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barButtonItemDeleteForecast
            // 
            this.barButtonItemDeleteForecast.Caption = "Eliminar";
            this.barButtonItemDeleteForecast.Enabled = false;
            this.barButtonItemDeleteForecast.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemDeleteForecast.Glyph")));
            this.barButtonItemDeleteForecast.Id = 152;
            this.barButtonItemDeleteForecast.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemDeleteForecast.Name = "barButtonItemDeleteForecast";
            this.barButtonItemDeleteForecast.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barButtonItemUpdateForecast
            // 
            this.barButtonItemUpdateForecast.Caption = "Modificar";
            this.barButtonItemUpdateForecast.Enabled = false;
            this.barButtonItemUpdateForecast.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemUpdateForecast.Glyph")));
            this.barButtonItemUpdateForecast.Id = 153;
            this.barButtonItemUpdateForecast.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemUpdateForecast.Name = "barButtonItemUpdateForecast";
            this.barButtonItemUpdateForecast.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barButtonItemDuplicate
            // 
            this.barButtonItemDuplicate.Caption = "Duplicar";
            this.barButtonItemDuplicate.Enabled = false;
            this.barButtonItemDuplicate.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemDuplicate.Glyph")));
            this.barButtonItemDuplicate.Id = 154;
            this.barButtonItemDuplicate.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemDuplicate.Name = "barButtonItemDuplicate";
            // 
            // barButtonItemReplace
            // 
            this.barButtonItemReplace.Caption = "Reemplazar";
            this.barButtonItemReplace.Enabled = false;
            this.barButtonItemReplace.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemReplace.Glyph")));
            this.barButtonItemReplace.Id = 155;
            this.barButtonItemReplace.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemReplace.Name = "barButtonItemReplace";
            this.barButtonItemReplace.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barButtonItemFinalize
            // 
            this.barButtonItemFinalize.Caption = "Finalizar";
            this.barButtonItemFinalize.Enabled = false;
            this.barButtonItemFinalize.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemFinalize.Glyph")));
            this.barButtonItemFinalize.Id = 156;
            this.barButtonItemFinalize.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemFinalize.Name = "barButtonItemFinalize";
            this.barButtonItemFinalize.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // BarButtonItemHistoricGraphic
            // 
            this.BarButtonItemHistoricGraphic.Caption = "Grficas histricas";
            this.BarButtonItemHistoricGraphic.Enabled = false;
            this.BarButtonItemHistoricGraphic.Id = 157;
            this.BarButtonItemHistoricGraphic.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("BarButtonItemHistoricGraphic.LargeGlyph")));
            this.BarButtonItemHistoricGraphic.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.BarButtonItemHistoricGraphic.Name = "BarButtonItemHistoricGraphic";
            toolTipTitleItem74.Text = "Grficas Histricas";
            toolTipItem72.LeftIndent = 6;
            toolTipItem72.Text = "Permite visualizar un histrico de las grficas de todos los indicadores.";
            superToolTip74.Items.Add(toolTipTitleItem74);
            superToolTip74.Items.Add(toolTipItem72);
            this.BarButtonItemHistoricGraphic.SuperTip = superToolTip74;
            this.BarButtonItemHistoricGraphic.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // BarButtonItemHistoryDetails
            // 
            this.BarButtonItemHistoryDetails.Caption = "Detalles histricos";
            this.BarButtonItemHistoryDetails.Enabled = false;
            this.BarButtonItemHistoryDetails.Id = 158;
            this.BarButtonItemHistoryDetails.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("BarButtonItemHistoryDetails.LargeGlyph")));
            this.BarButtonItemHistoryDetails.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.BarButtonItemHistoryDetails.Name = "BarButtonItemHistoryDetails";
            toolTipTitleItem75.Text = "Detalles Histricos";
            toolTipItem73.LeftIndent = 6;
            toolTipItem73.Text = "Permite visualizar un histrico de los detalles de los indicadores de desempeo.";
            superToolTip75.Items.Add(toolTipTitleItem75);
            superToolTip75.Items.Add(toolTipItem73);
            this.BarButtonItemHistoryDetails.SuperTip = superToolTip75;
            this.BarButtonItemHistoryDetails.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.BarButtonItemHistoryDetails.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BarButtonItemHistoryDetails_ItemClick);
            // 
            // BarButtonItem17
            // 
            this.BarButtonItem17.Caption = "Imprimir";
            this.BarButtonItem17.Id = 160;
            this.BarButtonItem17.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.G));
            this.BarButtonItem17.Name = "BarButtonItem17";
            this.BarButtonItem17.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            toolTipTitleItem76.Text = "(Ctrl+G)\r\nGuardar";
            superToolTip76.Items.Add(toolTipTitleItem76);
            this.BarButtonItem17.SuperTip = superToolTip76;
            // 
            // BarButtonItem18
            // 
            this.BarButtonItem18.Caption = "Preferencias de Visualizacin";
            this.BarButtonItem18.Id = 161;
            this.BarButtonItem18.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("BarButtonItem18.LargeGlyph")));
            this.BarButtonItem18.Name = "BarButtonItem18";
            toolTipTitleItem77.Text = "Preferencias de Visualizacin";
            toolTipItem74.LeftIndent = 6;
            toolTipItem74.Text = "Permite hacer cambios en las preferencias de visualizacin de los Indicadores de D" +
    "esempeo del Dashboard o a nivel de Indicadores de Desempeo.";
            superToolTip77.Items.Add(toolTipTitleItem77);
            superToolTip77.Items.Add(toolTipItem74);
            this.BarButtonItem18.SuperTip = superToolTip77;
            // 
            // BarEditItem7
            // 
            this.BarEditItem7.Caption = "Leyenda";
            this.BarEditItem7.Edit = null;
            this.BarEditItem7.Enabled = false;
            this.BarEditItem7.Id = 167;
            this.BarEditItem7.Name = "BarEditItem7";
            this.BarEditItem7.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            toolTipTitleItem78.Text = "Ver Leyenda";
            toolTipItem75.LeftIndent = 6;
            toolTipItem75.Text = "Habilitado permite ver la leyenda en las salas.";
            superToolTip78.Items.Add(toolTipTitleItem78);
            superToolTip78.Items.Add(toolTipItem75);
            this.BarEditItem7.SuperTip = superToolTip78;
            this.BarEditItem7.VisibleWhenVertical = true;
            this.BarEditItem7.Width = 10;
            // 
            // barButtonItemEvaluateOperator
            // 
            this.barButtonItemEvaluateOperator.Caption = "Evaluar operador";
            this.barButtonItemEvaluateOperator.Enabled = false;
            this.barButtonItemEvaluateOperator.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemEvaluateOperator.Glyph")));
            this.barButtonItemEvaluateOperator.Id = 170;
            this.barButtonItemEvaluateOperator.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemEvaluateOperator.Name = "barButtonItemEvaluateOperator";
            this.barButtonItemEvaluateOperator.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemEvaluateOperator.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BarButtonItemEvaluateOperator_ItemClick);
            // 
            // BarSubItem16
            // 
            this.BarSubItem16.Caption = "First Level";
            this.BarSubItem16.Id = 177;
            this.BarSubItem16.Name = "BarSubItem16";
            // 
            // BarSubItem17
            // 
            this.BarSubItem17.Caption = "Dispatch";
            this.BarSubItem17.Id = 178;
            this.BarSubItem17.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.BarStaticItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.BarStaticItem3),
            new DevExpress.XtraBars.LinkPersistInfo(this.BarStaticItem4)});
            this.BarSubItem17.Name = "BarSubItem17";
            // 
            // BarStaticItem2
            // 
            this.BarStaticItem2.Caption = "Bomberos";
            this.BarStaticItem2.Id = 180;
            this.BarStaticItem2.Name = "BarStaticItem2";
            this.BarStaticItem2.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // BarStaticItem3
            // 
            this.BarStaticItem3.Caption = "Policia Metropolitana";
            this.BarStaticItem3.Id = 181;
            this.BarStaticItem3.Name = "BarStaticItem3";
            this.BarStaticItem3.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // BarStaticItem4
            // 
            this.BarStaticItem4.Caption = "Proteccin Civil";
            this.BarStaticItem4.Id = 182;
            this.BarStaticItem4.Name = "BarStaticItem4";
            this.BarStaticItem4.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // BarButtonItem25
            // 
            this.BarButtonItem25.Caption = "Crear";
            this.BarButtonItem25.Id = 183;
            this.BarButtonItem25.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("BarButtonItem25.LargeGlyph")));
            this.BarButtonItem25.Name = "BarButtonItem25";
            toolTipTitleItem79.Text = "Crear un turno de trabajo";
            toolTipItem76.LeftIndent = 6;
            toolTipItem76.Text = "Seleccione para crear un nuevo turno de trabajo";
            superToolTip79.Items.Add(toolTipTitleItem79);
            superToolTip79.Items.Add(toolTipItem76);
            this.BarButtonItem25.SuperTip = superToolTip79;
            // 
            // BarButtonItem26
            // 
            this.BarButtonItem26.Caption = "Eliminar";
            this.BarButtonItem26.Id = 184;
            this.BarButtonItem26.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("BarButtonItem26.LargeGlyph")));
            this.BarButtonItem26.Name = "BarButtonItem26";
            toolTipTitleItem80.Text = "Eliminar turnos de trabajo";
            toolTipItem77.LeftIndent = 6;
            toolTipItem77.Text = "Seleccione para eliminar algn turno de trabajo.";
            superToolTip80.Items.Add(toolTipTitleItem80);
            superToolTip80.Items.Add(toolTipItem77);
            this.BarButtonItem26.SuperTip = superToolTip80;
            // 
            // BarButtonItem27
            // 
            this.BarButtonItem27.Caption = "Duplicar";
            this.BarButtonItem27.Id = 185;
            this.BarButtonItem27.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("BarButtonItem27.LargeGlyph")));
            this.BarButtonItem27.Name = "BarButtonItem27";
            toolTipTitleItem81.Text = "Duplicar turnos de trabajo";
            toolTipItem78.LeftIndent = 6;
            toolTipItem78.Text = "Seleccione para duplicar turnos de trabajo.";
            superToolTip81.Items.Add(toolTipTitleItem81);
            superToolTip81.Items.Add(toolTipItem78);
            this.BarButtonItem27.SuperTip = superToolTip81;
            // 
            // BarButtonItem28
            // 
            this.BarButtonItem28.Caption = "Pausas Programada";
            this.BarButtonItem28.Id = 186;
            this.BarButtonItem28.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("BarButtonItem28.LargeGlyph")));
            this.BarButtonItem28.Name = "BarButtonItem28";
            toolTipTitleItem82.Text = "Consultar Pausas Programadas";
            toolTipItem79.LeftIndent = 6;
            toolTipItem79.Text = "Seleccione un turno de trabajo para consultar su pausa programada.";
            superToolTip82.Items.Add(toolTipTitleItem82);
            superToolTip82.Items.Add(toolTipItem79);
            this.BarButtonItem28.SuperTip = superToolTip82;
            // 
            // BarButtonItem29
            // 
            this.BarButtonItem29.Caption = "Asignar Turnos";
            this.BarButtonItem29.Id = 187;
            this.BarButtonItem29.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("BarButtonItem29.LargeGlyph")));
            this.BarButtonItem29.Name = "BarButtonItem29";
            // 
            // BarButtonItem30
            // 
            this.BarButtonItem30.Caption = "Asignar ";
            this.BarButtonItem30.Id = 188;
            this.BarButtonItem30.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("BarButtonItem30.LargeGlyph")));
            this.BarButtonItem30.Name = "BarButtonItem30";
            toolTipTitleItem83.Text = "Asignar personal";
            toolTipItem80.LeftIndent = 6;
            toolTipItem80.Text = "Seleccione para asignar operadores a supervisiores.";
            superToolTip83.Items.Add(toolTipTitleItem83);
            superToolTip83.Items.Add(toolTipItem80);
            this.BarButtonItem30.SuperTip = superToolTip83;
            // 
            // BarButtonItem31
            // 
            this.BarButtonItem31.Caption = "Remover ";
            this.BarButtonItem31.Id = 189;
            this.BarButtonItem31.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("BarButtonItem31.LargeGlyph")));
            this.BarButtonItem31.Name = "BarButtonItem31";
            toolTipTitleItem84.Text = "Remover personal";
            toolTipItem81.LeftIndent = 6;
            toolTipItem81.Text = "Seleccione para remover personal a un supervisor.";
            superToolTip84.Items.Add(toolTipTitleItem84);
            superToolTip84.Items.Add(toolTipItem81);
            this.BarButtonItem31.SuperTip = superToolTip84;
            // 
            // BarButtonItem32
            // 
            this.BarButtonItem32.Caption = "Consultar Programa";
            this.BarButtonItem32.Id = 190;
            this.BarButtonItem32.Name = "BarButtonItem32";
            // 
            // BarButtonItem33
            // 
            this.BarButtonItem33.Caption = "Modificar";
            this.BarButtonItem33.Id = 191;
            this.BarButtonItem33.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("BarButtonItem33.LargeGlyph")));
            this.BarButtonItem33.Name = "BarButtonItem33";
            // 
            // BarButtonItem34
            // 
            this.BarButtonItem34.Caption = "Crear";
            this.BarButtonItem34.Id = 192;
            this.BarButtonItem34.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("BarButtonItem34.LargeGlyph")));
            this.BarButtonItem34.Name = "BarButtonItem34";
            toolTipTitleItem85.Text = "Crear Evaluaciones";
            toolTipItem82.LeftIndent = 6;
            toolTipItem82.Text = "Permite crear evaluaciones.";
            superToolTip85.Items.Add(toolTipTitleItem85);
            superToolTip85.Items.Add(toolTipItem82);
            this.BarButtonItem34.SuperTip = superToolTip85;
            // 
            // BarButtonItem35
            // 
            this.BarButtonItem35.Caption = "Crear";
            this.BarButtonItem35.Enabled = false;
            this.BarButtonItem35.Id = 193;
            this.BarButtonItem35.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("BarButtonItem35.LargeGlyph")));
            this.BarButtonItem35.Name = "BarButtonItem35";
            // 
            // BarButtonItem36
            // 
            this.BarButtonItem36.Caption = "Eliminar";
            this.BarButtonItem36.Enabled = false;
            this.BarButtonItem36.Id = 194;
            this.BarButtonItem36.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("BarButtonItem36.LargeGlyph")));
            this.BarButtonItem36.Name = "BarButtonItem36";
            // 
            // BarButtonItem37
            // 
            this.BarButtonItem37.Caption = "Duplicar";
            this.BarButtonItem37.Enabled = false;
            this.BarButtonItem37.Id = 195;
            this.BarButtonItem37.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("BarButtonItem37.LargeGlyph")));
            this.BarButtonItem37.Name = "BarButtonItem37";
            // 
            // BarButtonItem38
            // 
            this.BarButtonItem38.Caption = "Pausas Programadas";
            this.BarButtonItem38.Enabled = false;
            this.BarButtonItem38.Id = 196;
            this.BarButtonItem38.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("BarButtonItem38.LargeGlyph")));
            this.BarButtonItem38.Name = "BarButtonItem38";
            // 
            // BarButtonItem39
            // 
            this.BarButtonItem39.Caption = "Asignar operadores";
            this.BarButtonItem39.Enabled = false;
            this.BarButtonItem39.Id = 197;
            this.BarButtonItem39.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("BarButtonItem39.LargeGlyph")));
            this.BarButtonItem39.Name = "BarButtonItem39";
            // 
            // BarButtonItem40
            // 
            this.BarButtonItem40.Caption = "Modificar Pausas";
            this.BarButtonItem40.Enabled = false;
            this.BarButtonItem40.Id = 198;
            this.BarButtonItem40.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("BarButtonItem40.LargeGlyph")));
            this.BarButtonItem40.Name = "BarButtonItem40";
            // 
            // BarButtonItem41
            // 
            this.BarButtonItem41.Caption = "Crear Evaluaciones";
            this.BarButtonItem41.Enabled = false;
            this.BarButtonItem41.Id = 199;
            this.BarButtonItem41.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("BarButtonItem41.LargeGlyph")));
            this.BarButtonItem41.Name = "BarButtonItem41";
            // 
            // barButtonItemExit
            // 
            this.barButtonItemExit.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barButtonItemExit.AllowAllUp = true;
            this.barButtonItemExit.Caption = "barButtonItem19";
            this.barButtonItemExit.Id = 201;
            this.barButtonItemExit.ImageIndex = 0;
            this.barButtonItemExit.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4));
            this.barButtonItemExit.Name = "barButtonItemExit";
            this.barButtonItemExit.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            toolTipTitleItem86.Text = "Salir";
            superToolTip86.Items.Add(toolTipTitleItem86);
            this.barButtonItemExit.SuperTip = superToolTip86;
            this.barButtonItemExit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemExit_ItemClick);
            // 
            // barButtonGroup2
            // 
            this.barButtonGroup2.Caption = "barButtonGroup2";
            this.barButtonGroup2.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonGroup2.Glyph")));
            this.barButtonGroup2.Id = 202;
            this.barButtonGroup2.Name = "barButtonGroup2";
            // 
            // barButtonItemOperatorAssign
            // 
            this.barButtonItemOperatorAssign.Caption = "Asignacin por turnos";
            this.barButtonItemOperatorAssign.Id = 208;
            this.barButtonItemOperatorAssign.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.J));
            this.barButtonItemOperatorAssign.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemOperatorAssign.LargeGlyph")));
            this.barButtonItemOperatorAssign.Name = "barButtonItemOperatorAssign";
            this.barButtonItemOperatorAssign.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemOperatorAssign.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemOperatorAssign_ItemClick);
            // 
            // barButtonItemConfigureIndicators
            // 
            this.barButtonItemConfigureIndicators.Caption = "Preferencias";
            this.barButtonItemConfigureIndicators.Enabled = false;
            this.barButtonItemConfigureIndicators.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemConfigureIndicators.Glyph")));
            this.barButtonItemConfigureIndicators.Id = 210;
            this.barButtonItemConfigureIndicators.Name = "barButtonItemConfigureIndicators";
            this.barButtonItemConfigureIndicators.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemConfigureIndicators.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barButtonItemOperAffectIndicators
            // 
            this.barButtonItemOperAffectIndicators.Caption = "Detalles";
            this.barButtonItemOperAffectIndicators.Enabled = false;
            this.barButtonItemOperAffectIndicators.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemOperAffectIndicators.Glyph")));
            this.barButtonItemOperAffectIndicators.Id = 211;
            this.barButtonItemOperAffectIndicators.Name = "barButtonItemOperAffectIndicators";
            this.barButtonItemOperAffectIndicators.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barButtonItemCourses
            // 
            this.barButtonItemCourses.Caption = "Cursos";
            this.barButtonItemCourses.Id = 219;
            this.barButtonItemCourses.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.U));
            this.barButtonItemCourses.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemCourses.LargeGlyph")));
            this.barButtonItemCourses.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemCourses.Name = "barButtonItemCourses";
            this.barButtonItemCourses.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemCourses_ItemClick);
            // 
            // barButtonItemModifyCourses
            // 
            this.barButtonItemModifyCourses.Caption = "Modificar";
            this.barButtonItemModifyCourses.Enabled = false;
            this.barButtonItemModifyCourses.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemModifyCourses.Glyph")));
            this.barButtonItemModifyCourses.Id = 220;
            this.barButtonItemModifyCourses.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemModifyCourses.Name = "barButtonItemModifyCourses";
            this.barButtonItemModifyCourses.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barButtonItemReport
            // 
            this.barButtonItemReport.Caption = "Elaborar reporte";
            this.barButtonItemReport.Id = 221;
            this.barButtonItemReport.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.T));
            this.barButtonItemReport.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemReport.LargeGlyph")));
            this.barButtonItemReport.Name = "barButtonItemReport";
            this.barButtonItemReport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemReport_ItemClick);
            // 
            // barButtonItemEndReport
            // 
            this.barButtonItemEndReport.Caption = "Finalizar";
            this.barButtonItemEndReport.Enabled = false;
            this.barButtonItemEndReport.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemEndReport.Glyph")));
            this.barButtonItemEndReport.Id = 222;
            this.barButtonItemEndReport.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemEndReport.Name = "barButtonItemEndReport";
            this.barButtonItemEndReport.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barButtonItemRedo
            // 
            this.barButtonItemRedo.Caption = "Rehacer";
            this.barButtonItemRedo.Enabled = false;
            this.barButtonItemRedo.Glyph = global::SmartCadGuiCommon.XRDesignRibbonControllerResources.RibbonUserDesigner_Redo;
            this.barButtonItemRedo.Id = 223;
            this.barButtonItemRedo.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y));
            this.barButtonItemRedo.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemRedo.Name = "barButtonItemRedo";
            this.barButtonItemRedo.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barButtonItemUndo
            // 
            this.barButtonItemUndo.Caption = "Deshacer";
            this.barButtonItemUndo.Enabled = false;
            this.barButtonItemUndo.Glyph = global::SmartCadGuiCommon.XRDesignRibbonControllerResources.RibbonUserDesigner_Undo;
            this.barButtonItemUndo.Id = 224;
            this.barButtonItemUndo.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z));
            this.barButtonItemUndo.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemUndo.Name = "barButtonItemUndo";
            this.barButtonItemUndo.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barButtonItemPreferences
            // 
            this.barButtonItemPreferences.Caption = "Panel de indicadores";
            this.barButtonItemPreferences.Id = 225;
            this.barButtonItemPreferences.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L));
            this.barButtonItemPreferences.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPreferences.LargeGlyph")));
            this.barButtonItemPreferences.Name = "barButtonItemPreferences";
            this.barButtonItemPreferences.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPreferences_ItemClick);
            // 
            // barMdiChildrenListItemSelect
            // 
            this.barMdiChildrenListItemSelect.Caption = "Seleccionar";
            this.barMdiChildrenListItemSelect.Id = 227;
            this.barMdiChildrenListItemSelect.LargeGlyph = global::SmartCadGuiCommon.PrintRibbonControllerResources.RibbonPrintPreview_ClosePreviewLarge;
            this.barMdiChildrenListItemSelect.Name = "barMdiChildrenListItemSelect";
            // 
            // barButtonItemUpdate
            // 
            this.barButtonItemUpdate.Enabled = false;
            this.barButtonItemUpdate.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemUpdate.Glyph")));
            this.barButtonItemUpdate.Id = 228;
            this.barButtonItemUpdate.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F5);
            this.barButtonItemUpdate.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemUpdate.Name = "barButtonItemUpdate";
            this.barButtonItemUpdate.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barButtonItemSend
            // 
            this.barButtonItemSend.Enabled = false;
            this.barButtonItemSend.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemSend.Glyph")));
            this.barButtonItemSend.Id = 229;
            this.barButtonItemSend.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Enter));
            this.barButtonItemSend.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemSend.Name = "barButtonItemSend";
            this.barButtonItemSend.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barButtonItemSave
            // 
            this.barButtonItemSave.Enabled = false;
            this.barButtonItemSave.Glyph = global::SmartCadGuiCommon.PrintRibbonControllerResources.RibbonPrintPreview_Save;
            this.barButtonItemSave.Id = 230;
            this.barButtonItemSave.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S));
            this.barButtonItemSave.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemSave.Name = "barButtonItemSave";
            this.barButtonItemSave.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemSave_ItemClick);
            // 
            // barButtonItemPrint
            // 
            this.barButtonItemPrint.Enabled = false;
            this.barButtonItemPrint.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.Glyph")));
            this.barButtonItemPrint.Id = 231;
            this.barButtonItemPrint.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P));
            this.barButtonItemPrint.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemPrint.Name = "barButtonItemPrint";
            this.barButtonItemPrint.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPrint_ItemClick);
            // 
            // barButtonItemHelp
            // 
            this.barButtonItemHelp.Caption = "Help";
            this.barButtonItemHelp.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemHelp.Glyph")));
            this.barButtonItemHelp.Id = 232;
            this.barButtonItemHelp.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F1);
            this.barButtonItemHelp.Name = "barButtonItemHelp";
            toolTipTitleItem87.Text = "Ayuda en lnea";
            superToolTip87.Items.Add(toolTipTitleItem87);
            this.barButtonItemHelp.SuperTip = superToolTip87;
            this.barButtonItemHelp.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemHelp_ItemClick);
            // 
            // barButtonItemRefreshGraphic
            // 
            this.barButtonItemRefreshGraphic.Enabled = false;
            this.barButtonItemRefreshGraphic.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemRefreshGraphic.Glyph")));
            this.barButtonItemRefreshGraphic.Id = 239;
            this.barButtonItemRefreshGraphic.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F5);
            this.barButtonItemRefreshGraphic.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemRefreshGraphic.Name = "barButtonItemRefreshGraphic";
            this.barButtonItemRefreshGraphic.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemRefreshGraphic.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonRefreshChart_ItemClick);
            // 
            // barCheckItemFirstLevel
            // 
            this.barCheckItemFirstLevel.Caption = "First Level";
            this.barCheckItemFirstLevel.Checked = true;
            this.barCheckItemFirstLevel.GroupIndex = 1;
            this.barCheckItemFirstLevel.Id = 240;
            this.barCheckItemFirstLevel.Name = "barCheckItemFirstLevel";
            this.barCheckItemFirstLevel.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.barCheckItemFirstLevel_CheckedChanged);
            // 
            // barButtonItemDispatch
            // 
            this.barButtonItemDispatch.ActAsDropDown = true;
            this.barButtonItemDispatch.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.barButtonItemDispatch.Caption = "Dispatch";
            this.barButtonItemDispatch.DropDownControl = this.popupMenuDispatch;
            this.barButtonItemDispatch.GroupIndex = 1;
            this.barButtonItemDispatch.Id = 245;
            this.barButtonItemDispatch.Name = "barButtonItemDispatch";
            this.barButtonItemDispatch.DownChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemDispatch_DownChanged);
            this.barButtonItemDispatch.ItemPress += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemDispatch_ItemPress);
            // 
            // popupMenuDispatch
            // 
            this.popupMenuDispatch.Name = "popupMenuDispatch";
            this.popupMenuDispatch.Ribbon = this.RibbonControl;
            this.popupMenuDispatch.CloseUp += new System.EventHandler(this.popupMenuDispatch_CloseUp);
            // 
            // barButtonItemCloseReportsReader
            // 
            this.barButtonItemCloseReportsReader.Caption = "Ver reportes";
            this.barButtonItemCloseReportsReader.Id = 248;
            this.barButtonItemCloseReportsReader.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R));
            this.barButtonItemCloseReportsReader.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemCloseReportsReader.LargeGlyph")));
            this.barButtonItemCloseReportsReader.Name = "barButtonItemCloseReportsReader";
            this.barButtonItemCloseReportsReader.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemCloseReportsReader_ItemClick);
            // 
            // barButtonItemExtraTime
            // 
            this.barButtonItemExtraTime.Caption = "Tiempo extra";
            this.barButtonItemExtraTime.Id = 250;
            this.barButtonItemExtraTime.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.W));
            this.barButtonItemExtraTime.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemExtraTime.LargeGlyph")));
            this.barButtonItemExtraTime.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemExtraTime.Name = "barButtonItemExtraTime";
            this.barButtonItemExtraTime.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemExtraTime_ItemClick);
            // 
            // barButtonItemCreateExtraTime
            // 
            this.barButtonItemCreateExtraTime.Caption = "Crear";
            this.barButtonItemCreateExtraTime.Enabled = false;
            this.barButtonItemCreateExtraTime.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemCreateExtraTime.Glyph")));
            this.barButtonItemCreateExtraTime.Id = 251;
            this.barButtonItemCreateExtraTime.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemCreateExtraTime.Name = "barButtonItemCreateExtraTime";
            this.barButtonItemCreateExtraTime.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barButtonItemDeleteExtraTime
            // 
            this.barButtonItemDeleteExtraTime.Caption = "Eliminar";
            this.barButtonItemDeleteExtraTime.Enabled = false;
            this.barButtonItemDeleteExtraTime.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemDeleteExtraTime.Glyph")));
            this.barButtonItemDeleteExtraTime.Id = 252;
            this.barButtonItemDeleteExtraTime.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemDeleteExtraTime.Name = "barButtonItemDeleteExtraTime";
            this.barButtonItemDeleteExtraTime.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemDeleteExtraTime.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemDeleteExtraTime_ItemClick);
            // 
            // barButtonItemModifyExtraTime
            // 
            this.barButtonItemModifyExtraTime.Caption = "Modificar";
            this.barButtonItemModifyExtraTime.Enabled = false;
            this.barButtonItemModifyExtraTime.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemModifyExtraTime.Glyph")));
            this.barButtonItemModifyExtraTime.Id = 253;
            this.barButtonItemModifyExtraTime.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemModifyExtraTime.Name = "barButtonItemModifyExtraTime";
            this.barButtonItemModifyExtraTime.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemModifyExtraTime.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemModifyExtraTime_ItemClick);
            // 
            // barButtonItemVacationsAndAbsences
            // 
            this.barButtonItemVacationsAndAbsences.Caption = "Permisos y vacaciones";
            this.barButtonItemVacationsAndAbsences.Id = 254;
            this.barButtonItemVacationsAndAbsences.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.M));
            this.barButtonItemVacationsAndAbsences.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemVacationsAndAbsences.LargeGlyph")));
            this.barButtonItemVacationsAndAbsences.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemVacationsAndAbsences.Name = "barButtonItemVacationsAndAbsences";
            this.barButtonItemVacationsAndAbsences.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemVacationsAndAbsences_ItemClick);
            // 
            // barButtonItemCreateVacations
            // 
            this.barButtonItemCreateVacations.Caption = "Crear";
            this.barButtonItemCreateVacations.Enabled = false;
            this.barButtonItemCreateVacations.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemCreateVacations.Glyph")));
            this.barButtonItemCreateVacations.Id = 255;
            this.barButtonItemCreateVacations.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemCreateVacations.Name = "barButtonItemCreateVacations";
            this.barButtonItemCreateVacations.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemCreateVacations.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemCreateVacations_ItemClick);
            // 
            // barButtonItemDeleteVacations
            // 
            this.barButtonItemDeleteVacations.Caption = "Eliminar";
            this.barButtonItemDeleteVacations.Enabled = false;
            this.barButtonItemDeleteVacations.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemDeleteVacations.Glyph")));
            this.barButtonItemDeleteVacations.Id = 256;
            this.barButtonItemDeleteVacations.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemDeleteVacations.Name = "barButtonItemDeleteVacations";
            this.barButtonItemDeleteVacations.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemDeleteVacations.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemDeleteVacations_ItemClick);
            // 
            // barButtonItemModifyVacations
            // 
            this.barButtonItemModifyVacations.Caption = "Modificar";
            this.barButtonItemModifyVacations.Enabled = false;
            this.barButtonItemModifyVacations.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemModifyVacations.Glyph")));
            this.barButtonItemModifyVacations.Id = 257;
            this.barButtonItemModifyVacations.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemModifyVacations.Name = "barButtonItemModifyVacations";
            this.barButtonItemModifyVacations.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemModifyVacations.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemModifyVacations_ItemClick);
            // 
            // barEditItemSelectRoom
            // 
            this.barEditItemSelectRoom.Caption = "Nombre:";
            this.barEditItemSelectRoom.Edit = this.repositoryItemComboBoxSelectRoom;
            this.barEditItemSelectRoom.Enabled = false;
            this.barEditItemSelectRoom.Id = 258;
            this.barEditItemSelectRoom.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barEditItemSelectRoom.Name = "barEditItemSelectRoom";
            this.barEditItemSelectRoom.Width = 100;
            // 
            // repositoryItemComboBoxSelectRoom
            // 
            this.repositoryItemComboBoxSelectRoom.AutoHeight = false;
            this.repositoryItemComboBoxSelectRoom.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBoxSelectRoom.Name = "repositoryItemComboBoxSelectRoom";
            this.repositoryItemComboBoxSelectRoom.UseParentBackground = true;
            // 
            // barCheckItemViewSelected
            // 
            this.barCheckItemViewSelected.Caption = "Operador seleccionado";
            this.barCheckItemViewSelected.Enabled = false;
            this.barCheckItemViewSelected.Glyph = ((System.Drawing.Image)(resources.GetObject("barCheckItemViewSelected.Glyph")));
            this.barCheckItemViewSelected.Id = 259;
            this.barCheckItemViewSelected.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barCheckItemViewSelected.Name = "barCheckItemViewSelected";
            // 
            // barCheckItemViewAll
            // 
            this.barCheckItemViewAll.Caption = "Todos los operadores";
            this.barCheckItemViewAll.Enabled = false;
            this.barCheckItemViewAll.Glyph = ((System.Drawing.Image)(resources.GetObject("barCheckItemViewAll.Glyph")));
            this.barCheckItemViewAll.Id = 260;
            this.barCheckItemViewAll.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barCheckItemViewAll.Name = "barCheckItemViewAll";
            // 
            // barButtonItemConsultVacations
            // 
            this.barButtonItemConsultVacations.Caption = "Consulta";
            this.barButtonItemConsultVacations.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemConsultVacations.Glyph")));
            this.barButtonItemConsultVacations.Id = 261;
            this.barButtonItemConsultVacations.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemConsultVacations.Name = "barButtonItemConsultVacations";
            this.barButtonItemConsultVacations.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemConsultVacations.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemConsultVacations_ItemClick);
            // 
            // barButtonItemConsultExtraTime
            // 
            this.barButtonItemConsultExtraTime.Caption = "Consulta";
            this.barButtonItemConsultExtraTime.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemConsultExtraTime.Glyph")));
            this.barButtonItemConsultExtraTime.Id = 264;
            this.barButtonItemConsultExtraTime.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemConsultExtraTime.Name = "barButtonItemConsultExtraTime";
            this.barButtonItemConsultExtraTime.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemConsultExtraTime.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemConsultExtraTime_ItemClick);
            // 
            // barButtonGroupGeneralOptionsMonitor
            // 
            this.barButtonGroupGeneralOptionsMonitor.Id = 274;
            this.barButtonGroupGeneralOptionsMonitor.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonGroupGeneralOptionsMonitor.Name = "barButtonGroupGeneralOptionsMonitor";
            // 
            // barSubItemChartControl
            // 
            this.barSubItemChartControl.Caption = "Configurar grfica";
            this.barSubItemChartControl.Glyph = ((System.Drawing.Image)(resources.GetObject("barSubItemChartControl.Glyph")));
            this.barSubItemChartControl.Id = 278;
            this.barSubItemChartControl.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barCheckItemUpTreshold),
            new DevExpress.XtraBars.LinkPersistInfo(this.barCheckItemDownTreshold),
            new DevExpress.XtraBars.LinkPersistInfo(this.barCheckItemTrend)});
            this.barSubItemChartControl.Name = "barSubItemChartControl";
            this.barSubItemChartControl.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barCheckItemUpTreshold
            // 
            this.barCheckItemUpTreshold.Caption = "Upper threshold";
            this.barCheckItemUpTreshold.Id = 279;
            this.barCheckItemUpTreshold.Name = "barCheckItemUpTreshold";
            this.barCheckItemUpTreshold.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.barCheckItemUpTreshold_CheckedChanged);
            // 
            // barCheckItemDownTreshold
            // 
            this.barCheckItemDownTreshold.Caption = "Lower threshold";
            this.barCheckItemDownTreshold.Id = 280;
            this.barCheckItemDownTreshold.Name = "barCheckItemDownTreshold";
            this.barCheckItemDownTreshold.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.barCheckItemDownTreshold_CheckedChanged);
            // 
            // barCheckItemTrend
            // 
            this.barCheckItemTrend.Caption = "Trend";
            this.barCheckItemTrend.Id = 281;
            this.barCheckItemTrend.Name = "barCheckItemTrend";
            this.barCheckItemTrend.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.barCheckItemTrend_CheckedChanged);
            // 
            // barButtonItemOperatorPerformance
            // 
            this.barButtonItemOperatorPerformance.Caption = "Ver desempeo";
            this.barButtonItemOperatorPerformance.Id = 282;
            this.barButtonItemOperatorPerformance.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D));
            this.barButtonItemOperatorPerformance.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemOperatorPerformance.LargeGlyph")));
            this.barButtonItemOperatorPerformance.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemOperatorPerformance.Name = "barButtonItemOperatorPerformance";
            this.barButtonItemOperatorPerformance.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemOperatorPerformance_ItemClick);
            // 
            // barEditItemIndicatorSelection
            // 
            this.barEditItemIndicatorSelection.Edit = this.repositoryItemRadioGroup3;
            this.barEditItemIndicatorSelection.EditHeight = 50;
            this.barEditItemIndicatorSelection.EditValue = true;
            this.barEditItemIndicatorSelection.Enabled = false;
            this.barEditItemIndicatorSelection.Id = 289;
            this.barEditItemIndicatorSelection.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barEditItemIndicatorSelection.Name = "barEditItemIndicatorSelection";
            this.barEditItemIndicatorSelection.Width = 145;
            // 
            // repositoryItemRadioGroup3
            // 
            this.repositoryItemRadioGroup3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemRadioGroup3.Appearance.BackColor2 = System.Drawing.Color.Transparent;
            this.repositoryItemRadioGroup3.Appearance.BorderColor = System.Drawing.Color.Transparent;
            this.repositoryItemRadioGroup3.Appearance.Options.UseBackColor = true;
            this.repositoryItemRadioGroup3.Appearance.Options.UseBorderColor = true;
            this.repositoryItemRadioGroup3.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemRadioGroup3.AppearanceDisabled.BackColor2 = System.Drawing.Color.Transparent;
            this.repositoryItemRadioGroup3.AppearanceDisabled.BorderColor = System.Drawing.Color.Transparent;
            this.repositoryItemRadioGroup3.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemRadioGroup3.AppearanceDisabled.Options.UseBorderColor = true;
            this.repositoryItemRadioGroup3.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemRadioGroup3.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent;
            this.repositoryItemRadioGroup3.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent;
            this.repositoryItemRadioGroup3.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemRadioGroup3.AppearanceFocused.Options.UseBorderColor = true;
            this.repositoryItemRadioGroup3.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemRadioGroup3.AppearanceReadOnly.BackColor2 = System.Drawing.Color.Transparent;
            this.repositoryItemRadioGroup3.AppearanceReadOnly.BorderColor = System.Drawing.Color.Transparent;
            this.repositoryItemRadioGroup3.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemRadioGroup3.AppearanceReadOnly.Options.UseBorderColor = true;
            this.repositoryItemRadioGroup3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemRadioGroup3.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Primary Indicators"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Secondary Indicators")});
            this.repositoryItemRadioGroup3.LookAndFeel.SkinName = "Blue";
            this.repositoryItemRadioGroup3.Name = "repositoryItemRadioGroup3";
            this.repositoryItemRadioGroup3.UseParentBackground = true;
            this.repositoryItemRadioGroup3.SelectedIndexChanged += new System.EventHandler(this.repositoryItemRadioGroup3_SelectedIndexChanged);
            // 
            // barButtonItemPersonalAssignExtraTime
            // 
            this.barButtonItemPersonalAssignExtraTime.Caption = "Personal";
            this.barButtonItemPersonalAssignExtraTime.Enabled = false;
            this.barButtonItemPersonalAssignExtraTime.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPersonalAssignExtraTime.Glyph")));
            this.barButtonItemPersonalAssignExtraTime.Id = 290;
            this.barButtonItemPersonalAssignExtraTime.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemPersonalAssignExtraTime.Name = "barButtonItemPersonalAssignExtraTime";
            this.barButtonItemPersonalAssignExtraTime.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barButtonItemPersonalAssignVacations
            // 
            this.barButtonItemPersonalAssignVacations.Caption = "Personal";
            this.barButtonItemPersonalAssignVacations.Enabled = false;
            this.barButtonItemPersonalAssignVacations.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPersonalAssignVacations.Glyph")));
            this.barButtonItemPersonalAssignVacations.Id = 291;
            this.barButtonItemPersonalAssignVacations.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemPersonalAssignVacations.Name = "barButtonItemPersonalAssignVacations";
            this.barButtonItemPersonalAssignVacations.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barStaticItemLogo
            // 
            this.barStaticItemLogo.Enabled = false;
            this.barStaticItemLogo.Id = 293;
            this.barStaticItemLogo.Name = "barStaticItemLogo";
            this.barStaticItemLogo.ShowImageInToolbar = false;
            this.barStaticItemLogo.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemLogo.Width = 60;
            // 
            // barButtonItemChat
            // 
            this.barButtonItemChat.Caption = "Mensajeria";
            this.barButtonItemChat.Id = 304;
            this.barButtonItemChat.LargeGlyph = global::SmartCadGuiCommon.Properties.ResourcesGui.chat;
            this.barButtonItemChat.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemChat.Name = "barButtonItemChat";
            this.barButtonItemChat.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemChat_ItemClick);
            // 
            // barButtonItemRemoteControl
            // 
            this.barButtonItemRemoteControl.Caption = "Monitorear";
            this.barButtonItemRemoteControl.Enabled = false;
            this.barButtonItemRemoteControl.Id = 305;
            this.barButtonItemRemoteControl.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemRemoteControl.LargeGlyph")));
            this.barButtonItemRemoteControl.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemRemoteControl.Name = "barButtonItemRemoteControl";
            this.barButtonItemRemoteControl.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemRemoteControl.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barCheckItemTakeControl
            // 
            this.barCheckItemTakeControl.Caption = "barCheckItemTakeControl";
            this.barCheckItemTakeControl.Enabled = false;
            this.barCheckItemTakeControl.Id = 306;
            this.barCheckItemTakeControl.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barCheckItemTakeControl.Name = "barCheckItemTakeControl";
            this.barCheckItemTakeControl.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barCheckItemScaledView
            // 
            this.barCheckItemScaledView.Caption = "barCheckItemScaledView";
            this.barCheckItemScaledView.Enabled = false;
            this.barCheckItemScaledView.Id = 307;
            this.barCheckItemScaledView.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barCheckItemScaledView.Name = "barCheckItemScaledView";
            this.barCheckItemScaledView.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barButtonItemReConnect
            // 
            this.barButtonItemReConnect.Caption = "barButtonItemReConnect";
            this.barButtonItemReConnect.Enabled = false;
            this.barButtonItemReConnect.Id = 308;
            this.barButtonItemReConnect.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemReConnect.Name = "barButtonItemReConnect";
            this.barButtonItemReConnect.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barButtonItemAssign
            // 
            this.barButtonItemAssign.Caption = "Asignar";
            this.barButtonItemAssign.Enabled = false;
            this.barButtonItemAssign.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemAssign.Glyph")));
            this.barButtonItemAssign.Id = 309;
            this.barButtonItemAssign.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemAssign.Name = "barButtonItemAssign";
            this.barButtonItemAssign.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barButtonItemRemove
            // 
            this.barButtonItemRemove.Caption = "Eliminar";
            this.barButtonItemRemove.Enabled = false;
            this.barButtonItemRemove.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemRemove.Glyph")));
            this.barButtonItemRemove.Id = 310;
            this.barButtonItemRemove.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemRemove.Name = "barButtonItemRemove";
            this.barButtonItemRemove.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barButtonItemOpen
            // 
            this.barButtonItemOpen.Caption = "Detalles";
            this.barButtonItemOpen.Enabled = false;
            this.barButtonItemOpen.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemOpen.Glyph")));
            this.barButtonItemOpen.Id = 311;
            this.barButtonItemOpen.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemOpen.Name = "barButtonItemOpen";
            // 
            // barButtonItemLogo
            // 
            this.barButtonItemLogo.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemLogo.Glyph")));
            this.barButtonItemLogo.Id = 312;
            this.barButtonItemLogo.Name = "barButtonItemLogo";
            this.barButtonItemLogo.SmallWithoutTextWidth = 57;
            // 
            // RibbonPageMonitoring
            // 
            this.RibbonPageMonitoring.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupPerformance,
            this.ribbonPageGroupOperator,
            this.ribbonPageGroupGeneralOptMonitor});
            this.RibbonPageMonitoring.Name = "RibbonPageMonitoring";
            this.RibbonPageMonitoring.Text = "Monitoreo";
            // 
            // ribbonPageGroupPerformance
            // 
            this.ribbonPageGroupPerformance.AllowMinimize = false;
            this.ribbonPageGroupPerformance.AllowTextClipping = false;
            this.ribbonPageGroupPerformance.ItemLinks.Add(this.commandBarItemIndicators);
            this.ribbonPageGroupPerformance.ItemLinks.Add(this.barButtonItemConfigureIndicators);
            this.ribbonPageGroupPerformance.ItemLinks.Add(this.barButtonItemOperAffectIndicators);
            this.ribbonPageGroupPerformance.ItemLinks.Add(this.barSubItemChartControl);
            this.ribbonPageGroupPerformance.ItemLinks.Add(this.barButtonItemForecast, true);
            this.ribbonPageGroupPerformance.ItemLinks.Add(this.barButtonItemAddForecast);
            this.ribbonPageGroupPerformance.ItemLinks.Add(this.barButtonItemDeleteForecast);
            this.ribbonPageGroupPerformance.ItemLinks.Add(this.barButtonItemUpdateForecast);
            this.ribbonPageGroupPerformance.ItemLinks.Add(this.barButtonItemDuplicate);
            this.ribbonPageGroupPerformance.ItemLinks.Add(this.barButtonItemReplace);
            this.ribbonPageGroupPerformance.ItemLinks.Add(this.barButtonItemFinalize);
            this.ribbonPageGroupPerformance.ItemLinks.Add(this.BarButtonItemHistoricGraphic, true);
            this.ribbonPageGroupPerformance.ItemLinks.Add(this.BarButtonItemHistoryDetails, true);
            this.ribbonPageGroupPerformance.Kind = DevExpress.XtraReports.UserDesigner.XRDesignRibbonPageGroupKind.Edit;
            this.ribbonPageGroupPerformance.Name = "ribbonPageGroupPerformance";
            this.ribbonPageGroupPerformance.ShowCaptionButton = false;
            this.ribbonPageGroupPerformance.Text = "Desempeo";
            // 
            // ribbonPageGroupOperator
            // 
            this.ribbonPageGroupOperator.AllowMinimize = false;
            this.ribbonPageGroupOperator.AllowTextClipping = false;
            this.ribbonPageGroupOperator.ItemLinks.Add(this.barButtonItemOperatorActivities);
            this.ribbonPageGroupOperator.ItemLinks.Add(this.BarEditItem6, true);
            this.ribbonPageGroupOperator.ItemLinks.Add(this.barButtonItemOperatorPerformance, true);
            this.ribbonPageGroupOperator.ItemLinks.Add(this.barEditItemIndicatorSelection);
            this.ribbonPageGroupOperator.ItemLinks.Add(this.BarButtonItemSelectRoom, true);
            this.ribbonPageGroupOperator.ItemLinks.Add(this.barCheckItemViewSelected);
            this.ribbonPageGroupOperator.ItemLinks.Add(this.barCheckItemViewAll);
            this.ribbonPageGroupOperator.ItemLinks.Add(this.barEditItemSelectRoom);
            this.ribbonPageGroupOperator.ItemLinks.Add(this.barButtonItemChat, true);
            this.ribbonPageGroupOperator.ItemLinks.Add(this.barButtonItemRemoteControl, true);
            this.ribbonPageGroupOperator.ItemLinks.Add(this.barCheckItemTakeControl);
            this.ribbonPageGroupOperator.ItemLinks.Add(this.barCheckItemScaledView);
            this.ribbonPageGroupOperator.ItemLinks.Add(this.barButtonItemReConnect);
            this.ribbonPageGroupOperator.Kind = DevExpress.XtraReports.UserDesigner.XRDesignRibbonPageGroupKind.SizeAndLayout;
            this.ribbonPageGroupOperator.Name = "ribbonPageGroupOperator";
            this.ribbonPageGroupOperator.ShowCaptionButton = false;
            this.ribbonPageGroupOperator.Text = "Operador";
            // 
            // ribbonPageGroupGeneralOptMonitor
            // 
            this.ribbonPageGroupGeneralOptMonitor.AllowMinimize = false;
            this.ribbonPageGroupGeneralOptMonitor.AllowTextClipping = false;
            this.ribbonPageGroupGeneralOptMonitor.ItemLinks.Add(this.barButtonItemPrint);
            this.ribbonPageGroupGeneralOptMonitor.ItemLinks.Add(this.barButtonItemSave);
            this.ribbonPageGroupGeneralOptMonitor.ItemLinks.Add(this.BarButtonItemSendMonitor);
            this.ribbonPageGroupGeneralOptMonitor.ItemLinks.Add(this.barButtonItemRefreshGraphic);
            this.ribbonPageGroupGeneralOptMonitor.Name = "ribbonPageGroupGeneralOptMonitor";
            this.ribbonPageGroupGeneralOptMonitor.ShowCaptionButton = false;
            this.ribbonPageGroupGeneralOptMonitor.Text = "Opciones generales";
            // 
            // RibbonPageOperation
            // 
            this.RibbonPageOperation.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupPersonalFile,
            this.ribbonPageGroupExtraTime,
            this.ribbonPageGroupAssignment,
            this.ribbonPageGroupVacationAndAbsence,
            this.ribbonPageGroupTraining,
            this.ribbonPageGroupDistribution,
            this.ribbonPageGroupFinalReport,
            this.ribbonPageGroupOptions});
            this.RibbonPageOperation.Name = "RibbonPageOperation";
            this.RibbonPageOperation.Text = "Operacin";
            // 
            // ribbonPageGroupPersonalFile
            // 
            this.ribbonPageGroupPersonalFile.AllowMinimize = false;
            this.ribbonPageGroupPersonalFile.AllowTextClipping = false;
            this.ribbonPageGroupPersonalFile.ItemLinks.Add(this.printPreviewBarItemPersonalfile);
            this.ribbonPageGroupPersonalFile.ItemLinks.Add(this.barButtonItemEvaluateOperator);
            this.ribbonPageGroupPersonalFile.ItemLinks.Add(this.printPreviewBarItemModifyCategory);
            this.ribbonPageGroupPersonalFile.ItemLinks.Add(this.printPreviewBarItemPersonalFileObs);
            this.ribbonPageGroupPersonalFile.Kind = DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroupKind.Print;
            this.ribbonPageGroupPersonalFile.Name = "ribbonPageGroupPersonalFile";
            this.ribbonPageGroupPersonalFile.ShowCaptionButton = false;
            this.ribbonPageGroupPersonalFile.Text = "Expediente";
            // 
            // ribbonPageGroupExtraTime
            // 
            this.ribbonPageGroupExtraTime.AllowMinimize = false;
            this.ribbonPageGroupExtraTime.AllowTextClipping = false;
            this.ribbonPageGroupExtraTime.ItemLinks.Add(this.barButtonItemExtraTime);
            this.ribbonPageGroupExtraTime.ItemLinks.Add(this.barButtonItemCreateExtraTime);
            this.ribbonPageGroupExtraTime.ItemLinks.Add(this.barButtonItemDeleteExtraTime);
            this.ribbonPageGroupExtraTime.ItemLinks.Add(this.barButtonItemModifyExtraTime);
            this.ribbonPageGroupExtraTime.ItemLinks.Add(this.barButtonItemPersonalAssignExtraTime);
            this.ribbonPageGroupExtraTime.ItemLinks.Add(this.barButtonItemConsultExtraTime);
            this.ribbonPageGroupExtraTime.Name = "ribbonPageGroupExtraTime";
            this.ribbonPageGroupExtraTime.ShowCaptionButton = false;
            this.ribbonPageGroupExtraTime.Text = "Tiempo extra";
            // 
            // ribbonPageGroupAssignment
            // 
            this.ribbonPageGroupAssignment.AllowMinimize = false;
            this.ribbonPageGroupAssignment.AllowTextClipping = false;
            this.ribbonPageGroupAssignment.ItemLinks.Add(this.barButtonItemOperatorAssign);
            this.ribbonPageGroupAssignment.ItemLinks.Add(this.barButtonItemOpen);
            this.ribbonPageGroupAssignment.ItemLinks.Add(this.barButtonItemAssign);
            this.ribbonPageGroupAssignment.ItemLinks.Add(this.barButtonItemRemove);
            this.ribbonPageGroupAssignment.Name = "ribbonPageGroupAssignment";
            this.ribbonPageGroupAssignment.ShowCaptionButton = false;
            this.ribbonPageGroupAssignment.Text = "Asignacin";
            // 
            // ribbonPageGroupVacationAndAbsence
            // 
            this.ribbonPageGroupVacationAndAbsence.AllowMinimize = false;
            this.ribbonPageGroupVacationAndAbsence.AllowTextClipping = false;
            this.ribbonPageGroupVacationAndAbsence.ItemLinks.Add(this.barButtonItemVacationsAndAbsences);
            this.ribbonPageGroupVacationAndAbsence.ItemLinks.Add(this.barButtonItemCreateVacations);
            this.ribbonPageGroupVacationAndAbsence.ItemLinks.Add(this.barButtonItemDeleteVacations);
            this.ribbonPageGroupVacationAndAbsence.ItemLinks.Add(this.barButtonItemModifyVacations);
            this.ribbonPageGroupVacationAndAbsence.ItemLinks.Add(this.barButtonItemPersonalAssignVacations);
            this.ribbonPageGroupVacationAndAbsence.ItemLinks.Add(this.barButtonItemConsultVacations);
            this.ribbonPageGroupVacationAndAbsence.Name = "ribbonPageGroupVacationAndAbsence";
            this.ribbonPageGroupVacationAndAbsence.ShowCaptionButton = false;
            this.ribbonPageGroupVacationAndAbsence.Text = "Permisos y vacaciones";
            // 
            // ribbonPageGroupTraining
            // 
            this.ribbonPageGroupTraining.AllowMinimize = false;
            this.ribbonPageGroupTraining.AllowTextClipping = false;
            this.ribbonPageGroupTraining.ItemLinks.Add(this.barButtonItemCourses);
            this.ribbonPageGroupTraining.ItemLinks.Add(this.barItemCreateCourses);
            this.ribbonPageGroupTraining.ItemLinks.Add(this.barItemDeleteCourses);
            this.ribbonPageGroupTraining.ItemLinks.Add(this.barButtonItemModifyCourses);
            this.ribbonPageGroupTraining.ItemLinks.Add(this.barButtonTrainingCoursesSchedule);
            this.ribbonPageGroupTraining.ItemLinks.Add(this.barButtonOperatorTrainingAssign);
            this.ribbonPageGroupTraining.Kind = DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroupKind.PageSetup;
            this.ribbonPageGroupTraining.Name = "ribbonPageGroupTraining";
            this.ribbonPageGroupTraining.ShowCaptionButton = false;
            this.ribbonPageGroupTraining.Text = "Entrenamiento";
            // 
            // ribbonPageGroupDistribution
            // 
            this.ribbonPageGroupDistribution.AllowMinimize = false;
            this.ribbonPageGroupDistribution.AllowTextClipping = false;
            this.ribbonPageGroupDistribution.ItemLinks.Add(this.barItemNotificationDistribution);
            this.ribbonPageGroupDistribution.Kind = DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroupKind.Document;
            this.ribbonPageGroupDistribution.Name = "ribbonPageGroupDistribution";
            this.ribbonPageGroupDistribution.ShowCaptionButton = false;
            toolTipTitleItem88.Text = "Distribucin Manual de Solicitudes de Despacho";
            superToolTip88.Items.Add(toolTipTitleItem88);
            this.ribbonPageGroupDistribution.SuperTip = superToolTip88;
            this.ribbonPageGroupDistribution.Text = "Distribucin";
            this.ribbonPageGroupDistribution.Visible = false;
            // 
            // ribbonPageGroupFinalReport
            // 
            this.ribbonPageGroupFinalReport.AllowMinimize = false;
            this.ribbonPageGroupFinalReport.AllowTextClipping = false;
            this.ribbonPageGroupFinalReport.ItemLinks.Add(this.barButtonItemCloseReportsReader);
            this.ribbonPageGroupFinalReport.ItemLinks.Add(this.barButtonItemReport, true);
            this.ribbonPageGroupFinalReport.ItemLinks.Add(this.barButtonItemEndReport);
            this.ribbonPageGroupFinalReport.ItemLinks.Add(this.barButtonItemRedo);
            this.ribbonPageGroupFinalReport.ItemLinks.Add(this.barButtonItemUndo);
            this.ribbonPageGroupFinalReport.ItemLinks.Add(this.barButtonItemCut);
            this.ribbonPageGroupFinalReport.ItemLinks.Add(this.barButtonItemCopy);
            this.ribbonPageGroupFinalReport.ItemLinks.Add(this.barButtonItemPaste);
            this.ribbonPageGroupFinalReport.Kind = DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroupKind.Navigation;
            this.ribbonPageGroupFinalReport.Name = "ribbonPageGroupFinalReport";
            this.ribbonPageGroupFinalReport.ShowCaptionButton = false;
            this.ribbonPageGroupFinalReport.Text = "Reporte de cierre";
            // 
            // ribbonPageGroupOptions
            // 
            this.ribbonPageGroupOptions.AllowMinimize = false;
            this.ribbonPageGroupOptions.AllowTextClipping = false;
            this.ribbonPageGroupOptions.ItemLinks.Add(this.barButtonItemPrint);
            this.ribbonPageGroupOptions.ItemLinks.Add(this.barButtonItemSave);
            this.ribbonPageGroupOptions.ItemLinks.Add(this.barButtonItemSend);
            this.ribbonPageGroupOptions.ItemLinks.Add(this.barButtonItemUpdate);
            this.ribbonPageGroupOptions.Name = "ribbonPageGroupOptions";
            this.ribbonPageGroupOptions.ShowCaptionButton = false;
            this.ribbonPageGroupOptions.Text = "Opciones generales";
            // 
            // RibbonPageTools
            // 
            this.RibbonPageTools.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupPreferences,
            this.ribbonPageGroupAccessProfile});
            this.RibbonPageTools.Name = "RibbonPageTools";
            this.RibbonPageTools.Text = "Herramientas";
            // 
            // ribbonPageGroupPreferences
            // 
            this.ribbonPageGroupPreferences.AllowMinimize = false;
            this.ribbonPageGroupPreferences.AllowTextClipping = false;
            this.ribbonPageGroupPreferences.ItemLinks.Add(this.barButtonItemPreferences);
            this.ribbonPageGroupPreferences.Kind = DevExpress.XtraReports.UserDesigner.XRDesignRibbonPageGroupKind.HtmlNavigation;
            this.ribbonPageGroupPreferences.Name = "ribbonPageGroupPreferences";
            this.ribbonPageGroupPreferences.ShowCaptionButton = false;
            this.ribbonPageGroupPreferences.Text = "Preferencias";
            // 
            // ribbonPageGroupAccessProfile
            // 
            this.ribbonPageGroupAccessProfile.AllowMinimize = false;
            this.ribbonPageGroupAccessProfile.AllowTextClipping = false;
            this.ribbonPageGroupAccessProfile.ItemLinks.Add(this.barCheckItemFirstLevel);
            this.ribbonPageGroupAccessProfile.ItemLinks.Add(this.barButtonItemDispatch);
            this.ribbonPageGroupAccessProfile.Name = "ribbonPageGroupAccessProfile";
            this.ribbonPageGroupAccessProfile.ShowCaptionButton = false;
            this.ribbonPageGroupAccessProfile.Text = "Perfil de acceso";
            // 
            // repositoryItemPictureEdit1
            // 
            this.repositoryItemPictureEdit1.Name = "repositoryItemPictureEdit1";
            // 
            // repositoryItemTimeEdit1
            // 
            this.repositoryItemTimeEdit1.AutoHeight = false;
            this.repositoryItemTimeEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemTimeEdit1.Name = "repositoryItemTimeEdit1";
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.Appearance.BackColor = System.Drawing.Color.White;
            this.repositoryItemCheckEdit2.Appearance.BackColor2 = System.Drawing.Color.White;
            this.repositoryItemCheckEdit2.Appearance.Options.UseBackColor = true;
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.AutoWidth = true;
            this.repositoryItemCheckEdit2.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style6;
            this.repositoryItemCheckEdit2.LookAndFeel.SkinName = "Blue";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            // 
            // repositoryItemComboBoxWorkshift
            // 
            this.repositoryItemComboBoxWorkshift.AutoHeight = false;
            this.repositoryItemComboBoxWorkshift.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBoxWorkshift.Name = "repositoryItemComboBoxWorkshift";
            // 
            // repositoryItemDateEdit
            // 
            this.repositoryItemDateEdit.AutoHeight = false;
            this.repositoryItemDateEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit.Name = "repositoryItemDateEdit";
            this.repositoryItemDateEdit.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // repositoryItemPopupContainerEdit1
            // 
            this.repositoryItemPopupContainerEdit1.AutoHeight = false;
            this.repositoryItemPopupContainerEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit1.Name = "repositoryItemPopupContainerEdit1";
            // 
            // repositoryItemRadioGroup1
            // 
            this.repositoryItemRadioGroup1.Name = "repositoryItemRadioGroup1";
            // 
            // repositoryItemRadioGroup2
            // 
            this.repositoryItemRadioGroup2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.repositoryItemRadioGroup2.Columns = 2;
            this.repositoryItemRadioGroup2.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Indicadores primarios"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Indicadores secundarios")});
            this.repositoryItemRadioGroup2.Name = "repositoryItemRadioGroup2";
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Caption = "";
            this.repositoryItemCheckEdit3.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            // 
            // ribbonStatusBar1
            // 
            this.ribbonStatusBar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ribbonStatusBar1.ItemLinks.Add(this.labelUser);
            this.ribbonStatusBar1.ItemLinks.Add(this.labelConnected);
            this.ribbonStatusBar1.ItemLinks.Add(this.labelDate);
            this.ribbonStatusBar1.Location = new System.Drawing.Point(0, 727);
            this.ribbonStatusBar1.Name = "ribbonStatusBar1";
            this.ribbonStatusBar1.Ribbon = this.RibbonControl;
            this.ribbonStatusBar1.Size = new System.Drawing.Size(1280, 23);
            // 
            // labelUser
            // 
            this.labelUser.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUser.Appearance.Options.UseFont = true;
            this.labelUser.Caption = "Usuario";
            this.labelUser.Id = 26;
            this.labelUser.Name = "labelUser";
            this.labelUser.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // labelConnected
            // 
            this.labelConnected.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelConnected.Appearance.Options.UseFont = true;
            this.labelConnected.Caption = "Connected";
            this.labelConnected.Id = 25;
            this.labelConnected.Name = "labelConnected";
            this.labelConnected.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // labelDate
            // 
            this.labelDate.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.labelDate.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDate.Appearance.Options.UseFont = true;
            this.labelDate.Caption = "Date";
            this.labelDate.Id = 27;
            this.labelDate.Name = "labelDate";
            this.labelDate.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barStaticItem1.Appearance.Options.UseFont = true;
            this.barStaticItem1.Caption = "Usuario";
            this.barStaticItem1.Id = 26;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem5
            // 
            this.barStaticItem5.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barStaticItem5.Appearance.Options.UseFont = true;
            this.barStaticItem5.Caption = "Connected";
            this.barStaticItem5.Id = 25;
            this.barStaticItem5.Name = "barStaticItem5";
            this.barStaticItem5.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem6
            // 
            this.barStaticItem6.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barStaticItem6.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barStaticItem6.Appearance.Options.UseFont = true;
            this.barStaticItem6.Caption = "Date";
            this.barStaticItem6.Id = 27;
            this.barStaticItem6.Name = "barStaticItem6";
            this.barStaticItem6.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // contextMenuStripDesigner
            // 
            this.contextMenuStripDesigner.Name = "contextMenuStripDesigner";
            this.contextMenuStripDesigner.Size = new System.Drawing.Size(61, 4);
            // 
            // xtraTabbedMdiManager
            // 
            this.xtraTabbedMdiManager.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.xtraTabbedMdiManager.BorderStylePage = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.xtraTabbedMdiManager.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InActiveTabPageHeader;
            this.xtraTabbedMdiManager.HeaderButtons = ((DevExpress.XtraTab.TabButtons)(((DevExpress.XtraTab.TabButtons.Prev | DevExpress.XtraTab.TabButtons.Next) 
            | DevExpress.XtraTab.TabButtons.Default)));
            this.xtraTabbedMdiManager.HeaderButtonsShowMode = DevExpress.XtraTab.TabButtonShowMode.WhenNeeded;
            this.xtraTabbedMdiManager.MdiParent = this;
            this.xtraTabbedMdiManager.SetNextMdiChildMode = DevExpress.XtraTabbedMdi.SetNextMdiChildMode.TabControl;
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.AllowMinimize = false;
            this.ribbonPageGroup1.AllowTextClipping = false;
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItemUpdate);
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItemSend);
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItemSave);
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItemPrint);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.ShowCaptionButton = false;
            this.ribbonPageGroup1.Text = "Opciones generales";
            // 
            // barButtonRefreshChart
            // 
            this.barButtonRefreshChart.Caption = "Actualizar grfica";
            this.barButtonRefreshChart.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonRefreshChart.Glyph")));
            this.barButtonRefreshChart.Id = 209;
            this.barButtonRefreshChart.Name = "barButtonRefreshChart";
            this.barButtonRefreshChart.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonRefreshChart_ItemClick);
            // 
            // labelLogo
            // 
            this.labelLogo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLogo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.labelLogo.Image = ((System.Drawing.Image)(resources.GetObject("labelLogo.Image")));
            this.labelLogo.Location = new System.Drawing.Point(1223, 2);
            this.labelLogo.Name = "labelLogo";
            this.labelLogo.Size = new System.Drawing.Size(57, 22);
            this.labelLogo.TabIndex = 22;
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupCallInfo,
            this.ribbonPageGroupApplications,
            this.ribbonPageGroupGeneralOptions,
            this.ribbonPageGroupTools});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "ribbonPage1";
            // 
            // ribbonPageGroupCallInfo
            // 
            this.ribbonPageGroupCallInfo.AllowMinimize = false;
            this.ribbonPageGroupCallInfo.AllowTextClipping = false;
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barButtonItemStartRegIncident);
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barButtonItemCreateNewIncident, true);
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barButtonItemCancelIncident);
            this.ribbonPageGroupCallInfo.Name = "ribbonPageGroupCallInfo";
            this.ribbonPageGroupCallInfo.ShowCaptionButton = false;
            this.ribbonPageGroupCallInfo.Text = "ribbonPageGroupCallInfo";
            // 
            // barButtonItemStartRegIncident
            // 
            this.barButtonItemStartRegIncident.Caption = "ItemStartRegIncident";
            this.barButtonItemStartRegIncident.Enabled = false;
            this.barButtonItemStartRegIncident.Id = 12;
            this.barButtonItemStartRegIncident.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemStartRegIncident.LargeGlyph")));
            this.barButtonItemStartRegIncident.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemStartRegIncident.Name = "barButtonItemStartRegIncident";
            this.barButtonItemStartRegIncident.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItemCreateNewIncident
            // 
            this.barButtonItemCreateNewIncident.Caption = "ItemCreateNewIncident";
            this.barButtonItemCreateNewIncident.Enabled = false;
            this.barButtonItemCreateNewIncident.Id = 13;
            this.barButtonItemCreateNewIncident.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemCreateNewIncident.LargeGlyph")));
            this.barButtonItemCreateNewIncident.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemCreateNewIncident.Name = "barButtonItemCreateNewIncident";
            this.barButtonItemCreateNewIncident.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItemCancelIncident
            // 
            this.barButtonItemCancelIncident.Caption = "ItemCancelIncident";
            this.barButtonItemCancelIncident.Enabled = false;
            this.barButtonItemCancelIncident.Id = 15;
            this.barButtonItemCancelIncident.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemCancelIncident.LargeGlyph")));
            this.barButtonItemCancelIncident.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemCancelIncident.Name = "barButtonItemCancelIncident";
            this.barButtonItemCancelIncident.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // ribbonPageGroupApplications
            // 
            this.ribbonPageGroupApplications.AllowMinimize = false;
            this.ribbonPageGroupApplications.AllowTextClipping = false;
            this.ribbonPageGroupApplications.ItemLinks.Add(this.barButtonItem1);
            this.ribbonPageGroupApplications.ItemLinks.Add(this.barButtonItemHistory);
            this.ribbonPageGroupApplications.Name = "ribbonPageGroupApplications";
            this.ribbonPageGroupApplications.ShowCaptionButton = false;
            this.ribbonPageGroupApplications.Text = "Applications";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "ItemChat";
            this.barButtonItem1.Id = 5;
            this.barButtonItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.LargeGlyph")));
            this.barButtonItem1.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem1.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barButtonItemHistory
            // 
            this.barButtonItemHistory.Caption = "History";
            this.barButtonItemHistory.Id = 21;
            this.barButtonItemHistory.LargeGlyph = global::SmartCadGuiCommon.Properties.ResourcesGui.Historial;
            this.barButtonItemHistory.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemHistory.Name = "barButtonItemHistory";
            this.barButtonItemHistory.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // ribbonPageGroupGeneralOptions
            // 
            this.ribbonPageGroupGeneralOptions.AllowMinimize = false;
            this.ribbonPageGroupGeneralOptions.AllowTextClipping = false;
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItem2);
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItem4);
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemRefresh);
            this.ribbonPageGroupGeneralOptions.Name = "ribbonPageGroupGeneralOptions";
            this.ribbonPageGroupGeneralOptions.ShowCaptionButton = false;
            this.ribbonPageGroupGeneralOptions.Text = "ribbonPageGroupGeneralOptions";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "ItemPrint";
            this.barButtonItem2.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem2.Glyph")));
            this.barButtonItem2.Id = 6;
            this.barButtonItem2.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "ItemSave";
            this.barButtonItem4.Glyph = global::SmartCadGuiCommon.PrintRibbonControllerResources.RibbonPrintPreview_Save;
            this.barButtonItem4.Id = 7;
            this.barButtonItem4.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // barButtonItemRefresh
            // 
            this.barButtonItemRefresh.Caption = "ItemRefresh";
            this.barButtonItemRefresh.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemRefresh.Glyph")));
            this.barButtonItemRefresh.Id = 8;
            this.barButtonItemRefresh.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemRefresh.Name = "barButtonItemRefresh";
            // 
            // ribbonPageGroupTools
            // 
            this.ribbonPageGroupTools.AllowMinimize = false;
            this.ribbonPageGroupTools.AllowTextClipping = false;
            this.ribbonPageGroupTools.ItemLinks.Add(this.barButtonItemOpenMap);
            this.ribbonPageGroupTools.Name = "ribbonPageGroupTools";
            this.ribbonPageGroupTools.ShowCaptionButton = false;
            this.ribbonPageGroupTools.Text = "ribbonPageGroupTools";
            // 
            // barButtonItemOpenMap
            // 
            this.barButtonItemOpenMap.Caption = "ItemOpenMap";
            this.barButtonItemOpenMap.Id = 4;
            this.barButtonItemOpenMap.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemOpenMap.LargeGlyph")));
            this.barButtonItemOpenMap.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemOpenMap.Name = "barButtonItemOpenMap";
            this.barButtonItemOpenMap.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItem6
            // 
            this.barButtonItem6.Caption = "barButtonItemHelp";
            this.barButtonItem6.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem6.Glyph")));
            this.barButtonItem6.Id = 0;
            this.barButtonItem6.Name = "barButtonItem6";
            // 
            // barButtonItem7
            // 
            this.barButtonItem7.Caption = "barButtonItem4";
            this.barButtonItem7.Id = 24;
            this.barButtonItem7.Name = "barButtonItem7";
            this.barButtonItem7.SmallWithoutTextWidth = 70;
            // 
            // ribbonPage2
            // 
            this.ribbonPage2.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup2,
            this.ribbonPageGroup3,
            this.ribbonPageGroup4,
            this.ribbonPageGroup5});
            this.ribbonPage2.Name = "ribbonPage2";
            this.ribbonPage2.Text = "ribbonPage1";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.AllowMinimize = false;
            this.ribbonPageGroup2.AllowTextClipping = false;
            this.ribbonPageGroup2.ItemLinks.Add(this.barButtonItem8);
            this.ribbonPageGroup2.ItemLinks.Add(this.barButtonItem9, true);
            this.ribbonPageGroup2.ItemLinks.Add(this.barButtonItem10);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.ShowCaptionButton = false;
            this.ribbonPageGroup2.Text = "ribbonPageGroupCallInfo";
            // 
            // barButtonItem8
            // 
            this.barButtonItem8.Caption = "ItemStartRegIncident";
            this.barButtonItem8.Enabled = false;
            this.barButtonItem8.Id = 12;
            this.barButtonItem8.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem8.LargeGlyph")));
            this.barButtonItem8.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItem8.Name = "barButtonItem8";
            this.barButtonItem8.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItem9
            // 
            this.barButtonItem9.Caption = "ItemCreateNewIncident";
            this.barButtonItem9.Enabled = false;
            this.barButtonItem9.Id = 13;
            this.barButtonItem9.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem9.LargeGlyph")));
            this.barButtonItem9.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItem9.Name = "barButtonItem9";
            this.barButtonItem9.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItem10
            // 
            this.barButtonItem10.Caption = "ItemCancelIncident";
            this.barButtonItem10.Enabled = false;
            this.barButtonItem10.Id = 15;
            this.barButtonItem10.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem10.LargeGlyph")));
            this.barButtonItem10.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItem10.Name = "barButtonItem10";
            this.barButtonItem10.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.AllowMinimize = false;
            this.ribbonPageGroup3.AllowTextClipping = false;
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItem11);
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItem12);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.ShowCaptionButton = false;
            this.ribbonPageGroup3.Text = "Applications";
            // 
            // barButtonItem11
            // 
            this.barButtonItem11.Caption = "ItemChat";
            this.barButtonItem11.Id = 5;
            this.barButtonItem11.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem11.LargeGlyph")));
            this.barButtonItem11.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItem11.Name = "barButtonItem11";
            this.barButtonItem11.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem11.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barButtonItem12
            // 
            this.barButtonItem12.Caption = "History";
            this.barButtonItem12.Id = 21;
            this.barButtonItem12.LargeGlyph = global::SmartCadGuiCommon.Properties.ResourcesGui.Historial;
            this.barButtonItem12.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItem12.Name = "barButtonItem12";
            this.barButtonItem12.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.AllowMinimize = false;
            this.ribbonPageGroup4.AllowTextClipping = false;
            this.ribbonPageGroup4.ItemLinks.Add(this.barButtonItem13);
            this.ribbonPageGroup4.ItemLinks.Add(this.barButtonItem14);
            this.ribbonPageGroup4.ItemLinks.Add(this.barButtonItem15);
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            this.ribbonPageGroup4.ShowCaptionButton = false;
            this.ribbonPageGroup4.Text = "ribbonPageGroupGeneralOptions";
            // 
            // barButtonItem13
            // 
            this.barButtonItem13.Caption = "ItemPrint";
            this.barButtonItem13.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem13.Glyph")));
            this.barButtonItem13.Id = 6;
            this.barButtonItem13.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItem13.Name = "barButtonItem13";
            // 
            // barButtonItem14
            // 
            this.barButtonItem14.Caption = "ItemSave";
            this.barButtonItem14.Glyph = global::SmartCadGuiCommon.PrintRibbonControllerResources.RibbonPrintPreview_Save;
            this.barButtonItem14.Id = 7;
            this.barButtonItem14.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItem14.Name = "barButtonItem14";
            // 
            // barButtonItem15
            // 
            this.barButtonItem15.Caption = "ItemRefresh";
            this.barButtonItem15.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem15.Glyph")));
            this.barButtonItem15.Id = 8;
            this.barButtonItem15.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItem15.Name = "barButtonItem15";
            // 
            // ribbonPageGroup5
            // 
            this.ribbonPageGroup5.AllowMinimize = false;
            this.ribbonPageGroup5.AllowTextClipping = false;
            this.ribbonPageGroup5.ItemLinks.Add(this.barButtonItem16);
            this.ribbonPageGroup5.Name = "ribbonPageGroup5";
            this.ribbonPageGroup5.ShowCaptionButton = false;
            this.ribbonPageGroup5.Text = "ribbonPageGroupTools";
            // 
            // barButtonItem16
            // 
            this.barButtonItem16.Caption = "ItemOpenMap";
            this.barButtonItem16.Id = 4;
            this.barButtonItem16.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem16.LargeGlyph")));
            this.barButtonItem16.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItem16.Name = "barButtonItem16";
            this.barButtonItem16.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItem19
            // 
            this.barButtonItem19.Caption = "barButtonItemHelp";
            this.barButtonItem19.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem19.Glyph")));
            this.barButtonItem19.Id = 0;
            this.barButtonItem19.Name = "barButtonItem19";
            // 
            // barButtonItem20
            // 
            this.barButtonItem20.Caption = "barButtonItem4";
            this.barButtonItem20.Id = 24;
            this.barButtonItem20.Name = "barButtonItem20";
            this.barButtonItem20.SmallWithoutTextWidth = 70;
            // 
            // ribbonPage3
            // 
            this.ribbonPage3.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup6,
            this.ribbonPageGroup7,
            this.ribbonPageGroup8,
            this.ribbonPageGroup9});
            this.ribbonPage3.Name = "ribbonPage3";
            this.ribbonPage3.Text = "ribbonPage1";
            // 
            // ribbonPageGroup6
            // 
            this.ribbonPageGroup6.AllowMinimize = false;
            this.ribbonPageGroup6.AllowTextClipping = false;
            this.ribbonPageGroup6.ItemLinks.Add(this.barButtonItem21);
            this.ribbonPageGroup6.ItemLinks.Add(this.barButtonItem22, true);
            this.ribbonPageGroup6.ItemLinks.Add(this.barButtonItem23);
            this.ribbonPageGroup6.Name = "ribbonPageGroup6";
            this.ribbonPageGroup6.ShowCaptionButton = false;
            this.ribbonPageGroup6.Text = "ribbonPageGroupCallInfo";
            // 
            // barButtonItem21
            // 
            this.barButtonItem21.Caption = "ItemStartRegIncident";
            this.barButtonItem21.Enabled = false;
            this.barButtonItem21.Id = 12;
            this.barButtonItem21.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem21.LargeGlyph")));
            this.barButtonItem21.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItem21.Name = "barButtonItem21";
            this.barButtonItem21.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItem22
            // 
            this.barButtonItem22.Caption = "ItemCreateNewIncident";
            this.barButtonItem22.Enabled = false;
            this.barButtonItem22.Id = 13;
            this.barButtonItem22.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem22.LargeGlyph")));
            this.barButtonItem22.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItem22.Name = "barButtonItem22";
            this.barButtonItem22.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItem23
            // 
            this.barButtonItem23.Caption = "ItemCancelIncident";
            this.barButtonItem23.Enabled = false;
            this.barButtonItem23.Id = 15;
            this.barButtonItem23.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem23.LargeGlyph")));
            this.barButtonItem23.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItem23.Name = "barButtonItem23";
            this.barButtonItem23.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // ribbonPageGroup7
            // 
            this.ribbonPageGroup7.AllowMinimize = false;
            this.ribbonPageGroup7.AllowTextClipping = false;
            this.ribbonPageGroup7.ItemLinks.Add(this.barButtonItem24);
            this.ribbonPageGroup7.ItemLinks.Add(this.barButtonItem42);
            this.ribbonPageGroup7.Name = "ribbonPageGroup7";
            this.ribbonPageGroup7.ShowCaptionButton = false;
            this.ribbonPageGroup7.Text = "Applications";
            // 
            // barButtonItem24
            // 
            this.barButtonItem24.Caption = "ItemChat";
            this.barButtonItem24.Id = 5;
            this.barButtonItem24.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem24.LargeGlyph")));
            this.barButtonItem24.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItem24.Name = "barButtonItem24";
            this.barButtonItem24.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem24.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barButtonItem42
            // 
            this.barButtonItem42.Caption = "History";
            this.barButtonItem42.Id = 21;
            this.barButtonItem42.LargeGlyph = global::SmartCadGuiCommon.Properties.ResourcesGui.Historial;
            this.barButtonItem42.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItem42.Name = "barButtonItem42";
            this.barButtonItem42.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // ribbonPageGroup8
            // 
            this.ribbonPageGroup8.AllowMinimize = false;
            this.ribbonPageGroup8.AllowTextClipping = false;
            this.ribbonPageGroup8.ItemLinks.Add(this.barButtonItem43);
            this.ribbonPageGroup8.ItemLinks.Add(this.barButtonItem44);
            this.ribbonPageGroup8.ItemLinks.Add(this.barButtonItem45);
            this.ribbonPageGroup8.Name = "ribbonPageGroup8";
            this.ribbonPageGroup8.ShowCaptionButton = false;
            this.ribbonPageGroup8.Text = "ribbonPageGroupGeneralOptions";
            // 
            // barButtonItem43
            // 
            this.barButtonItem43.Caption = "ItemPrint";
            this.barButtonItem43.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem43.Glyph")));
            this.barButtonItem43.Id = 6;
            this.barButtonItem43.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItem43.Name = "barButtonItem43";
            // 
            // barButtonItem44
            // 
            this.barButtonItem44.Caption = "ItemSave";
            this.barButtonItem44.Glyph = global::SmartCadGuiCommon.PrintRibbonControllerResources.RibbonPrintPreview_Save;
            this.barButtonItem44.Id = 7;
            this.barButtonItem44.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItem44.Name = "barButtonItem44";
            // 
            // barButtonItem45
            // 
            this.barButtonItem45.Caption = "ItemRefresh";
            this.barButtonItem45.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem45.Glyph")));
            this.barButtonItem45.Id = 8;
            this.barButtonItem45.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItem45.Name = "barButtonItem45";
            // 
            // ribbonPageGroup9
            // 
            this.ribbonPageGroup9.AllowMinimize = false;
            this.ribbonPageGroup9.AllowTextClipping = false;
            this.ribbonPageGroup9.ItemLinks.Add(this.barButtonItem46);
            this.ribbonPageGroup9.Name = "ribbonPageGroup9";
            this.ribbonPageGroup9.ShowCaptionButton = false;
            this.ribbonPageGroup9.Text = "ribbonPageGroupTools";
            // 
            // barButtonItem46
            // 
            this.barButtonItem46.Caption = "ItemOpenMap";
            this.barButtonItem46.Id = 4;
            this.barButtonItem46.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem46.LargeGlyph")));
            this.barButtonItem46.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItem46.Name = "barButtonItem46";
            this.barButtonItem46.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItem47
            // 
            this.barButtonItem47.Caption = "barButtonItemHelp";
            this.barButtonItem47.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem47.Glyph")));
            this.barButtonItem47.Id = 0;
            this.barButtonItem47.Name = "barButtonItem47";
            // 
            // barButtonItem48
            // 
            this.barButtonItem48.Caption = "barButtonItem4";
            this.barButtonItem48.Id = 24;
            this.barButtonItem48.Name = "barButtonItem48";
            this.barButtonItem48.SmallWithoutTextWidth = 70;
            // 
            // SupervisionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1280, 750);
            this.Controls.Add(this.ribbonStatusBar1);
            this.Controls.Add(this.labelLogo);
            this.Controls.Add(this.RibbonControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.KeyPreview = true;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1278, 726);
            this.Name = "SupervisionForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SmartCAD - Mdulo de Supervisin";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SupervisionForm_FormClosing);
            this.Load += new System.EventHandler(this.SupervisionForm_Load);
            this.MdiChildActivate += new System.EventHandler(this.SupervisionForm_MdiChildActivate);
            this.contextMenuStripOperators.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RibbonControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenuDispatch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBoxSelectRoom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBoxWorkshift)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStripOperators;
        private System.Windows.Forms.ToolStripMenuItem verOcultarCronogramaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asignarCursoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem agregarObservacionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ubicacinEnLaSalaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem conversacinToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        private System.Windows.Forms.ToolStripMenuItem monitorearEstacinToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem monitorearPantallaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem monitorearLlamadaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator16;
        private System.Windows.Forms.ToolStripMenuItem camposToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem nombreToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem apellidosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem estacinToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem categoraToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem estatusToolStripMenuItem2;
        //private System.Windows.Forms.ToolTip toolTipSeats;
        internal DevExpress.XtraBars.Ribbon.RibbonControl RibbonControl;
        internal DevExpress.XtraReports.UserDesigner.CommandBarItem CommandBarItem46;
        internal DevExpress.XtraReports.UserDesigner.CommandBarItem CommandBarItem48;
        internal DevExpress.XtraReports.UserDesigner.CommandBarItem CommandBarItem49;
        internal DevExpress.XtraReports.UserDesigner.CommandBarItem CommandBarItem50;
        internal DevExpress.XtraReports.UserDesigner.CommandBarItem CommandBarItem51;
        internal DevExpress.XtraReports.UserDesigner.CommandBarItem CommandBarItem52;
        internal DevExpress.XtraReports.UserDesigner.CommandBarItem CommandBarItem53;
        internal DevExpress.XtraReports.UserDesigner.CommandBarItem CommandBarItem54;
        internal DevExpress.XtraReports.UserDesigner.CommandBarItem CommandBarItem55;
        internal DevExpress.XtraReports.UserDesigner.CommandBarItem CommandBarItem56;
        internal DevExpress.XtraReports.UserDesigner.CommandBarItem CommandBarItem57;
        internal DevExpress.XtraReports.UserDesigner.CommandBarItem CommandBarItem58;
        internal DevExpress.XtraReports.UserDesigner.CommandBarItem CommandBarItem59;
        internal DevExpress.XtraReports.UserDesigner.CommandBarItem CommandBarItem60;
        internal DevExpress.XtraReports.UserDesigner.CommandBarItem CommandBarItem61;
        internal DevExpress.XtraReports.UserDesigner.CommandBarItem CommandBarItem62;
        internal DevExpress.XtraReports.UserDesigner.CommandBarItem CommandBarItem63;
        internal DevExpress.XtraReports.UserDesigner.CommandBarItem CommandBarItem64;
        internal DevExpress.XtraReports.UserDesigner.CommandBarItem CommandBarItem65;
        internal DevExpress.XtraReports.UserDesigner.CommandBarItem CommandBarItem66;
        internal DevExpress.XtraReports.UserDesigner.CommandBarItem CommandBarItem67;
        internal DevExpress.XtraReports.UserDesigner.CommandBarItem CommandBarItem68;
        internal DevExpress.XtraReports.UserDesigner.CommandBarItem CommandBarItem69;
        internal DevExpress.XtraReports.UserDesigner.CommandBarItem CommandBarItem70;
        internal DevExpress.XtraReports.UserDesigner.CommandBarItem CommandBarItem71;
        internal DevExpress.XtraReports.UserDesigner.CommandColorBarItem CommandColorBarItem3;
        internal DevExpress.XtraReports.UserDesigner.CommandColorBarItem CommandColorBarItem4;
        internal DevExpress.XtraReports.UserDesigner.CommandBarItem CommandBarItem72;
        internal DevExpress.XtraReports.UserDesigner.CommandBarItem CommandBarItem73;
        internal DevExpress.XtraReports.UserDesigner.CommandBarItem CommandBarItem74;
        internal DevExpress.XtraReports.UserDesigner.CommandBarItem CommandBarItem75;
        internal DevExpress.XtraReports.UserDesigner.CommandBarItem CommandBarItem76;
        internal DevExpress.XtraReports.UserDesigner.CommandBarItem CommandBarItem77;
        internal DevExpress.XtraReports.UserDesigner.CommandBarItem CommandBarItem78;
        internal DevExpress.XtraReports.UserDesigner.CommandBarItem CommandBarItem79;
        internal DevExpress.XtraReports.UserDesigner.CommandBarItem CommandBarItem80;
        internal DevExpress.XtraReports.UserDesigner.CommandBarItem CommandBarItem81;
        internal DevExpress.XtraReports.UserDesigner.CommandBarItem CommandBarItem82;
        internal DevExpress.XtraReports.UserDesigner.CommandBarItem commandBarItemIndicators;
        internal DevExpress.XtraReports.UserDesigner.CommandBarItem CommandBarItem86;
        internal DevExpress.XtraReports.UserDesigner.CommandBarItem CommandBarItem90;
        internal DevExpress.XtraReports.UserDesigner.BarDockPanelsListItem BarDockPanelsListItem2;
        internal DevExpress.XtraReports.UserDesigner.XRDesignBarButtonGroup XrDesignBarButtonGroup4;
        internal DevExpress.XtraReports.UserDesigner.XRDesignBarButtonGroup XrDesignBarButtonGroup5;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem barButtonItemCopy;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem barButtonItemCut;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItemPersonalfile;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItemPersonalFileObs;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItemModifyCategory;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem6;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem barItemCreateCourses;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem barItemDeleteCourses;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem9;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem10;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem11;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem12;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem13;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem14;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem barButtonItemPaste;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem19;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem20;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem21;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem22;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem23;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem24;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem barButtonOperatorTrainingAssign;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem barButtonTrainingCoursesSchedule;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem28;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem29;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem30;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem31;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem32;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem33;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem34;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem35;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem36;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem37;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem38;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem39;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem40;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem41;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem42;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem barItemNotificationDistribution;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem PrintPreviewStaticItem1;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem PrintPreviewStaticItem2;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem PrintPreviewStaticItem3;
        internal DevExpress.XtraBars.BarButtonItem CommandBar;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemOperatorActivities;
        internal DevExpress.XtraBars.BarEditItem BarEditItem4;
        internal DevExpress.XtraBars.BarEditItem BarEditItem6;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem3;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItemSendMonitor;
        internal DevExpress.XtraBars.BarListItem BarListItem2;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem5;
        internal DevExpress.XtraBars.BarButtonGroup BarButtonGroup1;
        internal DevExpress.XtraBars.BarCheckItem BarCheckItem2;
        internal DevExpress.XtraBars.BarCheckItem BarCheckItem3;
        internal DevExpress.XtraBars.BarCheckItem BarCheckItem4;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItemSelectRoom;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemForecast;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemAddForecast;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemDeleteForecast;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemUpdateForecast;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemDuplicate;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemReplace;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemFinalize;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItemHistoricGraphic;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItemHistoryDetails;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem17;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem18;
        internal DevExpress.XtraBars.BarEditItem BarEditItem7;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemEvaluateOperator;
        internal DevExpress.XtraBars.BarSubItem BarSubItem16;
        internal DevExpress.XtraBars.BarSubItem BarSubItem17;
        internal DevExpress.XtraBars.BarStaticItem BarStaticItem2;
        internal DevExpress.XtraBars.BarStaticItem BarStaticItem3;
        internal DevExpress.XtraBars.BarStaticItem BarStaticItem4;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem25;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem26;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem27;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem28;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem29;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem30;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem31;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem32;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem33;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem34;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem35;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem36;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem37;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem38;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem39;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem40;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem41;
        internal DevExpress.XtraReports.UserDesigner.XRDesignRibbonPage RibbonPageMonitoring;
        internal DevExpress.XtraReports.UserDesigner.XRDesignRibbonPageGroup ribbonPageGroupPerformance;
        internal DevExpress.XtraReports.UserDesigner.XRDesignRibbonPageGroup ribbonPageGroupOperator;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPage RibbonPageOperation;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup ribbonPageGroupDistribution;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup ribbonPageGroupPersonalFile;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup ribbonPageGroupTraining;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup ribbonPageGroupFinalReport;
        internal DevExpress.XtraReports.UserDesigner.XRHtmlRibbonPage RibbonPageTools;
        internal DevExpress.XtraReports.UserDesigner.XRDesignRibbonPageGroup ribbonPageGroupPreferences;
        internal DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupAccessProfile;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel;
        private DevExpress.XtraBars.BarButtonItem barButtonItemExit;
        private System.Windows.Forms.ImageList imageList;
        private DevExpress.XtraBars.BarButtonGroup barButtonGroup2;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripDesigner;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager xtraTabbedMdiManager;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit repositoryItemTimeEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemModifyCourses;
        private DevExpress.XtraBars.BarButtonItem barButtonItemHelp;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBoxWorkshift;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupGeneralOptMonitor;
        private DevExpress.XtraBars.BarButtonItem barButtonRefreshChart;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit1;
        private DevExpress.XtraBars.PopupMenu popupMenuDispatch;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemUndo;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemEndReport;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemRedo;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemReport;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemPreferences;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemCloseReportsReader;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemConfigureIndicators;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemOperAffectIndicators;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemSave;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemPrint;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemRefreshGraphic;
        internal DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupAssignment;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemOperatorAssign;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemCourses;
        internal DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupOptions;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemUpdate;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemSend;
        internal DevExpress.XtraBars.BarCheckItem barCheckItemFirstLevel;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemDispatch;
        internal DevExpress.XtraBars.BarMdiChildrenListItem barMdiChildrenListItemSelect;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupExtraTime;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupVacationAndAbsence;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemVacationsAndAbsences;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemCreateVacations;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemDeleteVacations;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemModifyVacations;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemExtraTime;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemCreateExtraTime;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemDeleteExtraTime;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemModifyExtraTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBoxSelectRoom;
        internal DevExpress.XtraBars.BarEditItem barEditItemSelectRoom;
        internal DevExpress.XtraBars.BarCheckItem barCheckItemViewSelected;
        internal DevExpress.XtraBars.BarCheckItem barCheckItemViewAll;
        internal DevExpress.XtraBars.BarButtonGroup barButtonGroupGeneralOptionsMonitor;
        private DevExpress.XtraBars.BarSubItem barSubItemChartControl;
        private DevExpress.XtraBars.BarCheckItem barCheckItemUpTreshold;
        private DevExpress.XtraBars.BarCheckItem barCheckItemDownTreshold;
        private DevExpress.XtraBars.BarCheckItem barCheckItemTrend;
        private DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup repositoryItemRadioGroup2;
        private DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup repositoryItemRadioGroup1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup repositoryItemRadioGroup3;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemOperatorPerformance;
        internal DevExpress.XtraBars.BarEditItem barEditItemIndicatorSelection;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemPersonalAssignExtraTime;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemPersonalAssignVacations;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemConsultVacations;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemConsultExtraTime;        
        private DevExpress.XtraBars.BarStaticItem barStaticItemLogo;
		private System.Windows.Forms.Label labelLogo;
		internal DevExpress.XtraBars.BarButtonItem barButtonItemChat;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemRemoteControl;
        internal DevExpress.XtraBars.BarCheckItem barCheckItemTakeControl;
        internal DevExpress.XtraBars.BarCheckItem barCheckItemScaledView;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemReConnect;
        private DevExpress.XtraBars.BarButtonItem barButtonItemAssign;
        private DevExpress.XtraBars.BarButtonItem barButtonItemRemove;
        private DevExpress.XtraBars.BarButtonItem barButtonItemOpen;
        private DevExpress.XtraBars.BarButtonItem barButtonItemLogo;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItem5;
        private DevExpress.XtraBars.BarStaticItem barStaticItem6;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupCallInfo;
        public DevExpress.XtraBars.BarButtonItem barButtonItemStartRegIncident;
        public DevExpress.XtraBars.BarButtonItem barButtonItemCreateNewIncident;
        private DevExpress.XtraBars.BarButtonItem barButtonItemCancelIncident;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupApplications;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItemHistory;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupGeneralOptions;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.BarButtonItem barButtonItemRefresh;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupTools;
        private DevExpress.XtraBars.BarButtonItem barButtonItemOpenMap;
        public DevExpress.XtraBars.BarButtonItem barButtonItem6;
        private DevExpress.XtraBars.BarButtonItem barButtonItem7;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        public DevExpress.XtraBars.BarButtonItem barButtonItem8;
        public DevExpress.XtraBars.BarButtonItem barButtonItem9;
        private DevExpress.XtraBars.BarButtonItem barButtonItem10;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem11;
        private DevExpress.XtraBars.BarButtonItem barButtonItem12;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem13;
        private DevExpress.XtraBars.BarButtonItem barButtonItem14;
        private DevExpress.XtraBars.BarButtonItem barButtonItem15;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup5;
        private DevExpress.XtraBars.BarButtonItem barButtonItem16;
        public DevExpress.XtraBars.BarButtonItem barButtonItem19;
        private DevExpress.XtraBars.BarButtonItem barButtonItem20;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar1;
        private DevExpress.XtraBars.BarStaticItem labelUser;
        private DevExpress.XtraBars.BarStaticItem labelConnected;
        private DevExpress.XtraBars.BarStaticItem labelDate;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage3;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup6;
        public DevExpress.XtraBars.BarButtonItem barButtonItem21;
        public DevExpress.XtraBars.BarButtonItem barButtonItem22;
        private DevExpress.XtraBars.BarButtonItem barButtonItem23;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup7;
        private DevExpress.XtraBars.BarButtonItem barButtonItem24;
        private DevExpress.XtraBars.BarButtonItem barButtonItem42;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup8;
        private DevExpress.XtraBars.BarButtonItem barButtonItem43;
        private DevExpress.XtraBars.BarButtonItem barButtonItem44;
        private DevExpress.XtraBars.BarButtonItem barButtonItem45;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup9;
        private DevExpress.XtraBars.BarButtonItem barButtonItem46;
        public DevExpress.XtraBars.BarButtonItem barButtonItem47;
        private DevExpress.XtraBars.BarButtonItem barButtonItem48;
    }
}
