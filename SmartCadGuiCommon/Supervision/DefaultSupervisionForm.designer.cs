

using SmartCadControls;
using SmartCadControls.Controls;
using SmartCadGuiCommon.Controls;


namespace SmartCadGuiCommon
{
    partial class DefaultSupervisionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraCharts.XYDiagram xyDiagram2 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.Series series13 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.PointSeriesLabel pointSeriesLabel6 = new DevExpress.XtraCharts.PointSeriesLabel();
            DevExpress.XtraCharts.LineSeriesView lineSeriesView6 = new DevExpress.XtraCharts.LineSeriesView();
            DevExpress.XtraCharts.Series series14 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.PointSeriesLabel pointSeriesLabel7 = new DevExpress.XtraCharts.PointSeriesLabel();
            DevExpress.XtraCharts.LineSeriesView lineSeriesView7 = new DevExpress.XtraCharts.LineSeriesView();
            DevExpress.XtraCharts.Series series15 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.PointSeriesLabel pointSeriesLabel8 = new DevExpress.XtraCharts.PointSeriesLabel();
            DevExpress.XtraCharts.LineSeriesView lineSeriesView8 = new DevExpress.XtraCharts.LineSeriesView();
            DevExpress.XtraCharts.Series series16 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.PointSeriesLabel pointSeriesLabel9 = new DevExpress.XtraCharts.PointSeriesLabel();
            DevExpress.XtraCharts.LineSeriesView lineSeriesView9 = new DevExpress.XtraCharts.LineSeriesView();
            DevExpress.XtraCharts.PointSeriesLabel pointSeriesLabel10 = new DevExpress.XtraCharts.PointSeriesLabel();
            DevExpress.XtraCharts.LineSeriesView lineSeriesView10 = new DevExpress.XtraCharts.LineSeriesView();
            DevExpress.XtraCharts.GanttDiagram ganttDiagram2 = new DevExpress.XtraCharts.GanttDiagram();
            DevExpress.XtraCharts.ConstantLine constantLine3 = new DevExpress.XtraCharts.ConstantLine();
            DevExpress.XtraCharts.RectangleGradientFillOptions rectangleGradientFillOptions2 = new DevExpress.XtraCharts.RectangleGradientFillOptions();
            DevExpress.XtraCharts.ConstantLine constantLine4 = new DevExpress.XtraCharts.ConstantLine();
            DevExpress.XtraCharts.Series series17 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.RangeBarSeriesLabel rangeBarSeriesLabel10 = new DevExpress.XtraCharts.RangeBarSeriesLabel();
            DevExpress.XtraCharts.RangeBarPointOptions rangeBarPointOptions6 = new DevExpress.XtraCharts.RangeBarPointOptions();
            DevExpress.XtraCharts.OverlappedGanttSeriesView overlappedGanttSeriesView10 = new DevExpress.XtraCharts.OverlappedGanttSeriesView();
            DevExpress.XtraCharts.Series series18 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.RangeBarSeriesLabel rangeBarSeriesLabel11 = new DevExpress.XtraCharts.RangeBarSeriesLabel();
            DevExpress.XtraCharts.OverlappedGanttSeriesView overlappedGanttSeriesView11 = new DevExpress.XtraCharts.OverlappedGanttSeriesView();
            DevExpress.XtraCharts.Series series19 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.RangeBarSeriesLabel rangeBarSeriesLabel12 = new DevExpress.XtraCharts.RangeBarSeriesLabel();
            DevExpress.XtraCharts.OverlappedGanttSeriesView overlappedGanttSeriesView12 = new DevExpress.XtraCharts.OverlappedGanttSeriesView();
            DevExpress.XtraCharts.Series series20 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.RangeBarSeriesLabel rangeBarSeriesLabel13 = new DevExpress.XtraCharts.RangeBarSeriesLabel();
            DevExpress.XtraCharts.OverlappedGanttSeriesView overlappedGanttSeriesView13 = new DevExpress.XtraCharts.OverlappedGanttSeriesView();
            DevExpress.XtraCharts.Series series21 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.RangeBarSeriesLabel rangeBarSeriesLabel14 = new DevExpress.XtraCharts.RangeBarSeriesLabel();
            DevExpress.XtraCharts.RangeBarPointOptions rangeBarPointOptions7 = new DevExpress.XtraCharts.RangeBarPointOptions();
            DevExpress.XtraCharts.RangeBarPointOptions rangeBarPointOptions8 = new DevExpress.XtraCharts.RangeBarPointOptions();
            DevExpress.XtraCharts.OverlappedGanttSeriesView overlappedGanttSeriesView14 = new DevExpress.XtraCharts.OverlappedGanttSeriesView();
            DevExpress.XtraCharts.Series series22 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.RangeBarSeriesLabel rangeBarSeriesLabel15 = new DevExpress.XtraCharts.RangeBarSeriesLabel();
            DevExpress.XtraCharts.RangeBarPointOptions rangeBarPointOptions9 = new DevExpress.XtraCharts.RangeBarPointOptions();
            DevExpress.XtraCharts.RangeBarPointOptions rangeBarPointOptions10 = new DevExpress.XtraCharts.RangeBarPointOptions();
            DevExpress.XtraCharts.OverlappedGanttSeriesView overlappedGanttSeriesView15 = new DevExpress.XtraCharts.OverlappedGanttSeriesView();
            DevExpress.XtraCharts.Series series23 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.RangeBarSeriesLabel rangeBarSeriesLabel16 = new DevExpress.XtraCharts.RangeBarSeriesLabel();
            DevExpress.XtraCharts.OverlappedGanttSeriesView overlappedGanttSeriesView16 = new DevExpress.XtraCharts.OverlappedGanttSeriesView();
            DevExpress.XtraCharts.Series series24 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.RangeBarSeriesLabel rangeBarSeriesLabel17 = new DevExpress.XtraCharts.RangeBarSeriesLabel();
            DevExpress.XtraCharts.OverlappedGanttSeriesView overlappedGanttSeriesView17 = new DevExpress.XtraCharts.OverlappedGanttSeriesView();
            DevExpress.XtraCharts.RangeBarSeriesLabel rangeBarSeriesLabel18 = new DevExpress.XtraCharts.RangeBarSeriesLabel();
            DevExpress.XtraCharts.OverlappedGanttSeriesView overlappedGanttSeriesView18 = new DevExpress.XtraCharts.OverlappedGanttSeriesView();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DefaultSupervisionForm));
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            this.gridControlExOperators = new GridControlEx();
            this.gridView1 = new GridViewEx();
            this.labelExShowOperators = new LabelEx();
            this.radioButtonNoConOperators = new System.Windows.Forms.RadioButton();
            this.radioButtonConOperators = new System.Windows.Forms.RadioButton();
            this.radioButtonAllOperators = new System.Windows.Forms.RadioButton();
            this.indicatorDashBoardControlDepartment = new IndicatorDashBoardControl();
            this.indicatorDashBoardControlSystem = new IndicatorDashBoardControl();
            this.indicatorDashBoardControlGroup = new IndicatorDashBoardControl();
            this.labelControlAbsent = new DevExpress.XtraEditors.LabelControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.chartControl1 = new DevExpress.XtraCharts.ChartControl();
            this.indicatorDashBoardControlOperator = new IndicatorDashBoardControl();
            this.indicatorGridControl1 = new IndicatorGridControl();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.labelControlBusy = new DevExpress.XtraEditors.LabelControl();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.labelControlAvailable = new DevExpress.XtraEditors.LabelControl();
            this.textEditRoomDes = new DevExpress.XtraEditors.MemoEdit();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.labelControlReunion = new DevExpress.XtraEditors.LabelControl();
            this.labelControlRest = new DevExpress.XtraEditors.LabelControl();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.textEditRoomName = new DevExpress.XtraEditors.TextEdit();
            this.labelControlBathRoom = new DevExpress.XtraEditors.LabelControl();
            this.panelRoomDesing = new System.Windows.Forms.Panel();
            this.panelControlNoRoom = new System.Windows.Forms.Panel();
            this.labelControlNoRoom = new DevExpress.XtraEditors.LabelControl();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.chartControlOperatorStatuses = new DevExpress.XtraCharts.ChartControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupOperators = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemGrid = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupPerformance = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroupDashboard = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroupSystem = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemDashboard = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupDepartment = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemDepartment = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupMyGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemMyGroup = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabbedControlGroupMonitorActivities = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroupOperatorPerformance = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemGridPerformance = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemChartControl = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupDashBoard = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItemDashBoardRight = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItemDashboardOperator = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItemDashBoardLeft = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupOperatorActivities = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemChart = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupRoom = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItemRoomRight = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupLegend = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupRoomInfo = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemRoomName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemRoomDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItemRoomLeft = new DevExpress.XtraLayout.EmptySpaceItem();
            this.barButtonGroupGeneralOptions = new DevExpress.XtraBars.BarButtonGroup();
            this.toolTipSeats = new System.Windows.Forms.ToolTip(this.components);
            this.contextMenuStripDesigner = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.printingSystem = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.printableComponentLink = new DevExpress.XtraPrinting.PrintableComponentLink(this.components);
            this.RibbonControl = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItemOperatorActivities = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSave = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemRefreshGraphic = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItemSelectRoom = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItemSelectRoom = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemComboBoxSelectRoom = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.barCheckItemViewSelected = new DevExpress.XtraBars.BarCheckItem();
            this.barCheckItemViewAll = new DevExpress.XtraBars.BarCheckItem();
            this.BarButtonItemSendMonitor = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemOperatorPerformance = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItemIndicatorSelection = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemRadioGroup1 = new DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup();
            this.barSubItemOperatorChartControl = new DevExpress.XtraBars.BarSubItem();
            this.barCheckItemUpTreshold = new DevExpress.XtraBars.BarCheckItem();
            this.barCheckItemDownTreshold = new DevExpress.XtraBars.BarCheckItem();
            this.barCheckItemTrend = new DevExpress.XtraBars.BarCheckItem();
            this.barCheckItem4 = new DevExpress.XtraBars.BarCheckItem();
            this.barButtonItemChat = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemRemoteControl = new DevExpress.XtraBars.BarButtonItem();
            this.barCheckItemTakeControl = new DevExpress.XtraBars.BarCheckItem();
            this.barCheckItemScaleView = new DevExpress.XtraBars.BarCheckItem();
            this.barButtonItemReConnect = new DevExpress.XtraBars.BarButtonItem();
            this.RibbonPageMonitoring = new DevExpress.XtraReports.UserDesigner.XRDesignRibbonPage();
            this.ribbonPageGroupOperator = new DevExpress.XtraReports.UserDesigner.XRDesignRibbonPageGroup();
            this.ribbonPageGroupGeneralOptMonitor = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.repositoryItemDateEdit = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemComboBoxWorkShift = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemRadioGroup2 = new DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup();
            this.barSubItemChartControl = new DevExpress.XtraBars.BarSubItem();
            this.alertControl1 = new DevExpress.XtraBars.Alerter.AlertControl(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExOperators)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditRoomDes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditRoomName.Properties)).BeginInit();
            this.panelRoomDesing.SuspendLayout();
            this.panelControlNoRoom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControlOperatorStatuses)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(ganttDiagram2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(rangeBarSeriesLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(overlappedGanttSeriesView10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(rangeBarSeriesLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(overlappedGanttSeriesView11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(rangeBarSeriesLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(overlappedGanttSeriesView12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(rangeBarSeriesLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(overlappedGanttSeriesView13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(rangeBarSeriesLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(overlappedGanttSeriesView14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(rangeBarSeriesLabel15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(overlappedGanttSeriesView15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(rangeBarSeriesLabel16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(overlappedGanttSeriesView16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(rangeBarSeriesLabel17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(overlappedGanttSeriesView17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(rangeBarSeriesLabel18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(overlappedGanttSeriesView18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupOperators)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupPerformance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroupDashboard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupSystem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDashboard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupDepartment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupMyGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemMyGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroupMonitorActivities)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupOperatorPerformance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGridPerformance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemChartControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupDashBoard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItemDashBoardRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDashboardOperator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItemDashBoardLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupOperatorActivities)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupRoom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItemRoomRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupLegend)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupRoomInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemRoomName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemRoomDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItemRoomLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printableComponentLink.ImageCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RibbonControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBoxSelectRoom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBoxWorkShift)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup2)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControlExOperators
            // 
            this.gridControlExOperators.EnableAutoFilter = false;
            this.gridControlExOperators.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExOperators.Location = new System.Drawing.Point(7, 58);
            this.gridControlExOperators.MainView = this.gridView1;
            this.gridControlExOperators.Name = "gridControlExOperators";
            this.gridControlExOperators.Size = new System.Drawing.Size(618, 345);
            this.gridControlExOperators.TabIndex = 28;
            this.gridControlExOperators.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            this.gridControlExOperators.ViewTotalRows = false;
            // 
            // gridView1
            // 
            this.gridView1.AllowFocusedRowChanged = true;
            this.gridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView1.EnablePreviewLineForFocusedRow = false;
            this.gridView1.GridControl = this.gridControlExOperators;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView1.ViewTotalRows = false;
            // 
            // labelExShowOperators
            // 
            this.labelExShowOperators.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExShowOperators.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExShowOperators.Location = new System.Drawing.Point(10, 30);
            this.labelExShowOperators.Name = "labelExShowOperators";
            this.labelExShowOperators.Size = new System.Drawing.Size(59, 26);
            this.labelExShowOperators.TabIndex = 14;
            this.labelExShowOperators.Text = "Mostrar:";
            this.labelExShowOperators.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // radioButtonNoConOperators
            // 
            this.radioButtonNoConOperators.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonNoConOperators.Location = new System.Drawing.Point(270, 30);
            this.radioButtonNoConOperators.Name = "radioButtonNoConOperators";
            this.radioButtonNoConOperators.Size = new System.Drawing.Size(137, 26);
            this.radioButtonNoConOperators.TabIndex = 17;
            this.radioButtonNoConOperators.Text = "No conectados";
            this.radioButtonNoConOperators.UseVisualStyleBackColor = true;
            this.radioButtonNoConOperators.CheckedChanged += new System.EventHandler(this.RadioButtonOperatorsStatus_Checked);
            // 
            // radioButtonConOperators
            // 
            this.radioButtonConOperators.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonConOperators.Location = new System.Drawing.Point(165, 30);
            this.radioButtonConOperators.Name = "radioButtonConOperators";
            this.radioButtonConOperators.Size = new System.Drawing.Size(95, 26);
            this.radioButtonConOperators.TabIndex = 16;
            this.radioButtonConOperators.Text = "Conectados";
            this.radioButtonConOperators.UseVisualStyleBackColor = true;
            this.radioButtonConOperators.CheckedChanged += new System.EventHandler(this.RadioButtonOperatorsStatus_Checked);
            // 
            // radioButtonAllOperators
            // 
            this.radioButtonAllOperators.Checked = true;
            this.radioButtonAllOperators.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonAllOperators.Location = new System.Drawing.Point(79, 30);
            this.radioButtonAllOperators.Name = "radioButtonAllOperators";
            this.radioButtonAllOperators.Size = new System.Drawing.Size(76, 26);
            this.radioButtonAllOperators.TabIndex = 15;
            this.radioButtonAllOperators.TabStop = true;
            this.radioButtonAllOperators.Text = "Todos";
            this.radioButtonAllOperators.UseVisualStyleBackColor = true;
            this.radioButtonAllOperators.CheckedChanged += new System.EventHandler(this.RadioButtonOperatorsStatus_Checked);
            // 
            // indicatorDashBoardControlDepartment
            // 
            this.indicatorDashBoardControlDepartment.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.indicatorDashBoardControlDepartment.BackColor = System.Drawing.Color.White;
            this.indicatorDashBoardControlDepartment.DashBoardType = IndicatorDashBoardControl.DashBoardClassType.DEPARTMENT;
            this.indicatorDashBoardControlDepartment.IsFirstLevel = false;
            this.indicatorDashBoardControlDepartment.IsGeneralSupervisor = false;
            this.indicatorDashBoardControlDepartment.Location = new System.Drawing.Point(645, 53);
            this.indicatorDashBoardControlDepartment.Name = "indicatorDashBoardControlDepartment";
            this.indicatorDashBoardControlDepartment.Size = new System.Drawing.Size(612, 347);
            this.indicatorDashBoardControlDepartment.TabIndex = 0;
            this.indicatorDashBoardControlDepartment.TitleFirstGroup = "Sistema";
            this.indicatorDashBoardControlDepartment.TitleSecondGroup = "First Level";
            this.indicatorDashBoardControlDepartment.TitleThirdGroup = "Dispatch";
            // 
            // indicatorDashBoardControlSystem
            // 
            this.indicatorDashBoardControlSystem.BackColor = System.Drawing.Color.White;
            this.indicatorDashBoardControlSystem.DashBoardType = IndicatorDashBoardControl.DashBoardClassType.SYSTEM;
            this.indicatorDashBoardControlSystem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.indicatorDashBoardControlSystem.IsFirstLevel = false;
            this.indicatorDashBoardControlSystem.IsGeneralSupervisor = false;
            this.indicatorDashBoardControlSystem.Location = new System.Drawing.Point(645, 53);
            this.indicatorDashBoardControlSystem.Name = "indicatorDashBoardControlSystem";
            this.indicatorDashBoardControlSystem.Size = new System.Drawing.Size(612, 347);
            this.indicatorDashBoardControlSystem.TabIndex = 0;
            this.indicatorDashBoardControlSystem.TitleFirstGroup = "Call";
            this.indicatorDashBoardControlSystem.TitleSecondGroup = "First Level";
            this.indicatorDashBoardControlSystem.TitleThirdGroup = "Dispatch";
            // 
            // indicatorDashBoardControlGroup
            // 
            this.indicatorDashBoardControlGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.indicatorDashBoardControlGroup.BackColor = System.Drawing.Color.White;
            this.indicatorDashBoardControlGroup.DashBoardType = IndicatorDashBoardControl.DashBoardClassType.GROUP;
            this.indicatorDashBoardControlGroup.IsFirstLevel = false;
            this.indicatorDashBoardControlGroup.IsGeneralSupervisor = false;
            this.indicatorDashBoardControlGroup.Location = new System.Drawing.Point(645, 53);
            this.indicatorDashBoardControlGroup.Name = "indicatorDashBoardControlGroup";
            this.indicatorDashBoardControlGroup.Size = new System.Drawing.Size(612, 347);
            this.indicatorDashBoardControlGroup.TabIndex = 0;
            this.indicatorDashBoardControlGroup.TitleFirstGroup = "Call";
            this.indicatorDashBoardControlGroup.TitleSecondGroup = "First Level";
            this.indicatorDashBoardControlGroup.TitleThirdGroup = "Dispatch";
            // 
            // labelControlAbsent
            // 
            this.labelControlAbsent.Location = new System.Drawing.Point(965, 588);
            this.labelControlAbsent.Name = "labelControlAbsent";
            this.labelControlAbsent.Size = new System.Drawing.Size(202, 25);
            this.labelControlAbsent.StyleController = this.layoutControl1;
            this.labelControlAbsent.TabIndex = 16;
            this.labelControlAbsent.Text = "Absent";
            this.labelControlAbsent.Click += new System.EventHandler(this.labelControlAbsent_Click);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.gridControlExOperators);
            this.layoutControl1.Controls.Add(this.labelExShowOperators);
            this.layoutControl1.Controls.Add(this.radioButtonAllOperators);
            this.layoutControl1.Controls.Add(this.radioButtonNoConOperators);
            this.layoutControl1.Controls.Add(this.radioButtonConOperators);
            this.layoutControl1.Controls.Add(this.chartControl1);
            this.layoutControl1.Controls.Add(this.indicatorDashBoardControlOperator);
            this.layoutControl1.Controls.Add(this.indicatorGridControl1);
            this.layoutControl1.Controls.Add(this.indicatorDashBoardControlSystem);
            this.layoutControl1.Controls.Add(this.indicatorDashBoardControlGroup);
            this.layoutControl1.Controls.Add(this.pictureBox3);
            this.layoutControl1.Controls.Add(this.labelControlBusy);
            this.layoutControl1.Controls.Add(this.pictureBox2);
            this.layoutControl1.Controls.Add(this.pictureBox1);
            this.layoutControl1.Controls.Add(this.labelControlAvailable);
            this.layoutControl1.Controls.Add(this.labelControlAbsent);
            this.layoutControl1.Controls.Add(this.textEditRoomDes);
            this.layoutControl1.Controls.Add(this.pictureBox6);
            this.layoutControl1.Controls.Add(this.labelControlReunion);
            this.layoutControl1.Controls.Add(this.labelControlRest);
            this.layoutControl1.Controls.Add(this.pictureBox5);
            this.layoutControl1.Controls.Add(this.textEditRoomName);
            this.layoutControl1.Controls.Add(this.labelControlBathRoom);
            this.layoutControl1.Controls.Add(this.panelRoomDesing);
            this.layoutControl1.Controls.Add(this.pictureBox4);
            this.layoutControl1.Controls.Add(this.indicatorDashBoardControlDepartment);
            this.layoutControl1.Controls.Add(this.chartControlOperatorStatuses);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(46, 121, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1270, 780);
            this.layoutControl1.TabIndex = 18;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // chartControl1
            // 
            xyDiagram2.AxisX.DateTimeGridAlignment = DevExpress.XtraCharts.DateTimeMeasurementUnit.Minute;
            xyDiagram2.AxisX.DateTimeMeasureUnit = DevExpress.XtraCharts.DateTimeMeasurementUnit.Minute;
            xyDiagram2.AxisX.DateTimeOptions.Format = DevExpress.XtraCharts.DateTimeFormat.ShortTime;
            xyDiagram2.AxisX.MinorCount = 10;
            xyDiagram2.AxisX.Range.ScrollingRange.SideMarginsEnabled = true;
            xyDiagram2.AxisX.Range.SideMarginsEnabled = true;
            xyDiagram2.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram2.AxisY.Range.ScrollingRange.SideMarginsEnabled = true;
            xyDiagram2.AxisY.Range.SideMarginsEnabled = true;
            xyDiagram2.AxisY.VisibleInPanesSerializable = "-1";
            this.chartControl1.Diagram = xyDiagram2;
            this.chartControl1.Legend.AlignmentHorizontal = DevExpress.XtraCharts.LegendAlignmentHorizontal.Center;
            this.chartControl1.Legend.AlignmentVertical = DevExpress.XtraCharts.LegendAlignmentVertical.BottomOutside;
            this.chartControl1.Legend.Direction = DevExpress.XtraCharts.LegendDirection.LeftToRight;
            this.chartControl1.Legend.EquallySpacedItems = false;
            this.chartControl1.Location = new System.Drawing.Point(484, 440);
            this.chartControl1.Name = "chartControl1";
            series13.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.DateTime;
            pointSeriesLabel6.LineVisible = true;
            pointSeriesLabel6.Visible = false;
            series13.Label = pointSeriesLabel6;
            series13.LegendText = " ";
            series13.Name = "Calls";
            lineSeriesView6.Color = System.Drawing.Color.Black;
            lineSeriesView6.LineMarkerOptions.Size = 4;
            lineSeriesView6.LineStyle.DashStyle = DevExpress.XtraCharts.DashStyle.Dot;
            lineSeriesView6.LineStyle.Thickness = 1;
            series13.View = lineSeriesView6;
            series14.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.DateTime;
            pointSeriesLabel7.LineVisible = true;
            pointSeriesLabel7.Visible = false;
            series14.Label = pointSeriesLabel7;
            series14.LegendText = "Trend";
            series14.Name = "Trend";
            lineSeriesView7.Color = System.Drawing.Color.Blue;
            lineSeriesView7.LineMarkerOptions.Size = 4;
            lineSeriesView7.LineStyle.DashStyle = DevExpress.XtraCharts.DashStyle.Dot;
            lineSeriesView7.LineStyle.Thickness = 1;
            series14.View = lineSeriesView7;
            series15.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.DateTime;
            pointSeriesLabel8.LineVisible = true;
            pointSeriesLabel8.Visible = false;
            series15.Label = pointSeriesLabel8;
            series15.LegendText = "Upper Threshold";
            series15.Name = "Upper Threshold";
            lineSeriesView8.Color = System.Drawing.Color.DarkOrange;
            lineSeriesView8.LineMarkerOptions.Size = 4;
            lineSeriesView8.LineStyle.DashStyle = DevExpress.XtraCharts.DashStyle.Dot;
            lineSeriesView8.LineStyle.Thickness = 1;
            series15.View = lineSeriesView8;
            series16.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.DateTime;
            pointSeriesLabel9.LineVisible = true;
            pointSeriesLabel9.Visible = false;
            series16.Label = pointSeriesLabel9;
            series16.LegendText = "Lower Threshold";
            series16.Name = "Lower Threshold";
            lineSeriesView9.Color = System.Drawing.Color.Yellow;
            lineSeriesView9.LineMarkerOptions.Size = 4;
            lineSeriesView9.LineStyle.DashStyle = DevExpress.XtraCharts.DashStyle.Dot;
            lineSeriesView9.LineStyle.Thickness = 1;
            series16.View = lineSeriesView9;
            this.chartControl1.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series13,
        series14,
        series15,
        series16};
            pointSeriesLabel10.LineVisible = true;
            this.chartControl1.SeriesTemplate.Label = pointSeriesLabel10;
            this.chartControl1.SeriesTemplate.View = lineSeriesView10;
            this.chartControl1.Size = new System.Drawing.Size(779, 333);
            this.chartControl1.TabIndex = 4;
            // 
            // indicatorDashBoardControlOperator
            // 
            this.indicatorDashBoardControlOperator.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.indicatorDashBoardControlOperator.BackColor = System.Drawing.Color.White;
            this.indicatorDashBoardControlOperator.DashBoardType = IndicatorDashBoardControl.DashBoardClassType.OPERATOR;
            this.indicatorDashBoardControlOperator.IsFirstLevel = false;
            this.indicatorDashBoardControlOperator.IsGeneralSupervisor = false;
            this.indicatorDashBoardControlOperator.Location = new System.Drawing.Point(344, 440);
            this.indicatorDashBoardControlOperator.Name = "indicatorDashBoardControlOperator";
            this.indicatorDashBoardControlOperator.Size = new System.Drawing.Size(124, 333);
            this.indicatorDashBoardControlOperator.TabIndex = 0;
            this.indicatorDashBoardControlOperator.TitleFirstGroup = "Call";
            this.indicatorDashBoardControlOperator.TitleSecondGroup = "First Level";
            this.indicatorDashBoardControlOperator.TitleThirdGroup = "Dispatch";
            // 
            // indicatorGridControl1
            // 
            this.indicatorGridControl1.DataSource = null;
            this.indicatorGridControl1.IsUpdated = false;
            this.indicatorGridControl1.Location = new System.Drawing.Point(7, 440);
            this.indicatorGridControl1.Name = "indicatorGridControl1";
            this.indicatorGridControl1.Size = new System.Drawing.Size(321, 333);
            this.indicatorGridControl1.TabIndex = 1;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::SmartCadGuiCommon.Properties.ResourcesGui.BallImage_3;
            this.pictureBox3.Location = new System.Drawing.Point(927, 500);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(31, 26);
            this.pictureBox3.TabIndex = 9;
            this.pictureBox3.TabStop = false;
            // 
            // labelControlBusy
            // 
            this.labelControlBusy.Location = new System.Drawing.Point(965, 468);
            this.labelControlBusy.Name = "labelControlBusy";
            this.labelControlBusy.Size = new System.Drawing.Size(202, 25);
            this.labelControlBusy.StyleController = this.layoutControl1;
            this.labelControlBusy.TabIndex = 6;
            this.labelControlBusy.Text = "Busy";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::SmartCadGuiCommon.Properties.ResourcesGui.BallImage_3;
            this.pictureBox2.Location = new System.Drawing.Point(927, 470);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(31, 26);
            this.pictureBox2.TabIndex = 8;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::SmartCadGuiCommon.Properties.ResourcesGui.BallImage_3;
            this.pictureBox1.Location = new System.Drawing.Point(927, 440);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(31, 26);
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // labelControlAvailable
            // 
            this.labelControlAvailable.Location = new System.Drawing.Point(965, 438);
            this.labelControlAvailable.Name = "labelControlAvailable";
            this.labelControlAvailable.Size = new System.Drawing.Size(202, 25);
            this.labelControlAvailable.StyleController = this.layoutControl1;
            this.labelControlAvailable.TabIndex = 5;
            this.labelControlAvailable.Text = "Ready";
            // 
            // textEditRoomDes
            // 
            this.textEditRoomDes.Enabled = false;
            this.textEditRoomDes.Location = new System.Drawing.Point(43, 539);
            this.textEditRoomDes.Name = "textEditRoomDes";
            this.textEditRoomDes.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEditRoomDes.Properties.Appearance.Options.UseBackColor = true;
            this.textEditRoomDes.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White;
            this.textEditRoomDes.Properties.AppearanceReadOnly.Options.UseBackColor = true;
            this.textEditRoomDes.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.textEditRoomDes.Properties.ReadOnly = true;
            this.textEditRoomDes.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.textEditRoomDes.Size = new System.Drawing.Size(233, 118);
            this.textEditRoomDes.StyleController = this.layoutControl1;
            this.textEditRoomDes.TabIndex = 9;
            this.textEditRoomDes.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::SmartCadGuiCommon.Properties.ResourcesGui.BallImage_3;
            this.pictureBox6.Location = new System.Drawing.Point(927, 590);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(31, 26);
            this.pictureBox6.TabIndex = 12;
            this.pictureBox6.TabStop = false;
            // 
            // labelControlReunion
            // 
            this.labelControlReunion.Location = new System.Drawing.Point(965, 558);
            this.labelControlReunion.Name = "labelControlReunion";
            this.labelControlReunion.Size = new System.Drawing.Size(202, 25);
            this.labelControlReunion.StyleController = this.layoutControl1;
            this.labelControlReunion.TabIndex = 15;
            this.labelControlReunion.Text = "Reunion";
            // 
            // labelControlRest
            // 
            this.labelControlRest.Location = new System.Drawing.Point(965, 528);
            this.labelControlRest.Name = "labelControlRest";
            this.labelControlRest.Size = new System.Drawing.Size(202, 25);
            this.labelControlRest.StyleController = this.layoutControl1;
            this.labelControlRest.TabIndex = 14;
            this.labelControlRest.Text = "Rest";
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::SmartCadGuiCommon.Properties.ResourcesGui.BallImage_3;
            this.pictureBox5.Location = new System.Drawing.Point(927, 560);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(31, 26);
            this.pictureBox5.TabIndex = 11;
            this.pictureBox5.TabStop = false;
            // 
            // textEditRoomName
            // 
            this.textEditRoomName.Enabled = false;
            this.textEditRoomName.Location = new System.Drawing.Point(43, 481);
            this.textEditRoomName.Name = "textEditRoomName";
            this.textEditRoomName.Properties.AllowFocused = false;
            this.textEditRoomName.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White;
            this.textEditRoomName.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.textEditRoomName.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.textEditRoomName.Size = new System.Drawing.Size(233, 22);
            this.textEditRoomName.StyleController = this.layoutControl1;
            this.textEditRoomName.TabIndex = 7;
            this.textEditRoomName.TabStop = false;
            // 
            // labelControlBathRoom
            // 
            this.labelControlBathRoom.Location = new System.Drawing.Point(965, 498);
            this.labelControlBathRoom.Name = "labelControlBathRoom";
            this.labelControlBathRoom.Size = new System.Drawing.Size(202, 25);
            this.labelControlBathRoom.StyleController = this.layoutControl1;
            this.labelControlBathRoom.TabIndex = 13;
            this.labelControlBathRoom.Text = "Bathroom";
            // 
            // panelRoomDesing
            // 
            this.panelRoomDesing.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelRoomDesing.Controls.Add(this.panelControlNoRoom);
            this.panelRoomDesing.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panelRoomDesing.Location = new System.Drawing.Point(288, 442);
            this.panelRoomDesing.Name = "panelRoomDesing";
            this.panelRoomDesing.Size = new System.Drawing.Size(632, 323);
            this.panelRoomDesing.TabIndex = 1;
            this.panelRoomDesing.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panelRoomDesing_MouseClick);
            // 
            // panelControlNoRoom
            // 
            this.panelControlNoRoom.BackColor = System.Drawing.Color.White;
            this.panelControlNoRoom.Controls.Add(this.labelControlNoRoom);
            this.panelControlNoRoom.Location = new System.Drawing.Point(29, 274);
            this.panelControlNoRoom.Name = "panelControlNoRoom";
            this.panelControlNoRoom.Size = new System.Drawing.Size(577, 22);
            this.panelControlNoRoom.TabIndex = 4;
            this.panelControlNoRoom.Visible = false;
            // 
            // labelControlNoRoom
            // 
            this.labelControlNoRoom.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControlNoRoom.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControlNoRoom.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControlNoRoom.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControlNoRoom.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControlNoRoom.Location = new System.Drawing.Point(3, 3);
            this.labelControlNoRoom.Name = "labelControlNoRoom";
            this.labelControlNoRoom.Size = new System.Drawing.Size(571, 18);
            this.labelControlNoRoom.TabIndex = 3;
            this.labelControlNoRoom.Text = " en una sala operadores no conectados";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::SmartCadGuiCommon.Properties.ResourcesGui.BallImage_3;
            this.pictureBox4.Location = new System.Drawing.Point(927, 530);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(31, 26);
            this.pictureBox4.TabIndex = 10;
            this.pictureBox4.TabStop = false;
            // 
            // chartControlOperatorStatuses
            // 
            ganttDiagram2.AxisX.Alignment = DevExpress.XtraCharts.AxisAlignment.Zero;
            constantLine3.AxisValueSerializable = "A";
            constantLine3.Color = System.Drawing.Color.Silver;
            constantLine3.LineStyle.DashStyle = DevExpress.XtraCharts.DashStyle.Solid;
            constantLine3.LineStyle.Thickness = 2;
            constantLine3.Name = "ConstantLineSelectedUser";
            constantLine3.ShowInLegend = false;
            constantLine3.Title.Visible = false;
            constantLine3.Visible = false;
            ganttDiagram2.AxisX.ConstantLines.AddRange(new DevExpress.XtraCharts.ConstantLine[] {
            constantLine3});
            ganttDiagram2.AxisX.InterlacedColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            ganttDiagram2.AxisX.InterlacedFillStyle.FillMode = DevExpress.XtraCharts.FillMode.Gradient;
            rectangleGradientFillOptions2.GradientMode = DevExpress.XtraCharts.RectangleGradientMode.BottomToTop;
            ganttDiagram2.AxisX.InterlacedFillStyle.Options = rectangleGradientFillOptions2;
            ganttDiagram2.AxisX.MinorCount = 1;
            ganttDiagram2.AxisX.Range.Auto = false;
            ganttDiagram2.AxisX.Range.MaxValueInternal = 9.5D;
            ganttDiagram2.AxisX.Range.MinValueInternal = -0.5D;
            ganttDiagram2.AxisX.Range.ScrollingRange.SideMarginsEnabled = true;
            ganttDiagram2.AxisX.Range.SideMarginsEnabled = true;
            ganttDiagram2.AxisX.Reverse = false;
            ganttDiagram2.AxisX.VisibleInPanesSerializable = "-1";
            constantLine4.AxisValueSerializable = "A";
            constantLine4.Color = System.Drawing.Color.Silver;
            constantLine4.LineStyle.DashStyle = DevExpress.XtraCharts.DashStyle.Solid;
            constantLine4.LineStyle.Thickness = 2;
            constantLine4.Name = "ConstantLineNow";
            constantLine4.ShowInLegend = false;
            constantLine4.Title.Visible = false;
            constantLine4.Visible = false;
            ganttDiagram2.AxisY.ConstantLines.AddRange(new DevExpress.XtraCharts.ConstantLine[] {
            constantLine4});
            ganttDiagram2.AxisY.DateTimeGridAlignment = DevExpress.XtraCharts.DateTimeMeasurementUnit.Second;
            ganttDiagram2.AxisY.DateTimeMeasureUnit = DevExpress.XtraCharts.DateTimeMeasurementUnit.Second;
            ganttDiagram2.AxisY.DateTimeOptions.Format = DevExpress.XtraCharts.DateTimeFormat.Custom;
            ganttDiagram2.AxisY.DateTimeOptions.FormatString = "HH:mm";
            ganttDiagram2.AxisY.GridLines.Visible = false;
            ganttDiagram2.AxisY.Label.Angle = -90;
            ganttDiagram2.AxisY.MinorCount = 4;
            ganttDiagram2.AxisY.Range.ScrollingRange.SideMarginsEnabled = true;
            ganttDiagram2.AxisY.Range.SideMarginsEnabled = true;
            ganttDiagram2.AxisY.VisibleInPanesSerializable = "-1";
            ganttDiagram2.DefaultPane.BackColor = System.Drawing.Color.White;
            ganttDiagram2.EnableAxisXScrolling = true;
            ganttDiagram2.EnableAxisXZooming = true;
            ganttDiagram2.EnableAxisYScrolling = true;
            ganttDiagram2.EnableAxisYZooming = true;
            this.chartControlOperatorStatuses.Diagram = ganttDiagram2;
            this.chartControlOperatorStatuses.Legend.AlignmentHorizontal = DevExpress.XtraCharts.LegendAlignmentHorizontal.Center;
            this.chartControlOperatorStatuses.Legend.AlignmentVertical = DevExpress.XtraCharts.LegendAlignmentVertical.BottomOutside;
            this.chartControlOperatorStatuses.Legend.BackColor = System.Drawing.Color.Transparent;
            this.chartControlOperatorStatuses.Legend.Border.Color = System.Drawing.Color.Blue;
            this.chartControlOperatorStatuses.Legend.Border.Thickness = 2;
            this.chartControlOperatorStatuses.Legend.Border.Visible = false;
            this.chartControlOperatorStatuses.Legend.Direction = DevExpress.XtraCharts.LegendDirection.LeftToRight;
            this.chartControlOperatorStatuses.Legend.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.chartControlOperatorStatuses.Legend.HorizontalIndent = 6;
            this.chartControlOperatorStatuses.Legend.Padding.Left = 3;
            this.chartControlOperatorStatuses.Legend.Padding.Right = 3;
            this.chartControlOperatorStatuses.Legend.TextOffset = 3;
            this.chartControlOperatorStatuses.Location = new System.Drawing.Point(7, 440);
            this.chartControlOperatorStatuses.Name = "chartControlOperatorStatuses";
            this.chartControlOperatorStatuses.PaletteName = "Status Color";
            this.chartControlOperatorStatuses.PaletteRepository.Add("Status Color", new DevExpress.XtraCharts.Palette("Status Color", DevExpress.XtraCharts.PaletteScaleMode.Repeat, new DevExpress.XtraCharts.PaletteEntry[] {
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(154)))), ((int)(((byte)(67))))), System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(154)))), ((int)(((byte)(67)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(39)))), ((int)(((byte)(39)))), ((int)(((byte)(183))))), System.Drawing.Color.FromArgb(((int)(((byte)(39)))), ((int)(((byte)(39)))), ((int)(((byte)(183)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(6)))), ((int)(((byte)(6))))), System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(6)))), ((int)(((byte)(6)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(210)))), ((int)(((byte)(255))))), System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(210)))), ((int)(((byte)(255)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(203)))), ((int)(((byte)(203))))), System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(203)))), ((int)(((byte)(203)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(5))))), System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(5)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109))))), System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(168)))), ((int)(((byte)(84))))), System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(168)))), ((int)(((byte)(84))))))}));
            rangeBarSeriesLabel10.Visible = false;
            series17.Label = rangeBarSeriesLabel10;
            rangeBarPointOptions6.ValueAsDuration = true;
            series17.LegendPointOptions = rangeBarPointOptions6;
            series17.Name = "Ready";
            series17.SynchronizePointOptions = false;
            series17.ValueScaleType = DevExpress.XtraCharts.ScaleType.DateTime;
            series17.View = overlappedGanttSeriesView10;
            rangeBarSeriesLabel11.Visible = false;
            series18.Label = rangeBarSeriesLabel11;
            series18.Name = "Busy";
            series18.ValueScaleType = DevExpress.XtraCharts.ScaleType.DateTime;
            series18.View = overlappedGanttSeriesView11;
            rangeBarSeriesLabel12.Visible = false;
            series19.Label = rangeBarSeriesLabel12;
            series19.Name = "Absent";
            series19.ValueScaleType = DevExpress.XtraCharts.ScaleType.DateTime;
            series19.View = overlappedGanttSeriesView12;
            rangeBarSeriesLabel13.Visible = false;
            series20.Label = rangeBarSeriesLabel13;
            series20.Name = "Reunion";
            series20.ValueScaleType = DevExpress.XtraCharts.ScaleType.DateTime;
            series20.View = overlappedGanttSeriesView13;
            rangeBarSeriesLabel14.Visible = false;
            series21.Label = rangeBarSeriesLabel14;
            series21.LegendPointOptions = rangeBarPointOptions7;
            series21.Name = "Rest";
            rangeBarPointOptions8.ValueAsDuration = true;
            series21.PointOptions = rangeBarPointOptions8;
            series21.SynchronizePointOptions = false;
            series21.ValueScaleType = DevExpress.XtraCharts.ScaleType.DateTime;
            series21.View = overlappedGanttSeriesView14;
            rangeBarSeriesLabel15.Visible = false;
            series22.Label = rangeBarSeriesLabel15;
            series22.LegendPointOptions = rangeBarPointOptions9;
            series22.Name = "Bathroom";
            rangeBarPointOptions10.ValueAsDuration = true;
            series22.PointOptions = rangeBarPointOptions10;
            series22.SynchronizePointOptions = false;
            series22.ValueScaleType = DevExpress.XtraCharts.ScaleType.DateTime;
            series22.View = overlappedGanttSeriesView15;
            rangeBarSeriesLabel16.Visible = false;
            series23.Label = rangeBarSeriesLabel16;
            series23.Name = "NotConnected";
            series23.ValueScaleType = DevExpress.XtraCharts.ScaleType.DateTime;
            series23.View = overlappedGanttSeriesView16;
            rangeBarSeriesLabel17.Visible = false;
            series24.Label = rangeBarSeriesLabel17;
            series24.Name = "Training";
            series24.ValueScaleType = DevExpress.XtraCharts.ScaleType.DateTime;
            overlappedGanttSeriesView17.BarWidth = 0.2D;
            series24.View = overlappedGanttSeriesView17;
            this.chartControlOperatorStatuses.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series17,
        series18,
        series19,
        series20,
        series21,
        series22,
        series23,
        series24};
            this.chartControlOperatorStatuses.SeriesTemplate.Label = rangeBarSeriesLabel18;
            this.chartControlOperatorStatuses.SeriesTemplate.View = overlappedGanttSeriesView18;
            this.chartControlOperatorStatuses.Size = new System.Drawing.Size(1256, 333);
            this.chartControlOperatorStatuses.TabIndex = 1;
            this.chartControlOperatorStatuses.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ChartControlOperatorStatuses_MouseMove);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupOperators,
            this.layoutControlGroupPerformance,
            this.tabbedControlGroupMonitorActivities});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1270, 780);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroupOperators
            // 
            this.layoutControlGroupOperators.CustomizationFormText = "layoutControlGroupOperators";
            this.layoutControlGroupOperators.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem2,
            this.layoutControlItem1,
            this.layoutControlItemGrid});
            this.layoutControlGroupOperators.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupOperators.Name = "layoutControlGroupOperators";
            this.layoutControlGroupOperators.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupOperators.Size = new System.Drawing.Size(632, 413);
            this.layoutControlGroupOperators.Text = "layoutControlGroupOperators";
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(407, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(215, 31);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.radioButtonConOperators;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(155, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(105, 31);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(105, 31);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 0);
            this.layoutControlItem3.Size = new System.Drawing.Size(105, 31);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.radioButtonNoConOperators;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(260, 0);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(147, 31);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(147, 31);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 0);
            this.layoutControlItem4.Size = new System.Drawing.Size(147, 31);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.radioButtonAllOperators;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(69, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(86, 31);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(86, 31);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 0);
            this.layoutControlItem2.Size = new System.Drawing.Size(86, 31);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.labelExShowOperators;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(69, 31);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(69, 31);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 0);
            this.layoutControlItem1.Size = new System.Drawing.Size(69, 31);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItemGrid
            // 
            this.layoutControlItemGrid.Control = this.gridControlExOperators;
            this.layoutControlItemGrid.CustomizationFormText = "layoutControlItemGrid";
            this.layoutControlItemGrid.Location = new System.Drawing.Point(0, 31);
            this.layoutControlItemGrid.Name = "layoutControlItemGrid";
            this.layoutControlItemGrid.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 5);
            this.layoutControlItemGrid.Size = new System.Drawing.Size(622, 352);
            this.layoutControlItemGrid.Text = "layoutControlItemGrid";
            this.layoutControlItemGrid.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemGrid.TextToControlDistance = 0;
            this.layoutControlItemGrid.TextVisible = false;
            // 
            // layoutControlGroupPerformance
            // 
            this.layoutControlGroupPerformance.CustomizationFormText = "layoutControlGroupPerformance";
            this.layoutControlGroupPerformance.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroupDashboard});
            this.layoutControlGroupPerformance.Location = new System.Drawing.Point(632, 0);
            this.layoutControlGroupPerformance.Name = "layoutControlGroupPerformance";
            this.layoutControlGroupPerformance.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupPerformance.Size = new System.Drawing.Size(638, 413);
            this.layoutControlGroupPerformance.Text = "layoutControlGroupPerformance";
            // 
            // tabbedControlGroupDashboard
            // 
            this.tabbedControlGroupDashboard.CustomizationFormText = "tabbedControlGroupDashboard";
            this.tabbedControlGroupDashboard.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroupDashboard.Name = "tabbedControlGroupDashboard";
            this.tabbedControlGroupDashboard.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.tabbedControlGroupDashboard.SelectedTabPage = this.layoutControlGroupSystem;
            this.tabbedControlGroupDashboard.SelectedTabPageIndex = 0;
            this.tabbedControlGroupDashboard.Size = new System.Drawing.Size(628, 383);
            this.tabbedControlGroupDashboard.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupSystem,
            this.layoutControlGroupDepartment,
            this.layoutControlGroupMyGroup});
            this.tabbedControlGroupDashboard.Text = "tabbedControlGroupDashboard";
            // 
            // layoutControlGroupSystem
            // 
            this.layoutControlGroupSystem.CustomizationFormText = "layoutControlGroupSystem";
            this.layoutControlGroupSystem.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemDashboard});
            this.layoutControlGroupSystem.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupSystem.Name = "layoutControlGroupSystem";
            this.layoutControlGroupSystem.Size = new System.Drawing.Size(616, 351);
            this.layoutControlGroupSystem.Text = "layoutControlGroupSystem";
            // 
            // layoutControlItemDashboard
            // 
            this.layoutControlItemDashboard.Control = this.indicatorDashBoardControlSystem;
            this.layoutControlItemDashboard.CustomizationFormText = "layoutControlItemDashboard";
            this.layoutControlItemDashboard.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemDashboard.MaxSize = new System.Drawing.Size(616, 351);
            this.layoutControlItemDashboard.MinSize = new System.Drawing.Size(616, 351);
            this.layoutControlItemDashboard.Name = "layoutControlItemDashboard";
            this.layoutControlItemDashboard.Size = new System.Drawing.Size(616, 351);
            this.layoutControlItemDashboard.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemDashboard.Text = "layoutControlItemDashboard";
            this.layoutControlItemDashboard.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemDashboard.TextToControlDistance = 0;
            this.layoutControlItemDashboard.TextVisible = false;
            // 
            // layoutControlGroupDepartment
            // 
            this.layoutControlGroupDepartment.CustomizationFormText = "layoutControlGroupDepartment";
            this.layoutControlGroupDepartment.GroupBordersVisible = false;
            this.layoutControlGroupDepartment.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemDepartment});
            this.layoutControlGroupDepartment.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupDepartment.Name = "layoutControlGroupDepartment";
            this.layoutControlGroupDepartment.Size = new System.Drawing.Size(616, 351);
            this.layoutControlGroupDepartment.Text = "layoutControlGroupDepartment";
            // 
            // layoutControlItemDepartment
            // 
            this.layoutControlItemDepartment.Control = this.indicatorDashBoardControlDepartment;
            this.layoutControlItemDepartment.CustomizationFormText = "layoutControlItemDepartment";
            this.layoutControlItemDepartment.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemDepartment.Name = "layoutControlItemDepartment";
            this.layoutControlItemDepartment.Size = new System.Drawing.Size(616, 351);
            this.layoutControlItemDepartment.Text = "layoutControlItemDepartment";
            this.layoutControlItemDepartment.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemDepartment.TextToControlDistance = 0;
            this.layoutControlItemDepartment.TextVisible = false;
            // 
            // layoutControlGroupMyGroup
            // 
            this.layoutControlGroupMyGroup.CustomizationFormText = "layoutControlGroupMyGroup";
            this.layoutControlGroupMyGroup.GroupBordersVisible = false;
            this.layoutControlGroupMyGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemMyGroup});
            this.layoutControlGroupMyGroup.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupMyGroup.Name = "layoutControlGroupMyGroup";
            this.layoutControlGroupMyGroup.Size = new System.Drawing.Size(616, 351);
            this.layoutControlGroupMyGroup.Text = "layoutControlGroupMyGroup";
            // 
            // layoutControlItemMyGroup
            // 
            this.layoutControlItemMyGroup.Control = this.indicatorDashBoardControlGroup;
            this.layoutControlItemMyGroup.CustomizationFormText = "layoutControlItemMyGroup";
            this.layoutControlItemMyGroup.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemMyGroup.MinSize = new System.Drawing.Size(105, 25);
            this.layoutControlItemMyGroup.Name = "layoutControlItemMyGroup";
            this.layoutControlItemMyGroup.Size = new System.Drawing.Size(616, 351);
            this.layoutControlItemMyGroup.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemMyGroup.Text = "layoutControlItemMyGroup";
            this.layoutControlItemMyGroup.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemMyGroup.TextToControlDistance = 0;
            this.layoutControlItemMyGroup.TextVisible = false;
            // 
            // tabbedControlGroupMonitorActivities
            // 
            this.tabbedControlGroupMonitorActivities.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroupMonitorActivities.Location = new System.Drawing.Point(0, 413);
            this.tabbedControlGroupMonitorActivities.Name = "tabbedControlGroupMonitorActivities";
            this.tabbedControlGroupMonitorActivities.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.tabbedControlGroupMonitorActivities.SelectedTabPage = this.layoutControlGroupOperatorPerformance;
            this.tabbedControlGroupMonitorActivities.SelectedTabPageIndex = 1;
            this.tabbedControlGroupMonitorActivities.Size = new System.Drawing.Size(1270, 367);
            this.tabbedControlGroupMonitorActivities.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupOperatorActivities,
            this.layoutControlGroupOperatorPerformance,
            this.layoutControlGroupRoom});
            this.tabbedControlGroupMonitorActivities.Text = "tabbedControlGroupMonitorActivities";
            this.tabbedControlGroupMonitorActivities.SelectedPageChanged += new DevExpress.XtraLayout.LayoutTabPageChangedEventHandler(this.tabbedControlGroupMonitorActivities_SelectedPageChanged);
            // 
            // layoutControlGroupOperatorPerformance
            // 
            this.layoutControlGroupOperatorPerformance.CustomizationFormText = "layoutControlGroupOperatorPerformance";
            this.layoutControlGroupOperatorPerformance.GroupBordersVisible = false;
            this.layoutControlGroupOperatorPerformance.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemGridPerformance,
            this.layoutControlItemChartControl,
            this.layoutControlGroupDashBoard});
            this.layoutControlGroupOperatorPerformance.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupOperatorPerformance.Name = "layoutControlGroupOperatorPerformance";
            this.layoutControlGroupOperatorPerformance.Size = new System.Drawing.Size(1260, 337);
            this.layoutControlGroupOperatorPerformance.Text = "layoutControlGroupOperatorPerformance";
            // 
            // layoutControlItemGridPerformance
            // 
            this.layoutControlItemGridPerformance.Control = this.indicatorGridControl1;
            this.layoutControlItemGridPerformance.CustomizationFormText = "layoutControlItemGridPerformance";
            this.layoutControlItemGridPerformance.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemGridPerformance.MinSize = new System.Drawing.Size(111, 31);
            this.layoutControlItemGridPerformance.Name = "layoutControlItemGridPerformance";
            this.layoutControlItemGridPerformance.Size = new System.Drawing.Size(325, 337);
            this.layoutControlItemGridPerformance.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemGridPerformance.Text = "layoutControlItemGridPerformance";
            this.layoutControlItemGridPerformance.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemGridPerformance.TextToControlDistance = 0;
            this.layoutControlItemGridPerformance.TextVisible = false;
            // 
            // layoutControlItemChartControl
            // 
            this.layoutControlItemChartControl.Control = this.chartControl1;
            this.layoutControlItemChartControl.CustomizationFormText = "layoutControlItemChartControl";
            this.layoutControlItemChartControl.Location = new System.Drawing.Point(477, 0);
            this.layoutControlItemChartControl.MaxSize = new System.Drawing.Size(783, 0);
            this.layoutControlItemChartControl.MinSize = new System.Drawing.Size(783, 31);
            this.layoutControlItemChartControl.Name = "layoutControlItemChartControl";
            this.layoutControlItemChartControl.Size = new System.Drawing.Size(783, 337);
            this.layoutControlItemChartControl.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemChartControl.Text = "layoutControlItemChartControl";
            this.layoutControlItemChartControl.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemChartControl.TextToControlDistance = 0;
            this.layoutControlItemChartControl.TextVisible = false;
            // 
            // layoutControlGroupDashBoard
            // 
            this.layoutControlGroupDashBoard.CustomizationFormText = "layoutControlGroupDashBoard";
            this.layoutControlGroupDashBoard.GroupBordersVisible = false;
            this.layoutControlGroupDashBoard.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItemDashBoardRight,
            this.layoutControlItemDashboardOperator,
            this.emptySpaceItemDashBoardLeft});
            this.layoutControlGroupDashBoard.Location = new System.Drawing.Point(325, 0);
            this.layoutControlGroupDashBoard.Name = "layoutControlGroupDashBoard";
            this.layoutControlGroupDashBoard.Size = new System.Drawing.Size(152, 337);
            this.layoutControlGroupDashBoard.Text = "layoutControlGroupDashBoard";
            // 
            // emptySpaceItemDashBoardRight
            // 
            this.emptySpaceItemDashBoardRight.AllowHotTrack = false;
            this.emptySpaceItemDashBoardRight.CustomizationFormText = "emptySpaceItemDashBoardRight";
            this.emptySpaceItemDashBoardRight.Location = new System.Drawing.Point(140, 0);
            this.emptySpaceItemDashBoardRight.Name = "emptySpaceItemDashBoardRight";
            this.emptySpaceItemDashBoardRight.Size = new System.Drawing.Size(12, 337);
            this.emptySpaceItemDashBoardRight.Text = "emptySpaceItemDashBoardRight";
            this.emptySpaceItemDashBoardRight.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItemDashboardOperator
            // 
            this.layoutControlItemDashboardOperator.Control = this.indicatorDashBoardControlOperator;
            this.layoutControlItemDashboardOperator.CustomizationFormText = "layoutControlItemDashboardOperator";
            this.layoutControlItemDashboardOperator.Location = new System.Drawing.Point(12, 0);
            this.layoutControlItemDashboardOperator.MinSize = new System.Drawing.Size(105, 25);
            this.layoutControlItemDashboardOperator.Name = "layoutControlItemDashboardOperator";
            this.layoutControlItemDashboardOperator.Size = new System.Drawing.Size(128, 337);
            this.layoutControlItemDashboardOperator.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemDashboardOperator.Text = "layoutControlItemDashboardOperator";
            this.layoutControlItemDashboardOperator.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemDashboardOperator.TextToControlDistance = 0;
            this.layoutControlItemDashboardOperator.TextVisible = false;
            // 
            // emptySpaceItemDashBoardLeft
            // 
            this.emptySpaceItemDashBoardLeft.AllowHotTrack = false;
            this.emptySpaceItemDashBoardLeft.CustomizationFormText = "emptySpaceItemDashBoardLeft";
            this.emptySpaceItemDashBoardLeft.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItemDashBoardLeft.Name = "emptySpaceItemDashBoardLeft";
            this.emptySpaceItemDashBoardLeft.Size = new System.Drawing.Size(12, 337);
            this.emptySpaceItemDashBoardLeft.Text = "emptySpaceItemDashBoardLeft";
            this.emptySpaceItemDashBoardLeft.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupOperatorActivities
            // 
            this.layoutControlGroupOperatorActivities.CustomizationFormText = "layoutControlGroupOperatorActivities";
            this.layoutControlGroupOperatorActivities.GroupBordersVisible = false;
            this.layoutControlGroupOperatorActivities.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemChart});
            this.layoutControlGroupOperatorActivities.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupOperatorActivities.Name = "layoutControlGroupOperatorActivities";
            this.layoutControlGroupOperatorActivities.Size = new System.Drawing.Size(1260, 337);
            this.layoutControlGroupOperatorActivities.Text = "layoutControlGroupOperatorActivities";
            // 
            // layoutControlItemChart
            // 
            this.layoutControlItemChart.Control = this.chartControlOperatorStatuses;
            this.layoutControlItemChart.CustomizationFormText = "layoutControlItemChart";
            this.layoutControlItemChart.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemChart.MinSize = new System.Drawing.Size(105, 25);
            this.layoutControlItemChart.Name = "layoutControlItemChart";
            this.layoutControlItemChart.Size = new System.Drawing.Size(1260, 337);
            this.layoutControlItemChart.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemChart.Text = "layoutControlItemChart";
            this.layoutControlItemChart.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemChart.TextToControlDistance = 0;
            this.layoutControlItemChart.TextVisible = false;
            // 
            // layoutControlGroupRoom
            // 
            this.layoutControlGroupRoom.CustomizationFormText = "layoutControlGroupRoom";
            this.layoutControlGroupRoom.GroupBordersVisible = false;
            this.layoutControlGroupRoom.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItemRoomRight,
            this.layoutControlGroupLegend,
            this.layoutControlItem8,
            this.layoutControlGroupRoomInfo,
            this.emptySpaceItemRoomLeft});
            this.layoutControlGroupRoom.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupRoom.Name = "layoutControlGroupRoom";
            this.layoutControlGroupRoom.Size = new System.Drawing.Size(1260, 337);
            this.layoutControlGroupRoom.Text = "layoutControlGroupRoom";
            // 
            // emptySpaceItemRoomRight
            // 
            this.emptySpaceItemRoomRight.AllowHotTrack = false;
            this.emptySpaceItemRoomRight.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItemRoomRight.Location = new System.Drawing.Point(1167, 0);
            this.emptySpaceItemRoomRight.Name = "emptySpaceItemRoomRight";
            this.emptySpaceItemRoomRight.Size = new System.Drawing.Size(93, 337);
            this.emptySpaceItemRoomRight.Text = "emptySpaceItemRoomRight";
            this.emptySpaceItemRoomRight.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupLegend
            // 
            this.layoutControlGroupLegend.CustomizationFormText = "layoutControlGroupLegend";
            this.layoutControlGroupLegend.GroupBordersVisible = false;
            this.layoutControlGroupLegend.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem13,
            this.layoutControlItem15,
            this.layoutControlItem17,
            this.layoutControlItem19,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem12,
            this.layoutControlItem11,
            this.layoutControlItem14,
            this.layoutControlItem16,
            this.layoutControlItem18,
            this.layoutControlItem20,
            this.emptySpaceItem7});
            this.layoutControlGroupLegend.Location = new System.Drawing.Point(920, 0);
            this.layoutControlGroupLegend.Name = "layoutControlGroupLegend";
            this.layoutControlGroupLegend.Padding = new DevExpress.XtraLayout.Utils.Padding(20, 0, 10, 0);
            this.layoutControlGroupLegend.Size = new System.Drawing.Size(247, 337);
            this.layoutControlGroupLegend.Text = "layoutControlGroupLegend";
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.pictureBox3;
            this.layoutControlItem13.CustomizationFormText = "layoutControlItem13";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 60);
            this.layoutControlItem13.MaxSize = new System.Drawing.Size(35, 30);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(35, 30);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(35, 30);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Text = "layoutControlItem13";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextToControlDistance = 0;
            this.layoutControlItem13.TextVisible = false;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.pictureBox4;
            this.layoutControlItem15.CustomizationFormText = "layoutControlItem15";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 90);
            this.layoutControlItem15.MaxSize = new System.Drawing.Size(35, 30);
            this.layoutControlItem15.MinSize = new System.Drawing.Size(35, 30);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(35, 30);
            this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem15.Text = "layoutControlItem15";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextToControlDistance = 0;
            this.layoutControlItem15.TextVisible = false;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.pictureBox5;
            this.layoutControlItem17.CustomizationFormText = "layoutControlItem17";
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem17.MaxSize = new System.Drawing.Size(35, 30);
            this.layoutControlItem17.MinSize = new System.Drawing.Size(35, 30);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(35, 30);
            this.layoutControlItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem17.Text = "layoutControlItem17";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextToControlDistance = 0;
            this.layoutControlItem17.TextVisible = false;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.pictureBox6;
            this.layoutControlItem19.CustomizationFormText = "layoutControlItem19";
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 150);
            this.layoutControlItem19.MaxSize = new System.Drawing.Size(35, 30);
            this.layoutControlItem19.MinSize = new System.Drawing.Size(35, 30);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(35, 30);
            this.layoutControlItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem19.Text = "layoutControlItem19";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextToControlDistance = 0;
            this.layoutControlItem19.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.pictureBox1;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(35, 30);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(35, 30);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(35, 30);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.labelControlAvailable;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(35, 0);
            this.layoutControlItem10.MaxSize = new System.Drawing.Size(212, 30);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(212, 30);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 0, 5);
            this.layoutControlItem10.Size = new System.Drawing.Size(212, 30);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Text = "layoutControlItem10";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextToControlDistance = 0;
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.labelControlBusy;
            this.layoutControlItem12.CustomizationFormText = "layoutControlItem12";
            this.layoutControlItem12.Location = new System.Drawing.Point(35, 30);
            this.layoutControlItem12.MaxSize = new System.Drawing.Size(212, 30);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(212, 30);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 0, 5);
            this.layoutControlItem12.Size = new System.Drawing.Size(212, 30);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Text = "layoutControlItem12";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextToControlDistance = 0;
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.pictureBox2;
            this.layoutControlItem11.CustomizationFormText = "layoutControlItem11";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(35, 30);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(35, 30);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(35, 30);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "layoutControlItem11";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextToControlDistance = 0;
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.labelControlBathRoom;
            this.layoutControlItem14.CustomizationFormText = "layoutControlItem14";
            this.layoutControlItem14.Location = new System.Drawing.Point(35, 60);
            this.layoutControlItem14.MaxSize = new System.Drawing.Size(212, 30);
            this.layoutControlItem14.MinSize = new System.Drawing.Size(212, 30);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 0, 5);
            this.layoutControlItem14.Size = new System.Drawing.Size(212, 30);
            this.layoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem14.Text = "layoutControlItem14";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem14.TextToControlDistance = 0;
            this.layoutControlItem14.TextVisible = false;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.labelControlRest;
            this.layoutControlItem16.CustomizationFormText = "layoutControlItem16";
            this.layoutControlItem16.Location = new System.Drawing.Point(35, 90);
            this.layoutControlItem16.MaxSize = new System.Drawing.Size(212, 30);
            this.layoutControlItem16.MinSize = new System.Drawing.Size(212, 30);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 0, 5);
            this.layoutControlItem16.Size = new System.Drawing.Size(212, 30);
            this.layoutControlItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem16.Text = "layoutControlItem16";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem16.TextToControlDistance = 0;
            this.layoutControlItem16.TextVisible = false;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.labelControlReunion;
            this.layoutControlItem18.CustomizationFormText = "layoutControlItem18";
            this.layoutControlItem18.Location = new System.Drawing.Point(35, 120);
            this.layoutControlItem18.MaxSize = new System.Drawing.Size(212, 30);
            this.layoutControlItem18.MinSize = new System.Drawing.Size(212, 30);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 0, 5);
            this.layoutControlItem18.Size = new System.Drawing.Size(212, 30);
            this.layoutControlItem18.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem18.Text = "layoutControlItem18";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextToControlDistance = 0;
            this.layoutControlItem18.TextVisible = false;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.labelControlAbsent;
            this.layoutControlItem20.CustomizationFormText = "layoutControlItem20";
            this.layoutControlItem20.Location = new System.Drawing.Point(35, 150);
            this.layoutControlItem20.MaxSize = new System.Drawing.Size(212, 30);
            this.layoutControlItem20.MinSize = new System.Drawing.Size(212, 30);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 0, 5);
            this.layoutControlItem20.Size = new System.Drawing.Size(212, 30);
            this.layoutControlItem20.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem20.Text = "layoutControlItem20";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem20.TextToControlDistance = 0;
            this.layoutControlItem20.TextVisible = false;
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 180);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(247, 157);
            this.emptySpaceItem7.Text = "emptySpaceItem7";
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.panelRoomDesing;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(278, 0);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(642, 330);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(642, 330);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 4, 3);
            this.layoutControlItem8.Size = new System.Drawing.Size(642, 337);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlGroupRoomInfo
            // 
            this.layoutControlGroupRoomInfo.CustomizationFormText = "layoutControlGroupRoomInfo";
            this.layoutControlGroupRoomInfo.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemRoomName,
            this.layoutControlItemRoomDescription,
            this.emptySpaceItem2});
            this.layoutControlGroupRoomInfo.Location = new System.Drawing.Point(31, 0);
            this.layoutControlGroupRoomInfo.Name = "layoutControlGroupRoomInfo";
            this.layoutControlGroupRoomInfo.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupRoomInfo.Size = new System.Drawing.Size(247, 337);
            this.layoutControlGroupRoomInfo.Text = "layoutControlGroupRoomInfo";
            // 
            // layoutControlItemRoomName
            // 
            this.layoutControlItemRoomName.Control = this.textEditRoomName;
            this.layoutControlItemRoomName.CustomizationFormText = "layoutControlItemRoomName";
            this.layoutControlItemRoomName.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemRoomName.MaxSize = new System.Drawing.Size(237, 58);
            this.layoutControlItemRoomName.MinSize = new System.Drawing.Size(237, 58);
            this.layoutControlItemRoomName.Name = "layoutControlItemRoomName";
            this.layoutControlItemRoomName.Size = new System.Drawing.Size(237, 58);
            this.layoutControlItemRoomName.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemRoomName.Text = "layoutControlItemRoomName";
            this.layoutControlItemRoomName.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItemRoomName.TextSize = new System.Drawing.Size(167, 13);
            // 
            // layoutControlItemRoomDescription
            // 
            this.layoutControlItemRoomDescription.Control = this.textEditRoomDes;
            this.layoutControlItemRoomDescription.CustomizationFormText = "layoutControlItemRoomDescription";
            this.layoutControlItemRoomDescription.Location = new System.Drawing.Point(0, 58);
            this.layoutControlItemRoomDescription.MaxSize = new System.Drawing.Size(237, 138);
            this.layoutControlItemRoomDescription.MinSize = new System.Drawing.Size(237, 138);
            this.layoutControlItemRoomDescription.Name = "layoutControlItemRoomDescription";
            this.layoutControlItemRoomDescription.Size = new System.Drawing.Size(237, 138);
            this.layoutControlItemRoomDescription.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemRoomDescription.Text = "layoutControlItemRoomDescription";
            this.layoutControlItemRoomDescription.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItemRoomDescription.TextSize = new System.Drawing.Size(167, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 196);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(237, 111);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItemRoomLeft
            // 
            this.emptySpaceItemRoomLeft.AllowHotTrack = false;
            this.emptySpaceItemRoomLeft.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItemRoomLeft.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItemRoomLeft.Name = "emptySpaceItemRoomLeft";
            this.emptySpaceItemRoomLeft.Size = new System.Drawing.Size(31, 337);
            this.emptySpaceItemRoomLeft.Text = "emptySpaceItemRoomLeft";
            this.emptySpaceItemRoomLeft.TextSize = new System.Drawing.Size(0, 0);
            // 
            // barButtonGroupGeneralOptions
            // 
            this.barButtonGroupGeneralOptions.Id = -1;
            this.barButtonGroupGeneralOptions.Name = "barButtonGroupGeneralOptions";
            // 
            // contextMenuStripDesigner
            // 
            this.contextMenuStripDesigner.Name = "contextMenuStripDesigner";
            this.contextMenuStripDesigner.Size = new System.Drawing.Size(61, 4);
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "logout.gif");
            // 
            // printingSystem
            // 
            this.printingSystem.Links.AddRange(new object[] {
            this.printableComponentLink});
            // 
            // printableComponentLink
            // 
            this.printableComponentLink.Component = this.chartControlOperatorStatuses;
            // 
            // 
            // 
            this.printableComponentLink.ImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("printableComponentLink.ImageCollection.ImageStream")));
            this.printableComponentLink.Landscape = true;
            this.printableComponentLink.PrintingSystem = this.printingSystem;
            this.printableComponentLink.PrintingSystemBase = this.printingSystem;
            // 
            // RibbonControl
            // 
            this.RibbonControl.Dock = System.Windows.Forms.DockStyle.None;
            this.RibbonControl.ExpandCollapseItem.Id = 0;
            this.RibbonControl.ExpandCollapseItem.Name = "";
            this.RibbonControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RibbonControl.Images = this.imageList;
            this.RibbonControl.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.RibbonControl.ExpandCollapseItem,
            this.barButtonItemOperatorActivities,
            this.barButtonItemSave,
            this.barButtonItemPrint,
            this.barButtonItemRefreshGraphic,
            this.BarButtonItemSelectRoom,
            this.barEditItemSelectRoom,
            this.barCheckItemViewSelected,
            this.barCheckItemViewAll,
            this.BarButtonItemSendMonitor,
            this.barButtonItemOperatorPerformance,
            this.barEditItemIndicatorSelection,
            this.barSubItemOperatorChartControl,
            this.barCheckItemUpTreshold,
            this.barCheckItemDownTreshold,
            this.barCheckItemTrend,
            this.barCheckItem4,
            this.barButtonItemChat,
            this.barButtonItemRemoteControl,
            this.barCheckItemTakeControl,
            this.barCheckItemScaleView,
            this.barButtonItemReConnect});
            this.RibbonControl.ItemsVertAlign = DevExpress.Utils.VertAlignment.Top;
            this.RibbonControl.Location = new System.Drawing.Point(200, 59);
            this.RibbonControl.MaxItemId = 277;
            this.RibbonControl.MdiMergeStyle = DevExpress.XtraBars.Ribbon.RibbonMdiMergeStyle.Always;
            this.RibbonControl.Name = "RibbonControl";
            this.RibbonControl.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.RibbonPageMonitoring});
            this.RibbonControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit,
            this.repositoryItemComboBoxWorkShift,
            this.repositoryItemComboBoxSelectRoom,
            this.repositoryItemRadioGroup1,
            this.repositoryItemRadioGroup2});
            this.RibbonControl.ShowToolbarCustomizeItem = false;
            this.RibbonControl.Size = new System.Drawing.Size(1017, 119);
            this.RibbonControl.Toolbar.ShowCustomizeItem = false;
            this.RibbonControl.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            this.RibbonControl.TransparentEditors = true;
            // 
            // barButtonItemOperatorActivities
            // 
            this.barButtonItemOperatorActivities.Caption = "Ver actividades";
            this.barButtonItemOperatorActivities.Id = 119;
            this.barButtonItemOperatorActivities.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A));
            this.barButtonItemOperatorActivities.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemOperatorActivities.LargeGlyph")));
            this.barButtonItemOperatorActivities.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemOperatorActivities.Name = "barButtonItemOperatorActivities";
            toolTipTitleItem2.Text = "Actividades de los Operadores";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Permite visualizar las actividades de los operadores conectados.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.barButtonItemOperatorActivities.SuperTip = superToolTip2;
            this.barButtonItemOperatorActivities.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemOperatorActivities_ItemClick);
            // 
            // barButtonItemSave
            // 
            this.barButtonItemSave.Enabled = false;
            this.barButtonItemSave.Glyph = global::SmartCadGuiCommon.PrintRibbonControllerResources.RibbonPrintPreview_Save;
            this.barButtonItemSave.Id = 230;
            this.barButtonItemSave.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S));
            this.barButtonItemSave.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemSave.Name = "barButtonItemSave";
            this.barButtonItemSave.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemSave_ItemClick);
            // 
            // barButtonItemPrint
            // 
            this.barButtonItemPrint.Enabled = false;
            this.barButtonItemPrint.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.Glyph")));
            this.barButtonItemPrint.Id = 231;
            this.barButtonItemPrint.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P));
            this.barButtonItemPrint.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemPrint.Name = "barButtonItemPrint";
            this.barButtonItemPrint.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPrint_ItemClick);
            // 
            // barButtonItemRefreshGraphic
            // 
            this.barButtonItemRefreshGraphic.Enabled = false;
            this.barButtonItemRefreshGraphic.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemRefreshGraphic.Glyph")));
            this.barButtonItemRefreshGraphic.Id = 239;
            this.barButtonItemRefreshGraphic.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F5);
            this.barButtonItemRefreshGraphic.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemRefreshGraphic.Name = "barButtonItemRefreshGraphic";
            this.barButtonItemRefreshGraphic.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemRefreshGraphic.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemRefreshGraphic_ItemClick);
            // 
            // BarButtonItemSelectRoom
            // 
            this.BarButtonItemSelectRoom.Caption = "Ver sala";
            this.BarButtonItemSelectRoom.Id = 248;
            this.BarButtonItemSelectRoom.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F));
            this.BarButtonItemSelectRoom.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("BarButtonItemSelectRoom.LargeGlyph")));
            this.BarButtonItemSelectRoom.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.BarButtonItemSelectRoom.Name = "BarButtonItemSelectRoom";
            this.BarButtonItemSelectRoom.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BarButtonItemSelectRoom_ItemClick);
            // 
            // barEditItemSelectRoom
            // 
            this.barEditItemSelectRoom.Caption = "Nombre:";
            this.barEditItemSelectRoom.Edit = this.repositoryItemComboBoxSelectRoom;
            this.barEditItemSelectRoom.Id = 254;
            this.barEditItemSelectRoom.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barEditItemSelectRoom.Name = "barEditItemSelectRoom";
            this.barEditItemSelectRoom.Width = 100;
            this.barEditItemSelectRoom.EditValueChanged += new System.EventHandler(this.barEditItemSelectRoom_EditValueChanged);
            this.barEditItemSelectRoom.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barEditItemSelectRoom_ItemClick);
            // 
            // repositoryItemComboBoxSelectRoom
            // 
            this.repositoryItemComboBoxSelectRoom.AutoHeight = false;
            this.repositoryItemComboBoxSelectRoom.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBoxSelectRoom.Name = "repositoryItemComboBoxSelectRoom";
            this.repositoryItemComboBoxSelectRoom.ReadOnly = true;
            this.repositoryItemComboBoxSelectRoom.UseParentBackground = true;
            // 
            // barCheckItemViewSelected
            // 
            this.barCheckItemViewSelected.Caption = "Operador seleccionado";
            this.barCheckItemViewSelected.Glyph = ((System.Drawing.Image)(resources.GetObject("barCheckItemViewSelected.Glyph")));
            this.barCheckItemViewSelected.GroupIndex = 1;
            this.barCheckItemViewSelected.Id = 255;
            this.barCheckItemViewSelected.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barCheckItemViewSelected.Name = "barCheckItemViewSelected";
            this.barCheckItemViewSelected.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.barCheckItemViewSelected_CheckedChanged);
            // 
            // barCheckItemViewAll
            // 
            this.barCheckItemViewAll.Caption = "Todos";
            this.barCheckItemViewAll.Checked = true;
            this.barCheckItemViewAll.Glyph = ((System.Drawing.Image)(resources.GetObject("barCheckItemViewAll.Glyph")));
            this.barCheckItemViewAll.GroupIndex = 1;
            this.barCheckItemViewAll.Id = 256;
            this.barCheckItemViewAll.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barCheckItemViewAll.Name = "barCheckItemViewAll";
            this.barCheckItemViewAll.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.barCheckItemViewAll_CheckedChanged);
            // 
            // BarButtonItemSendMonitor
            // 
            this.BarButtonItemSendMonitor.Enabled = false;
            this.BarButtonItemSendMonitor.Glyph = ((System.Drawing.Image)(resources.GetObject("BarButtonItemSendMonitor.Glyph")));
            this.BarButtonItemSendMonitor.Id = 257;
            this.BarButtonItemSendMonitor.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Enter));
            this.BarButtonItemSendMonitor.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.BarButtonItemSendMonitor.Name = "BarButtonItemSendMonitor";
            this.BarButtonItemSendMonitor.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.BarButtonItemSendMonitor.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BarButtonItemSendMonitor_ItemClick);
            // 
            // barButtonItemOperatorPerformance
            // 
            this.barButtonItemOperatorPerformance.Caption = "Ver desempeo";
            this.barButtonItemOperatorPerformance.Id = 259;
            this.barButtonItemOperatorPerformance.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D));
            this.barButtonItemOperatorPerformance.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemOperatorPerformance.LargeGlyph")));
            this.barButtonItemOperatorPerformance.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemOperatorPerformance.Name = "barButtonItemOperatorPerformance";
            this.barButtonItemOperatorPerformance.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemOperatorPerformance_ItemClick);
            // 
            // barEditItemIndicatorSelection
            // 
            this.barEditItemIndicatorSelection.Edit = this.repositoryItemRadioGroup1;
            this.barEditItemIndicatorSelection.EditHeight = 50;
            this.barEditItemIndicatorSelection.EditValue = true;
            this.barEditItemIndicatorSelection.Id = 260;
            this.barEditItemIndicatorSelection.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barEditItemIndicatorSelection.Name = "barEditItemIndicatorSelection";
            this.barEditItemIndicatorSelection.Width = 145;
            // 
            // repositoryItemRadioGroup1
            // 
            this.repositoryItemRadioGroup1.AllowFocused = false;
            this.repositoryItemRadioGroup1.Appearance.BackColor = System.Drawing.Color.Orange;
            this.repositoryItemRadioGroup1.Appearance.BackColor2 = System.Drawing.Color.Maroon;
            this.repositoryItemRadioGroup1.Appearance.BorderColor = System.Drawing.Color.Transparent;
            this.repositoryItemRadioGroup1.Appearance.Options.UseBackColor = true;
            this.repositoryItemRadioGroup1.Appearance.Options.UseBorderColor = true;
            this.repositoryItemRadioGroup1.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemRadioGroup1.AppearanceDisabled.BackColor2 = System.Drawing.Color.Transparent;
            this.repositoryItemRadioGroup1.AppearanceDisabled.BorderColor = System.Drawing.Color.Transparent;
            this.repositoryItemRadioGroup1.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemRadioGroup1.AppearanceDisabled.Options.UseBorderColor = true;
            this.repositoryItemRadioGroup1.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemRadioGroup1.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent;
            this.repositoryItemRadioGroup1.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent;
            this.repositoryItemRadioGroup1.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemRadioGroup1.AppearanceFocused.Options.UseBorderColor = true;
            this.repositoryItemRadioGroup1.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemRadioGroup1.AppearanceReadOnly.BackColor2 = System.Drawing.Color.Transparent;
            this.repositoryItemRadioGroup1.AppearanceReadOnly.BorderColor = System.Drawing.Color.Transparent;
            this.repositoryItemRadioGroup1.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemRadioGroup1.AppearanceReadOnly.Options.UseBorderColor = true;
            this.repositoryItemRadioGroup1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemRadioGroup1.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Indicadores primarios"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Indicadores secundarios")});
            this.repositoryItemRadioGroup1.LookAndFeel.SkinName = "Blue";
            this.repositoryItemRadioGroup1.Name = "repositoryItemRadioGroup1";
            this.repositoryItemRadioGroup1.UseParentBackground = true;
            this.repositoryItemRadioGroup1.SelectedIndexChanged += new System.EventHandler(this.repositoryItemRadioGroup1_SelectedIndexChanged);
            // 
            // barSubItemOperatorChartControl
            // 
            this.barSubItemOperatorChartControl.Caption = "Configurar Grfica";
            this.barSubItemOperatorChartControl.Enabled = false;
            this.barSubItemOperatorChartControl.Glyph = ((System.Drawing.Image)(resources.GetObject("barSubItemOperatorChartControl.Glyph")));
            this.barSubItemOperatorChartControl.Id = 262;
            this.barSubItemOperatorChartControl.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barCheckItemUpTreshold),
            new DevExpress.XtraBars.LinkPersistInfo(this.barCheckItemDownTreshold),
            new DevExpress.XtraBars.LinkPersistInfo(this.barCheckItemTrend)});
            this.barSubItemOperatorChartControl.Name = "barSubItemOperatorChartControl";
            this.barSubItemOperatorChartControl.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barCheckItemUpTreshold
            // 
            this.barCheckItemUpTreshold.Caption = "barCheckItemSuperiror";
            this.barCheckItemUpTreshold.Id = 263;
            this.barCheckItemUpTreshold.Name = "barCheckItemUpTreshold";
            this.barCheckItemUpTreshold.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.barCheckItemUpTreshold_CheckedChanged);
            // 
            // barCheckItemDownTreshold
            // 
            this.barCheckItemDownTreshold.Caption = "barCheckItemInferior";
            this.barCheckItemDownTreshold.Id = 264;
            this.barCheckItemDownTreshold.Name = "barCheckItemDownTreshold";
            this.barCheckItemDownTreshold.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.barCheckItemDownTreshold_CheckedChanged);
            // 
            // barCheckItemTrend
            // 
            this.barCheckItemTrend.Caption = "barCheckItemTendencia";
            this.barCheckItemTrend.Id = 265;
            this.barCheckItemTrend.Name = "barCheckItemTrend";
            this.barCheckItemTrend.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.barCheckItemTrend_CheckedChanged);
            // 
            // barCheckItem4
            // 
            this.barCheckItem4.Caption = "barCheckItem4";
            this.barCheckItem4.Id = 266;
            this.barCheckItem4.Name = "barCheckItem4";
            // 
            // barButtonItemChat
            // 
            this.barButtonItemChat.Caption = "Mensajera";
            this.barButtonItemChat.Glyph = global::SmartCadGuiCommon.Properties.ResourcesGui.chat;
            this.barButtonItemChat.Id = 269;
            this.barButtonItemChat.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemChat.Name = "barButtonItemChat";
            this.barButtonItemChat.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemChat.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemChat_ItemClick);
            // 
            // barButtonItemRemoteControl
            // 
            this.barButtonItemRemoteControl.Caption = "Monitorear";
            this.barButtonItemRemoteControl.Enabled = false;
            this.barButtonItemRemoteControl.Id = 270;
            this.barButtonItemRemoteControl.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemRemoteControl.LargeGlyph")));
            this.barButtonItemRemoteControl.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemRemoteControl.Name = "barButtonItemRemoteControl";
            this.barButtonItemRemoteControl.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemRemoteControl.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.barButtonItemRemoteControl.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemRemoteControl_ItemClick);
            // 
            // barCheckItemTakeControl
            // 
            this.barCheckItemTakeControl.Caption = "barCheckItemTakeControl";
            this.barCheckItemTakeControl.Enabled = false;
            this.barCheckItemTakeControl.Id = 274;
            this.barCheckItemTakeControl.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barCheckItemTakeControl.Name = "barCheckItemTakeControl";
            this.barCheckItemTakeControl.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barCheckItemScaleView
            // 
            this.barCheckItemScaleView.Caption = "barCheckItemScaleView";
            this.barCheckItemScaleView.Enabled = false;
            this.barCheckItemScaleView.Id = 275;
            this.barCheckItemScaleView.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barCheckItemScaleView.Name = "barCheckItemScaleView";
            this.barCheckItemScaleView.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barButtonItemReConnect
            // 
            this.barButtonItemReConnect.Caption = "barButtonItemReConnect";
            this.barButtonItemReConnect.Enabled = false;
            this.barButtonItemReConnect.Id = 276;
            this.barButtonItemReConnect.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemReConnect.Name = "barButtonItemReConnect";
            this.barButtonItemReConnect.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // RibbonPageMonitoring
            // 
            this.RibbonPageMonitoring.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupOperator,
            this.ribbonPageGroupGeneralOptMonitor});
            this.RibbonPageMonitoring.Name = "RibbonPageMonitoring";
            this.RibbonPageMonitoring.Text = "Monitoreo";
            // 
            // ribbonPageGroupOperator
            // 
            this.ribbonPageGroupOperator.AllowMinimize = false;
            this.ribbonPageGroupOperator.AllowTextClipping = false;
            this.ribbonPageGroupOperator.ItemLinks.Add(this.barButtonItemOperatorActivities);
            this.ribbonPageGroupOperator.ItemLinks.Add(this.barButtonItemOperatorPerformance, true);
            this.ribbonPageGroupOperator.ItemLinks.Add(this.barEditItemIndicatorSelection);
            this.ribbonPageGroupOperator.ItemLinks.Add(this.barSubItemOperatorChartControl);
            this.ribbonPageGroupOperator.ItemLinks.Add(this.BarButtonItemSelectRoom, true);
            this.ribbonPageGroupOperator.ItemLinks.Add(this.barCheckItemViewSelected);
            this.ribbonPageGroupOperator.ItemLinks.Add(this.barCheckItemViewAll);
            this.ribbonPageGroupOperator.ItemLinks.Add(this.barEditItemSelectRoom);
            this.ribbonPageGroupOperator.ItemLinks.Add(this.barButtonItemChat, true);
            this.ribbonPageGroupOperator.ItemLinks.Add(this.barButtonItemRemoteControl, true);
            this.ribbonPageGroupOperator.ItemLinks.Add(this.barCheckItemTakeControl);
            this.ribbonPageGroupOperator.ItemLinks.Add(this.barCheckItemScaleView);
            this.ribbonPageGroupOperator.ItemLinks.Add(this.barButtonItemReConnect);
            this.ribbonPageGroupOperator.Kind = DevExpress.XtraReports.UserDesigner.XRDesignRibbonPageGroupKind.SizeAndLayout;
            this.ribbonPageGroupOperator.Name = "ribbonPageGroupOperator";
            this.ribbonPageGroupOperator.ShowCaptionButton = false;
            this.ribbonPageGroupOperator.Text = "Operador";
            // 
            // ribbonPageGroupGeneralOptMonitor
            // 
            this.ribbonPageGroupGeneralOptMonitor.AllowMinimize = false;
            this.ribbonPageGroupGeneralOptMonitor.AllowTextClipping = false;
            this.ribbonPageGroupGeneralOptMonitor.ItemLinks.Add(this.barButtonItemPrint);
            this.ribbonPageGroupGeneralOptMonitor.ItemLinks.Add(this.barButtonItemSave);
            this.ribbonPageGroupGeneralOptMonitor.ItemLinks.Add(this.BarButtonItemSendMonitor);
            this.ribbonPageGroupGeneralOptMonitor.ItemLinks.Add(this.barButtonItemRefreshGraphic);
            this.ribbonPageGroupGeneralOptMonitor.Name = "ribbonPageGroupGeneralOptMonitor";
            this.ribbonPageGroupGeneralOptMonitor.ShowCaptionButton = false;
            this.ribbonPageGroupGeneralOptMonitor.Text = "Opciones generales";
            // 
            // repositoryItemDateEdit
            // 
            this.repositoryItemDateEdit.AutoHeight = false;
            this.repositoryItemDateEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit.DisplayFormat.FormatString = "MM/dd/yyyy";
            this.repositoryItemDateEdit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit.EditFormat.FormatString = "MM/dd/yyyy";
            this.repositoryItemDateEdit.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItemDateEdit.Mask.EditMask = "MM/dd/yyyy";
            this.repositoryItemDateEdit.Name = "repositoryItemDateEdit";
            this.repositoryItemDateEdit.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // repositoryItemComboBoxWorkShift
            // 
            this.repositoryItemComboBoxWorkShift.AutoHeight = false;
            this.repositoryItemComboBoxWorkShift.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBoxWorkShift.Name = "repositoryItemComboBoxWorkShift";
            // 
            // repositoryItemRadioGroup2
            // 
            this.repositoryItemRadioGroup2.AllowFocused = false;
            this.repositoryItemRadioGroup2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemRadioGroup2.Appearance.BackColor2 = System.Drawing.Color.Transparent;
            this.repositoryItemRadioGroup2.Appearance.Options.UseBackColor = true;
            this.repositoryItemRadioGroup2.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemRadioGroup2.AppearanceDisabled.BackColor2 = System.Drawing.Color.Transparent;
            this.repositoryItemRadioGroup2.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemRadioGroup2.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemRadioGroup2.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent;
            this.repositoryItemRadioGroup2.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemRadioGroup2.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemRadioGroup2.AppearanceReadOnly.BackColor2 = System.Drawing.Color.Transparent;
            this.repositoryItemRadioGroup2.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemRadioGroup2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemRadioGroup2.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "fff"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "hhhkt")});
            this.repositoryItemRadioGroup2.Name = "repositoryItemRadioGroup2";
            this.repositoryItemRadioGroup2.UseParentBackground = true;
            // 
            // barSubItemChartControl
            // 
            this.barSubItemChartControl.Caption = "Configurar grfica";
            this.barSubItemChartControl.Glyph = ((System.Drawing.Image)(resources.GetObject("barSubItemChartControl.Glyph")));
            this.barSubItemChartControl.Id = 278;
            this.barSubItemChartControl.Name = "barSubItemChartControl";
            // 
            // alertControl1
            // 
            this.alertControl1.AlertClick += new DevExpress.XtraBars.Alerter.AlertClickEventHandler(this.alertControl1_AlertClick);
            // 
            // DefaultSupervisionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1270, 780);
            this.Controls.Add(this.RibbonControl);
            this.Controls.Add(this.layoutControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DefaultSupervisionForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "5";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DefaultSupervisionForm_FormClosing);
            this.Load += new System.EventHandler(this.DefaultSupervisionForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExOperators)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(xyDiagram2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditRoomDes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditRoomName.Properties)).EndInit();
            this.panelRoomDesing.ResumeLayout(false);
            this.panelControlNoRoom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(ganttDiagram2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(rangeBarSeriesLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(overlappedGanttSeriesView10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(rangeBarSeriesLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(overlappedGanttSeriesView11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(rangeBarSeriesLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(overlappedGanttSeriesView12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(rangeBarSeriesLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(overlappedGanttSeriesView13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(rangeBarSeriesLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(overlappedGanttSeriesView14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(rangeBarSeriesLabel15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(overlappedGanttSeriesView15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(rangeBarSeriesLabel16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(overlappedGanttSeriesView16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(rangeBarSeriesLabel17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(overlappedGanttSeriesView17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(rangeBarSeriesLabel18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(overlappedGanttSeriesView18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControlOperatorStatuses)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupOperators)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupPerformance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroupDashboard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupSystem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDashboard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupDepartment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupMyGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemMyGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroupMonitorActivities)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupOperatorPerformance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGridPerformance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemChartControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupDashBoard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItemDashBoardRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDashboardOperator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItemDashBoardLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupOperatorActivities)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupRoom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItemRoomRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupLegend)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupRoomInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemRoomName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemRoomDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItemRoomLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printableComponentLink.ImageCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RibbonControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBoxSelectRoom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBoxWorkShift)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RadioButton radioButtonNoConOperators;
        private System.Windows.Forms.RadioButton radioButtonConOperators;
        private System.Windows.Forms.RadioButton radioButtonAllOperators;
        private LabelEx labelExShowOperators;
        private IndicatorDashBoardControl indicatorDashBoardControlSystem;
        private IndicatorDashBoardControl indicatorDashBoardControlGroup;
        private IndicatorDashBoardControl indicatorDashBoardControlOperator;
        private System.Windows.Forms.Panel panelRoomDesing;
        private System.Windows.Forms.ToolTip toolTipSeats;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripDesigner;
        private DevExpress.XtraCharts.ChartControl chartControlOperatorStatuses;
        private System.Windows.Forms.ImageList imageList;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem;
        private DevExpress.XtraPrinting.PrintableComponentLink printableComponentLink;
        internal DevExpress.XtraBars.Ribbon.RibbonControl RibbonControl;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemOperatorActivities;
        internal DevExpress.XtraReports.UserDesigner.XRDesignRibbonPage RibbonPageMonitoring;
        internal DevExpress.XtraReports.UserDesigner.XRDesignRibbonPageGroup ribbonPageGroupOperator;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupGeneralOptMonitor;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItemSelectRoom;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemSave;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemPrint;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemRefreshGraphic;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBoxWorkShift;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBoxSelectRoom;
        internal DevExpress.XtraBars.BarEditItem barEditItemSelectRoom;
        internal DevExpress.XtraBars.BarCheckItem barCheckItemViewSelected;
        internal DevExpress.XtraBars.BarCheckItem barCheckItemViewAll;
        private IndicatorGridControl indicatorGridControl1;
        internal DevExpress.XtraCharts.ChartControl chartControl1;
        private GridControlEx gridControlExOperators;
        private GridViewEx gridView1;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItemSendMonitor;
        internal DevExpress.XtraBars.BarButtonGroup barButtonGroupGeneralOptions;
        private DevExpress.XtraEditors.LabelControl labelControlNoRoom;
        private System.Windows.Forms.Panel panelControlNoRoom;
        private DevExpress.XtraEditors.TextEdit textEditRoomName;
        private DevExpress.XtraEditors.MemoEdit textEditRoomDes;
        private DevExpress.XtraEditors.LabelControl labelControlBusy;
        private DevExpress.XtraEditors.LabelControl labelControlAvailable;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.LabelControl labelControlAbsent;
        private DevExpress.XtraEditors.LabelControl labelControlReunion;
        private DevExpress.XtraEditors.LabelControl labelControlRest;
        private DevExpress.XtraEditors.LabelControl labelControlBathRoom;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemOperatorPerformance;
        private DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup repositoryItemRadioGroup1;
        internal DevExpress.XtraBars.BarEditItem barEditItemIndicatorSelection;
        private DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup repositoryItemRadioGroup2;
        private IndicatorDashBoardControl indicatorDashBoardControlDepartment;
        private DevExpress.XtraBars.BarSubItem barSubItemOperatorChartControl;
        private DevExpress.XtraBars.BarSubItem barSubItemChartControl;
        private DevExpress.XtraBars.BarCheckItem barCheckItemUpTreshold;
        private DevExpress.XtraBars.BarCheckItem barCheckItemDownTreshold;
        private DevExpress.XtraBars.BarCheckItem barCheckItemTrend;
        private DevExpress.XtraBars.BarCheckItem barCheckItem4;
        private DevExpress.XtraBars.Alerter.AlertControl alertControl1;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemRemoteControl;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemChat;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupOperators;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemGrid;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupPerformance;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroupDashboard;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupMyGroup;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemMyGroup;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupSystem;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDashboard;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupDepartment;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDepartment;
        internal DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroupMonitorActivities;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupOperatorPerformance;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupOperatorActivities;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemChart;
        internal DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupRoom;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemGridPerformance;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemChartControl;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDashboardOperator;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupDashBoard;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItemDashBoardRight;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItemDashBoardLeft;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItemRoomRight;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupLegend;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupRoomInfo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemRoomName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemRoomDescription;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItemRoomLeft;
        internal DevExpress.XtraBars.BarCheckItem barCheckItemTakeControl;
        internal DevExpress.XtraBars.BarCheckItem barCheckItemScaleView;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemReConnect;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        
    }
}
