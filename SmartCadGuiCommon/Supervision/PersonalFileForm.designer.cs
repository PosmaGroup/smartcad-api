using SmartCadControls;
using SmartCadControls.Controls;
using SmartCadGuiCommon.Controls;

namespace SmartCadGuiCommon
{
    partial class PersonalFileForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PersonalFileForm));
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem9 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip10 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem10 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip11 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem11 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip12 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem12 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            this.listBoxDepartaments = new DevExpress.XtraEditors.ListBoxControl();
            this.layoutControlPersonalFile = new DevExpress.XtraLayout.LayoutControl();
            this.labelControlPhoto = new DevExpress.XtraEditors.LabelControl();
            this.panelExOperatorPicture = new PanelEx();
            this.pictureBoxPhoto = new System.Windows.Forms.PictureBox();
            this.gridControlCat = new GridControlEx();
            this.gridViewCat = new GridViewEx();
            this.labelName = new LabelEx();
            this.textBoxAddress2 = new DevExpress.XtraEditors.MemoEdit();
            this.labelLastName = new LabelEx();
            this.labelOperatorID = new LabelEx();
            this.labelRol = new LabelEx();
            this.labelDateBirth = new LabelEx();
            this.labelPhone = new LabelEx();
            this.gridControlCourses = new GridControlEx();
            this.gridViewCoursesRealized = new GridViewEx();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.comboBoxFilterEvaluations = new DevExpress.XtraEditors.ComboBoxEdit();
            this.radioButtonCoursesRealized = new RadioButtonEx();
            this.gridControlEva = new GridControlEx();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.printMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportPDFMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportHTMLMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gridViewAllEva = new GridViewEx();
            this.radioButtonCoursesNotRealized = new RadioButtonEx();
            this.textBoxDetails = new DevExpress.XtraEditors.MemoEdit();
            this.gridControlObs = new GridControlEx();
            this.gridViewObs = new GridViewEx();
            this.schedulerBarControl = new SchedulerBarControl();
            this.labelEmail = new LabelEx();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupUserPersonalInfo = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemPhoto = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemLastName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemOpId = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemDateBirth = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemPhone = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemAddress = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemEmail = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemRol = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemDepartments = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemCategory = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItemUserPhoto = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.tabbedControlGroupUser = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroupScheduler = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupUserInfo = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupObs = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupObsDetails = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupCourse = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupEval = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemShowFor = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.barButtonItemExit = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemHelp = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageGroupGeneralOptMonitor = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupRoom = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.XrDesignRibbonPageGroupViewActivities = new DevExpress.XtraReports.UserDesigner.XRDesignRibbonPageGroup();
            this.ribbonPageGroupPerformance = new DevExpress.XtraReports.UserDesigner.XRDesignRibbonPageGroup();
            this.RibbonPageMonitoring = new DevExpress.XtraReports.UserDesigner.XRDesignRibbonPage();
            this.ribbonPageGroupWindows = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupAccessProfile = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupPreferences = new DevExpress.XtraReports.UserDesigner.XRDesignRibbonPageGroup();
            this.RibbonPageTools = new DevExpress.XtraReports.UserDesigner.XRHtmlRibbonPage();
            this.barButtonItemUpdate = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageGroupFinalReport = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup();
            this.ribbonPageGroupTraining = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup();
            this.printPreviewBarItemPersonalfile = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.ribbonPageGroupAssignment = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.RibbonPageOperation = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPage();
            this.ribbonPageGroupPersonalFile = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup();
            this.barButtonItemEvaluateOperator = new DevExpress.XtraBars.BarButtonItem();
            this.printPreviewBarItemModifyCategory = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItemPersonalFileObs = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.ribbonPageGroupOptions = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.barButtonItemPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSave = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSend = new DevExpress.XtraBars.BarButtonItem();
            this.RibbonControl = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem2 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem3 = new DevExpress.XtraBars.BarStaticItem();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxDepartaments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlPersonalFile)).BeginInit();
            this.layoutControlPersonalFile.SuspendLayout();
            this.panelExOperatorPicture.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPhoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxAddress2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCourses)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCoursesRealized)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxFilterEvaluations.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlEva)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewAllEva)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxDetails.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlObs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewObs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupUserPersonalInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPhoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLastName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOpId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDateBirth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemRol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemUserPhoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroupUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupScheduler)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupUserInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupObs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupObsDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupCourse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupEval)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemShowFor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RibbonControl)).BeginInit();
            this.SuspendLayout();
            // 
            // listBoxDepartaments
            // 
            this.listBoxDepartaments.Location = new System.Drawing.Point(172, 540);
            this.listBoxDepartaments.Margin = new System.Windows.Forms.Padding(2);
            this.listBoxDepartaments.Name = "listBoxDepartaments";
            this.listBoxDepartaments.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.listBoxDepartaments.Size = new System.Drawing.Size(188, 117);
            this.listBoxDepartaments.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.listBoxDepartaments.StyleController = this.layoutControlPersonalFile;
            this.listBoxDepartaments.TabIndex = 38;
            // 
            // layoutControlPersonalFile
            // 
            this.layoutControlPersonalFile.AllowCustomizationMenu = false;
            this.layoutControlPersonalFile.Controls.Add(this.labelControlPhoto);
            this.layoutControlPersonalFile.Controls.Add(this.listBoxDepartaments);
            this.layoutControlPersonalFile.Controls.Add(this.panelExOperatorPicture);
            this.layoutControlPersonalFile.Controls.Add(this.gridControlCat);
            this.layoutControlPersonalFile.Controls.Add(this.labelName);
            this.layoutControlPersonalFile.Controls.Add(this.textBoxAddress2);
            this.layoutControlPersonalFile.Controls.Add(this.labelLastName);
            this.layoutControlPersonalFile.Controls.Add(this.labelOperatorID);
            this.layoutControlPersonalFile.Controls.Add(this.labelRol);
            this.layoutControlPersonalFile.Controls.Add(this.labelDateBirth);
            this.layoutControlPersonalFile.Controls.Add(this.labelPhone);
            this.layoutControlPersonalFile.Controls.Add(this.gridControlCourses);
            this.layoutControlPersonalFile.Controls.Add(this.comboBoxFilterEvaluations);
            this.layoutControlPersonalFile.Controls.Add(this.radioButtonCoursesRealized);
            this.layoutControlPersonalFile.Controls.Add(this.gridControlEva);
            this.layoutControlPersonalFile.Controls.Add(this.radioButtonCoursesNotRealized);
            this.layoutControlPersonalFile.Controls.Add(this.textBoxDetails);
            this.layoutControlPersonalFile.Controls.Add(this.gridControlObs);
            this.layoutControlPersonalFile.Controls.Add(this.schedulerBarControl);
            this.layoutControlPersonalFile.Controls.Add(this.labelEmail);
            this.layoutControlPersonalFile.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlPersonalFile.Location = new System.Drawing.Point(0, 0);
            this.layoutControlPersonalFile.Name = "layoutControlPersonalFile";
            this.layoutControlPersonalFile.Root = this.layoutControlGroup1;
            this.layoutControlPersonalFile.Size = new System.Drawing.Size(1270, 787);
            this.layoutControlPersonalFile.TabIndex = 29;
            this.layoutControlPersonalFile.Text = "layoutControl1";
            // 
            // labelControlPhoto
            // 
            this.labelControlPhoto.Location = new System.Drawing.Point(24, 106);
            this.labelControlPhoto.Name = "labelControlPhoto";
            this.labelControlPhoto.Size = new System.Drawing.Size(111, 20);
            this.labelControlPhoto.StyleController = this.layoutControlPersonalFile;
            this.labelControlPhoto.TabIndex = 39;
            this.labelControlPhoto.Text = "labelControlPhoto";
            // 
            // panelExOperatorPicture
            // 
            this.panelExOperatorPicture.BorderColor = System.Drawing.Color.Empty;
            this.panelExOperatorPicture.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelExOperatorPicture.BorderWidth = 0;
            this.panelExOperatorPicture.Controls.Add(this.pictureBoxPhoto);
            this.panelExOperatorPicture.LoadFromResources = true;
            this.panelExOperatorPicture.Location = new System.Drawing.Point(142, 49);
            this.panelExOperatorPicture.Margin = new System.Windows.Forms.Padding(2);
            this.panelExOperatorPicture.Name = "panelExOperatorPicture";
            this.panelExOperatorPicture.PanelColor = System.Drawing.Color.Empty;
            this.panelExOperatorPicture.Size = new System.Drawing.Size(101, 129);
            this.panelExOperatorPicture.TabIndex = 2;
            // 
            // pictureBoxPhoto
            // 
            this.pictureBoxPhoto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxPhoto.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxPhoto.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBoxPhoto.Name = "pictureBoxPhoto";
            this.pictureBoxPhoto.Size = new System.Drawing.Size(99, 127);
            this.pictureBoxPhoto.TabIndex = 0;
            this.pictureBoxPhoto.TabStop = false;
            // 
            // gridControlCat
            // 
            this.gridControlCat.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(2);
            this.gridControlCat.EnableAutoFilter = false;
            this.gridControlCat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlCat.Location = new System.Drawing.Point(172, 669);
            this.gridControlCat.MainView = this.gridViewCat;
            this.gridControlCat.Margin = new System.Windows.Forms.Padding(2);
            this.gridControlCat.Name = "gridControlCat";
            this.gridControlCat.Size = new System.Drawing.Size(188, 128);
            this.gridControlCat.TabIndex = 35;
            this.gridControlCat.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewCat});
            this.gridControlCat.ViewTotalRows = false;
            // 
            // gridViewCat
            // 
            this.gridViewCat.AllowFocusedRowChanged = true;
            this.gridViewCat.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewCat.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewCat.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewCat.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewCat.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewCat.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewCat.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewCat.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewCat.EnablePreviewLineForFocusedRow = false;
            this.gridViewCat.GridControl = this.gridControlCat;
            this.gridViewCat.Name = "gridViewCat";
            this.gridViewCat.OptionsCustomization.AllowFilter = false;
            this.gridViewCat.OptionsMenu.EnableColumnMenu = false;
            this.gridViewCat.OptionsMenu.EnableFooterMenu = false;
            this.gridViewCat.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridViewCat.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewCat.OptionsView.ColumnAutoWidth = false;
            this.gridViewCat.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewCat.OptionsView.ShowGroupPanel = false;
            this.gridViewCat.OptionsView.ShowIndicator = false;
            this.gridViewCat.ViewTotalRows = false;
            // 
            // labelName
            // 
            this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelName.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelName.Location = new System.Drawing.Point(172, 192);
            this.labelName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(188, 21);
            this.labelName.TabIndex = 25;
            this.labelName.Text = "labelName";
            this.labelName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxAddress2
            // 
            this.textBoxAddress2.Enabled = false;
            this.textBoxAddress2.Location = new System.Drawing.Point(172, 357);
            this.textBoxAddress2.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxAddress2.Name = "textBoxAddress2";
            this.textBoxAddress2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textBoxAddress2.Properties.Appearance.BackColor2 = System.Drawing.Color.White;
            this.textBoxAddress2.Properties.Appearance.Options.UseBackColor = true;
            this.textBoxAddress2.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White;
            this.textBoxAddress2.Properties.AppearanceReadOnly.BackColor2 = System.Drawing.Color.White;
            this.textBoxAddress2.Properties.AppearanceReadOnly.Options.UseBackColor = true;
            this.textBoxAddress2.Size = new System.Drawing.Size(188, 105);
            this.textBoxAddress2.StyleController = this.layoutControlPersonalFile;
            this.textBoxAddress2.TabIndex = 37;
            // 
            // labelLastName
            // 
            this.labelLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLastName.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelLastName.Location = new System.Drawing.Point(172, 225);
            this.labelLastName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelLastName.Name = "labelLastName";
            this.labelLastName.Size = new System.Drawing.Size(188, 21);
            this.labelLastName.TabIndex = 26;
            this.labelLastName.Text = "labelLastName";
            this.labelLastName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelOperatorID
            // 
            this.labelOperatorID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOperatorID.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelOperatorID.Location = new System.Drawing.Point(172, 258);
            this.labelOperatorID.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelOperatorID.Name = "labelOperatorID";
            this.labelOperatorID.Size = new System.Drawing.Size(188, 21);
            this.labelOperatorID.TabIndex = 27;
            this.labelOperatorID.Text = "labelOperatorID";
            this.labelOperatorID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelRol
            // 
            this.labelRol.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRol.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelRol.Location = new System.Drawing.Point(172, 507);
            this.labelRol.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelRol.Name = "labelRol";
            this.labelRol.Size = new System.Drawing.Size(188, 21);
            this.labelRol.TabIndex = 32;
            this.labelRol.Text = "labelRol";
            this.labelRol.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelDateBirth
            // 
            this.labelDateBirth.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDateBirth.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelDateBirth.Location = new System.Drawing.Point(172, 291);
            this.labelDateBirth.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelDateBirth.Name = "labelDateBirth";
            this.labelDateBirth.Size = new System.Drawing.Size(188, 21);
            this.labelDateBirth.TabIndex = 28;
            this.labelDateBirth.Text = "labelDateBirth";
            this.labelDateBirth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelPhone
            // 
            this.labelPhone.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPhone.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelPhone.Location = new System.Drawing.Point(172, 324);
            this.labelPhone.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelPhone.Name = "labelPhone";
            this.labelPhone.Size = new System.Drawing.Size(188, 21);
            this.labelPhone.TabIndex = 29;
            this.labelPhone.Text = "labelPhone";
            this.labelPhone.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gridControlCourses
            // 
            this.gridControlCourses.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(2);
            this.gridControlCourses.EnableAutoFilter = true;
            this.gridControlCourses.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlCourses.Location = new System.Drawing.Point(403, 476);
            this.gridControlCourses.MainView = this.gridViewCoursesRealized;
            this.gridControlCourses.Margin = new System.Windows.Forms.Padding(2);
            this.gridControlCourses.Name = "gridControlCourses";
            this.gridControlCourses.Size = new System.Drawing.Size(393, 313);
            this.gridControlCourses.TabIndex = 6;
            this.gridControlCourses.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewCoursesRealized,
            this.gridView2});
            this.gridControlCourses.ViewTotalRows = true;
            // 
            // gridViewCoursesRealized
            // 
            this.gridViewCoursesRealized.AllowFocusedRowChanged = true;
            this.gridViewCoursesRealized.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewCoursesRealized.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewCoursesRealized.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewCoursesRealized.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewCoursesRealized.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewCoursesRealized.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewCoursesRealized.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewCoursesRealized.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewCoursesRealized.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewCoursesRealized.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewCoursesRealized.EnablePreviewLineForFocusedRow = false;
            this.gridViewCoursesRealized.GridControl = this.gridControlCourses;
            this.gridViewCoursesRealized.Name = "gridViewCoursesRealized";
            this.gridViewCoursesRealized.OptionsCustomization.AllowFilter = false;
            this.gridViewCoursesRealized.OptionsMenu.EnableColumnMenu = false;
            this.gridViewCoursesRealized.OptionsMenu.EnableFooterMenu = false;
            this.gridViewCoursesRealized.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridViewCoursesRealized.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewCoursesRealized.OptionsView.ColumnAutoWidth = false;
            this.gridViewCoursesRealized.OptionsView.ShowAutoFilterRow = true;
            this.gridViewCoursesRealized.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewCoursesRealized.OptionsView.ShowFooter = true;
            this.gridViewCoursesRealized.OptionsView.ShowGroupPanel = false;
            this.gridViewCoursesRealized.OptionsView.ShowIndicator = false;
            this.gridViewCoursesRealized.ViewTotalRows = true;
            this.gridViewCoursesRealized.DoubleClick += new System.EventHandler(this.gridViewCoursesRealized_DoubleClick);
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gridControlCourses;
            this.gridView2.Name = "gridView2";
            // 
            // comboBoxFilterEvaluations
            // 
            this.comboBoxFilterEvaluations.Location = new System.Drawing.Point(969, 443);
            this.comboBoxFilterEvaluations.Margin = new System.Windows.Forms.Padding(2);
            this.comboBoxFilterEvaluations.Name = "comboBoxFilterEvaluations";
            this.comboBoxFilterEvaluations.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxFilterEvaluations.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxFilterEvaluations.Size = new System.Drawing.Size(156, 20);
            this.comboBoxFilterEvaluations.StyleController = this.layoutControlPersonalFile;
            this.comboBoxFilterEvaluations.TabIndex = 7;
            this.comboBoxFilterEvaluations.SelectedValueChanged += new System.EventHandler(this.comboBoxFilterEvaluations_SelectedValueChanged);
            // 
            // radioButtonCoursesRealized
            // 
            this.radioButtonCoursesRealized.Checked = true;
            this.radioButtonCoursesRealized.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.radioButtonCoursesRealized.Location = new System.Drawing.Point(406, 446);
            this.radioButtonCoursesRealized.Margin = new System.Windows.Forms.Padding(2);
            this.radioButtonCoursesRealized.Name = "radioButtonCoursesRealized";
            this.radioButtonCoursesRealized.Size = new System.Drawing.Size(118, 26);
            this.radioButtonCoursesRealized.TabIndex = 5;
            this.radioButtonCoursesRealized.TabStop = true;
            this.radioButtonCoursesRealized.Text = "Completed";
            this.radioButtonCoursesRealized.UseVisualStyleBackColor = true;
            this.radioButtonCoursesRealized.CheckedChanged += new System.EventHandler(this.radioButtonCoursesRealized_CheckedChanged);
            // 
            // gridControlEva
            // 
            this.gridControlEva.ContextMenuStrip = this.contextMenuStrip1;
            this.gridControlEva.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(2);
            this.gridControlEva.EnableAutoFilter = true;
            this.gridControlEva.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlEva.Location = new System.Drawing.Point(824, 474);
            this.gridControlEva.MainView = this.gridViewAllEva;
            this.gridControlEva.Margin = new System.Windows.Forms.Padding(2);
            this.gridControlEva.Name = "gridControlEva";
            this.gridControlEva.Size = new System.Drawing.Size(393, 315);
            this.gridControlEva.TabIndex = 6;
            this.gridControlEva.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewAllEva});
            this.gridControlEva.ViewTotalRows = true;
            this.gridControlEva.DoubleClick += new System.EventHandler(this.gridViewAllEva_DoubleClick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.printMenuItem,
            this.exportPDFMenuItem,
            this.exportHTMLMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.ShowImageMargin = false;
            this.contextMenuStrip1.Size = new System.Drawing.Size(163, 70);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            this.contextMenuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenuStrip1_ItemClicked);
            // 
            // printMenuItem
            // 
            this.printMenuItem.Name = "printMenuItem";
            this.printMenuItem.Size = new System.Drawing.Size(162, 22);
            this.printMenuItem.Text = "Imprimir";
            // 
            // exportPDFMenuItem
            // 
            this.exportPDFMenuItem.Name = "exportPDFMenuItem";
            this.exportPDFMenuItem.Size = new System.Drawing.Size(162, 22);
            this.exportPDFMenuItem.Text = "Exportar como PDF";
            // 
            // exportHTMLMenuItem
            // 
            this.exportHTMLMenuItem.Name = "exportHTMLMenuItem";
            this.exportHTMLMenuItem.Size = new System.Drawing.Size(162, 22);
            this.exportHTMLMenuItem.Text = "Exportar como HTML";
            // 
            // gridViewAllEva
            // 
            this.gridViewAllEva.AllowFocusedRowChanged = true;
            this.gridViewAllEva.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewAllEva.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewAllEva.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewAllEva.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewAllEva.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewAllEva.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewAllEva.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewAllEva.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewAllEva.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewAllEva.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewAllEva.EnablePreviewLineForFocusedRow = false;
            this.gridViewAllEva.GridControl = this.gridControlEva;
            this.gridViewAllEva.Name = "gridViewAllEva";
            this.gridViewAllEva.OptionsCustomization.AllowFilter = false;
            this.gridViewAllEva.OptionsMenu.EnableColumnMenu = false;
            this.gridViewAllEva.OptionsMenu.EnableFooterMenu = false;
            this.gridViewAllEva.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridViewAllEva.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewAllEva.OptionsView.ColumnAutoWidth = false;
            this.gridViewAllEva.OptionsView.ShowAutoFilterRow = true;
            this.gridViewAllEva.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewAllEva.OptionsView.ShowFooter = true;
            this.gridViewAllEva.OptionsView.ShowGroupPanel = false;
            this.gridViewAllEva.OptionsView.ShowIndicator = false;
            this.gridViewAllEva.ViewTotalRows = true;
            this.gridViewAllEva.ShowGridMenu += new DevExpress.XtraGrid.Views.Grid.GridMenuEventHandler(this.gridViewAllEva_ShowGridMenu);
            // 
            // radioButtonCoursesNotRealized
            // 
            this.radioButtonCoursesNotRealized.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.radioButtonCoursesNotRealized.Location = new System.Drawing.Point(534, 446);
            this.radioButtonCoursesNotRealized.Margin = new System.Windows.Forms.Padding(2);
            this.radioButtonCoursesNotRealized.Name = "radioButtonCoursesNotRealized";
            this.radioButtonCoursesNotRealized.Size = new System.Drawing.Size(259, 25);
            this.radioButtonCoursesNotRealized.TabIndex = 4;
            this.radioButtonCoursesNotRealized.TabStop = true;
            this.radioButtonCoursesNotRealized.Text = "Scheduled";
            this.radioButtonCoursesNotRealized.UseVisualStyleBackColor = true;
            this.radioButtonCoursesNotRealized.CheckedChanged += new System.EventHandler(this.radioButtonCoursesNotRealized_CheckedChanged);
            // 
            // textBoxDetails
            // 
            this.textBoxDetails.Location = new System.Drawing.Point(824, 76);
            this.textBoxDetails.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxDetails.Name = "textBoxDetails";
            this.textBoxDetails.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textBoxDetails.Properties.Appearance.Options.UseBackColor = true;
            this.textBoxDetails.Properties.ReadOnly = true;
            this.textBoxDetails.Size = new System.Drawing.Size(393, 319);
            this.textBoxDetails.StyleController = this.layoutControlPersonalFile;
            this.textBoxDetails.TabIndex = 1;
            // 
            // gridControlObs
            // 
            this.gridControlObs.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(2);
            this.gridControlObs.EnableAutoFilter = true;
            this.gridControlObs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.gridControlObs.Location = new System.Drawing.Point(403, 76);
            this.gridControlObs.MainView = this.gridViewObs;
            this.gridControlObs.Margin = new System.Windows.Forms.Padding(2);
            this.gridControlObs.Name = "gridControlObs";
            this.gridControlObs.Size = new System.Drawing.Size(393, 319);
            this.gridControlObs.TabIndex = 2;
            this.gridControlObs.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewObs});
            this.gridControlObs.ViewTotalRows = true;
            this.gridControlObs.ProcessGridKey += new System.Windows.Forms.KeyEventHandler(this.gridControlObs_ProcessGridKey);
            // 
            // gridViewObs
            // 
            this.gridViewObs.AllowFocusedRowChanged = true;
            this.gridViewObs.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewObs.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewObs.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewObs.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewObs.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewObs.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewObs.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewObs.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewObs.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewObs.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewObs.EnablePreviewLineForFocusedRow = false;
            this.gridViewObs.GridControl = this.gridControlObs;
            this.gridViewObs.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "", null, "")});
            this.gridViewObs.Name = "gridViewObs";
            this.gridViewObs.OptionsBehavior.AllowIncrementalSearch = true;
            this.gridViewObs.OptionsBehavior.Editable = false;
            this.gridViewObs.OptionsCustomization.AllowFilter = false;
            this.gridViewObs.OptionsMenu.EnableColumnMenu = false;
            this.gridViewObs.OptionsMenu.EnableFooterMenu = false;
            this.gridViewObs.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewObs.OptionsView.ColumnAutoWidth = false;
            this.gridViewObs.OptionsView.ShowAutoFilterRow = true;
            this.gridViewObs.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewObs.OptionsView.ShowFooter = true;
            this.gridViewObs.OptionsView.ShowGroupPanel = false;
            this.gridViewObs.OptionsView.ShowIndicator = false;
            this.gridViewObs.ViewTotalRows = true;
            this.gridViewObs.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            // 
            // schedulerBarControl
            // 
            this.schedulerBarControl.Location = new System.Drawing.Point(392, 45);
            this.schedulerBarControl.Margin = new System.Windows.Forms.Padding(2);
            this.schedulerBarControl.Name = "schedulerBarControl";
            this.schedulerBarControl.Size = new System.Drawing.Size(836, 755);
            this.schedulerBarControl.TabIndex = 0;
            // 
            // labelEmail
            // 
            this.labelEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEmail.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelEmail.Location = new System.Drawing.Point(172, 474);
            this.labelEmail.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelEmail.Name = "labelEmail";
            this.labelEmail.Size = new System.Drawing.Size(188, 21);
            this.labelEmail.TabIndex = 31;
            this.labelEmail.Text = "labelEmail";
            this.labelEmail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupUserPersonalInfo,
            this.tabbedControlGroupUser});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1253, 825);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroupUserPersonalInfo
            // 
            this.layoutControlGroupUserPersonalInfo.AppearanceGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupUserPersonalInfo.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroupUserPersonalInfo.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupUserPersonalInfo.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroupUserPersonalInfo.CustomizationFormText = "layoutControlGroupUserPersonalInfo";
            this.layoutControlGroupUserPersonalInfo.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemPhoto,
            this.layoutControlItemName,
            this.layoutControlItemLastName,
            this.layoutControlItemOpId,
            this.layoutControlItemDateBirth,
            this.layoutControlItemPhone,
            this.layoutControlItemAddress,
            this.layoutControlItemEmail,
            this.layoutControlItemRol,
            this.layoutControlItemDepartments,
            this.layoutControlItemCategory,
            this.emptySpaceItem1,
            this.layoutControlItemUserPhoto,
            this.emptySpaceItem3,
            this.emptySpaceItem4});
            this.layoutControlGroupUserPersonalInfo.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupUserPersonalInfo.Name = "layoutControlGroupUserPersonalInfo";
            this.layoutControlGroupUserPersonalInfo.Size = new System.Drawing.Size(367, 805);
            this.layoutControlGroupUserPersonalInfo.Text = "layoutControlGroupUserPersonalInfo";
            // 
            // layoutControlItemPhoto
            // 
            this.layoutControlItemPhoto.Control = this.panelExOperatorPicture;
            this.layoutControlItemPhoto.CustomizationFormText = "layoutControlItemPhoto";
            this.layoutControlItemPhoto.Location = new System.Drawing.Point(115, 0);
            this.layoutControlItemPhoto.MaxSize = new System.Drawing.Size(111, 145);
            this.layoutControlItemPhoto.MinSize = new System.Drawing.Size(111, 145);
            this.layoutControlItemPhoto.Name = "layoutControlItemPhoto";
            this.layoutControlItemPhoto.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 8, 8);
            this.layoutControlItemPhoto.Size = new System.Drawing.Size(111, 145);
            this.layoutControlItemPhoto.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemPhoto.Text = "layoutControlItemPhoto";
            this.layoutControlItemPhoto.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemPhoto.TextToControlDistance = 0;
            this.layoutControlItemPhoto.TextVisible = false;
            // 
            // layoutControlItemName
            // 
            this.layoutControlItemName.Control = this.labelName;
            this.layoutControlItemName.CustomizationFormText = "layoutControlItemName";
            this.layoutControlItemName.Location = new System.Drawing.Point(0, 145);
            this.layoutControlItemName.MaxSize = new System.Drawing.Size(343, 33);
            this.layoutControlItemName.MinSize = new System.Drawing.Size(343, 33);
            this.layoutControlItemName.Name = "layoutControlItemName";
            this.layoutControlItemName.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 6, 6);
            this.layoutControlItemName.Size = new System.Drawing.Size(343, 33);
            this.layoutControlItemName.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemName.Text = "layoutControlItemName";
            this.layoutControlItemName.TextSize = new System.Drawing.Size(141, 13);
            // 
            // layoutControlItemLastName
            // 
            this.layoutControlItemLastName.Control = this.labelLastName;
            this.layoutControlItemLastName.CustomizationFormText = "layoutControlItemLastName";
            this.layoutControlItemLastName.Location = new System.Drawing.Point(0, 178);
            this.layoutControlItemLastName.MaxSize = new System.Drawing.Size(343, 33);
            this.layoutControlItemLastName.MinSize = new System.Drawing.Size(343, 33);
            this.layoutControlItemLastName.Name = "layoutControlItemLastName";
            this.layoutControlItemLastName.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 6, 6);
            this.layoutControlItemLastName.Size = new System.Drawing.Size(343, 33);
            this.layoutControlItemLastName.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemLastName.Text = "layoutControlItemLastName";
            this.layoutControlItemLastName.TextSize = new System.Drawing.Size(141, 13);
            // 
            // layoutControlItemOpId
            // 
            this.layoutControlItemOpId.Control = this.labelOperatorID;
            this.layoutControlItemOpId.CustomizationFormText = "layoutControlItemOpId";
            this.layoutControlItemOpId.Location = new System.Drawing.Point(0, 211);
            this.layoutControlItemOpId.MaxSize = new System.Drawing.Size(343, 33);
            this.layoutControlItemOpId.MinSize = new System.Drawing.Size(343, 33);
            this.layoutControlItemOpId.Name = "layoutControlItemOpId";
            this.layoutControlItemOpId.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 6, 6);
            this.layoutControlItemOpId.Size = new System.Drawing.Size(343, 33);
            this.layoutControlItemOpId.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemOpId.Text = "layoutControlItemOpId";
            this.layoutControlItemOpId.TextSize = new System.Drawing.Size(141, 13);
            // 
            // layoutControlItemDateBirth
            // 
            this.layoutControlItemDateBirth.Control = this.labelDateBirth;
            this.layoutControlItemDateBirth.CustomizationFormText = "layoutControlItemDateBirth";
            this.layoutControlItemDateBirth.Location = new System.Drawing.Point(0, 244);
            this.layoutControlItemDateBirth.MaxSize = new System.Drawing.Size(343, 33);
            this.layoutControlItemDateBirth.MinSize = new System.Drawing.Size(343, 33);
            this.layoutControlItemDateBirth.Name = "layoutControlItemDateBirth";
            this.layoutControlItemDateBirth.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 6, 6);
            this.layoutControlItemDateBirth.Size = new System.Drawing.Size(343, 33);
            this.layoutControlItemDateBirth.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemDateBirth.Text = "layoutControlItemDateBirth";
            this.layoutControlItemDateBirth.TextSize = new System.Drawing.Size(141, 13);
            // 
            // layoutControlItemPhone
            // 
            this.layoutControlItemPhone.Control = this.labelPhone;
            this.layoutControlItemPhone.CustomizationFormText = "layoutControlItemPhone";
            this.layoutControlItemPhone.Location = new System.Drawing.Point(0, 277);
            this.layoutControlItemPhone.MaxSize = new System.Drawing.Size(343, 33);
            this.layoutControlItemPhone.MinSize = new System.Drawing.Size(343, 33);
            this.layoutControlItemPhone.Name = "layoutControlItemPhone";
            this.layoutControlItemPhone.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 6, 6);
            this.layoutControlItemPhone.Size = new System.Drawing.Size(343, 33);
            this.layoutControlItemPhone.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemPhone.Text = "layoutControlItemPhone";
            this.layoutControlItemPhone.TextSize = new System.Drawing.Size(141, 13);
            // 
            // layoutControlItemAddress
            // 
            this.layoutControlItemAddress.Control = this.textBoxAddress2;
            this.layoutControlItemAddress.CustomizationFormText = "layoutControlItemAddress";
            this.layoutControlItemAddress.Location = new System.Drawing.Point(0, 310);
            this.layoutControlItemAddress.MaxSize = new System.Drawing.Size(343, 117);
            this.layoutControlItemAddress.MinSize = new System.Drawing.Size(343, 117);
            this.layoutControlItemAddress.Name = "layoutControlItemAddress";
            this.layoutControlItemAddress.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 6, 6);
            this.layoutControlItemAddress.Size = new System.Drawing.Size(343, 117);
            this.layoutControlItemAddress.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemAddress.Text = "layoutControlItemAddress";
            this.layoutControlItemAddress.TextSize = new System.Drawing.Size(141, 13);
            // 
            // layoutControlItemEmail
            // 
            this.layoutControlItemEmail.Control = this.labelEmail;
            this.layoutControlItemEmail.CustomizationFormText = "layoutControlItemEmail";
            this.layoutControlItemEmail.Location = new System.Drawing.Point(0, 427);
            this.layoutControlItemEmail.MaxSize = new System.Drawing.Size(343, 33);
            this.layoutControlItemEmail.MinSize = new System.Drawing.Size(343, 33);
            this.layoutControlItemEmail.Name = "layoutControlItemEmail";
            this.layoutControlItemEmail.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 6, 6);
            this.layoutControlItemEmail.Size = new System.Drawing.Size(343, 33);
            this.layoutControlItemEmail.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemEmail.Text = "layoutControlItemEmail";
            this.layoutControlItemEmail.TextSize = new System.Drawing.Size(141, 13);
            // 
            // layoutControlItemRol
            // 
            this.layoutControlItemRol.Control = this.labelRol;
            this.layoutControlItemRol.CustomizationFormText = "layoutControlItemRol";
            this.layoutControlItemRol.Location = new System.Drawing.Point(0, 460);
            this.layoutControlItemRol.MaxSize = new System.Drawing.Size(343, 33);
            this.layoutControlItemRol.MinSize = new System.Drawing.Size(343, 33);
            this.layoutControlItemRol.Name = "layoutControlItemRol";
            this.layoutControlItemRol.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 6, 6);
            this.layoutControlItemRol.Size = new System.Drawing.Size(343, 33);
            this.layoutControlItemRol.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemRol.Text = "layoutControlItemRol";
            this.layoutControlItemRol.TextSize = new System.Drawing.Size(141, 13);
            // 
            // layoutControlItemDepartments
            // 
            this.layoutControlItemDepartments.Control = this.listBoxDepartaments;
            this.layoutControlItemDepartments.CustomizationFormText = "layoutControlItemDepartments";
            this.layoutControlItemDepartments.Location = new System.Drawing.Point(0, 493);
            this.layoutControlItemDepartments.MaxSize = new System.Drawing.Size(0, 129);
            this.layoutControlItemDepartments.MinSize = new System.Drawing.Size(173, 129);
            this.layoutControlItemDepartments.Name = "layoutControlItemDepartments";
            this.layoutControlItemDepartments.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 6, 6);
            this.layoutControlItemDepartments.Size = new System.Drawing.Size(343, 129);
            this.layoutControlItemDepartments.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemDepartments.Text = "layoutControlItemDepartments";
            this.layoutControlItemDepartments.TextSize = new System.Drawing.Size(141, 13);
            // 
            // layoutControlItemCategory
            // 
            this.layoutControlItemCategory.Control = this.gridControlCat;
            this.layoutControlItemCategory.CustomizationFormText = "layoutControlItemCategory";
            this.layoutControlItemCategory.Location = new System.Drawing.Point(0, 622);
            this.layoutControlItemCategory.MaxSize = new System.Drawing.Size(343, 140);
            this.layoutControlItemCategory.MinSize = new System.Drawing.Size(343, 140);
            this.layoutControlItemCategory.Name = "layoutControlItemCategory";
            this.layoutControlItemCategory.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 6, 6);
            this.layoutControlItemCategory.Size = new System.Drawing.Size(343, 140);
            this.layoutControlItemCategory.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemCategory.Text = "layoutControlItemCategory";
            this.layoutControlItemCategory.TextSize = new System.Drawing.Size(141, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(226, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(110, 30);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(117, 145);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItemUserPhoto
            // 
            this.layoutControlItemUserPhoto.Control = this.labelControlPhoto;
            this.layoutControlItemUserPhoto.CustomizationFormText = "layoutControlItemUserPhoto";
            this.layoutControlItemUserPhoto.Location = new System.Drawing.Point(0, 63);
            this.layoutControlItemUserPhoto.MinSize = new System.Drawing.Size(96, 24);
            this.layoutControlItemUserPhoto.Name = "layoutControlItemUserPhoto";
            this.layoutControlItemUserPhoto.Size = new System.Drawing.Size(115, 24);
            this.layoutControlItemUserPhoto.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemUserPhoto.Text = "layoutControlItemUserPhoto";
            this.layoutControlItemUserPhoto.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemUserPhoto.TextToControlDistance = 0;
            this.layoutControlItemUserPhoto.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(115, 63);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 87);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(115, 58);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // tabbedControlGroupUser
            // 
            this.tabbedControlGroupUser.CustomizationFormText = "tabbedControlGroupUser";
            this.tabbedControlGroupUser.Location = new System.Drawing.Point(367, 0);
            this.tabbedControlGroupUser.Name = "tabbedControlGroupUser";
            this.tabbedControlGroupUser.SelectedTabPage = this.layoutControlGroupScheduler;
            this.tabbedControlGroupUser.SelectedTabPageIndex = 1;
            this.tabbedControlGroupUser.Size = new System.Drawing.Size(866, 805);
            this.tabbedControlGroupUser.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupUserInfo,
            this.layoutControlGroupScheduler});
            this.tabbedControlGroupUser.Text = "tabbedControlGroupUser";
            // 
            // layoutControlGroupScheduler
            // 
            this.layoutControlGroupScheduler.CustomizationFormText = "layoutControlGroupScheduler";
            this.layoutControlGroupScheduler.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem7});
            this.layoutControlGroupScheduler.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupScheduler.Name = "layoutControlGroupScheduler";
            this.layoutControlGroupScheduler.Size = new System.Drawing.Size(842, 761);
            this.layoutControlGroupScheduler.Text = "layoutControlGroupScheduler";
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.schedulerBarControl;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem7.Size = new System.Drawing.Size(842, 761);
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlGroupUserInfo
            // 
            this.layoutControlGroupUserInfo.CustomizationFormText = "layoutControlGroupUserInfo";
            this.layoutControlGroupUserInfo.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupObs,
            this.layoutControlGroupObsDetails,
            this.layoutControlGroupCourse,
            this.layoutControlGroupEval});
            this.layoutControlGroupUserInfo.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupUserInfo.Name = "layoutControlGroupUserInfo";
            this.layoutControlGroupUserInfo.Size = new System.Drawing.Size(842, 761);
            this.layoutControlGroupUserInfo.Text = "layoutControlGroupUserInfo";
            // 
            // layoutControlGroupObs
            // 
            this.layoutControlGroupObs.CustomizationFormText = "layoutControlGroupObs";
            this.layoutControlGroupObs.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroupObs.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupObs.Name = "layoutControlGroupObs";
            this.layoutControlGroupObs.Size = new System.Drawing.Size(421, 367);
            this.layoutControlGroupObs.Text = "layoutControlGroupObs";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridControlObs;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(397, 323);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroupObsDetails
            // 
            this.layoutControlGroupObsDetails.CustomizationFormText = "layoutControlGroupObsDetails";
            this.layoutControlGroupObsDetails.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroupObsDetails.Location = new System.Drawing.Point(421, 0);
            this.layoutControlGroupObsDetails.Name = "layoutControlGroupObsDetails";
            this.layoutControlGroupObsDetails.Size = new System.Drawing.Size(421, 367);
            this.layoutControlGroupObsDetails.Text = "layoutControlGroupObsDetails";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.textBoxDetails;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(397, 323);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlGroupCourse
            // 
            this.layoutControlGroupCourse.CustomizationFormText = "layoutControlGroupCourse";
            this.layoutControlGroupCourse.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5});
            this.layoutControlGroupCourse.Location = new System.Drawing.Point(0, 367);
            this.layoutControlGroupCourse.Name = "layoutControlGroupCourse";
            this.layoutControlGroupCourse.Size = new System.Drawing.Size(421, 394);
            this.layoutControlGroupCourse.Text = "layoutControlGroupCourse";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.radioButtonCoursesRealized;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(128, 33);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(128, 33);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 2);
            this.layoutControlItem3.Size = new System.Drawing.Size(128, 33);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.radioButtonCoursesNotRealized;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(128, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 2);
            this.layoutControlItem4.Size = new System.Drawing.Size(269, 33);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.gridControlCourses;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 33);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(397, 317);
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlGroupEval
            // 
            this.layoutControlGroupEval.CustomizationFormText = "layoutControlGroupEval";
            this.layoutControlGroupEval.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemShowFor,
            this.layoutControlItem6,
            this.emptySpaceItem2});
            this.layoutControlGroupEval.Location = new System.Drawing.Point(421, 367);
            this.layoutControlGroupEval.Name = "layoutControlGroupEval";
            this.layoutControlGroupEval.Size = new System.Drawing.Size(421, 394);
            this.layoutControlGroupEval.Text = "layoutControlGroupEval";
            // 
            // layoutControlItemShowFor
            // 
            this.layoutControlItemShowFor.Control = this.comboBoxFilterEvaluations;
            this.layoutControlItemShowFor.CustomizationFormText = "layoutControlItemShowFor";
            this.layoutControlItemShowFor.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemShowFor.MaxSize = new System.Drawing.Size(305, 31);
            this.layoutControlItemShowFor.MinSize = new System.Drawing.Size(305, 31);
            this.layoutControlItemShowFor.Name = "layoutControlItemShowFor";
            this.layoutControlItemShowFor.Size = new System.Drawing.Size(305, 31);
            this.layoutControlItemShowFor.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemShowFor.Text = "layoutControlItemShowFor";
            this.layoutControlItemShowFor.TextSize = new System.Drawing.Size(141, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.gridControlEva;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 31);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(397, 319);
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(305, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(92, 31);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.Filter = "html | *.html";
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "logout.gif");
            // 
            // barButtonItemExit
            // 
            this.barButtonItemExit.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barButtonItemExit.AllowAllUp = true;
            this.barButtonItemExit.Caption = "barButtonItem19";
            this.barButtonItemExit.Id = 201;
            this.barButtonItemExit.ImageIndex = 0;
            this.barButtonItemExit.Name = "barButtonItemExit";
            this.barButtonItemExit.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            toolTipTitleItem7.Text = "Salir";
            superToolTip7.Items.Add(toolTipTitleItem7);
            this.barButtonItemExit.SuperTip = superToolTip7;
            // 
            // barButtonItemHelp
            // 
            this.barButtonItemHelp.Caption = "Ayuda en lnea";
            this.barButtonItemHelp.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemHelp.Glyph")));
            this.barButtonItemHelp.Id = 232;
            this.barButtonItemHelp.Name = "barButtonItemHelp";
            toolTipTitleItem8.Text = "Ayuda en lnea";
            superToolTip8.Items.Add(toolTipTitleItem8);
            this.barButtonItemHelp.SuperTip = superToolTip8;
            // 
            // ribbonPageGroupGeneralOptMonitor
            // 
            this.ribbonPageGroupGeneralOptMonitor.AllowMinimize = false;
            this.ribbonPageGroupGeneralOptMonitor.AllowTextClipping = false;
            this.ribbonPageGroupGeneralOptMonitor.Name = "ribbonPageGroupGeneralOptMonitor";
            this.ribbonPageGroupGeneralOptMonitor.ShowCaptionButton = false;
            this.ribbonPageGroupGeneralOptMonitor.Text = "Opciones generales";
            // 
            // ribbonPageGroupRoom
            // 
            this.ribbonPageGroupRoom.AllowMinimize = false;
            this.ribbonPageGroupRoom.AllowTextClipping = false;
            this.ribbonPageGroupRoom.Name = "ribbonPageGroupRoom";
            this.ribbonPageGroupRoom.ShowCaptionButton = false;
            this.ribbonPageGroupRoom.Text = "Ubicar en sala";
            // 
            // XrDesignRibbonPageGroupViewActivities
            // 
            this.XrDesignRibbonPageGroupViewActivities.AllowMinimize = false;
            this.XrDesignRibbonPageGroupViewActivities.AllowTextClipping = false;
            this.XrDesignRibbonPageGroupViewActivities.Kind = DevExpress.XtraReports.UserDesigner.XRDesignRibbonPageGroupKind.SizeAndLayout;
            this.XrDesignRibbonPageGroupViewActivities.Name = "XrDesignRibbonPageGroupViewActivities";
            this.XrDesignRibbonPageGroupViewActivities.ShowCaptionButton = false;
            this.XrDesignRibbonPageGroupViewActivities.Text = "Ver actividades";
            // 
            // ribbonPageGroupPerformance
            // 
            this.ribbonPageGroupPerformance.Kind = DevExpress.XtraReports.UserDesigner.XRDesignRibbonPageGroupKind.Edit;
            this.ribbonPageGroupPerformance.Name = "ribbonPageGroupPerformance";
            this.ribbonPageGroupPerformance.ShowCaptionButton = false;
            this.ribbonPageGroupPerformance.Text = "Desempeno";
            // 
            // RibbonPageMonitoring
            // 
            this.RibbonPageMonitoring.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupPerformance,
            this.XrDesignRibbonPageGroupViewActivities,
            this.ribbonPageGroupRoom,
            this.ribbonPageGroupGeneralOptMonitor});
            this.RibbonPageMonitoring.Name = "RibbonPageMonitoring";
            this.RibbonPageMonitoring.Text = "Monitoreo";
            // 
            // ribbonPageGroupWindows
            // 
            this.ribbonPageGroupWindows.AllowMinimize = false;
            this.ribbonPageGroupWindows.AllowTextClipping = false;
            this.ribbonPageGroupWindows.Name = "ribbonPageGroupWindows";
            this.ribbonPageGroupWindows.ShowCaptionButton = false;
            this.ribbonPageGroupWindows.Text = "Ventanas";
            // 
            // ribbonPageGroupAccessProfile
            // 
            this.ribbonPageGroupAccessProfile.AllowMinimize = false;
            this.ribbonPageGroupAccessProfile.AllowTextClipping = false;
            this.ribbonPageGroupAccessProfile.Name = "ribbonPageGroupAccessProfile";
            this.ribbonPageGroupAccessProfile.ShowCaptionButton = false;
            this.ribbonPageGroupAccessProfile.Text = "Perfil de acceso";
            // 
            // ribbonPageGroupPreferences
            // 
            this.ribbonPageGroupPreferences.Kind = DevExpress.XtraReports.UserDesigner.XRDesignRibbonPageGroupKind.HtmlNavigation;
            this.ribbonPageGroupPreferences.Name = "ribbonPageGroupPreferences";
            this.ribbonPageGroupPreferences.ShowCaptionButton = false;
            this.ribbonPageGroupPreferences.Text = "Preferencias";
            // 
            // RibbonPageTools
            // 
            this.RibbonPageTools.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupPreferences,
            this.ribbonPageGroupAccessProfile,
            this.ribbonPageGroupWindows});
            this.RibbonPageTools.Name = "RibbonPageTools";
            this.RibbonPageTools.Text = "Herramientas";
            // 
            // barButtonItemUpdate
            // 
            this.barButtonItemUpdate.Enabled = false;
            this.barButtonItemUpdate.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemUpdate.Glyph")));
            this.barButtonItemUpdate.Id = 228;
            this.barButtonItemUpdate.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F5);
            this.barButtonItemUpdate.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemUpdate.Name = "barButtonItemUpdate";
            this.barButtonItemUpdate.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // ribbonPageGroupFinalReport
            // 
            this.ribbonPageGroupFinalReport.Kind = DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroupKind.Navigation;
            this.ribbonPageGroupFinalReport.Name = "ribbonPageGroupFinalReport";
            this.ribbonPageGroupFinalReport.ShowCaptionButton = false;
            this.ribbonPageGroupFinalReport.Text = "Reporte de Cierre";
            // 
            // ribbonPageGroupTraining
            // 
            this.ribbonPageGroupTraining.Kind = DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroupKind.PageSetup;
            this.ribbonPageGroupTraining.Name = "ribbonPageGroupTraining";
            this.ribbonPageGroupTraining.ShowCaptionButton = false;
            superToolTip9.FixedTooltipWidth = true;
            toolTipTitleItem9.Text = "Page Setup";
            toolTipItem5.Appearance.Options.UseImage = true;
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Show the Page Setup dialog.";
            superToolTip9.Items.Add(toolTipTitleItem9);
            superToolTip9.Items.Add(toolTipItem5);
            superToolTip9.MaxWidth = 210;
            this.ribbonPageGroupTraining.SuperTip = superToolTip9;
            this.ribbonPageGroupTraining.Text = "Entrenamiento";
            // 
            // printPreviewBarItemPersonalfile
            // 
            this.printPreviewBarItemPersonalfile.Caption = "Ver expediente";
            this.printPreviewBarItemPersonalfile.Id = 66;
            this.printPreviewBarItemPersonalfile.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("printPreviewBarItemPersonalfile.LargeGlyph")));
            this.printPreviewBarItemPersonalfile.Name = "printPreviewBarItemPersonalfile";
            superToolTip10.FixedTooltipWidth = true;
            toolTipTitleItem10.Text = "() Expediente";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Permite consultar el expediente del operador";
            superToolTip10.Items.Add(toolTipTitleItem10);
            superToolTip10.Items.Add(toolTipItem6);
            superToolTip10.MaxWidth = 210;
            this.printPreviewBarItemPersonalfile.SuperTip = superToolTip10;
            // 
            // ribbonPageGroupAssignment
            // 
            this.ribbonPageGroupAssignment.AllowMinimize = false;
            this.ribbonPageGroupAssignment.AllowTextClipping = false;
            this.ribbonPageGroupAssignment.Name = "ribbonPageGroupAssignment";
            this.ribbonPageGroupAssignment.ShowCaptionButton = false;
            this.ribbonPageGroupAssignment.Text = "Asignacion";
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "logout.gif");
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "logout.gif");
            // 
            // RibbonPageOperation
            // 
            this.RibbonPageOperation.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupPersonalFile,
            this.ribbonPageGroupOptions});
            this.RibbonPageOperation.Name = "RibbonPageOperation";
            this.RibbonPageOperation.Text = "Operacion";
            // 
            // ribbonPageGroupPersonalFile
            // 
            this.ribbonPageGroupPersonalFile.AllowMinimize = false;
            this.ribbonPageGroupPersonalFile.AllowTextClipping = false;
            this.ribbonPageGroupPersonalFile.ItemLinks.Add(this.barButtonItemEvaluateOperator);
            this.ribbonPageGroupPersonalFile.ItemLinks.Add(this.printPreviewBarItemModifyCategory);
            this.ribbonPageGroupPersonalFile.ItemLinks.Add(this.printPreviewBarItemPersonalFileObs);
            this.ribbonPageGroupPersonalFile.Kind = DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroupKind.Print;
            this.ribbonPageGroupPersonalFile.Name = "ribbonPageGroupPersonalFile";
            this.ribbonPageGroupPersonalFile.ShowCaptionButton = false;
            this.ribbonPageGroupPersonalFile.Text = "Expediente";
            // 
            // barButtonItemEvaluateOperator
            // 
            this.barButtonItemEvaluateOperator.Caption = "Evaluar operador";
            this.barButtonItemEvaluateOperator.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemEvaluateOperator.Glyph")));
            this.barButtonItemEvaluateOperator.Id = 170;
            this.barButtonItemEvaluateOperator.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemEvaluateOperator.Name = "barButtonItemEvaluateOperator";
            this.barButtonItemEvaluateOperator.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemEvaluateOperator.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemEvaluateOperator_ItemClick);
            // 
            // printPreviewBarItemModifyCategory
            // 
            this.printPreviewBarItemModifyCategory.Caption = "Modificar categora";
            this.printPreviewBarItemModifyCategory.Glyph = ((System.Drawing.Image)(resources.GetObject("printPreviewBarItemModifyCategory.Glyph")));
            this.printPreviewBarItemModifyCategory.Id = 68;
            this.printPreviewBarItemModifyCategory.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("printPreviewBarItemModifyCategory.LargeGlyph")));
            this.printPreviewBarItemModifyCategory.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.printPreviewBarItemModifyCategory.Name = "printPreviewBarItemModifyCategory";
            this.printPreviewBarItemModifyCategory.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            superToolTip11.FixedTooltipWidth = true;
            toolTipTitleItem11.Text = "Actualizar categoria";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Permite actualizar la categoria del operador.";
            superToolTip11.Items.Add(toolTipTitleItem11);
            superToolTip11.Items.Add(toolTipItem7);
            superToolTip11.MaxWidth = 210;
            this.printPreviewBarItemModifyCategory.SuperTip = superToolTip11;
            this.printPreviewBarItemModifyCategory.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.printPreviewBarItemModifyCategory_ItemClick);
            // 
            // printPreviewBarItemPersonalFileObs
            // 
            this.printPreviewBarItemPersonalFileObs.Caption = "Agregar observacin";
            this.printPreviewBarItemPersonalFileObs.Glyph = ((System.Drawing.Image)(resources.GetObject("printPreviewBarItemPersonalFileObs.Glyph")));
            this.printPreviewBarItemPersonalFileObs.Id = 67;
            this.printPreviewBarItemPersonalFileObs.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.printPreviewBarItemPersonalFileObs.Name = "printPreviewBarItemPersonalFileObs";
            this.printPreviewBarItemPersonalFileObs.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            superToolTip12.FixedTooltipWidth = true;
            toolTipTitleItem12.Text = "Agregar Observacin";
            toolTipItem8.LeftIndent = 6;
            toolTipItem8.Text = "Permite agregar una observacin al operador seleccionado.";
            superToolTip12.Items.Add(toolTipTitleItem12);
            superToolTip12.Items.Add(toolTipItem8);
            superToolTip12.MaxWidth = 210;
            this.printPreviewBarItemPersonalFileObs.SuperTip = superToolTip12;
            this.printPreviewBarItemPersonalFileObs.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.printPreviewBarItemPersonalFileObs_ItemClick);
            // 
            // ribbonPageGroupOptions
            // 
            this.ribbonPageGroupOptions.AllowTextClipping = false;
            this.ribbonPageGroupOptions.ItemLinks.Add(this.barButtonItemPrint);
            this.ribbonPageGroupOptions.ItemLinks.Add(this.barButtonItemSave);
            this.ribbonPageGroupOptions.ItemLinks.Add(this.barButtonItemSend);
            this.ribbonPageGroupOptions.ItemLinks.Add(this.barButtonItemUpdate);
            this.ribbonPageGroupOptions.Name = "ribbonPageGroupOptions";
            this.ribbonPageGroupOptions.ShowCaptionButton = false;
            this.ribbonPageGroupOptions.Text = "Opciones generales";
            // 
            // barButtonItemPrint
            // 
            this.barButtonItemPrint.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.Glyph")));
            this.barButtonItemPrint.Id = 231;
            this.barButtonItemPrint.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P));
            this.barButtonItemPrint.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemPrint.Name = "barButtonItemPrint";
            this.barButtonItemPrint.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPrint_ItemClick);
            // 
            // barButtonItemSave
            // 
            this.barButtonItemSave.Glyph = global::SmartCadGuiCommon.PrintRibbonControllerResources.RibbonPrintPreview_Save;
            this.barButtonItemSave.Id = 230;
            this.barButtonItemSave.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S));
            this.barButtonItemSave.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemSave.Name = "barButtonItemSave";
            this.barButtonItemSave.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemSave_ItemClick);
            // 
            // barButtonItemSend
            // 
            this.barButtonItemSend.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemSend.Glyph")));
            this.barButtonItemSend.Id = 229;
            this.barButtonItemSend.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Enter));
            this.barButtonItemSend.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemSend.Name = "barButtonItemSend";
            this.barButtonItemSend.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemSend.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemSend_ItemClick);
            // 
            // RibbonControl
            // 
            this.RibbonControl.Dock = System.Windows.Forms.DockStyle.None;
            this.RibbonControl.ExpandCollapseItem.Id = 0;
            this.RibbonControl.ExpandCollapseItem.Name = "";
            this.RibbonControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RibbonControl.Images = this.imageList2;
            this.RibbonControl.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.RibbonControl.ExpandCollapseItem,
            this.printPreviewBarItemPersonalFileObs,
            this.printPreviewBarItemModifyCategory,
            this.barButtonItemEvaluateOperator,
            this.barButtonItemSend,
            this.barButtonItemSave,
            this.barButtonItemPrint,
            this.barButtonItemUpdate,
            this.barStaticItem1,
            this.barStaticItem2,
            this.barStaticItem3});
            this.RibbonControl.ItemsVertAlign = DevExpress.Utils.VertAlignment.Top;
            this.RibbonControl.LargeImages = this.imageList2;
            this.RibbonControl.Location = new System.Drawing.Point(548, 76);
            this.RibbonControl.Margin = new System.Windows.Forms.Padding(2);
            this.RibbonControl.MaxItemId = 261;
            this.RibbonControl.MdiMergeStyle = DevExpress.XtraBars.Ribbon.RibbonMdiMergeStyle.Always;
            this.RibbonControl.Name = "RibbonControl";
            this.RibbonControl.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.RibbonPageOperation});
            this.RibbonControl.ShowToolbarCustomizeItem = false;
            this.RibbonControl.Size = new System.Drawing.Size(248, 120);
            this.RibbonControl.Toolbar.ShowCustomizeItem = false;
            this.RibbonControl.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "Imprimir";
            this.barStaticItem1.Id = 258;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem2
            // 
            this.barStaticItem2.Caption = "Exportar como PDF";
            this.barStaticItem2.Id = 259;
            this.barStaticItem2.Name = "barStaticItem2";
            this.barStaticItem2.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem3
            // 
            this.barStaticItem3.Caption = "Exportar como HTML";
            this.barStaticItem3.Id = 260;
            this.barStaticItem3.Name = "barStaticItem3";
            this.barStaticItem3.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // PersonalFileForm
            // 
            this.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1270, 787);
            this.Controls.Add(this.RibbonControl);
            this.Controls.Add(this.layoutControlPersonalFile);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PersonalFileForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Expediente";
            this.Load += new System.EventHandler(this.PersonalFileForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.listBoxDepartaments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlPersonalFile)).EndInit();
            this.layoutControlPersonalFile.ResumeLayout(false);
            this.panelExOperatorPicture.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPhoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxAddress2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCourses)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCoursesRealized)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxFilterEvaluations.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlEva)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridViewAllEva)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxDetails.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlObs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewObs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupUserPersonalInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPhoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLastName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOpId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDateBirth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemRol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemUserPhoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroupUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupScheduler)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupUserInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupObs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupObsDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupCourse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupEval)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemShowFor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RibbonControl)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private PanelEx panelExOperatorPicture;
        private LabelEx labelLastName;
        private LabelEx labelName;
        private LabelEx labelRol;
        private LabelEx labelEmail;
        private LabelEx labelPhone;
        private LabelEx labelDateBirth;
        private LabelEx labelOperatorID;
        private System.Windows.Forms.PictureBox pictureBoxPhoto;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.ImageList imageList;
        private DevExpress.XtraBars.BarButtonItem barButtonItemExit;
        private DevExpress.XtraBars.BarButtonItem barButtonItemHelp;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupGeneralOptMonitor;
        internal DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupRoom;
        internal DevExpress.XtraReports.UserDesigner.XRDesignRibbonPageGroup XrDesignRibbonPageGroupViewActivities;
        internal DevExpress.XtraReports.UserDesigner.XRDesignRibbonPageGroup ribbonPageGroupPerformance;
        internal DevExpress.XtraReports.UserDesigner.XRDesignRibbonPage RibbonPageMonitoring;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupWindows;
        internal DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupAccessProfile;
        internal DevExpress.XtraReports.UserDesigner.XRDesignRibbonPageGroup ribbonPageGroupPreferences;
        internal DevExpress.XtraReports.UserDesigner.XRHtmlRibbonPage RibbonPageTools;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup ribbonPageGroupFinalReport;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup ribbonPageGroupTraining;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItemPersonalfile;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupAssignment;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ImageList imageList2;
        private GridControlEx gridControlCat;
        private GridViewEx gridViewCat;
        private GridControlEx gridControlEva;
        private GridViewEx gridViewAllEva;
        private GridControlEx gridControlObs;
        private GridViewEx gridViewObs;
        private GridControlEx gridControlCourses;
        private GridViewEx gridViewCoursesRealized;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private RadioButtonEx radioButtonCoursesRealized;
        private RadioButtonEx radioButtonCoursesNotRealized;
        internal DevExpress.XtraBars.Ribbon.RibbonControl RibbonControl;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPage RibbonPageOperation;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup ribbonPageGroupPersonalFile;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupOptions;
        private SchedulerBarControl schedulerBarControl;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemEvaluateOperator;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItemPersonalFileObs;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItemModifyCategory;
        private DevExpress.XtraEditors.MemoEdit textBoxAddress2;
        private DevExpress.XtraEditors.ListBoxControl listBoxDepartaments;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxFilterEvaluations;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemUpdate;
        private DevExpress.XtraEditors.MemoEdit textBoxDetails;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemSave;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemSend;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemPrint;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItem2;
        private DevExpress.XtraBars.BarStaticItem barStaticItem3;
        private System.Windows.Forms.ToolStripMenuItem printMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportPDFMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportHTMLMenuItem;
        private DevExpress.XtraLayout.LayoutControl layoutControlPersonalFile;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemPhoto;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemLastName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemOpId;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDateBirth;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemPhone;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemAddress;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemEmail;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemRol;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDepartments;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemCategory;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupUserPersonalInfo;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroupUser;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupUserInfo;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupObs;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupObsDetails;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupCourse;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupEval;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemShowFor;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupScheduler;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.LabelControl labelControlPhoto;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemUserPhoto;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
    }
}
