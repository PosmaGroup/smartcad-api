using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Smartmatic.SmartCad.Service;
using System.Collections;
using System.IO;
using System.Threading;

using DevExpress.XtraCharts;

using DevExpress.XtraBars;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.Utils;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Views.Base;
using System.Reflection;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraTab;
using DevExpress.XtraGrid;
using DevExpress.XtraEditors;
using DevExpress.XtraPrinting.Control;
using DevExpress.XtraPrinting;
using System.Diagnostics;
using DevExpress.XtraLayout.Utils;
using SmartCadControls.SyncBoxes;
using SmartCadCore.Core;
using SmartCadCore.ClientData;
using SmartCadControls.Controls;
using SmartCadControls;
using SmartCadCore.Common;
using SmartCadGuiCommon;
using SmartCadCore.Enums;
using SmartCadGuiCommon.SyncBoxes;
using SmartCadGuiCommon.Controls;
using SmartCadGuiCommon.Classes;

namespace SmartCadGuiCommon
{
    public partial class DefaultSupervisionForm : XtraForm
    {
        private readonly string NOT_CONNECTED = "NotConnected";
        private GridControlSynBox operatorSynBox;
        private const int SINGLE_SELECTION = 1;
        private const float FONT_SIZE = 9.75f;
        private SkinEngine skinEngine;
        public string supervisedApplicationName;
        private bool isGeneralSupervisor;
        private bool isFirstLevelSupervisor;
        private bool isDispatchSupervisor;
        private DepartmentTypeClientData selectedDepartmentType;
        Dictionary<string, BindingList<OperatorStatusChartData>> statusData = new Dictionary<string, BindingList<OperatorStatusChartData>>();
        /// <summary>
        /// This field serves to avoid reload data while the form is loading first time.
        /// </summary>
        private bool canReload;
        private IList globalIndicatorClassDashboards;
        private IList globalIndicators;
        private Dictionary<int, RoomClientData> globalRooms = new Dictionary<int,RoomClientData>();
        private int operatorClientCode;
        #region Synchronization objects
        
        private object syncCommited = new object();

        #endregion

        private OperatorClientData selectedOperator;

        public OperatorClientData SelectedOperator
        {
            get
            {
                selectedOperator = null;
                if (gridControlExOperators.SelectedItems.Count > 0)
                {
                    GridControlData gridData = gridControlExOperators.SelectedItems[0] as GridControlData;
                    selectedOperator = gridData.Tag as OperatorClientData;
                }
                return selectedOperator;
            }
        }

        public IList GlobalIndicatorClassDashboards
        {
            get
            {
                return globalIndicatorClassDashboards;
            }
            set
            {
                globalIndicatorClassDashboards = value;
            }
        }

        public IList GlobalIndicators
        {
            get
            {
                return globalIndicators;
            }
            set
            {
                globalIndicators = value;
            }
        }

        public string SupervisedApplicationName
        {
            get
            {
                return supervisedApplicationName;
            }
            set
            {
                supervisedApplicationName = value;
                GridControlDataOperator.SupervisedApplicationName = this.supervisedApplicationName;
                FormUtil.InvokeRequired(this,
                    delegate
                    {
                        bool isFirstLevel = UserApplicationClientData.FirstLevel.Name == SupervisedApplicationName;
                        indicatorDashBoardControlSystem.IsFirstLevel = isFirstLevel;
                        indicatorDashBoardControlDepartment.IsFirstLevel = isFirstLevel;
                        indicatorDashBoardControlGroup.IsFirstLevel = isFirstLevel;
                        indicatorDashBoardControlOperator.IsFirstLevel = isFirstLevel;     
                    });
            }
        }

        public DepartmentTypeClientData SelectedDepartmentType
        {
            get { return selectedDepartmentType; }
            set
            {
                DepartmentTypeClientData oldDept = null;
                if (selectedDepartmentType != null)
                {
                    oldDept = selectedDepartmentType.Clone() as DepartmentTypeClientData;
                }
                    selectedDepartmentType = value;
                    if (selectedDepartmentType!=null)
                    {
                        this.layoutControlGroupDepartment.Text = selectedDepartmentType.Name;
                        if (this.tabbedControlGroupDashboard.TabPages.IndexOf(this.layoutControlGroupDepartment) == -1)
                        {
                            tabbedControlGroupDashboard.BeginUpdate();
                            this.tabbedControlGroupDashboard.InsertTabPage(1, this.layoutControlGroupDepartment);
                            tabbedControlGroupDashboard.EndUpdate();
                        }
                    }
                    else
                    {
                        if (tabbedControlGroupDashboard.TabPages.Contains(layoutControlGroupDepartment))
                        {
                            tabbedControlGroupDashboard.BeginUpdate();
                            tabbedControlGroupDashboard.RemoveTabPage(layoutControlGroupDepartment);
                            tabbedControlGroupDashboard.EndUpdate();
                        }
                    }
                    if (selectedDepartmentType != oldDept || selectedDepartmentType == null)
                        ReloadData();
            }
        }

        public bool CheckLogIn
        {
            get
            {
                return Convert.ToBoolean((this.MdiParent as SupervisionForm).globalApplicationPreferences["CheckLogInMode"].Value);
            }
        }

		public DefaultSupervisionForm(SkinEngine skin, string applicationName, DepartmentTypeClientData deparment, 
            bool firstLevelSupervisor, bool dispatchSupervisor, bool isGeneralSupervisor)
        {
         
            InitializeComponent();
            LoadLanguage();

            isFirstLevelSupervisor = firstLevelSupervisor;
            isDispatchSupervisor = dispatchSupervisor;
            repositoryItemComboBoxSelectRoom.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            repositoryItemComboBoxSelectRoom.ReadOnly = false;
            repositoryItemComboBoxSelectRoom.ShowDropDown = DevExpress.XtraEditors.Controls.ShowDropDown.SingleClick;
            barEditItemSelectRoom.Enabled = false;
            barCheckItemViewAll.Checked = true;
            layoutControl1.AllowCustomizationMenu = false;
                   
            canReload = false;
            this.skinEngine = skin;
            operatorSynBox = new GridControlSynBox(gridControlExOperators);
            gridControlExOperators.EnableAutoFilter = true;
            gridControlExOperators.Type = typeof(GridControlDataOperator);
            gridControlExOperators.ViewTotalRows = true;
            gridControlExOperators.CellToolTipNeeded += new DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventHandler(gridControlExOperators_CellToolTipNeeded);
            
            GridControlDataOperator.SupervisedApplicationName = this.SupervisedApplicationName;
            this.SupervisedApplicationName = applicationName;            
            if (deparment != null)
            {
                this.SelectedDepartmentType = deparment;
            }
            else
            {
                this.SelectedDepartmentType = null;
            }
            this.isGeneralSupervisor = isGeneralSupervisor;

            if (isGeneralSupervisor)
            {
                gridControlExOperators.SetColumnVisible("Supervisor", true);
                this.tabbedControlGroupDashboard.RemoveTabPage(this.layoutControlGroupMyGroup);
            }
            else
            {
                gridControlExOperators.AutoAdjustColumnWidth();
            }
           
            //Load Rooms
            globalRooms = GetRooms();

            //Fill rooms comboBox
            FillRoomsRepository();

            if (repositoryItemComboBoxSelectRoom.Items.Count > 0)
            {
                RoomClientData selectedRoom = (RoomClientData)repositoryItemComboBoxSelectRoom.Items[0];
                barEditItemSelectRoom.EditValue = selectedRoom;
                barEditItemSelectRoom.EditValueChanged += barEditItemSelectRoom_EditValueChanged;
                barEditItemSelectRoom.ItemClick += new ItemClickEventHandler(barEditItemSelectRoom_ItemClick);
                RenderRoom(selectedRoom);
            }
            this.Activated += new EventHandler(DefaultSupervisionForm_Activated);
     
            ShowPrimaryIndicators(true);
     
           
        }

        void DefaultSupervisionForm_Activated(object sender, EventArgs e)
        {
            if (this.MdiParent != null)
                (this.MdiParent as SupervisionForm).RibbonControl.SelectedPage = (this.MdiParent as SupervisionForm).RibbonPageMonitoring;
    
        }

        void gridControlExOperators_CellToolTipNeeded(object sender, DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventArgs e)
        {
            GridControlEx selectedGrid = e.SelectedControl as GridControlEx;
            if (selectedGrid != null)
            {
                try
                {
                    GridView view = selectedGrid.GetViewAt(e.ControlMousePosition) as GridView;
                    if (view != null)
                    {
                        GridHitInfo hi = view.CalcHitInfo(e.ControlMousePosition);
                        if (hi.InRow && hi.Column != null && view.GetRow(hi.RowHandle) != null)
                        {
                            OperatorClientData operatorClient =
                                ((GridControlDataOperator)view.GetRow(hi.RowHandle)).OperatorClient;
                            SessionHistoryClientData lastSession = GetLastSessionHistory(operatorClient.LastSessions, supervisedApplicationName);
                            string text = view.GetRowCellDisplayText(hi.RowHandle, hi.Column);
                            int textWidth = TextRenderer.MeasureText(text, ((GridControl)view.GridControl).Font).Width;

                            if (e.Info == null)
                            {
                                e.Info = new DevExpress.Utils.ToolTipControlInfo(
                                    new CellToolTipInfo(hi.RowHandle, hi.Column, "cell"), text);
                            }

                            if (operatorClient != null && lastSession != null && lastSession.IsLoggedIn == true)
                            {
                                if (textWidth < hi.Column.Width)
                                {
                                    e.Info.Text = ApplicationUtil.GetFormattedElapsedTime(ResourceLoader.GetString2("ConnectedTime"),
                                        ServerServiceClient.GetInstance().GetTime(), lastSession.StartDateLogin.Value);
                                }
                            }
                            else 
							{
                                if (textWidth < hi.Column.Width)
                                    e.Info.Text = ResourceLoader.GetString2("ConnectedTime","00","00","00");                            
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    SmartLogger.Print(ex);
                }
            }            
        }

        void gridControlExOperators_SelectionChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
			if (gridControlExOperators.SelectedItems.Count > 0)
			{
				GridControlDataOperator gridData = gridControlExOperators.SelectedItems[0] as GridControlDataOperator;

                IList indicators = new ArrayList();
                foreach (IndicatorClassDashboardClientData icdcd in GlobalIndicatorClassDashboards)
                {
                    //YT: Agregue la segunda condicion del if, evaluar comportamiento.
                    if (icdcd.ClassName.Equals(IndicatorClassClientData.Operator.Name) && icdcd.ShowDashboard == true)
                    {
                        indicators.Add(ServerServiceClient.GetInstance().SearchClientObjects(
                            SmartCadHqls.GetCustomHql(
                                SmartCadHqls.GetIndicatorByName,
                                icdcd.IndicatorName))[0]);
                    }
                }
                IList[] dataSource = new IList[3];
                LoadIndicatorsForAGivenClass(indicators, IndicatorClassClientData.Operator);
                
                //When this option is tested, will activated monitoring.
                if (ApplicationUtil.GetOperatorStatus(gridData.OperatorClient, supervisedApplicationName).StatusName != NOT_CONNECTED)
					barButtonItemRemoteControl.Enabled = true;
			}
        }

        void gridControlExOperators_CalcRowHeight(object sender, RowHeightEventArgs e)
        {
            GridViewEx view = this.gridControlExOperators.MainView as GridViewEx;
            GridControlDataOperator gridData = view.GetRow(e.RowHandle) as GridControlDataOperator;
            bool selected = gridControlExOperators.SelectedItems.Contains(gridData);
            if (gridData != null)
            {
                int height = gridData.Photo.Height;
                if (selected == true)
                    e.RowHeight = height;
                else
                    e.RowHeight = -1;
            }
        }

        void gridControlExOperators_SelectionWillChange(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            this.indicatorGridControl1.Clear();
            FormUtil.InvokeRequired(chartControl1,
                delegate
                {
                    chartControl1.Series.BeginUpdate();
                    foreach (Series series in chartControl1.Series)
                    {
                        series.Points.Clear();
                    }
                    chartControl1.Series.EndUpdate();
                    layoutControlItemChartControl.Visibility = LayoutVisibility.Never;
                });
            
            //No sabemos por que se hizo esto, se comento temporalmente para ver el comportamiento de la aplicacion
            //GridControlDataOperator gridDataPrevious = null;
            //if (e != null)
            //{
            //    gridDataPrevious = (gridControlExOperators.MainView as GridView).GetRow(e.PrevFocusedRowHandle) as GridControlDataOperator;
            //}
            //if (gridDataPrevious != null)
            //{
            //    gridControlExOperators.AddOrUpdateItem(gridDataPrevious);
            //}

            FormUtil.InvokeRequired(gridControlExOperators,
                delegate
                {
                    gridView1.LayoutChanged();
                });

            if (gridControlExOperators.SelectedItems.Count > 0)
            {
                if (tabbedControlGroupMonitorActivities.SelectedTabPage == layoutControlGroupRoom)
                {
                    ShowOperatorsInRoom(true);
                } 

                GridControlDataOperator gridData = gridControlExOperators.SelectedItems[0] as GridControlDataOperator;

                if (ApplicationUtil.GetOperatorStatus(gridData.OperatorClient, supervisedApplicationName).StatusName == NOT_CONNECTED)
                    barButtonItemRemoteControl.Enabled = false;
                
                ((SupervisionForm)this.MdiParent).printPreviewBarItemPersonalfile.Enabled = true;


                //No sabemos por que se hizo esto, se comento temporalmente para ver el comportamiento de la aplicacion
                //gridData.PercentageSchedule = GetPreviousPercentageChronogram(gridData);
                //operatorSynBox.Sync(gridData, CommittedDataAction.Update);
                OperatorClientData selectedOper = gridData.OperatorClient;
				
                if (((GanttDiagram)chartControlOperatorStatuses.Diagram).AxisX.ConstantLines.Count > 0 &&
                    FindOperatorInChart(selectedOper) == true)
                {
                    ((GanttDiagram)chartControlOperatorStatuses.Diagram).AxisX.ConstantLines[0].AxisValue = selectedOper.FirstName + " " + selectedOper.LastName;
                    ((GanttDiagram)chartControlOperatorStatuses.Diagram).AxisX.ConstantLines[0].Visible = true;
                }    
            }
            else
            {
                barButtonItemRemoteControl.Enabled = false;
				((SupervisionForm)this.MdiParent).printPreviewBarItemPersonalfile.Enabled = false;
                if (((GanttDiagram)chartControlOperatorStatuses.Diagram).AxisX.ConstantLines.Count > 0)
                {
                    ((GanttDiagram)chartControlOperatorStatuses.Diagram).AxisX.ConstantLines[0].Visible = false;
                }
            }
        }

        private void SelectControl(OperatorClientData operatorClientData)
        {
            if (operatorClientData != null && operatorControl.ContainsKey(operatorClientData))
            {
                Control c = operatorControl[operatorClientData];
                control_MouseDown(c, null);
                control_MouseUp(c, null);
            }
        }

        private void LoadLanguage()
        {
            this.Text = ResourceLoader.GetString2("DefaultSupervisionFormText");
            this.layoutControlGroupOperators.Text = ResourceLoader.GetString("Operators");
            this.layoutControlGroupPerformance.Text = ResourceLoader.GetString("Performance");
            this.labelExShowOperators.Text = ResourceLoader.GetString("View")+":";
            this.radioButtonAllOperators.Text = ResourceLoader.GetString("All");
            this.radioButtonConOperators.Text = ResourceLoader.GetString("Connected");
            this.radioButtonNoConOperators.Text = ResourceLoader.GetString("MSG_NotConnected");
            this.barButtonItemOperatorPerformance.Caption = ResourceLoader.GetString2("ViewPerformance");
            this.layoutControlGroupOperatorPerformance.Text = ResourceLoader.GetString2("OperatorPerformance");
            this.layoutControlGroupOperatorActivities.Text = ResourceLoader.GetString2("OperatorActivities");
            this.barSubItemOperatorChartControl.Caption = ResourceLoader.GetString("ConfGraphic");
            this.barCheckItemDownTreshold.Caption = ResourceLoader.GetString2("DownTreshold");
            this.barCheckItemUpTreshold.Caption = ResourceLoader.GetString2("UpTreshold");
            this.barCheckItemTrend.Caption = ResourceLoader.GetString("Trend");
            //this.chartControlOperatorStatuses.Series["Tendencia"].LegendText = ResourceLoader.GetString("TrendAux");
            this.barButtonItemPrint.Caption = ResourceLoader.GetString2("Print");
            this.barButtonItemSave.Caption = ResourceLoader.GetString2("Save");
            this.BarButtonItemSendMonitor.Caption = ResourceLoader.GetString2("Send");
            this.barButtonItemRefreshGraphic.Caption = ResourceLoader.GetString2("Update");
			this.barButtonItemChat.Caption = ResourceLoader.GetString2("Chat");
            this.layoutControlGroupRoom.Text = ResourceLoader.GetString2("LocateInRoom");
            this.layoutControlGroupSystem.Text = ResourceLoader.GetString2("System");
            this.layoutControlGroupMyGroup.Text = ResourceLoader.GetString2("Group");
            this.layoutControlGroupDepartment.Text = "<" + ResourceLoader.GetString2("Department") + ">";
            this.barButtonItemRemoteControl.Caption = ResourceLoader.GetString2("Monitoring");

            this.indicatorDashBoardControlDepartment.TitleFirstGroup = ResourceLoader.GetString2("System");
            this.indicatorDashBoardControlDepartment.TitleSecondGroup = ResourceLoader.GetString2("FirstLevel");
            this.indicatorDashBoardControlDepartment.TitleThirdGroup = ResourceLoader.GetString2("Dispatch");

            this.indicatorDashBoardControlSystem.TitleFirstGroup = ResourceLoader.GetString2("Calls");
            this.indicatorDashBoardControlSystem.TitleSecondGroup = ResourceLoader.GetString2("FirstLevel");
            this.indicatorDashBoardControlSystem.TitleThirdGroup = ResourceLoader.GetString2("Dispatch");

            this.indicatorDashBoardControlGroup.TitleFirstGroup = ResourceLoader.GetString2("Calls");
            this.indicatorDashBoardControlGroup.TitleSecondGroup = ResourceLoader.GetString2("FirstLevel");
            this.indicatorDashBoardControlGroup.TitleThirdGroup = ResourceLoader.GetString2("Dispatch");

            this.indicatorDashBoardControlOperator.TitleFirstGroup = ResourceLoader.GetString2("Calls");
            this.indicatorDashBoardControlOperator.TitleSecondGroup = ResourceLoader.GetString2("FirstLevel");
            this.indicatorDashBoardControlOperator.TitleThirdGroup = ResourceLoader.GetString2("Dispatch");

            #region Monitoring
            RibbonPageMonitoring.Text = ResourceLoader.GetString("Monitoring");
            ribbonPageGroupOperator.Text = ResourceLoader.GetString2("OperatorsActivities");
            barButtonItemOperatorActivities.Caption = ResourceLoader.GetString2("SeeActivities");
            BarButtonItemSelectRoom.Caption = ResourceLoader.GetString2("ViewRoom");
            barEditItemSelectRoom.Caption = ResourceLoader.GetString2("$Message.Nombre") + ":";
            barCheckItemViewSelected.Caption = ResourceLoader.GetString("ViewSelected");
            barCheckItemViewAll.Caption = ResourceLoader.GetString("ViewAll");
            ribbonPageGroupGeneralOptMonitor.Text = ResourceLoader.GetString2("GeneralOptions");
            (barEditItemIndicatorSelection.Edit as DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup).Items[0].Description =
                ResourceLoader.GetString2("PrimaryIndicators");
            (barEditItemIndicatorSelection.Edit as DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup).Items[1].Description =
                ResourceLoader.GetString2("SecundaryIndicators");
            #endregion
            #region Rooms
            this.layoutControlGroupRoomInfo.Text = ResourceLoader.GetString2("RoomData");
            this.layoutControlGroupLegend.Text = ResourceLoader.GetString2("RoomLeyend");
            this.layoutControlItemRoomName.Text = ResourceLoader.GetString2("RoomName") + ":";
            this.layoutControlItemRoomDescription.Text = ResourceLoader.GetString2("RoomDescription") + ":";
            labelControlAvailable.Text = ResourceLoader.GetString2("Available");
            labelControlAbsent.Text = ResourceLoader.GetString2("Absent");
            labelControlRest.Text = ResourceLoader.GetString2("Rest");
            labelControlReunion.Text = ResourceLoader.GetString2("OnReunion");
            labelControlBathRoom.Text = ResourceLoader.GetString2("BathRoom");
            labelControlBusy.Text = ResourceLoader.GetString2("Busy");
            #endregion
            #region Ribbon bar ToolTips
            this.barButtonItemOperatorActivities.SuperTip = SupervisionForm.BuildToolTip("ToolTip_ViewOperatorActivities");
            this.barButtonItemOperatorPerformance.SuperTip = SupervisionForm.BuildToolTip("ToolTip_ViewOperatorPerformance");
            this.BarButtonItemSelectRoom.SuperTip = SupervisionForm.BuildToolTip("ToolTip_ViewRoom");
            this.barCheckItemViewSelected.SuperTip = SupervisionForm.BuildToolTip("ToolTip_ViewSelectedOperator");
            this.barCheckItemViewAll.SuperTip = SupervisionForm.BuildToolTip("ToolTip_AllOperators");
            this.barEditItemSelectRoom.SuperTip = SupervisionForm.BuildToolTip("ToolTip_RoomName");
            this.barButtonItemPrint.SuperTip = SupervisionForm.BuildToolTip("ToolTip_Print");
            this.barButtonItemSave.SuperTip = SupervisionForm.BuildToolTip("ToolTip_Save");
            this.BarButtonItemSendMonitor.SuperTip = SupervisionForm.BuildToolTip("ToolTip_Send");
            this.barButtonItemRefreshGraphic.SuperTip = SupervisionForm.BuildToolTip("ToolTip_Update");
            
            this.barButtonItemPrint.SuperTip = SupervisionForm.BuildToolTip("ToolTip_Print");
            this.barButtonItemSave.SuperTip = SupervisionForm.BuildToolTip("ToolTip_Save");
            #endregion

            pictureBox1.Image = ResourceLoader.GetImage("BallLightImage_3_Ready");
            pictureBox2.Image = ResourceLoader.GetImage("BallLightImage_3_Busy");
            pictureBox3.Image = ResourceLoader.GetImage("BallLightImage_3_Bathroom");
            pictureBox4.Image = ResourceLoader.GetImage("BallLightImage_3_Rest");
            pictureBox5.Image = ResourceLoader.GetImage("BallLightImage_3_Reunion");
            pictureBox6.Image = ResourceLoader.GetImage("BallLightImage_3_Absent");

            this.barButtonItemReConnect.Caption = ResourceLoader.GetString2("RemoteControlReconnect");
            this.barCheckItemScaleView.Caption = ResourceLoader.GetString2("RemoteControlScaleView");
            this.barCheckItemTakeControl.Caption = ResourceLoader.GetString2("RmoteControlTakeControl");

            this.indicatorDashBoardControlSystem.TitleFirstGroup = ResourceLoader.GetString2("Calls");
            this.indicatorDashBoardControlSystem.TitleSecondGroup = ResourceLoader.GetString2("FirstLevel");
            this.indicatorDashBoardControlSystem.TitleThirdGroup = ResourceLoader.GetString2("Dispatch");

            this.indicatorDashBoardControlGroup.TitleFirstGroup = ResourceLoader.GetString2("Calls");
            this.indicatorDashBoardControlGroup.TitleSecondGroup = ResourceLoader.GetString2("FirstLevel");
            this.indicatorDashBoardControlGroup.TitleThirdGroup = ResourceLoader.GetString2("Dispatch");

            this.indicatorDashBoardControlOperator.TitleFirstGroup = ResourceLoader.GetString2("Calls");
            this.indicatorDashBoardControlOperator.TitleSecondGroup = ResourceLoader.GetString2("FirstLevel");
            this.indicatorDashBoardControlOperator.TitleThirdGroup = ResourceLoader.GetString2("Dispatch");
        }

        private void DefaultSupervisionForm_Load(object sender, EventArgs e)
        {
           
            operatorClientCode = ServerServiceClient.GetInstance().OperatorClient.Code;
            this.indicatorGridControl1.Size = new Size(464, 340);
            SetSkin();

            this.indicatorGridControl1.gridControl1.Size = new Size(460, 335);
            this.indicatorGridControl1.gridControl1.Load += new EventHandler(gridControl1_Load);           
            this.indicatorGridControl1.FocusedRowChangedEvent += new IndicatorGridFocusedRowChangedEventHandler(indicatorGridControl1_FocusedRowChangedEvent);
            (gridControlExOperators.MainView as GridViewEx).SelectionWillChange += new FocusedRowChangedEventHandler(gridControlExOperators_SelectionWillChange);
            (gridControlExOperators.MainView as GridViewEx).SingleSelectionChanged += new FocusedRowChangedEventHandler(gridControlExOperators_SelectionChanged);
            (gridControlExOperators.MainView as GridView).CalcRowHeight += new RowHeightEventHandler(gridControlExOperators_CalcRowHeight);
            (this.MdiParent as SupervisionForm).SupervisionCommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(DefaultSupervisionForm_SupervisionCommittedChanges);
            repositoryItemComboBoxSelectRoom.SelectedIndexChanged += new EventHandler(repositoryItemComboBoxSelectRoom_SelectedIndexChanged);

            LoadData();
                    
            //Start the thread that will update the chart control.
            ThreadPool.QueueUserWorkItem(new WaitCallback(LoadSessionStatusHistoryChart), null);
            ConfigureDashBoardControls();
            this.radioButtonAllOperators.Checked = true;

            ThreadPool.QueueUserWorkItem(delegate(object state)
            {
                try
                {
                    LoadIndicatorResults();
                }
                catch (Exception ex)
                {
                    SmartLogger.Print(ex);
                }
            });
                      

            IList status =
                ServerServiceClient.GetInstance().SearchClientObjects(typeof(OperatorStatusClientData));

            for (int i = 0; i < status.Count; i++)
            {
				UpdateChartLegend((status[i] as OperatorStatusClientData));
				UpdateStatusLabel((status[i] as OperatorStatusClientData));
            }

            this.tabbedControlGroupMonitorActivities.SelectedTabPage = layoutControlGroupOperatorActivities;
   
           
        }

		private void UpdateChartLegend(OperatorStatusClientData operatorStatusClientData)
		{
			for (int j = 0; j < this.chartControlOperatorStatuses.Series.Count; j++)
			{
				if (this.chartControlOperatorStatuses.Series[j].Name ==
					operatorStatusClientData.Name)
				{
					this.chartControlOperatorStatuses.Series[j].LegendText =
						operatorStatusClientData.FriendlyName;
				}
				else if (this.chartControlOperatorStatuses.Series[j].Name == NOT_CONNECTED)
				{
					this.chartControlOperatorStatuses.Series[j].LegendText = ResourceLoader.GetString2("MSG_NotConnected");
				}
			}
		}

		private void UpdateStatusLabel(OperatorStatusClientData operatorStatusClientData)
		{
			if (operatorStatusClientData.Name.Equals(OperatorStatusClientData.Absent.Name))
				labelControlAbsent.Text = operatorStatusClientData.FriendlyName;
			else if (operatorStatusClientData.Name.Equals(OperatorStatusClientData.Bathroom.Name))
				labelControlBathRoom.Text = operatorStatusClientData.FriendlyName;
			else if (operatorStatusClientData.Name.Equals(OperatorStatusClientData.Busy.Name))
				labelControlBusy.Text = operatorStatusClientData.FriendlyName;
			else if (operatorStatusClientData.Name.Equals(OperatorStatusClientData.Ready.Name))
				labelControlAvailable.Text = operatorStatusClientData.FriendlyName;
			else if (operatorStatusClientData.Name.Equals(OperatorStatusClientData.Rest.Name))
				labelControlRest.Text = operatorStatusClientData.FriendlyName;
			else if (operatorStatusClientData.Name.Equals(OperatorStatusClientData.Reunion.Name))
				labelControlReunion.Text = operatorStatusClientData.FriendlyName;
		}



		public IList GetOperatorsOnGrid()
		{
			IList operators = null;
			FormUtil.InvokeRequired(gridControlExOperators,
				delegate
				{
					operators = new ArrayList(gridControlExOperators.Items);
				});
			return operators;
		}

        private bool FindOperatorInChart(OperatorClientData operatorClientData)
        {
            bool result = false;
            Dictionary<string, BindingList<OperatorStatusChartData>> aux = new Dictionary<string, BindingList<OperatorStatusChartData>>(statusData);
            foreach (KeyValuePair<string, BindingList<OperatorStatusChartData>> statusItem in aux)
            {
                for (int index = 0; index < statusItem.Value.Count && result == false; index++)
                {
                    OperatorStatusChartData oscd = statusItem.Value[index];
                    if (oscd.OperatorCode == operatorClientData.Code)
                        result = true;
                }
                if (result == true)
                    break;
            }
            return result;
        }

		IList<int> operatorsCodeToDelete = new BindingList<int>();

        private void gridControl1_Load(object sender, EventArgs e)
        {           
            this.chartControl1.Series[1].Visible = false;
            this.chartControl1.Series[2].Visible = false;
            this.chartControl1.Series[3].Visible = false;
            this.chartControl1.Legend.Visible = false;           
        }

        #region operatorChartControl
        int indicatorCodeSelected = -1;
        IList dataIndicatorCode = null;
        private bool upTresholdSerieVisible = false;
        private bool downTresholdSerieVisible = false;
        private bool trendSerieVisible = false;
        private XYDiagram Diagram
        {
            get
            {
                return this.chartControl1.Diagram as XYDiagram;
            }
        }
        private void indicatorGridControl1_FocusedRowChangedEvent(object sender,
            IndicatorGridFocusedRowChangedEventArgs e)
        {
            FormUtil.InvokeRequired(this.chartControl1, delegate
            {
                this.chartControl1.Visible = true;
                this.chartControl1.Legend.Visible = true;
                Diagram.EnableAxisXScrolling = true;
                Diagram.EnableAxisYScrolling = true;
                Diagram.EnableAxisXZooming = true;
                Diagram.EnableAxisYZooming = true;
                this.chartControl1.Series[0].Points.Clear();
                this.chartControl1.Series[2].Points.Clear();
                this.chartControl1.Series[3].Points.Clear();
                //this.chartControl1.Series[0].Points.Add(sp);


                if (e.List.Count > 0)
                {
                    this.chartControl1.Series.BeginUpdate();
                    layoutControlItemChartControl.Visibility = LayoutVisibility.Always;
                    this.chartControl1.Visible = true;
                    this.chartControl1.Series[0].LegendText = (e.List[0] as IndicatorResultClientData).IndicatorName;

                    indicatorCodeSelected = (e.List[0] as IndicatorResultClientData).IndicatorCode;
                    foreach (IndicatorResultClientData ind in e.List)
                    {
                        foreach (IndicatorResultValuesClientData data in ind.Values)
                        {
                            if (((this.gridControlExOperators.SelectedItems[0] as
                                 GridControlDataOperator).Tag as OperatorClientData).Code == data.CustomCode)
                            {
                                SeriesPoint sp = null;
                                if (data.MeasureUnit == "%")
                                {
                                    sp = new SeriesPoint(data.Date, new double[] { 
                                            (Convert.ToDouble(data.ResultValue)*100) });
                                }
                                else
                                {
                                    sp = new SeriesPoint(data.Date, new double[] { 
                                            Convert.ToDouble(data.ResultValue)});
                                }

                                FormUtil.InvokeRequired(this.chartControl1, delegate
                                {
                                    this.chartControl1.Series[0].Points.Add(sp);
                                });

                                if (upTresholdSerieVisible == true)
                                {
                                    ShowExtraResultSeries(ExtraSerieTypes.UpTreshold, true, data.Date);
                                }
                                if (downTresholdSerieVisible == true)
                                {
                                    ShowExtraResultSeries(ExtraSerieTypes.DownTreshold, true, data.Date);
                                }
                                break;
                            }
                        }
                    }
                    this.chartControl1.Series.EndUpdate();
                }
                else
                {
                    layoutControlItemChartControl.Visibility = LayoutVisibility.Never;
                    this.chartControl1.Visible = false;
                    this.chartControl1.Series[0].LegendText = string.Empty;
                }


                dataIndicatorCode = chartControl1.Series[0].Points;
                if (trendSerieVisible == true)
                {
                    this.chartControl1.Series[1].Points.Clear();
                    ShowExtraResultSeries(ExtraSerieTypes.Trend, true, DateTime.MinValue);
                }
                //if (upTresholdSerieVisible == true)
                //{
                //    this.chartControl1.Series[2].Points.Clear();
                //    ShowExtraResultSeries(ExtraSerieTypes.UpTreshold, true);
                //}
                //if (downTresholdSerieVisible == true)
                //{
                //    this.chartControl1.Series[3].Points.Clear();
                //    ShowExtraResultSeries(ExtraSerieTypes.DownTreshold, true);
                //}
            });
        }
        private void barCheckItemUpTreshold_CheckedChanged(object sender, ItemClickEventArgs e)
        {                     
            ShowExtraResultSeries(ExtraSerieTypes.UpTreshold,
                (sender as BarCheckItem).Checked, DateTime.MinValue);
        }
        private void barCheckItemDownTreshold_CheckedChanged(object sender, ItemClickEventArgs e)
        {                      
            ShowExtraResultSeries(ExtraSerieTypes.DownTreshold,
                (sender as BarCheckItem).Checked, DateTime.MinValue);
        }
        private void barCheckItemTrend_CheckedChanged(object sender, ItemClickEventArgs e)
        {           
            ShowExtraResultSeries(ExtraSerieTypes.Trend,
                  (sender as BarCheckItem).Checked, DateTime.MinValue);
        }
        public void ShowExtraResultSeries(ExtraSerieTypes extraSerieTypes, bool visible, DateTime OneHourLater)
        {

            if ((indicatorCodeSelected >= 0) && (OneHourLater != DateTime.MinValue))
            {
                DateTime now = ServerServiceClient.GetInstance().GetTime();

                IList indicators = ServerServiceClient.GetInstance().SearchClientObjects(
                             SmartCadHqls.GetCustomHql(SmartCadHqls.GetIndicatorByForeCastTimeAndByIndicatorCode,
                            now.ToString(ApplicationUtil.DataBaseFormattedDate),
                            OneHourLater.ToString(ApplicationUtil.DataBaseFormattedDate),
                            indicatorCodeSelected));

                if ((indicators.Count > 0) && (extraSerieTypes != ExtraSerieTypes.Trend))
                {
                    IndicatorGroupForecastClientData generalForecast = null;
                    IndicatorGroupForecastClientData currentSpecificForecast = null;
                    ArrayList specificsForecast = new ArrayList();

                    this.chartControl1.Series.BeginUpdate();

                    //Ocultar series que no se dean ver
                    if ((extraSerieTypes == ExtraSerieTypes.UpTreshold) && (visible == false))
                    {
                        this.chartControl1.Series[2].Points.Clear();
                    }
                    else if ((extraSerieTypes == ExtraSerieTypes.DownTreshold) && (visible == false))
                    {
                        this.chartControl1.Series[3].Points.Clear();

                    }
                    else if ((extraSerieTypes == ExtraSerieTypes.Trend) && (visible == false))
                    {
                        this.chartControl1.Series[1].Points.Clear();
                    }

                    //Separar grupos generales de especficos
                    foreach (IndicatorGroupForecastClientData indicatorGroupForecastClientData in indicators)
                    {
                        if (indicatorGroupForecastClientData.General == false)
                        {
                            specificsForecast.Add(indicatorGroupForecastClientData);
                        }
                        else
                        {
                            generalForecast = indicatorGroupForecastClientData;
                        }
                    }

                    //Colocar los ltimos 60 puntos en la grfica
                    //for (int i = 0; i < 60; i++)
                    {
                        //Si es general o no hay especfico
                        if ((specificsForecast.Count == 0) ||
                           (OneHourLater < (specificsForecast[0] as IndicatorGroupForecastClientData).Start))
                        {
                            if (extraSerieTypes == ExtraSerieTypes.UpTreshold)
                            {
                                SeriesPoint sp = new SeriesPoint(OneHourLater, new double[] { Convert.ToDouble(
                                    (generalForecast.ForecastValues[0] as IndicatorForecastClientData).High) });
                                FormUtil.InvokeRequired(this.chartControl1, delegate
                                {
                                    this.chartControl1.Series[2].Points.Add(sp);
                                });
                                this.chartControl1.Series[2].Visible = visible;

                            }
                            else if (extraSerieTypes == ExtraSerieTypes.DownTreshold)
                            {
                                SeriesPoint sp = new SeriesPoint(OneHourLater, new double[] { Convert.ToDouble(
                                    (generalForecast.ForecastValues[0] as IndicatorForecastClientData).Low) });
                                FormUtil.InvokeRequired(this.chartControl1, delegate
                                {
                                    this.chartControl1.Series[3].Points.Add(sp);
                                });
                                this.chartControl1.Series[3].Visible = visible;

                            }
                        }
                        //si es especfico
                        else if (OneHourLater < (specificsForecast[0] as IndicatorGroupForecastClientData).End)
                        {
                            currentSpecificForecast = (specificsForecast[0] as
                                IndicatorGroupForecastClientData);

                            if (extraSerieTypes == ExtraSerieTypes.UpTreshold)
                            {
                                SeriesPoint sp = new SeriesPoint(OneHourLater, new double[] { Convert.ToDouble(
                                    (currentSpecificForecast.ForecastValues[0] as 
                                    IndicatorForecastClientData).High) });
                                FormUtil.InvokeRequired(this.chartControl1, delegate
                                {
                                    this.chartControl1.Series[2].Points.Add(sp);
                                });
                                this.chartControl1.Series[2].Visible = visible;

                            }
                            else if (extraSerieTypes == ExtraSerieTypes.DownTreshold)
                            {
                                SeriesPoint sp = new SeriesPoint(OneHourLater, new double[] { Convert.ToDouble(
                                    (currentSpecificForecast.ForecastValues[0] as 
                                    IndicatorForecastClientData).Low) });
                                FormUtil.InvokeRequired(this.chartControl1, delegate
                                {
                                    this.chartControl1.Series[3].Points.Add(sp);
                                });
                                this.chartControl1.Series[3].Visible = visible;

                            }
                        }


                        //Si termino el perodo de un especfico
                        else if (OneHourLater > (specificsForecast[0] as IndicatorGroupForecastClientData).End)
                        {
                            specificsForecast.RemoveAt(0);
                            //i -= 1;
                        }

                        //OneHourLater = OneHourLater.AddMinutes(5);
                    }
                    trendSerieVisible = this.chartControl1.Series[1].Visible;
                    upTresholdSerieVisible = this.chartControl1.Series[2].Visible;
                    downTresholdSerieVisible = this.chartControl1.Series[3].Visible;

                    this.chartControl1.Series.EndUpdate();


                }
                
            }
         
            else if (OneHourLater == DateTime.MinValue)
            {
                
                if (extraSerieTypes == ExtraSerieTypes.UpTreshold)
                {
                    this.chartControl1.Series[2].Visible = visible;
                    upTresholdSerieVisible = visible;
                }
                if (extraSerieTypes == ExtraSerieTypes.DownTreshold)
                {
                    this.chartControl1.Series[3].Visible = visible;
                    downTresholdSerieVisible = visible;
                }
                if (extraSerieTypes == ExtraSerieTypes.Trend)
                {
                    CalculateTrend();
                    this.chartControl1.Series[1].Visible = trendSerieVisible = visible;
                }
            }



        }


        private void CalculateTrend()
        {

            if (dataIndicatorCode != null)
            {
                IList currentDataResult = new ArrayList();
                double mediaY = 0;
                double mediaX = 0;
                List<double> XiXm = new List<double>();
                List<double> YiYm = new List<double>();
                List<double> XY = new List<double>();
                List<double> X2 = new List<double>();
                double sumXY = 0;
                double sumX2 = 0;
                double b = 0;
                double a = 0;
                //Get mediaX and mediaY           
                #region mediaX and mediaY
                for (int i = 0; i < dataIndicatorCode.Count; i++)
                {
                    SeriesPoint point = (SeriesPoint)dataIndicatorCode[i];
                    currentDataResult.Add(point.Values[0]);
                    mediaY += point.Values[0];
                    mediaX += (i + 1);
                }
                mediaY = mediaY / dataIndicatorCode.Count;
                mediaX = mediaX / dataIndicatorCode.Count;
                #endregion mediaX and mediaY
                //Get XiXm and YiYm
                #region XiXm and YiYm
                for (int i = 0; i < currentDataResult.Count; i++)
                {
                    YiYm.Add((double)currentDataResult[i] - mediaY);
                    XiXm.Add((i + 1) - mediaX);
                }
                #endregion XiXm and YiYm
                //Get XY mediaXY and X2 mediaX2
                #region XY and X2
                for (int i = 0; i < currentDataResult.Count; i++)
                {
                    XY.Add(XiXm[i] * YiYm[i]);
                    X2.Add(XiXm[i] * XiXm[i]);
                    sumXY += XY[i];
                    sumX2 += X2[i];
                }
                #endregion XY and X2
                b = ((sumXY / currentDataResult.Count) / (sumX2 / currentDataResult.Count));
                a = mediaY - (b * mediaX);
                //Get trend
                #region trend
                for (int i = 0; i < dataIndicatorCode.Count; i++)
                {
                    SeriesPoint point = (SeriesPoint)dataIndicatorCode[i];
                    SeriesPoint sp1 = new SeriesPoint(point.DateTimeArgument, new double[] { (a + (b * (i + 1))) });
                    FormUtil.InvokeRequired(this.chartControl1, delegate
                    {
                        this.chartControl1.Series[1].Points.Add(sp1);
                    });
                }
                #endregion trend
            }
        }
        #endregion operatorChartControl




        void repositoryItemComboBoxSelectRoom_SelectedIndexChanged(object sender, EventArgs e)
        {
            //FormUtil.InvokeRequired(this,
            //delegate
            //{
            //    ShowOperatorsInRoom(true);
            //});        
        }

        private void ConfigureDashBoardControls()
        {
            if (GlobalIndicatorClassDashboards != null)
            {
                FormUtil.InvokeRequired(this,
                    delegate
                    {
                        indicatorDashBoardControlSystem.IsGeneralSupervisor = isGeneralSupervisor;
                        indicatorDashBoardControlDepartment.IsGeneralSupervisor = isGeneralSupervisor;
                        indicatorDashBoardControlGroup.IsGeneralSupervisor = isGeneralSupervisor;
                        indicatorDashBoardControlOperator.IsGeneralSupervisor = isGeneralSupervisor;

                        bool isFirstLevel = UserApplicationClientData.FirstLevel.Name == SupervisedApplicationName;
                        indicatorDashBoardControlSystem.IsFirstLevel = isFirstLevel;
                        indicatorDashBoardControlDepartment.IsFirstLevel = isFirstLevel;
                        indicatorDashBoardControlGroup.IsFirstLevel = isFirstLevel;
                        indicatorDashBoardControlOperator.IsFirstLevel = isFirstLevel;

                        indicatorDashBoardControlSystem.DashBoardType = IndicatorDashBoardControl.DashBoardClassType.SYSTEM;
                        indicatorDashBoardControlDepartment.DashBoardType = IndicatorDashBoardControl.DashBoardClassType.DEPARTMENT;
                        indicatorDashBoardControlGroup.DashBoardType = IndicatorDashBoardControl.DashBoardClassType.GROUP;
                        indicatorDashBoardControlOperator.DashBoardType = IndicatorDashBoardControl.DashBoardClassType.OPERATOR;

                        indicatorDashBoardControlSystem.SetPreferences(GlobalIndicatorClassDashboards);
                        indicatorDashBoardControlDepartment.SetPreferences(GlobalIndicatorClassDashboards);
                        indicatorDashBoardControlGroup.SetPreferences(GlobalIndicatorClassDashboards);
                        indicatorDashBoardControlOperator.SetPreferences(GlobalIndicatorClassDashboards);

                    });
                Application.DoEvents();
            }
        }

        void DefaultSupervisionForm_SupervisedApplicationChanged(object sender, SupervisedApplicationChangedEventArgs e)
        {
            SupervisedApplicationName = e.ApplicationName;
            ApplicationUtil.SupervisionApplicationName = e.ApplicationName;
            if (e.ApplicationName == UserApplicationClientData.Dispatch.Name)
            {
                SelectedDepartmentType = e.DepartmentType;
            }
            else
            {
                SelectedDepartmentType = null;
            }
        }

        private GridControlData GetObjectInGridControl(GridControlEx gridControl, ClientData clientObject)
        {
            GridControlData result = null;
            FormUtil.InvokeRequired(gridControl,
                delegate
                {
                    for (int i = 0; i < gridControl.Items.Count && result == null; i++)
                    {
                        GridControlData data = gridControl.Items[i] as GridControlData;
                        if (data.Tag.Code == clientObject.Code)
                        {
                            result = data;
                        }
                    }
                });
            return result;
        }

        //IList<int> operatorsCodeToUpdate = new BindingList<int>();

        void DefaultSupervisionForm_SupervisionCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
           
            try
            {
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    lock (syncCommited)
                    {
                        try
                        {
                            if (ServerServiceClient.GetInstance().OperatorClient == null)
                            { }
                        }
                        catch
                        { }
                        if (e.Objects[0] is IncidentClientData)//if it's an incident...
                        {
                            #region IncidentClientData
                            IncidentClientData incidentClient = e.Objects[0] as IncidentClientData;
                            #endregion
                        
                        }
                        else if (e.Objects[0] is OperatorCategoryClientData)
                        {
                            #region OperatorCategoryClientData
                            IList operators = GetOperatorsOnGrid();
                            IList operatorsToUpdate = new ArrayList();
                            foreach (GridControlData grid in operators)
                            {
                                OperatorClientData ope = grid.Tag as OperatorClientData;
                                foreach (OperatorCategoryClientData opeCategory in e.Objects)
                                {
                                    if (ope.OperatorCategoryCode == opeCategory.Code)
                                    {
                                        ope.OperatorCategory = opeCategory.FriendlyName;
                                        operatorsToUpdate.Add(ope.Code);
                                    }
                                }
                            }
                            if (operatorsToUpdate.Count > 0)
                            {
                                operatorsToUpdate = SearchOperators(operatorsToUpdate);
                                foreach (OperatorClientData oper in operatorsToUpdate)
                                {
                                    ProcessReceivedOperatorClient(oper, CommittedDataAction.Save);
                                }
                            }
                            #endregion
                        }
                        else if (e.Objects[0] is OperatorClientData)
                        {
                            #region OperatorClientData
                            OperatorClientData oper = e.Objects[0] as OperatorClientData;

                            if (e.Action != CommittedDataAction.Delete)
                            {
                                oper = (OperatorClientData)ServerServiceClient.GetInstance().SearchClientObject(
                                    SmartCadHqls.GetCustomHql(
                                    SmartCadHqls.OperatorWithSupervisors, oper.Code));

                                oper.DepartmentTypes = ((OperatorClientData)ServerServiceClient.GetInstance().SearchClientObject(
                                    SmartCadHqls.GetCustomHql(
                                    SmartCadHqls.OperatorWithDepartmentTypes, oper.Code))).DepartmentTypes;
                            }
                            //if (operatorsCodeToUpdate.Contains(oper.Code) == false)
                            //    operatorsCodeToUpdate.Add(oper.Code);
                            ProcessReceivedOperatorClient(oper, e.Action);                            
                            #endregion

                        }   
                        else if (e.Objects[0] is OperatorLightClientData)
						{                          
							#region OperatorLightClientData

                            IList operatorsToUpdate = new ArrayList();
							foreach (OperatorLightClientData oper in e.Objects)
                            {
                                GridControlData gridData = GetObjectInGridControl(gridControlExOperators, oper);
                                if (gridData != null && gridData.Tag.Code == oper.Code)
                                {
                                    bool firstLevel = SupervisedApplicationName == UserApplicationClientData.FirstLevel.Name;
                                    if (oper.Delete == true || (firstLevel && oper.FirstLevelAccess == false)
                                        || (!firstLevel && oper.DispatchAccess == false))
                                    {
                                        operatorSynBox.Sync(gridData, CommittedDataAction.Delete);
                                        operatorsCodeToDelete.Add(oper.Code);
                                    }
                                    else if (oper.Delete == false)
                                    {
                                        if (gridData is GridControlDataOperator && ((GridControlDataOperator)gridData).Schedule != oper.CurrentWorkShiftName)
                                            ((GridControlDataOperator)gridData).Schedule = oper.CurrentWorkShiftName;
                                    }
                                }
                                else if (gridData == null && oper.Delete == false)
                                {
                                    operatorsToUpdate.Add(oper.Code);
                                    //OperatorClientData clientOperator = new OperatorClientData();
                                    //clientOperator.Code = oper.Code;
                                    //clientOperator = ServerServiceClient.GetInstance().RefreshClient(clientOperator) as OperatorClientData;
                                    //if (clientOperator != null)
                                    //{
                                    //     ProcessReceivedOperatorClient(clientOperator, CommittedDataAction.Save);
                                    //}
                                }
							}

                            //Update operators in list
                            if (operatorsToUpdate.Count > 0) 
                            {
                                operatorsToUpdate = SearchOperators(operatorsToUpdate);

                                foreach (OperatorClientData oper in operatorsToUpdate)
                                {
                                    ProcessReceivedOperatorClient(oper, CommittedDataAction.Save);
                                }                                                            
                            }


                            //Delete all operators out of workshift schedule from Room
                            RoomClientData selectedRoom = (RoomClientData)barEditItemSelectRoom.EditValue;
                            if (selectedRoom != null && selectedRoom.Image != null)
                            {
                                RenderRoom(selectedRoom);
                                RenderSeats(selectedRoom, gridControlExOperators.Items);
                            }
							#endregion
                                                     
						}
                        else if (e.Objects[0] is WorkShiftVariationClientData)
                        {
                           
                            #region  WorkShiftVariationClientData
                            WorkShiftVariationClientData wsVariation = e.Objects[0] as WorkShiftVariationClientData;
                            IList supervisorWorkShifts = ServerServiceClient.GetInstance().OperatorClient.WorkShifts;

                            if (e.Action == CommittedDataAction.Delete) 
                            {
                                if (supervisorWorkShifts != null &&
                                    supervisorWorkShifts.Contains(wsVariation) == true)
                                {
                                    ServerServiceClient.GetInstance().OperatorClient.WorkShifts.Remove(wsVariation);
                                
                                }
                                IList operatorsToDelete = GetOperatorsByWorkshift(wsVariation);
                                if (operatorsToDelete.Count > 0)
                                {
                                    foreach (OperatorClientData oper in operatorsToDelete)
                                    {
                                        operatorSynBox.Sync(new GridControlDataOperator(oper), CommittedDataAction.Delete);
                                    }
                                }

                            }
                            else
                            {
                                List<GridControlDataOperator> operatorsToDelete = new List<GridControlDataOperator>();
                                FormUtil.InvokeRequired(gridControlExOperators,
                                    delegate
                                    {
                                        if (this.gridControlExOperators.DataSource != null)
                                        {
                                            foreach (GridControlDataOperator gridOper in this.gridControlExOperators.DataSource as BindingList<GridControlDataOperator>)
                                            {
                                                if (gridOper.OperatorClient.WorkShifts != null && gridOper.OperatorClient.WorkShifts.Contains(wsVariation))
                                                {
                                                    operatorsToDelete.Add(gridOper);
                                                }
                                            }
                                        }
                                    });

                                int departmentTypeCode = -1;
                                if (selectedDepartmentType != null)
                                {
                                    departmentTypeCode = selectedDepartmentType.Code;
                                }
                                IList operatorsToUpdate =
                                    ServerServiceClient.GetInstance().GetSupervisedOperators(supervisedApplicationName, departmentTypeCode);
                                int index = operatorsToUpdate.IndexOf(ServerServiceClient.GetInstance().OperatorClient);
                                if (index == -1)
                                {                              
                                    if (supervisorWorkShifts != null && supervisorWorkShifts.Contains(wsVariation))
                                    {
                                        ServerServiceClient.GetInstance().OperatorClient.WorkShifts.Remove(wsVariation);
                                        //ServerServiceClient.GetInstance().UserAccount.WorkShift = null;
                                    }
                                }
                                else
                                {
                                    if (operatorsToUpdate.Count > 0)
                                    {
                                        operatorSynBox.Sync(operatorsToUpdate);
                                        FormUtil.InvokeRequired(this,
                                            delegate
                                            {
                                                gridControlExOperators_SelectionWillChange(null, new FocusedRowChangedEventArgs(GridControlEx.InvalidRowHandle, GridControlEx.InvalidRowHandle));
                                            });
                                    }
                                }
                              
                                foreach (OperatorClientData oper in operatorsToUpdate)
                                {
                                    if (!oper.IsSupervisor)
                                    {
                                        GridControlDataOperator gridData = new GridControlDataOperator(oper);
                                        int index2 = operatorsToDelete.IndexOf(gridData);
                                        if (index2 != -1)
                                        {
                                            operatorsToDelete.RemoveAt(index2);
                                        }
                                        ProcessReceivedOperatorClient(oper, CommittedDataAction.Update);
                                    }
                                }
                              
                                foreach (GridControlDataOperator gridData in operatorsToDelete)
                                {
                                    gridControlExOperators.DeleteItem(gridData);
                                    operatorsCodeToDelete.Add(gridData.OperatorClient.Code);
                                }
                            }
                          
                            #endregion
                         
                                       
                        }
                        else if (e.Objects[0] is OperatorAssignClientData)
                        {
                            #region OperatorAssignClientData
                            DateTime now = ServerServiceClient.GetInstance().GetTimeFromDB();
                            foreach (OperatorAssignClientData operAssign in e.Objects)
                            {
                                if (operAssign.StartDate <= now && operAssign.EndDate >= now)
                                {
                                    OperatorClientData op = (OperatorClientData)ServerServiceClient.GetInstance().SearchClientObject(SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorByCode, operAssign.SupervisedOperatorCode));

                                    if (op != null)
                                          ProcessReceivedOperatorClient(op, CommittedDataAction.Update);
                                    
                                }
                            }


                            #endregion
                        
                        }
                        else if (e.Objects[0] is IndicatorResultClientData)
                        {
                            
                            foreach (IndicatorResultClientData indicatorResult in e.Objects)
                            {
                                #region IndicatorResultClientData

                                //IndicatorResultClientData indicatorResult = e.Objects[0] as IndicatorResultClientData;
                                foreach (IndicatorResultValuesClientData irvcd in indicatorResult.Values)
                                {
                                    IndicatorClassDashboardClientData icdcd = new IndicatorClassDashboardClientData();
                                    icdcd.ClassName = irvcd.IndicatorClassName;
                                    icdcd.IndicatorName = irvcd.IndicatorName;
                                    icdcd.IndicatorCustomCode = indicatorResult.IndicatorCustomCode;
                                    if (irvcd.IndicatorClassName.Equals(IndicatorClassClientData.System.Name))
                                    {
                                        IndicatorResultClientData result = new IndicatorResultClientData(indicatorResult, false);
                                        result.Values.Add(irvcd);

                                        SwapResultOnDashBoard(result);
                                    }
                                    else if (irvcd.IndicatorClassName.Equals(IndicatorClassClientData.Department.Name))
                                    {
                                        if (SelectedDepartmentType != null && SelectedDepartmentType.Code == irvcd.CustomCode)
                                        {
                                            IndicatorResultClientData result = new IndicatorResultClientData(indicatorResult, false);
                                            result.Values.Add(irvcd);

                                            SwapResultOnDashBoard1(result);
                                        }
                                    }
                                    else if (irvcd.IndicatorClassName.Equals(IndicatorClassClientData.Group.Name))
                                    {
                                        if (operatorClientCode == irvcd.CustomCode)
                                        {  
                                            IndicatorResultClientData result = new IndicatorResultClientData(indicatorResult, false);
                                            result.Values.Add(irvcd);

                                            SwapResultOnDashBoard2(result);

                                        }
                                    }
                                    else if (irvcd.IndicatorClassName.Equals(IndicatorClassClientData.Operator.Name))
                                    {
                                       
                                        FormUtil.InvokeRequired(this,
                                            delegate
                                            {
                                                if ((SupervisedApplicationName == UserApplicationClientData.FirstLevel.Name && irvcd.IndicatorCustomCode.StartsWith("PN")) ||
                                                    (SupervisedApplicationName == UserApplicationClientData.Dispatch.Name && irvcd.IndicatorCustomCode.StartsWith("D")))
                                                {
                                                    UpdatePercentageChronogram(indicatorResult, irvcd);
                                                    IList selectedItems = gridControlExOperators.SelectedItems;
                                                    if (selectedItems.Count > 0)
                                                    {
                                                        OperatorClientData oper = (selectedItems[0] as GridControlDataOperator).OperatorClient;
                                                        if (oper.Code == irvcd.CustomCode)
                                                        {
                                                            IndicatorResultClientData result = new IndicatorResultClientData(indicatorResult, false);
                                                            result.Values.Add(irvcd);

                                                            SwapResultOnDashBoard3(result);
                                                            this.indicatorGridControl1.AddOrUpdate(irvcd);
                                                            //This code should not be here because the indicatorGrid control should be fill with all the indicators.
                                                            //int index = GlobalIndicatorClassDashboards.IndexOf(icdcd);
                                                            //if (index != -1)
                                                            //{
                                                            //    if (((GlobalIndicatorClassDashboards[index] as IndicatorClassDashboardClientData).ShowDashboard) && (this.Visible == true))
                                                            //    {
                                                            //        this.indicatorGridControl1.AddOrUpdate(irvcd);
                                                            //    }
                                                            //}

                                                        }
                                                    }
                                                }
                                            });
                                      
                                    }
                                }
                                Application.DoEvents();
                                #endregion
                            }
                           
                        }
                        else if (e.Objects[0] is RoomClientData)
                        {
                            #region RoomClientData

                            RoomClientData room = e.Objects[0] as RoomClientData;
                            IDictionary<int, OperatorClientData> operatorsToUpdate = new Dictionary<int, OperatorClientData>();
                            IList operatorsInRoom = GetOperatorsByRoom(room);

                            if (e.Action == CommittedDataAction.Delete)
                            {
                                foreach (OperatorClientData oper in operatorsInRoom)
                                {
                                    OperatorClientData operUpdated = (OperatorClientData)ServerServiceClient.GetInstance().RefreshClient(oper);
                                    if (operUpdated != null)
                                    {
                                        operUpdated.RoomName = "";
                                        operatorsToUpdate.Add(operUpdated.Code, operUpdated);
                                    }
                                }

                                globalRooms.Remove(room.Code);
                            }
                            else
                            {
                                
                                foreach (OperatorClientData oper in GetOperatorsByRoomUpdated(room))
                                {
                                    if (operatorsToUpdate.ContainsKey(oper.Code) == false)
                                    {
                                        OperatorClientData operUpdated = (OperatorClientData)ServerServiceClient.GetInstance().RefreshClient(oper);
                                        if (operUpdated != null)
                                        {
                                            operUpdated.RoomName = room.Name;
                                            operatorsToUpdate.Add(operUpdated.Code, operUpdated);
                                        }
                                    }
                                }

                                if (globalRooms.ContainsKey(room.Code) == true)
                                    globalRooms[room.Code] = room;
                                else
                                    globalRooms.Add(room.Code, room);
                            }

                            if (operatorsToUpdate.Count > 0)
                            {
                                foreach (OperatorClientData oper in operatorsToUpdate.Values)
                                {
                                    operatorSynBox.Sync(new GridControlDataOperator(oper), CommittedDataAction.Update);
                                }
                            }

                            FormUtil.InvokeRequired(this,
                                    delegate
                                    {
                                        FillRoomsRepository();
                                        if (this.tabbedControlGroupMonitorActivities.SelectedTabPage == this.layoutControlGroupRoom)
                                        {
                                            ShowOperatorsInRoom(true);
                                        }
                                    });

                            #endregion
                          
                        }
                        else if (e.Objects[0] is RoomSeatClientData)
                        {
                            #region RoomSeatClientData
                            RoomSeatClientData roomSeat = e.Objects[0] as RoomSeatClientData;
                            IDictionary<int, OperatorClientData> operatorsToUpdate = new Dictionary<int, OperatorClientData>();
                                                        

                            RoomClientData room = null; 
                            if (globalRooms.ContainsKey(roomSeat.Room.Code) == true) 
                                     room = globalRooms[roomSeat.Room.Code];

                            if (room != null)
                            {                             

                                foreach (OperatorClientData oper in GetOperatorsByRoomUpdated(room))
                                {
                                    if (operatorsToUpdate.ContainsKey(oper.Code) == false)
                                    {
                                        OperatorClientData operUpdated = (OperatorClientData)ServerServiceClient.GetInstance().RefreshClient(oper);
                                        if (operUpdated != null)
                                        {
                                            operatorsToUpdate.Add(operUpdated.Code, operUpdated);
                                        }
                                    }
                                }
                            }

                            if (operatorsToUpdate.Count > 0)
                            {
                                foreach (OperatorClientData oper in operatorsToUpdate.Values)
                                {
                                    operatorSynBox.Sync(new GridControlDataOperator(oper), CommittedDataAction.Update);
                                }
                            }

                            FormUtil.InvokeRequired(this,
                                    delegate
                                    {
                                        FillRoomsRepository();
                                        if (this.tabbedControlGroupMonitorActivities.SelectedTabPage == this.layoutControlGroupRoom)
                                        {
                                            ShowOperatorsInRoom(true);
                                        }
                                    });

                            #endregion

                            
                        }
                        else if (e.Objects[0] is IndicatorClassDashboardClientData)
                        {
                            #region IndicatorClassDashboardClientData
                            IndicatorClassDashboardClientData classDashboard = e.Objects[0] as IndicatorClassDashboardClientData;
                            FormUtil.InvokeRequired(this,
                                delegate
                                {
                                    indicatorDashBoardControlSystem.AddOrUpdatePreference(classDashboard);
                                    indicatorDashBoardControlDepartment.AddOrUpdatePreference(classDashboard);
                                    indicatorDashBoardControlGroup.AddOrUpdatePreference(classDashboard);
                                    indicatorDashBoardControlOperator.AddOrUpdatePreference(classDashboard);
                                });
                            lock (GlobalIndicatorClassDashboards.SyncRoot)
                            {
                                int index = GlobalIndicatorClassDashboards.IndexOf(classDashboard);
                                if (index == -1)
                                {
                                    GlobalIndicatorClassDashboards.Add(classDashboard);
                                }
                                else
                                {
                                    GlobalIndicatorClassDashboards[index] = classDashboard;
                                }
                            }
                            #endregion

                           
                        }
                        else if (e.Objects[0] is OperatorCategoryHistoryClientData)
                        {
                            #region OperatorCategoryHistoryClientData
                            OperatorCategoryHistoryClientData category = e.Objects[0] as OperatorCategoryHistoryClientData;
                            OperatorClientData oper = new OperatorClientData();
                            oper.Code = category.OperatorCode;
                            oper = ((OperatorClientData)ServerServiceClient.GetInstance().RefreshClient(oper));
                            if (oper != null && ((IList)gridView1.DataSource).Contains(new GridControlDataOperator(oper)))
                            {
                                GridControlDataOperator gridData = new GridControlDataOperator(oper);
                                gridData.PercentageSchedule = GetPreviousPercentageChronogram(gridData);
                                operatorSynBox.Sync(gridData, CommittedDataAction.Update);
                            }
                            #endregion

                           
                        }
						else if (e.Objects[0] is ApplicationMessageClientData)
						{
							#region ApplicationMessageClientData
							ApplicationMessageClientData amcd = e.Objects[0] as ApplicationMessageClientData;
							FormUtil.InvokeRequired(this, delegate
							{
								if (amcd.ToOperators.Contains(ServerServiceClient.GetInstance().OperatorClient))
								{
                                    if ((MdiParent as SupervisionForm).operatorChatForm != null && (MdiParent as SupervisionForm).operatorChatForm.IsDisposed == false)
                                    {
                                        (MdiParent as SupervisionForm).operatorChatForm.OpenNewTab(amcd.Operator, amcd);
                                        //(MdiParent as SupervisionForm).operatorChatForm.TopMost = false;
                                    }
                                    else
                                    {
                                        alertControl1.Show(this, ResourceLoader.GetString2("MessageFrom") + ": " + amcd.Operator.Login, amcd.Message);
                                        if (isGeneralSupervisor == true)
                                            (MdiParent as SupervisionForm).operatorChatForm = new OperatorChatForm(supervisedApplicationName, ChatOperatorType.GeneralSupervisor);
                                        else
                                            (MdiParent as SupervisionForm).operatorChatForm = new OperatorChatForm(supervisedApplicationName, ChatOperatorType.SpecificSupervisor);

                                        if (selectedDepartmentType != null)
                                            (MdiParent as SupervisionForm).operatorChatForm.DepartmentTypeCode = selectedDepartmentType.Code;

                                        (MdiParent as SupervisionForm).operatorChatForm.OpenNewTab(amcd.Operator, amcd);
                                        (MdiParent as SupervisionForm).operatorChatForm.WindowState = FormWindowState.Minimized;
                                        (MdiParent as SupervisionForm).operatorChatForm.Show();
                                    }
									FlashingWindow.FlashWindowEx((MdiParent as SupervisionForm).operatorChatForm);
								}
							});
							#endregion

                           
						}
                        else if (e.Objects[0] is ApplicationPreferenceClientData)
                        {
                            #region ApplicationPreferenceData
                            foreach (ApplicationPreferenceClientData preference in e.Objects)
                            {
                                if (e.Action == CommittedDataAction.Update && preference.Name.Equals("ShowErrorDetails", StringComparison.CurrentCultureIgnoreCase) == true)
                                {
                                    MessageForm.CheckPreference = MessageForm.CheckPreference = (preference.Value.ToString().ToUpper() == "TRUE" ? true : false);
                                }
                            }
                            #endregion
                        }
                        else if (e.Objects[0] is OperatorStatusClientData)
                        {
                            #region OperatorStatusClientData

                            FormUtil.InvokeRequired(this, delegate
                            {
                                UpdateChartLegend(e.Objects[0] as OperatorStatusClientData);
                                UpdateStatusLabel(e.Objects[0] as OperatorStatusClientData);
                                LoadData(false);
                            });

                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
           
        }

        private IList SearchOperators(IList operators) 
        {
            IList result = new ArrayList();
            StringBuilder builder = new StringBuilder();
            builder.Append("(");

            foreach (int operCode in operators)
            {
                builder.Append(operCode);
                builder.Append(",");
            }

            builder.Remove(builder.Length - 1, 1);
            builder.Append(")");

            string hql = @"SELECT oper FROM OperatorData oper WHERE oper.Code IN {0}";

            result = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(hql, builder.ToString()));

            return result;
        }

        private void ProcessReceivedOperatorClient(OperatorClientData oper, CommittedDataAction actionToExecute)
        {
            #region OperatorClientData
            try
            {
                if (actionToExecute != CommittedDataAction.Delete)
                {
                    bool canBeSupervised = ServerServiceClient.GetInstance().CheckOperatorClientAccess(oper, SupervisedApplicationName);

                    if (SupervisedApplicationName == UserApplicationClientData.Dispatch.Name)
                    {
                        canBeSupervised = canBeSupervised && oper.DepartmentTypes.Contains(SelectedDepartmentType);
                    }
                    if (!canBeSupervised)
                    {
                        GridControlDataOperator grid = new GridControlDataOperator(oper);
                        operatorSynBox.Sync(grid, CommittedDataAction.Delete);
                        operatorsCodeToDelete.Add(oper.Code);						
                    }
                    else
                    {
                        if (oper.IsSupervisor == false)
                        {
                           
                            bool checkLoginMode = Convert.ToBoolean((this.MdiParent as SupervisionForm).globalApplicationPreferences["CheckLogInMode"].Value);
                            if (checkLoginMode == true)
                            {                            
                                bool operatorShoulBeConnected = OperatorShouldBeConnected(oper);

                                if (SupervisedByOperator(oper, operatorClientCode) == true ||
                                    (isGeneralSupervisor &&
                                        ((SupervisedApplicationName == "FirstLevel" && canBeSupervised) ||
                                        (SupervisedApplicationName == "Dispatch" && canBeSupervised && CanSuperviseOperatorDepartment(oper.DepartmentTypes)))))
                                {
                                    SessionHistoryClientData lastSession = GetLastSessionHistory(oper.LastSessions, SupervisedApplicationName);
                                    CommittedDataAction action = CommittedDataAction.Update;
                                    if (operatorShoulBeConnected == false &&
                                        (lastSession == null || lastSession.IsLoggedIn == false
                                        || SupervisedByOperator(oper, operatorClientCode) == true))
                                    {
                                        action = CommittedDataAction.Delete;
                                    }

                                    GridControlDataOperator gridData = new GridControlDataOperator(oper);
                                    gridData.PercentageSchedule = GetPreviousPercentageChronogram(gridData);
                                    operatorSynBox.Sync(gridData, action);
                                    if (actionToExecute == CommittedDataAction.Save)
                                    {
                                        FormUtil.InvokeRequired(this, delegate
                                        {
                                            gridControlExOperators.AddOrUpdateRepositoryItemComboBox(gridData);
                                        });
                                    }
                                    if (action == CommittedDataAction.Delete || oper.WorkShifts == null || oper.WorkShifts.Count == 0)
                                        operatorsCodeToDelete.Add(oper.Code);

                                    FormUtil.InvokeRequired(this,
                                        delegate
                                        {
                                            if (this.tabbedControlGroupMonitorActivities.SelectedTabPage == this.layoutControlGroupRoom)
                                            {
                                                ShowOperatorsInRoom(true);
                                            }
                                        });
                                }
                                else if (SupervisedByOperator(oper, operatorClientCode) == false &&
                                    isGeneralSupervisor == false)
                                {
                                    GridControlDataOperator grid = new GridControlDataOperator(oper);
                                    operatorSynBox.Sync(grid, CommittedDataAction.Delete);
                                    operatorsCodeToDelete.Add(oper.Code);
                                }
                            }
                            else
                            {
               
                                if (isGeneralSupervisor &&
                                        (SupervisedApplicationName == "FirstLevel" && canBeSupervised) ||
                                        (SupervisedApplicationName == "Dispatch" && canBeSupervised && CanSuperviseOperatorDepartment(oper.DepartmentTypes)))
                                {
                                    SessionHistoryClientData lastSession = GetLastSessionHistory(oper.LastSessions, SupervisedApplicationName);
                                    CommittedDataAction action = CommittedDataAction.Update;
                                    GridControlDataOperator gridData = new GridControlDataOperator(oper);
                                    gridData.PercentageSchedule = GetPreviousPercentageChronogram(gridData);
                                    operatorSynBox.Sync(gridData, action);
                                 
                                    if (actionToExecute == CommittedDataAction.Save)
                                    {
                                        FormUtil.InvokeRequired(this, delegate
                                        {
                                            gridControlExOperators.AddOrUpdateRepositoryItemComboBox(gridData);
                                        });
                                    }
                                    
                                    FormUtil.InvokeRequired(this,
                                    delegate
                                    {
                                        if (this.tabbedControlGroupMonitorActivities.SelectedTabPage == this.layoutControlGroupRoom)
                                        {
                                            ShowOperatorsInRoom(true);
                                        }
                                    });
                                }
                                else
                                {
                                    GridControlDataOperator grid = new GridControlDataOperator(oper);
                                    operatorSynBox.Sync(grid, CommittedDataAction.Delete);
                                    operatorsCodeToDelete.Add(oper.Code);
                                }

                            }                            
                        }
                        else
                        {
                            GridControlDataOperator gridData = new GridControlDataOperator(oper);
                            operatorSynBox.Sync(gridData, CommittedDataAction.Delete);
							operatorsCodeToDelete.Add(oper.Code);							
                        }
                    }
                }
                else 
                {
                    GridControlDataOperator grid = new GridControlDataOperator(oper);
                    operatorSynBox.Sync(grid, CommittedDataAction.Delete);
                    operatorsCodeToDelete.Add(oper.Code);  
                }
            }
            catch(Exception ex)
            {
                SmartLogger.Print(ex);
            }
            #endregion
        }


        private bool SupervisedByOperator(OperatorClientData oper, int supervisorCode)
        {
            OperatorAssignClientData assign = null;
            DateTime now = ServerServiceClient.GetInstance().GetTime();
            bool result = false;

            if (oper.Supervisors != null)
            {
                for (int i = 0; i < oper.Supervisors.Count && assign == null; i++)
                {
                    OperatorAssignClientData temp = oper.Supervisors[i] as OperatorAssignClientData;
                    if ((int)now.DayOfWeek == (int)temp.StartDate.DayOfWeek && temp.StartDate <= now && now <= temp.EndDate)
                    {
                        assign = temp;
                    }
                }
            }

            if (assign != null)
            {
                if (assign.SupervisorCode == supervisorCode)
                    result = true;
            }

            return result;
        }

        /// <summary>
        /// Find the date that is greater than the date passed.
        /// </summary>
        /// <param name="dates">List of dates.</param>
        /// <param name="date">Date used as comparison</param>
        /// <returns>The date from the list that is greater than the date passed.</returns>
        private DateTime FindSpecificDate(IList dates, DateTime date)
        {
            DateTime result = date;
            if (dates.Count > 0)
            {
                result = (DateTime)dates[0];
                //bool found = false;
                for (int i = 1; i < dates.Count; i++)
                {
                    DateTime pivot = (DateTime)dates[i];
                    if (date.TimeOfDay >= result.TimeOfDay)
                        result = pivot;
                    else
                        i = dates.Count;
                }

                //if (result == ApplicationUtil.SCHEDULE_REFERENCE_DATE.AddDays(1))
                //    result = date.Date.AddDays(1);
                //else 
                    if (result.Date < date.Date && date.Date == DateTime.Now.Date)
                    result = new DateTime(date.Year, date.Month, date.Day, result.Hour, result.Minute, result.Second);

                if (result < date)
                    result = date;
            }
            return result;
        }

		private void UpdateLastSessionStatusHistory(OperatorClientData ope, DateTime now, ArrayList dates, IList values)
		{
            //Todays date.
            DateTime startDate = now.Date;
			DateTime startDateStatus = DateTime.MaxValue;
            ArrayList starts = new ArrayList();
            ArrayList ends = new ArrayList();

            if (CheckLogIn == true)
            {
                //We ask for the start hour that the person should begin to work.

                starts = (ArrayList)dates[0];
                ends = (ArrayList)dates[1];
                //Look for the smallest hour, that is the time where the user starts its shift or extra time slot.
                foreach (DateTime dT in starts)
                {
                    if (dT < startDateStatus)
                        startDateStatus = dT;
                }
                TimeSpan elapsedTime = new TimeSpan(startDateStatus.Hour, startDateStatus.Minute, startDate.Second);
                startDate = startDate.Add(elapsedTime);
            }

            //Begin with the start date of the operator shift, then is used at the last pivotal point for the date.
            DateTime lastDate = startDate;
            OperatorStatusChartData oscd = null;
            for (int statusIndex = 0; statusIndex < values.Count; statusIndex++)
            {
                SessionStatusHistoryClientData status = (SessionStatusHistoryClientData)values[statusIndex];
                if (isGeneralSupervisor == true)
                {
                    if (status.StartDateStatus < now.Date)
                        status.StartDateStatus = now.Date;
                }
                else
                {
                    if (status.StartDateStatus < startDate)
                        status.StartDateStatus = startDate;
                }

                SessionStatusHistoryClientData nextStatus = null;
                if (statusIndex + 1 < values.Count)
                    nextStatus = (SessionStatusHistoryClientData)values[statusIndex + 1];

                //We calculate the end date because there could be case that the end date is null.
                DateTime endDate = DateTime.MaxValue;
                //If the status has an end date
                if (status.EndDateStatus != DateTime.MinValue)
                    endDate = status.EndDateStatus;
                //If there is a next status, the start date of the next one is the end of the current status.
                else if (nextStatus != null)
                    endDate = nextStatus.StartDateStatus;
                //If there is not next status the end is now.
                else
                    endDate = now;

                oscd = null;

                //If the last status DO NOT end where this status start add a not connected status between them.
                if (status.StartDateStatus != lastDate)
                {
                    //We have to check if the work shift it not continous, in order to leave the white space.
                    DateTime endRealDate = FindSpecificDate(ends, lastDate);
                    if (endRealDate < status.StartDateStatus && lastDate < endRealDate)
                    {
                        ArrayList workdates = new ArrayList();
                        workdates = GetWorkShiftLastDate(ope.WorkShifts, lastDate, endRealDate);
                        int nlimit = (workdates.Count > 0 ? (workdates.Count / 2) : workdates.Count);
                        for (int ncounter = 0; ncounter < nlimit; ncounter++)
                        {
                            if (ope.WorkShifts.Count > 0 && (DateTime)workdates[ncounter * 2] < (DateTime)workdates[(ncounter * 2) + 1] && workdates[ncounter * 2] != workdates[(ncounter * 2) + 1])
                            {
                                if (lastDate < (DateTime)workdates[(ncounter * 2) + 1] && lastDate >= (DateTime)workdates[(ncounter * 2)])
                                {
                                    if ((DateTime)workdates[(ncounter * 2) + 1] > endRealDate) workdates[(ncounter * 2) + 1] = endRealDate;
                                    if ((DateTime)workdates[(ncounter * 2) + 1] > now) workdates[(ncounter * 2) + 1] = now;
                                    if (lastDate <= (DateTime)workdates[(ncounter * 2) + 1])
                                    {
                                        oscd = new OperatorStatusChartData(lastDate, (DateTime)workdates[(ncounter * 2) + 1], NOT_CONNECTED, ope);
                                        statusData[oscd.Serie].Add(oscd);
                                    }
                                }
                            }
                        }
                    }
                    //We find the date that could be the start portion of the next segment.
                    bool next = false;
                    while (next == false)
                    {
                        next = true;
                        DateTime newLastDate = FindSpecificDate(starts, lastDate);
                        //If the date found is less than the start date of the next segment, if not the work shift was change.
                        if (lastDate < status.StartDateStatus &&
                            newLastDate < status.StartDateStatus)
                        {
                            lastDate = newLastDate;
                            endRealDate = FindSpecificDate(ends, lastDate);
                            if (endRealDate < status.StartDateStatus && lastDate < endRealDate)
                            {
                                ArrayList workdates = new ArrayList();
                                workdates = GetWorkShiftLastDate(ope.WorkShifts, lastDate, endRealDate);
                                int nlimit = (workdates.Count > 0 ? (workdates.Count / 2) : workdates.Count);
                                for (int ncounter = 0; ncounter < nlimit; ncounter++)
                                {
                                    if (ope.WorkShifts.Count > 0 && (DateTime)workdates[ncounter * 2] < (DateTime)workdates[(ncounter * 2) + 1] && workdates[ncounter * 2] != workdates[(ncounter * 2) + 1])
                                    {
                                        if ((DateTime)workdates[ncounter * 2] < lastDate) continue;
                                        if (endDate >= (DateTime)workdates[ncounter * 2] && endDate <= (DateTime)workdates[(ncounter * 2) + 1])
                                        {
                                            workdates[ncounter * 2] = endDate;
                                        }                                        
                                        if ((DateTime)workdates[(ncounter * 2) + 1] <= now && (DateTime)workdates[ncounter * 2] <= (DateTime)workdates[(ncounter * 2) + 1])
                                        {
                                            oscd = new OperatorStatusChartData((DateTime)workdates[ncounter * 2], (DateTime)workdates[(ncounter * 2) + 1], NOT_CONNECTED, ope);
                                            statusData[oscd.Serie].Add(oscd);
                                            lastDate = endRealDate;
                                            next = false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (lastDate < status.StartDateStatus)
                    {
                        bool checkStatus = false;
                        ArrayList workdates = new ArrayList();
                        workdates = GetWorkShiftLastDate(ope.WorkShifts, lastDate, status.StartDateStatus);
                        int nlimit = (workdates.Count > 0 ? (workdates.Count / 2) : workdates.Count);
                        for (int ncounter = 0; ncounter < nlimit; ncounter++)
                        {
                            if (ope.WorkShifts.Count > 0 && (DateTime)workdates[ncounter * 2] < (DateTime)workdates[(ncounter * 2) + 1] && workdates[ncounter * 2] != workdates[(ncounter * 2) + 1])
                            {
                                if (lastDate > (DateTime)workdates[ncounter * 2])
                                {
                                    workdates[ncounter * 2] = lastDate;
                                }
                                if ((DateTime)workdates[(ncounter * 2) + 1] > status.StartDateStatus && status.StartDateStatus >= (DateTime)workdates[ncounter * 2])
                                {
                                    workdates[(ncounter * 2) + 1] = status.StartDateStatus;
                                    checkStatus = true;
                                }
                                if ((DateTime)workdates[(ncounter * 2) + 1] > now)
                                {
                                    workdates[(ncounter * 2) + 1] = now;
                                }
                                if ((DateTime)workdates[ncounter * 2] <= (DateTime)workdates[(ncounter * 2) + 1])
                                {
                                    oscd = new OperatorStatusChartData((DateTime)workdates[ncounter * 2], (DateTime)workdates[(ncounter * 2) + 1], NOT_CONNECTED, ope);
                                    statusData[oscd.Serie].Add(oscd);
                                    if (checkStatus == true)
                                    {
                                        break;
                                    }
                                }
                            }
                         }
                    }
                }
                if (status.StartDateStatus <= endDate)
                {
                    ArrayList workdates = new ArrayList();
                    workdates = GetWorkShiftLastDate(ope.WorkShifts, status.StartDateStatus, endDate);
                    int nlimit = (workdates.Count > 0 ? (workdates.Count / 2) : workdates.Count);
                    for (int ncounter = 0; ncounter < nlimit; ncounter++)
                    {
                        if (status.StartDateStatus >= (DateTime)workdates[ncounter * 2])
                        {
                            oscd = new OperatorStatusChartData(status.StartDateStatus, endDate, status.StatusName, ope);
                            statusData[oscd.Serie].Add(oscd);
                            lastDate = oscd.End;
                        }
                    }
                }
            }
            //if the last session history is not till now
            if (lastDate != DateTime.MinValue && lastDate < now && values.Count > 0)
            {
                //We have to find the start date of the not connected portion, because it could be that it is not the end of the last segment.
                DateTime endRealDate = FindSpecificDate(ends, lastDate);
                if (endRealDate > lastDate && endRealDate < now)
                {
                    ArrayList workdates = new ArrayList();
                    workdates = GetWorkShiftLastDate(ope.WorkShifts, lastDate, endRealDate);
                    int nlimit = (workdates.Count > 0 ? (workdates.Count / 2) : workdates.Count);
                    for (int ncounter = 0; ncounter < nlimit; ncounter++)
                    {
                        if (ope.WorkShifts.Count > 0 && (DateTime)workdates[ncounter * 2] < (DateTime)workdates[(ncounter * 2) + 1] && workdates[ncounter * 2] != workdates[(ncounter * 2) + 1])
                        {
                            if (lastDate >= (DateTime)workdates[ncounter * 2] && lastDate <= (DateTime)workdates[(ncounter * 2) + 1])
                            {
                                workdates[ncounter * 2] = lastDate;
                            }
                            if ((DateTime)workdates[(ncounter * 2) + 1] <= now && lastDate <= (DateTime)workdates[(ncounter * 2) + 1] && lastDate >= (DateTime)workdates[ncounter * 2] && lastDate <= (DateTime)workdates[(ncounter * 2) + 1])
                            {
                                oscd = new OperatorStatusChartData(lastDate, (DateTime)workdates[(ncounter * 2) + 1], NOT_CONNECTED, ope);
                                statusData[oscd.Serie].Add(oscd);
                                lastDate = endRealDate;
                            }
                        }
                    }                   
                }

                bool next = false;
                while (next == false)
                {
                    next = true;
                    DateTime newLastDate = FindSpecificDate(starts, lastDate);
                    if (lastDate < newLastDate && newLastDate < now)
                    {
                        lastDate = newLastDate;
                        endRealDate = FindSpecificDate(ends, lastDate);
                        if (endRealDate > lastDate && endRealDate < now)
                        {
                            ArrayList workdates = new ArrayList();
                            workdates = GetWorkShiftLastDate(ope.WorkShifts, lastDate, endRealDate);
                            int nlimit = (workdates.Count > 0 ? (workdates.Count / 2) : workdates.Count);
                            for (int ncounter = 0; ncounter < nlimit; ncounter++)
                            {
                                if (ope.WorkShifts.Count > 0 && (DateTime)workdates[ncounter * 2] < (DateTime)workdates[(ncounter * 2) + 1] && workdates[ncounter * 2] != workdates[(ncounter * 2) + 1])
                                {
                                    if ((DateTime)workdates[(ncounter * 2) + 1] > now) workdates[(ncounter * 2) + 1] = now;
                                    if ((DateTime)workdates[(ncounter * 2)] <= (DateTime)workdates[(ncounter * 2) + 1] && lastDate <= (DateTime)workdates[(ncounter * 2)])
                                    {
                                        oscd = new OperatorStatusChartData((DateTime)workdates[(ncounter * 2)], (DateTime)workdates[(ncounter * 2) + 1], NOT_CONNECTED, ope);
                                        statusData[oscd.Serie].Add(oscd);
                                        lastDate = endRealDate;
                                        next = false;
                                    }
                                }
                            }
                        }
                    }
                }

                if (lastDate < now)
                {
                    ArrayList workdates = new ArrayList();
                    workdates = GetWorkShiftLastDate(ope.WorkShifts, lastDate, now);
                    int nlimit = (workdates.Count > 0 ? (workdates.Count / 2) : workdates.Count);
                    for (int ncounter = 0; ncounter < nlimit; ncounter++)
                    {
                        if (ope.WorkShifts.Count > 0 && (DateTime)workdates[ncounter * 2] < (DateTime)workdates[(ncounter * 2) + 1] && workdates[ncounter * 2] != workdates[(ncounter * 2) + 1])
                        {
                            lastDate = ((DateTime)workdates[(ncounter * 2)] < oscd.End ? oscd.End : (DateTime)workdates[(ncounter * 2)]);
                            if (lastDate < (DateTime)workdates[(ncounter * 2) + 1] && lastDate >= (DateTime)workdates[(ncounter * 2)])
                            {
                                if ((DateTime)workdates[(ncounter * 2) + 1] > now) workdates[(ncounter * 2) + 1] = now;
                                if (lastDate <= (DateTime)workdates[(ncounter * 2) + 1])
                                {
                                    oscd = new OperatorStatusChartData(lastDate, (DateTime)workdates[(ncounter * 2) + 1], NOT_CONNECTED, ope);
                                    statusData[oscd.Serie].Add(oscd);
                                }
                            }
                        }
                    }
                }
            }
            //If the operator has not been connected in the day.
            if (values.Count == 0 && startDate < now)
            {
                //We have to check if the work shift it not continous, in order to leave the white space.
                DateTime endRealDate = FindSpecificDate(ends, lastDate);
                if (endRealDate > lastDate && endRealDate < now)
                {
                    ArrayList workdates = new ArrayList();
                    workdates = GetWorkShiftLastDate(ope.WorkShifts, lastDate, endRealDate);
                    int nlimit = (workdates.Count > 0 ? (workdates.Count / 2) : workdates.Count);
                    for (int ncounter = 0; ncounter < nlimit; ncounter++)
                    {
                        if (ope.WorkShifts.Count > 0 && (DateTime)workdates[ncounter * 2] < (DateTime)workdates[(ncounter * 2) + 1] && workdates[ncounter * 2] != workdates[(ncounter * 2) + 1])
                        {
                            if ((DateTime)workdates[(ncounter * 2) + 1] <= now && (DateTime)workdates[ncounter * 2] <= (DateTime)workdates[(ncounter * 2) + 1])
                            {
                                oscd = new OperatorStatusChartData((DateTime)workdates[ncounter * 2], (DateTime)workdates[(ncounter * 2) + 1], NOT_CONNECTED, ope);
                                statusData[oscd.Serie].Add(oscd);
                                lastDate = endRealDate;
                            }
                        }
                    }
                }

                bool next = false;
                while (next == false)
                {
                    next = true;
                    DateTime newLastDate = FindSpecificDate(starts, lastDate);
                    if (lastDate < newLastDate && newLastDate < now)
                    {
                        lastDate = newLastDate;
                        endRealDate = FindSpecificDate(ends, lastDate);
                        if (endRealDate > lastDate && endRealDate < now)
                        {
                            ArrayList workdates = new ArrayList();
                            workdates = GetWorkShiftLastDate(ope.WorkShifts, lastDate, endRealDate);
                            int nlimit = (workdates.Count > 0 ? (workdates.Count / 2) : workdates.Count);
                            for (int ncounter = 0; ncounter < nlimit; ncounter++)
                            {
                                if (ope.WorkShifts.Count > 0 && (DateTime)workdates[ncounter * 2] < (DateTime)workdates[(ncounter * 2) + 1] && workdates[ncounter * 2] != workdates[(ncounter * 2) + 1])
                                {
                                    if ((DateTime)workdates[(ncounter * 2) + 1] <= now && (DateTime)workdates[ncounter * 2] <= (DateTime)workdates[(ncounter * 2) + 1])
                                    {
                                        oscd = new OperatorStatusChartData((DateTime)workdates[ncounter * 2], (DateTime)workdates[(ncounter * 2) + 1], NOT_CONNECTED, ope);
                                        statusData[oscd.Serie].Add(oscd);
                                        lastDate = endRealDate;
                                        next = false;
                                    }
                                }
                            }
                        }
                    }
                }

                if (lastDate < now)
                {
                    ArrayList workdates = new ArrayList();
                    workdates = GetWorkShiftLastDate(ope.WorkShifts, lastDate, now);
                    int nlimit = (workdates.Count > 0 ? (workdates.Count / 2) : workdates.Count);
                    for (int ncounter = 0; ncounter < nlimit; ncounter++)
                    {
                        if (ope.WorkShifts.Count > 0 && (DateTime)workdates[ncounter * 2] < (DateTime)workdates[(ncounter * 2) + 1] && workdates[ncounter * 2] != workdates[(ncounter * 2) + 1])
                        {
                            if (now < (DateTime)workdates[(ncounter * 2) + 1]) workdates[(ncounter * 2) + 1] = now;
                            if ((DateTime)workdates[(ncounter * 2) + 1] <= now && (DateTime)workdates[ncounter * 2] <= (DateTime)workdates[(ncounter * 2) + 1])
                            {
                                oscd = new OperatorStatusChartData((DateTime)workdates[ncounter * 2], (DateTime)workdates[(ncounter * 2) + 1], NOT_CONNECTED, ope);
                                statusData[oscd.Serie].Add(oscd);
                            }
                        }
                    }
                }
            }
		}

        private void UpdateTrainingCourseForThisOperator(OperatorClientData ope, IList parts, DateTime now)
        {
            foreach (TrainingCourseSchedulePartsClientData part in parts)
            {
                OperatorStatusChartData oscd = null;
                if (now.Date <= part.Start && part.Start < now)
                {
                    if (part.End <= now)
                        oscd = new OperatorStatusChartData(part.Start, part.End, "Training", ope, part.End);
                    else
                        oscd = new OperatorStatusChartData(part.Start, now, "Training", ope, part.End);
                }
                else if (now.Date > part.Start && part.Start < now && part.End > now.Date)
                {
                    if (part.End <= now)
                        oscd = new OperatorStatusChartData(now.Date, part.End, "Training", ope, part.End);
                    else
                        oscd = new OperatorStatusChartData(now.Date, now, "Training", ope, part.End);
                }
                if (oscd != null)
                {
                    int index = statusData["Training"].IndexOf(oscd);
                    if (index >= 0)
                        statusData["Training"][index].End = oscd.End;
                    else
                        statusData["Training"].Add(oscd);
                    statusData[oscd.Serie].Add(oscd);
                }
            }
        }

        private double[] GetPreviousPercentageChronogram(GridControlDataOperator gridOper)
        {
            double[] result = new double[] {0, 3};
            FormUtil.InvokeRequired(this.gridControlExOperators,
                delegate
                {
                    int index = this.gridControlExOperators.Items.IndexOf(gridOper);
                    if (index != -1)
                    {
                        GridControlDataOperator prev = this.gridControlExOperators.Items[index] as GridControlDataOperator;
                        result = prev.PercentageSchedule;
                    }
                });
            return result;
        }

        public void LoadSessionStatusHistoryChart(object obj)
        {
            while (true)
            {
                try
                {
                    DateTime now = ServerServiceClient.GetInstance().GetTime();

                    FormUtil.InvokeRequired(chartControlOperatorStatuses, delegate
                    {
                        chartControlOperatorStatuses.BeginInit();
                        chartControlOperatorStatuses.ClearCache();

                        statusData = new Dictionary<string, BindingList<OperatorStatusChartData>>();

                        statusData.Add("Busy", new BindingList<OperatorStatusChartData>());
                        statusData.Add("Ready", new BindingList<OperatorStatusChartData>());
                        statusData.Add("Reunion", new BindingList<OperatorStatusChartData>());
                        statusData.Add("Training", new BindingList<OperatorStatusChartData>());
                        statusData.Add("Bathroom", new BindingList<OperatorStatusChartData>());
                        statusData.Add(NOT_CONNECTED, new BindingList<OperatorStatusChartData>());
                        statusData.Add("Absent", new BindingList<OperatorStatusChartData>());
                        statusData.Add("Rest", new BindingList<OperatorStatusChartData>());
                    });
                    FormUtil.InvokeRequired(chartControlOperatorStatuses, delegate
                    {
                        SetNowTimeLine(now);
                    });

                    IList operators = null;
                    if (SelectedDepartmentType != null)
                    {
                        operators = ServerServiceClient.GetInstance().GetSupervisedOperators(
                            supervisedApplicationName, SelectedDepartmentType.Code);
                    }
                    else
                    {
                        operators = ServerServiceClient.GetInstance().GetSupervisedOperators(
                            supervisedApplicationName, -1);
                    }
                    
                    Dictionary<int, double[]> values = new Dictionary<int, double[]>();
                    if (operators.Count == 0)
                        gridControlExOperators.ClearData();
                    else
                    {
                        FormUtil.InvokeRequired(gridControlExOperators,
                        delegate
                        {
                            foreach (GridControlDataOperator item in gridControlExOperators.Items)
                            {
                                foreach (OperatorClientData oper in operators)
                                {
                                    if (oper.Code == item.OperatorClient.Code)
                                    {
                                        gridControlExOperators.BeginUpdate();
                                        item.Tag = oper;
                                        gridControlExOperators.EndUpdate();
                                    }
                                }
                            }
                        });
                    }

                    //operatorSynBox.Sync(operators);
                    operators = GetOperatorsOnGrid();

                    //Variables para buscar los intervalos
                    List<int> operatorsCodes = new List<int>();
                    Dictionary<int, ArrayList> result = new Dictionary<int, ArrayList>();

                    //variables para buscar los status.
                    Dictionary<string, IList> querys = new Dictionary<string, IList>();

                    foreach (GridControlData grid in operators)
                    {
                        try
                        {
                            OperatorClientData ope = grid.Tag as OperatorClientData;
                            operatorsCodes.Add(ope.Code);

                            querys.Add("Stats" + ope.Code.ToString(), new ArrayList());
                            querys["Stats" + ope.Code.ToString()].Add(SmartCadHqls.GetCustomHql(
                                                                  SmartCadHqls.GetOperatorSessionHistoryThatStarts,
                                                                  ope.Code,
                                                                  SupervisedApplicationName,
                                                                  ApplicationUtil.GetDataBaseFormattedDate(now.Date)));

                            querys.Add("Training" + ope.Code.ToString(), new ArrayList());
                            querys["Training" + ope.Code.ToString()].Add(SmartCadHqls.GetCustomHql(
                                SmartCadHqls.TrainingCourseSchedulePartByDateAndOperator,
                                ApplicationUtil.GetDataBaseFormattedDate(now.Date),
                                ApplicationUtil.GetDataBaseFormattedDate(now.Date.AddDays(1)),
                                ope.Code));
                        }
                        catch (Exception e)
                        {
                            SmartLogger.Print(e);
                        }
                    }
                    //We ask for the intervals that the operators should begin to work in the current day.
                    if (CheckLogIn == true && operatorsCodes.Count > 0)
                    {
                        result = (Dictionary<int, ArrayList>)ServerServiceClient.GetInstance().OperatorScheduleManagerMethod("GetOperatorTodaysSchedules", operatorsCodes);
                    }

                    if (querys.Count > 0)
                        querys = ServerServiceClient.GetInstance().SearchClientObjectsMaxRowsMultiQuery(true, querys);

                    foreach (GridControlData grid in operators)
                    {
                        try
                        {
                            OperatorClientData ope = grid.Tag as OperatorClientData;
                            UpdateLastSessionStatusHistory(ope, now, result.ContainsKey(ope.Code) == true ? result[ope.Code] : null, querys.ContainsKey("Stats" + ope.Code.ToString()) == true ? querys["Stats" + ope.Code.ToString()] : new ArrayList());
                            //Adds the training course for an operator.
                            UpdateTrainingCourseForThisOperator(ope, querys["Training" + ope.Code.ToString()], now);
                        }
                        catch (Exception e)
                        {
                            SmartLogger.Print(e);
                        }
                    }
                    FormUtil.InvokeRequired(chartControlOperatorStatuses, delegate
                    {
                        foreach (Series s in chartControlOperatorStatuses.Series)
                        {
                            s.Points.BeginUpdate();
                            s.DataSource = statusData[s.Name];
                            s.Points.EndUpdate();
                            s.ArgumentScaleType = ScaleType.Qualitative;
                            s.ArgumentDataMember = "OperatorName";
                            s.ValueScaleType = ScaleType.DateTime;
                            s.ValueDataMembers.AddRange(new string[] { "Start", "End" });
                            //Thread.Sleep(100);
                        }
                        if (SelectedOperator != null && ((GanttDiagram)chartControlOperatorStatuses.Diagram).AxisX.ConstantLines.Count > 0)
                        {
                            ((GanttDiagram)chartControlOperatorStatuses.Diagram).AxisX.ConstantLines[0].AxisValue = SelectedOperator.FirstName + " " + SelectedOperator.LastName;
                            ((GanttDiagram)chartControlOperatorStatuses.Diagram).AxisX.ConstantLines[0].Visible = true;
                        }
                        else
                        {
                            ((GanttDiagram)chartControlOperatorStatuses.Diagram).AxisX.ConstantLines[0].AxisValue = string.Empty;
                            ((GanttDiagram)chartControlOperatorStatuses.Diagram).AxisX.ConstantLines[0].Visible = false;

                        }
                        try
                        {
                            chartControlOperatorStatuses.EndInit();
                        }
                        catch
                        { }
                    });
                }
                catch
                {
                }
                Thread.Sleep(30000);
            }
        }

		private void SetNowTimeLine(DateTime now)
		{
			if (((GanttDiagram)chartControlOperatorStatuses.Diagram).AxisY.ConstantLines.Count > 0)
			{
				try
				{
					((GanttDiagram)chartControlOperatorStatuses.Diagram).AxisY.ConstantLines[0].AxisValue = now;
					((GanttDiagram)chartControlOperatorStatuses.Diagram).AxisY.ConstantLines[0].Visible = true;
				}
				catch
				{
				}
			}                        
		}

        private void UpdatePercentageChronogram(IndicatorResultClientData indicatorResult, IndicatorResultValuesClientData irvcd)
        {
            if (indicatorResult.IndicatorCustomCode == "PN10" || indicatorResult.IndicatorCustomCode == "D26")
            {
                OperatorClientData oper = new OperatorClientData();
                oper.Code = irvcd.CustomCode;
                GridControlDataOperator gridOperator = new GridControlDataOperator(oper);
                int index = gridControlExOperators.Items.IndexOf(gridOperator);
                if (index != -1)
                {
                    gridOperator = gridControlExOperators.Items[index] as GridControlDataOperator;
                    irvcd.ResultValue = irvcd.ResultValue.Replace(",", ".");
                    gridOperator.PercentageSchedule = new double[] { double.Parse(irvcd.ResultValue), irvcd.Threshold };
                 
                    gridControlExOperators.AddOrUpdateItem(gridOperator);
                    gridControlExOperators.AddOrUpdateRepositoryItemComboBox(gridOperator); // Se esta llamando a parte por q se comento la llamada dentro del AddOrUpdateItem. AA
                }
            }
        }

        private void SwapResultOnDashBoard(IndicatorResultClientData result)
        {
            FormUtil.InvokeRequired(indicatorDashBoardControlSystem, delegate
            {
                if (result.IndicatorTypeName.Equals(IndicatorTypeClientData.Calls.Name))
                    indicatorDashBoardControlSystem.AddOrUpdateItem(result, IndicatorDashBoardControl.IndicatorType.CALLS);
                else if (result.IndicatorTypeName.Equals(IndicatorTypeClientData.FirstLevel.Name))
                    indicatorDashBoardControlSystem.AddOrUpdateItem(result, IndicatorDashBoardControl.IndicatorType.FIRSTLEVEL);
                else if (result.IndicatorTypeName.Equals(IndicatorTypeClientData.Dispatch.Name))
                    indicatorDashBoardControlSystem.AddOrUpdateItem(result, IndicatorDashBoardControl.IndicatorType.DISPATCH);
            });
        }

        private void SwapResultOnDashBoard1(IndicatorResultClientData result)
        {
            FormUtil.InvokeRequired(indicatorDashBoardControlDepartment, delegate
            {
                if (result.IndicatorTypeName.Equals(IndicatorTypeClientData.Calls.Name))
                    indicatorDashBoardControlDepartment.AddOrUpdateItem(result, IndicatorDashBoardControl.IndicatorType.CALLS);
                else if (result.IndicatorTypeName.Equals(IndicatorTypeClientData.FirstLevel.Name))
                    indicatorDashBoardControlDepartment.AddOrUpdateItem(result, IndicatorDashBoardControl.IndicatorType.FIRSTLEVEL);
                else if (result.IndicatorTypeName.Equals(IndicatorTypeClientData.Dispatch.Name))
                    indicatorDashBoardControlDepartment.AddOrUpdateItem(result, IndicatorDashBoardControl.IndicatorType.DISPATCH);
            });
        }

        private void SwapResultOnDashBoard2(IndicatorResultClientData result)
        {
            FormUtil.InvokeRequired(indicatorDashBoardControlGroup, delegate
            {
                if (result.IndicatorTypeName.Equals(IndicatorTypeClientData.Calls.Name))
                    indicatorDashBoardControlGroup.AddOrUpdateItem(result, IndicatorDashBoardControl.IndicatorType.CALLS);
                else if (result.IndicatorTypeName.Equals(IndicatorTypeClientData.FirstLevel.Name))
                    indicatorDashBoardControlGroup.AddOrUpdateItem(result, IndicatorDashBoardControl.IndicatorType.FIRSTLEVEL);
                else if (result.IndicatorTypeName.Equals(IndicatorTypeClientData.Dispatch.Name))
                    indicatorDashBoardControlGroup.AddOrUpdateItem(result, IndicatorDashBoardControl.IndicatorType.DISPATCH);
            });
        }

        private void SwapResultOnDashBoard3(IndicatorResultClientData result)
        {
            FormUtil.InvokeRequired(indicatorDashBoardControlOperator, delegate
            {
                if (result.IndicatorTypeName.Equals(IndicatorTypeClientData.Calls.Name))
                    indicatorDashBoardControlOperator.AddOrUpdateItem(result, IndicatorDashBoardControl.IndicatorType.CALLS);
                else if (result.IndicatorTypeName.Equals(IndicatorTypeClientData.FirstLevel.Name))
                    indicatorDashBoardControlOperator.AddOrUpdateItem(result, IndicatorDashBoardControl.IndicatorType.FIRSTLEVEL);
                else if (result.IndicatorTypeName.Equals(IndicatorTypeClientData.Dispatch.Name))
                    indicatorDashBoardControlOperator.AddOrUpdateItem(result, IndicatorDashBoardControl.IndicatorType.DISPATCH);
            });
        }

        private IList GetOperatorsByWorkshift(WorkShiftVariationClientData wsVariation)
        {
            IList operators = new ArrayList();

            FormUtil.InvokeRequired(this.gridControlExOperators,
                delegate
                {
                    foreach (GridControlData item in gridControlExOperators.Items)
                    {
                        OperatorClientData oper = item.Tag as OperatorClientData;
                        if (oper.WorkShifts != null && oper.WorkShifts.Contains(wsVariation))
                        {
                            operators.Add(oper);
                        }

                    }
                });

            return operators;
        }

        private IList GetOperatorsByRoom(RoomClientData room)
        {
            IList operators = new ArrayList();
            FormUtil.InvokeRequired(this.gridControlExOperators,
                delegate
                {
                    foreach (GridControlDataOperator item in gridControlExOperators.Items)
                    {
                        OperatorClientData oper = item.Tag as OperatorClientData;
                        if (oper.RoomCode == room.Code)
                        {
                            operators.Add(oper);
                        }
                    }
                });
            return operators;
        }

        private IList GetOperatorsByRoomUpdated(RoomClientData room)
        {
            IList operators = new ArrayList();
            FormUtil.InvokeRequired(this.gridControlExOperators,
                delegate
                {
                    string computerName;
                    foreach (GridControlDataOperator grid in gridControlExOperators.Items)
                    {
                        OperatorClientData oper = grid.Tag as OperatorClientData;
                        IList names = (IList)ServerServiceClient.GetInstance().SearchBasicObjects(
                            SmartCadSQL.GetCustomSQL(SmartCadHqls.GetComputerNameByUserLogin, oper.Login));
                        if (names.Count > 0)
                        {
                            computerName = (string)names[0];
                            foreach (RoomSeatClientData seat in room.ListSeats)
                            {
                                if (seat.ComputerName.ToUpper() == computerName.ToUpper())
                                {
                                    operators.Add(oper);
                                }
                            }
                        }
                    }
                });
            return operators;
        }

        private bool CanSuperviseOperatorDepartment(IList operatorDepartments)
        {
            bool result = false;
            if (operatorDepartments != null)
            {
                for (int i = 0; i < operatorDepartments.Count && !result; i++)
                {
                    DepartmentTypeClientData department = (DepartmentTypeClientData)operatorDepartments[i];
                    if (ServerServiceClient.GetInstance().OperatorClient.DepartmentTypes.Contains(department) == true)
                    {
                        result = true;
                    }
                }
            }
            
            return result;
        }

        private IList GetOperatorCodes(IList operatorAssigns)
        {
            IList codes = new ArrayList();
            if (operatorAssigns != null)
            {
                foreach (OperatorAssignClientData assign in operatorAssigns)
                {
                    if (codes.Contains(assign.SupervisedOperatorCode) == false)
                    {
                        codes.Add(assign.SupervisedOperatorCode);
                    }
                }
            }
            return codes;
        }

        private bool OperatorShouldBeConnected(OperatorClientData oper)
        {
            IList args = new ArrayList();
            args.Add(oper.Code);
            return (bool)ServerServiceClient.GetInstance().OperatorScheduleManagerMethod("IsWorkingNow",args );                         
        }

        private void SetSkin()
        {
            try
            {
                skinEngine.AddElement("background", this);
                skinEngine.ApplyChanges();
            }
            catch (Exception ex)
            {
                MessageForm.Show(ResourceLoader.GetString2("$Message.Error"), ResourceLoader.GetString2("ErrorLoadingSkin"), ex);
            }
        }

        private SessionHistoryClientData GetLastSessionHistory(IList lastSessionHistories, string applicationName)
        {
            SessionHistoryClientData sessionHistory = null;
            if (lastSessionHistories != null)
            {
                foreach (SessionHistoryClientData sess in lastSessionHistories)
                {
                    if (sess.UserApplication == applicationName)
                    {
                        sessionHistory = sess;
                        break;
                    }
                }
            }
            return sessionHistory;
        }

        public void LoadData()
        {
            LoadData(false);
        }

        private void LoadData(bool reload)
        {
            //Load logged users
            try
            {
                IList loggedUsers = null;
                if (SelectedDepartmentType != null)
                {
                    loggedUsers = ServerServiceClient.GetInstance().GetSupervisedOperators(
                        supervisedApplicationName, SelectedDepartmentType.Code);
                }
                else
                {
                    loggedUsers = ServerServiceClient.GetInstance().GetSupervisedOperators(
                        supervisedApplicationName, -1);
                }
                if (loggedUsers.Count == 0) gridControlExOperators.ClearData();
                operatorSynBox.Sync(loggedUsers);                
                Application.DoEvents();
            }
            catch (Exception ex)
            {
                MessageForm.Show(ex.Message, ex);
            }
        }

        private void ReloadData()
        {
            if (canReload == true)
            {
                ClearControls();
                LoadData(true);
            }
            else
                canReload = true;
        }

        private void ClearControls()
        {
            FormUtil.InvokeRequired(this,
                delegate
                {
                    if (this.gridControlExOperators.DataSource != null)
                    {
                        (this.gridControlExOperators.DataSource as IList).Clear();
                    }
                    this.chartControlOperatorStatuses.ClearSelection();

                });
        }

        private void LoadIndicatorResults()
        {
            LoadIndicatorsForAGivenClass(GlobalIndicators, IndicatorClassClientData.System);
            LoadIndicatorsForAGivenClass(GlobalIndicators, IndicatorClassClientData.Department);
            LoadIndicatorsForAGivenClass(GlobalIndicators, IndicatorClassClientData.Group);
            LoadIndicatorsForAGivenClass(GlobalIndicators, IndicatorClassClientData.Operator);
        }

        private void LoadIndicatorsForAGivenClass(IList indicators, IndicatorClassClientData currentClass)
        {
            IList operatorIndicator = new ArrayList();
            IList[] dataSources = new ArrayList[3];
            dataSources[0] = new ArrayList();
            dataSources[1] = new ArrayList();
            dataSources[2] = new ArrayList();
            foreach (IndicatorClientData indicator in indicators)
            {
                bool isInDashBoard = false;
                foreach (IndicatorClassDashboardClientData icdcd in indicator.Classes)
                {
                    if (icdcd.ClassName.Equals(currentClass.Name) && icdcd.ShowDashboard == true)
                    {
                        isInDashBoard = true;
                        break;
                    }
                }

                if (isInDashBoard == true)
                {
                    IList results = ServerServiceClient.GetInstance().SearchClientObjects(
                        SmartCadHqls.GetCustomHql(
                            SmartCadHqls.GetLastResultForIndicator,
                            indicator.Code));
                    bool alwaysAdd = false;
                    FormUtil.InvokeRequired(this,
                        delegate
                        {
                            foreach (IndicatorResultClientData res in results)
                            {
                                IndicatorResultClientData toAddRess = new IndicatorResultClientData(res, false);

                                alwaysAdd = false;
                                foreach (IndicatorResultValuesClientData irvcd in res.Values)
                                {
                                    if (irvcd.IndicatorClassName.Equals(currentClass.Name))
                                    {
                                        if (currentClass.Name.Equals(IndicatorClassClientData.System.Name))
                                        {
                                            toAddRess.Values.Add(irvcd);
                                            alwaysAdd = true;
                                            break;
                                        }
                                        else if (currentClass.Name.Equals(IndicatorClassClientData.Department.Name))
                                        {
                                            if (SelectedDepartmentType != null && SelectedDepartmentType.Code == irvcd.CustomCode)
                                            {
                                                toAddRess.Values.Add(irvcd);
                                                break;
                                            }
                                        }
                                        else if (currentClass.Name.Equals(IndicatorClassClientData.Group.Name))
                                        {
                                            if (irvcd.CustomCode.Equals(operatorClientCode))
                                            {
                                                toAddRess.Values.Add(irvcd);
                                                break;
                                            }
                                        }
                                        else if (currentClass.Name.Equals(IndicatorClassClientData.Operator.Name))
                                        {
                                            if (gridControlExOperators.SelectedItems.Count > 0)
                                            {
                                                OperatorClientData oper = ((GridControlDataOperator)gridControlExOperators.SelectedItems[0]).OperatorClient;
                                                if (irvcd.CustomCode == oper.Code)
                                                {

                                                    toAddRess.Values.Add(irvcd);
                                                    //This lines should not be here, this indicators should be fill when the result comes from CommittedChanges.
                                                    //operatorIndicator.Add(irvcd);
                                                    //this.indicatorGridControl1.Load(operatorIndicator);

                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                                if (toAddRess.Values.Count > 0)
                                {
                                    if (indicator.TypeName.Equals(IndicatorTypeClientData.Calls.Name) && (selectedDepartmentType == null || alwaysAdd))
                                    {
                                        dataSources[0].Add(toAddRess);
                                    }
                                    else if (indicator.TypeName.Equals(IndicatorTypeClientData.FirstLevel.Name) && (selectedDepartmentType == null || alwaysAdd))
                                    {
                                        dataSources[1].Add(toAddRess);
                                    }
                                    else if (indicator.TypeName.Equals(IndicatorTypeClientData.Dispatch.Name) && (selectedDepartmentType != null || alwaysAdd))
                                    {
                                        dataSources[2].Add(toAddRess);
                                    }
                                }
                            }
                        });
                }
            }

            if (currentClass.Name.Equals(IndicatorClassClientData.System.Name))
            {
                FormUtil.InvokeRequired(indicatorDashBoardControlSystem, delegate
                {
                    indicatorDashBoardControlSystem.DataSource = dataSources;
                });
            }
            else if (currentClass.Name.Equals(IndicatorClassClientData.Department.Name))
            {
                FormUtil.InvokeRequired(indicatorDashBoardControlDepartment, delegate
                {
                    indicatorDashBoardControlDepartment.DataSource = dataSources;
                });
            }

            else if (currentClass.Name.Equals(IndicatorClassClientData.Group.Name))
            {
                FormUtil.InvokeRequired(indicatorDashBoardControlGroup, delegate
                {
                    indicatorDashBoardControlGroup.DataSource = dataSources;
                });
            }

            else if (currentClass.Name.Equals(IndicatorClassClientData.Operator.Name))
            {
                FormUtil.InvokeRequired(indicatorDashBoardControlOperator, delegate
                {
                    indicatorDashBoardControlOperator.DataSource = dataSources;
                });
            }
        }

        private void RadioButtonOperatorsStatus_Checked(object sender, EventArgs e)
        {
            RadioButton radioButton = sender as RadioButton;
            if (radioButton.Checked)
            {
                if (sender.Equals(this.radioButtonAllOperators))
                {
                    (gridControlExOperators.MainView as GridView).ClearColumnsFilter();
                }
                else if (sender.Equals(this.radioButtonConOperators))
                {
                    DevExpress.Data.Filtering.CriteriaOperator expression = new DevExpress.Data.Filtering.BinaryOperator("OperatorStatus", ResourceLoader.GetString("MSG_NotConnected"),DevExpress.Data.Filtering.BinaryOperatorType.NotEqual);
                    (gridControlExOperators.MainView as GridView).ActiveFilterCriteria = expression;
                }
                else if (sender.Equals(this.radioButtonNoConOperators))
                {
                    DevExpress.Data.Filtering.CriteriaOperator expression = new DevExpress.Data.Filtering.BinaryOperator("OperatorStatus", ResourceLoader.GetString("MSG_NotConnected"), DevExpress.Data.Filtering.BinaryOperatorType.Equal);
                    (gridControlExOperators.MainView as GridView).ActiveFilterCriteria = expression;
                }
                ShowOperatorsInRoom(true);             
            }
        }

        #region Tab Ubicar en Sala

        int createdControls = 0;
        //Cursor currentCursor = null;
        Dictionary<string, Control> controls = new Dictionary<string, Control>();
        Dictionary<string, OperatorClientData> assignSeats = new Dictionary<string, OperatorClientData>();
        Dictionary<string, SelectBorderControl> resizeList = new Dictionary<string, SelectBorderControl>();
        Dictionary<OperatorClientData,Control> operatorControl = new Dictionary<OperatorClientData,Control>();

        public void ShowOperatorsInRoom(bool renderRoom)
        {
            if (barEditItemSelectRoom.Enabled == false && gridControlExOperators.SelectedItems != null && gridControlExOperators.SelectedItems.Count > 0)
            {
                GridControlDataOperator gridData = gridControlExOperators.SelectedItems[0] as GridControlDataOperator;
                OperatorClientData selectedOper = gridData.OperatorClient;

                if (gridData.Status == ResourceLoader.GetString("MSG_NotConnected"))
                {
                    if (barCheckItemViewAll.Checked == true)
                    {

                        panelControlNoRoom.Visible = false;

                        if (renderRoom == true)
                        {
                            RoomClientData selectedRoom = (RoomClientData)barEditItemSelectRoom.EditValue;
                            if (selectedRoom != null && selectedRoom.Image != null)
                            {
                                RenderRoom(selectedRoom);
                                RenderSeats(selectedRoom, gridControlExOperators.Items);
                            }

                            if (operatorControl.Count == 0)
                            {
                                RenderRoomMessage(ResourceLoader.GetString2("NoOperatorsInRoom", ResourceLoader.GetString2(supervisedApplicationName)));
                            }
                        }
                        else
                        {
                            HideAllResizeBoxes();
                        }

                    }
                    else
                    {
                        RenderRoomMessage(ResourceLoader.GetString2("NoConnectedOperatorNoRoom"));
                    }
                }
                else
                {
                    panelControlNoRoom.Visible = false;

                    if (barEditItemSelectRoom.EditValue != null)
                    {
                        if (barCheckItemViewAll.Checked == true)
                        {
                            if (renderRoom == true)
                            {
                                RoomClientData selectedRoom = (RoomClientData)barEditItemSelectRoom.EditValue;
                                RenderRoom(selectedRoom);
                                RenderSeats(selectedRoom, gridControlExOperators.Items);
                            }

                            SelectControl(selectedOper);

                            if (operatorControl.Count == 0)
                            {
                                RenderRoomMessage(ResourceLoader.GetString2("NoOperatorsInRoom", ResourceLoader.GetString2(supervisedApplicationName)));
                            }
                        }
                        else
                        {
                            if (selectedOper.RoomName == null || selectedOper.RoomName == string.Empty)
                            {
                                panelControlNoRoom.Visible = true;
                                RenderRoomMessage(ResourceLoader.GetString2("NoOperatorInRoom"));
                            }
                            else
                            {
                                //IList rooms = ServerServiceClient.GetInstance().SearchClientObjects(
                                //    SmartCadSQL.GetCustomSQL(SmartCadHqls.GetRoomsByName, selectedOper.RoomName));
                                RoomClientData room = null;
                                if (globalRooms.ContainsKey(selectedOper.RoomCode) == true)
                                    room = globalRooms[selectedOper.RoomCode];

                                if (room != null)
                                {
                                    barEditItemSelectRoom.EditValue = room;
                                    barEditItemSelectRoom.EditValueChanged += new EventHandler(barEditItemSelectRoom_EditValueChanged);
                                    barEditItemSelectRoom.ItemClick += new ItemClickEventHandler(barEditItemSelectRoom_ItemClick);
                                    RenderRoom(room);
                                }
                                RenderSeats((RoomClientData)barEditItemSelectRoom.EditValue, gridControlExOperators.SelectedItems);
                                SelectControl(selectedOper);
                            }
                        }
                    }
                    else 
                    {
                        RenderRoomMessage(ResourceLoader.GetString2("NoConnectedOperatorNoRoom"));
                    }
                }
            }
            else if ( renderRoom == true && barEditItemSelectRoom.Enabled == true)
            {
                RoomClientData selectedRoom = (RoomClientData)barEditItemSelectRoom.EditValue;
                if (selectedRoom != null && selectedRoom.Image != null)
                {
                    RenderRoom(selectedRoom);
                    RenderSeats(selectedRoom, gridControlExOperators.Items);
                }
            }
        }

        private void RenderRoomMessage(string message)
        {
            DeleteAllSeats();

            Image im = ResourceLoader.GetImage("NoConnectedNoRoom");
            MemoryStream ms = new MemoryStream();
            im.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            im = GetThumbnailImage(ms.ToArray());
            im.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);

            RoomClientData room = new RoomClientData();
            room.Image = ms.ToArray();
            room.Name = string.Empty;
            room.Description = string.Empty;
            RenderRoom(room);

            panelControlNoRoom.Visible = true;
            labelControlNoRoom.Text = message;
            panelRoomDesing.Visible = false;
        }

        private Dictionary<int, RoomClientData> GetRooms()
        {
            Dictionary<int, RoomClientData> result = new Dictionary<int, RoomClientData>();
            IList listRooms = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetAllRooms);
            
            if (listRooms.Count > 0)
            {
                foreach (RoomClientData room in listRooms)
                {
                    result.Add(room.Code, room);
                }
                
            }

            return result;
        }

        private void FillRoomsRepository()
        {
            RoomClientData selectedRoom = barEditItemSelectRoom.EditValue as RoomClientData;
            repositoryItemComboBoxSelectRoom.Items.Clear();
            barEditItemSelectRoom.EditValue = null;
            
            if (globalRooms.Count > 0)
            {
                bool selected = false;
                foreach (RoomClientData room in globalRooms.Values)
                {
                    repositoryItemComboBoxSelectRoom.Items.Add(room);
                    if (selectedRoom != null && selectedRoom.Code == room.Code)
                    {
                        barEditItemSelectRoom.EditValue = room;
                        selected = true;
                    }                
                }
                repositoryItemComboBoxSelectRoom.Sorted = true;
                if (selected == false)
                {
                    barEditItemSelectRoom.EditValue = repositoryItemComboBoxSelectRoom.Items[0];
                }
            }
        }

        private void panelRoomDesing_MouseClick(object sender, MouseEventArgs e)
        {
            HideAllResizeBoxes();
        }

        private Control CreateControl(Control controlBase)
        {
            Control control = (Control)Activator.CreateInstance(controlBase.GetType());
            control.Name = control.GetType().Name + createdControls.ToString();
            control.Location = controlBase.Location;
            control.ContextMenuStrip = contextMenuStripDesigner;
            createdControls++;
            control.BackColor = System.Drawing.Color.Transparent;

            switch (control.GetType().Name)
            {
                case "PictureBox":
                    PictureBox newPictureBox = control as PictureBox;
                    newPictureBox.AutoSize = false;
                    control.AllowDrop = false;
                    control.MouseMove += new MouseEventHandler(control_MouseMove);
                    control.MouseDown += new MouseEventHandler(control_MouseDown);
                    control.MouseUp += new MouseEventHandler(control_MouseUp);
                    toolTipSeats.SetToolTip(control, "");
                    control.BackColor = Color.Transparent;
                    break;
                case "Label":
                    Label newLabel = control as Label;
                    newLabel.AutoSize = false;
                    break;
                default:
                    break;
            }
            return control;
        }

        private void control_MouseMove(object sender, MouseEventArgs e)
        {
            Control cntrl = sender as Control;
            if (cntrl != null)
            {
                if (controls.ContainsKey(cntrl.Name))
                {
                    OperatorClientData ope = assignSeats[cntrl.Text];
                    toolTipSeats.SetToolTip(cntrl, ope.FirstName + " " + ope.LastName + ", " + ResourceLoader.GetString2("SeatNumber") + " " + cntrl.Text);
                }
            }
        }

        private void control_MouseDown(object sender, MouseEventArgs e)
        {
            Control cntrl = sender as Control;
            if (cntrl != null)
            {
                if (controls.ContainsKey(cntrl.Name))
                {
                    OperatorClientData ope = assignSeats[cntrl.Text];
                    GridControlDataOperator grid = new GridControlDataOperator(ope);
                    if (radioButtonNoConOperators.Checked == true)
                    {
                        radioButtonAllOperators.Checked = true;
                    }
                    gridView1.FocusedRowHandle = gridView1.GetRowHandle(((IList)gridView1.DataSource).IndexOf(grid));
                }

            }
        }

        private void control_MouseUp(object sender, MouseEventArgs e)
        {
            HideAllResizeBoxes();
            Control cntrl = sender as Control;
            if (resizeList.ContainsKey(cntrl.Name))
            {
                SelectBorderControl resizeControl = resizeList[cntrl.Name];
                resizeControl.ShowResizeBoxes();
                resizeControl.ShowResizeBoxes();
            }
            cntrl.Focus();
            cntrl.Select();
        }

        private Image GetThumbnailImage(byte[] data)
        {
            MemoryStream ms = new MemoryStream(data);
            Image image = Image.FromStream(ms);
            Image thumbNailImage = image.GetThumbnailImage(panelRoomDesing.Width - 10, panelRoomDesing.Height - 10, new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallback), IntPtr.Zero);
            Size s = new Size(panelRoomDesing.Width, panelRoomDesing.Height);
            return ApplicationUtil.ResizeImage(image, s, System.Drawing.SystemColors.Control);
        }

        private bool ThumbnailCallback()
        {
            return true;
        }

        private void RenderRoom(RoomClientData room)
        {
            panelRoomDesing.BackgroundImage = GetThumbnailImage(room.Image);
            panelRoomDesing.BackgroundImageLayout = ImageLayout.Center;
            panelRoomDesing.SendToBack();

            Render method = Render.FromString(room.RenderMethod);
            foreach (Group g in method.Groups)
            {
                foreach (ElementRoom f in g.Element)
                {
                    Control control = null;

                    if (f.GuiType == Element.GUITYPE_TEXT)
                    {
                        control = CreateControl(new Label());
                        control.Text = f.Text;
                        control.BackColor = Color.Transparent;
                    }
                    control.Width = int.Parse(f.Width);
                    control.Height = int.Parse(f.Height);
                    control.Location = new Point(int.Parse(f.X), int.Parse(f.Y));
                    control.Update();

                    AddControlToRoomDesignerPanel(control, false, f.Code);
                }
            }
            FormUtil.InvokeRequired(textEditRoomDes, delegate
            {
                textEditRoomName.Text = room.Name;
                textEditRoomDes.Text = room.Description;
            });
        }

        private Dictionary<int, string> GetComputerNames(IList operators) 
        {
            Dictionary<int, string> result = new Dictionary<int, string>();
            StringBuilder builder = new StringBuilder();
            builder.Append("(");

            foreach (OperatorClientData oper in operators)
            {
                builder.Append(oper.Code);
                builder.Append(",");
            }

            builder.Remove(builder.Length - 1, 1);
            builder.Append(")");

            string hql = @"Select uaad.UserAccount.Code, uaad.ComputerName 
                            from SessionHistoryData uaad
                            where uaad.UserAccount.Code IN {0}
                            and uaad.IsLoggedIn = true
                            order by uaad.StartDateLogin desc";

            IList list = (IList)ServerServiceClient.GetInstance().SearchBasicObjects(SmartCadHqls.GetCustomHql(hql, builder.ToString()));

            foreach (object[] item in list)
            {
                if (result.ContainsKey((int)item[0]) == false)
                    result.Add((int)item[0], item[1].ToString());
            }

            return result;
                
        }

        private void RenderSeats(RoomClientData room, IList gridList)
        {
            DeleteAllSeats();
            operatorControl.Clear();
            string computerName = string.Empty;
            IList connectedOperators = new ArrayList();

            foreach (GridControlDataOperator grid in gridList)
            {
                if (grid.Room == room.Name && grid.Status != ResourceLoader.GetString("MSG_NotConnected"))
                    connectedOperators.Add(grid.OperatorClient);
            }

            if (connectedOperators.Count > 0)
            {
                //Obtengo el nombre de las maquinas para usuarios conectados en la aplicacion
                Dictionary<int, string> computerNameList = GetComputerNames(connectedOperators);

                foreach (OperatorClientData oper in connectedOperators)
                {
                    if (computerNameList.Count > 0)
                    {
                        computerName = computerNameList[oper.Code];
                        foreach (RoomSeatClientData seat in room.ListSeats)
                        {
                            if (seat.ComputerName.ToUpper() == computerName.ToUpper())
                            {
                                Control control = CreateControl(new PictureBox());
                                control.Text = seat.SeatName;
                                control.Width = seat.Width;
                                control.Height = seat.Height;
                                control.Location = new Point(seat.X, seat.Y);
                                (control as PictureBox).Image = ResourceLoader.GetImage(room.SeatsImage + "_" + ApplicationUtil.GetOperatorStatus(oper, supervisedApplicationName).StatusName);
                                (control as PictureBox).SizeMode = PictureBoxSizeMode.CenterImage;
                                assignSeats[seat.SeatName] = oper;
                                AddControlToRoomDesignerPanel(control, false, seat.Code);

                                operatorControl.Add(oper, control);
                            }
                        }
                    }
                }
            }
        }

        private void AddControlToRoomDesignerPanel(Control control, bool adjustLocation, int code)
        {
            if (controls.ContainsKey(control.Name) == false)
            {
                controls.Add(control.Name, control);
                this.panelRoomDesing.Controls.Add(control);
                
                if (adjustLocation == true)
                    control.Location = new Point(control.Location.X - panelRoomDesing.Location.X, control.Location.Y - panelRoomDesing.Location.Y);

                control.BringToFront();
                resizeList.Add(control.Name, new SelectBorderControl(control, false));
            }
        }

        private void HideAllResizeBoxes()
        {
            foreach (SelectBorderControl var in resizeList.Values)
            {
                var.HideResizeBoxes();
            }
        }

        private void DeleteAllSeats()
        {
            HideAllResizeBoxes();
            panelRoomDesing.Controls.Clear();
            controls.Clear();
            assignSeats.Clear();
            resizeList.Clear();            
            createdControls = 0;
        }

        private string GetSessionStatusName(string statusFriendlyName)
        {
            IList status = ServerServiceClient.GetInstance().SearchClientObjects(
                SmartCadSQL.GetCustomSQL(SmartCadHqls.GetOperatorStatusByFriendlyName, statusFriendlyName));
            if (status.Count > 0)
            {
                return ((OperatorStatusClientData)status[0]).Name;
            }
            else
            {
                return string.Empty;
            }
        }

        #endregion

        #region Tab Actividades del operador

        //YT: Estos metodos no se estaban usando.
        //private IList GetWorkShiftDates(int day, OperatorClientData supervisedOperator )
        //{
        //    IList dates = new ArrayList();
        //    foreach (WorkShiftScheduleClientData schedule in supervisedOperator.WorkShift.Schedules)
        //    {
        //        BitArray scheduleDays = GetScheduleDays(schedule);
        //        if (scheduleDays[day-1] == true)
        //        {
        //            dates.Add(schedule.Start);
        //        }
        //    }
        //    return dates;
        //}

        //private BitArray GetScheduleDays(WorkShiftScheduleClientData schedule)
        //{
        //    bool[] scheduleDays = new bool[] {
        //                                schedule.Monday, 
        //                                schedule.Tuesday, 
        //                                schedule.Wednesday, 
        //                                schedule.Thursday,
        //                                schedule.Friday, 
        //                                schedule.Saturday, 
        //                                schedule.Sunday
        //                                };
        //    return new BitArray(scheduleDays);
        //}

        public void ShowOperatorActivitiesTabPage()
        {
            this.tabbedControlGroupMonitorActivities.SelectedTabPage = this.layoutControlGroupOperatorActivities;
        }

        public void ShowRoomTabPage()
        {
            tabbedControlGroupMonitorActivities.SelectedTabPage = this.layoutControlGroupRoom;
        }
    
        private SessionStatusHistoryClientData GetLastSessionStatusHistory(OperatorClientData oper)
        {
            SessionStatusHistoryClientData result = null;

            if (oper.LastSessions.Count > 0)
            {
                SessionHistoryClientData sscd = oper.LastSessions[0] as SessionHistoryClientData;

                if (sscd.StatusHistoryList.Count > 1)
                {
                    ((ArrayList)sscd.StatusHistoryList).Sort((IComparer)new SessionStatusHistorySort());

                    SessionStatusHistoryClientData sessionStatus = (SessionStatusHistoryClientData)sscd.StatusHistoryList[sscd.StatusHistoryList.Count - 1];
                    result = sessionStatus;
                }
                else if (sscd.StatusHistoryList.Count == 1)
                {
                    result = (SessionStatusHistoryClientData)sscd.StatusHistoryList[0];
                }
            }
            return result;
        }

        #endregion

        private void barButtonItemOperatorActivities_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.tabbedControlGroupMonitorActivities.SelectedTabPage = this.layoutControlGroupOperatorActivities;
        }

        private void BarButtonItemSelectRoom_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            tabbedControlGroupMonitorActivities.SelectedTabPage = this.layoutControlGroupRoom;
        }

        private void barButtonItemSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (tabbedControlGroupMonitorActivities.SelectedTabPage != null &&
                    tabbedControlGroupMonitorActivities.SelectedTabPage.Equals(this.layoutControlGroupOperatorActivities))
                {
                    SaveFileDialog saveFileDialog = new SaveFileDialog();
                    saveFileDialog.DefaultExt = "jpg";
                    saveFileDialog.Filter = "jpeg files (*.jpg)|*.jpg";
                    DialogResult resSave = saveFileDialog.ShowDialog();
                    if (resSave == DialogResult.OK && !string.IsNullOrEmpty(saveFileDialog.FileName))
                    {
                        if (saveFileDialog.FilterIndex == 1)
                        {
                            chartControlOperatorStatuses.ExportToImage(saveFileDialog.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageForm.Show(ex.Message, ex);
            }
        }

        private void barButtonItemPrint_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (this.tabbedControlGroupMonitorActivities.SelectedTabPage != null &&
                    tabbedControlGroupMonitorActivities.SelectedTabPage.Equals(this.layoutControlGroupOperatorActivities))
            {
                if (chartControlOperatorStatuses.IsPrintingAvailable)
                {
                    chartControlOperatorStatuses.OptionsPrint.SizeMode = DevExpress.XtraCharts.Printing.PrintSizeMode.Stretch;
                    printableComponentLink.PrintDlg();
                }                
            }
        }

        private void barButtonItemRefreshGraphic_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            
        }

        private void barCheckItemViewSelected_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BarCheckItem checkItem = e.Item as BarCheckItem;
            if (checkItem != null && checkItem.Checked)
            {
                gridControlExOperators_SelectionWillChange(null, null);
                barEditItemSelectRoom.Enabled = false;
                barEditItemSelectRoom_EditValueChanged(null, null);
            }
        }

        private void barCheckItemViewAll_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BarCheckItem checkItem = e.Item as BarCheckItem;
            if (checkItem != null && checkItem.Checked && barEditItemSelectRoom.EditValue != null)
            {
                barEditItemSelectRoom.Enabled = true;
                barEditItemSelectRoom_EditValueChanged(null, null);
            }
        }

        private void barEditItemSelectRoom_ItemClick(object sender, ItemClickEventArgs e)
        {
            FormUtil.InvokeRequired(this,
            delegate
            {
                ShowOperatorsInRoom(true);
            });
        }

        public void ShowPrimaryIndicators(bool show)
        {
            if (show == true)
            {   
                layoutControlItemGridPerformance.Visibility = LayoutVisibility.Never;
                layoutControlItemChartControl.Visibility = LayoutVisibility.Never;
                layoutControlItemDashboardOperator.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
                layoutControlItemDashboardOperator.Size = new Size(611, 330);
                layoutControlItemDashboardOperator.MinSize = new Size(611, 330);
                layoutControlItemDashboardOperator.MaxSize = new Size(611, 330);
                layoutControlGroupDashBoard.Visibility = LayoutVisibility.Always;
                emptySpaceItemDashBoardLeft.Width = emptySpaceItemDashBoardRight.Width = ((int)((emptySpaceItemDashBoardLeft.Width + emptySpaceItemDashBoardRight.Width) / 2));
            }
            else 
            {
                layoutControlGroupDashBoard.Visibility = LayoutVisibility.Never;
                layoutControlItemChartControl.Visibility = LayoutVisibility.Never;
                layoutControlItemGridPerformance.Visibility = LayoutVisibility.Always;
             
            }

        }

        private void barEditItemSelectRoom_EditValueChanged(object sender, EventArgs e)
        {
            barEditItemSelectRoom_ItemClick(null, null);
        }

        public void ShowPerformanceOperatorTabPage()
        {
            tabbedControlGroupMonitorActivities.SelectedTabPage = this.layoutControlGroupOperatorPerformance;
        }

        private void barButtonItemOperatorPerformance_ItemClick(object sender, ItemClickEventArgs e)
        {
            tabbedControlGroupMonitorActivities.SelectedTabPage = layoutControlGroupOperatorPerformance;
        }

        private void repositoryItemRadioGroup1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender != null)
            {
                RadioGroup rg = sender as RadioGroup;

                if (rg.SelectedIndex != 0)
                    this.barSubItemOperatorChartControl.Enabled = true;
                else
                    barSubItemOperatorChartControl.Enabled = false;


                this.ShowPrimaryIndicators(rg.SelectedIndex == 0);
                this.Focus();
                tabbedControlGroupMonitorActivities.SelectedTabPage = this.layoutControlGroupOperatorPerformance;
            }
        }

        private bool EnsureLogout()
        {
            DialogResult dialogResult = MessageForm.Show(ResourceLoader.GetString2("$Message.ExitApplication"), MessageFormType.Question);

            return dialogResult == DialogResult.Yes;
        }

        private void DefaultSupervisionForm_FormClosing(object sender, FormClosingEventArgs e)
        {           
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
            }
            else
            {
                if (EnsureLogout())
                {
                    if ((this.MdiParent as SupervisionForm) != null)
                    {
                        (this.MdiParent as SupervisionForm).EnsuredLogout = true;
                        (this.MdiParent as SupervisionForm).SupervisionCommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(DefaultSupervisionForm_SupervisionCommittedChanges);
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

		ToolTipController toolTipController1 = new ToolTipController();

        private bool updatingChartTooltip = false;
		private void ChartControlOperatorStatuses_MouseMove(object sender, MouseEventArgs e)
		{
            try
            {
                if (updatingChartTooltip == false)
                {
                    updatingChartTooltip = true;
                    // Obtain hit information under the test point.
                    ChartHitInfo hi = chartControlOperatorStatuses.CalcHitInfo(e.X, e.Y);
                    if (hi != null)
                    {
                        // Obtain the series point under the test point.
                        SeriesPoint point = hi.SeriesPoint;
                        // Check whether the series point was clicked or not.
                        if (point != null)
                        {
                            // Obtain the series point argument.
                            // Obtain series point values.
                            string values = ResourceLoader.GetString2("Start") + ": " + point.DateTimeValues[0].ToString("HH:mm:ss");
                            if (point.Values.Length > 1)
                            {
                                for (int i = 1; i < point.Values.Length; i++)
                                {
                                    values = values + " " + ResourceLoader.GetString2("End") + ": " + point.DateTimeValues[i].ToString("HH:mm:ss");
                                }
                            }
                            string total = ResourceLoader.GetString2("Total") + ": ";
                            TimeSpan totalTime = point.DateTimeValues[1].Subtract(point.DateTimeValues[0]);
                            total += totalTime.ToString();
                            total = total.Substring(0, 15);
                            string serieName = string.Empty;
                            foreach (string serie in statusData.Keys)
                            {
                                BindingList<OperatorStatusChartData> list = statusData[serie];
                                OperatorClientData ope = new OperatorClientData();
                                string[] names = point.Argument.Split(' ');
                                if (names.Length > 1)
                                {
                                    ope.FirstName = names[0];
                                    ope.LastName = names[1];

                                    if (list.Contains(new OperatorStatusChartData(point.DateTimeValues[0], point.DateTimeValues[1], serie, ope)))
                                    {
                                        serieName = ResourceLoader.GetString2("Status") + ": " + chartControlOperatorStatuses.Series[serie].LegendText;
                                    }
                                }
                            }
                            if (serieName != string.Empty)
                            {
                                // Show the tooltip.
                                toolTipController1.AutoPopDelay = 2500;
                                toolTipController1.ShowHint(values + "\n" + total, serieName);
                                //toolTipController1.HideHint();
                            }
                        }
                        else
                        {
                            // Hide the tooltip.
                            toolTipController1.HideHint();
                        }
                    }
                    else
                    {
                        toolTipController1.HideHint();
                    }
                }
            }
            catch
            { }
            finally
            {
                updatingChartTooltip = false;
            }
		}

        private void BarButtonItemSendMonitor_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                if (this.tabbedControlGroupMonitorActivities.SelectedTabPage != null)
                {
                    if (chartControlOperatorStatuses.IsPrintingAvailable)
                    {
                        chartControlOperatorStatuses.OptionsPrint.SizeMode = DevExpress.XtraCharts.Printing.PrintSizeMode.Stretch;

                        string fileName = ResourceLoader.GetString2("OperatorsChart");
                        string fileDirectory = SmartCadConfiguration.DistFolder;
                        if (!Directory.Exists(fileDirectory))
                        {
                            Directory.CreateDirectory(fileDirectory);
                        }
                        string filePath = fileDirectory + fileName + ".pdf";
                        if (File.Exists(filePath))
                        {
                            File.Delete(filePath);
                        }

                        PdfExportOptions pdfeo = new PdfExportOptions();
                        chartControlOperatorStatuses.ExportToPdf(filePath);

                        if (chartControlOperatorStatuses.Series[0].DataSource != null)
                        {
                            MAPI mapi = new MAPI();
                            mapi.AddAttachment(filePath);
                            mapi.SendMailPopup(ResourceLoader.GetString2("OperatorsChart"), "");
                        }
                        else
                        {
                            MessageForm.Show(ResourceLoader.GetString2("ChartIsNotLoaded"), MessageFormType.Information);
                        }
                    }
                   
                }
            }
            catch (Exception ex)
            {
                MessageForm.Show(ex.Message, ex);
            }
        }

		private void alertControl1_AlertClick(object sender, DevExpress.XtraBars.Alerter.AlertClickEventArgs e)
		{

			(MdiParent as SupervisionForm).operatorChatForm.WindowState = FormWindowState.Normal;
			(MdiParent as SupervisionForm).operatorChatForm.Activate();
		}

		private void barButtonItemRemoteControl_ItemClick(object sender, ItemClickEventArgs e)
		{
			bool find = false;
			if (((SupervisionForm)this.MdiParent).remoteControlFormList == null)
			{
				((SupervisionForm)this.MdiParent).remoteControlFormList = new List<RemoteControlForm>();
			}
            IList sessionsOp = new ArrayList();
            if (SelectedOperator != null)
                sessionsOp = new ArrayList(SelectedOperator.LastSessions);
			((ArrayList)sessionsOp).Sort((IComparer)new SessionHistorySort());

            if (sessionsOp.Count > 0)
            {
                SessionHistoryClientData lastSessionOp = (SessionHistoryClientData)sessionsOp[sessionsOp.Count - 1];

                IList sessionsSup = new ArrayList(ServerServiceClient.GetInstance().OperatorClient.LastSessions);
                ((ArrayList)sessionsSup).Sort((IComparer)new SessionHistorySort());

                SessionHistoryClientData lastSessionSup = (SessionHistoryClientData)sessionsSup[sessionsSup.Count - 1];

                if (lastSessionOp.ComputerName.Equals(lastSessionSup.ComputerName) == false)
                {
                    foreach (RemoteControlForm rcf in ((SupervisionForm)this.MdiParent).remoteControlFormList)
                    {
                        if (rcf.Oper.Code == selectedOperator.Code)
                        {
                            rcf.Focus();
                            find = true;
                            break;
                        }
                    }

                    if (find == false)
                    {
                        try
                        {
                            RemoteControlForm remoteControlForm = new RemoteControlForm(selectedOperator, supervisedApplicationName);
                            remoteControlForm.MdiParent = this.MdiParent;
                            remoteControlForm.Activate();
                            remoteControlForm.Show();

                            ((SupervisionForm)this.MdiParent).remoteControlFormList.Add(remoteControlForm);
                        }
                        catch { }
                    }
                }
                else
                    MessageForm.Show(ResourceLoader.GetString2("UnableMonitorOperatorSameMachineSupervisor"), MessageFormType.Information);
            }
            else
            { // hubo un error de foco. se ignora.
                barButtonItemRemoteControl.Enabled = false;
            }
		}

		private void barButtonItemChat_ItemClick(object sender, ItemClickEventArgs e)
		{
			(this.MdiParent as SupervisionForm).OpenChatForm();
		}

        private void labelControlAbsent_Click(object sender, EventArgs e)
        {

        }

        private void tabbedControlGroupMonitorActivities_SelectedPageChanged(object sender, DevExpress.XtraLayout.LayoutTabPageChangedEventArgs e)
        {
            if (e.Page == this.layoutControlGroupOperatorActivities)
            {
                if (gridView1.RowCount > 0)
                {
                    barButtonItemSave.Enabled = true;
                    barButtonItemPrint.Enabled = true;
                    barButtonItemRefreshGraphic.Enabled = true;
                }
                BarButtonItemSendMonitor.Enabled = true;
            }
            else
            {
                barButtonItemSave.Enabled = false;
                barButtonItemPrint.Enabled = false;
                barButtonItemRefreshGraphic.Enabled = false;
                BarButtonItemSendMonitor.Enabled = false;

            }
            if (e.Page == this.layoutControlGroupRoom)
            {
                emptySpaceItemRoomLeft.Width = emptySpaceItemRoomRight.Width = ((int)((emptySpaceItemRoomLeft.Width + emptySpaceItemRoomRight.Width) / 2)); 
               
                FormUtil.InvokeRequired(this,
                delegate
                {
                    barCheckItemViewSelected.Enabled = true;
                    barCheckItemViewAll.Enabled = true;
                    barEditItemSelectRoom.Enabled = true;
                    barEditItemSelectRoom.EditValueChanged += new EventHandler(barEditItemSelectRoom_EditValueChanged);
                    barEditItemSelectRoom.ItemClick += new ItemClickEventHandler(barEditItemSelectRoom_ItemClick);
                    ShowOperatorsInRoom(true);
                });
            }
            else
            {
                barCheckItemViewSelected.Enabled = false;
                barCheckItemViewAll.Enabled = false;
                barEditItemSelectRoom.Enabled = false;
            }
        }

        public ArrayList GetWorkShiftLastDate(IList workshift, DateTime startdate, DateTime enddate)
        {
            DateTime now = ServerServiceClient.GetInstance().GetTime();
            ArrayList retval = new ArrayList();
            if (workshift.Count > 0)
            {   
                foreach (WorkShiftVariationClientData wsv in workshift)
                {
                    wsv.Operators = (IList)ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorsByWorkShiftVariation, wsv.Code));
                    foreach (WorkShiftScheduleVariationClientData wssv in wsv.Schedules)
                    {
                        if (wssv.Start.Date == startdate.Date)
                        {
                            if (isGeneralSupervisor == true)
                            {
                                retval.Add(wssv.Start);
                                retval.Add(wssv.End);
                            }
                            else
                            {
                                foreach (OperatorClientData ope in wsv.Operators)
                                {
                                    if (ope.Code == operatorClientCode)
                                    {
                                        retval.Add(wssv.Start);
                                        retval.Add(wssv.End);
                                    }
                                }
                            }

                        }
                    }
                }
            }
            return retval;
        }
    }

    public class SessionHistorySort : IComparer
    {
		int IComparer.Compare(object a, object b)
		{
			SessionHistoryClientData ssha = (SessionHistoryClientData)a;
			SessionHistoryClientData sshb = (SessionHistoryClientData)b;

			if (ssha.StartDateLogin.HasValue == true && sshb.StartDateLogin.HasValue == true)
			{
				if (ssha.StartDateLogin.Value > sshb.StartDateLogin.Value)
					return 1;
				else if (ssha.StartDateLogin.Value < sshb.StartDateLogin.Value)
					return -1;
				else
					return 0;
			}
			return 0;
		}
    }
	
	public class SessionStatusHistorySort : IComparer
	{
		int IComparer.Compare(object a, object b)
		{
			SessionStatusHistoryClientData ssha = (SessionStatusHistoryClientData)a;
			SessionStatusHistoryClientData sshb = (SessionStatusHistoryClientData)b;

			if (ssha.EndDateStatus > sshb.EndDateStatus)
				return 1;
			else if (ssha.EndDateStatus < sshb.EndDateStatus)
				return -1;
			else
				return 0;
		}
	}
}
