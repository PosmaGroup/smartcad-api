using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraScheduler;
using SmartCadCore.Common;


namespace SmartCadGuiCommon
{
    public partial class AppointmentForm : Form
    {
        public AppointmentForm()
        {
            InitializeComponent();
            LoadLanguage();
        }

        public AppointmentForm(Appointment appointment): this()
        {
            this.textName.Text = appointment.Subject;
            
            if (appointment.LabelId == 0)
                this.Icon = ResourceLoader.GetIcon("$Icon.WorkShift");
            else if (appointment.LabelId == 1)
                this.Icon = ResourceLoader.GetIcon("$Icon.ExtraTime");
            else if (appointment.LabelId == 2)
                this.Icon = ResourceLoader.GetIcon("$Icon.VacationOrPermission");

            if ((int)appointment.CustomFields["AType"] == 0)
                this.textType.Text = ResourceLoader.GetString2("TypeWorkShift");
            else if ((int)appointment.CustomFields["AType"] == 1)
                this.textType.Text = ResourceLoader.GetString2("ExtraTime");
            else if ((int)appointment.CustomFields["AType"] == 2)
                this.textType.Text = ResourceLoader.GetString2("VacationsAndPermissions");
        
            this.textBoxDescription.Text = appointment.Description;
            this.textDateStart.Text = appointment.Start.ToShortDateString();
            this.textTimeStart.Text = appointment.Start.ToString("HH:mm");
            this.textDateEnd.Text = appointment.End.ToShortDateString();
            this.textTimeEnd.Text = appointment.End.ToString("HH:mm");

        }

        private void LoadLanguage() 
        {
            Text = ResourceLoader.GetString2("Details");
          labelDateEnd.Text = ResourceLoader.GetString2("EndDate") + ":";
          labelDateStart.Text = ResourceLoader.GetString2("StartDate") + ":";
          labelDescription.Text = ResourceLoader.GetString2("Description") + ":";
          labelImage.Text = ResourceLoader.GetString2("Type") + ":";
          labelName.Text = ResourceLoader.GetString2("Name") + ":";
        } 

        private void AppointmentForm_Load(object sender, EventArgs e)
        {

        }

    }
}