
using SmartCadControls;
using SmartCadCore.Common;
namespace SmartCadGuiCommon
{
    partial class OperatorAssignRecurrenceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OperatorAssignRecurrenceForm));
            this.layoutControlRecurrenceForm = new DevExpress.XtraLayout.LayoutControl();
            this.checkEditSelectAll = new DevExpress.XtraEditors.CheckEdit();
            this.radioButtonAll = new System.Windows.Forms.RadioButton();
            this.checkedComboBoxEditDaysOfWeek = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonAccept = new DevExpress.XtraEditors.SimpleButton();
            this.gridControlExAssignment = new GridControlEx();
            this.gridViewExAssignment = new GridViewEx();
            this.radioButtonDaysOfWeek = new System.Windows.Forms.RadioButton();
            this.radioButtonDay = new System.Windows.Forms.RadioButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupAssignment = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlRecurrenceForm)).BeginInit();
            this.layoutControlRecurrenceForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSelectAll.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkedComboBoxEditDaysOfWeek.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExAssignment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExAssignment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupAssignment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControlRecurrenceForm
            // 
            this.layoutControlRecurrenceForm.AllowCustomizationMenu = false;
            this.layoutControlRecurrenceForm.Appearance.DisabledLayoutGroupCaption.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControlRecurrenceForm.Appearance.DisabledLayoutGroupCaption.Options.UseForeColor = true;
            this.layoutControlRecurrenceForm.Appearance.DisabledLayoutItem.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControlRecurrenceForm.Appearance.DisabledLayoutItem.Options.UseForeColor = true;
            this.layoutControlRecurrenceForm.Controls.Add(this.checkEditSelectAll);
            this.layoutControlRecurrenceForm.Controls.Add(this.radioButtonAll);
            this.layoutControlRecurrenceForm.Controls.Add(this.checkedComboBoxEditDaysOfWeek);
            this.layoutControlRecurrenceForm.Controls.Add(this.simpleButtonCancel);
            this.layoutControlRecurrenceForm.Controls.Add(this.simpleButtonAccept);
            this.layoutControlRecurrenceForm.Controls.Add(this.gridControlExAssignment);
            this.layoutControlRecurrenceForm.Controls.Add(this.radioButtonDaysOfWeek);
            this.layoutControlRecurrenceForm.Controls.Add(this.radioButtonDay);
            this.layoutControlRecurrenceForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlRecurrenceForm.Location = new System.Drawing.Point(0, 0);
            this.layoutControlRecurrenceForm.Name = "layoutControlRecurrenceForm";
            this.layoutControlRecurrenceForm.Root = this.layoutControlGroup1;
            this.layoutControlRecurrenceForm.Size = new System.Drawing.Size(462, 486);
            this.layoutControlRecurrenceForm.TabIndex = 0;
            this.layoutControlRecurrenceForm.Text = "layoutControl1";
            // 
            // checkEditSelectAll
            // 
            this.checkEditSelectAll.Location = new System.Drawing.Point(12, 420);
            this.checkEditSelectAll.Name = "checkEditSelectAll";
            this.checkEditSelectAll.Properties.Caption = ResourceLoader.GetString2("SelectAll");
            this.checkEditSelectAll.Size = new System.Drawing.Size(439, 19);
            this.checkEditSelectAll.StyleController = this.layoutControlRecurrenceForm;
            this.checkEditSelectAll.TabIndex = 12;
            this.checkEditSelectAll.CheckedChanged += new System.EventHandler(this.checkEditSelectAll_CheckedChanged);
            // 
            // radioButtonAll
            // 
            this.radioButtonAll.Location = new System.Drawing.Point(12, 26);
            this.radioButtonAll.Name = "radioButtonAll";
            this.radioButtonAll.Size = new System.Drawing.Size(62, 28);
            this.radioButtonAll.TabIndex = 11;
            this.radioButtonAll.TabStop = true;
            this.radioButtonAll.Text = "Todos";
            this.radioButtonAll.UseVisualStyleBackColor = true;
            this.radioButtonAll.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // checkedComboBoxEditDaysOfWeek
            // 
            this.checkedComboBoxEditDaysOfWeek.Enabled = false;
            this.checkedComboBoxEditDaysOfWeek.Location = new System.Drawing.Point(239, 30);
            this.checkedComboBoxEditDaysOfWeek.Name = "checkedComboBoxEditDaysOfWeek";
            this.checkedComboBoxEditDaysOfWeek.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.checkedComboBoxEditDaysOfWeek.Properties.ShowAllItemCaption = "(Todos)";
            this.checkedComboBoxEditDaysOfWeek.Properties.ShowAllItemVisible = false;
            this.checkedComboBoxEditDaysOfWeek.Properties.ShowButtons = false;
            this.checkedComboBoxEditDaysOfWeek.Properties.ShowPopupCloseButton = false;
            this.checkedComboBoxEditDaysOfWeek.Size = new System.Drawing.Size(212, 20);
            this.checkedComboBoxEditDaysOfWeek.StyleController = this.layoutControlRecurrenceForm;
            this.checkedComboBoxEditDaysOfWeek.TabIndex = 10;
            this.checkedComboBoxEditDaysOfWeek.EditValueChanged += new System.EventHandler(this.checkedComboBoxEditDaysOfWeek_EditValueChanged);
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(381, 455);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(75, 25);
            this.simpleButtonCancel.StyleController = this.layoutControlRecurrenceForm;
            this.simpleButtonCancel.TabIndex = 9;
            this.simpleButtonCancel.Text = "simpleButtonCancel";
            // 
            // simpleButtonAccept
            // 
            this.simpleButtonAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButtonAccept.Enabled = false;
            this.simpleButtonAccept.Location = new System.Drawing.Point(295, 455);
            this.simpleButtonAccept.Name = "simpleButtonAccept";
            this.simpleButtonAccept.Size = new System.Drawing.Size(75, 25);
            this.simpleButtonAccept.StyleController = this.layoutControlRecurrenceForm;
            this.simpleButtonAccept.TabIndex = 8;
            this.simpleButtonAccept.Text = "simpleButtonAccept";
            this.simpleButtonAccept.Click += new System.EventHandler(this.simpleButtonAccept_Click);
            // 
            // gridControlExAssignment
            // 
            this.gridControlExAssignment.EnableAutoFilter = true;
            this.gridControlExAssignment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExAssignment.Location = new System.Drawing.Point(9, 58);
            this.gridControlExAssignment.MainView = this.gridViewExAssignment;
            this.gridControlExAssignment.Name = "gridControlExAssignment";
            this.gridControlExAssignment.Size = new System.Drawing.Size(445, 354);
            this.gridControlExAssignment.TabIndex = 7;
            this.gridControlExAssignment.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExAssignment});
            this.gridControlExAssignment.ViewTotalRows = true;
            // 
            // gridViewExAssignment
            // 
            this.gridViewExAssignment.AllowFocusedRowChanged = true;
            this.gridViewExAssignment.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExAssignment.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExAssignment.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExAssignment.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExAssignment.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewExAssignment.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewExAssignment.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExAssignment.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExAssignment.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExAssignment.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExAssignment.EnablePreviewLineForFocusedRow = false;
            this.gridViewExAssignment.GridControl = this.gridControlExAssignment;
            this.gridViewExAssignment.Name = "gridViewExAssignment";
            this.gridViewExAssignment.OptionsMenu.EnableFooterMenu = false;
            this.gridViewExAssignment.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExAssignment.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExAssignment.OptionsView.ShowFooter = true;
            this.gridViewExAssignment.OptionsView.ShowGroupPanel = false;
            this.gridViewExAssignment.ViewTotalRows = true;
            this.gridViewExAssignment.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewExAssignment_CellValueChanging);
            // 
            // radioButtonDaysOfWeek
            // 
            this.radioButtonDaysOfWeek.Location = new System.Drawing.Point(148, 26);
            this.radioButtonDaysOfWeek.Name = "radioButtonDaysOfWeek";
            this.radioButtonDaysOfWeek.Size = new System.Drawing.Size(88, 28);
            this.radioButtonDaysOfWeek.TabIndex = 5;
            this.radioButtonDaysOfWeek.TabStop = true;
            this.radioButtonDaysOfWeek.Text = "Dia/Semana";
            this.radioButtonDaysOfWeek.UseVisualStyleBackColor = true;
            this.radioButtonDaysOfWeek.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // radioButtonDay
            // 
            this.radioButtonDay.Location = new System.Drawing.Point(85, 26);
            this.radioButtonDay.Name = "radioButtonDay";
            this.radioButtonDay.Size = new System.Drawing.Size(52, 28);
            this.radioButtonDay.TabIndex = 4;
            this.radioButtonDay.TabStop = true;
            this.radioButtonDay.Text = "Dia";
            this.radioButtonDay.UseVisualStyleBackColor = true;
            this.radioButtonDay.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.emptySpaceItem1,
            this.layoutControlGroupAssignment});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(462, 486);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.simpleButtonAccept;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(288, 448);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.simpleButtonCancel;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(374, 448);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 448);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(288, 36);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupAssignment
            // 
            this.layoutControlGroupAssignment.CustomizationFormText = "layoutControlGroupAssignment";
            this.layoutControlGroupAssignment.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem7,
            this.layoutControlItem8});
            this.layoutControlGroupAssignment.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupAssignment.Name = "layoutControlGroupAssignment";
            this.layoutControlGroupAssignment.Size = new System.Drawing.Size(460, 448);
            this.layoutControlGroupAssignment.Spacing = new DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4);
            this.layoutControlGroupAssignment.Text = "layoutControlGroupAssignment";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.gridControlExAssignment;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 31);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem4.Size = new System.Drawing.Size(450, 359);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.radioButtonDay;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(73, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(63, 31);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(63, 31);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 1, 1);
            this.layoutControlItem1.Size = new System.Drawing.Size(63, 31);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.radioButtonDaysOfWeek;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(136, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(95, 31);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(95, 31);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 1, 1, 1);
            this.layoutControlItem2.Size = new System.Drawing.Size(95, 31);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.checkedComboBoxEditDaysOfWeek;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(231, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 5, 5, 5);
            this.layoutControlItem3.Size = new System.Drawing.Size(219, 31);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.radioButtonAll;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(73, 31);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(73, 31);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 1, 1);
            this.layoutControlItem7.Size = new System.Drawing.Size(73, 31);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.checkEditSelectAll;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 390);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(450, 30);
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // OperatorAssignRecurrenceForm
            // 
            this.AcceptButton = this.simpleButtonAccept;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonCancel;
            this.ClientSize = new System.Drawing.Size(462, 486);
            this.Controls.Add(this.layoutControlRecurrenceForm);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(470, 520);
            this.Name = "OperatorAssignRecurrenceForm";
            this.Text = "OperatorAssignRecurrenceForm";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlRecurrenceForm)).EndInit();
            this.layoutControlRecurrenceForm.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSelectAll.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkedComboBoxEditDaysOfWeek.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExAssignment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExAssignment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupAssignment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControlRecurrenceForm;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private DevExpress.XtraEditors.SimpleButton simpleButtonAccept;
        private GridControlEx gridControlExAssignment;
        private GridViewEx gridViewExAssignment;
        private System.Windows.Forms.RadioButton radioButtonDaysOfWeek;
        private System.Windows.Forms.RadioButton radioButtonDay;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupAssignment;
        private DevExpress.XtraEditors.CheckedComboBoxEdit checkedComboBoxEditDaysOfWeek;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private System.Windows.Forms.RadioButton radioButtonAll;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.CheckEdit checkEditSelectAll;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
    }
}