using SmartCadControls;
using SmartCadGuiCommon.Controls;

namespace SmartCadGuiCommon
{
    partial class SupervisorCloseReportReaderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SupervisorCloseReportReaderForm));
            this.richTextBoxCloseMessages = new System.Windows.Forms.RichTextBox();
            this.indicatorDashBoardControl1 = new IndicatorDashBoardControl();
            this.gridControlCloseReports = new GridControlEx();
            this.gridView1 = new GridViewEx();
            this.RibbonControl = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.barButtonItemCopy = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.barButtonItemCut = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.barButtonItemPaste = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.barButtonItemSend = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSave = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemUpdate = new DevExpress.XtraBars.BarButtonItem();
            this.RibbonPageOperation = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPage();
            this.ribbonPageGroupFinalReport = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup();
            this.ribbonPageGroupOptions = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.layoutControlCloseReportReader = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupCloseReports = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupDashBoard = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupCloseMessages = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCloseReports)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCloseReportReader)).BeginInit();
            this.layoutControlCloseReportReader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupCloseReports)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupDashBoard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupCloseMessages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            this.SuspendLayout();
            // 
            // richTextBoxCloseMessages
            // 
            this.richTextBoxCloseMessages.BackColor = System.Drawing.Color.White;
            this.richTextBoxCloseMessages.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.richTextBoxCloseMessages.Location = new System.Drawing.Point(6, 403);
            this.richTextBoxCloseMessages.Name = "richTextBoxCloseMessages";
            this.richTextBoxCloseMessages.ReadOnly = true;
            this.richTextBoxCloseMessages.ShortcutsEnabled = false;
            this.richTextBoxCloseMessages.Size = new System.Drawing.Size(1259, 375);
            this.richTextBoxCloseMessages.TabIndex = 15;
            this.richTextBoxCloseMessages.TabStop = false;
            this.richTextBoxCloseMessages.Text = "";
            this.richTextBoxCloseMessages.SelectionChanged += new System.EventHandler(this.richTextBoxCloseMessages_SelectionChanged);
            // 
            // indicatorDashBoardControl1
            // 
            this.indicatorDashBoardControl1.BackColor = System.Drawing.Color.White;
            this.indicatorDashBoardControl1.DashBoardType = IndicatorDashBoardControl.DashBoardClassType.SYSTEM;
            this.indicatorDashBoardControl1.IsFirstLevel = false;
            this.indicatorDashBoardControl1.IsGeneralSupervisor = false;
            this.indicatorDashBoardControl1.Location = new System.Drawing.Point(653, 24);
            this.indicatorDashBoardControl1.Name = "indicatorDashBoardControl1";
            this.indicatorDashBoardControl1.Size = new System.Drawing.Size(612, 348);
            this.indicatorDashBoardControl1.TabIndex = 18;
            this.indicatorDashBoardControl1.TitleFirstGroup = "Call";
            this.indicatorDashBoardControl1.TitleSecondGroup = "First Level";
            this.indicatorDashBoardControl1.TitleThirdGroup = "Dispatch";
            // 
            // gridControlCloseReports
            // 
            this.gridControlCloseReports.EnableAutoFilter = true;
            this.gridControlCloseReports.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlCloseReports.Location = new System.Drawing.Point(6, 24);
            this.gridControlCloseReports.MainView = this.gridView1;
            this.gridControlCloseReports.Name = "gridControlCloseReports";
            this.gridControlCloseReports.Size = new System.Drawing.Size(638, 348);
            this.gridControlCloseReports.TabIndex = 16;
            this.gridControlCloseReports.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            this.gridControlCloseReports.ViewTotalRows = true;
            // 
            // gridView1
            // 
            this.gridView1.AllowFocusedRowChanged = true;
            this.gridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView1.EnablePreviewLineForFocusedRow = false;
            this.gridView1.GridControl = this.gridControlCloseReports;
            this.gridView1.GroupFormat = "[#image]{1} {2}";
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsCustomization.AllowFilter = false;
            this.gridView1.OptionsMenu.EnableColumnMenu = false;
            this.gridView1.OptionsMenu.EnableFooterMenu = false;
            this.gridView1.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupedColumns = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.ViewTotalRows = true;
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            // 
            // RibbonControl
            // 
            this.RibbonControl.ApplicationIcon = null;
            this.RibbonControl.Dock = System.Windows.Forms.DockStyle.None;
            this.RibbonControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RibbonControl.Images = this.imageList;
            this.RibbonControl.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItemCopy,
            this.barButtonItemCut,
            this.barButtonItemPaste,
            this.barButtonItemSend,
            this.barButtonItemSave,
            this.barButtonItemPrint,
            this.barButtonItemUpdate});
            this.RibbonControl.ItemsVertAlign = DevExpress.Utils.VertAlignment.Top;
            this.RibbonControl.LargeImages = this.imageList;
            this.RibbonControl.Location = new System.Drawing.Point(833, 345);
            this.RibbonControl.MaxItemId = 251;
            this.RibbonControl.MdiMergeStyle = DevExpress.XtraBars.Ribbon.RibbonMdiMergeStyle.Always;
            this.RibbonControl.Name = "RibbonControl";
            this.RibbonControl.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.RibbonPageOperation});
            this.RibbonControl.SelectedPage = this.RibbonPageOperation;
            this.RibbonControl.ShowToolbarCustomizeItem = false;
            this.RibbonControl.Size = new System.Drawing.Size(184, 115);
            this.RibbonControl.Toolbar.ShowCustomizeItem = false;
            this.RibbonControl.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "logout.gif");
            // 
            // barButtonItemCopy
            // 
            this.barButtonItemCopy.Caption = "Copiar";
            this.barButtonItemCopy.Enabled = false;
            this.barButtonItemCopy.Glyph = global::SmartCadGuiCommon.XRDesignRibbonControllerResources.RibbonUserDesigner_Copy;
            this.barButtonItemCopy.Id = 64;
            this.barButtonItemCopy.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C));
            this.barButtonItemCopy.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemCopy.Name = "barButtonItemCopy";
            this.barButtonItemCopy.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemCopy.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.PrintPreviewBarItemCopy_ItemClick);
            // 
            // barButtonItemCut
            // 
            this.barButtonItemCut.Caption = "Cortar";
            this.barButtonItemCut.Enabled = false;
            this.barButtonItemCut.Glyph = global::SmartCadGuiCommon.XRDesignRibbonControllerResources.RibbonUserDesigner_Cut;
            this.barButtonItemCut.Id = 65;
            this.barButtonItemCut.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X));
            this.barButtonItemCut.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemCut.Name = "barButtonItemCut";
            this.barButtonItemCut.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barButtonItemPaste
            // 
            this.barButtonItemPaste.Caption = "Pegar";
            this.barButtonItemPaste.Enabled = false;
            this.barButtonItemPaste.Glyph = global::SmartCadGuiCommon.XRDesignRibbonControllerResources.RibbonUserDesigner_Paste;
            this.barButtonItemPaste.Id = 79;
            this.barButtonItemPaste.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V));
            this.barButtonItemPaste.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemPaste.Name = "barButtonItemPaste";
            this.barButtonItemPaste.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barButtonItemSend
            // 
            this.barButtonItemSend.Enabled = false;
            this.barButtonItemSend.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemSend.Glyph")));
            this.barButtonItemSend.Id = 229;
            this.barButtonItemSend.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Enter));
            this.barButtonItemSend.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemSend.Name = "barButtonItemSend";
            this.barButtonItemSend.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemSend.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemSend_ItemClick);
            // 
            // barButtonItemSave
            // 
            this.barButtonItemSave.Enabled = false;
            this.barButtonItemSave.Glyph = global::SmartCadGuiCommon.PrintRibbonControllerResources.RibbonPrintPreview_Save;
            this.barButtonItemSave.Id = 230;
            this.barButtonItemSave.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S));
            this.barButtonItemSave.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemSave.Name = "barButtonItemSave";
            this.barButtonItemSave.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemSave_ItemClick);
            // 
            // barButtonItemPrint
            // 
            this.barButtonItemPrint.Enabled = false;
            this.barButtonItemPrint.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.Glyph")));
            this.barButtonItemPrint.Id = 231;
            this.barButtonItemPrint.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P));
            this.barButtonItemPrint.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemPrint.Name = "barButtonItemPrint";
            this.barButtonItemPrint.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPrint_ItemClick);
            // 
            // barButtonItemUpdate
            // 
            this.barButtonItemUpdate.Enabled = false;
            this.barButtonItemUpdate.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemUpdate.Glyph")));
            this.barButtonItemUpdate.Id = 249;
            this.barButtonItemUpdate.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F5);
            this.barButtonItemUpdate.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemUpdate.Name = "barButtonItemUpdate";
            this.barButtonItemUpdate.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // RibbonPageOperation
            // 
            this.RibbonPageOperation.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupFinalReport,
            this.ribbonPageGroupOptions});
            this.RibbonPageOperation.Name = "RibbonPageOperation";
            this.RibbonPageOperation.Text = "Operacion";
            // 
            // ribbonPageGroupFinalReport
            // 
            this.ribbonPageGroupFinalReport.AllowMinimize = false;
            this.ribbonPageGroupFinalReport.AllowTextClipping = false;
            this.ribbonPageGroupFinalReport.ItemLinks.Add(this.barButtonItemCut);
            this.ribbonPageGroupFinalReport.ItemLinks.Add(this.barButtonItemCopy);
            this.ribbonPageGroupFinalReport.ItemLinks.Add(this.barButtonItemPaste);
            this.ribbonPageGroupFinalReport.Kind = DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroupKind.Navigation;
            this.ribbonPageGroupFinalReport.Name = "ribbonPageGroupFinalReport";
            this.ribbonPageGroupFinalReport.ShowCaptionButton = false;
            this.ribbonPageGroupFinalReport.Text = "Reporte de cierre";
            // 
            // ribbonPageGroupOptions
            // 
            this.ribbonPageGroupOptions.AllowMinimize = false;
            this.ribbonPageGroupOptions.AllowTextClipping = false;
            this.ribbonPageGroupOptions.ItemLinks.Add(this.barButtonItemPrint);
            this.ribbonPageGroupOptions.ItemLinks.Add(this.barButtonItemSave);
            this.ribbonPageGroupOptions.ItemLinks.Add(this.barButtonItemSend);
            this.ribbonPageGroupOptions.ItemLinks.Add(this.barButtonItemUpdate);
            this.ribbonPageGroupOptions.Name = "ribbonPageGroupOptions";
            this.ribbonPageGroupOptions.ShowCaptionButton = false;
            this.ribbonPageGroupOptions.Text = "Opciones generales";
            // 
            // layoutControlCloseReportReader
            // 
            this.layoutControlCloseReportReader.AllowCustomizationMenu = false;
            this.layoutControlCloseReportReader.Appearance.DisabledLayoutGroupCaption.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControlCloseReportReader.Appearance.DisabledLayoutGroupCaption.Options.UseForeColor = true;
            this.layoutControlCloseReportReader.Appearance.DisabledLayoutItem.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControlCloseReportReader.Appearance.DisabledLayoutItem.Options.UseForeColor = true;
            this.layoutControlCloseReportReader.Controls.Add(this.richTextBoxCloseMessages);
            this.layoutControlCloseReportReader.Controls.Add(this.indicatorDashBoardControl1);
            this.layoutControlCloseReportReader.Controls.Add(this.gridControlCloseReports);
            this.layoutControlCloseReportReader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlCloseReportReader.Location = new System.Drawing.Point(0, 0);
            this.layoutControlCloseReportReader.Name = "layoutControlCloseReportReader";
            this.layoutControlCloseReportReader.Root = this.layoutControlGroup1;
            this.layoutControlCloseReportReader.Size = new System.Drawing.Size(1270, 787);
            this.layoutControlCloseReportReader.TabIndex = 1;
            this.layoutControlCloseReportReader.Text = "layoutControl1";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupCloseReports,
            this.layoutControlGroupDashBoard,
            this.layoutControlGroupCloseMessages});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1270, 787);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroupCloseReports
            // 
            this.layoutControlGroupCloseReports.CustomizationFormText = "layoutControlGroupCloseReports";
            this.layoutControlGroupCloseReports.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroupCloseReports.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupCloseReports.Name = "layoutControlGroupCloseReports";
            this.layoutControlGroupCloseReports.Size = new System.Drawing.Size(647, 379);
            this.layoutControlGroupCloseReports.Text = "layoutControlGroupCloseReports";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridControlCloseReports;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 5);
            this.layoutControlItem1.Size = new System.Drawing.Size(641, 355);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroupDashBoard
            // 
            this.layoutControlGroupDashBoard.CustomizationFormText = "layoutControlGroupDashBoard";
            this.layoutControlGroupDashBoard.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroupDashBoard.Location = new System.Drawing.Point(647, 0);
            this.layoutControlGroupDashBoard.Name = "layoutControlGroupDashBoard";
            this.layoutControlGroupDashBoard.Size = new System.Drawing.Size(621, 379);
            this.layoutControlGroupDashBoard.Text = "layoutControlGroupDashBoard";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.indicatorDashBoardControl1;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(615, 355);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(615, 355);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 5);
            this.layoutControlItem2.Size = new System.Drawing.Size(615, 355);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlGroupCloseMessages
            // 
            this.layoutControlGroupCloseMessages.CustomizationFormText = "layoutControlGroupCloseMessages";
            this.layoutControlGroupCloseMessages.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.layoutControlGroupCloseMessages.Location = new System.Drawing.Point(0, 379);
            this.layoutControlGroupCloseMessages.Name = "layoutControlGroupCloseMessages";
            this.layoutControlGroupCloseMessages.Size = new System.Drawing.Size(1268, 406);
            this.layoutControlGroupCloseMessages.Text = "layoutControlGroupCloseMessages";
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.Filter = "html | *.html";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.richTextBoxCloseMessages;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 5);
            this.layoutControlItem3.Size = new System.Drawing.Size(1262, 382);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // SupervisorCloseReportReaderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1270, 787);
            this.Controls.Add(this.layoutControlCloseReportReader);
            this.Controls.Add(this.RibbonControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SupervisorCloseReportReaderForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SupervisorCloseReportReader";
            this.Load += new System.EventHandler(this.SupervisorCloseReportReaderForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCloseReports)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCloseReportReader)).EndInit();
            this.layoutControlCloseReportReader.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupCloseReports)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupDashBoard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupCloseMessages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            this.ResumeLayout(false);

        }


        #endregion

        private System.Windows.Forms.RichTextBox richTextBoxCloseMessages;
        private GridControlEx gridControlCloseReports;
        private GridViewEx gridView1;
        private IndicatorDashBoardControl indicatorDashBoardControl1;
        internal DevExpress.XtraBars.Ribbon.RibbonControl RibbonControl;
        private System.Windows.Forms.ImageList imageList;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem barButtonItemCopy;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem barButtonItemCut;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem barButtonItemPaste;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemSend;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemSave;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemPrint;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemUpdate;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPage RibbonPageOperation;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup ribbonPageGroupFinalReport;
        internal DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupOptions;
        private DevExpress.XtraLayout.LayoutControl layoutControlCloseReportReader;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupCloseReports;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupDashBoard;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupCloseMessages;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
    }
}