
using SmartCadGuiCommon.Controls;
namespace SmartCadGuiCommon
{
    partial class IndicatorsForm : DevExpress.XtraEditors.XtraForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControlIndicators = new DevExpress.XtraLayout.LayoutControl();
            this.indicatorSystemPanelControl1 = new IndicatorSystemPanelControl();
            this.indicatorGroupFirstLevelPanelControl1 = new IndicatorGroupFirstLevelPanelControl();
            this.indicatorGroupDisaptchPanelControl1 = new IndicatorGroupDisaptchPanelControl();
            this.indicatorControlGroup = new IndicatorControl();
            this.indicatorDepartmentPanelControl1 = new IndicatorDepartmentPanelControl();
            this.indicatorControlDepartment = new IndicatorControl();
            this.indicatorControlSystem = new IndicatorControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroupIndicators = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroupSystem = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemIndicatorControlSystem = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemIndicatorSystemPanelControl1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupDepartment = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemIndicatorControlDepartment = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemIndicatorDepartmentPanelControl1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupMyGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemindicatorControlMyGroup = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemIndicatorGroupDisaptchPanelControl1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemIndicatorGroupFirstLevelPanelControl1 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlIndicators)).BeginInit();
            this.layoutControlIndicators.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroupIndicators)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupSystem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIndicatorControlSystem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIndicatorSystemPanelControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupDepartment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIndicatorControlDepartment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIndicatorDepartmentPanelControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupMyGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemindicatorControlMyGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIndicatorGroupDisaptchPanelControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIndicatorGroupFirstLevelPanelControl1)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControlIndicators
            // 
            this.layoutControlIndicators.Controls.Add(this.indicatorSystemPanelControl1);
            this.layoutControlIndicators.Controls.Add(this.indicatorGroupFirstLevelPanelControl1);
            this.layoutControlIndicators.Controls.Add(this.indicatorGroupDisaptchPanelControl1);
            this.layoutControlIndicators.Controls.Add(this.indicatorControlGroup);
            this.layoutControlIndicators.Controls.Add(this.indicatorDepartmentPanelControl1);
            this.layoutControlIndicators.Controls.Add(this.indicatorControlDepartment);
            this.layoutControlIndicators.Controls.Add(this.indicatorControlSystem);
            this.layoutControlIndicators.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlIndicators.Location = new System.Drawing.Point(0, 0);
            this.layoutControlIndicators.Name = "layoutControlIndicators";
            this.layoutControlIndicators.Root = this.layoutControlGroup1;
            this.layoutControlIndicators.Size = new System.Drawing.Size(1264, 755);
            this.layoutControlIndicators.TabIndex = 1;
            this.layoutControlIndicators.Text = "layoutControl1";
            // 
            // indicatorSystemPanelControl1
            // 
            this.indicatorSystemPanelControl1.Location = new System.Drawing.Point(7, 388);
            this.indicatorSystemPanelControl1.LookAndFeel.SkinName = "Blue";
            this.indicatorSystemPanelControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.indicatorSystemPanelControl1.Name = "indicatorSystemPanelControl1";
            this.indicatorSystemPanelControl1.Size = new System.Drawing.Size(1250, 360);
            this.indicatorSystemPanelControl1.TabIndex = 1;
            // 
            // indicatorGroupFirstLevelPanelControl1
            // 
            this.indicatorGroupFirstLevelPanelControl1.Location = new System.Drawing.Point(7, 572);
            this.indicatorGroupFirstLevelPanelControl1.LookAndFeel.SkinName = "Blue";
            this.indicatorGroupFirstLevelPanelControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.indicatorGroupFirstLevelPanelControl1.Name = "indicatorGroupFirstLevelPanelControl1";
            this.indicatorGroupFirstLevelPanelControl1.Size = new System.Drawing.Size(1250, 176);
            this.indicatorGroupFirstLevelPanelControl1.TabIndex = 1;
            // 
            // indicatorGroupDisaptchPanelControl1
            // 
            this.indicatorGroupDisaptchPanelControl1.Location = new System.Drawing.Point(7, 388);
            this.indicatorGroupDisaptchPanelControl1.LookAndFeel.SkinName = "Blue";
            this.indicatorGroupDisaptchPanelControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.indicatorGroupDisaptchPanelControl1.Name = "indicatorGroupDisaptchPanelControl1";
            this.indicatorGroupDisaptchPanelControl1.Size = new System.Drawing.Size(1250, 180);
            this.indicatorGroupDisaptchPanelControl1.TabIndex = 2;
            this.indicatorGroupDisaptchPanelControl1.Visible = false;
            // 
            // indicatorControlGroup
            // 
            this.indicatorControlGroup.Location = new System.Drawing.Point(7, 27);
            this.indicatorControlGroup.Name = "indicatorControlGroup";
            this.indicatorControlGroup.Size = new System.Drawing.Size(1250, 357);
            this.indicatorControlGroup.TabIndex = 0;
            // 
            // indicatorDepartmentPanelControl1
            // 
            this.indicatorDepartmentPanelControl1.Location = new System.Drawing.Point(7, 388);
            this.indicatorDepartmentPanelControl1.LookAndFeel.SkinName = "Blue";
            this.indicatorDepartmentPanelControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.indicatorDepartmentPanelControl1.Name = "indicatorDepartmentPanelControl1";
            this.indicatorDepartmentPanelControl1.Size = new System.Drawing.Size(1250, 360);
            this.indicatorDepartmentPanelControl1.TabIndex = 1;
            // 
            // indicatorControlDepartment
            // 
            this.indicatorControlDepartment.Location = new System.Drawing.Point(7, 27);
            this.indicatorControlDepartment.Name = "indicatorControlDepartment";
            this.indicatorControlDepartment.Size = new System.Drawing.Size(1250, 357);
            this.indicatorControlDepartment.TabIndex = 0;
            // 
            // indicatorControlSystem
            // 
            this.indicatorControlSystem.Location = new System.Drawing.Point(7, 27);
            this.indicatorControlSystem.Name = "indicatorControlSystem";
            this.indicatorControlSystem.Size = new System.Drawing.Size(1250, 357);
            this.indicatorControlSystem.TabIndex = 0;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroupIndicators});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1264, 755);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // tabbedControlGroupIndicators
            // 
            this.tabbedControlGroupIndicators.CustomizationFormText = "tabbedControlGroupIndicators";
            this.tabbedControlGroupIndicators.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroupIndicators.Name = "tabbedControlGroupIndicators";
            this.tabbedControlGroupIndicators.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.tabbedControlGroupIndicators.SelectedTabPage = this.layoutControlGroupSystem;
            this.tabbedControlGroupIndicators.SelectedTabPageIndex = 0;
            this.tabbedControlGroupIndicators.Size = new System.Drawing.Size(1264, 755);
            this.tabbedControlGroupIndicators.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupSystem,
            this.layoutControlGroupDepartment,
            this.layoutControlGroupMyGroup});
            this.tabbedControlGroupIndicators.Text = "tabbedControlGroupIndicators";
            // 
            // layoutControlGroupSystem
            // 
            this.layoutControlGroupSystem.CustomizationFormText = "layoutControlGroupSystem";
            this.layoutControlGroupSystem.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemIndicatorControlSystem,
            this.layoutControlItemIndicatorSystemPanelControl1});
            this.layoutControlGroupSystem.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupSystem.Name = "layoutControlGroupSystem";
            this.layoutControlGroupSystem.Size = new System.Drawing.Size(1254, 725);
            this.layoutControlGroupSystem.Text = "layoutControlGroupSystem";
            // 
            // layoutControlItemIndicatorControlSystem
            // 
            this.layoutControlItemIndicatorControlSystem.Control = this.indicatorControlSystem;
            this.layoutControlItemIndicatorControlSystem.CustomizationFormText = "layoutControlItemIndicatorControlSystem";
            this.layoutControlItemIndicatorControlSystem.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemIndicatorControlSystem.Name = "layoutControlItemIndicatorControlSystem";
            this.layoutControlItemIndicatorControlSystem.Size = new System.Drawing.Size(1254, 361);
            this.layoutControlItemIndicatorControlSystem.Text = "layoutControlItemIndicatorControlSystem";
            this.layoutControlItemIndicatorControlSystem.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemIndicatorControlSystem.TextToControlDistance = 0;
            this.layoutControlItemIndicatorControlSystem.TextVisible = false;
            // 
            // layoutControlItemIndicatorSystemPanelControl1
            // 
            this.layoutControlItemIndicatorSystemPanelControl1.Control = this.indicatorSystemPanelControl1;
            this.layoutControlItemIndicatorSystemPanelControl1.CustomizationFormText = "layoutControlItemIndicatorSystemPanelControl1";
            this.layoutControlItemIndicatorSystemPanelControl1.Location = new System.Drawing.Point(0, 361);
            this.layoutControlItemIndicatorSystemPanelControl1.Name = "layoutControlItemIndicatorSystemPanelControl1";
            this.layoutControlItemIndicatorSystemPanelControl1.Size = new System.Drawing.Size(1254, 364);
            this.layoutControlItemIndicatorSystemPanelControl1.Text = "layoutControlItemIndicatorSystemPanelControl1";
            this.layoutControlItemIndicatorSystemPanelControl1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemIndicatorSystemPanelControl1.TextToControlDistance = 0;
            this.layoutControlItemIndicatorSystemPanelControl1.TextVisible = false;
            // 
            // layoutControlGroupDepartment
            // 
            this.layoutControlGroupDepartment.CustomizationFormText = "layoutControlGroupDepartment";
            this.layoutControlGroupDepartment.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemIndicatorControlDepartment,
            this.layoutControlItemIndicatorDepartmentPanelControl1});
            this.layoutControlGroupDepartment.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupDepartment.Name = "layoutControlGroupDepartment";
            this.layoutControlGroupDepartment.Size = new System.Drawing.Size(1254, 725);
            this.layoutControlGroupDepartment.Text = "layoutControlGroupDepartment";
            // 
            // layoutControlItemIndicatorControlDepartment
            // 
            this.layoutControlItemIndicatorControlDepartment.Control = this.indicatorControlDepartment;
            this.layoutControlItemIndicatorControlDepartment.CustomizationFormText = "layoutControlItemIndicatorControlDepartment";
            this.layoutControlItemIndicatorControlDepartment.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemIndicatorControlDepartment.Name = "layoutControlItemIndicatorControlDepartment";
            this.layoutControlItemIndicatorControlDepartment.Size = new System.Drawing.Size(1254, 361);
            this.layoutControlItemIndicatorControlDepartment.Text = "layoutControlItemIndicatorControlDepartment";
            this.layoutControlItemIndicatorControlDepartment.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemIndicatorControlDepartment.TextToControlDistance = 0;
            this.layoutControlItemIndicatorControlDepartment.TextVisible = false;
            // 
            // layoutControlItemIndicatorDepartmentPanelControl1
            // 
            this.layoutControlItemIndicatorDepartmentPanelControl1.Control = this.indicatorDepartmentPanelControl1;
            this.layoutControlItemIndicatorDepartmentPanelControl1.CustomizationFormText = "layoutControlItemIndicatorDepartmentPanelControl1";
            this.layoutControlItemIndicatorDepartmentPanelControl1.Location = new System.Drawing.Point(0, 361);
            this.layoutControlItemIndicatorDepartmentPanelControl1.Name = "layoutControlItemIndicatorDepartmentPanelControl1";
            this.layoutControlItemIndicatorDepartmentPanelControl1.Size = new System.Drawing.Size(1254, 364);
            this.layoutControlItemIndicatorDepartmentPanelControl1.Text = "layoutControlItemIndicatorDepartmentPanelControl1";
            this.layoutControlItemIndicatorDepartmentPanelControl1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemIndicatorDepartmentPanelControl1.TextToControlDistance = 0;
            this.layoutControlItemIndicatorDepartmentPanelControl1.TextVisible = false;
            // 
            // layoutControlGroupMyGroup
            // 
            this.layoutControlGroupMyGroup.CustomizationFormText = "layoutControlGroupMyGroup";
            this.layoutControlGroupMyGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemindicatorControlMyGroup,
            this.layoutControlItemIndicatorGroupDisaptchPanelControl1,
            this.layoutControlItemIndicatorGroupFirstLevelPanelControl1});
            this.layoutControlGroupMyGroup.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupMyGroup.Name = "layoutControlGroupMyGroup";
            this.layoutControlGroupMyGroup.Size = new System.Drawing.Size(1254, 725);
            this.layoutControlGroupMyGroup.Text = "layoutControlGroupMyGroup";
            // 
            // layoutControlItemindicatorControlMyGroup
            // 
            this.layoutControlItemindicatorControlMyGroup.Control = this.indicatorControlGroup;
            this.layoutControlItemindicatorControlMyGroup.CustomizationFormText = "layoutControlItemindicatorControlMyGroup";
            this.layoutControlItemindicatorControlMyGroup.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemindicatorControlMyGroup.Name = "layoutControlItemindicatorControlMyGroup";
            this.layoutControlItemindicatorControlMyGroup.Size = new System.Drawing.Size(1254, 361);
            this.layoutControlItemindicatorControlMyGroup.Text = "layoutControlItemindicatorControlMyGroup";
            this.layoutControlItemindicatorControlMyGroup.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemindicatorControlMyGroup.TextToControlDistance = 0;
            this.layoutControlItemindicatorControlMyGroup.TextVisible = false;
            // 
            // layoutControlItemIndicatorGroupDisaptchPanelControl1
            // 
            this.layoutControlItemIndicatorGroupDisaptchPanelControl1.Control = this.indicatorGroupDisaptchPanelControl1;
            this.layoutControlItemIndicatorGroupDisaptchPanelControl1.CustomizationFormText = "layoutControlItemIndicatorGroupDisaptchPanelControl1";
            this.layoutControlItemIndicatorGroupDisaptchPanelControl1.Location = new System.Drawing.Point(0, 361);
            this.layoutControlItemIndicatorGroupDisaptchPanelControl1.Name = "layoutControlItemIndicatorGroupDisaptchPanelControl1";
            this.layoutControlItemIndicatorGroupDisaptchPanelControl1.Size = new System.Drawing.Size(1254, 184);
            this.layoutControlItemIndicatorGroupDisaptchPanelControl1.Text = "layoutControlItemIndicatorGroupDisaptchPanelControl1";
            this.layoutControlItemIndicatorGroupDisaptchPanelControl1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemIndicatorGroupDisaptchPanelControl1.TextToControlDistance = 0;
            this.layoutControlItemIndicatorGroupDisaptchPanelControl1.TextVisible = false;
            // 
            // layoutControlItemIndicatorGroupFirstLevelPanelControl1
            // 
            this.layoutControlItemIndicatorGroupFirstLevelPanelControl1.Control = this.indicatorGroupFirstLevelPanelControl1;
            this.layoutControlItemIndicatorGroupFirstLevelPanelControl1.CustomizationFormText = "layoutControlItemIndicatorGroupFirstLevelPanelControl1";
            this.layoutControlItemIndicatorGroupFirstLevelPanelControl1.Location = new System.Drawing.Point(0, 545);
            this.layoutControlItemIndicatorGroupFirstLevelPanelControl1.Name = "layoutControlItemIndicatorGroupFirstLevelPanelControl1";
            this.layoutControlItemIndicatorGroupFirstLevelPanelControl1.Size = new System.Drawing.Size(1254, 180);
            this.layoutControlItemIndicatorGroupFirstLevelPanelControl1.Text = "layoutControlItemIndicatorGroupFirstLevelPanelControl1";
            this.layoutControlItemIndicatorGroupFirstLevelPanelControl1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemIndicatorGroupFirstLevelPanelControl1.TextToControlDistance = 0;
            this.layoutControlItemIndicatorGroupFirstLevelPanelControl1.TextVisible = false;
            // 
            // IndicatorsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 755);
            this.Controls.Add(this.layoutControlIndicators);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "IndicatorsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Indicadores de desempeno";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.IndicatorsForm_FormClosing);
            this.Load += new System.EventHandler(this.IndicatorsForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlIndicators)).EndInit();
            this.layoutControlIndicators.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroupIndicators)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupSystem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIndicatorControlSystem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIndicatorSystemPanelControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupDepartment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIndicatorControlDepartment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIndicatorDepartmentPanelControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupMyGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemindicatorControlMyGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIndicatorGroupDisaptchPanelControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIndicatorGroupFirstLevelPanelControl1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private IndicatorSystemPanelControl indicatorSystemPanelControl1;
        internal IndicatorControl indicatorControlSystem;
        internal IndicatorControl indicatorControlGroup;
        internal IndicatorControl indicatorControlDepartment;
        private IndicatorGroupFirstLevelPanelControl indicatorGroupFirstLevelPanelControl1;
        private IndicatorDepartmentPanelControl indicatorDepartmentPanelControl1;
        private IndicatorGroupDisaptchPanelControl indicatorGroupDisaptchPanelControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControlIndicators;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemIndicatorSystemPanelControl1;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroupIndicators;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupDepartment;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemIndicatorControlDepartment;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupSystem;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemIndicatorControlSystem;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupMyGroup;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemindicatorControlMyGroup;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemIndicatorGroupDisaptchPanelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemIndicatorGroupFirstLevelPanelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemIndicatorDepartmentPanelControl1;

    }
}