
using SmartCadControls;
namespace SmartCadGuiCommon
{
    partial class TrainingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TrainingForm));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            this.gridControlOperators = new GridControlEx();
            this.gridViewOperators = new GridViewEx();
            this.gridColumnOperatorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnWorkShift = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControlParts = new GridControlEx();
            this.gridViewParts = new GridViewEx();
            this.gridColumnRoom = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnPartStart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnPartEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.gridControlSchedules = new GridControlEx();
            this.gridViewSchedules = new GridViewEx();
            this.gridColumnScheduleName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnScheduleStart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnScheduleEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnTrainer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControlTrainingCourse = new GridControlEx();
            this.gridViewTrainingCourse = new GridViewEx();
            this.gridColumnName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnContact = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnContactTelephone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.RibbonControl = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.barButtonOperatorTrainingAssign = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.barButtonTrainingCoursesSchedule = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.barButtonItemModifyCourses = new DevExpress.XtraBars.BarButtonItem();
            this.barItemCreateCourses = new DevExpress.XtraBars.BarButtonItem();
            this.barItemDeleteCourses = new DevExpress.XtraBars.BarButtonItem();
            this.RibbonPageOperation = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPage();
            this.ribbonPageGroupTraining = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup();
            this.layoutControlTrainingCourse = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupTrainingCourse = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupSchedules = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupOperators = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupParts = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlOperators)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewOperators)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlParts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewParts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSchedules)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSchedules)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTrainingCourse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTrainingCourse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlTrainingCourse)).BeginInit();
            this.layoutControlTrainingCourse.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupTrainingCourse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupSchedules)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupOperators)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupParts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControlOperators
            // 
            this.gridControlOperators.EnableAutoFilter = true;
            this.gridControlOperators.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlOperators.Location = new System.Drawing.Point(643, 554);
            this.gridControlOperators.MainView = this.gridViewOperators;
            this.gridControlOperators.Name = "gridControlOperators";
            this.gridControlOperators.Size = new System.Drawing.Size(618, 224);
            this.gridControlOperators.TabIndex = 32;
            this.gridControlOperators.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewOperators});
            this.gridControlOperators.ViewTotalRows = false;
            // 
            // gridViewOperators
            // 
            this.gridViewOperators.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewOperators.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewOperators.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewOperators.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewOperators.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewOperators.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewOperators.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewOperators.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewOperators.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewOperators.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewOperators.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnOperatorName,
            this.gridColumnCategory,
            this.gridColumnWorkShift});
            this.gridViewOperators.EnablePreviewLineForFocusedRow = false;
            this.gridViewOperators.GridControl = this.gridControlOperators;
            this.gridViewOperators.GroupFormat = "[#image]{1} {2}";
            this.gridViewOperators.Name = "gridViewOperators";
            this.gridViewOperators.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewOperators.OptionsMenu.EnableColumnMenu = false;
            this.gridViewOperators.OptionsMenu.EnableFooterMenu = false;
            this.gridViewOperators.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridViewOperators.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewOperators.OptionsView.ShowAutoFilterRow = true;
            this.gridViewOperators.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewOperators.OptionsView.ShowFooter = true;
            this.gridViewOperators.OptionsView.ShowGroupPanel = false;
            this.gridViewOperators.OptionsView.ShowIndicator = false;
            this.gridViewOperators.ViewTotalRows = false;
            // 
            // gridColumnOperatorName
            // 
            this.gridColumnOperatorName.Caption = "gridColumn1";
            this.gridColumnOperatorName.FieldName = "Name";
            this.gridColumnOperatorName.Name = "gridColumnOperatorName";
            this.gridColumnOperatorName.OptionsColumn.AllowEdit = false;
            this.gridColumnOperatorName.OptionsFilter.AllowFilter = false;
            this.gridColumnOperatorName.Visible = true;
            this.gridColumnOperatorName.VisibleIndex = 0;
            this.gridColumnOperatorName.Width = 220;
            // 
            // gridColumnCategory
            // 
            this.gridColumnCategory.Caption = "gridColumn3";
            this.gridColumnCategory.FieldName = "Category";
            this.gridColumnCategory.Name = "gridColumnCategory";
            this.gridColumnCategory.OptionsColumn.AllowEdit = false;
            this.gridColumnCategory.OptionsFilter.AllowFilter = false;
            this.gridColumnCategory.Visible = true;
            this.gridColumnCategory.VisibleIndex = 1;
            this.gridColumnCategory.Width = 220;
            // 
            // gridColumnWorkShift
            // 
            this.gridColumnWorkShift.Caption = "gridColumn4";
            this.gridColumnWorkShift.FieldName = "WorkShift";
            this.gridColumnWorkShift.Name = "gridColumnWorkShift";
            this.gridColumnWorkShift.OptionsColumn.AllowEdit = false;
            this.gridColumnWorkShift.OptionsFilter.AllowFilter = false;
            this.gridColumnWorkShift.SummaryItem.DisplayFormat = "Total: {0:0000}";
            this.gridColumnWorkShift.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count;
            this.gridColumnWorkShift.Visible = true;
            this.gridColumnWorkShift.VisibleIndex = 2;
            this.gridColumnWorkShift.Width = 80;
            // 
            // gridControlParts
            // 
            this.gridControlParts.EnableAutoFilter = true;
            this.gridControlParts.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlParts.Location = new System.Drawing.Point(10, 554);
            this.gridControlParts.MainView = this.gridViewParts;
            this.gridControlParts.Name = "gridControlParts";
            this.gridControlParts.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit1});
            this.gridControlParts.Size = new System.Drawing.Size(616, 224);
            this.gridControlParts.TabIndex = 32;
            this.gridControlParts.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewParts});
            this.gridControlParts.ViewTotalRows = false;
            // 
            // gridViewParts
            // 
            this.gridViewParts.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewParts.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewParts.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewParts.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewParts.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewParts.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewParts.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewParts.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewParts.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewParts.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewParts.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnRoom,
            this.gridColumnPartStart,
            this.gridColumnPartEnd});
            this.gridViewParts.EnablePreviewLineForFocusedRow = false;
            this.gridViewParts.GridControl = this.gridControlParts;
            this.gridViewParts.GroupFormat = "[#image]{1} {2}";
            this.gridViewParts.Name = "gridViewParts";
            this.gridViewParts.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewParts.OptionsMenu.EnableColumnMenu = false;
            this.gridViewParts.OptionsMenu.EnableFooterMenu = false;
            this.gridViewParts.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridViewParts.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewParts.OptionsView.ShowAutoFilterRow = true;
            this.gridViewParts.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewParts.OptionsView.ShowFooter = true;
            this.gridViewParts.OptionsView.ShowGroupPanel = false;
            this.gridViewParts.OptionsView.ShowIndicator = false;
            this.gridViewParts.ViewTotalRows = false;
            // 
            // gridColumnRoom
            // 
            this.gridColumnRoom.Caption = "gridColumn1";
            this.gridColumnRoom.FieldName = "Room";
            this.gridColumnRoom.Name = "gridColumnRoom";
            this.gridColumnRoom.OptionsColumn.AllowEdit = false;
            this.gridColumnRoom.OptionsFilter.AllowFilter = false;
            this.gridColumnRoom.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.gridColumnRoom.Visible = true;
            this.gridColumnRoom.VisibleIndex = 0;
            this.gridColumnRoom.Width = 190;
            // 
            // gridColumnPartStart
            // 
            this.gridColumnPartStart.Caption = "gridColumn2";
            this.gridColumnPartStart.DisplayFormat.FormatString = "MM/dd/yyyy HH:mm";
            this.gridColumnPartStart.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumnPartStart.FieldName = "StartDate";
            this.gridColumnPartStart.Name = "gridColumnPartStart";
            this.gridColumnPartStart.OptionsColumn.AllowEdit = false;
            this.gridColumnPartStart.OptionsFilter.AllowFilter = false;
            this.gridColumnPartStart.UnboundType = DevExpress.Data.UnboundColumnType.DateTime;
            this.gridColumnPartStart.Visible = true;
            this.gridColumnPartStart.VisibleIndex = 1;
            this.gridColumnPartStart.Width = 200;
            // 
            // gridColumnPartEnd
            // 
            this.gridColumnPartEnd.Caption = "gridColumn3";
            this.gridColumnPartEnd.DisplayFormat.FormatString = "MM/dd/yyyy HH:mm";
            this.gridColumnPartEnd.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumnPartEnd.FieldName = "EndDate";
            this.gridColumnPartEnd.Name = "gridColumnPartEnd";
            this.gridColumnPartEnd.OptionsColumn.AllowEdit = false;
            this.gridColumnPartEnd.OptionsFilter.AllowFilter = false;
            this.gridColumnPartEnd.SummaryItem.DisplayFormat = "Total: {0:0000}";
            this.gridColumnPartEnd.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count;
            this.gridColumnPartEnd.UnboundType = DevExpress.Data.UnboundColumnType.DateTime;
            this.gridColumnPartEnd.Visible = true;
            this.gridColumnPartEnd.VisibleIndex = 2;
            this.gridColumnPartEnd.Width = 200;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.DisplayFormat.FormatString = "g";
            this.repositoryItemDateEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItemDateEdit1.EditFormat.FormatString = "g";
            this.repositoryItemDateEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.Mask.EditMask = "g";
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            this.repositoryItemDateEdit1.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // gridControlSchedules
            // 
            this.gridControlSchedules.EnableAutoFilter = true;
            this.gridControlSchedules.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlSchedules.Location = new System.Drawing.Point(10, 307);
            this.gridControlSchedules.MainView = this.gridViewSchedules;
            this.gridControlSchedules.Name = "gridControlSchedules";
            this.gridControlSchedules.Size = new System.Drawing.Size(1251, 212);
            this.gridControlSchedules.TabIndex = 32;
            this.gridControlSchedules.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewSchedules});
            this.gridControlSchedules.ViewTotalRows = false;
            // 
            // gridViewSchedules
            // 
            this.gridViewSchedules.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewSchedules.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewSchedules.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewSchedules.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewSchedules.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewSchedules.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewSchedules.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewSchedules.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewSchedules.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewSchedules.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewSchedules.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnScheduleName,
            this.gridColumnScheduleStart,
            this.gridColumnScheduleEnd,
            this.gridColumnTrainer,
            this.gridColumnStatus});
            this.gridViewSchedules.EnablePreviewLineForFocusedRow = false;
            this.gridViewSchedules.GridControl = this.gridControlSchedules;
            this.gridViewSchedules.GroupFormat = "[#image]{1} {2}";
            this.gridViewSchedules.Name = "gridViewSchedules";
            this.gridViewSchedules.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewSchedules.OptionsMenu.EnableColumnMenu = false;
            this.gridViewSchedules.OptionsMenu.EnableFooterMenu = false;
            this.gridViewSchedules.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridViewSchedules.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewSchedules.OptionsView.ShowAutoFilterRow = true;
            this.gridViewSchedules.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewSchedules.OptionsView.ShowFooter = true;
            this.gridViewSchedules.OptionsView.ShowGroupPanel = false;
            this.gridViewSchedules.OptionsView.ShowIndicator = false;
            this.gridViewSchedules.ViewTotalRows = false;
            this.gridViewSchedules.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewSchedules_FocusedRowChanged);
            // 
            // gridColumnScheduleName
            // 
            this.gridColumnScheduleName.Caption = "gridColumn1";
            this.gridColumnScheduleName.FieldName = "Name";
            this.gridColumnScheduleName.Name = "gridColumnScheduleName";
            this.gridColumnScheduleName.OptionsColumn.AllowEdit = false;
            this.gridColumnScheduleName.OptionsFilter.AllowFilter = false;
            this.gridColumnScheduleName.Visible = true;
            this.gridColumnScheduleName.VisibleIndex = 0;
            this.gridColumnScheduleName.Width = 252;
            // 
            // gridColumnScheduleStart
            // 
            this.gridColumnScheduleStart.Caption = "gridColumn2";
            this.gridColumnScheduleStart.FieldName = "Start";
            this.gridColumnScheduleStart.DisplayFormat.FormatString = "MM/dd/yyyy";
            this.gridColumnScheduleStart.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumnScheduleStart.Name = "gridColumnScheduleStart";
            this.gridColumnScheduleStart.OptionsColumn.AllowEdit = false;
            this.gridColumnScheduleStart.OptionsFilter.AllowFilter = false;
            this.gridColumnScheduleStart.Visible = true;
            this.gridColumnScheduleStart.VisibleIndex = 1;
            this.gridColumnScheduleStart.Width = 252;
            // 
            // gridColumnScheduleEnd
            // 
            this.gridColumnScheduleEnd.Caption = "gridColumn3";
            this.gridColumnScheduleEnd.FieldName = "End";
            this.gridColumnScheduleEnd.DisplayFormat.FormatString = "MM/dd/yyyy";
            this.gridColumnScheduleEnd.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumnScheduleEnd.Name = "gridColumnScheduleEnd";
            this.gridColumnScheduleEnd.OptionsColumn.AllowEdit = false;
            this.gridColumnScheduleEnd.OptionsFilter.AllowFilter = false;
            this.gridColumnScheduleEnd.Visible = true;
            this.gridColumnScheduleEnd.VisibleIndex = 2;
            this.gridColumnScheduleEnd.Width = 252;
            // 
            // gridColumnTrainer
            // 
            this.gridColumnTrainer.Caption = "gridColumn4";
            this.gridColumnTrainer.FieldName = "Trainer";
            this.gridColumnTrainer.Name = "gridColumnTrainer";
            this.gridColumnTrainer.OptionsColumn.AllowEdit = false;
            this.gridColumnTrainer.OptionsFilter.AllowFilter = false;
            this.gridColumnTrainer.Visible = true;
            this.gridColumnTrainer.VisibleIndex = 3;
            this.gridColumnTrainer.Width = 252;
            // 
            // gridColumnStatus
            // 
            this.gridColumnStatus.Caption = "gridColumn1";
            this.gridColumnStatus.FieldName = "Status";
            this.gridColumnStatus.Name = "gridColumnStatus";
            this.gridColumnStatus.OptionsColumn.AllowEdit = false;
            this.gridColumnStatus.OptionsFilter.AllowFilter = false;
            this.gridColumnStatus.SummaryItem.DisplayFormat = "Total: {0:0000}";
            this.gridColumnStatus.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count;
            this.gridColumnStatus.Visible = true;
            this.gridColumnStatus.VisibleIndex = 4;
            this.gridColumnStatus.Width = 80;
            // 
            // gridControlTrainingCourse
            // 
            this.gridControlTrainingCourse.EnableAutoFilter = true;
            this.gridControlTrainingCourse.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlTrainingCourse.Location = new System.Drawing.Point(10, 28);
            this.gridControlTrainingCourse.MainView = this.gridViewTrainingCourse;
            this.gridControlTrainingCourse.Name = "gridControlTrainingCourse";
            this.gridControlTrainingCourse.Size = new System.Drawing.Size(1251, 244);
            this.gridControlTrainingCourse.TabIndex = 32;
            this.gridControlTrainingCourse.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewTrainingCourse});
            this.gridControlTrainingCourse.ViewTotalRows = false;
            this.gridControlTrainingCourse.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.gridControlTrainingCourse_MouseDoubleClick);
            // 
            // gridViewTrainingCourse
            // 
            this.gridViewTrainingCourse.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewTrainingCourse.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewTrainingCourse.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewTrainingCourse.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewTrainingCourse.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewTrainingCourse.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewTrainingCourse.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewTrainingCourse.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewTrainingCourse.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewTrainingCourse.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewTrainingCourse.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnName,
            this.gridColumnContact,
            this.gridColumnContactTelephone});
            this.gridViewTrainingCourse.EnablePreviewLineForFocusedRow = false;
            this.gridViewTrainingCourse.GridControl = this.gridControlTrainingCourse;
            this.gridViewTrainingCourse.GroupFormat = "[#image]{1} {2}";
            this.gridViewTrainingCourse.Name = "gridViewTrainingCourse";
            this.gridViewTrainingCourse.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewTrainingCourse.OptionsCustomization.AllowFilter = false;
            this.gridViewTrainingCourse.OptionsMenu.EnableColumnMenu = false;
            this.gridViewTrainingCourse.OptionsMenu.EnableFooterMenu = false;
            this.gridViewTrainingCourse.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridViewTrainingCourse.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewTrainingCourse.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewTrainingCourse.OptionsView.ShowAutoFilterRow = true;
            this.gridViewTrainingCourse.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewTrainingCourse.OptionsView.ShowFooter = true;
            this.gridViewTrainingCourse.OptionsView.ShowGroupPanel = false;
            this.gridViewTrainingCourse.OptionsView.ShowIndicator = false;
            this.gridViewTrainingCourse.ViewTotalRows = false;
            this.gridViewTrainingCourse.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewTrainingCourse_FocusedRowChanged);
            // 
            // gridColumnName
            // 
            this.gridColumnName.Caption = "gridColumn1";
            this.gridColumnName.FieldName = "Name";
            this.gridColumnName.Name = "gridColumnName";
            this.gridColumnName.OptionsColumn.AllowEdit = false;
            this.gridColumnName.OptionsFilter.AllowFilter = false;
            this.gridColumnName.Visible = true;
            this.gridColumnName.VisibleIndex = 0;
            this.gridColumnName.Width = 420;
            // 
            // gridColumnContact
            // 
            this.gridColumnContact.Caption = "gridColumn2";
            this.gridColumnContact.FieldName = "ContactPerson";
            this.gridColumnContact.Name = "gridColumnContact";
            this.gridColumnContact.OptionsColumn.AllowEdit = false;
            this.gridColumnContact.OptionsFilter.AllowFilter = false;
            this.gridColumnContact.Visible = true;
            this.gridColumnContact.VisibleIndex = 1;
            this.gridColumnContact.Width = 420;
            // 
            // gridColumnContactTelephone
            // 
            this.gridColumnContactTelephone.Caption = "gridColumn3";
            this.gridColumnContactTelephone.FieldName = "ContactTelephone";
            this.gridColumnContactTelephone.Name = "gridColumnContactTelephone";
            this.gridColumnContactTelephone.OptionsColumn.AllowEdit = false;
            this.gridColumnContactTelephone.OptionsFilter.AllowFilter = false;
            this.gridColumnContactTelephone.SummaryItem.DisplayFormat = "Total: {0:0000}";
            this.gridColumnContactTelephone.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count;
            this.gridColumnContactTelephone.Visible = true;
            this.gridColumnContactTelephone.VisibleIndex = 2;
            this.gridColumnContactTelephone.Width = 80;
            // 
            // RibbonControl
            // 
            this.RibbonControl.ApplicationIcon = null;
            this.RibbonControl.Dock = System.Windows.Forms.DockStyle.None;
            this.RibbonControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RibbonControl.Images = this.imageList;
            this.RibbonControl.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonOperatorTrainingAssign,
            this.barButtonTrainingCoursesSchedule,
            this.barButtonItemModifyCourses,
            this.barItemCreateCourses,
            this.barItemDeleteCourses});
            this.RibbonControl.ItemsVertAlign = DevExpress.Utils.VertAlignment.Top;
            this.RibbonControl.LargeImages = this.imageList;
            this.RibbonControl.Location = new System.Drawing.Point(781, 62);
            this.RibbonControl.MaxItemId = 256;
            this.RibbonControl.MdiMergeStyle = DevExpress.XtraBars.Ribbon.RibbonMdiMergeStyle.Always;
            this.RibbonControl.Name = "RibbonControl";
            this.RibbonControl.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.RibbonPageOperation});
            this.RibbonControl.SelectedPage = this.RibbonPageOperation;
            this.RibbonControl.ShowToolbarCustomizeItem = false;
            this.RibbonControl.Size = new System.Drawing.Size(250, 115);
            this.RibbonControl.Toolbar.ShowCustomizeItem = false;
            this.RibbonControl.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "logout.gif");
            // 
            // barButtonOperatorTrainingAssign
            // 
            this.barButtonOperatorTrainingAssign.Caption = "Participantes";
            this.barButtonOperatorTrainingAssign.Enabled = false;
            this.barButtonOperatorTrainingAssign.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonOperatorTrainingAssign.Glyph")));
            this.barButtonOperatorTrainingAssign.Id = 88;
            this.barButtonOperatorTrainingAssign.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonOperatorTrainingAssign.Name = "barButtonOperatorTrainingAssign";
            this.barButtonOperatorTrainingAssign.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            superToolTip1.FixedTooltipWidth = true;
            toolTipTitleItem1.Text = "Asignar participantes a cursos.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.MaxWidth = 210;
            this.barButtonOperatorTrainingAssign.SuperTip = superToolTip1;
            this.barButtonOperatorTrainingAssign.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonOperatorTrainingAssign_ItemClick);
            // 
            // barButtonTrainingCoursesSchedule
            // 
            this.barButtonTrainingCoursesSchedule.Caption = "Horarios";
            this.barButtonTrainingCoursesSchedule.Enabled = false;
            this.barButtonTrainingCoursesSchedule.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonTrainingCoursesSchedule.Glyph")));
            this.barButtonTrainingCoursesSchedule.Id = 90;
            this.barButtonTrainingCoursesSchedule.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonTrainingCoursesSchedule.Name = "barButtonTrainingCoursesSchedule";
            this.barButtonTrainingCoursesSchedule.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            superToolTip2.FixedTooltipWidth = true;
            toolTipTitleItem2.Text = "Page Margins";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Select the margin sizes for the entire document.\r\n\r\nTo apply specific margin size" +
                "s to the document, click Custom Margins.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem1);
            superToolTip2.MaxWidth = 210;
            this.barButtonTrainingCoursesSchedule.SuperTip = superToolTip2;
            this.barButtonTrainingCoursesSchedule.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonTrainingCoursesSchedule_ItemClick);
            // 
            // barButtonItemModifyCourses
            // 
            this.barButtonItemModifyCourses.Caption = "Modificar";
            this.barButtonItemModifyCourses.Enabled = false;
            this.barButtonItemModifyCourses.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemModifyCourses.Glyph")));
            this.barButtonItemModifyCourses.Id = 252;
            this.barButtonItemModifyCourses.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemModifyCourses.Name = "barButtonItemModifyCourses";
            this.barButtonItemModifyCourses.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemModifyCourses.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemModifyCourses_ItemClick);
            // 
            // barItemCreateCourses
            // 
            this.barItemCreateCourses.Caption = "Crear";
            this.barItemCreateCourses.Glyph = ((System.Drawing.Image)(resources.GetObject("barItemCreateCourses.Glyph")));
            this.barItemCreateCourses.Id = 253;
            this.barItemCreateCourses.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barItemCreateCourses.Name = "barItemCreateCourses";
            this.barItemCreateCourses.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barItemCreateCourses.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barItemCreateCourses_ItemClick);
            // 
            // barItemDeleteCourses
            // 
            this.barItemDeleteCourses.Caption = "Eliminar";
            this.barItemDeleteCourses.Enabled = false;
            this.barItemDeleteCourses.Glyph = ((System.Drawing.Image)(resources.GetObject("barItemDeleteCourses.Glyph")));
            this.barItemDeleteCourses.Id = 254;
            this.barItemDeleteCourses.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barItemDeleteCourses.Name = "barItemDeleteCourses";
            this.barItemDeleteCourses.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barItemDeleteCourses.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barItemDeleteCourses_ItemClick);
            // 
            // RibbonPageOperation
            // 
            this.RibbonPageOperation.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupTraining});
            this.RibbonPageOperation.Name = "RibbonPageOperation";
            this.RibbonPageOperation.Text = "Operacion";
            // 
            // ribbonPageGroupTraining
            // 
            this.ribbonPageGroupTraining.AllowMinimize = false;
            this.ribbonPageGroupTraining.AllowTextClipping = false;
            this.ribbonPageGroupTraining.ItemLinks.Add(this.barItemCreateCourses);
            this.ribbonPageGroupTraining.ItemLinks.Add(this.barItemDeleteCourses);
            this.ribbonPageGroupTraining.ItemLinks.Add(this.barButtonItemModifyCourses);
            this.ribbonPageGroupTraining.ItemLinks.Add(this.barButtonTrainingCoursesSchedule);
            this.ribbonPageGroupTraining.ItemLinks.Add(this.barButtonOperatorTrainingAssign);
            this.ribbonPageGroupTraining.Kind = DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroupKind.PageSetup;
            this.ribbonPageGroupTraining.Name = "ribbonPageGroupTraining";
            this.ribbonPageGroupTraining.ShowCaptionButton = false;
            superToolTip3.FixedTooltipWidth = true;
            toolTipTitleItem3.Text = "Page Setup";
            toolTipItem2.Appearance.Options.UseImage = true;
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Show the Page Setup dialog.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem2);
            superToolTip3.MaxWidth = 210;
            this.ribbonPageGroupTraining.SuperTip = superToolTip3;
            this.ribbonPageGroupTraining.Text = "Entrenamiento";
            // 
            // layoutControlTrainingCourse
            // 
            this.layoutControlTrainingCourse.AllowCustomizationMenu = false;
            this.layoutControlTrainingCourse.Appearance.DisabledLayoutGroupCaption.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControlTrainingCourse.Appearance.DisabledLayoutGroupCaption.Options.UseForeColor = true;
            this.layoutControlTrainingCourse.Appearance.DisabledLayoutItem.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControlTrainingCourse.Appearance.DisabledLayoutItem.Options.UseForeColor = true;
            this.layoutControlTrainingCourse.Controls.Add(this.gridControlOperators);
            this.layoutControlTrainingCourse.Controls.Add(this.gridControlParts);
            this.layoutControlTrainingCourse.Controls.Add(this.gridControlSchedules);
            this.layoutControlTrainingCourse.Controls.Add(this.gridControlTrainingCourse);
            this.layoutControlTrainingCourse.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlTrainingCourse.Location = new System.Drawing.Point(0, 0);
            this.layoutControlTrainingCourse.Name = "layoutControlTrainingCourse";
            this.layoutControlTrainingCourse.Root = this.layoutControlGroup1;
            this.layoutControlTrainingCourse.Size = new System.Drawing.Size(1270, 787);
            this.layoutControlTrainingCourse.TabIndex = 36;
            this.layoutControlTrainingCourse.Text = "layoutControl1";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupTrainingCourse,
            this.layoutControlGroupSchedules,
            this.layoutControlGroupOperators,
            this.layoutControlGroupParts});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1270, 787);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroupTrainingCourse
            // 
            this.layoutControlGroupTrainingCourse.CustomizationFormText = "layoutControlGroupTrainingCourse";
            this.layoutControlGroupTrainingCourse.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroupTrainingCourse.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupTrainingCourse.Name = "layoutControlGroupTrainingCourse";
            this.layoutControlGroupTrainingCourse.Size = new System.Drawing.Size(1268, 279);
            this.layoutControlGroupTrainingCourse.Text = "layoutControlGroupTrainingCourse";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridControlTrainingCourse;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1262, 255);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroupSchedules
            // 
            this.layoutControlGroupSchedules.CustomizationFormText = "layoutControlGroupSchedules";
            this.layoutControlGroupSchedules.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroupSchedules.Location = new System.Drawing.Point(0, 279);
            this.layoutControlGroupSchedules.Name = "layoutControlGroupSchedules";
            this.layoutControlGroupSchedules.Size = new System.Drawing.Size(1268, 247);
            this.layoutControlGroupSchedules.Text = "layoutControlGroupSchedules";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridControlSchedules;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(1262, 223);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlGroupOperators
            // 
            this.layoutControlGroupOperators.CustomizationFormText = "layoutControlGroupOperators";
            this.layoutControlGroupOperators.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4});
            this.layoutControlGroupOperators.Location = new System.Drawing.Point(633, 526);
            this.layoutControlGroupOperators.Name = "layoutControlGroupOperators";
            this.layoutControlGroupOperators.Size = new System.Drawing.Size(635, 259);
            this.layoutControlGroupOperators.Text = "layoutControlGroupOperators";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.gridControlOperators;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(629, 235);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlGroupParts
            // 
            this.layoutControlGroupParts.CustomizationFormText = "layoutControlGroupParts";
            this.layoutControlGroupParts.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.layoutControlGroupParts.Location = new System.Drawing.Point(0, 526);
            this.layoutControlGroupParts.Name = "layoutControlGroupParts";
            this.layoutControlGroupParts.Size = new System.Drawing.Size(633, 259);
            this.layoutControlGroupParts.Text = "layoutControlGroupParts";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.gridControlParts;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(627, 235);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // TrainingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1270, 787);
            this.Controls.Add(this.RibbonControl);
            this.Controls.Add(this.layoutControlTrainingCourse);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TrainingForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Curso de entrenamiento";
            this.Load += new System.EventHandler(this.TrainingForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlOperators)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewOperators)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlParts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewParts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSchedules)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSchedules)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTrainingCourse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTrainingCourse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlTrainingCourse)).EndInit();
            this.layoutControlTrainingCourse.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupTrainingCourse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupSchedules)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupOperators)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupParts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            this.ResumeLayout(false);

        }              

      

        #endregion

        internal DevExpress.XtraBars.Ribbon.RibbonControl RibbonControl;
        private System.Windows.Forms.ImageList imageList;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem barButtonOperatorTrainingAssign;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem barButtonTrainingCoursesSchedule;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPage RibbonPageOperation;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup ribbonPageGroupTraining;
        internal DevExpress.XtraBars.BarButtonItem barItemCreateCourses;
        internal DevExpress.XtraBars.BarButtonItem barItemDeleteCourses;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemModifyCourses;
        private GridControlEx gridControlTrainingCourse;
        private GridViewEx gridViewTrainingCourse;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnContact;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnContactTelephone;
        private GridControlEx gridControlSchedules;
        private GridViewEx gridViewSchedules;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnScheduleName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnScheduleStart;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnScheduleEnd;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnTrainer;
        private GridControlEx gridControlParts;
        private GridViewEx gridViewParts;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnRoom;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnPartStart;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnPartEnd;
        private GridControlEx gridControlOperators;
        private GridViewEx gridViewOperators;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnOperatorName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnCategory;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnWorkShift;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnStatus;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraLayout.LayoutControl layoutControlTrainingCourse;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupTrainingCourse;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupSchedules;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupOperators;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupParts;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
    }
}