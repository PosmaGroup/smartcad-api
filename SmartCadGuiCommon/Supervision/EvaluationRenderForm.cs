using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Smartmatic.SmartCad.Service;
using System.Reflection;
using System.Collections;
using DevExpress.XtraEditors;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadControls.Controls;

namespace SmartCadGuiCommon
{
    public partial class EvaluationRenderForm : Form
    {
        //array for separation of anwsers.
        private int[] sizeSeparates = new int[3] { 104, 85, 61 };
        private ClientEvaluationScale scale;
        private Dictionary<string, int> questionsAnswers;
        private Dictionary<string, double> questionsWeigth;
        private Dictionary<string, string> questionsNames;

        private bool buttonOkPressed = false;
        
        private string Yes = ResourceLoader.GetString2("$Message.Yes");
        private string No = ResourceLoader.GetString2("$Message.No");

        private OperatorEvaluationClientData operatorEvaluation;
        private ServerServiceClient server; 

        public OperatorEvaluationClientData OperatorEvaluation
        {
            get { return operatorEvaluation; }
            set { operatorEvaluation = value; }
        }
	

        public EvaluationRenderForm()
        {
            InitializeComponent();
            questionsAnswers = new Dictionary<string, int>();
            questionsWeigth = new Dictionary<string, double>();
            questionsNames = new Dictionary<string, string>();
            LoadLenguage();
        }

        public EvaluationRenderForm(EvaluationClientData evaluation, int evaluatorCode, OperatorClientData operatr)
            : this()
        {
            server = ServerServiceClient.GetInstance();
            string categoryName = GetActualCategory(operatr).CategoryFriendlyName; 
            OperatorEvaluation = new OperatorEvaluationClientData();
            operatorEvaluation.EvaluationName = evaluation.Name;
            operatorEvaluation.EvaluatorCode = evaluatorCode;
            operatorEvaluation.Operator = operatr;

            operatorEvaluation.RoleOperator = operatr.RoleFriendlyName;
            operatorEvaluation.Category = categoryName;

            this.labelOperatorName.Text = operatr.FirstName + " "+ operatr.LastName;
            this.labelRole.Text = operatr.RoleFriendlyName;
            this.labelCategory.Text = categoryName;
         
            DateTime date = server.GetTime();
            operatorEvaluation.Date = date;
            this.labelDate.Text = date.ToShortDateString();
       
            this.labelTime.Text =  date.ToShortTimeString();
          
            RenderEvaluation(evaluation);
            this.panelRender.Visible = true;

            OperatorClientData supervisor = new OperatorClientData();
            supervisor.Code = evaluatorCode;
            supervisor = (OperatorClientData)server.RefreshClient(supervisor);
            this.labelExNameEvaluation.Text = evaluation.Name;
            this.labelExEvaluator.Text =  supervisor.FirstName + " " + supervisor.LastName;
  
        }

        public EvaluationRenderForm(OperatorEvaluationClientData opeEva,string qualificationText ) : this()
        {
            /*OperatorEvaluationData data  = new OperatorEvaluationData();
            data.Code = opeEva.Code;
            data = (OperatorEvaluationData)ServerServiceClient.GetInstance().Refresh(data);*/
            server = ServerServiceClient.GetInstance();
			opeEva = (OperatorEvaluationClientData)server.SearchClientObject(SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorEvaluationWithAnswersByCode, opeEva.Code));
			
			this.labelOperatorName.Text = opeEva.Operator.FirstName + " " + opeEva.Operator.LastName;
            this.labelRole.Text = opeEva.RoleOperator;
            this.labelCategory.Text = opeEva.Category;


            this.labelDate.Text = opeEva.Date.ToShortDateString();
            this.labelTime.Text = opeEva.Date.ToShortTimeString();
            RenderPreviousEvaluation(opeEva);
            this.buttonCancel.Text = ResourceLoader.GetString2("Accept");
            this.buttonOK.Visible = false;
            this.buttonOK.Enabled = false;
            this.Text = ResourceLoader.GetString2("ActionPastOperatorEvaluation");
            this.labelQualification.Text = ResourceLoader.GetString2("ScoreEvaluation")+": " + qualificationText;
            this.labelExNameEvaluation.Text =  opeEva.EvaluationName;
            this.labelExEvaluator.Text = opeEva.EvaluatorFirstName + " " + opeEva.EvaluatorLastName;
        }


        private void LoadLenguage()
        {
            labelExEvalName.Text = ResourceLoader.GetString2("Name") + ": ";
            labelTitleEvaluator.Text = ResourceLoader.GetString2("Evaluator") + ": ";
            this.Text = ResourceLoader.GetString2("ActionOperatorEvaluation");
            this.groupControlEvaluationInfo.Text = ResourceLoader.GetString2("EvaluationInfo");
            labelExTitleTime.Text = ResourceLoader.GetString2("Time") + ": ";
            labelExTitleDate.Text = ResourceLoader.GetString2("Date") + ": ";
            this.labelExName.Text = ResourceLoader.GetString2("Name") + ": ";
            this.labelExTitleRole.Text = ResourceLoader.GetString2("SRole") + ": ";
            this.labelExCat.Text = ResourceLoader.GetString2("Category") + ": ";
        }

        private OperatorCategoryHistoryClientData GetActualCategory(OperatorClientData ope)
        {
            foreach (OperatorCategoryHistoryClientData opecat in ope.CategoryList)
            {
                if (opecat.EndDate == DateTime.MinValue)
                {
                    return opecat;
                }
            }
            return null;
        }

        private void RenderPreviousEvaluation(OperatorEvaluationClientData data)
        {
            int index = 0;
            int howMany = 0;
            int indexSeparation = 0;
            switch (data.Scale)
            {
                case ClientEvaluationScale.OneToThree:
                    howMany = 3;
                    indexSeparation = 1;
                    break;
                case ClientEvaluationScale.OneToFive:
                    howMany = 5;
                    indexSeparation = 2;
                    break;
                case ClientEvaluationScale.YesNo:
                    howMany = 2;
                    indexSeparation = 0;
                    break;
                default:
                    break;
            }
            //server.InitializeLazy(data, data.Questions);
            Graphics g = Graphics.FromImage(ResourceLoader.GetImage("ResizeBox"));

            for (int i = 0; i < data.Questions.Count; i++)
            {
                OperatorEvaluationQuestionAnswerClientData question = data.Questions[i] as OperatorEvaluationQuestionAnswerClientData;
                PanelControl panel = new PanelControl();
                index = data.Questions.Count - i;
                panel.Name = index.ToString();
                panel.Size = new Size(433, 90);
                panel.Dock = DockStyle.Top;

                LabelEx labelQuestion = new LabelEx();
                labelQuestion.Text = index + ".     " + question.QuestionName;
                labelQuestion.Location = new Point(23, 23);
                labelQuestion.Font = new Font(this.Font.FontFamily, this.Font.Size, FontStyle.Bold);
                SizeF s = TextRenderer.MeasureText(labelQuestion.Text, labelQuestion.Font);
                labelQuestion.Size = s.ToSize();

                panel.Controls.Add(labelQuestion);

                LabelControl labelAnswer = new LabelControl();
                labelAnswer.Text = ResourceLoader.GetString2("Answer") + ": ";
                labelAnswer.Location = new Point(28, 59);
                s = TextRenderer.MeasureText(labelAnswer.Text, this.Font);
                labelAnswer.Size = s.ToSize();

                panel.Controls.Add(labelAnswer);
                this.panelRender.Controls.Add(panel);
                AssingRadioButtonsWithAnswer(howMany, indexSeparation, panel,question.Answer);
                
            }        
        }

        private void RenderEvaluation(EvaluationClientData evaluation)
        {
            int index = 1;
            int howMany = 0;
            //int indexSeparation = 0;
            switch (evaluation.Scale)
            {
                case EvaluationClientData.EvaluationScales.OneToThree:
                    howMany = 3;
                    //indexSeparation = 1;
                    scale = ClientEvaluationScale.OneToThree;
                    break;
                case EvaluationClientData.EvaluationScales.OneToFive:
                    howMany = 5;
                    //indexSeparation = 2;
                    scale = ClientEvaluationScale.OneToFive;
                    break;
                case EvaluationClientData.EvaluationScales.YesNo:
                    howMany = 2;
                    //indexSeparation = 0;
                    scale = ClientEvaluationScale.YesNo;
                    break;
                default:
                    break;
            }
            if (evaluation.Questions == null)
            {
                evaluation = (EvaluationClientData)server.SearchClientObject(SmartCadHqls.GetCustomHql(SmartCadHqls.GetEvaluationByCode, evaluation.Code));
            }

            for (int i = evaluation.Questions.Count - 1; i >=0 ; i--)
            {
                EvaluationQuestionClientData question = evaluation.Questions[i] as EvaluationQuestionClientData;
                PanelControl panel = new PanelControl();
                index = i + 1;
                panel.Name = index.ToString();
                questionsAnswers.Add(panel.Name, -1);
                questionsWeigth.Add(panel.Name, question.Weighting);
                panel.Size = new Size(433, 90);
                panel.Dock = DockStyle.Top;

                LabelEx labelQuestion = new LabelEx();
                questionsNames.Add(panel.Name, question.Name);
                labelQuestion.Text = index + ".     " + question.Name;
                labelQuestion.Location = new Point(23, 23);
                labelQuestion.Font = new Font(this.Font.FontFamily, this.Font.Size, FontStyle.Bold);
                SizeF s = TextRenderer.MeasureText(labelQuestion.Text, labelQuestion.Font);
                labelQuestion.Size = s.ToSize();

                panel.Controls.Add(labelQuestion);

                LabelControl labelAnswer = new LabelControl();
                labelAnswer.Text = ResourceLoader.GetString2("Answer") + ": ";
                labelAnswer.Location = new Point(28, 59);
                s = TextRenderer.MeasureText(labelAnswer.Text, this.Font);
                labelAnswer.Size = s.ToSize();
                panel.Controls.Add(labelAnswer);
                this.panelRender.Controls.Add(panel);
                AssingRadioButtons(howMany, 7, panel);
            }            
        }

        private void AssingRadioButtonsWithAnswer(int howMany, int indexSeparation, PanelControl panel,int answer)
        {
            int x = 133;
            int y = 57;
            RadioGroup rg = new RadioGroup();
            rg.Properties.ReadOnly = true;
            rg.Location = new Point(x, y);
            rg.Size = new Size(323, 30);
            rg.SelectedIndex = -1;
            if (howMany > 2)
            {
                for (int i = 0; i < howMany; i++)
                {
                    DevExpress.XtraEditors.Controls.RadioGroupItem rgi = new DevExpress.XtraEditors.Controls.RadioGroupItem();
                    rgi.Description = (i + 1) + "";
                    rg.Properties.Items.Add(rgi);
                    rgi.Changed += new EventHandler(radioButton_CheckedChanged);
                }
                rg.SelectedIndex = answer - 1;
                panel.Controls.Add(rg);
            }
            else
            {
                for (int i = 0; i < howMany; i++)
                {
                    DevExpress.XtraEditors.Controls.RadioGroupItem rgi = new DevExpress.XtraEditors.Controls.RadioGroupItem();
                    rgi.Description = (i == 0) ? (Yes) : (No);
                    rg.Properties.Items.Add(rgi);
                    rgi.Changed += new EventHandler(radioButton_CheckedChanged);
                }
                panel.Controls.Add(rg);
                rg.SelectedIndex = (answer + 1)%2;
            }
        }

        private void AssingRadioButtons(int howMany, int xss, PanelControl panel)
        {
            int x = 133;
            int y = 57;
            RadioGroup rg = new RadioGroup();
            rg.Properties.EnableFocusRect = false;
            rg.Location = new Point(x, y);
            rg.Size = new Size(323, 30);
            rg.SelectedIndex = -1;
            rg.SelectedIndexChanged += new EventHandler(radioButton_CheckedChanged);
            rg.Properties.MouseWheel += new MouseEventHandler(Properties_MouseWheel);
            if (howMany > 2)
            {
                for (int i = 0; i < howMany; i++)
                {
                    DevExpress.XtraEditors.Controls.RadioGroupItem rgi = new DevExpress.XtraEditors.Controls.RadioGroupItem();
                    rgi.Description = (i + 1) + "";
                    rg.Properties.Items.Add(rgi);
                    rgi.Changed += new EventHandler(radioButton_CheckedChanged);
                }
                panel.Controls.Add(rg);
            }
            else 
            {
                for (int i = 0; i < howMany; i++)
                {
                    DevExpress.XtraEditors.Controls.RadioGroupItem rgi = new DevExpress.XtraEditors.Controls.RadioGroupItem();
                    rgi.Description = (i == 0) ? (Yes) : (No);
                    rg.Properties.Items.Add(rgi);
                    rgi.Changed+= new EventHandler(radioButton_CheckedChanged);
                }
                panel.Controls.Add(rg);
            }
        }


        void Properties_MouseWheel(object sender, MouseEventArgs e)
        {
            ((DevExpress.Utils.DXMouseEventArgs)e).Handled = true;
        }

        void radioButton_CheckedChanged(object sender, EventArgs e)
        {
            RadioGroup rg = sender as RadioGroup;
            string key = rg.Parent.Name;
            if (questionsAnswers.ContainsKey(key)) 
            {
                switch (scale) 
                { 
                    case ClientEvaluationScale.YesNo:
                        questionsAnswers[key] = (rg.SelectedIndex+1)%2;
                        break;
                    case ClientEvaluationScale.OneToFive:
                    case ClientEvaluationScale.OneToThree:
                        questionsAnswers[key] = rg.SelectedIndex + 1;
                        break;

                }
                 
            }
            this.panelRender.Focus();
            EvaluationParameter_Change(null, null);
        }

        private int PrivateParse(string text)
        {

            if (text == Yes) 
            { 
                return 1;
            }
            else if(text == No)
            {
                return 0;
            }
            else
            {
                return Int32.Parse(text);
            }
        }


        private void EvaluationParameter_Change(object sender, EventArgs e)
        {

            bool lesszero = false;
            foreach (int var in questionsAnswers.Values) 
            {
                if (var < 0) 
                {
                    lesszero = true;
                    break;
                }
            }
            if (lesszero)
                this.buttonOK.Enabled = false;
            else
                this.buttonOK.Enabled = true;
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            try
            {
                buttonOkPressed = true;
                
                BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[1] { GetType().GetMethod("buttonOk_Help", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[0] });
                processForm.CanThrowError = true;
                processForm.ShowDialog();
            }
            catch (Exception ex)
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, ex);
                buttonOkPressed = false;
            }
        }

        private void buttonOk_Help() 
        {
            ArrayList list = new ArrayList();
            if (scale == ClientEvaluationScale.YesNo)
            {
                int howManyYes = 0;
                foreach (string var in questionsAnswers.Keys)
                {
                    OperatorEvaluationQuestionAnswerClientData item = new OperatorEvaluationQuestionAnswerClientData();
                    item.Answer = questionsAnswers[var];
                    item.QuestionName = questionsNames[var];
                    list.Add(item);
                    howManyYes += questionsAnswers[var];
                }
                operatorEvaluation.Qualification = (howManyYes * 100) / questionsAnswers.Count;
                operatorEvaluation.Questions = list;
            }
            else
            {
                double qualification = 0.0;
                foreach (string var in questionsAnswers.Keys)
                {
                    OperatorEvaluationQuestionAnswerClientData item = new OperatorEvaluationQuestionAnswerClientData();
                    item.Answer = questionsAnswers[var];
                    item.QuestionName = questionsNames[var];
                    list.Add(item);
                    qualification += (questionsWeigth[var] * 0.01) * questionsAnswers[var];
                }
                //add 0.5 to round with casting.
                qualification += 0.5;
                operatorEvaluation.Qualification = (int)qualification;
                operatorEvaluation.Questions = list;
            }
            operatorEvaluation.Scale = scale;
            server.SaveOrUpdateClientData(operatorEvaluation);
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            bool showMessage = false;
            foreach (int var in questionsAnswers.Values)
            {
                if (var > 0)
                {
                    showMessage = true;
                    break;
                }       
            }
            if (showMessage)
            {
                DialogResult res = MessageForm.Show(ResourceLoader.GetString2("QuestionCancelEvaluationMessage"), MessageFormType.WarningQuestion);
                if (res == DialogResult.No)
                {
                    this.DialogResult = DialogResult.None;
                }
            }
                
        }

    }
}
