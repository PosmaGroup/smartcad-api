using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Collections;
using System.ServiceModel;
using System.IO;
using System.Xml;
using System.Xml.Xsl;
using System.Reflection;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Utils;
using DevExpress.XtraGrid.Columns;
using SmartCadCore.ClientData;
using SmartCadControls.SyncBoxes;
using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadControls;
using SmartCadGuiCommon.Util;
using SmartCadControls.Controls;
using Smartmatic.SmartCad.Service;

namespace SmartCadGuiCommon
{
    public partial class IncidentNotificationDistributionForm : DevExpress.XtraEditors.XtraForm 
    {
        #region Synchronization objects

        private object syncCommited = new object();
        private bool removingNotification = false;
        private bool assigninigNotification = false;

        #endregion

        #region Fields and constants
        private GridControlSynBox operatorSynBox;
        private GridControlSynBox notificationInProgressSyncBox;
        private GridControlSynBox notificationToDistributeSyncBox;

        private const int SINGLE_SELECTION = 1;
        private XslCompiledTransform xslCompiledTransform;
        private XslCompiledTransform xslCctvCompiledTransform;
        private SkinEngine skinEngine;
        private const float FONT_SIZE = 9.75f;
        private System.Threading.Timer refreshIncidentReportRight;
        private System.Threading.Timer refreshIncidentReportLeft;
        private DepartmentTypeClientData selectedDepartment;
        private bool isGeneralSupervisor;
        private bool clickInRow = false;
        private bool leftDetailsActive = false;
        #endregion

        public IncidentNotificationDistributionForm(DepartmentTypeClientData department, bool isGeneralSupervisor)
        {
            InitializeComponent();
            this.isGeneralSupervisor = isGeneralSupervisor;
            operatorSynBox = new GridControlSynBox(gridControlExOperators);
            gridControlExOperators.EnableAutoFilter = true;
            gridControlExOperators.Type = typeof(GridControlDataOperatorNotificationDistribution);
            gridControlExOperators.ViewTotalRows = true;
            
            notificationInProgressSyncBox = new GridControlSynBox(gridControlExNotificationInProgress);
            gridControlExNotificationInProgress.EnableAutoFilter = true;
            gridControlExNotificationInProgress.Type = typeof(GridControlDataNotificationInProgress);
            gridControlExNotificationInProgress.ViewTotalRows = true;
            
            notificationToDistributeSyncBox = new GridControlSynBox(gridControlExNotificationDistribution);
            gridControlExNotificationDistribution.EnableAutoFilter = true;
            gridControlExNotificationDistribution.Type = typeof(GridControlDataNotificationToDistribute);
            gridControlExNotificationDistribution.ViewTotalRows = true;
            
            refreshIncidentReportRight = new System.Threading.Timer(new TimerCallback(OnRefreshIncidentReportTimerRight));
            refreshIncidentReportLeft = new System.Threading.Timer(new TimerCallback(OnRefreshIncidentReportTimerLeft));
            barButtonItemPrint.Enabled = false;
            barButtonItemSave.Enabled = false;
            barButtonItemUpdate.Enabled = false;
            barButtonItemSend.Enabled = false;
            
            selectedDepartment = department;
            if (isGeneralSupervisor)
            {
                gridControlExOperators.SetColumnVisible("Supervisor", true);                
            }
        }

        public DepartmentTypeClientData SelectedDepartment
        {
            get
            {
                return selectedDepartment;
            }
            set
            {
                selectedDepartment = value;
                FormUtil.InvokeRequired(this,
                    delegate
                    {
                        if (this.gridControlExOperators.DataSource != null)
                        {
                            (this.gridControlExOperators.DataSource as IList).Clear();
                        }
                        this.gridControlExNotificationInProgress.ClearData();
                        this.gridControlExNotificationDistribution.ClearData();
                    });
                if (value!=null)
                {
                    LoadData();
                }

            }
        }

        private void OnRefreshIncidentReportTimerRight(object state)
        {
            refreshIncidentReportRight.Change(Timeout.Infinite, Timeout.Infinite);

            ThreadPool.QueueUserWorkItem(delegate
            {
                try
                {
                    PerformIncidentReport(gridControlExNotificationInProgress);
                }
                catch (Exception ex)
                {
                    SmartLogger.Print(ex);
                }
            });
        }

        private void OnRefreshIncidentReportTimerLeft(object state)
        {
            refreshIncidentReportLeft.Change(Timeout.Infinite, Timeout.Infinite);
            ThreadPool.QueueUserWorkItem(delegate
            {
                try
                {
                    PerformIncidentReport(gridControlExNotificationDistribution);
                    foreach (var t in gridControlExNotificationDistribution.Items)
                    { 
                    }
                }
                catch (Exception ex)
                {
                    SmartLogger.Print(ex);
                }
            });
        }

        private void IncidentNotificationDistributionForm_Load(object sender, EventArgs e)
        {
            (this.MdiParent as SupervisionForm).SupervisionCommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(IncidentNotificationDistributionForm_CommittedChanges);
            //(this.MdiParent as SupervisionForm).SupervisedApplicationChanged += new EventHandler<SupervisedApplicationChangedEventArgs>(IncidentNotificationDistributionForm_SupervisedApplicationChanged);
            (gridControlExOperators.MainView as GridViewEx).SelectionWillChange +=new FocusedRowChangedEventHandler(gridControlExOperators_SelectionWillChange);
            (gridControlExOperators.MainView as GridViewEx).SingleSelectionChanged += new FocusedRowChangedEventHandler(gridControlExOperators_SelectionChanged);
            (gridControlExNotificationInProgress.MainView as GridViewEx).SelectionWillChange += new FocusedRowChangedEventHandler(gridControlExNotificationInProgress_SelectionWillChange);
            (gridControlExNotificationInProgress.MainView as GridViewEx).SingleSelectionChanged += new FocusedRowChangedEventHandler(gridControlExNotificationInProgress_SelectionChanged);            
            (gridControlExNotificationDistribution.MainView as GridViewEx).SelectionWillChange += new FocusedRowChangedEventHandler(gridControlExNotificationDistribution_SelectionWillChange);
            (gridControlExNotificationDistribution.MainView as GridViewEx).SingleSelectionChanged += new FocusedRowChangedEventHandler(gridControlExNotificationDistribution_SelectionChanged);
            
            SetInitialFilters();
            LoadResources();
            LoadData();
            SetSkin();
            XmlReaderSettings xmlReaderSettings = new XmlReaderSettings();
            xmlReaderSettings.ProhibitDtd = false;
            xslCompiledTransform = new XslCompiledTransform();
            xslCctvCompiledTransform = new XslCompiledTransform();
            xslCctvCompiledTransform.Load(XmlReader.Create(SmartCadConfiguration.CctvIncidentXslt, xmlReaderSettings));
            xslCompiledTransform.Load(XmlReader.Create(SmartCadConfiguration.IncidentXslt, xmlReaderSettings));
            this.Activated += new EventHandler(IncidentNotificationDistributionForm_Activated);
            this.simpleButtonAssign.Text = ResourceLoader.GetString2("Assign");
        }

        void IncidentNotificationDistributionForm_Activated(object sender, EventArgs e)
        {
            if (this.MdiParent != null)
                (this.MdiParent as SupervisionForm).RibbonControl.SelectedPage = (this.MdiParent as SupervisionForm).RibbonPageOperation;
    
        }




        void IncidentNotificationDistributionForm_SupervisedApplicationChanged(object sender, SupervisedApplicationChangedEventArgs e)
        {
            if (e.ApplicationName == UserApplicationClientData.Dispatch.Name)
            {
                SelectedDepartment = e.DepartmentType;
            }
            else
            {
                this.Close();
            }
        }

        private void SetInitialFilters()
        {
            (gridControlExOperators.MainView as GridView).ClearColumnsFilter();
        }

        private void SetSkin()
        {
            try
            {
                string skinFile = SmartCadConfiguration.SkinsFolder + "/Clasic.xml";

                skinEngine = SkinEngine.Load(skinFile);

                ProfessionalColorTable colorTable = new CustomToolStripColors(
                    skinEngine.Colors["ToolStripGradientBegin"],
                    skinEngine.Colors["ToolStripGradientMiddle"],
                    skinEngine.Colors["ToolStripGradientEnd"],

                    skinEngine.Colors["MenuStripGradientBegin"],
                    skinEngine.Colors["MenuStripGradientEnd"],

                    skinEngine.Colors["StatusStripGradientBegin"],
                    skinEngine.Colors["StatusStripGradientEnd"]
                );

                skinEngine.AddElement("background", this);


                skinEngine.AddCompleteElement(this);

                skinEngine.ApplyChanges();
            }
            catch (Exception ex)
            {
                MessageForm.Show(ResourceLoader.GetString2("$Message.Error"), ResourceLoader.GetString2("ErrorLoadingSkin"), ex);
            }
        }

        private void LoadResources()
        {
            this.Text = ResourceLoader.GetString2("DistributionDispatchNotification");
            this.layoutControlGroupNotifications.Text = ResourceLoader.GetString2("NotificationToDistribute");
            this.layoutControlGroupNotificationsInProcess.Text = "  " + ResourceLoader.GetString2("labelNotifInProgressOf");
            this.layoutControlGroupIncidentDetailsLeft.Text = ResourceLoader.GetString2("IncidentDetails");
            this.layoutControlGroupIncidentDetailsRight.Text = ResourceLoader.GetString2("IncidentDetails");
            this.simpleButtonRemove.Text = ResourceLoader.GetString2("Remove");
            this.contextMenuStripNotificationsInProgress.Text = ResourceLoader.GetString2("Remove");
            this.quitarToolStripMenuItem.Text = ResourceLoader.GetString2("Remove");
            this.layoutControlGroupOperators.Text = ResourceLoader.GetString2("Operators");
            barButtonItemPrint.Caption = ResourceLoader.GetString2("Print");
            barButtonItemSave.Caption = ResourceLoader.GetString2("Save");
            barButtonItemSend.Caption = ResourceLoader.GetString2("Send");
            barButtonItemUpdate.Caption = ResourceLoader.GetString2("Update");
            this.RibbonPageOperation.Text = ResourceLoader.GetString2("Operation");
            this.ribbonPageGroupOptions.Text = ResourceLoader.GetString2("GeneralOptions");
        }

        private void LoadData()
        {
            //Load all notifications in manual supervision state
            try
            {
                if (ServerServiceClient.GetInstance().OperatorClient.DepartmentTypes.Contains(SelectedDepartment))
                {
                    IList notifications = ServerServiceClient.GetInstance().SearchClientObjects(
                        SmartCadHqls.GetCustomHql(SmartCadHqls.GetIncidentNotificationInSupervisorStatusByDepartment,
                        SelectedDepartment.Code));
                    IList filteredNotifications = new ArrayList();
                    foreach (IncidentNotificationClientData notification in notifications)
                    {
                        if (SelectedDepartment.Code == notification.DepartmentStation.DepartmentZone.DepartmentType.Code &&
                            ServerServiceClient.GetInstance().OperatorClient.DepartmentTypes.Contains(notification.DepartmentType))
                        {
                            filteredNotifications.Add(notification);
                        }
                    }
                    if (filteredNotifications.Count > 0)
                    {
                        notificationToDistributeSyncBox.Sync(filteredNotifications);

                    }
                }
            }
            catch (Exception ex)
            {
                MessageForm.Show(ex.Message, ex);
            }
            //Load logged users
            try
            {
                IList supervisedUsers = ServerServiceClient.GetInstance().GetSupervisedOperators(
                    UserApplicationClientData.Dispatch.Name, SelectedDepartment.Code);
                IList loggedUsers = GetLoggedUsers(supervisedUsers);
                if (loggedUsers.Count > 0)
                {
                    operatorSynBox.Sync(loggedUsers);
                }
            }
            catch(Exception ex)
            {
                MessageForm.Show(ex.Message, ex);
            }
        }

        private IList GetLoggedUsers(IList users)
        {
            IList result = new ArrayList();
            foreach (OperatorClientData oper in users)
            {
                if (oper.LastSessions != null)
                {
                    SessionHistoryClientData lastSession = GetLastSessionHistory(oper.LastSessions, "Dispatch");
                    if (lastSession != null && lastSession.IsLoggedIn.Value == true)
                    {
                        result.Add(oper);
                    }
                }
            }
            return result;
        }

        void IncidentNotificationDistributionForm_CommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    lock (syncCommited)
                    {
                        try
                        {
                            if (ServerServiceClient.GetInstance().OperatorClient == null)
                            { }
                        }
                        catch
                        { }
                        if (e.Objects[0] is IncidentNotificationClientData)
                        {
                            #region IncidentNotificationClientData
                            IncidentNotificationClientData incidentNotificationClient = e.Objects[0] as IncidentNotificationClientData;
                            IncidentNotificationClientData selectedNotification = null;
                            if (incidentNotificationClient.Status.Name.Equals(IncidentNotificationStatusClientData.AutomaticSupervisor.Name) || //In supervisor 
                                incidentNotificationClient.Status.Name.Equals(IncidentNotificationStatusClientData.ManualSupervisor.Name))
                            {
                                if (incidentNotificationClient.DepartmentStation.DepartmentZone.DepartmentType.Code == SelectedDepartment.Code)
                                {
                                    notificationToDistributeSyncBox.Sync(new GridControlDataNotificationToDistribute(incidentNotificationClient), e.Action);
                                    FormUtil.InvokeRequired(gridControlExNotificationDistribution,
                                    delegate
                                    {
                                        if (gridControlExNotificationDistribution.SelectedItems.Count == SINGLE_SELECTION)
                                        {
                                            selectedNotification = (gridControlExNotificationDistribution.SelectedItems[0] as GridControlData).Tag as IncidentNotificationClientData;
                                        }
                                    });
                                    if (selectedNotification != null && selectedNotification.IncidentCode == incidentNotificationClient.IncidentCode)
                                    {
                                        PerformIncidentReport(gridControlExNotificationDistribution);
                                    }
                                }
                                notificationInProgressSyncBox.Sync(new GridControlDataNotificationInProgress(incidentNotificationClient), CommittedDataAction.Delete);
                                FormUtil.InvokeRequired(gridControlExNotificationInProgress,
                                    delegate
                                    {
                                        if (gridControlExNotificationInProgress.SelectedItems.Count == SINGLE_SELECTION)
                                        {
                                            selectedNotification = (gridControlExNotificationInProgress.SelectedItems[0] as GridControlData).Tag as IncidentNotificationClientData;
                                        }
                                    });
                                if (selectedNotification != null && selectedNotification.IncidentCode == incidentNotificationClient.IncidentCode)
                                {
                                    PerformIncidentReport(gridControlExNotificationInProgress);
                                }                                
                            }
                            else if (incidentNotificationClient.Status.Name.Equals(IncidentNotificationStatusClientData.Cancelled.Name) ||
                                    incidentNotificationClient.Status.Name.Equals(IncidentNotificationStatusClientData.Closed.Name))
                            {
                                notificationToDistributeSyncBox.Sync(new GridControlDataNotificationToDistribute(incidentNotificationClient), CommittedDataAction.Delete);
                                FormUtil.InvokeRequired(gridControlExNotificationDistribution,
                                    delegate
                                    {
                                        if (gridControlExNotificationDistribution.SelectedItems.Count == SINGLE_SELECTION)
                                        {
                                            selectedNotification = (gridControlExNotificationDistribution.SelectedItems[0] as GridControlData).Tag as IncidentNotificationClientData;
                                        }
                                    });
                                if (selectedNotification != null && selectedNotification.IncidentCode == incidentNotificationClient.IncidentCode)
                                {
                                    PerformIncidentReport(gridControlExNotificationDistribution);
                                }
                                notificationInProgressSyncBox.Sync(new GridControlDataNotificationInProgress(incidentNotificationClient), CommittedDataAction.Delete);
                                FormUtil.InvokeRequired(gridControlExNotificationInProgress,
                                    delegate
                                    {
                                        if (gridControlExNotificationInProgress.SelectedItems.Count == SINGLE_SELECTION)
                                        {
                                            selectedNotification = (gridControlExNotificationInProgress.SelectedItems[0] as GridControlData).Tag as IncidentNotificationClientData;
                                        }
                                    });
                                if (selectedNotification != null && selectedNotification.IncidentCode == incidentNotificationClient.IncidentCode)
                                {
                                    PerformIncidentReport(gridControlExNotificationInProgress);
                                }
                            }
                            else if (!incidentNotificationClient.Status.Name.Equals(IncidentNotificationStatusClientData.New.Name))
                            {
                                if (incidentNotificationClient.DepartmentStation.DepartmentZone.DepartmentType.Code == SelectedDepartment.Code)
                                {
                                    notificationToDistributeSyncBox.Sync(new GridControlDataNotificationToDistribute(incidentNotificationClient), CommittedDataAction.Delete);
                                    FormUtil.InvokeRequired(gridControlExNotificationDistribution,
                                    delegate
                                    {
                                        if (gridControlExNotificationDistribution.SelectedItems.Count == SINGLE_SELECTION)
                                        {
                                            selectedNotification = (gridControlExNotificationDistribution.SelectedItems[0] as GridControlData).Tag as IncidentNotificationClientData;
                                        }
                                    });
                                    if (selectedNotification != null && selectedNotification.IncidentCode == incidentNotificationClient.IncidentCode)
                                    {
                                        PerformIncidentReport(gridControlExNotificationDistribution);
                                    }
                                }
                                OperatorClientData selectedOperator = null;
                                FormUtil.InvokeRequired(gridControlExOperators,
                                    delegate
                                    {
                                        if (gridControlExOperators.SelectedItems.Count > 0)
                                        {
                                            selectedOperator =
                                                (gridControlExOperators.SelectedItems[0] as GridControlDataOperatorNotificationDistribution).OperatorClient;
                                        }
                                    });
                                if (selectedOperator != null && incidentNotificationClient.DispatchOperatorCode == selectedOperator.Code)
                                {
                                    GridControlDataNotificationInProgress gridData = new GridControlDataNotificationInProgress(incidentNotificationClient);
                                    if (e.Action == CommittedDataAction.Update || e.Action == CommittedDataAction.Save)
                                    {
                                        gridControlExOperators_SelectionChanged(this.gridControlExOperators.MainView as GridView, null);
                                    }
                                    FormUtil.InvokeRequired(gridControlExNotificationInProgress,
                                    delegate
                                    {
                                        if (gridControlExNotificationInProgress.SelectedItems.Count == SINGLE_SELECTION)
                                        {
                                            selectedNotification = (gridControlExNotificationInProgress.SelectedItems[0] as GridControlData).Tag as IncidentNotificationClientData;
                                        }
                                    });
                                    if (selectedNotification != null && selectedNotification.IncidentCode == incidentNotificationClient.IncidentCode)
                                    {
                                        PerformIncidentReport(gridControlExNotificationInProgress);
                                    }
                                }
                            }
                            
                            EnableButtonsAndMenus(gridControlExNotificationDistribution);
                            
                            #endregion
                        }
                        else if (e.Objects[0] is OperatorClientData)
                        {
                            #region OperatorClientData
                            OperatorClientData oper = e.Objects[0] as OperatorClientData;
                            ProcessReceivedOperatorClient(oper, e.Action);
                            #endregion
                        }
                        else if (e.Objects[0] is OperatorLightClientData)
                        {
                            foreach (OperatorLightClientData oper in e.Objects)
                            {
                                GridControlData gridData = GetObjectInGridControl(gridControlExOperators, oper);
                                if (gridData != null && gridData.Tag.Code == oper.Code)
                                {
                                    if (oper.Delete == true || oper.DispatchAccess == false)
                                    {
                                        operatorSynBox.Sync(gridData, CommittedDataAction.Delete);
                                    }
                                }
                                else if (gridData == null && oper.Delete == false)
                                {
                                    OperatorClientData clientOperator = new OperatorClientData();
                                    clientOperator.Code = oper.Code;
                                    clientOperator =
                                        ServerServiceClient.GetInstance().RefreshClient(clientOperator) as OperatorClientData;
                                    if (clientOperator != null)
                                    {
                                        ProcessReceivedOperatorClient(clientOperator, CommittedDataAction.Update);
                                    }
                                }
                            }
                        }
                        else if (e.Objects[0] is WorkShiftVariationClientData)
                        {
                            #region WorkShiftVariationClientData

                            WorkShiftVariationClientData wsVariation = e.Objects[0] as WorkShiftVariationClientData;
                        
                            if (e.Action == CommittedDataAction.Delete)
                            {
                                if (ServerServiceClient.GetInstance().OperatorClient.WorkShifts != null &&
                                    ServerServiceClient.GetInstance().OperatorClient.WorkShifts.Contains(wsVariation))
                                {
                                    ServerServiceClient.GetInstance().OperatorClient.WorkShifts.Remove(wsVariation);
                                }
                                
                                IList operatorsToDelete = GetOperatorsByWorkshift(wsVariation);
                                if (operatorsToDelete.Count > 0)
                                {
                                    foreach (OperatorClientData oper in operatorsToDelete)
                                    {
                                        operatorSynBox.Sync(new GridControlDataOperatorNotificationDistribution(oper), CommittedDataAction.Delete);
                                    }
                                }
                            }
                            else
                            {
                                List<GridControlDataOperatorNotificationDistribution> operatorsToDelete = new List<GridControlDataOperatorNotificationDistribution>();
                                FormUtil.InvokeRequired(this,
                                    delegate
                                    {
                                        if (this.gridControlExOperators.DataSource != null)
                                        {
                                            foreach (GridControlDataOperatorNotificationDistribution gridOper in this.gridControlExOperators.DataSource as BindingList<GridControlDataOperatorNotificationDistribution>)
                                            {
                                                if (gridOper.OperatorClient.WorkShifts != null && gridOper.OperatorClient.WorkShifts.Contains(wsVariation))
                                                {
                                                    operatorsToDelete.Add(gridOper);
                                                }
                                            }
                                        }
                                    });
                                IList operatorsToUpdate =
                                    ServerServiceClient.GetInstance().GetOperatorsWorkingNowByAppByWorkShift(UserApplicationClientData.Dispatch.Name, wsVariation.Code);
                               
                                int index = operatorsToUpdate.IndexOf(ServerServiceClient.GetInstance().OperatorClient);
                                if (index == -1)
                                {
                                    IList supervisorWorkShift = ServerServiceClient.GetInstance().OperatorClient.WorkShifts;
                                    if (supervisorWorkShift != null && supervisorWorkShift.Contains(wsVariation.Code))
                                    {
                                        ServerServiceClient.GetInstance().OperatorClient.WorkShifts.Remove(wsVariation);
                                        //ServerServiceClient.GetInstance().UserAccount.WorkShift = null;
                                    }
                                }
                                else
                                {
                                    int inx = ServerServiceClient.GetInstance().OperatorClient.WorkShifts.IndexOf(wsVariation);
                                    if (inx > -1)
                                        ServerServiceClient.GetInstance().OperatorClient.WorkShifts[inx] = wsVariation;
                                    else
                                        ServerServiceClient.GetInstance().OperatorClient.WorkShifts.Add(wsVariation);

                                    IList supervisedUsers = ServerServiceClient.GetInstance().GetSupervisedOperators(
                                        "Dispatch", selectedDepartment.Code);
                                    IList loggedUsers = GetLoggedUsers(supervisedUsers);
                                    if (loggedUsers.Count > 0)
                                    {
                                        operatorSynBox.Sync(loggedUsers);
                                    }
                                }
                                foreach (OperatorClientData oper in operatorsToUpdate)
                                {
                                    if (!oper.IsSupervisor)
                                    {
                                        GridControlDataOperatorNotificationDistribution gridData = new GridControlDataOperatorNotificationDistribution(oper);
                                        int index2 = operatorsToDelete.IndexOf(gridData);
                                        if (index2 != -1)
                                        {
                                            operatorsToDelete.RemoveAt(index2);
                                        }
                                        ProcessReceivedOperatorClient(oper, e.Action);
                                    }
                                }

                                FormUtil.InvokeRequired(this,
                                    delegate
                                    {
                                        foreach (GridControlDataOperatorNotificationDistribution gridData in operatorsToDelete)
                                        {
                                            gridControlExOperators.DeleteItem(gridData);
                                        }
                                    });
                            }
                            #endregion
                        }
                        else if (e.Objects[0] is OperatorAssignClientData)
                        {
                            #region OperatorAssignClientData
                            //OperatorAssignClientData assign = e.Objects[0] as OperatorAssignClientData;
                            foreach (OperatorAssignClientData assign in e.Objects)
                            {
                                OperatorClientData oper = new OperatorClientData();
                                oper.Code = assign.SupervisedOperatorCode;
                                oper = (OperatorClientData)ServerServiceClient.GetInstance().RefreshClient(oper);
                                if (oper != null)
                                {
                                    SessionHistoryClientData lastSession = GetLastSessionHistory(oper.LastSessions, UserApplicationClientData.Dispatch.Name);
									bool canBeSupervised = ServerServiceClient.GetInstance().CheckOperatorClientAccess(oper, UserApplicationClientData.Dispatch.Name);
                                    if (oper.IsSupervisor == false &&
                                            (assign.SupervisorCode == ServerServiceClient.GetInstance().OperatorClient.Code ||
                                              (isGeneralSupervisor && canBeSupervised)))
                                    {
                                        GridControlDataOperatorNotificationDistribution gridData = new GridControlDataOperatorNotificationDistribution(oper);

                                        if (lastSession != null && lastSession.IsLoggedIn.Value)
                                        {
                                            if (isGeneralSupervisor)
                                                operatorSynBox.Sync(gridData, CommittedDataAction.Update);
                                            else
                                                operatorSynBox.Sync(gridData, e.Action);
                                        }
                                        else
                                        {
                                            operatorSynBox.Sync(gridData, CommittedDataAction.Delete);
                                        }

                                    }
                                }
                            }
                            #endregion
                        }
                        else if (e.Objects[0] is DepartmentTypeClientData)
                        {
                            #region DepartmentTypeClientData
                            DepartmentTypeClientData deptType = e.Objects[0] as DepartmentTypeClientData;
                            if (e.Action == CommittedDataAction.Update)
                            {
                                BindingList<GridControlDataNotificationToDistribute> dataSource =
                                    gridControlExNotificationDistribution.DataSource as BindingList<GridControlDataNotificationToDistribute>;
                                int count = dataSource.Count;
                                for (int i = 0; i < count; i++)
                                {
                                    if (dataSource[i].IncidentNotification.DepartmentType.Equals(deptType))
                                    {
                                        dataSource[i].IncidentNotification.DepartmentType = deptType;
                                        notificationToDistributeSyncBox.Sync(dataSource[i], CommittedDataAction.Update);
                                    }
                                }

                                BindingList<GridControlDataNotificationInProgress> dataSourceNiP =
                                    gridControlExNotificationInProgress.DataSource as BindingList<GridControlDataNotificationInProgress>;
                                count = dataSourceNiP.Count;
                                for (int i = 0; i < count; i++)
                                {
                                    if (dataSourceNiP[i].IncidentNotification.DepartmentType.Equals(deptType))
                                    {
                                        dataSourceNiP[i].IncidentNotification.DepartmentType = deptType;
                                        notificationInProgressSyncBox.Sync(dataSourceNiP[i], CommittedDataAction.Update);
                                    }
                                }

                            }
                            #endregion
                        }
                        else if (e.Objects[0] is DepartmentZoneClientData)
                        {
                            #region DepartmentZoneClientData
                            DepartmentZoneClientData zone = e.Objects[0] as DepartmentZoneClientData;
                            if (e.Action == CommittedDataAction.Update)
                            {
                                BindingList<GridControlDataNotificationToDistribute> dataSource =
                                    gridControlExNotificationDistribution.DataSource as BindingList<GridControlDataNotificationToDistribute>;
                                int count = dataSource.Count;
                                for (int i = 0; i < count; i++)
                                {
                                    if (dataSource[i].IncidentNotification.DepartmentStation.DepartmentZone.Equals(zone))
                                    {
                                        dataSource[i].IncidentNotification.DepartmentStation.DepartmentZone = zone;
                                        notificationToDistributeSyncBox.Sync(dataSource[i], CommittedDataAction.Update);
                                    }
                                }

                                BindingList<GridControlDataNotificationInProgress> dataSourceNiP =
                                    gridControlExNotificationInProgress.DataSource as BindingList<GridControlDataNotificationInProgress>;
                                count = dataSourceNiP.Count;
                                for (int i = 0; i < count; i++)
                                {
                                    if (dataSourceNiP[i].IncidentNotification.DepartmentStation.DepartmentZone.Equals(zone))
                                    {
                                        dataSourceNiP[i].IncidentNotification.DepartmentStation.DepartmentZone = zone;
                                        notificationInProgressSyncBox.Sync(dataSourceNiP[i], CommittedDataAction.Update);
                                    }
                                }
                            }
                            #endregion
                        }
                        else if (e.Objects[0] is DepartmentStationClientData)
                        {
                            #region DepartmentStationClientData
                            DepartmentStationClientData station = e.Objects[0] as DepartmentStationClientData;
                            if (e.Action == CommittedDataAction.Update)
                            {
                                BindingList<GridControlDataNotificationToDistribute> dataSource =
                                    gridControlExNotificationDistribution.DataSource as BindingList<GridControlDataNotificationToDistribute>;
                                int count = dataSource.Count;
                                for (int i = 0; i < count; i++)
                                {
                                    if (dataSource[i].IncidentNotification.DepartmentStation.Equals(station))
                                    {
                                        dataSource[i].IncidentNotification.DepartmentStation = station;
                                        notificationToDistributeSyncBox.Sync(dataSource[i], CommittedDataAction.Update);
                                    }
                                }

                                BindingList<GridControlDataNotificationInProgress> dataSourceNiP =
                                    gridControlExNotificationInProgress.DataSource as BindingList<GridControlDataNotificationInProgress>;
                                count = dataSourceNiP.Count;
                                for (int i = 0; i < count; i++)
                                {
                                    if (dataSourceNiP[i].IncidentNotification.DepartmentStation.Equals(station))
                                    {
                                        dataSourceNiP[i].IncidentNotification.DepartmentStation = station;
                                        notificationInProgressSyncBox.Sync(dataSourceNiP[i], CommittedDataAction.Update);
                                    }
                                }
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private GridControlData GetObjectInGridControl(GridControlEx gridControl, ClientData clientObject)
        {
            GridControlData result = null;
            FormUtil.InvokeRequired(gridControl,
                delegate
                {
                    for (int i = 0; i < gridControl.Items.Count && result == null; i++)
                    {
                        GridControlData data = gridControl.Items[i] as GridControlData;
                        if (data.Tag.Code == clientObject.Code)
                        {
                            result = data;
                        }
                    }
                });
            return result;
        }

        private IList GetOperatorsByWorkshift(WorkShiftVariationClientData wsVariation)
        {
            IList operators = new ArrayList();
            FormUtil.InvokeRequired(this.gridControlExOperators,
                delegate
                {
                    foreach (GridControlData item in gridControlExOperators.Items)
                    {
                        OperatorClientData oper = item.Tag as OperatorClientData;
                        if (oper.WorkShifts.Contains(wsVariation))
                        {
                            operators.Add(oper);
                        }
                    }
                });
            return operators;
        }

        private void ProcessReceivedOperatorClient(OperatorClientData oper, CommittedDataAction actionToExecute)
        {
            #region OperatorClientData
            try
            {
                bool canBeSupervised = ServerServiceClient.GetInstance().CheckOperatorClientAccess(oper, UserApplicationClientData.Dispatch.Name);
                canBeSupervised = canBeSupervised && oper.DepartmentTypes.Contains(SelectedDepartment);
                if (actionToExecute == CommittedDataAction.Delete || !canBeSupervised)
                {
                    GridControlDataOperatorNotificationDistribution grid = new GridControlDataOperatorNotificationDistribution(oper);
                    operatorSynBox.Sync(grid, CommittedDataAction.Delete);
                }
                else
                {
                    if (oper.IsSupervisor == false)
                    {
                        bool operatorShoulBeConnected = OperatorShouldBeConnected(oper);
                        if (oper.SupervisorCodes.Contains(ServerServiceClient.GetInstance().OperatorClient.Code) ||
                            (isGeneralSupervisor && CanSuperviseOperatorDepartment(oper.DepartmentTypes)))
                        {
                            SessionHistoryClientData lastSession = GetLastSessionHistory(oper.LastSessions, UserApplicationClientData.Dispatch.Name);
                            if (isGeneralSupervisor)
                            {
                                if (lastSession == null || lastSession.IsLoggedIn == false)
                                {
                                    actionToExecute = CommittedDataAction.Delete;
                                }
                                else
                                {
                                    actionToExecute = CommittedDataAction.Update;
                                }
                            }
                            else
                            {
                                if (operatorShoulBeConnected == false || lastSession == null || lastSession.IsLoggedIn == false)
                                {
                                    actionToExecute = CommittedDataAction.Delete;
                                }
                                else
                                {
                                    actionToExecute = CommittedDataAction.Update;
                                }
                            }                            
                            GridControlDataOperatorNotificationDistribution grid = new GridControlDataOperatorNotificationDistribution(oper);
                            operatorSynBox.Sync(grid, actionToExecute);
                            EnableButtonsAndMenus(gridControlExOperators);
                        }                        
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }            
            #endregion
        }

        private bool CanSuperviseOperatorDepartment(IList operatorDepartments)
        {
            bool result = false;
            for (int i = 0; i < operatorDepartments.Count && !result; i++)
            {
                DepartmentTypeClientData dept = (DepartmentTypeClientData)operatorDepartments[i];
                if (ServerServiceClient.GetInstance().OperatorClient.DepartmentTypes.Contains(dept) == true)
                {
                    result = true;
                }
            }

            return result;
        }

        private bool OperatorShouldBeConnected(OperatorClientData oper)
        {
            IList args = new ArrayList();
            args.Add(oper.Code);
            return (bool)ServerServiceClient.GetInstance().OperatorScheduleManagerMethod("IsWorkingNow", args);            
        }

        private void gridControlExOperators_SelectionChanged(object sender, FocusedRowChangedEventArgs e)
        {
            FormUtil.InvokeRequired(this,
                delegate
                {
                    EnableButtonsAndMenus(sender);
                    GridControlEx gridControl = (GridControlEx)(sender as GridView).GridControl;

                    if (gridControl != null)
                    {
                        if (gridControl.SelectedItems.Count == SINGLE_SELECTION)
                        {
                            OperatorClientData oper = (gridControl.SelectedItems[0] as GridControlDataOperatorNotificationDistribution).OperatorClient;

                            ThreadPool.QueueUserWorkItem(delegate(object state)
                            {
                                try
                                {
                                    gridControlExOperators_SelectionChanged_Help(oper);
                                }
                                catch (Exception ex)
                                {
                                    SmartLogger.Print(ex);
                                }
                            });
                        }
                        gridControl.Focus();
                    }
                });
        }

        private void gridControlExOperators_SelectionWillChange(object sender, FocusedRowChangedEventArgs e)
        {
            FormUtil.InvokeRequired(this,
                delegate
                {
                    this.simpleButtonAssign.Enabled = false;
                    this.contextMenuStripNotficationsToDistribute.Enabled = false;
                    this.asignarToolStripMenuItem.Enabled = false;

                    this.gridControlExNotificationInProgress.ClearData();
                    this.layoutControlGroupNotificationsInProcess.Text = "  " + ResourceLoader.GetString2("labelNotifInProgressOf");
                });
        }

        private void gridControlExOperators_SelectionChanged_Help(OperatorClientData oper)
        {
            //Show all notifications assigned to selected operator
            if (oper != null)
            {
                FormUtil.InvokeRequired(this,
                    delegate
                    {
                        this.layoutControlGroupNotificationsInProcess.Text = "  " + ResourceLoader.GetString2("labelNotifInProgressOf") +
                            " " + oper.FirstName + " " + oper.LastName;                        
                    });
                IList notificationList = ServerServiceClient.GetInstance().SearchClientObjects(
                    SmartCadHqls.GetCustomHql(SmartCadHqls.GetIncidentNotificationsByDispatchOperator, oper.Login));
                if (notificationList.Count > 0)
                {
                    notificationInProgressSyncBox.Sync(notificationList);
                }
            }            
        }

        private void gridControlExNotificationDistribution_SelectionChanged(object sender, FocusedRowChangedEventArgs e)
        {
            if (((GridView)gridControlExNotificationDistribution.MainView).FocusedRowHandle > -1)
            {
                PerformIncidentReport(gridControlExNotificationDistribution);
                EnableButtonsAndMenus(gridControlExNotificationDistribution);
            }
        }

        private SessionStatusHistoryClientData GetLastStatus(SessionHistoryClientData lastSession)
        {
            SessionStatusHistoryClientData status = null;
            if (lastSession != null)
            {
                if (lastSession.IsLoggedIn.Value == true && lastSession.StatusHistoryList != null)
                {
                    bool found = false;
                    DateTime minDate = DateTime.MinValue;
                    for (int i = 0; i < lastSession.StatusHistoryList.Count && found == false; i++)
                    {
                        SessionStatusHistoryClientData sshcd = (SessionStatusHistoryClientData)lastSession.StatusHistoryList[i];
                        if (sshcd.EndDateStatus == DateTime.MinValue)
                        {
                            status = sshcd;
                            found = true;
                        }
                        else if (sshcd.EndDateStatus > minDate)
                        {
                            status = sshcd;
                            minDate = status.EndDateStatus;
                        }
                    }
                }
            }
            return status;
        }

        private void EnableButtonsAndMenus(object sender)
        {
            FormUtil.InvokeRequired(this,
                delegate
                {
                    this.simpleButtonRemove.Enabled = false;
                    this.contextMenuStripNotificationsInProgress.Enabled = false;
                    this.quitarToolStripMenuItem.Enabled = false;
                });
            if (sender != null)
            {
                FormUtil.InvokeRequired(this,
                    delegate
                    {
                        if (this.gridControlExNotificationDistribution.SelectedItems.Count == SINGLE_SELECTION &&
                            this.gridControlExOperators.SelectedItems.Count == SINGLE_SELECTION)
                        {
                            OperatorClientData oper = (this.gridControlExOperators.SelectedItems[0] as GridControlDataOperatorNotificationDistribution).OperatorClient;
                            SessionHistoryClientData lastSession = GetLastSessionHistory(oper.LastSessions, UserApplicationClientData.Dispatch.Name);
                            SessionStatusHistoryClientData statusHistory = GetLastSessionStatusHistory(oper);
                            IncidentNotificationClientData selectedNotification =
                                (gridControlExNotificationDistribution.SelectedItems[0] as GridControlDataNotificationToDistribute).IncidentNotification;
                            if (oper != null && selectedNotification != null && lastSession != null &&
                                oper.DepartmentTypes.IndexOf(selectedNotification.DepartmentType) != -1 &&
                                lastSession.IsLoggedIn == true && statusHistory != null && statusHistory.StatusName != "Absent")
                            {
                                this.simpleButtonAssign.Enabled = true;
                                this.contextMenuStripNotficationsToDistribute.Enabled = true;
                                this.asignarToolStripMenuItem.Enabled = true;
                            }
                            else
                            {
                                this.simpleButtonAssign.Enabled = false;
                                this.contextMenuStripNotficationsToDistribute.Enabled = false;
                                this.asignarToolStripMenuItem.Enabled = false;
                            }
                        }
                        else
                        {
                            this.simpleButtonAssign.Enabled = false;
                            this.contextMenuStripNotficationsToDistribute.Enabled = false;
                            this.asignarToolStripMenuItem.Enabled = false;
                        }
                    });
                FormUtil.InvokeRequired(this,
                    delegate
                    {
                        if (this.gridControlExNotificationInProgress.SelectedItems.Count == SINGLE_SELECTION)
                        {
                            IncidentNotificationClientData selectedNotification =
                                (this.gridControlExNotificationInProgress.SelectedItems[0] as GridControlDataNotificationInProgress).IncidentNotification;
                            if (selectedNotification != null
                                    && selectedNotification.DepartmentType.Code == SelectedDepartment.Code
                                    && ServerServiceClient.GetInstance().OperatorClient.DepartmentTypes.Contains(SelectedDepartment))
                            {
                                this.simpleButtonRemove.Enabled = true;
                                this.contextMenuStripNotificationsInProgress.Enabled = true;
                                this.quitarToolStripMenuItem.Enabled = true;
                            }
                            else
                            {
                                this.simpleButtonRemove.Enabled = false;
                                this.contextMenuStripNotificationsInProgress.Enabled = false;
                                this.quitarToolStripMenuItem.Enabled = false;
                            }
                        }
                        else
                        {
                            this.simpleButtonRemove.Enabled = false;
                            this.contextMenuStripNotificationsInProgress.Enabled = false;
                            this.quitarToolStripMenuItem.Enabled = false;
                        }
                    });
            }
        }


        private SessionStatusHistoryClientData GetLastSessionStatusHistory(OperatorClientData oper)
        {
            SessionStatusHistoryClientData result = null;

            if (oper.LastSessions.Count > 0)
            {
                SessionHistoryClientData sscd = oper.LastSessions[0] as SessionHistoryClientData;

                if (sscd.StatusHistoryList.Count > 1)
                {
                    ((ArrayList)sscd.StatusHistoryList).Sort((IComparer)new SessionStatusHistorySort());    
                }

                if (sscd.StatusHistoryList.Count > 0)
                {
                    SessionStatusHistoryClientData sessionStatus = (SessionStatusHistoryClientData)sscd.StatusHistoryList[0];
                    result = sessionStatus;
                }
            }
            return result;
        }

        private object performIncidentReportSync = new object();
        private void PerformIncidentReport(object sender)
        {
            lock (performIncidentReportSync)
            {
                GridControlEx gridControl = sender as GridControlEx;
                if (gridControl != null)
                {
                    IncidentNotificationClientData selectedNotification = null;
                    FormUtil.InvokeRequired(gridControl,
                        delegate
                        {
                            if (gridControl.SelectedItems.Count == SINGLE_SELECTION)
                            {
                                selectedNotification = (gridControl.SelectedItems[0] as GridControlData).Tag as IncidentNotificationClientData;
                            }
                        });
                    if (selectedNotification != null)
                    {
                        ArrayList parameters = new ArrayList();
                        parameters.Add(selectedNotification.IncidentCode);

                        SelectedIncidentTask selectedIncidentTask = new SelectedIncidentTask(parameters);
                        ArrayList tasks = new ArrayList();
                        tasks.Add(selectedIncidentTask);

                        ThreadPool.QueueUserWorkItem(delegate(object obj)
                        {
                            try
                            {
                                SelectedIncidentHelp(selectedIncidentTask, obj);
                            }
                            catch (Exception ex)
                            {
                                MessageForm.Show(ex.Message, ex);
                            }
                        }, sender);
                    }
                    //else {
                    //    FormUtil.InvokeRequired(webBrowserExIncidentDetailsLeft,
                    //   delegate
                    //   {
                    //       if (webBrowserExIncidentDetailsLeft.IsDisposed == false)
                    //       {
                    //           webBrowserExIncidentDetailsLeft.Clear();
                    //           webBrowserExIncidentDetailsLeft.DocumentText = "<span style='HEIGHT:100%;FONT-SIZE: 10pt; FONT-FAMILY: Sans-Serif'>" + ResourceLoader.GetString2("NoIncidentSelected") + "</span>";
                    //       }
                    //   });
                        
                    //}
                }
            }
        }

        private void gridControlExNotificationDistribution_SelectionWillChange(object sender, FocusedRowChangedEventArgs e)
        {
            if (((GridView)gridControlExNotificationDistribution.MainView).FocusedRowHandle > -1)
            {
                FormUtil.InvokeRequired(this,
                    delegate
                    {
                        this.simpleButtonAssign.Enabled = false;
                        this.contextMenuStripNotficationsToDistribute.Enabled = false;
                        this.asignarToolStripMenuItem.Enabled = false;
                        this.barButtonItemPrint.Enabled = false;
                        this.barButtonItemSave.Enabled = false;
                        this.barButtonItemUpdate.Enabled = false;
                    });

            }
            FormUtil.InvokeRequired(webBrowserExIncidentDetailsLeft,
                  delegate
                  {
                      if (webBrowserExIncidentDetailsLeft.IsDisposed == false)
                      {
                          webBrowserExIncidentDetailsLeft.Clear();
                          webBrowserExIncidentDetailsLeft.DocumentText = "<span style='HEIGHT:100%;FONT-SIZE: 10pt; FONT-FAMILY: Sans-Serif'>" + ResourceLoader.GetString2("NoIncidentSelected") + "</span>";
                      }
                  });
        }

        private void gridControlExNotificationInProgress_SelectionChanged(object sender, FocusedRowChangedEventArgs e)
        {
            PerformIncidentReport(gridControlExNotificationInProgress);
            EnableButtonsAndMenus(gridControlExNotificationInProgress);
        }

        private void gridControlExNotificationInProgress_SelectionWillChange(object sender, FocusedRowChangedEventArgs e)
        {
            if (this.Disposing == false)
            {
                FormUtil.InvokeRequired(this,
                    delegate
                    {
                        this.simpleButtonRemove.Enabled = false;
                        this.contextMenuStripNotificationsInProgress.Enabled = false;
                        this.quitarToolStripMenuItem.Enabled = false;
                        if (webBrowserExIncidentDetailsRight.IsDisposed == false)
                        {
                            webBrowserExIncidentDetailsRight.Clear();
                            webBrowserExIncidentDetailsRight.DocumentText = "<span style='HEIGHT:100%;FONT-SIZE: 10pt; FONT-FAMILY: Sans-Serif'>" + ResourceLoader.GetString2("NoIncidentSelected") + "</span>";
                        }
                        this.barButtonItemPrint.Enabled = false;
                        this.barButtonItemSave.Enabled = false;
                        this.barButtonItemUpdate.Enabled = false;
                    });
            }
        }

        private void SelectedIncidentHelp(SelectedIncidentTask selectedIncidentTask, object sender)
        {
            if (selectedIncidentTask != null)
            {
                ArrayList tasks = new ArrayList();
                tasks.Add(selectedIncidentTask);
                try
                {
                    IList result = ServerServiceClient.GetInstance().RunTasks(tasks);
                    if (result != null && result.Count > 0 && (result[0] as IList) != null)
                    {
                        IList tempList = (IList)result[0];
                        if (tempList != null && tempList.Count > 0)
                        {
                            SelectedIncidentTaskResult selectedIncidentLightData = tempList[0] as SelectedIncidentTaskResult;
                            if (selectedIncidentLightData != null)
                            {
                                FillIncidentDetails(selectedIncidentLightData, sender);                                
                            }
                        }
                    }
                }
                catch (CommunicationObjectFaultedException)
                {
                    throw;
                }
                catch (FaultException ex)
                {
                    if (ex.Code != null && ex.Code.Name != null
                        && ex.Code.Name.Equals(typeof(DatabaseServerDownException).Name))
                    {
                        throw new CommunicationObjectFaultedException(ex.Message, ex);
                    }
                    else
                    {
                        SmartLogger.Print(ex);
                    }
                }
                catch (Exception ex)
                {
                    SmartLogger.Print(ex);
                }
            }                        
        }

        private void FillIncidentDetails(SelectedIncidentTaskResult selectedIncidentTask, object sender)
        {
            try
            {
                GridControlEx gridControl = sender as GridControlEx;
                string outputHtml = "notification_distribution";
                if (gridControl.Equals(gridControlExNotificationDistribution))
                {
                    outputHtml += "Dist.html";
                }
                else
                {
                    outputHtml += "Progress.html";
                }
                string xml = BuildHtml(selectedIncidentTask);
                xml = xml.Replace("language=\"javascript\" />", "language=\"javascript\"></script>");

                FileStream fs = File.Open(outputHtml, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite);
                byte[] byteXML = Encoding.Unicode.GetBytes(xml);
                fs.Write(byteXML, 0, byteXML.Length);
                fs.Flush();
                fs.Close();
                if (gridControl.Equals(gridControlExNotificationDistribution))
                {
                    FormUtil.InvokeRequired(this,
                       delegate
                       {
                           this.layoutControlGroupIncidentDetailsLeft.Text = "                      ";
                           this.layoutControlGroupIncidentDetailsLeft.Text += ResourceLoader.GetString2("IncidentDetails");
                           this.layoutControlGroupIncidentDetailsLeft.Text += ResourceLoader.GetString2("UpdatedAt");
                           this.layoutControlGroupIncidentDetailsLeft.Text += ServerServiceClient.GetInstance().GetTime().ToString("G", ApplicationUtil.GetCurrentCulture());
                       });

                    FormUtil.InvokeRequired(webBrowserExIncidentDetailsLeft, delegate
                    {
                        webBrowserExIncidentDetailsLeft.Navigate(new FileInfo(outputHtml).FullName);
                        leftDetailsActive = true;
                    });
                    FormUtil.InvokeRequired(this,
                        delegate
                        {
                            this.barButtonItemSave.Enabled = true;
                            this.barButtonItemPrint.Enabled = true;
                            this.barButtonItemUpdate.Enabled = true;
                        });
                }
                else
                {
                    FormUtil.InvokeRequired(this,
                       delegate
                       {
                           this.layoutControlGroupIncidentDetailsRight.Text = "                      ";
                           this.layoutControlGroupIncidentDetailsRight.Text += ResourceLoader.GetString2("IncidentDetails");
                           this.layoutControlGroupIncidentDetailsRight.Text += ResourceLoader.GetString2("UpdatedAt");
                           this.layoutControlGroupIncidentDetailsRight.Text += ServerServiceClient.GetInstance().GetTime().ToString("G", ApplicationUtil.GetCurrentCulture());
                       });

                    FormUtil.InvokeRequired(webBrowserExIncidentDetailsRight, delegate
                    {
                        webBrowserExIncidentDetailsRight.Navigate(new FileInfo(outputHtml).FullName);
                        leftDetailsActive = false;
                    });
                    FormUtil.InvokeRequired(this,
                        delegate
                        {
                            this.barButtonItemSave.Enabled = true;
                            this.barButtonItemPrint.Enabled = true;
                            this.barButtonItemUpdate.Enabled = true;
                        });
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private string BuildHtml(SelectedIncidentTaskResult result)
        {
            try
            {
                StringBuilder sb = new StringBuilder();

                XmlReader input = XmlReader.Create(new StringReader(result.Xml));
#if DEBUG
                File.WriteAllText("incidentNotification_distribution.html", result.Xml);
#endif
                XmlWriter output = XmlWriter.Create(sb);

                if (result.SourceApplication == UserApplicationClientData.FirstLevel.Name)
                {
                    xslCompiledTransform.Transform(input, output);
                }
                else
                {
                    xslCctvCompiledTransform.Transform(input, output);
                }

                return sb.ToString();
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
                return "";
            }
        }

        private void AssignIncidentNotificationToOperator_Handler(object sender, EventArgs e)
        {
            //Assign notification
            if (assigninigNotification == false)
            {
                assigninigNotification = true;
                try
                {
                    OperatorClientData selectedOperator = null;
                    IncidentNotificationClientData selectedNotification = null;
                    SessionStatusHistoryClientData statusHistory = null;
                    FormUtil.InvokeRequired(this.gridControlExNotificationDistribution,
                            delegate
                            {
                                if (this.gridControlExNotificationDistribution.SelectedItems.Count == SINGLE_SELECTION)
                                {
                                    selectedNotification =
                                        (this.gridControlExNotificationDistribution.SelectedItems[0] as GridControlDataNotificationToDistribute).IncidentNotification;
                                }
                            });
                    FormUtil.InvokeRequired(this.gridControlExOperators,
                        delegate
                        {
                            if (this.gridControlExOperators.SelectedItems.Count == SINGLE_SELECTION)
                            {
                                OperatorClientData oper = (this.gridControlExOperators.SelectedItems[0] as GridControlDataOperatorNotificationDistribution).OperatorClient;
                                if (oper != null)
                                {
                                    selectedOperator = oper;
                                    statusHistory = GetLastSessionStatusHistory(oper);
                                }
                            }
                        });

                    
                            
                                //statusHistory.StatusName != "Absent"
                    if (statusHistory.StatusName != OperatorStatusClientData.Ready.Name && statusHistory.StatusName != OperatorStatusClientData.Busy.Name)
                    {
                        DialogResult dResult = MessageForm.Show(ResourceLoader.GetString2("StatusNotificationMessage", statusHistory.StatusFriendlyName), MessageFormType.Question);
                        if (dResult != DialogResult.Yes)
                            return;
                    }
                    SessionHistoryClientData lastSession = GetLastSessionHistory(selectedOperator.LastSessions, UserApplicationClientData.Dispatch.Name);
                    if (selectedNotification != null && selectedOperator != null)
                    {
                        if (selectedOperator.DepartmentTypes.IndexOf(selectedNotification.DepartmentStation.DepartmentZone.DepartmentType) != -1)
                        {
                            if (lastSession != null && lastSession.IsLoggedIn.Value)
                            {
                                if (selectedNotification.Status.Name == IncidentNotificationStatusClientData.New.Name)
                                {
                                    AssignNotificationInStatusNew(selectedNotification, selectedOperator);
                                }
                                else //Reassign released incidentNotifications
                                {
                                    ReassignIncidentNotification(selectedNotification, selectedOperator);
                                }

                            }
                            else
                            {
                                MessageForm.Show(ResourceLoader.GetString2("Msg_OperatorIsNotLogged"),
                                    MessageFormType.Warning);
                            }
                        }
                        else
                        {
                            MessageForm.Show(ResourceLoader.GetString2("Msg_OperatorCouldNotAttendNotificationOfType"),
                                MessageFormType.Information);
                        }
                    }
                    else
                    {
                        MessageForm.Show(ResourceLoader.GetString2("OperationNotDone"),
                            MessageFormType.Warning);
                    }
                }
                finally
                {
                    assigninigNotification = false;
                }
            }
        }

        private void ReassignIncidentNotification(IncidentNotificationClientData selectedNotification, OperatorClientData selectedOperator)
        {
            try
            {
                bool result = ServerServiceClient.GetInstance().AssignIncidentNotificationToOperator(selectedNotification, selectedOperator);
                if (result == false)
                {
                    MessageForm.Show(ResourceLoader.GetString2("Msg_AssignmentCouldNotBeDone"), MessageFormType.Warning);
                }
            }
            catch (Exception ex)
            {
                try
                {
                    selectedNotification = ServerServiceClient.GetInstance().RefreshClient(selectedNotification) as IncidentNotificationClientData;
                }
                catch (Exception ex1)
                {
                    SmartLogger.Print(ex1);
                }
                if (ex is FaultException)
                {
                    FaultException fex = (FaultException)ex;
                    if (fex.Code != null && fex.Code.Name == typeof(DatabaseStaleObjectException).Name)
                    {
                        MessageForm.Show(ResourceLoader.GetString("NotificationAlreadyAssignedToOperator"), ex);
                    }
                    else
                    {
                        MessageForm.Show(ex.Message, ex);
                    }
                }
                else
                {
                    MessageForm.Show(ex.Message, ex);
                }
            }            
        }

        private void AssignIncidentNotificationToManualSupervisor(object sender, EventArgs e)
        {
            if (removingNotification == false)
            {
                removingNotification = true;
                IncidentNotificationClientData selectedNotification = null;
                try
                {
                    FormUtil.InvokeRequired(this.gridControlExNotificationInProgress,
                        delegate
                        {
                            if (this.gridControlExNotificationInProgress.SelectedItems.Count == SINGLE_SELECTION)
                            {
                                selectedNotification =
                                    (this.gridControlExNotificationInProgress.SelectedItems[0] as GridControlDataNotificationInProgress).IncidentNotification;
                            }
                        });
                    if (selectedNotification != null
                            && selectedNotification.DepartmentType.Code == SelectedDepartment.Code
                            && ServerServiceClient.GetInstance().OperatorClient.DepartmentTypes.Contains(SelectedDepartment))
                    {
                        ServerServiceClient.GetInstance().AssignIncidentNotificationToManualSupervisor(selectedNotification);
                    }
                }
                catch (Exception ex)
                {
                    try
                    {
                        ThreadPool.QueueUserWorkItem(delegate(object state)
                        {
                            try
                            {
                                AskUpdatedObjectToServer(selectedNotification);
                            }
                            catch
                            { }
                        });
                    }
                    catch (Exception ex1)
                    {
                        SmartLogger.Print(ex1);
                    }
                    if (ex is FaultException)
                    {
                        FaultException fex = (FaultException)ex;
                        if (fex.Code != null && fex.Code.Name == typeof(DatabaseStaleObjectException).Name)
                        {
                            MessageForm.Show(ResourceLoader.GetString("NotificationAlreadyAssignedToOperator"), ex);
                        }
                        else
                        {
                            MessageForm.Show(ex.Message, ex);
                        }
                    }
                    else
                    {
                        MessageForm.Show(ex.Message, ex);
                    }
                }
                finally
                {
                    removingNotification = false;
                }
            }
        }

        private void AssignNotificationInStatusNew(IncidentNotificationClientData incidentNotification,
            OperatorClientData selectedOperator)
        {           
            try
            {
                if (selectedOperator != null)
                {
                    incidentNotification.DispatchOperatorCode = selectedOperator.Code;
                    incidentNotification.Status = IncidentNotificationStatusClientData.Assigned;
                    incidentNotification.LastStatusUpdate = ServerServiceClient.GetInstance().GetTime();
                    incidentNotification.StartDate = ServerServiceClient.GetInstance().GetTime();
                    ServerServiceClient.GetInstance().SaveOrUpdateClientData(incidentNotification);
                }
            }
            catch (CommunicationObjectFaultedException)
            {
                throw;
            }
            catch (FaultException ex)
            {
                if (ex.Code != null && ex.Code.Name != null
                    && ex.Code.Name.Equals(typeof(DatabaseServerDownException).Name))
                {
                    throw new CommunicationObjectFaultedException(ex.Message, ex);
                }
                else
                {
                    incidentNotification.DispatchOperatorCode = 0;
                    incidentNotification.Status = IncidentNotificationStatusClientData.New;
                    incidentNotification.LastStatusUpdate = DateTime.MaxValue;
                    incidentNotification.StartDate = DateTime.MaxValue;
                    ThreadPool.QueueUserWorkItem(delegate(object state)
                    {
                        try
                        {
                            AskUpdatedObjectToServer(incidentNotification);
                        }
                        catch
                        { }
                    });
                    MessageForm.Show(ResourceLoader.GetString2("RequestAssignedAnotherOperator"), MessageFormType.Warning);
                }
            }
            catch (Exception ex)
            {
                MessageForm.Show(ex.Message, ex);
            }
        }

        private void AskUpdatedObjectToServer(ClientData clientObjectData)
        {
            if (clientObjectData != null && clientObjectData.Code > 0)
            {
                try
                {
                    ClientData objectData;
                    ConstructorInfo constructor = clientObjectData.GetType().GetConstructor(Type.EmptyTypes);
                    objectData = constructor.Invoke(null) as ClientData;
                    objectData.Code = clientObjectData.Code;
                    objectData = ServerServiceClient.GetInstance().RefreshClient(objectData);
                    if (objectData != null)
                    {
                        CommittedObjectDataCollectionEventArgs args =
                            new CommittedObjectDataCollectionEventArgs(objectData, CommittedDataAction.Update, null);
                        IncidentNotificationDistributionForm_CommittedChanges(null, args);
                    }
                }
                catch (Exception ex)
                {
                    SmartLogger.Print(ex);
                }
            }
        }

        private void contextMenuStripNotificationsInProgress_Opening(object sender, CancelEventArgs e)
        {
            if (clickInRow == true)
            {
                FormUtil.InvokeRequired(this,
                    delegate
                    {
                        if (this.gridControlExNotificationInProgress.SelectedItems.Count == SINGLE_SELECTION)
                        {
                            IncidentNotificationClientData selectedNotification =
                                        (this.gridControlExNotificationInProgress.SelectedItems[0] as GridControlDataNotificationInProgress).IncidentNotification;
                            if (selectedNotification != null
                                    && selectedNotification.DepartmentType.Code == SelectedDepartment.Code
                                    && ServerServiceClient.GetInstance().OperatorClient.DepartmentTypes.Contains(SelectedDepartment))
                            {
                                this.simpleButtonRemove.Enabled = true;
                                this.contextMenuStripNotificationsInProgress.Enabled = true;
                                this.quitarToolStripMenuItem.Enabled = true;
                            }
                            else
                            {
                                this.simpleButtonRemove.Enabled = false;
                                this.contextMenuStripNotificationsInProgress.Enabled = false;
                                this.quitarToolStripMenuItem.Enabled = false;
                                e.Cancel = true;
                            }
                        }
                        else
                        {
                            this.simpleButtonRemove.Enabled = false;
                            this.contextMenuStripNotificationsInProgress.Enabled = false;
                            this.quitarToolStripMenuItem.Enabled = false;
                            e.Cancel = true;
                        }
                    });
            }
            else {
                e.Cancel = true;
            }          
        }

    

        private void contextMenuStripNotficationsToDistribute_Opening(object sender, CancelEventArgs e)
        {
            FormUtil.InvokeRequired(this,
                delegate
                {
                    EnableButtonsAndMenus(this.gridControlExNotificationDistribution);
                    if (this.simpleButtonAssign.Enabled == false || clickInRow == false)
                    {
                        e.Cancel = true;
                    }
                });
        }


     

     

        private SessionHistoryClientData GetLastSessionHistory(IList lastSessionHistories, string applicationName)
        {
            SessionHistoryClientData sessionHistory = null;
            foreach (SessionHistoryClientData sess in lastSessionHistories)
            {
                if (sess.UserApplication == applicationName)
                {
                    sessionHistory = sess;
                    break;
                }
            }
            return sessionHistory;
        }

        private void IncidentNotificationDistributionForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.MdiParent != null)
            {
                (this.MdiParent as SupervisionForm).SupervisionCommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(IncidentNotificationDistributionForm_CommittedChanges);
                //(this.MdiParent as SupervisionForm).SupervisedApplicationChanged -= new EventHandler<SupervisedApplicationChangedEventArgs>(IncidentNotificationDistributionForm_SupervisedApplicationChanged);
            }
            (gridControlExOperators.MainView as GridViewEx).SelectionWillChange -= new FocusedRowChangedEventHandler(gridControlExOperators_SelectionWillChange);
            (gridControlExOperators.MainView as GridViewEx).SingleSelectionChanged -= new FocusedRowChangedEventHandler(gridControlExOperators_SelectionChanged);
            (gridControlExNotificationInProgress.MainView as GridViewEx).SelectionWillChange -= new FocusedRowChangedEventHandler(gridControlExNotificationInProgress_SelectionWillChange);
            (gridControlExNotificationInProgress.MainView as GridViewEx).SingleSelectionChanged -= new FocusedRowChangedEventHandler(gridControlExNotificationInProgress_SelectionChanged);
            (gridControlExNotificationDistribution.MainView as GridViewEx).SelectionWillChange -= new FocusedRowChangedEventHandler(gridControlExNotificationDistribution_SelectionWillChange);
            (gridControlExNotificationDistribution.MainView as GridViewEx).SingleSelectionChanged -= new FocusedRowChangedEventHandler(gridControlExNotificationDistribution_SelectionChanged);
        }

        private void gridViewNotificationDistribution_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right) 
            {
                DevExpress.Utils.DXMouseEventArgs pointXY = e as DevExpress.Utils.DXMouseEventArgs;
                if (pointXY != null)
                {
                    if (gridViewNotificationDistribution.CalcHitInfo(pointXY.X, pointXY.Y).InRow)
                        clickInRow = true;
                    else
                        clickInRow = false;
                }    
            }
        }

        private void gridViewNotificationInProgress_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                DevExpress.Utils.DXMouseEventArgs pointXY = e as DevExpress.Utils.DXMouseEventArgs;
                if (pointXY != null)
                {
                    if (this.gridViewNotificationInProgress.CalcHitInfo(pointXY.X, pointXY.Y).InRow)
                        clickInRow = true;
                    else
                        clickInRow = false;
                }
            }
        }

        private void barButtonItemPrint_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            printDialog = new PrintDialog();
            if (printDialog.ShowDialog() == DialogResult.OK)
            {
                if (leftDetailsActive == true)
                    webBrowserExIncidentDetailsLeft.Print();
                else
                    webBrowserExIncidentDetailsRight.Print();
            }
        }

        private void barButtonItemSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (leftDetailsActive == true)
                webBrowserExIncidentDetailsLeft.ShowSaveAsDialog();
            else
                webBrowserExIncidentDetailsRight.ShowSaveAsDialog();
        }

        private void barButtonItemUpdate_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (leftDetailsActive == true)
                    refreshIncidentReportLeft.Change(1000, Timeout.Infinite);
                else 
                    refreshIncidentReportRight.Change(1000, Timeout.Infinite);
                
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

    }
}
