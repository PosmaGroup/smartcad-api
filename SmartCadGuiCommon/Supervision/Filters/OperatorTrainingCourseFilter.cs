using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using SmartCadControls.Filters;
using SmartCadControls;
using SmartCadCore.ClientData;
using SmartCadCore.Core;


namespace SmartCadGuiCommon.Filters
{
    public class OperatorTrainingCourseFilter : FilterBase
    {
        public OperatorTrainingCourseFilter(DataGridEx dataGrid, IList parameters)
            : base(dataGrid, parameters)
        {

        }

        public override bool Filter(object obj)
        {
            return true;
        }

        public override bool FilterByText(object obj, string text)
        {
            bool result = false;
            OperatorTrainingCourseClientData oper = obj as OperatorTrainingCourseClientData;
            string textToSearch = ApplicationUtil.GetStringWithoutAccent(text).ToLower();
            string fname = ApplicationUtil.GetStringWithoutAccent(oper.OperatorFirstName).ToLower();
            string lname = ApplicationUtil.GetStringWithoutAccent(oper.OperatorLastName).ToLower();
            if (fname.Contains(textToSearch) || lname.Contains(textToSearch))
                result = true;


            return result;
        }
    }
}
