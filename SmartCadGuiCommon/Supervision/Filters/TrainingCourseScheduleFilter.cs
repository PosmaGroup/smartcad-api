using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using SmartCadControls.Filters;
using SmartCadControls;
using SmartCadCore.ClientData;
using SmartCadCore.Core;


namespace SmartCadGuiCommon.Filters
{
    public class TrainingCourseScheduleFilter : FilterBase
    {
        public TrainingCourseScheduleFilter(DataGridEx dataGrid, IList parameters)
            : base(dataGrid, parameters)
        {

        }

        public override bool Filter(object obj)
        {
            return true;
        }

        public override bool FilterByText(object obj, string text)
        {
            bool result = false;
            TrainingCourseScheduleClientData course = obj as TrainingCourseScheduleClientData;
            string textToSearch = ApplicationUtil.GetStringWithoutAccent(text).ToLower();
            string name = ApplicationUtil.GetStringWithoutAccent(course.Name).ToLower();

            if (name.Contains(textToSearch))
                result = true;


            return result;
        }
    }
}
