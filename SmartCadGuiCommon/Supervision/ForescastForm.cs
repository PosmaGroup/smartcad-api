using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Smartmatic.SmartCad.Service;
using System.ServiceModel;
using DevExpress.Data.Filtering;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.Utils;
using SmartCadControls.SyncBoxes;
using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadControls;
using SmartCadCore.ClientData;
using SmartCadControls.Controls;



namespace SmartCadGuiCommon
{
    public partial class ForescastForm : XtraForm
    {
        ArrayList departmentTypes = new ArrayList();
        IList forecastList = null;
        ForecastOperationForm editorForm;
        SupervisionForm parent;
        int lastDeleteCode = 0;
        int lasfFinalizeCode = 0;
        System.Windows.Forms.Timer timerCheckButtons;
        private int indexCheckedListBoxForecastUpdates = 0;
        private int indexCheckedListBoxForecastHistory = 0;
        private int separationForecastTime;
        private GridControlSynBox forecastSyncBox;

        public ForescastForm(SupervisionForm parent,int separation)
        {
            InitializeComponent();
            separationForecastTime = separation;
            this.parent = parent;
            LoadLanguage();
            LoadData();
            LoadEvents();
            this.gridViewForecast.ColumnFilterChanged += new EventHandler(gridViewForecast_ColumnFilterChanged);
            this.gridViewForecast.CustomFilterDisplayText += new ConvertEditValueEventHandler(gridViewForecast_CustomFilterDisplayText);
            this.gridViewForecast.OptionsView.ColumnAutoWidth = true;
            this.gridControlForecast.AllowRestoreSelectionAndFocusedRow = DefaultBoolean.True;
        }

        void gridViewForecast_CustomFilterDisplayText(object sender, ConvertEditValueEventArgs e)
        {
            
        }

        void gridViewForecast_ColumnFilterChanged(object sender, EventArgs e)
        {
            
        }

        private void LoadLanguage()
        {
            this.RibbonPageMonitoring.Text = ResourceLoader.GetString("Monitoring");
            this.Text = ResourceLoader.GetString2("ForecastForm");
            this.layoutControlItemFilter.Text = ResourceLoader.GetString2("ShowBy") + ":";
            this.layoutControlGroupForecast.Text = ResourceLoader.GetString2("ForecastForm");
  
            this.radioGroup1.Properties.Items[0].Description = ResourceLoader.GetString2("ForecastUpdates");
            this.radioGroup1.Properties.Items[1].Description = ResourceLoader.GetString2("ForecastHistory");
            this.radioGroup1.Properties.Items[2].Description = ResourceLoader.GetString2("Range");
            
            
            this.checkedListBoxForecastUpdates.Items[0].Value= ResourceLoader.GetString2("Active");
            this.checkedListBoxForecastUpdates.Items[1].Value= ResourceLoader.GetString2("NotStarteds");
            this.checkedListBoxForecastUpdates.Items[2].Value = ResourceLoader.GetString2("InWait");

            
           // this.gridBandConfig.Caption = ResourceLoader.GetString2("Configurated");
           // this.gridBandExec.Caption = ResourceLoader.GetString2("Executing");

         //   this.gridColumnTypeConfig.Caption = ResourceLoader.GetString2("StructType");
         //   this.gridColumnTypeExe.Caption = ResourceLoader.GetString2("StructType");
         //   this.gridColumnDepartamentExe.Caption = ResourceLoader.GetString2("DepartmentType");

            this.checkedListBoxForecastHistory.Items[0].Value = ResourceLoader.GetString2("Finished");
            this.checkedListBoxForecastHistory.Items[1].Value = ResourceLoader.GetString2("CancelledPlural");

            this.layoutControlItemFrom.Text = ResourceLoader.GetString2("StartDate") + ":";
            this.layoutControlItemTo.Text = ResourceLoader.GetString2("EndDate") + ":";
            
         //   this.gridColumnName.Caption = ResourceLoader.GetString2("Name");
         //   this.gridColumnStart.Caption = ResourceLoader.GetString2("Initiate");
         //   this.gridColumnEnd.Caption = ResourceLoader.GetString2("Terminate");
         //   this.gridColumnStatus.Caption = ResourceLoader.GetString2("Status");
         //   this.gridColumnGeneral.Caption = ResourceLoader.GetString2("General");
         //   this.gridColumnDepartmentType.Caption = ResourceLoader.GetString2("DepartmentType");

            this.gridColumnIndicator.Caption = ResourceLoader.GetString2("Indicators");
            this.gridColumnIndicatorType.Caption = ResourceLoader.GetString2("IndicatorType");
            this.gridColumnPostivePattern.Caption = ResourceLoader.GetString2("PositivePattern");
            this.gridColumnLow.Caption = ResourceLoader.GetString2("ThresholdLow");
            this.gridColumnHigh.Caption = ResourceLoader.GetString2("ThresholdHigh");
            this.gridColumnMeasureUnit.Caption = ResourceLoader.GetString2("MeasureUnit");

            this.ribbonPageGroupPerformance.Text = ResourceLoader.GetString("Performance");
            this.barButtonItemAddForecast.Caption = ResourceLoader.GetString2("Create");
            this.barButtonItemDeleteForecast.Caption = ResourceLoader.GetString2("$Message.Delete");
            this.barButtonItemUpdateForecast.Caption = ResourceLoader.GetString2("Modify");
            this.barButtonItemDuplicate.Caption = ResourceLoader.GetString2("Duplicate");
            this.barButtonItemReplace.Caption = ResourceLoader.GetString2("Replace");
            this.barButtonItemFinalize.Caption = ResourceLoader.GetString2("Finalize");

            this.barButtonItemForecast.Caption = ResourceLoader.GetString2("Forecast");
            this.barButtonItemAddForecast.Caption = ResourceLoader.GetString2("Create");
            this.barButtonItemDeleteForecast.Caption = ResourceLoader.GetString2("$Message.Delete");
            this.barButtonItemUpdateForecast.Caption = ResourceLoader.GetString2("Modify");
            this.barButtonItemDuplicate.Caption = ResourceLoader.GetString2("Duplicate");
            this.barButtonItemReplace.Caption = ResourceLoader.GetString2("Replace");
            this.barButtonItemFinalize.Caption = ResourceLoader.GetString2("Finalize");
            this.BarButtonItemHistoricGraphic.Caption = ResourceLoader.GetString2("HistoricGraphics");
            this.BarButtonItemHistoryDetails.Caption = ResourceLoader.GetString2("HistoricDetails");

            this.ribbonPageGroupGeneralOptMonitor.Text = ResourceLoader.GetString2("GeneralOptions");

            this.barButtonItemRestore.Caption = ResourceLoader.GetString2("RestoreForecast");

            this.barButtonItemPrint.Caption = ResourceLoader.GetString2("Print");
            this.barButtonItemSave.Caption = ResourceLoader.GetString2("Save");
            this.BarButtonItemSendMonitor.Caption = ResourceLoader.GetString2("Send");
            this.barButtonItemRefreshGraphic.Caption = ResourceLoader.GetString2("Update");
            #region Ribbon bar ToolTips
            this.barButtonItemForecast.SuperTip = SupervisionForm.BuildToolTip("ToolTip_ViewForecast");
            this.barButtonItemAddForecast.SuperTip = SupervisionForm.BuildToolTip("ToolTip_CreateForecast");
            this.barButtonItemDeleteForecast.SuperTip = SupervisionForm.BuildToolTip("ToolTip_DeleteForecast");
            this.barButtonItemUpdateForecast.SuperTip = SupervisionForm.BuildToolTip("ToolTip_ModifyForecast");
            this.barButtonItemDuplicate.SuperTip = SupervisionForm.BuildToolTip("ToolTip_DuplicateForecast");
            this.barButtonItemReplace.SuperTip = SupervisionForm.BuildToolTip("ToolTip_ReplaceForecast");
            this.barButtonItemFinalize.SuperTip = SupervisionForm.BuildToolTip("ToolTip_FinalizeForecast");
            this.barButtonItemPrint.SuperTip = SupervisionForm.BuildToolTip("ToolTip_Print");
            this.barButtonItemSave.SuperTip = SupervisionForm.BuildToolTip("ToolTip_Save");
            this.BarButtonItemSendMonitor.SuperTip = SupervisionForm.BuildToolTip("ToolTip_Send");
            this.barButtonItemRefreshGraphic.SuperTip = SupervisionForm.BuildToolTip("ToolTip_Update");
            
            this.barButtonItemPrint.SuperTip = SupervisionForm.BuildToolTip("ToolTip_Print");
            this.barButtonItemSave.SuperTip = SupervisionForm.BuildToolTip("ToolTip_Save");
            this.barButtonItemSend.SuperTip = SupervisionForm.BuildToolTip("ToolTip_Send");
            #endregion

            this.layoutControlGroup2.TextVisible = true;
            this.layoutControlGroup2.Text = ResourceLoader.GetString2("Configuration");
  
        }

        private void LoadData()
        {
            departmentTypes = new ArrayList(ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetDepartmentsType));
 
            forecastSyncBox = new GridControlSynBox(gridControlForecast);
            gridControlForecast.Type = typeof(IndicatorGroupForecastGridData);
            forecastList = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetIndicatorGroupForecastWithForecastValues);

            forecastSyncBox.Sync(forecastList);
            radioGroup1.SelectedIndex = 0;
            dateEditFrom.DateTime = DateTime.Now;
            dateEditTo.DateTime = dateEditFrom.DateTime;
            FillDepartmentForecastGroup();
            gridViewForecast.FocusedRowHandle = GridControlEx.AutoFilterRowHandle;
            gridViewForecast.SelectRow(GridControlEx.AutoFilterRowHandle);
        }

        private void LoadEvents()
        {
            parent.SupervisionCommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(parent_SupervisionCommittedChanges);
        }

        void parent_SupervisionCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    try
                    {
                        if (e.Objects[0] is IndicatorGroupForecastClientData)
                        {
                            CommittedDataAction cda = e.Action;
                            foreach (IndicatorGroupForecastClientData newData in e.Objects)
                            {
                                //The e.Action is recheck because in the case that multiple objects have different actions.
                                IndicatorGroupForecastClientData oldData = (CurrentGridData == null) ? (null) : (CurrentGridData.IndicatorClient);
                                if (newData.Version > 1 && cda != CommittedDataAction.Delete)
                                    cda = CommittedDataAction.Update;
                                else if (newData.Version == 1 && cda != CommittedDataAction.Delete)
                                    cda = CommittedDataAction.Save;
                                FormUtil.InvokeRequired(this, delegate
                                {
                                    if (oldData != null && oldData.Code == newData.Code)
                                    {
                                        if (editorForm == null && cda == CommittedDataAction.Update)
                                        {
                                            if (CurrentGridData.IndicatorClient.Code != newData.Code && newData.Code != lasfFinalizeCode)
                                                MessageForm.Show(ResourceLoader.GetString2("UpdateForecastForm"), MessageFormType.Information);
                                        }
                                        else if (editorForm == null && cda != CommittedDataAction.Delete && lastDeleteCode != newData.Code)
                                        {
                                            MessageForm.Show(ResourceLoader.GetString2("UpdateForecastForm"), MessageFormType.Information);
                                        }
                                        else if (newData.Code != lastDeleteCode && cda == CommittedDataAction.Delete)
                                        {
                                            MessageForm.Show(ResourceLoader.GetString2("DeleteForecastForm"), MessageFormType.Information);
                                        }
                                    }
                                    forecastSyncBox.Sync(new IndicatorGroupForecastGridData(newData), cda);
                                });
                            }
                            FormUtil.InvokeRequired(this, delegate
                            {
                                gridViewForecast_FocusedRowChanged(gridViewForecast, new FocusedRowChangedEventArgs(gridViewForecast.FocusedRowHandle, gridViewForecast.FocusedRowHandle));
                                CheckButtons();
                                lastDeleteCode = 0;
                            });
                        }
                        if (e.Objects[0] is ApplicationPreferenceClientData)
                        {
                            ApplicationPreferenceClientData preference = e.Objects[0] as ApplicationPreferenceClientData;
                            if (e.Action == CommittedDataAction.Update)
                            {
                                if (preference.Name.Equals("TimeIntervalForecastSeparation"))
                                {
                                    separationForecastTime = int.Parse(preference.Value);
                                }                              
                            }
                        }
                    }
                    catch
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private void UnCheckItems(CheckedListBoxControl obj)
        {
            foreach (CheckedListBoxItem var in obj.Items)
            {
                var.CheckState = CheckState.Unchecked;
            }
        }

        private void DisableGroupFilterHistory() 
        {
            this.checkedListBoxForecastHistory.Enabled = false;
            this.checkedListBoxForecastHistory.ItemCheck -= checkedListBoxForecastHistory_ItemCheck;
            UnCheckItems(checkedListBoxForecastHistory);
            this.checkedListBoxForecastHistory.ItemCheck += checkedListBoxForecastHistory_ItemCheck;
        }

        private void DisableGroupFilterUpdates()
        {
            this.checkedListBoxForecastUpdates.Enabled = false;
            this.checkedListBoxForecastUpdates.ItemCheck -= checkedListBoxForecastUpdates_ItemCheck;
            UnCheckItems(checkedListBoxForecastUpdates);
            this.checkedListBoxForecastUpdates.ItemCheck += checkedListBoxForecastUpdates_ItemCheck;
        }

        private void EnableGroupFilterDates(bool enable) 
        {
            this.dateEditFrom.Enabled = enable;
            this.dateEditTo.Enabled = enable;
            this.simpleButtonSearch.Enabled = enable;
        }

        private void EnableGroupFilterChekedListBox(CheckedListBoxControl listBox,int indexSelected) 
        {
            listBox.Enabled = true;
            listBox.Items[indexSelected].CheckState = CheckState.Checked;
            listBox.SelectedIndex = indexSelected;
        }

        private void radioGroup1_SelectedIndexChanged(object sender, EventArgs e)
        {
            dateEditFrom.Enabled = false;
            dateEditTo.Enabled = false;
            simpleButtonSearch.Enabled = false;
            this.gridViewForecast.ClearColumnsFilter();
            if (radioGroup1.SelectedIndex == 0)
            { 
                EnableGroupFilterChekedListBox(checkedListBoxForecastUpdates, indexCheckedListBoxForecastUpdates);
                DisableGroupFilterHistory();
                EnableGroupFilterDates(false);
                CheckButtons();
            }
            else if (radioGroup1.SelectedIndex == 1)
            {
                EnableGroupFilterChekedListBox(checkedListBoxForecastHistory, indexCheckedListBoxForecastHistory);
                DisableGroupFilterUpdates();
                CheckButtons();
                //DevExpress.Data.Filtering.CriteriaOperator expr1 = new DevExpress.Data.Filtering.BinaryOperator("Start", DateTime.Now, DevExpress.Data.Filtering.BinaryOperatorType.Less);
                //DevExpress.Data.Filtering.CriteriaOperator expr2 = new DevExpress.Data.Filtering.BinaryOperator("End", DateTime.Now, DevExpress.Data.Filtering.BinaryOperatorType.Greater);
                //DevExpress.Data.Filtering.CriteriaOperator expr3 = new DevExpress.Data.Filtering.BinaryOperator("Status", ResourceLoader.GetString2("Active"), DevExpress.Data.Filtering.BinaryOperatorType.Equal);
                //gridViewForecast.ActiveFilterCriteria = DevExpress.Data.Filtering.GroupOperator.And(new DevExpress.Data.Filtering.CriteriaOperator[] { expr1, expr2,  expr3});
               // gridViewForecast.ActiveFilterCriteria = new DevExpress.Data.Filtering.BinaryOperator("Status", ResourceLoader.GetString2("Active"), DevExpress.Data.Filtering.BinaryOperatorType.Equal);
            }
            else if (radioGroup1.SelectedIndex == 2)
            {
                DisableGroupFilterUpdates();
                DisableGroupFilterHistory();
                EnableGroupFilterDates(true);
                CheckButtons();
               // gridViewForecast.ActiveFilterCriteria = new DevExpress.Data.Filtering.BinaryOperator("Status", ResourceLoader.GetString2("Inactive"), DevExpress.Data.Filtering.BinaryOperatorType.Equal);
            }
          /*  else if (radioGroup1.SelectedIndex == 3)
            {
                //gridViewForecast.ActiveFilterCriteria = new DevExpress.Data.Filtering.BinaryOperator("Start", ServerServiceClient.GetInstance().GetTime(), DevExpress.Data.Filtering.BinaryOperatorType.GreaterOrEqual);
              //  gridViewForecast.ActiveFilterCriteria = new DevExpress.Data.Filtering.BinaryOperator("Status", ResourceLoader.GetString2("NotStarted"), DevExpress.Data.Filtering.BinaryOperatorType.Equal);
            }
            else if (radioGroup1.SelectedIndex == 4)
            {
                //gridViewForecast.ActiveFilterCriteria = new DevExpress.Data.Filtering.BinaryOperator("End", ServerServiceClient.GetInstance().GetTime(), DevExpress.Data.Filtering.BinaryOperatorType.LessOrEqual);
              //  gridViewForecast.ActiveFilterCriteria = new DevExpress.Data.Filtering.BinaryOperator("Status", ResourceLoader.GetString2("Finished"), DevExpress.Data.Filtering.BinaryOperatorType.Equal);
            }
            else if (radioGroup1.SelectedIndex == 5)
            {
                //gridViewForecast.ActiveFilterCriteria = new DevExpress.Data.Filtering.BinaryOperator("End", ServerServiceClient.GetInstance().GetTime(), DevExpress.Data.Filtering.BinaryOperatorType.LessOrEqual);
               // gridViewForecast.ActiveFilterCriteria = new DevExpress.Data.Filtering.BinaryOperator("Status", ResourceLoader.GetString2("Cancelled"), DevExpress.Data.Filtering.BinaryOperatorType.Equal);
            }
            else if (radioGroup1.SelectedIndex == 6)
            {
                dateEditFrom.Enabled = true;
                dateEditTo.Enabled = true;
                simpleButtonSearch.Enabled = true;
                gridViewForecast.ClearColumnsFilter();
                simpleButtonSearch_Click(null, null);
            }*/
        }

        private void gridViewForecast_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle >= 0)
            {
                IndicatorGroupForecastGridData igfgd = (IndicatorGroupForecastGridData)gridViewForecast.GetRow(e.FocusedRowHandle);
                if (igfgd != null)
                {
                    List<IndicatorForecastDetailGridData> forecastDetailGridData = new List<IndicatorForecastDetailGridData>();
                    foreach (IndicatorForecastClientData data in igfgd.Values)
                    {
                        IndicatorForecastDetailGridData gridData = new IndicatorForecastDetailGridData(data);
                        if (forecastDetailGridData.Contains(gridData) == false)
                            forecastDetailGridData.Add(gridData);
                    }

                    gridControlForecastDetails.BeginInit();
                    gridControlForecastDetails.DataSource = forecastDetailGridData;
                    gridControlForecastDetails.EndInit();
                    gridViewForecastDetail.ExpandAllGroups();
                    gridViewForecastDetail.BestFitColumns();
                }

                CheckButtons();
            }
            else
            {
                gridControlForecastDetails.DataSource = null;
                barButtonItemAddForecast.Enabled = false;
                barButtonItemDuplicate.Enabled = false;
                barButtonItemUpdateForecast.Enabled = false;
                barButtonItemReplace.Enabled = false;
                barButtonItemFinalize.Enabled = false;
                barButtonItemDeleteForecast.Enabled = false;
                barButtonItemRestore.Enabled = false;
                barButtonItemDeleteForecast.Enabled = false;
            }
        }

        private void dateEditFrom_EditValueChanged(object sender, EventArgs e)
        {
            dateEditTo.Properties.MinValue = dateEditFrom.DateTime;
            dateEditTo.DateTime = dateEditFrom.DateTime;
        }

        private void simpleButtonSearch_Click(object sender, EventArgs e)
        {
            gridViewForecast.Columns["StartDate"].FilterMode = DevExpress.XtraGrid.ColumnFilterMode.Value;
            gridViewForecast.Columns["EndDate"].FilterMode = DevExpress.XtraGrid.ColumnFilterMode.Value;
            DevExpress.Data.Filtering.CriteriaOperator expr1 = new DevExpress.Data.Filtering.BinaryOperator("StartDate", dateEditFrom.DateTime, DevExpress.Data.Filtering.BinaryOperatorType.GreaterOrEqual);
            DevExpress.Data.Filtering.CriteriaOperator expr2 = new DevExpress.Data.Filtering.BinaryOperator("StartDate", dateEditTo.DateTime, DevExpress.Data.Filtering.BinaryOperatorType.GreaterOrEqual);
            DevExpress.Data.Filtering.CriteriaOperator exprA = DevExpress.Data.Filtering.GroupOperator.And(new DevExpress.Data.Filtering.CriteriaOperator[] { expr1, expr2 });

            expr1 = new DevExpress.Data.Filtering.BinaryOperator("EndDate", dateEditFrom.DateTime, DevExpress.Data.Filtering.BinaryOperatorType.LessOrEqual);
            expr2 = new DevExpress.Data.Filtering.BinaryOperator("EndDate", dateEditTo.DateTime, DevExpress.Data.Filtering.BinaryOperatorType.LessOrEqual);
            DevExpress.Data.Filtering.CriteriaOperator exprB =  DevExpress.Data.Filtering.GroupOperator.And(new DevExpress.Data.Filtering.CriteriaOperator[] { expr1, expr2 });

            gridViewForecast.ActiveFilterCriteria = !(exprA | exprB);
            //DevExpress.Data.Filtering.GroupOperator.And(new DevExpress.Data.Filtering.CriteriaOperator[] { expr1, expr2 });
            //gridViewForecast.ActiveFilter.NonColumnFilterCriteria = DevExpress.Data.Filtering.GroupOperator.And(new DevExpress.Data.Filtering.CriteriaOperator[] { expr1, expr2 });
        }

        void editorForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            CheckButtons(true);
            editorForm.FormClosed -= new FormClosedEventHandler(editorForm_FormClosed);
            editorForm = null;
            gridControlForecastDetails.RefreshDataSource();

        }

        private void CheckButtons()
        {
            CheckButtons(false);
            FillDepartmentForecastGroup();
        }

        private void CheckButtons(bool editFormClosed)
        {
            IndicatorGroupForecastGridData current = (IndicatorGroupForecastGridData)gridViewForecast.GetFocusedRow();
            bool enabled = editorForm == null || editFormClosed == true;
            bool enabled2 = gridViewForecast.SelectedRowsCount > 0;
            string focusval = gridViewForecast.GetFocusedRowCellDisplayText("Name");
            bool general = false;
            bool started = false;
            bool finished = false;
            bool restore = false;
            if (current != null)
            {
                general = current.General;
                started = current.StartDate <= DateTime.Now;
                finished = current.EndDate < DateTime.Now;
                restore = current.General && !current.FactoryIndicatorGroupForecast && (current.InternalStatus == ForecastStatus.Active || current.InternalStatus == ForecastStatus.InWait);
                if (restore)
                { 
                
                }
            }
            barButtonItemAddForecast.Enabled = enabled;
            barButtonItemDuplicate.Enabled = enabled && enabled2 && (focusval != "" && focusval != null);
            barButtonItemUpdateForecast.Enabled = enabled && enabled2 && started == false && general == false && (focusval != "" && focusval != null);
            barButtonItemReplace.Enabled = enabled && enabled2 && started && finished == false;
            barButtonItemFinalize.Enabled = enabled2 && general == false && started == true && finished == false;
            barButtonItemDeleteForecast.Enabled = enabled2 && current != null && current.InternalStatus == ForecastStatus.NotStarted;
            barButtonItemRestore.Enabled = restore;
        }

        private void ForescastForm_Activated(object sender, EventArgs e)
        {
            CheckButtons();
            if (this.MdiParent != null)
                (this.MdiParent as SupervisionForm).RibbonControl.SelectedPage = (this.MdiParent as SupervisionForm).RibbonPageMonitoring;
    
        }

        private IndicatorGroupForecastGridData CurrentGridData
        {
            get { return (IndicatorGroupForecastGridData)gridViewForecast.GetFocusedRow(); }
        }

        #region Operations

        private void barButtonItemAddForecast_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OpenForecastOperationForm(GetGeneralIndicator().Clone() as IndicatorGroupForecastClientData, ForecastOperationFormOperation.Create);
        }

        private void barButtonItemDeleteForecast_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult dr = MessageForm.Show(ResourceLoader.GetString2("DeleteIndicatorForecastGroupForm", CurrentGridData.IndicatorClient.Name), MessageFormType.Question);
            if (dr == DialogResult.Yes)
            {
                if (CurrentGridData.IndicatorClient.General == false)
                {
                    IndicatorGroupForecastClientData delete = new IndicatorGroupForecastClientData();
                    delete.Code = CurrentGridData.IndicatorClient.Code;
                    delete.Version = CurrentGridData.IndicatorClient.Version;
                    lastDeleteCode = delete.Code;
                    try
                    {
                        ServerServiceClient.GetInstance().DeleteClientObject(delete);
                    }
                    catch (FaultException ex)
                    {
                        MessageForm.Show(ex.Message, MessageFormType.Error);
                    }
                    catch (Exception ex)
                    {
                        DialogResult = DialogResult.None;
                        MessageForm.Show(ex.Message, ex);
                    }
                }
                else
                {
                    BindingList<IndicatorGroupForecastGridData> listAux = (BindingList<IndicatorGroupForecastGridData>)gridControlForecast.DataSource;
                    IndicatorGroupForecastClientData generalActive = null;
                    for (int i = 0; i < listAux.Count; i++)
                    {
                        IndicatorGroupForecastGridData igfgd = listAux[i];
                        string none = igfgd.Status;
                        if ((igfgd.InternalStatus == ForecastStatus.Active || igfgd.InternalStatus == ForecastStatus.InWait) && igfgd.General == true)
                        {
                            generalActive = igfgd.IndicatorClient;
                            break;
                        }
                    }
                    
                    IndicatorGroupForecastClientData delete = new IndicatorGroupForecastClientData();
                    delete.Code = CurrentGridData.IndicatorClient.Code;
                    delete.Version = CurrentGridData.IndicatorClient.Version;
                    lastDeleteCode = delete.Code;
                        
                    try
                    {
                        generalActive.End = DateTime.MaxValue;
                        ServerServiceClient.GetInstance().DeleteClientObject(delete);
                        ServerServiceClient.GetInstance().SaveOrUpdateClientData(generalActive);
                    }
                    catch (FaultException ex)
                    {
                        MessageForm.Show(ex.Message, MessageFormType.Error);
                    }
                    catch (Exception ex)
                    {
                        DialogResult = DialogResult.None;
                        MessageForm.Show(ex.Message, ex);
                    }
                }
            }
        }

        private void barButtonItemUpdateForecast_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            IndicatorGroupForecastClientData aux = CurrentGridData.IndicatorClient as IndicatorGroupForecastClientData;
            IndicatorGroupForecastClientData clone = CurrentGridData.IndicatorClient.Clone() as IndicatorGroupForecastClientData;
            clone.ForecastValues.Clear();

            for (int i = 0; i < aux.ForecastValues.Count; i++)
            {
                IndicatorForecastClientData ifcd = aux.ForecastValues[i] as IndicatorForecastClientData;
                IndicatorForecastClientData ifcdClone = ifcd.Clone() as IndicatorForecastClientData;
                ifcdClone.Code = ifcd.Code;
                ifcdClone.Version = ifcd.Version;
                clone.ForecastValues.Add(ifcdClone);
            }
            clone.Code = aux.Code;
            clone.Version = aux.Version;
            OpenForecastOperationForm(clone, ForecastOperationFormOperation.Update);
        }

        private void barButtonItemDuplicate_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OpenForecastOperationForm(CurrentGridData.IndicatorClient.Clone() as IndicatorGroupForecastClientData, ForecastOperationFormOperation.Duplicate);
        }

        private void barButtonItemReplace_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            IndicatorGroupForecastClientData aux = CurrentGridData.IndicatorClient as IndicatorGroupForecastClientData;
            IndicatorGroupForecastClientData clone = CurrentGridData.IndicatorClient.Clone() as IndicatorGroupForecastClientData;
            clone.ForecastValues.Clear();

            for (int i = 0; i < aux.ForecastValues.Count; i++)
            {
                IndicatorForecastClientData ifcd = aux.ForecastValues[i] as IndicatorForecastClientData;
                IndicatorForecastClientData ifcdClone = ifcd.Clone() as IndicatorForecastClientData;
                ifcdClone.Code = ifcd.Code;
                ifcdClone.Version = ifcd.Version;
                clone.ForecastValues.Add(ifcdClone);
            }
            clone.Code = aux.Code;
            clone.Version = aux.Version;
            OpenForecastOperationForm(clone, ForecastOperationFormOperation.Replace);
        }

        private void barButtonItemFinalize_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            IndicatorGroupForecastClientData clientData = CurrentGridData.IndicatorClient;
            DialogResult dr = MessageForm.Show(ResourceLoader.GetString2("FinishedIndicatorForecastGroupForm", CurrentGridData.IndicatorClient.Name), MessageFormType.Question);
            if (dr == DialogResult.Yes)
            {
                clientData.End = ServerServiceClient.GetInstance().GetTime();
                lasfFinalizeCode = clientData.Code;
                try
                {
                    lastDeleteCode = clientData.Code;
                    ServerServiceClient.GetInstance().SaveOrUpdateClientData(clientData);
                }
                catch (FaultException ex)
                {
                    MessageForm.Show(ex.Message, MessageFormType.Error);
                }
                catch (Exception ex)
                {
                    MessageForm.Show(ex.Message, ex);
                }
            }
        }

        private void barButtonItemRestore_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            IndicatorGroupForecastClientData igfcd = (IndicatorGroupForecastClientData)CurrentGridData.IndicatorClient.Clone();
            igfcd.Code = CurrentGridData.IndicatorClient.Code;
            igfcd.Version = CurrentGridData.IndicatorClient.Version;
            OpenForecastOperationForm(igfcd, ForecastOperationFormOperation.Restore);
        }

        private void OpenForecastOperationForm(IndicatorGroupForecastClientData dataForm, ForecastOperationFormOperation operation)
        {
            editorForm = new ForecastOperationForm(dataForm, operation, parent,separationForecastTime);
            editorForm.FormClosed += new FormClosedEventHandler(editorForm_FormClosed);
            DialogResult res = editorForm.ShowDialog();
            if (res == DialogResult.OK)
            {
                CheckButtons();
            }
        }
        #endregion

        private IndicatorGroupForecastClientData GetGeneralIndicator()
        {
            IndicatorGroupForecastGridData result = null;
            bool found = false;
            IList data = gridControlForecast.Items;
            for (int i = 0; i < data.Count && found == false; i++)
            {
                IndicatorGroupForecastGridData aux = data[0] as IndicatorGroupForecastGridData;
                if (aux.General == true)
                {
                    found = true;
                    result = aux;
                }
            }
            return result.IndicatorClient;
        }

        private void ForescastForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (timerCheckButtons != null)
            {
                timerCheckButtons.Stop();
                timerCheckButtons.Dispose();
            }
            parent.SupervisionCommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(parent_SupervisionCommittedChanges);
        }

        private void ForescastForm_Load(object sender, EventArgs e)
        {
            timerCheckButtons = new System.Windows.Forms.Timer();
            timerCheckButtons.Tick += new EventHandler(timerCheckButtons_Tick);
            timerCheckButtons.Interval = 5000;
            timerCheckButtons.Start();
        }

        void timerCheckButtons_Tick(object sender, EventArgs e)
        {
            //CriteriaOperator co = gridViewForecast.ActiveFilterCriteria;
            //gridViewForecast.ActiveFilterCriteria = co;
            FillDepartmentForecastGroup();
            //gridViewForecast.RefreshData();
        }

        private void FillDepartmentForecastGroup()
        {
            IndicatorGroupForecastGridData specificAllGridData = null;
            IndicatorGroupForecastGridData generalAllGridData = null;

            bool onlyGeneralRunning = true;
            BindingList<IndicatorGroupForecastGridData> listAux = (BindingList<IndicatorGroupForecastGridData>)gridControlForecast.DataSource;
            for (int i = 0; i < listAux.Count; i++)
            {
                IndicatorGroupForecastGridData igfgd = listAux[i];
                string none = igfgd.Status;
                if (igfgd.Active == true && igfgd.General == false)
                {
                    onlyGeneralRunning = false;
                    IndicatorForecastClientData ifcd = igfgd.IndicatorClient.ForecastValues[0] as IndicatorForecastClientData;
                    specificAllGridData = igfgd;
                }
                else if (igfgd.Active == true && igfgd.General == true)
                {
                    generalAllGridData = igfgd;
                }
                
            }
            if (specificAllGridData != null)
            {
                specificAllGridData.Running = true;
                specificAllGridData.DepartmentTypeExec = ResourceLoader.GetString2("All");
                specificAllGridData.TypeRunning = ResourceLoader.GetString2("All");
                if (generalAllGridData != null)
                {
                    generalAllGridData.Running = false;
                    generalAllGridData.DepartmentTypeExec = string.Empty;
                    generalAllGridData.TypeRunning = string.Empty;
                }
            }
            else if (generalAllGridData != null)
            {
                generalAllGridData.Running = (specificAllGridData != null)?(false):(true);
                if (onlyGeneralRunning)
                {
                    generalAllGridData.DepartmentTypeExec = ResourceLoader.GetString2("All");
                    generalAllGridData.TypeRunning = ResourceLoader.GetString2("All");
                }
                else
                {
                    if (generalAllGridData.Running)
                    {
                        generalAllGridData.TypeRunning = ResourceLoader.GetString2("All");
                    }
                    else
                    {
                        generalAllGridData.DepartmentTypeExec = string.Empty;
                        generalAllGridData.TypeRunning = string.Empty;
                    }
                }
            }

            IndicatorGroupForecastGridData current = (IndicatorGroupForecastGridData)gridViewForecast.GetFocusedRow();

            if (current != null)
            {
                List<IndicatorForecastDetailGridData> forecastDetailGridData = new List<IndicatorForecastDetailGridData>();
                foreach (IndicatorForecastClientData data in current.Values)
                {
                    IndicatorForecastDetailGridData gridData = new IndicatorForecastDetailGridData(data);
                    if (forecastDetailGridData.Contains(gridData) == false)
                        forecastDetailGridData.Add(gridData);
                }

                gridControlForecastDetails.BeginInit();
                gridControlForecastDetails.DataSource = forecastDetailGridData;
                gridControlForecastDetails.EndInit();
                gridViewForecastDetail.ExpandAllGroups();
                gridViewForecastDetail.BestFitColumns();
            }

            gridViewForecast.RefreshData();
            CheckButtons(false);
        }


        private void checkedListBoxForecastUpdates_ItemCheck(object sender, DevExpress.XtraEditors.Controls.ItemCheckEventArgs e)
        {
            MantainOneItemCheck(checkedListBoxForecastUpdates,indexCheckedListBoxForecastUpdates);
            
            string[] array = new string[]
                {ResourceLoader.GetString2("Active"),
                    ResourceLoader.GetString2("NotStarted"),
                    ResourceLoader.GetString2("InWait")};
            CriteriaOperator local = ApplyFilterByListBox(checkedListBoxForecastUpdates,array);
            this.gridViewForecast.ActiveFilter.NonColumnFilterCriteria = local;
            //this.gridViewForecast.ActiveFilterCriteria = local;
        }

        private CriteriaOperator ApplyFilterByListBox(CheckedListBoxControl listBox, string[] array)
        {
            int index = 0;
            CriteriaOperator local = null;
            foreach (CheckedListBoxItem var in listBox.Items)
            {
                if (var.CheckState == CheckState.Checked)
                {
                    CriteriaOperator expr1 = new BinaryOperator("Status", array[index], BinaryOperatorType.Like);
                    //if (ReferenceEquals(local, null) == false)
                    if (local != null)
                        local = CriteriaOperator.Or(new CriteriaOperator[] { expr1, local });
                    else
                        local = expr1;
                }
                index++;
            }
            return local;
        }

        private void MantainOneItemCheck(CheckedListBoxControl listBox ,int indexPredeterminated)
        {
            int howmany = 0;
            foreach (CheckedListBoxItem var in listBox.Items)
            {
                if (var.CheckState == CheckState.Unchecked)
                {
                    howmany++;
                }
            }
            if (howmany == listBox.Items.Count)
            {
                int sindex = (listBox.SelectedIndex < 0) ? (indexPredeterminated) : (listBox.SelectedIndex);
                listBox.Items[sindex].CheckState = CheckState.Checked;
                listBox.SelectedIndex = sindex;
            }
        }

        private void checkedListBoxForecastHistory_ItemCheck(object sender, DevExpress.XtraEditors.Controls.ItemCheckEventArgs e)
        {
            MantainOneItemCheck(checkedListBoxForecastHistory, indexCheckedListBoxForecastHistory);
            
            string[] array = new string[]
                {ResourceLoader.GetString2("Finished"),
                    ResourceLoader.GetString2("Cancelled")};

            CriteriaOperator local = ApplyFilterByListBox(checkedListBoxForecastHistory, array);
            this.gridViewForecast.ActiveFilter.NonColumnFilterCriteria = local;
            //this.gridViewForecast.ActiveFilterCriteria = local;
        }

        private void gridViewForecast_DragObjectOver(object sender, DevExpress.XtraGrid.Views.Base.DragObjectOverEventArgs e)
        {
            GridColumn column = e.DragObject as GridColumn;
            if (column != null && e.DropInfo.Index < 0)
            {
                e.DropInfo.Valid = false;
            }
        }

        private void toolTipController1_GetActiveObjectInfo(object sender, DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventArgs e)
        {
            if (/*e.Info == null && */e.SelectedControl == gridControlForecast)
            {
                GridView view = gridControlForecast.FocusedView as GridView;
                GridHitInfo info = view.CalcHitInfo(e.ControlMousePosition);
                if (info.InRowCell)
                {
                    if (info.Column.FieldName == "Name")
                    {
                        IndicatorGroupForecastGridData over = (IndicatorGroupForecastGridData)gridViewForecast.GetRow(info.RowHandle);

                        if (over != null)
                        {
                            toolTipController1.Active = true;
                            toolTipController1.InitialDelay = 500;
                            toolTipController1.ReshowDelay = 5000;
                            SuperToolTip sTooltip2 = new SuperToolTip();
                            // Create an object to initialize the SuperToolTip.
                            ToolTipControllerShowEventArgs args = new ToolTipControllerShowEventArgs();
                            ToolTipTitleItem titleItem1 = new ToolTipTitleItem();
                            titleItem1.Text = over.Name;

                            args.ToolTipLocation = ToolTipLocation.BottomCenter;
                            ToolTipItem item1 = new ToolTipItem();
                            item1.Text = over.IndicatorClient.Description;

                            ToolTipItem item2 = new ToolTipItem();
                            item2.Text = ResourceLoader.GetString2("Description") + " :";
                            item2.Font = new Font(item2.Font, FontStyle.Bold);
                            sTooltip2.Items.Add(titleItem1);
                            sTooltip2.Items.Add(item2);
                            sTooltip2.Items.Add(item1);
                            
                            args.SuperTip = sTooltip2;
                            args.SelectedControl = gridControlForecast;

                            toolTipController1.SetSuperTip(gridControlForecast, sTooltip2);
                            toolTipController1.ShowHint(args);
                        }

                        //string text = view.GetRowCellDisplayText(info.RowHandle, info.Column);
                        //e.Info = new DevExpress.Utils.ToolTipControlInfo(new GridToolTipInfo(view, new CellToolTipInfo(info.RowHandle, info.Column, "Text")), text);
                    }
                }
                else
                {
                    
                    toolTipController1.Active = false;
                    toolTipController1.SetSuperTip(gridControlForecast,null);
                    toolTipController1.ReshowDelay = 0;
                    toolTipController1.InitialDelay = 0;
                    e.Info = null;
                }
            }
        }

        private void gridViewForecast_RowCountChanged(object sender, EventArgs e)
        {
            gridViewForecast_FocusedRowChanged(gridViewForecast, new FocusedRowChangedEventArgs(gridViewForecast.FocusedRowHandle, gridViewForecast.FocusedRowHandle));
        }

        private void gridViewForecast_RowClick(object sender, RowClickEventArgs e)
        {
            gridViewForecast_FocusedRowChanged(gridViewForecast, new FocusedRowChangedEventArgs(gridViewForecast.FocusedRowHandle, gridViewForecast.FocusedRowHandle));
        }
    }

    public enum ForecastStatus
    {
        Active,
        InWait,
        Cancelled,
        NotStarted,
        Finished
    }

    public class IndicatorGroupForecastGridData : GridControlData
    {
        bool running = true;
        bool active = false;
        //bool factory = false;
        string departmentType = string.Empty;
        string typeRunning = string.Empty;


        public IndicatorGroupForecastGridData(IndicatorGroupForecastClientData client) : base(client)
        {
            GetStatusForecast();
            typeRunning = TypeConfig;
        }

        private static string supervisedApplicationName;

        public static string SupervisedApplicationName
        {
            get { return supervisedApplicationName; }
            set { supervisedApplicationName = value; }
        }

        [GridControlRowInfoData(Visible = true, Width = 50, Searchable = true)]
        public string Name
        {
            get { return IndicatorClient.Name; }
        }

        [GridControlRowInfoData(Visible = true, Width = 80,DisplayFormat = "MM/dd/yyyy HH:mm", Searchable = true)]
        public DateTime StartDate
        {
            get { return IndicatorClient.Start; }
        }

        [GridControlRowInfoData(Visible = true, Width = 80, DisplayFormat = "MM/dd/yyyy HH:mm", Searchable = true)]
        public DateTime EndDate
        {
            get { return IndicatorClient.End; }
        }

        [GridControlRowInfoData(Visible = true, Width = 80, Searchable = true)]
        public string Status
        {
            get
            {
                return GetStatusForecast();
            }
        }

        [GridControlRowInfoData(Visible = true, Width = 80, Searchable = true)]
        public string IsGeneral
        {
            get { return IndicatorClient.General == true ? ResourceLoader.GetString2("$Message.Yes") : ResourceLoader.GetString2("$Message.No"); }
        }

        [GridControlRowInfoData(Visible = true, Width = 75, Searchable = true)]
        public string DepartmentTypeConfig
        {
            get
            {
                if (General)
                {
                    return ResourceLoader.GetString2("All");
                }
                else 
                {
                    if (IndicatorClient.ForecastValues != null && IndicatorClient.ForecastValues.Count !=0)
                    {
                        IndicatorForecastClientData ifcd = IndicatorClient.ForecastValues[0] as IndicatorForecastClientData;
                        if (ifcd.DepartmentType != null)
                            return ifcd.DepartmentType.Name;
                    }
                    return ResourceLoader.GetString2("All");    
                }
                
            }
        }

        public string DepartmentTypeExec
        {
            get
            {
                if (General)
                {
                    return departmentType;
                }
                else 
                {
                    if (IndicatorClient.ForecastValues != null)
                    {
                        IndicatorForecastClientData ifcd = IndicatorClient.ForecastValues[0] as IndicatorForecastClientData;
                        if (ifcd.DepartmentType != null)
                            return ifcd.DepartmentType.Name;
                    }
                    if (!active)
                        return ResourceLoader.GetString2("All");
                    else
                        return departmentType;
                }
            }
            set
            {
                departmentType = value;
            }
        }

        [GridControlRowInfoData(Visible = true, Width = 80, Searchable = true)]
        public string TypeConfig
        {
            get 
            {
                if (ResourceLoader.GetString2("All") == DepartmentTypeConfig)
                {
                    return ResourceLoader.GetString2("All");
                }
                else
                    return ResourceLoader.GetString2("Dispatch");
            }
        }

        public string TypeRunning
        {
            get
            {
                if (!active)
                    return TypeConfig;
                else
                    return typeRunning;
            }
            set 
            {
                typeRunning = value;
            }
        }

        public IList Values
        {
            get { return IndicatorClient.ForecastValues; }
        }

        public IndicatorGroupForecastClientData IndicatorClient
        {
            get { return this.Tag as IndicatorGroupForecastClientData; }
            set { this.Tag = value; }
        }

        public bool FactoryIndicatorGroupForecast
        {
            get 
            {
                return IndicatorClient.FactoryData;
            }
        }

        public bool General
        {
            get { return IndicatorClient.General; }
        }

        public bool Running
        {
            get { return running; }
            set { running = value; }
        }

        public bool Active
        {
            get { return active; }
        }

        public ForecastStatus InternalStatus
        { 
            get{
                if (StartDate < DateTime.Now && DateTime.Now < EndDate)
                {
                    if (running == true)
                        return ForecastStatus.Active;
                    else
                        return ForecastStatus.InWait;
                }
                else if (StartDate == EndDate)
                    return ForecastStatus.Cancelled;
                else if (StartDate > ServerServiceClient.GetInstance().GetTime())
                    return ForecastStatus.NotStarted;
                else
                {
                    return ForecastStatus.Finished;
                }
            }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is IndicatorGroupForecastGridData)
            {
                result = this.IndicatorClient.Code ==((IndicatorGroupForecastGridData)obj).IndicatorClient.Code && this.IndicatorClient.Code > 0;
                if (result == false)
                {
                    result = this.Name == ((IndicatorGroupForecastGridData)obj).Name &&
                             ApplicationUtil.CompareDateTime(this.StartDate, ((IndicatorGroupForecastGridData)obj).StartDate) &&
                             ApplicationUtil.CompareDateTime(this.EndDate, ((IndicatorGroupForecastGridData)obj).EndDate);
                }
            }
            return result;
        }

        private string GetStatusForecast()
        {
            string status = string.Empty;
            active = false;
            if (StartDate < DateTime.Now && DateTime.Now  < EndDate )
            {
                active = true;
                if (running == true)
                    status = ResourceLoader.GetString2("Active");
                else
                    status = ResourceLoader.GetString2("InWait");
            }
            else if (StartDate == EndDate)
                status = ResourceLoader.GetString2("Cancelled");
            else if (StartDate > ServerServiceClient.GetInstance().GetTime())
                status = ResourceLoader.GetString2("NotStarted");
            else
            {
                status = ResourceLoader.GetString2("Finished");
                typeRunning = string.Empty;
            }
            return status;
        }
    }

    public class IndicatorForecastDetailGridData
    {
        IndicatorForecastClientData data;

        public IndicatorForecastDetailGridData(IndicatorForecastClientData client)
        {
            data = client;
        }

        public string Indicator
        {
            get { return data.Indicator.Name; }
        }

        public string IndicatorType
        {
            get { return data.Indicator.Type; }
        }

        public string PositivePattern
        {
            get { return ResourceLoader.GetString2(data.Pattern); }
        }

        public int LowFactory
        {
            get ;
            set ;
        }

        public int HighFactory
        {
            get;
            set;
        }


        public int Low
        {
            get { return data.Low; }
            set { data.Low = value; }
        }

        public int High
        {
            get { return data.High; }
            set { data.High = value; }
        }

        public string MeasureUnit
        {
            get { return data.Indicator.MeasureUnit; }
        }

        public IndicatorForecastClientData Data
        {
            get { return data; }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is IndicatorForecastDetailGridData)
            {
                result = this.Data.Code == ((IndicatorForecastDetailGridData)obj).Data.Code;
            }
            return result;
        }

        public void SetFactoryValues(int low, int high)
        {
            this.LowFactory = low;
            this.HighFactory = high;
        }
    }
}
