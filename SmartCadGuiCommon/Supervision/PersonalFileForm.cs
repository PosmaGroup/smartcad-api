using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Smartmatic.SmartCad.Service;
using System.Collections;
using System.IO;
using DevExpress.Data.Filtering;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraPrinting.Control;
using System.Net.Mail;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using SmartCadCore.ClientData;
using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadCore.Model;
using SmartCadControls;
using SmartCadGuiCommon.SyncBoxes;

namespace SmartCadGuiCommon
{
    public partial class PersonalFileForm : XtraForm
    {

        private int supervisorCode;
        OperatorClientData selectedOperator;
        BindingList<OperatorObservationGridData> dataSourceObs = null;
        BindingList<OperatorEvaluationGridData> dataSourceEva = null;
        BindingList<OperatorTrainingCourseGridData> dataSourceCourses = null;
        BindingList<OperatorCategoryHistoryGridData> dataSourceCat = null;
        private string supervisedApplicationName;
		private bool firstLevel;
        private Dictionary<int, bool> TrainingCourseMap;

        private Dictionary<string, int> numberMonthFilter = null;

		public int operatorCode { get { return selectedOperator.Code; }}

		public bool FirstLevel
		{
			get
			{
				return firstLevel;
			}
			set
			{
				firstLevel = value;
			}
		}

        public string SupervisedApplicationName
        {
            get
            {
                return supervisedApplicationName;
            }
            set
            {
                if (string.IsNullOrEmpty(value) == false && value != supervisedApplicationName)
                {
                    supervisedApplicationName = value;
                    this.Close();
                }
            }
        }

        public PersonalFileForm()
        {  
            InitializeComponent();

            InitializeGridControl();
            this.FormClosing += new FormClosingEventHandler(PersonalFileForm_FormClosing);
            this.Activated += new EventHandler(PersonalFileForm_Activated);
            TrainingCourseMap = new Dictionary<int, bool>();
            LoadLanguage();
            numberMonthFilter = new Dictionary<string, int>();
      
            textBoxAddress2.Enabled = true;
            textBoxAddress2.Properties.AllowFocused = false;
            textBoxAddress2.Properties.ReadOnly = true;
            
        }

        void PersonalFileForm_Activated(object sender, EventArgs e)
        {
            if (this.MdiParent != null)
            (this.MdiParent as SupervisionForm).RibbonControl.SelectedPage = (this.MdiParent as SupervisionForm).RibbonPageOperation;
        }

        private void InitializeGridControl()
        {
            gridControlObs.Type = typeof(OperatorObservationGridData);
            gridControlObs.EnableAutoFilter = true;
            gridControlObs.ViewTotalRows = true;
            
           
            gridControlEva.Type = typeof(OperatorEvaluationGridData);
            gridControlEva.EnableAutoFilter = true;
            gridControlEva.ViewTotalRows = true;

            gridControlCourses.Type = typeof(OperatorTrainingCourseGridData);
            gridControlCourses.EnableAutoFilter = true;
            gridControlCourses.ViewTotalRows = true;

            gridControlCat.Type = typeof(OperatorCategoryHistoryGridData);
        }
        private void LoadLanguage()
        {
            #region Operator Information

            this.Text = ResourceLoader.GetString2("PersonalFileFormTitle");
            this.layoutControlGroupUserPersonalInfo.Text = ResourceLoader.GetString2("InfoOperator");
            this.labelControlPhoto.Text = ResourceLoader.GetString2("Photo") + ":";
            this.layoutControlItemName.Text = ResourceLoader.GetString2("FullFirstName") + ":";
            this.layoutControlItemLastName.Text = ResourceLoader.GetString2("FullLastName") + ":";
            this.layoutControlItemOpId.Text = ResourceLoader.GetString2("PersonID") + ":";
            this.layoutControlItemDateBirth.Text = ResourceLoader.GetString2("DateofBirth") + ":";
            this.layoutControlItemPhone.Text = ResourceLoader.GetString2("Phone") + ":";
            this.layoutControlItemAddress.Text = ResourceLoader.GetString2("Address") + ":";
            this.layoutControlItemEmail.Text = ResourceLoader.GetString2("Mail") + ":";
            this.layoutControlItemRol.Text = ResourceLoader.GetString2("SRole") + ":";
            this.layoutControlItemDepartments.Text = ResourceLoader.GetString2("Departments") + ":";
            this.layoutControlItemCategory.Text = ResourceLoader.GetString2("Category") + ":";
            this.RibbonPageOperation.Text = ResourceLoader.GetString2("Operation");
            this.layoutControlGroupUserInfo.Text = ResourceLoader.GetString2("InfoOperatorTabTitle");
            this.layoutControlGroupScheduler.OptionsToolTip.ToolTip = ResourceLoader.GetString2("ToolTip_OperatorSelectionSchedule"); ;
            #endregion

            #region Observations

            this.layoutControlGroupObs.Text = ResourceLoader.GetString2("Observations");

          //  this.columnType.Caption = ResourceLoader.GetString2("$Message.Type");
          //  this.columnDate.Caption = ResourceLoader.GetString2("DateTime");
          //  this.columnSupervisor.Caption = ResourceLoader.GetString2("Supervisor");
            this.layoutControlGroupObsDetails.Text = ResourceLoader.GetString2("Details");

            #endregion

            #region Courses

            this.layoutControlGroupCourse.Text = ResourceLoader.GetString2("TrainingCourses");
            //this.columnCourseName.Caption = ResourceLoader.GetString2("$Message.Name");
            //this.columnStartDate.Caption = ResourceLoader.GetString2("StartDate");
            //this.columnDateCourse.Caption = ResourceLoader.GetString2("EndDate");
            #endregion
            
            #region Evaluations

            this.layoutControlGroupEval.Text = ResourceLoader.GetString2("Evaluations");
            this.layoutControlItemShowFor.Text = ResourceLoader.GetString2("ShowBy") + ":";
          
            #endregion

            #region tabs

            this.layoutControlGroupUserInfo.Text = ResourceLoader.GetString2("InfoOperatorTabTitle");
            this.layoutControlGroupScheduler.Text = ResourceLoader.GetString2("SchedulerOperatorTabTitle");

            this.

            #endregion

            #region BarItems
            
            barButtonItemEvaluateOperator.Caption = ResourceLoader.GetString2("EvaluateOperator");
            printPreviewBarItemModifyCategory.Caption = ResourceLoader.GetString2("ModifyCategory");
            printPreviewBarItemPersonalFileObs.Caption = ResourceLoader.GetString2("AddObservation");
            RibbonPageOperation.Text = ResourceLoader.GetString2("Operation");
            this.barButtonItemPrint.Caption = ResourceLoader.GetString2("Print");
            this.barButtonItemSave.Caption = ResourceLoader.GetString2("Save");
            this.barButtonItemSend.Caption = ResourceLoader.GetString2("Send");
            this.barButtonItemUpdate.Caption = ResourceLoader.GetString2("Update");
            this.ribbonPageGroupOptions.Text = ResourceLoader.GetString2("GeneralOptions");
            this.ribbonPageGroupPersonalFile.Text = ResourceLoader.GetString2("PersonalRecord");
            #endregion

         
            #region Ribbon bar ToolTips
            this.printPreviewBarItemPersonalfile.SuperTip = SupervisionForm.BuildToolTip("ToolTip_ViewPersonalFile");
            this.barButtonItemEvaluateOperator.SuperTip = SupervisionForm.BuildToolTip("ToolTip_EvaluateOperator");
            this.printPreviewBarItemModifyCategory.SuperTip = SupervisionForm.BuildToolTip("ToolTip_ModifyOperatorCategory");
            this.printPreviewBarItemPersonalFileObs.SuperTip = SupervisionForm.BuildToolTip("ToolTip_AddOperatorObservation");

            this.barButtonItemPrint.SuperTip = SupervisionForm.BuildToolTip("ToolTip_Print");
            this.barButtonItemSave.SuperTip = SupervisionForm.BuildToolTip("ToolTip_Save");
            this.barButtonItemSend.SuperTip = SupervisionForm.BuildToolTip("ToolTip_Send");
            this.barButtonItemUpdate.SuperTip = SupervisionForm.BuildToolTip("ToolTip_Update");

            this.ribbonPageGroupAccessProfile.SuperTip = SupervisionForm.BuildToolTip("ToolTip_AccessProfileGroup");
            #endregion
        }

        void PersonalFileForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.MdiParent != null)
            {
                (this.MdiParent as SupervisionForm).SupervisionCommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(PersonalFileForm_SupervisionCommittedChanges);
            }
        }

        public PersonalFileForm(int sprvsrCode,OperatorClientData ope, string supervisedApplication):this()
        {      
            supervisorCode = sprvsrCode;
            this.supervisedApplicationName = supervisedApplication;
            selectedOperator = ope;         
         
        }

        private void SeekAddWorkShiftVariations(int code)
        {
            string hql = SmartCadHqls.GetCustomHql(
				SmartCadHqls.GetWorkShiftVariationWithSchedulesByOperatorCode,
				code);

            IList list = ServerServiceClient.GetInstance().SearchClientObjects(hql);
            
            foreach (WorkShiftVariationClientData var in list)
               this.schedulerBarControl.CreateAddAppointment(var);
            
        }

        private void SeekAddCategory(int operatorCode)
        {
            string hql = SmartCadHqls.GetCustomHql(
				SmartCadHqls.GetOperatorCategoryHistoryByOperatorCode,
				operatorCode);
            IList list = ServerServiceClient.GetInstance().SearchClientObjects(hql);
            dataSourceCat = new BindingList<OperatorCategoryHistoryGridData>();
            foreach (OperatorCategoryHistoryClientData var in list) 
            {
                dataSourceCat.Add(new OperatorCategoryHistoryGridData(var));
            }
            gridControlCat.BeginInit();
            gridControlCat.DataSource = dataSourceCat;
            gridControlCat.EndInit();
        }

        private void SetOperator(OperatorClientData operatr) 
        {
            //ServerServiceClient server = ServerServiceClient.GetInstance();
            //OperatorClientData ope = new OperatorClientData();
            //ope.Code = operatr.Code;
            //ope = (OperatorClientData)server.RefreshClient(ope);
            this.labelName.Text = operatr.FirstName;
            this.labelLastName.Text = operatr.LastName;
            this.labelOperatorID.Text = operatr.PersonID;
            this.labelDateBirth.Text = operatr.Birthday.Value.ToShortDateString();
            this.labelEmail.Text = operatr.Email;
            this.labelPhone.Text = operatr.Telephone;
            this.labelRol.Text = operatr.RoleFriendlyName;
            this.pictureBoxPhoto.Image = GetImage(operatr.Picture);
            this.textBoxAddress2.Text = operatr.Address;
            listBoxDepartaments.Items.Clear();
            //server.InitializeLazy(ope,ope.DepartmentTypes);
            foreach (DepartmentTypeClientData dep in operatr.DepartmentTypes)
            {
                this.listBoxDepartaments.Items.Add(dep);
            }
            
        }

        private void SeekAddEvaluations(int operatorCode)
        {
            string hql = SmartCadHqls.GetCustomHql(
				SmartCadHqls.GetOperatorEvaluationsByOperatorCode,
				operatorCode);
            IList list = ServerServiceClient.GetInstance().SearchClientObjects(hql);

            dataSourceEva = new BindingList<OperatorEvaluationGridData>();
            foreach (OperatorEvaluationClientData var in list) 
            {
                OperatorEvaluationGridData data = new OperatorEvaluationGridData(var);
                dataSourceEva.Add(data);
            }
            gridControlEva.BeginInit();
            gridControlEva.DataSource = dataSourceEva;
            gridControlEva.EndInit();
        }

        private void SeekAddTrainingCourses(int operatorCode)
        {
            string hql = SmartCadHqls.GetCustomHql(
				SmartCadHqls.GetOperatorTrainingCourseByOperatorCode, 
				operatorCode);
            IList list = ServerServiceClient.GetInstance().SearchClientObjects(hql);
            dataSourceCourses = new BindingList<OperatorTrainingCourseGridData>();

            foreach (OperatorTrainingCourseClientData var in list)
            {
                IList selectedTrainingCourse = (IList)ServerServiceClient.GetInstance().SearchBasicObjects(
                                                               SmartCadHqls.GetCustomHql(SmartCadHqls.TrainingCourseSchedulePartByOperatorAndCourse,
                                                               operatorCode,
                                                               var.CourseCode
                                                               ));
                foreach (TrainingCourseSchedulePartsData schedlist in selectedTrainingCourse)
                {
                    var.StarDate = (DateTime)schedlist.Start;
                    var.EndDate = (DateTime)schedlist.End;
                }
                OperatorTrainingCourseGridData data = new OperatorTrainingCourseGridData(var);
                dataSourceCourses.Add(data);
            }
            gridControlCourses.BeginInit();
            gridControlCourses.DataSource = dataSourceCourses;
            gridControlCourses.EndInit();
   
        }

        private void SeekAddObservations(int operatorCode)
        {
            string hql = SmartCadHqls.GetCustomHql(
				SmartCadHqls.GetOperatorObservationByOperatorCode,
				operatorCode);
            IList listObs = ServerServiceClient.GetInstance().SearchClientObjects(hql);

            dataSourceObs = new BindingList<OperatorObservationGridData>();
            foreach (OperatorObservationClientData var in listObs)
            {
                OperatorObservationGridData data2 = new OperatorObservationGridData(var);
                dataSourceObs.Add(data2);
            }
            gridControlObs.BeginInit();
            gridControlObs.DataSource = dataSourceObs;
            gridControlObs.EndInit();
        }

        private void UpdateOperator(OperatorClientData ope) 
        {
            string hql = SmartCadHqls.GetCustomHql(
				SmartCadHqls.GetOperatorByCodeWithCategories,
				ope.Code);
            IList list = ServerServiceClient.GetInstance().SearchClientObjects(hql);
            if (list.Count > 0)
            {
                this.selectedOperator = list[0] as OperatorClientData;
            }
            else 
            {
                //NOTA
                //Este mensaje tiene muy pocas probabilidades de aparecer ya que la busqueda
                //de arriba se hace por codigo, sin embargo existe, lo coloco
                //para tener por lo menos un mensaje de error en caso de que aparezca.
                // Gustavo Fandio 22/08/08.
                MessageForm.Show(ResourceLoader.GetString2("ErrorUpdatingOperator"), MessageFormType.Error);
            }
            
        }

        public void AddObservation(OperatorObservationClientData opeObs) 
        {
            OperatorObservationGridData data = new OperatorObservationGridData(opeObs);
         
            FormUtil.InvokeRequired(this, delegate
            {
                dataSourceObs.Add(data);

                gridControlObs.BeginUpdate();
                gridControlObs.DataSource = dataSourceObs;
                gridControlObs.EndUpdate();
                gridViewObs.FocusedRowHandle = gridViewObs.GetRowHandle(dataSourceObs.Count - 1); 
            
                gridView1_FocusedRowChanged(gridViewObs, new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs(gridViewObs.FocusedRowHandle,  gridViewObs.FocusedRowHandle));

            });
            
            UpdateOperator(this.selectedOperator);
			//gridViewObs.SelectRange(dataSourceObs.IndexOf(data),dataSourceObs.IndexOf(data));
        }

        private void AddTrainingCourse(OperatorTrainingCourseClientData opeCourse)
        {
            OperatorTrainingCourseGridData data = new OperatorTrainingCourseGridData(opeCourse);
            
            if (!dataSourceCourses.Contains(data))
            {               
                FormUtil.InvokeRequired(this, delegate
                {
                    dataSourceCourses.Add(data);
                    gridControlCourses.BeginUpdate();
                    gridControlCourses.DataSource = dataSourceCourses;
                    gridControlCourses.EndUpdate();
                    gridViewCoursesRealized.FocusedRowHandle = gridViewCoursesRealized.GetRowHandle(dataSourceCourses.Count - 1);
                });
            }
        }

        private void DeleteTrainingCourse(OperatorTrainingCourseClientData opeCourse)
        {
            OperatorTrainingCourseGridData data = new OperatorTrainingCourseGridData(opeCourse);
            
            if (dataSourceCourses.Contains(data))
            {            
                FormUtil.InvokeRequired(this, delegate
                {
                    dataSourceCourses.Remove(data);
                    gridControlCourses.BeginUpdate();
                    gridControlCourses.DataSource = dataSourceCourses;
                    gridControlCourses.EndUpdate();
                    gridViewCoursesRealized.FocusedRowHandle = gridViewCoursesRealized.GetRowHandle(GridControlEx.AutoFilterRowHandle);
                });
            }
        }

        public void AddCategory(OperatorCategoryHistoryClientData opeCat)
        {
            OperatorCategoryHistoryGridData data = new OperatorCategoryHistoryGridData(opeCat);

            if (!dataSourceCat.Contains(data))
            {             
                FormUtil.InvokeRequired(this, delegate
                {
                    dataSourceCat.Add(data);
                    gridControlCat.BeginUpdate();
                    gridControlCat.DataSource = dataSourceCat;
                    gridControlCat.EndUpdate();
                    gridViewCat.FocusedRowHandle = gridViewCat.GetRowHandle(dataSourceCat.Count - 1);
                });
            }
            else
            {
                //TODO print something or anything else
            }
        }

        public void AddEvaluation(OperatorEvaluationClientData opeEva)
        {
            OperatorEvaluationGridData data = new OperatorEvaluationGridData(opeEva);
                    
            FormUtil.InvokeRequired(this, delegate
            {
                dataSourceEva.Add(data);
                gridControlEva.BeginUpdate();
                gridControlEva.DataSource = dataSourceEva;
                gridControlEva.EndUpdate();
                gridViewAllEva.FocusedRowHandle = gridViewAllEva.GetRowHandle(dataSourceEva.Count - 1);
            });
        }


        private void InitComboBoxEvaluations()
        {
            string all = ResourceLoader.GetString2("SecurityForm.Structs.All");
            numberMonthFilter.Add(all, -1);
            string actual = ResourceLoader.GetString2("ActualMonth");
            numberMonthFilter.Add(actual, 0);

            this.comboBoxFilterEvaluations.Properties.Items.AddRange(new string[]{all,actual});

            for (int i = 2; i <= 12; i++)
            {
                string set = ResourceLoader.GetString2("Previous"+(i) + "Month");
                numberMonthFilter.Add(set, i);
                this.comboBoxFilterEvaluations.Properties.Items.Add(set);
            }
            this.comboBoxFilterEvaluations.SelectedIndex = 0;
        }

        private Image GetImage(byte[] data)
        {
            if (data == null)
                return null;
            MemoryStream ms = new MemoryStream(data);
            Image image = Image.FromStream(ms);
            return image.GetThumbnailImage(pictureBoxPhoto.Width, pictureBoxPhoto.Height, new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallback), IntPtr.Zero); ;
            
        }

        private bool ThumbnailCallback()
        {
            return true;
        }

        private void PersonalFileForm_Load(object sender, EventArgs e)
        {
            (this.MdiParent as SupervisionForm).SupervisionCommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(PersonalFileForm_SupervisionCommittedChanges);
            
            SetOperator(selectedOperator);
            SeekAddObservations(selectedOperator.Code);
            SeekAddTrainingCourses(selectedOperator.Code);
            SeekAddEvaluations(selectedOperator.Code);
            SeekAddCategory(selectedOperator.Code);
            SeekAddWorkShiftVariations(selectedOperator.Code);
                    
            if (radioButtonCoursesRealized.Checked == false)
                this.radioButtonCoursesRealized.Checked = true;
            else
                radioButtonCoursesRealized_CheckedChanged(radioButtonCoursesRealized, null);

            InitComboBoxEvaluations();
        }



        void PersonalFileForm_SupervisionCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    if (e.Objects[0] is OperatorObservationClientData)
                    {
                        OperatorObservationClientData opeObs = e.Objects[0] as OperatorObservationClientData;
                        if ( e.Action == CommittedDataAction.Save && opeObs.OperatorCode == this.selectedOperator.Code)
                        {
                           AddObservation(opeObs);
                        }
                    }
                    else if (e.Objects[0] is OperatorCategoryHistoryClientData)
                    {
                        OperatorCategoryHistoryClientData opeCate = e.Objects[0] as OperatorCategoryHistoryClientData;
                        if ( e.Action == CommittedDataAction.Save && opeCate.OperatorCode == this.selectedOperator.Code)
                        {
                            AddCategory(opeCate);
							UpdateOperator(selectedOperator);
                        }
                    }
                    else if (e.Objects[0] is OperatorEvaluationClientData)
                    {
                        OperatorEvaluationClientData opeEva = e.Objects[0] as OperatorEvaluationClientData;
                        if (e.Action == CommittedDataAction.Save && opeEva.Operator.Code == this.selectedOperator.Code)
                        {
                            AddEvaluation(opeEva);
                        }
                    }
                    else if (e.Objects[0] is OperatorCategoryClientData)
                    {
                        
                    }
                    else if (e.Objects[0] is OperatorClientData)
                    {
                        OperatorClientData op = e.Objects[0] as OperatorClientData;
                        if(op.Code == this.selectedOperator.Code)
                        {
                            FormUtil.InvokeRequired(this, delegate
                            {
                                SetOperator(op);
                            });
                        }
                    }
                    else if(e.Objects[0] is OperatorTrainingCourseClientData)
                    {
                        OperatorTrainingCourseClientData obj = e.Objects[0] as OperatorTrainingCourseClientData;
                        if (e.Action == CommittedDataAction.Save && obj.OperatorCode == this.selectedOperator.Code)
                        {
                            AddTrainingCourse(obj);
                        }
                        else if (e.Action == CommittedDataAction.Update)
                        {
                            UpdateTrainingCourse(obj);
                        }
                        else if (e.Action == CommittedDataAction.Delete)
                        {
                            DeleteTrainingCourse(obj);
                        }
                    }
                    else if (e.Objects[0] is TrainingCourseClientData)
                    {
                        TrainingCourseClientData obj = e.Objects[0] as TrainingCourseClientData;
                        
                        if (e.Action == CommittedDataAction.Update)
                        {
                            CheckTrainingCourseInForm(obj,CommittedDataAction.Update);
                        }
                        else if (e.Action == CommittedDataAction.Delete)
                        { 
                            
                        }
                    }
                    else if (e.Objects[0] is TrainingCourseScheduleClientData)
                    {
                        TrainingCourseScheduleClientData obj = e.Objects[0] as TrainingCourseScheduleClientData;

                        if (e.Action == CommittedDataAction.Update)
                        {
                            CheckTrainingCourseScheduleInForm(obj, e.Action);
                        }
                        else if (e.Action == CommittedDataAction.Delete)
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }

        }

        private void CheckTrainingCourseScheduleInForm(TrainingCourseScheduleClientData obj, CommittedDataAction committedDataAction)
        {
            for (int i = 0; i < dataSourceCourses.Count; i++)
            {
                OperatorTrainingCourseGridData var = dataSourceCourses[i] as OperatorTrainingCourseGridData;
                if (var.OperatorTrainingCourse.CourseScheduleCode == obj.Code)
                {
                    
                    FormUtil.InvokeRequired(this, delegate
                    {
                        if (committedDataAction == CommittedDataAction.Update)
                        {
                            var.OperatorTrainingCourse.StarDate = obj.Start;
                            var.OperatorTrainingCourse.EndDate = obj.End;
                            var.OperatorTrainingCourse.CourseScheduleTrainer = obj.Trainer;
                        }
                        else if (committedDataAction == CommittedDataAction.Delete)
                        {
                            dataSourceCourses.RemoveAt(i);
                        }

                        gridControlCourses.BeginUpdate();
                        gridControlCourses.DataSource = dataSourceCourses;
                        gridControlCourses.EndUpdate();
                    });
                    break;
                }
            }
        }

        private void CheckTrainingCourseInForm(TrainingCourseClientData obj, CommittedDataAction committedDataAction)
        {
 	        for (int i = 0; i < dataSourceCourses.Count; i++)
            {
                OperatorTrainingCourseGridData var = dataSourceCourses[i] as OperatorTrainingCourseGridData;
                if (var.OperatorTrainingCourse.CourseCode == obj.Code)
                {                    
                    FormUtil.InvokeRequired(this, delegate
                    {
                        if (committedDataAction == CommittedDataAction.Update)
                            var.OperatorTrainingCourse.CourseName = obj.Name;
                        else if (committedDataAction == CommittedDataAction.Delete)
                            dataSourceCourses.RemoveAt(i);

                        gridControlCourses.BeginUpdate();
                        gridControlCourses.DataSource = dataSourceCourses;
                        gridControlCourses.EndUpdate();
                    });
                    break;
                }
            }
        }

        private void UpdateTrainingCourse(OperatorTrainingCourseClientData obj)
        {
            OperatorTrainingCourseGridData data = new OperatorTrainingCourseGridData(obj);

            for (int i = 0; i < dataSourceCourses.Count; i++)
            {
                OperatorTrainingCourseGridData var = dataSourceCourses[i] as OperatorTrainingCourseGridData;
                if (var.OperatorTrainingCourse.Code == data.OperatorTrainingCourse.Code)
                {                  
                    FormUtil.InvokeRequired(this, delegate
                    {
                        dataSourceCourses[i] = data;

                        gridControlCourses.BeginUpdate();
                        gridControlCourses.DataSource = dataSourceCourses;
                        gridControlCourses.EndUpdate();
                    });
                }
            }
        }

 
        private ReportPersonaFile BuildReport()
        {
            ReportPersonaFile report = new ReportPersonaFile(this.selectedOperator);

            List<List<string>> listObs = GetObservations();
            report.SetObservations(listObs);
            List<List<string>> listCourses = GetCourses();
            report.SetCourses(listCourses);
            List<List<string>> listEvaluations = GetEvaluations();
            report.SetEvaluations(listEvaluations);
            return report;
        }

        private List<List<string>> GetEvaluations()
        {
            List<List<string>> listEvaluations = new List<List<string>>();
            List<OperatorEvaluationGridData> lst = this.gridViewAllEva.DataSource as List<OperatorEvaluationGridData>;
            if (lst != null)
            foreach (OperatorEvaluationGridData var in lst)
            {
                List<string> sub = new List<string>();
                sub.AddRange(new string[] { var.EvaluationName, var.Qualification, var.Date.ToShortDateString(), var.EvaluationName });
                listEvaluations.Add(sub);
            }
            return listEvaluations;
        }

        private List<List<string>> GetCourses()
        {
            List<List<string>> listCourses = new List<List<string>>();
            IList lst = this.gridControlCourses.Items;
            if (lst != null)
            foreach (OperatorTrainingCourseGridData var in lst)
            {
                if (ServerServiceClient.GetInstance().GetTime() > var.StartDate && var.StartDate != DateTime.MaxValue && ServerServiceClient.GetInstance().GetTime() > var.EndDate && var.EndDate != DateTime.MaxValue)
                {
                    List<string> sub = new List<string>();
                    sub.AddRange(new string[] { var.CourseName, var.EndDate.ToShortDateString() });
                    listCourses.Add(sub);
                }
            }
            return listCourses;
        }

        private List<List<string>> GetObservations()
        {
            List<List<string>> listObs = new List<List<string>>();
            IList lst = this.gridControlObs.Items;
            if (lst != null)
                foreach (OperatorObservationGridData var in lst)
                {
                    List<string> sub = new List<string>();
                    sub.AddRange(new string[] { var.ObservationType, var.Observation.Observation, var.DateTime, var.Supervisor });
                    listObs.Add(sub);
                }
            return listObs;
        }

     
        private void radioButtonLastMonthEvaluations_CheckedChanged(object sender, EventArgs e)
        {
            DateTime now = ServerServiceClient.GetInstance().GetTimeFromDB();
            bool visible = ((RadioButton)sender).Checked;
            if (visible)
            {
                gridViewAllEva.ActiveFilterCriteria = new BinaryOperator("Date", now.Date.AddDays(1-now.Day), BinaryOperatorType.GreaterOrEqual);
            }
        }

        private void radioButtonLasSemesterEvaluations_CheckedChanged(object sender, EventArgs e)
        {
            DateTime now = ServerServiceClient.GetInstance().GetTimeFromDB();
            bool visible = ((RadioButton)sender).Checked;
            if (visible)
            {
                DateTime sixMonths = now.Date.AddMonths(-6);
                sixMonths.AddDays(1 - sixMonths.Day);
                gridViewAllEva.ActiveFilterCriteria = new BinaryOperator("Date", sixMonths , BinaryOperatorType.GreaterOrEqual);
            }
        }

        private void radioButtonCoursesRealized_CheckedChanged(object sender, EventArgs e)
        {
            DateTime now = ServerServiceClient.GetInstance().GetTime();
            bool visible = ((RadioButton)sender).Checked;
            if (visible)
            {
                gridViewCoursesRealized.Columns["EndDate"].FilterMode = DevExpress.XtraGrid.ColumnFilterMode.Value;
                gridViewCoursesRealized.ActiveFilterCriteria = new BinaryOperator("EndDate", now, BinaryOperatorType.LessOrEqual);
            }
        }

        private void radioButtonCoursesNotRealized_CheckedChanged(object sender, EventArgs e)
        {
            DateTime now = ServerServiceClient.GetInstance().GetTime();
            bool visible = ((RadioButton)sender).Checked;
            if (visible)
            {
                gridViewCoursesRealized.Columns["EndDate"].FilterMode = DevExpress.XtraGrid.ColumnFilterMode.Value;
                gridViewCoursesRealized.ActiveFilterCriteria = new BinaryOperator("EndDate", now, BinaryOperatorType.Greater);
            }
        }

        private void radioButtonAllEvaluations_CheckedChanged(object sender, EventArgs e)
        {
            DateTime now = ServerServiceClient.GetInstance().GetTimeFromDB();
            bool visible = ((RadioButton)sender).Checked;
            if (visible) 
            {
                gridViewAllEva.ClearColumnsFilter();
            }
        }

        private void barButtonItemEvaluateOperator_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            int code = this.selectedOperator.Code;
            SelectEvaluationForm form = new SelectEvaluationForm(code, supervisedApplicationName);
            if (form.ShowDialog() == DialogResult.OK)
            {
                EvaluationClientData evaluation = form.OperatorEvaluation;
                if (evaluation != null)
                {
                    EvaluationRenderForm render = new EvaluationRenderForm(evaluation, supervisorCode, this.selectedOperator);
                    render.ShowDialog();
                }
            }
        }

        private void printPreviewBarItemModifyCategory_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OperatorCategoryChangeForm form = new OperatorCategoryChangeForm(this.selectedOperator);
            if (!form.IsDisposed)
            {
                form.ShowDialog();
            }
        }

        private void printPreviewBarItemPersonalFileObs_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OperatorObservationForm form = new OperatorObservationForm(supervisorCode, selectedOperator.Code);
			form.firstLevel = FirstLevel;			
            form.ShowDialog();
        }

        private void barButtonItemSend_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ReportPersonaFile report = BuildReport();
            PrintControl con = new PrintControl();

            report.CreateDocument();
            con.PrintingSystem = report.PrintingSystem;
            report.PrintingSystem.ExportOptions.Email.Subject = " ";
            con.PrintingSystem.Document.Name = ResourceLoader.GetString2("PersonalRecord") + " " + this.selectedOperator.FirstName + " " + this.selectedOperator.LastName; ;
            con.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.SendPdf);
            
        }

        private void barButtonItemPrint_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ReportPersonaFile report = BuildReport();

            DialogResult res = report.PrintDialog();
        }

        private void barButtonItemSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            saveFileDialog.Title = ResourceLoader.GetString2("SaveAsFile");
            saveFileDialog.FileName = ResourceLoader.GetString2("PersonalRecord") + " " + this.selectedOperator.FirstName + " " + this.selectedOperator.LastName;
            DialogResult res = saveFileDialog.ShowDialog();
            if (res == DialogResult.OK || res == DialogResult.Yes)
            {
                ReportPersonaFile report = BuildReport();
                report.ExportToHtml(saveFileDialog.FileName);
            }
        }

        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle >= 0)
            {
                OperatorObservationGridData data = (OperatorObservationGridData)this.gridViewObs.GetRow(e.FocusedRowHandle);

                this.textBoxDetails.Text = ResourceLoader.GetString2("Date") + ": " + data.Observation.Date.ToLongDateString() + " \r\n" + ResourceLoader.GetString2("Time") + ": " +
                                            data.Observation.Date.ToString("HH:mm") +
                                            "\r\n" + ResourceLoader.GetString2("Supervisor") + ": " + data.Observation.SupervisorName +
                                            "\r\n" + ResourceLoader.GetString2("ObservationType") + ": " + data.Observation.FriendlyNameObservationType + "\r\n\r\n" +
                                            ResourceLoader.GetString2("Observation") + ":\r\n\r\n" + data.Observation.Observation;
            }
            else
            {
                this.textBoxDetails.Text = "";
            }
        }

        private void gridViewCoursesRealized_DoubleClick(object sender, EventArgs e)
        {
            DevExpress.Utils.DXMouseEventArgs pointXY = e as DevExpress.Utils.DXMouseEventArgs;
            if (pointXY != null)
            {
                if (gridViewCoursesRealized.CalcHitInfo(pointXY.X, pointXY.Y).InRow)
                {
                    OperatorTrainingCourseGridData data = this.gridViewCoursesRealized.GetFocusedRow() as OperatorTrainingCourseGridData;
                    if (data != null)
                    {
                        string hql = SmartCadHqls.GetCustomHql(
							SmartCadHqls.GetTrainingCourseByCode, 
							data.OperatorTrainingCourse.CourseCode);
                        IList list = ServerServiceClient.GetInstance().SearchClientObjects(hql);
                        if (list.Count > 0)
                        {
                            TrainingCourseClientData client = list[0] as TrainingCourseClientData;
                            TrainingCreateForm form = new TrainingCreateForm(client);
                            form.SetReadOnly(true);
                            form.ShowDialog();
                        }
                    }
                }
            }

        }

        private void gridViewAllEva_DoubleClick(object sender, EventArgs e)
        {
            DevExpress.Utils.DXMouseEventArgs pointXY = e as DevExpress.Utils.DXMouseEventArgs;
            if (pointXY != null)
            {
                if (gridViewAllEva.CalcHitInfo(pointXY.X, pointXY.Y).InRow)
                {
                    OperatorEvaluationGridData gridData = ((OperatorEvaluationGridData)gridViewAllEva.GetRow(gridViewAllEva.FocusedRowHandle));
                    if (gridData != null)
                    {
                        EvaluationRenderForm form = new EvaluationRenderForm(gridData.OperatorEvaluation, gridData.Qualification);
                        form.ShowDialog();
                    }
                }
            }
        }

        private void comboBoxFilterEvaluations_SelectedValueChanged(object sender, EventArgs e)
        {
            ComboBoxEdit cb = sender as ComboBoxEdit;
            if (cb != null)
            { 
                string key = cb.EditValue.ToString();
                if( numberMonthFilter.ContainsKey(key))
                {
                    DateTime now = ServerServiceClient.GetInstance().GetTimeFromDB();
                    int months = numberMonthFilter[key];
                    gridViewAllEva.Columns["Date"].FilterMode = DevExpress.XtraGrid.ColumnFilterMode.Value;
                    if (months < 0)
                    {
                        gridViewAllEva.ClearColumnsFilter();
                    }
                    else if (months == 0)
                    {
                        gridViewAllEva.ActiveFilterCriteria = new BinaryOperator("Date", now.Date.AddDays(1 - now.Day), BinaryOperatorType.GreaterOrEqual);
                    }
                    else 
                    {
                        DateTime nMonths = now.Date.AddMonths(months*(-1));
                        nMonths = nMonths.AddDays(1 - nMonths.Day);
                        gridViewAllEva.ActiveFilterCriteria = new BinaryOperator("Date", nMonths, BinaryOperatorType.GreaterOrEqual);
                    }
                }
            }
        }

        private void gridControlObs_ProcessGridKey(object sender, KeyEventArgs e)
        {
            System.Threading.Thread t = new System.Threading.Thread(MethodStopIncrementalSearch);
            t.Start();
            
        }


        private void MethodStopIncrementalSearch() 
        {
            System.Threading.Thread.Sleep(500);
            gridViewObs.StopIncrementalSearch();
        }

        private void gridViewAllEva_ShowGridMenu(object sender, DevExpress.XtraGrid.Views.Grid.GridMenuEventArgs e)
        {
            this.contextMenuStrip1.Enabled = false;
            GridView view = sender as GridView;
            GridHitInfo hitInfo = view.CalcHitInfo(e.Point);
            if (hitInfo.InRow && hitInfo.RowHandle != GridControlEx.AutoFilterRowHandle)
            {
                this.contextMenuStrip1.Enabled = true;
                view.FocusedRowHandle = hitInfo.RowHandle;
            }
        }

        private void contextMenuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (e.ClickedItem.Name == "printMenuItem") 
            { 
                OperatorEvaluationGridData gridData = ((OperatorEvaluationGridData)gridViewAllEva.GetFocusedRow());
                EvaluationReport reportEva = new EvaluationReport(gridData.OperatorEvaluation, gridData.Qualification);
                reportEva.CreateDocument();
                reportEva.PrintDialog();
            }
            else if (e.ClickedItem.Name == "exportPDFMenuItem")
            {
                
                OperatorEvaluationGridData gridData = ((OperatorEvaluationGridData)gridViewAllEva.GetFocusedRow());
                EvaluationReport reportEva = new EvaluationReport(gridData.OperatorEvaluation, gridData.Qualification);
                reportEva.CreateDocument();
                saveFileDialog.Filter = "pdf | *.pdf";
                saveFileDialog.DefaultExt = "pdf";
                string evaName = gridData.EvaluationName+"_"+gridData.OperatorEvaluation.Operator.FirstName + "_"+gridData.OperatorEvaluation.Operator.FirstName;
                saveFileDialog.FileName = evaName;
                DialogResult res = saveFileDialog.ShowDialog(this);
                if (res == DialogResult.OK)
                {
                    reportEva.ExportToPdf(saveFileDialog.FileName);
                }
            }
            else
            {
                OperatorEvaluationGridData gridData = ((OperatorEvaluationGridData)gridViewAllEva.GetFocusedRow());
                EvaluationReport reportEva = new EvaluationReport(gridData.OperatorEvaluation, gridData.Qualification);
                reportEva.CreateDocument();
                saveFileDialog.Filter = "html | *.html";
                saveFileDialog.DefaultExt = "html";
                string evaName = gridData.EvaluationName + "_" + gridData.OperatorEvaluation.Operator.FirstName + "_" + gridData.OperatorEvaluation.Operator.FirstName;
                saveFileDialog.FileName = evaName;
                DialogResult res = saveFileDialog.ShowDialog(this);
                if (res == DialogResult.OK)
                {
                    reportEva.ExportToHtml(saveFileDialog.FileName);
                }
            }
            
            contextMenuStrip1.Close();
            contextMenuStrip1.Enabled = false;
            
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            e.Cancel = !this.contextMenuStrip1.Enabled;
        }
    }
}
