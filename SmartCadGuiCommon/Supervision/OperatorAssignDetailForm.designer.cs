using SmartCadControls;
namespace SmartCadGuiCommon
{
    partial class OperatorAssignDetailForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OperatorAssignDetailForm));
            this.labelControlName = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlDetail = new DevExpress.XtraLayout.LayoutControl();
            this.gridControlExWithoutSupervision = new GridControlEx();
            this.gridViewExWithoutSupervision = new GridViewEx();
            this.simpleButtonAccept = new DevExpress.XtraEditors.SimpleButton();
            this.gridControlExAssignments = new GridControlEx();
            this.gridViewExAssignments = new GridViewEx();
            this.gridViewEx1 = new GridViewEx();
            this.labelControlEnd = new DevExpress.XtraEditors.LabelControl();
            this.labelControlBegin = new DevExpress.XtraEditors.LabelControl();
            this.labelControlWorkShift = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupInf = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemBegin = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemWSName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemEnd = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupAssignments = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupWithoutSupervision = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlDetail)).BeginInit();
            this.layoutControlDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExWithoutSupervision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExWithoutSupervision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExAssignments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExAssignments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEx1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupInf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBegin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWSName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupAssignments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupWithoutSupervision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControlName
            // 
            this.labelControlName.Location = new System.Drawing.Point(147, 30);
            this.labelControlName.Name = "labelControlName";
            this.labelControlName.Size = new System.Drawing.Size(84, 13);
            this.labelControlName.StyleController = this.layoutControlDetail;
            this.labelControlName.TabIndex = 0;
            this.labelControlName.Text = "labelControlName";
            // 
            // layoutControlDetail
            // 
            this.layoutControlDetail.AllowCustomizationMenu = false;
            this.layoutControlDetail.Appearance.DisabledLayoutGroupCaption.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControlDetail.Appearance.DisabledLayoutGroupCaption.Options.UseForeColor = true;
            this.layoutControlDetail.Appearance.DisabledLayoutItem.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControlDetail.Appearance.DisabledLayoutItem.Options.UseForeColor = true;
            this.layoutControlDetail.Controls.Add(this.gridControlExWithoutSupervision);
            this.layoutControlDetail.Controls.Add(this.simpleButtonAccept);
            this.layoutControlDetail.Controls.Add(this.gridControlExAssignments);
            this.layoutControlDetail.Controls.Add(this.labelControlEnd);
            this.layoutControlDetail.Controls.Add(this.labelControlBegin);
            this.layoutControlDetail.Controls.Add(this.labelControlWorkShift);
            this.layoutControlDetail.Controls.Add(this.labelControlName);
            this.layoutControlDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlDetail.Location = new System.Drawing.Point(0, 0);
            this.layoutControlDetail.Name = "layoutControlDetail";
            this.layoutControlDetail.Root = this.layoutControlGroup1;
            this.layoutControlDetail.Size = new System.Drawing.Size(392, 516);
            this.layoutControlDetail.TabIndex = 1;
            this.layoutControlDetail.Text = "layoutControl1";
            // 
            // gridControlExWithoutSupervision
            // 
            this.gridControlExWithoutSupervision.EnableAutoFilter = true;
            this.gridControlExWithoutSupervision.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExWithoutSupervision.Location = new System.Drawing.Point(12, 336);
            this.gridControlExWithoutSupervision.MainView = this.gridViewExWithoutSupervision;
            this.gridControlExWithoutSupervision.Name = "gridControlExWithoutSupervision";
            this.gridControlExWithoutSupervision.Size = new System.Drawing.Size(369, 133);
            this.gridControlExWithoutSupervision.TabIndex = 9;
            this.gridControlExWithoutSupervision.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExWithoutSupervision});
            this.gridControlExWithoutSupervision.ViewTotalRows = true;
            // 
            // gridViewExWithoutSupervision
            // 
            this.gridViewExWithoutSupervision.AllowFocusedRowChanged = true;
            this.gridViewExWithoutSupervision.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExWithoutSupervision.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExWithoutSupervision.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExWithoutSupervision.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExWithoutSupervision.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewExWithoutSupervision.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewExWithoutSupervision.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExWithoutSupervision.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExWithoutSupervision.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExWithoutSupervision.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExWithoutSupervision.EnablePreviewLineForFocusedRow = false;
            this.gridViewExWithoutSupervision.GridControl = this.gridControlExWithoutSupervision;
            this.gridViewExWithoutSupervision.Name = "gridViewExWithoutSupervision";
            this.gridViewExWithoutSupervision.OptionsMenu.EnableFooterMenu = false;
            this.gridViewExWithoutSupervision.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExWithoutSupervision.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExWithoutSupervision.OptionsView.ShowFooter = true;
            this.gridViewExWithoutSupervision.ViewTotalRows = true;
            // 
            // simpleButtonAccept
            // 
            this.simpleButtonAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButtonAccept.Location = new System.Drawing.Point(309, 483);
            this.simpleButtonAccept.Name = "simpleButtonAccept";
            this.simpleButtonAccept.Size = new System.Drawing.Size(75, 25);
            this.simpleButtonAccept.StyleController = this.layoutControlDetail;
            this.simpleButtonAccept.TabIndex = 8;
            this.simpleButtonAccept.Text = "simpleButton1";
            // 
            // gridControlExAssignments
            // 
            this.gridControlExAssignments.EnableAutoFilter = true;
            this.gridControlExAssignments.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExAssignments.Location = new System.Drawing.Point(9, 147);
            this.gridControlExAssignments.MainView = this.gridViewExAssignments;
            this.gridControlExAssignments.Name = "gridControlExAssignments";
            this.gridControlExAssignments.Size = new System.Drawing.Size(375, 157);
            this.gridControlExAssignments.TabIndex = 7;
            this.gridControlExAssignments.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExAssignments,
            this.gridViewEx1});
            this.gridControlExAssignments.ViewTotalRows = true;
            // 
            // gridViewExAssignments
            // 
            this.gridViewExAssignments.AllowFocusedRowChanged = true;
            this.gridViewExAssignments.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExAssignments.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExAssignments.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExAssignments.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExAssignments.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewExAssignments.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewExAssignments.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExAssignments.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExAssignments.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExAssignments.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExAssignments.EnablePreviewLineForFocusedRow = false;
            this.gridViewExAssignments.GridControl = this.gridControlExAssignments;
            this.gridViewExAssignments.Name = "gridViewExAssignments";
            this.gridViewExAssignments.OptionsMenu.EnableFooterMenu = false;
            this.gridViewExAssignments.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExAssignments.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExAssignments.OptionsView.ShowFooter = true;
            this.gridViewExAssignments.ViewTotalRows = true;
            // 
            // gridViewEx1
            // 
            this.gridViewEx1.AllowFocusedRowChanged = true;
            this.gridViewEx1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewEx1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewEx1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewEx1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewEx1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewEx1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewEx1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewEx1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewEx1.EnablePreviewLineForFocusedRow = false;
            this.gridViewEx1.GridControl = this.gridControlExAssignments;
            this.gridViewEx1.Name = "gridViewEx1";
            this.gridViewEx1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewEx1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewEx1.ViewTotalRows = false;
            // 
            // labelControlEnd
            // 
            this.labelControlEnd.Location = new System.Drawing.Point(147, 102);
            this.labelControlEnd.Name = "labelControlEnd";
            this.labelControlEnd.Size = new System.Drawing.Size(75, 13);
            this.labelControlEnd.StyleController = this.layoutControlDetail;
            this.labelControlEnd.TabIndex = 6;
            this.labelControlEnd.Text = "labelControlEnd";
            // 
            // labelControlBegin
            // 
            this.labelControlBegin.Location = new System.Drawing.Point(147, 78);
            this.labelControlBegin.Name = "labelControlBegin";
            this.labelControlBegin.Size = new System.Drawing.Size(83, 13);
            this.labelControlBegin.StyleController = this.layoutControlDetail;
            this.labelControlBegin.TabIndex = 5;
            this.labelControlBegin.Text = "labelControlBegin";
            // 
            // labelControlWorkShift
            // 
            this.labelControlWorkShift.Location = new System.Drawing.Point(147, 54);
            this.labelControlWorkShift.Name = "labelControlWorkShift";
            this.labelControlWorkShift.Size = new System.Drawing.Size(104, 13);
            this.labelControlWorkShift.StyleController = this.layoutControlDetail;
            this.labelControlWorkShift.TabIndex = 4;
            this.labelControlWorkShift.Text = "labelControlWorkShift";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupInf,
            this.layoutControlGroupAssignments,
            this.emptySpaceItem1,
            this.layoutControlItem2,
            this.layoutControlGroupWithoutSupervision});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(392, 516);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroupInf
            // 
            this.layoutControlGroupInf.CustomizationFormText = "layoutControlGroupInf";
            this.layoutControlGroupInf.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemBegin,
            this.layoutControlItemName,
            this.layoutControlItemWSName,
            this.layoutControlItemEnd});
            this.layoutControlGroupInf.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupInf.Name = "layoutControlGroupInf";
            this.layoutControlGroupInf.Size = new System.Drawing.Size(386, 120);
            this.layoutControlGroupInf.Text = "layoutControlGroupInf";
            // 
            // layoutControlItemBegin
            // 
            this.layoutControlItemBegin.Control = this.labelControlBegin;
            this.layoutControlItemBegin.CustomizationFormText = "layoutControlItemBegin";
            this.layoutControlItemBegin.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItemBegin.Name = "layoutControlItemBegin";
            this.layoutControlItemBegin.Size = new System.Drawing.Size(380, 24);
            this.layoutControlItemBegin.Text = "layoutControlItemBegin";
            this.layoutControlItemBegin.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemBegin.TextSize = new System.Drawing.Size(130, 20);
            // 
            // layoutControlItemName
            // 
            this.layoutControlItemName.Control = this.labelControlName;
            this.layoutControlItemName.CustomizationFormText = "layoutControlItemName";
            this.layoutControlItemName.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemName.Name = "layoutControlItemName";
            this.layoutControlItemName.Size = new System.Drawing.Size(380, 24);
            this.layoutControlItemName.Text = "layoutControlItemName";
            this.layoutControlItemName.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemName.TextSize = new System.Drawing.Size(130, 20);
            // 
            // layoutControlItemWSName
            // 
            this.layoutControlItemWSName.Control = this.labelControlWorkShift;
            this.layoutControlItemWSName.CustomizationFormText = "layoutControlItemWSName";
            this.layoutControlItemWSName.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItemWSName.Name = "layoutControlItemWSName";
            this.layoutControlItemWSName.Size = new System.Drawing.Size(380, 24);
            this.layoutControlItemWSName.Text = "layoutControlItemWSName";
            this.layoutControlItemWSName.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemWSName.TextSize = new System.Drawing.Size(130, 20);
            // 
            // layoutControlItemEnd
            // 
            this.layoutControlItemEnd.Control = this.labelControlEnd;
            this.layoutControlItemEnd.CustomizationFormText = "layoutControlItemEnd";
            this.layoutControlItemEnd.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItemEnd.Name = "layoutControlItemEnd";
            this.layoutControlItemEnd.Size = new System.Drawing.Size(380, 24);
            this.layoutControlItemEnd.Text = "layoutControlItemEnd";
            this.layoutControlItemEnd.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemEnd.TextSize = new System.Drawing.Size(130, 20);
            // 
            // layoutControlGroupAssignments
            // 
            this.layoutControlGroupAssignments.CustomizationFormText = "layoutControlGroupAssignments";
            this.layoutControlGroupAssignments.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroupAssignments.Location = new System.Drawing.Point(0, 120);
            this.layoutControlGroupAssignments.Name = "layoutControlGroupAssignments";
            this.layoutControlGroupAssignments.Size = new System.Drawing.Size(386, 186);
            this.layoutControlGroupAssignments.Text = "layoutControlGroupAssignments";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridControlExAssignments;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem1.Size = new System.Drawing.Size(380, 162);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 474);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(300, 36);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.simpleButtonAccept;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(300, 474);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlGroupWithoutSupervision
            // 
            this.layoutControlGroupWithoutSupervision.CustomizationFormText = "layoutControlGroupWithoutSupervision";
            this.layoutControlGroupWithoutSupervision.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.layoutControlGroupWithoutSupervision.Location = new System.Drawing.Point(0, 306);
            this.layoutControlGroupWithoutSupervision.Name = "layoutControlGroupWithoutSupervision";
            this.layoutControlGroupWithoutSupervision.Size = new System.Drawing.Size(386, 168);
            this.layoutControlGroupWithoutSupervision.Text = "layoutControlGroupWithoutSupervision";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.gridControlExWithoutSupervision;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(380, 144);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // OperatorAssignDetailForm
            // 
            this.AcceptButton = this.simpleButtonAccept;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(392, 516);
            this.Controls.Add(this.layoutControlDetail);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(400, 550);
            this.Name = "OperatorAssignDetailForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "OperatorAssignDetailForm";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlDetail)).EndInit();
            this.layoutControlDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExWithoutSupervision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExWithoutSupervision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExAssignments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExAssignments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEx1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupInf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBegin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWSName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupAssignments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupWithoutSupervision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControlName;
        private DevExpress.XtraLayout.LayoutControl layoutControlDetail;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemName;
        private DevExpress.XtraEditors.LabelControl labelControlEnd;
        private DevExpress.XtraEditors.LabelControl labelControlBegin;
        private DevExpress.XtraEditors.LabelControl labelControlWorkShift;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemWSName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemBegin;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemEnd;
        private GridControlEx gridControlExAssignments;
        private GridViewEx gridViewExAssignments;
        private GridViewEx gridViewEx1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupInf;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupAssignments;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.SimpleButton simpleButtonAccept;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private GridControlEx gridControlExWithoutSupervision;
        private GridViewEx gridViewExWithoutSupervision;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupWithoutSupervision;
    }
}