﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Smartmatic.SmartCad.Service;
using DevExpress.XtraScheduler;
using DevExpress.XtraEditors;
using DevExpress.Data.Filtering;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Utils;
using DevExpress.XtraGrid.Columns;
using SmartCadCore.ClientData;
using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadGuiCommon.SyncBoxes;

namespace SmartCadGuiCommon
{
    public partial class ConsultExtraTimeVacationsForms : XtraForm
    {

        public enum ModeConsult 
        { 
            ExtraTime,
            VacationsPermisions,
            None
        }


        private List<OperatorConsultExtraVacationsGridData> dataSourceOperators;
        private Dictionary<int, WorkShiftVariationClientData> setWorkShift;
        private Dictionary<int, List<OperatorClientData>> mapWorkShifVariations;
        private Dictionary<int, int> WorkShifVariationsCounts;
        private Dictionary<int, List<int>> operatorToWorkShifVariations;
        private ModeConsult mode;
        private WorkShiftVariationClientData all;
      //  private WorkShiftClientData notAsociated;
        private int operatorsSelected = 0;

        private enum FilterActive 
        { 
            OperatorFilter,
            WorkShiftFilter,
            None
        };

        private FilterActive filter;
        //private CriteriaOperator workShiftCriteria;

        public ConsultExtraTimeVacationsForms()
        {
            InitializeComponent();
            dataSourceOperators = new List<OperatorConsultExtraVacationsGridData>();
            mapWorkShifVariations = new Dictionary<int, List<OperatorClientData>>();
            WorkShifVariationsCounts = new Dictionary<int, int>();
            operatorToWorkShifVariations = new Dictionary<int, List<int>>();
            this.schedulerControlDefaultView.Dock = DockStyle.Fill;
            this.schedulerControlCustomView.Visible = false;
            this.FormClosing += new FormClosingEventHandler(ConsultExtraTimeVacationsForms_FormClosing);
        }

        private void ConsultExtraTimeVacationsForms_Load(object sender, EventArgs e)
        {
            (this.MdiParent as SupervisionForm).SupervisionCommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(ConsultExtraTimeVacationsForms_SupervisionCommittedChanges);
        }

        void ConsultExtraTimeVacationsForms_FormClosing(object sender, FormClosingEventArgs e)
        {
            if ((this.MdiParent as SupervisionForm) != null)
                (this.MdiParent as SupervisionForm).SupervisionCommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(ConsultExtraTimeVacationsForms_SupervisionCommittedChanges);
        }

        public ConsultExtraTimeVacationsForms(string applicationName,DepartmentTypeClientData departamentType,ModeConsult mode, bool isGeneralSupervisor) : this()
        {
            this.mode = mode;
            LoadLanguage();
            LoadData(applicationName, departamentType, isGeneralSupervisor);

            this.radioGroupFilter.SelectedIndex = 0;
            filter = FilterActive.WorkShiftFilter;

            if (isGeneralSupervisor == false)
            {
                this.barButtonItemCreateExtraTime.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                this.barButtonItemDeleteExtraTime.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                this.barButtonItemModifyExtraTime.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                this.barButtonItemPersonalAssignExtraTime.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                this.barButtonItemCreateVacations.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                this.barButtonItemDeleteVacations.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                this.barButtonItemModifyVacations.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                this.barButtonItemPersonalAssignVacations.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            }
            else
            {
                this.barButtonItemCreateExtraTime.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                this.barButtonItemDeleteExtraTime.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                this.barButtonItemModifyExtraTime.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                this.barButtonItemPersonalAssignExtraTime.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                this.barButtonItemCreateVacations.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                this.barButtonItemDeleteVacations.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                this.barButtonItemModifyVacations.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                this.barButtonItemPersonalAssignVacations.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
            }

            //Initialize dateNavigator
            dateNavigator1.BeginInit();
            this.dateNavigator1.SchedulerControl = this.schedulerControlDefaultView;
            dateNavigator1.SchedulerControl.Start = DateTime.Now.AddDays(-DateTime.Now.Day);
            this.dateNavigator1.View = DevExpress.XtraEditors.Controls.DateEditCalendarViewType.MonthInfo;
            dateNavigator1.EndInit();
         
        }

        private void CreateAppointmentAllOperator()
        {
            List<Appointment> listapp = new List<Appointment>();
            foreach (OperatorConsultExtraVacationsGridData data in dataSourceOperators)
            {
                OperatorClientData ope = data.Operator;

              //  string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetWorkShiftVariationWithSchedulesByOperatorCode, ope.Code);
              //  IList list = ServerServiceClient.GetInstance().SearchClientObjects(hql);
             
                List<int> listAux = new List<int>();
                foreach (WorkShiftVariationClientData var in ope.WorkShifts)
                {
                    
                    if (mapWorkShifVariations.ContainsKey(var.Code))
                    {
                        mapWorkShifVariations[var.Code].Add(ope);
                        listAux.Add(var.Code);
                    }
                    else
                    {
                        //bool modeOk = false;
                        if (var.Type != WorkShiftVariationClientData.WorkShiftType.TimeOff)
                        {
                            if (mode == ModeConsult.ExtraTime)
                            {
                                listAux.Add(var.Code);
                                WorkShifVariationsCounts.Add(var.Code, 0);
                                listapp.AddRange(GetListAppointments(var, ope.FirstName, ope.LastName, ope.Code));
                                mapWorkShifVariations.Add(var.Code, new List<OperatorClientData>(new OperatorClientData[] { ope }));            
                            }
                        }
                        else 
                        {
                            if (mode == ModeConsult.VacationsPermisions)
                            {
                                listAux.Add(var.Code);
                                WorkShifVariationsCounts.Add(var.Code, 0);
                                listapp.AddRange(GetListAppointments(var, ope.FirstName, ope.LastName, ope.Code));
                                mapWorkShifVariations.Add(var.Code, new List<OperatorClientData>(new OperatorClientData[] { ope }));
                            }
                        }
                    }
                }
                operatorToWorkShifVariations.Add(ope.Code, listAux);
               
               
            }
            schedulerControlCustomView.Storage.BeginUpdate();
            schedulerControlCustomView.Storage.Appointments.AddRange(listapp.ToArray());
            schedulerControlCustomView.Storage.EndUpdate();
        }

        private void LoadData(string applicationName, DepartmentTypeClientData departamentType, bool isGeneralSupervisor)
        {   
            GetListOperatorsByCase(isGeneralSupervisor,departamentType);

            all = new WorkShiftVariationClientData();
            all.Code = -1;
            all.Name = ResourceLoader.GetString2("All");
            
			this.gridControlOperators.Type = typeof(OperatorConsultExtraVacationsGridData);
            this.gridControlOperators.BeginInit();
            this.gridControlOperators.DataSource = dataSourceOperators;
            this.gridControlOperators.EndInit();
            this.gridViewOperators.ExpandAllGroups();
			this.gridControlOperators.ViewTotalRows = true;

			foreach (GridColumn col in gridViewOperators.Columns)
			{
				if (col.FieldName.Equals("Visible"))
					col.OptionsColumn.AllowEdit = true;
				else
					col.OptionsColumn.AllowEdit = false;
			}
			gridViewOperators.Columns["Visible"].OptionsColumn.ReadOnly = false;
            gridViewOperators.Columns["Visible"].OptionsColumn.ShowCaption = false;
            gridViewOperators.OptionsBehavior.Editable = true;

			CreateAppointmentAllOperator();
            SeekSetWorkShift(departamentType, isGeneralSupervisor);
            comboBoxWorkShift.Properties.Items.AddRange(setWorkShift.Values);
            comboBoxWorkShift.Properties.Items.Add(all);
            comboBoxWorkShift.SelectedItem = all;
        }

        

        private void SeekSetWorkShift(DepartmentTypeClientData departamentType, bool isGeneralSupervisor)
        {
            //IList list = null;
            //bool isTimeOff = mode == ModeConsult.VacationsPermisions ? true : false;
            
            //setWorkShift = new Dictionary<int, WorkShiftVariationClientData>();
            //string hql = "";

            //if (isGeneralSupervisor)
            //{
            //    if (isTimeOff == true)
            //    {
            //        if (departamentType != null)
            //            hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetWorkShiftVariationTimeOffByTypeAndAccess, (int)WorkShiftVariationClientData.WorkShiftType.TimeOff, UserApplicationClientData.Dispatch.Name, departamentType.Name);
            //        else
            //            hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetWorkShiftVariationTimeOffByTypeAndAccess, (int)WorkShiftVariationClientData.WorkShiftType.TimeOff, UserApplicationClientData.FirstLevel.Name, null);
            //    }
            //    else
            //    {
            //     if(departamentType != null)
            //        hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetWorkShiftVariationByTypeAndAccess, (int)WorkShiftVariationClientData.WorkShiftType.TimeOff, UserApplicationClientData.Dispatch.Name, departamentType.Name);
            //     else
            //        hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetWorkShiftVariationByTypeAndAccess, (int)WorkShiftVariationClientData.WorkShiftType.TimeOff, UserApplicationClientData.FirstLevel.Name, null);
                
            //    }

            //    list = ServerServiceClient.GetInstance().SearchClientObjects(hql);
            //    foreach (WorkShiftVariationClientData var in list)
            //    {
            //        if (!setWorkShift.ContainsKey(var.Code))
            //        {
            //            setWorkShift.Add(var.Code, var);
            //        }
            //    }
            //}
            //else
            //{
            //    ArrayList listSet = new ArrayList();
            //    foreach (OperatorConsultExtraVacationsGridData var in dataSourceOperators)
            //    {
            //        if (var.Operator.WorkShifts != null && var.Operator.WorkShifts.Count > 0)
            //        {
            //            if (isTimeOff == false)
            //              hql = SmartCadHqls.GetCustomHql(SmartCadHqls.CountWorkShiftVariationByTypeOperatorCode, (int)WorkShiftVariationClientData.WorkShiftType.TimeOff, var.Operator.Code);
            //            else
            //              hql = SmartCadHqls.GetCustomHql(SmartCadHqls.CountWorkShiftVariationTimeOffByTypeOperatorCode, (int)WorkShiftVariationClientData.WorkShiftType.TimeOff, var.Operator.Code);
                       
            //            ArrayList listCount = (ArrayList)ServerServiceClient.GetInstance().SearchBasicObjects(hql);
            //            if (listCount.Count > 0)
            //            {
            //                long count = (long)listCount[0];
            //                if (count > 0)
            //                {
            //                    listSet.AddRange(var.Operator.WorkShifts);
            //                }
            //            }
            //        }
            //    }
            //    foreach (WorkShiftVariationClientData var in listSet)
            //    {
            //        if (!setWorkShift.ContainsKey(var.Code))
            //        {
            //            setWorkShift.Add(var.Code, var);
            //        }
            //    }
            //}
            
            
        }

        private IList GetListOperatorsByCase(bool isGeneralSupervisor, DepartmentTypeClientData departamentType)
        {
            IList retval = null;
            string hql = "";
            if (isGeneralSupervisor)
            {
                if (departamentType == null || string.IsNullOrEmpty(departamentType.Name))
                {
                    retval = GetListOperatorsFirstLevel();
                }
                else
                {
                    retval = GetListOperatorDispatch(departamentType);
                }
            }
            else
            {
                int code = ServerServiceClient.GetInstance().OperatorClient.Code;
                
                hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorsDistinctBySupervisorCode,code);
                retval = (IList)ServerServiceClient.GetInstance().SearchClientObjects(hql);
                foreach (OperatorClientData var in retval)
                {
                    dataSourceOperators.Add(new OperatorConsultExtraVacationsGridData(var));
                }
            }

          
            SetWorkShiftsToOperators();

            for (int i = 0; i < dataSourceOperators.Count; i++)
            {
                OperatorClientData oper = dataSourceOperators[i].Operator;

                for (int j = 0; j < oper.WorkShifts.Count; j++)
                {
                    WorkShiftVariationClientData ws = oper.WorkShifts[j] as WorkShiftVariationClientData;

                    if (setWorkShift.ContainsKey(ws.Code) == true)
                        ws = setWorkShift[ws.Code];

                    oper.WorkShifts[j] = ws;
                }
                dataSourceOperators[i] = new OperatorConsultExtraVacationsGridData(oper);
            }


            return retval;
        }

        private void SetWorkShiftsToOperators() 
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("(-1,");
            
            foreach (OperatorConsultExtraVacationsGridData operC  in dataSourceOperators)
            {
                builder.Append(operC.Operator.Code);
                builder.Append(",");
            }
            builder.Remove(builder.Length - 1, 1);
            builder.Append(")");

            string hql = string.Empty;

            if (mode == ModeConsult.ExtraTime)
            {
                hql = SmartCadHqls.GetCustomHql(@"SELECT distinct var 
                                                     FROM WorkShiftVariationData var 
                                                          inner join var.Operators operators
                                                          left join fetch var.Schedules schedules
                                                     WHERE operators.Operator.Code in {0} AND var.Type != 2 AND  var.ObjectType = 0", builder.ToString());
            }
            else 
            {
                hql = SmartCadHqls.GetCustomHql(@"SELECT distinct var 
                                                     FROM WorkShiftVariationData var 
                                                          inner join var.Operators operators
                                                          left join fetch var.Schedules schedules
                                                     WHERE operators.Operator.Code in {0} AND var.Type = 2  AND var.ObjectType = 0", builder.ToString());
            }
            
            IList list = ServerServiceClient.GetInstance().SearchClientObjects(hql);

            setWorkShift = new Dictionary<int, WorkShiftVariationClientData>();
            foreach (WorkShiftVariationClientData var in list)
            {
               setWorkShift.Add(var.Code, var);  
            }

        }


        private IList GetListOperatorDispatch(DepartmentTypeClientData departamentType)
        {
            IList retval = null;
            string hql = "";

            hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetDispatchOperatorsWithWorkShifts, departamentType.Code);
            retval = ServerServiceClient.GetInstance().SearchClientObjects(hql);

            //ArrayList opeList = new ArrayList();
            ////get operators..
            //hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetDispatchOperatorsByDeparment, departamentType.Code);
            //retval = ServerServiceClient.GetInstance().SearchClientObjects(hql);
            //opeList.AddRange(retval);
            ////get supervisors..
            //hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetSupervisorsDispacthByDepartmentName, departamentType.Name);
            //retval = ServerServiceClient.GetInstance().SearchClientObjects(hql);
            //opeList.AddRange(retval);

            //retval.Clear();
            foreach (OperatorClientData var in retval)
            {
                dataSourceOperators.Add(new OperatorConsultExtraVacationsGridData(var));
            }
            return retval;
        }

        private IList GetListOperatorsFirstLevel()
        {
            IList retval = null;
            string hql ="";

            //get firstLevel operators and supervisors
            hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetFirstLevelOperatorsWithWorkShifts);
            retval = ServerServiceClient.GetInstance().SearchClientObjects(hql);
                       
            //ArrayList opeList = new ArrayList();
            ////get operators..
            //hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorsByApplication, UserApplicationClientData.FirstLevel.Name);
            //retval = ServerServiceClient.GetInstance().SearchClientObjects(hql);
            //opeList.AddRange(retval);
            ////get supervisors..
            //hql = SmartCadHqls.GetSupervisorsFirstLevel;
            //retval = ServerServiceClient.GetInstance().SearchClientObjects(hql);
            //opeList.AddRange(retval);
          
            //retval.Clear();

            foreach (OperatorClientData var in retval)
            {                 
                    dataSourceOperators.Add(new OperatorConsultExtraVacationsGridData(var));
                
            }
            return retval;
        }

        private void CreateAppointmentByOperator(OperatorClientData ope)
        {
            string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetWorkShiftVariationWithSchedulesByOperatorCode, ope.Code);
            IList list = ServerServiceClient.GetInstance().SearchClientObjects(hql);
            List<Appointment> listapp = new List<Appointment>();
            foreach (WorkShiftVariationClientData var in list)
            {

                if (mapWorkShifVariations.ContainsKey(var.Code))
                {
                    mapWorkShifVariations[var.Code].Add(ope);
                }
                else
                {
                    listapp.AddRange(GetListAppointments(var, ope.FirstName, ope.LastName, ope.Code));
                    mapWorkShifVariations.Add(var.Code, new List<OperatorClientData>(new OperatorClientData[] { ope }));
                }
            }
            schedulerControlCustomView.Storage.BeginUpdate();
            schedulerControlCustomView.Storage.Appointments.AddRange(listapp.ToArray());
            schedulerControlCustomView.Storage.EndUpdate();
        }

        private List<Appointment> GetListAppointments(WorkShiftVariationClientData client,string operatorName,string operatorLastName,int operatorCode)
        {
            List<Appointment> retval = new List<Appointment>();

            foreach (WorkShiftScheduleVariationClientData var in client.Schedules)
            {
                Appointment app = this.schedulerStorage1.CreateAppointment(AppointmentType.Normal);
                app.Start = var.Start;
                app.End = var.End;
                app.Subject = client.Name;
                app.Description = client.Description;
                if (client.Type == WorkShiftVariationClientData.WorkShiftType.ExtraTime)
                {
                    app.CustomFields["AType"] = ResourceLoader.GetString2("ExtraTime");
                    app.LabelId = 1;
                }
                else if (client.Type == WorkShiftVariationClientData.WorkShiftType.TimeOff)
                {
                    app.CustomFields["AType"] = ResourceLoader.GetString2("FreeTimeFormText");
                    app.LabelId = 2;
                }
				else if (client.Type == WorkShiftVariationClientData.WorkShiftType.WorkShift)
				{
					app.CustomFields["AType"] = ResourceLoader.GetString2("TypeWorkShift");
					app.LabelId = 0;
				}
                //app.CustomFields["WorkShiftCode"] = client.WorkShiftCode;
                app.CustomFields["WorkShiftVariationCode"] = client.Code;
                app.CustomFields["WorkShiftVariationScheduleCode"] = var.Code;
                app.CustomFields["Color"] = schedulerControlCustomView.Storage.Appointments.Labels[app.LabelId].Color;
                retval.Add(app);
            }
            return retval;
        }

        private void LoadLanguage()
        {
            if (mode == ModeConsult.VacationsPermisions)
            {
                this.Text = ResourceLoader.GetString2("TitleConsultVacationsForm");
                layoutControlItemComboWs.Text = ResourceLoader.GetString2("VacationAndPermission") + ":"; 
                radioGroupFilter.Properties.Items[0].Description = ResourceLoader.GetString2("VacationsAndPermissions");
            }
            else
            {
                this.Text = ResourceLoader.GetString2("TitleConsultWorkShiftForm");
                layoutControlItemComboWs.Text = ResourceLoader.GetString2("Schedule") + ":";
                radioGroupFilter.Properties.Items[0].Description = ResourceLoader.GetString2("Schedules");
            }
            
            radioGroupFilter.Properties.Items[1].Description = ResourceLoader.GetString2("Personal");
            this.schedulerControlDefaultView.OptionsView.NavigationButtons.NextCaption = ResourceLoader.GetString2("NextEvent");
            this.schedulerControlDefaultView.OptionsView.NavigationButtons.PrevCaption = ResourceLoader.GetString2("PreviousEvent");
            this.schedulerControlCustomView.OptionsView.NavigationButtons.NextCaption = ResourceLoader.GetString2("NextEvent");
            this.schedulerControlCustomView.OptionsView.NavigationButtons.PrevCaption = ResourceLoader.GetString2("PreviousEvent");


            this.viewNavigatorBackwardItem1.Caption = ResourceLoader.GetString2("ViewNavigatorBackwardTitle");
            this.viewNavigatorBackwardItem1.SuperTip = GetSuperToolTip("ViewNavigatorBackwardTitle", "ViewNavigatorBackwardSuperTipContents");



            this.viewNavigatorForwardItem1.Caption = ResourceLoader.GetString2("ViewNavigatorForwardTitle");
            this.viewNavigatorForwardItem1.SuperTip = GetSuperToolTip("ViewNavigatorForwardTitle", "ViewNavigatorForwardSuperTipContents");

            this.viewNavigatorTodayItem1.Caption = ResourceLoader.GetString2("ViewNavigatorTodayTitle");
            this.viewNavigatorTodayItem1.SuperTip = GetSuperToolTip("ViewNavigatorTodayTitle", "ViewNavigatorTodaySuperTipContents");

            this.viewNavigatorZoomInItem1.Caption = ResourceLoader.GetString2("ZoomIn");
            this.viewNavigatorZoomInItem1.SuperTip = GetSuperToolTip("ZoomIn", "ZoomInSuperToolTipContents");


            this.viewNavigatorZoomOutItem1.Caption = ResourceLoader.GetString2("ZoomOut");
            this.viewNavigatorZoomOutItem1.SuperTip = GetSuperToolTip("ZoomOut", "ZoomOutSuperToolTipContents");

            this.viewSelectorItem1.Caption = ResourceLoader.GetString2("Day");
            this.viewSelectorItem1.SuperTip = GetSuperToolTip("Day", "DaySuperToolTipContents");

            this.viewSelectorItem2.Caption = ResourceLoader.GetString2("WorkWeek");
            this.viewSelectorItem2.SuperTip = GetSuperToolTip("WorkWeek", "WorkWeekSuperToolTipContents");

            this.viewSelectorItem3.Caption = ResourceLoader.GetString2("Week");
            this.viewSelectorItem3.SuperTip = GetSuperToolTip("Week", "WeekSuperToolTipContents");

            this.viewSelectorItem4.Caption = ResourceLoader.GetString2("Month");
            this.viewSelectorItem4.SuperTip = GetSuperToolTip("Month", "MonthSuperToolTipContents");

            this.viewSelectorItem5.Caption = ResourceLoader.GetString2("TimeLine");
            this.viewSelectorItem5.SuperTip = GetSuperToolTip("TimeLine", "TimeLineSuperToolTipContents");



            this.dateNavigator1.TodayButton.Text = ResourceLoader.GetString2("Today");
            this.dockPanel1.Text = ResourceLoader.GetString2("Tools");
            
            this.dateNavigator1.TodayButton.Text = ResourceLoader.GetString2("ViewNavigatorTodayTitle");


            // Method 1
            SuperToolTip sTooltip1 = new SuperToolTip();
            // Create a tooltip item that represents a header.
            ToolTipTitleItem titleItem1 = new ToolTipTitleItem();
            titleItem1.Text = ResourceLoader.GetString2("TimeLine");
            // Create a tooltip item that represents the SuperTooltip's contents.
            ToolTipItem item1 = new ToolTipItem();
            item1.Text = ResourceLoader.GetString2("ToolTipSchedulerBarControl");
            // Add the tooltip items to the SuperTooltip.
            sTooltip1.Items.Add(titleItem1);
            sTooltip1.Items.Add(item1);
            this.toolTipController.SetSuperTip(dateNavigator1, sTooltip1);

            checkEdit1.Text = ResourceLoader.GetString2("SelectAll");
            //this.layoutControlGroup2.Text = ResourceLoader.GetString2("Searchs");
            //this.layoutControlGroup5.Text = ResourceLoader.GetString2("VariationOperators");
            this.layoutControlGroupCalendar.Text = ResourceLoader.GetString2("Calendar");
            this.layoutControlGroupFilter.Text = ResourceLoader.GetString2("Filter");

            barButtonItemPersonalAssignExtraTime.Caption = ResourceLoader.GetString2("Personal");
            barButtonItemPersonalAssignVacations.Caption = ResourceLoader.GetString2("Personal");
            RibbonPageOperation.Text = ResourceLoader.GetString2("Operation");
            ribbonPageGroupExtraTime.Text = ResourceLoader.GetString2("ExtraTime");
            ribbonPageGroupExtraTime.Visible = false;
            ribbonPageGroupVacationAndAbsence.Text = ResourceLoader.GetString2("VacationsAndPermissions");
            barButtonItemCreateExtraTime.Caption = ResourceLoader.GetString2("$Message.Crear");
            barButtonItemDeleteExtraTime.Caption = ResourceLoader.GetString2("$Message.Delete");
            barButtonItemModifyExtraTime.Caption = ResourceLoader.GetString2("Modify");
            barButtonItemCreateVacations.Caption = ResourceLoader.GetString2("$Message.Crear");
            barButtonItemDeleteVacations.Caption = ResourceLoader.GetString2("$Message.Delete");
            barButtonItemModifyVacations.Caption = ResourceLoader.GetString2("Modify");

            #region Ribbon bar ToolTips
            this.barButtonItemCreateExtraTime.SuperTip = SupervisionForm.BuildToolTip("ToolTip_CreateWorkShift");
            this.barButtonItemDeleteExtraTime.SuperTip = SupervisionForm.BuildToolTip("ToolTip_DeleteWorkShift");
            this.barButtonItemModifyExtraTime.SuperTip = SupervisionForm.BuildToolTip("ToolTip_ModifyWorkShift");
            this.barButtonItemConsultExtraTime.SuperTip = SupervisionForm.BuildToolTip("ToolTip_RequestWorkShift");

            this.barButtonItemCreateVacations.SuperTip = SupervisionForm.BuildToolTip("ToolTip_CreateVacations");
            this.barButtonItemDeleteVacations.SuperTip = SupervisionForm.BuildToolTip("ToolTip_DeleteVacations");
            this.barButtonItemModifyVacations.SuperTip = SupervisionForm.BuildToolTip("ToolTip_ModifyVacations");
            this.barButtonItemConsultVacations.SuperTip = SupervisionForm.BuildToolTip("ToolTip_ConsultVacations");

            #endregion
        }

        private SuperToolTip GetSuperToolTip(string tooltiptext, string supertooltipcontents)
        {
            ToolTipTitleItem tttitem = new ToolTipTitleItem();
            ToolTipItem ttitem = new ToolTipItem();
            tttitem.Text = ResourceLoader.GetString2(tooltiptext);
            ttitem.LeftIndent = 6;
            ttitem.Text = ResourceLoader.GetString2(supertooltipcontents);
            SuperToolTip supertt = new SuperToolTip();
            supertt.Items.Add(tttitem);
            supertt.Items.Add(ttitem);
            return supertt;
        }

        private void ReestablishAllAppointments()
        {
            CreateAppointmentAllOperator();
        }


        private void UnSelectAllOperators() 
        {
            gridViewOperators.BeginDataUpdate();
            for (int i = 0; i < gridViewOperators.DataRowCount; i++)
            {
                OperatorConsultExtraVacationsGridData row = gridViewOperators.GetRow(i) as OperatorConsultExtraVacationsGridData;
                if (row != null)
                {
                    row.Visible = false;
                }
            }
            gridViewOperators.EndDataUpdate();
        }

        private void DeleteAllAppointmets() 
        {
            UnSelectAllOperators();
            this.schedulerStorage1.BeginUpdate();
            for (int i = this.schedulerStorage1.Appointments.Count-1; i >=0; i--)
            {
                Appointment app = this.schedulerStorage1.Appointments[i] as Appointment;
                app.Delete();
            }
            this.schedulerStorage1.EndUpdate();    
        }

        private void SetFilterNoSearch()
        {
            switch (filter)
            {
                case FilterActive.OperatorFilter:
                    filter = FilterActive.None;
                    gridViewOperators.ClearColumnsFilter();
                    break;
                default:
                    break;
            }
        }

        private void SetFilterNoWorkshift()
        {
            switch (filter)
            {
                case FilterActive.WorkShiftFilter:
                    filter = FilterActive.None;
                    gridViewOperators.ClearColumnsFilter();
                    break;
                default:
                    break;
            }
        }


        private void gridViewOperators_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            
        }

        private void gridViewOperators_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.RowHandle >= 0) 
            {
                bool value = (bool)e.Value;
                OperatorConsultExtraVacationsGridData gridData = (OperatorConsultExtraVacationsGridData)gridViewOperators.GetFocusedRow();
                if (operatorToWorkShifVariations[gridData.Operator.Code].Count <= 0)
                {
                    gridViewOperators.BeginDataUpdate();
                    gridData.Visible = false;
                    gridViewOperators.EndDataUpdate();
                }
                if (value)
                {
                    operatorsSelected++;
                    List<int> listOperatorWorkShiftVariations = operatorToWorkShifVariations[gridData.Operator.Code];
                    for (int i = 0; i < listOperatorWorkShiftVariations.Count; i++)
                    {
                        WorkShifVariationsCounts[listOperatorWorkShiftVariations[i]]++;
                    }
                }
                else
                {
                    operatorsSelected--;
                    List<int> listOperatorWorkShiftVariations = operatorToWorkShifVariations[gridData.Operator.Code];
                    for (int i = 0; i < listOperatorWorkShiftVariations.Count; i++)
                    {
                        WorkShifVariationsCounts[listOperatorWorkShiftVariations[i]]--;
                    }
                    // RemoveAppointment(gridData.Operator.Code);
                }
                this.checkEdit1.CheckStateChanged -= new EventHandler(checkEdit1_CheckStateChanged);
                if (operatorsSelected == dataSourceOperators.Count)
                {
                    this.checkEdit1.CheckState = CheckState.Checked;
                }
                else if (operatorsSelected <= 0)
                {
                    this.checkEdit1.CheckState = CheckState.Unchecked;
                }
                else
                {
                    this.checkEdit1.CheckState = CheckState.Indeterminate;
                }

                this.checkEdit1.CheckStateChanged += new EventHandler(checkEdit1_CheckStateChanged);
                schedulerControlDefaultView.ActiveView.LayoutChanged();
                this.dateNavigator1.ResumeLayout();
            }
        }

        private void barCheckItem1_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }

        private void seeAllButton_Click(object sender, EventArgs e)
        {
            gridViewOperators.ClearColumnsFilter();
            filter = FilterActive.None;
           // this.comboBoxWorkShift.SelectedIndex = -1;
        }


        private void layoutControl1_GroupExpandChanged(object sender, DevExpress.XtraLayout.Utils.LayoutGroupEventArgs e)
        {
            
        }

        private void schedulerControlDefaultView_EditAppointmentFormShowing(object sender, AppointmentFormEventArgs e)
        {
            e.DialogResult = DialogResult.None;
            AppointmentFormEx form = new AppointmentFormEx(e.Appointment, mapWorkShifVariations[(int)e.Appointment.CustomFields["WorkShiftVariationCode"]]);
            form.ShowDialog();
            e.DialogResult = form.DialogResult;
            e.Handled = true;
        }

        private void schedulerControlDefaultView_PreparePopupMenu(object sender, PreparePopupMenuEventArgs e)
        {
            e.Menu = null;
        }

        private void schedulerStorage1_FilterAppointment(object sender, PersistentObjectCancelEventArgs e)
        {
            try
            {
                if (filter == FilterActive.None)
                {
                    e.Cancel = true;
                }
                else if (filter == FilterActive.WorkShiftFilter)
                {
                    WorkShiftVariationClientData var = comboBoxWorkShift.SelectedItem as WorkShiftVariationClientData;
                    int code = ((Appointment)e.Object).CustomFields["WorkShiftVariationCode"] != null ? (int)((Appointment)e.Object).CustomFields["WorkShiftVariationCode"] : 0;
                    if (var.Code == -1)
                    {
                        e.Cancel = code <= 0;
                    }
                    else
                    {
                        e.Cancel = var.Code != code;
                    }
                }
                else if (filter == FilterActive.OperatorFilter)
                {
                    int code = (int)((Appointment)e.Object).CustomFields["WorkShiftVariationCode"];
                    e.Cancel = WorkShifVariationsCounts[code] == 0;
                }
            }
            catch { }
        }

        private void checkEdit1_CheckStateChanged(object sender, EventArgs e)
        {
            bool val = false;
            if (this.checkEdit1.EditValue != null)
            {
                val = (bool)this.checkEdit1.EditValue;
                operatorsSelected = 0;
            }
            else
            {
                this.checkEdit1.CheckStateChanged -= new EventHandler(checkEdit1_CheckStateChanged);
                this.checkEdit1.CheckState = CheckState.Unchecked;
                this.checkEdit1.CheckStateChanged += new EventHandler(checkEdit1_CheckStateChanged);
            }
            
            
            gridViewOperators.BeginUpdate();


            foreach (OperatorConsultExtraVacationsGridData gridData in gridControlOperators.Items)
            {
                List<int> listOperatorWorkShiftVariations = operatorToWorkShifVariations[gridData.Operator.Code];
                
                if (val)
                {
                    operatorsSelected++;
                    for (int i = 0; i < listOperatorWorkShiftVariations.Count; i++)
                    {
                        gridData.Visible = val;
                        WorkShifVariationsCounts[listOperatorWorkShiftVariations[i]]++;
                    }
                }
                else
                {

                    foreach (int key in mapWorkShifVariations.Keys)
                    {
                        WorkShifVariationsCounts[key] = 0;
                    }
                    operatorsSelected--;
                    gridData.Visible = val;
                }
                

            }
            gridViewOperators.EndUpdate();
            schedulerControlDefaultView.ActiveView.LayoutChanged();
        }

        private void comboBoxWorkShift_EditValueChanged(object sender, EventArgs e)
        {
            if (this.comboBoxWorkShift.SelectedIndex != -1)
            {
                WorkShiftVariationClientData var = this.comboBoxWorkShift.SelectedItem as WorkShiftVariationClientData;
                schedulerControlDefaultView.ActiveView.LayoutChanged();
            }
            else
            {
                this.comboBoxWorkShift.SelectedItem = all;
            }
        }

        private void radioGroupFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.radioGroupFilter.SelectedIndex > 0)
            {
                EnableVisiblePanel(false, true);
                filter = FilterActive.OperatorFilter;
                schedulerControlDefaultView.ActiveView.LayoutChanged();
            }
            else
            {
                EnableVisiblePanel(true, false);
                filter = FilterActive.WorkShiftFilter;
                schedulerControlDefaultView.ActiveView.LayoutChanged();
            }
        }

        private void EnableVisiblePanel(bool group1, bool group2)
        {
            if (group1 == true)
            {
                layoutControlGroupCombo.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                this.comboBoxWorkShift.Enabled = true;
            }
            else 
            {
                layoutControlGroupCombo.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                this.comboBoxWorkShift.Enabled = false;
            }


            if (group2 == true)
            {
                this.checkEdit1.Enabled = true;
                this.gridControlOperators.Enabled = true;
                layoutControlItemDataGrid.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            }
            else 
            {
                this.checkEdit1.Enabled = false;
                this.gridControlOperators.Enabled = false;
                layoutControlItemDataGrid.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                
            }
            
        }

        private void radioGroupFilter_Properties_MouseWheel(object sender, MouseEventArgs e)
        {
            ((DevExpress.Utils.DXMouseEventArgs)e).Handled = true;
        }

        private void schedulerControlDefaultView_InitAppointmentDisplayText(object sender, AppointmentDisplayTextEventArgs e)
        {
            e.Text = e.Appointment.Subject;
        }

        private void barButtonItemConsultExtraTime_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (this.MdiParent != null)
            {
                (this.MdiParent as SupervisionForm).barButtonItemConsultExtraTime_ItemClick(null, null);
            }
        }

        private void barButtonItemConsultVacations_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (this.MdiParent != null)
            {
                (this.MdiParent as SupervisionForm).barButtonItemConsultVacations_ItemClick(null, null);
            }
        }

       

        void ConsultExtraTimeVacationsForms_SupervisionCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    if (e.Objects[0] is WorkShiftVariationClientData)
                    {
                        #region WorkShiftVariationClientData
                        WorkShiftVariationClientData ws = e.Objects[0] as WorkShiftVariationClientData;

                        switch (e.Action)
                        {
                            case CommittedDataAction.Save:
                                break;
                            case CommittedDataAction.Update:
                                FormUtil.InvokeRequired(this, delegate
                                {
                                    if (mapWorkShifVariations.ContainsKey(ws.Code) == true)
                                    {
                                        if (ws.Operators.Count == 0)
                                        {
                                            //Remove all workshift Appointments
                                            for (int i = 0; i < schedulerControlCustomView.Storage.Appointments.Count; i++)
                                            {
                                                Appointment app = schedulerControlCustomView.Storage.Appointments[i];
                                                if (app.CustomFields["WorkShiftVariationCode"] != null && ((int)app.CustomFields["WorkShiftVariationCode"]) == ws.Code)
                                                {
                                                    schedulerControlCustomView.Storage.BeginUpdate();
                                                    schedulerControlCustomView.Storage.Appointments.Remove(app);
                                                    schedulerControlCustomView.Storage.EndUpdate();

                                                }
                                            }
                                            //Delete operators that are not in workshift
                                            for (int i = 0; i < mapWorkShifVariations[ws.Code].Count; i++)
                                            {
                                                OperatorClientData ope = mapWorkShifVariations[ws.Code][i];
                                                if (mapWorkShifVariations[ws.Code].Contains(ope) == true)
                                                    mapWorkShifVariations[ws.Code].Remove(ope);

                                                operatorToWorkShifVariations[ope.Code].Remove(ws.Code);
                                            }
                                            schedulerControlCustomView.RefreshData();
                                        }
                                        else
                                        {
                                            //Add new operators in the workshift
                                            foreach (OperatorClientData ope in ws.Operators)
                                            {
                                                if (mapWorkShifVariations[ws.Code].Contains(ope) == false)
                                                    mapWorkShifVariations[ws.Code].Add(ope);

                                                if (operatorToWorkShifVariations.ContainsKey(ope.Code) == false)
                                                    operatorToWorkShifVariations.Add(ope.Code, new List<int>() { ws.Code });
                                                else
                                                    operatorToWorkShifVariations[ope.Code].Add(ws.Code);
                                            }

                                            //Delete operators that are not in workshift
                                            for (int i = 0; i < mapWorkShifVariations[ws.Code].Count; i++)
                                            {
                                                OperatorClientData ope = mapWorkShifVariations[ws.Code][i];
                                                if (ws.Operators.Contains(ope) == false)
                                                {
                                                    if (mapWorkShifVariations[ws.Code].Contains(ope) == true)
                                                        mapWorkShifVariations[ws.Code].Remove(ope);

                                                    operatorToWorkShifVariations[ope.Code].Remove(ws.Code);
                                                }
                                            }

                                            //Add or Update Appointments 
                                            foreach (WorkShiftScheduleVariationClientData schedule in ws.Schedules)
                                            {
                                                bool exist = false;
                                                for (int i = 0; i < schedulerControlCustomView.Storage.Appointments.Count; i++)
                                                {
                                                    Appointment app = schedulerControlCustomView.Storage.Appointments[i];
                                                    if (app.CustomFields["WorkShiftVariationCode"] != null && ((int)app.CustomFields["WorkShiftVariationScheduleCode"]) == schedule.Code)
                                                    {
                                                        //Update existing Appointment 
                                                        schedulerControlCustomView.Storage.BeginUpdate();
                                                        app.Start = schedule.Start;
                                                        app.End = schedule.End;
                                                        app.Subject = ws.Name;
                                                        app.Description = ws.Description;
                                                        schedulerControlCustomView.Storage.EndUpdate();
                                                        exist = true;
                                                        break;
                                                    }
                                                }
                                                if (exist == false)
                                                {
                                                    //Add new Appointment 
                                                    Appointment app = this.schedulerStorage1.CreateAppointment(AppointmentType.Normal);
                                                    app.Start = schedule.Start;
                                                    app.End = schedule.End;
                                                    app.Subject = ws.Name;
                                                    app.Description = ws.Description;
                                                    if (ws.Type == WorkShiftVariationClientData.WorkShiftType.ExtraTime)
                                                    {
                                                        app.CustomFields["AType"] = ResourceLoader.GetString2("ExtraTime");
                                                        app.LabelId = 1;
                                                    }
                                                    else if (ws.Type == WorkShiftVariationClientData.WorkShiftType.TimeOff)
                                                    {
                                                        app.CustomFields["AType"] = ResourceLoader.GetString2("FreeTimeFormText");
                                                        app.LabelId = 2;
                                                    }
                                                    else if (ws.Type == WorkShiftVariationClientData.WorkShiftType.WorkShift)
                                                    {
                                                        app.CustomFields["AType"] = ResourceLoader.GetString2("TypeWorkShift");
                                                        app.LabelId = 0;
                                                    }
                                                    app.CustomFields["WorkShiftVariationCode"] = ws.Code;
                                                    app.CustomFields["WorkShiftVariationScheduleCode"] = schedule.Code;
                                                    app.CustomFields["Color"] = schedulerControlCustomView.Storage.Appointments.Labels[app.LabelId].Color;

                                                    schedulerControlCustomView.Storage.BeginUpdate();
                                                    schedulerControlCustomView.Storage.Appointments.Add(app);
                                                    schedulerControlCustomView.Storage.EndUpdate();
                                                }
                                            }
                                            //Remove no longer aplicable Appointments
                                            for (int i = 0; i < schedulerControlCustomView.Storage.Appointments.Count; i++)
                                            {
                                                Appointment app = schedulerControlCustomView.Storage.Appointments[i];
                                                if (app.CustomFields["WorkShiftVariationCode"] != null && ((int)app.CustomFields["WorkShiftVariationCode"]) == ws.Code)
                                                {
                                                    WorkShiftScheduleVariationClientData schedule = new WorkShiftScheduleVariationClientData();
                                                    schedule.Code = ((int)app.CustomFields["WorkShiftVariationScheduleCode"]);
                                                    if (ws.Schedules.Contains(schedule) == false)
                                                    {
                                                        schedulerControlCustomView.Storage.BeginUpdate();
                                                        schedulerControlCustomView.Storage.Appointments.Remove(app);
                                                        schedulerControlCustomView.Storage.EndUpdate();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (((ws.Type != WorkShiftVariationClientData.WorkShiftType.TimeOff) && (mode == ModeConsult.ExtraTime)) ||
                                             ((ws.Type == WorkShiftVariationClientData.WorkShiftType.TimeOff) && (mode == ModeConsult.VacationsPermisions)))
                                        {
                                            List<Appointment> listapp = new List<Appointment>();

                                            WorkShifVariationsCounts.Add(ws.Code, 0);
                                            mapWorkShifVariations.Add(ws.Code, new List<OperatorClientData>(ws.Operators.Cast<OperatorClientData>()));
                                            foreach (OperatorClientData ope in ws.Operators)
                                            {
                                                listapp.AddRange(GetListAppointments(ws, ope.FirstName, ope.LastName, ope.Code));
                                                
                                                if (operatorToWorkShifVariations.ContainsKey(ope.Code) == false)
                                                    operatorToWorkShifVariations.Add(ope.Code,new List<int>(){ws.Code});
                                                else
                                                    operatorToWorkShifVariations[ope.Code].Add(ws.Code);
                                            }

                                            schedulerControlCustomView.Storage.BeginUpdate();
                                            schedulerControlCustomView.Storage.Appointments.AddRange(listapp.ToArray());
                                            schedulerControlCustomView.Storage.EndUpdate();
                                        }
                                    }
                                });
                                break;
                            case CommittedDataAction.Delete:
                                break;
                            default:
                                break;
                        }
                        #endregion
                    }
                    else if (e.Objects[0] is OperatorClientData)
                    {
                        #region OperatorClientData
                        OperatorClientData oper = e.Objects[0] as OperatorClientData;
                        if (e.Action == CommittedDataAction.Update)
                        {
                            //Update operators in the workshift
                            foreach (WorkShiftVariationClientData ws in oper.WorkShifts)
                            {
                                if (mapWorkShifVariations.ContainsKey(ws.Code) == true)
                                {
                                    mapWorkShifVariations[ws.Code][mapWorkShifVariations[ws.Code].IndexOf(oper)] = oper;
                                    break;
                                }
                            }
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }


    }
}
