using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Smartmatic.SmartCad.Service;
using System.Collections;
using System.Threading;
using System.Reflection;
using SmartCadCore.Common;
using SmartCadCore.ClientData;
using SmartCadCore.Core;
using SmartCadGuiCommon.Controls;

namespace SmartCadGuiCommon
{
   


    public partial class IndicatorsForm : DevExpress.XtraEditors.XtraForm
    {
        public event IndicatorOperatorFormChangeEventHandler IndicatorOperatorFormChangedEvent;     
        public IndicatorsForm()
        {
            InitializeComponent();
            LoadLanguage();
            
        }       
        private void LoadLanguage()
        {
            this.Text = ResourceLoader.GetString2("PerformanceIndicators");
            this.layoutControlGroupSystem.Text = ResourceLoader.GetString2("System");
            this.layoutControlGroupMyGroup.Text = ResourceLoader.GetString2("Group");
            this.layoutControlGroupDepartment.Text = ResourceLoader.GetString2("Department");
            this.indicatorControlSystem.chartControl1.Series[1].LegendText = ResourceLoader.GetString2("Trend");
        }
        public void SetGeneralDispatchSupervisorMode()
        {
            this.indicatorControlSystem.indicatorGridControl1.Clear();
            LoadIndicators();
            ShowTab(this.layoutControlGroupDepartment);
            ShowTab(this.layoutControlGroupSystem);
            HideTab(this.layoutControlGroupMyGroup);
            if ((this.layoutControlGroupDepartment != null) && (this.layoutControlGroupDepartment.Text != (this.MdiParent as SupervisionForm).SelectedDepartment.Name))
            {
                SetDepartmentTapPageName();
                this.indicatorControlDepartment.chartControl1.Series[0].Points.Clear();
                this.indicatorControlDepartment.chartControl1.Series[1].Points.Clear();
                this.indicatorControlDepartment.indicatorGridControl1.DataSource = new BindingList<IndicatorGridControlData>();//List<Smartmatic.SmartCad.Gui.Controls.IndicatorGridControlData>();
            }
            
           
        }
        public void SetGeneralFirtsLevelSupervisorMode()
        {
            this.indicatorControlSystem.indicatorGridControl1.Clear();
            LoadIndicators();
            HideTab(layoutControlGroupDepartment);
            ShowTab(layoutControlGroupSystem);
            HideTab(layoutControlGroupMyGroup);         
        }
        private void SetSupervisorMode()
        {
            if ((this.MdiParent as SupervisionForm).IsGeneralSupervisor)
            {
                if (((this.MdiParent as SupervisionForm).SupervisedApplicationName ==
                    UserApplicationClientData.Dispatch.Name))
                {
                    SetGeneralDispatchSupervisorMode();
                }
                else
                {
                    SetGeneralFirtsLevelSupervisorMode();
                }
            }
            else if ((this.MdiParent as SupervisionForm).IsFirstLevelSupervisor)
            {
                SetFirstLevelSupervisorMode();
            }
            else if ((this.MdiParent as SupervisionForm).IsDispatchSupervisor)
            {
                SetDispatchSupervisorMode();
            }
        }
        private void SetFirstLevelSupervisorMode()
        {
            HideTab(layoutControlGroupDepartment);
            ShowTab(layoutControlGroupSystem);
            ShowTab(layoutControlGroupMyGroup);               
        }
        private void SetDispatchSupervisorMode()
        {
            ShowTab(layoutControlGroupDepartment);
            ShowTab(layoutControlGroupSystem);
            ShowTab(layoutControlGroupMyGroup);                  
        }
        private void ShowTab(DevExpress.XtraLayout.LayoutControlGroup tabpage)
        {
            if (this.tabbedControlGroupIndicators.TabPages.IndexOf(tabpage) == -1)
            {
                if (tabpage.Name == "layoutControlGroupSystem")
                {
                    tabbedControlGroupIndicators.InsertTabPage(0, tabpage);
                }
                else if (tabpage.Name == "layoutControlGroupDepartment")
                {
                    tabbedControlGroupIndicators.InsertTabPage(1, tabpage);
                }
                else if (tabpage.Name == "layoutControlGroupMyGroup")
                {
                    tabbedControlGroupIndicators.InsertTabPage(2, tabpage);
                }
            }
        }       
        private void HideTab(DevExpress.XtraLayout.LayoutControlGroup tabpage)
        {
            if (this.tabbedControlGroupIndicators.TabPages.IndexOf(tabpage) != -1)
            {
                tabbedControlGroupIndicators.RemoveTabPage(tabpage);
            }
        }
        private void SetDepartmentTapPageName()
        {
            this.layoutControlGroupDepartment.Text = (this.MdiParent as SupervisionForm).SelectedDepartment.Name;
            this.layoutControlGroupDepartment.Tag = (this.MdiParent as SupervisionForm).SelectedDepartment;
        }  
      
        private void IndicatorsForm_Load(object sender, EventArgs e)
        {
            this.Activated += new EventHandler(IndicatorsForm_Activated);
            SetSupervisorMode();
            this.indicatorControlGroup.indicatorGridControl1.IndicatorOperatorFormChangedEvent
                += new IndicatorOperatorFormChangeEventHandler(
                    indicatorGridControl1_IndicatorOperatorFormChangedEvent);
            this.indicatorControlSystem.indicatorGridControl1.IndicatorOperatorFormChangedEvent
                += new IndicatorOperatorFormChangeEventHandler(
                    indicatorGridControl1_IndicatorOperatorFormChangedEvent);
            this.indicatorControlDepartment.indicatorGridControl1.IndicatorOperatorFormChangedEvent
                += new IndicatorOperatorFormChangeEventHandler(
                    indicatorGridControl1_IndicatorOperatorFormChangedEvent);
            (this.MdiParent as SupervisionForm).SupervisionCommittedChanges +=
                new EventHandler<CommittedObjectDataCollectionEventArgs>(
                    IndicatorsForm_SupervisionCommittedChanges);

            if ((this.MdiParent as SupervisionForm).SupervisedApplicationName ==
                       UserApplicationClientData.FirstLevel.Name)
            {
                ShowPanelControl(true);          
            }
            else
            {
                ShowPanelControl(false);
                if ((this.MdiParent as SupervisionForm).SelectedDepartment != null)
                {
                    FormUtil.InvokeRequired(this, delegate
                    {
                        SetDepartmentTapPageName();
                    });
                }       
            }
            LoadIndicators();
        }


        private void ShowPanelControl(bool showFirstLevel) 
        {
            if (showFirstLevel == true)
            {
                layoutControlItemIndicatorGroupDisaptchPanelControl1.HideToCustomization();
                layoutControlItemIndicatorGroupFirstLevelPanelControl1.RestoreFromCustomization(layoutControlItemindicatorControlMyGroup, DevExpress.XtraLayout.Utils.InsertType.Bottom);
            }else
            {
            
                layoutControlItemIndicatorGroupFirstLevelPanelControl1.HideToCustomization();
                layoutControlItemIndicatorGroupDisaptchPanelControl1.RestoreFromCustomization(layoutControlItemindicatorControlMyGroup, DevExpress.XtraLayout.Utils.InsertType.Bottom);
            }
        
        }


        void IndicatorsForm_Activated(object sender, EventArgs e)
        {
            if (this.MdiParent != null)
                (this.MdiParent as SupervisionForm).RibbonControl.SelectedPage = (this.MdiParent as SupervisionForm).RibbonPageMonitoring;

        }
        #region LoadIndicators
        private void LoadIndicators()
        {
            //This method is for loading the indicators when the form open. Right now this functionality if not used.
            //return;

            IList systemIndicator = new ArrayList();
            IList systemIncindetOpenIndicators = new ArrayList();
            IList systemDispatchOrder = new ArrayList();
            IList systemOperators = new ArrayList();
            IList systemEmergency = new ArrayList();

            IList departmentIndicator = new ArrayList();

            IList groupIndicator = new ArrayList();
            IList groupOperators = new ArrayList();
            IList groupIncidents = new ArrayList();
            IList groupEmergency = new ArrayList();
            IList groupDispatchOrderImportantStatus = new ArrayList();
            IList groupSpeedCalls = new ArrayList();
            IList departmentOperators = new ArrayList();
            IList departmentIncidents = new ArrayList();
            IList departmentDispatchOrderImportantStatus = new ArrayList();

            IList indicators = ServerServiceClient.GetInstance().SearchClientObjects(
                    SmartCadHqls.GetAllLastResultValues);
          
            
            if ((this.MdiParent as SupervisionForm).SelectedDepartment != null)
            {
                FormUtil.InvokeRequired(this, delegate
                {
                    SetDepartmentTapPageName();
                });
            }            
            foreach (IndicatorResultClientData indicator in indicators)
            {
                foreach (IndicatorResultValuesClientData irvcd in indicator.Values)
                {
                    if ((this.MdiParent as SupervisionForm).SupervisedApplicationName == 
                        UserApplicationClientData.FirstLevel.Name) 
                    {
                        LoadFirtsLevelIndicators(systemIndicator, systemIncindetOpenIndicators,
                                systemDispatchOrder, systemOperators, systemEmergency, departmentIndicator,
                                groupIndicator, groupOperators, groupEmergency, indicator, irvcd, groupSpeedCalls);
                    }                    
                    else if ((this.MdiParent as SupervisionForm).SupervisedApplicationName == 
                        UserApplicationClientData.Dispatch.Name)
                    {
                        LoadDispatchIndicators(systemIndicator, systemIncindetOpenIndicators,
                                systemDispatchOrder, systemOperators, systemEmergency, departmentIndicator,
                                groupIndicator, groupOperators, groupEmergency, indicator, irvcd, groupIncidents, 
                                groupDispatchOrderImportantStatus, departmentOperators, departmentIncidents, departmentDispatchOrderImportantStatus);
                    }
                }
            }
            #region System
            this.indicatorSystemPanelControl1.incidentIndicatorControl2.incidentGridIndicatorControl1.Load(systemIncindetOpenIndicators);
            this.indicatorSystemPanelControl1.dispatchOrderGridIndicatorControl1.Load(systemDispatchOrder);            
            this.indicatorSystemPanelControl1.operatorIndicatorControl21.operatorGridIndicatorControl2.Load(systemOperators);
            this.indicatorSystemPanelControl1.emergencyIndicatorControl1.emergencyGridIndicatorControl1.Load(systemEmergency);
            this.indicatorControlSystem.indicatorGridControl1.Load(systemIndicator);
            #endregion system

            #region  Group And Department
            this.indicatorControlGroup.indicatorGridControl1.Load(groupIndicator);
            
            if ((this.MdiParent as SupervisionForm).SupervisedApplicationName ==
                       UserApplicationClientData.FirstLevel.Name)
            {
                ShowPanelControl(true);
                this.indicatorGroupFirstLevelPanelControl1.operatorIndicatorControl21.operatorGridIndicatorControl2.Load(groupOperators);
                this.indicatorGroupFirstLevelPanelControl1.emergencyIndicatorControl1.emergencyGridIndicatorControl1.Load(groupEmergency);
                this.indicatorGroupFirstLevelPanelControl1.speedRequestIndicatorChartControl1.Load();
            }
            else
            {
                ShowPanelControl(false);
                this.indicatorGroupDisaptchPanelControl1.operatorIndicatorControl21.operatorGridIndicatorControl2.Load(groupOperators);
                this.indicatorGroupDisaptchPanelControl1.incidentIndicatorControl2.incidentGridIndicatorControl1.Load(groupIncidents);
                this.indicatorGroupDisaptchPanelControl1.dispatchOrderImprtantStatusIndicatorControl1.dispatchOrderImprtantStatusGridIndicatorControl1.Load(groupDispatchOrderImportantStatus);
                this.indicatorControlDepartment.indicatorGridControl1.Load(departmentIndicator);

                this.indicatorDepartmentPanelControl1.operatorIndicatorControl21.operatorGridIndicatorControl2.Load(departmentOperators);
                this.indicatorDepartmentPanelControl1.incidentIndicatorControl1.incidentGridIndicatorControl1.Load(departmentIncidents);
                this.indicatorDepartmentPanelControl1.dispatchOrderImprtantStatusIndicatorControl1.dispatchOrderImprtantStatusGridIndicatorControl1.Load(departmentDispatchOrderImportantStatus);
            }

            #endregion Group And Department

            Application.DoEvents();
        }        
        private void LoadFirtsLevelIndicators(IList systemIndicator, IList systemIncindetOpenIndicators,
            IList systemDispatchOrder, IList systemOperators, IList systemEmergency, IList departmentIndicator,
            IList groupIndicator, IList groupOperators, IList groupEmergency, IndicatorResultClientData indicator,
            IndicatorResultValuesClientData irvcd, IList groupSpeedCalls)
        {
            #region system
            if (irvcd.IndicatorClassName == IndicatorClassClientData.System.Name)
            {
                if ((indicator.IndicatorCustomCode == "PND15") ||
                    (indicator.IndicatorCustomCode == "PND16"))
                {
                    systemIncindetOpenIndicators.Add(irvcd);
                }
                else if (indicator.IndicatorCustomCode == "PND17")
                {
                    systemDispatchOrder.Add(irvcd);
                }
                else if ((indicator.IndicatorCustomCode == "PN04") ||
                    (indicator.IndicatorCustomCode == "PN05"))
                {
                    systemOperators.Add(irvcd);
                }
                else if ((indicator.IndicatorCustomCode == "PND22") ||
                    (indicator.IndicatorCustomCode == "PND23"))
                {
                    systemEmergency.Add(irvcd);
                }
                else
                {
                    if ((indicator.IndicatorCustomCode != "DPN15") &&
                    (indicator.IndicatorCustomCode != "DPN16") &&
                    (indicator.IndicatorCustomCode != "DPN22") &&
                    (indicator.IndicatorCustomCode != "DPN23"))
                        systemIndicator.Add(irvcd);
                }
            }
            #endregion system

            #region group

            if ((irvcd.IndicatorClassName == IndicatorClassClientData.Group.Name) &&
           ((irvcd.IndicatorTypeName == "FIRSTLEVEL") ||
           (irvcd.IndicatorTypeName == "CALLS")))
            {
                if (irvcd.CustomCode == ServerServiceClient.GetInstance().OperatorClient.Code)
                {

                    if ((indicator.IndicatorCustomCode == "PN04") ||
                   (indicator.IndicatorCustomCode == "PN05"))
                    {
                        groupOperators.Add(irvcd);
                    }
                    else if ((indicator.IndicatorCustomCode == "PND22") ||
                   (indicator.IndicatorCustomCode == "PND23"))
                    {
                        groupEmergency.Add(irvcd);
                    }
                    else if (indicator.IndicatorCustomCode == "PN19")
                    {
                        groupSpeedCalls.Add(irvcd);
                    }
                    else
                    {
                        groupIndicator.Add(irvcd);
                    }
                }
            }
            #endregion group
        }
        private void LoadDispatchIndicators(IList systemIndicator, IList systemIncindetOpenIndicators,
            IList systemDispatchOrder, IList systemOperators, IList systemEmergency, IList departmentIndicator,
            IList groupIndicator, IList groupOperators, IList groupEmergency, IndicatorResultClientData indicator,
            IndicatorResultValuesClientData irvcd, IList groupIncidents, IList groupDispatchOrderImportantStatus,
            IList departmentOperators, IList departmentIncidents, IList departmentDispatchOrderImportantStatus)
        {
            #region system
            if (irvcd.IndicatorClassName == IndicatorClassClientData.System.Name)
            {
                if ((indicator.IndicatorCustomCode == "DPN15") ||
                    (indicator.IndicatorCustomCode == "DPN16"))
                {
                    systemIncindetOpenIndicators.Add(irvcd);
                }

                else if (indicator.IndicatorCustomCode == "PND17")
                {
                    systemDispatchOrder.Add(irvcd);
                }

                else if ((indicator.IndicatorCustomCode == "D22") ||
                    (indicator.IndicatorCustomCode == "D23"))
                {
                    systemOperators.Add(irvcd);
                }
                else if ((indicator.IndicatorCustomCode == "DPN22") ||
                    (indicator.IndicatorCustomCode == "DPN23"))
                {

                    systemEmergency.Add(irvcd);
                }
                else
                {
                    if ((indicator.IndicatorCustomCode != "PND15") &&
                    (indicator.IndicatorCustomCode != "PND16") &&
                    (indicator.IndicatorCustomCode != "PND17") &&
                    (indicator.IndicatorCustomCode != "PND22") &&
                    (indicator.IndicatorCustomCode != "PND23"))
                        systemIndicator.Add(irvcd);
                }
            }
            #endregion system

            #region department
            else if ((this.MdiParent as SupervisionForm).SupervisedApplicationName == UserApplicationClientData.Dispatch.Name
                && irvcd.IndicatorClassName == IndicatorClassClientData.Department.Name
                && irvcd.IndicatorTypeName == "DISPATCH")
            {

                if (irvcd.CustomCode == (this.MdiParent as SupervisionForm).SelectedDepartment.Code)
                {

                    if ((indicator.IndicatorCustomCode == "D22") ||
                 (indicator.IndicatorCustomCode == "D23"))
                    {
                        departmentOperators.Add(irvcd);
                    }

                    else if ((indicator.IndicatorCustomCode == "DPN15") ||
                 (indicator.IndicatorCustomCode == "DPN16"))
                    {
                        departmentIncidents.Add(irvcd);
                    }
                    else if ((indicator.IndicatorCustomCode == "D39") ||
                 (indicator.IndicatorCustomCode == "D40") ||
                        (indicator.IndicatorCustomCode == "D41"))
                    {
                        departmentDispatchOrderImportantStatus.Add(irvcd);
                    }
                    else
                    {
                        departmentIndicator.Add(irvcd);
                    }
                }
            }
            #endregion department

            #region group
            else if ((irvcd.IndicatorClassName == IndicatorClassClientData.Group.Name) &&
                irvcd.IndicatorTypeName == "DISPATCH")
            {
                if (irvcd.CustomCode == ServerServiceClient.GetInstance().OperatorClient.Code)
                {

                    if ((indicator.IndicatorCustomCode == "D22") ||
                 (indicator.IndicatorCustomCode == "D23"))
                    {
                        groupOperators.Add(irvcd);
                    }

                    else if ((indicator.IndicatorCustomCode == "DPN15") ||
                 (indicator.IndicatorCustomCode == "DPN16"))
                    {
                        groupIncidents.Add(irvcd);
                    }
                    else if ((indicator.IndicatorCustomCode == "D39") ||
                 (indicator.IndicatorCustomCode == "D40") ||
                        (indicator.IndicatorCustomCode == "D41"))
                    {
                        groupDispatchOrderImportantStatus.Add(irvcd);
                    }
                    else
                    {
                        groupIndicator.Add(irvcd);
                    }
                }
            }
            #endregion group
        }
        #endregion LoadIndicators
        private void indicatorGridControl1_IndicatorOperatorFormChangedEvent(object sender, 
            IndicatorOperatorFormChangeEventArgs e)
        {
            if (IndicatorOperatorFormChangedEvent != null)
            {
                IndicatorOperatorFormChangedEvent(sender, e);
            }
        }
        
        private void IndicatorsForm_SupervisionCommittedChanges(object sender, 
            CommittedObjectDataCollectionEventArgs e)
        {

            if (e.Objects != null && e.Objects.Count > 0)
            {
                if (e.Objects[0] is IndicatorResultClientData)
                {   
                    foreach(IndicatorResultClientData indicatorResultClientData in e.Objects)
                    {
                    //IndicatorResultClientData indicatorResultClientData = (e.Objects[0] as IndicatorResultClientData);

                        if (indicatorResultClientData.Values.Count > 0)
                        {
                            foreach (IndicatorResultValuesClientData irvcd in indicatorResultClientData.Values)
                            {
                                if ((this.MdiParent as SupervisionForm).SupervisedApplicationName ==
                                    UserApplicationClientData.FirstLevel.Name)
                                {
                                    UpdateFirstLevelIndicators(indicatorResultClientData, irvcd);
                                }
                                else if ((this.MdiParent as SupervisionForm).SupervisedApplicationName ==
                                    UserApplicationClientData.Dispatch.Name)
                                {
                                    UpdateDispatchIndicators(indicatorResultClientData, irvcd);
                                }
                            }
                        }
                    }
                }
                else if (e.Objects[0] is DepartmentTypeClientData)
                {
                    DepartmentTypeClientData updatedDept = e.Objects[0] as DepartmentTypeClientData;
                    FormUtil.InvokeRequired(this,
                        delegate
                        {
                            DepartmentTypeClientData actualDept = layoutControlGroupDepartment.Tag as DepartmentTypeClientData;
                            if (actualDept != null && actualDept.Equals(updatedDept))
                            {
                                layoutControlGroupDepartment.Text = updatedDept.Name;
                                layoutControlGroupDepartment.Tag = updatedDept;
                            }
                        });
                }
            }
        }        
        private void UpdateFirstLevelIndicators(IndicatorResultClientData indicatorResultClientData, 
            IndicatorResultValuesClientData irvcd)
        {
            #region system

            if (irvcd.IndicatorClassName == IndicatorClassClientData.System.Name) 
                
            {
                //if ((indicatorResultClientData.IndicatorCustomCode == "PND15")||
                //    (indicatorResultClientData.IndicatorCustomCode == "PND16"))
                if (indicatorResultClientData.IndicatorCustomCode == "PND24")
                {
                    this.indicatorSystemPanelControl1.incidentIndicatorControl2.incidentGridIndicatorControl1.AddOrUpdate(irvcd);
                }                
                else if (indicatorResultClientData.IndicatorCustomCode == "D11")
                {
                    this.indicatorSystemPanelControl1.dispatchOrderGridIndicatorControl1.AddOrUpdate(irvcd);
                }              
                //else if ((indicatorResultClientData.IndicatorCustomCode == "PN01") ||
                //    (indicatorResultClientData.IndicatorCustomCode == "PN03")||
                //    (indicatorResultClientData.IndicatorCustomCode == "PN04") ||
                //    (indicatorResultClientData.IndicatorCustomCode == "PN05")||
                //    (indicatorResultClientData.IndicatorCustomCode == "PN15")||
                //    (indicatorResultClientData.IndicatorCustomCode == "PN16")||
                //    (indicatorResultClientData.IndicatorCustomCode == "PN17")||
                //    (indicatorResultClientData.IndicatorCustomCode == "PN18"))   
                else if (indicatorResultClientData.IndicatorCustomCode == "PN20")
                {
                    this.indicatorSystemPanelControl1.operatorIndicatorControl21.operatorGridIndicatorControl2.AddOrUpdate(irvcd);                    
                }
                //else if ((indicatorResultClientData.IndicatorCustomCode == "PND22") ||
                //    (indicatorResultClientData.IndicatorCustomCode == "PND23") ||
                //    (indicatorResultClientData.IndicatorCustomCode == "L08") ||
                //    (indicatorResultClientData.IndicatorCustomCode == "L13"))
                else if (indicatorResultClientData.IndicatorCustomCode == "PND25")
                {                    
                    this.indicatorSystemPanelControl1.emergencyIndicatorControl1.emergencyGridIndicatorControl1.AddOrUpdate(irvcd);                    
                }
                else
                {
                    if ((indicatorResultClientData.IndicatorCustomCode != "DPN15") &&
                   (indicatorResultClientData.IndicatorCustomCode != "DPN16") &&                  
                   (indicatorResultClientData.IndicatorCustomCode != "DPN22") &&
                   (indicatorResultClientData.IndicatorCustomCode != "DPN23"))
                        this.indicatorControlSystem.indicatorGridControl1.AddOrUpdate(irvcd);

                }
            }
            #endregion system

      

            #region group
            else if ((irvcd.IndicatorClassName == IndicatorClassClientData.Group.Name) && 
                ((irvcd.IndicatorTypeName == "FIRSTLEVEL") || 
                (irvcd.IndicatorTypeName == "CALLS")))
            {
                if (irvcd.CustomCode == ServerServiceClient.GetInstance().OperatorClient.Code)
                {
                   
                    // if ((indicatorResultClientData.IndicatorCustomCode == "PN01") ||
                    //(indicatorResultClientData.IndicatorCustomCode == "PN03")||
                    //(indicatorResultClientData.IndicatorCustomCode == "PN04") ||
                    //(indicatorResultClientData.IndicatorCustomCode == "PN05")||
                    //(indicatorResultClientData.IndicatorCustomCode == "PN15")||
                    //(indicatorResultClientData.IndicatorCustomCode == "PN16")||
                    //(indicatorResultClientData.IndicatorCustomCode == "PN17")||
                    //(indicatorResultClientData.IndicatorCustomCode == "PN18"))   
                      if(indicatorResultClientData.IndicatorCustomCode == "PN20")
                    {
                        this.indicatorGroupFirstLevelPanelControl1.operatorIndicatorControl21.operatorGridIndicatorControl2.AddOrUpdate(irvcd);                        
                    }
                     //else if ((indicatorResultClientData.IndicatorCustomCode == "PND22") ||
                     //        (indicatorResultClientData.IndicatorCustomCode == "PND23") ||
                     //        (indicatorResultClientData.IndicatorCustomCode == "L08") ||
                     //        (indicatorResultClientData.IndicatorCustomCode == "L13"))
                      else if (indicatorResultClientData.IndicatorCustomCode == "PND25")
                     {
                         this.indicatorGroupFirstLevelPanelControl1.emergencyIndicatorControl1.emergencyGridIndicatorControl1.AddOrUpdate(irvcd);
                     }
                     else if (indicatorResultClientData.IndicatorCustomCode == "PN19")
                     {
                         IList data = new ArrayList();
                         data.Add(irvcd);
                         this.indicatorGroupFirstLevelPanelControl1.speedRequestIndicatorChartControl1.AddOrUpdate(data);
                     }
                     else
                     {
                         this.indicatorControlGroup.indicatorGridControl1.AddOrUpdate(irvcd);
                     }
                }
            }
            #endregion group

        }
        private void UpdateDispatchIndicators(IndicatorResultClientData indicatorResultClientData, 
            IndicatorResultValuesClientData irvcd)
        {
            #region system
            if (irvcd.IndicatorClassName == IndicatorClassClientData.System.Name)
            {
                //if ((indicatorResultClientData.IndicatorCustomCode == "DPN15")||
                //    (indicatorResultClientData.IndicatorCustomCode == "DPN16"))
                if (indicatorResultClientData.IndicatorCustomCode == "PND24")
                {
                    this.indicatorSystemPanelControl1.incidentIndicatorControl2.incidentGridIndicatorControl1.AddOrUpdate(irvcd);
                }
                else if (indicatorResultClientData.IndicatorCustomCode == "D11")
                {
                    this.indicatorSystemPanelControl1.dispatchOrderGridIndicatorControl1.AddOrUpdate(irvcd);
                }
                //else if ((indicatorResultClientData.IndicatorCustomCode == "D20") ||
                //    (indicatorResultClientData.IndicatorCustomCode == "D22") ||
                //    (indicatorResultClientData.IndicatorCustomCode == "D23") ||
                //    (indicatorResultClientData.IndicatorCustomCode == "D34") ||
                //    (indicatorResultClientData.IndicatorCustomCode == "D35") ||
                //    (indicatorResultClientData.IndicatorCustomCode == "D36") ||
                //    (indicatorResultClientData.IndicatorCustomCode == "D37") ||
                //    (indicatorResultClientData.IndicatorCustomCode == "D38"))
                else if (indicatorResultClientData.IndicatorCustomCode == "D51")
                {
                    this.indicatorSystemPanelControl1.operatorIndicatorControl21.operatorGridIndicatorControl2.AddOrUpdate(irvcd);
                }
                //else if ((indicatorResultClientData.IndicatorCustomCode == "DPN22") ||
                //    (indicatorResultClientData.IndicatorCustomCode == "DPN23")||
                //    (indicatorResultClientData.IndicatorCustomCode == "L08")||
                //    (indicatorResultClientData.IndicatorCustomCode == "L13"))
                else if (indicatorResultClientData.IndicatorCustomCode == "PND25")
                {
                    this.indicatorSystemPanelControl1.emergencyIndicatorControl1.emergencyGridIndicatorControl1.AddOrUpdate(irvcd);
                }
                else
                {

                    if ((indicatorResultClientData.IndicatorCustomCode != "PND15") &&
                  (indicatorResultClientData.IndicatorCustomCode != "PND16") &&                  
                  (indicatorResultClientData.IndicatorCustomCode != "PND22") &&
                  (indicatorResultClientData.IndicatorCustomCode != "PND23"))
                    {
                        this.indicatorControlSystem.indicatorGridControl1.AddOrUpdate(irvcd);
                    }
                }
            }
            #endregion system

            #region department
            else if ((this.MdiParent as SupervisionForm).SupervisedApplicationName == UserApplicationClientData.Dispatch.Name &&
                     irvcd.IndicatorClassName == IndicatorClassClientData.Department.Name &&
                (irvcd.IndicatorTypeName == "DISPATCH"))
            {
                if (irvcd.CustomCode == (this.MdiParent as SupervisionForm).SelectedDepartment.Code)
                {

                    //if ((indicatorResultClientData.IndicatorCustomCode == "D22") ||
                    //  (indicatorResultClientData.IndicatorCustomCode == "D23") ||
                    //  (indicatorResultClientData.IndicatorCustomCode == "D20") ||
                    //  (indicatorResultClientData.IndicatorCustomCode == "D34") ||
                    //  (indicatorResultClientData.IndicatorCustomCode == "D35") ||
                    //  (indicatorResultClientData.IndicatorCustomCode == "D36") ||
                    //  (indicatorResultClientData.IndicatorCustomCode == "D37") ||
                    //  (indicatorResultClientData.IndicatorCustomCode == "D38"))
                    if (indicatorResultClientData.IndicatorCustomCode == "D51")
                    {
                        this.indicatorDepartmentPanelControl1.operatorIndicatorControl21.operatorGridIndicatorControl2.AddOrUpdate(irvcd);

                    }
                    //else if ((indicatorResultClientData.IndicatorCustomCode == "DPN15") ||
                    //   (indicatorResultClientData.IndicatorCustomCode == "DPN16"))
                    else if (indicatorResultClientData.IndicatorCustomCode == "DPN24")                       
                    {
                        this.indicatorDepartmentPanelControl1.incidentIndicatorControl1.incidentGridIndicatorControl1.AddOrUpdate(irvcd);
                    }
                    //else if ((indicatorResultClientData.IndicatorCustomCode == "D39") ||
                    //    (indicatorResultClientData.IndicatorCustomCode == "D40") ||
                    //    (indicatorResultClientData.IndicatorCustomCode == "D41")||
                    //    (indicatorResultClientData.IndicatorCustomCode == "D45")||
                    //    (indicatorResultClientData.IndicatorCustomCode == "D46")||
                    //    (indicatorResultClientData.IndicatorCustomCode == "D47")||
                    //    (indicatorResultClientData.IndicatorCustomCode == "D48")||
                    //    (indicatorResultClientData.IndicatorCustomCode == "D49"))
                    else if(indicatorResultClientData.IndicatorCustomCode == "D50")
                    {
                        this.indicatorDepartmentPanelControl1.dispatchOrderImprtantStatusIndicatorControl1.dispatchOrderImprtantStatusGridIndicatorControl1.AddOrUpdate(irvcd);
                    }
                    else if (indicatorResultClientData.IndicatorCustomCode == "D07")                        
                    {
                        this.indicatorDepartmentPanelControl1.newDispatchOrderGridIndicatorControl1.AddOrUpdate(irvcd);
                    }
                    else
                    {
                        this.indicatorControlDepartment.indicatorGridControl1.AddOrUpdate(irvcd);
                    }
                                     
                }
            }
            #endregion department

            #region group
            else if (irvcd.IndicatorClassName == IndicatorClassClientData.Group.Name &&
                irvcd.IndicatorTypeName == "DISPATCH")
            {
                if (irvcd.CustomCode == ServerServiceClient.GetInstance().OperatorClient.Code)
                {
                    
                    //if ((indicatorResultClientData.IndicatorCustomCode == "D22") ||
                    //   (indicatorResultClientData.IndicatorCustomCode == "D23") ||
                    //   (indicatorResultClientData.IndicatorCustomCode == "D20") ||
                    //   (indicatorResultClientData.IndicatorCustomCode == "D34") ||
                    //   (indicatorResultClientData.IndicatorCustomCode == "D35") ||
                    //   (indicatorResultClientData.IndicatorCustomCode == "D36") ||
                    //   (indicatorResultClientData.IndicatorCustomCode == "D37") ||
                    //   (indicatorResultClientData.IndicatorCustomCode == "D38"))
                    if (indicatorResultClientData.IndicatorCustomCode == "D51")
                    {
                        this.indicatorGroupDisaptchPanelControl1.operatorIndicatorControl21.operatorGridIndicatorControl2.AddOrUpdate(irvcd);
                        
                    }
                    //else if ((indicatorResultClientData.IndicatorCustomCode == "DPN17")||
                    //    (indicatorResultClientData.IndicatorCustomCode == "DPN18"))
                    else if (indicatorResultClientData.IndicatorCustomCode == "DPN25")
                    {
                        this.indicatorGroupDisaptchPanelControl1.incidentIndicatorControl2.incidentGridIndicatorControl1.AddOrUpdate(irvcd);
                    }
                    //else if ((indicatorResultClientData.IndicatorCustomCode == "D39") ||
                    //    (indicatorResultClientData.IndicatorCustomCode == "D40") ||
                    //    (indicatorResultClientData.IndicatorCustomCode == "D41") ||
                    //    (indicatorResultClientData.IndicatorCustomCode == "D45") ||                        
                    //    (indicatorResultClientData.IndicatorCustomCode == "D47") ||
                    //    (indicatorResultClientData.IndicatorCustomCode == "D48") ||
                    //    (indicatorResultClientData.IndicatorCustomCode == "D49"))
                    else if (indicatorResultClientData.IndicatorCustomCode == "D50")
                    {
                        this.indicatorGroupDisaptchPanelControl1.dispatchOrderImprtantStatusIndicatorControl1.dispatchOrderImprtantStatusGridIndicatorControl1.AddOrUpdate(irvcd);
                    }
                    else
                    {
                        this.indicatorControlGroup.indicatorGridControl1.AddOrUpdate(irvcd);
                    }
                }
            }
            #endregion group

        }             
        private void IndicatorsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            (this.MdiParent as SupervisionForm).SupervisionCommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(IndicatorsForm_SupervisionCommittedChanges);
        }
    }
}