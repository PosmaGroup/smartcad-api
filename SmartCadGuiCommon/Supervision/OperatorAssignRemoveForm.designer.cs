using SmartCadControls;
namespace SmartCadGuiCommon
{
    partial class OperatorAssignRemoveForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OperatorAssignRemoveForm));
            this.textEditName = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlRemoveForm = new DevExpress.XtraLayout.LayoutControl();
            this.simpleButtonDetail = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonSearch = new DevExpress.XtraEditors.SimpleButton();
            this.dateEditEnd = new DevExpress.XtraEditors.DateEdit();
            this.dateEditStart = new DevExpress.XtraEditors.DateEdit();
            this.checkEditAllOper = new DevExpress.XtraEditors.CheckEdit();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonAccept = new DevExpress.XtraEditors.SimpleButton();
            this.gridControlExOperators = new GridControlEx();
            this.gridViewExOperators = new GridViewEx();
            this.radioButtonByOperator = new System.Windows.Forms.RadioButton();
            this.checkEditAll = new DevExpress.XtraEditors.CheckEdit();
            this.gridControlExAssignments = new GridControlEx();
            this.gridViewExAssignments = new GridViewEx();
            this.radioButtonByRange = new System.Windows.Forms.RadioButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupOperatorsAssigned = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupByRange = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemEnd = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemStart = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupByOperator = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupOperator = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemName = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.textEditName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlRemoveForm)).BeginInit();
            this.layoutControlRemoveForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAllOper.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExOperators)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExOperators)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAll.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExAssignments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExAssignments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupOperatorsAssigned)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupByRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupByOperator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupOperator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).BeginInit();
            this.SuspendLayout();
            // 
            // textEditName
            // 
            this.textEditName.Enabled = false;
            this.textEditName.Location = new System.Drawing.Point(142, 44);
            this.textEditName.Name = "textEditName";
            this.textEditName.Size = new System.Drawing.Size(306, 20);
            this.textEditName.StyleController = this.layoutControlRemoveForm;
            this.textEditName.TabIndex = 0;
            // 
            // layoutControlRemoveForm
            // 
            this.layoutControlRemoveForm.AllowCustomizationMenu = false;
            this.layoutControlRemoveForm.Controls.Add(this.simpleButtonDetail);
            this.layoutControlRemoveForm.Controls.Add(this.simpleButtonSearch);
            this.layoutControlRemoveForm.Controls.Add(this.dateEditEnd);
            this.layoutControlRemoveForm.Controls.Add(this.dateEditStart);
            this.layoutControlRemoveForm.Controls.Add(this.checkEditAllOper);
            this.layoutControlRemoveForm.Controls.Add(this.simpleButtonCancel);
            this.layoutControlRemoveForm.Controls.Add(this.simpleButtonAccept);
            this.layoutControlRemoveForm.Controls.Add(this.gridControlExOperators);
            this.layoutControlRemoveForm.Controls.Add(this.radioButtonByOperator);
            this.layoutControlRemoveForm.Controls.Add(this.checkEditAll);
            this.layoutControlRemoveForm.Controls.Add(this.gridControlExAssignments);
            this.layoutControlRemoveForm.Controls.Add(this.radioButtonByRange);
            this.layoutControlRemoveForm.Controls.Add(this.textEditName);
            this.layoutControlRemoveForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlRemoveForm.Location = new System.Drawing.Point(0, 0);
            this.layoutControlRemoveForm.Name = "layoutControlRemoveForm";
            this.layoutControlRemoveForm.Root = this.layoutControlGroup1;
            this.layoutControlRemoveForm.Size = new System.Drawing.Size(472, 576);
            this.layoutControlRemoveForm.TabIndex = 1;
            this.layoutControlRemoveForm.Text = "layoutControl1";
            // 
            // simpleButtonDetail
            // 
            this.simpleButtonDetail.Location = new System.Drawing.Point(366, 317);
            this.simpleButtonDetail.Name = "simpleButtonDetail";
            this.simpleButtonDetail.Size = new System.Drawing.Size(82, 32);
            this.simpleButtonDetail.StyleController = this.layoutControlRemoveForm;
            this.simpleButtonDetail.TabIndex = 17;
            this.simpleButtonDetail.Text = "Details";
            this.simpleButtonDetail.Click += new System.EventHandler(this.simpleButtonDetail_Click);
            // 
            // simpleButtonSearch
            // 
            this.simpleButtonSearch.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonSearch.Image")));
            this.simpleButtonSearch.Location = new System.Drawing.Point(371, 141);
            this.simpleButtonSearch.Name = "simpleButtonSearch";
            this.simpleButtonSearch.Size = new System.Drawing.Size(27, 25);
            this.simpleButtonSearch.StyleController = this.layoutControlRemoveForm;
            this.simpleButtonSearch.TabIndex = 14;
            this.simpleButtonSearch.Click += new System.EventHandler(this.simpleButtonSearch_Click);
            // 
            // dateEditEnd
            // 
            this.dateEditEnd.EditValue = null;
            this.dateEditEnd.Location = new System.Drawing.Point(314, 140);
            this.dateEditEnd.Name = "dateEditEnd";
            this.dateEditEnd.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dateEditEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEnd.Properties.DisplayFormat.FormatString = "MM/dd/yyyy HH:mm";
            this.dateEditEnd.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.dateEditEnd.Properties.EditFormat.FormatString = "MM/dd/yyyy HH:mm";
            this.dateEditEnd.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.dateEditEnd.Properties.Mask.EditMask = "MM/dd/yyyy HH:mm";
            this.dateEditEnd.Properties.ShowToday = false;
            this.dateEditEnd.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditEnd.Size = new System.Drawing.Size(50, 20);
            this.dateEditEnd.StyleController = this.layoutControlRemoveForm;
            this.dateEditEnd.TabIndex = 13;
            // 
            // dateEditStart
            // 
            this.dateEditStart.EditValue = null;
            this.dateEditStart.Location = new System.Drawing.Point(142, 140);
            this.dateEditStart.Name = "dateEditStart";
            this.dateEditStart.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dateEditStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStart.Properties.DisplayFormat.FormatString = "MM/dd/yyyy HH:mm";
            this.dateEditStart.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.dateEditStart.Properties.EditFormat.FormatString = "MM/dd/yyyy HH:mm";
            this.dateEditStart.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.dateEditStart.Properties.Mask.EditMask = "MM/dd/yyyy HH:mm";
            this.dateEditStart.Properties.ShowToday = false;
            this.dateEditStart.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditStart.Size = new System.Drawing.Size(50, 20);
            this.dateEditStart.StyleController = this.layoutControlRemoveForm;
            this.dateEditStart.TabIndex = 12;
            this.dateEditStart.EditValueChanged += new System.EventHandler(this.dateEditStart_EditValueChanged);
            // 
            // checkEditAllOper
            // 
            this.checkEditAllOper.Location = new System.Drawing.Point(24, 490);
            this.checkEditAllOper.Name = "checkEditAllOper";
            this.checkEditAllOper.Properties.Caption = "checkEditAllOper";
            this.checkEditAllOper.Size = new System.Drawing.Size(110, 19);
            this.checkEditAllOper.StyleController = this.layoutControlRemoveForm;
            this.checkEditAllOper.TabIndex = 11;
            this.checkEditAllOper.CheckedChanged += new System.EventHandler(this.checkEditAllOper_CheckedChanged);
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(378, 532);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(82, 32);
            this.simpleButtonCancel.StyleController = this.layoutControlRemoveForm;
            this.simpleButtonCancel.TabIndex = 10;
            this.simpleButtonCancel.Text = "simpleButton2";
            // 
            // simpleButtonAccept
            // 
            this.simpleButtonAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButtonAccept.Enabled = false;
            this.simpleButtonAccept.Location = new System.Drawing.Point(292, 532);
            this.simpleButtonAccept.Name = "simpleButtonAccept";
            this.simpleButtonAccept.Size = new System.Drawing.Size(82, 32);
            this.simpleButtonAccept.StyleController = this.layoutControlRemoveForm;
            this.simpleButtonAccept.TabIndex = 9;
            this.simpleButtonAccept.Text = "simpleButton1";
            this.simpleButtonAccept.Click += new System.EventHandler(this.simpleButtonAccept_Click);
            // 
            // gridControlExOperators
            // 
            this.gridControlExOperators.EnableAutoFilter = true;
            this.gridControlExOperators.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExOperators.Location = new System.Drawing.Point(24, 353);
            this.gridControlExOperators.MainView = this.gridViewExOperators;
            this.gridControlExOperators.Name = "gridControlExOperators";
            this.gridControlExOperators.Size = new System.Drawing.Size(424, 133);
            this.gridControlExOperators.TabIndex = 8;
            this.gridControlExOperators.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExOperators});
            this.gridControlExOperators.ViewTotalRows = true;
            // 
            // gridViewExOperators
            // 
            this.gridViewExOperators.AllowFocusedRowChanged = true;
            this.gridViewExOperators.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExOperators.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExOperators.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExOperators.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExOperators.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewExOperators.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewExOperators.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExOperators.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExOperators.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExOperators.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExOperators.EnablePreviewLineForFocusedRow = false;
            this.gridViewExOperators.GridControl = this.gridControlExOperators;
            this.gridViewExOperators.Name = "gridViewExOperators";
            this.gridViewExOperators.OptionsMenu.EnableFooterMenu = false;
            this.gridViewExOperators.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExOperators.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExOperators.OptionsView.ShowFooter = true;
            this.gridViewExOperators.OptionsView.ShowGroupPanel = false;
            this.gridViewExOperators.ViewTotalRows = true;
            this.gridViewExOperators.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewExOperators_CellValueChanging);
            // 
            // radioButtonByOperator
            // 
            this.radioButtonByOperator.Location = new System.Drawing.Point(132, 112);
            this.radioButtonByOperator.Name = "radioButtonByOperator";
            this.radioButtonByOperator.Size = new System.Drawing.Size(99, 24);
            this.radioButtonByOperator.TabIndex = 7;
            this.radioButtonByOperator.TabStop = true;
            this.radioButtonByOperator.Text = "Por operador";
            this.radioButtonByOperator.UseVisualStyleBackColor = true;
            this.radioButtonByOperator.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // checkEditAll
            // 
            this.checkEditAll.Location = new System.Drawing.Point(24, 317);
            this.checkEditAll.Name = "checkEditAll";
            this.checkEditAll.Properties.Caption = "checkEditAll";
            this.checkEditAll.Size = new System.Drawing.Size(124, 19);
            this.checkEditAll.StyleController = this.layoutControlRemoveForm;
            this.checkEditAll.TabIndex = 6;
            this.checkEditAll.CheckedChanged += new System.EventHandler(this.checkEditAll_CheckedChanged);
            // 
            // gridControlExAssignments
            // 
            this.gridControlExAssignments.EnableAutoFilter = true;
            this.gridControlExAssignments.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExAssignments.Location = new System.Drawing.Point(24, 171);
            this.gridControlExAssignments.MainView = this.gridViewExAssignments;
            this.gridControlExAssignments.Name = "gridControlExAssignments";
            this.gridControlExAssignments.Size = new System.Drawing.Size(424, 142);
            this.gridControlExAssignments.TabIndex = 5;
            this.gridControlExAssignments.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExAssignments});
            this.gridControlExAssignments.ViewTotalRows = true;
            // 
            // gridViewExAssignments
            // 
            this.gridViewExAssignments.AllowFocusedRowChanged = true;
            this.gridViewExAssignments.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExAssignments.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExAssignments.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExAssignments.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExAssignments.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewExAssignments.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewExAssignments.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExAssignments.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExAssignments.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExAssignments.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExAssignments.EnablePreviewLineForFocusedRow = false;
            this.gridViewExAssignments.GridControl = this.gridControlExAssignments;
            this.gridViewExAssignments.Name = "gridViewExAssignments";
            this.gridViewExAssignments.OptionsMenu.EnableFooterMenu = false;
            this.gridViewExAssignments.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExAssignments.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExAssignments.OptionsView.ShowFooter = true;
            this.gridViewExAssignments.OptionsView.ShowGroupPanel = false;
            this.gridViewExAssignments.ViewTotalRows = true;
            this.gridViewExAssignments.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewExAssignments_CellValueChanging);
            // 
            // radioButtonByRange
            // 
            this.radioButtonByRange.Location = new System.Drawing.Point(27, 112);
            this.radioButtonByRange.Name = "radioButtonByRange";
            this.radioButtonByRange.Size = new System.Drawing.Size(95, 24);
            this.radioButtonByRange.TabIndex = 4;
            this.radioButtonByRange.TabStop = true;
            this.radioButtonByRange.Text = "Por rango";
            this.radioButtonByRange.UseVisualStyleBackColor = true;
            this.radioButtonByRange.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.emptySpaceItem4,
            this.layoutControlGroupOperatorsAssigned,
            this.layoutControlGroupOperator});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(472, 576);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.simpleButtonAccept;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(280, 520);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.simpleButtonCancel;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(366, 520);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 520);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(280, 36);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupOperatorsAssigned
            // 
            this.layoutControlGroupOperatorsAssigned.CustomizationFormText = "layoutControlGroupOperatorsAssigned";
            this.layoutControlGroupOperatorsAssigned.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.layoutControlItem1,
            this.layoutControlGroupByRange,
            this.layoutControlGroupByOperator,
            this.layoutControlItem4});
            this.layoutControlGroupOperatorsAssigned.Location = new System.Drawing.Point(0, 68);
            this.layoutControlGroupOperatorsAssigned.Name = "layoutControlGroupOperatorsAssigned";
            this.layoutControlGroupOperatorsAssigned.Size = new System.Drawing.Size(452, 452);
            this.layoutControlGroupOperatorsAssigned.Text = "layoutControlGroupOperatorsAssigned";
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(214, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(214, 28);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.radioButtonByRange;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(105, 28);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(105, 28);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 2, 2);
            this.layoutControlItem1.Size = new System.Drawing.Size(105, 28);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroupByRange
            // 
            this.layoutControlGroupByRange.CustomizationFormText = "layoutControlGroupByRange";
            this.layoutControlGroupByRange.GroupBordersVisible = false;
            this.layoutControlGroupByRange.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemEnd,
            this.layoutControlItemStart,
            this.layoutControlItem11,
            this.emptySpaceItem2,
            this.layoutControlItem3,
            this.emptySpaceItem6,
            this.layoutControlItem2,
            this.layoutControlItem12});
            this.layoutControlGroupByRange.Location = new System.Drawing.Point(0, 28);
            this.layoutControlGroupByRange.Name = "layoutControlGroupByRange";
            this.layoutControlGroupByRange.Size = new System.Drawing.Size(428, 213);
            this.layoutControlGroupByRange.Text = "layoutControlGroupByRange";
            // 
            // layoutControlItemEnd
            // 
            this.layoutControlItemEnd.Control = this.dateEditEnd;
            this.layoutControlItemEnd.CustomizationFormText = "layoutControlItemEnd";
            this.layoutControlItemEnd.Location = new System.Drawing.Point(172, 0);
            this.layoutControlItemEnd.Name = "layoutControlItemEnd";
            this.layoutControlItemEnd.Size = new System.Drawing.Size(172, 31);
            this.layoutControlItemEnd.Text = "layoutControlItemEnd";
            this.layoutControlItemEnd.TextSize = new System.Drawing.Size(114, 13);
            // 
            // layoutControlItemStart
            // 
            this.layoutControlItemStart.Control = this.dateEditStart;
            this.layoutControlItemStart.CustomizationFormText = "layoutControlItemStart";
            this.layoutControlItemStart.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemStart.Name = "layoutControlItemStart";
            this.layoutControlItemStart.Size = new System.Drawing.Size(172, 31);
            this.layoutControlItemStart.Text = "layoutControlItemStart";
            this.layoutControlItemStart.TextSize = new System.Drawing.Size(114, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.simpleButtonSearch;
            this.layoutControlItem11.CustomizationFormText = "layoutControlItem11";
            this.layoutControlItem11.Location = new System.Drawing.Point(344, 0);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(37, 31);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(37, 31);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 3, 3);
            this.layoutControlItem11.ShowInCustomizationForm = false;
            this.layoutControlItem11.Size = new System.Drawing.Size(37, 31);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "layoutControlItem11";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextToControlDistance = 0;
            this.layoutControlItem11.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(128, 177);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(110, 30);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(214, 36);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.checkEditAll;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 177);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(128, 36);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(128, 36);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(128, 36);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(381, 0);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(47, 31);
            this.emptySpaceItem6.Text = "emptySpaceItem6";
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridControlExAssignments;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 31);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(428, 146);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.simpleButtonDetail;
            this.layoutControlItem12.CustomizationFormText = "layoutControlItem12";
            this.layoutControlItem12.Location = new System.Drawing.Point(342, 177);
            this.layoutControlItem12.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Text = "layoutControlItem12";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextToControlDistance = 0;
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlGroupByOperator
            // 
            this.layoutControlGroupByOperator.CustomizationFormText = "layoutControlGroupByOperaotr";
            this.layoutControlGroupByOperator.GroupBordersVisible = false;
            this.layoutControlGroupByOperator.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem5,
            this.layoutControlItem8,
            this.layoutControlItem5});
            this.layoutControlGroupByOperator.Location = new System.Drawing.Point(0, 241);
            this.layoutControlGroupByOperator.Name = "layoutControlGroupByOperator";
            this.layoutControlGroupByOperator.Size = new System.Drawing.Size(428, 167);
            this.layoutControlGroupByOperator.Text = "layoutControlGroupByOperator";
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(114, 137);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(314, 30);
            this.emptySpaceItem5.Text = "emptySpaceItem5";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.checkEditAllOper;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 137);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(114, 30);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(114, 30);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(114, 30);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.gridControlExOperators;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(428, 137);
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.radioButtonByOperator;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(105, 0);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(109, 28);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(109, 28);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 2, 2);
            this.layoutControlItem4.Size = new System.Drawing.Size(109, 28);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlGroupOperator
            // 
            this.layoutControlGroupOperator.CustomizationFormText = "layoutControlGroupOperator";
            this.layoutControlGroupOperator.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemName});
            this.layoutControlGroupOperator.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupOperator.Name = "layoutControlGroupOperator";
            this.layoutControlGroupOperator.Size = new System.Drawing.Size(452, 68);
            this.layoutControlGroupOperator.Text = "layoutControlGroupOperator";
            // 
            // layoutControlItemName
            // 
            this.layoutControlItemName.Control = this.textEditName;
            this.layoutControlItemName.CustomizationFormText = "layoutControlItemName";
            this.layoutControlItemName.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemName.Name = "layoutControlItemName";
            this.layoutControlItemName.Size = new System.Drawing.Size(428, 24);
            this.layoutControlItemName.Text = "layoutControlItemName";
            this.layoutControlItemName.TextSize = new System.Drawing.Size(114, 13);
            // 
            // OperatorAssignRemoveForm
            // 
            this.AcceptButton = this.simpleButtonAccept;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonCancel;
            this.ClientSize = new System.Drawing.Size(472, 576);
            this.Controls.Add(this.layoutControlRemoveForm);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(480, 610);
            this.Name = "OperatorAssignRemoveForm";
            this.Text = "OperatorAssignRemoveForm";
            this.Load += new System.EventHandler(this.OperatorAssignRemoveForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.textEditName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlRemoveForm)).EndInit();
            this.layoutControlRemoveForm.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAllOper.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExOperators)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExOperators)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAll.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExAssignments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExAssignments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupOperatorsAssigned)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupByRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupByOperator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupOperator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit textEditName;
        private DevExpress.XtraLayout.LayoutControl layoutControlRemoveForm;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemName;
        private GridControlEx gridControlExAssignments;
        private GridViewEx gridViewExAssignments;
        private System.Windows.Forms.RadioButton radioButtonByRange;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private System.Windows.Forms.RadioButton radioButtonByOperator;
        private DevExpress.XtraEditors.CheckEdit checkEditAll;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private GridControlEx gridControlExOperators;
        private GridViewEx gridViewExOperators;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private DevExpress.XtraEditors.SimpleButton simpleButtonAccept;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.CheckEdit checkEditAllOper;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraEditors.DateEdit dateEditEnd;
        private DevExpress.XtraEditors.DateEdit dateEditStart;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemStart;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemEnd;
        private DevExpress.XtraEditors.SimpleButton simpleButtonSearch;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupOperatorsAssigned;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupOperator;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupByRange;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupByOperator;
        private DevExpress.XtraEditors.SimpleButton simpleButtonDetail;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
    }
}