namespace SmartCadGuiCommon
{
    partial class SelectEvaluationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SelectEvaluationForm));
            this.buttonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.buttonOK = new DevExpress.XtraEditors.SimpleButton();
            this.listBoxControlEvaluations = new DevExpress.XtraEditors.ListBoxControl();
            this.layoutControlSelectEval = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemEvaluations = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControlEvaluations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlSelectEval)).BeginInit();
            this.layoutControlSelectEval.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemEvaluations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(221, 175);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 25);
            this.buttonCancel.StyleController = this.layoutControlSelectEval;
            this.buttonCancel.TabIndex = 6;
            this.buttonCancel.Text = "Cancelar";
            // 
            // buttonOK
            // 
            this.buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOK.Location = new System.Drawing.Point(135, 175);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 25);
            this.buttonOK.StyleController = this.layoutControlSelectEval;
            this.buttonOK.TabIndex = 5;
            this.buttonOK.Text = "Accept";
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // listBoxControlEvaluations
            // 
            this.listBoxControlEvaluations.Location = new System.Drawing.Point(7, 32);
            this.listBoxControlEvaluations.Name = "listBoxControlEvaluations";
            this.listBoxControlEvaluations.Size = new System.Drawing.Size(289, 132);
            this.listBoxControlEvaluations.StyleController = this.layoutControlSelectEval;
            this.listBoxControlEvaluations.TabIndex = 4;
            this.listBoxControlEvaluations.SelectedIndexChanged += new System.EventHandler(this.checkedListBoxEvaluation_SelectedIndexChanged);
            // 
            // layoutControlSelectEval
            // 
            this.layoutControlSelectEval.AllowCustomizationMenu = false;
            this.layoutControlSelectEval.Appearance.DisabledLayoutGroupCaption.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControlSelectEval.Appearance.DisabledLayoutGroupCaption.Options.UseForeColor = true;
            this.layoutControlSelectEval.Appearance.DisabledLayoutItem.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControlSelectEval.Appearance.DisabledLayoutItem.Options.UseForeColor = true;
            this.layoutControlSelectEval.Controls.Add(this.listBoxControlEvaluations);
            this.layoutControlSelectEval.Controls.Add(this.buttonCancel);
            this.layoutControlSelectEval.Controls.Add(this.buttonOK);
            this.layoutControlSelectEval.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlSelectEval.Location = new System.Drawing.Point(0, 0);
            this.layoutControlSelectEval.Name = "layoutControlSelectEval";
            this.layoutControlSelectEval.Root = this.layoutControlGroup1;
            this.layoutControlSelectEval.Size = new System.Drawing.Size(302, 206);
            this.layoutControlSelectEval.TabIndex = 7;
            this.layoutControlSelectEval.Text = "layoutControl1";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemEvaluations,
            this.layoutControlItem2,
            this.layoutControlItem1,
            this.emptySpaceItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(302, 206);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItemEvaluations
            // 
            this.layoutControlItemEvaluations.Control = this.listBoxControlEvaluations;
            this.layoutControlItemEvaluations.CustomizationFormText = "layoutControlItemEvaluations";
            this.layoutControlItemEvaluations.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemEvaluations.Name = "layoutControlItemEvaluations";
            this.layoutControlItemEvaluations.Size = new System.Drawing.Size(300, 168);
            this.layoutControlItemEvaluations.Text = "layoutControlItemEvaluations";
            this.layoutControlItemEvaluations.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItemEvaluations.TextSize = new System.Drawing.Size(142, 20);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.buttonOK;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(128, 168);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.buttonCancel;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(214, 168);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 168);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(128, 36);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // SelectEvaluationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(302, 206);
            this.Controls.Add(this.layoutControlSelectEval);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(310, 240);
            this.Name = "SelectEvaluationForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Seleccionar evaluacion";
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControlEvaluations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlSelectEval)).EndInit();
            this.layoutControlSelectEval.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemEvaluations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.ListBoxControl listBoxControlEvaluations;
        private DevExpress.XtraEditors.SimpleButton buttonCancel;
        private DevExpress.XtraEditors.SimpleButton buttonOK;
        private DevExpress.XtraLayout.LayoutControl layoutControlSelectEval;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemEvaluations;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
    }
}