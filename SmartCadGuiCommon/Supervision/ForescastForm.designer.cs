using SmartCadControls;
namespace SmartCadGuiCommon
{
    partial class ForescastForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ForescastForm));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.checkedListBoxForecastHistory = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.radioGroup1 = new DevExpress.XtraEditors.RadioGroup();
            this.gridControlForecastDetails = new DevExpress.XtraGrid.GridControl();
            this.gridViewForecastDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnIndicator = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnIndicatorType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnPostivePattern = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnLow = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnHigh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnMeasureUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButtonSearch = new DevExpress.XtraEditors.SimpleButton();
            this.dateEditFrom = new DevExpress.XtraEditors.DateEdit();
            this.gridControlForecast = new GridControlEx();
            this.gridViewForecast = new GridViewEx();
            this.toolTipController1 = new DevExpress.Utils.ToolTipController(this.components);
            this.dateEditTo = new DevExpress.XtraEditors.DateEdit();
            this.checkedListBoxForecastUpdates = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupForecast = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemFilter = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItemFrom = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTo = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.RibbonControl = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.BarButtonItemSendMonitor = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemAddForecast = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemDeleteForecast = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemUpdateForecast = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemDuplicate = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemReplace = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemFinalize = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSend = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSave = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemRefreshGraphic = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemForecast = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItemHistoricGraphic = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItemHistoryDetails = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemRestore = new DevExpress.XtraBars.BarButtonItem();
            this.RibbonPageMonitoring = new DevExpress.XtraReports.UserDesigner.XRDesignRibbonPage();
            this.ribbonPageGroupPerformance = new DevExpress.XtraReports.UserDesigner.XRDesignRibbonPageGroup();
            this.ribbonPageGroupGeneralOptMonitor = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkedListBoxForecastHistory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlForecastDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewForecastDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlForecast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewForecast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkedListBoxForecastUpdates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupForecast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RibbonControl)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowCustomizationMenu = false;
            this.layoutControl1.Controls.Add(this.checkedListBoxForecastHistory);
            this.layoutControl1.Controls.Add(this.radioGroup1);
            this.layoutControl1.Controls.Add(this.gridControlForecastDetails);
            this.layoutControl1.Controls.Add(this.simpleButtonSearch);
            this.layoutControl1.Controls.Add(this.dateEditFrom);
            this.layoutControl1.Controls.Add(this.gridControlForecast);
            this.layoutControl1.Controls.Add(this.dateEditTo);
            this.layoutControl1.Controls.Add(this.checkedListBoxForecastUpdates);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1270, 787);
            this.layoutControl1.TabIndex = 8;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // checkedListBoxForecastHistory
            // 
            this.checkedListBoxForecastHistory.CheckOnClick = true;
            this.checkedListBoxForecastHistory.Enabled = false;
            this.checkedListBoxForecastHistory.Items.AddRange(new DevExpress.XtraEditors.Controls.CheckedListBoxItem[] {
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem(null),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem(null)});
            this.checkedListBoxForecastHistory.Location = new System.Drawing.Point(375, 65);
            this.checkedListBoxForecastHistory.Name = "checkedListBoxForecastHistory";
            this.checkedListBoxForecastHistory.Size = new System.Drawing.Size(232, 62);
            this.checkedListBoxForecastHistory.StyleController = this.layoutControl1;
            this.checkedListBoxForecastHistory.TabIndex = 11;
            this.checkedListBoxForecastHistory.ItemCheck += new DevExpress.XtraEditors.Controls.ItemCheckEventHandler(this.checkedListBoxForecastHistory_ItemCheck);
            // 
            // radioGroup1
            // 
            this.radioGroup1.Location = new System.Drawing.Point(122, 27);
            this.radioGroup1.Name = "radioGroup1";
            this.radioGroup1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.radioGroup1.Properties.Appearance.Options.UseBackColor = true;
            this.radioGroup1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.radioGroup1.Properties.Columns = 3;
            this.radioGroup1.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, ""),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "")});
            this.radioGroup1.Size = new System.Drawing.Size(812, 34);
            this.radioGroup1.StyleController = this.layoutControl1;
            this.radioGroup1.TabIndex = 1;
            this.radioGroup1.SelectedIndexChanged += new System.EventHandler(this.radioGroup1_SelectedIndexChanged);
            // 
            // gridControlForecastDetails
            // 
            this.gridControlForecastDetails.Location = new System.Drawing.Point(12, 372);
            this.gridControlForecastDetails.MainView = this.gridViewForecastDetail;
            this.gridControlForecastDetails.Name = "gridControlForecastDetails";
            this.gridControlForecastDetails.Size = new System.Drawing.Size(1246, 403);
            this.gridControlForecastDetails.TabIndex = 2;
            this.gridControlForecastDetails.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewForecastDetail});
            // 
            // gridViewForecastDetail
            // 
            this.gridViewForecastDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnIndicator,
            this.gridColumnIndicatorType,
            this.gridColumnPostivePattern,
            this.gridColumnLow,
            this.gridColumnHigh,
            this.gridColumnMeasureUnit});
            this.gridViewForecastDetail.GridControl = this.gridControlForecastDetails;
            this.gridViewForecastDetail.GroupCount = 1;
            this.gridViewForecastDetail.GroupFormat = "[#image]{1} {2}";
            this.gridViewForecastDetail.Name = "gridViewForecastDetail";
            this.gridViewForecastDetail.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewForecastDetail.OptionsMenu.EnableColumnMenu = false;
            this.gridViewForecastDetail.OptionsMenu.EnableFooterMenu = false;
            this.gridViewForecastDetail.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridViewForecastDetail.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewForecastDetail.OptionsView.ShowGroupPanel = false;
            this.gridViewForecastDetail.OptionsView.ShowIndicator = false;
            this.gridViewForecastDetail.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumnIndicatorType, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumnIndicator
            // 
            this.gridColumnIndicator.Caption = "gridColumn1";
            this.gridColumnIndicator.FieldName = "Indicator";
            this.gridColumnIndicator.Name = "gridColumnIndicator";
            this.gridColumnIndicator.OptionsColumn.AllowEdit = false;
            this.gridColumnIndicator.OptionsFilter.AllowAutoFilter = false;
            this.gridColumnIndicator.OptionsFilter.AllowFilter = false;
            this.gridColumnIndicator.Visible = true;
            this.gridColumnIndicator.VisibleIndex = 0;
            // 
            // gridColumnIndicatorType
            // 
            this.gridColumnIndicatorType.Caption = "gridColumn2";
            this.gridColumnIndicatorType.FieldName = "IndicatorType";
            this.gridColumnIndicatorType.Name = "gridColumnIndicatorType";
            this.gridColumnIndicatorType.OptionsColumn.AllowEdit = false;
            this.gridColumnIndicatorType.OptionsFilter.AllowAutoFilter = false;
            this.gridColumnIndicatorType.OptionsFilter.AllowFilter = false;
            this.gridColumnIndicatorType.Visible = true;
            this.gridColumnIndicatorType.VisibleIndex = 1;
            // 
            // gridColumnPostivePattern
            // 
            this.gridColumnPostivePattern.Caption = "gridColumn3";
            this.gridColumnPostivePattern.FieldName = "PositivePattern";
            this.gridColumnPostivePattern.Name = "gridColumnPostivePattern";
            this.gridColumnPostivePattern.OptionsColumn.AllowEdit = false;
            this.gridColumnPostivePattern.OptionsFilter.AllowAutoFilter = false;
            this.gridColumnPostivePattern.OptionsFilter.AllowFilter = false;
            this.gridColumnPostivePattern.Visible = true;
            this.gridColumnPostivePattern.VisibleIndex = 1;
            // 
            // gridColumnLow
            // 
            this.gridColumnLow.Caption = "gridColumn4";
            this.gridColumnLow.FieldName = "Low";
            this.gridColumnLow.Name = "gridColumnLow";
            this.gridColumnLow.OptionsColumn.AllowEdit = false;
            this.gridColumnLow.OptionsFilter.AllowAutoFilter = false;
            this.gridColumnLow.OptionsFilter.AllowFilter = false;
            this.gridColumnLow.Visible = true;
            this.gridColumnLow.VisibleIndex = 2;
            // 
            // gridColumnHigh
            // 
            this.gridColumnHigh.Caption = "gridColumn5";
            this.gridColumnHigh.FieldName = "High";
            this.gridColumnHigh.Name = "gridColumnHigh";
            this.gridColumnHigh.OptionsColumn.AllowEdit = false;
            this.gridColumnHigh.OptionsFilter.AllowAutoFilter = false;
            this.gridColumnHigh.OptionsFilter.AllowFilter = false;
            this.gridColumnHigh.Visible = true;
            this.gridColumnHigh.VisibleIndex = 3;
            // 
            // gridColumnMeasureUnit
            // 
            this.gridColumnMeasureUnit.Caption = "gridColumn6";
            this.gridColumnMeasureUnit.FieldName = "MeasureUnit";
            this.gridColumnMeasureUnit.Name = "gridColumnMeasureUnit";
            this.gridColumnMeasureUnit.OptionsColumn.AllowEdit = false;
            this.gridColumnMeasureUnit.OptionsFilter.AllowAutoFilter = false;
            this.gridColumnMeasureUnit.OptionsFilter.AllowFilter = false;
            this.gridColumnMeasureUnit.Visible = true;
            this.gridColumnMeasureUnit.VisibleIndex = 4;
            // 
            // simpleButtonSearch
            // 
            this.simpleButtonSearch.Enabled = false;
            this.simpleButtonSearch.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonSearch.Image")));
            this.simpleButtonSearch.Location = new System.Drawing.Point(875, 96);
            this.simpleButtonSearch.Name = "simpleButtonSearch";
            this.simpleButtonSearch.Size = new System.Drawing.Size(35, 31);
            this.simpleButtonSearch.StyleController = this.layoutControl1;
            this.simpleButtonSearch.TabIndex = 7;
            this.simpleButtonSearch.ToolTip = "Buscar";
            this.simpleButtonSearch.Click += new System.EventHandler(this.simpleButtonSearch_Click);
            // 
            // dateEditFrom
            // 
            this.dateEditFrom.EditValue = null;
            this.dateEditFrom.Enabled = false;
            this.dateEditFrom.Location = new System.Drawing.Point(777, 65);
            this.dateEditFrom.Name = "dateEditFrom";
            this.dateEditFrom.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dateEditFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFrom.Properties.DisplayFormat.FormatString = "MM/dd/yyyy HH:mm";
            this.dateEditFrom.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.dateEditFrom.Properties.EditFormat.FormatString = "MM/dd/yyyy HH:mm";
            this.dateEditFrom.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.dateEditFrom.Properties.Mask.EditMask = "MM/dd/yyyy HH:mm";
            this.dateEditFrom.Properties.ShowToday = false;
            this.dateEditFrom.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFrom.Properties.VistaTimeProperties.Mask.EditMask = "";
            this.dateEditFrom.Size = new System.Drawing.Size(94, 20);
            this.dateEditFrom.StyleController = this.layoutControl1;
            this.dateEditFrom.TabIndex = 3;
            this.dateEditFrom.EditValueChanged += new System.EventHandler(this.dateEditFrom_EditValueChanged);
            // 
            // gridControlForecast
            // 
            this.gridControlForecast.EnableAutoFilter = true;
            this.gridControlForecast.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlForecast.Location = new System.Drawing.Point(7, 131);
            this.gridControlForecast.MainView = this.gridViewForecast;
            this.gridControlForecast.Name = "gridControlForecast";
            this.gridControlForecast.Size = new System.Drawing.Size(1256, 207);
            this.gridControlForecast.TabIndex = 4;
            this.gridControlForecast.ToolTipController = this.toolTipController1;
            this.gridControlForecast.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewForecast});
            this.gridControlForecast.ViewTotalRows = false;
            // 
            // gridViewForecast
            // 
            this.gridViewForecast.AllowFocusedRowChanged = true;
            this.gridViewForecast.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewForecast.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewForecast.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewForecast.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewForecast.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewForecast.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewForecast.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewForecast.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewForecast.EnablePreviewLineForFocusedRow = false;
            this.gridViewForecast.GridControl = this.gridControlForecast;
            this.gridViewForecast.Name = "gridViewForecast";
            this.gridViewForecast.OptionsDetail.EnableMasterViewMode = false;
            this.gridViewForecast.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewForecast.OptionsView.ColumnAutoWidth = false;
            this.gridViewForecast.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewForecast.ViewTotalRows = false;
            this.gridViewForecast.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gridViewForecast_RowClick);
            this.gridViewForecast.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewForecast_FocusedRowChanged);
            this.gridViewForecast.RowCountChanged += new System.EventHandler(this.gridViewForecast_RowCountChanged);
            // 
            // toolTipController1
            // 
            this.toolTipController1.GetActiveObjectInfo += new DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventHandler(this.toolTipController1_GetActiveObjectInfo);
            // 
            // dateEditTo
            // 
            this.dateEditTo.EditValue = null;
            this.dateEditTo.Enabled = false;
            this.dateEditTo.Location = new System.Drawing.Point(777, 96);
            this.dateEditTo.Name = "dateEditTo";
            this.dateEditTo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dateEditTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditTo.Properties.DisplayFormat.FormatString = "MM/dd/yyyy HH:mm";
            this.dateEditTo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.dateEditTo.Properties.EditFormat.FormatString = "MM/dd/yyyy HH:mm";
            this.dateEditTo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.dateEditTo.Properties.Mask.EditMask = "MM/dd/yyyy HH:mm";
            this.dateEditTo.Properties.ShowToday = false;
            this.dateEditTo.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditTo.Properties.VistaTimeProperties.Mask.EditMask = "";
            this.dateEditTo.Size = new System.Drawing.Size(94, 20);
            this.dateEditTo.StyleController = this.layoutControl1;
            this.dateEditTo.TabIndex = 4;
            // 
            // checkedListBoxForecastUpdates
            // 
            this.checkedListBoxForecastUpdates.CheckOnClick = true;
            this.checkedListBoxForecastUpdates.Enabled = false;
            this.checkedListBoxForecastUpdates.Items.AddRange(new DevExpress.XtraEditors.Controls.CheckedListBoxItem[] {
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem(null),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem(null),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem(null)});
            this.checkedListBoxForecastUpdates.Location = new System.Drawing.Point(88, 65);
            this.checkedListBoxForecastUpdates.Name = "checkedListBoxForecastUpdates";
            this.checkedListBoxForecastUpdates.Size = new System.Drawing.Size(209, 62);
            this.checkedListBoxForecastUpdates.StyleController = this.layoutControl1;
            this.checkedListBoxForecastUpdates.TabIndex = 10;
            this.checkedListBoxForecastUpdates.ItemCheck += new DevExpress.XtraEditors.Controls.ItemCheckEventHandler(this.checkedListBoxForecastUpdates_ItemCheck);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupForecast});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1270, 787);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroupForecast
            // 
            this.layoutControlGroupForecast.CustomizationFormText = "layoutControlGroupForecast";
            this.layoutControlGroupForecast.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemFilter,
            this.emptySpaceItem2,
            this.layoutControlItem3,
            this.emptySpaceItem3,
            this.layoutControlItem4,
            this.emptySpaceItem5,
            this.emptySpaceItem4,
            this.layoutControlItemFrom,
            this.layoutControlItemTo,
            this.layoutControlItem7,
            this.emptySpaceItem6,
            this.layoutControlItem1,
            this.splitterItem1,
            this.layoutControlGroup2,
            this.emptySpaceItem1});
            this.layoutControlGroupForecast.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupForecast.Name = "layoutControlGroupForecast";
            this.layoutControlGroupForecast.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupForecast.Size = new System.Drawing.Size(1270, 787);
            this.layoutControlGroupForecast.Text = "layoutControlGroupForecast";
            // 
            // layoutControlItemFilter
            // 
            this.layoutControlItemFilter.Control = this.radioGroup1;
            this.layoutControlItemFilter.CustomizationFormText = "layoutControlItemFilter";
            this.layoutControlItemFilter.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemFilter.MaxSize = new System.Drawing.Size(931, 38);
            this.layoutControlItemFilter.MinSize = new System.Drawing.Size(931, 38);
            this.layoutControlItemFilter.Name = "layoutControlItemFilter";
            this.layoutControlItemFilter.Size = new System.Drawing.Size(931, 38);
            this.layoutControlItemFilter.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemFilter.Text = "layoutControlItemFilter";
            this.layoutControlItemFilter.TextSize = new System.Drawing.Size(111, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 38);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(81, 66);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(81, 66);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(81, 66);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.checkedListBoxForecastUpdates;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(81, 38);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(213, 66);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(213, 66);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(213, 66);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(294, 38);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(74, 66);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(74, 66);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(74, 66);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.checkedListBoxForecastHistory;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(368, 38);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(236, 66);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(236, 66);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(236, 66);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(604, 38);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(51, 66);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(51, 66);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(51, 66);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.Text = "emptySpaceItem5";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(931, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(329, 38);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItemFrom
            // 
            this.layoutControlItemFrom.Control = this.dateEditFrom;
            this.layoutControlItemFrom.CustomizationFormText = "layoutControlItemFrom";
            this.layoutControlItemFrom.Location = new System.Drawing.Point(655, 38);
            this.layoutControlItemFrom.MaxSize = new System.Drawing.Size(213, 31);
            this.layoutControlItemFrom.MinSize = new System.Drawing.Size(213, 31);
            this.layoutControlItemFrom.Name = "layoutControlItemFrom";
            this.layoutControlItemFrom.Size = new System.Drawing.Size(213, 31);
            this.layoutControlItemFrom.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemFrom.Text = "layoutControlItemFrom";
            this.layoutControlItemFrom.TextSize = new System.Drawing.Size(111, 13);
            // 
            // layoutControlItemTo
            // 
            this.layoutControlItemTo.Control = this.dateEditTo;
            this.layoutControlItemTo.CustomizationFormText = "layoutControlItemTo";
            this.layoutControlItemTo.Location = new System.Drawing.Point(655, 69);
            this.layoutControlItemTo.Name = "layoutControlItemTo";
            this.layoutControlItemTo.Size = new System.Drawing.Size(213, 35);
            this.layoutControlItemTo.Text = "layoutControlItemTo";
            this.layoutControlItemTo.TextSize = new System.Drawing.Size(111, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.simpleButtonSearch;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(868, 69);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(39, 35);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(39, 35);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(39, 35);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(907, 38);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(110, 30);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(353, 66);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.Text = "emptySpaceItem6";
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridControlForecast;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 104);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1260, 211);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.CustomizationFormText = "splitterItem1";
            this.splitterItem1.Location = new System.Drawing.Point(0, 315);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(1260, 5);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.ExpandButtonLocation = DevExpress.Utils.GroupElementLocation.BeforeText;
            this.layoutControlGroup2.ExpandButtonMode = DevExpress.Utils.Controls.ExpandButtonMode.Inverted;
            this.layoutControlGroup2.ExpandButtonVisible = true;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 320);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup2.ShowInCustomizationForm = false;
            this.layoutControlGroup2.Size = new System.Drawing.Size(1260, 437);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridControlForecastDetails;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(1250, 407);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(868, 38);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(39, 31);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // RibbonControl
            // 
            this.RibbonControl.Dock = System.Windows.Forms.DockStyle.None;
            this.RibbonControl.ExpandCollapseItem.Id = 0;
            this.RibbonControl.ExpandCollapseItem.Name = "";
            this.RibbonControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RibbonControl.Images = this.imageList;
            this.RibbonControl.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.RibbonControl.ExpandCollapseItem,
            this.BarButtonItemSendMonitor,
            this.barButtonItemAddForecast,
            this.barButtonItemDeleteForecast,
            this.barButtonItemUpdateForecast,
            this.barButtonItemDuplicate,
            this.barButtonItemReplace,
            this.barButtonItemFinalize,
            this.barButtonItemSend,
            this.barButtonItemSave,
            this.barButtonItemPrint,
            this.barButtonItemRefreshGraphic,
            this.barButtonItemForecast,
            this.BarButtonItemHistoricGraphic,
            this.BarButtonItemHistoryDetails,
            this.barButtonItemRestore});
            this.RibbonControl.ItemsVertAlign = DevExpress.Utils.VertAlignment.Top;
            this.RibbonControl.LargeImages = this.imageList;
            this.RibbonControl.Location = new System.Drawing.Point(183, 336);
            this.RibbonControl.MaxItemId = 263;
            this.RibbonControl.MdiMergeStyle = DevExpress.XtraBars.Ribbon.RibbonMdiMergeStyle.Always;
            this.RibbonControl.Name = "RibbonControl";
            this.RibbonControl.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.RibbonPageMonitoring});
            this.RibbonControl.ShowToolbarCustomizeItem = false;
            this.RibbonControl.Size = new System.Drawing.Size(550, 120);
            this.RibbonControl.Toolbar.ShowCustomizeItem = false;
            this.RibbonControl.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "logout.gif");
            // 
            // BarButtonItemSendMonitor
            // 
            this.BarButtonItemSendMonitor.DropDownEnabled = false;
            this.BarButtonItemSendMonitor.Enabled = false;
            this.BarButtonItemSendMonitor.Glyph = ((System.Drawing.Image)(resources.GetObject("BarButtonItemSendMonitor.Glyph")));
            this.BarButtonItemSendMonitor.Id = 127;
            this.BarButtonItemSendMonitor.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Enter));
            this.BarButtonItemSendMonitor.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.BarButtonItemSendMonitor.Name = "BarButtonItemSendMonitor";
            this.BarButtonItemSendMonitor.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            toolTipTitleItem1.Text = "Enviar adjunto a un E- Mail";
            superToolTip1.Items.Add(toolTipTitleItem1);
            this.BarButtonItemSendMonitor.SuperTip = superToolTip1;
            // 
            // barButtonItemAddForecast
            // 
            this.barButtonItemAddForecast.Caption = "Crear";
            this.barButtonItemAddForecast.Enabled = false;
            this.barButtonItemAddForecast.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemAddForecast.Glyph")));
            this.barButtonItemAddForecast.Id = 151;
            this.barButtonItemAddForecast.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemAddForecast.Name = "barButtonItemAddForecast";
            this.barButtonItemAddForecast.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            toolTipTitleItem2.Text = "Crear Pronosticos";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Permite crear un pronostico nuevo";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem1);
            this.barButtonItemAddForecast.SuperTip = superToolTip2;
            this.barButtonItemAddForecast.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemAddForecast_ItemClick);
            // 
            // barButtonItemDeleteForecast
            // 
            this.barButtonItemDeleteForecast.Caption = "Eliminar";
            this.barButtonItemDeleteForecast.Enabled = false;
            this.barButtonItemDeleteForecast.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemDeleteForecast.Glyph")));
            this.barButtonItemDeleteForecast.Id = 152;
            this.barButtonItemDeleteForecast.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemDeleteForecast.Name = "barButtonItemDeleteForecast";
            this.barButtonItemDeleteForecast.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            toolTipTitleItem3.Text = "Eliminar Pronosticos";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Permite eliminar un pronostico existente.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem2);
            this.barButtonItemDeleteForecast.SuperTip = superToolTip3;
            this.barButtonItemDeleteForecast.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemDeleteForecast_ItemClick);
            // 
            // barButtonItemUpdateForecast
            // 
            this.barButtonItemUpdateForecast.Caption = "Modificar";
            this.barButtonItemUpdateForecast.Enabled = false;
            this.barButtonItemUpdateForecast.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemUpdateForecast.Glyph")));
            this.barButtonItemUpdateForecast.Id = 153;
            this.barButtonItemUpdateForecast.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemUpdateForecast.Name = "barButtonItemUpdateForecast";
            this.barButtonItemUpdateForecast.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            toolTipTitleItem4.Text = "Modificar Pronosticos";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Premite modificar pronosticos existentes que no estan en curso.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem3);
            this.barButtonItemUpdateForecast.SuperTip = superToolTip4;
            this.barButtonItemUpdateForecast.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemUpdateForecast_ItemClick);
            // 
            // barButtonItemDuplicate
            // 
            this.barButtonItemDuplicate.Caption = "Duplicar";
            this.barButtonItemDuplicate.Enabled = false;
            this.barButtonItemDuplicate.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemDuplicate.Glyph")));
            this.barButtonItemDuplicate.Id = 154;
            this.barButtonItemDuplicate.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemDuplicate.Name = "barButtonItemDuplicate";
            toolTipTitleItem5.Text = "Duplicar Pronosticos";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Permite hacer una copia de cualquier pronosticos para luego  hacer cambios sobre " +
                "el nuevo pronostico duplicado.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem4);
            this.barButtonItemDuplicate.SuperTip = superToolTip5;
            this.barButtonItemDuplicate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemDuplicate_ItemClick);
            // 
            // barButtonItemReplace
            // 
            this.barButtonItemReplace.Caption = "Reemplazar";
            this.barButtonItemReplace.Enabled = false;
            this.barButtonItemReplace.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemReplace.Glyph")));
            this.barButtonItemReplace.Id = 155;
            this.barButtonItemReplace.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemReplace.Name = "barButtonItemReplace";
            this.barButtonItemReplace.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            toolTipTitleItem6.Text = "Reemplazar Pronosticos";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Se utiliza solo para pronosticos en curso, y es posible actualizar la fecha siemp" +
                "re que sea hacia adelante, mientras que la fecha final quedara bloqueda.";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem5);
            this.barButtonItemReplace.SuperTip = superToolTip6;
            this.barButtonItemReplace.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemReplace_ItemClick);
            // 
            // barButtonItemFinalize
            // 
            this.barButtonItemFinalize.Caption = "Finalizar";
            this.barButtonItemFinalize.Enabled = false;
            this.barButtonItemFinalize.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemFinalize.Glyph")));
            this.barButtonItemFinalize.Id = 156;
            this.barButtonItemFinalize.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemFinalize.Name = "barButtonItemFinalize";
            this.barButtonItemFinalize.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            toolTipTitleItem7.Text = "Finalzar un pronostico";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Detiene un pronostico que actualmente este ejecutandose o en curso.";
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem6);
            this.barButtonItemFinalize.SuperTip = superToolTip7;
            this.barButtonItemFinalize.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemFinalize_ItemClick);
            // 
            // barButtonItemSend
            // 
            this.barButtonItemSend.Enabled = false;
            this.barButtonItemSend.Glyph = global::SmartCadGuiCommon.PrintRibbonControllerResources.RibbonPrintPreview_SendFile;
            this.barButtonItemSend.Id = 229;
            this.barButtonItemSend.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemSend.Name = "barButtonItemSend";
            // 
            // barButtonItemSave
            // 
            this.barButtonItemSave.Enabled = false;
            this.barButtonItemSave.Glyph = global::SmartCadGuiCommon.PrintRibbonControllerResources.RibbonPrintPreview_Save;
            this.barButtonItemSave.Id = 230;
            this.barButtonItemSave.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S));
            this.barButtonItemSave.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemSave.Name = "barButtonItemSave";
            this.barButtonItemSave.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barButtonItemPrint
            // 
            this.barButtonItemPrint.Enabled = false;
            this.barButtonItemPrint.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.Glyph")));
            this.barButtonItemPrint.Id = 231;
            this.barButtonItemPrint.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P));
            this.barButtonItemPrint.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemPrint.Name = "barButtonItemPrint";
            this.barButtonItemPrint.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barButtonItemRefreshGraphic
            // 
            this.barButtonItemRefreshGraphic.Enabled = false;
            this.barButtonItemRefreshGraphic.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemRefreshGraphic.Glyph")));
            this.barButtonItemRefreshGraphic.Id = 239;
            this.barButtonItemRefreshGraphic.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F5);
            this.barButtonItemRefreshGraphic.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemRefreshGraphic.Name = "barButtonItemRefreshGraphic";
            this.barButtonItemRefreshGraphic.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barButtonItemForecast
            // 
            this.barButtonItemForecast.Caption = "Pronosticos";
            this.barButtonItemForecast.Id = 258;
            this.barButtonItemForecast.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O));
            this.barButtonItemForecast.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemForecast.LargeGlyph")));
            this.barButtonItemForecast.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemForecast.Name = "barButtonItemForecast";
            // 
            // BarButtonItemHistoricGraphic
            // 
            this.BarButtonItemHistoricGraphic.Caption = "Graficas historicas";
            this.BarButtonItemHistoricGraphic.Enabled = false;
            this.BarButtonItemHistoricGraphic.Id = 259;
            this.BarButtonItemHistoricGraphic.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("BarButtonItemHistoricGraphic.LargeGlyph")));
            this.BarButtonItemHistoricGraphic.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.BarButtonItemHistoricGraphic.Name = "BarButtonItemHistoricGraphic";
            toolTipTitleItem8.Text = "Graficas Historicas/Permite visualizar un historico de las graficas de todos los " +
                "indicadores.";
            superToolTip8.Items.Add(toolTipTitleItem8);
            this.BarButtonItemHistoricGraphic.SuperTip = superToolTip8;
            this.BarButtonItemHistoricGraphic.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // BarButtonItemHistoryDetails
            // 
            this.BarButtonItemHistoryDetails.Caption = "Detalles historicos";
            this.BarButtonItemHistoryDetails.Enabled = false;
            this.BarButtonItemHistoryDetails.Id = 260;
            this.BarButtonItemHistoryDetails.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("BarButtonItemHistoryDetails.LargeGlyph")));
            this.BarButtonItemHistoryDetails.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.BarButtonItemHistoryDetails.Name = "BarButtonItemHistoryDetails";
            this.BarButtonItemHistoryDetails.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barButtonItemRestore
            // 
            this.barButtonItemRestore.Caption = "Restaurar";
            this.barButtonItemRestore.Enabled = false;
            this.barButtonItemRestore.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemRestore.Glyph")));
            this.barButtonItemRestore.Id = 262;
            this.barButtonItemRestore.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemRestore.Name = "barButtonItemRestore";
            this.barButtonItemRestore.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemRestore_ItemClick);
            // 
            // RibbonPageMonitoring
            // 
            this.RibbonPageMonitoring.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupPerformance,
            this.ribbonPageGroupGeneralOptMonitor});
            this.RibbonPageMonitoring.Name = "RibbonPageMonitoring";
            this.RibbonPageMonitoring.Text = "Monitoreo";
            // 
            // ribbonPageGroupPerformance
            // 
            this.ribbonPageGroupPerformance.AllowMinimize = false;
            this.ribbonPageGroupPerformance.AllowTextClipping = false;
            this.ribbonPageGroupPerformance.ItemLinks.Add(this.barButtonItemForecast, true);
            this.ribbonPageGroupPerformance.ItemLinks.Add(this.barButtonItemAddForecast);
            this.ribbonPageGroupPerformance.ItemLinks.Add(this.barButtonItemDeleteForecast);
            this.ribbonPageGroupPerformance.ItemLinks.Add(this.barButtonItemUpdateForecast);
            this.ribbonPageGroupPerformance.ItemLinks.Add(this.barButtonItemDuplicate);
            this.ribbonPageGroupPerformance.ItemLinks.Add(this.barButtonItemReplace);
            this.ribbonPageGroupPerformance.ItemLinks.Add(this.barButtonItemRestore);
            this.ribbonPageGroupPerformance.ItemLinks.Add(this.barButtonItemFinalize);
            this.ribbonPageGroupPerformance.ItemLinks.Add(this.BarButtonItemHistoricGraphic, true);
            this.ribbonPageGroupPerformance.ItemLinks.Add(this.BarButtonItemHistoryDetails, true);
            this.ribbonPageGroupPerformance.Kind = DevExpress.XtraReports.UserDesigner.XRDesignRibbonPageGroupKind.Edit;
            this.ribbonPageGroupPerformance.Name = "ribbonPageGroupPerformance";
            this.ribbonPageGroupPerformance.ShowCaptionButton = false;
            this.ribbonPageGroupPerformance.Text = "Rendimiento";
            // 
            // ribbonPageGroupGeneralOptMonitor
            // 
            this.ribbonPageGroupGeneralOptMonitor.AllowMinimize = false;
            this.ribbonPageGroupGeneralOptMonitor.AllowTextClipping = false;
            this.ribbonPageGroupGeneralOptMonitor.ItemLinks.Add(this.barButtonItemPrint);
            this.ribbonPageGroupGeneralOptMonitor.ItemLinks.Add(this.barButtonItemSave);
            this.ribbonPageGroupGeneralOptMonitor.ItemLinks.Add(this.BarButtonItemSendMonitor);
            this.ribbonPageGroupGeneralOptMonitor.ItemLinks.Add(this.barButtonItemRefreshGraphic);
            this.ribbonPageGroupGeneralOptMonitor.Name = "ribbonPageGroupGeneralOptMonitor";
            this.ribbonPageGroupGeneralOptMonitor.ShowCaptionButton = false;
            this.ribbonPageGroupGeneralOptMonitor.Text = "Opciones generales";
            // 
            // ForescastForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1270, 787);
            this.Controls.Add(this.RibbonControl);
            this.Controls.Add(this.layoutControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ForescastForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ForescastForm";
            this.Activated += new System.EventHandler(this.ForescastForm_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ForescastForm_FormClosed);
            this.Load += new System.EventHandler(this.ForescastForm_Load);
            this.MdiChildActivate += new System.EventHandler(this.ForescastForm_Activated);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkedListBoxForecastHistory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlForecastDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewForecastDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlForecast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewForecast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkedListBoxForecastUpdates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupForecast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RibbonControl)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.Views.Grid.GridView gridViewForecastDetail;
        internal DevExpress.XtraGrid.Columns.GridColumn gridColumnIndicator;
        internal DevExpress.XtraGrid.Columns.GridColumn gridColumnIndicatorType;
        internal DevExpress.XtraGrid.Columns.GridColumn gridColumnPostivePattern;
        internal DevExpress.XtraGrid.Columns.GridColumn gridColumnLow;
        internal DevExpress.XtraGrid.Columns.GridColumn gridColumnHigh;
        internal DevExpress.XtraGrid.Columns.GridColumn gridColumnMeasureUnit;
        internal DevExpress.XtraEditors.RadioGroup radioGroup1;
        internal DevExpress.XtraGrid.GridControl gridControlForecastDetails;
        internal DevExpress.XtraEditors.DateEdit dateEditTo;
        internal DevExpress.XtraEditors.DateEdit dateEditFrom;
        private DevExpress.XtraEditors.SimpleButton simpleButtonSearch;
        internal DevExpress.XtraBars.Ribbon.RibbonControl RibbonControl;
        private System.Windows.Forms.ImageList imageList;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItemSendMonitor;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemAddForecast;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemDeleteForecast;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemUpdateForecast;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemDuplicate;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemReplace;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemFinalize;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemSend;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemSave;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemPrint;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemRefreshGraphic;
        internal DevExpress.XtraReports.UserDesigner.XRDesignRibbonPage RibbonPageMonitoring;
        internal DevExpress.XtraReports.UserDesigner.XRDesignRibbonPageGroup ribbonPageGroupPerformance;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupGeneralOptMonitor;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemForecast;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItemHistoricGraphic;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItemHistoryDetails;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private DevExpress.XtraEditors.CheckedListBoxControl checkedListBoxForecastHistory;
        private DevExpress.XtraEditors.CheckedListBoxControl checkedListBoxForecastUpdates;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemRestore;
        private GridControlEx gridControlForecast;
        private GridViewEx gridViewForecast;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.Utils.ToolTipController toolTipController1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemFrom;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemFilter;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupForecast;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
    }
}