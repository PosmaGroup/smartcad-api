namespace SmartCadGuiCommon
{
    partial class SupervisorClosedReportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SupervisorClosedReportForm));
            this.richTextBoxCloseMessages = new System.Windows.Forms.RichTextBox();
            this.richTextBoxCloseMessage = new System.Windows.Forms.RichTextBox();
            this.simpleButtonAdd = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlCloseReport = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupCloseMessages = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupNewMessage = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.RibbonControl = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.barButtonItemCopy = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.barButtonItemCut = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.barButtonItemPaste = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.barButtonItemEndReport = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemRedo = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemUndo = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSend = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSave = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemUpdate = new DevExpress.XtraBars.BarButtonItem();
            this.RibbonPageOperation = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPage();
            this.ribbonPageGroupFinalReport = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup();
            this.ribbonPageGroupOptions = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCloseReport)).BeginInit();
            this.layoutControlCloseReport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupCloseMessages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupNewMessage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // richTextBoxCloseMessages
            // 
            this.richTextBoxCloseMessages.BackColor = System.Drawing.Color.White;
            this.richTextBoxCloseMessages.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.richTextBoxCloseMessages.Location = new System.Drawing.Point(6, 24);
            this.richTextBoxCloseMessages.Name = "richTextBoxCloseMessages";
            this.richTextBoxCloseMessages.ShortcutsEnabled = false;
            this.richTextBoxCloseMessages.Size = new System.Drawing.Size(1259, 605);
            this.richTextBoxCloseMessages.TabIndex = 13;
            this.richTextBoxCloseMessages.TabStop = false;
            this.richTextBoxCloseMessages.Text = "";
            this.richTextBoxCloseMessages.ReadOnly = true;
            this.richTextBoxCloseMessages.SelectionChanged += new System.EventHandler(this.richTextBoxCloseMessages_SelectionChanged);
            // 
            // richTextBoxCloseMessage
            // 
            this.richTextBoxCloseMessage.BackColor = System.Drawing.Color.White;
            this.richTextBoxCloseMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.richTextBoxCloseMessage.Location = new System.Drawing.Point(9, 657);
            this.richTextBoxCloseMessage.Name = "richTextBoxCloseMessage";
            this.richTextBoxCloseMessage.ShortcutsEnabled = false;
            this.richTextBoxCloseMessage.Size = new System.Drawing.Size(1155, 118);
            this.richTextBoxCloseMessage.TabIndex = 0;
            this.richTextBoxCloseMessage.Text = "";
            this.richTextBoxCloseMessage.SelectionChanged += new System.EventHandler(this.richTextBoxCloseMessage_SelectionChanged);
            this.richTextBoxCloseMessage.TextChanged += new System.EventHandler(this.richTextBoxCloseMessage_TextChanged);
            // 
            // simpleButtonAdd
            // 
            this.simpleButtonAdd.Enabled = false;
            this.simpleButtonAdd.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonAdd.Image")));
            this.simpleButtonAdd.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonAdd.Location = new System.Drawing.Point(1167, 657);
            this.simpleButtonAdd.Name = "simpleButtonAdd";
            this.simpleButtonAdd.Size = new System.Drawing.Size(95, 118);
            this.simpleButtonAdd.StyleController = this.layoutControlCloseReport;
            this.simpleButtonAdd.TabIndex = 1;
            this.simpleButtonAdd.Text = "simpleButton3";
            this.simpleButtonAdd.Click += new System.EventHandler(this.simpleButtonAdd_Click);
            // 
            // layoutControlCloseReport
            // 
            this.layoutControlCloseReport.AllowCustomizationMenu = false;
            this.layoutControlCloseReport.Appearance.DisabledLayoutGroupCaption.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControlCloseReport.Appearance.DisabledLayoutGroupCaption.Options.UseForeColor = true;
            this.layoutControlCloseReport.Appearance.DisabledLayoutItem.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControlCloseReport.Appearance.DisabledLayoutItem.Options.UseForeColor = true;
            this.layoutControlCloseReport.Controls.Add(this.simpleButtonAdd);
            this.layoutControlCloseReport.Controls.Add(this.richTextBoxCloseMessage);
            this.layoutControlCloseReport.Controls.Add(this.richTextBoxCloseMessages);
            this.layoutControlCloseReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlCloseReport.Location = new System.Drawing.Point(0, 0);
            this.layoutControlCloseReport.Name = "layoutControlCloseReport";
            this.layoutControlCloseReport.Root = this.layoutControlGroup1;
            this.layoutControlCloseReport.Size = new System.Drawing.Size(1270, 787);
            this.layoutControlCloseReport.TabIndex = 7;
            this.layoutControlCloseReport.Text = "layoutControl1";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupCloseMessages});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1270, 787);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroupCloseMessages
            // 
            this.layoutControlGroupCloseMessages.CustomizationFormText = "layoutControlGroupCLoseMessages";
            this.layoutControlGroupCloseMessages.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlGroupNewMessage});
            this.layoutControlGroupCloseMessages.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupCloseMessages.Name = "layoutControlGroupCloseMessages";
            this.layoutControlGroupCloseMessages.Size = new System.Drawing.Size(1268, 785);
            this.layoutControlGroupCloseMessages.Text = "layoutControlGroupCloseMessages";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.richTextBoxCloseMessages;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 5);
            this.layoutControlItem1.Size = new System.Drawing.Size(1262, 612);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroupNewMessage
            // 
            this.layoutControlGroupNewMessage.CustomizationFormText = "layoutControlGroupNewMessage";
            this.layoutControlGroupNewMessage.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem2});
            this.layoutControlGroupNewMessage.Location = new System.Drawing.Point(0, 612);
            this.layoutControlGroupNewMessage.Name = "layoutControlGroupNewMessage";
            this.layoutControlGroupNewMessage.Size = new System.Drawing.Size(1262, 149);
            this.layoutControlGroupNewMessage.Text = "layoutControlGroupNewMessage";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.simpleButtonAdd;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(1158, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(98, 125);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(98, 125);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 5);
            this.layoutControlItem3.Size = new System.Drawing.Size(98, 125);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.richTextBoxCloseMessage;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 5);
            this.layoutControlItem2.Size = new System.Drawing.Size(1158, 125);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // RibbonControl
            // 
            this.RibbonControl.ApplicationIcon = null;
            this.RibbonControl.Dock = System.Windows.Forms.DockStyle.None;
            this.RibbonControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RibbonControl.Images = this.imageList;
            this.RibbonControl.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItemCopy,
            this.barButtonItemCut,
            this.barButtonItemPaste,
            this.barButtonItemEndReport,
            this.barButtonItemRedo,
            this.barButtonItemUndo,
            this.barButtonItemSend,
            this.barButtonItemSave,
            this.barButtonItemPrint,
            this.barButtonItemUpdate});
            this.RibbonControl.ItemsVertAlign = DevExpress.Utils.VertAlignment.Top;
            this.RibbonControl.LargeImages = this.imageList;
            this.RibbonControl.Location = new System.Drawing.Point(406, 6);
            this.RibbonControl.MaxItemId = 251;
            this.RibbonControl.MdiMergeStyle = DevExpress.XtraBars.Ribbon.RibbonMdiMergeStyle.Always;
            this.RibbonControl.Name = "RibbonControl";
            this.RibbonControl.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.RibbonPageOperation});
            this.RibbonControl.SelectedPage = this.RibbonPageOperation;
            this.RibbonControl.ShowToolbarCustomizeItem = false;
            this.RibbonControl.Size = new System.Drawing.Size(350, 115);
            this.RibbonControl.Toolbar.ShowCustomizeItem = false;
            this.RibbonControl.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "logout.gif");
            // 
            // barButtonItemCopy
            // 
            this.barButtonItemCopy.Caption = "Copiar";
            this.barButtonItemCopy.Enabled = false;
            this.barButtonItemCopy.Glyph = global::SmartCadGuiCommon.XRDesignRibbonControllerResources.RibbonUserDesigner_Copy;
            this.barButtonItemCopy.Id = 64;
            this.barButtonItemCopy.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C));
            this.barButtonItemCopy.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemCopy.Name = "barButtonItemCopy";
            this.barButtonItemCopy.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemCopy.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.PrintPreviewBarItemCopy_ItemClick);
            // 
            // barButtonItemCut
            // 
            this.barButtonItemCut.Caption = "Cortar";
            this.barButtonItemCut.Enabled = false;
            this.barButtonItemCut.Glyph = global::SmartCadGuiCommon.XRDesignRibbonControllerResources.RibbonUserDesigner_Cut;
            this.barButtonItemCut.Id = 65;
            this.barButtonItemCut.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X));
            this.barButtonItemCut.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemCut.Name = "barButtonItemCut";
            this.barButtonItemCut.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemCut.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.PrintPreviewBarItemCut_ItemClick);
            // 
            // barButtonItemPaste
            // 
            this.barButtonItemPaste.Caption = "Pegar";
            this.barButtonItemPaste.Enabled = false;
            this.barButtonItemPaste.Glyph = global::SmartCadGuiCommon.XRDesignRibbonControllerResources.RibbonUserDesigner_Paste;
            this.barButtonItemPaste.Id = 79;
            this.barButtonItemPaste.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V));
            this.barButtonItemPaste.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemPaste.Name = "barButtonItemPaste";
            this.barButtonItemPaste.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemPaste.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.PrintPreviewBarItemPaste_ItemClick);
            // 
            // barButtonItemEndReport
            // 
            this.barButtonItemEndReport.Caption = "Finalizar";
            this.barButtonItemEndReport.Enabled = false;
            this.barButtonItemEndReport.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemEndReport.Glyph")));
            this.barButtonItemEndReport.Id = 222;
            this.barButtonItemEndReport.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemEndReport.Name = "barButtonItemEndReport";
            this.barButtonItemEndReport.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemEndReport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemEndReport_ItemClick);
            // 
            // barButtonItemRedo
            // 
            this.barButtonItemRedo.Caption = "Rehacer";
            this.barButtonItemRedo.Enabled = false;
            this.barButtonItemRedo.Glyph = global::SmartCadGuiCommon.XRDesignRibbonControllerResources.RibbonUserDesigner_Redo;
            this.barButtonItemRedo.Id = 223;
            this.barButtonItemRedo.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y));
            this.barButtonItemRedo.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemRedo.Name = "barButtonItemRedo";
            this.barButtonItemRedo.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemRedo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemRedo_ItemClick);
            // 
            // barButtonItemUndo
            // 
            this.barButtonItemUndo.Caption = "Deshacer";
            this.barButtonItemUndo.Enabled = false;
            this.barButtonItemUndo.Glyph = global::SmartCadGuiCommon.XRDesignRibbonControllerResources.RibbonUserDesigner_Undo;
            this.barButtonItemUndo.Id = 224;
            this.barButtonItemUndo.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z));
            this.barButtonItemUndo.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemUndo.Name = "barButtonItemUndo";
            this.barButtonItemUndo.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemUndo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemUndo_ItemClick);
            // 
            // barButtonItemSend
            // 
            this.barButtonItemSend.Enabled = false;
            this.barButtonItemSend.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemSend.Glyph")));
            this.barButtonItemSend.Id = 229;
            this.barButtonItemSend.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Enter));
            this.barButtonItemSend.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemSend.Name = "barButtonItemSend";
            this.barButtonItemSend.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemSend.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemSend_ItemClick);
            // 
            // barButtonItemSave
            // 
            this.barButtonItemSave.Enabled = false;
            this.barButtonItemSave.Glyph = global::SmartCadGuiCommon.PrintRibbonControllerResources.RibbonPrintPreview_Save;
            this.barButtonItemSave.Id = 230;
            this.barButtonItemSave.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S));
            this.barButtonItemSave.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemSave.Name = "barButtonItemSave";
            this.barButtonItemSave.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemSave_ItemClick);
            // 
            // barButtonItemPrint
            // 
            this.barButtonItemPrint.Enabled = false;
            this.barButtonItemPrint.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.Glyph")));
            this.barButtonItemPrint.Id = 231;
            this.barButtonItemPrint.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P));
            this.barButtonItemPrint.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemPrint.Name = "barButtonItemPrint";
            this.barButtonItemPrint.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPrint_ItemClick);
            // 
            // barButtonItemUpdate
            // 
            this.barButtonItemUpdate.Enabled = false;
            this.barButtonItemUpdate.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemUpdate.Glyph")));
            this.barButtonItemUpdate.Id = 249;
            this.barButtonItemUpdate.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F5);
            this.barButtonItemUpdate.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemUpdate.Name = "barButtonItemUpdate";
            this.barButtonItemUpdate.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // RibbonPageOperation
            // 
            this.RibbonPageOperation.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupFinalReport,
            this.ribbonPageGroupOptions});
            this.RibbonPageOperation.Name = "RibbonPageOperation";
            this.RibbonPageOperation.Text = "Operacion";
            // 
            // ribbonPageGroupFinalReport
            // 
            this.ribbonPageGroupFinalReport.AllowMinimize = false;
            this.ribbonPageGroupFinalReport.AllowTextClipping = false;
            this.ribbonPageGroupFinalReport.ItemLinks.Add(this.barButtonItemEndReport);
            this.ribbonPageGroupFinalReport.ItemLinks.Add(this.barButtonItemRedo);
            this.ribbonPageGroupFinalReport.ItemLinks.Add(this.barButtonItemUndo);
            this.ribbonPageGroupFinalReport.ItemLinks.Add(this.barButtonItemCut);
            this.ribbonPageGroupFinalReport.ItemLinks.Add(this.barButtonItemCopy);
            this.ribbonPageGroupFinalReport.ItemLinks.Add(this.barButtonItemPaste);
            this.ribbonPageGroupFinalReport.Kind = DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroupKind.Navigation;
            this.ribbonPageGroupFinalReport.Name = "ribbonPageGroupFinalReport";
            this.ribbonPageGroupFinalReport.ShowCaptionButton = false;
            this.ribbonPageGroupFinalReport.Text = "Reporte de cierre";
            // 
            // ribbonPageGroupOptions
            // 
            this.ribbonPageGroupOptions.AllowMinimize = false;
            this.ribbonPageGroupOptions.AllowTextClipping = false;
            this.ribbonPageGroupOptions.ItemLinks.Add(this.barButtonItemPrint);
            this.ribbonPageGroupOptions.ItemLinks.Add(this.barButtonItemSave);
            this.ribbonPageGroupOptions.ItemLinks.Add(this.barButtonItemSend);
            this.ribbonPageGroupOptions.ItemLinks.Add(this.barButtonItemUpdate);
            this.ribbonPageGroupOptions.Name = "ribbonPageGroupOptions";
            this.ribbonPageGroupOptions.ShowCaptionButton = false;
            this.ribbonPageGroupOptions.Text = "Opciones generales";
            // 
            // SupervisorClosedReportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1270, 787);
            this.Controls.Add(this.RibbonControl);
            this.Controls.Add(this.layoutControlCloseReport);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SupervisorClosedReportForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SupervisorClosedReportForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SupervisorClosedReportForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCloseReport)).EndInit();
            this.layoutControlCloseReport.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupCloseMessages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupNewMessage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButtonAdd;
        private System.Windows.Forms.RichTextBox richTextBoxCloseMessage;
        private System.Windows.Forms.RichTextBox richTextBoxCloseMessages;
        internal DevExpress.XtraBars.Ribbon.RibbonControl RibbonControl;
        private System.Windows.Forms.ImageList imageList;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem barButtonItemCopy;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem barButtonItemCut;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem barButtonItemPaste;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPage RibbonPageOperation;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup ribbonPageGroupFinalReport;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemEndReport;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemRedo;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemUndo;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemSend;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemSave;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemPrint;
        internal DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupOptions;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemUpdate;
        private DevExpress.XtraLayout.LayoutControl layoutControlCloseReport;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupCloseMessages;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupNewMessage;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;

    }
}
