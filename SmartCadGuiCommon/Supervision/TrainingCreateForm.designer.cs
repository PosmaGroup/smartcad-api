using SmartCadControls.Controls;
namespace SmartCadGuiCommon
{
    partial class TrainingCreateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonExOk = new DevExpress.XtraEditors.SimpleButton();
            this.TrainingCreateFormConvertedLayout = new DevExpress.XtraLayout.LayoutControl();
            this.textBoxExLink = new TextBoxEx();
            this.textBoxExPhone = new TextBoxEx();
            this.buttonExCancel = new DevExpress.XtraEditors.SimpleButton();
            this.textBoxExContact = new TextBoxEx();
            this.textBoxExContent = new TextBoxEx();
            this.textBoxExObj = new TextBoxEx();
            this.textBoxExName = new TextBoxEx();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemOk = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupTrainingCourse = new DevExpress.XtraLayout.LayoutControlGroup();
            this.textBoxExNameitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.textBoxExObjitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.textBoxExContentitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.textBoxExContactitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.textBoxExPhoneitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.textBoxExLinkitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutConverter1 = new DevExpress.XtraLayout.Converter.LayoutConverter(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.TrainingCreateFormConvertedLayout)).BeginInit();
            this.TrainingCreateFormConvertedLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupTrainingCourse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxExNameitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxExObjitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxExContentitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxExContactitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxExPhoneitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxExLinkitem)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonExOk
            // 
            this.buttonExOk.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExOk.Appearance.Options.UseFont = true;
            this.buttonExOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonExOk.Location = new System.Drawing.Point(277, 302);
            this.buttonExOk.Name = "buttonExOk";
            this.buttonExOk.Size = new System.Drawing.Size(75, 25);
            this.buttonExOk.StyleController = this.TrainingCreateFormConvertedLayout;
            this.buttonExOk.TabIndex = 2;
            this.buttonExOk.Text = "Accept";
            this.buttonExOk.Click += new System.EventHandler(this.buttonExOk_Click);
            // 
            // TrainingCreateFormConvertedLayout
            // 
            this.TrainingCreateFormConvertedLayout.AllowCustomizationMenu = false;
            this.TrainingCreateFormConvertedLayout.Appearance.DisabledLayoutGroupCaption.ForeColor = System.Drawing.SystemColors.GrayText;
            this.TrainingCreateFormConvertedLayout.Appearance.DisabledLayoutGroupCaption.Options.UseForeColor = true;
            this.TrainingCreateFormConvertedLayout.Appearance.DisabledLayoutItem.ForeColor = System.Drawing.SystemColors.GrayText;
            this.TrainingCreateFormConvertedLayout.Appearance.DisabledLayoutItem.Options.UseForeColor = true;
            this.TrainingCreateFormConvertedLayout.Controls.Add(this.textBoxExLink);
            this.TrainingCreateFormConvertedLayout.Controls.Add(this.textBoxExPhone);
            this.TrainingCreateFormConvertedLayout.Controls.Add(this.buttonExCancel);
            this.TrainingCreateFormConvertedLayout.Controls.Add(this.textBoxExContact);
            this.TrainingCreateFormConvertedLayout.Controls.Add(this.buttonExOk);
            this.TrainingCreateFormConvertedLayout.Controls.Add(this.textBoxExContent);
            this.TrainingCreateFormConvertedLayout.Controls.Add(this.textBoxExObj);
            this.TrainingCreateFormConvertedLayout.Controls.Add(this.textBoxExName);
            this.TrainingCreateFormConvertedLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TrainingCreateFormConvertedLayout.Location = new System.Drawing.Point(0, 0);
            this.TrainingCreateFormConvertedLayout.Name = "TrainingCreateFormConvertedLayout";
            this.TrainingCreateFormConvertedLayout.Root = this.layoutControlGroup1;
            this.TrainingCreateFormConvertedLayout.Size = new System.Drawing.Size(444, 333);
            this.TrainingCreateFormConvertedLayout.TabIndex = 12;
            // 
            // textBoxExLink
            // 
            this.textBoxExLink.AllowsLetters = true;
            this.textBoxExLink.AllowsNumbers = true;
            this.textBoxExLink.AllowsPunctuation = true;
            this.textBoxExLink.AllowsSeparators = true;
            this.textBoxExLink.AllowsSymbols = true;
            this.textBoxExLink.AllowsWhiteSpaces = false;
            this.textBoxExLink.ExtraAllowedChars = "";
            this.textBoxExLink.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExLink.Location = new System.Drawing.Point(124, 268);
            this.textBoxExLink.MaxLength = 100;
            this.textBoxExLink.Name = "textBoxExLink";
            this.textBoxExLink.NonAllowedCharacters = "";
            this.textBoxExLink.RegularExpresion = "";
            this.textBoxExLink.Size = new System.Drawing.Size(311, 20);
            this.textBoxExLink.TabIndex = 11;
            // 
            // textBoxExPhone
            // 
            this.textBoxExPhone.AllowsLetters = false;
            this.textBoxExPhone.AllowsNumbers = true;
            this.textBoxExPhone.AllowsPunctuation = false;
            this.textBoxExPhone.AllowsSeparators = false;
            this.textBoxExPhone.AllowsSymbols = false;
            this.textBoxExPhone.AllowsWhiteSpaces = false;
            this.textBoxExPhone.ExtraAllowedChars = "-,/";
            this.textBoxExPhone.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExPhone.Location = new System.Drawing.Point(124, 237);
            this.textBoxExPhone.MaxLength = 17;
            this.textBoxExPhone.Name = "textBoxExPhone";
            this.textBoxExPhone.NonAllowedCharacters = "";
            this.textBoxExPhone.RegularExpresion = "";
            this.textBoxExPhone.Size = new System.Drawing.Size(311, 20);
            this.textBoxExPhone.TabIndex = 9;
            this.textBoxExPhone.TextChanged += new System.EventHandler(this.ButtonsActivation);
            // 
            // buttonExCancel
            // 
            this.buttonExCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExCancel.Appearance.Options.UseFont = true;
            this.buttonExCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonExCancel.Location = new System.Drawing.Point(363, 302);
            this.buttonExCancel.Name = "buttonExCancel";
            this.buttonExCancel.Size = new System.Drawing.Size(75, 25);
            this.buttonExCancel.StyleController = this.TrainingCreateFormConvertedLayout;
            this.buttonExCancel.TabIndex = 3;
            this.buttonExCancel.Text = "Cancelar";
            // 
            // textBoxExContact
            // 
            this.textBoxExContact.AllowsLetters = true;
            this.textBoxExContact.AllowsNumbers = true;
            this.textBoxExContact.AllowsPunctuation = true;
            this.textBoxExContact.AllowsSeparators = true;
            this.textBoxExContact.AllowsSymbols = true;
            this.textBoxExContact.AllowsWhiteSpaces = true;
            this.textBoxExContact.ExtraAllowedChars = "";
            this.textBoxExContact.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExContact.Location = new System.Drawing.Point(124, 206);
            this.textBoxExContact.MaxLength = 40;
            this.textBoxExContact.Name = "textBoxExContact";
            this.textBoxExContact.NonAllowedCharacters = "";
            this.textBoxExContact.RegularExpresion = "";
            this.textBoxExContact.Size = new System.Drawing.Size(311, 20);
            this.textBoxExContact.TabIndex = 7;
            this.textBoxExContact.TextChanged += new System.EventHandler(this.ButtonsActivation);
            // 
            // textBoxExContent
            // 
            this.textBoxExContent.AcceptsReturn = true;
            this.textBoxExContent.AllowsLetters = true;
            this.textBoxExContent.AllowsNumbers = true;
            this.textBoxExContent.AllowsPunctuation = true;
            this.textBoxExContent.AllowsSeparators = true;
            this.textBoxExContent.AllowsSymbols = true;
            this.textBoxExContent.AllowsWhiteSpaces = true;
            this.textBoxExContent.ExtraAllowedChars = "";
            this.textBoxExContent.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExContent.Location = new System.Drawing.Point(124, 132);
            this.textBoxExContent.MaxLength = 200;
            this.textBoxExContent.Multiline = true;
            this.textBoxExContent.Name = "textBoxExContent";
            this.textBoxExContent.NonAllowedCharacters = "";
            this.textBoxExContent.RegularExpresion = "";
            this.textBoxExContent.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxExContent.Size = new System.Drawing.Size(311, 63);
            this.textBoxExContent.TabIndex = 5;
            this.textBoxExContent.TextChanged += new System.EventHandler(this.ButtonsActivation);
            // 
            // textBoxExObj
            // 
            this.textBoxExObj.AcceptsReturn = true;
            this.textBoxExObj.AllowsLetters = true;
            this.textBoxExObj.AllowsNumbers = true;
            this.textBoxExObj.AllowsPunctuation = true;
            this.textBoxExObj.AllowsSeparators = true;
            this.textBoxExObj.AllowsSymbols = true;
            this.textBoxExObj.AllowsWhiteSpaces = true;
            this.textBoxExObj.ExtraAllowedChars = "";
            this.textBoxExObj.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExObj.Location = new System.Drawing.Point(124, 59);
            this.textBoxExObj.MaxLength = 200;
            this.textBoxExObj.Multiline = true;
            this.textBoxExObj.Name = "textBoxExObj";
            this.textBoxExObj.NonAllowedCharacters = "";
            this.textBoxExObj.RegularExpresion = "";
            this.textBoxExObj.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxExObj.Size = new System.Drawing.Size(311, 62);
            this.textBoxExObj.TabIndex = 3;
            this.textBoxExObj.TextChanged += new System.EventHandler(this.ButtonsActivation);
            // 
            // textBoxExName
            // 
            this.textBoxExName.AllowsLetters = true;
            this.textBoxExName.AllowsNumbers = true;
            this.textBoxExName.AllowsPunctuation = true;
            this.textBoxExName.AllowsSeparators = true;
            this.textBoxExName.AllowsSymbols = true;
            this.textBoxExName.AllowsWhiteSpaces = true;
            this.textBoxExName.ExtraAllowedChars = "";
            this.textBoxExName.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExName.Location = new System.Drawing.Point(124, 28);
            this.textBoxExName.MaxLength = 40;
            this.textBoxExName.Name = "textBoxExName";
            this.textBoxExName.NonAllowedCharacters = "";
            this.textBoxExName.RegularExpresion = "";
            this.textBoxExName.Size = new System.Drawing.Size(311, 20);
            this.textBoxExName.TabIndex = 1;
            this.textBoxExName.TextChanged += new System.EventHandler(this.ButtonsActivation);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItemOk,
            this.emptySpaceItem1,
            this.layoutControlGroupTrainingCourse});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(444, 333);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.buttonExCancel;
            this.layoutControlItem3.CustomizationFormText = "buttonExCancelitem";
            this.layoutControlItem3.Location = new System.Drawing.Point(356, 295);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem3.Name = "buttonExCancelitem";
            this.layoutControlItem3.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "buttonExCancelitem";
            this.layoutControlItem3.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItemOk
            // 
            this.layoutControlItemOk.Control = this.buttonExOk;
            this.layoutControlItemOk.CustomizationFormText = "buttonExOkitem";
            this.layoutControlItemOk.Location = new System.Drawing.Point(270, 295);
            this.layoutControlItemOk.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItemOk.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItemOk.Name = "layoutControlItemOk";
            this.layoutControlItemOk.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItemOk.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemOk.Text = "layoutControlItemOk";
            this.layoutControlItemOk.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemOk.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemOk.TextToControlDistance = 0;
            this.layoutControlItemOk.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 295);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(270, 36);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupTrainingCourse
            // 
            this.layoutControlGroupTrainingCourse.CustomizationFormText = "layoutControlGroupTrainingCourse";
            this.layoutControlGroupTrainingCourse.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.textBoxExNameitem,
            this.textBoxExObjitem,
            this.textBoxExContentitem,
            this.textBoxExContactitem,
            this.textBoxExPhoneitem,
            this.textBoxExLinkitem});
            this.layoutControlGroupTrainingCourse.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupTrainingCourse.Name = "layoutControlGroupTrainingCourse";
            this.layoutControlGroupTrainingCourse.Size = new System.Drawing.Size(442, 295);
            this.layoutControlGroupTrainingCourse.Text = "layoutControlGroupTrainingCourse";
            // 
            // textBoxExNameitem
            // 
            this.textBoxExNameitem.Control = this.textBoxExName;
            this.textBoxExNameitem.CustomizationFormText = "textBoxExNameitem";
            this.textBoxExNameitem.Location = new System.Drawing.Point(0, 0);
            this.textBoxExNameitem.Name = "textBoxExNameitem";
            this.textBoxExNameitem.Size = new System.Drawing.Size(436, 31);
            this.textBoxExNameitem.Text = "textBoxExNameitem";
            this.textBoxExNameitem.TextLocation = DevExpress.Utils.Locations.Left;
            this.textBoxExNameitem.TextSize = new System.Drawing.Size(109, 20);
            // 
            // textBoxExObjitem
            // 
            this.textBoxExObjitem.Control = this.textBoxExObj;
            this.textBoxExObjitem.CustomizationFormText = "textBoxExObjitem";
            this.textBoxExObjitem.Location = new System.Drawing.Point(0, 31);
            this.textBoxExObjitem.MinSize = new System.Drawing.Size(145, 31);
            this.textBoxExObjitem.Name = "textBoxExObjitem";
            this.textBoxExObjitem.Size = new System.Drawing.Size(436, 73);
            this.textBoxExObjitem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.textBoxExObjitem.Text = "textBoxExObjitem";
            this.textBoxExObjitem.TextLocation = DevExpress.Utils.Locations.Left;
            this.textBoxExObjitem.TextSize = new System.Drawing.Size(109, 20);
            // 
            // textBoxExContentitem
            // 
            this.textBoxExContentitem.Control = this.textBoxExContent;
            this.textBoxExContentitem.CustomizationFormText = "textBoxExContentitem";
            this.textBoxExContentitem.Location = new System.Drawing.Point(0, 104);
            this.textBoxExContentitem.MinSize = new System.Drawing.Size(145, 31);
            this.textBoxExContentitem.Name = "textBoxExContentitem";
            this.textBoxExContentitem.Size = new System.Drawing.Size(436, 74);
            this.textBoxExContentitem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.textBoxExContentitem.Text = "textBoxExContentitem";
            this.textBoxExContentitem.TextLocation = DevExpress.Utils.Locations.Left;
            this.textBoxExContentitem.TextSize = new System.Drawing.Size(109, 20);
            // 
            // textBoxExContactitem
            // 
            this.textBoxExContactitem.Control = this.textBoxExContact;
            this.textBoxExContactitem.CustomizationFormText = "textBoxExContactitem";
            this.textBoxExContactitem.Location = new System.Drawing.Point(0, 178);
            this.textBoxExContactitem.Name = "textBoxExContactitem";
            this.textBoxExContactitem.Size = new System.Drawing.Size(436, 31);
            this.textBoxExContactitem.Text = "textBoxExContactitem";
            this.textBoxExContactitem.TextLocation = DevExpress.Utils.Locations.Left;
            this.textBoxExContactitem.TextSize = new System.Drawing.Size(109, 20);
            // 
            // textBoxExPhoneitem
            // 
            this.textBoxExPhoneitem.Control = this.textBoxExPhone;
            this.textBoxExPhoneitem.CustomizationFormText = "textBoxExPhoneitem";
            this.textBoxExPhoneitem.Location = new System.Drawing.Point(0, 209);
            this.textBoxExPhoneitem.Name = "textBoxExPhoneitem";
            this.textBoxExPhoneitem.Size = new System.Drawing.Size(436, 31);
            this.textBoxExPhoneitem.Text = "textBoxExPhoneitem";
            this.textBoxExPhoneitem.TextLocation = DevExpress.Utils.Locations.Left;
            this.textBoxExPhoneitem.TextSize = new System.Drawing.Size(109, 20);
            // 
            // textBoxExLinkitem
            // 
            this.textBoxExLinkitem.Control = this.textBoxExLink;
            this.textBoxExLinkitem.CustomizationFormText = "textBoxExLinkitem";
            this.textBoxExLinkitem.Location = new System.Drawing.Point(0, 240);
            this.textBoxExLinkitem.Name = "textBoxExLinkitem";
            this.textBoxExLinkitem.Size = new System.Drawing.Size(436, 31);
            this.textBoxExLinkitem.Text = "textBoxExLinkitem";
            this.textBoxExLinkitem.TextLocation = DevExpress.Utils.Locations.Left;
            this.textBoxExLinkitem.TextSize = new System.Drawing.Size(109, 20);
            // 
            // TrainingCreateForm
            // 
            this.AcceptButton = this.buttonExOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonExCancel;
            this.ClientSize = new System.Drawing.Size(444, 333);
            this.Controls.Add(this.TrainingCreateFormConvertedLayout);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(450, 365);
            this.Name = "TrainingCreateForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TrainingCreateForm";
            ((System.ComponentModel.ISupportInitialize)(this.TrainingCreateFormConvertedLayout)).EndInit();
            this.TrainingCreateFormConvertedLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupTrainingCourse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxExNameitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxExObjitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxExContentitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxExContactitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxExPhoneitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxExLinkitem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton buttonExOk;
        private DevExpress.XtraEditors.SimpleButton buttonExCancel;
        private TextBoxEx textBoxExLink;
        private TextBoxEx textBoxExPhone;
        private TextBoxEx textBoxExContact;
        private TextBoxEx textBoxExContent;
        private TextBoxEx textBoxExName;
        private TextBoxEx textBoxExObj;
        private DevExpress.XtraLayout.LayoutControl TrainingCreateFormConvertedLayout;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem textBoxExLinkitem;
        private DevExpress.XtraLayout.LayoutControlItem textBoxExPhoneitem;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem textBoxExContactitem;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemOk;
        private DevExpress.XtraLayout.LayoutControlItem textBoxExContentitem;
        private DevExpress.XtraLayout.LayoutControlItem textBoxExObjitem;
        private DevExpress.XtraLayout.LayoutControlItem textBoxExNameitem;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.Converter.LayoutConverter layoutConverter1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupTrainingCourse;
    }
}
