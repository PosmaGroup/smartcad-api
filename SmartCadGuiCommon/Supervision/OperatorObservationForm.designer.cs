namespace SmartCadGuiCommon
{
    partial class OperatorObservationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OperatorObservationForm));
            this.buttonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.buttonOK = new DevExpress.XtraEditors.SimpleButton();
            this.textBoxObservation = new DevExpress.XtraEditors.MemoEdit();
            this.comboBoxObservationTypes = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutControlObs = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemObservation = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxObservation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxObservationTypes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlObs)).BeginInit();
            this.layoutControlObs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemObservation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(261, 155);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 25);
            this.buttonCancel.StyleController = this.layoutControlObs;
            this.buttonCancel.TabIndex = 9;
            this.buttonCancel.Text = "Cancelar";
            // 
            // buttonOK
            // 
            this.buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOK.Location = new System.Drawing.Point(175, 155);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 25);
            this.buttonOK.StyleController = this.layoutControlObs;
            this.buttonOK.TabIndex = 8;
            this.buttonOK.Text = "Accept";
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // textBoxObservation
            // 
            this.textBoxObservation.EditValue = "";
            this.textBoxObservation.Location = new System.Drawing.Point(7, 63);
            this.textBoxObservation.Name = "textBoxObservation";
            this.textBoxObservation.Size = new System.Drawing.Size(329, 81);
            this.textBoxObservation.StyleController = this.layoutControlObs;
            this.textBoxObservation.TabIndex = 7;
            this.textBoxObservation.EditValueChanged += new System.EventHandler(this.OperatorObservationParametersChange);
            this.textBoxObservation.TextChanged += new System.EventHandler(this.OperatorObservationParametersChange);
            // 
            // comboBoxObservationTypes
            // 
            this.comboBoxObservationTypes.Location = new System.Drawing.Point(158, 7);
            this.comboBoxObservationTypes.Name = "comboBoxObservationTypes";
            this.comboBoxObservationTypes.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxObservationTypes.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxObservationTypes.Size = new System.Drawing.Size(178, 20);
            this.comboBoxObservationTypes.StyleController = this.layoutControlObs;
            this.comboBoxObservationTypes.TabIndex = 6;
            this.comboBoxObservationTypes.SelectedValueChanged += new System.EventHandler(this.comboBoxObservationTypes_SelectionChangeCommitted);
            // 
            // layoutControlObs
            // 
            this.layoutControlObs.AllowCustomizationMenu = false;
            this.layoutControlObs.Appearance.DisabledLayoutGroupCaption.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControlObs.Appearance.DisabledLayoutGroupCaption.Options.UseForeColor = true;
            this.layoutControlObs.Appearance.DisabledLayoutItem.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControlObs.Appearance.DisabledLayoutItem.Options.UseForeColor = true;
            this.layoutControlObs.Controls.Add(this.buttonCancel);
            this.layoutControlObs.Controls.Add(this.comboBoxObservationTypes);
            this.layoutControlObs.Controls.Add(this.buttonOK);
            this.layoutControlObs.Controls.Add(this.textBoxObservation);
            this.layoutControlObs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlObs.Location = new System.Drawing.Point(0, 0);
            this.layoutControlObs.Name = "layoutControlObs";
            this.layoutControlObs.Root = this.layoutControlGroup1;
            this.layoutControlObs.Size = new System.Drawing.Size(342, 186);
            this.layoutControlObs.TabIndex = 8;
            this.layoutControlObs.Text = "layoutControl1";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemType,
            this.layoutControlItemObservation,
            this.layoutControlItem2,
            this.layoutControlItem1,
            this.emptySpaceItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(342, 186);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItemType
            // 
            this.layoutControlItemType.Control = this.comboBoxObservationTypes;
            this.layoutControlItemType.CustomizationFormText = "layoutControlItemType";
            this.layoutControlItemType.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemType.Name = "layoutControlItemType";
            this.layoutControlItemType.Size = new System.Drawing.Size(340, 31);
            this.layoutControlItemType.Text = "layoutControlItemType";
            this.layoutControlItemType.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemType.TextSize = new System.Drawing.Size(146, 20);
            // 
            // layoutControlItemObservation
            // 
            this.layoutControlItemObservation.Control = this.textBoxObservation;
            this.layoutControlItemObservation.CustomizationFormText = "layoutControlItemObservation";
            this.layoutControlItemObservation.Location = new System.Drawing.Point(0, 31);
            this.layoutControlItemObservation.Name = "layoutControlItemObservation";
            this.layoutControlItemObservation.Size = new System.Drawing.Size(340, 117);
            this.layoutControlItemObservation.Text = "layoutControlItemObservation";
            this.layoutControlItemObservation.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItemObservation.TextSize = new System.Drawing.Size(146, 20);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.buttonOK;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(168, 148);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.buttonCancel;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(254, 148);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 148);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(168, 36);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // OperatorObservationForm
            // 
            this.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(342, 186);
            this.Controls.Add(this.layoutControlObs);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(350, 220);
            this.Name = "OperatorObservationForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Agregar observacion";
            this.Load += new System.EventHandler(this.OperatorObservationForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.textBoxObservation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxObservationTypes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlObs)).EndInit();
            this.layoutControlObs.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemObservation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.ComboBoxEdit comboBoxObservationTypes;
        private DevExpress.XtraEditors.MemoEdit textBoxObservation;
        private DevExpress.XtraEditors.SimpleButton buttonCancel;
        private DevExpress.XtraEditors.SimpleButton buttonOK;
        private DevExpress.XtraLayout.LayoutControl layoutControlObs;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemObservation;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
    }
}