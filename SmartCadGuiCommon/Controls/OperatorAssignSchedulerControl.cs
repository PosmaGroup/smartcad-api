using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraScheduler;
using System.Collections;
using DevExpress.Utils.Menu;
using Smartmatic.SmartCad.Service;
using DevExpress.Utils;
using System.Linq;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadCore.Core;


namespace SmartCadGuiCommon.Controls
{
    public partial class OperatorAssignSchedulerControl : DevExpress.XtraEditors.XtraUserControl
    {

        private int selectedOperatorCode;
        private int selectedWorkShiftCode;
        BindingList<Appointment> listApp = new BindingList<Appointment>();
        private int filter;
        private bool clean = false;
        private bool isOperatorView = true;
        private bool isOperatorSelected = false;
        private Dictionary<string, List<OperatorAssignClientData>> listAssignments = new Dictionary<string, List<OperatorAssignClientData>>();
        private Dictionary<int, OperatorClientData> listOperators = new Dictionary<int, OperatorClientData>();
        private bool isEnabledRemove = false;
        private bool isEnabledAssign = false; 

        public OperatorAssignSchedulerControl()
        {

            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo(System.Globalization.CultureInfo.CurrentCulture.Name);
            System.Globalization.DateTimeFormatInfo format = new System.Globalization.DateTimeFormatInfo();
            format.ShortTimePattern = "HH:mm";
            ci.DateTimeFormat = format;
            System.Threading.Thread.CurrentThread.CurrentCulture = ci;

            InitializeComponent();
            LoadLanguage();
            schedulerControlDefault.Start = DateTime.Now;
        
            LoadLegends();
           
        }

        public Appointment SelectedAppointment 
        {
            get {
                if (schedulerControlDefault.SelectedAppointments.Count > 0)
                        return schedulerControlDefault.SelectedAppointments[0];
                else
                    return null;
            }
           
        }

        public bool IsOperatorView
        {
            get
            {                
                return isOperatorView;
            }
            set 
            {
                isOperatorView = value;
            }
        }

        public bool IsOperatorSelected
        {
            get
            {
                return isOperatorSelected;
            }
            set
            {
                isOperatorSelected = value;
            }
        }

        public bool EnabledRemove
        {
            get
            {
                return isEnabledRemove;
            }
            set
            {
                isEnabledRemove = value;
            }
        }

        public bool EnabledAssign
        {
            get
            {
                return isEnabledAssign;
            }
            set
            {
                isEnabledAssign = value;
            }
        }

        private void LoadLegends()
        {
            Bitmap image = new Bitmap(16, 16, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            Color notSupervised = schedulerControlDefault.Storage.Appointments.Labels[0].Color;
            Color partially = schedulerControlDefault.Storage.Appointments.Labels[1].Color;
            Color totally = schedulerControlDefault.Storage.Appointments.Labels[2].Color;
            Graphics g = Graphics.FromImage(image);
            g.FillRectangle(new SolidBrush(notSupervised), 0, 0, 16, 16);
            g.DrawRectangle(new Pen(Color.Black), 0, 0, 15, 15);
            this.labelControlNotSupervised.Appearance.Image = image;
            this.labelControlNotSupervised.ImageAlignToText = ImageAlignToText.LeftCenter;
            g.Dispose();


            Bitmap image2 = new Bitmap(16, 16, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            g = Graphics.FromImage(image2);
            g.FillRectangle(new SolidBrush(partially), 0, 0, 16, 16);
            g.DrawRectangle(new Pen(Color.Black), 0, 0, 15, 15);
            this.labelControlPartially.Appearance.Image = image2;
            this.labelControlPartially.ImageAlignToText = ImageAlignToText.LeftCenter;
            g.Dispose();



            Bitmap image3 = new Bitmap(16, 16, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            g = Graphics.FromImage(image3);
            g.FillRectangle(new SolidBrush(totally), 0, 0, 16, 16);
            g.DrawRectangle(new Pen(Color.Black), 0, 0, 15, 15);
            this.labelControlTotally.Appearance.Image = image3;
            this.labelControlTotally.ImageAlignToText = ImageAlignToText.LeftCenter;
            g.Dispose();

        }


        public void CreateAppointments(OperatorClientData oper, WorkShiftVariationClientData wsv, WorkShiftScheduleVariationClientData schedule, List<OperatorAssignClientData> assignList, int howIsSupervised, bool isSupervisor)
        {            
            Appointment app = this.schedulerControlDefault.Storage.CreateAppointment(AppointmentType.Normal);
            app.Start = schedule.Start;
            app.End = schedule.End;
            
            app.Subject = wsv.Name;
            app.CustomFields["OperatorCode"] = oper.Code;
            app.CustomFields["WorkShiftCode"] = wsv.Code;
            app.CustomFields["ScheduleCode"] = schedule.Code;
            app.CustomFields["IsSupervisor"] = isSupervisor;

            if (wsv.Type == WorkShiftVariationClientData.WorkShiftType.WorkShift)
                app.StatusId = 0;
            else
                app.StatusId = 1;

            app.LabelId = howIsSupervised;

            listApp.Add(app);
          
            if (listAssignments.ContainsKey(oper.Code + "|" + schedule.Code) == false)
                listAssignments.Add(oper.Code + "|" + schedule.Code, assignList);
       
        }

        public void FillListOperators(IList list)
        {
            foreach (OperatorClientData oper in list)
            {
                if (listOperators.ContainsKey(oper.Code) == false)
                    listOperators.Add(oper.Code, oper);
            }
        }


        public void LoadAppointments()
        {
            schedulerControlDefault.Storage.BeginUpdate();
            schedulerControlDefault.Storage.Appointments.AddRange(listApp.ToArray());
            schedulerControlDefault.Storage.EndUpdate();
        }


        public void CreateAppointmentByWorkShift(OperatorClientData oper, WorkShiftVariationClientData wsv, WorkShiftScheduleVariationClientData schedule, List<OperatorAssignClientData> assignList, int howIsSupervised, bool isSupervisor) 
        {
            Appointment app = this.schedulerControlDefault.Storage.CreateAppointment(AppointmentType.Normal);
            app.Start = schedule.Start;
            app.End = schedule.End;
            app.Subject = wsv.Name;
            app.CustomFields["OperatorCode"] = oper.Code;
            app.CustomFields["WorkShiftCode"] = wsv.Code;
            app.CustomFields["ScheduleCode"] = schedule.Code;
            app.CustomFields["IsSupervisor"] = isSupervisor;
            
            if (wsv.Type == WorkShiftVariationClientData.WorkShiftType.WorkShift)
                app.StatusId = 0;
            else
                app.StatusId = 1;

            app.LabelId = howIsSupervised;

            schedulerControlDefault.Storage.Appointments.Add(app);
           
            if (listAssignments.ContainsKey(oper.Code + "|" + schedule.Code) == false)
               listAssignments.Add(oper.Code + "|" + schedule.Code, assignList);

        }

        public int AppointmentsByOperator(int operCode)
        {
            List<Appointment> appList = schedulerControlDefault.Storage.Appointments.Items.Where(app => (int)app.CustomFields["OperatorCode"] == operCode).ToList();
            return appList.Count ;
        }

        public void RemoveAppointmentByWorkShift(int wsCode)
        {           
            List<Appointment> appToDelete = schedulerControlDefault.Storage.Appointments.Items.Where(app => (int)app.CustomFields["WorkShiftCode"] == wsCode).ToList();
            string key = string.Empty;
            schedulerControlDefault.BeginUpdate();
            foreach (Appointment app in appToDelete)
            {               
                key = (int)app.CustomFields["OperatorCode"] + "|" + (int)app.CustomFields["ScheduleCode"];
                listAssignments.Remove(key);
                schedulerControlDefault.Storage.Appointments.Remove(app);
            }
            schedulerControlDefault.EndUpdate();
        }

        public void RemoveAppointmentByOperator(int operCode)
        {
            List<Appointment> appToDelete = schedulerControlDefault.Storage.Appointments.Items.Where(app => (int)app.CustomFields["OperatorCode"] == operCode).ToList();
            string key = string.Empty;
            schedulerControlDefault.BeginUpdate();
            foreach (Appointment app in appToDelete)
            {
                key = (int)app.CustomFields["OperatorCode"] + "|" + (int)app.CustomFields["ScheduleCode"];
                listAssignments.Remove(key);
                schedulerControlDefault.Storage.Appointments.Remove(app);
            }
            schedulerControlDefault.EndUpdate();

            if (listOperators.ContainsKey(operCode) == true)
                listOperators.Remove(operCode);
        }

        public void UpdateAppointmentByOperator(OperatorClientData oper, bool isSupervisor)
        {
            List<Appointment> appToDelete = schedulerControlDefault.Storage.Appointments.Items.Where(app => (int)app.CustomFields["OperatorCode"] == oper.Code).ToList();
            string key = string.Empty;
            schedulerControlDefault.BeginUpdate();
            foreach (Appointment app in appToDelete)
            {
                app.CustomFields["IsSupervisor"] = isSupervisor;
            }
            schedulerControlDefault.EndUpdate();
            listOperators[oper.Code] = oper;
        }

        public void RemoveExpiredAppointments(DateTime date, int timeToDelete)
        {
            List<Appointment> appToDelete = schedulerControlDefault.Storage.Appointments.Items.Where(app => app.End.AddMinutes(timeToDelete) < date).ToList();
            string key = string.Empty;
            schedulerControlDefault.BeginUpdate();
            foreach (Appointment app in appToDelete)
            {
                key = (int)app.CustomFields["OperatorCode"] + "|" + (int)app.CustomFields["ScheduleCode"];
                listAssignments.Remove(key);
                schedulerControlDefault.Storage.Appointments.Remove(app);
            }
            schedulerControlDefault.EndUpdate();
        }

     

        public void OperatorUpdate(OperatorClientData oper, CommittedDataAction action, bool changeRole)
        {
            if (action == CommittedDataAction.Update)
            {
                if (listOperators.ContainsKey(oper.Code) == true)
                    listOperators[oper.Code] = oper;
                else
                    listOperators.Add(oper.Code, oper);
                
                
                if (changeRole == true) 
                {
                    List<Appointment> appToUpdate = schedulerControlDefault.Storage.Appointments.Items.Where(app => (int)app.CustomFields["OperatorCode"] == oper.Code).ToList();
                    
                    schedulerControlDefault.Storage.BeginUpdate();

                    if (appToUpdate.Count > 0)
                    {
                        foreach (Appointment app in appToUpdate)
                        {
                            app.CustomFields["IsSupervisor"] = !((bool)app.CustomFields["IsSupervisor"]);
                            app.LabelId = 0;
                        }
                    }
                    else 
                    {
                        foreach (WorkShiftVariationClientData wsv in oper.WorkShifts)
                        {
                            foreach (WorkShiftScheduleVariationClientData schedule in wsv.Schedules)
                                CreateAppointmentByWorkShift(oper, wsv, schedule, new List<OperatorAssignClientData>(), 0, oper.IsSupervisor);
                        }
                    }

                    schedulerControlDefault.Storage.EndUpdate();

                }
            }
            else if (action == CommittedDataAction.Delete)
            {
                List<Appointment> appToUpdate = schedulerControlDefault.Storage.Appointments.Items.Where(app => (int)app.CustomFields["OperatorCode"] == oper.Code).ToList();

                schedulerControlDefault.Storage.BeginUpdate();

                foreach (Appointment app in appToUpdate)
                {
                    schedulerControlDefault.Storage.Appointments.Remove(app);
                    if (listAssignments.ContainsKey(oper.Code + "|" + (int)app.CustomFields["ScheduleCode"]) == true)
                        listAssignments.Remove(oper.Code + "|" + (int)app.CustomFields["ScheduleCode"]); 
                }
                schedulerControlDefault.Storage.EndUpdate();

                if (listOperators.ContainsKey(oper.Code) == true)
                    listOperators.Remove(oper.Code);               

            }
                     
        }

        public void AssignmentUpdate(OperatorAssignClientData operAssign, int isSupervised, CommittedDataAction action, bool isSupervisor) 
        {
            Appointment appToUpdate;
                
            if (isSupervisor == false)
                appToUpdate =schedulerControlDefault.Storage.Appointments.Items.First(app => (int)app.CustomFields["OperatorCode"] == operAssign.SupervisedOperatorCode &&
                                                                                                        (int)app.CustomFields["ScheduleCode"] == operAssign.SupervisedScheduleVariation.Code);
            else
                appToUpdate =schedulerControlDefault.Storage.Appointments.Items.First(app => (int)app.CustomFields["OperatorCode"] == operAssign.SupervisorCode &&
                                                                                                     (int)app.CustomFields["ScheduleCode"] == operAssign.SupervisorScheduleVariation.Code);

            appToUpdate.LabelId = isSupervised;
            string key = (int)appToUpdate.CustomFields["OperatorCode"] + "|" + (int)appToUpdate.CustomFields["ScheduleCode"];
            List<OperatorAssignClientData> list = listAssignments[key];
           
            if (list != null) {

                if (action == CommittedDataAction.Save)
                    list.Add(operAssign);
                else 
                {
                    int index = -1;

                    index = list.IndexOf(operAssign);

                    if (index > -1) {
                        if (action == CommittedDataAction.Update)
                            list[index] = operAssign;
                        else
                            list.RemoveAt(index);                    
                    }
                
                }

                listAssignments[key] = list;
            }                        
        }

        public void SchedulerBeginUpdate() 
        {

            schedulerControlDefault.Storage.BeginUpdate();
        }

        public void SchedulerEndUpdate()
        {

            schedulerControlDefault.Storage.EndUpdate();
       
        }

        private void LoadLanguage()
        {
            this.viewNavigatorBackwardItem1.Caption = ResourceLoader.GetString2("ViewNavigatorBackwardTitle");
            this.viewNavigatorBackwardItem1.SuperTip = GetSuperToolTip("ViewNavigatorBackwardTitle", "ViewNavigatorBackwardSuperTipContents");
            
            this.viewNavigatorForwardItem1.Caption = ResourceLoader.GetString2("ViewNavigatorForwardTitle");
            this.viewNavigatorForwardItem1.SuperTip = GetSuperToolTip("ViewNavigatorForwardTitle", "ViewNavigatorForwardSuperTipContents");

            this.viewNavigatorTodayItem1.Caption = ResourceLoader.GetString2("ViewNavigatorTodayTitle");
            this.viewNavigatorTodayItem1.SuperTip = GetSuperToolTip("ViewNavigatorTodayTitle", "ViewNavigatorTodaySuperTipContents");

            this.viewNavigatorZoomInItem1.Caption = ResourceLoader.GetString2("ZoomIn");
            this.viewNavigatorZoomInItem1.SuperTip = GetSuperToolTip("ZoomIn", "ZoomInSuperToolTipContents");


            this.viewNavigatorZoomOutItem1.Caption = ResourceLoader.GetString2("ZoomOut");
            this.viewNavigatorZoomOutItem1.SuperTip = GetSuperToolTip("ZoomOut", "ZoomOutSuperToolTipContents");

            this.viewSelectorItem1.Caption = ResourceLoader.GetString2("Day");
            this.viewSelectorItem1.SuperTip = GetSuperToolTip("Day", "DaySuperToolTipContents");

            this.viewSelectorItem2.Caption = ResourceLoader.GetString2("WorkWeek");
            this.viewSelectorItem2.SuperTip = GetSuperToolTip("WorkWeek", "WorkWeekSuperToolTipContents");

            this.viewSelectorItem3.Caption = ResourceLoader.GetString2("Week");
            this.viewSelectorItem3.SuperTip = GetSuperToolTip("Week", "WeekSuperToolTipContents");

            this.viewSelectorItem4.Caption = ResourceLoader.GetString2("Month");
            this.viewSelectorItem4.SuperTip = GetSuperToolTip("Month", "MonthSuperToolTipContents");
                        
            this.schedulerControlDefault.OptionsView.NavigationButtons.NextCaption = ResourceLoader.GetString2("NextAppointmentCaption");
            this.schedulerControlDefault.OptionsView.NavigationButtons.PrevCaption = ResourceLoader.GetString2("PrevAppointmentCaption");
            dockPanelScheduler.Text = ResourceLoader.GetString2("Calendar");
            labelControlNotSupervised.Text = ResourceLoader.GetString2("NotSupervised");
            labelControlPartially.Text = ResourceLoader.GetString2("PartiallySupervised");
            labelControlTotally.Text = ResourceLoader.GetString2("Supervised");
            layoutControlGroupLegend.Text = ResourceLoader.GetString2("Legend");
            dateNavigator.TodayButton.Text = ResourceLoader.GetString2("Today");
        
    
        }

        private SuperToolTip GetSuperToolTip(string tooltiptext, string supertooltipcontents)
        {
            ToolTipTitleItem tttitem = new ToolTipTitleItem();
            ToolTipItem ttitem = new ToolTipItem();
            tttitem.Text = ResourceLoader.GetString2(tooltiptext);
            ttitem.LeftIndent = 6;
            ttitem.Text = ResourceLoader.GetString2(supertooltipcontents);
            SuperToolTip supertt = new SuperToolTip();
            supertt.Items.Add(tttitem);
            supertt.Items.Add(ttitem);
            return supertt;
        }

 
        public void UpdateScheduler(int operCode, int wsvCode, int filterOperator) 
        {
            clean = false;
            selectedOperatorCode = operCode;
            selectedWorkShiftCode = wsvCode;
            filter = filterOperator;

            FormUtil.InvokeRequired(this, delegate
            {
                schedulerControlDefault.ActiveView.LayoutChanged();
            });
        }

        public void UpdateScheduler(int operCode)
        {
            clean = false;
            selectedOperatorCode = operCode;
           
            FormUtil.InvokeRequired(this, delegate
            {
                schedulerControlDefault.ActiveView.LayoutChanged();
            });
        }

        public void CleanScheduler()
        {
            clean = true;
            FormUtil.InvokeRequired(this, delegate
            {
                schedulerControlDefault.ActiveView.LayoutChanged();
            });
        }

        public void GoToFirstAppointment(int operCode, int wsCode) 
        {
            IEnumerable<Appointment> appointments = schedulerControlDefault.Storage.Appointments.Items.Where(app => (int)app.CustomFields["OperatorCode"] == operCode && (int)app.CustomFields["WorkShiftCode"] == wsCode && (app.LabelId == filter || filter == 3));

            if (appointments.Count() > 0)
            {
                DateTime start = appointments.Min(app => app.Start);

                if (start != DateTime.MinValue)
                {
                    schedulerControlDefault.GoToDate(start, schedulerControlDefault.ActiveViewType);
                }
            }
        }

        public void GoToFirstAppointment(int code, bool isSupervisor)
        {
            DateTime start = new DateTime();

            if (isSupervisor == true)
                start = schedulerControlDefault.Storage.Appointments.Items.Where(app => (int)app.CustomFields["OperatorCode"] == code).Min(app => app.Start);
            else
                start = schedulerControlDefault.Storage.Appointments.Items.Where(app => (int)app.CustomFields["WorkShiftCode"] == code && (app.LabelId == filter || filter == 3)).Min(app => app.Start);

            if (start != DateTime.MinValue)
            {
                schedulerControlDefault.GoToDate(start, schedulerControlDefault.ActiveViewType);
            }

        }


        private void schedulerControlDefault_EditAppointmentFormShowing(object sender, AppointmentFormEventArgs e)
        {
			if (e.Appointment.CustomFields["OperatorCode"] != null)
			{
				OperatorAssignDetailForm form = new OperatorAssignDetailForm(e.Appointment);
				form.ShowDialog();
			}
			e.Handled = true;
        }

   

        private void schedulerStorageDefault_FilterAppointment(object sender, PersistentObjectCancelEventArgs e)
        {
            if (clean == false && e.Object != null)
            {
                int operCode = (int)((Appointment)e.Object).CustomFields["OperatorCode"];

                if (isOperatorView == true) //Tengo seleccionado un item del grid de operadores.
                {
                    int wsvCode = (int)((Appointment)e.Object).CustomFields["WorkShiftCode"];
                    int label = ((Appointment)e.Object).LabelId;
                    bool isSupervisor = (bool)((Appointment)e.Object).CustomFields["IsSupervisor"];
                    if (selectedOperatorCode != 0)
                    {
                        e.Cancel = !((operCode == selectedOperatorCode) && (wsvCode == selectedWorkShiftCode) && (filter == 3 || label == filter));
                    }
                    else
                    {
                        e.Cancel = !((wsvCode == selectedWorkShiftCode) && (filter == 3 || label == filter) && !isSupervisor);
                    }
                }
                else //Item grid de supervisores 
                {
                    e.Cancel = !(operCode == selectedOperatorCode);
                }
            }
            else
                e.Cancel = true;

          
        }


        public event SelectedAppointment_Changed SelectedAppointmentChanged;

        public delegate void SelectedAppointment_Changed(object sender, Appointment newApp);

        private void schedulerControlDefault_SelectionChanged(object sender, EventArgs e)
        {
            if (SelectedAppointmentChanged != null && schedulerControlDefault.SelectedAppointments.Count > 0)
            {
                SelectedAppointmentChanged(this, schedulerControlDefault.SelectedAppointments[0] as Appointment);

            }
            else if (SelectedAppointmentChanged != null)
                SelectedAppointmentChanged(this, null);

            
        }


        public enum CustomMenuItem 
        {
            DETAIL = 0,
            ASSIGN = 1,
            DELETE = 2        
        }

        private void schedulerControlDefault_PreparePopupMenu(object sender, PreparePopupMenuEventArgs e)
        {
            if (e.Menu.Id == SchedulerMenuItemId.DefaultMenu)
                e.Menu = null;
            else if (e.Menu.Id == SchedulerMenuItemId.AppointmentMenu)
            {
                SchedulerPopupMenu customMenu = new SchedulerPopupMenu();
                DXMenuItem item = new DXMenuItem(ResourceLoader.GetString2("Details"), DXMenuItem_ItemClick, ResourceLoader.GetImage("Image.OperatorAssignDetail"));
                item.Tag = CustomMenuItem.DETAIL;
                customMenu.Items.Add(item);
                item = new DXMenuItem(ResourceLoader.GetString2("Assign"), DXMenuItem_ItemClick, ResourceLoader.GetImage("Image.OperatorAssignCreate"));
                item.Tag = CustomMenuItem.ASSIGN;
                item.Enabled = isEnabledAssign;
                customMenu.Items.Add(item);
                item = new DevExpress.Utils.Menu.DXMenuItem(ResourceLoader.GetString2("Delete"), DXMenuItem_ItemClick, ResourceLoader.GetImage("Image.OperatorAssignDelete"));
                item.Tag = CustomMenuItem.DELETE;
                item.Enabled = isEnabledRemove;
                customMenu.Items.Add(item);
                e.Menu = customMenu;
            
            }
        }
        

        public event CustomMenuClick CustomMenu_Click;
        
        public delegate void CustomMenuClick(object sender, DevExpress.Utils.Menu.DXMenuItem item);

        private void DXMenuItem_ItemClick(object sender, EventArgs e)
        {
            CustomMenu_Click(this, sender as DevExpress.Utils.Menu.DXMenuItem); 
        }
        
        private void schedulerControlDefault_InitAppointmentDisplayText(object sender, AppointmentDisplayTextEventArgs e)
        {
            string key = (int)e.Appointment.CustomFields["OperatorCode"] + "|" + (int)e.Appointment.CustomFields["ScheduleCode"];
            List<OperatorAssignClientData> assignList = listAssignments[key];
            string description = string.Empty;

            if (isOperatorView == true)
            {
                if (assignList != null && assignList.Count > 0)
                {
                    description = ResourceLoader.GetString2("Supervisors") + ":\n";

                    foreach (OperatorAssignClientData assign in assignList)
                    {
                        description += assign.StartDate.ToString("HH:mm") + " - " + assign.EndDate.ToString("HH:mm") + "  " + assign.SupFirstName + " " + assign.SupLastName + "\n";
                    }
                }

                if (isOperatorSelected == true)
                    e.Text = string.Empty;
                else
                {
                    OperatorClientData oper = listOperators[(int)e.Appointment.CustomFields["OperatorCode"]];
                    e.Text = oper.FirstName + " " + oper.LastName;
                }
            }
            else
            {
                if (assignList != null && assignList.Count > 0)
                {
                    description = ResourceLoader.GetString2("Operators") + ":\n";

                    foreach (OperatorAssignClientData assign in assignList)
                    {
                        description += assign.StartDate.ToString("HH:mm") + " - " + assign.EndDate.ToString("HH:mm") + "  " + assign.OperFirstName + " " + assign.OperLastName + "\n";
                    }
                }
           
                e.Text = string.Empty;
            }
                        
            e.Description = description;
        }

    }

}
