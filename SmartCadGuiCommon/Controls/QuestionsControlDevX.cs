using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Collections;

using Smartmatic.SmartCad.Service;
using System.IO;
using System.Xml;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using SmartCadControls.Controls;
using SmartCadCore.Common;
using SmartCadCore.ClientData;
using DevExpress.XtraLayout;
using SmartCadFirstLevel.Gui;
using SmartCadCore.Core;
using SmartCadCore.Enums;
using SmartCadFirstLevel.Gui.Handler;
using SmartCadGuiCommon.Enums;
using SmartCadGuiCommon.Controls;
using SmartCadGuiCommon.Classes;
using Smartmatic.SmartCad.Gui;

namespace SmartCadGuiCommon.Controls
{

    public partial class QuestionsControlDevX : HeaderPanelControl
    {
        #region Constructors

        public QuestionsControlDevX()
        {
            InitializeComponent();

            buttonIncidentTypes.Enabled = true;
            
            generalFont = new Font(FontFamily.GenericSansSerif, 10.0f);
            LoadLanguage();
        }

        #endregion

        private void LoadLanguage()
        {
            layoutControlItemIncidentTypes.Text = ResourceLoader.GetString2("Availables");
            layoutControlItemSelectedIncidentTypes.Text = ResourceLoader.GetString2("Selected") + " *";
            buttonIncidentTypes.Text = ResourceLoader.GetString2("IncidentTypes");

            SuperToolTip tooltipAddIncidentType = new SuperToolTip();
            ToolTipItem itemAddIncidentType = new ToolTipItem() { Text = ResourceLoader.GetString2("AddIncidentTypeButtonTooltip") };
            tooltipAddIncidentType.Items.Add(itemAddIncidentType);
            toolTipController.SetSuperTip(buttonAddIncidentType, tooltipAddIncidentType);

            SuperToolTip tooltipAddAllIncidentType = new SuperToolTip();
            ToolTipItem itemAddAllIncidentType = new ToolTipItem() { Text = ResourceLoader.GetString2("AddAllIncidentTypeButtonTooltip") };
            tooltipAddAllIncidentType.Items.Add(itemAddAllIncidentType);
            toolTipController.SetSuperTip(buttonAddAllIncidentType, tooltipAddAllIncidentType);

            SuperToolTip tooltipRemoveIncidentType = new SuperToolTip();
            ToolTipItem itemRemoveIncidentType = new ToolTipItem() { Text = ResourceLoader.GetString2("RemoveIncidentTypeButtonTooltip") };
            tooltipRemoveIncidentType.Items.Add(itemRemoveIncidentType);
            toolTipController.SetSuperTip(buttonRemoveIncidentType, tooltipRemoveIncidentType);

            SuperToolTip tooltipRemoveAllIncidentType = new SuperToolTip();
            ToolTipItem itemRemoveAllIncidentType = new ToolTipItem() { Text = ResourceLoader.GetString2("RemoveAllIncidentTypeButtonTooltip") };
            tooltipRemoveAllIncidentType.Items.Add(itemRemoveAllIncidentType);
            toolTipController.SetSuperTip(buttonRemoveAllIncidentType, tooltipRemoveAllIncidentType);
        }

        #region Fields

        public FrontClientMode clientMode;
        public event EventHandler<EventArgs> AnswerUpdated;
        ArrayList selectedIncidentTypeQuestionsWithStatus;
        int buttonQuestionOffSetIndex;
        IList selectedIncidentTypes;
        IList incidentTypes;
        IList questionsWithAnswers = new ArrayList();
        public event EventHandler<EventArgs> IncidentTypesSelected;
        private AddressClientData incidentAddress;

        private Dictionary<int, ReportAnswerClientData> questionHistory = new Dictionary<int, ReportAnswerClientData>();
        private SmartCadCore.Enums.FrontClientStateEnum frontClientState;
        private Dictionary<int, IncidentTypeClientData> globalIncidentTypes;
        private SmartCadCore.Enums.CctvClientStateEnum cctvClientState = SmartCadCore.Enums.CctvClientStateEnum.WaitingForCctvIncident;

        private List<VistaButtonEx> buttonList;

        private Color notAskedStatusColor;
        private Color askingStatusColor;
        private Color askedStatusColor;
        private Color disabledButtonColor;

        private Color activeQuestionTextColor;
        private Color inactiveQuestionTextColor;
        private Color activeAnswerTextColor;
        private Color inactiveAnswerTextColor;

        private Font generalFont;

        private string clickedButtonName;

        private int numLayer;
        private bool incidentAdded = false;
        public delegate void RemoveIncident(IList selectedIncidentTypes);
        public event RemoveIncident removeIncident;
        Dictionary<string, QuestionPossibleAnswerClientData> answerIds;
        Dictionary<string, PossibleAnswerAnswerClientData> dicPaad;
        #endregion

        #region Properties

        public string NamePressedButton
        {
            get
            {
                return clickedButtonName;
            }
            set
            {
                clickedButtonName = value;
            }
        }

        public bool IsPanelIncidentTypeActive
        {
            get
            {
                return this.panelIncidentTypes.Visible;
            }
        }

        public FrontClientStateEnum FrontClientState
        {
            get
            {
                return this.frontClientState;
            }
            set
            {
                this.frontClientState = value;
                if (value == FrontClientStateEnum.WaitingForIncident)
                    SetFrontClientWaitingForCallState();
                else if (value == FrontClientStateEnum.RegisteringIncident)
                    SetFrontClientRegisteringCallState();
                else if (value == FrontClientStateEnum.UpdatingIncident)
                    SetFrontClientUpdatingCallState();
            }
        }

        public SmartCadCore.Enums.CctvClientStateEnum CctvClientState
        {
            get
            {
                return this.cctvClientState;
            }
            set
            {
                this.cctvClientState = value;
                if (value == SmartCadCore.Enums.CctvClientStateEnum.WaitingForCctvIncident)
                    SetFrontClientWaitingForCallState();
                else if (value == SmartCadCore.Enums.CctvClientStateEnum.RegisteringCctvIncident)
                    SetFrontClientRegisteringCallState();
            }
        }

        public IList QuestionsWithAnswers
        {
            get
            {
                return this.questionsWithAnswers;
            }
            set
            {
                this.questionsWithAnswers = value;
            }
        }

        public IList SelectedIncidentTypes
        {
            get
            {
                return this.selectedIncidentTypes;
            }
            set
            {
                this.selectedIncidentTypes = value;
            }
        }

        public Dictionary<int, IncidentTypeClientData> GlobalIncidentTypes
        {
            get
            {
                return globalIncidentTypes;
            }
            set
            {
                globalIncidentTypes = value;
            }
        }

        [RefreshProperties(RefreshProperties.All),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
        Browsable(false)]
        public AddressClientData IncidentAddress
        {
            get
            {
                if (this.incidentAddress == null)
                {
                    this.incidentAddress = new AddressClientData(string.Empty, string.Empty, string.Empty, string.Empty, false);
                }

                return this.incidentAddress;
            }
            set
            {
                this.incidentAddress = value;
            }
        }

        public Color NotAskedStatusColor
        {
            get
            {
                return notAskedStatusColor;
            }
            set
            {
                notAskedStatusColor = value;
            }
        }

        public Color AskingStatusColor
        {
            get
            {
                return askingStatusColor;
            }
            set
            {
                askingStatusColor = value;
            }
        }

        public Color AskedStatusColor
        {
            get
            {
                return askedStatusColor;
            }
            set
            {
                askedStatusColor = value;
            }
        }

        public Color DisabledButtonColor
        {
            get
            {
                return disabledButtonColor;
            }
            set
            {
                disabledButtonColor = value;

                if (buttonList != null)
                {
                    foreach (VistaButtonEx button in buttonList)
                    {
                        button.DisabledButtonColor = disabledButtonColor;
                    }
                }
            }
        }

        public Color ActiveQuestionTextColor
        {
            get
            {
                return activeQuestionTextColor;
            }
            set
            {
                activeQuestionTextColor = value;
            }
        }

        public Color InactiveQuestionTextColor
        {
            get
            {
                return inactiveQuestionTextColor;
            }
            set
            {
                inactiveQuestionTextColor = value;
            }
        }

        public Color ActiveAnswerTextColor
        {
            get
            {
                return activeAnswerTextColor;
            }
            set
            {
                activeAnswerTextColor = value;
            }
        }

        public Color InactiveAnswerTextColor
        {
            get
            {
                return inactiveAnswerTextColor;
            }
            set
            {
                inactiveAnswerTextColor = value;
            }
        }

        #endregion

        public void FocusIncidentTypes()
        {
            this.listBoxIncidentTypes.SelectedIndex = 0;
        }


        private void FillIncidentTypes()
        {
            this.selectedIncidentTypes = new ArrayList();
            this.listBoxSelectedIncidentTypes.BeginUpdate();
            this.listBoxSelectedIncidentTypes.Items.Clear();
            this.listBoxSelectedIncidentTypes.EndUpdate();

            this.incidentTypes = new ArrayList(this.GlobalIncidentTypes.Values);
            ArrayList sortedList = new ArrayList(this.incidentTypes);
            sortedList.Sort(new IncidentTypeComparer());
            this.listBoxIncidentTypes.BeginUpdate();
            listBoxIncidentTypes.Items.Clear();
            foreach (IncidentTypeClientData incidentType in sortedList)
            {
                this.listBoxIncidentTypes.Items.Add(incidentType);
            }
            this.listBoxIncidentTypes.EndUpdate();
        }

        private void FillButtonsWithQuestions()
        {
            int buttonIndex = 1;
            int questionIndex;
            CleanAllButtons();

            for (questionIndex = buttonQuestionOffSetIndex;
                questionIndex < selectedIncidentTypeQuestionsWithStatus.Count &&
                questionIndex < buttonQuestionOffSetIndex + 15;
                questionIndex++)
            {
                QuestionWithStatus currentQuestionWithStatus = (QuestionWithStatus)selectedIncidentTypeQuestionsWithStatus[questionIndex];
                string currentButtonName = "buttonExQuestion" + buttonIndex;
                VistaButtonEx button = (VistaButtonEx)this.layoutControl3.Controls[currentButtonName];

                button.Text = currentQuestionWithStatus.Question.ShortText;

                if (currentQuestionWithStatus.Question.RequirementType == RequirementTypeClientEnum.Recommended)
                    button.Text = button.Text + " *";
                else if (currentQuestionWithStatus.Question.RequirementType == RequirementTypeClientEnum.Required)
                    button.Text = button.Text + " **";

                button.Tag = questionIndex;
                if (button.Left == 2 && button.Top == 2)
                {
                    buttonExQuestion1_Click(button, null);
                }

                if (currentQuestionWithStatus.IsPersisted == true)
                {
                    if (currentQuestionWithStatus.QuestionStatus == QuestionStatusEnum.Asking)
                        dragDropLayoutControl1.Enabled = false;

                    button.Enabled = false;
                }
                else
                    button.Enabled = true;
                button.ButtonColor = currentQuestionWithStatus.StatusColor;
                buttonIndex++;
            }

            if (questionIndex < selectedIncidentTypeQuestionsWithStatus.Count)
                buttonExForward.Enabled = true;
            else
                buttonExForward.Enabled = false;
        }

        private bool isQuestionWithStatusAlreadyAdded(QuestionWithStatus questionWithStatusToSearch)
        {
            bool found = false;
            int index = 0;
            while (found != true && index < selectedIncidentTypeQuestionsWithStatus.Count)
            {
                QuestionWithStatus currentQuestionWithStatus = selectedIncidentTypeQuestionsWithStatus[index] as QuestionWithStatus;
                if (currentQuestionWithStatus.Question.CustomCode == questionWithStatusToSearch.Question.CustomCode)
                    found = true;
                index++;
            }
            return found;
        }

        public void CleanControls()
        {
            textBoxSelectedIncidentTypes.Text = string.Empty;
            textBoxSelectedIncidentTypes.ToolTip = string.Empty;
            CleanAllButtons();
            for (int i = 0; i < dragDropLayoutControl1.layoutControl.Controls.Count; i++)
            {
                Control control = dragDropLayoutControl1.layoutControl.Controls[i];
                if ((answerIds != null && answerIds.ContainsKey(control.Name)) || control.Name.Equals("copyAddress") || control.Name.Equals("searchButton"))
                {
                    dragDropLayoutControl1.layoutControl.Controls.RemoveAt(i);
                    i--;
                }
            }
            while (dragDropLayoutControl1.layoutControl.Root.Items.Count > 0)
            {
                dragDropLayoutControl1.layoutControl.Root.Items.RemoveAt(0);
            }
            while (dragDropLayoutControl1.layoutControl.Items.Count > 1)
            {
                if (dragDropLayoutControl1.layoutControl.Items[0].Name.Equals(dragDropLayoutControl1.layoutControl.Root.Name) == true)
                    dragDropLayoutControl1.layoutControl.Items.RemoveAt(1);
            }
            dragDropLayoutControl1.Enabled = true;
            FillIncidentTypes();
            questionsWithAnswers = new ArrayList();
            questionHistory.Clear();
            if (selectedIncidentTypeQuestionsWithStatus != null)
                selectedIncidentTypeQuestionsWithStatus.Clear();

            incidentAddress = new AddressClientData(string.Empty, string.Empty, string.Empty, string.Empty, false);
            incidentAddress.Lon = 0.0;
            incidentAddress.Lat = 0.0;
        }

        private void CleanButtons(int buttonIndex)
        {
            for (int i = buttonIndex; i < 16; i++)
            {
                try
                {
                    string buttonName = "buttonExQuestion" + i;
                    VistaButtonEx button = (VistaButtonEx)this.layoutControl3.Controls[buttonName];
                    button.Enabled = true;//to ensure the buttonn's text cleaning...
                    button.Text = "";
                    button.Enabled = false;
                    QuestionWithStatus questionWithStatus = selectedIncidentTypeQuestionsWithStatus[(int)button.Tag] as QuestionWithStatus;
                    if (questionWithStatus.QuestionStatus != QuestionStatusEnum.Asked)
                        questionWithStatus.QuestionStatus = QuestionStatusEnum.NotAsked;
                    //if (button.ButtonColor != questionWithStatus.StatusColor)
                        button.ButtonColor = questionWithStatus.StatusColor;
                    button.Tag = null;
                }
                catch { }
            }
        }

        private void CleanAllButtons()
        {
            CleanButtons(1);
        }

        private void ButtonsChecker()
        {
            ButtonsCheker(new VistaButtonEx());
        }

        private void ButtonsCheker(VistaButtonEx pressedButton)
        {
            for (int i = 1; i < 16; i++)
            {
                string buttonName = "buttonExQuestion" + i;
                VistaButtonEx button = (VistaButtonEx)this.layoutControl3.Controls[buttonName];
                if (button.Tag != null && button.Equals(pressedButton) == false)
                {
                    QuestionWithStatus questionWithStatus = selectedIncidentTypeQuestionsWithStatus[(int)button.Tag] as QuestionWithStatus;
                    if (questionWithStatus.QuestionStatus == QuestionStatusEnum.Asking)
                    {
                        if (SearchQuestionWithAnswer(questionWithStatus.Question).Count > 0)
                        {
                            questionWithStatus.QuestionStatus = QuestionStatusEnum.Asked;
                            button.ButtonColor = questionWithStatus.StatusColor;
                        }
                        else
                        {
                            questionWithStatus.QuestionStatus = QuestionStatusEnum.NotAsked;
                            button.ButtonColor = questionWithStatus.StatusColor;
                        }
                        selectedIncidentTypeQuestionsWithStatus[(int)button.Tag] = questionWithStatus;
                    }
                }
            }
        }

        internal void SetIncidentAddress(AddressClientData addressToSet)
        {
            foreach (object control in dragDropLayoutControl1.layoutControl.Items)
            {
                LayoutControlItem ctrl = null;
                if (control is LayoutControlItem)
                {
                    ctrl = control as LayoutControlItem;
                }
                if (ctrl != null && ctrl.Control is TextBoxEx)
                {
                    QuestionPossibleAnswerClientData itpad = answerIds[ctrl.Control.Name];

                    if (itpad.Description == ResourceLoader.GetString2("Zone"))
                    {
                        ctrl.Control.Text = addressToSet.Zone;
                        ctrl.Control.Focus();
                    }
                    else if (itpad.Description == ResourceLoader.GetString2("Street"))
                        ctrl.Control.Text = addressToSet.Street;
                    else if (itpad.Description == ResourceLoader.GetString2("Reference"))
                        ctrl.Control.Text = addressToSet.Reference;
                    else if (itpad.Description == ResourceLoader.GetString2("More"))
                        ctrl.Control.Text = addressToSet.More;
                }
            }

            addressToSet.Lon = this.incidentAddress.Lon;
            addressToSet.Lat = this.incidentAddress.Lat;
            addressToSet.IsSynchronized = this.incidentAddress.IsSynchronized;

            this.incidentAddress = addressToSet;
        }

        private void CreateOrUpdateIncidentAddress(Control ctrl)
        {
            if (this.incidentAddress == null)
                this.incidentAddress = new AddressClientData("", "", "", "", false);
            if (ctrl.Tag != null)
            {
                object[] tags = ctrl.Tag as object[];
                QuestionPossibleAnswerClientData itpad = answerIds[ctrl.Name] as QuestionPossibleAnswerClientData;
                if (itpad.Description == ResourceLoader.GetString2("Zone"))
                    this.incidentAddress.Zone = ctrl.Text;
                else if (itpad.Description == ResourceLoader.GetString2("Street"))
                    this.incidentAddress.Street = ctrl.Text;
                else if (itpad.Description == ResourceLoader.GetString2("Reference"))
                    this.incidentAddress.Reference = ctrl.Text;
                else if (itpad.Description == ResourceLoader.GetString2("More"))
                    this.incidentAddress.More = ctrl.Text;
            }
        }

        private void AddOrUpdateQuestionWithAnswer(ReportAnswerClientData phoneReportAnswerData, bool radioButton)
        {
            bool toUpdate = false;
            int index = 0;
            while (toUpdate == false && index < questionsWithAnswers.Count)
            {
                ReportAnswerClientData answer = (ReportAnswerClientData)questionsWithAnswers[index];
                if (answer.QuestionCode == phoneReportAnswerData.QuestionCode)
                    toUpdate = true;
                index = index + 1;
            }
            if (toUpdate == true)
            {
                ((ReportAnswerClientData)questionsWithAnswers[index - 1]).UpdateAnswers(phoneReportAnswerData, radioButton);
                AddOrUpdateQuestionHistory(phoneReportAnswerData, radioButton);
            }
            else
            {
                questionsWithAnswers.Add(phoneReportAnswerData);
                AddOrUpdateQuestionHistory(phoneReportAnswerData, radioButton);
            }
            if (this.AnswerUpdated != null)
                this.AnswerUpdated(this, EventArgs.Empty);
        }


        private void AddOrUpdateQuestionHistory(ReportAnswerClientData phoneReportAnswerData, bool radioButton)
        {
            if (questionHistory.ContainsKey(phoneReportAnswerData.QuestionCode) == true)
                questionHistory[phoneReportAnswerData.QuestionCode].UpdateAnswers(phoneReportAnswerData, radioButton);
            else
                questionHistory.Add(phoneReportAnswerData.QuestionCode, phoneReportAnswerData);
        }

        private void DeleteQuestionHistory(ReportAnswerClientData phoneReportAnswerData)
        {
            questionHistory.Remove(phoneReportAnswerData.QuestionCode);
        }

        public void SelectNextNotAskedQuestion(VistaButtonEx pressedButton)
        {
            for (int i = 1; i < 16; i++)
            {
                string buttonName = "buttonExQuestion" + i;
                VistaButtonEx button = (VistaButtonEx)this.layoutControl3.Controls[buttonName];
                if (button.Tag != null && button.Equals(pressedButton) == false)
                {
                    QuestionWithStatus questionWithStatus = selectedIncidentTypeQuestionsWithStatus[(int)button.Tag] as QuestionWithStatus;
                    if (questionWithStatus.QuestionStatus == QuestionStatusEnum.NotAsked)
                    {
                        buttonExQuestion1_Click(button, null);
                        break;
                    }
                    else if (questionWithStatus.QuestionStatus == QuestionStatusEnum.Asking)
                    {
                        bool found = false;
                        for (int j = 0; j < questionsWithAnswers.Count; j++)
                        {
                            if (questionWithStatus.Question.Code == ((ReportAnswerClientData)questionsWithAnswers[j]).QuestionCode)
                            {
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                        {
                            RenderAnswer(questionWithStatus.Question.RenderMethod, questionWithStatus.Question);
                            break;
                        }
                    }
                }
            }
        }

        private void buttonExQuestion1_Click(object sender, EventArgs ev)
        {
            VistaButtonEx button = (VistaButtonEx)sender;
            this.clickedButtonName = button.Name;
            QuestionWithStatus questionWithStatus = selectedIncidentTypeQuestionsWithStatus[(int)button.Tag] as QuestionWithStatus;
            QuestionClientData question = questionWithStatus.Question;
            QuestionStatusEnum questionStatus = questionWithStatus.QuestionStatus;
            this.labelExLongQuestion.Text = question.Text;

            ButtonsCheker(button);

            if (questionStatus == QuestionStatusEnum.NotAsked)
            {
                questionWithStatus.QuestionStatus = QuestionStatusEnum.Asking;
                RenderAnswer(question.RenderMethod, question);
            }
            else if (questionStatus == QuestionStatusEnum.Asked)
            {
                questionWithStatus.QuestionStatus = QuestionStatusEnum.Asking;
                RenderAnswer(question.RenderMethod, question);
            }
            else
                button.Focus();
            button.ButtonColor = questionWithStatus.StatusColor;
        }

        private void RenderAnswer(string render, QuestionClientData question)
        {
            //first, delete all the controls from previous questions
            dragDropLayoutControl1.layoutControl.BeginUpdate();
            for (int i = 0; i < dragDropLayoutControl1.layoutControl.Controls.Count; i++)
            {
                Control control = dragDropLayoutControl1.layoutControl.Controls[i];
                if ((answerIds != null && answerIds.ContainsKey(control.Name)) || control.Name.Equals("copyAddress") || control.Name.Equals("searchButton"))
                {
                    dragDropLayoutControl1.layoutControl.Controls.RemoveAt(i);
                    i--;
                }
            }
            while (dragDropLayoutControl1.layoutControl.Root.Items.Count > 0)
            {
                dragDropLayoutControl1.layoutControl.Root.Items.RemoveAt(0);
            }
            while (dragDropLayoutControl1.layoutControl.Items.Count > 1)
            {
                if (dragDropLayoutControl1.layoutControl.Items[0].Name.Equals(dragDropLayoutControl1.layoutControl.Root.Name) == true)
                    dragDropLayoutControl1.layoutControl.Items.RemoveAt(1);
            }
            dragDropLayoutControl1.Enabled = true;
            dragDropLayoutControl1.layoutControl.EndUpdate();

            IList answers = question.PossibleAnswers;
            IList answersAnswered = SearchQuestionWithAnswer(question);
            dicPaad = new Dictionary<string, PossibleAnswerAnswerClientData>();
            foreach (PossibleAnswerAnswerClientData paad in answersAnswered)
            {
                if (dicPaad.ContainsKey(paad.QuestionPossibleAnswer.ControlName) == false)
                {
                    //string[] arr = paad.TextAnswer.Split(':');
                    //try { if (arr[1] == null) { } else paad.TextAnswer = arr[1]; }
                    //catch { }
                    dicPaad.Add(paad.QuestionPossibleAnswer.ControlName, paad);
                }
            }

            MemoryStream ms = GetMemoryStreamWithXML(render, question);
            dragDropLayoutControl1.layoutControl.RestoreLayoutFromStream(ms);

            Control firstControl = null;
            foreach (Control ctrl in dragDropLayoutControl1.layoutControl.Controls)
            {
                if (ctrl is TextBoxEx || ctrl is CheckEdit || ctrl is RadioButton)
                {
                    if (firstControl == null)
                    {
                        firstControl = ctrl;
                    }
                    else
                    {
                        if (ctrl.Location.X < firstControl.Location.X ||
                            ctrl.Location.Y < firstControl.Location.Y)
                        {
                            firstControl = ctrl;
                        }
                    }
                    
                }
            }
            if (firstControl != null)
            {
                if (!(firstControl is RadioButton))
                {
                    firstControl.Focus(); 
                }                
            }

            //TODO: Propiedades extras agregar.
            answerIds = new Dictionary<string, QuestionPossibleAnswerClientData>();

            foreach (QuestionPossibleAnswerClientData answer in answers)
                if (answer.ControlName!=null && answerIds.ContainsKey(answer.ControlName) == false)
                    answerIds.Add(answer.ControlName, answer);
        }

        private MemoryStream GetMemoryStreamWithXML(string render, QuestionClientData question)
        {
            byte[] byteArray = Encoding.UTF8.GetBytes(render);
            MemoryStream ms = new MemoryStream(byteArray);
            ms.Position = 0;
            XmlDocument doc = new XmlDocument();
            doc.Load(ms);
            XmlNode firstChild = doc.FirstChild;
            var applications = ServerServiceClient.GetInstance().GetActiveApplications();
            var add = true;
            foreach (XmlNode node in firstChild.ChildNodes)
            {
                if (node.Name == "Control")
                {
                    Control control = (Control)Activator.CreateInstance(Type.GetType(node.Attributes["type"].Value));
                    control.Name = node.Attributes["name"].Value;
                    control.Parent = this;
                    control.Text = "";
                    if (control.Name.Equals("copyAddress"))
                    {
                        control.Click += new EventHandler(new WhereButtonHandler().OnClick);
                        (control as DevExpress.XtraEditors.SimpleButton).Image = ResourceLoader.GetImage(node.Attributes["image"].Value);
                        toolTipMain.SetToolTip(control, ResourceLoader.GetString2("CopyAddressToolTip"));
                    }
                    else if (control.Name.Equals("searchButton"))
                    {
                        
                        if (applications.Contains(SmartCadApplications.SmartCadMap))
                        {
                        control.Click += new EventHandler(new SearchAddressButtonHandler().OnClick);
                        (control as DevExpress.XtraEditors.SimpleButton).Image = ResourceLoader.GetImage(node.Attributes["image"].Value);
                        toolTipMain.SetToolTip(control, ResourceLoader.GetString2("SearchButtonToolTip"));
                        
                            //control.Visible = true;
                            //control.Enabled = true;
                            //add = false;
                        }
                    }
                    if (control is TextBoxEx)
                    {
                        string[] ans = new string[2];
                        if (dicPaad.ContainsKey(control.Name))
                        {
                            if (dicPaad[control.Name].TextAnswer.Trim() != string.Empty)
                                ans = dicPaad[control.Name].TextAnswer.Split(':');
                            if (ans[1] == null)
                                (control as TextBoxEx).Text = dicPaad.ContainsKey(control.Name) ? dicPaad[control.Name].TextAnswer : "";
                            else
                                (control as TextBoxEx).Text = dicPaad.ContainsKey(control.Name) ? ans[1] : "";
                        }
                        (control as TextBoxEx).TextChanged += new EventHandler(this.t_TextChanged);
                        (control as TextBoxEx).MaxLength = 250;
                        if (node.Attributes["multiline"] != null)
                        {
                            (control as TextBoxEx).MaxLength = 1200;
                            (control as TextBoxEx).Multiline = bool.Parse(node.Attributes["multiline"].Value);
                            (control as TextBoxEx).AcceptsReturn = true;
                            (control as TextBoxEx).ScrollBars = ScrollBars.Vertical;
                        }

                    }
                    else if (control is RadioButton)
                    {
                        (control as RadioButton).Checked = dicPaad.ContainsKey(control.Name) ? true : false;
                        (control as RadioButton).CheckedChanged += new EventHandler(this.rb_CheckedChanged);
                    }
                    else if (control is DevExpress.XtraEditors.CheckEdit)
                    {
                        (control as DevExpress.XtraEditors.CheckEdit).Checked = dicPaad.ContainsKey(control.Name) ? true : false;
                        (control as DevExpress.XtraEditors.CheckEdit).CheckedChanged += new EventHandler(this.chk_Click);
                    }
                    control.Tag = question;
                    if (add) 
                    {
                        dragDropLayoutControl1.layoutControl.Controls.Add(control);
                    }
                    else
                    {
                        add = true;
                    }

                }
            }


            ms.Position = 0;
            return ms;
        }

        private Label CreateLabel(Element element)
        {
            Label label = new Label();
            label.Location = new Point(int.Parse(element.X), int.Parse(element.Y));
            label.Text = element.Text;
            label.AutoSize = true;
            label.BackColor = Color.Transparent;
            label.Font = generalFont;
            return label;
        }

        private ButtonEx CreateButton(Element element)
        {
            ButtonEx button = new ButtonEx();

            button.Location = new Point(int.Parse(element.X), int.Parse(element.Y));
            button.Size = new Size(int.Parse(element.Width), int.Parse(element.Height));
            button.Text = element.Text;
            button.Font = generalFont;
            toolTipMain.SetToolTip(button, element.ToolTip);

            if (!element.Image.Equals(string.Empty))
            {
                button.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                button.ForeColor = System.Drawing.Color.Black;
                button.Image = ((System.Drawing.Image)ResourceLoader.GetImage(element.Image));
                button.BackColor = System.Drawing.Color.LightGray;
            }

            Type type = Type.GetType(element.Handler);

            if (type != null && typeof(ButtonHandler).IsAssignableFrom(type))
            {
                ButtonHandler buttonHandler = (ButtonHandler)type.GetConstructor(new Type[] { }).Invoke(null);
                button.Click += new EventHandler(buttonHandler.OnClick);
                button.MouseHover += new EventHandler(questionControlButton_MouseHover);
                button.MouseLeave += new EventHandler(questionControlButton_MouseLeave);
            }

            return button;
        }

        void rb_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton r = sender as RadioButton;
            QuestionClientData question = r.Tag as QuestionClientData;
            QuestionPossibleAnswerClientData answer = null;
            bool oneChecked = false;

            if (answerIds.ContainsKey(r.Name) == true)
                answer = answerIds[r.Name];

            if (answer != null)
            {
                foreach (LayoutControlItem ctr in dragDropLayoutControl1.layoutControl.Root.Items)
                {
                    if (ctr.Control is RadioButton)
                    {
                        RadioButton rb = ctr.Control as RadioButton;
                        if (rb.Checked == true)
                        {
                            oneChecked = true;
                            break;
                        }
                    }
                }
                if (oneChecked == true)
                {
                    ReportAnswerClientData newPhoneReportAnswer = new ReportAnswerClientData(question.Code, answer, answerIds[r.Name].Description);
                    newPhoneReportAnswer.QuestionText = question.Text;
                    AddOrUpdateQuestionWithAnswer(newPhoneReportAnswer, r.Checked == false);
                }
                else
                    DeleteQuestionWithAnswer(question);
            }
        }

        void chk_Click(object sender, EventArgs e)
        {
            ReportAnswerClientData prad = new ReportAnswerClientData();
            prad.Answers = new ArrayList();
            bool atLeastOneAnswered = false;
            CheckEdit chk = sender as CheckEdit;
            QuestionClientData question = chk.Tag as QuestionClientData;
            QuestionPossibleAnswerClientData answer = null;

            if (answerIds.ContainsKey(chk.Name) == true)
                answer = answerIds[chk.Name];

            if (answer != null)
            {
                foreach (LayoutControlItem ctr in dragDropLayoutControl1.layoutControl.Root.Items)
                {
                    if (ctr.Control is CheckEdit)
                    {
                        if (((CheckEdit)ctr.Control).Checked == true)
                        {
                            atLeastOneAnswered = true;
                            break;
                        }
                    }
                    else if (ctr.Control is TextBoxEx)
                    {
                        if (ctr.Control.Text.Trim() != string.Empty)
                        {
                            atLeastOneAnswered = true;
                            break;
                        }
                    }
                    else if (ctr.Control is RadioButton)
                    {
                        if (((RadioButton)ctr.Control).Checked == true)
                        {
                            atLeastOneAnswered = true;
                            break;
                        }
                    }
                }

                if (chk != null)
                {
                    prad.QuestionCode = question.Code;
                    PossibleAnswerAnswerClientData paad = new PossibleAnswerAnswerClientData();
                    paad.ReportAnswerCode = prad.Code;
                    paad.QuestionPossibleAnswer = answer;
                    paad.TextAnswer = answerIds[chk.Name].Description;
                    prad.Answers.Add(paad);
                    prad.QuestionText = question.Text;

                    if (chk.Checked == true)
                        AddOrUpdateQuestionWithAnswer(prad, false);
                    else
                        AddOrUpdateQuestionWithAnswer(prad, true);

                    if (atLeastOneAnswered == false)
                        DeleteQuestionWithAnswer(question);

                }
            }

        }

        void t_TextChanged(object sender, EventArgs e)
        {
            ReportAnswerClientData prad = new ReportAnswerClientData();
            prad.Answers = new ArrayList();
            bool atLeastOneAnswered = false;

            TextBoxEx txtBox = sender as TextBoxEx;
            QuestionClientData question = txtBox.Tag as QuestionClientData;
            QuestionPossibleAnswerClientData answer = null;

            if (answerIds.ContainsKey(txtBox.Name) == true)
                answer = answerIds[txtBox.Name];

            if (answer != null)
            {

                foreach (LayoutControlItem ctr in dragDropLayoutControl1.layoutControl.Root.Items)
                {
                    if (ctr.Control is TextBoxEx)
                    {
                        if (ctr.Control.Text.Trim() != string.Empty)
                        {
                            atLeastOneAnswered = true;
                            break;
                        }
                    }
                    else if (ctr.Control is RadioButton)
                    {
                        if (((RadioButton)ctr.Control).Checked == true)
                        {
                            atLeastOneAnswered = true;
                            break;
                        }
                    }
                    else if (ctr.Control is CheckEdit)
                    {
                        if (((CheckEdit)ctr.Control).Checked == true)
                        {
                            atLeastOneAnswered = true;
                            break;
                        }
                    }
                }

                if (txtBox != null)
                {
                    if (question.CustomCode == "WHERE")
                        CreateOrUpdateIncidentAddress(txtBox);

                    prad.QuestionCode = question.Code;
                    PossibleAnswerAnswerClientData paad = new PossibleAnswerAnswerClientData();
                    paad.ReportAnswerCode = prad.Code;
                    paad.QuestionPossibleAnswer = answer;
                    paad.TextAnswer = answer.Description + " :" + txtBox.Text;
                    prad.Answers.Add(paad);
                    prad.QuestionText = question.Text;
                    prad.QuestionDesc = answer.Description;
                    if (txtBox.Text.Trim() != string.Empty)
                        AddOrUpdateQuestionWithAnswer(prad, false);
                    else
                        AddOrUpdateQuestionWithAnswer(prad, true);

                    if (atLeastOneAnswered == false)
                        DeleteQuestionWithAnswer(question);
                }
            }
        }

        private IList SearchQuestionWithAnswer(QuestionClientData question)
        {
            bool found = false;
            int index = 0;
            IList toReturn = new ArrayList();
            while (found == false && index < questionsWithAnswers.Count)
            {
                ReportAnswerClientData answer = (ReportAnswerClientData)questionsWithAnswers[index];
                if (answer.QuestionCode == question.Code)
                {
                    found = true;
                    toReturn = answer.Answers;
                }
                index = index + 1;
            }
            return toReturn;
        }

        private void QuestionsControl_Load(object sender, EventArgs e)
        {
            if (this.DesignMode == false)
            {
                SetupButtons();

                this.FillIncidentTypes();
                this.layoutControlItem29.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                this.layoutControlItem1.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                this.layoutControlItem9.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;

                this.textBoxSelectedIncidentTypes.Paint += new PaintEventHandler(textBoxSelectedIncidentTypes_Paint);
                this.listBoxIncidentTypes.KeyDown += new KeyEventHandler(listBoxIncidentTypes_KeyDown);
                this.listBoxSelectedIncidentTypes.KeyDown += new KeyEventHandler(listBoxSelectedIncidentTypes_KeyDown);
                this.listBoxIncidentTypes.Enter += new EventHandler(listBoxIncidentTypes_Enter);
                this.listBoxSelectedIncidentTypes.Enter += new EventHandler(listBoxSelectedIncidentTypes_Enter);
                this.listBoxSelectedIncidentTypes.ScrollAlwaysVisible = true;
                this.listBoxIncidentTypes.ScrollAlwaysVisible = true;
            }

            this.dragDropLayoutControl1.layoutControl.LookAndFeel.UseDefaultLookAndFeel = false;
            this.dragDropLayoutControl1.layoutControl.OptionsView.ShareLookAndFeelWithChildren = false;
            this.dragDropLayoutControl1.layoutControl.Root.AppearanceGroup.BackColor = this.BackColor;
            this.groupControlBody.Text = ResourceLoader.GetString2("TypesIncident");
            this.dragDropLayoutControl1.AllowDrop = false;
            this.dragDropLayoutControl1.layoutControl.AllowDrop = false;

        }

        void listBoxSelectedIncidentTypes_Enter(object sender, EventArgs e)
        {
            this.listBoxIncidentTypes.SelectedIndex = -1;
        }

        void listBoxIncidentTypes_Enter(object sender, EventArgs e)
        {
            this.listBoxSelectedIncidentTypes.SelectedIndex = -1;
        }

        void listBoxSelectedIncidentTypes_KeyDown(object sender, KeyEventArgs e)
        {
            int currentIndex = listBoxSelectedIncidentTypes.SelectedIndex;
            if (e.KeyCode == Keys.Left || e.KeyCode == Keys.NumPad4)
            {
                ArrayList unselectedIncidentTypes = new ArrayList(listBoxSelectedIncidentTypes.SelectedItems);
                RemoveSelectedIncidentType(unselectedIncidentTypes);
                if (currentIndex - 1 >= 0)
                    listBoxSelectedIncidentTypes.SelectedIndex = (currentIndex - 1);
                else
                    if (listBoxSelectedIncidentTypes.Items.Count > 0)
                        listBoxSelectedIncidentTypes.SelectedIndex = 0;
            }
            else if ((e.KeyCode == Keys.Enter || e.KeyCode == Keys.Space) && buttonOK.Enabled == true)
                buttonOK_Click(buttonOK, EventArgs.Empty);
        }

        void listBoxIncidentTypes_KeyDown(object sender, KeyEventArgs e)
        {
            int currentIndex = listBoxIncidentTypes.SelectedIndex;
            if (e.KeyCode == Keys.Right || e.KeyCode == Keys.NumPad6)
            {
                AddSelectedIncidentType(listBoxIncidentTypes.SelectedItems);
                if (incidentAdded == true)
                    if (currentIndex - 1 >= 0)
                        listBoxIncidentTypes.SelectedIndex = (currentIndex - 1);
                    else
                        if (listBoxIncidentTypes.Items.Count > 0)
                            listBoxIncidentTypes.SelectedIndex = 0;
            }
            else if ((e.KeyCode == Keys.Enter || e.KeyCode == Keys.Space) && buttonOK.Enabled == true)
                buttonOK_Click(buttonOK, EventArgs.Empty);
        }


        private void textBoxSelectedIncidentTypes_Paint(object sender, PaintEventArgs e)
        {
            Rectangle rect1 = this.textBoxSelectedIncidentTypes.DisplayRectangle;
            Rectangle rect = new Rectangle(rect1.X, rect1.Y, rect1.Width - 1, rect1.Height - 1);

            using (Pen pen = new Pen(IncidentTypeLabelBorderColor))
                e.Graphics.DrawRectangle(pen, rect);
        }

        private void SetupButtons()
        {
            buttonList = new List<VistaButtonEx>();

            for (int i = 1; i <= 15; i++)
            {
                string buttonName = "buttonExQuestion" + i;
                VistaButtonEx button = (VistaButtonEx)this.layoutControl3.Controls[buttonName];
                buttonList.Add(button);
            }
        }

        private void DeleteQuestionWithAnswer(QuestionClientData question)
        {
            bool found = false;
            int index = 0;
            while (found != true && index < questionsWithAnswers.Count)
            {
                ReportAnswerClientData answer = (ReportAnswerClientData)questionsWithAnswers[index];
                if (answer.QuestionCode == question.Code)
                    found = true;
                index = index + 1;
            }
            if (found == true)
            {
                ReportAnswerClientData answer = (ReportAnswerClientData)questionsWithAnswers[index - 1];
                DeleteQuestionHistory(answer);
                questionsWithAnswers.RemoveAt(index - 1);
            }
            if (this.AnswerUpdated != null)
                this.AnswerUpdated(this, EventArgs.Empty);
        }

        private void buttonAddIncidentType_Click(object sender, EventArgs e)
        {
            AddSelectedIncidentType(listBoxIncidentTypes.SelectedItems);
        }

        private void listBoxIncidentTypes_DoubleClick(object sender, EventArgs e)
        {
            AddSelectedIncidentType(listBoxIncidentTypes.SelectedItems);
        }

        private void buttonAddAllIncidentType_Click(object sender, EventArgs e)
        {
            ArrayList toAdd = new ArrayList();
            foreach (IncidentTypeClientData incidentType in listBoxIncidentTypes.Items)
                if (incidentType.NoEmergency != true && incidentType.Exclusive != true)
                    toAdd.Add(incidentType);
            AddSelectedIncidentType(toAdd);
        }

        private void AddSelectedIncidentType(IList selectedIncidentTypes)
        {
            ArrayList toDelete = new ArrayList();
            foreach (IncidentTypeClientData incident in selectedIncidentTypes)
            {
                if (listBoxSelectedIncidentTypes.Items.Count > 0)
                {
                    if ((((IncidentTypeClientData)listBoxSelectedIncidentTypes.Items[0]).Exclusive != true) && (incident.Exclusive != true))
                    {
                        toDelete.Add(incident);
                        incidentAdded = true;
                        listBoxSelectedIncidentTypes.Items.Add(incident);

                    }
                    else
                        incidentAdded = false;
                }
                else
                {
                    toDelete.Add(incident);
                    incidentAdded = true;
                    listBoxSelectedIncidentTypes.Items.Add(incident);
                }
            }
            foreach (IncidentTypeClientData incident in toDelete)
                listBoxIncidentTypes.Items.Remove(incident);
            ArrayList sortedSelectedIncidentTypes = new ArrayList(listBoxSelectedIncidentTypes.Items);
            sortedSelectedIncidentTypes.Sort(new IncidentTypeComparer());
            FillListBox(listBoxSelectedIncidentTypes, sortedSelectedIncidentTypes);
            ValidateButtonOk();
        }

        private void ValidateButtonOk()
        {
            if (listBoxSelectedIncidentTypes.Items.Count > 0)
                this.buttonOK.Enabled = true;
            else
                this.buttonOK.Enabled = false;
        }

        private void RemoveSelectedIncidentType(IList selectedIncidentTypes)
        {
            ArrayList toDelete = new ArrayList();
            foreach (IncidentTypeClientData incident in selectedIncidentTypes)
            {
                toDelete.Add(incident);
                listBoxIncidentTypes.Items.Add(incident);
            }
            foreach (IncidentTypeClientData incident in toDelete)
                listBoxSelectedIncidentTypes.Items.Remove(incident);
            ArrayList sortedIncidentTypes = new ArrayList(listBoxIncidentTypes.Items);
            sortedIncidentTypes.Sort(new IncidentTypeComparer());
            FillListBox(listBoxIncidentTypes, sortedIncidentTypes);
            ValidateButtonOk();
            if (removeIncident != null)
                removeIncident(selectedIncidentTypes);
        }

        private void buttonRemoveIncidentType_Click(object sender, EventArgs e)
        {
            ArrayList unselectedIncidentTypes = new ArrayList(listBoxSelectedIncidentTypes.SelectedItems);
            RemoveSelectedIncidentType(unselectedIncidentTypes);
            if (listBoxSelectedIncidentTypes.Items.Count == 0)
                FixedTexBox();
        }

        public void FixedTexBox()
        {
            textBoxSelectedIncidentTypes.Text = string.Empty;
            textBoxSelectedIncidentTypes.ToolTip = string.Empty;
            toolTipMain.RemoveAll();
        }

        private void buttonRemoveAllIncidentType_Click(object sender, EventArgs e)
        {
            ArrayList unselectedIncidentTypes = new ArrayList(listBoxSelectedIncidentTypes.Items);
            RemoveSelectedIncidentType(unselectedIncidentTypes);
            FixedTexBox();
        }

        private void listBoxSelectedIncidentTypes_DoubleClick(object sender, EventArgs e)
        {
            ArrayList unselectedIncidentTypes = new ArrayList(listBoxSelectedIncidentTypes.SelectedItems);
            RemoveSelectedIncidentType(unselectedIncidentTypes);
        }

        private void buttonIncidentTypes_Click(object sender, EventArgs e)
        {
            if (this.layoutControlItem29.Visibility == DevExpress.XtraLayout.Utils.LayoutVisibility.Always)
            {
                layoutControlItem9.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                this.layoutControlItem1.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                layoutControlItem29.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            }
        }

        private void FillSelectedIncidentTypeQuestions(IList selectedIncidentTypes)
        {
            //To fill with 0 all spaces to fill.
            //In the method AddQuestionWithAnswerInSpecificIndex we insert the question in there respective index.
            for (int i = 0; i < questionHistory.Count; i++)
                questionsWithAnswers.Add(i);

            foreach (IncidentTypeClientData incidentType in selectedIncidentTypes)
            {
                textBoxSelectedIncidentTypes.Text += incidentType.ToString() + ", ";

                foreach (IncidentTypeQuestionClientData incidentQuestion in incidentType.IncidentTypeQuestions)
                {
                    if (incidentQuestion.Question != null)
                    {
                        QuestionWithStatus questionWithStatusToAdd = new QuestionWithStatus(incidentQuestion.Question, QuestionStatusEnum.NotAsked, this);

                        if (isQuestionWithStatusAlreadyAdded(questionWithStatusToAdd) != true)
                        {
                            if (questionHistory.ContainsKey(incidentQuestion.Question.Code) == true)
                            {
                                questionWithStatusToAdd.QuestionStatus = QuestionStatusEnum.Asked;
                                AddQuestionWithAnswerInSpecificIndex(incidentQuestion.Question.Code);
                            }
                            else
                                questionWithStatusToAdd.QuestionStatus = QuestionStatusEnum.NotAsked;

                            selectedIncidentTypeQuestionsWithStatus.Add(questionWithStatusToAdd);
                        }
                    }
                    else
                        SmartLogger.Print("data inconsistente en el tipo de incidente " + incidentType.Name.ToString());
                }
            }

            if (questionsWithAnswers.Count > 0)
            {
                for (int i = 0; i < questionsWithAnswers.Count; i++)
                {
                    ReportAnswerClientData pracd = questionsWithAnswers[i] as ReportAnswerClientData;
                    if (pracd == null)
                    {
                        questionsWithAnswers.RemoveAt(i);
                        i--;
                    }
                }
                if (this.AnswerUpdated != null)
                    this.AnswerUpdated(this, EventArgs.Empty);
            }
            if (selectedIncidentTypeQuestionsWithStatus != null)
                selectedIncidentTypeQuestionsWithStatus.Sort(new QuestionWithStatusComparer());

            if (textBoxSelectedIncidentTypes.Text.Trim() != "")
                textBoxSelectedIncidentTypes.Text = textBoxSelectedIncidentTypes.Text.Substring(0, textBoxSelectedIncidentTypes.Text.Length - 2);

            textBoxSelectedIncidentTypes.ToolTip = textBoxSelectedIncidentTypes.Text;
        }

        private void AddQuestionWithAnswerInSpecificIndex(int questionCode)
        {
            List<int> questionCodes = new List<int>(questionHistory.Keys);
            questionsWithAnswers[questionCodes.IndexOf(questionCode)] = questionHistory[questionCode];
        }

        public void PressedButtonOk()
        {
            buttonOK_Click(null, null);
        }
        public void UpdateTextBoxSelectedIncidentTypes(ArrayList updateIncidentTextBox)
        {
            this.FixedTexBox();
            FillSelectedIncidentTypeQuestions(updateIncidentTextBox);
        }

        private string GetFocusedButtonName()
        {
            foreach (VistaButtonEx var in this.buttonList)
                if (var.Focused)
                    return var.Name;
            return string.Empty;
        }

        public void SetNextQuestion()
        {
            foreach (Control c in dragDropLayoutControl1.layoutControl.Controls)
            {
                if (c is TextBoxEx)
                {
                    TextBoxEx t = c as TextBoxEx;
                    if (t.Multiline == true)
                        return;
                }
            }
            
            string clickedButtonNumber = this.clickedButtonName.Remove(0, 16);
            string focusedButtonName = GetFocusedButtonName();

            if (string.IsNullOrEmpty(focusedButtonName) == false)
            {
                LoadButtonWithName(focusedButtonName);
                return;
            }

            int n = this.selectedIncidentTypeQuestionsWithStatus.Count;
            int nextButtonNumber = int.Parse(clickedButtonNumber) + 15 * numLayer;
            if (nextButtonNumber % 15 != 0)
            {
                nextButtonNumber = ((nextButtonNumber + 1) % (n + 1));
                if (nextButtonNumber == 0)
                    nextButtonNumber = LoadFirstQuestionPanel();
                int number = nextButtonNumber - buttonQuestionOffSetIndex;
                if (!GetQuestionButtonWithNumber(number).Enabled)
                {
                    LoadButtonWithNumber(number);
                    SetNextQuestion();
                }
                else
                {
                    LoadButtonWithNumber(number);
                }
            }
            else
            {
                nextButtonNumber = ((nextButtonNumber + 1) % (n + 1));
                if (nextButtonNumber == 0)
                    nextButtonNumber = LoadFirstQuestionPanel();
                else
                    buttonExForward_Click(null, null);
                LoadButtonWithNumber(1);
                SetNextQuestion();
            }
        }

        private int LoadFirstQuestionPanel()
        {
            buttonQuestionOffSetIndex = 0;
            if (this.buttonExBack.Enabled)
            {
                ButtonsChecker();
                FillButtonsWithQuestions();
                this.buttonExBack.Enabled = false;
            }
            numLayer = 0;
            return 1;
        }

        private void LoadButtonWithName(string buttonName)
        {
            VistaButtonEx button = (VistaButtonEx)this.layoutControl3.Controls[buttonName];
            buttonExQuestion1_Click(button, null);
        }

        private void LoadButtonWithNumber(int number)
        {
            string buttonName = "buttonExQuestion" + number;
            LoadButtonWithName(buttonName);
        }

        private VistaButtonEx GetQuestionButtonWithNumber(int number)
        {
            return (VistaButtonEx)this.layoutControl3.Controls["buttonExQuestion" + number];
        }

        public void ShowPanelSelected()
        {
            buttonOK_Click(new object(), new EventArgs());
        }
        private void buttonOK_Click(object sender, EventArgs e)
        {
            CleanAllButtons();
            //initializing variables...
            numLayer = 0;
            selectedIncidentTypeQuestionsWithStatus = new ArrayList();
            buttonQuestionOffSetIndex = 0;
            questionsWithAnswers = new ArrayList();
            textBoxSelectedIncidentTypes.Text = string.Empty;
            textBoxSelectedIncidentTypes.ToolTip = string.Empty;
            this.labelExLongQuestion.Text = string.Empty;

            FillSelectedIncidentTypeQuestions(listBoxSelectedIncidentTypes.Items);

            //interface...
            buttonExBack.Enabled = false;

            if (selectedIncidentTypeQuestionsWithStatus.Count > 16)
                buttonExForward.Enabled = true;
            else
                buttonExForward.Enabled = false;

            layoutControlItem9.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            this.layoutControlItem1.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            layoutControlItem29.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;

            dragDropLayoutControl1.layoutControl.BeginUpdate();
            for (int i = 0; i < dragDropLayoutControl1.layoutControl.Controls.Count; i++)
            {
                Control control = dragDropLayoutControl1.layoutControl.Controls[i];
                if ((answerIds != null && answerIds.ContainsKey(control.Name)) || control.Name.Equals("copyAddress") || control.Name.Equals("searchButton"))
                {
                    dragDropLayoutControl1.layoutControl.Controls.RemoveAt(i);
                    i--;
                }
            }
            while (dragDropLayoutControl1.layoutControl.Root.Items.Count > 0)
            {
                dragDropLayoutControl1.layoutControl.Root.Items.RemoveAt(0);
            }
            while (dragDropLayoutControl1.layoutControl.Items.Count > 1)
            {
                if (dragDropLayoutControl1.layoutControl.Items[0].Name.Equals(dragDropLayoutControl1.layoutControl.Root.Name) == true)
                    dragDropLayoutControl1.layoutControl.Items.RemoveAt(1);
            }
            dragDropLayoutControl1.layoutControl.EndUpdate();
            dragDropLayoutControl1.Enabled = true;
            FillButtonsWithQuestions();
            this.buttonExQuestion1.Focus();


            //setting the property that would be accessed from the front client...            
            this.SelectedIncidentTypes = new ArrayList(listBoxSelectedIncidentTypes.Items);
            //generating the event that would be catched by the front client...

            OnIncidentTypesSelected(EventArgs.Empty);
            SelectNextNotAskedQuestion(new VistaButtonEx());
        }

        protected virtual void OnIncidentTypesSelected(EventArgs e)
        {
            if (IncidentTypesSelected != null)
                IncidentTypesSelected(this, e);
        }

        private void FillListBox(ListBox listBoxToFill, ArrayList sortedList)
        {
            listBoxToFill.Items.Clear();
            foreach (IncidentTypeClientData incidentType in sortedList)
                listBoxToFill.Items.Add(incidentType);
        }

        private void buttonExForward_Click(object sender, EventArgs e)
        {
            ButtonsChecker();
            for (int i = 0; i < dragDropLayoutControl1.layoutControl.Controls.Count; i++)
            {
                Control control = dragDropLayoutControl1.layoutControl.Controls[i];
                if ((answerIds != null && answerIds.ContainsKey(control.Name)) || control.Name.Equals("copyAddress") || control.Name.Equals("searchButton"))
                {
                    dragDropLayoutControl1.layoutControl.Controls.RemoveAt(i);
                    i--;
                }
            }
            while (dragDropLayoutControl1.layoutControl.Root.Items.Count > 0)
            {
                dragDropLayoutControl1.layoutControl.Root.Items.RemoveAt(0);
            }
            while (dragDropLayoutControl1.layoutControl.Items.Count > 1)
            {
                if (dragDropLayoutControl1.layoutControl.Items[0].Name.Equals(dragDropLayoutControl1.layoutControl.Root.Name) == true)
                    dragDropLayoutControl1.layoutControl.Items.RemoveAt(1);
            }
            dragDropLayoutControl1.Enabled = true;
            buttonQuestionOffSetIndex = buttonQuestionOffSetIndex + 15;
            FillButtonsWithQuestions();
            buttonExBack.Enabled = true;
            numLayer++;
        }

        private void buttonExBack_Click(object sender, EventArgs e)
        {
            ButtonsChecker();
            for (int i = 0; i < dragDropLayoutControl1.layoutControl.Controls.Count; i++)
            {
                Control control = dragDropLayoutControl1.layoutControl.Controls[i];
                if ((answerIds != null && answerIds.ContainsKey(control.Name)) || control.Name.Equals("copyAddress") || control.Name.Equals("searchButton"))
                {
                    dragDropLayoutControl1.layoutControl.Controls.RemoveAt(i);
                    i--;
                }
            }
            while (dragDropLayoutControl1.layoutControl.Root.Items.Count > 0)
            {
                dragDropLayoutControl1.layoutControl.Root.Items.RemoveAt(0);
            }
            while (dragDropLayoutControl1.layoutControl.Items.Count > 1)
            {
                if (dragDropLayoutControl1.layoutControl.Items[0].Name.Equals(dragDropLayoutControl1.layoutControl.Root.Name) == true)
                    dragDropLayoutControl1.layoutControl.Items.RemoveAt(1);
            }
            dragDropLayoutControl1.Enabled = true;
            buttonQuestionOffSetIndex = buttonQuestionOffSetIndex - 15;
            FillButtonsWithQuestions();
            if (buttonQuestionOffSetIndex == 0)
            {
                buttonExBack.Enabled = false;
            }
            numLayer--;
        }

        private void SetFrontClientUpdatingCallState()
        {
            this.layoutControlItem1.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            this.layoutControlItem29.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            this.layoutControlItem9.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            this.buttonIncidentTypes.Enabled = false;

            foreach (ReportAnswerClientData pracd in questionsWithAnswers)
            {
                foreach (QuestionWithStatus qws in selectedIncidentTypeQuestionsWithStatus)
                {
                    if (qws.Question.Code == pracd.QuestionCode && qws.QuestionStatus != QuestionStatusEnum.NotAsked)
                    {
                        qws.IsPersisted = true;
                        break;
                    }
                }
            }
            FillButtonsWithQuestions();
        }

        private void SetFrontClientRegisteringCallState()
        {
            this.Enabled = true;
        }

        #region IncidentTypes Actions
        public void SetFrontClientChangedIncidentTypeState()
        {
            this.layoutControlItem9.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            this.layoutControlItem1.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            this.layoutControlItem29.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            this.buttonIncidentTypes.Enabled = true;
            ValidateButtonOk();
        }
        public void UpdateIncidentTClientData(ArrayList IncidentChanges)
        {
            for (int i = 0; i < listBoxIncidentTypes.Items.Count; i++)
            {
                for (int j = 0; j < IncidentChanges.Count; j++)
                {
                    if ((listBoxIncidentTypes.Items[i] as IncidentTypeClientData).Code ==
                        (IncidentChanges[j] as IncidentTypeClientData).Code)
                    {
                        (listBoxIncidentTypes.Items[i] as IncidentTypeClientData).IncidentTypeDepartmentTypeClients =
                            (IncidentChanges[j] as IncidentTypeClientData).IncidentTypeDepartmentTypeClients;
                    }
                }
            }
            for (int i = 0; i < listBoxSelectedIncidentTypes.Items.Count; i++)
            {
                for (int j = 0; j < IncidentChanges.Count; j++)
                {
                    if ((listBoxSelectedIncidentTypes.Items[i] as IncidentTypeClientData).Code ==
                        (IncidentChanges[j] as IncidentTypeClientData).Code)
                    {
                        (listBoxSelectedIncidentTypes.Items[i] as IncidentTypeClientData).IncidentTypeDepartmentTypeClients =
                            (IncidentChanges[j] as IncidentTypeClientData).IncidentTypeDepartmentTypeClients;
                    }
                }
            }
        }

        public void UpdateIncidentQuestions(ArrayList Changes)
        {
            string buttonName = string.Empty;
            VistaButtonEx button = null;
            bool firtsQuestion = true;
            for (int i = 0; i < Changes.Count; i++)
            {
                for (int j = 1; j < 16; j++)
                {
                    buttonName = "buttonExQuestion" + j;
                    button = (VistaButtonEx)this.layoutControl3.Controls[buttonName];

                    if (button.Tag != null)
                    {
                        QuestionWithStatus questionWithStatus = selectedIncidentTypeQuestionsWithStatus[(int)button.Tag] as QuestionWithStatus;

                        if (string.Compare((Changes[i] as QuestionClientData).ShortText, button.Text.Substring(0, button.Text.Length - 2)) == 0)
                        {
                            for (int k = 0; k < questionsWithAnswers.Count; k++)
                            {
                                if ((Changes[i] as QuestionClientData).Code ==
                                    (questionsWithAnswers[k] as ReportAnswerClientData).QuestionCode)
                                {
                                    questionsWithAnswers.RemoveAt(k);
                                }
                            }

                            if (firtsQuestion == true)
                            {
                                firtsQuestion = false;
                                buttonExQuestion1_Click(button, new EventArgs());
                                questionWithStatus.QuestionStatus = QuestionStatusEnum.Asking;
                                button.ButtonColor = questionWithStatus.StatusColor;
                            }
                            else
                            {
                                questionWithStatus.QuestionStatus = QuestionStatusEnum.NotAsked;
                                button.ButtonColor = questionWithStatus.StatusColor;
                            }
                        }
                    }
                }
            }
        }
        public void UpdateIncidentType(IncidentTypeClientData incidentType)
        {
            for (int i = 0; i < listBoxSelectedIncidentTypes.Items.Count; i++)
            {
                IncidentTypeClientData selectedIncidentTypeClientData = listBoxSelectedIncidentTypes.Items[i] as IncidentTypeClientData;
                if (selectedIncidentTypeClientData.Code == incidentType.Code)
                {
                    if (textBoxSelectedIncidentTypes.Text.IndexOf(selectedIncidentTypeClientData.ToString()) > -1)
                    {
                        textBoxSelectedIncidentTypes.Text = textBoxSelectedIncidentTypes.Text.Replace(selectedIncidentTypeClientData.ToString(), incidentType.ToString());
                    }
                    textBoxSelectedIncidentTypes.ToolTip = textBoxSelectedIncidentTypes.Text;
                    listBoxSelectedIncidentTypes.Items[i] = incidentType;
                    i = listBoxSelectedIncidentTypes.Items.Count;
                }
            }

            ArrayList sortedIncidentTypes = new ArrayList(listBoxIncidentTypes.Items);
            sortedIncidentTypes.Sort(new IncidentTypeComparer());
            FillListBox(listBoxIncidentTypes, sortedIncidentTypes);
            ValidateButtonOk();
        }

        public void DeleteIncidentType(IncidentTypeClientData incidentType)
        {
            listBoxSelectedIncidentTypes.Items.Remove(incidentType);

            string[] incTypes = textBoxSelectedIncidentTypes.Text.Trim().Split(',');
            textBoxSelectedIncidentTypes.Text = string.Empty;
            for (int i = 0; i < incTypes.Length; i++)
            {
                if (incTypes[i].ToString().Trim() != incidentType.ToString())
                    textBoxSelectedIncidentTypes.Text += incTypes[i].ToString().Trim() + ", ";
            }

            if (textBoxSelectedIncidentTypes.Text.Trim() != string.Empty)
                textBoxSelectedIncidentTypes.Text = textBoxSelectedIncidentTypes.Text.Substring(0, textBoxSelectedIncidentTypes.Text.Length - 2);
            textBoxSelectedIncidentTypes.ToolTip = textBoxSelectedIncidentTypes.Text;

            ArrayList sortedIncidentTypes = new ArrayList(listBoxIncidentTypes.Items);
            sortedIncidentTypes.Sort(new IncidentTypeComparer());
            FillListBox(listBoxIncidentTypes, sortedIncidentTypes);
            ValidateButtonOk();
        }
        #endregion

        #region Question Actions
        public void SetFrontClientChangedQuestionsState()
        {
            this.layoutControlItem9.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            this.layoutControlItem1.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            this.layoutControlItem29.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            this.buttonIncidentTypes.Enabled = true;
            ValidateButtonOk();
        }


        public void UpdateQuestion(QuestionClientData question)
        {
            foreach (IncidentTypeClientData itcd in SelectedIncidentTypes)
            {
                for (int i = 0; i < itcd.IncidentTypeQuestions.Count; i++)
                {
                    IncidentTypeQuestionClientData itqcd = itcd.IncidentTypeQuestions[i] as IncidentTypeQuestionClientData;
                    if (itqcd.Question != null && itqcd.Question.Code == question.Code)
                    {
                        itqcd.Question = question;
                    }
                }
            }
            buttonOK_Click(null, null);
        }

        public void DeleteQuestion(QuestionClientData question)
        {
            foreach (IncidentTypeClientData itcd in SelectedIncidentTypes)
            {
                for (int i = 0; i < itcd.IncidentTypeQuestions.Count; i++)
                {
                    IncidentTypeQuestionClientData itqcd = itcd.IncidentTypeQuestions[i] as IncidentTypeQuestionClientData;
                    if (itqcd.Question != null && itqcd.Question.Code == question.Code)
                    {
                        itcd.IncidentTypeQuestions.RemoveAt(i);
                        i--;
                    }
                }
            }
            buttonOK_Click(null, null);
        }
        #endregion
        private void SetFrontClientWaitingForCallState()
        {
            this.layoutControlItem9.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            this.layoutControlItem1.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            this.layoutControlItem29.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            this.buttonIncidentTypes.Enabled = true;
            this.buttonOK.Enabled = false;
            this.Enabled = false;
        }

        public override bool Active
        {
            get
            {
                return base.Active;
            }
            set
            {
                base.Active = value;

                Image img;

                img = pictureBoxNumber.Image;

                pictureBoxNumber.Image = ResourceLoader.GetImage(GetNumberImageName());

                if (img != null)
                    img.Dispose();
                if (Active)
                {
                    this.pictureBoxNumber.LookAndFeel.Style = GetStyle(ActiveColor);
                    this.groupControlBody.LookAndFeel.Style = GetStyle(ActiveColor);
                    this.layoutControl1.LookAndFeel.Style = GetStyle(ActiveColor);
                    this.layoutControl2.LookAndFeel.Style = GetStyle(ActiveColor);
                    this.layoutControl3.LookAndFeel.Style = GetStyle(ActiveColor);
                    this.dragDropLayoutControl1.layoutControl.LookAndFeel.Style = GetStyle(ActiveColor);
                }
                else
                {
                    this.pictureBoxNumber.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
                    this.groupControlBody.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
                    this.layoutControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
                    this.layoutControl2.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
                    this.layoutControl3.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
                    this.dragDropLayoutControl1.layoutControl.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
                    this.pictureBoxNumber.LookAndFeel.SkinName = InactiveColor;
                    this.groupControlBody.LookAndFeel.SkinName = InactiveColor;
                    this.layoutControl1.LookAndFeel.SkinName = InactiveColor;
                    this.layoutControl2.LookAndFeel.SkinName = InactiveColor;
                    this.layoutControl3.LookAndFeel.SkinName = InactiveColor;
                    this.dragDropLayoutControl1.layoutControl.LookAndFeel.SkinName = InactiveColor;
                }
            }
        }

        public new bool Focus()
        {
            return textBoxSelectedIncidentTypes.Focus();
            //return this.buttonIncidentTypes.Focus();
        }

        private Color incidentTypeLabelBorderColor;

        public Color IncidentTypeLabelBorderColor
        {
            get
            {
                return incidentTypeLabelBorderColor;
            }
            set
            {
                incidentTypeLabelBorderColor = value;
            }
        }


        private void questionControlButton_MouseHover(object sender, EventArgs e)
        {
            if (((Button)sender).Text.Equals("Copiar"))
            {

                toolTipMain.SetToolTip(((Button)sender), "Copiar");

            }
            else if (((Button)sender).Text.Equals("Buscar"))
            {

                toolTipMain.SetToolTip(((Button)sender), "Buscar");
            }
        }

        private void questionControlButton_MouseLeave(object sender, EventArgs e)
        {
            if (sender.GetType().Name.Equals("ButtonEx"))
                toolTipMain.Hide((Button)sender);
            else if (sender.GetType().Name.Equals("VistaButtonEx"))
            {
                VistaButtonEx c = ((VistaButtonEx)sender);
                if (((VistaButtonEx)sender).Focused)
                    ((VistaButtonEx)sender).HighlightColor = Color.SlateGray;
            }
        }

        private void listBoxIncidentTypes_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                listBoxIncidentTypes_DoubleClick(sender, null);
            }
        }
    }
}