using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using DevExpress.XtraGrid.Views.Base;
using Smartmatic.SmartCad.Service;
using DevExpress.XtraGrid.Views.Layout;
using DevExpress.XtraGrid.Views.Grid;
using System.Threading;
using DevExpress.XtraGrid.Columns;
using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadCore.ClientData;
using SmartCadControls.Controls;

namespace SmartCadGuiCommon.Controls
{
    public delegate void OperatorIndicatorCharEventHandler2(object sender, OperatorIndicatorChartEventArgs2 e);
    public partial class OperatorGridIndicatorControl2 : UserControl
    {
        public event OperatorIndicatorCharEventHandler2 operatorIndicatorEvent2;
        private BindingList<OperatorGridControlData2> dataSource;
        public OperatorGridIndicatorControl2()
        {
            InitializeComponent();
            LoadLanguage();
        }

      

        private void LoadLanguage()
        {
            gridColumnIndicator.Caption = ResourceLoader.GetString2("Operators");
            gridColumnValue.Caption = ResourceLoader.GetString2("RealValue"); 
            gridColumnPercentValue.Caption = ResourceLoader.GetString2("Percent");            
        }
        public BindingList<OperatorGridControlData2> DataSource
        {
            get
            {
                return dataSource;
            }
            set
            {
                dataSource = value;
                FormUtil.InvokeRequired(this.gridControl1, delegate
                {
                    this.gridControl1.DataSource = dataSource;
                });
            }
        }
        public void Load(IList list)
        {
            BindingList<OperatorGridControlData2> listValue = new BindingList<OperatorGridControlData2>();            
            foreach (IndicatorResultValuesClientData value in list)
            {
                OperatorGridControlData2 data = new OperatorGridControlData2(value);
                if (listValue.Contains(data) == false)
                {
                    listValue.Add(data);
                }
            }
            DataSource = listValue;

            OperatorIndicatorChartEventArgs2 eventArgs =
                new OperatorIndicatorChartEventArgs2();
            eventArgs.List = new ArrayList();
            eventArgs.List = listValue;
            if (operatorIndicatorEvent2 != null)
            {
                operatorIndicatorEvent2(this, eventArgs);
            }
        }
        public void AddOrUpdate(IndicatorResultValuesClientData result)
        {
            if (DataSource == null)
            {
                DataSource = new BindingList<OperatorGridControlData2>();
            }
            else
            {
                BindingList<OperatorGridControlData2> dataSourceAux =
                    new BindingList<OperatorGridControlData2>();

                foreach (OperatorGridControlData2 ope in dataSource)
                {
                    dataSourceAux.Add(ope);
                }
                foreach (OperatorGridControlData2 ope in dataSourceAux)
                {
                    if (result.Date > ope.Date)
                    {
                        FormUtil.InvokeRequired(this.gridControl1, delegate
                        {

                            gridControl1.DeleteItem(ope);
                        });
                    }

                }
            }


            OperatorGridControlData2 indicatorGridControlData = new OperatorGridControlData2(result);           
            int index = dataSource.IndexOf(indicatorGridControlData);

            //if (((result.IndicatorCustomCode == "PN01") ||
            //    (result.IndicatorCustomCode == "PN03")||
            //    (result.IndicatorCustomCode == "PN15")||
            //    (result.IndicatorCustomCode == "PN16")||
            //    (result.IndicatorCustomCode == "PN17")||
            //    (result.IndicatorCustomCode == "PN18")||
            //    (result.IndicatorCustomCode == "D20")||
            //    (result.IndicatorCustomCode == "D34") ||
            //    (result.IndicatorCustomCode == "D35") ||
            //    (result.IndicatorCustomCode == "D36")||
            //    (result.IndicatorCustomCode == "D37")||
            //    (result.IndicatorCustomCode == "D38"))
            //    &&(dataSource.Count >0 ))

            if (((result.Description == ResourceLoader.GetString2("IndicatorD20Name")) ||
              (result.Description == ResourceLoader.GetString2("IndicatorD34Name")) ||
              (result.Description == ResourceLoader.GetString2("IndicatorD35Name")) ||
              (result.Description == ResourceLoader.GetString2("IndicatorD36Name")) ||
              (result.Description == ResourceLoader.GetString2("IndicatorD37Name")) ||
              (result.Description == ResourceLoader.GetString2("IndicatorD38Name"))) 
              && (dataSource.Count > 0))
            {

                int dataIndex = 0;
                
                for(int i=0; i< dataSource.Count; i++)
                {
                    OperatorGridControlData2 op = dataSource[i];
                    //if ((op.IndicatorCustomCode == "PN04") ||
                    //     (op.IndicatorCustomCode == "D22"))
                    if (op.Name == ResourceLoader.GetString2("IndicatorD22Name"))
                    {
                        dataIndex = i;
                        break;
                    }
                }
                
               
                if (dataSource[dataIndex].ListResults == null)
                {
                    dataSource[dataIndex].ListResults = new List<OperatorGridControlData2>();
                }
                int listResultsIndex = dataSource[dataIndex].ListResults.IndexOf(indicatorGridControlData);
                if (listResultsIndex > -1)
                {
                   dataSource[dataIndex].ListResults[listResultsIndex] = new OperatorGridControlData2(result);

                }
                else
                {
                    dataSource[dataIndex].ListResults.Add(new OperatorGridControlData2(result));
                }

                if (gridControl1.Views.Count > 1)
                    ((GridView)gridControl1.Views[1]).RefreshData();
            }
            else if (index > -1) 
               
            {
                OperatorGridControlData2 data = dataSource[index];

                List<double> values = ApplicationUtil.ConvertIndicatorResultValuesFromString(
              result.ResultValue);

               

                data.Name = result.IndicatorName;
                data.IndicatorResultCode = result.IndicatorResultCode;
                data.IndicatorResultClassCode = result.IndicatorResultClassCode;
                data.RealValue = values[0].ToString();
                data.PercentValue = values[1].ToString();
                FormUtil.InvokeRequired(this.gridControl1, delegate
                {
                    dataSource[index] = data;
                });
           
            }
            //else if( (result.IndicatorCustomCode == "PN04") ||
            //    (result.IndicatorCustomCode == "D22") ||
            //    (result.IndicatorCustomCode == "PN05") ||
            //    (result.IndicatorCustomCode == "D23"))
            else if ((result.Description == ResourceLoader.GetString2("IndicatorD22Name")) ||
                (result.Description == ResourceLoader.GetString2("IndicatorD23Name")))
            {
                FormUtil.InvokeRequired(this.gridControl1, delegate
                {
                    dataSource.Add(new OperatorGridControlData2(result));
                });
            }

            OperatorIndicatorChartEventArgs2 eventArgs =
                new OperatorIndicatorChartEventArgs2();
            eventArgs.List = new ArrayList();
            eventArgs.List = dataSource;
            if (operatorIndicatorEvent2 != null)
            {
                operatorIndicatorEvent2(this, eventArgs);
            }
        }

        private void gridView1_DragObjectOver(object sender, DragObjectOverEventArgs e)
        {
            GridColumn column = e.DragObject as GridColumn;
            if (column != null && e.DropInfo.Index < 0)
            {
                e.DropInfo.Valid = false;
            }
        }
    }

    public class OperatorGridControlData2: GridControlData
    {
        private string realValue;
        private string percentValue;      
        private string name;
        private int indicatorResultCode;
        private int indicatorResultClassCode;
        private string indicatorCustomCode;
        private DateTime date;
        private List<OperatorGridControlData2> listResults = new List<OperatorGridControlData2>();        

        public OperatorGridControlData2(IndicatorResultValuesClientData indicatorResultValuesClientData): base(indicatorResultValuesClientData)
        {
            List<double> values = ApplicationUtil.ConvertIndicatorResultValuesFromString(
                indicatorResultValuesClientData.ResultValue);

            realValue = values[0].ToString();
            if (values.Count > 1)
            {
                percentValue = values[1].ToString();
            }
            name = indicatorResultValuesClientData.Description;//indicatorResultValuesClientData.IndicatorName;
            indicatorResultCode = indicatorResultValuesClientData.IndicatorResultCode;
            indicatorCustomCode = indicatorResultValuesClientData.IndicatorCustomCode;
            indicatorResultClassCode = indicatorResultValuesClientData.IndicatorResultClassCode;
            date = indicatorResultValuesClientData.Date;

        }
        public string IndicatorCustomCode
        {
            get
            {
                return indicatorCustomCode;
            }

            set
            {
                indicatorCustomCode = value;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }
        public string RealValue
        {
            get
            {
                return realValue;
            }
            set 
            {
                realValue = value;
            }
        }
        public string PercentValue
        {
            get
            {
                return percentValue;
            }
            set
            {
                percentValue = value;
            }
        }

        public List<OperatorGridControlData2> ListResults 
        {
            get { return listResults; }
            set { listResults = value; }
        }


        public int IndicatorResultCode
        {
            get
            {
                return indicatorResultCode;
            }

            set
            {
                indicatorResultCode = value;
            }
        }
        public int IndicatorResultClassCode
        {
            get
            {
                return indicatorResultClassCode;
            }
            set
            {
                indicatorResultClassCode = value;
            }
        }
        public DateTime Date
        {
            get
            {
                return date;
            }

            set
            {
                date = value;
            }
        }
        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is OperatorGridControlData2)
            {
                result = this.Name == ((OperatorGridControlData2)obj).Name;
            }
            return result;
        }
    }


    public class OperatorIndicatorChartEventArgs2
    {
        private IList list;
        public IList List
        {
            get
            {
                return list;
            }
            set
            {
                list = value;
            }
        }
    }


}
