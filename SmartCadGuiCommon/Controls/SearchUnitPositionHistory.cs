using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using DevExpress.Data.Filtering;
using SmartCadCore.Common;
using SmartCadGuiCommon;

namespace SmartCadGuiCommon.Controls
{
	public partial class SearchUnitPositionHistory : DevExpress.XtraEditors.XtraUserControl
	{
		public SearchUnitPositionHistory()
		{
			InitializeComponent();
            dateEditStart.DateTime = DateTime.Now.AddDays(-1);
            dateEditEnd.DateTime = DateTime.Now;
            dateEditEnd.Properties.MinValue = dateEditStart.DateTime;
            dateEditEnd.Properties.MaxValue = DateTime.Now;
            dateEditStart.Properties.MaxValue = DateTime.Now;
            SearchUnitPositionHistoryControl_ParametersChanged();
			LoadLanguage();
        }

		private void LoadLanguage()
		{
            layoutControlItemStartDate.Text = ResourceLoader.GetString2("StartDate");
            layoutControlItemEndDate.Text = ResourceLoader.GetString2("EndDate");
			simpleButtonSearch.Text = ResourceLoader.GetString2("Search");
		}

		private void simpleButtonSearch_Click(object sender, EventArgs e)
		{
            DateTime start = (DateTime)dateEditStart.EditValue;
            DateTime end = (DateTime)dateEditEnd.EditValue;
            (this.ParentForm as UnitTrackingForm).DrawUnitsTrack(start,end);
		}

        private void SearchUnitPositionHistoryControl_ParametersChanged()
        {
            simpleButtonSearch.Enabled = dateEditEnd.DateTime > dateEditStart.DateTime;
        }

        private void dateEditEnd_EditValueChanged(object sender, EventArgs e)
        {
            dateEditEnd.Properties.MinValue = dateEditStart.DateTime;
            dateEditEnd.Properties.MaxValue = DateTime.Now;
            if (dateEditEnd.DateTime < dateEditStart.DateTime)
            {
                dateEditEnd.DateTime = dateEditStart.DateTime;
            }
            SearchUnitPositionHistoryControl_ParametersChanged();
        }

        private void dateEdit_EditValueChanged(object sender, EventArgs e)
        {
            dateEditEnd_EditValueChanged(null, null);
            SearchUnitPositionHistoryControl_ParametersChanged();
        }
	}
}
