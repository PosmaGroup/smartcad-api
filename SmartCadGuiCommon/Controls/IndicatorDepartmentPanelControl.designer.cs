namespace SmartCadGuiCommon.Controls
{
    partial class IndicatorDepartmentPanelControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControlIndicatorDepartment = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.operatorIndicatorControl21 = new OperatorIndicatorControl2();
            this.newDispatchOrderGridIndicatorControl1 = new NewDispatchOrderGridIndicatorControl();
            this.dispatchOrderImprtantStatusIndicatorControl1 = new DispatchOrderImprtantStatusIndicatorControl();
            this.incidentIndicatorControl1 = new IncidentIndicatorControl();
            this.layoutControlItemOperatorIndicator = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemDispatchOrderIndicator = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemIncidentIndicator = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemNewDispatchOrder = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlIndicatorDepartment)).BeginInit();
            this.layoutControlIndicatorDepartment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOperatorIndicator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDispatchOrderIndicator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIncidentIndicator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemNewDispatchOrder)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControlIndicatorDepartment
            // 
            this.layoutControlIndicatorDepartment.Appearance.DisabledLayoutGroupCaption.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControlIndicatorDepartment.Appearance.DisabledLayoutGroupCaption.Options.UseForeColor = true;
            this.layoutControlIndicatorDepartment.Appearance.DisabledLayoutItem.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControlIndicatorDepartment.Appearance.DisabledLayoutItem.Options.UseForeColor = true;
            this.layoutControlIndicatorDepartment.Controls.Add(this.operatorIndicatorControl21);
            this.layoutControlIndicatorDepartment.Controls.Add(this.newDispatchOrderGridIndicatorControl1);
            this.layoutControlIndicatorDepartment.Controls.Add(this.dispatchOrderImprtantStatusIndicatorControl1);
            this.layoutControlIndicatorDepartment.Controls.Add(this.incidentIndicatorControl1);
            this.layoutControlIndicatorDepartment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlIndicatorDepartment.Location = new System.Drawing.Point(0, 0);
            this.layoutControlIndicatorDepartment.Name = "layoutControlIndicatorDepartment";
            this.layoutControlIndicatorDepartment.Root = this.layoutControlGroup1;
            this.layoutControlIndicatorDepartment.Size = new System.Drawing.Size(1270, 345);
            this.layoutControlIndicatorDepartment.TabIndex = 4;
            this.layoutControlIndicatorDepartment.Text = "layoutControl1";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemOperatorIndicator,
            this.layoutControlItemDispatchOrderIndicator,
            this.layoutControlItemIncidentIndicator,
            this.layoutControlItemNewDispatchOrder});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1270, 345);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // operatorIndicatorControl21
            // 
            this.operatorIndicatorControl21.Location = new System.Drawing.Point(4, 4);
            this.operatorIndicatorControl21.LookAndFeel.SkinName = "Blue";
            this.operatorIndicatorControl21.LookAndFeel.UseDefaultLookAndFeel = false;
            this.operatorIndicatorControl21.Name = "operatorIndicatorControl21";
            this.operatorIndicatorControl21.Size = new System.Drawing.Size(629, 166);
            this.operatorIndicatorControl21.TabIndex = 0;
            // 
            // newDispatchOrderGridIndicatorControl1
            // 
            this.newDispatchOrderGridIndicatorControl1.DataSource = null;
            this.newDispatchOrderGridIndicatorControl1.Location = new System.Drawing.Point(638, 175);
            this.newDispatchOrderGridIndicatorControl1.Name = "newDispatchOrderGridIndicatorControl1";
            this.newDispatchOrderGridIndicatorControl1.Size = new System.Drawing.Size(629, 167);
            this.newDispatchOrderGridIndicatorControl1.TabIndex = 3;
            // 
            // dispatchOrderImprtantStatusIndicatorControl1
            // 
            this.dispatchOrderImprtantStatusIndicatorControl1.Location = new System.Drawing.Point(638, 4);
            this.dispatchOrderImprtantStatusIndicatorControl1.LookAndFeel.SkinName = "Blue";
            this.dispatchOrderImprtantStatusIndicatorControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.dispatchOrderImprtantStatusIndicatorControl1.Name = "dispatchOrderImprtantStatusIndicatorControl1";
            this.dispatchOrderImprtantStatusIndicatorControl1.Size = new System.Drawing.Size(629, 166);
            this.dispatchOrderImprtantStatusIndicatorControl1.TabIndex = 2;
            // 
            // incidentIndicatorControl1
            // 
            this.incidentIndicatorControl1.Location = new System.Drawing.Point(4, 175);
            this.incidentIndicatorControl1.LookAndFeel.SkinName = "Blue";
            this.incidentIndicatorControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.incidentIndicatorControl1.Name = "incidentIndicatorControl1";
            this.incidentIndicatorControl1.Size = new System.Drawing.Size(629, 167);
            this.incidentIndicatorControl1.TabIndex = 1;
            // 
            // layoutControlItemOperatorIndicator
            // 
            this.layoutControlItemOperatorIndicator.Control = this.operatorIndicatorControl21;
            this.layoutControlItemOperatorIndicator.CustomizationFormText = "layoutControlItemOperatorIndicator";
            this.layoutControlItemOperatorIndicator.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemOperatorIndicator.Name = "layoutControlItemOperatorIndicator";
            this.layoutControlItemOperatorIndicator.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItemOperatorIndicator.Size = new System.Drawing.Size(634, 171);
            this.layoutControlItemOperatorIndicator.Text = "layoutControlItemOperatorIndicator";
            this.layoutControlItemOperatorIndicator.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemOperatorIndicator.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemOperatorIndicator.TextToControlDistance = 0;
            this.layoutControlItemOperatorIndicator.TextVisible = false;
            // 
            // layoutControlItemDispatchOrderIndicator
            // 
            this.layoutControlItemDispatchOrderIndicator.Control = this.dispatchOrderImprtantStatusIndicatorControl1;
            this.layoutControlItemDispatchOrderIndicator.CustomizationFormText = "layoutControlItemDispatchOrderIndicator";
            this.layoutControlItemDispatchOrderIndicator.Location = new System.Drawing.Point(634, 0);
            this.layoutControlItemDispatchOrderIndicator.Name = "layoutControlItemDispatchOrderIndicator";
            this.layoutControlItemDispatchOrderIndicator.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItemDispatchOrderIndicator.Size = new System.Drawing.Size(634, 171);
            this.layoutControlItemDispatchOrderIndicator.Text = "layoutControlItemDispatchOrderIndicator";
            this.layoutControlItemDispatchOrderIndicator.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemDispatchOrderIndicator.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemDispatchOrderIndicator.TextToControlDistance = 0;
            this.layoutControlItemDispatchOrderIndicator.TextVisible = false;
            // 
            // layoutControlItemIncidentIndicator
            // 
            this.layoutControlItemIncidentIndicator.Control = this.incidentIndicatorControl1;
            this.layoutControlItemIncidentIndicator.CustomizationFormText = "layoutControlItemIncidentIndicator";
            this.layoutControlItemIncidentIndicator.Location = new System.Drawing.Point(0, 171);
            this.layoutControlItemIncidentIndicator.Name = "layoutControlItemIncidentIndicator";
            this.layoutControlItemIncidentIndicator.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItemIncidentIndicator.Size = new System.Drawing.Size(634, 172);
            this.layoutControlItemIncidentIndicator.Text = "layoutControlItemIncidentIndicator";
            this.layoutControlItemIncidentIndicator.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemIncidentIndicator.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemIncidentIndicator.TextToControlDistance = 0;
            this.layoutControlItemIncidentIndicator.TextVisible = false;
            // 
            // layoutControlItemNewDispatchOrder
            // 
            this.layoutControlItemNewDispatchOrder.Control = this.newDispatchOrderGridIndicatorControl1;
            this.layoutControlItemNewDispatchOrder.CustomizationFormText = "layoutControlItemNewDispatchOrder";
            this.layoutControlItemNewDispatchOrder.Location = new System.Drawing.Point(634, 171);
            this.layoutControlItemNewDispatchOrder.Name = "layoutControlItemNewDispatchOrder";
            this.layoutControlItemNewDispatchOrder.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItemNewDispatchOrder.Size = new System.Drawing.Size(634, 172);
            this.layoutControlItemNewDispatchOrder.Text = "layoutControlItemNewDispatchOrder";
            this.layoutControlItemNewDispatchOrder.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemNewDispatchOrder.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemNewDispatchOrder.TextToControlDistance = 0;
            this.layoutControlItemNewDispatchOrder.TextVisible = false;
            // 
            // IndicatorDepartmentPanelControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControlIndicatorDepartment);
            this.LookAndFeel.SkinName = "Blue";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "IndicatorDepartmentPanelControl";
            this.Size = new System.Drawing.Size(1270, 345);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlIndicatorDepartment)).EndInit();
            this.layoutControlIndicatorDepartment.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOperatorIndicator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDispatchOrderIndicator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIncidentIndicator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemNewDispatchOrder)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal OperatorIndicatorControl2 operatorIndicatorControl21;
        internal IncidentIndicatorControl incidentIndicatorControl1;
        internal DispatchOrderImprtantStatusIndicatorControl dispatchOrderImprtantStatusIndicatorControl1;
        internal NewDispatchOrderGridIndicatorControl newDispatchOrderGridIndicatorControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControlIndicatorDepartment;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemOperatorIndicator;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDispatchOrderIndicator;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemIncidentIndicator;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemNewDispatchOrder;



    }
}
