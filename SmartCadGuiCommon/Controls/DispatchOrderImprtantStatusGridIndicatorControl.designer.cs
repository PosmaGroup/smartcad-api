using SmartCadControls;
namespace SmartCadGuiCommon.Controls
{
    partial class DispatchOrderImprtantStatusGridIndicatorControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControl1 = new GridControlEx();
            this.gridView1 = new GridViewEx();
            this.gridColumnIndicator = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnPercentValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemPictureEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Name = "";
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.LookAndFeel.SkinName = "Blue";
            this.gridControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPictureEdit1});
            this.gridControl1.Size = new System.Drawing.Size(264, 137);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnIndicator,
            this.gridColumnValue,
            this.gridColumnPercentValue});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupFormat = "[#image]{1} {2}";
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsCustomization.AllowFilter = false;
            this.gridView1.OptionsMenu.EnableColumnMenu = false;
            this.gridView1.OptionsMenu.EnableFooterMenu = false;
            this.gridView1.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gridView1.OptionsSelection.UseIndicatorForSelection = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.DragObjectOver += new DevExpress.XtraGrid.Views.Base.DragObjectOverEventHandler(this.gridView1_DragObjectOver);
            // 
            // gridColumnIndicator
            // 
            this.gridColumnIndicator.Caption = "Type";
            this.gridColumnIndicator.FieldName = "Name";
            this.gridColumnIndicator.Name = "gridColumnIndicator";
            this.gridColumnIndicator.OptionsColumn.AllowEdit = false;
            this.gridColumnIndicator.OptionsColumn.ReadOnly = true;
            this.gridColumnIndicator.Visible = true;
            this.gridColumnIndicator.VisibleIndex = 0;
            // 
            // gridColumnValue
            // 
            this.gridColumnValue.Caption = "Amount";
            this.gridColumnValue.FieldName = "RealValue";
            this.gridColumnValue.Name = "gridColumnValue";
            this.gridColumnValue.OptionsColumn.AllowEdit = false;
            this.gridColumnValue.OptionsColumn.ReadOnly = true;
            this.gridColumnValue.Visible = true;
            this.gridColumnValue.VisibleIndex = 1;
            // 
            // gridColumnPercentValue
            // 
            this.gridColumnPercentValue.Caption = "%";
            this.gridColumnPercentValue.FieldName = "PercentValue";
            this.gridColumnPercentValue.Name = "gridColumnPercentValue";
            this.gridColumnPercentValue.OptionsColumn.AllowEdit = false;
            this.gridColumnPercentValue.OptionsColumn.ReadOnly = true;
            this.gridColumnPercentValue.Visible = true;
            this.gridColumnPercentValue.VisibleIndex = 2;
            // 
            // repositoryItemPictureEdit1
            // 
            this.repositoryItemPictureEdit1.Name = "repositoryItemPictureEdit1";
            // 
            // IncidentGridIndicatorControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl1);
            this.Name = "IncidentGridIndicatorControl";
            this.Size = new System.Drawing.Size(264, 137);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        //internal DevExpress.XtraGrid.GridControl gridControl1;        
        internal GridControlEx gridControl1;
        //private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private GridViewEx gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnIndicator;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnValue;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnPercentValue;
    }
}
