using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraCharts;


namespace SmartCadGuiCommon.Controls
{
    public partial class IndicatorGroupFirstLevelPanelControl : DevExpress.XtraEditors.XtraUserControl
    {
        public IndicatorGroupFirstLevelPanelControl()
        {
            InitializeComponent();
            if (this.emergencyIndicatorControl1 != null) 
            {

                this.emergencyIndicatorControl1.emergencyGridIndicatorControl1.Size = new Size(255, 145);
                this.emergencyIndicatorControl1.chartControl1.Location = 
                    new Point(255,this.emergencyIndicatorControl1.chartControl1.Location.Y-3);
                this.emergencyIndicatorControl1.chartControl1.Size = new Size(303, 145);
                this.emergencyIndicatorControl1.Size = new Size(558, 145);
               

            }
        }

    }
}
