using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using DevExpress.XtraGrid.Views.Base;
using Smartmatic.SmartCad.Service;
using DevExpress.XtraGrid.Views.Layout;
using DevExpress.XtraGrid.Views.Grid;
using System.Threading;
using DevExpress.XtraGrid.Columns;
using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadCore.ClientData;
using SmartCadControls.Controls;

namespace SmartCadGuiCommon.Controls
{
    public delegate void DispatchOrderImprtantStatusIndicatorCharEventHandler(object sender, DispatchOrderImprtantStatusIndicatorChartEventArgs e);
    public partial class DispatchOrderImprtantStatusGridIndicatorControl : UserControl
    {
        public event DispatchOrderImprtantStatusIndicatorCharEventHandler DispatchOrderImprtantStatusIndicatorEvent;
        private BindingList<DispatchOrderImprtantStatusGridControlData> dataSource;
        public DispatchOrderImprtantStatusGridIndicatorControl()
        {
            InitializeComponent();
            LoadLanguage();
        }

      

        private void LoadLanguage()
        {
            gridColumnIndicator.Caption = ResourceLoader.GetString2("DispatchesRequest");            
            gridColumnValue.Caption = ResourceLoader.GetString2("RealValue");
            gridColumnPercentValue.Caption = ResourceLoader.GetString2("Percent");            
        }
        public BindingList<DispatchOrderImprtantStatusGridControlData> DataSource
        {
            get
            {
                return dataSource;
            }
            set
            {
                dataSource = value;
                FormUtil.InvokeRequired(this.gridControl1, delegate
                {
                    this.gridControl1.DataSource = dataSource;
                });
            }
        }
        public void Load(IList list)
        {
            BindingList<DispatchOrderImprtantStatusGridControlData> listValue = new BindingList<DispatchOrderImprtantStatusGridControlData>();            
            foreach (IndicatorResultValuesClientData value in list)
            {
                DispatchOrderImprtantStatusGridControlData data = new DispatchOrderImprtantStatusGridControlData(value);
                if (listValue.Contains(data) == false)
                {
                    listValue.Add(data);
                }
            }
            DataSource = listValue;

            DispatchOrderImprtantStatusIndicatorChartEventArgs eventArgs =
                new DispatchOrderImprtantStatusIndicatorChartEventArgs();
            eventArgs.List = new ArrayList(listValue);
            //eventArgs.List = listValue;
            if (DispatchOrderImprtantStatusIndicatorEvent != null)
            {
                DispatchOrderImprtantStatusIndicatorEvent(this, eventArgs);
            }
        }
        public void AddOrUpdate(IndicatorResultValuesClientData result)
        {
            if (DataSource == null)
            {
                DataSource = new BindingList<DispatchOrderImprtantStatusGridControlData>();
            }
            else
            {
                BindingList<DispatchOrderImprtantStatusGridControlData> dataSourceAux = 
                    new BindingList<DispatchOrderImprtantStatusGridControlData>();

                foreach (DispatchOrderImprtantStatusGridControlData dis in dataSource)
                {
                    dataSourceAux.Add(dis);
                }
                foreach (DispatchOrderImprtantStatusGridControlData dis in dataSourceAux)
                {
                    if (result.Date > dis.Date)
                    {
                        FormUtil.InvokeRequired(this.gridControl1, delegate
              {
                  
                  gridControl1.DeleteItem(dis);                
              });
                    }

                }
            }

            DispatchOrderImprtantStatusGridControlData indicatorGridControlData = new DispatchOrderImprtantStatusGridControlData(result);
            DispatchOrderImprtantStatusGridControlData indicatorSelected = null;
            int[] rowIndexSelected = this.gridView1.GetSelectedRows();
            if (rowIndexSelected.Length > 0)
            {
                indicatorSelected = (gridView1.GetRow(rowIndexSelected[0])
                   as DispatchOrderImprtantStatusGridControlData);
            }
            int index = dataSource.IndexOf(indicatorGridControlData);
            if (index > -1)
            {
                FormUtil.InvokeRequired(this.gridControl1, delegate
               {
                   dataSource[index] = new DispatchOrderImprtantStatusGridControlData(result);
               });
                
            }
            else
            {
                FormUtil.InvokeRequired(this.gridControl1, delegate
               {
                   dataSource.Add(new DispatchOrderImprtantStatusGridControlData(result));
               });
            }

            DispatchOrderImprtantStatusIndicatorChartEventArgs eventArgs =
                new DispatchOrderImprtantStatusIndicatorChartEventArgs();
            eventArgs.List = new ArrayList(dataSource);
            //eventArgs.List = dataSource;
            if (DispatchOrderImprtantStatusIndicatorEvent != null)
            {
                DispatchOrderImprtantStatusIndicatorEvent(this, eventArgs);
            }
        }

        private void gridView1_DragObjectOver(object sender, DragObjectOverEventArgs e)
        {
            GridColumn column = e.DragObject as GridColumn;
            if (column != null && e.DropInfo.Index < 0)
            {
                e.DropInfo.Valid = false;
            }

        }
        //private void gridView1_FocusedRowChanged_1(object sender, FocusedRowChangedEventArgs e)
        //{

            //if (e.FocusedRowHandle >= 0)
            //{
            //    IncidentGridControlData rowSelected = (gridView1.GetRow(e.FocusedRowHandle)
            //        as IncidentGridControlData);
            //    DateTime temp = ServerServiceClient.GetInstance().GetTimeFromDB();
            //    int hour;
            //    if (temp.Hour > 0)
            //        hour = temp.Hour - 1;
            //    else
            //        hour = temp.Hour;
            //    DateTime toDay = new DateTime(temp.Year, temp.Month, temp.Day, hour, temp.Minute, temp.Second);
            //    IList indicators = ServerServiceClient.GetInstance().SearchClientObjects(
            //       SmartCadHqls.GetCustomHql(SmartCadHqls.GetAllIndicatorsResultByindicatorCode,
            //       rowSelected.IndicatorResultCode,
            //       rowSelected.IndicatorResultClassCode,
            //       toDay.ToString(ApplicationUtil.DataBaseFormattedDate)),
            //       typeof(IndicatorResultClientData));

            //    IncidentIndicatorChartEventArgs eventArgs =
            //        new IIncidentIndicatorChartEventArgs();
            //    eventArgs.List = new ArrayList();
            //    eventArgs.List = indicators;


            //    if (FocusedRowChangedEvent != null)
            //    {
            //        FocusedRowChangedEvent(this, eventArgs);
            //    }
            //}

        //}

         
    }

    public class DispatchOrderImprtantStatusGridControlData: GridControlData
    {
        private string realValue;
        private string percentValue;
        private string name;
        private int indicatorResultCode;
        private int indicatorResultClassCode;
        private DateTime date;

        public DispatchOrderImprtantStatusGridControlData(IndicatorResultValuesClientData indicatorResultValuesClientData): 
            base (indicatorResultValuesClientData)
        {
            List<double> values =  ApplicationUtil.ConvertIndicatorResultValuesFromString(
                indicatorResultValuesClientData.ResultValue);

            realValue = values[0].ToString();
            percentValue = values[1].ToString();
            name = indicatorResultValuesClientData.Description;
            indicatorResultCode = indicatorResultValuesClientData.IndicatorResultCode;
            indicatorResultClassCode = indicatorResultValuesClientData.IndicatorResultClassCode;
            date = indicatorResultValuesClientData.Date;

        }
        
        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }
        public string RealValue
        {
            get
            {
                return realValue;
            }
            set 
            {
                realValue = value;
            }
        }
        public string PercentValue
        {
            get
            {
                return percentValue;
            }
            set
            {
                percentValue = value;
            }
        }
        public int IndicatorResultCode
        {
            get
            {
                return indicatorResultCode;
            }

            set
            {
                indicatorResultCode = value;
            }
        }
        public int IndicatorResultClassCode
        {
            get
            {
                return indicatorResultClassCode;
            }
            set
            {
                indicatorResultClassCode = value;
            }
        }
        public DateTime Date
        {
            get
            {
                return date;
            }

            set
            {
                date = value;
            }
        }
        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is DispatchOrderImprtantStatusGridControlData)
            {
                result = this.Name == ((DispatchOrderImprtantStatusGridControlData)obj).Name;
            }
            return result;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        //public override string PreviewFieldValue
        //{
        //    get
        //    {
        //        return base.PreviewFieldValue;
        //    }
        //    set
        //    {
        //        base.PreviewFieldValue = value;
        //    }
        //}
    }

    public class DispatchOrderImprtantStatusIndicatorChartEventArgs
    {
        private IList list;
        public IList List
        {
            get
            {
                return list;
            }
            set
            {
                list = value;
            }
        }
    }


}
