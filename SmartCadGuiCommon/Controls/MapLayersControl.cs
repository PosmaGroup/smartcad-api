﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Threading;
using System.Reflection;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using System.Collections;
using SmartCadCore.Core;
using Smartmatic.SmartCad.Service;
using SmartCadCore.Common;
using SmartCadCore.ClientData;
using SmartCadControls.Util;
using SmartCadControls.Controls;
using SmartCadGuiCommon.Services;
using SmartCadCore.Enums;
using Smartmatic.SmartCad.Map;


namespace SmartCadGuiCommon.Controls
{
	public partial class MapLayersControl : DevExpress.XtraEditors.XtraUserControl
	{
        public event EventHandler<EventArgs> TreeViewMapLayerChange;
        public event EventHandler<EventArgs> TreeViewSmartCADLayerChange;

		#region Fields

        private bool creatingTempObject;

		private const bool showRoutesTemporal = true;
		
		public bool closeBackGroundDialog = true;

        private object treeNodeSync = new object();

		private ShapeType currentshapeType = ShapeType.None;
        //estas variables le dicen a mapas que departamento se esta
        //editando, es muy importante que mapas siempre sepa que accion
        //esta haciendo y sobre que zona y estacion esta trabajando.
        public int currentDepartmentSelected = -1;
        private int currentZoneSelected = -1;
        private string currentStationName;

        private int currentRouteEdit;
		
		#endregion

		#region Properties

		public MapControlEx MapControlEx { get; set; }
		
        public GeoPoint LaspointInRoute { get; set; }

        List<GeoPoint> points = new List<GeoPoint>();
		
		public ShapeType CurrentShapeType
		{
			get
			{
				return currentshapeType;
			}
			set
			{
				currentshapeType = value;
				if (currentshapeType == ShapeType.None)
				{
					LaspointInRoute = new GeoPoint(0,0);
				}
			}
		}
		
		//TreeView Operations
        public TreeViewUnits TreeViewUnits { get; set; }

		public TreeViewIncidents TreeViewIncidents { get; set; }

		public TreeViewStations TreeViewStations { get; set; }

		public TreeViewRoutes TreeViewRoutes { get; set; }

		public TreeViewStructs TreeViewStructs { get; set; }

		public TreeViewZones TreeViewZones { get; set; }

        //Table Operations

        public MapTableUnits TableUnits { get; set; }

        public MapTableIncidents TableIncidents { get; set; }

        public MapTableStations TableStations { get; set; }

        public MapTableRoutes TableRoutes { get; set; }

        public MapTableStructs TableStructs { get; set; }

        public MapTableZones TableZones { get; set; }


		public IDictionary<string, ApplicationPreferenceClientData> GlobalApplicationPreference { get; set; }

		public List<TreeListNodeExType> PermissionsUser { get; set; }

		public string City { get { return MapFormDevX.City; } }

		public string Country { get { return MapFormDevX.Country; } }

		public List<DepartmentTypeClientData> ListDepartmentByUser { get; set; }

		public bool CanSeeAllDeps { get { return bool.Parse(GlobalApplicationPreference["ViewAllDepartmentsInMap"].Value); } }

		public List<UnitClientData> UnitsList { get; set; }

        public bool MapIsInitialized = false;
        
		#endregion
		
		#region Constructor
		public MapLayersControl()
		{
            InitializeComponent();
		}
		#endregion
		
		void MapLayersControl_Load(object sender, System.EventArgs e)
		{
            LoadLanguage();
            ApplicationServerService.GisAction += new EventHandler<GisActionEventArgs>(serverServiceClient_GisAction);
            if (DesignMode == false)
            {
                ServerServiceClient.GetInstance().CommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(serverService_CommittedChanges);
                this.MapControlEx.FeatureAdding += new MapControlEx.FeatureAddingEventHandler(Tools_FeatureAdding);
                this.MapControlEx.ToolUsed += new MapControlEx.ToolUsedEventHandler(ToolUsed);
                this.MapControlEx.ToolEnding += new MapControlEx.ToolEndingEventHandler(ToolEnding);
                this.MapControlEx.ToolActivated += new MapControlEx.ToolActivatedEventHandler(Tools_Activated);
            }
		}

		private void LoadLanguage()
		{
			//Asignacion del nombre del mapa de la ciudad.
			layoutControlGroupMaps.Text = ResourceLoader.GetString2("GeoGraphicalScope");
			layoutControlGroupSmartCad.Text = ResourceLoader.GetString2("SmartCadLayerMaps");
		}

		void serverService_CommittedChanges(object sender, CommittedObjectDataCollectionEventArgs args)
		{
			try
			{
				if (args != null && args.Objects != null && args.Objects.Count > 0)
				{
					if (args.Objects[0] is UnitTypeClientData && PermissionsUser.Contains(TreeListNodeExType.UnitsRoot))
					{
						#region    UnitTypeClientData
						UnitTypeClientData unitType = args.Objects[0] as UnitTypeClientData;
						string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetUnitCodesByType, unitType.Code);
						IList list = (IList)ServerServiceClient.GetInstance().SearchBasicObjects(hql);
						foreach (int var in list)
						{
							UnitClientData unit = new UnitClientData();
							unit.Code = var;
							unit = (UnitClientData)ServerServiceClient.GetInstance().RefreshClient(unit);
							if (unit != null)
							{
								TableUnits.UpdateObject(unit);
							}
						}
						#endregion
					}
                    else if (args.Objects[0] is UnitClientData && PermissionsUser.Contains(TreeListNodeExType.UnitsRoot))
                    {
                        #region UnitClientData
                        switch (args.Action)
                        {
                            case CommittedDataAction.Save:
                                TableUnits.InsertObject(args.Objects[0] as UnitClientData);
                                TableUnits.MakeObjectVisible(true, (args.Objects[0] as UnitClientData).Code);
                                break;
                            case CommittedDataAction.Update:
                                TableUnits.UpdateObject(args.Objects[0] as UnitClientData);
                                break;
                            case CommittedDataAction.Delete:
                                TableUnits.DeleteObject(args.Objects[0] as UnitClientData);
                                break;
                            default:
                                break;
                        }
                        #endregion
                    }
                    else if (args.Objects[0] is GPSClientData && PermissionsUser.Contains(TreeListNodeExType.UnitsRoot))
                    {
                        #region GPSClientData
                        //GPSClientData must be arrive like Update Action
                        foreach (GPSClientData gps in args.Objects)
                        {
                            TableUnits.UpdateObject(gps);
                        }
                        #endregion
                    }
					else if (args.Objects[0] is IncidentTypeClientData && PermissionsUser.Contains(TreeListNodeExType.IncidentRoot))
					{
						#region    IncidentTypeClientData
						IncidentTypeClientData incType = args.Objects[0] as IncidentTypeClientData;
						string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetIncidentOpensByIncidentType, IncidentStatusClientData.Open.Name, incType.Code);

						IList list = (IList)ServerServiceClient.GetInstance().SearchClientObjects(hql);
						foreach (IncidentClientData var in list)
						{
                            TableIncidents.UpdateObject(var);
						}
						#endregion
					}
					else if (args.Objects[0] is IncidentClientData && PermissionsUser.Contains(TreeListNodeExType.IncidentRoot))
                    {
                        #region IncidentClientData
                        switch (args.Action)
                        {
                            case CommittedDataAction.Save:
                                TableIncidents.InsertObject(args.Objects[0] as IncidentClientData);
                                if(TreeViewIncidents.Root.CheckState != CheckState.Unchecked)
                                    TableIncidents.MakeObjectVisible(true, ((IncidentClientData)args.Objects[0]).Code);
                                break;
                            case CommittedDataAction.Update:
                                TableIncidents.UpdateObject(args.Objects[0] as IncidentClientData);
                                break;
                            case CommittedDataAction.Delete:
                                TableIncidents.DeleteObject(args.Objects[0] as IncidentClientData);
                                break;
                            default:
                                break;
                        }
                        #endregion
                    }
					else if (args.Objects[0] is StructClientData && PermissionsUser.Contains(TreeListNodeExType.PostRoot))
                    {
                        #region StructClientData
                        switch (args.Action)
                        {
                            case CommittedDataAction.Save:
                                TableStructs.InsertObject(args.Objects[0] as StructClientData);
                                if(TreeViewStructs.Root.CheckState != CheckState.Unchecked)
                                    TableStructs.MakeAllVisible(true);
                                break;
                            case CommittedDataAction.Update:
                                TableStructs.UpdateObject(args.Objects[0] as StructClientData);
                                break;
                            case CommittedDataAction.Delete:
                                TableStructs.DeleteObject(args.Objects[0] as StructClientData);
                                break;
                            default:
                                break;
                        }
                        #endregion
                    }
					else if (args.Objects[0] is DepartmentZoneClientData && PermissionsUser.Contains(TreeListNodeExType.DepartamentZoneRoot))
                    {
                        #region  DepartmentZoneClientData
                        switch (args.Action)
                        {
                            case CommittedDataAction.Save:
                                TableZones.InsertObject(args.Objects[0] as DepartmentZoneClientData);
                                InsertPolygonZoneInTreeView((DepartmentZoneClientData)args.Objects[0]);
                                break;
                            case CommittedDataAction.Update:
                                TableZones.UpdateObject(args.Objects[0] as DepartmentZoneClientData);
                                UpdatePolygonZoneInTreeView((DepartmentZoneClientData)args.Objects[0]);
                                break;
                            case CommittedDataAction.Delete:
                                TableZones.DeleteObject(args.Objects[0] as DepartmentZoneClientData);
                                DeletePolygonZoneInTreeView((DepartmentZoneClientData)args.Objects[0]);
                                break;
                            default:
                                break;
                        }
                        #endregion
					}
					else if (args.Objects[0] is DepartmentStationClientData && PermissionsUser.Contains(TreeListNodeExType.DepartamentStationRoot))
                    {
                        #region DepartmentStationClientData
                        switch (args.Action)
                        {
                            case CommittedDataAction.Save:
                                TableStations.InsertObject(args.Objects[0] as DepartmentStationClientData);
                                InsertPolygonStationInTreeView((DepartmentStationClientData)args.Objects[0]);
                                break;
                            case CommittedDataAction.Update:
                                TableStations.UpdateObject(args.Objects[0] as DepartmentStationClientData);
                                UpdatePolygonStationInTreeView((DepartmentStationClientData)args.Objects[0]);
                                break;
                            case CommittedDataAction.Delete:
                                TableStations.DeleteObject(args.Objects[0] as DepartmentStationClientData);
                                DeletePolygonStationInTreeView((DepartmentStationClientData)args.Objects[0]);
                                break;
                            default:
                                break;
                        }
                        #endregion
                    }
					else if (args.Objects[0] is DepartmentTypeClientData && (PermissionsUser.Contains(TreeListNodeExType.DepartamentStationRoot) || PermissionsUser.Contains(TreeListNodeExType.DepartamentZoneRoot) || PermissionsUser.Contains(TreeListNodeExType.UnitsRoot)))
                    {
                        #region DepartmentTypeClientData
                        DepartmentTypeClientData depClientData = args.Objects[0] as DepartmentTypeClientData;
						switch (args.Action)
						{
							case CommittedDataAction.Save:
                                InsertDepartamentTypeInTreeView(depClientData);
								break;
							case CommittedDataAction.Delete:
                                DeleteDepartamentTypeInTreeView(depClientData);
								break;
							case CommittedDataAction.Update:
                                //MapControlEx.ObjectsActions(MapLayersActions.UpdateObject,depClientData);
                                TableZones.UpdateObject(depClientData);
                                TableStations.UpdateObject(depClientData);
                                TableRoutes.UpdateObject(depClientData);
                                UpdateDepartamentTypeInTreeView(depClientData);
								break;
                        }
                        #endregion
                    }
					else if (args.Objects[0] is RouteClientData)
                    {
                        #region RouteClientData
                        switch (args.Action)
                        {
                            case CommittedDataAction.Save:
                                TableRoutes.InsertObject(args.Objects[0] as RouteClientData);
                                InsertRouteInTreeView((RouteClientData)args.Objects[0]);
                                break;
                            case CommittedDataAction.Update:
                                TableRoutes.UpdateObject(args.Objects[0] as RouteClientData);
                                UpdateRouteInTreeView((RouteClientData)args.Objects[0]);
                                break;
                            case CommittedDataAction.Delete:
                                TableRoutes.DeleteObject(args.Objects[0] as RouteClientData);
                                DeleteRouteInTreeView((RouteClientData)args.Objects[0]);
                                break;
                            default:
                                break;
                        }
                        #endregion
                    }
					else if (args.Objects[0] is ApplicationPreferenceClientData)
					{
						ApplicationPreferenceClientData preference = args.Objects[0] as ApplicationPreferenceClientData;
						if (GlobalApplicationPreference.ContainsKey(preference.Name))
							GlobalApplicationPreference[preference.Name] = preference;
					}
				}
			}
			catch (Exception ex)
			{
				SmartLogger.Print(ex);
			}
		}

		#region Insert Methods

        private void InsertDepartamentTypeInTreeView(DepartmentTypeClientData depClientData)
        {
            FormUtil.InvokeRequired(this, () =>
            {
                treeListSmartCad.BeginUpdate();
                TreeViewStations.InsertNode(depClientData);
                TreeViewUnits.InsertNode(depClientData);
                TreeViewZones.InsertNode(depClientData);
                treeListSmartCad.EndUpdate();
            });
        }

        private void InsertPolygonZoneInTreeView(DepartmentZoneClientData zone)
		{
            FormUtil.InvokeRequired(this, () =>
            {
                treeListSmartCad.BeginUpdate();
                TreeViewZones.InsertNode(zone);
                TreeViewStations.InsertNode(zone);
                TreeViewUnits.InsertNode(zone);
                treeListSmartCad.EndUpdate();

                if (TreeViewZones.zonesOn[zone.Code])
                    TableZones.MakeObjectVisible(true, zone.Code);
            });
		}

        private void InsertPolygonStationInTreeView(DepartmentStationClientData station)
        {
            FormUtil.InvokeRequired(this, () =>
            {
                //currentZoneSelected = -1;
                treeListSmartCad.BeginUpdate();
                TreeViewStations.InsertNode(station);
                TreeViewUnits.InsertNode(station);
                treeListSmartCad.EndUpdate();

                if (TreeViewStations.stationsOn[station.Code])
                    TableStations.MakeObjectVisible(true, station.Code);
            });
        }

        private void InsertRouteInTreeView(RouteClientData route)
		{
            FormUtil.InvokeRequired(this, () =>
            {
                treeListSmartCad.BeginUpdate();
                TreeViewRoutes.InsertNode(route);
                treeListSmartCad.EndUpdate();

                if (TreeViewRoutes.routesOn[route.Code])
                    TableRoutes.MakeObjectVisible(true, route.Code);
            });
		}

		#endregion

		#region Update Methods

        private void UpdatePolygonZoneInTreeView(DepartmentZoneClientData dep)
        {
            FormUtil.InvokeRequired(this, delegate
            {
                TreeViewZones.UpdateNode(dep);
                TreeViewStations.UpdateNode(dep);
                TreeViewUnits.UpdateNode(dep);
            });
        }

		private void UpdateDepartamentTypeInTreeView(DepartmentTypeClientData depClientData)
		{
            FormUtil.InvokeRequired(this, delegate
            {
                TreeViewStations.UpdateNode(depClientData);
                TreeViewUnits.UpdateNode(depClientData);
                TreeViewZones.UpdateNode(depClientData);
            });
		}

        private void UpdatePolygonStationInTreeView(DepartmentStationClientData station)
        {
            FormUtil.InvokeRequired(this, delegate
            {
                treeListSmartCad.BeginUpdate();
                TreeViewUnits.UpdateNode(station);
                TreeViewStations.UpdateNode(station);
                treeListSmartCad.EndUpdate();
            });
        }

        private void UpdateRouteInTreeView(RouteClientData route)
		{
            FormUtil.InvokeRequired(this, delegate
            {
                treeListSmartCad.BeginUpdate();
                TreeViewRoutes.UpdateNode(route);
                treeListSmartCad.EndUpdate();
            });
		}

		#endregion

		#region Delete Methods

        private void DeleteDepartamentTypeInTreeView(DepartmentTypeClientData depClientData)
        {
            FormUtil.InvokeRequired(this, delegate
            {
                TreeViewStations.DeleteNode(depClientData);
                TreeViewUnits.DeleteNode(depClientData);
                TreeViewZones.DeleteNode(depClientData);
            });
        }

        private void DeleteRouteInTreeView(RouteClientData route)
		{
            FormUtil.InvokeRequired(this, delegate
            {
                TreeViewRoutes.DeleteNode(route);
            });
		}

        private void DeletePolygonStationInTreeView(DepartmentStationClientData depClientData)
		{
            FormUtil.InvokeRequired(this, delegate
            {
                TreeViewUnits.DeleteNode(depClientData);
                TreeViewStations.DeleteNode(depClientData);
            });
		}

        private void DeletePolygonZoneInTreeView(DepartmentZoneClientData depClientData)
		{
            FormUtil.InvokeRequired(this, delegate
            {
                TreeViewZones.DeleteNode(depClientData);
                TreeViewUnits.DeleteNode(depClientData);
                TreeViewStations.DeleteNode(depClientData);
            });
		}

		#endregion

		#region Create Methods

        public void CreateMapsLayersTreeView()
        {
            Dictionary<string, TreeListNodeEx> parentNodes = new Dictionary<string, TreeListNodeEx>();
            TreeListNodeEx rootNode = (TreeListNodeEx)treeListMaps.AppendNode(null, null);
            rootNode.LayerName = ResourceLoader.GetString2("GeoGraphicalScope");
            rootNode.Expanded = true;
            rootNode.CheckState = CheckState.Indeterminate;

            Dictionary<string, string> layers = MapControlEx.GetLayersName();

            treeListMaps.BeginSort();
            foreach (string layerName in layers.Keys)
            {
                if (layerName.EndsWith("Labels") == false)
                {
                    TreeListNodeEx childNode = (TreeListNodeEx)treeListMaps.AppendNode(layerName, rootNode);
                    childNode.LayerName = layers[layerName];
                    childNode.MapLayerName = layerName;
                    childNode.Expanded = true;
                    childNode.CheckState = GetInitialStateLayer(layerName);
                    childNode.TypeNodeLeaf = TreeListNodeExType.Layer;
                    parentNodes.Add(layerName, childNode);
                    List<string> list = new List<string>();
                    list.Add(layerName);
                    childNode.Tag = list;

                    MapControlEx.EnableLayer(layerName, (childNode.CheckState == CheckState.Checked) ? true : false);
                }
            }

            foreach (string layerName in layers.Keys)
            {
                if (layerName.EndsWith("Labels") == true)
                {
                    string parentName = layerName.Substring(0, layerName.IndexOf("Labels"));
                    if (parentNodes.ContainsKey(parentName))
                    {
                        TreeListNodeEx childNode = (TreeListNodeEx)treeListMaps.AppendNode(layerName, parentNodes[parentName]);
                        childNode.LayerName = layers[layerName];
                        childNode.MapLayerName = layerName;
                        childNode.Expanded = true;
                        childNode.CheckState = GetInitialStateLayer(layerName);
                        childNode.TypeNodeLeaf = TreeListNodeExType.Layer;
                        List<string> list = new List<string>();
                        list.Add(layerName);
                        childNode.Tag = list;

                        MapControlEx.EnableLayer(layerName, (childNode.CheckState == CheckState.Checked) ? true : false);
                    }
                }
            }
            treeListMaps.EndSort();
            treeListMaps.BeginUpdate();
            rootNode.Expanded = true;
            treeListMaps.EndUpdate();
        }

        internal void UpdateMapsLayersToTreeView()
        {
            lock (treeNodeSync)
            {
                FormUtil.InvokeRequired(this, delegate
                {
                    int nodesChecked = 0;
                    int nodesUnChecked = 0;
                    foreach (TreeListNodeEx node in treeListMaps.Nodes[0].Nodes)
                    {
                        IList list = node.Tag as IList;
                        if (list != null)
                        {
                            node.CheckState = (MapControlEx.IsLayerEnable((string)list[0]) == true ? CheckState.Checked : CheckState.Unchecked);
                            if (node.CheckState == CheckState.Checked)
                                nodesChecked++;
                            else
                                nodesUnChecked++;
                            CheckNodesChildens(node, node.CheckState);
                        }
                    }
                    if (nodesChecked > 0 && nodesUnChecked == 0)
                        treeListMaps.Nodes[0].CheckState = CheckState.Checked;
                    else if (nodesUnChecked > 0 && nodesChecked == 0)
                        treeListMaps.Nodes[0].CheckState = CheckState.Unchecked;
                    else
                        treeListMaps.Nodes[0].CheckState = CheckState.Indeterminate;
                });
            }
        }

        internal void UpdateMapsLayersFromTreeView()
        {
            lock (treeNodeSync)
            {
                FormUtil.InvokeRequired(this, delegate
                {
                    foreach (TreeListNodeEx node in treeListMaps.Nodes[0].Nodes)
                    {
                        IList list = node.Tag as IList;
                        if (list != null)
                        {
                            MapControlEx.EnableLayer((string)list[0], (node.CheckState == CheckState.Checked) ? true : false);
                        }
                    }
                });
            }
        }

        public void CreateMapLayersSmartCad(string postFix, bool smartCADNodes = true)
        {
            MapControlEx.PostFixTables = postFix;
            treeListSmartCad.BeginSort();
            TreeListNodeEx supeRootNode = (TreeListNodeEx)treeListSmartCad.AppendNode(null, null);
            supeRootNode.LayerName = ResourceLoader.GetString2("SmartCadLayerMaps");
            supeRootNode.CheckState = CheckState.Indeterminate;
            supeRootNode.Expanded = true;
            supeRootNode.TypeNodeLeaf = TreeListNodeExType.Root;
            
            if (smartCADNodes == true)
            {
                treeListSmartCad.ExpandAll();
                CreateAllSubNodes(supeRootNode);
                MapControlEx.CreateLineTable("RouteTemp");
                PrepareTreeListSmartCad(supeRootNode);
            }
            else
            {
                layoutControlGroupSmartCad.HideToCustomization();
            }
            MapControlEx.CreateLineTable("Distance");

            treeListSmartCad.EndSort();
        }

        private void CreateAllSubNodes(TreeListNodeEx superRootNode)
        {
            var applications = ServerServiceClient.GetInstance().GetActiveApplications();
            IList listD = null;
            if (!CanSeeAllDeps)
                listD = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetDepartmentsTypeByOperatorCode, ServerServiceClient.GetInstance().OperatorClient.Code));
            if (listD != null && listD.Count > 0)
                ListDepartmentByUser = listD.Cast<DepartmentTypeClientData>().ToList();
            else
                ListDepartmentByUser = ServerServiceClient.GetInstance().SearchClientObjects(typeof(DepartmentTypeClientData)).Cast<DepartmentTypeClientData>().ToList();

            List<TreeListNodeExType> permision = FilterByTreeViewByProfile();

            if (permision.Contains(TreeListNodeExType.IncidentRoot))
            {
                #region IncidentRoot
                List<IncidentClientData> incidentList = GetIncidentList();
                TreeListNodeEx incidentsNode = (TreeListNodeEx)treeListSmartCad.AppendNode(ResourceLoader.GetString2("Incidents"), superRootNode);
                incidentsNode.LayerName = ResourceLoader.GetString2("Incidents");
                incidentsNode.TypeNodeLeaf = TreeListNodeExType.IncidentRoot;
                incidentsNode.CheckState = CheckState.Indeterminate;
                incidentsNode.ExpandAll();

                TableIncidents = new MapTableIncidents(this.MapControlEx, incidentList);
                TreeViewIncidents = (TreeViewIncidents)CreateParentControlTable<IncidentClientData>(superRootNode, null);
                //colocamos en el treelist los incidentes.
                AddTreeViewIncidentsNodes(incidentsNode);

                //Incidentes Labels.
                TreeListNodeEx incidentNode = (TreeListNodeEx)treeListSmartCad.AppendNode(ResourceLoader.GetString2("VECcsCaracasCC1Labels"), incidentsNode);
                incidentNode.LayerName = ResourceLoader.GetString2("VECcsCaracasCC1Labels");
                incidentNode.TypeNodeLeaf = TreeListNodeExType.LabelLayer;
                incidentNode.CheckState = CheckState.Checked;
                List<String> listL = new List<String>();
                listL.Add(MapControlEx.DynTableIncidentsName + "Labels");
                incidentNode.Tag = listL;
                #endregion
            }
            if (permision.Contains(TreeListNodeExType.PostRoot) && applications.Contains(SmartCadApplications.SmartCadCCTV) || applications.Contains(SmartCadApplications.SmartCadAlarm))
            {
                #region PostRoot
                List<StructClientData> postList = GetPostList();
                TreeListNodeEx cctvNode = (TreeListNodeEx)treeListSmartCad.AppendNode(ResourceLoader.GetString2("Cctv"), superRootNode);
                cctvNode.LayerName = ResourceLoader.GetString2("Cctv");
                cctvNode.TypeNodeLeaf = TreeListNodeExType.PostRoot;
                cctvNode.CheckState = CheckState.Unchecked;

                TableStructs = new MapTableStructs(this.MapControlEx, postList);
                TreeViewStructs = (TreeViewStructs)CreateParentControlTable<StructClientData>(superRootNode, null);

                IList listZones = ServerServiceClient.GetInstance().SearchClientObjects(typeof(CctvZoneClientData));
                foreach (CctvZoneClientData zone in listZones)
                {
                    TreeListNodeEx zoneNode = (TreeListNodeEx)treeListSmartCad.AppendNode(zone.Name, cctvNode);
                    zoneNode.LayerName = ResourceLoader.GetString2(zone.Name);
                    zoneNode.TypeNodeLeaf = TreeListNodeExType.PostLayer;
                    zoneNode.Tag = zone.Code;
                }

                //Postes
                TreeListNodeEx postNode = (TreeListNodeEx)treeListSmartCad.AppendNode(ResourceLoader.GetString2(/*Country + City +*/ MapControlEx.DynTablePostName + "Labels"), cctvNode);
                postNode.LayerName = ResourceLoader.GetString2(/*Country + City + */MapControlEx.DynTablePostName + "Labels");
                postNode.TypeNodeLeaf = TreeListNodeExType.LabelLayer;
                List<String> listL = new List<String>();
                listL.Add(MapControlEx.DynTablePostName + "Labels");
                postNode.Tag = listL;

                #endregion
            }

            IList listDepTypes = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetDepartmentTypeWithZonesStations, true);

            if (permision.Contains(TreeListNodeExType.UnitsRoot))
            {
                #region UnitsRoot

                if (CanSeeAllDeps)
                    UnitsList = GetListUnits();
                else
                    UnitsList = GetListUnitsOperator();

                TableUnits = new MapTableUnits(this.MapControlEx, UnitsList);
                TreeViewUnits = (TreeViewUnits)CreateParentControlTable<UnitClientData>(superRootNode, listDepTypes);

                #endregion
            }
            if (permision.Contains(TreeListNodeExType.DepartamentZoneRoot))
            {
                #region DepartamentZoneRoot
                List<DepartmentZoneClientData> polygonZoneList = GetPolygonZoneList();
                TableZones = new MapTableZones(this.MapControlEx, polygonZoneList);
                TreeViewZones = (TreeViewZones)CreateParentControlTable<DepartmentZoneClientData>(superRootNode, listDepTypes);
                #endregion
            }
            if (permision.Contains(TreeListNodeExType.DepartamentStationRoot))
            {
                #region DepartamentStationRoot
                List<DepartmentStationClientData> polygonStationList = GetPolygonStationList();
                TableStations = new MapTableStations(this.MapControlEx, polygonStationList);
                TreeViewStations = (TreeViewStations)CreateParentControlTable<DepartmentStationClientData>(superRootNode, listDepTypes);
                #endregion
            }
            if (showRoutesTemporal)
            {
                #region RouteRoot
                List<RouteClientData> routeList = GetRoutesList();
                TableRoutes = new MapTableRoutes(this.MapControlEx, routeList);
                TreeViewRoutes = (TreeViewRoutes)CreateParentControlTable<RouteClientData>(superRootNode, listDepTypes);
                #endregion
            }
            CreateNodesAux(superRootNode);
        }

        private ITreeViewOperations<T> CreateParentControlTable<T>(TreeListNodeEx node, IList objectsList)
		{
			ITreeViewOperations<T> table = null;
			if (typeof(T) == typeof(UnitClientData))
			{
                table = (ITreeViewOperations<T>)(new TreeViewUnits());
				((TreeViewUnits)table).Root = BuildDepartamentUnitsNode(node, objectsList); ;
			}
			else if (typeof(T) == typeof(IncidentClientData))
			{
                table = (ITreeViewOperations<T>)(new TreeViewIncidents());
				((TreeViewIncidents)table).Root = node;
			}
			else if (typeof(T) == typeof(StructClientData))
			{
                table = (ITreeViewOperations<T>)(new TreeViewStructs());
				((TreeViewStructs)table).Root = node;
			}
			else if (typeof(T) == typeof(DepartmentZoneClientData))
			{
                table = (ITreeViewOperations<T>)(new TreeViewZones());
                ((TreeViewZones)table).Root = BuildDepartamentZonesNode(node, objectsList);
			}
			else if (typeof(T) == typeof(DepartmentStationClientData))
			{
                table = (ITreeViewOperations<T>)new TreeViewStations();
                ((TreeViewStations)table).Root = BuildDepartamentStationNode(node, objectsList);
			}
			else if (typeof(T) == typeof(RouteClientData))
			{
                table = (ITreeViewOperations<T>)new TreeViewRoutes();
				((TreeViewRoutes)table).Root = BuildRoutesNode(node ,objectsList);
			}
			return table;
		}

        private TreeListNodeEx BuildDepartamentZonesNode(TreeListNodeEx superRoot, IList listDepTypes)
		{
			TreeListNodeEx rootZones = (TreeListNodeEx)treeListSmartCad.AppendNode(null, superRoot);
			rootZones.LayerName = ResourceLoader.GetString2("DepartmentZones");
			rootZones.TypeNodeLeaf = TreeListNodeExType.DepartamentZoneRoot;

			if(listDepTypes == null)
                listDepTypes = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetDepartmentTypeWithZones, true);


			foreach (DepartmentTypeClientData depType in listDepTypes)
			{
				if (!CanSeeAllDeps && (ListDepartmentByUser.Count(d => d.Code == depType.Code) <= 0))
					continue;

				TreeListNodeEx depNode = (TreeListNodeEx)treeListSmartCad.AppendNode(null, rootZones);
				depNode.LayerName = depType.Name;
				depNode.TypeNodeLeaf = TreeListNodeExType.DepartamentZoneUbicationRoot;
				depNode.Tag = depType.Code;

				foreach (DepartmentZoneClientData zone in depType.DepartmentZones)
				{
					//add zone
					TreeListNodeEx clonezoneNode = (TreeListNodeEx)treeListSmartCad.AppendNode(null, depNode);
					clonezoneNode.LayerName = zone.Name;
					clonezoneNode.TypeNodeLeaf = TreeListNodeExType.DepartamentZoneUbicationLayer;
					clonezoneNode.Tag = zone.Code;
				}
			}
			TreeListNodeEx labelZone = (TreeListNodeEx)treeListSmartCad.AppendNode(null, rootZones);
			labelZone.LayerName = ResourceLoader.GetString2("VECcsCaracasCC1Labels");
			labelZone.TypeNodeLeaf = TreeListNodeExType.LabelLayer;
            List<String> listL = new List<String>();
            listL.Add(MapControlEx.DynTablePolygonZoneName + "Labels");
            labelZone.Tag = listL;

			return rootZones;
		}

        private TreeListNodeEx BuildDepartamentStationNode(TreeListNodeEx superRoot, IList listDepTypes)
		{
			TreeListNodeEx rootStations = (TreeListNodeEx)treeListSmartCad.AppendNode(ResourceLoader.GetString2("Polygon"), superRoot);
			rootStations.LayerName = ResourceLoader.GetString2("DepartmentStations");
			rootStations.TypeNodeLeaf = TreeListNodeExType.DepartamentStationRoot;

			TreeListNodeEx labelZone = (TreeListNodeEx)treeListSmartCad.AppendNode(null, rootStations);
			labelZone.LayerName = ResourceLoader.GetString2("VECcsCaracasCC1Labels");
			labelZone.TypeNodeLeaf = TreeListNodeExType.LabelLayer;
            List<String> listL = new List<String>();
            listL.Add(MapControlEx.DynTablePolygonStationName + "Labels");
            labelZone.Tag = listL;

			if(listDepTypes == null)
                listDepTypes = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetDepartmentTypeWithZonesStations, true);

			foreach (DepartmentTypeClientData depType in listDepTypes)
			{
				if (!CanSeeAllDeps && (ListDepartmentByUser.Count(d => d.Code == depType.Code) <= 0))
					continue;
				//add dep

				TreeListNodeEx depNode = (TreeListNodeEx)treeListSmartCad.AppendNode(null, rootStations);
				depNode.LayerName = depType.Name;
				depNode.TypeNodeLeaf = TreeListNodeExType.DepartamentStationUbicationRoot;
				depNode.Tag = depType.Code;

				foreach (DepartmentZoneClientData zone in depType.DepartmentZones)
				{
					TreeListNodeEx zoneNode = (TreeListNodeEx)treeListSmartCad.AppendNode(null, depNode);
					zoneNode.LayerName = zone.Name;
					zoneNode.TypeNodeLeaf = TreeListNodeExType.DepartamentStationUbicationRoot;
					zoneNode.Tag = zone.Code;

					foreach (DepartmentStationClientData station in zone.DepartmentStations)
					{
						TreeListNodeEx stationNode = (TreeListNodeEx)treeListSmartCad.AppendNode(null, zoneNode);
						stationNode.LayerName = station.Name;
						stationNode.TypeNodeLeaf = TreeListNodeExType.DepartamentStationUbicationLayer;
						stationNode.Tag = station.Code;
					}
				}
			}
			return rootStations;
		}

		private TreeListNodeEx BuildDepartamentUnitsNode(TreeListNodeEx superRootNode, IList listDepTypes)
		{
			TreeListNodeEx rootNode = (TreeListNodeEx)treeListSmartCad.AppendNode(null, superRootNode);
			rootNode.LayerName = ResourceLoader.GetString2("UNITS");
			rootNode.TypeNodeLeaf = TreeListNodeExType.UnitsRoot;
			rootNode.CheckState = CheckState.Unchecked;

			if(listDepTypes == null)
                listDepTypes = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetDepartmentTypeWithZonesStations, true);

			foreach (DepartmentTypeClientData depType in listDepTypes)
			{
				if (!CanSeeAllDeps && (ListDepartmentByUser.Count(d => d.Code == depType.Code) <= 0))
					continue;
				//add dep

				TreeListNodeEx depNode = (TreeListNodeEx)treeListSmartCad.AppendNode(null, rootNode);
				depNode.LayerName = depType.Name;
				depNode.TypeNodeLeaf = TreeListNodeExType.DepartamentType;
				depNode.Tag = depType.Code;
				depNode.CheckState = CheckState.Unchecked;

				foreach (DepartmentZoneClientData zone in depType.DepartmentZones)
				{
					TreeListNodeEx zoneNode = (TreeListNodeEx)treeListSmartCad.AppendNode(null, depNode);
					zoneNode.LayerName = zone.Name;
					zoneNode.TypeNodeLeaf = TreeListNodeExType.DepartamentTypeZone;
					zoneNode.Tag = zone.Code;
					zoneNode.CheckState = CheckState.Unchecked;

					foreach (DepartmentStationClientData station in zone.DepartmentStations)
					{
						TreeListNodeEx stationNode = (TreeListNodeEx)treeListSmartCad.AppendNode(null, zoneNode);
						stationNode.LayerName = station.Name;
						stationNode.TypeNodeLeaf = TreeListNodeExType.DepartamentStation;
						stationNode.Tag = station.Code;
						stationNode.CheckState = CheckState.Unchecked;
					}
				}
			}

			//Unidades Labels
			TreeListNodeEx unitNode = (TreeListNodeEx)treeListSmartCad.AppendNode(ResourceLoader.GetString2("VECcsUnidadesLabels"), rootNode);
			unitNode.LayerName = ResourceLoader.GetString2("VECcsUnidadesLabels");
			unitNode.TypeNodeLeaf = TreeListNodeExType.LabelLayer;
            unitNode.CheckState = CheckState.Checked;
            List<String> listL = new List<String>();
            listL.Add(MapControlEx.DynTableUnitsName + "Labels");
            unitNode.Tag = listL;

           // MapControlEx.EnableLayer(unitNode.LayerName, true);

			return rootNode;
		}

        private TreeListNodeEx BuildRoutesNode(TreeListNodeEx superRoot, IList listDepTypes)
		{
			TreeListNodeEx rootRoutes = (TreeListNodeEx)treeListSmartCad.AppendNode(ResourceLoader.GetString2("Polygon"), superRoot);
			rootRoutes.LayerName = ResourceLoader.GetString2("Routes");
			rootRoutes.TypeNodeLeaf = TreeListNodeExType.RouteRoot;

			if(listDepTypes == null)
                listDepTypes = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetDepartmentTypeWithZonesStations, true);

			foreach (DepartmentTypeClientData depType in listDepTypes)
			{
				if (!CanSeeAllDeps && (ListDepartmentByUser.Count(d => d.Code == depType.Code) <= 0))
					continue;

				TreeListNodeEx depNode = (TreeListNodeEx)treeListSmartCad.AppendNode(null, rootRoutes);
				depNode.LayerName = depType.Name;
				depNode.TypeNodeLeaf = TreeListNodeExType.RouteDepartmetTypeRoot;
				depNode.Tag = depType.Code;

                IList listRoutes = ServerServiceClient.GetInstance().SearchClientObjects(
                    SmartCadHqls.GetCustomHql(SmartCadHqls.GetRoutesByDepartmentType,depType.Code), true);
                foreach (RouteClientData  route in listRoutes)
                {
                    TreeListNodeEx routeNode = (TreeListNodeEx)treeListSmartCad.AppendNode(null, depNode);
                    routeNode.LayerName = route.Name;
                    routeNode.TypeNodeLeaf = TreeListNodeExType.RouteLayer;
                    routeNode.Tag = route.Code;
                }
			}

			TreeListNodeEx labelRoutes = (TreeListNodeEx)treeListSmartCad.AppendNode(null, rootRoutes);
			labelRoutes.LayerName = ResourceLoader.GetString2("VECcsCaracasCC1Labels");
			labelRoutes.TypeNodeLeaf = TreeListNodeExType.LabelLayer;
            List<String> listL = new List<String>();
            listL.Add(MapControlEx.DynTableRoute+ "Labels");
            labelRoutes.Tag = listL;

			return rootRoutes;
		}

        private void CreateNodesAux(TreeListNodeEx supeRootNode)
        {
            //TreeListNodeEx lpr = (TreeListNodeEx)treeListSmartCad.AppendNode(ResourceLoader.GetString2("LPR"), supeRootNode);
            //lpr.LayerName = ResourceLoader.GetString2("LPR");
            //lpr.TypeNodeLeaf = TreeListNodeExType.LPRRoot;


            //TreeListNodeEx movilSec = (TreeListNodeEx)treeListSmartCad.AppendNode(ResourceLoader.GetString2("MovilSecurity"), supeRootNode);
            //movilSec.LayerName = ResourceLoader.GetString2("MovilSecurity");
            //movilSec.TypeNodeLeaf = TreeListNodeExType.MovilSecurityRoot;
        }

		#endregion

		public void serverServiceClient_GisAction(object sender, GisActionEventArgs e)
		{
			try
			{
				if (e is SendActivateLayerEventArgs)
                {
                    #region  SendActivateLayerEventArgs
                    MapControlEx.ClearTables();
                    SendActivateLayerEventArgs alea = e as SendActivateLayerEventArgs;
                    CurrentShapeType = alea.Type;

                    CheckState newState = (alea.Activate)? CheckState.Checked : CheckState.Unchecked;
					
					if (alea.Type == ShapeType.Zone)
					{
                        ActivateTreeNodeList(TreeListNodeExType.DepartamentZoneRoot, newState);
					}
					else if (alea.Type == ShapeType.Station)
					{
                        ActivateTreeNodeList(TreeListNodeExType.DepartamentStationRoot, newState);
					}
                    else if (alea.Type == ShapeType.Route)
                    {
                        ActivateTreeNodeList(TreeListNodeExType.RouteRoot, newState);
                    }
                    else if (alea.Type == ShapeType.Struct)
                    {
                        //This line was added to allow drawing the structs without zone.
                        TableStructs.MakeAllVisible(alea.Activate);
                        ActivateTreeNodeList(TreeListNodeExType.PostRoot, newState);
                    }
                    else if (alea.Type == ShapeType.Unit)
                    {
                        //Since the units is a huge table its better activate all together.
                        TableUnits.MakeAllVisible(newState == CheckState.Checked);
                        ActivateTreeNodeList(TreeListNodeExType.UnitsRoot, newState);
                    }
                    if(alea.DepartmentType !=-1) currentDepartmentSelected = alea.DepartmentType;
                    if(alea.Zone != -1) currentZoneSelected = alea.Zone;
                    if(alea.Station != "") currentStationName = alea.Station;
                    #endregion
                }
                else if (e is SendGeoPointListEventArgs)
                {
                    WorkWithSendGeoPointListEventArgs(e);
                }
                else if (e is DisplayGeoPointEventArgs)
                {
                    #region  DisplayGeoPointEventArgs
                    GeoPoint point = (e as DisplayGeoPointEventArgs).Point;

                    if (point.Lat != 0 && point.Lon != 0)
                    {
                        MapControlEx.CenterMapInPoint(point);
                        Thread.Sleep(200);
                        MapControlEx.HighLightSingle(point);
                    }
                    #endregion 
                }
                else if (e is SendSelectDepartmentType)
                {
                    currentDepartmentSelected = (e as SendSelectDepartmentType).DepartmentCode; 
                }
				else if (e is HLObjectsEventArgs)
                {
                    #region  HLObjectsEventArgs
                    HLObjectsEventArgs hlObjs = e as HLObjectsEventArgs;
                    if (hlObjs.HLObject.Count > 1)
                    {
                        switch (hlObjs.HLObject[0].HLType)
                        {
                            case HLGisObjectType.UnitsRecommended:
                                MapControlEx.HighLightUnits(
                                    new MapObjectIncident(
                                        hlObjs.HLObject[0].Data as IncidentClientData),
                                    hlObjs.HLObject, Color.Yellow);
                                break;
                            case HLGisObjectType.UnitsAsigned:
                                MapControlEx.HighLightUnits(
                                    new MapObjectIncident(
                                        hlObjs.HLObject[0].Data as IncidentClientData),
                                    hlObjs.HLObject, Color.BlueViolet);
                                break;
                            case HLGisObjectType.UnitsRecommendedAsigned:
                                MapControlEx.HighLightUnits(hlObjs.HLObject);
                                break;
                            default:
                                break;
                        }
                        MapControlEx.HighLightListObjects(hlObjs.HLObject);
                    }
                    else
                    {
                        switch (hlObjs.HLObject[0].HLType)
                        {
                            case HLGisObjectType.Unit:
                                MapControlEx.HighLightUnit(new MapObjectUnit(hlObjs.HLObject[0].Data as UnitClientData));
                                break;
                            case HLGisObjectType.Incident:
                                MapControlEx.HighLightIncident(new MapObjectIncident(hlObjs.HLObject[0].Data as IncidentClientData));
                                break;
                            default:
                                break;
                        }
                    }
                    #endregion
                }
                else if (e is SendACKEventArgs)
                {
                    if (((SendACKEventArgs)e).Connected)
                    {
                        ApplicationServiceClient.Current(UserApplicationClientData.Administration).SendACK(true);
                    }
                    else
                    {
                        currentDepartmentSelected = -1;
                        currentZoneSelected = -1;
                        currentStationName = "";
                    }
                }
			}
			catch (GisException gis)
			{
                MessageForm.Show(gis.Message, MessageFormType.Error);
                MapControlEx.ClearTables();
			}
			catch (Exception ex)
			{
				SmartLogger.Print(ex);
			}
		}

        /// <summary>
        /// Metodo auxiliar para GisAction
        /// </summary>
        /// <param name="e"></param>
        private void WorkWithSendGeoPointListEventArgs(GisActionEventArgs e)
        {
            SendGeoPointListEventArgs ea = e as SendGeoPointListEventArgs;
            if (ea != null)
            {
                CurrentShapeType = ea.Type;
                currentZoneSelected = ea.ZoneCode;
                currentDepartmentSelected = ea.DepartmentCode;
                currentStationName = ea.StationName;

                if (ea.Type == ShapeType.Zone && ea.Points.Count > 0)
                {
                    //points = ea.Points.ConvertAll<GeoPoint>(delegate(GeoPoint g) { return new GeoPoint(g.Lon, g.Lat); });
                    CheckZoneZoneOverlaps(ea.Points);
                }
                if (ea.Type == ShapeType.Station && ea.Points.Count > 0)
                {
                    //points = ea.Points.ConvertAll<GeoPoint>(delegate(GeoPoint g) { return new GeoPoint(g.Lon, g.Lat); });
                    CheckConstraintsForStation(ea.Points);
                }
                if (ea.Type == ShapeType.Route && ea.Points != null && ea.Points.Count > 1)
                {
                    int codeRoute = ea.ZoneCode;
                    currentRouteEdit = codeRoute;
                }
                //else if ((ea.Type == ShapeType.Route || ea.Type == ShapeType.Station || ea.Type == ShapeType.Zone) && MapIsInitialized == false)
                else if (MapIsInitialized == false)
                {
                    currentRouteEdit = 0;
                    MapControlEx.ClearActions(MapActions.ClearDistanceTable);
                }
            }
        }

		public void ActivateTreeNodeList(TreeListNodeExType type, CheckState state)
        {
            lock (treeNodeSync)
            {
                FormUtil.InvokeRequired(this, delegate
                {
                    if (treeListSmartCad.Nodes.Count > 0)
                    {
                        foreach (TreeListNodeEx node in treeListSmartCad.Nodes[0].Nodes)
                        {
                            if (node.TypeNodeLeaf == type && node.CheckState != state)
                            {
                                node.CheckState = state;
                                CheckNodesChildens(node, state);
                                break;
                            }
                        }
                    }
                });
            }
        }

        private void CheckNodesChildens(TreeListNodeEx node, CheckState state)
        {
            if (node.HasChildren == false)
            {
                if (state == CheckState.Checked && node.CheckState == CheckState.Unchecked)
                {
                    switch (node.TypeNodeLeaf)
                    {
                        case TreeListNodeExType.DepartamentZoneUbicationLayer:
                            TableZones.MakeObjectVisible(true, (int)node.Tag);
                            break;
                        case TreeListNodeExType.DepartamentStationUbicationLayer:
                            TableStations.MakeObjectVisible(true, (int)node.Tag);
                            break;
                        case TreeListNodeExType.RouteLayer:
                            TableRoutes.MakeObjectVisible(true, (int)node.Tag);
                            break;
                        case TreeListNodeExType.PostLayer:
                            TableStructs.MakeObjectVisible(true, (int)node.Tag);
                            break;
                        //case TreeListNodeExType.DepartamentStation:
                        //    TableUnits.MakeObjectsVisible(true, new List<int>(){(int)node.Tag});
                        //    break;
                        case TreeListNodeExType.LabelLayer:
                            node.CheckState = CheckState.Checked; 
                            SetCheckLabels(node);
                            break;
                        default:
                            break;
                    }
                }
                else if (state == CheckState.Unchecked && node.CheckState != CheckState.Unchecked)
                {
                    //NO ACTION
                }
            }
            else
            {
                foreach (TreeListNodeEx n in node.Nodes)
                {
                    CheckNodesChildens(n , state);
                    n.CheckState = state;
                }
                node.CheckState = state;
            }
        }

		private void treeListSmartCad_AfterCheckNode(object sender, DevExpress.XtraTreeList.NodeEventArgs e)
		{
            TreeListNodeEx node = e.Node as TreeListNodeEx;
			closeBackGroundDialog = false;
			RaiseBackGroundProcess();
			switch (node.TypeNodeLeaf)
			{
				case TreeListNodeExType.Layer:
					break;
				case TreeListNodeExType.UnitsRoot:
					WorkWithDepartaments(node);
					break;
				case TreeListNodeExType.Zone:
					break;
				case TreeListNodeExType.Area:
					break;
				case TreeListNodeExType.DepartamentStationUbicationRoot:
					WorkWithUbication(node);
					break;
				case TreeListNodeExType.DepartamentStationRoot:
					WorkWithDepartamentStationRoot(node);
					break;
				case TreeListNodeExType.LabelLayer:
					SetCheckLabels(node);
					break;
				case TreeListNodeExType.DepartamentZoneUbicationLayer:
					SetCheckUbication(node, typeof(DepartmentZoneClientData));
					break;
				case TreeListNodeExType.DepartamentStationUbicationLayer:
                    SetCheckUbication(node, typeof(DepartmentStationClientData));
					break;
				case TreeListNodeExType.LabelRoot:
					WorkWithLabels(node);
					break;
				case TreeListNodeExType.DepartamentTypeZone:
				case TreeListNodeExType.DepartamentStation:
				case TreeListNodeExType.DepartamentType:
					SetCheckDepartaments(node);
					break;
				case TreeListNodeExType.Root:
					CheckAllNodes(e.Node);
					break;
				case TreeListNodeExType.DepartamentZoneRoot:
					WorkWithDepartamentZoneRoot(node);
					break;
				case TreeListNodeExType.UbicationRoot:
				case TreeListNodeExType.DepartamentZoneUbicationRoot:
					WorkWithUbication(node);
					break;
				case TreeListNodeExType.UbicationLayer:
					break;
				case TreeListNodeExType.PostRoot:
					WorkWithPost(node);
					break;
				case TreeListNodeExType.PostLayer:
                    TableStructs.MakeObjectVisible((node.CheckState== CheckState.Checked ? true:false), (int)node.Tag);
                    SetCheckedParentNodes2(node, node.CheckState);
					break;
				case TreeListNodeExType.PoligonRoot:
					break;
				case TreeListNodeExType.PoligonLayer:
					break;
				case TreeListNodeExType.IncidentRoot:
					WorkWithIncidents(node);
					break;
				case TreeListNodeExType.IncidentLayer:
					SetCheckIncidents(node);
					break;
				case TreeListNodeExType.CCTVLayer:
					SetCheckCCTV(node);
					break;
                case TreeListNodeExType.ALARMLayer:
                    SetCheckAlarm(node);
                    break;
				case TreeListNodeExType.None:
					break;
				case TreeListNodeExType.RouteLayer:
					WorkWithRoute(e.Node);
					break;
				case TreeListNodeExType.RouteDepartmetTypeRoot:
					WorkWithRoutes(e.Node);
					break;
				case TreeListNodeExType.RouteRoot:
					WorkWithRoutesRoot(e.Node);
					break;
				default:
					SetCheckedParentNodes2(e.Node, e.Node.CheckState);
					break;
			}
			closeBackGroundDialog = true;
		}

		private void SetCheckedParentNodes2(TreeListNode node, CheckState check)
		{
			if (node.ParentNode != null)
			{
				bool b = false;
				CheckState state;
				for (int i = 0; i < node.ParentNode.Nodes.Count; i++)
				{
					state = (CheckState)node.ParentNode.Nodes[i].CheckState;
					if (!check.Equals(state))
					{
						b = !b;
						break;
					}
				}
				node.ParentNode.CheckState = b ? CheckState.Indeterminate : check;
				SetCheckedParentNodes2(node.ParentNode, check);
			}
			else
			{
				CalculateStateRoot(node);
			}
		}

		private void SetCheckedParentNodes(TreeListNode node, CheckState check)
		{
			if (node.ParentNode != null)
			{
				if (node.ParentNode.Tag != null)
				{
                    List<string> layers = (node.ParentNode.Tag as List<string>);
                    foreach (string layerName in layers)
                    {
                        MapControlEx.EnableLayer(layerName, ((check == CheckState.Checked) ? true : false));
                    }
					node.ParentNode.CheckState = (check == CheckState.Checked) ? CheckState.Checked : CheckState.Indeterminate;
				}
				else
				{
					node.ParentNode.CheckState = (check == CheckState.Checked) ? CheckState.Checked : CheckState.Unchecked;
				}
				SetCheckedParentNodes(node.ParentNode, check);
			}
			else
			{
				CalculateStateRoot(node);
			}
		}

		private void CalculateStateRoot(TreeListNode node)
		{
			bool check = false;
			bool uncheck = false;
			for (int i = 0; i < node.Nodes.Count; i++)
			{
				switch (node.Nodes[i].CheckState)
				{
					case CheckState.Checked:
						check = true;
						break;
					case CheckState.Indeterminate:
						i = node.Nodes.Count;
						check = true;
						uncheck = true;
						break;
					case CheckState.Unchecked:
						uncheck = true;
						break;
					default:
						break;
				}
			}
			CheckState state = CheckState.Unchecked;
			if (check && uncheck)
			{
				state = CheckState.Indeterminate;
			}
			else if (check)
			{
				state = CheckState.Checked;
			}
			else if (uncheck)
			{
				state = CheckState.Unchecked;
			}
			node.CheckState = state;
		}

		private void SetCheckCCTV(TreeListNodeEx node)
		{
            TableIncidents.MakeObjectsVisible((node.CheckState== CheckState.Checked? true:false),null, GlobalApplicationPreference["CCTVIncidentCodePrefix"].Value, GlobalApplicationPreference["IsIncidentCodeSuffix"].Value);
			SetCheckedParentNodes2(node, node.CheckState);
		}

		private void SetCheckIncidents(TreeListNodeEx node)
		{
            TableIncidents.MakeObjectsVisible( (node.CheckState == CheckState.Checked ? true : false),
                                        null, GlobalApplicationPreference["FrontClientIncidentCodePrefix"].Value, GlobalApplicationPreference["IsIncidentCodeSuffix"].Value);
			SetCheckedParentNodes2(node, node.CheckState);
		}

        private void SetCheckAlarm(TreeListNodeEx node)
        {
            TableIncidents.MakeObjectsVisible((node.CheckState == CheckState.Checked ? true : false),
                                        null, GlobalApplicationPreference["ALARMIncidentCodePrefix"].Value, GlobalApplicationPreference["IsIncidentCodeSuffix"].Value);
            SetCheckedParentNodes2(node, node.CheckState);
        }

		private void treeListMaps_BeforeCheckNode(object sender, DevExpress.XtraTreeList.CheckNodeEventArgs e)
		{
			if (e.PrevState == CheckState.Unchecked)
				e.State = CheckState.Checked;
			else if (e.PrevState == CheckState.Checked)
				e.State = CheckState.Indeterminate;
			else
				e.State = CheckState.Unchecked;
			if (!e.Node.HasChildren)
			{
				e.State = (e.PrevState == CheckState.Checked ? CheckState.Unchecked : CheckState.Checked);
			}
		}

		private void SetIntermedietedChilds(TreeListNode node)
		{
			for (int i = 0; i < node.Nodes.Count; i++)
			{
				if (!node.Nodes[i].HasChildren)
				{
					node.Nodes[i].CheckState = CheckState.Unchecked;
                    List<string> layers = (node.Nodes[i].Tag as List<string>);
                    foreach (string layerName in layers)
                    {
                        MapControlEx.EnableLayer(layerName, true);
                    }				
				}
				else
				{
					node.Nodes[i].CheckState = CheckState.Indeterminate;
					List<string> layers = (node.Nodes[i].Tag as List<string>);
                    foreach (string layerName in layers)
                    {
                        MapControlEx.EnableLayer(layerName, true);
                    }		
					SetCheckedChildNodes(node.Nodes[i], CheckState.Unchecked);
				}
			}
		}

		private void SetCheckedChildNodes(TreeListNode node, CheckState check)
		{
			for (int i = 0; i < node.Nodes.Count; i++)
			{
				node.Nodes[i].CheckState = (check == CheckState.Checked ? CheckState.Checked : CheckState.Unchecked);
                List<string> layers = (node.Nodes[i].Tag as List<string>);
                foreach (string layerName in layers)
                {
                    MapControlEx.EnableLayer(layerName, ((check == CheckState.Checked) ? true : false));
                }
                SetCheckedChildNodes(node.Nodes[i], check);
			}
		}

        private void treeListMaps_AfterCheckNode(object sender, DevExpress.XtraTreeList.NodeEventArgs e)
        {
            bool enableLayer = true;
            switch (e.Node.CheckState)
            {
                case CheckState.Checked:
                    SetCheckedChildNodes(e.Node, CheckState.Checked);
                    SetCheckedParentNodes(e.Node, CheckState.Checked);
                    break;
                case CheckState.Indeterminate:
                    if (e.Node.Level > 0)
                        SetCheckedChildNodes(e.Node, CheckState.Unchecked);
                    else
                    {
                        SetIntermedietedChilds(e.Node);
                        e.Node.CheckState = CheckState.Indeterminate;
                    }
                    break;
                case CheckState.Unchecked:
                    SetCheckedChildNodes(e.Node, CheckState.Unchecked);
                    enableLayer = false;
                    SetCheckedParentNodes(e.Node, CheckState.Indeterminate);
                    break;
            }
            List<string> layers = (e.Node.Tag as List<string>);
            if (layers != null)
            {
                foreach (string layerName in layers)
                {
                    MapControlEx.EnableLayer(layerName, enableLayer);
                }
            }

            if (TreeViewMapLayerChange != null)
            {
                TreeViewMapLayerChange(sender, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Cuando seleccionan una herramienta nueva, pasa por aqui.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Tools_Activated(object sender, ToolActivatedEventArgs e)
        {
            SetCursor(e.ToolName);
        }

        /// <summary>
        /// Este metodo coloca el cursor adecuado dependiendo de la 
        /// herramienta seleccionada y maneja la limpieza de distancia
        /// y las tablas temporales.
        /// </summary>
        /// <param name="toolName"></param>
        private void SetCursor(string toolName)
        {
            Bitmap image = new Bitmap(ResourceLoader.GetImage("$Image.SelectCursor"));
            this.MapControlEx.ShowDistance = false;
            bool useDefaultCursor = false;
            switch (toolName)
            {
                case MapControlEx.ZOOM_IN_CODE:
                    image = new Bitmap(ResourceLoader.GetImage("$Image.ZoomInCursor"));
                    break;
                case MapControlEx.ZOOM_OUT_CODE:
                    image = new Bitmap(ResourceLoader.GetImage("$Image.ZoomOutCursor"));
                    break;
                case MapControlEx.PAN_CODE:
                    image = new Bitmap(ResourceLoader.GetImage("$Image.PanCursor"));
                    break;
                case MapControlEx.SELECT_CODE:
                    useDefaultCursor = true;
                    break;
                case MapControlEx.SELECT_RADIUS_CODE:
                    image = new Bitmap(ResourceLoader.GetImage("$Image.SelectRadiusCursor"));
                    break;
                case MapControlEx.SELECT_RECT_CODE:
                    image = new Bitmap(ResourceLoader.GetImage("$Image.SelectRectCursor"));
                    break;
                case MapControlEx.CENTER_CODE:
                    image = new Bitmap(ResourceLoader.GetImage("$Image.CenterCursor"));
                    break;
                case MapControlEx.ADD_POLYGON_CODE:
                    useDefaultCursor = true;
                    break;
                case MapControlEx.ADD_POSITION_CODE:
                    image = new Bitmap(ResourceLoader.GetImage("$Image.PointCursor"));
                    break;
                case MapControlEx.ADD_POLYLINE_CODE:
                    this.MapControlEx.ShowDistance = true;

                    break;
            }
            //this.MapControlEx.RecalculateAll();
            if (useDefaultCursor == false)
            {
                if (toolName == MapControlEx.ADD_POLYLINE_CODE)
                    MapControlEx.SetToolUseDefaultCursor(false, Cursors.Cross);
                else
                    MapControlEx.SetToolUseDefaultCursor(false, new Cursor(image.GetHicon()));
            }
            if (this.MapControlEx.ShowDistance == false)
            {
                if (CurrentShapeType == ShapeType.Distance)
                {
                    MapControlEx.ClearActions(MapActions.ClearDistanceTable);

                    this.MapControlEx.Distance = 0;
                }
            }
        }

        /// <summary>
        /// Metodo importante...
        /// Este metodo maneja la utilizacion de las herramientas
        /// maneja 3 estados, cuando se comienza a utilizar la herramienta
        /// cuando se esta usando, y cuando termina de usarla que es cuando 
        /// se le da doble click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolUsed(object sender, ToolUsedEventArgs e)
        {
            this.MapControlEx.RefreshZoomTrackBar();
            if ((e != null) && (e.ToolStatus == ToolStatus.Starting))
            {
                InitializeDataToolsMaps(e);
            }
            else if ((e != null) && (e.ToolStatus == ToolStatus.InProgress))
            {
                points.Add(e.Position); 

                if (points.Count >= 2 && e.ToolName == MapControlEx.ADD_POLYLINE_CODE)
                {
                    //Added for version 3.4.1
                    ApplicationServiceClient.Current(UserApplicationClientData.Administration).SendGeoPointList(points, CurrentShapeType);
                    //InsertDistanceSegment();
                }
            }
            else if (e == null)
            {
                if (CurrentShapeType == ShapeType.Distance)
                {
                    this.MapControlEx.totalDistance = 0;
                    this.MapControlEx.Distance = 0;
                }
                else
                {
                    if (this.MapControlEx.ToolInUse == MapControlEx.ADD_POLYLINE_CODE)
                    {
                        List<GeoPoint> geoPoints = new List<GeoPoint>();

                        if (CurrentShapeType == ShapeType.Station)
                        {
                            foreach (GeoPoint point in points)
                            {
                                if (point.Lon != 0 && point.Lat != 0)
                                    geoPoints.Add(point);
                            }
                            CheckConstraintsForStation(geoPoints);
                        }
                        else if (CurrentShapeType == ShapeType.Zone)
                        {
                            if (currentDepartmentSelected == -1)
                                throw new GisException(ResourceLoader.GetString2("DepartmentNotSelectedForZone"));

                            foreach (GeoPoint point in points)
                            {
                                if (point.Lon != 0 && point.Lat != 0)
                                    geoPoints.Add(point);
                            }
                            CheckZoneZoneOverlaps(geoPoints);
                        }
                        else if (this.CurrentShapeType == ShapeType.Route)
                        {
                            foreach (GeoPoint point in points)
                            {
                                geoPoints.Add(point);
                            }
                            if (points.Count != 0)
                            {
                                LaspointInRoute = points.Last();
                            }
                        }
                        if(geoPoints.Count > 0)
                            ApplicationServiceClient.Current(UserApplicationClientData.Administration).SendGeoPointList(geoPoints, CurrentShapeType);
                    }
                    else if (this.MapControlEx.ToolInUse == MapControlEx.ADD_POSITION_CODE)
                    {
                        FinalizeAddPosition();
                    }
                }

                points.Clear();
                this.ParentForm.Refresh();
            }
            else if (this.MapControlEx.ToolInUse == MapControlEx.ADD_POSITION_CODE)
            {
                FinalizeAddPosition();
                this.ParentForm.Refresh();
            }
            else if (this.MapControlEx.ToolInUse == MapControlEx.ADD_POLYLINE_CODE)
            {
                MapControlEx.InsertTempPolygonInLayer(points, MapControlEx.DynTableTemp);

                //TODO: USARLO ASI-> llamando a InsertTempObject. AA
                //MapPolygon mp = new MapPolygon(0, Color.Black);
                //mp.Geometry = points;
                //MapControlEx.InsertTempObject(mp, MapControlEx.DynTableTemp);

                creatingTempObject = true;
                this.ParentForm.Refresh();
            }
        }

        /// <summary>
        /// Metodo auxiliar
        /// </summary>
        /// <param name="e"></param>
        private void InitializeDataToolsMaps(ToolUsedEventArgs e)
        {
            if (this.CurrentShapeType != ShapeType.Route)
            {//Administracion
                MapControlEx.totalDistance = 0;
                this.MapControlEx.Distance = 0;
                if (this.CurrentShapeType != ShapeType.Station && this.CurrentShapeType != ShapeType.Zone)
                    MapControlEx.ClearActions(MapActions.ClearDistanceTable);
            }
            if (e.ToolName == MapControlEx.PAN_CODE)
            {//Pan normal..
                Bitmap image = new Bitmap(ResourceLoader.GetImage("$Image.PanCloseCursor"));
                MapControlEx.SetToolUseDefaultCursor(false, new Cursor(image.GetHicon()));
            }
            if (creatingTempObject == true && e.ToolName == MapControlEx.ADD_POINT)
            {//Primer nivel..
                MapControlEx.ClearActions(MapActions.ClearTempTable);
                points.Clear();
                creatingTempObject = false;
            }
            points.Clear();
            if ((this.CurrentShapeType == ShapeType.Route || this.CurrentShapeType == ShapeType.Station || this.CurrentShapeType == ShapeType.Zone) && e.ToolName == MapControlEx.ADD_POLYLINE_CODE)
            {
                if (LaspointInRoute.Lon != 0.0 && LaspointInRoute.Lat != 0.0)
                {
                    points.Add(LaspointInRoute);
                    points.Add(e.Position);
                    InsertDistanceSegment();
                    LaspointInRoute = new GeoPoint(0, 0);
                    points.RemoveAt(0);
                }
                else
                {
                    points.Add(e.Position);
                }
            }
            else
            {
                points.Add(e.Position);
            }
        }

        private void InsertDistanceSegment()
        {
            MapControlEx.InsertDistanceSegment(points, (CurrentShapeType == ShapeType.Distance));
        }

        /// <summary>
        /// Este metodo chequea las restricciones de la creacion o edicion de una estacion.
        /// </summary>
        /// <param name="geoPoints"></param>
        private void CheckConstraintsForStation(List<GeoPoint> geoPoints)
        {
            string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetDepartmentZoneAddressDataByZoneOrderByPointNumber, currentZoneSelected);
            List<DepartmentZoneAddressClientData> zonesAddress = ServerServiceClient.GetInstance().SearchClientObjects(hql).Cast<DepartmentZoneAddressClientData>().ToList();
            List<GeoPoint> zonePoints = zonesAddress.ConvertAll<GeoPoint>(delegate(DepartmentZoneAddressClientData obj)
            {
                return new GeoPoint(obj.Lon, obj.Lat);
            });

            if (geoPoints.Count > 0)
            {
                CheckStationZoneOverlaps(zonePoints);
                CheckStationStationOverlaps(geoPoints);
            }
        }

        /// <summary>
        /// Este metodo chequea si se solapan las estaciones.
        /// </summary>
        private void CheckStationStationOverlaps(List<GeoPoint> geoPoints)
        {
            string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetDepartmentStationAddressDataByZone, currentZoneSelected);
            List<DepartmentStationAddressClientData> stationsAddress = ServerServiceClient.GetInstance().SearchClientObjects(hql).Cast<DepartmentStationAddressClientData>().ToList();
            var stationsGrouped = stationsAddress.GroupBy<DepartmentStationAddressClientData, string>(z => z.Station);

            foreach (var dz in stationsGrouped)
            {
                var listSorted = dz.OrderBy(a => Int32.Parse(a.PointNumber));
                if (listSorted.ElementAt(0).StationName != currentStationName)
                {
                    List<GeoPoint> pnts = new List<GeoPoint>();
                    foreach (var dscad in listSorted) {pnts.Add(new GeoPoint(dscad.Lon, dscad.Lat));}

                    if (MapControlEx.CheckOverlaps(geoPoints, pnts, MapControlEx.DynTablePolygonStationName) == true)
                    {
                        MapControlEx.ClearActions(MapActions.ClearDistanceTable);
                        LaspointInRoute = new GeoPoint(0, 0);
                        throw new GisException(ResourceLoader.GetString2("StationOverlapAreaException"));
                    }
                }
            }
        }

        /// <summary>
        /// Este metodo chequea que la estacion este dentro de su zona.
        /// </summary>
        private void CheckStationZoneOverlaps(List<GeoPoint> zonePoints)
        {
            if (MapControlEx.CheckContains(zonePoints, points, MapControlEx.DynTablePolygonZoneName) == false)
            {
                LaspointInRoute = new GeoPoint(0, 0);
                MapControlEx.ClearActions(MapActions.ClearDistanceTable);
                throw new GisException(ResourceLoader.GetString2("ZoneDoNotHaveAreaException"));
            }
        }

        /// <summary>
        /// Este metodo chequea si la zona se solapa con otra zona
        /// </summary>
        private void CheckZoneZoneOverlaps(List<GeoPoint> zonePoints)
        {
            if (zonePoints.Count > 0)
            {
                string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetDepartmentZoneAddressByDepartmentType, currentDepartmentSelected);
                List<DepartmentZoneAddressClientData> dpes = ServerServiceClient.GetInstance().SearchClientObjects(hql).Cast<DepartmentZoneAddressClientData>().ToList();
                var dpesGrouped = dpes.GroupBy<DepartmentZoneAddressClientData, int>(z => z.DepartamentZoneCode);

                foreach (var dz in dpesGrouped)
                {
                    var listSorted = dz.OrderBy(a => Int32.Parse(a.PointNumber));
                    if (listSorted.ElementAt(0).DepartamentZoneCode != currentZoneSelected)
                    {
                        List<GeoPoint> pnts = new List<GeoPoint>();
                        foreach (var dscad in listSorted) { pnts.Add(new GeoPoint(dscad.Lon, dscad.Lat)); }

                        if (MapControlEx.CheckOverlaps(zonePoints, pnts, MapControlEx.DynTablePolygonZoneName) == true)
                        {
                            MapControlEx.ClearActions(MapActions.ClearDistanceTable);
                            LaspointInRoute = new GeoPoint(0, 0);

                            string depName = "";
                            try
                            {
                                DepartmentTypeClientData dt =
                                    (DepartmentTypeClientData)ServerServiceClient.GetInstance().SearchClientObject(
                                        SmartCadHqls.GetCustomHql(SmartCadHqls.GetDepartmentsTypeByCode, currentDepartmentSelected));

                                depName = dt.Name;
                            }
                            catch { }
                            
                            throw new GisException(ResourceLoader.GetString2("ZoneOverlapAreaException", depName));
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Metodo auxiliar
        /// </summary>
        private void FinalizeAddPosition()
        {
            if (points.Count == 1)
            {
                GeoPoint geoPoint = new GeoPoint(points[0].Lon, points[0].Lat);
                MapControlEx.InsertPointInTempLayer(geoPoint);

                //TODO: USARLO ASI-> llamando a InsertTempObject. AA
                //MapPoint mp = new MapPoint(0, new GeoPoint(points[0].Lon, points[0].Lat), "");
                //MapControlEx.InsertTempObject(mp, MapControlEx.DynTableTemp);

                creatingTempObject = true;

                if (MapIsInitialized)
                {
                    UserApplicationClientData application = (CurrentShapeType == ShapeType.Incident) ?
                        UserApplicationClientData.FirstLevel :
                        UserApplicationClientData.Administration;

                    ApplicationServiceClient.Current(application).SendGeoPoint(geoPoint);
                }
            }
            points.Clear();
        }

        private void ToolEnding(object sender, ToolEndingEventArgs e)
        {
            if (e.ToolName == MapControlEx.PAN_CODE)
            {
                Bitmap image = new Bitmap(ResourceLoader.GetImage("$Image.PanCursor"));

                MapControlEx.SetToolUseDefaultCursor(false, new Cursor(image.GetHicon()));
            }
        }

        /// <summary>
        /// Este metodo se llama cuando una herramienta se agrega al set
        /// de herramientas de mapas.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Tools_FeatureAdding(object sender, FeatureAddingEventArgs e)
        {
            //e.Cancel = true;
            ToolUsed(null, null);
        }

        private void treeListMaps_CompareNodeValues(object sender, CompareNodeValuesEventArgs e)
        {
            TreeListNodeEx node1 = e.Node1 as TreeListNodeEx;
            TreeListNodeEx node2 = e.Node2 as TreeListNodeEx;
            if (!string.IsNullOrEmpty(node1.MapLayerName))
            {
                if (node1.MapLayerName.EndsWith("Labels") == true)
                {
                    e.Result = 1;
                }
                if (node2.MapLayerName.EndsWith("Labels") == true)
                {
                    e.Result = -1;
                }
                else
                {
                    e.Result = GetIndexInTree(node1.MapLayerName) - GetIndexInTree(node2.MapLayerName);
                }
            }
        }
        
        private CheckState GetInitialStateLayer(string layerName)
		{
            LayerElement le = SmartCadConfiguration.SmartCadSection.Maps[MapControlEx.CurrentMap].Layers[layerName] as LayerElement;
			if(le!= null && le.Selectable.ToLower() == bool.TrueString.ToLower())
                return CheckState.Checked;
            else
                return CheckState.Unchecked;
		}

		private int GetIndexInTree(string name)
		{
			try
			{
				return Int32.Parse(ResourceLoader.GetString2(Country + City + name + "OrderInTree"));
			}
			catch
			{
				return -1;
			}
		}

		private void treeListSmartCad_CompareNodeValues(object sender, DevExpress.XtraTreeList.CompareNodeValuesEventArgs e)
		{

			TreeListNodeEx node1 = e.Node1 as TreeListNodeEx;
			TreeListNodeEx node2 = e.Node2 as TreeListNodeEx;
			if (node2.TypeNodeLeaf != TreeListNodeExType.None && node1.TypeNodeLeaf != TreeListNodeExType.None)
			{
				if (e.Node1.Level == 0)
				{

				}
				else if (e.Node1.Level == 1)
				{
					List<TreeListNodeExType> typeArr = new List<TreeListNodeExType> { TreeListNodeExType.DepartamentZoneRoot, 
                        TreeListNodeExType.DepartamentStationRoot, 
                        TreeListNodeExType.UnitsRoot, 
                        TreeListNodeExType.IncidentRoot, 
                        TreeListNodeExType.PostRoot, 
                        TreeListNodeExType.ALARMRoot};
					e.Result = (typeArr.IndexOf(node1.TypeNodeLeaf) - typeArr.IndexOf(node2.TypeNodeLeaf));
				}
				else if (e.Node1.Level == 2)
				{
					if (node2.TypeNodeLeaf == node1.TypeNodeLeaf)
					{
						e.Result = node1.LayerName.CompareTo(node2.LayerName);
					}
					else
					{
						List<TreeListNodeExType> typeArr = new List<TreeListNodeExType> { 
                            TreeListNodeExType.AreaLayer, 
                            TreeListNodeExType.UbicationLayer, 
                            TreeListNodeExType.LabelLayer, 
                            TreeListNodeExType.IncidentLayer, 
                            TreeListNodeExType.CCTVLayer, 
                            TreeListNodeExType.DepartamentType, 
                            TreeListNodeExType.UbicationRoot, 
                            TreeListNodeExType.DepartamentZoneUbicationRoot, 
                            TreeListNodeExType.DepartamentStationUbicationRoot, 
                            TreeListNodeExType.PostLayer, 
                            TreeListNodeExType.ALARMLayer };
						e.Result = (typeArr.IndexOf(node1.TypeNodeLeaf) - typeArr.IndexOf(node2.TypeNodeLeaf));
					}
				}
				else
				{
					if (node1.LayerName != null && node2.LayerName != null)
						e.Result = node1.LayerName.CompareTo(node2.LayerName);
				}
			}
		}

		private void WorkWithDepartamentStationRoot(TreeListNodeEx node)
		{
			CheckState ch = node.CheckState;
            TableStations.MakeAllVisible(ch == CheckState.Checked);
			for (int i = 0; i < node.Nodes.Count; i++)
			{
				TreeListNodeEx ndx = node.Nodes[i] as TreeListNodeEx;
				ndx.CheckState = ch;
				if (ndx.TypeNodeLeaf == TreeListNodeExType.DepartamentStationUbicationRoot || ndx.TypeNodeLeaf == TreeListNodeExType.DepartamentStationUbicationRoot)
				{
					WorkWithUbication(ndx);
				}
                else if (ndx.TypeNodeLeaf == TreeListNodeExType.LabelLayer)
                {
                    SetCheckLabels(ndx);
                }
			}
		}

		private void WorkWithDepartamentZoneRoot(TreeListNodeEx node)
		{
			CheckState ch = node.CheckState;
            TableZones.MakeAllVisible(ch == CheckState.Checked);

			for (int i = 0; i < node.Nodes.Count; i++)
			{
				TreeListNodeEx ndx = node.Nodes[i] as TreeListNodeEx;
				ndx.CheckState = ch;
				if (ndx.TypeNodeLeaf == TreeListNodeExType.DepartamentZoneUbicationRoot || ndx.TypeNodeLeaf == TreeListNodeExType.DepartamentZoneUbicationLayer)
				{
					WorkWithUbication(ndx);
				}
                else if (ndx.TypeNodeLeaf == TreeListNodeExType.LabelLayer)
                {
                    SetCheckLabels(ndx);
                }
			}
		}

		private void WorkWithUbication(TreeListNodeEx node)
		{
			CheckState original = node.CheckState;
			for (int i = 0; i < node.Nodes.Count; i++)
			{
				TreeListNodeEx ndx = node.Nodes[i] as TreeListNodeEx;
				ndx.CheckState = original;
				if (ndx.TypeNodeLeaf == TreeListNodeExType.DepartamentZoneUbicationRoot || ndx.TypeNodeLeaf == TreeListNodeExType.DepartamentZoneUbicationLayer)
				{
                    SetCheckUbication(ndx, typeof(DepartmentZoneClientData));
				}
				else if (ndx.TypeNodeLeaf == TreeListNodeExType.LabelLayer)
				{
					WorkWithLabels(ndx);
				}
				else if (ndx.TypeNodeLeaf == TreeListNodeExType.DepartamentStationUbicationRoot || ndx.TypeNodeLeaf == TreeListNodeExType.DepartamentStationUbicationLayer)
				{
                    SetCheckUbication(ndx, typeof(DepartmentStationClientData));
				}
			}
		}

		private void SetCheckUbication(TreeListNodeEx node, Type t)
		{
			if (node.CheckState == CheckState.Checked)
			{
				if (node.HasChildren)
				{
					List<int> stationsOn = new List<int>();
					SetCheckedChildNodes2(node, node.CheckState, stationsOn);
                    MakeObjectsVisible(t, true, stationsOn);
				}
				else
				{
					int codeStation = (int)node.Tag;
                    MakeObjectVisible(t, true, codeStation);
				}
				SetCheckedParentNodes2(node, node.CheckState);
			}                      
			else if (node.CheckState == CheckState.Unchecked)
			{
				if (node.HasChildren)
				{
					List<int> stationsOff = new List<int>();
					SetCheckedChildNodes2(node, node.CheckState, stationsOff);
                    MakeObjectsVisible(t, false, stationsOff);
				}
				else
				{
					int codeStation = (int)node.Tag;
                    MakeObjectVisible(t, false, codeStation);

				}
				SetCheckedParentNodes2(node, node.CheckState);

			}
			else
			{
				MessageForm.Show("ERROR", MessageFormType.Information);
			}
		}

        private void MakeObjectsVisible(Type t, bool state, List<int> stationsOn)
        {
            if (t == typeof(DepartmentStationClientData))
                TableStations.MakeObjectsVisible(state, stationsOn);
            else
                TableZones.MakeObjectsVisible(state, stationsOn);
        }

        private void MakeObjectVisible(Type t, bool state, int codeStation)
        {
            if (t == typeof(DepartmentStationClientData))
                TableStations.MakeObjectVisible(state, codeStation);
            else
                TableZones.MakeObjectVisible(state, codeStation);
        }

		private void SetCheckedChildNodes2(TreeListNode node, CheckState check, List<int> codes)
		{
			for (int i = 0; i < node.Nodes.Count; i++)
			{
				node.Nodes[i].CheckState = check;
				if (!node.Nodes[i].HasChildren)
				{
					TreeListNodeEx aux = node.Nodes[i] as TreeListNodeEx;
					if (aux.TypeNodeLeaf == TreeListNodeExType.DepartamentStation || aux.TypeNodeLeaf == TreeListNodeExType.DepartamentStationUbicationLayer)
					{
						int codeStation = (int)node.Nodes[i].Tag;
						codes.Add(codeStation);
					}
				}
				else
				{
					SetCheckedChildNodes2(node.Nodes[i], check, codes);
				}
			}
		}

		private void WorkWithPost(TreeListNodeEx node)
		{
            CheckState original = node.CheckState;
            TableStructs.MakeAllVisible(original == CheckState.Checked);

			for (int i = 0; i < node.Nodes.Count; i++)
			{
				TreeListNodeEx ndx = node.Nodes[i] as TreeListNodeEx;
				ndx.CheckState = original;
                //if (ndx.TypeNodeLeaf == TreeListNodeExType.PostLayer)
                //{
                //    TableStructs.MakeObjectVisible((original == CheckState.Checked ? true : false), (int)ndx.Tag);
                //}
                //else
                if (ndx.TypeNodeLeaf == TreeListNodeExType.LabelLayer)
				{
					SetCheckLabels(ndx);
				}
			}
			node.CheckState = original;
            SetCheckedParentNodes2(node, original);
		}

        private void WorkWithIncidents(TreeListNodeEx node)
        {
            CheckState original = node.CheckState;
            bool originalstate = (original == CheckState.Checked ? true : false);

            for (int i = 0; i < node.Nodes.Count; i++)
            {
                node.Nodes[i].CheckState = original;
                TreeListNodeEx ndx = node.Nodes[i] as TreeListNodeEx;

                if (ndx.TypeNodeLeaf == TreeListNodeExType.IncidentLayer)
                {
                    TableIncidents.MakeObjectsVisible(originalstate, null, GlobalApplicationPreference["FrontClientIncidentCodePrefix"].Value, GlobalApplicationPreference["IsIncidentCodeSuffix"].Value);
                }
                else if (ndx.TypeNodeLeaf == TreeListNodeExType.CCTVLayer)
                {
                    TableIncidents.MakeObjectsVisible(originalstate, null, GlobalApplicationPreference["CCTVIncidentCodePrefix"].Value, GlobalApplicationPreference["IsIncidentCodeSuffix"].Value);
                }
                else if (ndx.TypeNodeLeaf == TreeListNodeExType.LPRLayer || ndx.TypeNodeLeaf == TreeListNodeExType.MovilSecurityLayer)
                {
                }
                else if (ndx.TypeNodeLeaf == TreeListNodeExType.ALARMLayer)
                {
                    TableIncidents.MakeObjectsVisible(originalstate, null, GlobalApplicationPreference["ALARMIncidentCodePrefix"].Value, GlobalApplicationPreference["IsIncidentCodeSuffix"].Value);
                }
                else
                {
                    if (node.Nodes[i].Tag != null)
                    {
                        List<string> layers = node.Nodes[i].Tag as List<string>;
                        foreach (string layerName in layers)
                        {
                            MapControlEx.EnableLayer(layerName, (original == CheckState.Checked) ? true : false);
                        }
                    }
                }
            }
        }

        private void WorkWithLabels(TreeListNodeEx node)
        {
            CheckState original = node.CheckState;
            for (int i = 0; i < node.Nodes.Count; i++)
            {
                node.Nodes[i].CheckState = original;
                SetCheckLabels(node.Nodes[i]);
            }
        }

		private void SetCheckLabels(TreeListNode treeListNode)
		{
			TreeListNodeEx aux = treeListNode as TreeListNodeEx;
			SetCheckedParentNodes2(treeListNode, treeListNode.CheckState);

            List<string> layers = (aux.Tag as List<string>);
            foreach (string layerName in layers)
            {
                MapControlEx.EnableLayer(layerName, ((aux.CheckState == CheckState.Checked) ? true : false));
            }            
		}

        private void WorkWithDepartaments(TreeListNodeEx node)
        {
            CheckState original = node.CheckState;
            TableUnits.MakeAllVisible(original == CheckState.Checked);

            for (int i = 0; i < node.Nodes.Count; i++)
            {
                TreeListNodeEx ndx = node.Nodes[i] as TreeListNodeEx;
                ndx.CheckState = original;
                if (ndx.TypeNodeLeaf != TreeListNodeExType.LabelLayer)
                    SetCheckDepartaments(node.Nodes[i]);
                else
                    SetCheckLabels(ndx);
            }
            
        }

		private void SetCheckDepartaments(TreeListNode node)
		{
			if (node.CheckState == CheckState.Checked)
			{
				if (node.HasChildren)
				{
					List<int> stationsOn = new List<int>();
					SetCheckedChildNodes2(node, node.CheckState, stationsOn);
					TableUnits.MakeObjectsVisible(true, stationsOn);
				}
				else
				{
					int codeStation = (int)node.Tag;
                    TableUnits.MakeObjectVisible(true, codeStation);
				}
				SetCheckedParentNodes2(node, node.CheckState);
			}
			else if (node.CheckState == CheckState.Unchecked)
			{
				if (node.HasChildren)
				{
					List<int> stationsOff = new List<int>();
					SetCheckedChildNodes2(node, node.CheckState, stationsOff);
                    TableUnits.MakeObjectsVisible(false, stationsOff);
				}
				else
				{
					int codeStation = (int)node.Tag;
                    TableUnits.MakeObjectVisible(false, codeStation);
				}
				SetCheckedParentNodes2(node, node.CheckState);
			}
			else
			{
				MessageForm.Show("ERROR", MessageFormType.Information);
			}
		}

		private void CheckAllNodes(TreeListNode root)
		{
			CheckState original = root.CheckState;
			for (int i = 0; i < root.Nodes.Count; i++)
			{
				TreeListNodeEx node = root.Nodes[i] as TreeListNodeEx;
				node.CheckState = original;
				switch (node.TypeNodeLeaf)
				{
					case TreeListNodeExType.DepartamentZoneRoot:
						WorkWithDepartamentZoneRoot(node);
						break;
					case TreeListNodeExType.DepartamentStationRoot:
						WorkWithDepartamentStationRoot(node);
						break;
					case TreeListNodeExType.UnitsRoot:
						WorkWithDepartaments(node);
						break;
					case TreeListNodeExType.IncidentRoot:
						WorkWithIncidents(node);
						break;
					case TreeListNodeExType.PostRoot:
						WorkWithPost(node);
						break;
                    case TreeListNodeExType.RouteRoot:
                        WorkWithRoutesRoot(node);
                        break;
					default:
						break;
				}
			}
			root.CheckState = original;
		}

		private void WorkWithRoutesRoot(TreeListNode node)
		{
			CheckState original = node.CheckState;
			for (int i = 0; i < node.Nodes.Count; i++)
			{
				TreeListNodeEx ndx = node.Nodes[i] as TreeListNodeEx;
				ndx.CheckState = original;
				Console.WriteLine(" = " + ndx.LayerName);
				if (ndx.TypeNodeLeaf == TreeListNodeExType.RouteDepartmetTypeRoot)
				{
					WorkWithRoutes(ndx);
				}
				if (ndx.TypeNodeLeaf == TreeListNodeExType.LabelLayer)
				{
					SetCheckLabels(ndx);
				}
			}
			node.CheckState = original;
		}

		private void WorkWithRoutes(TreeListNode node)
		{
			CheckState original = node.CheckState;
			for (int i = 0; i < node.Nodes.Count; i++)
			{
				TreeListNodeEx ndx = node.Nodes[i] as TreeListNodeEx;
				ndx.CheckState = original;
				WorkWithRoute(ndx);
			}
		}

		private void WorkWithRoute(TreeListNode node)
		{
			int code = (int)node.Tag;
            TableRoutes.MakeObjectVisible((node.CheckState == CheckState.Checked?true:false), code);

			SetCheckedParentNodes2(node, node.CheckState);
		}

		public void RaiseBackGroundProcess()
		{
			Thread th = new Thread(new ThreadStart(CallBackGroundProcess));
			th.Start();
		}

		private void CallBackGroundProcess()
		{
			try
			{
				BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[1] { GetType().GetMethod("BackGroundProcess", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[0] });
				processForm.CanThrowError = true;
				processForm.ShowDialog();
			}
			catch
			{

			}
		}

		private void BackGroundProcess()
		{
			do
				Thread.Sleep(250);
			while (!closeBackGroundDialog);
		}

		private void treeListSmartCad_BeforeCheckNode(object sender, DevExpress.XtraTreeList.CheckNodeEventArgs e)
		{
			e.State = (e.PrevState == CheckState.Checked ? CheckState.Unchecked : CheckState.Checked);
		}

		private List<TreeListNodeExType> FilterByTreeViewByProfile()
		{
			int role = ServerServiceClient.GetInstance().OperatorClient.Code;
			string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorWithAccessByApp, role, UserApplicationClientData.Map.Name);
			IList listAccess = ServerServiceClient.GetInstance().SearchClientObjects(hql);
			PermissionsUser = new List<TreeListNodeExType>();

			Dictionary<string, TreeListNodeExType> correlation = new Dictionary<string, TreeListNodeExType>();
			correlation["MapLayerUnits"] = TreeListNodeExType.UnitsRoot;
			correlation["MapLayerPostMap"] = TreeListNodeExType.PostRoot;
			correlation["MapLayerDepartmentStations"] = TreeListNodeExType.DepartamentStationRoot;
			correlation["MapLayerDepartmentZones"] = TreeListNodeExType.DepartamentZoneRoot;
			correlation["MapLayerIncidents"] = TreeListNodeExType.IncidentRoot;
			correlation["MapLayerLPR"] = TreeListNodeExType.LPRRoot;
			correlation["MapLayerMovilSecurity"] = TreeListNodeExType.MovilSecurityRoot;
            correlation["MapLayerAlarm"] = TreeListNodeExType.ALARMRoot;

			foreach (UserAccessClientData var in listAccess)
				if (correlation.ContainsKey(var.UserPermission.UserResource.Name) == true ||
					(var.UserPermission.UserResource.Name.Equals("HistoricTracks") &&
					 var.UserPermission.UserResource.Name.Equals("FleetControl")))
					PermissionsUser.Add(correlation[var.UserPermission.UserResource.Name]);

			return PermissionsUser;
		}

		private void AddTreeViewIncidentsNodes(TreeListNodeEx incidentsNode)
		{
            var applications = ServerServiceClient.GetInstance().GetActiveApplications();


            if (applications.Contains(SmartCadApplications.SmartCadFirstLevel)) 
            {
                TreeListNodeEx firtsLevel = (TreeListNodeEx)treeListSmartCad.AppendNode(ResourceLoader.GetString2("FirstLevel"), incidentsNode);
                firtsLevel.LayerName = ResourceLoader.GetString2("FirstLevel");
                firtsLevel.TypeNodeLeaf = TreeListNodeExType.IncidentLayer;
                firtsLevel.CheckState = CheckState.Checked;
            }
            if (applications.Contains(SmartCadApplications.SmartCadCCTV) || applications.Contains(SmartCadApplications.SmartCadAlarm)) 
            {
                TreeListNodeEx cctv = (TreeListNodeEx)treeListSmartCad.AppendNode(ResourceLoader.GetString2("Cctv"), incidentsNode);
                cctv.LayerName = ResourceLoader.GetString2("Cctv");
                cctv.TypeNodeLeaf = TreeListNodeExType.CCTVLayer;
                cctv.CheckState = CheckState.Checked;
            }
			

            //COMENTED FOR RELEASE 3.2.8 NOT REQUIRED

            //Until is added to the maps.
            //TreeListNodeEx lpr = (TreeListNodeEx)treeListSmartCad.AppendNode(ResourceLoader.GetString2("LPR"), incidentsNode);
            //lpr.LayerName = ResourceLoader.GetString2("LPR");
            //lpr.TypeNodeLeaf = TreeListNodeExType.LPRLayer;

            //TreeListNodeEx dvr = (TreeListNodeEx)treeListSmartCad.AppendNode(ResourceLoader.GetString2("MovilSecurity"), incidentsNode);
            //dvr.LayerName = ResourceLoader.GetString2("MovilSecurity");
            //dvr.TypeNodeLeaf = TreeListNodeExType.MovilSecurityLayer;

            //TreeListNodeEx alarm = (TreeListNodeEx)treeListSmartCad.AppendNode(ResourceLoader.GetString2("Alarm"), incidentsNode);
            //alarm.LayerName = ResourceLoader.GetString2("Alarm");
            //alarm.TypeNodeLeaf = TreeListNodeExType.ALARMLayer;
            //alarm.CheckState = CheckState.Checked;

			/*List<MapLayer> list = new List<MapLayer>();
			list.Add(lyr);   
			firtsLevel.Tag = cctv.Tag = lpr.Tag = dvr.Tag = list;*/
		}

        /// <summary>
        /// List of units depending on the  Application Preference
        /// </summary>
        /// <returns></returns>
        public List<UnitClientData> GetUnits()
        {
            if (CanSeeAllDeps)
                return GetListUnits();
            else
                return GetListUnitsOperator();
        }

		public List<UnitClientData> GetListUnits()
		{
			IList list = ServerServiceClient.GetInstance().SearchClientObjectsMaxRows(SmartCadHqls.GetUnitOrderByCode, true);
			List<UnitClientData> retval = new List<UnitClientData>();
			foreach (UnitClientData unit in list)
			{
				retval.Add(unit);
			}
			return retval;
		}

        private List<UnitClientData> GetListUnitsOperator()
		{
            string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetUnitsByOperatorLogin, ServerServiceClient.GetInstance().OperatorClient.Login);
            IList list = ServerServiceClient.GetInstance().SearchClientObjects(hql);
            List<UnitClientData> retval = new List<UnitClientData>();
			foreach (UnitClientData unit in list)
			{
				retval.Add(unit);
			}
			return retval;
		}

		private List<IncidentClientData> GetIncidentList()
		{
			//traer los incidentes abiertos....
			IList list = ServerServiceClient.GetInstance().SearchClientObjectsMaxRows(SmartCadHqls.GetCustomHql(SmartCadHqls.GetIncidentsByStatus, IncidentStatusClientData.Open.Name), true);
			List<IncidentClientData> retval = new List<IncidentClientData>();
			foreach (IncidentClientData var in list)
			{
				retval.Add(var);
			}

			return retval;
		}

		private List<StructClientData> GetPostList()
		{
			IList list =
			   ServerServiceClient.GetInstance().SearchClientObjects(typeof(StructClientData));
			List<StructClientData> retval = new List<StructClientData>();
			foreach (StructClientData var in list)
			{
				retval.Add(var);
			}

			return retval;
		}

		private List<DepartmentZoneClientData> GetPolygonZoneList()
		{

			IList list =ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetDepartmentZonesWithAddress);
			List<DepartmentZoneClientData> retval = new List<DepartmentZoneClientData>();
			foreach (DepartmentZoneClientData var in list)
			{
				retval.Add(var);
			}

			return retval;
		}

		private List<DepartmentStationClientData> GetPolygonStationList()
		{
            IList list = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetDepartmentsStation);
            List<DepartmentStationClientData> retval = new List<DepartmentStationClientData>();
            foreach (DepartmentStationClientData var in list)
            {
                retval.Add(var);
            }

			return retval;
		}

		private List<RouteClientData> GetRoutesList()
		{
			List<RouteClientData> retval = ServerServiceClient.GetInstance().SearchClientObjects("SELECT obj FROM RouteData obj", true).Cast<RouteClientData>().ToList();
			return retval;
		}

		private void PrepareTreeListSmartCad(TreeListNodeEx supeRootNode)
		{
			treeListSmartCad.BeginUpdate();
			supeRootNode.Expanded = true;
			for (int i = 0; i < supeRootNode.Nodes.Count; i++)
			{
				TreeListNodeEx aux = supeRootNode.Nodes[i] as TreeListNodeEx;
				if (aux.TypeNodeLeaf == TreeListNodeExType.IncidentRoot)
				{
					aux.Expanded = true;
				}
				else
					aux.Expanded = false;
			}
			treeListSmartCad.EndUpdate();
		}

		private void treeListMaps_AfterExpand(object sender, NodeEventArgs e)
        {
            treeListMaps.BestFitColumns();
        }

        private void treeListMaps_AfterCollapse(object sender, NodeEventArgs e)
        {
            treeListMaps.BestFitColumns();
        }

        private void treeListMaps_SizeChanged(object sender, EventArgs e)
        {
            treeListMaps.BestFitColumns();
        }

        private void treeListSmartCad_SizeChanged(object sender, EventArgs e)
        {
            treeListSmartCad.BestFitColumns();
        }

        private void treeListSmartCad_AfterCollapse(object sender, NodeEventArgs e)
        {
            treeListSmartCad.BestFitColumns();
        }

		public void HideSmartCadContent()
		{
			layoutControlGroupSmartCad.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
		}

        internal void ClearMap()
        {
            points.Clear();
            MapControlEx.ClearActions(MapActions.ClearTempTable);
            MapControlEx.ClearActions(MapActions.ClearDistanceTable);

            FormUtil.InvokeRequired(this, delegate
           {
               if (MapIsInitialized)
               {
                   if (creatingTempObject == true)
                   {
                       ApplicationServiceClient.Current(UserApplicationClientData.FirstLevel).SendGeoPoint(new GeoPoint(0, 0));
                   }
                   if (CurrentShapeType == ShapeType.Route || CurrentShapeType == ShapeType.Station || CurrentShapeType == ShapeType.Zone)
                   {// We Clear Form in Administration.

                       ApplicationServiceClient.Current(UserApplicationClientData.Administration).SendGeoPointList(new List<GeoPoint>(), ShapeType.None);
                   }
               }
            });
            SendKeys.Send("{ESC}");
        }
    }
}
