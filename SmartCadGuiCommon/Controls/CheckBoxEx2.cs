﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SmartCadGuiCommon.Controls
{
    public class CheckBoxEx2 : CheckBox
    {
        private string notVisibleText = "";

        public string NotVisibleText
        {

            get
            {
                return this.notVisibleText;
            }
            set
            {
                this.notVisibleText = value;
            }
        }


        protected override void OnPaint(PaintEventArgs pevent)
        {
            base.OnPaint(pevent);

            if (Enabled == false)
                pevent.Graphics.DrawString(this.NotVisibleText, this.Font, new SolidBrush(SystemColors.ControlDark), 17, 1);
            else
                pevent.Graphics.DrawString(this.NotVisibleText, this.Font, new SolidBrush(Color.Black), 17, 1);
        }
    }
}
