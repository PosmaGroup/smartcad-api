using SmartCadControls.Controls;
using System.Windows.Forms;

namespace SmartCadGuiCommon.Controls
{
    partial class QuestionsControlDevX
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonExBack = new ButtonEx();
            this.buttonExForward = new ButtonEx();
            this.buttonExQuestion15 = new VistaButtonEx();
            this.buttonExQuestion14 = new VistaButtonEx();
            this.buttonExQuestion13 = new VistaButtonEx();
            this.buttonExQuestion6 = new VistaButtonEx();
            this.buttonExQuestion11 = new VistaButtonEx();
            this.buttonExQuestion12 = new VistaButtonEx();
            this.buttonExQuestion4 = new VistaButtonEx();
            this.buttonExQuestion8 = new VistaButtonEx();
            this.buttonExQuestion1 = new VistaButtonEx();
            this.buttonExQuestion5 = new VistaButtonEx();
            this.buttonExQuestion9 = new VistaButtonEx();
            this.buttonExQuestion7 = new VistaButtonEx();
            this.buttonExQuestion2 = new VistaButtonEx();
            this.buttonExQuestion3 = new VistaButtonEx();
            this.buttonExQuestion10 = new VistaButtonEx();
            this.labelExLongQuestion = new LabelEx();
            this.panelIncidentTypes = new PanelEx();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.listBoxIncidentTypes = new ListBoxEx();
            this.buttonOK = new VistaButtonEx();
            this.buttonAddIncidentType = new VistaButtonEx();
            this.buttonRemoveAllIncidentType = new VistaButtonEx();
            this.listBoxSelectedIncidentTypes = new ListBoxEx();
            this.buttonAddAllIncidentType = new VistaButtonEx();
            this.buttonRemoveIncidentType = new VistaButtonEx();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemIncidentTypes = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemAddIncidentType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemAddAllIncidentType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemRemoveIncidentType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemRemoveAllIncidentType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemSelectedIncidentTypes = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemButtonOk = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.buttonIncidentTypes = new VistaButtonEx();
            this.toolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.groupControlBody = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.textBoxSelectedIncidentTypes = new DevExpress.XtraEditors.TextEdit();
            this.pictureBoxNumber = new DevExpress.XtraEditors.PictureEdit();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.dragDropLayoutControl1 = new DragDropLayoutControl();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemLabelExLongQuestion = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemNext = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemBack = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemButtonIncidentTypes = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemSelectedIncidents = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemPictureBox = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.toolTipController = new DevExpress.Utils.ToolTipController(this.components);
            this.panelIncidentTypes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIncidentTypes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAddIncidentType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAddAllIncidentType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemRemoveIncidentType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemRemoveAllIncidentType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSelectedIncidentTypes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemButtonOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlBody)).BeginInit();
            this.groupControlBody.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxSelectedIncidentTypes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLabelExLongQuestion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemNext)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemButtonIncidentTypes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSelectedIncidents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonExBack
            // 
            this.buttonExBack.Enabled = false;
            this.buttonExBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExBack.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonExBack.ForeColor = System.Drawing.Color.Black;
            this.buttonExBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonExBack.Location = new System.Drawing.Point(586, 86);
            this.buttonExBack.Name = "buttonExBack";
            this.buttonExBack.Size = new System.Drawing.Size(21, 21);
            this.buttonExBack.TabIndex = 16;
            this.buttonExBack.Text = "<";
            this.buttonExBack.UseVisualStyleBackColor = true;
            this.buttonExBack.Click += new System.EventHandler(this.buttonExBack_Click);
            this.buttonExBack.MouseLeave += new System.EventHandler(this.questionControlButton_MouseLeave);
            // 
            // buttonExForward
            // 
            this.buttonExForward.Enabled = false;
            this.buttonExForward.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExForward.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonExForward.ForeColor = System.Drawing.Color.Black;
            this.buttonExForward.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonExForward.Location = new System.Drawing.Point(611, 86);
            this.buttonExForward.Name = "buttonExForward";
            this.buttonExForward.Size = new System.Drawing.Size(21, 21);
            this.buttonExForward.TabIndex = 17;
            this.buttonExForward.Text = ">";
            this.buttonExForward.UseVisualStyleBackColor = true;
            this.buttonExForward.Click += new System.EventHandler(this.buttonExForward_Click);
            this.buttonExForward.MouseLeave += new System.EventHandler(this.questionControlButton_MouseLeave);
            // 
            // buttonExQuestion15
            // 
            this.buttonExQuestion15.BackColor = System.Drawing.Color.Transparent;
            this.buttonExQuestion15.BaseColor = System.Drawing.Color.White;
            this.buttonExQuestion15.ButtonColor = System.Drawing.Color.LightGray;
            this.buttonExQuestion15.DisabledButtonColor = System.Drawing.Color.Gray;
            this.buttonExQuestion15.Enabled = false;
            this.buttonExQuestion15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExQuestion15.ForeColor = System.Drawing.Color.Black;
            this.buttonExQuestion15.Location = new System.Drawing.Point(506, 58);
            this.buttonExQuestion15.Name = "buttonExQuestion15";
            this.buttonExQuestion15.Size = new System.Drawing.Size(126, 24);
            this.buttonExQuestion15.TabIndex = 14;
            this.buttonExQuestion15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonExQuestion15.Click += new System.EventHandler(this.buttonExQuestion1_Click);
            this.buttonExQuestion15.MouseLeave += new System.EventHandler(this.questionControlButton_MouseLeave);
            // 
            // buttonExQuestion14
            // 
            this.buttonExQuestion14.BackColor = System.Drawing.Color.Transparent;
            this.buttonExQuestion14.BaseColor = System.Drawing.Color.White;
            this.buttonExQuestion14.ButtonColor = System.Drawing.Color.LightGray;
            this.buttonExQuestion14.DisabledButtonColor = System.Drawing.Color.Gray;
            this.buttonExQuestion14.Enabled = false;
            this.buttonExQuestion14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExQuestion14.ForeColor = System.Drawing.Color.Black;
            this.buttonExQuestion14.Location = new System.Drawing.Point(380, 58);
            this.buttonExQuestion14.Name = "buttonExQuestion14";
            this.buttonExQuestion14.Size = new System.Drawing.Size(122, 24);
            this.buttonExQuestion14.TabIndex = 13;
            this.buttonExQuestion14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonExQuestion14.Click += new System.EventHandler(this.buttonExQuestion1_Click);
            this.buttonExQuestion14.MouseLeave += new System.EventHandler(this.questionControlButton_MouseLeave);
            // 
            // buttonExQuestion13
            // 
            this.buttonExQuestion13.BackColor = System.Drawing.Color.Transparent;
            this.buttonExQuestion13.BaseColor = System.Drawing.Color.White;
            this.buttonExQuestion13.ButtonColor = System.Drawing.Color.LightGray;
            this.buttonExQuestion13.DisabledButtonColor = System.Drawing.Color.Gray;
            this.buttonExQuestion13.Enabled = false;
            this.buttonExQuestion13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExQuestion13.ForeColor = System.Drawing.Color.Black;
            this.buttonExQuestion13.Location = new System.Drawing.Point(254, 58);
            this.buttonExQuestion13.Name = "buttonExQuestion13";
            this.buttonExQuestion13.Size = new System.Drawing.Size(122, 24);
            this.buttonExQuestion13.TabIndex = 12;
            this.buttonExQuestion13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonExQuestion13.Click += new System.EventHandler(this.buttonExQuestion1_Click);
            this.buttonExQuestion13.MouseLeave += new System.EventHandler(this.questionControlButton_MouseLeave);
            // 
            // buttonExQuestion6
            // 
            this.buttonExQuestion6.BackColor = System.Drawing.Color.Transparent;
            this.buttonExQuestion6.BaseColor = System.Drawing.Color.White;
            this.buttonExQuestion6.ButtonColor = System.Drawing.Color.LightGray;
            this.buttonExQuestion6.DisabledButtonColor = System.Drawing.Color.Gray;
            this.buttonExQuestion6.Enabled = false;
            this.buttonExQuestion6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExQuestion6.ForeColor = System.Drawing.Color.Black;
            this.buttonExQuestion6.Location = new System.Drawing.Point(2, 30);
            this.buttonExQuestion6.Name = "buttonExQuestion6";
            this.buttonExQuestion6.Size = new System.Drawing.Size(122, 24);
            this.buttonExQuestion6.TabIndex = 5;
            this.buttonExQuestion6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonExQuestion6.Click += new System.EventHandler(this.buttonExQuestion1_Click);
            this.buttonExQuestion6.MouseLeave += new System.EventHandler(this.questionControlButton_MouseLeave);
            // 
            // buttonExQuestion11
            // 
            this.buttonExQuestion11.BackColor = System.Drawing.Color.Transparent;
            this.buttonExQuestion11.BaseColor = System.Drawing.Color.White;
            this.buttonExQuestion11.ButtonColor = System.Drawing.Color.LightGray;
            this.buttonExQuestion11.DisabledButtonColor = System.Drawing.Color.Gray;
            this.buttonExQuestion11.Enabled = false;
            this.buttonExQuestion11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExQuestion11.ForeColor = System.Drawing.Color.Black;
            this.buttonExQuestion11.Location = new System.Drawing.Point(2, 58);
            this.buttonExQuestion11.Name = "buttonExQuestion11";
            this.buttonExQuestion11.Size = new System.Drawing.Size(122, 24);
            this.buttonExQuestion11.TabIndex = 10;
            this.buttonExQuestion11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonExQuestion11.Click += new System.EventHandler(this.buttonExQuestion1_Click);
            this.buttonExQuestion11.MouseLeave += new System.EventHandler(this.questionControlButton_MouseLeave);
            // 
            // buttonExQuestion12
            // 
            this.buttonExQuestion12.BackColor = System.Drawing.Color.Transparent;
            this.buttonExQuestion12.BaseColor = System.Drawing.Color.White;
            this.buttonExQuestion12.ButtonColor = System.Drawing.Color.LightGray;
            this.buttonExQuestion12.DisabledButtonColor = System.Drawing.Color.Gray;
            this.buttonExQuestion12.Enabled = false;
            this.buttonExQuestion12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExQuestion12.ForeColor = System.Drawing.Color.Black;
            this.buttonExQuestion12.Location = new System.Drawing.Point(128, 58);
            this.buttonExQuestion12.Name = "buttonExQuestion12";
            this.buttonExQuestion12.Size = new System.Drawing.Size(122, 24);
            this.buttonExQuestion12.TabIndex = 11;
            this.buttonExQuestion12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonExQuestion12.Click += new System.EventHandler(this.buttonExQuestion1_Click);
            this.buttonExQuestion12.MouseLeave += new System.EventHandler(this.questionControlButton_MouseLeave);
            // 
            // buttonExQuestion4
            // 
            this.buttonExQuestion4.BackColor = System.Drawing.Color.Transparent;
            this.buttonExQuestion4.BaseColor = System.Drawing.Color.White;
            this.buttonExQuestion4.ButtonColor = System.Drawing.Color.LightGray;
            this.buttonExQuestion4.DisabledButtonColor = System.Drawing.Color.Gray;
            this.buttonExQuestion4.Enabled = false;
            this.buttonExQuestion4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExQuestion4.ForeColor = System.Drawing.Color.Black;
            this.buttonExQuestion4.Location = new System.Drawing.Point(380, 2);
            this.buttonExQuestion4.Name = "buttonExQuestion4";
            this.buttonExQuestion4.Size = new System.Drawing.Size(122, 24);
            this.buttonExQuestion4.TabIndex = 3;
            this.buttonExQuestion4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonExQuestion4.Click += new System.EventHandler(this.buttonExQuestion1_Click);
            this.buttonExQuestion4.MouseLeave += new System.EventHandler(this.questionControlButton_MouseLeave);
            // 
            // buttonExQuestion8
            // 
            this.buttonExQuestion8.BackColor = System.Drawing.Color.Transparent;
            this.buttonExQuestion8.BaseColor = System.Drawing.Color.White;
            this.buttonExQuestion8.ButtonColor = System.Drawing.Color.LightGray;
            this.buttonExQuestion8.DisabledButtonColor = System.Drawing.Color.Gray;
            this.buttonExQuestion8.Enabled = false;
            this.buttonExQuestion8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExQuestion8.ForeColor = System.Drawing.Color.Black;
            this.buttonExQuestion8.Location = new System.Drawing.Point(254, 30);
            this.buttonExQuestion8.Name = "buttonExQuestion8";
            this.buttonExQuestion8.Size = new System.Drawing.Size(122, 24);
            this.buttonExQuestion8.TabIndex = 7;
            this.buttonExQuestion8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonExQuestion8.Click += new System.EventHandler(this.buttonExQuestion1_Click);
            this.buttonExQuestion8.MouseLeave += new System.EventHandler(this.questionControlButton_MouseLeave);
            // 
            // buttonExQuestion1
            // 
            this.buttonExQuestion1.BackColor = System.Drawing.Color.Transparent;
            this.buttonExQuestion1.BaseColor = System.Drawing.Color.White;
            this.buttonExQuestion1.ButtonColor = System.Drawing.Color.LightGray;
            this.buttonExQuestion1.DisabledButtonColor = System.Drawing.Color.Gray;
            this.buttonExQuestion1.Enabled = false;
            this.buttonExQuestion1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExQuestion1.ForeColor = System.Drawing.Color.Black;
            this.buttonExQuestion1.Location = new System.Drawing.Point(2, 2);
            this.buttonExQuestion1.Name = "buttonExQuestion1";
            this.buttonExQuestion1.Size = new System.Drawing.Size(122, 24);
            this.buttonExQuestion1.TabIndex = 0;
            this.buttonExQuestion1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonExQuestion1.Click += new System.EventHandler(this.buttonExQuestion1_Click);
            this.buttonExQuestion1.MouseLeave += new System.EventHandler(this.questionControlButton_MouseLeave);
            // 
            // buttonExQuestion5
            // 
            this.buttonExQuestion5.BackColor = System.Drawing.Color.Transparent;
            this.buttonExQuestion5.BaseColor = System.Drawing.Color.White;
            this.buttonExQuestion5.ButtonColor = System.Drawing.Color.LightGray;
            this.buttonExQuestion5.DisabledButtonColor = System.Drawing.Color.Gray;
            this.buttonExQuestion5.Enabled = false;
            this.buttonExQuestion5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExQuestion5.ForeColor = System.Drawing.Color.Black;
            this.buttonExQuestion5.Location = new System.Drawing.Point(506, 2);
            this.buttonExQuestion5.Name = "buttonExQuestion5";
            this.buttonExQuestion5.Size = new System.Drawing.Size(126, 24);
            this.buttonExQuestion5.TabIndex = 4;
            this.buttonExQuestion5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonExQuestion5.Click += new System.EventHandler(this.buttonExQuestion1_Click);
            this.buttonExQuestion5.MouseLeave += new System.EventHandler(this.questionControlButton_MouseLeave);
            // 
            // buttonExQuestion9
            // 
            this.buttonExQuestion9.BackColor = System.Drawing.Color.Transparent;
            this.buttonExQuestion9.BaseColor = System.Drawing.Color.White;
            this.buttonExQuestion9.ButtonColor = System.Drawing.Color.LightGray;
            this.buttonExQuestion9.DisabledButtonColor = System.Drawing.Color.Gray;
            this.buttonExQuestion9.Enabled = false;
            this.buttonExQuestion9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExQuestion9.ForeColor = System.Drawing.Color.Black;
            this.buttonExQuestion9.Location = new System.Drawing.Point(380, 30);
            this.buttonExQuestion9.Name = "buttonExQuestion9";
            this.buttonExQuestion9.Size = new System.Drawing.Size(122, 24);
            this.buttonExQuestion9.TabIndex = 8;
            this.buttonExQuestion9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonExQuestion9.Click += new System.EventHandler(this.buttonExQuestion1_Click);
            this.buttonExQuestion9.MouseLeave += new System.EventHandler(this.questionControlButton_MouseLeave);
            // 
            // buttonExQuestion7
            // 
            this.buttonExQuestion7.BackColor = System.Drawing.Color.Transparent;
            this.buttonExQuestion7.BaseColor = System.Drawing.Color.White;
            this.buttonExQuestion7.ButtonColor = System.Drawing.Color.LightGray;
            this.buttonExQuestion7.DisabledButtonColor = System.Drawing.Color.Gray;
            this.buttonExQuestion7.Enabled = false;
            this.buttonExQuestion7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExQuestion7.ForeColor = System.Drawing.Color.Black;
            this.buttonExQuestion7.Location = new System.Drawing.Point(128, 30);
            this.buttonExQuestion7.Name = "buttonExQuestion7";
            this.buttonExQuestion7.Size = new System.Drawing.Size(122, 24);
            this.buttonExQuestion7.TabIndex = 6;
            this.buttonExQuestion7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonExQuestion7.Click += new System.EventHandler(this.buttonExQuestion1_Click);
            this.buttonExQuestion7.MouseLeave += new System.EventHandler(this.questionControlButton_MouseLeave);
            // 
            // buttonExQuestion2
            // 
            this.buttonExQuestion2.BackColor = System.Drawing.Color.Transparent;
            this.buttonExQuestion2.BaseColor = System.Drawing.Color.White;
            this.buttonExQuestion2.ButtonColor = System.Drawing.Color.LightGray;
            this.buttonExQuestion2.DisabledButtonColor = System.Drawing.Color.Gray;
            this.buttonExQuestion2.Enabled = false;
            this.buttonExQuestion2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExQuestion2.ForeColor = System.Drawing.Color.Black;
            this.buttonExQuestion2.Location = new System.Drawing.Point(128, 2);
            this.buttonExQuestion2.Name = "buttonExQuestion2";
            this.buttonExQuestion2.Size = new System.Drawing.Size(122, 24);
            this.buttonExQuestion2.TabIndex = 1;
            this.buttonExQuestion2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonExQuestion2.Click += new System.EventHandler(this.buttonExQuestion1_Click);
            this.buttonExQuestion2.MouseLeave += new System.EventHandler(this.questionControlButton_MouseLeave);
            // 
            // buttonExQuestion3
            // 
            this.buttonExQuestion3.BackColor = System.Drawing.Color.Transparent;
            this.buttonExQuestion3.BaseColor = System.Drawing.Color.White;
            this.buttonExQuestion3.ButtonColor = System.Drawing.Color.LightGray;
            this.buttonExQuestion3.DisabledButtonColor = System.Drawing.Color.Gray;
            this.buttonExQuestion3.Enabled = false;
            this.buttonExQuestion3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExQuestion3.ForeColor = System.Drawing.Color.Black;
            this.buttonExQuestion3.Location = new System.Drawing.Point(254, 2);
            this.buttonExQuestion3.Name = "buttonExQuestion3";
            this.buttonExQuestion3.Size = new System.Drawing.Size(122, 24);
            this.buttonExQuestion3.TabIndex = 2;
            this.buttonExQuestion3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonExQuestion3.Click += new System.EventHandler(this.buttonExQuestion1_Click);
            this.buttonExQuestion3.MouseLeave += new System.EventHandler(this.questionControlButton_MouseLeave);
            // 
            // buttonExQuestion10
            // 
            this.buttonExQuestion10.BackColor = System.Drawing.Color.Transparent;
            this.buttonExQuestion10.BaseColor = System.Drawing.Color.White;
            this.buttonExQuestion10.ButtonColor = System.Drawing.Color.LightGray;
            this.buttonExQuestion10.DisabledButtonColor = System.Drawing.Color.Gray;
            this.buttonExQuestion10.Enabled = false;
            this.buttonExQuestion10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExQuestion10.ForeColor = System.Drawing.Color.Black;
            this.buttonExQuestion10.Location = new System.Drawing.Point(506, 30);
            this.buttonExQuestion10.Name = "buttonExQuestion10";
            this.buttonExQuestion10.Size = new System.Drawing.Size(126, 24);
            this.buttonExQuestion10.TabIndex = 9;
            this.buttonExQuestion10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonExQuestion10.Click += new System.EventHandler(this.buttonExQuestion1_Click);
            this.buttonExQuestion10.MouseLeave += new System.EventHandler(this.questionControlButton_MouseLeave);
            // 
            // labelExLongQuestion
            // 
            this.labelExLongQuestion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExLongQuestion.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExLongQuestion.Location = new System.Drawing.Point(5, 89);
            this.labelExLongQuestion.Name = "labelExLongQuestion";
            this.labelExLongQuestion.Size = new System.Drawing.Size(464, 15);
            this.labelExLongQuestion.TabIndex = 15;
            this.labelExLongQuestion.Text = "Texto completo de la pregunta que se esta haciendo";
            this.labelExLongQuestion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panelIncidentTypes
            // 
            this.panelIncidentTypes.BorderColor = System.Drawing.Color.Empty;
            this.panelIncidentTypes.BorderWidth = 0;
            this.panelIncidentTypes.Controls.Add(this.layoutControl1);
            this.panelIncidentTypes.ForeColor = System.Drawing.Color.Black;
            this.panelIncidentTypes.LoadFromResources = true;
            this.panelIncidentTypes.Location = new System.Drawing.Point(2, 47);
            this.panelIncidentTypes.Name = "panelIncidentTypes";
            this.panelIncidentTypes.PanelColor = System.Drawing.Color.Empty;
            this.panelIncidentTypes.Size = new System.Drawing.Size(634, 178);
            this.panelIncidentTypes.TabIndex = 2;
            this.panelIncidentTypes.Visible = false;
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowCustomizationMenu = false;
            this.layoutControl1.Controls.Add(this.listBoxIncidentTypes);
            this.layoutControl1.Controls.Add(this.buttonOK);
            this.layoutControl1.Controls.Add(this.buttonAddIncidentType);
            this.layoutControl1.Controls.Add(this.buttonRemoveAllIncidentType);
            this.layoutControl1.Controls.Add(this.listBoxSelectedIncidentTypes);
            this.layoutControl1.Controls.Add(this.buttonAddAllIncidentType);
            this.layoutControl1.Controls.Add(this.buttonRemoveIncidentType);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.LookAndFeel.SkinName = "Money Twins";
            this.layoutControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.layoutControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsFocus.AllowFocusControlOnActivatedTabPage = true;
            this.layoutControl1.OptionsFocus.EnableAutoTabOrder = false;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(634, 178);
            this.layoutControl1.TabIndex = 16;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // listBoxIncidentTypes
            // 
            this.listBoxIncidentTypes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.listBoxIncidentTypes.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.listBoxIncidentTypes.FormattingEnabled = true;
            this.listBoxIncidentTypes.HorizontalScrollbar = true;
            this.listBoxIncidentTypes.IntegralHeight = false;
            this.listBoxIncidentTypes.Location = new System.Drawing.Point(5, 18);
            this.listBoxIncidentTypes.Name = "listBoxIncidentTypes";
            this.listBoxIncidentTypes.Size = new System.Drawing.Size(284, 127);
            this.listBoxIncidentTypes.TabIndex = 0;
            this.listBoxIncidentTypes.DoubleClick += new System.EventHandler(this.listBoxIncidentTypes_DoubleClick);
            this.listBoxIncidentTypes.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.listBoxIncidentTypes_KeyPress);
            // 
            // buttonOK
            // 
            this.buttonOK.BackColor = System.Drawing.Color.Transparent;
            this.buttonOK.ButtonColor = System.Drawing.Color.Transparent;
            this.buttonOK.DisabledButtonColor = System.Drawing.Color.Gray;
            this.buttonOK.Enabled = false;
            this.buttonOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOK.Location = new System.Drawing.Point(542, 152);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(90, 24);
            this.buttonOK.TabIndex = 6;
            this.buttonOK.Text = "Accept";
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            this.buttonOK.MouseLeave += new System.EventHandler(this.questionControlButton_MouseLeave);
            // 
            // buttonAddIncidentType
            // 
            this.buttonAddIncidentType.BackColor = System.Drawing.Color.Transparent;
            this.buttonAddIncidentType.ButtonColor = System.Drawing.Color.Transparent;
            this.buttonAddIncidentType.DisabledButtonColor = System.Drawing.Color.Gray;
            this.buttonAddIncidentType.Location = new System.Drawing.Point(297, 29);
            this.buttonAddIncidentType.Name = "buttonAddIncidentType";
            this.buttonAddIncidentType.Size = new System.Drawing.Size(39, 22);
            this.buttonAddIncidentType.TabIndex = 1;
            this.buttonAddIncidentType.Text = ">";
            this.toolTipMain.SetToolTip(this.buttonAddIncidentType, "Agregar tipo de incidente");
            this.buttonAddIncidentType.Click += new System.EventHandler(this.buttonAddIncidentType_Click);
            this.buttonAddIncidentType.MouseLeave += new System.EventHandler(this.questionControlButton_MouseLeave);
            // 
            // buttonRemoveAllIncidentType
            // 
            this.buttonRemoveAllIncidentType.BackColor = System.Drawing.Color.Transparent;
            this.buttonRemoveAllIncidentType.ButtonColor = System.Drawing.Color.Transparent;
            this.buttonRemoveAllIncidentType.DisabledButtonColor = System.Drawing.Color.Gray;
            this.buttonRemoveAllIncidentType.Location = new System.Drawing.Point(297, 112);
            this.buttonRemoveAllIncidentType.Name = "buttonRemoveAllIncidentType";
            this.buttonRemoveAllIncidentType.Size = new System.Drawing.Size(39, 21);
            this.buttonRemoveAllIncidentType.TabIndex = 4;
            this.buttonRemoveAllIncidentType.Text = "<<";
            this.toolTipMain.SetToolTip(this.buttonRemoveAllIncidentType, "Eliminar todos los tipos de incidentes");
            this.buttonRemoveAllIncidentType.Click += new System.EventHandler(this.buttonRemoveAllIncidentType_Click);
            this.buttonRemoveAllIncidentType.MouseLeave += new System.EventHandler(this.questionControlButton_MouseLeave);
            // 
            // listBoxSelectedIncidentTypes
            // 
            this.listBoxSelectedIncidentTypes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.listBoxSelectedIncidentTypes.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.listBoxSelectedIncidentTypes.FormattingEnabled = true;
            this.listBoxSelectedIncidentTypes.HorizontalScrollbar = true;
            this.listBoxSelectedIncidentTypes.IntegralHeight = false;
            this.listBoxSelectedIncidentTypes.Location = new System.Drawing.Point(344, 18);
            this.listBoxSelectedIncidentTypes.Name = "listBoxSelectedIncidentTypes";
            this.listBoxSelectedIncidentTypes.Size = new System.Drawing.Size(285, 127);
            this.listBoxSelectedIncidentTypes.TabIndex = 5;
            this.listBoxSelectedIncidentTypes.DoubleClick += new System.EventHandler(this.listBoxSelectedIncidentTypes_DoubleClick);
            // 
            // buttonAddAllIncidentType
            // 
            this.buttonAddAllIncidentType.BackColor = System.Drawing.Color.Transparent;
            this.buttonAddAllIncidentType.ButtonColor = System.Drawing.Color.Transparent;
            this.buttonAddAllIncidentType.DisabledButtonColor = System.Drawing.Color.Gray;
            this.buttonAddAllIncidentType.Location = new System.Drawing.Point(297, 57);
            this.buttonAddAllIncidentType.Name = "buttonAddAllIncidentType";
            this.buttonAddAllIncidentType.Size = new System.Drawing.Size(39, 21);
            this.buttonAddAllIncidentType.TabIndex = 2;
            this.buttonAddAllIncidentType.Text = ">>";
            this.toolTipMain.SetToolTip(this.buttonAddAllIncidentType, "Agregar todos los tipos de incidentes");
            this.buttonAddAllIncidentType.Click += new System.EventHandler(this.buttonAddAllIncidentType_Click);
            this.buttonAddAllIncidentType.MouseLeave += new System.EventHandler(this.questionControlButton_MouseLeave);
            // 
            // buttonRemoveIncidentType
            // 
            this.buttonRemoveIncidentType.BackColor = System.Drawing.Color.Transparent;
            this.buttonRemoveIncidentType.ButtonColor = System.Drawing.Color.Transparent;
            this.buttonRemoveIncidentType.DisabledButtonColor = System.Drawing.Color.Gray;
            this.buttonRemoveIncidentType.Location = new System.Drawing.Point(297, 84);
            this.buttonRemoveIncidentType.Name = "buttonRemoveIncidentType";
            this.buttonRemoveIncidentType.Size = new System.Drawing.Size(39, 22);
            this.buttonRemoveIncidentType.TabIndex = 3;
            this.buttonRemoveIncidentType.Text = "<";
            this.toolTipMain.SetToolTip(this.buttonRemoveIncidentType, "Eliminar tipo de incidente");
            this.buttonRemoveIncidentType.Click += new System.EventHandler(this.buttonRemoveIncidentType_Click);
            this.buttonRemoveIncidentType.MouseLeave += new System.EventHandler(this.questionControlButton_MouseLeave);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(252)))), ((int)(((byte)(226)))));
            this.layoutControlGroup1.AppearanceGroup.Options.UseBackColor = true;
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemIncidentTypes,
            this.layoutControlItemAddIncidentType,
            this.layoutControlItemAddAllIncidentType,
            this.layoutControlItemRemoveIncidentType,
            this.layoutControlItemRemoveAllIncidentType,
            this.layoutControlItemSelectedIncidentTypes,
            this.layoutControlItemButtonOk,
            this.emptySpaceItem2,
            this.emptySpaceItem1,
            this.emptySpaceItem8});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup1.Size = new System.Drawing.Size(634, 178);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItemIncidentTypes
            // 
            this.layoutControlItemIncidentTypes.Control = this.listBoxIncidentTypes;
            this.layoutControlItemIncidentTypes.CustomizationFormText = "layoutControlItemIncidentTypes";
            this.layoutControlItemIncidentTypes.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemIncidentTypes.MinSize = new System.Drawing.Size(165, 50);
            this.layoutControlItemIncidentTypes.Name = "layoutControlItemIncidentTypes";
            this.layoutControlItemIncidentTypes.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 2, 5);
            this.layoutControlItemIncidentTypes.Size = new System.Drawing.Size(294, 150);
            this.layoutControlItemIncidentTypes.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemIncidentTypes.Text = "layoutControlItemIncidentTypes";
            this.layoutControlItemIncidentTypes.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItemIncidentTypes.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItemIncidentTypes.TextSize = new System.Drawing.Size(155, 13);
            this.layoutControlItemIncidentTypes.TextToControlDistance = 3;
            // 
            // layoutControlItemAddIncidentType
            // 
            this.layoutControlItemAddIncidentType.Control = this.buttonAddIncidentType;
            this.layoutControlItemAddIncidentType.CustomizationFormText = "layoutControlItemAddIncidentType";
            this.layoutControlItemAddIncidentType.Location = new System.Drawing.Point(294, 26);
            this.layoutControlItemAddIncidentType.MaxSize = new System.Drawing.Size(45, 28);
            this.layoutControlItemAddIncidentType.MinSize = new System.Drawing.Size(45, 28);
            this.layoutControlItemAddIncidentType.Name = "layoutControlItemAddIncidentType";
            this.layoutControlItemAddIncidentType.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItemAddIncidentType.Size = new System.Drawing.Size(45, 28);
            this.layoutControlItemAddIncidentType.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemAddIncidentType.Text = "layoutControlItemAddIncidentType";
            this.layoutControlItemAddIncidentType.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemAddIncidentType.TextToControlDistance = 0;
            this.layoutControlItemAddIncidentType.TextVisible = false;
            // 
            // layoutControlItemAddAllIncidentType
            // 
            this.layoutControlItemAddAllIncidentType.Control = this.buttonAddAllIncidentType;
            this.layoutControlItemAddAllIncidentType.CustomizationFormText = "layoutControlItemAddAllIncidentType";
            this.layoutControlItemAddAllIncidentType.Location = new System.Drawing.Point(294, 54);
            this.layoutControlItemAddAllIncidentType.MaxSize = new System.Drawing.Size(45, 27);
            this.layoutControlItemAddAllIncidentType.MinSize = new System.Drawing.Size(45, 27);
            this.layoutControlItemAddAllIncidentType.Name = "layoutControlItemAddAllIncidentType";
            this.layoutControlItemAddAllIncidentType.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItemAddAllIncidentType.Size = new System.Drawing.Size(45, 27);
            this.layoutControlItemAddAllIncidentType.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemAddAllIncidentType.Text = "layoutControlItemAddAllIncidentType";
            this.layoutControlItemAddAllIncidentType.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemAddAllIncidentType.TextToControlDistance = 0;
            this.layoutControlItemAddAllIncidentType.TextVisible = false;
            // 
            // layoutControlItemRemoveIncidentType
            // 
            this.layoutControlItemRemoveIncidentType.Control = this.buttonRemoveIncidentType;
            this.layoutControlItemRemoveIncidentType.CustomizationFormText = "layoutControlItemRemoveIncidentType";
            this.layoutControlItemRemoveIncidentType.Location = new System.Drawing.Point(294, 81);
            this.layoutControlItemRemoveIncidentType.MaxSize = new System.Drawing.Size(45, 28);
            this.layoutControlItemRemoveIncidentType.MinSize = new System.Drawing.Size(45, 28);
            this.layoutControlItemRemoveIncidentType.Name = "layoutControlItemRemoveIncidentType";
            this.layoutControlItemRemoveIncidentType.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItemRemoveIncidentType.Size = new System.Drawing.Size(45, 28);
            this.layoutControlItemRemoveIncidentType.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemRemoveIncidentType.Text = "layoutControlItemRemoveIncidentType";
            this.layoutControlItemRemoveIncidentType.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemRemoveIncidentType.TextToControlDistance = 0;
            this.layoutControlItemRemoveIncidentType.TextVisible = false;
            // 
            // layoutControlItemRemoveAllIncidentType
            // 
            this.layoutControlItemRemoveAllIncidentType.Control = this.buttonRemoveAllIncidentType;
            this.layoutControlItemRemoveAllIncidentType.CustomizationFormText = "layoutControlItemRemoveAllIncidentType";
            this.layoutControlItemRemoveAllIncidentType.Location = new System.Drawing.Point(294, 109);
            this.layoutControlItemRemoveAllIncidentType.MaxSize = new System.Drawing.Size(45, 27);
            this.layoutControlItemRemoveAllIncidentType.MinSize = new System.Drawing.Size(45, 27);
            this.layoutControlItemRemoveAllIncidentType.Name = "layoutControlItemRemoveAllIncidentType";
            this.layoutControlItemRemoveAllIncidentType.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItemRemoveAllIncidentType.Size = new System.Drawing.Size(45, 27);
            this.layoutControlItemRemoveAllIncidentType.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemRemoveAllIncidentType.Text = "layoutControlItemRemoveAllIncidentType";
            this.layoutControlItemRemoveAllIncidentType.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemRemoveAllIncidentType.TextToControlDistance = 0;
            this.layoutControlItemRemoveAllIncidentType.TextVisible = false;
            // 
            // layoutControlItemSelectedIncidentTypes
            // 
            this.layoutControlItemSelectedIncidentTypes.Control = this.listBoxSelectedIncidentTypes;
            this.layoutControlItemSelectedIncidentTypes.CustomizationFormText = "layoutControlItemSelectedIncidentTypes";
            this.layoutControlItemSelectedIncidentTypes.Location = new System.Drawing.Point(339, 0);
            this.layoutControlItemSelectedIncidentTypes.MinSize = new System.Drawing.Size(206, 50);
            this.layoutControlItemSelectedIncidentTypes.Name = "layoutControlItemSelectedIncidentTypes";
            this.layoutControlItemSelectedIncidentTypes.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 2, 5);
            this.layoutControlItemSelectedIncidentTypes.Size = new System.Drawing.Size(295, 150);
            this.layoutControlItemSelectedIncidentTypes.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemSelectedIncidentTypes.Text = "layoutControlItemSelectedIncidentTypes";
            this.layoutControlItemSelectedIncidentTypes.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItemSelectedIncidentTypes.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItemSelectedIncidentTypes.TextSize = new System.Drawing.Size(196, 13);
            this.layoutControlItemSelectedIncidentTypes.TextToControlDistance = 3;
            // 
            // layoutControlItemButtonOk
            // 
            this.layoutControlItemButtonOk.Control = this.buttonOK;
            this.layoutControlItemButtonOk.CustomizationFormText = "layoutControlItemButtonOk";
            this.layoutControlItemButtonOk.Location = new System.Drawing.Point(540, 150);
            this.layoutControlItemButtonOk.MaxSize = new System.Drawing.Size(94, 28);
            this.layoutControlItemButtonOk.MinSize = new System.Drawing.Size(94, 28);
            this.layoutControlItemButtonOk.Name = "layoutControlItemButtonOk";
            this.layoutControlItemButtonOk.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItemButtonOk.Size = new System.Drawing.Size(94, 28);
            this.layoutControlItemButtonOk.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemButtonOk.Text = "layoutControlItemButtonOk";
            this.layoutControlItemButtonOk.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemButtonOk.TextToControlDistance = 0;
            this.layoutControlItemButtonOk.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(294, 0);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(45, 26);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(45, 26);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.emptySpaceItem2.Size = new System.Drawing.Size(45, 26);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(294, 136);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(30, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(30, 1);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.emptySpaceItem1.Size = new System.Drawing.Size(45, 14);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
            this.emptySpaceItem8.Location = new System.Drawing.Point(0, 150);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(540, 28);
            this.emptySpaceItem8.Text = "emptySpaceItem8";
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // buttonIncidentTypes
            // 
            this.buttonIncidentTypes.BackColor = System.Drawing.Color.Transparent;
            this.buttonIncidentTypes.BaseColor = System.Drawing.Color.White;
            this.buttonIncidentTypes.ButtonColor = System.Drawing.Color.LightGray;
            this.buttonIncidentTypes.DisabledButtonColor = System.Drawing.Color.Gray;
            this.buttonIncidentTypes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonIncidentTypes.ForeColor = System.Drawing.Color.Black;
            this.buttonIncidentTypes.Location = new System.Drawing.Point(47, 4);
            this.buttonIncidentTypes.Name = "buttonIncidentTypes";
            this.buttonIncidentTypes.Size = new System.Drawing.Size(112, 24);
            this.buttonIncidentTypes.TabIndex = 0;
            this.buttonIncidentTypes.Click += new System.EventHandler(this.buttonIncidentTypes_Click);
            this.buttonIncidentTypes.MouseLeave += new System.EventHandler(this.questionControlButton_MouseLeave);
            // 
            // groupControlBody
            // 
            this.groupControlBody.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(252)))), ((int)(((byte)(226)))));
            this.groupControlBody.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(252)))), ((int)(((byte)(226)))));
            this.groupControlBody.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(94)))), ((int)(((byte)(50)))));
            this.groupControlBody.Appearance.ForeColor = System.Drawing.Color.Black;
            this.groupControlBody.Appearance.Options.UseBackColor = true;
            this.groupControlBody.Appearance.Options.UseBorderColor = true;
            this.groupControlBody.Appearance.Options.UseForeColor = true;
            this.groupControlBody.AppearanceCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(172)))), ((int)(((byte)(68)))));
            this.groupControlBody.AppearanceCaption.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(94)))), ((int)(((byte)(50)))));
            this.groupControlBody.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControlBody.AppearanceCaption.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.groupControlBody.AppearanceCaption.Options.UseBackColor = true;
            this.groupControlBody.AppearanceCaption.Options.UseFont = true;
            this.groupControlBody.Controls.Add(this.layoutControl2);
            this.groupControlBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControlBody.Location = new System.Drawing.Point(0, 0);
            this.groupControlBody.LookAndFeel.SkinName = "Blue";
            this.groupControlBody.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.groupControlBody.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControlBody.Name = "groupControlBody";
            this.groupControlBody.Size = new System.Drawing.Size(642, 476);
            this.groupControlBody.TabIndex = 0;
            // 
            // layoutControl2
            // 
            this.layoutControl2.AllowCustomizationMenu = false;
            this.layoutControl2.Controls.Add(this.textBoxSelectedIncidentTypes);
            this.layoutControl2.Controls.Add(this.buttonIncidentTypes);
            this.layoutControl2.Controls.Add(this.panelIncidentTypes);
            this.layoutControl2.Controls.Add(this.pictureBoxNumber);
            this.layoutControl2.Controls.Add(this.layoutControl3);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(2, 19);
            this.layoutControl2.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.layoutControl2.LookAndFeel.UseDefaultLookAndFeel = false;
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsFocus.EnableAutoTabOrder = false;
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(638, 455);
            this.layoutControl2.TabIndex = 42;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // textBoxSelectedIncidentTypes
            // 
            this.textBoxSelectedIncidentTypes.Location = new System.Drawing.Point(166, 5);
            this.textBoxSelectedIncidentTypes.Name = "textBoxSelectedIncidentTypes";
            this.textBoxSelectedIncidentTypes.Properties.Appearance.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxSelectedIncidentTypes.Properties.Appearance.Options.UseBackColor = true;
            this.textBoxSelectedIncidentTypes.Properties.ReadOnly = true;
            this.textBoxSelectedIncidentTypes.Size = new System.Drawing.Size(467, 20);
            this.textBoxSelectedIncidentTypes.StyleController = this.layoutControl2;
            this.textBoxSelectedIncidentTypes.TabIndex = 44;
            // 
            // pictureBoxNumber
            // 
            this.pictureBoxNumber.Location = new System.Drawing.Point(2, 2);
            this.pictureBoxNumber.Name = "pictureBoxNumber";
            this.pictureBoxNumber.Properties.ShowMenu = false;
            this.pictureBoxNumber.Size = new System.Drawing.Size(41, 41);
            this.pictureBoxNumber.StyleController = this.layoutControl2;
            this.pictureBoxNumber.TabIndex = 0;
            // 
            // layoutControl3
            // 
            this.layoutControl3.AllowCustomizationMenu = false;
            this.layoutControl3.Controls.Add(this.dragDropLayoutControl1);
            this.layoutControl3.Controls.Add(this.buttonExForward);
            this.layoutControl3.Controls.Add(this.buttonExBack);
            this.layoutControl3.Controls.Add(this.buttonExQuestion1);
            this.layoutControl3.Controls.Add(this.buttonExQuestion15);
            this.layoutControl3.Controls.Add(this.buttonExQuestion3);
            this.layoutControl3.Controls.Add(this.labelExLongQuestion);
            this.layoutControl3.Controls.Add(this.buttonExQuestion14);
            this.layoutControl3.Controls.Add(this.buttonExQuestion2);
            this.layoutControl3.Controls.Add(this.buttonExQuestion4);
            this.layoutControl3.Controls.Add(this.buttonExQuestion5);
            this.layoutControl3.Controls.Add(this.buttonExQuestion13);
            this.layoutControl3.Controls.Add(this.buttonExQuestion11);
            this.layoutControl3.Controls.Add(this.buttonExQuestion6);
            this.layoutControl3.Controls.Add(this.buttonExQuestion7);
            this.layoutControl3.Controls.Add(this.buttonExQuestion10);
            this.layoutControl3.Controls.Add(this.buttonExQuestion12);
            this.layoutControl3.Controls.Add(this.buttonExQuestion8);
            this.layoutControl3.Controls.Add(this.buttonExQuestion9);
            this.layoutControl3.Location = new System.Drawing.Point(2, 229);
            this.layoutControl3.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.layoutControl3.LookAndFeel.UseDefaultLookAndFeel = false;
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.OptionsFocus.AllowFocusControlOnActivatedTabPage = true;
            this.layoutControl3.OptionsFocus.EnableAutoTabOrder = false;
            this.layoutControl3.Root = this.layoutControlGroup3;
            this.layoutControl3.Size = new System.Drawing.Size(634, 224);
            this.layoutControl3.TabIndex = 43;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // dragDropLayoutControl1
            // 
            this.dragDropLayoutControl1.Location = new System.Drawing.Point(4, 113);
            this.dragDropLayoutControl1.Name = "dragDropLayoutControl1";
            this.dragDropLayoutControl1.Size = new System.Drawing.Size(626, 107);
            this.dragDropLayoutControl1.TabIndex = 0;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AppearanceGroup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(252)))), ((int)(((byte)(226)))));
            this.layoutControlGroup3.AppearanceGroup.Options.UseBackColor = true;
            this.layoutControlGroup3.CustomizationFormText = "Root";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.layoutControlItem20,
            this.layoutControlItem22,
            this.layoutControlItem23,
            this.layoutControlItem24,
            this.layoutControlItem19,
            this.layoutControlItem18,
            this.layoutControlItem21,
            this.layoutControlItem17,
            this.layoutControlItem1,
            this.layoutControlItemLabelExLongQuestion,
            this.layoutControlItemNext,
            this.layoutControlItemBack,
            this.emptySpaceItem3});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "Root";
            this.layoutControlGroup3.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup3.Size = new System.Drawing.Size(634, 224);
            this.layoutControlGroup3.Text = "Root";
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.buttonExQuestion1;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem10.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(118, 28);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem10.Size = new System.Drawing.Size(126, 28);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Text = "layoutControlItem10";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextToControlDistance = 0;
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.buttonExQuestion2;
            this.layoutControlItem11.CustomizationFormText = "layoutControlItem11";
            this.layoutControlItem11.Location = new System.Drawing.Point(126, 0);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(118, 28);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem11.Size = new System.Drawing.Size(126, 28);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "layoutControlItem11";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextToControlDistance = 0;
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.buttonExQuestion3;
            this.layoutControlItem12.CustomizationFormText = "layoutControlItem12";
            this.layoutControlItem12.Location = new System.Drawing.Point(252, 0);
            this.layoutControlItem12.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(118, 28);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem12.Size = new System.Drawing.Size(126, 28);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Text = "layoutControlItem12";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextToControlDistance = 0;
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.buttonExQuestion4;
            this.layoutControlItem13.CustomizationFormText = "layoutControlItem13";
            this.layoutControlItem13.Location = new System.Drawing.Point(378, 0);
            this.layoutControlItem13.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(118, 28);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem13.Size = new System.Drawing.Size(126, 28);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Text = "layoutControlItem13";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextToControlDistance = 0;
            this.layoutControlItem13.TextVisible = false;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.buttonExQuestion5;
            this.layoutControlItem14.CustomizationFormText = "layoutControlItem14";
            this.layoutControlItem14.Location = new System.Drawing.Point(504, 0);
            this.layoutControlItem14.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem14.MinSize = new System.Drawing.Size(118, 28);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem14.Size = new System.Drawing.Size(130, 28);
            this.layoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem14.Text = "layoutControlItem14";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem14.TextToControlDistance = 0;
            this.layoutControlItem14.TextVisible = false;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.buttonExQuestion6;
            this.layoutControlItem15.CustomizationFormText = "layoutControlItem15";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItem15.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem15.MinSize = new System.Drawing.Size(118, 28);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem15.Size = new System.Drawing.Size(126, 28);
            this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem15.Text = "layoutControlItem15";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextToControlDistance = 0;
            this.layoutControlItem15.TextVisible = false;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.buttonExQuestion7;
            this.layoutControlItem16.CustomizationFormText = "layoutControlItem16";
            this.layoutControlItem16.Location = new System.Drawing.Point(126, 28);
            this.layoutControlItem16.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem16.MinSize = new System.Drawing.Size(118, 28);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem16.Size = new System.Drawing.Size(126, 28);
            this.layoutControlItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem16.Text = "layoutControlItem16";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem16.TextToControlDistance = 0;
            this.layoutControlItem16.TextVisible = false;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.buttonExQuestion11;
            this.layoutControlItem20.CustomizationFormText = "layoutControlItem20";
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 56);
            this.layoutControlItem20.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem20.MinSize = new System.Drawing.Size(118, 28);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem20.Size = new System.Drawing.Size(126, 28);
            this.layoutControlItem20.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem20.Text = "layoutControlItem20";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem20.TextToControlDistance = 0;
            this.layoutControlItem20.TextVisible = false;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.buttonExQuestion13;
            this.layoutControlItem22.CustomizationFormText = "layoutControlItem22";
            this.layoutControlItem22.Location = new System.Drawing.Point(252, 56);
            this.layoutControlItem22.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem22.MinSize = new System.Drawing.Size(118, 28);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem22.Size = new System.Drawing.Size(126, 28);
            this.layoutControlItem22.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem22.Text = "layoutControlItem22";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem22.TextToControlDistance = 0;
            this.layoutControlItem22.TextVisible = false;
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.buttonExQuestion14;
            this.layoutControlItem23.CustomizationFormText = "layoutControlItem23";
            this.layoutControlItem23.Location = new System.Drawing.Point(378, 56);
            this.layoutControlItem23.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem23.MinSize = new System.Drawing.Size(118, 28);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem23.Size = new System.Drawing.Size(126, 28);
            this.layoutControlItem23.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem23.Text = "layoutControlItem23";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem23.TextToControlDistance = 0;
            this.layoutControlItem23.TextVisible = false;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.buttonExQuestion15;
            this.layoutControlItem24.CustomizationFormText = "layoutControlItem24";
            this.layoutControlItem24.Location = new System.Drawing.Point(504, 56);
            this.layoutControlItem24.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem24.MinSize = new System.Drawing.Size(118, 28);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem24.Size = new System.Drawing.Size(130, 28);
            this.layoutControlItem24.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem24.Text = "layoutControlItem24";
            this.layoutControlItem24.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem24.TextToControlDistance = 0;
            this.layoutControlItem24.TextVisible = false;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.buttonExQuestion10;
            this.layoutControlItem19.CustomizationFormText = "layoutControlItem19";
            this.layoutControlItem19.Location = new System.Drawing.Point(504, 28);
            this.layoutControlItem19.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem19.MinSize = new System.Drawing.Size(118, 28);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem19.Size = new System.Drawing.Size(130, 28);
            this.layoutControlItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem19.Text = "layoutControlItem19";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextToControlDistance = 0;
            this.layoutControlItem19.TextVisible = false;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.buttonExQuestion9;
            this.layoutControlItem18.CustomizationFormText = "layoutControlItem18";
            this.layoutControlItem18.Location = new System.Drawing.Point(378, 28);
            this.layoutControlItem18.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem18.MinSize = new System.Drawing.Size(118, 28);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem18.Size = new System.Drawing.Size(126, 28);
            this.layoutControlItem18.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem18.Text = "layoutControlItem18";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextToControlDistance = 0;
            this.layoutControlItem18.TextVisible = false;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.buttonExQuestion12;
            this.layoutControlItem21.CustomizationFormText = "layoutControlItem21";
            this.layoutControlItem21.Location = new System.Drawing.Point(126, 56);
            this.layoutControlItem21.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem21.MinSize = new System.Drawing.Size(118, 28);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem21.Size = new System.Drawing.Size(126, 28);
            this.layoutControlItem21.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem21.Text = "layoutControlItem21";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem21.TextToControlDistance = 0;
            this.layoutControlItem21.TextVisible = false;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.buttonExQuestion8;
            this.layoutControlItem17.CustomizationFormText = "layoutControlItem17";
            this.layoutControlItem17.Location = new System.Drawing.Point(252, 28);
            this.layoutControlItem17.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem17.MinSize = new System.Drawing.Size(118, 28);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem17.Size = new System.Drawing.Size(126, 28);
            this.layoutControlItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem17.Text = "layoutControlItem17";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextToControlDistance = 0;
            this.layoutControlItem17.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dragDropLayoutControl1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 109);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4);
            this.layoutControlItem1.Size = new System.Drawing.Size(634, 115);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItemLabelExLongQuestion
            // 
            this.layoutControlItemLabelExLongQuestion.Control = this.labelExLongQuestion;
            this.layoutControlItemLabelExLongQuestion.CustomizationFormText = "layoutControlItemLabelExLongQuestion";
            this.layoutControlItemLabelExLongQuestion.Location = new System.Drawing.Point(0, 84);
            this.layoutControlItemLabelExLongQuestion.MaxSize = new System.Drawing.Size(0, 20);
            this.layoutControlItemLabelExLongQuestion.MinSize = new System.Drawing.Size(30, 20);
            this.layoutControlItemLabelExLongQuestion.Name = "layoutControlItemLabelExLongQuestion";
            this.layoutControlItemLabelExLongQuestion.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 0);
            this.layoutControlItemLabelExLongQuestion.Size = new System.Drawing.Size(474, 25);
            this.layoutControlItemLabelExLongQuestion.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemLabelExLongQuestion.Text = "layoutControlItemLabelExLongQuestion";
            this.layoutControlItemLabelExLongQuestion.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemLabelExLongQuestion.TextToControlDistance = 0;
            this.layoutControlItemLabelExLongQuestion.TextVisible = false;
            // 
            // layoutControlItemNext
            // 
            this.layoutControlItemNext.Control = this.buttonExForward;
            this.layoutControlItemNext.CustomizationFormText = "layoutControlItemNext";
            this.layoutControlItemNext.Location = new System.Drawing.Point(609, 84);
            this.layoutControlItemNext.MaxSize = new System.Drawing.Size(25, 25);
            this.layoutControlItemNext.MinSize = new System.Drawing.Size(25, 25);
            this.layoutControlItemNext.Name = "layoutControlItemNext";
            this.layoutControlItemNext.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItemNext.Size = new System.Drawing.Size(25, 25);
            this.layoutControlItemNext.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemNext.Text = "layoutControlItemNext";
            this.layoutControlItemNext.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemNext.TextToControlDistance = 0;
            this.layoutControlItemNext.TextVisible = false;
            // 
            // layoutControlItemBack
            // 
            this.layoutControlItemBack.Control = this.buttonExBack;
            this.layoutControlItemBack.CustomizationFormText = "layoutControlItemBack";
            this.layoutControlItemBack.Location = new System.Drawing.Point(584, 84);
            this.layoutControlItemBack.MaxSize = new System.Drawing.Size(25, 25);
            this.layoutControlItemBack.MinSize = new System.Drawing.Size(25, 25);
            this.layoutControlItemBack.Name = "layoutControlItemBack";
            this.layoutControlItemBack.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItemBack.Size = new System.Drawing.Size(25, 25);
            this.layoutControlItemBack.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemBack.Text = "layoutControlItemBack";
            this.layoutControlItemBack.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemBack.TextToControlDistance = 0;
            this.layoutControlItemBack.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(474, 84);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(1, 1);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 0);
            this.emptySpaceItem3.Size = new System.Drawing.Size(110, 25);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AppearanceGroup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(252)))), ((int)(((byte)(226)))));
            this.layoutControlGroup2.AppearanceGroup.Options.UseBackColor = true;
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemButtonIncidentTypes,
            this.layoutControlItem9,
            this.layoutControlItem29,
            this.layoutControlItemSelectedIncidents,
            this.layoutControlItemPictureBox,
            this.emptySpaceItem6,
            this.emptySpaceItem7});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup2.Size = new System.Drawing.Size(638, 455);
            this.layoutControlGroup2.Text = "Root";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItemButtonIncidentTypes
            // 
            this.layoutControlItemButtonIncidentTypes.Control = this.buttonIncidentTypes;
            this.layoutControlItemButtonIncidentTypes.CustomizationFormText = "layoutControlItemButtonIncidentTypes";
            this.layoutControlItemButtonIncidentTypes.Location = new System.Drawing.Point(45, 0);
            this.layoutControlItemButtonIncidentTypes.MaxSize = new System.Drawing.Size(116, 30);
            this.layoutControlItemButtonIncidentTypes.MinSize = new System.Drawing.Size(116, 30);
            this.layoutControlItemButtonIncidentTypes.Name = "layoutControlItemButtonIncidentTypes";
            this.layoutControlItemButtonIncidentTypes.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 4, 2);
            this.layoutControlItemButtonIncidentTypes.Size = new System.Drawing.Size(116, 30);
            this.layoutControlItemButtonIncidentTypes.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemButtonIncidentTypes.Text = "layoutControlItemButtonIncidentTypes";
            this.layoutControlItemButtonIncidentTypes.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemButtonIncidentTypes.TextToControlDistance = 0;
            this.layoutControlItemButtonIncidentTypes.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.panelIncidentTypes;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 45);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(1, 1);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem9.Size = new System.Drawing.Size(638, 182);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.Control = this.layoutControl3;
            this.layoutControlItem29.CustomizationFormText = "layoutControlItem29";
            this.layoutControlItem29.Location = new System.Drawing.Point(0, 227);
            this.layoutControlItem29.MinSize = new System.Drawing.Size(1, 1);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem29.Size = new System.Drawing.Size(638, 228);
            this.layoutControlItem29.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem29.Text = "layoutControlItem29";
            this.layoutControlItem29.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem29.TextToControlDistance = 0;
            this.layoutControlItem29.TextVisible = false;
            // 
            // layoutControlItemSelectedIncidents
            // 
            this.layoutControlItemSelectedIncidents.Control = this.textBoxSelectedIncidentTypes;
            this.layoutControlItemSelectedIncidents.CustomizationFormText = "layoutControlItemSelectedIncidents";
            this.layoutControlItemSelectedIncidents.Location = new System.Drawing.Point(161, 0);
            this.layoutControlItemSelectedIncidents.Name = "layoutControlItemSelectedIncidents";
            this.layoutControlItemSelectedIncidents.Size = new System.Drawing.Size(477, 30);
            this.layoutControlItemSelectedIncidents.Text = "layoutControlItemSelectedIncidents";
            this.layoutControlItemSelectedIncidents.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemSelectedIncidents.TextToControlDistance = 0;
            this.layoutControlItemSelectedIncidents.TextVisible = false;
            // 
            // layoutControlItemPictureBox
            // 
            this.layoutControlItemPictureBox.Control = this.pictureBoxNumber;
            this.layoutControlItemPictureBox.CustomizationFormText = "layoutControlItemPictureBox";
            this.layoutControlItemPictureBox.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemPictureBox.MaxSize = new System.Drawing.Size(45, 45);
            this.layoutControlItemPictureBox.MinSize = new System.Drawing.Size(45, 45);
            this.layoutControlItemPictureBox.Name = "layoutControlItemPictureBox";
            this.layoutControlItemPictureBox.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItemPictureBox.Size = new System.Drawing.Size(45, 45);
            this.layoutControlItemPictureBox.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemPictureBox.Text = "layoutControlItemPictureBox";
            this.layoutControlItemPictureBox.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemPictureBox.TextToControlDistance = 0;
            this.layoutControlItemPictureBox.TextVisible = false;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(161, 30);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(477, 15);
            this.emptySpaceItem6.Text = "emptySpaceItem6";
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(45, 30);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(116, 15);
            this.emptySpaceItem7.Text = "emptySpaceItem7";
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 51);
            this.emptySpaceItem4.Name = "emptySpaceItem1";
            this.emptySpaceItem4.Size = new System.Drawing.Size(51, 69);
            this.emptySpaceItem4.Text = "emptySpaceItem1";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 120);
            this.emptySpaceItem5.Name = "emptySpaceItem2";
            this.emptySpaceItem5.Size = new System.Drawing.Size(654, 104);
            this.emptySpaceItem5.Text = "emptySpaceItem2";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // QuestionsControlDevX
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupControlBody);
            this.Name = "QuestionsControlDevX";
            this.Size = new System.Drawing.Size(642, 476);
            this.Load += new System.EventHandler(this.QuestionsControl_Load);
            this.panelIncidentTypes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIncidentTypes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAddIncidentType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAddAllIncidentType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemRemoveIncidentType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemRemoveAllIncidentType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSelectedIncidentTypes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemButtonOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlBody)).EndInit();
            this.groupControlBody.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textBoxSelectedIncidentTypes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLabelExLongQuestion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemNext)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemButtonIncidentTypes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSelectedIncidents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private VistaButtonEx buttonExQuestion1;
        private VistaButtonEx buttonExQuestion12;
        private VistaButtonEx buttonExQuestion8;
        private VistaButtonEx buttonExQuestion4;
        private VistaButtonEx buttonExQuestion11;
        private VistaButtonEx buttonExQuestion7;
        private VistaButtonEx buttonExQuestion3;
        private VistaButtonEx buttonExQuestion10;
        private VistaButtonEx buttonExQuestion6;
        private VistaButtonEx buttonExQuestion2;
        private VistaButtonEx buttonExQuestion9;
		private VistaButtonEx buttonExQuestion5;
        private VistaButtonEx buttonExQuestion14;
        private VistaButtonEx buttonExQuestion13;
        private VistaButtonEx buttonExQuestion15;
        private PanelEx panelIncidentTypes;
        private VistaButtonEx buttonRemoveAllIncidentType;
        private VistaButtonEx buttonRemoveIncidentType;
        private VistaButtonEx buttonAddAllIncidentType;
        private VistaButtonEx buttonAddIncidentType;
        public ListBoxEx listBoxSelectedIncidentTypes;
        public ListBoxEx listBoxIncidentTypes;
        private VistaButtonEx buttonIncidentTypes;
        private LabelEx labelExLongQuestion;
        private ButtonEx buttonExBack;
		private ButtonEx buttonExForward;
		private DevExpress.XtraEditors.GroupControl groupControlBody;
        private System.Windows.Forms.ToolTip toolTipMain;
        private VistaButtonEx buttonOK;
		private DevExpress.XtraLayout.LayoutControl layoutControl1;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemIncidentTypes;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemAddIncidentType;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemAddAllIncidentType;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemRemoveIncidentType;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemRemoveAllIncidentType;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemSelectedIncidentTypes;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemButtonOk;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
		private DevExpress.XtraLayout.LayoutControl layoutControl2;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemButtonIncidentTypes;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
		private DevExpress.XtraLayout.LayoutControl layoutControl3;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemLabelExLongQuestion;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemBack;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemNext;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraEditors.TextEdit textBoxSelectedIncidentTypes;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemSelectedIncidents;
		private DevExpress.XtraEditors.PictureEdit pictureBoxNumber;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
		private DragDropLayoutControl dragDropLayoutControl1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemPictureBox;
        private DevExpress.Utils.ToolTipController toolTipController;
    }
}
