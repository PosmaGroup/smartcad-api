using SmartCadControls.Controls;

namespace SmartCadGuiCommon.Controls
{
    partial class DepartmentsInvolvedControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DepartmentsInvolvedControl));
            this.departmentTypesControl = new SmartCadControls.Controls.DepartmentTypesControl();
            this.groupControlBody = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.pictureBoxNumber = new DevExpress.XtraEditors.PictureEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemDepartmentTypes = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlBody)).BeginInit();
            this.groupControlBody.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartmentTypes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // departmentTypesControl
            // 
            this.departmentTypesControl.BackColor = System.Drawing.Color.Transparent;
            this.departmentTypesControl.ForeColor = System.Drawing.Color.Black;
            this.departmentTypesControl.GlobalDepartmentTypes = null;
            this.departmentTypesControl.GlobalNotificationPriorities = null;
            this.departmentTypesControl.Location = new System.Drawing.Point(47, 2);
            this.departmentTypesControl.Name = "departmentTypesControl";
            this.departmentTypesControl.SelectedDepartmentTypesWithPriority = ((System.Collections.IList)(resources.GetObject("departmentTypesControl.SelectedDepartmentTypesWithPriority")));
            this.departmentTypesControl.Size = new System.Drawing.Size(337, 171);
            this.departmentTypesControl.TabIndex = 41;
            // 
            // groupControlBody
            // 
            this.groupControlBody.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(252)))), ((int)(((byte)(226)))));
            this.groupControlBody.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(252)))), ((int)(((byte)(226)))));
            this.groupControlBody.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(94)))), ((int)(((byte)(50)))));
            this.groupControlBody.Appearance.ForeColor = System.Drawing.Color.Red;
            this.groupControlBody.Appearance.Options.UseBackColor = true;
            this.groupControlBody.Appearance.Options.UseBorderColor = true;
            this.groupControlBody.Appearance.Options.UseForeColor = true;
            this.groupControlBody.AppearanceCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(172)))), ((int)(((byte)(68)))));
            this.groupControlBody.AppearanceCaption.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(94)))), ((int)(((byte)(50)))));
            this.groupControlBody.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControlBody.AppearanceCaption.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.groupControlBody.AppearanceCaption.Options.UseBackColor = true;
            this.groupControlBody.AppearanceCaption.Options.UseFont = true;
            this.groupControlBody.Controls.Add(this.layoutControl1);
            this.groupControlBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControlBody.Location = new System.Drawing.Point(0, 0);
            this.groupControlBody.LookAndFeel.SkinName = "Blue";
            this.groupControlBody.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.groupControlBody.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControlBody.Margin = new System.Windows.Forms.Padding(0);
            this.groupControlBody.Name = "groupControlBody";
            this.groupControlBody.Size = new System.Drawing.Size(390, 196);
            this.groupControlBody.TabIndex = 0;
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowCustomizationMenu = false;
            this.layoutControl1.Controls.Add(this.pictureBoxNumber);
            this.layoutControl1.Controls.Add(this.departmentTypesControl);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.ForeColor = System.Drawing.Color.Black;
            this.layoutControl1.Location = new System.Drawing.Point(2, 19);
            this.layoutControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.layoutControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsFocus.AllowFocusControlOnActivatedTabPage = true;
            this.layoutControl1.OptionsFocus.MoveFocusDirection = DevExpress.XtraLayout.MoveFocusDirection.DownThenAcross;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(386, 175);
            this.layoutControl1.TabIndex = 43;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // pictureBoxNumber
            // 
            this.pictureBoxNumber.Location = new System.Drawing.Point(2, 2);
            this.pictureBoxNumber.Name = "pictureBoxNumber";
            this.pictureBoxNumber.Properties.ShowMenu = false;
            this.pictureBoxNumber.Size = new System.Drawing.Size(41, 41);
            this.pictureBoxNumber.StyleController = this.layoutControl1;
            this.pictureBoxNumber.TabIndex = 43;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(252)))), ((int)(((byte)(226)))));
            this.layoutControlGroup1.AppearanceGroup.Options.UseBackColor = true;
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemDepartmentTypes,
            this.layoutControlItem1,
            this.emptySpaceItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(386, 175);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItemDepartmentTypes
            // 
            this.layoutControlItemDepartmentTypes.Control = this.departmentTypesControl;
            this.layoutControlItemDepartmentTypes.CustomizationFormText = "layoutControlItemDepartmentTypes";
            this.layoutControlItemDepartmentTypes.Location = new System.Drawing.Point(45, 0);
            this.layoutControlItemDepartmentTypes.Name = "layoutControlItemDepartmentTypes";
            this.layoutControlItemDepartmentTypes.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItemDepartmentTypes.Size = new System.Drawing.Size(341, 175);
            this.layoutControlItemDepartmentTypes.Text = "layoutControlItemDepartmentTypes";
            this.layoutControlItemDepartmentTypes.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemDepartmentTypes.TextToControlDistance = 0;
            this.layoutControlItemDepartmentTypes.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.pictureBoxNumber;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(45, 45);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(45, 45);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem1.Size = new System.Drawing.Size(45, 45);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 45);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(45, 130);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // DepartmentsInvolvedControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupControlBody);
            this.Name = "DepartmentsInvolvedControl";
            this.Size = new System.Drawing.Size(390, 196);
            this.Load += new System.EventHandler(this.DepartmentsInvolvedControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControlBody)).EndInit();
            this.groupControlBody.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartmentTypes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

		private DevExpress.XtraEditors.GroupControl groupControlBody;
		public DepartmentTypesControl departmentTypesControl;
		private DevExpress.XtraLayout.LayoutControl layoutControl1;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDepartmentTypes;
		private DevExpress.XtraEditors.PictureEdit pictureBoxNumber;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
    }
}
