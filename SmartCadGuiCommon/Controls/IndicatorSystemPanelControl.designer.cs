namespace SmartCadGuiCommon.Controls
{
    partial class IndicatorSystemPanelControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControlIndicatorSystemPanelControl = new DevExpress.XtraLayout.LayoutControl();
            this.operatorIndicatorControl21 = new OperatorIndicatorControl2();
            this.dispatchOrderGridIndicatorControl1 = new DispatchOrderGridIndicatorControl();
            this.emergencyIndicatorControl1 = new EmergencyIndicatorControl();
            this.incidentIndicatorControl2 = new IncidentIndicatorControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemOperatorIndicatorControl21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemIncidentIndicatorControl2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemDispatchOrderGridIndicatorControl1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemEmergencyIndicatorControl1 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlIndicatorSystemPanelControl)).BeginInit();
            this.layoutControlIndicatorSystemPanelControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOperatorIndicatorControl21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIncidentIndicatorControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDispatchOrderGridIndicatorControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemEmergencyIndicatorControl1)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControlIndicatorSystemPanelControl
            // 
            this.layoutControlIndicatorSystemPanelControl.Controls.Add(this.operatorIndicatorControl21);
            this.layoutControlIndicatorSystemPanelControl.Controls.Add(this.dispatchOrderGridIndicatorControl1);
            this.layoutControlIndicatorSystemPanelControl.Controls.Add(this.emergencyIndicatorControl1);
            this.layoutControlIndicatorSystemPanelControl.Controls.Add(this.incidentIndicatorControl2);
            this.layoutControlIndicatorSystemPanelControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlIndicatorSystemPanelControl.Location = new System.Drawing.Point(0, 0);
            this.layoutControlIndicatorSystemPanelControl.Name = "layoutControlIndicatorSystemPanelControl";
            this.layoutControlIndicatorSystemPanelControl.Root = this.layoutControlGroup1;
            this.layoutControlIndicatorSystemPanelControl.Size = new System.Drawing.Size(1270, 345);
            this.layoutControlIndicatorSystemPanelControl.TabIndex = 6;
            this.layoutControlIndicatorSystemPanelControl.Text = "layoutControl1";
            // 
            // operatorIndicatorControl21
            // 
            this.operatorIndicatorControl21.Location = new System.Drawing.Point(2, 2);
            this.operatorIndicatorControl21.LookAndFeel.SkinName = "Blue";
            this.operatorIndicatorControl21.LookAndFeel.UseDefaultLookAndFeel = false;
            this.operatorIndicatorControl21.Name = "operatorIndicatorControl21";
            this.operatorIndicatorControl21.Size = new System.Drawing.Size(631, 168);
            this.operatorIndicatorControl21.TabIndex = 5;
            // 
            // dispatchOrderGridIndicatorControl1
            // 
            this.dispatchOrderGridIndicatorControl1.DataSource = null;
            this.dispatchOrderGridIndicatorControl1.Location = new System.Drawing.Point(637, 174);
            this.dispatchOrderGridIndicatorControl1.Name = "dispatchOrderGridIndicatorControl1";
            this.dispatchOrderGridIndicatorControl1.Size = new System.Drawing.Size(631, 169);
            this.dispatchOrderGridIndicatorControl1.TabIndex = 2;
            // 
            // emergencyIndicatorControl1
            // 
            this.emergencyIndicatorControl1.Location = new System.Drawing.Point(637, 2);
            this.emergencyIndicatorControl1.LookAndFeel.SkinName = "Blue";
            this.emergencyIndicatorControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.emergencyIndicatorControl1.Name = "emergencyIndicatorControl1";
            this.emergencyIndicatorControl1.Size = new System.Drawing.Size(631, 168);
            this.emergencyIndicatorControl1.TabIndex = 4;
            // 
            // incidentIndicatorControl2
            // 
            this.incidentIndicatorControl2.Location = new System.Drawing.Point(2, 174);
            this.incidentIndicatorControl2.LookAndFeel.SkinName = "Blue";
            this.incidentIndicatorControl2.LookAndFeel.UseDefaultLookAndFeel = false;
            this.incidentIndicatorControl2.Name = "incidentIndicatorControl2";
            this.incidentIndicatorControl2.Size = new System.Drawing.Size(631, 169);
            this.incidentIndicatorControl2.TabIndex = 1;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemOperatorIndicatorControl21,
            this.layoutControlItemIncidentIndicatorControl2,
            this.layoutControlItemDispatchOrderGridIndicatorControl1,
            this.layoutControlItemEmergencyIndicatorControl1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1270, 345);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItemOperatorIndicatorControl21
            // 
            this.layoutControlItemOperatorIndicatorControl21.Control = this.operatorIndicatorControl21;
            this.layoutControlItemOperatorIndicatorControl21.CustomizationFormText = "layoutControlItemOperatorIndicatorControl21";
            this.layoutControlItemOperatorIndicatorControl21.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemOperatorIndicatorControl21.Name = "layoutControlItemOperatorIndicatorControl21";
            this.layoutControlItemOperatorIndicatorControl21.Size = new System.Drawing.Size(635, 172);
            this.layoutControlItemOperatorIndicatorControl21.Text = "layoutControlItemOperatorIndicatorControl21";
            this.layoutControlItemOperatorIndicatorControl21.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemOperatorIndicatorControl21.TextToControlDistance = 0;
            this.layoutControlItemOperatorIndicatorControl21.TextVisible = false;
            // 
            // layoutControlItemIncidentIndicatorControl2
            // 
            this.layoutControlItemIncidentIndicatorControl2.Control = this.incidentIndicatorControl2;
            this.layoutControlItemIncidentIndicatorControl2.CustomizationFormText = "layoutControlItemIncidentIndicatorControl2";
            this.layoutControlItemIncidentIndicatorControl2.Location = new System.Drawing.Point(0, 172);
            this.layoutControlItemIncidentIndicatorControl2.Name = "layoutControlItemIncidentIndicatorControl2";
            this.layoutControlItemIncidentIndicatorControl2.Size = new System.Drawing.Size(635, 173);
            this.layoutControlItemIncidentIndicatorControl2.Text = "layoutControlItemIncidentIndicatorControl2";
            this.layoutControlItemIncidentIndicatorControl2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemIncidentIndicatorControl2.TextToControlDistance = 0;
            this.layoutControlItemIncidentIndicatorControl2.TextVisible = false;
            // 
            // layoutControlItemDispatchOrderGridIndicatorControl1
            // 
            this.layoutControlItemDispatchOrderGridIndicatorControl1.Control = this.dispatchOrderGridIndicatorControl1;
            this.layoutControlItemDispatchOrderGridIndicatorControl1.CustomizationFormText = "layoutControlItemDispatchOrderGridIndicatorControl1";
            this.layoutControlItemDispatchOrderGridIndicatorControl1.Location = new System.Drawing.Point(635, 172);
            this.layoutControlItemDispatchOrderGridIndicatorControl1.Name = "layoutControlItemDispatchOrderGridIndicatorControl1";
            this.layoutControlItemDispatchOrderGridIndicatorControl1.Size = new System.Drawing.Size(635, 173);
            this.layoutControlItemDispatchOrderGridIndicatorControl1.Text = "layoutControlItemDispatchOrderGridIndicatorControl1";
            this.layoutControlItemDispatchOrderGridIndicatorControl1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemDispatchOrderGridIndicatorControl1.TextToControlDistance = 0;
            this.layoutControlItemDispatchOrderGridIndicatorControl1.TextVisible = false;
            // 
            // layoutControlItemEmergencyIndicatorControl1
            // 
            this.layoutControlItemEmergencyIndicatorControl1.Control = this.emergencyIndicatorControl1;
            this.layoutControlItemEmergencyIndicatorControl1.CustomizationFormText = "layoutControlItemEmergencyIndicatorControl1";
            this.layoutControlItemEmergencyIndicatorControl1.Location = new System.Drawing.Point(635, 0);
            this.layoutControlItemEmergencyIndicatorControl1.Name = "layoutControlItemEmergencyIndicatorControl1";
            this.layoutControlItemEmergencyIndicatorControl1.Size = new System.Drawing.Size(635, 172);
            this.layoutControlItemEmergencyIndicatorControl1.Text = "layoutControlItemEmergencyIndicatorControl1";
            this.layoutControlItemEmergencyIndicatorControl1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemEmergencyIndicatorControl1.TextToControlDistance = 0;
            this.layoutControlItemEmergencyIndicatorControl1.TextVisible = false;
            // 
            // IndicatorSystemPanelControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControlIndicatorSystemPanelControl);
            this.LookAndFeel.SkinName = "Blue";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "IndicatorSystemPanelControl";
            this.Size = new System.Drawing.Size(1270, 345);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlIndicatorSystemPanelControl)).EndInit();
            this.layoutControlIndicatorSystemPanelControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOperatorIndicatorControl21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIncidentIndicatorControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDispatchOrderGridIndicatorControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemEmergencyIndicatorControl1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal IncidentIndicatorControl incidentIndicatorControl2;
        internal DispatchOrderGridIndicatorControl dispatchOrderGridIndicatorControl1;
        internal EmergencyIndicatorControl emergencyIndicatorControl1;
        internal OperatorIndicatorControl2 operatorIndicatorControl21;
        private DevExpress.XtraLayout.LayoutControl layoutControlIndicatorSystemPanelControl;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemOperatorIndicatorControl21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemIncidentIndicatorControl2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDispatchOrderGridIndicatorControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemEmergencyIndicatorControl1;

    }
}
