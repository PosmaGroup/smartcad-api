namespace SmartCadGuiCommon.Controls
{
    partial class DispatchOrderGridIndicatorControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.advBandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.bandedGridColumnDepartmentName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnOpenHeight = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnOpenMedium = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnOpenLow = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnOpenTotal = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnCloseHeight = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnCloseMedium = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnCloseLow = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnCloseTotal = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemGridLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemCalcEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.gridBandDispatch = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBandOpen = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBandClosed = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.LookAndFeel.SkinName = "Blue";
            this.gridControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gridControl1.MainView = this.advBandedGridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemGridLookUpEdit1,
            this.repositoryItemCalcEdit1});
            this.gridControl1.Size = new System.Drawing.Size(695, 141);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.advBandedGridView1});
            // 
            // advBandedGridView1
            // 
            this.advBandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBandDispatch,
            this.gridBandOpen,
            this.gridBandClosed});
            this.advBandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bandedGridColumnOpenHeight,
            this.bandedGridColumnOpenMedium,
            this.bandedGridColumnOpenLow,
            this.bandedGridColumnOpenTotal,
            this.bandedGridColumnCloseHeight,
            this.bandedGridColumnCloseMedium,
            this.bandedGridColumnCloseLow,
            this.bandedGridColumnCloseTotal,
            this.bandedGridColumnDepartmentName});
            this.advBandedGridView1.GridControl = this.gridControl1;
            this.advBandedGridView1.GroupFormat = "[#image]{1} {2}";
            this.advBandedGridView1.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "OpenHeight", this.bandedGridColumnOpenHeight, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "OpenMedium", this.bandedGridColumnOpenMedium, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "OpenLow", this.bandedGridColumnOpenLow, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "OpenTotal", this.bandedGridColumnOpenTotal, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CloseHeight", this.bandedGridColumnCloseHeight, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CloseMedium", this.bandedGridColumnCloseMedium, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CloseLow", this.bandedGridColumnCloseLow, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CloseTotal", this.bandedGridColumnCloseTotal, "SUM={0}")});
            this.advBandedGridView1.Name = "advBandedGridView1";
            this.advBandedGridView1.OptionsCustomization.AllowFilter = false;
            this.advBandedGridView1.OptionsMenu.EnableColumnMenu = false;
            this.advBandedGridView1.OptionsMenu.EnableFooterMenu = false;
            this.advBandedGridView1.OptionsMenu.EnableGroupPanelMenu = false;
            this.advBandedGridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.advBandedGridView1.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.advBandedGridView1.OptionsSelection.UseIndicatorForSelection = false;
            this.advBandedGridView1.OptionsView.ShowFooter = true;
            this.advBandedGridView1.OptionsView.ShowGroupPanel = false;
            this.advBandedGridView1.OptionsView.ShowIndicator = false;
            this.advBandedGridView1.DragObjectOver += new DevExpress.XtraGrid.Views.Base.DragObjectOverEventHandler(this.advBandedGridView1_DragObjectOver);
            // 
            // bandedGridColumnDepartmentName
            // 
            this.bandedGridColumnDepartmentName.Caption = "Organismo";
            this.bandedGridColumnDepartmentName.FieldName = "DepartmentName";
            this.bandedGridColumnDepartmentName.Name = "bandedGridColumnDepartmentName";
            this.bandedGridColumnDepartmentName.OptionsColumn.AllowEdit = false;
            this.bandedGridColumnDepartmentName.Visible = true;
            // 
            // bandedGridColumnOpenHeight
            // 
            this.bandedGridColumnOpenHeight.Caption = "Alta";
            this.bandedGridColumnOpenHeight.FieldName = "OpenHeight";
            this.bandedGridColumnOpenHeight.Name = "bandedGridColumnOpenHeight";
            this.bandedGridColumnOpenHeight.OptionsColumn.AllowEdit = false;
            this.bandedGridColumnOpenHeight.SummaryItem.DisplayFormat = "{0}";
            this.bandedGridColumnOpenHeight.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.bandedGridColumnOpenHeight.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.bandedGridColumnOpenHeight.Visible = true;
            // 
            // bandedGridColumnOpenMedium
            // 
            this.bandedGridColumnOpenMedium.Caption = "Media";
            this.bandedGridColumnOpenMedium.FieldName = "OpenMedium";
            this.bandedGridColumnOpenMedium.Name = "bandedGridColumnOpenMedium";
            this.bandedGridColumnOpenMedium.SummaryItem.DisplayFormat = "{0}";
            this.bandedGridColumnOpenMedium.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.bandedGridColumnOpenMedium.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.bandedGridColumnOpenMedium.Visible = true;
            // 
            // bandedGridColumnOpenLow
            // 
            this.bandedGridColumnOpenLow.Caption = "Baja";
            this.bandedGridColumnOpenLow.FieldName = "OpenLow";
            this.bandedGridColumnOpenLow.Name = "bandedGridColumnOpenLow";
            this.bandedGridColumnOpenLow.OptionsColumn.AllowEdit = false;
            this.bandedGridColumnOpenLow.SummaryItem.DisplayFormat = "{0}";
            this.bandedGridColumnOpenLow.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.bandedGridColumnOpenLow.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.bandedGridColumnOpenLow.Visible = true;
            // 
            // bandedGridColumnOpenTotal
            // 
            this.bandedGridColumnOpenTotal.Caption = "Total";
            this.bandedGridColumnOpenTotal.FieldName = "OpenTotal";
            this.bandedGridColumnOpenTotal.Name = "bandedGridColumnOpenTotal";
            this.bandedGridColumnOpenTotal.OptionsColumn.AllowEdit = false;
            this.bandedGridColumnOpenTotal.SummaryItem.DisplayFormat = "{0}";
            this.bandedGridColumnOpenTotal.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.bandedGridColumnOpenTotal.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.bandedGridColumnOpenTotal.Visible = true;
            // 
            // bandedGridColumnCloseHeight
            // 
            this.bandedGridColumnCloseHeight.Caption = "Alta";
            this.bandedGridColumnCloseHeight.FieldName = "CloseHeight";
            this.bandedGridColumnCloseHeight.Name = "bandedGridColumnCloseHeight";
            this.bandedGridColumnCloseHeight.OptionsColumn.AllowEdit = false;
            this.bandedGridColumnCloseHeight.SummaryItem.DisplayFormat = "{0}";
            this.bandedGridColumnCloseHeight.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.bandedGridColumnCloseHeight.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.bandedGridColumnCloseHeight.Visible = true;
            this.bandedGridColumnCloseHeight.Width = 77;
            // 
            // bandedGridColumnCloseMedium
            // 
            this.bandedGridColumnCloseMedium.Caption = "Media";
            this.bandedGridColumnCloseMedium.FieldName = "CloseMedium";
            this.bandedGridColumnCloseMedium.Name = "bandedGridColumnCloseMedium";
            this.bandedGridColumnCloseMedium.OptionsColumn.AllowEdit = false;
            this.bandedGridColumnCloseMedium.SummaryItem.DisplayFormat = "{0}";
            this.bandedGridColumnCloseMedium.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.bandedGridColumnCloseMedium.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.bandedGridColumnCloseMedium.Visible = true;
            this.bandedGridColumnCloseMedium.Width = 77;
            // 
            // bandedGridColumnCloseLow
            // 
            this.bandedGridColumnCloseLow.Caption = "Baja";
            this.bandedGridColumnCloseLow.FieldName = "CloseLow";
            this.bandedGridColumnCloseLow.Name = "bandedGridColumnCloseLow";
            this.bandedGridColumnCloseLow.OptionsColumn.AllowEdit = false;
            this.bandedGridColumnCloseLow.SummaryItem.DisplayFormat = "{0}";
            this.bandedGridColumnCloseLow.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.bandedGridColumnCloseLow.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.bandedGridColumnCloseLow.Visible = true;
            this.bandedGridColumnCloseLow.Width = 77;
            // 
            // bandedGridColumnCloseTotal
            // 
            this.bandedGridColumnCloseTotal.Caption = "Total";
            this.bandedGridColumnCloseTotal.FieldName = "CloseTotal";
            this.bandedGridColumnCloseTotal.Name = "bandedGridColumnCloseTotal";
            this.bandedGridColumnCloseTotal.OptionsColumn.AllowEdit = false;
            this.bandedGridColumnCloseTotal.SummaryItem.DisplayFormat = "{0}";
            this.bandedGridColumnCloseTotal.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.bandedGridColumnCloseTotal.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.bandedGridColumnCloseTotal.Visible = true;
            this.bandedGridColumnCloseTotal.Width = 80;
            // 
            // repositoryItemGridLookUpEdit1
            // 
            this.repositoryItemGridLookUpEdit1.AutoHeight = false;
            this.repositoryItemGridLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit1.Name = "repositoryItemGridLookUpEdit1";
            this.repositoryItemGridLookUpEdit1.View = this.repositoryItemGridLookUpEdit1View;
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // repositoryItemCalcEdit1
            // 
            this.repositoryItemCalcEdit1.AutoHeight = false;
            this.repositoryItemCalcEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCalcEdit1.Name = "repositoryItemCalcEdit1";
            // 
            // gridBandDispatch
            // 
            this.gridBandDispatch.Caption = "Solicitudes de despacho";
            this.gridBandDispatch.Columns.Add(this.bandedGridColumnDepartmentName);
            this.gridBandDispatch.Name = "gridBandDispatch";
            this.gridBandDispatch.Width = 75;
            // 
            // gridBandOpen
            // 
            this.gridBandOpen.Caption = "Abiertas";
            this.gridBandOpen.Columns.Add(this.bandedGridColumnOpenHeight);
            this.gridBandOpen.Columns.Add(this.bandedGridColumnOpenMedium);
            this.gridBandOpen.Columns.Add(this.bandedGridColumnOpenLow);
            this.gridBandOpen.Columns.Add(this.bandedGridColumnOpenTotal);
            this.gridBandOpen.Name = "gridBandOpen";
            this.gridBandOpen.Width = 300;
            // 
            // gridBandClosed
            // 
            this.gridBandClosed.Caption = "Cerradas";
            this.gridBandClosed.Columns.Add(this.bandedGridColumnCloseHeight);
            this.gridBandClosed.Columns.Add(this.bandedGridColumnCloseMedium);
            this.gridBandClosed.Columns.Add(this.bandedGridColumnCloseLow);
            this.gridBandClosed.Columns.Add(this.bandedGridColumnCloseTotal);
            this.gridBandClosed.Name = "gridBandClosed";
            this.gridBandClosed.Width = 311;
            // 
            // DispatchOrderGridIndicatorControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl1);
            this.Name = "DispatchOrderGridIndicatorControl";
            this.Size = new System.Drawing.Size(695, 141);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView advBandedGridView1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnOpenHeight;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnOpenLow;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnOpenTotal;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnCloseHeight;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnCloseMedium;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnCloseLow;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnCloseTotal;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnDepartmentName;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnOpenMedium;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBandDispatch;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBandOpen;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBandClosed;
    }
}
