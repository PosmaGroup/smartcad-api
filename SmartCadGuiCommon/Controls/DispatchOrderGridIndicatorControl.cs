using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using DevExpress.XtraGrid.Views.Base;
using Smartmatic.SmartCad.Service;
using DevExpress.XtraGrid.Views.Layout;
using DevExpress.XtraGrid.Views.Grid;
using System.Threading;
using DevExpress.XtraGrid.Columns;
using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadCore.ClientData;

namespace SmartCadGuiCommon.Controls
{
   
    public partial class DispatchOrderGridIndicatorControl : UserControl
    {
       

        private BindingList<DispatchOrderGridControlData> dataSource;
        public DispatchOrderGridIndicatorControl()
        {
            InitializeComponent();
            LoadLanguage();            
        }       
        private void LoadLanguage()
        {
            gridBandClosed.Caption = ResourceLoader.GetString2("Closed");
            gridBandOpen.Caption = ResourceLoader.GetString2("Open");
            gridBandDispatch.Caption = ResourceLoader.GetString2("DispatchOrders");
            bandedGridColumnDepartmentName.Caption = ResourceLoader.GetString2("Department");
            bandedGridColumnOpenHeight.Caption = ResourceLoader.GetString2("High");
            bandedGridColumnOpenMedium.Caption = ResourceLoader.GetString2("Medium");
            bandedGridColumnOpenLow.Caption = ResourceLoader.GetString2("Low");
            bandedGridColumnOpenTotal.Caption = ResourceLoader.GetString2("Total");
            bandedGridColumnCloseHeight.Caption = ResourceLoader.GetString2("High");
            bandedGridColumnCloseMedium.Caption = ResourceLoader.GetString2("Medium");
            bandedGridColumnCloseLow.Caption = ResourceLoader.GetString2("Low");
            bandedGridColumnCloseTotal.Caption = ResourceLoader.GetString2("Total");
            
        }
        public BindingList<DispatchOrderGridControlData> DataSource
        {
            get
            {
                return dataSource;
            }
            set
            {
                dataSource = value;
                FormUtil.InvokeRequired(this.gridControl1, delegate
                {
                    this.gridControl1.DataSource = dataSource;
                });
            }
        }
        public void Load(IList list)
        {
            BindingList<DispatchOrderGridControlData> listValue = new BindingList<DispatchOrderGridControlData>();            
            foreach (IndicatorResultValuesClientData value in list)
            {
                DispatchOrderGridControlData data = new DispatchOrderGridControlData(value);
                if (listValue.Contains(data) == false)
                {
                    listValue.Add(data);
                   
                }
            }          
            DataSource = listValue;
           
        }
        public void AddOrUpdate(IndicatorResultValuesClientData result)
        {
            if (DataSource == null)
            {
                DataSource = new BindingList<DispatchOrderGridControlData>();
            }

            DispatchOrderGridControlData indicatorGridControlData = new DispatchOrderGridControlData(result);           
            int index = dataSource.IndexOf(indicatorGridControlData);
            if (index > -1)
            {
                FormUtil.InvokeRequired(this.gridControl1, delegate
                {
                    dataSource[index] = new DispatchOrderGridControlData(result);
                });

            }
            else
            {
                FormUtil.InvokeRequired(this.gridControl1, delegate
                {
                    dataSource.Add(new DispatchOrderGridControlData(result));
                });
            }
          
        }

        private void advBandedGridView1_DragObjectOver(object sender, DragObjectOverEventArgs e)
        {
            GridColumn column = e.DragObject as GridColumn;
            if (column != null && e.DropInfo.Index < 0)
            {
                e.DropInfo.Valid = false;
            }
            else if ((e.DragObject as DevExpress.XtraGrid.Views.BandedGrid.GridBand) != null)
            {
                e.DropInfo.Valid = false;

            }

        }
             
    }
    public class DispatchOrderGridControlData
    {
        private int openHeight;
        private int openMedium;
        private int openLow;
        private int openTotal;
        private int closeHeight;
        private int closeMedium;
        private int closeLow;
        private int closeTotal;
        private string departmentName;

        public int OpenHeight
        {
            get
            {
                return openHeight;
            }

            set
            {
                openHeight = value;
            }
        }
        public int OpenMedium
        {
            get
            {
                return openMedium;
            }

            set
            {
                openMedium = value;
            }
        }
        public int OpenLow
        {
            get
            {
                return openLow;
            }

            set
            {
                openLow = value;
            }
        }
        public int OpenTotal
        {
            get
            {
                return openTotal;
            }

            set
            {
                openTotal = value;
            }
        }
        public int CloseHeight
        {
            get
            {
                return closeHeight;
            }

            set
            {
                closeHeight = value;
            }
        }
        public int CloseMedium
        {
            get
            {
                return closeMedium;
            }

            set
            {
                closeMedium = value;
            }
        }
        public int CloseLow
        {
            get
            {
                return closeLow;
            }

            set
            {
                closeLow = value;
            }
        }
        public int CloseTotal
        {
            get
            {
                return closeTotal;
            }

            set
            {
                closeTotal = value;
            }
        }       
        public string DepartmentName
        {
            get { return departmentName; }
            set { departmentName = value; }
        }

        public DispatchOrderGridControlData(IndicatorResultValuesClientData indicatorResultValuesClientData)
        {
            if (indicatorResultValuesClientData.ResultValue != null)
            {
                List<double> values = ApplicationUtil.ConvertIndicatorResultValuesFromString(
                    indicatorResultValuesClientData.ResultValue);

                openHeight = Convert.ToInt32(values[0]);//.ToString();
                openMedium = Convert.ToInt32(values[1]);//.ToString();
                openLow = Convert.ToInt32(values[2]);//.ToString();
                openTotal = Convert.ToInt32(values[3]);//.ToString();
                closeHeight = Convert.ToInt32(values[4]);//.ToString();
                closeMedium = Convert.ToInt32(values[5]);//.ToString();
                closeLow = Convert.ToInt32(values[6]);//.ToString();
                closeTotal = Convert.ToInt32(values[7]);//.ToString();
                IList department = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(
                    SmartCadHqls.GetDepartmentsTypeByCode,
                            indicatorResultValuesClientData.CustomCode));
                departmentName = (department[0] as DepartmentTypeClientData).Name;
            }
        }
         
        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is DispatchOrderGridControlData)
            {
                result = this.DepartmentName == ((DispatchOrderGridControlData)obj).DepartmentName;
            }
            return result;
        }
    }

   


}
