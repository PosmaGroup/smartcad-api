
namespace SmartCadGuiCommon.Controls
{
    partial class IndicatorGroupFirstLevelPanelControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControlIndicatorFirstLevel = new DevExpress.XtraLayout.LayoutControl();
            this.operatorIndicatorControl21 = new OperatorIndicatorControl2();
            this.speedRequestIndicatorChartControl1 = new SpeedRequestIndicatorChartControl();
            this.emergencyIndicatorControl1 = new EmergencyIndicatorControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemOperatorIndicator = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemEmergencyIndicator = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemSpeedRequestChart = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlIndicatorFirstLevel)).BeginInit();
            this.layoutControlIndicatorFirstLevel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOperatorIndicator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemEmergencyIndicator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSpeedRequestChart)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControlIndicatorFirstLevel
            // 
            this.layoutControlIndicatorFirstLevel.Controls.Add(this.operatorIndicatorControl21);
            this.layoutControlIndicatorFirstLevel.Controls.Add(this.speedRequestIndicatorChartControl1);
            this.layoutControlIndicatorFirstLevel.Controls.Add(this.emergencyIndicatorControl1);
            this.layoutControlIndicatorFirstLevel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlIndicatorFirstLevel.Location = new System.Drawing.Point(0, 0);
            this.layoutControlIndicatorFirstLevel.Name = "layoutControlIndicatorFirstLevel";
            this.layoutControlIndicatorFirstLevel.Root = this.layoutControlGroup1;
            this.layoutControlIndicatorFirstLevel.Size = new System.Drawing.Size(1270, 345);
            this.layoutControlIndicatorFirstLevel.TabIndex = 7;
            this.layoutControlIndicatorFirstLevel.Text = "layoutControl1";
            // 
            // operatorIndicatorControl21
            // 
            this.operatorIndicatorControl21.Location = new System.Drawing.Point(2, 2);
            this.operatorIndicatorControl21.LookAndFeel.SkinName = "Blue";
            this.operatorIndicatorControl21.LookAndFeel.UseDefaultLookAndFeel = false;
            this.operatorIndicatorControl21.Name = "operatorIndicatorControl21";
            this.operatorIndicatorControl21.Size = new System.Drawing.Size(631, 168);
            this.operatorIndicatorControl21.TabIndex = 5;
            // 
            // speedRequestIndicatorChartControl1
            // 
            this.speedRequestIndicatorChartControl1.Location = new System.Drawing.Point(637, 2);
            this.speedRequestIndicatorChartControl1.LookAndFeel.SkinName = "Blue";
            this.speedRequestIndicatorChartControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.speedRequestIndicatorChartControl1.Name = "speedRequestIndicatorChartControl1";
            this.speedRequestIndicatorChartControl1.Size = new System.Drawing.Size(631, 341);
            this.speedRequestIndicatorChartControl1.TabIndex = 6;
            // 
            // emergencyIndicatorControl1
            // 
            this.emergencyIndicatorControl1.Location = new System.Drawing.Point(2, 174);
            this.emergencyIndicatorControl1.LookAndFeel.SkinName = "Blue";
            this.emergencyIndicatorControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.emergencyIndicatorControl1.Name = "emergencyIndicatorControl1";
            this.emergencyIndicatorControl1.Size = new System.Drawing.Size(631, 169);
            this.emergencyIndicatorControl1.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemOperatorIndicator,
            this.layoutControlItemEmergencyIndicator,
            this.layoutControlItemSpeedRequestChart});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1270, 345);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItemOperatorIndicator
            // 
            this.layoutControlItemOperatorIndicator.Control = this.operatorIndicatorControl21;
            this.layoutControlItemOperatorIndicator.CustomizationFormText = "layoutControlItemOperatorIndicator";
            this.layoutControlItemOperatorIndicator.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemOperatorIndicator.Name = "layoutControlItemOperatorIndicator";
            this.layoutControlItemOperatorIndicator.Size = new System.Drawing.Size(635, 172);
            this.layoutControlItemOperatorIndicator.Text = "layoutControlItemOperatorIndicator";
            this.layoutControlItemOperatorIndicator.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemOperatorIndicator.TextToControlDistance = 0;
            this.layoutControlItemOperatorIndicator.TextVisible = false;
            // 
            // layoutControlItemEmergencyIndicator
            // 
            this.layoutControlItemEmergencyIndicator.Control = this.emergencyIndicatorControl1;
            this.layoutControlItemEmergencyIndicator.CustomizationFormText = "layoutControlItemEmergencyIndicator";
            this.layoutControlItemEmergencyIndicator.Location = new System.Drawing.Point(0, 172);
            this.layoutControlItemEmergencyIndicator.Name = "layoutControlItemEmergencyIndicator";
            this.layoutControlItemEmergencyIndicator.Size = new System.Drawing.Size(635, 173);
            this.layoutControlItemEmergencyIndicator.Text = "layoutControlItemEmergencyIndicator";
            this.layoutControlItemEmergencyIndicator.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemEmergencyIndicator.TextToControlDistance = 0;
            this.layoutControlItemEmergencyIndicator.TextVisible = false;
            // 
            // layoutControlItemSpeedRequestChart
            // 
            this.layoutControlItemSpeedRequestChart.Control = this.speedRequestIndicatorChartControl1;
            this.layoutControlItemSpeedRequestChart.CustomizationFormText = "layoutControlItemSpeedRequestChart";
            this.layoutControlItemSpeedRequestChart.Location = new System.Drawing.Point(635, 0);
            this.layoutControlItemSpeedRequestChart.Name = "layoutControlItemSpeedRequestChart";
            this.layoutControlItemSpeedRequestChart.Size = new System.Drawing.Size(635, 345);
            this.layoutControlItemSpeedRequestChart.Text = "layoutControlItemSpeedRequestChart";
            this.layoutControlItemSpeedRequestChart.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemSpeedRequestChart.TextToControlDistance = 0;
            this.layoutControlItemSpeedRequestChart.TextVisible = false;
            // 
            // IndicatorGroupFirstLevelPanelControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControlIndicatorFirstLevel);
            this.LookAndFeel.SkinName = "Blue";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "IndicatorGroupFirstLevelPanelControl";
            this.Size = new System.Drawing.Size(1270, 345);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlIndicatorFirstLevel)).EndInit();
            this.layoutControlIndicatorFirstLevel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOperatorIndicator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemEmergencyIndicator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSpeedRequestChart)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal EmergencyIndicatorControl emergencyIndicatorControl1;
        internal OperatorIndicatorControl2 operatorIndicatorControl21;
        internal SpeedRequestIndicatorChartControl speedRequestIndicatorChartControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControlIndicatorFirstLevel;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemOperatorIndicator;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemEmergencyIndicator;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemSpeedRequestChart;

    }
}
