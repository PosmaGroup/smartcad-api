using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using DevExpress.XtraGrid.Views.Base;
using Smartmatic.SmartCad.Service;
using DevExpress.XtraGrid.Views.Layout;
using DevExpress.XtraGrid.Views.Grid;
using System.Threading;
using DevExpress.XtraGrid.Columns;
using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadCore.ClientData;
using SmartCadControls.Controls;

namespace SmartCadGuiCommon.Controls
{
    public delegate void EmergencyIndicatorCharEventHandler(object sender, EmergencyIndicatorChartEventArgs e);
    public partial class EmergencyGridIndicatorControl : UserControl
    {
        public event EmergencyIndicatorCharEventHandler EmergencyIndicatorEvent;
        private BindingList<EmergencyGridControlData> dataSource;
        public EmergencyGridIndicatorControl()
        {
            InitializeComponent();
            LoadLanguage();
        }

      

        private void LoadLanguage()
        {
            gridColumnIndicator.Caption = ResourceLoader.GetString2("Calls");
            gridColumnValue.Caption = ResourceLoader.GetString2("RealValue"); 
            gridColumnPercentValue.Caption = ResourceLoader.GetString2("Percent");
            //gridColumnMediaTime.Caption = ResourceLoader.GetString2("MediaTime");
        }
        public BindingList<EmergencyGridControlData> DataSource
        {
            get
            {
                return dataSource;
            }
            set
            {
                dataSource = value;
                FormUtil.InvokeRequired(this.gridControl1, delegate
                {
                    this.gridControl1.DataSource = dataSource;
                });
            }
        }
        public void Load(IList list)
        {
            BindingList<EmergencyGridControlData> listValue = new BindingList<EmergencyGridControlData>();            
            foreach (IndicatorResultValuesClientData value in list)
            {
                EmergencyGridControlData data = new EmergencyGridControlData(value);
                if (listValue.Contains(data) == false)
                {
                    listValue.Add(data);
                }
            }
            DataSource = listValue;

            EmergencyIndicatorChartEventArgs eventArgs =
                new EmergencyIndicatorChartEventArgs();
            eventArgs.List = new ArrayList();
            eventArgs.List = listValue;
            if (EmergencyIndicatorEvent != null)
            {
                EmergencyIndicatorEvent(this, eventArgs);
            }
        }
        public void AddOrUpdate(IndicatorResultValuesClientData result)
        {
            if (DataSource == null)
            {
                DataSource = new BindingList<EmergencyGridControlData>();
            }
            else
            {
                BindingList<EmergencyGridControlData> dataSourceAux =
                    new BindingList<EmergencyGridControlData>();

                foreach (EmergencyGridControlData emergency in dataSource)
                {
                    dataSourceAux.Add(emergency);
                }
                foreach (EmergencyGridControlData emergency in dataSourceAux)
                {
                    if (result.Date > emergency.Date)
                    {
                        FormUtil.InvokeRequired(this.gridControl1, delegate
                        {

                            gridControl1.DeleteItem(emergency);
                        });
                    }

                }
            }
            EmergencyGridControlData indicatorGridControlData = new EmergencyGridControlData(result);
            
            int index = dataSource.IndexOf(indicatorGridControlData);

            //if (((result.IndicatorCustomCode == "L08") ||
            //    (result.IndicatorCustomCode == "L13"))
            //    &&(dataSource.Count >0))
            if ((result.Description ==  ResourceLoader.GetString2("IndicatorL08Name") ||
              result.Description == ResourceLoader.GetString2("IndicatorL13Name"))
              && (dataSource.Count > 0))
            {

                int dataIndex = 1;

              

                for (int i = 0; i < dataSource.Count; i++)
                {
                    EmergencyGridControlData em = dataSource[i];
                    //if ((em.IndicatorCustomCode == "PND22") ||
                    //     (em.IndicatorCustomCode == "DPN22"))
                    if (em.Name == ResourceLoader.GetString2("IndicatorPND22Name"))
                    {
                        dataIndex = i;
                        break;
                    }
                }


               

                if (dataSource[dataIndex].ListResults == null)
                {
                    dataSource[dataIndex].ListResults = new List<EmergencyGridControlData>();
                }
                int listResultsIndex = dataSource[dataIndex].ListResults.IndexOf(indicatorGridControlData);
                if (listResultsIndex > -1)
                {
                    dataSource[dataIndex].ListResults[listResultsIndex] = new EmergencyGridControlData(result);

                }
                else
                {
                    dataSource[dataIndex].ListResults.Add(new EmergencyGridControlData(result));
                }

                if (gridControl1.Views.Count > 1)
                    ((GridView)gridControl1.Views[1]).RefreshData();
            }            
            else if (index > -1)
            {
                EmergencyGridControlData data = dataSource[index];

                List<double> values = ApplicationUtil.ConvertIndicatorResultValuesFromString(
              result.ResultValue);

               

                data.Name = result.IndicatorName;
                data.IndicatorResultCode = result.IndicatorResultCode;
                data.IndicatorResultClassCode = result.IndicatorResultClassCode;
                data.RealValue = values[0].ToString();
                data.PercentValue = values[1].ToString();
               // data.MediaTime = values[2].ToString();
                FormUtil.InvokeRequired(this.gridControl1, delegate
                {
                    dataSource[index] = data;
                });
           
            }
            //else  if ((result.IndicatorCustomCode == "PND22") ||
            //             (result.IndicatorCustomCode == "DPN22")||
            //    (result.IndicatorCustomCode == "DPN23")||
            //    (result.IndicatorCustomCode == "PND23"))
            else if ((result.Description == ResourceLoader.GetString2("IndicatorPND22Name")) ||
                         (result.Description == ResourceLoader.GetString2("IndicatorPND23Name")))
            {
                FormUtil.InvokeRequired(this.gridControl1, delegate
                {
                    dataSource.Add(new EmergencyGridControlData(result));
                });
            }

            EmergencyIndicatorChartEventArgs eventArgs =
                new EmergencyIndicatorChartEventArgs();
            eventArgs.List = new ArrayList();
            eventArgs.List = dataSource;
            if (EmergencyIndicatorEvent != null)
            {
                EmergencyIndicatorEvent(this, eventArgs);
            }
        }

        private void gridView1_DragObjectOver(object sender, DragObjectOverEventArgs e)
        {
            GridColumn column = e.DragObject as GridColumn;
            if (column != null && e.DropInfo.Index < 0)
            {
                e.DropInfo.Valid = false;
            }

        }
        
         
    }

    public class EmergencyGridControlData: GridControlData
    {
        private string realValue;
        private string percentValue;
      
        private string name;
        private int indicatorResultCode;
        private int indicatorResultClassCode;
        private DateTime date;
        private string indicatorCustomCode;
        private List<EmergencyGridControlData> listResults;

        public EmergencyGridControlData(IndicatorResultValuesClientData indicatorResultValuesClientData): base(indicatorResultValuesClientData)
        {
            List<double> values =  ApplicationUtil.ConvertIndicatorResultValuesFromString(
                indicatorResultValuesClientData.ResultValue);

            realValue = values[0].ToString();
            percentValue = values[1].ToString();

            name = indicatorResultValuesClientData.Description;//IndicatorName;
            indicatorResultCode = indicatorResultValuesClientData.IndicatorResultCode;
            indicatorResultClassCode = indicatorResultValuesClientData.IndicatorResultClassCode;
            indicatorCustomCode = indicatorResultValuesClientData.IndicatorCustomCode; 
            date = indicatorResultValuesClientData.Date;
            
            
        }
        public string IndicatorCustomCode
        {
            get
            {
                return indicatorCustomCode;
            }

            set
            {
                indicatorCustomCode = value;
            }
        }
        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }
        public string RealValue
        {
            get
            {
                return realValue;
            }
            set 
            {
                realValue = value;
            }
        }
        public string PercentValue
        {
            get
            {
                return percentValue;
            }
            set
            {
                percentValue = value;
            }
        }
        //public string MediaTime
        //{
        //    get
        //    {
        //        return mediaTime;
        //    }
        //    set
        //    {
        //        mediaTime = value;
        //    }
        //}
        public List<EmergencyGridControlData> ListResults 
        {
            get { return listResults; }
            set { listResults = value; }
        }
        public int IndicatorResultCode
        {
            get
            {
                return indicatorResultCode;
            }

            set
            {
                indicatorResultCode = value;
            }
        }
        public int IndicatorResultClassCode
        {
            get
            {
                return indicatorResultClassCode;
            }
            set
            {
                indicatorResultClassCode = value;
            }
        }
        public DateTime Date
        {
            get
            {
                return date;
            }

            set
            {
                date = value;
            }
        }
        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is EmergencyGridControlData)
            {
                result = this.Name == ((EmergencyGridControlData)obj).Name;
            }
            return result;
        }
    }

   
    public class EmergencyIndicatorChartEventArgs
    {
        private IList list;
        public IList List
        {
            get
            {
                return list;
            }
            set
            {
                list = value;
            }
        }
    }


}
