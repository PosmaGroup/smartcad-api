﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Smartmatic.SmartCad.Service;
using System.IO;
using System.Collections;
using System.Threading;
using VncSharp;
using SmartCadCore.ClientData;
using SmartCadCore.Core;
using SmartCadCore.Common;

namespace SmartCadGuiCommon.Controls
{
    
    //Removed reference to dll C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\v3.0\UIAutomationProvider.dll
    public partial class RemoteControlForm : Form
    {
        private string textForm;
        private OperatorClientData oper;
        private object syncCommited = new object();

        public OperatorClientData Oper
        {
            get { return oper; }
            set { this.oper = value; }
        }


        public RemoteControlForm(OperatorClientData oper, string app)
        {
            this.oper = oper;
            InitializeComponent();
            LoadLanguage();

            remoteDesktop.GetPassword = new AuthenticateDelegate(GetPassword);
            Connect();
        }

        private void RemoteControlForm_Load(object sender, EventArgs e)
        {
            //selectedAppData.Message = "true";
            //ServerServiceClient.GetInstance().SendClientData(SelectedAppData);

           ((SupervisionForm)this.MdiParent).SupervisionCommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(RemoteControlForm_SupervisionCommittedChanges);


        }

        void RemoteControlForm_SupervisionCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    lock (syncCommited)
                    {
                        try
                        {
                            if (ServerServiceClient.GetInstance().OperatorClient == null)
                            { }
                        }
                        catch
                        { }
                        if (e.Objects[0] is OperatorClientData)
                        {
                            if (e.Action == CommittedDataAction.Update) 
                            {
                                OperatorClientData operIn = e.Objects[0] as OperatorClientData;
                                if (oper.Code == operIn.Code && ApplicationUtil.GetOperatorStatus(operIn, (this.ParentForm as SupervisionForm).SupervisedApplicationName).StatusName == "NotConnected") 
                                {
                                    MessageForm.Show(ResourceLoader.GetString2("El operador {0} se ha desconectado de la aplicacion", oper.FirstName + " " + oper.LastName), MessageFormType.Information);
                                }
                            }
                        }
                    }
                } 
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }  
        }

        private void RemoteControlForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //selectedAppData.Message = "false";
            //ServerServiceClient.GetInstance().SendClientData(SelectedAppData);

            Disconnect();

            ((SupervisionForm)this.MdiParent).remoteControlFormList.Remove(this);
            ((SupervisionForm)this.MdiParent).SupervisionCommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(RemoteControlForm_SupervisionCommittedChanges);
        }

        public void LoadLanguage()
        {
            this.RibbonPageMonitoring.Text = ResourceLoader.GetString("Monitoring");
            Text = ResourceLoader.GetString2("RemoteControlForm") + textForm;
            barButtonItemReConnect.Caption = ResourceLoader.GetString2("RemoteControlReconnect");
            barCheckItemScaledView.Caption = ResourceLoader.GetString2("RemoteControlScaleView");
            barCheckItemTakeControl.Caption = ResourceLoader.GetString2("RmoteControlTakeControl");
            this.ribbonPageGroupOperator.Text = ResourceLoader.GetString2("OperatorsActivities");
            this.xrDesignRibbonPageGroup3.Text = ResourceLoader.GetString2("OperatorsActivities");
            this.xrDesignRibbonPageGroup1.Text = ResourceLoader.GetString2("OperatorsActivities");
        }

        private void Connect()
        {
            string host = string.Empty;
            IList names = (IList)ServerServiceClient.GetInstance().SearchBasicObjects(
                        SmartCadSQL.GetCustomSQL(SmartCadHqls.GetComputerNameByUserLogin, oper.Login));
            if (names.Count > 0)
            {
                host = (string)names[0];
            }

            if (host != null)
            {
                try
                {
                    remoteDesktop.Connect(host, true, false);
                }
                catch (VncProtocolException vex)
                {
                    MessageForm.Show(ResourceLoader.GetString2("UnableToConnectToVNC"), MessageFormType.Error);
                    throw;
                }
                catch (Exception ex)
                {
                    MessageForm.Show(ResourceLoader.GetString2("UnableToConnectHostVNC", host), MessageFormType.Error);
                    throw;
                }
            }
        }

        private static string GetPassword()
        {
            return "";
        }

        private void ConnectionLost(object sender, EventArgs e)
        {
            barButtonItemReConnect.Enabled = true;
            barCheckItemTakeControl.Enabled = false;
            barCheckItemTakeControl.Checked = false;
            barCheckItemScaledView.Enabled = false;
            barCheckItemScaledView.Checked = false;
        }

        private void Disconnect()
        {
            if (remoteDesktop.IsConnected)
                remoteDesktop.Disconnect();
        }

        private void barCheckItemTakeControl_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (remoteDesktop.IsConnected)
                remoteDesktop.SetInputMode(!barCheckItemTakeControl.Checked);
        }

        private void barCheckItemScaledView_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (remoteDesktop.IsConnected)
                remoteDesktop.SetScalingMode(barCheckItemScaledView.Checked);
        }

        private void barButtonItemReConnect_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (remoteDesktop.IsConnected == false)
                Connect();
        }

        private void ConnectComplete(object sender, ConnectEventArgs e)
        {
            barButtonItemReConnect.Enabled = false;
            barCheckItemTakeControl.Enabled = true;
            barCheckItemScaledView.Enabled = true;
        }
    }
}
