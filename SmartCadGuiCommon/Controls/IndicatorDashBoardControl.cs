using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Layout;
using DevExpress.XtraGrid.Columns;
using System.Collections;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors.Controls;
using DevExpress.Utils;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Layout.ViewInfo;
using System.Globalization;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadCore.Core;
using DevExpress.XtraEditors;

namespace SmartCadGuiCommon.Controls
{
    public partial class IndicatorDashBoardControl : UserControl
    {
        public enum IndicatorType
        {
            CALLS,
            FIRSTLEVEL,
            DISPATCH
        }

        public enum DashBoardClassType
        {
            SYSTEM,
            DEPARTMENT,
            GROUP,
            OPERATOR
        }

        private const int MAX_COLUMNS = 3;
        private readonly double WIDTH_PER_COLUMN;
        private IList<IndicatorClassDashboardClientData> preferencesDashBoard;
        private bool isFirstLevel;
        private bool isGeneralSupervisor;
        private bool isConfigured;

        public bool IsFirstLevel
        {
            get
            {
                return isFirstLevel;
            }
            set
            {
                isFirstLevel = value;
                AdjustControlLayout();                
            }
        }

        public bool IsGeneralSupervisor
        {
            get
            {
                return isGeneralSupervisor;
            }
            set
            {
                isGeneralSupervisor = value;
            }
        }

        public IndicatorDashBoardControl()
        {
            InitializeComponent();
            WIDTH_PER_COLUMN = Math.Truncate((double)this.Size.Width / (double) MAX_COLUMNS);
            preferencesDashBoard = new List<IndicatorClassDashboardClientData>();
            isConfigured = false;
            gridControlOne.DataSource = new BindingList<IndicatorDashBoardData>();
            gridControlTwo.DataSource = new BindingList<IndicatorDashBoardData>();
            gridControlThree.DataSource = new BindingList<IndicatorDashBoardData>();
            repositoryItemImageComboBoxOne.Items.Clear();
            repositoryItemImageComboBoxTwo.Items.Clear();
            repositoryItemImageComboBoxThree.Items.Clear();
            groupBoxOne.Visible = false;
            groupBoxTwo.Visible = false;
            groupBoxThree.Visible = false;
            LoadLanguage();
        }

        public void LoadLanguage()
        {
            TitleFirstGroup = ResourceLoader.GetString2("Calls");
            TitleSecondGroup = ResourceLoader.GetString2("FirstLevel");
            TitleThirdGroup = ResourceLoader.GetString2("Dispatch");
        }

        [Browsable(false)]
        internal IList []DataSource
        {
            get
            {
                return new ArrayList[3];
            }
            set
            {
                if (isConfigured == true)
                {
                    if (value != null && value.Length == MAX_COLUMNS &&
                        value[0] != null && value[1]!= null && value[2] != null)
                    {
                        InitData(value);
                    }
                }
            }
        }

        private DashBoardClassType dashBoardType = DashBoardClassType.SYSTEM;

        [Description("Gets or sets the class of indicator result will be displayed."),
            Category("Behavior"), Browsable(true)]
        public DashBoardClassType DashBoardType
        {
            get
            {
                return dashBoardType;
            }
            set
            {
                dashBoardType = value;
            }
        }

        [Description("Gets or sets the title of the first group of indicators."),
            Category("Behavior"), Browsable(true)]
        public string TitleFirstGroup
        {
            get { return this.groupBoxOne.Text; }
            set { this.groupBoxOne.Text = value; }
        }

        [Description("Gets or sets the title of the second group of indicators."),
            Category("Behavior"), Browsable(true)]
        public string TitleSecondGroup
        {
            get { return this.groupBoxTwo.Text; }
            set { this.groupBoxTwo.Text = value; }
        }

        [Description("Gets or sets the title of the third group of indicators."),
            Category("Behavior"), Browsable(true)]
        public string TitleThirdGroup
        {
            get { return this.groupBoxThree.Text; }
            set { this.groupBoxThree.Text = value; }
        }

        private void InitData(IList []dataSource)
        {
            BindingList<IndicatorDashBoardData> newDatasourceOne = new BindingList<IndicatorDashBoardData>();
            IndicatorDashBoardData dashBoardData = null;
            int index = -1;
            foreach (IndicatorResultClientData result in dataSource[0])
            {
                dashBoardData = new IndicatorDashBoardData(result);
                index = newDatasourceOne.IndexOf(dashBoardData);
                if (index == -1)
                {
                    newDatasourceOne.Add(dashBoardData);
                }
                else
                {
                    newDatasourceOne[index] = dashBoardData;
                }
            }
            BindingList<IndicatorDashBoardData> newDatasourceTwo = new BindingList<IndicatorDashBoardData>();
            foreach (IndicatorResultClientData result in dataSource[1])
            {
                dashBoardData = new IndicatorDashBoardData(result);
                index = newDatasourceTwo.IndexOf(dashBoardData);
                if (index == -1)
                {
                    newDatasourceTwo.Add(dashBoardData);
                }
                else
                {
                    newDatasourceTwo[index] = dashBoardData;
                }
            }
            BindingList<IndicatorDashBoardData> newDatasourceThree = new BindingList<IndicatorDashBoardData>();
            foreach (IndicatorResultClientData result in dataSource[2])
            {
                dashBoardData = new IndicatorDashBoardData(result);
                index = newDatasourceThree.IndexOf(dashBoardData);
                if (index == -1)
                {
                    newDatasourceThree.Add(dashBoardData);
                }
                else
                {
                    newDatasourceThree[index] = dashBoardData;
                }
            }
            FillRepositoryComboBox(repositoryItemImageComboBoxOne, newDatasourceOne);
 
            FillRepositoryComboBox(repositoryItemImageComboBoxTwo, newDatasourceTwo);
            FillRepositoryComboBox(repositoryItemImageComboBoxThree, newDatasourceThree);
            gridControlOne.DataSource = newDatasourceOne;
            gridControlTwo.DataSource = newDatasourceTwo;
            gridControlThree.DataSource = newDatasourceThree;            
        }

        public void ClearData()
        {
            
            DeleteRepositoryItemComboBox(repositoryItemImageComboBoxOne, new IndicatorDashBoardData());
            DeleteRepositoryItemComboBox(repositoryItemImageComboBoxTwo, new IndicatorDashBoardData());
            DeleteRepositoryItemComboBox(repositoryItemImageComboBoxThree, new IndicatorDashBoardData());
            gridControlOne.DataSource = new BindingList<IndicatorDashBoardData>();
            gridControlTwo.DataSource = new BindingList<IndicatorDashBoardData>();
            gridControlThree.DataSource = new BindingList<IndicatorDashBoardData>();

        }

        private void AddOrUpdateRepositoryItemComboBox(RepositoryItemImageComboBox repositoryCombo,
            IndicatorDashBoardData data)
        {
            ImageComboBoxItem item = CreateRepositoryItem(repositoryCombo, data);
            ImageComboBoxItem existentItem = repositoryCombo.Items.GetItem(item.Value);
            try
            {
                repositoryCombo.BeginUpdate();
                if (existentItem != null)
                {
                    existentItem.Description = item.Description;
                    existentItem.ImageIndex = item.ImageIndex;
                }
                else
                {
                    repositoryCombo.Items.Add(item);
                }
            }
            finally
            {
                repositoryCombo.EndUpdate();
            }            
        }

        private void DeleteRepositoryItemComboBox(RepositoryItemImageComboBox repositoryCombo,
            IndicatorDashBoardData data)
        {
            ImageComboBoxItem item = CreateRepositoryItem(repositoryCombo, data);
            item = repositoryCombo.Items.GetItem(item.Value);
            if (item != null)
            {
                int index = repositoryCombo.Items.IndexOf(item);
                if (index != -1)
                {
                    repositoryCombo.Items.RemoveAt(index);
                }
            }
        }

        private void FillRepositoryComboBox(RepositoryItemImageComboBox repositoryCombo, BindingList<IndicatorDashBoardData> newDatasource)
        {
            repositoryCombo.Items.Clear();
            foreach (IndicatorDashBoardData data in newDatasource)
            {
                repositoryCombo.Items.Add(CreateRepositoryItem(repositoryCombo, data));
            }
        }

        private ImageComboBoxItem CreateRepositoryItem(RepositoryItemImageComboBox repositoryCombo, IndicatorDashBoardData data)
        {
            //string space = " ";
            double resultValue = 0;
            string description = null;
            string value = null;
            int imageIndex = 0; 
            try
            {
                resultValue = Convert.ToDouble(data.IndicatorResultValue);                
            }
            catch
            { 
            }
            description = resultValue.ToString();
            if (string.IsNullOrEmpty(data.MeasureUnit) == false && data.MeasureUnit.Contains("seg") == true)
            {
                description = ApplicationUtil.GetTimeSpanCustomString(TimeSpan.FromSeconds(resultValue));
                //description = TimeSpan.FromSeconds(Convert.ToInt32(resultValue)).ToString();
            }
            else if (string.IsNullOrEmpty(data.MeasureUnit) == false && data.MeasureUnit.Contains("%") == true)
            {
                description = resultValue.ToString("P", CultureInfo.InvariantCulture);
            }
            else
            {
                description += data.MeasureUnit;
            }
            value = data.CustomCode;
            
            if (data.Threshold == 1) // green
            {
                imageIndex = 1;
            }
            else if (data.Threshold == 2) //yellow
            {
                imageIndex = 2;
            }
            else if (data.Threshold == 3) // red
            {
                imageIndex = 3;
            }
            else // undefined
            {
                imageIndex = 0;
            }
            ImageComboBoxItem item = new ImageComboBoxItem(description, value, imageIndex);
            return item;
        }

        private void DeleteResultItemByPreference(IndicatorClassDashboardClientData preference)
        {
            if (preference.ClassName == this.DashBoardType.ToString())
            {
                int resultIndex = -1;
                IndicatorDashBoardData dashBoardData = new IndicatorDashBoardData();
                dashBoardData.CustomCode = preference.IndicatorCustomCode;

                GridControl selectedGridControl = null;
                RepositoryItemImageComboBox repository = null;
                if (preference.IndicatorTypeName == IndicatorType.CALLS.ToString())
                {
                    selectedGridControl = gridControlOne;
                    repository = repositoryItemImageComboBoxOne;
                }
                else if (preference.IndicatorTypeName == IndicatorType.FIRSTLEVEL.ToString())
                {
                    selectedGridControl = gridControlTwo;
                    repository = repositoryItemImageComboBoxTwo;
                }
                else if (preference.IndicatorTypeName == IndicatorType.DISPATCH.ToString())
                {
                    selectedGridControl = gridControlThree;
                    repository = repositoryItemImageComboBoxThree;
                }

                if (selectedGridControl != null)
                {
                    resultIndex = (selectedGridControl.DataSource as BindingList<IndicatorDashBoardData>).IndexOf(dashBoardData);
                    if (resultIndex != -1)
                    {
                        dashBoardData = (IndicatorDashBoardData)(selectedGridControl.DataSource as BindingList<IndicatorDashBoardData>)[resultIndex];
                        DeleteRepositoryItemComboBox(repository, dashBoardData);
                        (selectedGridControl.DataSource as BindingList<IndicatorDashBoardData>).RemoveAt(resultIndex);                                                    
                    }
                }
            }
        }

        /// <summary>
        /// Add or update a value of an indicator that has to be shown in the control.
        /// </summary>
        /// <param name="indicatorResult">Indicator result.</param>
        /// <param name="indicatorType">Indicator Type.</param>
        public void AddOrUpdateItem(IndicatorResultClientData indicatorResult, IndicatorType indicatorType)
        {
            AddOrUpdateItem(indicatorResult, indicatorType, false);
        }

        /// <summary>
        /// Add or update a value of an indicator that has to be shown in the control.
        /// </summary>
        /// <param name="indicatorResult">Indicator result.</param>
        /// <param name="indicatorType">Indicator Type.</param>
        /// <param name="showIndicator">Indicate if the force the result to be added.</param>
        public void AddOrUpdateItem(IndicatorResultClientData indicatorResult, IndicatorType indicatorType, bool showIndicator)
        {
            if (isConfigured)
            {
                IndicatorDashBoardData dashBoardData = new IndicatorDashBoardData(indicatorResult);
                if (ThereIsPreferenceForIndicatorResult(dashBoardData, indicatorType) == true || showIndicator == true)
                {
                    GridControl selectedGridControl = null;
                    RepositoryItemImageComboBox repository = null;
                    //LayoutView selectedLayout = null;
                    if (indicatorType == IndicatorType.CALLS)
                    {
                        selectedGridControl = gridControlOne;
                        repository = repositoryItemImageComboBoxOne;
                    }
                    else if (indicatorType == IndicatorType.FIRSTLEVEL)
                    {
                        selectedGridControl = gridControlTwo;
                        repository = repositoryItemImageComboBoxTwo;
                    }
                    else if (indicatorType == IndicatorType.DISPATCH)
                    {
                        selectedGridControl = gridControlThree;
                        repository = repositoryItemImageComboBoxThree;
                    }
                    
                    AddOrUpdateRepositoryItemComboBox(repository, dashBoardData);
                    BindingList<IndicatorDashBoardData> oldDatasource = selectedGridControl.DataSource as BindingList<IndicatorDashBoardData>;
                    int dataSourceIndex = oldDatasource.IndexOf(dashBoardData);
                    FormUtil.InvokeRequired(this, delegate
                    {
                        if (dataSourceIndex == -1)
                        {
                            oldDatasource.Add(dashBoardData);
                        }
                        else
                        {
                            oldDatasource[dataSourceIndex] = dashBoardData;
                        }
                    });
                }
            }
        }

        private bool ThereIsPreferenceForIndicatorResult(IndicatorDashBoardData dashboardData, IndicatorType indicatorType)
        {
            bool result = false;
            IndicatorClassDashboardClientData preference = new IndicatorClassDashboardClientData();
            preference.IndicatorCustomCode = dashboardData.CustomCode;
            preference.ClassName = dashboardData.ClassName;
            int index = preferencesDashBoard.IndexOf(preference);
            if (index != -1)                
            {
                preference = preferencesDashBoard[index];
                if (preference.IndicatorTypeName == indicatorType.ToString())
                {
                    result = preferencesDashBoard[index].ShowDashboard;
                }
            }
            return result;
        }

        public void AddOrUpdatePreference(IndicatorClassDashboardClientData preference)
        {
            if (preference.ClassName == this.DashBoardType.ToString())
            {
                int index = preferencesDashBoard.IndexOf(preference);
                if (index != -1)
                {
                    preferencesDashBoard[index] = preference;
                }
                else
                {
                    preferencesDashBoard.Add(preference);
                }
                if (preference.ShowDashboard == false)
                {
                    DeleteResultItemByPreference(preference);
                }
            }
            AdjustControlLayout();
        }

        /// <summary>
        /// Set the list of indicators preferences to be shown.
        /// </summary>
        /// <param name="preferenceList">List of preferences to be shown.</param>
        public void SetPreferences(IList preferenceList)
        {
            SetPreferences(preferenceList, false);
        }

        /// <summary>
        /// Set the list of indicators preferences to be shown.
        /// </summary>
        /// <param name="preferencesList">List of preferences to be shown.</param>
        /// <param name="showPreferences">Indicates is the preference are to be shown, even if they are not visible.</param>
        public void SetPreferences(IList preferencesList, bool showPreferences)
        {
            preferencesDashBoard.Clear();
            if (gridControlOne.DataSource as BindingList<IndicatorDashBoardData> != null)
            {
                (gridControlOne.DataSource as BindingList<IndicatorDashBoardData>).Clear();
            }
            if (gridControlTwo.DataSource as BindingList<IndicatorDashBoardData> != null)
            {
                (gridControlTwo.DataSource as BindingList<IndicatorDashBoardData>).Clear();
            }
            if (gridControlThree.DataSource as BindingList<IndicatorDashBoardData> != null)
            {
                (gridControlThree.DataSource as BindingList<IndicatorDashBoardData>).Clear();
            }
            int index = -1;
            foreach (IndicatorClassDashboardClientData preference in preferencesList)
            {
                if (preference.ClassName == this.DashBoardType.ToString())
                {
                    index = preferencesDashBoard.IndexOf(preference);
                    if (index != -1)
                    {
                        preferencesDashBoard[index] = preference;
                    }
                    else
                    {
                        preferencesDashBoard.Add(preference);
                    }
                }
            }
            FormUtil.InvokeRequired(this,
                delegate
                {
                    AdjustControlLayout(showPreferences);
                });
        }

        /// <summary>
        /// Adjust the control to the columns needed to be drawn.
        /// </summary>
        private void AdjustControlLayout()
        {
            AdjustControlLayout(false);
        }

        /// <summary>
        /// Adjust the control to the columns needed to be drawn.
        /// </summary>
        /// <param name="showPreferences">Indicates is the preference are to be shown, even if they are not visible.</param>
        private void AdjustControlLayout(bool showPreferences)
        {
            int neededColumnsGroupOne = GetNeededColumns(0, showPreferences);
            int neededColumnsGroupTwo = GetNeededColumns(1, showPreferences);
            int neededColumnsGroupThree = GetNeededColumns(2, showPreferences);
            int usedColumns = 0;
            if ((neededColumnsGroupOne + usedColumns) <= MAX_COLUMNS)
            {
                usedColumns += neededColumnsGroupOne;
            }
            else
            {
                neededColumnsGroupOne = 0;
            }
            if ((neededColumnsGroupTwo + usedColumns) <= MAX_COLUMNS)
            {
                usedColumns += neededColumnsGroupTwo;
            }
            else
            {
                neededColumnsGroupTwo = 0;
            }
            if ((neededColumnsGroupThree + usedColumns) <= MAX_COLUMNS)
            {
                usedColumns += neededColumnsGroupThree;
            }
            else
            {
                neededColumnsGroupThree = 0;
            }
            AdjustDimensionsInControl(neededColumnsGroupOne, neededColumnsGroupTwo, neededColumnsGroupThree);
            if (isConfigured == false && preferencesDashBoard.Count > 0)
            {
                isConfigured = true;
            }
        }

        private void AdjustDimensionsInControl(int neededColumnsGroupOne, int neededColumnsGroupTwo, int neededColumnsGroupThree)
        {
            /* Por categoria, en base al numero de columnas requeridas, voy disminuyendo la cantidad de columnas 
             * disponibles para las otras categorias
             */
            if (neededColumnsGroupOne > 0)
                neededColumnsGroupOne = 2;

            Point initialLocation = new Point(0, 0);
            double categoryWidth = neededColumnsGroupOne * WIDTH_PER_COLUMN;

            Point groupBoxOneLocation = initialLocation;
            Size groupBoxOneSize = new Size((int)categoryWidth, this.Size.Height - 2);
            
            initialLocation = new Point(initialLocation.X + (int)categoryWidth, 0);
            categoryWidth = neededColumnsGroupTwo * WIDTH_PER_COLUMN;

            Point groupBoxTwoLocation = initialLocation;
            Size groupBoxTwoSize = new Size((int)categoryWidth, this.Size.Height - 2);

            initialLocation = new Point(initialLocation.X + (int)categoryWidth, 0);
            categoryWidth = neededColumnsGroupThree * WIDTH_PER_COLUMN;

            Point groupBoxThreeLocation = initialLocation;
            Size groupBoxThreeSize = new Size((int)categoryWidth, this.Size.Height - 2);
            int usedColumns = neededColumnsGroupOne + neededColumnsGroupTwo + neededColumnsGroupThree;
            if (usedColumns < MAX_COLUMNS)
            {
                if (neededColumnsGroupThree > 0)
                {
                    categoryWidth = ((MAX_COLUMNS - usedColumns) + neededColumnsGroupThree) * WIDTH_PER_COLUMN;
                    groupBoxThreeSize = new Size((int)categoryWidth, this.Size.Height - 2);
                }
                else if (neededColumnsGroupTwo > 0)
                {
                    categoryWidth = ((MAX_COLUMNS - usedColumns) + neededColumnsGroupTwo) * WIDTH_PER_COLUMN;
                    groupBoxTwoSize = new Size((int)categoryWidth, this.Size.Height - 2);
                }
                else if (neededColumnsGroupOne > 0)
                {
                    categoryWidth = ((MAX_COLUMNS - usedColumns) + neededColumnsGroupOne) * WIDTH_PER_COLUMN;
                    groupBoxOneSize= new Size((int)categoryWidth, this.Size.Height - 2);
                }

            }            

            if (neededColumnsGroupOne > 0)
            {
                XtraScrollableControl x = new XtraScrollableControl();

                //DevExpress.XtraEditors.HScrollBar hbar = new DevExpress.XtraEditors.HScrollBar();
                //hbar.Width = 100;
                //hbar.Top = 20;
                //hbar.Left = 20;
                //Controls.Add(hbar);

                this.groupBoxOne.Visible = true;
                this.gridControlOne.Visible = true;
                this.groupBoxOne.Size = groupBoxOneSize;
                //this.groupBoxOne.ScrollControlIntoView = this.ScrollToControl(this);
                //hbar.Dock;
                x.Dock = DockStyle.Fill;
               
                this.AdjustFormScrollbars(true);
                this.groupBoxOne.Location = groupBoxOneLocation;
                
                this.gridControlOne.Location = new Point(3, 21);
                this.gridControlOne.Size = new Size(this.groupBoxOne.Size.Width - 6, this.groupBoxOne.Size.Height - 24);
            }
            else
            {
                this.groupBoxOne.Visible = false;
                this.gridControlOne.Visible = false;
            }
            if (neededColumnsGroupTwo > 0)
            {
                this.groupBoxTwo.Visible = true;
                this.gridControlTwo.Visible = true;
                this.groupBoxTwo.Size = groupBoxTwoSize;
                this.groupBoxTwo.Location = groupBoxTwoLocation;
                this.gridControlTwo.Location = new Point(3, 21);
                this.gridControlTwo.Size = new Size(this.groupBoxTwo.Size.Width - 6, this.groupBoxTwo.Size.Height - 24);
            }
            else
            {
                this.groupBoxTwo.Visible = false;
                this.gridControlTwo.Visible = false;
            }
            if (neededColumnsGroupThree > 0)
            {
                this.groupBoxThree.Visible = true;
                this.gridControlThree.Visible = true;
                this.groupBoxThree.Size = groupBoxThreeSize;
                this.groupBoxThree.Location = groupBoxThreeLocation;
                this.gridControlThree.Location = new Point(3, 21);
                this.gridControlThree.Size = new Size(this.groupBoxThree.Size.Width - 6, this.groupBoxThree.Size.Height - 24);
            }
            else
            {
                this.groupBoxThree.Visible = false;
                this.gridControlThree.Visible = false;
            }            
        }

        /// <summary>
        /// Calculate how many calling need to be shown.
        /// </summary>
        /// <param name="columnIndex">Column index</param>
        /// <returns>The amount of needed columns per indicator class.</returns>
        private int GetNeededColumns(int columnIndex)
        {
            return GetNeededColumns(columnIndex, false);
        }

        /// <summary>
        /// Calculate how many calling need to be shown.
        /// The parameter showPreference work only in the System Class Type.
        /// </summary>
        /// <param name="columnIndex">Column index.</param>
        /// <param name="showPreference">Indicate if it force to show the preference.</param>
        /// <returns>The amount of needed columns per indicator class.</returns>
        private int GetNeededColumns(int columnIndex, bool showPreference)
        {
            int result = 0;
            
            if (this.DashBoardType == DashBoardClassType.SYSTEM)
            {
                if (columnIndex == 0) 
                {
                    foreach (IndicatorClassDashboardClientData preference in preferencesDashBoard)
                    {
                        if ((preference.IndicatorTypeName == IndicatorType.CALLS.ToString() 
                            && preference.ShowDashboard) || (preference.IndicatorTypeName == IndicatorType.CALLS.ToString() && showPreference))
                        {
                            result++;
                        }
                    }
                }
                else if (columnIndex == 1)
                {
                    foreach (IndicatorClassDashboardClientData preference in preferencesDashBoard)
                    {
                        if ((preference.IndicatorTypeName == IndicatorType.FIRSTLEVEL.ToString()
                            && preference.ShowDashboard) || (preference.IndicatorTypeName == IndicatorType.FIRSTLEVEL.ToString() && showPreference))
                        {
                            result++;
                        }
                    }
                }
                else if (columnIndex == 2)
                {
                    foreach (IndicatorClassDashboardClientData preference in preferencesDashBoard)
                    {
                        if ((preference.IndicatorTypeName == IndicatorType.DISPATCH.ToString()
                            && preference.ShowDashboard) || (preference.IndicatorTypeName == IndicatorType.DISPATCH.ToString() && showPreference))
                        {
                            result++;
                        }
                    }
                }
            }
            else if (this.DashBoardType == DashBoardClassType.DEPARTMENT)
            {
                if (columnIndex == 2 && !IsFirstLevel)
                {
                    foreach (IndicatorClassDashboardClientData preference in preferencesDashBoard)
                    {
                        if (preference.IndicatorTypeName == IndicatorType.DISPATCH.ToString()
                            && preference.ShowDashboard)
                        {
                            result++;
                        }
                    }
                }
            }
            else if (this.DashBoardType == DashBoardClassType.GROUP && !IsGeneralSupervisor)
            {
                if (columnIndex == 0 && IsFirstLevel)
                {
                    foreach (IndicatorClassDashboardClientData preference in preferencesDashBoard)
                    {
                        if (preference.IndicatorTypeName == IndicatorType.CALLS.ToString()
                            && preference.ShowDashboard)
                        {
                            result++;
                        }
                    }
                }
                else if (columnIndex == 1 && IsFirstLevel)
                {
                    foreach (IndicatorClassDashboardClientData preference in preferencesDashBoard)
                    {
                        if (preference.IndicatorTypeName == IndicatorType.FIRSTLEVEL.ToString()
                            && preference.ShowDashboard)
                        {
                            result++;
                        }
                    }
                }
                else if (columnIndex == 2 && !IsFirstLevel)
                {
                    foreach (IndicatorClassDashboardClientData preference in preferencesDashBoard)
                    {
                        if (preference.IndicatorTypeName == IndicatorType.DISPATCH.ToString()
                            && preference.ShowDashboard)
                        {
                            result++;
                        }
                    }
                }
            }
            else if (this.DashBoardType == DashBoardClassType.OPERATOR)
            {
                if (columnIndex == 0 && IsFirstLevel)  
                {
                    foreach (IndicatorClassDashboardClientData preference in preferencesDashBoard)
                    {
                        if (preference.IndicatorTypeName == IndicatorType.CALLS.ToString()
                            && preference.ShowDashboard)
                        {
                            result++;
                        }
                    }
                }
                else if (columnIndex == 1 && IsFirstLevel)
                {
                    foreach (IndicatorClassDashboardClientData preference in preferencesDashBoard)
                    {
                        if (preference.IndicatorTypeName == IndicatorType.FIRSTLEVEL.ToString()
                            && preference.ShowDashboard)
                        {
                            result++;
                        }
                    }
                }
                else if (columnIndex == 2 && !IsFirstLevel)
                {
                    foreach (IndicatorClassDashboardClientData preference in preferencesDashBoard)
                    {
                        if (preference.IndicatorTypeName == IndicatorType.DISPATCH.ToString()
                            && preference.ShowDashboard)
                        {
                            result++;
                        }
                    }
                }
            }
            int neededColumns = 0;
            if (result >= 1 && result <= 6)
            {
                neededColumns = 1;
            }
            else if (result >= 7 && result <= 12)
            {
                neededColumns = 2;
            }
            else if (result >= 13 && result <= 18)
            {
                neededColumns = 3;
            }
            return neededColumns;
        }
        
        private string GetCellHintText(LayoutView view, int rowHandle, DevExpress.XtraGrid.Columns.GridColumn column)
        {
            return view.GetRowCellDisplayText(rowHandle, column);
        }

        private void toolTipController_GetActiveObjectInfo(object sender, DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventArgs e)
        {
            GridControl selectedGrid = e.SelectedControl as GridControl;
            if (selectedGrid != null)
            {
                ToolTipControlInfo info = null;
                try
                {
                    LayoutView view = selectedGrid.GetViewAt(e.ControlMousePosition) as LayoutView;
                    if (view != null)
                    {
                        LayoutViewHitInfo hi = view.CalcHitInfo(e.ControlMousePosition);
                        if (hi.InField && hi.Column != null && (hi.Column == layoutViewColumn1 || hi.Column == layoutViewColumn3 || hi.Column == layoutViewColumn5))
                        {
                            IndicatorDashBoardData data = view.GetRow(hi.RowHandle) as IndicatorDashBoardData;
                            if (data != null)
                            {
                                string textToDisplay = data.Description;
                                info = new ToolTipControlInfo(new CellToolTipInfo(hi.RowHandle, hi.Column, "cell"), textToDisplay);
                            }
                        }                        
                    }
                }
                finally
                {
                    e.Info = info;
                }
            }
        }

        
    }

    internal class IndicatorDashBoardData
    {
        private int code;

        public int Code
        {
            get { return code; }
            set { code = value; }
        }

        private string customCode;

        public string CustomCode
        {
            get { return customCode; }
            set { customCode = value; }
        }

        private string indicatorFriendlyName;

        public string IndicatorFriendlyName
        {
            get { return indicatorFriendlyName; }
            set { indicatorFriendlyName = value; }
        }

        private string indicatorResultValue;

        public string IndicatorResultValue
        {
            get { return indicatorResultValue; }
            set { indicatorResultValue = value; }
        }

        private int threshold;

        public int Threshold
        {
            get { return threshold; }
            set { threshold = value; }
        }

        private string measureUnit;

        public string MeasureUnit
        {
            get { return measureUnit; }
            set { measureUnit = value; }
        }

        private string indicatorName;

        public string IndicatorName
        {
            get { return indicatorName; }
            set { indicatorName = value; }
        }

        private int trend;
        public int Trend
        {
            get { return trend; }
            set { trend = value; }
        }

        private string className;

        public string ClassName
        {
            get { return className; }
            set { className = value; }
        }

        private string description;

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public IndicatorDashBoardData()
        { }

        public IndicatorDashBoardData(IndicatorResultClientData indicatorResult)
        {
            this.Code = indicatorResult.Code;
            this.CustomCode = indicatorResult.IndicatorCustomCode;
            this.IndicatorName = indicatorResult.IndicatorName;
            this.MeasureUnit = indicatorResult.IndicatorMeasureUnit;
            this.IndicatorFriendlyName = string.IsNullOrEmpty(indicatorResult.IndicatorMnemonic) ? indicatorResult.IndicatorCustomCode : indicatorResult.IndicatorMnemonic;
            this.Description = this.IndicatorFriendlyName + " - " + indicatorResult.IndicatorName;
            if (indicatorResult.Values != null)
            {
                IndicatorResultValuesClientData resultClient = indicatorResult.Values[0] as IndicatorResultValuesClientData;
                this.Threshold = resultClient.Threshold;
                this.IndicatorResultValue = resultClient.ResultValue;
                this.Trend = resultClient.Trend;
                this.ClassName = resultClient.IndicatorClassName;
            }            
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            IndicatorDashBoardData objectClientData = obj as IndicatorDashBoardData;
            if (objectClientData != null)
            {
                if (this.CustomCode == objectClientData.CustomCode)
                    result = true;
            }
            return result;
        }

        public override int GetHashCode()
        {
            return this.GetType().GetHashCode() * 100000 + this.CustomCode.GetHashCode() + base.GetHashCode();
        }
    }
}
