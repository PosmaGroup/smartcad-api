﻿using System.Windows.Forms;

namespace SmartCadGuiCommon.Controls
{
    partial class RemoteControlForm: Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RemoteControlForm));
            this.ribbonPageGroupOperator = new DevExpress.XtraReports.UserDesigner.XRDesignRibbonPageGroup();
            this.barSubItemOperatorChartControl = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItemOperatorPerformance = new DevExpress.XtraBars.BarButtonItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.remoteDesktop = new VncSharp.RemoteDesktop();
            this.RibbonControl = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.barButtonItemSave = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemRefreshGraphic = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItemSelectRoom = new DevExpress.XtraBars.BarEditItem();
            this.BarButtonItemSendMonitor = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItemIndicatorSelection = new DevExpress.XtraBars.BarEditItem();
            this.barCheckItemUpTreshold = new DevExpress.XtraBars.BarCheckItem();
            this.barCheckItemDownTreshold = new DevExpress.XtraBars.BarCheckItem();
            this.barCheckItemTrend = new DevExpress.XtraBars.BarCheckItem();
            this.barCheckItem4 = new DevExpress.XtraBars.BarCheckItem();
            this.barCheckItemTakeControl = new DevExpress.XtraBars.BarCheckItem();
            this.barCheckItemScaledView = new DevExpress.XtraBars.BarCheckItem();
            this.barButtonItemReConnect = new DevExpress.XtraBars.BarButtonItem();
            this.RibbonPageMonitoring = new DevExpress.XtraReports.UserDesigner.XRDesignRibbonPage();
            this.xrDesignRibbonPageGroup3 = new DevExpress.XtraReports.UserDesigner.XRDesignRibbonPageGroup();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.xrDesignRibbonPageGroup2 = new DevExpress.XtraReports.UserDesigner.XRDesignRibbonPageGroup();
            this.xrDesignRibbonPageGroup1 = new DevExpress.XtraReports.UserDesigner.XRDesignRibbonPageGroup();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ribbonPageGroupOperator
            // 
            this.ribbonPageGroupOperator.AllowMinimize = false;
            this.ribbonPageGroupOperator.AllowTextClipping = false;
            this.ribbonPageGroupOperator.Kind = DevExpress.XtraReports.UserDesigner.XRDesignRibbonPageGroupKind.SizeAndLayout;
            this.ribbonPageGroupOperator.Name = "ribbonPageGroupOperator";
            this.ribbonPageGroupOperator.ShowCaptionButton = false;
            this.ribbonPageGroupOperator.Text = "Operador";
            // 
            // barSubItemOperatorChartControl
            // 
            this.barSubItemOperatorChartControl.Caption = "Configurar Grfica";
            this.barSubItemOperatorChartControl.Enabled = false;
            this.barSubItemOperatorChartControl.Glyph = ((System.Drawing.Image)(resources.GetObject("barSubItemOperatorChartControl.Glyph")));
            this.barSubItemOperatorChartControl.Id = 262;
            this.barSubItemOperatorChartControl.Name = "barSubItemOperatorChartControl";
            this.barSubItemOperatorChartControl.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barButtonItemOperatorPerformance
            // 
            this.barButtonItemOperatorPerformance.Caption = "Ver desempeo";
            this.barButtonItemOperatorPerformance.Id = 259;
            this.barButtonItemOperatorPerformance.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D));
            this.barButtonItemOperatorPerformance.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemOperatorPerformance.LargeGlyph")));
            this.barButtonItemOperatorPerformance.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemOperatorPerformance.Name = "barButtonItemOperatorPerformance";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(10, 10);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1297, 787);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            // 
            // remoteDesktop
            // 
            this.remoteDesktop.AutoScroll = true;
            this.remoteDesktop.AutoScrollMinSize = new System.Drawing.Size(608, 427);
            this.remoteDesktop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.remoteDesktop.Location = new System.Drawing.Point(10, 10);
            this.remoteDesktop.Name = "remoteDesktop";
            this.remoteDesktop.Size = new System.Drawing.Size(1297, 787);
            this.remoteDesktop.TabIndex = 2;
            this.remoteDesktop.ConnectionLost += new System.EventHandler(this.ConnectionLost);
            this.remoteDesktop.ConnectComplete += new VncSharp.ConnectCompleteHandler(this.ConnectComplete);
            // 
            // RibbonControl
            // 
            this.RibbonControl.ApplicationIcon = null;
            this.RibbonControl.Dock = System.Windows.Forms.DockStyle.None;
            this.RibbonControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RibbonControl.Images = this.imageList;
            this.RibbonControl.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItemSave,
            this.barButtonItemPrint,
            this.barButtonItemRefreshGraphic,
            this.barEditItemSelectRoom,
            this.BarButtonItemSendMonitor,
            this.barEditItemIndicatorSelection,
            this.barCheckItemUpTreshold,
            this.barCheckItemDownTreshold,
            this.barCheckItemTrend,
            this.barCheckItem4,
            this.barCheckItemTakeControl,
            this.barCheckItemScaledView,
            this.barButtonItemReConnect});
            this.RibbonControl.ItemsVertAlign = DevExpress.Utils.VertAlignment.Top;
            this.RibbonControl.Location = new System.Drawing.Point(201, 0);
            this.RibbonControl.MaxItemId = 284;
            this.RibbonControl.MdiMergeStyle = DevExpress.XtraBars.Ribbon.RibbonMdiMergeStyle.Always;
            this.RibbonControl.Name = "RibbonControl";
            this.RibbonControl.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.RibbonPageMonitoring});
            this.RibbonControl.SelectedPage = this.RibbonPageMonitoring;
            this.RibbonControl.ShowToolbarCustomizeItem = false;
            this.RibbonControl.Size = new System.Drawing.Size(168, 118);
            this.RibbonControl.Toolbar.ShowCustomizeItem = false;
            this.RibbonControl.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            this.RibbonControl.TransparentEditors = true;
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "logout.gif");
            // 
            // barButtonItemSave
            // 
            this.barButtonItemSave.Id = 274;
            this.barButtonItemSave.Name = "barButtonItemSave";
            // 
            // barButtonItemPrint
            // 
            this.barButtonItemPrint.Id = 275;
            this.barButtonItemPrint.Name = "barButtonItemPrint";
            // 
            // barButtonItemRefreshGraphic
            // 
            this.barButtonItemRefreshGraphic.Id = 276;
            this.barButtonItemRefreshGraphic.Name = "barButtonItemRefreshGraphic";
            // 
            // barEditItemSelectRoom
            // 
            this.barEditItemSelectRoom.Edit = null;
            this.barEditItemSelectRoom.Id = 277;
            this.barEditItemSelectRoom.Name = "barEditItemSelectRoom";
            // 
            // BarButtonItemSendMonitor
            // 
            this.BarButtonItemSendMonitor.Id = 278;
            this.BarButtonItemSendMonitor.Name = "BarButtonItemSendMonitor";
            // 
            // barEditItemIndicatorSelection
            // 
            this.barEditItemIndicatorSelection.Edit = null;
            this.barEditItemIndicatorSelection.Id = 279;
            this.barEditItemIndicatorSelection.Name = "barEditItemIndicatorSelection";
            // 
            // barCheckItemUpTreshold
            // 
            this.barCheckItemUpTreshold.Id = 280;
            this.barCheckItemUpTreshold.Name = "barCheckItemUpTreshold";
            // 
            // barCheckItemDownTreshold
            // 
            this.barCheckItemDownTreshold.Id = 281;
            this.barCheckItemDownTreshold.Name = "barCheckItemDownTreshold";
            // 
            // barCheckItemTrend
            // 
            this.barCheckItemTrend.Id = 282;
            this.barCheckItemTrend.Name = "barCheckItemTrend";
            // 
            // barCheckItem4
            // 
            this.barCheckItem4.Id = 283;
            this.barCheckItem4.Name = "barCheckItem4";
            // 
            // barCheckItemTakeControl
            // 
            this.barCheckItemTakeControl.Caption = "barCheckItemTakeControl";
            this.barCheckItemTakeControl.Id = 271;
            this.barCheckItemTakeControl.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barCheckItemTakeControl.Name = "barCheckItemTakeControl";
            this.barCheckItemTakeControl.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.barCheckItemTakeControl_CheckedChanged);
            // 
            // barCheckItemScaledView
            // 
            this.barCheckItemScaledView.Caption = "barCheckItemScaledView";
            this.barCheckItemScaledView.Id = 272;
            this.barCheckItemScaledView.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barCheckItemScaledView.Name = "barCheckItemScaledView";
            this.barCheckItemScaledView.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.barCheckItemScaledView_CheckedChanged);
            // 
            // barButtonItemReConnect
            // 
            this.barButtonItemReConnect.Caption = "barButtonItemReConnect";
            this.barButtonItemReConnect.Enabled = false;
            this.barButtonItemReConnect.Id = 273;
            this.barButtonItemReConnect.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemReConnect.Name = "barButtonItemReConnect";
            this.barButtonItemReConnect.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemReConnect_ItemClick);
            // 
            // RibbonPageMonitoring
            // 
            this.RibbonPageMonitoring.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.xrDesignRibbonPageGroup3});
            this.RibbonPageMonitoring.Name = "RibbonPageMonitoring";
            this.RibbonPageMonitoring.Text = "Monitoreo";
            // 
            // xrDesignRibbonPageGroup3
            // 
            this.xrDesignRibbonPageGroup3.AllowMinimize = false;
            this.xrDesignRibbonPageGroup3.AllowTextClipping = false;
            this.xrDesignRibbonPageGroup3.ItemLinks.Add(this.barCheckItemTakeControl);
            this.xrDesignRibbonPageGroup3.ItemLinks.Add(this.barCheckItemScaledView);
            this.xrDesignRibbonPageGroup3.ItemLinks.Add(this.barButtonItemReConnect);
            this.xrDesignRibbonPageGroup3.Kind = DevExpress.XtraReports.UserDesigner.XRDesignRibbonPageGroupKind.SizeAndLayout;
            this.xrDesignRibbonPageGroup3.Name = "xrDesignRibbonPageGroup3";
            this.xrDesignRibbonPageGroup3.ShowCaptionButton = false;
            this.xrDesignRibbonPageGroup3.Text = "Operador";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.remoteDesktop);
            this.panelControl1.Controls.Add(this.pictureBox1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Padding = new System.Windows.Forms.Padding(10);
            this.panelControl1.Size = new System.Drawing.Size(1317, 807);
            this.panelControl1.TabIndex = 1;
            // 
            // xrDesignRibbonPageGroup2
            // 
            this.xrDesignRibbonPageGroup2.Kind = DevExpress.XtraReports.UserDesigner.XRDesignRibbonPageGroupKind.Report;
            this.xrDesignRibbonPageGroup2.Name = "xrDesignRibbonPageGroup2";
            // 
            // xrDesignRibbonPageGroup1
            // 
            this.xrDesignRibbonPageGroup1.AllowMinimize = false;
            this.xrDesignRibbonPageGroup1.AllowTextClipping = false;
            this.xrDesignRibbonPageGroup1.ItemLinks.Add(this.barButtonItemOperatorPerformance, true);
            this.xrDesignRibbonPageGroup1.ItemLinks.Add(this.barSubItemOperatorChartControl);
            this.xrDesignRibbonPageGroup1.Kind = DevExpress.XtraReports.UserDesigner.XRDesignRibbonPageGroupKind.SizeAndLayout;
            this.xrDesignRibbonPageGroup1.Name = "xrDesignRibbonPageGroup1";
            this.xrDesignRibbonPageGroup1.ShowCaptionButton = false;
            this.xrDesignRibbonPageGroup1.Text = "Operador";
            // 
            // RemoteControlForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1317, 807);
            this.Controls.Add(RibbonControl);
            this.Controls.Add(this.panelControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RemoteControlForm";
            this.Text = "Monitoreo de pantalla";
            this.Load += new System.EventHandler(this.RemoteControlForm_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RemoteControlForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraReports.UserDesigner.XRDesignRibbonPageGroup ribbonPageGroupOperator;
        private DevExpress.XtraBars.BarSubItem barSubItemOperatorChartControl;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemOperatorPerformance;
        private PictureBox pictureBox1;
        private VncSharp.RemoteDesktop remoteDesktop;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        internal DevExpress.XtraReports.UserDesigner.XRDesignRibbonPageGroup xrDesignRibbonPageGroup2;
        internal DevExpress.XtraReports.UserDesigner.XRDesignRibbonPageGroup xrDesignRibbonPageGroup1;
        internal DevExpress.XtraBars.Ribbon.RibbonControl RibbonControl;
        private ImageList imageList;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemSave;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemPrint;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemRefreshGraphic;
        internal DevExpress.XtraBars.BarEditItem barEditItemSelectRoom;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItemSendMonitor;
        internal DevExpress.XtraBars.BarEditItem barEditItemIndicatorSelection;
        private DevExpress.XtraBars.BarCheckItem barCheckItemUpTreshold;
        private DevExpress.XtraBars.BarCheckItem barCheckItemDownTreshold;
        private DevExpress.XtraBars.BarCheckItem barCheckItemTrend;
        private DevExpress.XtraBars.BarCheckItem barCheckItem4;
        internal DevExpress.XtraReports.UserDesigner.XRDesignRibbonPage RibbonPageMonitoring;
        internal DevExpress.XtraReports.UserDesigner.XRDesignRibbonPageGroup xrDesignRibbonPageGroup3;
        internal DevExpress.XtraBars.BarCheckItem barCheckItemTakeControl;
        internal DevExpress.XtraBars.BarCheckItem barCheckItemScaledView;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemReConnect;

    }
}