using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using DevExpress.XtraGrid.Views.Base;
using Smartmatic.SmartCad.Service;
using DevExpress.XtraGrid.Views.Layout;
using DevExpress.XtraGrid.Views.Grid;
using System.Threading;
using DevExpress.XtraGrid.Columns;
using System.Reflection;
using System.Globalization;
using DevExpress.XtraGrid.Views.BandedGrid;
using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadCore.ClientData;
namespace SmartCadGuiCommon.Controls
{
    public delegate void IndicatorGridFocusedRowChangedEventHandler(object sender, IndicatorGridFocusedRowChangedEventArgs e);
    public delegate void IndicatorOperatorFormChangeEventHandler(object sender, IndicatorOperatorFormChangeEventArgs e);
    
    public partial class IndicatorGridControl : UserControl
    {        
        public event IndicatorGridFocusedRowChangedEventHandler FocusedRowChangedEvent;
        public event IndicatorOperatorFormChangeEventHandler IndicatorOperatorFormChangedEvent;
        private BindingList<IndicatorGridControlData> dataSource;       
        private bool isUpdated = true;
        double resSummaryCalculateD42 = 0;
        double resSummaryCalculateD43 = 0;
        double resSummaryCalculateD44 = 0;
        double resSummaryCalculatePercentage = 0;
        string totalTime = string.Empty;

        #region DevExpress properties
        [Category("Behavior"), Browsable(true)]
        public DevExpress.XtraGrid.Views.Grid.GridView GridView1
        {
            get { return gridView1; }
        }
        [Category("Behavior"), Browsable(true)]
        public DevExpress.XtraGrid.Columns.GridColumn GridColumnIndicator
        {
            get { return gridColumnIndicator; }
        }
        [Category("Behavior"), Browsable(true)]
        public DevExpress.XtraGrid.Columns.GridColumn GridColumnCondition
        {
            get { return gridColumnCondition; }
        }
        [Category("Behavior"), Browsable(true)]
        public DevExpress.XtraGrid.Columns.GridColumn GridColumnRealValue
        {
            get { return gridColumnRealValue; }
        }
        [Category("Behavior"), Browsable(true)]
        public DevExpress.XtraGrid.Columns.GridColumn GridColumnTrend
        {
            get { return gridColumnTrend; }
        }
        [Category("Behavior"), Browsable(true)]
        public DevExpress.XtraGrid.Columns.GridColumn GridColumnCategory
        {
            get { return gridColumnCategory; }
        }
        [Category("Behavior"), Browsable(true)]
        public DevExpress.XtraGrid.GridControl GridControl1
        {
            get { return gridControl1; }
        }
        [Category("Behavior"), Browsable(true)]
        public DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit RepositoryItemPictureEdit1
        {
            get { return repositoryItemPictureEdit1; }
        }
        #endregion DevExpress properties

        public bool IsUpdated
        {
            get { return isUpdated; }
            set { isUpdated = value; }
        }
        public IndicatorGridControl()
        {
            InitializeComponent();
            LoadLanguage();
            this.gridView1.BeforeLeaveRow += new RowAllowEventHandler(gridView1_BeforeLeaveRow);
            this.bandedGridView1.CustomSummaryCalculate += new DevExpress.Data.CustomSummaryEventHandler(bandedGridView1_CustomSummaryCalculate);
            this.bandedGridView2.CustomSummaryCalculate += new DevExpress.Data.CustomSummaryEventHandler(bandedGridView2_CustomSummaryCalculate);
            this.bandedGridView3.CustomSummaryCalculate += new DevExpress.Data.CustomSummaryEventHandler(bandedGridView3_CustomSummaryCalculate);
            
            this.gridViewD08.CustomSummaryCalculate += new DevExpress.Data.CustomSummaryEventHandler(gridView_CustomSummaryCalculate);
            this.gridViewD14.CustomSummaryCalculate += new DevExpress.Data.CustomSummaryEventHandler(gridView_CustomSummaryCalculate);
            this.gridViewD15.CustomSummaryCalculate += new DevExpress.Data.CustomSummaryEventHandler(gridView_CustomSummaryCalculate);
            this.gridViewD30.CustomSummaryCalculate += new DevExpress.Data.CustomSummaryEventHandler(gridView_CustomSummaryCalculate);
            this.gridViewD31.CustomSummaryCalculate += new DevExpress.Data.CustomSummaryEventHandler(gridView_CustomSummaryCalculate);

            this.gridViewD01.CustomSummaryCalculate += new DevExpress.Data.CustomSummaryEventHandler(gridViewTime_CustomSummaryCalculate);
            this.gridViewD04.CustomSummaryCalculate += new DevExpress.Data.CustomSummaryEventHandler(gridViewTime_CustomSummaryCalculate);
            this.gridViewD25.CustomSummaryCalculate += new DevExpress.Data.CustomSummaryEventHandler(gridViewTime_CustomSummaryCalculate);
            this.gridViewD27.CustomSummaryCalculate += new DevExpress.Data.CustomSummaryEventHandler(gridViewTime_CustomSummaryCalculate);
            this.gridViewD28.CustomSummaryCalculate += new DevExpress.Data.CustomSummaryEventHandler(gridViewTime_CustomSummaryCalculate);
           

        }

        private void gridViewTime_CustomSummaryCalculate(object sender, DevExpress.Data.CustomSummaryEventArgs e)
        {
            if (e.SummaryProcess == DevExpress.Data.CustomSummaryProcess.Start)
            {                
            }
            if (e.SummaryProcess == DevExpress.Data.CustomSummaryProcess.Calculate)
            {
                if (e.IsTotalSummary && (e.Item is DevExpress.XtraGrid.GridColumnSummaryItem))
                {
                    DevExpress.XtraGrid.GridColumnSummaryItem item = 
                        (e.Item as DevExpress.XtraGrid.GridColumnSummaryItem);

                    IndicatorGridControlData data = null;                                     
                    int hour = 0;
                    int minutes = 0;
                    int seconds = 0;                                       
                    for (int i = 0; i < gridView1.RowCount; i++)
                    {
                        data = gridView1.GetRow(i) as IndicatorGridControlData;
                        if ((data.Data.IndicatorCustomCode == "D01") &&
                            ((sender as GridView).Name == "gridViewD01") ||
                            (data.Data.IndicatorCustomCode == "D04") &&
                            ((sender as GridView).Name == "gridViewD04") ||
                            (data.Data.IndicatorCustomCode == "D25") &&
                            ((sender as GridView).Name == "gridViewD25") ||
                            (data.Data.IndicatorCustomCode == "D27") &&
                            ((sender as GridView).Name == "gridViewD27") ||
                            (data.Data.IndicatorCustomCode == "D28") &&
                            ((sender as GridView).Name == "gridViewD28"))
                        {
                            break;
                        }
                    }
                    if (data.Data.IndicatorCustomCode == "D01")
                    {

                        foreach (DescriptionTimeIndicatorGridControlData valueTime
                            in data.D01)
                        {
                            object stringTime = valueTime.Time.Split(new char[] { ':' });
                            hour += int.Parse((stringTime as string[])[0]);
                            minutes += int.Parse((stringTime as string[])[1]);
                            seconds += int.Parse((stringTime as string[])[2]);                                               
                        }
                        if (seconds >= 60)
                        {
                            minutes += seconds / 60;
                            seconds = seconds % 60;
                        }
                        if (minutes >= 60)
                        {
                            hour += minutes / 60;
                            minutes = minutes % 60;
                        }
                        totalTime = string.Format("{0:00}:", hour) +
                            string.Format("{0:00}:", minutes) +
                            string.Format("{0:00}", seconds);
                        hour = 0;
                        hour = 0;
                        hour = 0;
                    }
                    else if (data.Data.IndicatorCustomCode == "D04")
                    {

                        foreach (DescriptionTimeIndicatorGridControlData valueTime
                            in data.D04)
                        {
                            object stringTime = valueTime.Time.Split(new char[] { ':' });
                            hour += int.Parse((stringTime as string[])[0]);
                            minutes += int.Parse((stringTime as string[])[1]);
                            seconds += int.Parse((stringTime as string[])[2]);                            
                        }
                        if (seconds >= 60)
                        {
                            minutes += seconds / 60;
                            seconds = seconds % 60;
                        }
                        if (minutes >= 60)
                        {
                            hour += minutes / 60;
                            minutes = minutes % 60;
                        }
                        totalTime = string.Format("{0:00}:", hour) +
                            string.Format("{0:00}:", minutes) +
                            string.Format("{0:00}", seconds);
                        hour = 0;
                        hour = 0;
                        hour = 0;
                    }
                    else if (data.Data.IndicatorCustomCode == "D25")
                    {

                        foreach (DescriptionTimeIndicatorGridControlData valueTime
                            in data.D25)
                        {
                            object stringTime = valueTime.Time.Split(new char[] { ':' });
                            hour += int.Parse((stringTime as string[])[0]);
                            minutes += int.Parse((stringTime as string[])[1]);
                            seconds += int.Parse((stringTime as string[])[2]);                           
                        }
                        if (seconds >= 60)
                        {
                            minutes += seconds / 60;
                            seconds = seconds % 60;
                        }
                        if (minutes >= 60)
                        {
                            hour += minutes / 60;
                            minutes = minutes % 60;
                        }
                        totalTime = string.Format("{0:00}:", hour) +
                            string.Format("{0:00}:", minutes) +
                             string.Format("{0:00}", seconds);
                        hour = 0;
                        hour = 0;
                        hour = 0;
                    }
                    else if (data.Data.IndicatorCustomCode == "D27")
                    {

                        foreach (DescriptionTimeIndicatorGridControlData valueTime
                            in data.D27)
                        {
                            object stringTime = valueTime.Time.Split(new char[] { ':' });
                            hour += int.Parse((stringTime as string[])[0]);
                            minutes += int.Parse((stringTime as string[])[1]);
                            seconds += int.Parse((stringTime as string[])[2]);                            
                        }
                        if (seconds >= 60)
                        {
                            minutes += seconds / 60;
                            seconds = seconds % 60;
                        }
                        if (minutes >= 60)
                        {
                            hour += minutes / 60;
                            minutes = minutes % 60;
                        }
                        totalTime = string.Format("{0:00}:", hour) + 
                            string.Format("{0:00}:", minutes) +
                            string.Format("{0:00}", seconds);
                        hour = 0;
                        hour = 0;
                        hour = 0;
                    }
                    else if (data.Data.IndicatorCustomCode == "D28")
                    {

                        foreach (DescriptionTimeIndicatorGridControlData valueTime
                            in data.D28)
                        {                            
                            object stringTime = valueTime.Time.Split(new char[] { ':' });                            
                            hour += int.Parse((stringTime as string[])[0]);
                            minutes += int.Parse((stringTime as string[])[1]);
                            seconds += int.Parse((stringTime as string[])[2]);                           
                        }
                        if (seconds >= 60)
                        {
                            minutes += seconds / 60;
                            seconds = seconds % 60;
                        }
                        if (minutes >= 60)
                        {
                            hour += minutes / 60;
                            minutes = minutes % 60;
                        }
                        totalTime = string.Format("{0:00}:", hour) +
                            string.Format("{0:00}:", minutes) +
                            string.Format("{0:00}", seconds);
                        hour = 0;
                        hour = 0;
                        hour = 0;
                    }     
                }
            }
            if (e.SummaryProcess == DevExpress.Data.CustomSummaryProcess.Finalize)
            {                          
                e.TotalValue = totalTime;
                totalTime = string.Empty;
            }

        }
        
        private void gridView_CustomSummaryCalculate(object sender,
            DevExpress.Data.CustomSummaryEventArgs e)
        {
            if (e.SummaryProcess == DevExpress.Data.CustomSummaryProcess.Start)
            {
                resSummaryCalculatePercentage = 0.0;
            }
            if (e.SummaryProcess == DevExpress.Data.CustomSummaryProcess.Calculate)
            {
                if (e.IsTotalSummary && (e.Item is DevExpress.XtraGrid.GridColumnSummaryItem))
                {
                    DevExpress.XtraGrid.GridColumnSummaryItem item = (e.Item as DevExpress.XtraGrid.GridColumnSummaryItem);

                    IndicatorGridControlData data = null;
                    resSummaryCalculatePercentage = 0.0;
                    for (int i = 0; i < gridView1.RowCount; i++)
                    {
                        data = gridView1.GetRow(i) as IndicatorGridControlData;
                        if ((data.Data.IndicatorCustomCode == "D08") &&
                            ((sender as GridView).Name == "gridViewD08") ||
                            (data.Data.IndicatorCustomCode == "D14") &&
                            ((sender as GridView).Name == "gridViewD14") ||
                            (data.Data.IndicatorCustomCode == "D15") &&
                            ((sender as GridView).Name == "gridViewD15") ||
                            (data.Data.IndicatorCustomCode == "D30") &&
                            ((sender as GridView).Name == "gridViewD30") ||
                            (data.Data.IndicatorCustomCode == "D31") &&
                            ((sender as GridView).Name == "gridViewD31"))
                        {
                            break;
                        }
                    }
                    if (data.Data.IndicatorCustomCode == "D08")
                    {

                        foreach (DescriptionValuePercentageIndicatorGridControlData valuePercentage
                            in data.D08)
                        {

                            resSummaryCalculatePercentage += valuePercentage.Percentage;
                        }
                    }
                    else if (data.Data.IndicatorCustomCode == "D14")
                    {

                        foreach (DescriptionValuePercentageIndicatorGridControlData valuePercentage
                            in data.D14)
                        {
                            resSummaryCalculatePercentage += valuePercentage.RealValue;
                        }
                        double totalUnits = (long)ServerServiceClient.GetInstance().SearchBasicObject(
                         SmartCadHqls.GetCustomHql(
                         SmartCadHqls.GetAmountUnitGroupByDepartmentType,
                         data.Data.CustomCode));

                        resSummaryCalculatePercentage = resSummaryCalculatePercentage / totalUnits;
                    }
                    else if (data.Data.IndicatorCustomCode == "D15")
                    {

                        foreach (DescriptionValuePercentageIndicatorGridControlData valuePercentage
                            in data.D15)
                        {

                            resSummaryCalculatePercentage += valuePercentage.RealValue;
                        }
                        double totalUnits = (long)ServerServiceClient.GetInstance().SearchBasicObject(
                          SmartCadHqls.GetCustomHql(
                          SmartCadHqls.GetAmountUnitGroupByDepartmentType,
                          data.Data.CustomCode));

                        resSummaryCalculatePercentage = resSummaryCalculatePercentage / totalUnits;
                    }
                    else if (data.Data.IndicatorCustomCode == "D30")
                    {
                        foreach (DescriptionValuePercentageIndicatorGridControlData valuePercentage
                            in data.D30)
                        {
                            resSummaryCalculatePercentage += valuePercentage.RealValue;
                        }
                        double totalUnits = (long)ServerServiceClient.GetInstance().SearchBasicObject(
                          SmartCadHqls.GetCustomHql(
                          SmartCadHqls.GetAmountUnitGroupByDepartmentType,
                          data.Data.CustomCode));
                        resSummaryCalculatePercentage = resSummaryCalculatePercentage / totalUnits;
                    }
                    else if (data.Data.IndicatorCustomCode == "D31")
                    {
                        foreach (DescriptionValuePercentageIndicatorGridControlData valuePercentage
                            in data.D31)
                        {

                            resSummaryCalculatePercentage += valuePercentage.Percentage;
                        }
                        double totalUnits = (long)ServerServiceClient.GetInstance().SearchBasicObject(
                          SmartCadHqls.GetCustomHql(
                          SmartCadHqls.GetAmountUnitGroupByDepartmentType,
                          data.Data.CustomCode));
                        resSummaryCalculatePercentage = resSummaryCalculatePercentage / totalUnits;
                    }
                    resSummaryCalculatePercentage = Math.Round(resSummaryCalculatePercentage, 4);

                }
            }
            if (e.SummaryProcess == DevExpress.Data.CustomSummaryProcess.Finalize)
            {
                
                e.TotalValue = resSummaryCalculatePercentage.ToString("P", CultureInfo.InvariantCulture);
            }

        }
        private void bandedGridView3_CustomSummaryCalculate(object sender,
          DevExpress.Data.CustomSummaryEventArgs e)
        {

            if (e.SummaryProcess == DevExpress.Data.CustomSummaryProcess.Start)
            {
                resSummaryCalculateD44 = 0.0;
            }
            if (e.SummaryProcess == DevExpress.Data.CustomSummaryProcess.Calculate)
            {
                if (e.IsTotalSummary && (e.Item is DevExpress.XtraGrid.GridColumnSummaryItem))
                {
                    DevExpress.XtraGrid.GridColumnSummaryItem item = (e.Item as DevExpress.XtraGrid.GridColumnSummaryItem);

                    IndicatorGridControlData data = null;
                    for (int i = 0; i < gridView1.RowCount; i++)
                    {
                        data = gridView1.GetRow(i) as IndicatorGridControlData;
                        if (data.Data.IndicatorCustomCode == "D44")
                        {
                            break;
                        }
                    }

                    double totalCurrentValue = 0;
                    double totalValue = 0;
                    if (data.Data.IndicatorCustomCode == "D44")
                    {

                        foreach (DescriptionValuePercentageTableIndicatorGridControlData valuePercentage
                            in data.D44)
                        {
                            if (item.FieldName == "InProgressPercentage")
                            {
                                totalCurrentValue += valuePercentage.InProgressValue;
                            }
                            else if (item.FieldName == "AssignedPercentage")
                            {
                                totalCurrentValue += valuePercentage.AssignedValue;
                            }
                            else if (item.FieldName == "PendingPercentage")
                            {
                                totalCurrentValue += valuePercentage.PendingValue;
                            }
                            else if (item.FieldName == "DelayedPercentage")
                            {
                                totalCurrentValue += valuePercentage.DelayedValue;
                            }
                            else if (item.FieldName == "NewPercentage")
                            {
                                totalCurrentValue += valuePercentage.NewValue;
                            }
                            totalValue += valuePercentage.TotalValue;
                        }
                        if (item.FieldName == "TotalValuePercentage")
                        {
                            resSummaryCalculateD44 = 100;
                        }
                        else
                        {
                            if (totalValue > 0)
                            {
                                resSummaryCalculateD44 = (totalCurrentValue * 100) / totalValue;
                            }
                            else
                            {
                                resSummaryCalculateD44 = 0;
                            }
                            resSummaryCalculateD44 = Math.Round(resSummaryCalculateD44, 2);
                        }

                    }

                }
            }
            if (e.SummaryProcess == DevExpress.Data.CustomSummaryProcess.Finalize)
            {
                e.TotalValue = resSummaryCalculateD44;
            }

        }              

        private void bandedGridView2_CustomSummaryCalculate(object sender, 
            DevExpress.Data.CustomSummaryEventArgs e)
        {
            
            if (e.SummaryProcess == DevExpress.Data.CustomSummaryProcess.Start)
            {
                resSummaryCalculateD43 = 0.0;
            }
            if (e.SummaryProcess == DevExpress.Data.CustomSummaryProcess.Calculate)
            {
                if (e.IsTotalSummary && (e.Item is DevExpress.XtraGrid.GridColumnSummaryItem))
                {
                    DevExpress.XtraGrid.GridColumnSummaryItem item = (e.Item as DevExpress.XtraGrid.GridColumnSummaryItem);

                    IndicatorGridControlData data = null;
                    for (int i = 0; i < gridView1.RowCount; i++)
                    {
                        data = gridView1.GetRow(i) as IndicatorGridControlData;
                        if (data.Data.IndicatorCustomCode == "D43")
                        {
                            break;
                        }
                    }                                        
                    if (data.Data.IndicatorCustomCode == "D43")
                    {                        
                        resSummaryCalculateD43 = 100;
                    }
                    
                }
            }
            if (e.SummaryProcess == DevExpress.Data.CustomSummaryProcess.Finalize)
            {
                e.TotalValue = resSummaryCalculateD43;
            }

        }

        private void bandedGridView1_CustomSummaryCalculate(object sender,
           DevExpress.Data.CustomSummaryEventArgs e)
        {

            if (e.SummaryProcess == DevExpress.Data.CustomSummaryProcess.Start)
            {
                resSummaryCalculateD42 = 0.0;
            }
            if (e.SummaryProcess == DevExpress.Data.CustomSummaryProcess.Calculate)
            {
                if (e.IsTotalSummary && (e.Item is DevExpress.XtraGrid.GridColumnSummaryItem))
                {
                    DevExpress.XtraGrid.GridColumnSummaryItem item = (e.Item as DevExpress.XtraGrid.GridColumnSummaryItem);

                    IndicatorGridControlData data = null;
                    for (int i = 0; i < gridView1.RowCount; i++)
                    {
                        data = gridView1.GetRow(i) as IndicatorGridControlData;
                        if (data.Data.IndicatorCustomCode == "D42")
                        {
                            break;
                        }
                    }

                    double totalCurrentValue = 0;
                    double totalValue = 0;
                    if (data.Data.IndicatorCustomCode == "D42")
                    {

                        foreach (DescriptionValuePercentageTableIndicatorGridControlData valuePercentage
                            in data.D42)
                        {
                            if (item.FieldName == "InProgressPercentage")
                            {
                                totalCurrentValue += valuePercentage.InProgressValue;
                            }
                            else if (item.FieldName == "AssignedPercentage")
                            {
                                totalCurrentValue += valuePercentage.AssignedValue;
                            }
                            else if (item.FieldName == "PendingPercentage")
                            {
                                totalCurrentValue += valuePercentage.PendingValue;
                            }
                            else if (item.FieldName == "DelayedPercentage")
                            {
                                totalCurrentValue += valuePercentage.DelayedValue;
                            }
                            else if (item.FieldName == "NewPercentage")
                            {
                                totalCurrentValue += valuePercentage.NewValue;
                            }
                            totalValue += valuePercentage.TotalValue;
                        }
                        if (item.FieldName == "TotalValuePercentage")
                        {
                            resSummaryCalculateD42 = 100;
                        }
                        else
                        {
                            if (totalValue > 0)
                            {
                                resSummaryCalculateD42 = (totalCurrentValue * 100) / totalValue;
                            }
                            else
                            {
                                resSummaryCalculateD42 = 0;
                            }
                            resSummaryCalculateD42 = Math.Round(resSummaryCalculateD42, 2);
                        }

                    }

                }
            }
            if (e.SummaryProcess == DevExpress.Data.CustomSummaryProcess.Finalize)
            {
                e.TotalValue = resSummaryCalculateD42;
            }

        }              

        private void LoadLanguage()
        {            
            gridColumnIndicator.Caption = ResourceLoader.GetString2("Indicator");
            gridColumnCategory.Caption = ResourceLoader.GetString2("IndicatorType");
            gridColumnCondition.Caption = ResourceLoader.GetString2("Threshold");
            gridColumnRealValue.Caption = ResourceLoader.GetString2("RealValue");
            gridColumnTrend.Caption = ResourceLoader.GetString2("Trend");

            this.gridBand1.Caption = ResourceLoader.GetString2("Incident");
            this.gridBand2.Caption = ResourceLoader.GetString2("IndicatorD47Name");
            this.gridBand3.Caption = ResourceLoader.GetString2("Pending");
            this.gridBand4.Caption = ResourceLoader.GetString2("IndicatorD45Name");
            this.gridBand5.Caption = ResourceLoader.GetString2("InProcess");
            this.gridBand6.Caption = ResourceLoader.GetString2("Total");
            this.gridBand7.Caption = ResourceLoader.GetString2("Incident");
            this.gridBand8.Caption = ResourceLoader.GetString2("IndicatorD47Name");
            this.gridBand9.Caption = ResourceLoader.GetString2("Pending");
            this.gridBand10.Caption = ResourceLoader.GetString2("IndicatorD45Name");
            this.gridBand11.Caption = ResourceLoader.GetString2("InProcess");
            this.gridBand13.Caption = ResourceLoader.GetString2("Total");
            this.gridBand13.Caption = ResourceLoader.GetString2("IndicatorD47Name");
            this.gridBand14.Caption = ResourceLoader.GetString2("IndicatorD47Name");                      
            this.gridBand15.Caption = ResourceLoader.GetString2("Pending");
            this.gridBand16.Caption = ResourceLoader.GetString2("IndicatorD45Name");
            this.gridBand17.Caption = ResourceLoader.GetString2("InProcess");
            this.gridBand18.Caption = ResourceLoader.GetString2("Total");
            this.gridBand19.Caption = ResourceLoader.GetString2("IndicatorD46Name");
            this.gridBand20.Caption = ResourceLoader.GetString2("IndicatorD46Name");
            this.gridBand21.Caption = ResourceLoader.GetString2("IndicatorD46Name");


            #region MultipleValues
            //Indicator D01
            gridColumnD01Description.Caption = ResourceLoader.GetString2("IncidentType");
            gridColumnD01Time.Caption = ResourceLoader.GetString2("AverageTimeFormat");

            //Indicator D04
            gridColumnD04Description.Caption = ResourceLoader.GetString2("IncidentType");
            gridColumnD04Time.Caption = ResourceLoader.GetString2("AverageTimeFormat");

            //Indicator D08
            gridColumnD08Description.Caption = ResourceLoader.GetString2("IncidentType");
            gridColumnD08RealValue.Caption = ResourceLoader.GetString2("RealValue");
            gridColumnD08Percentage.Caption = ResourceLoader.GetString2("%");

            //Indicator D14
            gridColumnD14Description.Caption = ResourceLoader.GetString2("Station");
            gridColumnD14RealValue.Caption = ResourceLoader.GetString2("RealValue");
            gridColumnD14Percentage.Caption = ResourceLoader.GetString2("%");

            //Indicator D15
            gridColumnD15Description.Caption = ResourceLoader.GetString2("Zone");
            gridColumnD15RealValue.Caption = ResourceLoader.GetString2("RealValue");
            gridColumnD15Percentage.Caption = ResourceLoader.GetString2("%");

            //Indicator D27
            gridColumnD27Description.Caption = ResourceLoader.GetString2("Station");
            gridColumnD27Time.Caption = ResourceLoader.GetString2("AverageTimeFormat");

            //Indicator D28
            gridColumnD28Description.Caption = ResourceLoader.GetString2("Zone");
            gridColumnD28Time.Caption = ResourceLoader.GetString2("AverageTimeFormat");

            //Indicator D30
            gridColumnD30Description.Caption = ResourceLoader.GetString2("Zone");
            gridColumnD30RealValue.Caption = ResourceLoader.GetString2("RealValue");
            gridColumnD30Percentage.Caption = ResourceLoader.GetString2("%");

            //Indicator D31
            gridColumnD31Description.Caption = ResourceLoader.GetString2("Station");
            gridColumnD31RealValue.Caption = ResourceLoader.GetString2("RealValue");
            gridColumnD31Percentage.Caption = ResourceLoader.GetString2("%");

            //Indicator D25
            gridColumnD25Description.Caption = ResourceLoader.GetString2("IncidentType");
            gridColumnD25Time.Caption = ResourceLoader.GetString2("AverageTimeFormat");

            //Indicator D42
            gridColumnD42Description.Caption = ResourceLoader.GetString2("Type");
            gridColumnD42AssignedValue.Caption = ResourceLoader.GetString2("RealValue");
            gridColumnD42AssignedPercentage.Caption = ResourceLoader.GetString2("%");
            gridColumnD42PendingValue.Caption = ResourceLoader.GetString2("RealValue");
            gridColumnD42PendingPercentage.Caption = ResourceLoader.GetString2("%");
            gridColumnD42DelayedValue.Caption = ResourceLoader.GetString2("RealValue");
            gridColumnD42DelayedPercentage.Caption = ResourceLoader.GetString2("%");
            gridColumnD42InProgressValue.Caption = ResourceLoader.GetString2("RealValue");
            gridColumnD42InProgressPercentage.Caption = ResourceLoader.GetString2("%");
            gridColumnD42NewValue.Caption = ResourceLoader.GetString2("RealValue");
            gridColumnD42NewPercentage.Caption = ResourceLoader.GetString2("%");
            gridColumnD42TotalValue.Caption = ResourceLoader.GetString2("RealValue");
            gridColumnD42TotalValuePercentage.Caption = ResourceLoader.GetString2("%");

            //Indicator D43
            bandedGridColumnD43Description.Caption = ResourceLoader.GetString2("Type");
            bandedGridColumnD43AssignedValue.Caption = ResourceLoader.GetString2("RealValue");
            bandedGridColumnD43AssignedPercentage.Caption = ResourceLoader.GetString2("%");
            bandedGridColumnD43PendingValue.Caption = ResourceLoader.GetString2("RealValue");
            bandedGridColumnD43PendingPercentage.Caption = ResourceLoader.GetString2("%");
            bandedGridColumnD43DelayedValue.Caption = ResourceLoader.GetString2("RealValue");
            bandedGridColumnD43DelayedPercentage.Caption = ResourceLoader.GetString2("%");
            bandedGridColumnD43InProgressValue.Caption = ResourceLoader.GetString2("RealValue");
            bandedGridColumnD43InProgressPercentage.Caption = ResourceLoader.GetString2("%");
            bandedGridColumnD43NewValue.Caption = ResourceLoader.GetString2("RealValue");
            bandedGridColumnD43NewPercentage.Caption = ResourceLoader.GetString2("%");
            bandedGridColumnD43TotalValue.Caption = ResourceLoader.GetString2("RealValue");
            bandedGridColumnD43TotalPercentage.Caption = ResourceLoader.GetString2("%");

            //Indicator D44
            bandedGridColumnD44Description.Caption = ResourceLoader.GetString2("Type");
            bandedGridColumnD44AssignedValue.Caption = ResourceLoader.GetString2("RealValue");
            bandedGridColumnD44AssignedPercentage.Caption = ResourceLoader.GetString2("%");
            bandedGridColumnD44PendingValue.Caption = ResourceLoader.GetString2("RealValue");
            bandedGridColumnD44PendingPercentage.Caption = ResourceLoader.GetString2("%");
            bandedGridColumnD44DelayedValue.Caption = ResourceLoader.GetString2("RealValue");
            bandedGridColumnD44DelayedPercentage.Caption = ResourceLoader.GetString2("%");
            bandedGridColumnD44InProgressValue.Caption = ResourceLoader.GetString2("RealValue");
            bandedGridColumnD44InProgressPercentage.Caption = ResourceLoader.GetString2("%");
            bandedGridColumnD44NewValue.Caption = ResourceLoader.GetString2("RealValue");
            bandedGridColumnD44NewPercentage.Caption = ResourceLoader.GetString2("%");
            bandedGridColumnD44TotalValue.Caption = ResourceLoader.GetString2("RealValue");
            bandedGridColumnD44TotalValuePercentage.Caption = ResourceLoader.GetString2("%");
            
            
            #endregion
        }
        public BindingList<IndicatorGridControlData> DataSource
        {
            get
            {
                return dataSource;
            }
            set
            {
                dataSource = value;
                FormUtil.InvokeRequired(this.gridControl1, delegate
                {
                    this.gridControl1.DataSource = dataSource;
                });
            }
        }
        public void Load(IList list)
        {
            BindingList<IndicatorGridControlData> listValue = new BindingList<IndicatorGridControlData>();           
            foreach (IndicatorResultValuesClientData value in list)
            {
                if (value.LocationGrid == true)
                {
                    IndicatorGridControlData indicatorGridControlData = new IndicatorGridControlData(value.IndicatorName, value.IndicatorCustomCode);
                    int index = listValue.IndexOf(indicatorGridControlData);
                    if (index > -1)
                    {
                        IndicatorGridControlData aux = listValue[index];
                        if (aux.MultipleValues == true)
                        {
                            aux.AddOrUpdateInternalList(value);
                        }
                        else
                        {
                            aux.Data = value;
                            listValue[index] = aux;
                        }
                    }
                    else
                    {
                        listValue.Add(new IndicatorGridControlData(value));
                    }
                }
            }
            DataSource = listValue;
        }

        public void Clear()
        {
            this.DataSource = null;
        }        
        
        public void AddOrUpdate(IndicatorResultValuesClientData result)
        {
            if (DataSource == null)
            {
                DataSource = new BindingList<IndicatorGridControlData>();
            }

            if (result.LocationGrid == true)
            {
                IndicatorGridControlData indicatorSelected = null;
                int[] rowIndexSelected = this.gridView1.GetSelectedRows();
                if (rowIndexSelected.Length > 0)
                {
                    indicatorSelected = (gridView1.GetRow(rowIndexSelected[0]) as IndicatorGridControlData);
                }
                IndicatorGridControlData indicatorGridControlData = new IndicatorGridControlData(result.IndicatorName, result.IndicatorCustomCode);
                int index = dataSource.IndexOf(indicatorGridControlData);
                if (index > -1)
                {
                    IndicatorGridControlData aux = dataSource[index];
                    if (aux.MultipleValues == true)
                    {
                        aux.AddOrUpdateInternalList(result);

                        int rowHandle = gridView1.GetRowHandle(index);
                        if (gridView1.IsMasterRow(rowHandle) == true &&
                            gridView1.GetMasterRowExpanded(rowHandle) == true)
                        {
                            FormUtil.InvokeRequired(this, delegate
                                {
                                    gridView1.CollapseMasterRow(rowHandle);
                                    gridView1.ExpandMasterRow(rowHandle);
                                });
                        }                       

                    }
                    else
                    {
                        aux.Data = result;
                        
                        FormUtil.InvokeRequired(this.gridControl1, delegate
                        {
                            dataSource[index] = aux;
                            if (this.gridView1 != null)
                            {
                                if ((rowIndexSelected != null) &&
                                    (rowIndexSelected.Length > 0) &&
                                    (rowIndexSelected[0] != -1) &&
                                    (indicatorSelected != null) &&
                                    (indicatorSelected.Data.IndicatorCustomCode == result.IndicatorCustomCode))
                                {
                                    isUpdated = true;
                                    this.gridView1_FocusedRowChanged(null,
                               new FocusedRowChangedEventArgs(-1, this.gridView1.GetRowHandle(index)));
                                }
                            }
                        });
                    }
                }
                else
                {
                    FormUtil.InvokeRequired(this.gridControl1, delegate
                        {
                            dataSource.Add(new IndicatorGridControlData(result));
                        });
                }
            }
        }
      
        private void gridView1_BeforeLeaveRow(object sender, RowAllowEventArgs e)
        {
            //isUpdated = true;
            
        }
        private void gridView1_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle >= 0)
            {
                if (isUpdated == false)
                {
                    BackgroundProcessForm processForm =
                        new BackgroundProcessForm(this,
                      new MethodInfo[1] { GetType().GetMethod("DoFocusedRowChanged", 
                        BindingFlags.NonPublic | BindingFlags.Instance) },
                         new object[1][] { new object[2] { sender, e } });
                    processForm.CanThrowError = false;
                    processForm.ShowDialog();
                }
                else
                {
                    DoFocusedRowChanged(sender, e);
                }
            }
            else
            {
                IndicatorGridFocusedRowChangedEventArgs eventArgs = new IndicatorGridFocusedRowChangedEventArgs();
                eventArgs.List = new ArrayList();
                eventArgs.Clear = true;

                try
                {
                    if (FocusedRowChangedEvent != null)
                    {
                        FocusedRowChangedEvent(this, eventArgs);
                    }
                }
                catch { }
            }
            isUpdated = false;
        }

        private void DoFocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            try
            {
                if (e.FocusedRowHandle >= 0)
                {
                    IndicatorGridControlData rowSelected = (gridView1.GetRow(e.FocusedRowHandle)
                        as IndicatorGridControlData);

                    if (rowSelected.MultipleValues == false && ApplicationUtil.IndicatorMultipleResult(rowSelected.Data) == false)
                    {
                        DateTime temp = ServerServiceClient.GetInstance().GetTimeFromDB();
                        int hour;
                        if (temp.Hour > 0)
                            hour = temp.Hour - 1;
                        else
                            hour = temp.Hour;
                        DateTime toDay = new DateTime(temp.Year, temp.Month, temp.Day, hour, temp.Minute, temp.Second);

                        IList indicators = ServerServiceClient.GetInstance().SearchClientObjects(
                             SmartCadHqls.GetCustomHql(SmartCadHqls.GetAllIndicatorsResultByindicatorCode,
                             rowSelected.Data.IndicatorResultCode,
                             rowSelected.Data.IndicatorResultClassCode,
                             toDay.ToString(ApplicationUtil.DataBaseFormattedDate)));


                        IndicatorGridFocusedRowChangedEventArgs eventArgs = new IndicatorGridFocusedRowChangedEventArgs();
                        eventArgs.List = new ArrayList(indicators);
                        if ((e.PrevFocusedRowHandle == -1) && (sender == null))
                        {
                            eventArgs.Clear = false;
                        }
                        else
                        {
                            eventArgs.Clear = true;
                        }


                        if (FocusedRowChangedEvent != null)
                        {
                            FocusedRowChangedEvent(this, eventArgs);
                        }


                        bool isOperatorIndicator = false;
                        
                        if ((indicators != null && indicators.Count > 0) && (this.ParentForm != null && this.ParentForm.Name == "IndicatorsForm"))
                        {
                            IList indicator = ServerServiceClient.GetInstance().SearchClientObjects(
                                    SmartCadHqls.GetCustomHql(SmartCadHqls.GetIndicatorByCustomCode,
                                    (indicators[0] as IndicatorResultClientData).IndicatorCustomCode));
                            if (indicator.Count > 0)
                            {
                                foreach (IndicatorClassDashboardClientData classData in (indicator[0] as IndicatorClientData).Classes)
                                {

                                    if (classData.ClassName == "OPERATOR")
                                    {
                                        isOperatorIndicator = true;
                                        IndicatorOperatorFormChangeEventArgs args = new IndicatorOperatorFormChangeEventArgs();
                                        args.CustomCde = (indicator[0] as IndicatorClientData).CustomCode;
                                        args.Name = (indicator[0] as IndicatorClientData).Name;
                                        args.Show = true;

                                        if (IndicatorOperatorFormChangedEvent != null)
                                        {
                                            IndicatorOperatorFormChangedEvent(this, args);
                                        }
                                    }
                                }
                            }
                        }                        
                        if (isOperatorIndicator == false)
                        {
                            IndicatorOperatorFormChangeEventArgs args = new IndicatorOperatorFormChangeEventArgs();
                            args.Show = false;
                            if (IndicatorOperatorFormChangedEvent != null)
                            {
                                IndicatorOperatorFormChangedEvent(this, args);
                            }
                        }
                        else
                        {
                            isOperatorIndicator = false;
                        }
                    }
                    else
                    {
                        IndicatorGridFocusedRowChangedEventArgs eventArgs = new IndicatorGridFocusedRowChangedEventArgs();
                        eventArgs.List = new ArrayList();
                        eventArgs.Clear = true;

                        if (FocusedRowChangedEvent != null)
                        {
                            FocusedRowChangedEvent(this, eventArgs);
                        }
                    }
                }
            }
            catch (Exception eeeeeee)
            {
                Console.WriteLine(eeeeeee.ToString());
            }
        }

        private void gridView_DragObjectOver(object sender, DragObjectOverEventArgs e)
        {
            GridColumn column = e.DragObject as GridColumn;
            GridBand gridBand = e.DragObject as GridBand;
            if ((column != null || gridBand != null) && e.DropInfo.Index < 0)
            {
                e.DropInfo.Valid = false;
            }
        }             
    }

    public class IndicatorGridControlData
    {
        private IndicatorResultValuesClientData data;
        private bool multipleValues = false;

        private List<DescriptionTimeIndicatorGridControlData> listD01 = new List<DescriptionTimeIndicatorGridControlData>();
        private List<DescriptionTimeIndicatorGridControlData> listD04 = new List<DescriptionTimeIndicatorGridControlData>();
        private List<DescriptionValuePercentageIndicatorGridControlData> listD08 = new List<DescriptionValuePercentageIndicatorGridControlData>();
        private List<DescriptionValuePercentageIndicatorGridControlData> listD14 = new List<DescriptionValuePercentageIndicatorGridControlData>();
        private List<DescriptionValuePercentageIndicatorGridControlData> listD15 = new List<DescriptionValuePercentageIndicatorGridControlData>();
        private List<DescriptionValuePercentageIndicatorGridControlData> listD30 = new List<DescriptionValuePercentageIndicatorGridControlData>();
        private List<DescriptionValuePercentageIndicatorGridControlData> listD31 = new List<DescriptionValuePercentageIndicatorGridControlData>();
        private List<DescriptionTimeIndicatorGridControlData> listD27 = new List<DescriptionTimeIndicatorGridControlData>();
        private List<DescriptionTimeIndicatorGridControlData> listD28 = new List<DescriptionTimeIndicatorGridControlData>();
        private List<DescriptionTimeIndicatorGridControlData> listD25 = new List<DescriptionTimeIndicatorGridControlData>();
        private List<DescriptionValuePercentageTableIndicatorGridControlData> listD42 = new
            List<DescriptionValuePercentageTableIndicatorGridControlData>();
        private List<DescriptionValuePercentageTableIndicatorGridControlData> listD43 = new
            List<DescriptionValuePercentageTableIndicatorGridControlData>();

        private List<DescriptionValuePercentageTableIndicatorGridControlData> listD44 = new
           List<DescriptionValuePercentageTableIndicatorGridControlData>();

        public IndicatorGridControlData(IndicatorResultValuesClientData indicatorResultValuesClientData)
        {
            AddOrUpdateInternalList(indicatorResultValuesClientData);
            if ((multipleValues == true && data == null) || multipleValues == false)
                Data = indicatorResultValuesClientData;
        }

        public IndicatorGridControlData(string name, string customCode)
        {
            data = new IndicatorResultValuesClientData();
            data.IndicatorName = name;
            data.IndicatorCustomCode = customCode;
        }

        public IndicatorResultValuesClientData Data
        {
            get { return data; }
            set
            {
                data = value;
            }
        }

        public bool MultipleValues
        {
            get
            {
                return multipleValues;
            }
        }

        public Image Threshold
        {
            get
            {
                if (data.Threshold == 2 && multipleValues == false)
                     return ResourceLoader.GetImage("IMAGE_YellowSemaphoreLightSmall");
                else if (data.Threshold == 3 && multipleValues == false)
                     return ResourceLoader.GetImage("IMAGE_RedSemaphoreLightSmall");
                else if (data.Threshold == 1 && multipleValues == false)
                     return ResourceLoader.GetImage("IMAGE_GreenSemaphoreLightSmall");
                 else
                     return ResourceLoader.GetImage("$Image.Transparent");

            }
        }
        public Image Trend
        {
            get
            {
                if (data.Trend == 0 || multipleValues == true)
                    return ResourceLoader.GetImage("$Image.Transparent");
                else
                    return ResourceLoader.GetImage("TrendImage" + data.Trend.ToString());
            }
        }
        public string Name
        {
            get
            {
                return data.IndicatorCustomCode + " - " + data.IndicatorName;
            }
        }
        public string RealValue
        {
            get
            {
                if (multipleValues == false)
                {
                    if (data.MeasureUnit == "seg")
                        return ApplicationUtil.GetTimeSpanCustomString(TimeSpan.FromSeconds(double.Parse(data.ResultValue)));
                    else if (data.MeasureUnit == "%")
                        return double.Parse(data.ResultValue).ToString("P", CultureInfo.InvariantCulture);
                    else
                        return (data.ResultValue + " " + data.MeasureUnit);
                }
                else
                    return string.Empty;
            }
        }
        public string Type
        {
            get
            {
                return data.IndicatorTypeFriendlyName;
            }
        }

        public string Description
        {
            get
            {
                return data.IndicatorDescription;
            }
        }

        public List<DescriptionTimeIndicatorGridControlData> D01
        {
            get
            {
                return listD01;
            }
        }

        public List<DescriptionTimeIndicatorGridControlData> D04
        {
            get
            {
                return listD04;
            }
        }

        public List<DescriptionValuePercentageIndicatorGridControlData> D08
        {
            get
            {
                return listD08;
            }
        }

        public List<DescriptionValuePercentageIndicatorGridControlData> D14
        {
            get
            {
                return listD14;
            }
        }

        public List<DescriptionValuePercentageIndicatorGridControlData> D15
        {
            get
            {
                return listD15;
            }
        }

        public List<DescriptionTimeIndicatorGridControlData> D27
        {
            get
            {
                return listD27;
            }
        }

        public List<DescriptionTimeIndicatorGridControlData> D28
        {
            get
            {
                return listD28;
            }
        }

        public List<DescriptionValuePercentageIndicatorGridControlData> D30
        {
            get
            {
                return listD30;
            }
        }

        public List<DescriptionValuePercentageIndicatorGridControlData> D31
        {
            get
            {
                return listD31;
            }
        }

        public List<DescriptionValuePercentageTableIndicatorGridControlData> D42
        {
            get
            {
                return listD42;
            }
        }

        public List<DescriptionValuePercentageTableIndicatorGridControlData> D43
        {
            get
            {
                return listD43;
            }
        }

        public List<DescriptionValuePercentageTableIndicatorGridControlData> D44
        {
            get
            {
                return listD44;
            }
        }

        public List<DescriptionTimeIndicatorGridControlData> D25
        {
            get
            {
                return listD25;
            }
        }

        public void AddOrUpdateInternalList(IndicatorResultValuesClientData result)
        {
            multipleValues = false;
            if (result.IndicatorCustomCode == "D01")
            {
                multipleValues = true;
                if (listD01 == null)
                    listD01 = new List<DescriptionTimeIndicatorGridControlData>();

                if (data != null)
                {
                    if (result.Date > data.Date)
                    {
                        if (listD01 != null)
                            listD01.Clear();
                        data.Date = result.Date;
                    }
                }

                DescriptionTimeIndicatorGridControlData d01 = new DescriptionTimeIndicatorGridControlData(result);
                int index = listD01.IndexOf(d01);
                if (index > -1)
                {
                    listD01[index] = d01;
                }
                else
                {
                    listD01.Add(d01);
                }
            }
            else if (result.IndicatorCustomCode == "D04")
            {
                multipleValues = true;
                if (listD04 == null)
                    listD04 = new List<DescriptionTimeIndicatorGridControlData>();

                if (data != null)
                {
                    if (result.Date > data.Date)
                    {
                        if (listD04 != null)
                            listD04.Clear();
                        data.Date = result.Date;
                    }
                }

                DescriptionTimeIndicatorGridControlData d04 = new DescriptionTimeIndicatorGridControlData(result);
                int index = listD04.IndexOf(d04);
                if (index > -1)
                {
                    listD04[index] = d04;
                }
                else
                {
                    listD04.Add(d04);
                }
            }
            else if (result.IndicatorCustomCode == "D08")
            {
                multipleValues = true;
                if (listD08 == null)
                    listD08 = new List<DescriptionValuePercentageIndicatorGridControlData>();

                if (data != null)
                {
                    if (result.Date > data.Date)
                    {
                        if (listD08 != null)
                            listD08.Clear();
                        data.Date = result.Date;
                    }
                }

                DescriptionValuePercentageIndicatorGridControlData d08 = new DescriptionValuePercentageIndicatorGridControlData(result);
                int index = listD08.IndexOf(d08);
                if (index > -1)
                {
                    listD08[index] = d08;
                }
                else
                {
                    listD08.Add(d08);
                }
            }
            else if (result.IndicatorCustomCode == "D14")
            {
                multipleValues = true;
                if (listD14 == null)
                    listD14 = new List<DescriptionValuePercentageIndicatorGridControlData>();

                DescriptionValuePercentageIndicatorGridControlData d14 = new DescriptionValuePercentageIndicatorGridControlData(result);
                int index = listD14.IndexOf(d14);
                if (index > -1)
                {
                    listD14[index] = d14;
                }
                else
                {
                    listD14.Add(d14);
                }
            }
            else if (result.IndicatorCustomCode == "D15")
            {
                multipleValues = true;
                if (listD15 == null)
                    listD15 = new List<DescriptionValuePercentageIndicatorGridControlData>();

                DescriptionValuePercentageIndicatorGridControlData d15 = new DescriptionValuePercentageIndicatorGridControlData(result);
                int index = listD15.IndexOf(d15);
                if (index > -1)
                {
                    listD15[index] = d15;
                }
                else
                {
                    listD15.Add(d15);
                }
            }
            else if (result.IndicatorCustomCode == "D27")
            {
                multipleValues = true;
                if (listD27 == null)
                    listD27 = new List<DescriptionTimeIndicatorGridControlData>();

                DescriptionTimeIndicatorGridControlData d27 = new DescriptionTimeIndicatorGridControlData(result);
                int index = listD27.IndexOf(d27);
                if (index > -1)
                {
                    listD27[index] = d27;
                }
                else
                {
                    listD27.Add(d27);
                }
            }
            else if (result.IndicatorCustomCode == "D28")
            {
                multipleValues = true;
                if (listD28 == null)
                    listD28 = new List<DescriptionTimeIndicatorGridControlData>();

                DescriptionTimeIndicatorGridControlData d28 = new DescriptionTimeIndicatorGridControlData(result);
                int index = listD28.IndexOf(d28);
                if (index > -1)
                {
                    listD28[index] = d28;
                }
                else
                {
                    listD28.Add(d28);
                }
            }
            else if (result.IndicatorCustomCode == "D30")
            {
                multipleValues = true;
                if (listD30 == null)
                    listD30 = new List<DescriptionValuePercentageIndicatorGridControlData>();

                DescriptionValuePercentageIndicatorGridControlData d30 = new DescriptionValuePercentageIndicatorGridControlData(result);
                int index = listD30.IndexOf(d30);
                if (index > -1)
                {
                    listD30[index] = d30;
                }
                else
                {
                    listD30.Add(d30);
                }
            }
            else if (result.IndicatorCustomCode == "D31")
            {
                multipleValues = true;
                if (listD31 == null)
                    listD31 = new List<DescriptionValuePercentageIndicatorGridControlData>();

                DescriptionValuePercentageIndicatorGridControlData d31 = new DescriptionValuePercentageIndicatorGridControlData(result);
                int index = listD31.IndexOf(d31);
                if (index > -1)
                {
                    listD31[index] = d31;
                }
                else
                {
                    listD31.Add(d31);
                }
            }
            else if (result.IndicatorCustomCode == "D25")
            {
                multipleValues = true;
                if (listD25 == null)
                    listD25 = new List<DescriptionTimeIndicatorGridControlData>();

                if (data != null)
                {
                    if (result.Date > data.Date)
                    {
                        if (listD25 != null)
                            listD25.Clear();
                        data.Date = result.Date;
                    }
                }

                DescriptionTimeIndicatorGridControlData d25 = new DescriptionTimeIndicatorGridControlData(result);
                int index = listD25.IndexOf(d25);
                if (index > -1)
                {
                    listD25[index] = d25;
                }
                else
                {
                    listD25.Add(d25);
                }
            }
            else if (result.IndicatorCustomCode == "D42")
            {
                multipleValues = true;
                if (listD42 == null)
                    listD42 = new List<DescriptionValuePercentageTableIndicatorGridControlData>();

                DescriptionValuePercentageTableIndicatorGridControlData d42 = new 
                    DescriptionValuePercentageTableIndicatorGridControlData(result);
                int index = listD42.IndexOf(d42);
                if (index > -1)
                {
                    listD42[index] = d42;
                }
                else
                {
                    listD42.Add(d42);
                }                
            }
            else if (result.IndicatorCustomCode == "D43")
            {
                multipleValues = true;
                if (listD43 == null)
                    listD43 = new List<DescriptionValuePercentageTableIndicatorGridControlData>();

                DescriptionValuePercentageTableIndicatorGridControlData d43 = new
                    DescriptionValuePercentageTableIndicatorGridControlData(result);
                int index = listD43.IndexOf(d43);
                if (index > -1)
                {
                    listD43[index] = d43;
                }
                else
                {
                    listD43.Add(d43);
                }
            }
            else if (result.IndicatorCustomCode == "D44")
            {
                multipleValues = true;
                if (listD44 == null)
                    listD44 = new List<DescriptionValuePercentageTableIndicatorGridControlData>();

                DescriptionValuePercentageTableIndicatorGridControlData d44 = new
                    DescriptionValuePercentageTableIndicatorGridControlData(result);
                int index = listD44.IndexOf(d44);
                if (index > -1)
                {
                    listD44[index] = d44;
                }
                else
                {
                    listD44.Add(d44);
                }
            }       
        }

        public override string ToString()
        {
            return Data.IndicatorCustomCode;
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is IndicatorGridControlData)
            {
                result = this.Name == ((IndicatorGridControlData)obj).Name;
            }
            return result;
        }
    }

    public class DescriptionTimeIndicatorGridControlData
    {
        private IndicatorResultValuesClientData data;
        private List<double> values;

        public DescriptionTimeIndicatorGridControlData(IndicatorResultValuesClientData indicatorResultValuesClientData)
        {
            data = indicatorResultValuesClientData;
            values = ApplicationUtil.ConvertIndicatorResultValuesFromString(data.ResultValue);
        }

        public string Description
        {
            get
            {
                return data.Description;
            }
        }

        public string Time
        {
            get
            {                
                return ApplicationUtil.GetTimeSpanCustomString(TimeSpan.FromSeconds(values[0]));
            }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is DescriptionTimeIndicatorGridControlData)
            {
                result = this.Description == ((DescriptionTimeIndicatorGridControlData)obj).Description;
            }
            return result;
        }
    }

    public class DescriptionValuePercentageIndicatorGridControlData
    {
        private IndicatorResultValuesClientData data;
        private List<double> values;

        public DescriptionValuePercentageIndicatorGridControlData(IndicatorResultValuesClientData indicatorResultValuesClientData)
        {
            data = indicatorResultValuesClientData;
            values = ApplicationUtil.ConvertIndicatorResultValuesFromString(data.ResultValue);
        }

        public string Description
        {
            get
            {
                return data.Description;
            }
        }

        public double RealValue
        {
            get
            {
                return values[0];
            }
        }

        public double Percentage
        {
            get
            {
                if (values.Count > 1)
                {
                    return values[1];
                }
                else
                    return 0;
            }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is DescriptionValuePercentageIndicatorGridControlData)
            {
                result = this.Description == ((DescriptionValuePercentageIndicatorGridControlData)obj).Description;
            }
            return result;
        }
    }


    public class DescriptionValuePercentageTableIndicatorGridControlData
    {
        private IndicatorResultValuesClientData data;
        private List<double> values;

        public DescriptionValuePercentageTableIndicatorGridControlData(IndicatorResultValuesClientData indicatorResultValuesClientData)
        {
            data = indicatorResultValuesClientData;
            values = ApplicationUtil.ConvertIndicatorResultValuesFromString(data.ResultValue);
        }

        public string Description
        {
            get
            {
                return data.Description;
            }
        }

        public double AssignedValue
        {
            get
            {
                return values[0];
            }
        }
        public double AssignedPercentage
        {
            get
            {
                return values[1];
            }
        }

        public double PendingValue
        {
            get
            {
                return values[2];
            }
        }
        public double PendingPercentage
        {
            get
            {
                return values[3];
            }
        }

        public double DelayedValue
        {
            get
            {
                return values[4];
            }
        }
        public double DelayedPercentage
        {
            get
            {
                return values[5];
            }
        }

        public double InProgressValue
        {
            get
            {
                return values[6];
            }
        }
        public double InProgressPercentage
        {
            get
            {
                return values[7];
            }
        }

        public double NewValue
        {
            get
            {
                return values[8];
            }
        }
        public double NewPercentage
        {
            get
            {
                return values[9];
            }
        }        

        public double TotalValue
        {
            get
            {
                return values[10];
            }
        }

        public double TotalValuePercentage
        {
            get
            {
                return values[11];
            }
        }


        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is DescriptionValuePercentageTableIndicatorGridControlData)
            {
                result = this.Description == ((DescriptionValuePercentageTableIndicatorGridControlData)obj).Description;
            }
            return result;
        }
    }


    public class DescriptionPercentageIndicatorGridControlData
    {
        private IndicatorResultValuesClientData data;
        private List<double> values;

        public DescriptionPercentageIndicatorGridControlData(IndicatorResultValuesClientData indicatorResultValuesClientData)
        {
            data = indicatorResultValuesClientData;
            values = ApplicationUtil.ConvertIndicatorResultValuesFromString(data.ResultValue);
        }

        public string Description
        {
            get
            {
                return data.Description;
            }
        }

        public double Percentage
        {
            get
            {
                return values[0];
            }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is DescriptionPercentageIndicatorGridControlData)
            {
                result = this.Description == ((DescriptionPercentageIndicatorGridControlData)obj).Description;
            }
            return result;
        }
    }

    public class IndicatorGridFocusedRowChangedEventArgs
    {
        private IList list;
        private bool clear;
        public IList List
        {
            get
            {
                return list;
            }
            set 
            {
                list = value;
            }
        }
        public bool Clear
        {
            get
            {
                return clear;
            }
            set
            {
                clear = value;
            }
        }    
    }

    public class IndicatorOperatorFormChangeEventArgs
    {
        private string name;
        private string customCode;
        private bool show;
        public string Name
        {
            get { return name; }
            set { name = value; }                            
        }
        public string CustomCde
        {
            get { return customCode; }
            set { customCode = value; }
        }
        public bool Show
        {
            get { return show; }
            set { show = value; }
        }
    }
}
