﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraEditors;
using SmartCadGuiCommon.Controls;
using SmartCadCore.ClientData;
using SmartCadControls;
using SmartCadCore.Common;
using SmartCadCore.Enums;
using SmartCadCore.Core;


namespace SmartCadGuiCommon
{
	public class GridControlDataAdmRoute : GridControlDataAdm
	{
		public GridControlDataAdmRoute(RouteClientData route)
			: base(route)
		{
            ApplicationOwner.Add(SmartCadApplications.SmartCadAdministration);
		}

		public RouteClientData Route
		{
			get
			{
				return this.Tag as RouteClientData;
			}
		}

		[GridControlRowInfoData(Searchable = true)]
		public string Name
		{
			get
			{
				return Route.Name;
			}

		}

		[GridControlRowInfoData(Searchable = true)]
		public string Department
		{
			get
			{
				return Route.Department.Name;
			}
		}

		[GridControlRowInfoData(Searchable = true)]
		public string CustomCode
		{
			get
			{
				return Route.CustomCode;
			}
		}

		[GridControlRowInfoData(Searchable = true)]
		public string Type
		{
			get
			{
				return ResourceLoader.GetString2(Route.Type.ToString());
			}
		}

		public override bool Equals(object obj)
		{
			bool result = false;
			if (obj is GridControlDataAdmRoute)
			{
				result = Route.Code == (obj as GridControlDataAdmRoute).Route.Code;
			}
			return result;
		}

		#region Override GridControlDataAdm

		public override System.Windows.Forms.Form GetForm(AdministrationRibbonForm form, ClientData data, FormBehavior behaviour)
		{
			return new RouteForm(form, data as RouteClientData, behaviour);
		}

		public override string GetDeletedMessage
		{
			get { return ResourceLoader.GetString2("DeleteRouteMessage"); }
		}

		public override string GetNotFoundMessage
		{
			get { return ResourceLoader.GetString2("DeleteRouteData"); }
		}

		public override bool ShowPreview
		{
			get { return false; }
		}

		public override bool PageSearch
		{
			get { return false; }
		}

		public override bool InitializeCollections
		{
			get { return false; }
		}

		public override bool Refresh
		{
			get { return true; }
		}

		public override string GroupText
		{
			get { return ResourceLoader.GetString2("Routes"); }
		}

		public override string Query
		{
			get { return SmartCadHqls.GetRoutes; }
		}

        protected override System.Drawing.Image ViewImage
        {
            get
            {
                return ResourceLoader.GetImage("$Image.Route");
            }
        }

        //BATAAN VERSION AA
        public override bool Visible
        {
            get
            {
                return false;
            }
        }

        public override string PageText
        {
            get { return ResourceLoader.GetString("MobileUnits"); }
        }

        protected override int PageOrder
        {
            get { return 4; }
        }

        protected override UserResourceClientData Resource
        {
            get { return RouteClientData.Resource; }
        }

        protected override string ViewToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ViewRoute");
            }
        }

        protected override string AddToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_CreateRoute");
            }
        }

        protected override string DeleteToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_DeleteRoute");
            }
        }

        protected override string EditToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ModifyRoute");
            }
        }

        protected override int GroupOrder
        {
            get
            {
                return 4;
            }
        }

		#endregion
	}
}

