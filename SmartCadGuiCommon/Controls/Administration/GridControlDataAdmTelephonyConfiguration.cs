﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadControls;
using SmartCadCore.Enums;
using Smartmatic.SmartCad.Service;

namespace SmartCadGuiCommon.Controls
{
    class GridControlDataAdmTelephonyConfiguration : GridControlDataAdm
    {
        public GridControlDataAdmTelephonyConfiguration(TelephonyConfigurationClientData telephony)
            : base(telephony)
        {
            if (!ServerServiceClient.GetInstance().CheckVirtualCall())
                ApplicationOwner.Add(SmartCadApplications.SmartCadAdministration);
            else
                ApplicationOwner.Add(SmartCadApplications.SmartCadTelephony);
        }

        #region Properties
        public TelephonyConfigurationClientData Telephony
        {
            get
            {
                return this.Tag as TelephonyConfigurationClientData;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string Computer
        {
            get
            {
                return Telephony.COMPUTER;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public int Extension
        {
            get
            {
                return Telephony.Extension;
            }
        }

        [GridControlRowInfoData(Searchable = true, Visible = false)]
        public string Password
        {
            get
            {
                return Telephony.Password;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string Virtual
        {
            get
            {
                if (Telephony.Virtual)
                    return ResourceLoader.GetString2("Yes");
                else
                    return ResourceLoader.GetString2("No");
            }
        }
        #endregion

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataAdmTelephonyConfiguration)
            {
                result = Telephony.Code == (obj as GridControlDataAdmTelephonyConfiguration).Telephony.Code;
            }
            return result;
        }

        #region GridControlDataAdm
        public override System.Windows.Forms.Form GetForm(AdministrationRibbonForm form, ClientData data, FormBehavior behaviour)
        {
            return new TelephonyConfigurationForm(form, data as TelephonyConfigurationClientData, behaviour);
        }

        public override string GetDeletedMessage
        {
            get { return ResourceLoader.GetString2("DeleteTelephonyConfiguration"); }
        }

        public override string GetNotFoundMessage
        {
            get { return ResourceLoader.GetString2("DeleteTelephonyConfigurationData"); }
        }

        public override bool ShowPreview
        {
            get { return false; }
        }

        public override bool PageSearch
        {
            get { return false; }
        }

        public override bool InitializeCollections
        {
            get { return false; }
        }

        public override bool Refresh
        {
            get { return true; }
        }

        public override string GroupText
        {
            get { return ResourceLoader.GetString("TelephonyConfiguration"); }
        }

        public override string PageText
        {
            get { return ResourceLoader.GetString("Configuration"); }
        }

        protected override int PageOrder
        {
            get { return 0; }
        }

        protected override int GroupOrder
        {
            get { return 1; }
        }

        public override string Query
        {
            get { return SmartCadHqls.GetAllTelephonyConfiguration; }
        }

        protected override UserResourceClientData Resource
        {
            get { return TelephonyConfigurationClientData.Resource; }
        }

        protected override System.Drawing.Image ViewImage
        {
            get
            {
                return ResourceLoader.GetImage("TelephoneConfigurationIcon");
            }
        }

        protected override string ViewToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ViewTelephonyConfiguration");
            }
        }

        protected override string AddToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_CreateTelephonyConfiguration");
            }
        }

        protected override string DeleteToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_DeleteTelephonyConfiguration");
            }
        }

        protected override string EditToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ModifyTelephonyConfiguration");
            }
        }
        #endregion
    }
}
