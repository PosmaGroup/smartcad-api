﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadControls;
using Smartmatic.SmartCad.Service;
using SmartCadCore.Enums;

namespace SmartCadGuiCommon.Controls
{
    public class GridControlDataAdmUnitOfficerAssignment : GridControlDataAdm
    {
        public GridControlDataAdmUnitOfficerAssignment(DepartmentStationAssignHistoryClientData assignment)
            : base(assignment)
        {
            ApplicationOwner.Add(SmartCadApplications.SmartCadAdministration);
        }

        public DepartmentStationAssignHistoryClientData Assignment
        {
            get
            {
                return this.Tag as DepartmentStationAssignHistoryClientData;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string DepartmentType
        {
            get
            {
                return Assignment.DepartmentStation.DepartamentTypeName;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string DepartmentZone
        {
            get
            {
                //TODO:quitar
                if (Assignment.DepartmentStation.DepartmentZone != null)
                    return Assignment.DepartmentStation.DepartmentZone.Name;
                return "";
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string DepartmentStation
        {
            get
            {
                return Assignment.DepartmentStation.Name;
            }
        }

        [GridControlRowInfoData(DisplayFormat = "MM/dd/yyyy HH:mm tt", Searchable = true)]
        public DateTime StartDate
        {
            get
            {
                return Assignment.StartDate;
            }
        }

        [GridControlRowInfoData(DisplayFormat = "MM/dd/yyyy HH:mm tt", Searchable = true)]
        public DateTime EndDate
        {
            get
            {
                return Assignment.EndDate;
            }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataAdmUnitOfficerAssignment)
            {
                result = Assignment.Code.Equals((obj as GridControlDataAdmUnitOfficerAssignment).Assignment.Code);
            }
            return result;
        }

        protected override int GroupOrder
        {
            get
            {
                return 5;
            }
        }

        #region GridControlDataAdm
        public override System.Windows.Forms.Form GetForm(AdministrationRibbonForm form, ClientData data, FormBehavior behaviour)
        {
            return new UnitOfficerForm(form, data as DepartmentStationAssignHistoryClientData, behaviour);
        }

        public override string GetDeletedMessage
        {
            get { return ResourceLoader.GetString2("DeleteUnitOfficer"); }
        }

        public override string GetNotFoundMessage
        {
            get { return ResourceLoader.GetString2("DeleteDepartmentStationAssignHistoryData"); }
        }

        public override bool ShowPreview
        {
            get { return false; }
        }

        public override bool PageSearch
        {
            get { return true; }
        }

        public override bool InitializeCollections
        {
            get { return true; }
        }

        public override bool Refresh
        {
            get { return true; }
        }

        public override string GroupText
        {
            get { return ResourceLoader.GetString2("UnitOfficer"); }
        }

        public override string Query
        {
            get
            {
                string now = ServerServiceClient.GetInstance().GetTime().ToString("yyyyMMdd HH:mm:ss");
                string queryHQL = SmartCadHqls.GetCustomHql(SmartCadHqls.GetAllDepartmentStationAssignments, now); 
                return queryHQL; 
            }
        }

        protected override System.Drawing.Image ViewImage
        {
            get
            {
                return ResourceLoader.GetImage("$Image.Assigment");
            }
        }

        public override string PageText
        {
            get
            {
                return ResourceLoader.GetString("Dispatch");
            }
        }

        protected override int PageOrder
        {
            get { return 5; }
        }

        protected override UserResourceClientData Resource
        {
            get
            {
                return UnitOfficerAssignHistoryClientData.Resource;
            }
        }

        protected override string ViewToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ViewAssigment");
            }
        }

        protected override string AddToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_CreateAssignment");
            }
        }

        protected override string DeleteToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_DeleteAssignment");
            }
        }

        protected override string EditToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ModifyAssignment");
            }
        }
        #endregion
    }
}
