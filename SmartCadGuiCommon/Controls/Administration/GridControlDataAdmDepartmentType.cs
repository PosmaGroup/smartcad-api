﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadControls;
using SmartCadCore.Enums;

namespace SmartCadGuiCommon.Controls
{
    public class GridControlDataAdmDepartmentType : GridControlDataAdm
    {
        public GridControlDataAdmDepartmentType(DepartmentTypeClientData departmentType)
            : base(departmentType)
        {
            ApplicationOwner.Add(SmartCadApplications.SmartCadAdministration);
        }

        public DepartmentTypeClientData DepartmentType
        {
            get
            {
                return this.Tag as DepartmentTypeClientData;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string Name
        {
            get
            {
                return DepartmentType.Name;
            }

        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataAdmDepartmentType)
            {
                result = DepartmentType.Code == (obj as GridControlDataAdmDepartmentType).DepartmentType.Code;
            }
            return result;
        }

        #region Override GridControlDataAdm

        public override System.Windows.Forms.Form GetForm(AdministrationRibbonForm form, ClientData data, FormBehavior behaviour)
        {
            return new DepartmentTypeXtraForm(form, data as DepartmentTypeClientData, behaviour);
        }

        public override string GetDeletedMessage
        {
            get { return ResourceLoader.GetString("DeleteDepartmentTypeMessage"); }
        }

        public override string GetNotFoundMessage
        {
            get { return ResourceLoader.GetString2("DeleteDepartmentTypeData"); }
        }

        public override bool ShowPreview
        {
            get { return false; }
        }

        public override bool PageSearch
        {
            get { return true; }
        }

        public override bool InitializeCollections
        {
            get { return false; }
        }

        public override bool Refresh
        {
            get { return false; }
        }

        public override string GroupText
        {
            get { return ResourceLoader.GetString("DepartmentsAdmGroupText"); }
        }

        public override string Query
        {
            get { return SmartCadHqls.GetDepartmentsType; }
        }

        protected override System.Drawing.Image ViewImage
        {
            get
            {
                return ResourceLoader.GetImage("$Image.DepartmentType");
            }
        }

        public override string PageText
        {
            get { return ResourceLoader.GetString("DepartmentsAdmPageText"); }
        }

        protected override int PageOrder
        {
            get { return 3; }
        }

        protected override UserResourceClientData Resource
        {
            get { return DepartmentTypeClientData.Resource; }
        }

        protected override string ViewToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ViewDepartmentType");
            }
        }

        protected override string AddToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_CreateDepartmentType");
            }
        }

        protected override string DeleteToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_DeleteDepartmentType");
            }
        }

        protected override string EditToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ModifyDepartmentType");
            }
        }

        protected override int GroupOrder
        {
            get
            {
                return 0;
            }
        }
        #endregion
    }
}
