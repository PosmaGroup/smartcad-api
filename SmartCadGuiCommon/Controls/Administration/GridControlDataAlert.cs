﻿using DevExpress.XtraEditors.Repository;
using SmartCadControls;
using SmartCadControls.Controls;
using SmartCadCore.ClientData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadGuiCommon.Controls.Administration
{
    public class GridControlDataAlert : GridControlData
    {

        private bool check;
        private int maxValue;
        private string priority;
        private int priorityValue;
        private int departmentAlertCode;
        private int departmentAlertVersion;

        public GridControlDataAlert(AlertClientData alert)
            : base(alert)
        {

        }

        public AlertClientData Alert
        {
            get { return this.Tag as AlertClientData; }
        }

        public int Code
        {
            get { return Alert.Code; }
        }

        public int DepartmentAlertCode
        {
            get { return departmentAlertCode; }
            set { departmentAlertCode = value; }
        }

        public int DepartmentAlertVersion
        {
            get { return departmentAlertVersion; }
            set { departmentAlertVersion = value; }
        }

        [GridControlRowInfoData(Visible = true, RepositoryType = typeof(RepositoryItemCheckEdit), Searchable = false)]
        public bool Checked
        {
            get { return check; }
            set { check = value; }
        }

        [GridControlRowInfoData(Visible = true)]
        public string Name
        {
            get { return Alert.Name; }
        }

        public string CustomCode
        {
            get { return Alert.CustomCode; }
        }

        [GridControlRowInfoData(Visible = true, RepositoryType = typeof(RepositoryItemComboBox))]
        public string Priority
        {
            get
            {
                return priority;
            }
            set
            {
                priority = value;
            }

        }

        public int PriorityValue
        {
            get
            {
                return priorityValue;
            }
            set
            {
                priorityValue = value;
            }

        }

        [GridControlRowInfoData(Visible = true, RepositoryType = typeof(RepositoryItemSpinEdit))]
        public int MaxValue
        {
            get
            {
                return maxValue;
            }
            set
            {
                maxValue = value;
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string Measure
        {
            get
            {
                return Alert.Measure;
            }
        }


        public override bool Equals(object obj)
        {
            bool result = false;
            GridControlDataAlert var = obj as GridControlDataAlert;
            if (var != null)
            {
                AlertClientData alert = (AlertClientData)var.Alert;
                if (this.Alert.Equals(alert) == true)
                    result = true;
            }
            return result;

        }

    }
}
