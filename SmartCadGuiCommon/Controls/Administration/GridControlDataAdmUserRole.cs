﻿using SmartCadControls;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadCore.Enums;
using SmartCadGuiCommon;
using SmartCadGuiCommon.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Smartmatic.SmartCad.Gui
{
    public class GridControlDataAdmUserRole : GridControlDataAdm
    {
        public GridControlDataAdmUserRole(UserRoleClientData role)
            : base(role)
        {
            ApplicationOwner.Add(SmartCadApplications.SmartCadAdministration);
        }

        public UserRoleClientData Role
        {
            get
            {
                return this.Tag as UserRoleClientData;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string RoleName
        {
            get
            {
                return Role.FriendlyName;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string RoleDescription
        {
            get
            {
                return Role.Description;
            }
        }

        protected override System.Drawing.Image ViewImage
        {
            get
            {
                return ResourceLoader.GetImage("$Image.UserRole");
            }
        }

        public override string PageText
        {
            get
            {
                return ResourceLoader.GetString("UserAccounts");
            }
        }

        protected override int PageOrder
        {
            get { return 1; }
        }

        protected override UserResourceClientData Resource
        {
            get
            {
                return UserRoleClientData.Resource;
            }
        }

        protected override string ViewToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ViewRole");
            }
        }

        protected override string AddToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_CreateRole");
            }
        }

        protected override string DeleteToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_DeleteRole");
            }
        }

        protected override string EditToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ModifyRole");
            }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataAdmUserRole)
            {
                result = Role.Code == (obj as GridControlDataAdmUserRole).Role.Code;
            }
            return result;
        }

        #region GridControlDataAdm
        public override System.Windows.Forms.Form GetForm(AdministrationRibbonForm form, ClientData data, FormBehavior behaviour)
        {
            return new RoleForm(form, data as UserRoleClientData, behaviour);
        }

        public override string GetDeletedMessage
        {
            get { return ResourceLoader.GetString2("DeleteRoleMessage"); }
        }

        public override string GetNotFoundMessage
        {
            get { return ResourceLoader.GetString2("DeleteUserRoleData"); }
        }

        public override bool ShowPreview
        {
            get { return false; }
        }

        public override bool PageSearch
        {
            get { return false; }
        }

        public override bool InitializeCollections
        {
            get { return false; }
        }

        public override bool Refresh
        {
            get { return false; }
        }

        public override string GroupText
        {
            get { return ResourceLoader.GetString2("Role"); }
        }

        public override string Query
        {
            get { return SmartCadHqls.GetUserRoles; }
        }

        protected override int GroupOrder
        {
            get
            {
                return 1;
            }
        }
        #endregion
    }
}
