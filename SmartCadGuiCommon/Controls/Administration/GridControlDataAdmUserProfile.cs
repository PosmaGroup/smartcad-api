﻿using SmartCadControls;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadCore.Enums;
using SmartCadGuiCommon;
using SmartCadGuiCommon.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Smartmatic.SmartCad.Gui
{
    public class GridControlDataAdmUserProfile : GridControlDataAdm
    {
        public GridControlDataAdmUserProfile(UserProfileClientData profile)
            : base(profile)
        { ApplicationOwner.Add(SmartCadApplications.SmartCadAdministration); }

        public UserProfileClientData Profile
        {
            get
            {
                return this.Tag as UserProfileClientData;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string ProfileName
        {
            get
            {
                return Profile.FriendlyName;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string ProfileDescription
        {
            get
            {
                return Profile.Description;
            }
        }

        protected override System.Drawing.Image ViewImage
        {
            get
            {
                return ResourceLoader.GetImage("$Image.UserProfile");
            }
        }

        public override string PageText
        {
            get
            {
                return ResourceLoader.GetString("UserAccounts");
            }
        }

        protected override int PageOrder
        {
            get { return 1; }
        }

        protected override UserResourceClientData Resource
        {
            get
            {
                return UserProfileClientData.Resource;
            }
        }

        protected override string ViewToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ViewProfile");
            }
        }

        protected override string AddToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_CreateProfile");
            }
        }

        protected override string DeleteToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_DeleteProfile");
            }
        }

        protected override string EditToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_EditProfile");
            }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataAdmUserProfile)
            {
                result = Profile.Code == (obj as GridControlDataAdmUserProfile).Profile.Code;
            }
            return result;
        }

        #region GridControlDataAdm
        public override System.Windows.Forms.Form GetForm(AdministrationRibbonForm form, ClientData data, FormBehavior behaviour)
        {
            return new ProfileForm(form, data as UserProfileClientData, behaviour);
        }

        public override string GetDeletedMessage
        {
            get { return ResourceLoader.GetString2("DeleteProfileMessage"); }
        }

        public override string GetNotFoundMessage
        {
            get { return ResourceLoader.GetString2("DeleteUserProfileData"); }
        }

        public override bool ShowPreview
        {
            get { return false; }
        }

        public override bool PageSearch
        {
            get { return false; }
        }

        public override bool InitializeCollections
        {
            get { return false; }
        }

        public override bool Refresh
        {
            get { return false; }
        }

        public override string GroupText
        {
            get { return ResourceLoader.GetString2("Profile"); }
        }

        public override string Query
        {
            get { return SmartCadHqls.GetUserProfiles; }
        }

        protected override int GroupOrder
        {
            get
            {
                return 0;
            }
        }
        #endregion
    }
}
