using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using SmartCadGuiCommon.Controls;
using SmartCadCore.ClientData;
using SmartCadControls;
using SmartCadCore.Common;
using SmartCadGuiCommon;
using SmartCadCore.Enums;
using SmartCadCore.Core;


namespace Smartmatic.SmartCad.Gui
{
    public class GridControlDataAdmOperatorStatus : GridControlDataAdm
    {
        public GridControlDataAdmOperatorStatus(OperatorStatusClientData status)
            : base(status)
        {
            ApplicationOwner.Add(SmartCadApplications.SmartCadAdministration);
        }

        public OperatorStatusClientData Status
        {
            get
            {
                return this.Tag as OperatorStatusClientData;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string OperatorStatusName
        {
            get
            {
                return Status.FriendlyName;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string OperatorStatusType
        {
            get
            {
                if (Status.NotReady.Value == true)
                    return ResourceLoader.GetString2("NotAvailable");
                else
                    return ResourceLoader.GetString2("Available");
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string OperatorStatusPause
        {
            get
            {
                if (Status.IsPause())
                {
                    return ResourceLoader.GetString2("$Message.Yes");
                }
                else
                {
                    return ResourceLoader.GetString2("$Message.No");
                }
            }
        }

        [GridControlRowInfoData(Searchable = false)]
        public Color OperatorStatusColor
        {
            get
            {
                return Status.Color;
            }
        }

        protected override System.Drawing.Image ViewImage
        {
            get
            {
                return ResourceLoader.GetImage("$Image.OperatorStatus");
            }
        }

        public override string PageText
        {
            get { return ResourceLoader.GetString("UserAccounts"); }
        }

        protected override int PageOrder
        {
            get { return 1; }
        }

        protected override UserResourceClientData Resource
        {
            get { return OperatorStatusClientData.Resource; }
        }

        protected override string ViewToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ViewStatus");
            }
        }

        protected override string EditToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ModifyStatus");
            }
        }

        protected override List<DevExpress.XtraBars.BarButtonItem> CreateBasicButtons()
        {
            Items = new List<DevExpress.XtraBars.BarButtonItem>();

            Items.Add(CreateViewButton());

            Items.Add(CreateEditButton());

            return Items;
        }
        
        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataAdmOperatorStatus)
            {
                result = Status.Code == (obj as GridControlDataAdmOperatorStatus).Status.Code;
            }
            return result;
        }

        #region GridControlDataAdm
        public override System.Windows.Forms.Form GetForm(AdministrationRibbonForm form,ClientData data, FormBehavior behaviour)
        {
            return new OperatorStatusForm(form, data as OperatorStatusClientData, behaviour);
        }

        public override string GetDeletedMessage
        {
            get { throw new NotImplementedException(); }
        }

        public override string GetNotFoundMessage
        {
            get { throw new NotImplementedException(); }
        }

        public override bool ShowPreview
        {
            get { return false; }
        }

        public override bool PageSearch
        {
            get { return false; }
        }

        public override bool InitializeCollections
        {
            get { return false; }
        }

        public override bool Refresh
        {
            get { return false; }
        }

        public override string GroupText
        {
            get { return ResourceLoader.GetString2("OperatorStatus"); }
        }

        public override string Query
        {
            get { return SmartCadHqls.GetOperatorStatus; }
        }

        protected override int GroupOrder
        {
            get
            {
                return 3;
            }
        }
        #endregion
    }
}
