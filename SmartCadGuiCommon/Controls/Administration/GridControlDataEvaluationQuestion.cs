﻿using DevExpress.XtraEditors.Repository;
using SmartCadControls;
using SmartCadControls.Controls;
using SmartCadCore.ClientData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadGuiCommon.Controls.Administration
{
    public class GridControlDataEvaluationQuestion : GridControlData
    {
        private bool check;
        private int maxValue;
        private string priority;
        private int priorityValue;
        private int departmentAlertCode;
        private int departmentAlertVersion;

        public GridControlDataEvaluationQuestion(EvaluationQuestionClientData question)
            : base(question)
        {

        }

        public EvaluationQuestionClientData Question
        {
            get { return this.Tag as EvaluationQuestionClientData; }
        }

        public int Code
        {
            get { return Question.Code; }
        }


        [GridControlRowInfoData(Visible = true, RepositoryType = typeof(RepositoryItemTextEdit), Width = 270, Searchable = true)]
        public string Name
        {
            get { return Question.Name; }
            set { Question.Name = value; }
        }

        [GridControlRowInfoData(Visible = true, RepositoryType = typeof(RepositoryItemSpinEdit), Width = 120, Searchable = true, DisplayFormat = "f")]
        public float Weighting
        {
            get
            {
                return Question.Weighting;
            }
            set
            {
                Question.Weighting = value;
            }
        }


        public override bool Equals(object obj)
        {
            bool result = false;
            GridControlDataEvaluationQuestion var = obj as GridControlDataEvaluationQuestion;
            if (var != null)
            {
                EvaluationQuestionClientData question = (EvaluationQuestionClientData)var.Question;
                if (this.Question.Equals(question) == true)
                    result = true;
            }
            return result;

        }

    }
}
