﻿using SmartCadControls;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadCore.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace SmartCadGuiCommon.Controls
{
    public class GridControlDataAdmRoom : GridControlDataAdm
    {
        public GridControlDataAdmRoom(RoomClientData room)
            : base(room)
        {
            ApplicationOwner.Add(SmartCadApplications.SmartCadSupervision);
        }

        public RoomClientData Room
        {
            get
            {
                return this.Tag as RoomClientData;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string RoomName
        {
            get
            {
                return Room.Name;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string RoomDescription
        {
            get
            {
                return Room.Description;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public int RoomSeats
        {
            get
            {
                return Room.Seats;
            }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataAdmRoom)
            {
                result = Room.Code == (obj as GridControlDataAdmRoom).Room.Code;
            }
            return result;
        }

        #region GridControlDataAdm
        public override System.Windows.Forms.Form GetForm(AdministrationRibbonForm form, ClientData data, FormBehavior behaviour)
        {
            return new RoomAdministrationForm(form, data as RoomClientData, behaviour);
        }

        public override string GetDeletedMessage
        {
            get { return ResourceLoader.GetString2("DeleteRoom"); }
        }

        public override string GetNotFoundMessage
        {
            get { return ResourceLoader.GetString2("DeleteRoomData"); }
        }

        public override bool ShowPreview
        {
            get { return false; }
        }

        public override bool PageSearch
        {
            get { return false; }
        }

        public override bool InitializeCollections
        {
            get { return true; }
        }

        public override bool Refresh
        {
            get { return true; }
        }

        public override string GroupText
        {
            get { return ResourceLoader.GetString2("Rooms"); }
        }

        public override string Query
        {
            get { return SmartCadHqls.GetAllRooms; }
        }

        protected override System.Drawing.Image ViewImage
        {
            get
            {
                return ResourceLoader.GetImage("$Image.Room");
            }
        }

        public override string PageText
        {
            get
            {
                return ResourceLoader.GetString("Supervision");
            }
        }

        protected override int PageOrder
        {
            get { return 7; }
        }

        protected override UserResourceClientData Resource
        {
            get
            {
                return RoomClientData.Resource;
            }
        }

        protected override string ViewToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ViewAdmRoom");
            }
        }

        protected override string AddToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_CreateRoom");
            }
        }

        protected override string DeleteToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_DeleteRoom");
            }
        }

        protected override string EditToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ModifyRoom");
            }
        }

        protected override int GroupOrder
        {
            get
            {
                return 0;
            }
        }
        #endregion
    }
}
