﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using Smartmatic.SmartCad.Service;
using DevExpress.XtraBars;
using SmartCadGuiCommon.Controls;
using SmartCadCore.ClientData;
using SmartCadControls;
using SmartCadGuiCommon;
using SmartCadCore.Common;
using SmartCadCore.Enums;
using SmartCadCore.Core;
using Smartmatic.SmartCad.Gui.Classes;

namespace Smartmatic.SmartCad.Gui
{
    public class GridControlDataAdmCamera : GridControlDataAdm
    {
        public GridControlDataAdmCamera(CameraClientData camera)
            : base(camera)
        {
            ApplicationOwner.Add(SmartCadApplications.SmartCadCCTV);
        }

        public CameraClientData Camera
        {
            get
            {
                return this.Tag as CameraClientData;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string CameraName
        {
            get
            {
                return Camera.Name;
            }

        }

        [GridControlRowInfoData(Searchable = true)]
        public string CameraIp
        {
            get
            {
                return Camera.Ip;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string CameraIpServer
        {
            get
            {
                return Camera.IpServer;
            }
        }

        //Deprecated. Was used in manual mode.
        //[GridControlRowInfoData(Searchable = true)]
        //public string CameraType
        //{
        //    get
        //    {
        //        return Camera.Type.Name;
        //    }
        //}

        [GridControlRowInfoData(Searchable = true)]
        public string CCTVZone
        {
            get
            {
                string zone = string.Empty;
                if (Camera.StructClientData != null && Camera.StructClientData.CctvZone != null)
                {
                    zone = Camera.StructClientData.CctvZone.Name;
                }
                return zone;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string CameraStruct
        {
            get
            {
                if (Camera.StructClientData != null)
                    return Camera.StructClientData.Name;
                else
                    return string.Empty;
            }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataAdmCamera)
            {
                result = Camera.Code == (obj as GridControlDataAdmCamera).Camera.Code;
            }
            return result;
        }

        #region GridControlDataAdm
        public override System.Windows.Forms.Form GetForm(AdministrationRibbonForm form, ClientData data, FormBehavior behaviour)
        {
            return new CameraForm(form, data as CameraClientData, behaviour);
        }

        public override string GetDeletedMessage
        {
            get { return ResourceLoader.GetString2("DeleteCameraMessage"); }
        }

        public override string GetNotFoundMessage
        {
            get { return ResourceLoader.GetString2("DeleteCameraData"); }
        }

        public override bool ShowPreview
        {
            get { return false; }
        }

        public override bool PageSearch
        {
            get { return false; }
        }

        public override bool InitializeCollections
        {
            get { return false; }
        }

        public override bool Refresh
        {
            get { return true; }
        }

        public override string GroupText
        {
            get { return ResourceLoader.GetString2("Cameras"); }
        }

        public override string Query
        {
            get { return SmartCadHqls.GetAllCameras; }
        }
        protected override System.Drawing.Image ViewImage
        {
            get
            {
                return ResourceLoader.GetImage("$Image.Camera");
            }
        }

        public override string PageText
        {
            get
            {
                return ResourceLoader.GetString("Cctv");
            }
        }

        protected override int PageOrder
        {
            get { return 6; }
        }

        protected override UserResourceClientData Resource
        {
            get
            {
                return CameraClientData.Resource;
            }
        }

        protected override string ViewToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ViewCameras");
            }
        }

        protected override string AddToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_CreateCamera");
            }
        }

        protected override string DeleteToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_DeleteCamera");
            }
        }

        protected override string EditToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ModifyCamera");
            }
        }

        protected override List<DevExpress.XtraBars.BarButtonItem> CreateBasicButtons()
        {
            //LM: Check VMS type
            if (AdministrationForm.Configuration.VmsServerIp == "")
            {
                return base.CreateBasicButtons();
            }
            else
            {
                Items = new List<DevExpress.XtraBars.BarButtonItem>();

                Items.Add(CreateViewButton());

                Items.Add(CreateEditButton());

                Items.Add(CreateButton(ImportCamerasCaption, null, ImportCamerasImage, ImportCamerasClick, false, Resource, UserActionClientData.Insert, System.Windows.Forms.Shortcut.CtrlI, ImportCamerasToolTip));

                return Items;
            }
        }

        protected override int GroupOrder
        {
            get
            {
                return 2;
            }
        }

        #region Import Cameras
        private string ImportCamerasCaption { get { return ResourceLoader.GetString2("ImportCameras"); } }

        private Image ImportCamerasImage { get { return ResourceLoader.GetImage("$Image.ImportCameras"); } }

        private string ImportCamerasToolTip { get { return ResourceLoader.GetString2("ToolTip_ImportCameras"); } }

        private void ImportCamerasClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                if (AdministrationForm.Configuration.VmsServerIp != "")
                {
                    ImportCameras import = new ImportCameras();
                    if (AdministrationForm.Configuration.VmsLogin != "")
                    {
                        import.Initialize(AdministrationForm.Configuration.VmsLogin,
                                AdministrationForm.Configuration.VmsPassword,
                                AdministrationForm.Configuration.VmsServerIp,
                                AdministrationForm.Configuration.VmsPort 
                                );
                    }
                    else
                    {
                        import.Initialize(ServerServiceClient.GetInstance().OperatorClient.Login,
                            ServerServiceClient.GetInstance().OperatorClient.Password,
                            AdministrationForm.Configuration.VmsServerIp,
                            AdministrationForm.Configuration.VmsPort
                            );
                    }
                    import.ExportFromVMS();
                    ViewClick(sender, e);
                }
                else
                {
                    MessageForm.Show(ResourceLoader.GetString2("NotVMSConfigured"), MessageFormType.Error);
                }
            }
            catch (Exception ex)
            {
                MessageForm.Show("Error", ResourceLoader.GetString2("UnableToImport"), ex);
            }
        }
        #endregion
        #endregion
    }
}
