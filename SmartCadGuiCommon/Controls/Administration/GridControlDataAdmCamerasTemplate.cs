﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadGuiCommon.Controls;
using SmartCadCore.ClientData;
using SmartCadControls;
using SmartCadCore.Core;
using Smartmatic.SmartCad.Gui;
using SmartCadCore.Common;
using SmartCadCore.Enums;

namespace SmartCadGuiCommon
{
public class GridControlDataAdmCamerasTemplate : GridControlDataAdm
    {
        public GridControlDataAdmCamerasTemplate(CamerasTemplateClientData cameraTemplate)
            : base(cameraTemplate)
        {
            ApplicationOwner.Add(SmartCadApplications.SmartCadCCTV);
        }

        public CamerasTemplateClientData CameraTemplate
        {
            get
            {
                return this.Tag as CamerasTemplateClientData;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string Name
        {
            get
            {
                return CameraTemplate.Name;
            }

        }

        [GridControlRowInfoData(Searchable = true)]
        public int Cameras
        {
            get
            {
                return CameraTemplate.Cameras.Count;
            }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataAdmCamerasTemplate)
            {
                result = CameraTemplate.Code == (obj as GridControlDataAdmCamerasTemplate).CameraTemplate.Code;
            }
            return result;
        }

        #region GridControlDataAdm
        public override System.Windows.Forms.Form GetForm(AdministrationRibbonForm form, ClientData data, FormBehavior behaviour)
        {
            return new CamerasTemplateForm(data as CamerasTemplateClientData, behaviour);
        }

        public override string GetDeletedMessage
        {
            get { return ResourceLoader.GetString("DeleteCamerasTemplateMessage"); }
        }

        public override string GetNotFoundMessage
        {
            get { return ResourceLoader.GetString2("DeleteCamerasTemplateData"); }
        }

        public override bool ShowPreview
        {
            get { return false; }
        }

        public override bool PageSearch
        {
            get { return false; }
        }

        public override bool InitializeCollections
        {
            get { return true; }
        }

        public override bool Refresh
        {
            get { return true; }
        }

        public override string GroupText
        {
            get { return ResourceLoader.GetString("CamerasTemplates"); }
        }

        public override string Query
        {
            get { return SmartCadHqls.GetCamerasTemplates; }
        }

        protected override System.Drawing.Image ViewImage
        {
            get
            {
                return ResourceLoader.GetImage("$Image.CamerasTemplate");
            }
        }

        public override string PageText
        {
            get { return ResourceLoader.GetString("Cctv"); }
        }

        public override bool Visible
        {
            get
            {
                return false;
            }
        }

        protected override int PageOrder
        {
            get { return 3; }
        }

        protected override UserResourceClientData Resource
        {
            get { return CamerasTemplateClientData.Resource; }
        }

        protected override string ViewToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ViewCamerasTemplate");
            }
        }

        protected override string AddToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_CreateCamerasTemplate");
            }
        }

        protected override string DeleteToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_DeleteCamerasTemplate");
            }
        }

        protected override string EditToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ModifyCamerasTemplate");
            }
        }
        #endregion
    }
}
