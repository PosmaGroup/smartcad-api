﻿using SmartCadControls;
using SmartCadControls.Controls;
using SmartCadCore.ClientData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadGuiCommon.Controls.Administration
{
    public class GridControlDataWorkShift : GridControlData
    {

        public GridControlDataWorkShift(WorkShiftVariationClientData data)
            : base(data)
        {

        }


        public WorkShiftVariationClientData WorkShift
        {
            get { return this.Tag as WorkShiftVariationClientData; }
        }


        public int Code
        {
            get { return WorkShift.Code; }
        }

        [GridControlRowInfoData(Visible = true, Width = 150)]
        public string Name
        {
            get { return WorkShift.Name; }
        }

        [GridControlRowInfoData(Visible = true, Width = 150)]
        public string Description
        {
            get { return WorkShift.Description; }
        }



        public override bool Equals(object obj)
        {
            bool result = false;
            GridControlDataWorkShift var = obj as GridControlDataWorkShift;
            if (var != null)
            {
                if (this.Code == var.Code)
                    result = true;
            }
            return result;
        }

    }
}
