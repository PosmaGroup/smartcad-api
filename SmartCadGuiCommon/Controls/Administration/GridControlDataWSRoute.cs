﻿using SmartCadControls;
using SmartCadControls.Controls;
using SmartCadCore.ClientData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadGuiCommon.Controls.Administration
{
    public class GridControlDataWSRoute : GridControlData
    {

        public GridControlDataWSRoute(WorkShiftRouteClientData data)
            : base(data)
        {

        }


        public WorkShiftRouteClientData WorkShiftRoute
        {
            get { return this.Tag as WorkShiftRouteClientData; }
        }


        public int Code
        {
            get { return WorkShiftRoute.Code; }

        }

        [GridControlRowInfoData(Visible = true, Width = 150)]
        public string WorkShift
        {
            get { return WorkShiftRoute.WorkShiftName; }
        }

        public int WorkShiftCode
        {
            get { return WorkShiftRoute.WorkShiftCode; }
        }

        [GridControlRowInfoData(Visible = true, Width = 150)]
        public string Route
        {
            get { return WorkShiftRoute.Route.Name; }
        }

        public int RouteCode
        {
            get { return WorkShiftRoute.Route.Code; }
        }



        public override bool Equals(object obj)
        {
            bool result = false;
            GridControlDataWSRoute var = obj as GridControlDataWSRoute;
            if (var != null)
            {
                if (this.Code == var.Code)
                    result = true;
            }
            return result;
        }

    }
}
