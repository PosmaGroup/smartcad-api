﻿using SmartCadControls;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadCore.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace SmartCadGuiCommon.Controls
{
    public class GridControlDataAdmQuestion : GridControlDataAdm
    {
        public GridControlDataAdmQuestion(QuestionClientData question)
            : base(question)
        {
            ApplicationOwner.Add(SmartCadApplications.SmartCadFirstLevel);
        }
        public QuestionClientData Question
        {
            get
            {
                return this.Tag as QuestionClientData;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string QuestionName
        {
            get
            {
                return Question.Text;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string QuestionMnemonic
        {
            get
            {
                return Question.ShortText;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string QuestionRequirementType
        {
            get
            {
                if (Question.RequirementType.HasValue == true)
                {
                    if (Question.RequirementType.Value == RequirementTypeClientEnum.Required)
                        return ResourceLoader.GetString2("Required");
                    else if (Question.RequirementType.Value == RequirementTypeClientEnum.Recommended)
                        return ResourceLoader.GetString2("Recommended");
                    else
                        return ResourceLoader.GetString2("NotApply");
                }
                else
                {
                    return ResourceLoader.GetString2("NotApply");
                }
            }
        }
        
        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataAdmQuestion)
            {
                result = Question.Code == (obj as GridControlDataAdmQuestion).Question.Code;
            }
            return result;
        }

        #region GridControlDataAdm
        public override System.Windows.Forms.Form GetForm(AdministrationRibbonForm form, ClientData data, FormBehavior behaviour)
        {
            return new QuestionForm(form, data as QuestionClientData, behaviour);
        }

        public override string GetDeletedMessage
        {
            get { return ResourceLoader.GetString2("DeleteQuestion"); }
        }

        public override string GetNotFoundMessage
        {
            get { return ResourceLoader.GetString2("DeleteQuestionData"); }
        }

        public override bool ShowPreview
        {
            get { return false; }
        }

        public override bool PageSearch
        {
            get { return false; }
        }

        public override bool InitializeCollections
        {
            get { return true; }
        }

        public override bool Refresh
        {
            get { return true; }
        }

        public override string GroupText
        {
            get { return ResourceLoader.GetString2("QuestionFirstLevel"); }
        }

        public override string Query
        {
            get { return SmartCadHqls.GetCustomHql(SmartCadHqls.GetQuestions, (int)QuestionClientType.Incident); }
        }

        protected override System.Drawing.Image ViewImage
        {
            get
            {
                return ResourceLoader.GetImage("$Image.Question");
            }
        }

        public override string PageText
        {
            get { return ResourceLoader.GetString("FirstLevel"); }
        }

        protected override int PageOrder
        {
            get { return 2; }
        }

        protected override UserResourceClientData Resource
        {
            get { return QuestionClientData.Resource; }
        }

        protected override string ViewToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ViewQuestion");
            }
        }

        protected override string AddToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_CreateQuestions");
            }
        }

        protected override string DeleteToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_DeleteQuestions");
            }
        }

        protected override string EditToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ModifyQuestions");
            }
        }

        protected override int GroupOrder
        {
            get
            {
                return 1;
            }
        }
        #endregion
    }
}
