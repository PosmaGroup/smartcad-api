﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using Smartmatic.SmartCad.Service;
using SmartCadCore.Core;
using SmartCadCore.Common;
using DevExpress.XtraBars;
using System.Drawing;
using SmartCadControls;

using SmartCadCore.Enums;

namespace SmartCadGuiCommon.Controls
{
    public class GridControlDataAdmEvaluation : GridControlDataAdm
    {
        public GridControlDataAdmEvaluation(EvaluationClientData evaluation)
            : base(evaluation)
        {
            ApplicationOwner.Add(SmartCadApplications.SmartCadSupervision);
        }

        public EvaluationClientData Evaluation
        {
            get
            {
                return this.Tag as EvaluationClientData;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string EvaluationName
        {
            get
            {
                return Evaluation.Name;
            }

        }

        [GridControlRowInfoData(Searchable = true)]
        public string EvaluationApplications
        {
            get
            {
                String result = string.Empty;
                if (Evaluation.Applications != null)
                {
                    foreach (EvaluationUserApplicationClientData app in Evaluation.Applications)
                    {
                        result += ResourceLoader.GetString2(app.ToString());
                        result += ", ";
                    }
                    result = string.IsNullOrEmpty(result) == true ? result : result.Remove(result.Length - 2);
                }
                return result;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string EvaluationDepartamentsType
        {
            get
            {
                String result = string.Empty;

                if (Evaluation.Departments != null)
                {
                    foreach (EvaluationDepartmentTypeClientData dep in Evaluation.Departments)
                    {
                        result += ResourceLoader.GetString2(dep.ToString());
                        result += ", ";
                    }
                    result = string.IsNullOrEmpty(result) == true ? result : result.Remove(result.Length - 2);
                }
                return result;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string EvaluationOperatorCategory
        {
            get
            {
                return Evaluation.OperatorCategory.FriendlyName;
            }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataAdmEvaluation)
            {
                result = Evaluation.Code == (obj as GridControlDataAdmEvaluation).Evaluation.Code;
            }
            return result;
        }

        #region GridControlDataAdm
        public override System.Windows.Forms.Form GetForm(AdministrationRibbonForm form, ClientData data, FormBehavior behaviour)
        {
            return new EvaluationsForm(form, data as EvaluationClientData, behaviour, false);
        }

        public override string GetDeletedMessage
        {
            get { return ResourceLoader.GetString2("DeleteEvaluationMSG"); }
        }

        public override string GetNotFoundMessage
        {
            get { return ResourceLoader.GetString2("DeleteEvaluationData"); }
        }

        public override bool ShowPreview
        {
            get { return false; }
        }

        public override bool PageSearch
        {
            get { return false; }
        }

        public override bool InitializeCollections
        {
            get { return true; }
        }

        public override bool Refresh
        {
            get { return true; }
        }

        public override string GroupText
        {
            get { return ResourceLoader.GetString2("PerformanceEvaluation"); }
        }

        public override string Query
        {
            get { return SmartCadHqls.GetEvaluations; }
        }

        protected override System.Drawing.Image ViewImage
        {
            get
            {
                return ResourceLoader.GetImage("$Image.Evaluation");
            }
        }

        public override string PageText
        {
            get
            {
                return ResourceLoader.GetString("Supervision");
            }
        }

        protected override int PageOrder
        {
            get { return 7; }
        }

        protected override int GroupOrder
        {
            get
            {
                return 1;
            }
        }

        protected override UserResourceClientData Resource
        {
            get
            {
                return EvaluationClientData.Resource;
            }
        }

        protected override string ViewToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ViewEvaluation");
            }
        }

        protected override string AddToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_CreateEvaluation");
            }
        }

        protected override string DeleteToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_DeleteEvaluation");
            }
        }

        protected override string EditToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ModifyEvaluation");
            }
        }

        protected override List<DevExpress.XtraBars.BarButtonItem> CreateBasicButtons()
        {
            Items = base.CreateBasicButtons();

            Items.Add(CreateButton(DuplicateEvaluationCaption, null, DuplicateEvaluationImage, DuplicateEvaluationClick, false, Resource, UserActionClientData.Update, System.Windows.Forms.Shortcut.CtrlD, DuplicateEvaluationToolTip));

            return Items;
        }
        #endregion

        #region Duplicate Evaluation
        private string DuplicateEvaluationCaption { get { return ResourceLoader.GetString2("Duplicate"); } }

        private Image DuplicateEvaluationImage { get { return ResourceLoader.GetImage("$Image.Duplicate"); } }

        private string DuplicateEvaluationToolTip { get { return ResourceLoader.GetString2("ToolTip_DuplicateEvaluation"); } }

        private void DuplicateEvaluationClick(object sender, ItemClickEventArgs e)
        {
            EvaluationClientData originalEval = (GridControl.SelectedItems[0] as GridControlDataAdmEvaluation).Evaluation;
            EvaluationClientData eval = new EvaluationClientData(originalEval.Name + "_" + ResourceLoader.GetString2("DuplicateNameCopy"));
            eval.Code = 0;
            eval.Scale = originalEval.Scale;
            eval.OperatorCategory = originalEval.OperatorCategory;
            eval.Description = originalEval.Description;
            eval.Questions = originalEval.Questions;
            eval.Questions = new List<EvaluationQuestionClientData>();
            foreach (EvaluationQuestionClientData ques in originalEval.Questions)
            {
                EvaluationQuestionClientData question = ques.Clone() as EvaluationQuestionClientData;
                eval.Questions.Add(question);
            }
            eval.Departments = new List<EvaluationDepartmentTypeClientData>();
            foreach (EvaluationDepartmentTypeClientData dep in originalEval.Departments)
            {
                EvaluationDepartmentTypeClientData evaDep = dep.Clone() as EvaluationDepartmentTypeClientData;
                eval.Departments.Add(evaDep);
            }
            eval.Applications = new List<EvaluationUserApplicationClientData>();
            foreach (EvaluationUserApplicationClientData app in originalEval.Applications)
            {
                EvaluationUserApplicationClientData evaAppUser = app.Clone() as EvaluationUserApplicationClientData;
                eval.Applications.Add(evaAppUser);
            }

            EvaluationsForm form = new EvaluationsForm(AdministrationForm, eval, FormBehavior.Create, true);
            form.ShowDialog();
        }
        #endregion
    }
}
