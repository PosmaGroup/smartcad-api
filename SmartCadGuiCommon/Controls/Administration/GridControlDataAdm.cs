﻿using DevExpress.XtraBars;
using DevExpress.XtraBars.Ribbon;
using SmartCadControls;
using SmartCadControls.Controls;
using SmartCadCore.ClientData;
using SmartCadCore.ClientData.Util;
using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadCore.Enums;
using Smartmatic.SmartCad.Service;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SmartCadGuiCommon.Controls
{
    public abstract class GridControlDataAdm : GridControlData
    {
        protected List<string> ApplicationOwner = new List<string>();
        protected static int BarButtonID = 100;
        protected static int RibbonPageGroupID = 100;
        protected static int RibbonPageID = 100;
        protected static string BarButtonName = "BarButtonId";
        protected static GridControlEx GridControl { get { return AdministrationForm.GridControl; } }
        protected static GridViewEx GridView { get { return AdministrationForm.GridView; } }
        internal static AdministrationRibbonForm AdministrationForm { get; set; }
        public static GridControlDataAdm Current = null;
        protected List<BarButtonItem> Items { get; set; }
        private object searchLock = new object();

        public GridControlDataAdm(ClientData tagArg)
            : base(tagArg)
        { }

        public abstract Form GetForm(AdministrationRibbonForm form, ClientData data, FormBehavior behaviour);

        public abstract string GetDeletedMessage
        {
            get;
        }

        public abstract string GetNotFoundMessage
        {
            get;
        }

        public abstract bool ShowPreview
        {
            get;
        }

        public abstract bool PageSearch
        {
            get;
        }

        public abstract bool InitializeCollections
        {
            get;
        }

        public abstract bool Refresh
        {
            get;
        }

        public abstract string GroupText
        {
            get;
        }

        public virtual string PageText
        {
            get
            {
                return "Page";
            }
        }

        public abstract string Query
        {
            get;
        }

        public virtual bool Visible
        {
            get
            {
                return true;
            }
        }

        private RibbonPageGroup PageGroup
        {
            get;
            set;
        }

        #region Page and Group creation

        public static void InitializeMenus(ArrayList installedApplications)
        {
            Type[] types = Assembly.GetExecutingAssembly().GetTypes();

            List<GridControlDataAdm> GridControlDataAdmTypes = new List<GridControlDataAdm>();

            foreach (Type type in types)
            {
                if (type.IsClass == true
                    && type.IsAbstract == false
                    && type.IsSubclassOf(typeof(GridControlDataAdm)))
                {
                    GridControlDataAdm specificType = type.GetConstructors()[0].Invoke((new object[1] { null })) as GridControlDataAdm;
                    foreach (var owner in specificType.ApplicationOwner) 
                    {
                        if ((specificType.Visible) && (installedApplications.Contains(owner))) 
                        {
                            GridControlDataAdmTypes.Add(specificType);
                            break;
                        }                            
                    }                    
                }
            }

            GridControlDataAdmTypes.OrderBy(item => item.PageOrder).ThenBy(item => item.GroupOrder).ToList().ForEach(item => item.CreatePage());

            GridControlDataAdm grid = GridControlDataAdmTypes.Find(item => item is GridControlDataAdmApplicationPreference);
            if (grid.AllButtons.Find(button => button.Enabled == true) != null)
            {
                grid.ViewClick(null, null);
            }
            else
            {
                grid = GridControlDataAdmTypes.Find(item => item.AllButtons.Find(b => b.Enabled == true) != null);
                if (grid != null)
                {
                    grid.ViewClick(null, null);
                    AdministrationForm.RibbonControl.SelectedPage = grid.PageGroup.Page;
                }
            }
        }

        private void CreatePage()
        {
            RibbonPage ribbonPage = null;

            foreach (RibbonPage page in AdministrationForm.RibbonControl.Pages)
            {
                if (page.Text == PageText)
                {
                    ribbonPage = page;
                    break;
                }
            }

            if (ribbonPage == null)
            {
                ribbonPage = new RibbonPage();
                ribbonPage.Name = "ribbonPage" + RibbonPageID;
                ribbonPage.Text = PageText;
                AdministrationForm.RibbonControl.Pages.Insert(PageOrder, ribbonPage);
            }

            if (GroupOrder > -1)
                ribbonPage.Groups.Insert(GroupOrder, CreateGroup());
            else
                ribbonPage.Groups.Add(CreateGroup());
        }

        private RibbonPageGroup CreateGroup()
        {
            RibbonPageGroup ribbon = new RibbonPageGroup();
            ribbon.AllowMinimize = false;
            ribbon.AllowTextClipping = false;
            CreateBasicButtons().ForEach(i => ribbon.ItemLinks.Add(i));
            ribbon.Name = "ribbonPageGroup" + RibbonPageGroupID++;
            ribbon.ShowCaptionButton = false;
            ribbon.Text = GroupText;
            PageGroup = ribbon;
            return ribbon;
        }

        protected virtual List<BarButtonItem> CreateBasicButtons()
        {
            Items = new List<BarButtonItem>();

            Items.Add(CreateViewButton());

            Items.Add(CreateButton(AddCaption, null, AddImage, AddClick, false, Resource, UserActionClientData.Insert, Shortcut.CtrlN, AddToolTip));

            Items.Add(CreateButton(DeleteCaption, null, DeleteImage, DeleteClick, false, Resource, UserActionClientData.Delete, Shortcut.Del, DeleteToolTip));

            Items.Add(CreateEditButton());

            return Items;
        }

        protected BarButtonItem CreateViewButton()
        {
            return CreateButton(ViewCaption, ViewImage, null, ViewClick,
                AdministrationForm.CheckAccessBarButtonItem(Resource, UserActionClientData.Search),
                Resource, UserActionClientData.Search, Shortcut.CtrlV, ViewToolTip);
        }

        protected BarButtonItem CreateEditButton()
        {
            return CreateButton(EditCaption, null, EditImage, EditClick, false, Resource, UserActionClientData.Update, Shortcut.CtrlP, EditToolTip);
        }

        protected BarButtonItem CreateButton(string caption, Image largeGlyph, Image glyph,
                                             ItemClickEventHandler handler, bool enabled,
                                             UserResourceClientData resource, UserActionClientData action,
                                             Shortcut shortcut, string toolTip)
        {
            return CreateButton(caption, BarButtonID++, largeGlyph, glyph, BarButtonName, handler, enabled, resource, action, shortcut, toolTip);
        }

        protected BarButtonItem CreateButton(string caption, int id, Image largeGlyph, Image glyph,
                                             string name, ItemClickEventHandler handler, bool enabled,
                                             UserResourceClientData resource, UserActionClientData action,
                                             Shortcut shorcut, string toolTip)
        {
            BarButtonItem button = new BarButtonItem();
            button.ItemShortcut = new BarShortcut(shorcut);
            button.Caption = caption;
            button.Id = id;
            button.LargeGlyph = largeGlyph;
            button.Glyph = glyph;
            button.Name = name + GroupText + button.Id;
            button.ItemClick += handler;
            button.Enabled = enabled;
            button.Tag = new ClientData[] { resource, action };
            button.SuperTip = AdministrationForm.BuildToolTip(toolTip);
            return button;
        }

        protected virtual string ViewCaption
        {
            get
            {
                return ResourceLoader.GetString2("View");
            }
        }

        protected virtual Image ViewImage
        {
            get
            {
                return ResourceLoader.GetImage("$Image.AdministrationLogo");
            }
        }

        protected virtual string ViewToolTip
        {
            get
            {
                return ResourceLoader.GetString2("View");
            }
        }

        protected virtual string AddCaption
        {
            get
            {
                return ResourceLoader.GetString2("$Message.Crear");
            }
        }

        protected virtual Image AddImage
        {
            get
            {
                return ResourceLoader.GetImage("$Images.AddIcon");
            }
        }

        protected virtual string AddToolTip
        {
            get
            {
                return ResourceLoader.GetString2("View");
            }
        }

        protected virtual string DeleteCaption
        {
            get
            {
                return ResourceLoader.GetString2("$Message.Delete");
            }
        }

        protected virtual Image DeleteImage
        {
            get
            {
                return ResourceLoader.GetImage("$Images.DeleteIcon");
            }
        }

        protected virtual string DeleteToolTip
        {
            get
            {
                return ResourceLoader.GetString2("View");
            }
        }

        protected virtual string EditCaption
        {
            get
            {
                return ResourceLoader.GetString2("Modify");
            }
        }

        protected virtual Image EditImage
        {
            get
            {
                return ResourceLoader.GetImage("$Images.EditIcon");
            }
        }

        protected virtual string EditToolTip
        {
            get
            {
                return ResourceLoader.GetString2("View");
            }
        }

        protected void DeActivate(GridControlDataAdm gridControlData)
        {
            gridControlData.Items.ForEach(
                delegate(BarButtonItem i)
                {
                    ClientData[] clients = i.Tag as ClientData[];
                    if (i.Caption != ViewCaption)
                        i.Enabled = false;
                    else
                        i.Enabled = AdministrationForm.CheckAccessBarButtonItem(clients[0] as UserResourceClientData, clients[1] as UserActionClientData);
                });
        }

        public void Activate()
        {
            Items.ForEach(
                delegate(BarButtonItem i)
                {
                    ClientData[] clients = i.Tag as ClientData[];
                    if (i != null)
                    {
                        UserResourceClientData resource = (UserResourceClientData)clients[0];
                        UserActionClientData action = (UserActionClientData)clients[1];
                        i.Enabled = AdministrationForm.CheckAccessBarButtonItem(resource, action);
                        AdministrationForm.lblFunctionName.Text = Regex.Replace(i.Name.Replace("BarButtonId", ""), @"\d", "");
                        if (action != UserActionClientData.Search && action != UserActionClientData.Insert)
                            i.Enabled &= GridControl.SelectedItems.Count > 0;
                    }
                });

            AdministrationForm.AssignButtonShorcutGroup();
        }

        protected void ViewClick(object sender, ItemClickEventArgs e)
        {
            if (Current != null && Current != this)
                DeActivate(Current);

            Current = this;

            GridView.ClearColumnsFilter();
            GridControl.EnableAutoFilter = true;
            GridControl.Type = GetType();
            GridControl.ViewTotalRows = true;
            GridView.OptionsView.ShowPreview = ShowPreview;
            GridView.EnablePreviewLineForFocusedRow = ShowPreview;
            GridView.OptionsView.ColumnAutoWidth = true;



            lock (searchLock)
            {


                IList list; 

                // Codigo modificado para efectos del Prototipo. Se agrega el condicional que genera la difulcación.
                if (GetType() != typeof(GridControlDataAdmRule))
                {
                    list = AdministrationForm.RunQuery(PageSearch, InitializeCollections, GetType(), Query, new string[] { }) as IList;

                    if (list != null && list.Count > 0)
                    {
                        AdministrationForm.SyncBox.Sync(list);
                    }
                }
            }

            Activate();

            AdministrationForm.RibbonControl.Refresh();
        }
/*
        #region "Prototipo Cornelio"

        protected IList FillTestingList()
            {
             //   List<RuleClientData> list = new List<RuleClientData>();
                IList listado;
              //  RuleClientData obj;
                
                int Cont = 9;

               /* for (int i = 0; i <= Cont; i++)
                {
                    obj = new RuleClientData();
                    obj.Camera = "192.168.200.90" + i.ToString();
                    obj.Rule = "Object / People acrossing a line " + i.ToString();
                    obj.Value = "Active";
                    obj.Code = i;
                    obj.Version = i;
                    list.Add(obj);
                    obj = null;
                }
                * /
                listado = null; // list;
               // list = null;
            return listado;
        }


        #endregion
*/
        protected void AddClick(object sender, ItemClickEventArgs e)
        {
            //ViewClick(sender, e);
            Form form = GetForm(AdministrationForm, null, FormBehavior.Create);
            form.ShowDialog();
        }

        protected void DeleteClick(object sender, ItemClickEventArgs e)
        {
            AdministrationForm.DeleteObjects(GetDeletedMessage, GetNotFoundMessage);
            Activate();

            //EnableOptions(e.Item as BarButtonItem);
        }

        protected void EditClick(object sender, ItemClickEventArgs e)
        {
            ClientData selected = (GridControl.SelectedItems[0] as GridControlDataAdm).Tag;
            if (Refresh)
                selected = ServerServiceClient.GetInstance().RefreshClient(selected, InitializeCollections);
            if (selected != null)
            {
                Form form = GetForm(AdministrationForm, selected, FormBehavior.Edit);
                form.ShowDialog();
                Activate();
            }
        }

        protected abstract UserResourceClientData Resource { get; }

        public List<BarButtonItem> AllButtons
        {
            get { return Items; }
        }

        public List<BarButtonItem> ActionButtons
        {
            get { if (Items == null) return new List<BarButtonItem>(); else return Items.FindAll(item => item.Caption != ViewCaption); }
        }

        public void Edit()
        {
            if (GridControl.SelectedItems.Count > 0)
                EditClick(null, null);
        }

        protected virtual int PageOrder
        {
            get { return 0; }
        }

        protected virtual int GroupOrder
        {
            get { return -1; }
        }
        #endregion
    }
}
