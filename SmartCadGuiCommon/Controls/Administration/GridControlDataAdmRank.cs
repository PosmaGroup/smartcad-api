﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadControls;
using SmartCadCore.Enums;

namespace SmartCadGuiCommon.Controls
{
    public class GridControlDataAdmRank : GridControlDataAdm
    {
        public GridControlDataAdmRank(RankClientData rank)
            : base(rank)
        {
            ApplicationOwner.Add(SmartCadApplications.SmartCadAdministration);
        }

        public RankClientData Rank
        {
            get
            {
                return this.Tag as RankClientData;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string Name
        {
            get
            {
                return Rank.Name;
            }

        }

        [GridControlRowInfoData(Searchable = true)]
        public string Description
        {
            get
            {
                return Rank.Description;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string Department
        {
            get
            {
                return Rank.DepartmentTypeName;
            }
        }
        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataAdmRank)
            {
                result = Rank.Code == (obj as GridControlDataAdmRank).Rank.Code;
            }
            return result;
        }

        #region GridControlDataAdm

        public override System.Windows.Forms.Form GetForm(AdministrationRibbonForm form, ClientData data, FormBehavior behaviour)
        {
            return new RankXtraForm(form, data as RankClientData, behaviour);
        }

        public override string GetDeletedMessage
        {
            get { return ResourceLoader.GetString("DeleteRankMessage"); }
        }

        public override string GetNotFoundMessage
        {
            get { return ResourceLoader.GetString2("DeleteRankData"); }
        }

        public override bool ShowPreview
        {
            get { return false; }
        }

        public override bool PageSearch
        {
            get { return false; }
        }

        public override bool InitializeCollections
        {
            get { return true; }
        }

        public override bool Refresh
        {
            get { return false; }
        }

        public override string GroupText
        {
            get { return ResourceLoader.GetString2("Ranks"); }
        }

        public override string Query
        {
            get { return SmartCadHqls.GetRanks; }
        }

        protected override System.Drawing.Image ViewImage
        {
            get
            {
                return ResourceLoader.GetImage("$Image.Rank");
            }
        }

        public override string PageText
        {
            get
            {
                return ResourceLoader.GetString("Dispatch");
            }
        }

        protected override int PageOrder
        {
            get { return 5; }
        }

        protected override UserResourceClientData Resource
        {
            get
            {
                return RankClientData.Resource;
            }
        }

        protected override string ViewToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ViewRank");
            }
        }

        protected override string AddToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_CreateRank");
            }
        }

        protected override string DeleteToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_DeleteRank");
            }
        }

        protected override string EditToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ModifyRank");
            }
        }

        protected override int GroupOrder
        {
            get
            {
                return 0;
            }
        }
        #endregion
    }
}
