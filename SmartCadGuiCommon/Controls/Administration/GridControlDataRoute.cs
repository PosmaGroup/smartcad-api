﻿using SmartCadControls;
using SmartCadControls.Controls;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadGuiCommon.Controls.Administration
{
    public class GridControlDataRoute : GridControlData
    {

        public GridControlDataRoute(RouteClientData data)
            : base(data)
        {

        }


        public RouteClientData Route
        {
            get { return this.Tag as RouteClientData; }
        }


        public int Code
        {
            get { return Route.Code; }
        }

        [GridControlRowInfoData(Visible = true, Width = 150)]
        public string Name
        {
            get { return Route.Name; }
        }

        [GridControlRowInfoData(Visible = true, Width = 150)]
        public string Type
        {
            get { return ResourceLoader.GetString2(Route.Type.ToString()); }
        }

        public string CustomCode
        {
            get { return Route.CustomCode; }
        }


        public override bool Equals(object obj)
        {
            bool result = false;
            GridControlDataRoute var = obj as GridControlDataRoute;
            if (var != null)
            {
                if (this.Code == var.Code)
                    result = true;
            }
            return result;
        }

    }
}
