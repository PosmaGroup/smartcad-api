﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DevExpress.XtraBars;
using System.Drawing;
using SmartCadGuiCommon.Controls;
using SmartCadCore.ClientData;
using SmartCadControls;
using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadCore.Enums;
using SmartCadGuiCommon;

namespace Smartmatic.SmartCad.Gui
{
    public class GridControlDataAdmUserAccount : GridControlDataAdm
    {
        public GridControlDataAdmUserAccount(OperatorClientData oper)
            : base(oper)
        { ApplicationOwner.Add(SmartCadApplications.SmartCadAdministration); }

        public OperatorClientData OperatorClient
        {
            get
            {
                return this.Tag as OperatorClientData;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string PersonLastName
        {
            get
            {
                return OperatorClient.LastName;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string PersonName
        {
            get
            {
                return OperatorClient.FirstName;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string PersonID
        {
            get
            {
                return OperatorClient.PersonID;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string SRole
        {
            get
            {
                return OperatorClient.RoleFriendlyName;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string UserID
        {
            get
            {
                return OperatorClient.Login;
            }
        }
        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataAdmUserAccount)
            {
                result = OperatorClient.Code == (obj as GridControlDataAdmUserAccount).OperatorClient.Code;
            }
            return result;
        }

        #region GridControlDataAdm
        public override System.Windows.Forms.Form GetForm(AdministrationRibbonForm form, ClientData data, FormBehavior behaviour)
        {
            return new UserAccountForm(form, data as OperatorClientData, behaviour);
        }

        public override string GetDeletedMessage
        {
            get { return ResourceLoader.GetString2("DeleteUserMessage"); }
        }

        public override string GetNotFoundMessage
        {
            get { return ResourceLoader.GetString2("DeleteOperatorData"); }
        }

        public override bool ShowPreview
        {
            get { return false; }
        }

        public override bool PageSearch
        {
            get { return false; }
        }

        public override bool InitializeCollections
        {
            get { return true; }
        }

        public override bool Refresh
        {
            get { return true; }
        }

        public override string GroupText
        {
            get { return ResourceLoader.GetString2("Users"); }
        }

        public override string Query
        {
            get { return SmartCadHqls.GetOperatorsWithoutSupervisor; }
        }

        protected override System.Drawing.Image ViewImage
        {
            get
            {
                return ResourceLoader.GetImage("$Image.User");
            }
        }

        public override string PageText
        {
            get { return ResourceLoader.GetString("UserAccounts"); }
        }

        protected override int PageOrder
        {
            get { return 1; }
        }

        protected override string ViewToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ViewUserAccount");
            }
        }

        protected override string AddToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_CreateUser");
            }
        }

        protected override string DeleteToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_DeleteUser");
            }
        }

        protected override string EditToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ModifyUser");
            }
        }

        protected override UserResourceClientData Resource
        {
            get { return OperatorClientData.Resource; }
        }

        protected override List<DevExpress.XtraBars.BarButtonItem> CreateBasicButtons()
        {
            Items = base.CreateBasicButtons();

            Items.Add(CreateButton(CameraAcessCaption, null, CameraAcessImage, CameraAccessClick, true, Resource, UserActionClientData.Search, System.Windows.Forms.Shortcut.CtrlB, CameraAcessToolTip));

            Items.Add(CreateButton(OperatorCategoryCaption, null, OperatorCategoryImage, OperatorCategoryClick, true, Resource, UserActionClientData.Search, System.Windows.Forms.Shortcut.CtrlG, OperatorCategoryToolTip));

            return Items;
        }
        #endregion

        #region CameraAccess
        private string CameraAcessCaption { get { return ResourceLoader.GetString2("AccessToCameras"); } }

        private Image CameraAcessImage { get { return ResourceLoader.GetImage("$Image.CameraAccess"); } }

        private string CameraAcessToolTip { get { return ResourceLoader.GetString2("ToolTip_AccessToCameras"); } }

        private void CameraAccessClick(object sender, ItemClickEventArgs e)
        {
            SecurityForm form = null;
            if (GridControl.SelectedItems.Count > 0)
            {
                form = new SecurityForm(((GridControl.SelectedItems[0]
                   as GridControlDataAdm).Tag
                   as OperatorClientData), FormBehavior.Edit);
            }
            else
            {
                form = new SecurityForm(null, FormBehavior.Edit);

            }
            form.ShowDialog();
        }

        protected override int GroupOrder
        {
            get
            {
                return 2;
            }
        }
        #endregion

        #region Operator Category
        private string OperatorCategoryCaption { get { return ResourceLoader.GetString2("Categories"); } }

        private Image OperatorCategoryImage { get { return ResourceLoader.GetImage("$Image.OperatorCategory"); } }

        private string OperatorCategoryToolTip { get { return ResourceLoader.GetString2("ToolTip_OperatorCategory"); } }

        private void OperatorCategoryClick(object sender, ItemClickEventArgs e)
        {
            OperatorCategoryForm form = new OperatorCategoryForm();
            form.ShowDialog();
        }
        #endregion
    }
}
