﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadControls;
using SmartCadCore.Enums;

namespace SmartCadGuiCommon.Controls
{
    public class GridControlDataAdmDepartmentZone : GridControlDataAdm
    {
        public GridControlDataAdmDepartmentZone(DepartmentZoneClientData departmentZone)
            : base(departmentZone)
        {
            ApplicationOwner.Add(SmartCadApplications.SmartCadAdministration);
        }

        public DepartmentZoneClientData DepartmentZone
        {
            get
            {
                return this.Tag as DepartmentZoneClientData;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string Name
        {
            get
            {
                return DepartmentZone.Name;
            }

        }

        [GridControlRowInfoData(Searchable = true)]
        public string CustomCode
        {
            get
            {
                return DepartmentZone.CustomCode;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string DepartmentType
        {
            get
            {
                return DepartmentZone.DepartmentType.Name;
            }
        }

        [GridControlRowInfoData]
        public string Coordinates
        {
            get
            {
                string result = ResourceLoader.GetString2("$Message.Yes");
                if (this.DepartmentZone.DepartmentZoneAddress == null ||
                    this.DepartmentZone.DepartmentZoneAddress.Count == 0)
                {
                    result = ResourceLoader.GetString2("$Message.No");
                }
                return result;
            }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataAdmDepartmentZone)
            {
                result = DepartmentZone.Code == (obj as GridControlDataAdmDepartmentZone).DepartmentZone.Code;
            }
            return result;
        }

        #region Override GridControlDataAdm

        public override System.Windows.Forms.Form GetForm(AdministrationRibbonForm form, ClientData data, FormBehavior behaviour)
        {
            return new DepartmentZoneXtraForm(form, data as DepartmentZoneClientData, behaviour);
        }

        public override string GetDeletedMessage
        {
            get { return ResourceLoader.GetString("DeleteDepartmentZoneMessage"); }
        }

        public override string GetNotFoundMessage
        {
            get { return ResourceLoader.GetString2("DeleteDepartmentZoneData"); }
        }

        public override bool ShowPreview
        {
            get { return false; }
        }

        public override bool PageSearch
        {
            get { return true; }
        }

        public override bool InitializeCollections
        {
            get { return true; }
        }

        public override bool Refresh
        {
            get { return false; }
        }

        public override string GroupText
        {
            get { return ResourceLoader.GetString("DepartmentZones"); }
        }

        public override string Query
        {
            get { return SmartCadHqls.DepartmentZonesWithStationAndAddress; }
        }

        protected override System.Drawing.Image ViewImage
        {
            get
            {
                return ResourceLoader.GetImage("$Image.DepartmentZone");
            }
        }

        public override string PageText
        {
            get { return ResourceLoader.GetString("DepartmentsAdmPageText"); }
        }

        protected override int PageOrder
        {
            get { return 3; }
        }

        protected override UserResourceClientData Resource
        {
            get { return DepartmentZoneClientData.Resource; }
        }

        protected override string ViewToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ViewDepartmentZone");
            }
        }

        protected override string AddToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_CreateDepartmentZone");
            }
        }

        protected override string DeleteToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_DeleteDepartmentZone");
            }
        }

        protected override string EditToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ModifyDepartmentZone");
            }
        }

        protected override int GroupOrder
        {
            get
            {
                return 1;
            }
        }
        #endregion
    }
}
