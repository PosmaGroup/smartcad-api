﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadControls;
using SmartCadCore.Enums;

namespace SmartCadGuiCommon.Controls
{
    public class GridControlDataAdmPosition : GridControlDataAdm
    {
        public GridControlDataAdmPosition(PositionClientData position)
            : base(position)
        {
            ApplicationOwner.Add(SmartCadApplications.SmartCadAdministration);
        }

        public PositionClientData Position
        {
            get
            {
                return this.Tag as PositionClientData;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string Name
        {
            get
            {
                return Position.Name;
            }

        }

        [GridControlRowInfoData(Searchable = true)]
        public string Description
        {
            get
            {
                return Position.Description;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string Department
        {
            get
            {
                return Position.DepartmentTypeName;
            }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataAdmPosition)
            {
                result = Position.Code == (obj as GridControlDataAdmPosition).Position.Code;
            }
            return result;
        }

        #region GridControlDataAdm
        public override System.Windows.Forms.Form GetForm(AdministrationRibbonForm form, ClientData data, FormBehavior behaviour)
        {
            return new PositionXtraForm(form, data as PositionClientData, behaviour);
        }

        public override string GetDeletedMessage
        {
            get { return ResourceLoader.GetString("DeletePositionMessage"); }
        }

        public override string GetNotFoundMessage
        {
            get { return ResourceLoader.GetString2("DeletePositionData"); }
        }

        public override bool ShowPreview
        {
            get { return false; }
        }

        public override bool PageSearch
        {
            get { return false; }
        }

        public override bool InitializeCollections
        {
            get { return true; }
        }

        public override bool Refresh
        {
            get { return true; }
        }

        public override string GroupText
        {
            get { return ResourceLoader.GetString2("Positions"); }
        }

        public override string Query
        {
            get { return SmartCadHqls.GetPositions; }
        }

        protected override System.Drawing.Image ViewImage
        {
            get
            {
                return ResourceLoader.GetImage("$Image.Position");
            }
        }

        public override string PageText
        {
            get
            {
                return ResourceLoader.GetString("Dispatch");
            }
        }

        protected override int PageOrder
        {
            get { return 5; }
        }

        protected override UserResourceClientData Resource
        {
            get
            {
                return PositionClientData.Resource;
            }
        }

        protected override string ViewToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ViewPosition");
            }
        }

        protected override string AddToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_CreatePosition");
            }
        }

        protected override string DeleteToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_DeletePosition");
            }
        }

        protected override string EditToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ModifyPosition");
            }
        }

        protected override int GroupOrder
        {
            get
            {
                return 1;
            }
        }

        #endregion
    }
}
