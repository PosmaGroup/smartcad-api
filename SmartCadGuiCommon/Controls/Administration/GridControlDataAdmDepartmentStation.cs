﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadControls;
using SmartCadCore.Enums;

namespace SmartCadGuiCommon.Controls
{
    public class GridControlDataAdmDepartmentStation : GridControlDataAdm
    {
        public GridControlDataAdmDepartmentStation(DepartmentStationClientData departmentStation)
            : base(departmentStation)
        {
            ApplicationOwner.Add(SmartCadApplications.SmartCadAdministration);
        }
        
        public DepartmentStationClientData DepartmentStation
        {
            get
            {
                return this.Tag as DepartmentStationClientData;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string Name
        {
            get
            {
                return DepartmentStation.Name;
            }

        }

        [GridControlRowInfoData(Searchable = true)]
        public string CustomCode
        {
            get
            {
                return DepartmentStation.CustomCode;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string DepartmentZone
        {
            get
            {
                return DepartmentStation.DepartmentZone.Name;
            }
        }

        [GridControlRowInfoData]
        public string Coordinates
        {
            get
            {
                string result = ResourceLoader.GetString2("$Message.No");
                if (this.DepartmentStation.Points == true)
                {
                    result = ResourceLoader.GetString2("$Message.Yes");
                }
                return result;
            }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataAdmDepartmentStation)
            {
                result = DepartmentStation.Code == (obj as GridControlDataAdmDepartmentStation).DepartmentStation.Code;
            }
            return result;
        }

        #region GridControlDataAdm
        public override System.Windows.Forms.Form GetForm(AdministrationRibbonForm form, ClientData data, FormBehavior behaviour)
        {
            return new DepartmentStationXtraForm(form, data as DepartmentStationClientData, behaviour);
        }

        public override string GetDeletedMessage
        {
            get { return ResourceLoader.GetString("DeleteDepartmentStationMessage"); }
        }

        public override string GetNotFoundMessage
        {
            get { return ResourceLoader.GetString2("DeleteDepartmentStationData"); }
        }

        public override bool ShowPreview
        {
            get { return false; }
        }

        public override bool PageSearch
        {
            get { return false; }
        }

        public override bool InitializeCollections
        {
            get { return true; }
        }

        public override bool Refresh
        {
            get { return true; }
        }

        public override string GroupText
        {
            get { return ResourceLoader.GetString("DepartmentStations"); }
        }

        public override string Query
        {
            get { return SmartCadHqls.GetDepartmentsStation; }
        }

        protected override System.Drawing.Image ViewImage
        {
            get
            {
                return ResourceLoader.GetImage("$Image.DepartmentStation");
            }
        }

        public override string PageText
        {
            get { return ResourceLoader.GetString("DepartmentsAdmPageText"); }
        }

        protected override int PageOrder
        {
            get { return 3; }
        }

        protected override UserResourceClientData Resource
        {
            get { return DepartmentStationClientData.Resource; }
        }

        protected override string ViewToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ViewDepartmentStation");
            }
        }

        protected override string AddToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_CreateDepartmentStation");
            }
        }

        protected override string DeleteToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_DeleteDepartmentStation");
            }
        }

        protected override string EditToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ModifyDepartmentStation");
            }
        }

        protected override int GroupOrder
        {
            get
            {
                return 2;
            }
        }
        #endregion
    }
}
