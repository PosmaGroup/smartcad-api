﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadControls;
using SmartCadCore.Enums;

namespace SmartCadGuiCommon.Controls
{
    public class GridControlDataAdmStruct : GridControlDataAdm
    {
        public GridControlDataAdmStruct(StructClientData structure)
            : base(structure)
        {
            ApplicationOwner.Add(SmartCadApplications.SmartCadCCTV);
            ApplicationOwner.Add(SmartCadApplications.SmartCadAlarm);
        }

        public StructClientData Structure
        {
            get
            {
                return this.Tag as StructClientData;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string StructName
        {
            get
            {
                return Structure.Name;
            }

        }

        [GridControlRowInfoData(Searchable = true)]
        public string StructType
        {
            get
            {
                return Structure.Type.Name;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string CCTVZone
        {
            get
            {
                if (Structure.CctvZone != null)
                    return Structure.CctvZone.Name;
                else
                    return string.Empty;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string StructState
        {
            get
            {
                return Structure.State;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string StructTown
        {
            get
            {
                return Structure.Town;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public int StructCameras
        {
            get
            {
                return Structure.CamerasNumber;
            }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataAdmStruct)
            {
                result = Structure.Code == (obj as GridControlDataAdmStruct).Structure.Code;
            }
            return result;
        }

        #region GridControlDataAdm
        public override System.Windows.Forms.Form GetForm(AdministrationRibbonForm form,ClientData data, FormBehavior behaviour)
        {
            return new StructForm(form, data as StructClientData, behaviour);
        }

        public override string GetDeletedMessage
        {
            get { return ResourceLoader.GetString("DeleteStruct"); }
        }

        public override string GetNotFoundMessage
        {
            get { return ResourceLoader.GetString2("DeleteStructData"); }
        }

        public override bool ShowPreview
        {
            get { return false; }
        }

        public override bool PageSearch
        {
            get { return true; }
        }

        public override bool InitializeCollections
        {
            get { return true; }
        }

        public override bool Refresh
        {
            get { return true; }
        }

        public override string GroupText
        {
            get { return ResourceLoader.GetString("Structs"); }
        }

        public override string Query
        {
            get { return SmartCadHqls.GetAllStructs; }
        }

        protected override System.Drawing.Image ViewImage
        {
            get
            {
                return ResourceLoader.GetImage("$Image.Struct");
            }
        }

        public override string PageText
        {
            get
            {
                return ResourceLoader.GetString("Cctv");
            }
        }

        protected override int PageOrder
        {
            get { return 6; }
        }

        protected override UserResourceClientData Resource
        {
            get
            {
                return StructClientData.Resource;
            }
        }

        protected override string ViewToolTip
        {
            get
            {
                return ResourceLoader.GetString("TootTip_ViewStructs");
            }
        }

        protected override string AddToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_CreateStruct");
            }
        }

        protected override string DeleteToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_DeleteStruct");
            }
        }

        protected override string EditToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ModifyStruct");
            }
        }

        protected override int GroupOrder
        {
            get
            {
                return 1;
            }
        }
        #endregion
    }
}
