﻿using SmartCadControls;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadCore.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadGuiCommon.Controls
{
    public class GridControlDataAdmApplicationPreference : GridControlDataAdm
    {
        public GridControlDataAdmApplicationPreference(ApplicationPreferenceClientData preference)
            : base(preference)
        {
            ApplicationOwner.Add(SmartCadApplications.SmartCadAdministration);
        }

        public ApplicationPreferenceClientData Preference
        {
            get
            {
                return this.Tag as ApplicationPreferenceClientData;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string Name
        {
            get
            {
                return ResourceLoader.GetString2(Preference.Name);
            }

        }

        [GridControlRowInfoData(Searchable = true)]
        public string Type
        {
            get
            {
                return ResourceLoader.GetString2(Preference.Type);
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string Module
        {
            get
            {
                return ResourceLoader.GetString2(Preference.Module);
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string PreferenceValue
        {
            get
            {
                string valuePref = Preference.Value;
                if (valuePref.ToLower().Equals(true.ToString().ToLower()))
                {
                    valuePref = ResourceLoader.GetString2(Preference.MinValue);
                }
                else if (valuePref.ToLower().Equals(false.ToString().ToLower()))
                {
                    valuePref = ResourceLoader.GetString2(Preference.MaxValue);
                }
                return valuePref;
            }
        }

        public override string PreviewFieldValue
        {
            get
            {
                return ResourceLoader.GetString2(Preference.Description);
            }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataAdmApplicationPreference)
            {
                result = Preference.Code == (obj as GridControlDataAdmApplicationPreference).Preference.Code;
            }
            return result;
        }

        #region Override GridControlDataAdm

        public override System.Windows.Forms.Form GetForm(AdministrationRibbonForm form, ClientData data, FormBehavior behaviour)
        {
            return new ApplicationPreferenceXtraForm(form, data as ApplicationPreferenceClientData);
        }

        public override string GetDeletedMessage
        {
            get { throw new NotImplementedException(); }
        }

        public override string GetNotFoundMessage
        {
            get { throw new NotImplementedException(); }
        }

        public override bool ShowPreview
        {
            get { return true; }
        }

        public override bool PageSearch
        {
            get { return false; }
        }

        public override bool InitializeCollections
        {
            get { return true; }
        }

        public override bool Refresh
        {
            get { return true; }
        }

        public override string GroupText
        {
            get { return ResourceLoader.GetString2("Preferences"); }
        }

        public override string Query
        {
            get { return SmartCadHqls.GetAllApplicationPreferences; }
        }

        protected override System.Drawing.Image ViewImage
        {
            get
            {
                return ResourceLoader.GetImage("$Image.ApplicationPreference");
            }
        }

        public override string PageText
        {
            get { return ResourceLoader.GetString("Configuration"); }
        }

        protected override int PageOrder
        {
            get { return 0; }
        }

        protected override string ViewToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ViewPreferences");
            }
        }

        protected override string EditToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ModifyPreference");
            }
        }

        protected override UserResourceClientData Resource
        {
            get { return ApplicationPreferenceClientData.Resource; }
        }

        protected override List<DevExpress.XtraBars.BarButtonItem> CreateBasicButtons()
        {
            Items = new List<DevExpress.XtraBars.BarButtonItem>();

            Items.Add(CreateViewButton());

            Items.Add(CreateEditButton());

            return Items;
        }


        protected override int GroupOrder
        {
            get
            {
                return 0;
            }
        }
        #endregion
    }
}
