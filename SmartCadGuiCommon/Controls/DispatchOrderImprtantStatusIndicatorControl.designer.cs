namespace SmartCadGuiCommon.Controls
{
    partial class DispatchOrderImprtantStatusIndicatorControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraCharts.Series series2 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.PieSeriesLabel pieSeriesLabel3 = new DevExpress.XtraCharts.PieSeriesLabel();
            DevExpress.XtraCharts.PiePointOptions piePointOptions2 = new DevExpress.XtraCharts.PiePointOptions();
            DevExpress.XtraCharts.PieSeriesView pieSeriesView3 = new DevExpress.XtraCharts.PieSeriesView();
            DevExpress.XtraCharts.PieSeriesLabel pieSeriesLabel4 = new DevExpress.XtraCharts.PieSeriesLabel();
            DevExpress.XtraCharts.PieSeriesView pieSeriesView4 = new DevExpress.XtraCharts.PieSeriesView();
            this.chartControl1 = new DevExpress.XtraCharts.ChartControl();
            this.layoutControlDispatchOrder = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.dispatchOrderImprtantStatusGridIndicatorControl1 = new DispatchOrderImprtantStatusGridIndicatorControl();
            this.layoutControlItemGrid = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemChart = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlDispatchOrder)).BeginInit();
            this.layoutControlDispatchOrder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemChart)).BeginInit();
            this.SuspendLayout();
            // 
            // chartControl1
            // 
            this.chartControl1.Location = new System.Drawing.Point(349, 3);
            this.chartControl1.Name = "chartControl1";
            pieSeriesLabel3.LineVisible = true;
            pieSeriesLabel3.Visible = false;
            series2.Label = pieSeriesLabel3;
            piePointOptions2.ValueNumericOptions.Format = DevExpress.XtraCharts.NumericFormat.Percent;
            piePointOptions2.ValueNumericOptions.Precision = 0;
            series2.LegendPointOptions = piePointOptions2;
            series2.Name = "Series 1";
            series2.SeriesPointsSortingKey = DevExpress.XtraCharts.SeriesPointKey.Value_1;
            series2.SynchronizePointOptions = false;
            pieSeriesView3.RuntimeExploding = false;
            series2.View = pieSeriesView3;
            this.chartControl1.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series2};
            pieSeriesLabel4.LineVisible = true;
            this.chartControl1.SeriesTemplate.Label = pieSeriesLabel4;
            pieSeriesView4.RuntimeExploding = false;
            this.chartControl1.SeriesTemplate.View = pieSeriesView4;
            this.chartControl1.Size = new System.Drawing.Size(344, 187);
            this.chartControl1.TabIndex = 2;
            // 
            // layoutControlDispatchOrder
            // 
            this.layoutControlDispatchOrder.Appearance.DisabledLayoutGroupCaption.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControlDispatchOrder.Appearance.DisabledLayoutGroupCaption.Options.UseForeColor = true;
            this.layoutControlDispatchOrder.Appearance.DisabledLayoutItem.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControlDispatchOrder.Appearance.DisabledLayoutItem.Options.UseForeColor = true;
            this.layoutControlDispatchOrder.Controls.Add(this.dispatchOrderImprtantStatusGridIndicatorControl1);
            this.layoutControlDispatchOrder.Controls.Add(this.chartControl1);
            this.layoutControlDispatchOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlDispatchOrder.Location = new System.Drawing.Point(0, 0);
            this.layoutControlDispatchOrder.Name = "layoutControlDispatchOrder";
            this.layoutControlDispatchOrder.Root = this.layoutControlGroup1;
            this.layoutControlDispatchOrder.Size = new System.Drawing.Size(695, 192);
            this.layoutControlDispatchOrder.TabIndex = 4;
            this.layoutControlDispatchOrder.Text = "layoutControl1";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemGrid,
            this.layoutControlItemChart});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(695, 192);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // dispatchOrderImprtantStatusGridIndicatorControl1
            // 
            this.dispatchOrderImprtantStatusGridIndicatorControl1.DataSource = null;
            this.dispatchOrderImprtantStatusGridIndicatorControl1.Location = new System.Drawing.Point(3, 3);
            this.dispatchOrderImprtantStatusGridIndicatorControl1.Name = "dispatchOrderImprtantStatusGridIndicatorControl1";
            this.dispatchOrderImprtantStatusGridIndicatorControl1.Size = new System.Drawing.Size(343, 187);
            this.dispatchOrderImprtantStatusGridIndicatorControl1.TabIndex = 3;
            // 
            // layoutControlItemGrid
            // 
            this.layoutControlItemGrid.Control = this.dispatchOrderImprtantStatusGridIndicatorControl1;
            this.layoutControlItemGrid.CustomizationFormText = "layoutControlItemGrid";
            this.layoutControlItemGrid.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemGrid.Name = "layoutControlItemGrid";
            this.layoutControlItemGrid.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItemGrid.Size = new System.Drawing.Size(346, 190);
            this.layoutControlItemGrid.Text = "layoutControlItemGrid";
            this.layoutControlItemGrid.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemGrid.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemGrid.TextToControlDistance = 0;
            this.layoutControlItemGrid.TextVisible = false;
            // 
            // layoutControlItemChart
            // 
            this.layoutControlItemChart.Control = this.chartControl1;
            this.layoutControlItemChart.CustomizationFormText = "layoutControlItemChart";
            this.layoutControlItemChart.Location = new System.Drawing.Point(346, 0);
            this.layoutControlItemChart.Name = "layoutControlItemChart";
            this.layoutControlItemChart.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItemChart.Size = new System.Drawing.Size(347, 190);
            this.layoutControlItemChart.Text = "layoutControlItemChart";
            this.layoutControlItemChart.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemChart.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemChart.TextToControlDistance = 0;
            this.layoutControlItemChart.TextVisible = false;
            // 
            // DispatchOrderImprtantStatusIndicatorControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControlDispatchOrder);
            this.LookAndFeel.SkinName = "Blue";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "DispatchOrderImprtantStatusIndicatorControl";
            this.Size = new System.Drawing.Size(695, 192);
            ((System.ComponentModel.ISupportInitialize)(pieSeriesLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlDispatchOrder)).EndInit();
            this.layoutControlDispatchOrder.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemChart)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraCharts.ChartControl chartControl1;
        internal DispatchOrderImprtantStatusGridIndicatorControl dispatchOrderImprtantStatusGridIndicatorControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControlDispatchOrder;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemGrid;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemChart;
    }
}
