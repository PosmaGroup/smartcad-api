﻿using SmartCadControls;
using SmartCadControls.Controls;
using SmartCadCore.Core;
using SmartCadCore.Enums;
using Smartmatic.SmartCad.Map;
using System.Windows.Forms;

namespace SmartCadGuiCommon.Controls
{
    partial class MapLayoutDevX : UserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MapLayoutDevX));
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanelTools = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.comboBoxSearchType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.mapLayersControl = new MapLayersControl();
            this.gridControlSearchResult = new GridControlEx();
            this.gridViewSeachResult = new GridViewEx();
            this.buttonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.textEditZone = new DevExpress.XtraEditors.TextEdit();
            this.buttonSearch = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupSearch = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemSearchType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupVisualization = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.buttonDoStuffWithMaps = new DevExpress.XtraEditors.SimpleButton();
            this.toolbarImageList = new System.Windows.Forms.ImageList(this.components);
            this.defaultToolTipController1 = new DevExpress.Utils.DefaultToolTipController(this.components);
            this.toolTipController1 = new DevExpress.Utils.ToolTipController(this.components);
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanelTools.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxSearchType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSearchResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSeachResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditZone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSearchType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupVisualization)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            this.SuspendLayout();
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanelTools});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // dockPanelTools
            // 
            this.dockPanelTools.Controls.Add(this.dockPanel1_Container);
            this.dockPanelTools.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelTools.ForeColor = System.Drawing.Color.White;
            this.dockPanelTools.ID = new System.Guid("3fdf6c77-0fc0-43e5-ba09-cc82438867d3");
            this.dockPanelTools.Location = new System.Drawing.Point(0, 0);
            this.dockPanelTools.Name = "dockPanelTools";
            this.dockPanelTools.Options.AllowDockFill = false;
            this.dockPanelTools.Options.AllowFloating = false;
            this.dockPanelTools.Options.FloatOnDblClick = false;
            this.dockPanelTools.Options.ShowCloseButton = false;
            this.dockPanelTools.Options.ShowMaximizeButton = false;
            this.dockPanelTools.OriginalSize = new System.Drawing.Size(209, 200);
            this.dockPanelTools.Size = new System.Drawing.Size(209, 605);
            this.dockPanelTools.Text = "Herramientas";
            // 
            // dockPanel1_Container
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.dockPanel1_Container, DevExpress.Utils.DefaultBoolean.Default);
            this.dockPanel1_Container.Controls.Add(this.layoutControl1);
            this.dockPanel1_Container.Location = new System.Drawing.Point(4, 23);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(201, 578);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.comboBoxSearchType);
            this.layoutControl1.Controls.Add(this.mapLayersControl);
            this.layoutControl1.Controls.Add(this.gridControlSearchResult);
            this.layoutControl1.Controls.Add(this.buttonCancel);
            this.layoutControl1.Controls.Add(this.textEditZone);
            this.layoutControl1.Controls.Add(this.buttonSearch);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(239, 193, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(201, 578);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // comboBoxSearchType
            // 
            this.comboBoxSearchType.Location = new System.Drawing.Point(31, 27);
            this.comboBoxSearchType.Name = "comboBoxSearchType";
            this.comboBoxSearchType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxSearchType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxSearchType.Size = new System.Drawing.Size(163, 20);
            this.comboBoxSearchType.StyleController = this.layoutControl1;
            this.comboBoxSearchType.TabIndex = 20;
            this.comboBoxSearchType.SelectedIndexChanged += new System.EventHandler(this.comboBoxSearchType_SelectedIndexChanged);
            // 
            // mapLayersControl
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.mapLayersControl, DevExpress.Utils.DefaultBoolean.Default);
            this.mapLayersControl.CurrentShapeType = ShapeType.None;
            this.mapLayersControl.GlobalApplicationPreference = null;
            this.mapLayersControl.LaspointInRoute = null;
            this.mapLayersControl.ListDepartmentByUser = null;
            this.mapLayersControl.Location = new System.Drawing.Point(5, 300);
            this.mapLayersControl.MapControlEx = null;
            this.mapLayersControl.Margin = new System.Windows.Forms.Padding(0);
            this.mapLayersControl.Name = "mapLayersControl";
            this.mapLayersControl.PermissionsUser = null;
            this.mapLayersControl.Size = new System.Drawing.Size(191, 273);
            this.mapLayersControl.TabIndex = 19;
            this.mapLayersControl.TableIncidents = null;
            this.mapLayersControl.TableRoutes = null;
            this.mapLayersControl.TableStations = null;
            this.mapLayersControl.TableStructs = null;
            this.mapLayersControl.TableUnits = null;
            this.mapLayersControl.TableZones = null;
            this.mapLayersControl.TreeViewIncidents = null;
            this.mapLayersControl.TreeViewRoutes = null;
            this.mapLayersControl.TreeViewStations = null;
            this.mapLayersControl.TreeViewStructs = null;
            this.mapLayersControl.TreeViewUnits = null;
            this.mapLayersControl.TreeViewZones = null;
            this.mapLayersControl.UnitsList = null;
            // 
            // gridControlSearchResult
            // 
            this.gridControlSearchResult.EnableAutoFilter = true;
            this.gridControlSearchResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlSearchResult.Location = new System.Drawing.Point(7, 86);
            this.gridControlSearchResult.MainView = this.gridViewSeachResult;
            this.gridControlSearchResult.Name = "gridControlSearchResult";
            this.gridControlSearchResult.Size = new System.Drawing.Size(187, 182);
            this.gridControlSearchResult.TabIndex = 18;
            this.gridControlSearchResult.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewSeachResult});
            this.gridControlSearchResult.ViewTotalRows = true;
            // 
            // gridViewSeachResult
            // 
            this.gridViewSeachResult.AllowFocusedRowChanged = true;
            this.gridViewSeachResult.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewSeachResult.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewSeachResult.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewSeachResult.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewSeachResult.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewSeachResult.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewSeachResult.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewSeachResult.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewSeachResult.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewSeachResult.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewSeachResult.EnablePreviewLineForFocusedRow = false;
            this.gridViewSeachResult.GridControl = this.gridControlSearchResult;
            this.gridViewSeachResult.Name = "gridViewSeachResult";
            this.gridViewSeachResult.OptionsDetail.EnableMasterViewMode = false;
            this.gridViewSeachResult.OptionsMenu.EnableFooterMenu = false;
            this.gridViewSeachResult.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewSeachResult.OptionsView.ColumnAutoWidth = false;
            this.gridViewSeachResult.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewSeachResult.OptionsView.ShowFooter = true;
            this.gridViewSeachResult.ViewTotalRows = true;
            this.gridViewSeachResult.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewSeachResult_FocusedRowChanged);
            this.gridViewSeachResult.Click += new System.EventHandler(this.gridViewSeachResult_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Image = global::SmartCadGuiCommon.PrintRibbonControllerResources.RibbonPrintPreview_ClosePreview;
            this.buttonCancel.Location = new System.Drawing.Point(157, 51);
            this.buttonCancel.MaximumSize = new System.Drawing.Size(30, 0);
            this.buttonCancel.MinimumSize = new System.Drawing.Size(30, 0);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(30, 31);
            this.buttonCancel.StyleController = this.layoutControl1;
            this.buttonCancel.TabIndex = 17;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // textEditZone
            // 
            this.textEditZone.Location = new System.Drawing.Point(7, 51);
            this.textEditZone.Name = "textEditZone";
            this.textEditZone.Size = new System.Drawing.Size(110, 20);
            this.textEditZone.StyleController = this.layoutControl1;
            this.textEditZone.TabIndex = 4;
            this.textEditZone.EditValueChanged += new System.EventHandler(this.textEditZone_EditValueChanged);
            // 
            // buttonSearch
            // 
            this.buttonSearch.Image = global::SmartCadGuiCommon.XRDesignRibbonControllerResources.RibbonUserDesigner_Zoom;
            this.buttonSearch.Location = new System.Drawing.Point(121, 51);
            this.buttonSearch.MaximumSize = new System.Drawing.Size(25, 0);
            this.buttonSearch.MinimumSize = new System.Drawing.Size(25, 0);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(25, 31);
            this.buttonSearch.StyleController = this.layoutControl1;
            this.buttonSearch.TabIndex = 7;
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupSearch,
            this.layoutControlGroupVisualization});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(201, 578);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroupSearch
            // 
            this.layoutControlGroupSearch.CustomizationFormText = "Busqueda";
            this.layoutControlGroupSearch.ExpandButtonLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroupSearch.ExpandButtonVisible = true;
            this.layoutControlGroupSearch.ExpandOnDoubleClick = true;
            this.layoutControlGroupSearch.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem4,
            this.layoutControlItem3,
            this.layoutControlItem2,
            this.layoutControlItemSearchType});
            this.layoutControlGroupSearch.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupSearch.Name = "layoutControlGroupSearch";
            this.layoutControlGroupSearch.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupSearch.Size = new System.Drawing.Size(201, 275);
            this.layoutControlGroupSearch.Text = "Busqueda";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.textEditZone;
            this.layoutControlItem1.CustomizationFormText = "Urb/Sector:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(0, 35);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(61, 35);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(114, 35);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.buttonSearch;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(114, 24);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(36, 35);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(36, 35);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(36, 35);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.gridControlSearchResult;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 59);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(191, 186);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.buttonCancel;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(150, 24);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(41, 35);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(41, 35);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(41, 35);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItemSearchType
            // 
            this.layoutControlItemSearchType.Control = this.comboBoxSearchType;
            this.layoutControlItemSearchType.CustomizationFormText = "text";
            this.layoutControlItemSearchType.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemSearchType.Name = "layoutControlItemSearchType";
            this.layoutControlItemSearchType.Size = new System.Drawing.Size(191, 24);
            this.layoutControlItemSearchType.Text = "text";
            this.layoutControlItemSearchType.TextSize = new System.Drawing.Size(20, 13);
            // 
            // layoutControlGroupVisualization
            // 
            this.layoutControlGroupVisualization.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroupVisualization.ExpandButtonLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroupVisualization.ExpandButtonVisible = true;
            this.layoutControlGroupVisualization.ExpandOnDoubleClick = true;
            this.layoutControlGroupVisualization.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5});
            this.layoutControlGroupVisualization.Location = new System.Drawing.Point(0, 275);
            this.layoutControlGroupVisualization.Name = "layoutControlGroupVisualization";
            this.layoutControlGroupVisualization.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlGroupVisualization.Size = new System.Drawing.Size(201, 303);
            this.layoutControlGroupVisualization.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlGroupVisualization.Text = "Visualizacion";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.mapLayersControl;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(195, 277);
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // panelControl1
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.panelControl1, DevExpress.Utils.DefaultBoolean.Default);
            this.panelControl1.Controls.Add(this.buttonDoStuffWithMaps);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(209, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(703, 605);
            this.panelControl1.TabIndex = 1;
            // 
            // buttonDoStuffWithMaps
            // 
            this.buttonDoStuffWithMaps.Enabled = false;
            this.buttonDoStuffWithMaps.Location = new System.Drawing.Point(173, 301);
            this.buttonDoStuffWithMaps.Name = "buttonDoStuffWithMaps";
            this.buttonDoStuffWithMaps.Size = new System.Drawing.Size(75, 23);
            this.buttonDoStuffWithMaps.TabIndex = 1;
            this.buttonDoStuffWithMaps.Text = "simpleButton1";
            this.buttonDoStuffWithMaps.Visible = false;
            this.buttonDoStuffWithMaps.Click += new System.EventHandler(this.buttonDoStuffWithMaps_Click);
            // 
            // toolbarImageList
            // 
            this.toolbarImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("toolbarImageList.ImageStream")));
            this.toolbarImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.toolbarImageList.Images.SetKeyName(0, "Select.gif");
            this.toolbarImageList.Images.SetKeyName(1, "Zoom-in.gif");
            this.toolbarImageList.Images.SetKeyName(2, "Zoom-out.gif");
            this.toolbarImageList.Images.SetKeyName(3, "Pan.gif");
            this.toolbarImageList.Images.SetKeyName(4, "Refresh.gif");
            this.toolbarImageList.Images.SetKeyName(5, "AddPosition.gif");
            this.toolbarImageList.Images.SetKeyName(6, "RemovePosition.gif");
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.comboBoxSearchType;
            this.layoutControlItem6.CustomizationFormText = "text";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(275, 31);
            this.layoutControlItem6.Text = "text";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(20, 20);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // MapLayoutDevX
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this, DevExpress.Utils.DefaultBoolean.Default);
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.dockPanelTools);
            this.Name = "MapLayoutDevX";
            this.Size = new System.Drawing.Size(912, 605);
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanelTools.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxSearchType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSearchResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSeachResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditZone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSearchType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupVisualization)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            this.ResumeLayout(false);

        }

       

        #endregion

        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelTools;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit textEditZone;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.SimpleButton buttonSearch;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupSearch;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupVisualization;
        private System.Windows.Forms.ImageList toolbarImageList;
        private DevExpress.Utils.DefaultToolTipController defaultToolTipController1;
        private DevExpress.Utils.ToolTipController toolTipController1;
        private MapControlEx mapControlEx1;
        private DevExpress.XtraEditors.SimpleButton buttonCancel;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private GridControlEx gridControlSearchResult;
        private GridViewEx gridViewSeachResult;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.SimpleButton buttonDoStuffWithMaps;
		public MapLayersControl mapLayersControl;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxSearchType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemSearchType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
    }
}
