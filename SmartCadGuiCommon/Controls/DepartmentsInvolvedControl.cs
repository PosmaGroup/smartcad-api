using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using SmartCadControls.Controls;
using SmartCadCore.Common;
using SmartCadCore.Enums;

namespace SmartCadGuiCommon.Controls
{
    
    public partial class DepartmentsInvolvedControl :  HeaderPanelControl
    {

        private FrontClientStateEnum frontClientState;
     
        public DepartmentsInvolvedControl()
        {
            InitializeComponent();                         
        }

        public FrontClientStateEnum FrontClientState
        {
            get
            {
                return this.frontClientState;
            }
            set
            {
                this.frontClientState = value;
                if (value == FrontClientStateEnum.WaitingForIncident)
                    SetFrontClientWaitingForCallState();
                else if (value == FrontClientStateEnum.RegisteringIncident)
                    SetFrontClientRegisteringCallState();
                else if (value == FrontClientStateEnum.UpdatingIncident)
                    SetFrontClientUpdatingCallState();
            }
        }

        private void SetFrontClientUpdatingCallState()
        {
               this.Enabled = false;
        }

        private void SetFrontClientRegisteringCallState()
        {
                this.Enabled = true;
        }

        private void SetFrontClientWaitingForCallState()
        {
            this.Enabled = false;
        }

        public new bool Focus()
        {
            return this.departmentTypesControl.Focus();
        }

        public override bool Active
        {
            get
            {
                return base.Active;
            }
            set
            {
                base.Active = value;

				Image img;

				img = pictureBoxNumber.Image;

				pictureBoxNumber.Image = ResourceLoader.GetImage(GetNumberImageName());

				if (img != null)
					img.Dispose();
				if (Active)
				{
					this.pictureBoxNumber.LookAndFeel.Style = GetStyle(ActiveColor);
					this.groupControlBody.LookAndFeel.Style = GetStyle(ActiveColor);
                    this.layoutControl1.LookAndFeel.Style = GetStyle(ActiveColor);
				}
				else
				{
					this.pictureBoxNumber.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
					this.groupControlBody.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
					this.layoutControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
					this.pictureBoxNumber.LookAndFeel.SkinName = InactiveColor;
					this.groupControlBody.LookAndFeel.SkinName = InactiveColor;
					this.layoutControl1.LookAndFeel.SkinName = InactiveColor;
             
				}
            }
        }

      private void DepartmentsInvolvedControl_Load(object sender, EventArgs e)
        {
            this.groupControlBody.Text = ResourceLoader.GetString2("DispatchRequest");
        }
    }
}
