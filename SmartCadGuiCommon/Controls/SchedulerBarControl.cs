using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraScheduler;
using Smartmatic.SmartCad.Service;
using DevExpress.XtraBars;
using DevExpress.Utils;
using SmartCadCore.Common;

using SmartCadCore.ClientData;


namespace SmartCadGuiCommon.Controls
{
    public partial class SchedulerBarControl : DevExpress.XtraEditors.XtraUserControl
    {

        private int filter = -1;

        public SchedulerBarControl()
        {
            InitializeComponent();
            LoadLanguage();
            LoadComboBoxExtraVacations();
            LoadLegends();
            schedulerControl.ActiveView.AppointmentDisplayOptions.EndTimeVisibility = AppointmentTimeVisibility.Never;
            schedulerControl.ActiveView.AppointmentDisplayOptions.StartTimeVisibility = AppointmentTimeVisibility.Never;
            this.dateNavigator.SchedulerControl.Start = DateTime.Now.AddDays(-DateTime.Now.Day);

        }

        private void LoadLegends()
        {
            Color workShift = schedulerControl.Storage.Appointments.Labels[0].Color;
            Color extra = schedulerControl.Storage.Appointments.Labels[1].Color;
            Color timeOff = schedulerControl.Storage.Appointments.Labels[2].Color;
           
            
            Bitmap image = new Bitmap(16, 16, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            Graphics g = Graphics.FromImage(image);
            g.FillRectangle(new SolidBrush(workShift), 0, 0, 16, 16);
            g.DrawRectangle(new Pen(Color.Black), 0, 0, 15, 15);
            this.labelControlWorkShift.Appearance.Image = image;
            this.labelControlWorkShift.ImageAlignToText = ImageAlignToText.LeftCenter;
            g.Dispose();

            Bitmap image1 = new Bitmap(16, 16, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            g = Graphics.FromImage(image1);
            g.FillRectangle(new SolidBrush(extra), 0, 0, 16, 16);
            g.DrawRectangle(new Pen(Color.Black), 0, 0, 15, 15);
            this.labelControlExtra.Appearance.Image = image1;
            this.labelControlExtra.ImageAlignToText = ImageAlignToText.LeftCenter;
            g.Dispose();


            Bitmap image2 = new Bitmap(16, 16, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            g = Graphics.FromImage(image2);
            g.FillRectangle(new SolidBrush(timeOff), 0, 0, 16, 16);
            g.DrawRectangle(new Pen(Color.Black), 0, 0, 15, 15);
            this.labelControlVacations.Appearance.Image = image2;
            this.labelControlVacations.ImageAlignToText = ImageAlignToText.LeftCenter;
            g.Dispose();
        }

        private void LoadComboBoxExtraVacations()
        {
            this.repositoryItemImageComboBox1.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem(ResourceLoader.GetString2("All"), 0, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem(ResourceLoader.GetString2("TypeWorkShift"), 1, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem(ResourceLoader.GetString2("ExtraTime"), 2, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem(ResourceLoader.GetString2("VacationsAndPermissions"), 3, -1)});
            this.comboBoxExtraVacations.EditValue = 0;
        }

        private void LoadLanguage()
        {
            this.dockCalendar.TabText = this.dockCalendar.Text = ResourceLoader.GetString2("SchedulerOperatorTabTitle");
            this.layoutControlGroupLegend.Text = ResourceLoader.GetString2("Legend");

            //this.toolTipController1.SetToolTip(this.dateNavigator,  ResourceLoader.GetString2("HelpCalendarPersonalFileForm"));

            this.labelControlExtra.Text = ResourceLoader.GetString2("ExtraTime");
            this.labelControlVacations.Text = ResourceLoader.GetString2("FreeTimeFormText");
            labelControlWorkShift.Text = ResourceLoader.GetString2("TypeWorkShift");

            this.labelBarShowFor.Caption = ResourceLoader.GetString2("ShowBy") + ":";
            this.viewNavigatorBackwardItem1.Caption = ResourceLoader.GetString2("ViewNavigatorBackwardTitle");
            this.viewNavigatorBackwardItem1.SuperTip = GetSuperToolTip("ViewNavigatorBackwardTitle","ViewNavigatorBackwardSuperTipContents");



            this.viewNavigatorForwardItem1.Caption = ResourceLoader.GetString2("ViewNavigatorForwardTitle");
            this.viewNavigatorForwardItem1.SuperTip = GetSuperToolTip("ViewNavigatorForwardTitle", "ViewNavigatorForwardSuperTipContents");

            this.viewNavigatorTodayItem1.Caption = ResourceLoader.GetString2("ViewNavigatorTodayTitle");
            this.viewNavigatorTodayItem1.SuperTip = GetSuperToolTip("ViewNavigatorTodayTitle", "ViewNavigatorTodaySuperTipContents");

            this.viewNavigatorZoomInItem1.Caption = ResourceLoader.GetString2("ZoomIn");
            this.viewNavigatorZoomInItem1.SuperTip = GetSuperToolTip("ZoomIn", "ZoomInSuperToolTipContents");


            this.viewNavigatorZoomOutItem1.Caption = ResourceLoader.GetString2("ZoomOut");
            this.viewNavigatorZoomOutItem1.SuperTip = GetSuperToolTip("ZoomOut", "ZoomOutSuperToolTipContents");

            this.viewSelectorItem1.Caption = ResourceLoader.GetString2("Day");
            this.viewSelectorItem1.SuperTip = GetSuperToolTip("Day", "DaySuperToolTipContents");

            this.viewSelectorItem2.Caption = ResourceLoader.GetString2("WorkWeek");
            this.viewSelectorItem2.SuperTip = GetSuperToolTip("WorkWeek", "WorkWeekSuperToolTipContents");

            this.viewSelectorItem3.Caption = ResourceLoader.GetString2("Week");
            this.viewSelectorItem3.SuperTip = GetSuperToolTip("Week", "WeekSuperToolTipContents");

            this.viewSelectorItem4.Caption = ResourceLoader.GetString2("Month");
            this.viewSelectorItem4.SuperTip = GetSuperToolTip("Month", "MonthSuperToolTipContents");

            this.viewSelectorItem5.Caption = ResourceLoader.GetString2("Calendar");
            this.viewSelectorItem5.SuperTip = GetSuperToolTip("Calendar", "TimeLineSuperToolTipContents");

            //toolTipController.SetToolTip(dateNavigator,ResourceLoader.GetString2("ToolTipSchedulerBarControl"));

            this.dateNavigator.TodayButton.Text = ResourceLoader.GetString2("Today");


            // Super tool tip (tooltipcontroller.setToolTip) doesnt work
            SuperToolTip sTooltip1 = new SuperToolTip();
            ToolTipTitleItem titleItem1 = new ToolTipTitleItem();
            titleItem1.Text = ResourceLoader.GetString2("TimeLine");
            ToolTipItem item1 = new ToolTipItem();
            item1.Text = ResourceLoader.GetString2("ToolTipSchedulerBarControl");
            sTooltip1.Items.Add(titleItem1);
            sTooltip1.Items.Add(item1);
            this.toolTipController.SetSuperTip(dateNavigator, sTooltip1);

            this.schedulerControl.OptionsView.NavigationButtons.NextCaption = ResourceLoader.GetString2("NextAppointmentCaption");
            this.schedulerControl.OptionsView.NavigationButtons.PrevCaption = ResourceLoader.GetString2("PrevAppointmentCaption");
        }

        private SuperToolTip GetSuperToolTip(string tooltiptext,string supertooltipcontents) 
        {
            ToolTipTitleItem tttitem = new ToolTipTitleItem();
            ToolTipItem ttitem = new ToolTipItem();
            tttitem.Text = ResourceLoader.GetString2(tooltiptext);
            ttitem.LeftIndent = 6;
            ttitem.Text = ResourceLoader.GetString2(supertooltipcontents);
            SuperToolTip supertt = new SuperToolTip();
            supertt.Items.Add(tttitem);
            supertt.Items.Add(ttitem);
            return supertt;
        }


        private void schedulerControl_EditAppointmentFormShowing(object sender, DevExpress.XtraScheduler.AppointmentFormEventArgs e)
        {
            e.DialogResult = DialogResult.None;
            AppointmentForm form = new AppointmentForm(e.Appointment);
            form.ShowDialog();
            e.DialogResult = form.DialogResult;
            e.Handled = true;
        }

        public void AddAppointment( Appointment app)
        { 
            
        }

        public void CreateAddAppointment(WorkShiftVariationClientData client)
        {
            //client = (WorkShiftVariationClientData)ServerServiceClient.GetInstance().SearchClientObject(
            //            SmartCadHqls.GetCustomHql(SmartCadHqls.GetWorkShiftVariationWithSchedulesByCode, client.Code));
            schedulerControl.BeginUpdate();
            foreach (WorkShiftScheduleVariationClientData var in client.Schedules)
            {
                Appointment app = this.schedulerStorage.CreateAppointment(AppointmentType.Normal);
                app.Start = var.Start;
                app.End = var.End;
                app.Subject = client.Name;
                app.Description = client.Description;
                if (client.Type == WorkShiftVariationClientData.WorkShiftType.WorkShift) 
                {
                    app.CustomFields["AType"] = (int)WorkShiftVariationClientData.WorkShiftType.WorkShift;
                    app.LabelId = 0;
                }
                else if (client.Type == WorkShiftVariationClientData.WorkShiftType.ExtraTime)
                {
                    app.CustomFields["AType"] = (int)WorkShiftVariationClientData.WorkShiftType.ExtraTime;
                    app.LabelId = 1;
                }
                else if (client.Type == WorkShiftVariationClientData.WorkShiftType.TimeOff)
                {
                    app.CustomFields["AType"] = (int)WorkShiftVariationClientData.WorkShiftType.TimeOff;
                    app.LabelId = 2;
                }
                
                app.CustomFields["Color"] = schedulerControl.Storage.Appointments.Labels[app.LabelId].Color;
                
                schedulerControl.Storage.Appointments.Add(app);  
            }
            schedulerControl.EndUpdate();
        }

        public void AddAppointmentsByOperator(OperatorClientData ope)
        { 
            
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.dateNavigator.Visible = true;
        }

        private void barEditItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.dateNavigator.Visible = true;
        }

        private void dateNavigator_Click(object sender, EventArgs e)
        {

        }

        private void comboBoxExtraVacations_EditValueChanged(object sender, EventArgs e)
        {
            BarEditItem item = sender as BarEditItem;
            if (item != null) 
            {
                int index = (int)item.EditValue;
                switch (index)
                {
                    case 0:
                        filter = -1;
                        schedulerControl.ActiveView.LayoutChanged();
                        break;
                    case 1:
                        filter = 0;
                        schedulerControl.ActiveView.LayoutChanged();
                        break;
                    case 2:
                        filter = 1;
                        schedulerControl.ActiveView.LayoutChanged();
                        break;
                    case 3:
                        filter = 2;
                        schedulerControl.ActiveView.LayoutChanged();
                        break;
                }
            }
        }

        private void schedulerStorage_FilterAppointment(object sender, PersistentObjectCancelEventArgs e)
        {
            if (filter == -1)
            {
                e.Cancel = false;
            }
            else if (filter == 0) 
            {
                e.Cancel = ((Appointment)e.Object).LabelId != 0;
            }
            else if (filter == 1)
            {
                e.Cancel = ((Appointment)e.Object).LabelId != 1;
            }
            else if (filter == 2)
            {
                e.Cancel = ((Appointment)e.Object).LabelId != 2;
            }
        }

        private void schedulerControl_PreparePopupMenu(object sender, PreparePopupMenuEventArgs e)
        {
            e.Menu = null;
        }

        private void schedulerControl_InitAppointmentDisplayText(object sender, AppointmentDisplayTextEventArgs e)
        {
            e.Text = e.Appointment.Subject;
        }
    }
}
