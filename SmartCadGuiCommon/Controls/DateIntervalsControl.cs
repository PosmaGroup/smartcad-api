using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using DevExpress.XtraEditors;
using SmartCadCore.Common;
using SmartCadGuiCommon.SyncBoxes;




namespace SmartCadGuiCommon.Controls
{
    public partial class DateIntervalsControl : UserControl
    {
        private const int DAYS_COUNT = 7;
        public DateIntervalsControl()
        {
            InitializeComponent();
            LoadLanguage();
            radioButtonScheduler.Checked = true;
            radioButtonDayMonthly.Checked = true;
            radioButtonEveryDaily.Checked = true;
            FillCombos();
            dateNavigator1.DateTime = DateTime.Now.AddMonths(1);
            dateNavigator1.DateTime = DateTime.Now;
            toolTip1.SetToolTip(dateNavigator1, ResourceLoader.GetString2("ToolTip_DateNavigatorControl"));
            dateEditFrom.Properties.MinValue = DateTime.Now;
            dateEditTo.Properties.MinValue = DateTime.Now;
            dateEditFrom.DateTime = DateTime.Now;
            dateEditTo.DateTime = DateTime.Now;
        }

        private void LoadLanguage() 
        {
           layoutControlItemBegin.Text = ResourceLoader.GetString2("LabelStartTime");
           layoutControlItemEnd.Text = ResourceLoader.GetString2("LabelEndTime");
           
            dateNavigator1.TodayButton.Text = ResourceLoader.GetString2("Today");

            checkEditThur.Text = ResourceLoader.GetString2("Thursday");
            checkFrid.Text = ResourceLoader.GetString2("Friday");
            checkMon.Text = ResourceLoader.GetString2("Monday");
            checkSat.Text = ResourceLoader.GetString2("Saturday");
            checkSund.Text = ResourceLoader.GetString2("Sunday");
            checkTues.Text = ResourceLoader.GetString2("Tuesday");
            checkWed.Text = ResourceLoader.GetString2("Wednesday");

            radioButtonDaily.Text = ResourceLoader.GetString2("Daily");
            radioButtonMonthly.Text = ResourceLoader.GetString2("Monthly");
            radioButtonRotative.Text = ResourceLoader.GetString2("Rotative");
            radioButtonScheduler.Text = ResourceLoader.GetString2("ByCalendar");
            radioButtonWeekly.Text = ResourceLoader.GetString2("Weekly");

            radioButtonEveryDaily.Text = ResourceLoader.GetString2("Every");
            layoutControlItemDaysDaily.Text = ResourceLoader.GetString2("DayOrDays");
            radioButtonWeekday.Text = ResourceLoader.GetString2("EveryWeekday");

            labelEvery.Text = ResourceLoader.GetString2("Every");
            layoutControlItemWeeksWeekly.Text = " " + ResourceLoader.GetString2("WeekOrWeeks") + " " + ResourceLoader.GetString2("The").ToLower() + ":";
            
            radioButtonTheMonthly.Text = ResourceLoader.GetString2("The");
            layoutControlItemEveryMonthly.Text = ResourceLoader.GetString2("OfEvery");
            layoutControlItemEveryMonthly1.Text = ResourceLoader.GetString2("OfEvery");
            layoutControlItemMonthMonthly.Text = ResourceLoader.GetString2("MonthOrMonths");
            layoutControlItemMonthMonthly1.Text = ResourceLoader.GetString2("MonthOrMonths");            
            radioButtonDayMonthly.Text = ResourceLoader.GetString2("Day");
            labelWorkingDay.Text = ResourceLoader.GetString2("WorkingDay")+ ":";
            labelFree.Text = ResourceLoader.GetString2("Rest") + ":";
            layoutControlItemHoursFree.Text = ResourceLoader.GetString2("HourOrHours");
            layoutControlItemHoursWork.Text = ResourceLoader.GetString2("HourOrHours");
            layoutControlItemTo.Text = ResourceLoader.GetString2("LabelTo");
            layoutControlItemFrom.Text = ResourceLoader.GetString2("LabelFrom");

        }

        private void FillCombos()
        {
            comboBoxEditEnum.Properties.Items.Clear();
            comboBoxEditDays.Properties.Items.Clear();
            comboBoxEditEnum.Properties.Items.AddRange(new string[] { 
                                                                    ResourceLoader.GetString2("First"),
                                                                    ResourceLoader.GetString2("Second"),
                                                                    ResourceLoader.GetString2("Third"),
                                                                    ResourceLoader.GetString2("Fourth"),
                                                                    ResourceLoader.GetString2("Fifth")
                                                                    });
            comboBoxEditDays.Properties.Items.AddRange(new string[] { 
                                                                    ResourceLoader.GetString2("Sunday"),
                                                                    ResourceLoader.GetString2("Monday"),
                                                                    ResourceLoader.GetString2("Tuesday"),
                                                                    ResourceLoader.GetString2("Wednesday"),
                                                                    ResourceLoader.GetString2("Thursday"),
                                                                    ResourceLoader.GetString2("Friday"),
                                                                    ResourceLoader.GetString2("Saturday")
                                                                  
        });
      
        }



        private void radioButton_CheckedChanged(object sender, EventArgs e)
        {
            timeEditEnd.Enabled = true;
            dateEditFrom.Enabled = true;
            dateEditTo.Enabled = true;
            layoutControlGroupDaily.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            layoutControlGroupMonthly.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            layoutControlGroupWeekly.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            layoutControlGroupScheduler.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            this.layoutControlGroupRotative.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            emptySpaceItemHeader.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
  
            if (radioButtonScheduler.Checked == true)
            {
                emptySpaceItemHeader.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                layoutControlGroupScheduler.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                this.emptySpaceItemHeader.Height = 0;
                dateEditFrom.Enabled = false;
                dateEditTo.Enabled = false;
            }
            else if (radioButtonDaily.Checked == true)
            {
                layoutControlGroupDaily.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;          
            }
            else if(radioButtonWeekly.Checked == true)
            {
                layoutControlGroupWeekly.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            }
            else if (radioButtonMonthly.Checked == true)
            {
                layoutControlGroupMonthly.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            }
            else if (radioButtonRotative.Checked == true) 
            {
                layoutControlGroupRotative.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                timeEditEnd.Enabled = false;
            }

            this.emptySpaceItemHeader.MaxSize = new Size(emptySpaceItemHeader.Width, emptySpaceItemHeader.Height);
            //RaiseParameters_Change();     
        }

        public IList GetDateIntervals()
        {
            RaiseParameters_Change(null,null);
            IList Intervals = new ArrayList();
            int errors = 0;

            DateTime startDate = BuildDate(dateEditFrom.DateTime.Date, timeEditBegin.Time); 
            DateTime endDate = dateEditTo.DateTime.Date.AddDays(1).AddSeconds(-1);
            int step = 1;

            if (radioButtonScheduler.Checked == true)
            {
                if (dateNavigator1.Selection != null)
                {
                    foreach (DateTime date in dateNavigator1.Selection)
                    {
                        DateTime start = BuildDate(date, timeEditBegin.Time);
                        DateTime end = BuildDate(date, timeEditEnd.Time);
                        if (start >= DateTime.Now)
                        {
                            VariationScheduleGridData gridData = new VariationScheduleGridData();
                            gridData.BeginTime = start;
                            gridData.EndTime = end;
                          
                            gridData.Date = start;
                            Intervals.Add(gridData);
                        }
                    }
                }
            }
            else if (radioButtonDaily.Checked == true)
            {

                if (radioButtonEveryDaily.Checked == true)
                {
                    step = (int)spinEditDayDaily.Value;
                    DateTime end = BuildDate(endDate, timeEditEnd.Time);

                    for (DateTime st = startDate; st <= endDate; st = st.AddDays(step))
                    {
                        VariationScheduleGridData gridData = new VariationScheduleGridData();
                        gridData.BeginTime = st;
                        gridData.EndTime = end.AddDays(step);
                        gridData.Date = st;
                        Intervals.Add(gridData);
                    }
                }
                else if (radioButtonWeekday.Checked == true)
                {
                    DateTime end = BuildDate(endDate, timeEditEnd.Time);

                    for (DateTime st = startDate; st <= endDate; st = st.AddDays(step))
                    {
                        if (st.DayOfWeek != DayOfWeek.Sunday && st.DayOfWeek != DayOfWeek.Saturday)
                        {
                            VariationScheduleGridData gridData = new VariationScheduleGridData();
                            gridData.BeginTime = st;
                            gridData.EndTime = end.AddDays(step);
                            gridData.Date = st;
                            Intervals.Add(gridData);
                        }
                    }
                }               
                
            }
            else if (radioButtonWeekly.Checked == true)
            {
                DateTime end = BuildDate(endDate, timeEditEnd.Time);

                for (DateTime st = startDate; st <= endDate; st = st.AddDays(step))
                {
                    VariationScheduleGridData gridData;
                    if (st.DayOfWeek == DayOfWeek.Monday && this.checkMon.Checked == true)
                    {
                        gridData = new VariationScheduleGridData();
                        gridData.BeginTime = st;
                        gridData.EndTime = end;
                        gridData.Date = st;
                        Intervals.Add(gridData);
                    }
                    else if (st.DayOfWeek == DayOfWeek.Tuesday && this.checkTues.Checked == true)
                    {
                        gridData = new VariationScheduleGridData();
                        gridData.BeginTime = st;
                        gridData.EndTime = end;
                        gridData.Date = st;
                        Intervals.Add(gridData);
                    }
                    else if (st.DayOfWeek == DayOfWeek.Wednesday && this.checkWed.Checked == true)
                    {
                        gridData = new VariationScheduleGridData();
                        gridData.BeginTime = st;
                        gridData.EndTime = end;
                        gridData.Date = st;
                        Intervals.Add(gridData);
                    }
                    else if (st.DayOfWeek == DayOfWeek.Thursday && this.checkEditThur.Checked == true)
                    {
                        gridData = new VariationScheduleGridData();
                        gridData.BeginTime = st;
                        gridData.EndTime = end;
                        gridData.Date = st;
                        Intervals.Add(gridData);
                    }
                    else if (st.DayOfWeek == DayOfWeek.Friday && this.checkFrid.Checked == true)
                    {
                        gridData = new VariationScheduleGridData();
                        gridData.BeginTime = st;
                        gridData.EndTime = end;
                        gridData.Date = st;
                        Intervals.Add(gridData);
                    }
                    else if (st.DayOfWeek == DayOfWeek.Saturday && this.checkSat.Checked == true)
                    {
                        gridData = new VariationScheduleGridData();
                        gridData.BeginTime = st;
                        gridData.EndTime = end;
                        gridData.Date = st;
                        Intervals.Add(gridData);
                    }
                    else if (st.DayOfWeek == DayOfWeek.Sunday && this.checkSund.Checked == true)
                    {
                        gridData = new VariationScheduleGridData();
                        gridData.BeginTime = st;
                        gridData.EndTime = end;
                        gridData.Date = st;
                        Intervals.Add(gridData);


                    }

                    if (st.DayOfWeek == DayOfWeek.Sunday)
                        st = st.AddDays((double)(DAYS_COUNT * (spinEditWeekWeekly.Value - 1)));

                }
            }
            else if (radioButtonMonthly.Checked == true)
            {
                errors = 0;
                if (radioButtonDayMonthly.Checked == true)
                {
                    for (DateTime st = startDate; (st.Month <= endDate.Month && st.Year == endDate.Year) || st.Year < endDate.Year; st = st.AddMonths(((int)spinEditMonth.Value)))
                    {
                        VariationScheduleGridData gridData = new VariationScheduleGridData();
                        if (DateTime.DaysInMonth(st.Year, st.Month) >= ((int)spinEditDay.Value))
                        {
                            DateTime date = new DateTime(st.Year, st.Month, ((int)spinEditDay.Value), st.Hour, st.Minute, 0);
                            if (date >= startDate && date <= endDate)
                            {
                                gridData.BeginTime = BuildDate(date,timeEditBegin.Time);
                                gridData.EndTime = BuildDate(date,timeEditEnd.Time);
                                gridData.Date = date;
                                Intervals.Add(gridData);
                            }
                        }
                        else
                        {
                            errors++;
                        }

                    }
                    if (errors > 0)
                    {
                        if (errors == 1)
                            MessageForm.Show(ResourceLoader.GetString2("UnrepresentableOneDate", errors.ToString()), MessageFormType.Error);
                        else
                            MessageForm.Show(ResourceLoader.GetString2("UnrepresentableMultipleDates", errors.ToString()), MessageFormType.Error);
                                                
                    }

                }
                else if (radioButtonTheMonthly.Checked == true)
                {
                    int count = -1;
                    DateTime start = new DateTime(startDate.Year, startDate.Month, 1, startDate.Hour, startDate.Minute, 0);
                    errors = 0;
                    
                    for (DateTime st = start; st <= endDate; st = st.AddDays(step))
                    {
                        if ((int)st.DayOfWeek == comboBoxEditDays.SelectedIndex)
                        {
                            count++;
                            if (count == comboBoxEditEnum.SelectedIndex)
                            {
                                count = -1;
                                DateTime date = new DateTime(st.Year, st.Month, ((int)spinEditDay.Value), st.Hour, st.Minute, 0);
                                if (date >= startDate)
                                {
                                    VariationScheduleGridData gridData = new VariationScheduleGridData();
                                    gridData.BeginTime = BuildDate(st,timeEditBegin.Time);
                                    gridData.EndTime = BuildDate(st, timeEditEnd.Time);
                                    gridData.Date = st;
                                    Intervals.Add(gridData);
                                }
                                st = st.AddMonths((int)spinEditMonth1.Value);
                                st = st.AddDays(-st.Day);
                            }

                        }

                        if (st.Day == DateTime.DaysInMonth(st.Year, st.Month)) 
                        {
                            st = st.AddMonths((int)spinEditMonth1.Value);
                            st = st.AddDays(-st.Day);
                            if (count > -1)
                            {
                                errors++;
                                count = -1;
                            }
                        }


                    }
                    if (errors > 0)
                    {
                        if (errors == 1)
                            MessageForm.Show(ResourceLoader.GetString2("UnrepresentableOneDate", errors.ToString()), MessageFormType.Error);
                        else
                            MessageForm.Show(ResourceLoader.GetString2("UnrepresentableMultipleDates", errors.ToString()), MessageFormType.Error);
                    }
                }
            }
            else if (radioButtonRotative.Checked == true)
            {
                startDate = new DateTime(startDate.Year, startDate.Month, startDate.Day, timeEditBegin.Time.Hour, timeEditBegin.Time.Minute, 0);
                
                for (DateTime st = startDate; st <= endDate; st = st.AddHours((int)spinEditHoursFree.Value))
                {
                    VariationScheduleGridData gridData;
                    TimeSpan timeToWork = new TimeSpan((int)spinEditHoursToWork.Value, 0, 0);
                    DateTime nextDay = new DateTime(st.Year, st.Month, st.Day).AddDays(1);
                    TimeSpan timeToNextDay = nextDay.Subtract(st);
                   
                    while (timeToWork > timeToNextDay)
                    {
                        gridData = new VariationScheduleGridData();
                        gridData.BeginTime = st;
                        st = st.AddTicks(timeToNextDay.Ticks);
                        gridData.EndTime = new DateTime(gridData.BeginTime.Year, gridData.BeginTime.Month, gridData.BeginTime.Day);
                        gridData.Date = gridData.BeginTime.Date;
                        Intervals.Add(gridData);
                        timeToWork = timeToWork.Subtract(timeToNextDay);
                        nextDay = new DateTime(st.Year, st.Month, st.Day).AddDays(1);
                        timeToNextDay = nextDay.Subtract(st);

                    }

                    gridData = new VariationScheduleGridData();
                    gridData.BeginTime = st;
                    st = st.AddHours(timeToWork.TotalHours);
                    gridData.EndTime = st;
                    gridData.Date = st;
                    Intervals.Add(gridData);
                                        
                }
            }

            return Intervals;
        }

        public DateTime BuildDate(DateTime baseDate, DateTime timeBase)
        {
            return new DateTime(baseDate.Year, baseDate.Month, baseDate.Day, timeBase.Hour, timeBase.Minute, timeBase.Second);    
        }

        public void ClearSelection()
        {
            if (radioButtonScheduler.Checked == true)
            {
                dateNavigator1.TodayButton.PerformClick();
            }
            else if (radioButtonDaily.Checked == true)
            {
                spinEditDayDaily.Reset();
            }
            else if (radioButtonWeekday.Checked == true) 
            {
                spinEditWeekWeekly.Reset();
                checkEditThur.Reset();
                checkTues.Reset();
                checkWed.Reset();
                checkSat.Reset();
                checkSund.Reset();
                checkMon.Reset();
                checkFrid.Reset();

            }
            else if (radioButtonMonthly.Checked == true)
            {
                comboBoxEditEnum.Reset();
                comboBoxEditDays.Reset();
                spinEditMonth1.Reset();
                spinEditMonth.Reset();
                spinEditDay.Reset();
            }
            else if (radioButtonRotative.Checked == true)
            {
                spinEditHoursFree.Reset();
                spinEditHoursToWork.Reset();
            }
        }

        public event DateIntervalsParameters_Change Parameters_Change;

        public delegate void DateIntervalsParameters_Change(object sender, int result);

        public virtual void RaiseParameters_Change()
        {
            int result = 0;
            //calculo para ver si se activa
            if (radioButtonScheduler.Checked == true)
            {
                if (dateNavigator1.Selection.Count > 0)
                {
                    result = 2; //La hora de inicio debe ser menor a la de fin
                    DateTime timeCero = new DateTime(timeEditBegin.Time.Year,timeEditBegin.Time.Month,timeEditBegin.Time.Day,0,0,0);                
                    if ((timeEditEnd.Time.TimeOfDay == timeCero.TimeOfDay) || (timeEditBegin.Time.TimeOfDay < timeEditEnd.Time.TimeOfDay))
                    {
                        foreach (DateTime date in dateNavigator1.Selection)
                        {
                            DateTime date2 = date.Add(timeEditBegin.Time.TimeOfDay);
                            if (date2 >= DateTime.Now)
                            {
                                result = 0; //True
                                break;
                            }
                            else
                            {
                                result = 3; //La hora de inicio debe ser mayor o igual a la actual
                            }
                        }
                    }
                }
                else
                {
                    result = 1; //Faltan campos por llenar
                }
            }
            else if (radioButtonRotative.Checked == false)
            {
                DateTime timeCero = new DateTime(timeEditBegin.Time.Year, timeEditBegin.Time.Month, timeEditBegin.Time.Day, 0, 0, 0);
                if (((timeEditEnd.Time == timeCero) || (timeEditBegin.Time.TimeOfDay < timeEditEnd.Time.TimeOfDay)) == false)
                {
                    result = 2;//La hora de inicio debe ser menor a la de fin
                }
                else
                {
                    if (radioButtonMonthly.Checked == true)
                    {
                        if (radioButtonDayMonthly.Checked == true)
                        {
                            if (spinEditDay.Value < 1 || spinEditDay.Value > 31 ||
                                spinEditMonth.Value < 1 || spinEditMonth.Value > 12)
                                result = 1; // Faltan campos por llenar
                        }
                        else if (radioButtonTheMonthly.Checked == true)
                        {
                            if (comboBoxEditEnum.SelectedItem == null ||
                                comboBoxEditDays.SelectedItem == null ||
                                spinEditMonth1.Value < 1 || spinEditMonth1.Value > 12)
                                result = 1; // Faltan campos por llenar
                        }
                    }
                }

                if (dateEditFrom.DateTime > dateEditTo.DateTime)
                {
                    result = 4; //La fecha de inicio debe ser menor que la fecha fin.

                }
            }
            if (Parameters_Change != null)
                Parameters_Change(this, result);
        }
        
        public void RaiseParameters_Change(object sender, EventArgs args)
        {
            RaiseParameters_Change();
        }

        public void SetFocusToDateEditFrom() 
        {
            dateEditFrom.Focus();
            dateEditFrom.Refresh();
        }
        protected override void OnGotFocus(EventArgs e)
        {
            dateEditFrom.Focus();

            base.OnGotFocus(e);
        }
        
        private void dateNavigator1_EditDateModified(object sender, EventArgs e)
        {
            if (dateNavigator1.Selection.Count > 0)
            {
                IList tempList = new ArrayList(dateNavigator1.Selection);
                foreach (DateTime date in tempList)
                {
                    if (date < DateTime.Today)
                    {
                        if (dateNavigator1.Selection.Count > 1)
                            dateNavigator1.Selection.Remove(date);
                        else
                            dateNavigator1.DateTime = DateTime.Today;
                    }
                }
            }
            //RaiseParameters_Change(sender, e);
        }

        private void dateNavigator1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(dateNavigator1, ResourceLoader.GetString2("ToolTip_DateNavigatorControl"));
            toolTip1.AutoPopDelay = 800;
        }

        private void dateNavigator1_CustomDrawDayNumberCell(object sender, DevExpress.XtraEditors.Calendar.CustomDrawDayNumberCellEventArgs e)
        {
            if (e.Date < DateTime.Now.Subtract(DateTime.Now.TimeOfDay))
            {
                e.Style.ForeColor = System.Drawing.Color.LightGray;
            }
        }

        private void spinEditDayDaily_Enter(object sender, EventArgs e)
        {
            radioButtonEveryDaily.Checked = true;
        }

        private void spinEditDay_Enter(object sender, EventArgs e)
        {
            radioButtonDayMonthly.Checked = true;
        }

        private void comboBoxEditEnum_Enter(object sender, EventArgs e)
        {
            radioButtonTheMonthly.Checked = true;
        }

        private void dateEditFrom_EditValueChanged(object sender, EventArgs e)
        {
            dateEditTo.Properties.MinValue = dateEditFrom.DateTime;
        }

                
        private void timeEdit_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            //RaiseParameters_Change();
        }

        private void timeEditBegin_EditValueChanged(object sender, EventArgs e)
        {
            //RaiseParameters_Change();
        }

    }


    //public partial class Month
    //{
    //    private string name;
    //    private int number;

    //    public Month(int number)
    //    {
    //        this.Number = number;
    //    }

    //    public string Name
    //    {
    //        get { return this.name; }
    //    }

    //    public int Number
    //    {
    //        get { return this.number; }
    //        set
    //        {
    //            this.number = value;
    //            this.name = ResourceLoader.GetString2("Month" + value);
    //        }
    //    }

    //    public override string ToString()
    //    {
    //        return this.name;
    //    }

    //    public override bool Equals(object obj)
    //    {
    //        if (obj.GetType() == typeof(Month))
    //        {
    //            return (this.number == ((Month)obj).Number);
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }
    //}

}

