using SmartCadControls.Controls;
namespace SmartCadGuiCommon.Controls
{
    partial class IndicatorDashboardPreferenceClass
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxEditIndicatorType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutControlPreference = new DevExpress.XtraLayout.LayoutControl();
            this.textBoxExIndicatorDescription = new TextBoxEx();
            this.listBoxControlIndicators = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemIndicator = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemIndicatorType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditIndicatorType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlPreference)).BeginInit();
            this.layoutControlPreference.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControlIndicators)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIndicator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIndicatorType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBoxEditIndicatorType
            // 
            this.comboBoxEditIndicatorType.Location = new System.Drawing.Point(7, 32);
            this.comboBoxEditIndicatorType.Name = "comboBoxEditIndicatorType";
            this.comboBoxEditIndicatorType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditIndicatorType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEditIndicatorType.Size = new System.Drawing.Size(157, 20);
            this.comboBoxEditIndicatorType.StyleController = this.layoutControlPreference;
            this.comboBoxEditIndicatorType.TabIndex = 1;
            // 
            // layoutControlPreference
            // 
            this.layoutControlPreference.AllowCustomizationMenu = false;
            this.layoutControlPreference.Appearance.DisabledLayoutGroupCaption.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControlPreference.Appearance.DisabledLayoutGroupCaption.Options.UseForeColor = true;
            this.layoutControlPreference.Appearance.DisabledLayoutItem.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControlPreference.Appearance.DisabledLayoutItem.Options.UseForeColor = true;
            this.layoutControlPreference.Controls.Add(this.comboBoxEditIndicatorType);
            this.layoutControlPreference.Controls.Add(this.textBoxExIndicatorDescription);
            this.layoutControlPreference.Controls.Add(this.listBoxControlIndicators);
            this.layoutControlPreference.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlPreference.Location = new System.Drawing.Point(0, 0);
            this.layoutControlPreference.Name = "layoutControlPreference";
            this.layoutControlPreference.Root = this.layoutControlGroup1;
            this.layoutControlPreference.Size = new System.Drawing.Size(491, 363);
            this.layoutControlPreference.TabIndex = 8;
            this.layoutControlPreference.Text = "layoutControl1";
            // 
            // textBoxExIndicatorDescription
            // 
            this.textBoxExIndicatorDescription.AllowsLetters = true;
            this.textBoxExIndicatorDescription.AllowsNumbers = true;
            this.textBoxExIndicatorDescription.AllowsPunctuation = true;
            this.textBoxExIndicatorDescription.AllowsSeparators = true;
            this.textBoxExIndicatorDescription.AllowsSymbols = true;
            this.textBoxExIndicatorDescription.AllowsWhiteSpaces = true;
            this.textBoxExIndicatorDescription.ExtraAllowedChars = "";
            this.textBoxExIndicatorDescription.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExIndicatorDescription.Location = new System.Drawing.Point(7, 267);
            this.textBoxExIndicatorDescription.Multiline = true;
            this.textBoxExIndicatorDescription.Name = "textBoxExIndicatorDescription";
            this.textBoxExIndicatorDescription.NonAllowedCharacters = "";
            this.textBoxExIndicatorDescription.RegularExpresion = "";
            this.textBoxExIndicatorDescription.Size = new System.Drawing.Size(478, 90);
            this.textBoxExIndicatorDescription.TabIndex = 6;
            // 
            // listBoxControlIndicators
            // 
            this.listBoxControlIndicators.HorizontalScrollbar = true;
            this.listBoxControlIndicators.Location = new System.Drawing.Point(7, 88);
            this.listBoxControlIndicators.Name = "listBoxControlIndicators";
            this.listBoxControlIndicators.Size = new System.Drawing.Size(478, 143);
            this.listBoxControlIndicators.StyleController = this.layoutControlPreference;
            this.listBoxControlIndicators.TabIndex = 7;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemIndicator,
            this.layoutControlItemIndicatorType,
            this.layoutControlItemDescription,
            this.emptySpaceItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(491, 363);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItemIndicator
            // 
            this.layoutControlItemIndicator.Control = this.listBoxControlIndicators;
            this.layoutControlItemIndicator.CustomizationFormText = "layoutControlItemIndicator";
            this.layoutControlItemIndicator.Location = new System.Drawing.Point(0, 56);
            this.layoutControlItemIndicator.Name = "layoutControlItemIndicator";
            this.layoutControlItemIndicator.Size = new System.Drawing.Size(489, 179);
            this.layoutControlItemIndicator.Text = "layoutControlItemIndicator";
            this.layoutControlItemIndicator.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItemIndicator.TextSize = new System.Drawing.Size(154, 20);
            // 
            // layoutControlItemIndicatorType
            // 
            this.layoutControlItemIndicatorType.Control = this.comboBoxEditIndicatorType;
            this.layoutControlItemIndicatorType.CustomizationFormText = "layoutControlItemIndicatorType";
            this.layoutControlItemIndicatorType.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemIndicatorType.Name = "layoutControlItemIndicatorType";
            this.layoutControlItemIndicatorType.Size = new System.Drawing.Size(168, 56);
            this.layoutControlItemIndicatorType.Text = "layoutControlItemIndicatorType";
            this.layoutControlItemIndicatorType.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItemIndicatorType.TextSize = new System.Drawing.Size(154, 20);
            // 
            // layoutControlItemDescription
            // 
            this.layoutControlItemDescription.Control = this.textBoxExIndicatorDescription;
            this.layoutControlItemDescription.CustomizationFormText = "layoutControlItemDescription";
            this.layoutControlItemDescription.Location = new System.Drawing.Point(0, 235);
            this.layoutControlItemDescription.MinSize = new System.Drawing.Size(165, 56);
            this.layoutControlItemDescription.Name = "layoutControlItemDescription";
            this.layoutControlItemDescription.Size = new System.Drawing.Size(489, 126);
            this.layoutControlItemDescription.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemDescription.Text = "layoutControlItemDescription";
            this.layoutControlItemDescription.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItemDescription.TextSize = new System.Drawing.Size(154, 20);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(168, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(321, 56);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // IndicatorDashboardPreferenceClass
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControlPreference);
            this.Name = "IndicatorDashboardPreferenceClass";
            this.Size = new System.Drawing.Size(491, 363);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditIndicatorType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlPreference)).EndInit();
            this.layoutControlPreference.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControlIndicators)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIndicator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIndicatorType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.ComboBoxEdit comboBoxEditIndicatorType;
        internal TextBoxEx textBoxExIndicatorDescription;
        internal DevExpress.XtraEditors.CheckedListBoxControl listBoxControlIndicators;
        private DevExpress.XtraLayout.LayoutControl layoutControlPreference;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        internal DevExpress.XtraLayout.LayoutControlItem layoutControlItemIndicator;
        internal DevExpress.XtraLayout.LayoutControlItem layoutControlItemIndicatorType;
        internal DevExpress.XtraLayout.LayoutControlItem layoutControlItemDescription;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;

    }
}
