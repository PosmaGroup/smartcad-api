namespace SmartCadGuiCommon.Controls
{
    partial class OperatorIndicatorControl2
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraCharts.Series series1 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.PieSeriesLabel pieSeriesLabel1 = new DevExpress.XtraCharts.PieSeriesLabel();
            DevExpress.XtraCharts.PiePointOptions piePointOptions1 = new DevExpress.XtraCharts.PiePointOptions();
            DevExpress.XtraCharts.PieSeriesView pieSeriesView1 = new DevExpress.XtraCharts.PieSeriesView();
            DevExpress.XtraCharts.Series series2 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.PieSeriesLabel pieSeriesLabel2 = new DevExpress.XtraCharts.PieSeriesLabel();
            DevExpress.XtraCharts.PiePointOptions piePointOptions2 = new DevExpress.XtraCharts.PiePointOptions();
            DevExpress.XtraCharts.PieSeriesView pieSeriesView2 = new DevExpress.XtraCharts.PieSeriesView();
            DevExpress.XtraCharts.PieSeriesLabel pieSeriesLabel3 = new DevExpress.XtraCharts.PieSeriesLabel();
            DevExpress.XtraCharts.PieSeriesView pieSeriesView3 = new DevExpress.XtraCharts.PieSeriesView();
            this.chartControl1 = new DevExpress.XtraCharts.ChartControl();
            this.operatorGridIndicatorControl2 = new OperatorGridIndicatorControl2();
            this.layoutControlOperatorIndicatorControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemOperatorGridIndicatorControl2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemChartControl1 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlOperatorIndicatorControl2)).BeginInit();
            this.layoutControlOperatorIndicatorControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOperatorGridIndicatorControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemChartControl1)).BeginInit();
            this.SuspendLayout();
            // 
            // chartControl1
            // 
            this.chartControl1.Location = new System.Drawing.Point(282, 3);
            this.chartControl1.Name = "chartControl1";
            pieSeriesLabel1.LineVisible = true;
            pieSeriesLabel1.Position = DevExpress.XtraCharts.PieSeriesLabelPosition.Radial;
            pieSeriesLabel1.Visible = false;
            series1.Label = pieSeriesLabel1;
            piePointOptions1.PointView = DevExpress.XtraCharts.PointView.ArgumentAndValues;
            piePointOptions1.ValueNumericOptions.Format = DevExpress.XtraCharts.NumericFormat.Percent;
            piePointOptions1.ValueNumericOptions.Precision = 0;
            series1.LegendPointOptions = piePointOptions1;
            series1.Name = "Series 1";
            series1.SynchronizePointOptions = false;
            pieSeriesView1.RuntimeExploding = false;
            series1.View = pieSeriesView1;
            pieSeriesLabel2.LineVisible = true;
            pieSeriesLabel2.Position = DevExpress.XtraCharts.PieSeriesLabelPosition.Inside;
            pieSeriesLabel2.Visible = false;
            series2.Label = pieSeriesLabel2;
            piePointOptions2.PointView = DevExpress.XtraCharts.PointView.ArgumentAndValues;
            piePointOptions2.ValueNumericOptions.Format = DevExpress.XtraCharts.NumericFormat.Percent;
            piePointOptions2.ValueNumericOptions.Precision = 0;
            series2.LegendPointOptions = piePointOptions2;
            series2.Name = "Series 2";
            series2.SynchronizePointOptions = false;
            pieSeriesView2.RuntimeExploding = false;
            series2.View = pieSeriesView2;
            this.chartControl1.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series1,
        series2};
            pieSeriesLabel3.LineVisible = true;
            this.chartControl1.SeriesTemplate.Label = pieSeriesLabel3;
            pieSeriesView3.RuntimeExploding = false;
            this.chartControl1.SeriesTemplate.View = pieSeriesView3;
            this.chartControl1.Size = new System.Drawing.Size(276, 187);
            this.chartControl1.TabIndex = 4;
            // 
            // operatorGridIndicatorControl2
            // 
            this.operatorGridIndicatorControl2.DataSource = null;
            this.operatorGridIndicatorControl2.Location = new System.Drawing.Point(3, 3);
            this.operatorGridIndicatorControl2.Name = "operatorGridIndicatorControl2";
            this.operatorGridIndicatorControl2.Size = new System.Drawing.Size(276, 187);
            this.operatorGridIndicatorControl2.TabIndex = 3;
            // 
            // layoutControlOperatorIndicatorControl2
            // 
            this.layoutControlOperatorIndicatorControl2.Appearance.DisabledLayoutGroupCaption.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControlOperatorIndicatorControl2.Appearance.DisabledLayoutGroupCaption.Options.UseForeColor = true;
            this.layoutControlOperatorIndicatorControl2.Appearance.DisabledLayoutItem.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControlOperatorIndicatorControl2.Appearance.DisabledLayoutItem.Options.UseForeColor = true;
            this.layoutControlOperatorIndicatorControl2.Controls.Add(this.operatorGridIndicatorControl2);
            this.layoutControlOperatorIndicatorControl2.Controls.Add(this.chartControl1);
            this.layoutControlOperatorIndicatorControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlOperatorIndicatorControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlOperatorIndicatorControl2.Name = "layoutControlOperatorIndicatorControl2";
            this.layoutControlOperatorIndicatorControl2.Root = this.layoutControlGroup1;
            this.layoutControlOperatorIndicatorControl2.Size = new System.Drawing.Size(560, 192);
            this.layoutControlOperatorIndicatorControl2.TabIndex = 5;
            this.layoutControlOperatorIndicatorControl2.Text = "layoutControl1";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemOperatorGridIndicatorControl2,
            this.layoutControlItemChartControl1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(560, 192);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItemOperatorGridIndicatorControl2
            // 
            this.layoutControlItemOperatorGridIndicatorControl2.Control = this.operatorGridIndicatorControl2;
            this.layoutControlItemOperatorGridIndicatorControl2.CustomizationFormText = "layoutControlItemOperatorGridIndicatorControl2";
            this.layoutControlItemOperatorGridIndicatorControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemOperatorGridIndicatorControl2.Name = "layoutControlItemOperatorGridIndicatorControl2";
            this.layoutControlItemOperatorGridIndicatorControl2.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItemOperatorGridIndicatorControl2.Size = new System.Drawing.Size(279, 190);
            this.layoutControlItemOperatorGridIndicatorControl2.Text = "layoutControlItemOperatorGridIndicatorControl2";
            this.layoutControlItemOperatorGridIndicatorControl2.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemOperatorGridIndicatorControl2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemOperatorGridIndicatorControl2.TextToControlDistance = 0;
            this.layoutControlItemOperatorGridIndicatorControl2.TextVisible = false;
            // 
            // layoutControlItemChartControl1
            // 
            this.layoutControlItemChartControl1.Control = this.chartControl1;
            this.layoutControlItemChartControl1.CustomizationFormText = "layoutControlItemChartControl1";
            this.layoutControlItemChartControl1.Location = new System.Drawing.Point(279, 0);
            this.layoutControlItemChartControl1.Name = "layoutControlItemChartControl1";
            this.layoutControlItemChartControl1.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItemChartControl1.Size = new System.Drawing.Size(279, 190);
            this.layoutControlItemChartControl1.Text = "layoutControlItemChartControl1";
            this.layoutControlItemChartControl1.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemChartControl1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemChartControl1.TextToControlDistance = 0;
            this.layoutControlItemChartControl1.TextVisible = false;
            // 
            // OperatorIndicatorControl2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControlOperatorIndicatorControl2);
            this.LookAndFeel.SkinName = "Blue";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "OperatorIndicatorControl2";
            this.Size = new System.Drawing.Size(560, 192);
            ((System.ComponentModel.ISupportInitialize)(pieSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlOperatorIndicatorControl2)).EndInit();
            this.layoutControlOperatorIndicatorControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOperatorGridIndicatorControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemChartControl1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal OperatorGridIndicatorControl2 operatorGridIndicatorControl2;
        private DevExpress.XtraCharts.ChartControl chartControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControlOperatorIndicatorControl2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemOperatorGridIndicatorControl2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemChartControl1;
    }
}
