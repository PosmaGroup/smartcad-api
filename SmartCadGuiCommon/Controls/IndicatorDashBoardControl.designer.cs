namespace SmartCadGuiCommon.Controls
{
    partial class IndicatorDashBoardControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IndicatorDashBoardControl));
            this.groupBoxOne = new DevExpress.XtraEditors.GroupControl();
            this.gridControlOne = new DevExpress.XtraGrid.GridControl();
            this.layoutView1 = new DevExpress.XtraGrid.Views.Layout.LayoutView();
            this.layoutViewColumn1 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.layoutViewColumn2 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.repositoryItemImageComboBoxOne = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.imageListIndicatorControl = new System.Windows.Forms.ImageList(this.components);
            this.toolTipController = new DevExpress.Utils.ToolTipController(this.components);
            this.groupBoxTwo = new DevExpress.XtraEditors.GroupControl();
            this.gridControlTwo = new DevExpress.XtraGrid.GridControl();
            this.layoutView2 = new DevExpress.XtraGrid.Views.Layout.LayoutView();
            this.layoutViewColumn3 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.repositoryItemMemoEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.layoutViewField_layoutViewColumn3 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn4 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.repositoryItemImageComboBoxTwo = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.layoutViewField_layoutViewColumn4 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewCard2 = new DevExpress.XtraGrid.Views.Layout.LayoutViewCard();
            this.groupBoxThree = new DevExpress.XtraEditors.GroupControl();
            this.gridControlThree = new DevExpress.XtraGrid.GridControl();
            this.layoutView3 = new DevExpress.XtraGrid.Views.Layout.LayoutView();
            this.layoutViewColumn5 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.repositoryItemMemoEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.layoutViewField_layoutViewColumn5 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn6 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.repositoryItemImageComboBoxThree = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.layoutViewField_layoutViewColumn6 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewCard3 = new DevExpress.XtraGrid.Views.Layout.LayoutViewCard();
            this.layoutViewField1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewField2 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewField3 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewField4 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewField5 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewField6 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewCard1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewCard();
            this.layoutViewField_layoutViewColumn1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewField_layoutViewColumn2 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxOne)).BeginInit();
            this.groupBoxOne.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlOne)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBoxOne)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxTwo)).BeginInit();
            this.groupBoxTwo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTwo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBoxTwo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxThree)).BeginInit();
            this.groupBoxThree.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlThree)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBoxThree)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBoxOne
            // 
            this.groupBoxOne.AppearanceCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxOne.AppearanceCaption.Options.UseFont = true;
            this.groupBoxOne.AppearanceCaption.Options.UseTextOptions = true;
            this.groupBoxOne.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.groupBoxOne.Controls.Add(this.gridControlOne);
            this.groupBoxOne.Location = new System.Drawing.Point(0, 0);
            this.groupBoxOne.LookAndFeel.SkinName = "Blue";
            this.groupBoxOne.Name = "groupBoxOne";
            this.groupBoxOne.Size = new System.Drawing.Size(204, 338);
            this.groupBoxOne.TabIndex = 0;
            this.groupBoxOne.Text = "Sistema";
            // 
            // gridControlOne
            // 
            this.gridControlOne.Location = new System.Drawing.Point(3, 21);
            this.gridControlOne.LookAndFeel.SkinName = "Blue";
            this.gridControlOne.LookAndFeel.UseWindowsXPTheme = true;
            this.gridControlOne.MainView = this.layoutView1;
            this.gridControlOne.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.gridControlOne.Name = "gridControlOne";
            this.gridControlOne.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemImageComboBoxOne,
            this.repositoryItemMemoEdit1});
            this.gridControlOne.Size = new System.Drawing.Size(198, 282);
            this.gridControlOne.TabIndex = 0;
            this.gridControlOne.ToolTipController = this.toolTipController;
            this.gridControlOne.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.layoutView1});
            // 
            // layoutView1
            // 
            this.layoutView1.Appearance.FieldCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.layoutView1.Appearance.FieldCaption.Options.UseFont = true;
            this.layoutView1.Appearance.FieldValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.layoutView1.Appearance.FieldValue.Options.UseFont = true;
            this.layoutView1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.layoutView1.CardHorzInterval = 3;
            this.layoutView1.CardMinSize = new System.Drawing.Size(100, 25);
            this.layoutView1.CardVertInterval = 7;
            this.layoutView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.LayoutViewColumn[] {
            this.layoutViewColumn1,
            this.layoutViewColumn2});
            this.layoutView1.FieldCaptionFormat = "{0}";
            this.layoutView1.GridControl = this.gridControlOne;
            this.layoutView1.Images = this.imageListIndicatorControl;
            this.layoutView1.Name = "layoutView1";
            this.layoutView1.OptionsBehavior.AllowExpandCollapse = false;
            this.layoutView1.OptionsBehavior.ScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Auto;
            this.layoutView1.OptionsCustomization.AllowFilter = false;
            this.layoutView1.OptionsCustomization.AllowSort = false;
            this.layoutView1.OptionsCustomization.ShowGroupCardCaptions = false;
            this.layoutView1.OptionsCustomization.ShowGroupCardIndents = false;
            this.layoutView1.OptionsCustomization.ShowGroupCards = false;
            this.layoutView1.OptionsCustomization.ShowGroupFields = false;
            this.layoutView1.OptionsCustomization.ShowGroupHiddenItems = false;
            this.layoutView1.OptionsCustomization.ShowGroupLayout = false;
            this.layoutView1.OptionsCustomization.ShowGroupLayoutTreeView = false;
            this.layoutView1.OptionsCustomization.ShowGroupView = false;
            this.layoutView1.OptionsCustomization.ShowResetShrinkButtons = false;
            this.layoutView1.OptionsCustomization.ShowSaveLoadLayoutButtons = false;
            this.layoutView1.OptionsCustomization.UseAdvancedRuntimeCustomization = true;
            this.layoutView1.OptionsItemText.AlignMode = DevExpress.XtraGrid.Views.Layout.FieldTextAlignMode.CustomSize;
            this.layoutView1.OptionsItemText.TextToControlDistance = 0;
            this.layoutView1.OptionsMultiRecordMode.MaxCardColumns = 3;
            this.layoutView1.OptionsMultiRecordMode.MaxCardRows = 6;
            this.layoutView1.OptionsView.AllowHotTrackFields = false;
            this.layoutView1.OptionsView.CardArrangeRule = DevExpress.XtraGrid.Views.Layout.LayoutCardArrangeRule.AllowPartialCards;
            this.layoutView1.OptionsView.ShowCardCaption = false;
            this.layoutView1.OptionsView.ShowCardExpandButton = false;
            this.layoutView1.OptionsView.ShowCardLines = false;
            this.layoutView1.OptionsView.ShowFieldHints = false;
            this.layoutView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.layoutView1.OptionsView.ShowHeaderPanel = false;
            this.layoutView1.OptionsView.ViewMode = DevExpress.XtraGrid.Views.Layout.LayoutViewMode.Column;
            this.layoutView1.PaintStyleName = "Skin";
            this.layoutView1.TemplateCard = this.layoutViewCard1;
            // 
            // layoutViewColumn1
            // 
            this.layoutViewColumn1.AppearanceHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.layoutViewColumn1.AppearanceHeader.Options.UseFont = true;
            this.layoutViewColumn1.Caption = "layoutViewColumn1";
            this.layoutViewColumn1.ColumnEdit = this.repositoryItemMemoEdit1;
            this.layoutViewColumn1.FieldName = "IndicatorFriendlyName";
            this.layoutViewColumn1.LayoutViewField = this.layoutViewField_layoutViewColumn1;
            this.layoutViewColumn1.Name = "layoutViewColumn1";
            this.layoutViewColumn1.OptionsColumn.AllowEdit = false;
            this.layoutViewColumn1.OptionsColumn.AllowFocus = false;
            this.layoutViewColumn1.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.layoutViewColumn1.OptionsColumn.AllowIncrementalSearch = false;
            this.layoutViewColumn1.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.layoutViewColumn1.OptionsColumn.AllowMove = false;
            this.layoutViewColumn1.OptionsColumn.AllowSize = false;
            this.layoutViewColumn1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.layoutViewColumn1.OptionsColumn.ReadOnly = true;
            this.layoutViewColumn1.OptionsColumn.ShowCaption = false;
            this.layoutViewColumn1.OptionsFilter.AllowAutoFilter = false;
            this.layoutViewColumn1.OptionsFilter.AllowFilter = false;
            this.layoutViewColumn1.OptionsFilter.ImmediateUpdateAutoFilter = false;
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.repositoryItemMemoEdit1.LinesCount = 2;
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            this.repositoryItemMemoEdit1.ReadOnly = true;
            this.repositoryItemMemoEdit1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            // 
            // layoutViewColumn2
            // 
            this.layoutViewColumn2.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutViewColumn2.AppearanceCell.Options.UseFont = true;
            this.layoutViewColumn2.AppearanceHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.layoutViewColumn2.AppearanceHeader.Options.UseFont = true;
            this.layoutViewColumn2.Caption = "layoutViewColumn2";
            this.layoutViewColumn2.ColumnEdit = this.repositoryItemImageComboBoxOne;
            this.layoutViewColumn2.FieldName = "CustomCode";
            this.layoutViewColumn2.LayoutViewField = this.layoutViewField_layoutViewColumn2;
            this.layoutViewColumn2.Name = "layoutViewColumn2";
            this.layoutViewColumn2.OptionsColumn.AllowEdit = false;
            this.layoutViewColumn2.OptionsColumn.AllowFocus = false;
            this.layoutViewColumn2.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.layoutViewColumn2.OptionsColumn.AllowIncrementalSearch = false;
            this.layoutViewColumn2.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.layoutViewColumn2.OptionsColumn.AllowMove = false;
            this.layoutViewColumn2.OptionsColumn.AllowSize = false;
            this.layoutViewColumn2.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.layoutViewColumn2.OptionsColumn.ReadOnly = true;
            this.layoutViewColumn2.OptionsColumn.ShowCaption = false;
            this.layoutViewColumn2.OptionsFilter.AllowAutoFilter = false;
            this.layoutViewColumn2.OptionsFilter.AllowFilter = false;
            this.layoutViewColumn2.OptionsFilter.ImmediateUpdateAutoFilter = false;
            // 
            // repositoryItemImageComboBoxOne
            // 
            this.repositoryItemImageComboBoxOne.AutoHeight = false;
            this.repositoryItemImageComboBoxOne.MaxLength = 25;
            this.repositoryItemImageComboBoxOne.Name = "repositoryItemImageComboBoxOne";
            this.repositoryItemImageComboBoxOne.SmallImages = this.imageListIndicatorControl;
            // 
            // imageListIndicatorControl
            // 
            this.imageListIndicatorControl.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListIndicatorControl.ImageStream")));
            this.imageListIndicatorControl.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListIndicatorControl.Images.SetKeyName(0, "transparentBox16x16.gif");
            this.imageListIndicatorControl.Images.SetKeyName(1, "pelota-verde.png");
            this.imageListIndicatorControl.Images.SetKeyName(2, "pelota-amarilla.png");
            this.imageListIndicatorControl.Images.SetKeyName(3, "pelota-roja.png");
            // 
            // toolTipController
            // 
            this.toolTipController.GetActiveObjectInfo += new DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventHandler(this.toolTipController_GetActiveObjectInfo);
            // 
            // groupBoxTwo
            // 
            this.groupBoxTwo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.groupBoxTwo.Appearance.Options.UseBackColor = true;
            this.groupBoxTwo.AppearanceCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxTwo.AppearanceCaption.Options.UseFont = true;
            this.groupBoxTwo.AppearanceCaption.Options.UseTextOptions = true;
            this.groupBoxTwo.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.groupBoxTwo.Controls.Add(this.gridControlTwo);
            this.groupBoxTwo.Location = new System.Drawing.Point(204, 0);
            this.groupBoxTwo.LookAndFeel.SkinName = "Blue";
            this.groupBoxTwo.Name = "groupBoxTwo";
            this.groupBoxTwo.Size = new System.Drawing.Size(204, 338);
            this.groupBoxTwo.TabIndex = 1;
            this.groupBoxTwo.Text = "First Level";
            // 
            // gridControlTwo
            // 
            this.gridControlTwo.Location = new System.Drawing.Point(3, 21);
            this.gridControlTwo.LookAndFeel.SkinName = "Blue";
            this.gridControlTwo.LookAndFeel.UseWindowsXPTheme = true;
            this.gridControlTwo.MainView = this.layoutView2;
            this.gridControlTwo.Name = "gridControlTwo";
            this.gridControlTwo.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemImageComboBoxTwo,
            this.repositoryItemMemoEdit2});
            this.gridControlTwo.Size = new System.Drawing.Size(198, 282);
            this.gridControlTwo.TabIndex = 0;
            this.gridControlTwo.ToolTipController = this.toolTipController;
            this.gridControlTwo.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.layoutView2});
            // 
            // layoutView2
            // 
            this.layoutView2.Appearance.FieldCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.layoutView2.Appearance.FieldCaption.Options.UseFont = true;
            this.layoutView2.Appearance.FieldValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.layoutView2.Appearance.FieldValue.Options.UseFont = true;
            this.layoutView2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.layoutView2.CardHorzInterval = 3;
            this.layoutView2.CardMinSize = new System.Drawing.Size(100, 25);
            this.layoutView2.CardVertInterval = 7;
            this.layoutView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.LayoutViewColumn[] {
            this.layoutViewColumn3,
            this.layoutViewColumn4});
            this.layoutView2.FieldCaptionFormat = "{0}";
            this.layoutView2.GridControl = this.gridControlTwo;
            this.layoutView2.Images = this.imageListIndicatorControl;
            this.layoutView2.Name = "layoutView2";
            this.layoutView2.OptionsBehavior.AllowExpandCollapse = false;
            this.layoutView2.OptionsBehavior.ScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Auto;
            this.layoutView2.OptionsCustomization.AllowFilter = false;
            this.layoutView2.OptionsCustomization.AllowSort = false;
            this.layoutView2.OptionsCustomization.ShowGroupCardCaptions = false;
            this.layoutView2.OptionsCustomization.ShowGroupCardIndents = false;
            this.layoutView2.OptionsCustomization.ShowGroupCards = false;
            this.layoutView2.OptionsCustomization.ShowGroupFields = false;
            this.layoutView2.OptionsCustomization.ShowGroupHiddenItems = false;
            this.layoutView2.OptionsCustomization.ShowGroupLayout = false;
            this.layoutView2.OptionsCustomization.ShowGroupLayoutTreeView = false;
            this.layoutView2.OptionsCustomization.ShowGroupView = false;
            this.layoutView2.OptionsCustomization.ShowResetShrinkButtons = false;
            this.layoutView2.OptionsCustomization.ShowSaveLoadLayoutButtons = false;
            this.layoutView2.OptionsCustomization.UseAdvancedRuntimeCustomization = true;
            this.layoutView2.OptionsItemText.AlignMode = DevExpress.XtraGrid.Views.Layout.FieldTextAlignMode.CustomSize;
            this.layoutView2.OptionsItemText.TextToControlDistance = 0;
            this.layoutView2.OptionsMultiRecordMode.MaxCardColumns = 3;
            this.layoutView2.OptionsMultiRecordMode.MaxCardRows = 6;
            this.layoutView2.OptionsView.AllowHotTrackFields = false;
            this.layoutView2.OptionsView.CardArrangeRule = DevExpress.XtraGrid.Views.Layout.LayoutCardArrangeRule.AllowPartialCards;
            this.layoutView2.OptionsView.ShowCardCaption = false;
            this.layoutView2.OptionsView.ShowCardExpandButton = false;
            this.layoutView2.OptionsView.ShowCardLines = false;
            this.layoutView2.OptionsView.ShowFieldHints = false;
            this.layoutView2.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.layoutView2.OptionsView.ShowHeaderPanel = false;
            this.layoutView2.OptionsView.ViewMode = DevExpress.XtraGrid.Views.Layout.LayoutViewMode.MultiRow;
            this.layoutView2.PaintStyleName = "Skin";
            this.layoutView2.TemplateCard = this.layoutViewCard2;
            // 
            // layoutViewColumn3
            // 
            this.layoutViewColumn3.AppearanceHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.layoutViewColumn3.AppearanceHeader.Options.UseFont = true;
            this.layoutViewColumn3.Caption = "layoutViewColumn3";
            this.layoutViewColumn3.ColumnEdit = this.repositoryItemMemoEdit2;
            this.layoutViewColumn3.FieldName = "IndicatorFriendlyName";
            this.layoutViewColumn3.LayoutViewField = this.layoutViewField_layoutViewColumn3;
            this.layoutViewColumn3.Name = "layoutViewColumn3";
            this.layoutViewColumn3.OptionsColumn.AllowEdit = false;
            this.layoutViewColumn3.OptionsColumn.AllowFocus = false;
            this.layoutViewColumn3.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.layoutViewColumn3.OptionsColumn.AllowIncrementalSearch = false;
            this.layoutViewColumn3.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.layoutViewColumn3.OptionsColumn.AllowMove = false;
            this.layoutViewColumn3.OptionsColumn.AllowSize = false;
            this.layoutViewColumn3.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.layoutViewColumn3.OptionsColumn.ReadOnly = true;
            this.layoutViewColumn3.OptionsColumn.ShowCaption = false;
            this.layoutViewColumn3.OptionsFilter.AllowAutoFilter = false;
            this.layoutViewColumn3.OptionsFilter.AllowFilter = false;
            this.layoutViewColumn3.OptionsFilter.ImmediateUpdateAutoFilter = false;
            // 
            // repositoryItemMemoEdit2
            // 
            this.repositoryItemMemoEdit2.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.repositoryItemMemoEdit2.LinesCount = 2;
            this.repositoryItemMemoEdit2.Name = "repositoryItemMemoEdit2";
            this.repositoryItemMemoEdit2.ReadOnly = true;
            this.repositoryItemMemoEdit2.ScrollBars = System.Windows.Forms.ScrollBars.None;
            // 
            // layoutViewField_layoutViewColumn3
            // 
            this.layoutViewField_layoutViewColumn3.EditorPreferredWidth = 98;
            this.layoutViewField_layoutViewColumn3.ImageToTextDistance = 2;
            this.layoutViewField_layoutViewColumn3.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_layoutViewColumn3.Name = "layoutViewField_layoutViewColumn3";
            this.layoutViewField_layoutViewColumn3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 2, 2, 2);
            this.layoutViewField_layoutViewColumn3.Size = new System.Drawing.Size(103, 42);
            this.layoutViewField_layoutViewColumn3.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 2, 2);
            this.layoutViewField_layoutViewColumn3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutViewField_layoutViewColumn3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewField_layoutViewColumn3.TextToControlDistance = 0;
            this.layoutViewField_layoutViewColumn3.TextVisible = false;
            this.layoutViewField_layoutViewColumn3.TrimClientAreaToControl = false;
            // 
            // layoutViewColumn4
            // 
            this.layoutViewColumn4.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutViewColumn4.AppearanceCell.Options.UseFont = true;
            this.layoutViewColumn4.AppearanceHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.layoutViewColumn4.AppearanceHeader.Options.UseFont = true;
            this.layoutViewColumn4.Caption = "layoutViewColumn4";
            this.layoutViewColumn4.ColumnEdit = this.repositoryItemImageComboBoxTwo;
            this.layoutViewColumn4.FieldName = "CustomCode";
            this.layoutViewColumn4.LayoutViewField = this.layoutViewField_layoutViewColumn4;
            this.layoutViewColumn4.Name = "layoutViewColumn4";
            this.layoutViewColumn4.OptionsColumn.AllowEdit = false;
            this.layoutViewColumn4.OptionsColumn.AllowFocus = false;
            this.layoutViewColumn4.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.layoutViewColumn4.OptionsColumn.AllowIncrementalSearch = false;
            this.layoutViewColumn4.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.layoutViewColumn4.OptionsColumn.AllowMove = false;
            this.layoutViewColumn4.OptionsColumn.AllowSize = false;
            this.layoutViewColumn4.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.layoutViewColumn4.OptionsColumn.ReadOnly = true;
            this.layoutViewColumn4.OptionsColumn.ShowCaption = false;
            this.layoutViewColumn4.OptionsFilter.AllowAutoFilter = false;
            this.layoutViewColumn4.OptionsFilter.AllowFilter = false;
            this.layoutViewColumn4.OptionsFilter.ImmediateUpdateAutoFilter = false;
            // 
            // repositoryItemImageComboBoxTwo
            // 
            this.repositoryItemImageComboBoxTwo.AutoHeight = false;
            this.repositoryItemImageComboBoxTwo.MaxLength = 25;
            this.repositoryItemImageComboBoxTwo.Name = "repositoryItemImageComboBoxTwo";
            this.repositoryItemImageComboBoxTwo.SmallImages = this.imageListIndicatorControl;
            // 
            // layoutViewField_layoutViewColumn4
            // 
            this.layoutViewField_layoutViewColumn4.EditorPreferredWidth = 68;
            this.layoutViewField_layoutViewColumn4.ImageToTextDistance = 2;
            this.layoutViewField_layoutViewColumn4.Location = new System.Drawing.Point(103, 0);
            this.layoutViewField_layoutViewColumn4.MaxSize = new System.Drawing.Size(0, 34);
            this.layoutViewField_layoutViewColumn4.MinSize = new System.Drawing.Size(77, 34);
            this.layoutViewField_layoutViewColumn4.Name = "layoutViewField_layoutViewColumn4";
            this.layoutViewField_layoutViewColumn4.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 0, 2, 2);
            this.layoutViewField_layoutViewColumn4.Size = new System.Drawing.Size(77, 42);
            this.layoutViewField_layoutViewColumn4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutViewField_layoutViewColumn4.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 2, 2);
            this.layoutViewField_layoutViewColumn4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutViewField_layoutViewColumn4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewField_layoutViewColumn4.TextToControlDistance = 0;
            this.layoutViewField_layoutViewColumn4.TextVisible = false;
            this.layoutViewField_layoutViewColumn4.TrimClientAreaToControl = false;
            // 
            // layoutViewCard2
            // 
            this.layoutViewCard2.CustomizationFormText = "layoutViewTemplateCard";
            this.layoutViewCard2.ExpandButtonLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutViewCard2.GroupBordersVisible = false;
            this.layoutViewCard2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutViewField_layoutViewColumn3,
            this.layoutViewField_layoutViewColumn4});
            this.layoutViewCard2.Name = "layoutViewCard2";
            this.layoutViewCard2.OptionsItemText.TextToControlDistance = 0;
            this.layoutViewCard2.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutViewCard2.Text = "TemplateCard";
            // 
            // groupBoxThree
            // 
            this.groupBoxThree.AppearanceCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxThree.AppearanceCaption.Options.UseFont = true;
            this.groupBoxThree.AppearanceCaption.Options.UseTextOptions = true;
            this.groupBoxThree.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.groupBoxThree.Controls.Add(this.gridControlThree);
            this.groupBoxThree.Location = new System.Drawing.Point(408, 0);
            this.groupBoxThree.LookAndFeel.SkinName = "Blue";
            this.groupBoxThree.Name = "groupBoxThree";
            this.groupBoxThree.Size = new System.Drawing.Size(204, 338);
            this.groupBoxThree.TabIndex = 2;
            this.groupBoxThree.Text = "Dispatch";
            // 
            // gridControlThree
            // 
            this.gridControlThree.Location = new System.Drawing.Point(3, 21);
            this.gridControlThree.LookAndFeel.SkinName = "Blue";
            this.gridControlThree.LookAndFeel.UseWindowsXPTheme = true;
            this.gridControlThree.MainView = this.layoutView3;
            this.gridControlThree.Name = "gridControlThree";
            this.gridControlThree.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemImageComboBoxThree,
            this.repositoryItemMemoEdit3});
            this.gridControlThree.Size = new System.Drawing.Size(198, 282);
            this.gridControlThree.TabIndex = 1;
            this.gridControlThree.ToolTipController = this.toolTipController;
            this.gridControlThree.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.layoutView3});
            // 
            // layoutView3
            // 
            this.layoutView3.Appearance.FieldCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.layoutView3.Appearance.FieldCaption.Options.UseFont = true;
            this.layoutView3.Appearance.FieldValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.layoutView3.Appearance.FieldValue.Options.UseFont = true;
            this.layoutView3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.layoutView3.CardHorzInterval = 3;
            this.layoutView3.CardMinSize = new System.Drawing.Size(100, 25);
            this.layoutView3.CardVertInterval = 7;
            this.layoutView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.LayoutViewColumn[] {
            this.layoutViewColumn5,
            this.layoutViewColumn6});
            this.layoutView3.FieldCaptionFormat = "{0}";
            this.layoutView3.GridControl = this.gridControlThree;
            this.layoutView3.Images = this.imageListIndicatorControl;
            this.layoutView3.Name = "layoutView3";
            this.layoutView3.OptionsBehavior.AllowExpandCollapse = false;
            this.layoutView3.OptionsBehavior.ScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Auto;
            this.layoutView3.OptionsCustomization.AllowFilter = false;
            this.layoutView3.OptionsCustomization.AllowSort = false;
            this.layoutView3.OptionsCustomization.ShowGroupCardCaptions = false;
            this.layoutView3.OptionsCustomization.ShowGroupCardIndents = false;
            this.layoutView3.OptionsCustomization.ShowGroupCards = false;
            this.layoutView3.OptionsCustomization.ShowGroupFields = false;
            this.layoutView3.OptionsCustomization.ShowGroupHiddenItems = false;
            this.layoutView3.OptionsCustomization.ShowGroupLayout = false;
            this.layoutView3.OptionsCustomization.ShowGroupLayoutTreeView = false;
            this.layoutView3.OptionsCustomization.ShowGroupView = false;
            this.layoutView3.OptionsCustomization.ShowResetShrinkButtons = false;
            this.layoutView3.OptionsCustomization.ShowSaveLoadLayoutButtons = false;
            this.layoutView3.OptionsCustomization.UseAdvancedRuntimeCustomization = true;
            this.layoutView3.OptionsItemText.AlignMode = DevExpress.XtraGrid.Views.Layout.FieldTextAlignMode.CustomSize;
            this.layoutView3.OptionsItemText.TextToControlDistance = 0;
            this.layoutView3.OptionsMultiRecordMode.MaxCardColumns = 3;
            this.layoutView3.OptionsMultiRecordMode.MaxCardRows = 6;
            this.layoutView3.OptionsView.AllowHotTrackFields = false;
            this.layoutView3.OptionsView.CardArrangeRule = DevExpress.XtraGrid.Views.Layout.LayoutCardArrangeRule.AllowPartialCards;
            this.layoutView3.OptionsView.ShowCardCaption = false;
            this.layoutView3.OptionsView.ShowCardExpandButton = false;
            this.layoutView3.OptionsView.ShowCardLines = false;
            this.layoutView3.OptionsView.ShowFieldHints = false;
            this.layoutView3.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.layoutView3.OptionsView.ShowHeaderPanel = false;
            this.layoutView3.OptionsView.ViewMode = DevExpress.XtraGrid.Views.Layout.LayoutViewMode.MultiRow;
            this.layoutView3.PaintStyleName = "Skin";
            this.layoutView3.TemplateCard = this.layoutViewCard3;
            // 
            // layoutViewColumn5
            // 
            this.layoutViewColumn5.AppearanceHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.layoutViewColumn5.AppearanceHeader.Options.UseFont = true;
            this.layoutViewColumn5.Caption = "layoutViewColumn5";
            this.layoutViewColumn5.ColumnEdit = this.repositoryItemMemoEdit3;
            this.layoutViewColumn5.FieldName = "IndicatorFriendlyName";
            this.layoutViewColumn5.LayoutViewField = this.layoutViewField_layoutViewColumn5;
            this.layoutViewColumn5.Name = "layoutViewColumn5";
            this.layoutViewColumn5.OptionsColumn.AllowEdit = false;
            this.layoutViewColumn5.OptionsColumn.AllowFocus = false;
            this.layoutViewColumn5.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.layoutViewColumn5.OptionsColumn.AllowIncrementalSearch = false;
            this.layoutViewColumn5.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.layoutViewColumn5.OptionsColumn.AllowMove = false;
            this.layoutViewColumn5.OptionsColumn.AllowSize = false;
            this.layoutViewColumn5.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.layoutViewColumn5.OptionsColumn.ReadOnly = true;
            this.layoutViewColumn5.OptionsColumn.ShowCaption = false;
            this.layoutViewColumn5.OptionsFilter.AllowAutoFilter = false;
            this.layoutViewColumn5.OptionsFilter.AllowFilter = false;
            this.layoutViewColumn5.OptionsFilter.ImmediateUpdateAutoFilter = false;
            // 
            // repositoryItemMemoEdit3
            // 
            this.repositoryItemMemoEdit3.LinesCount = 2;
            this.repositoryItemMemoEdit3.Name = "repositoryItemMemoEdit3";
            this.repositoryItemMemoEdit3.ReadOnly = true;
            this.repositoryItemMemoEdit3.ScrollBars = System.Windows.Forms.ScrollBars.None;
            // 
            // layoutViewField_layoutViewColumn5
            // 
            this.layoutViewField_layoutViewColumn5.EditorPreferredWidth = 98;
            this.layoutViewField_layoutViewColumn5.ImageToTextDistance = 2;
            this.layoutViewField_layoutViewColumn5.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_layoutViewColumn5.Name = "layoutViewField_layoutViewColumn5";
            this.layoutViewField_layoutViewColumn5.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 2, 2, 2);
            this.layoutViewField_layoutViewColumn5.Size = new System.Drawing.Size(103, 42);
            this.layoutViewField_layoutViewColumn5.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 2, 2);
            this.layoutViewField_layoutViewColumn5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutViewField_layoutViewColumn5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewField_layoutViewColumn5.TextToControlDistance = 0;
            this.layoutViewField_layoutViewColumn5.TextVisible = false;
            this.layoutViewField_layoutViewColumn5.TrimClientAreaToControl = false;
            // 
            // layoutViewColumn6
            // 
            this.layoutViewColumn6.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutViewColumn6.AppearanceCell.Options.UseFont = true;
            this.layoutViewColumn6.AppearanceHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.layoutViewColumn6.AppearanceHeader.Options.UseFont = true;
            this.layoutViewColumn6.Caption = "layoutViewColumn6";
            this.layoutViewColumn6.ColumnEdit = this.repositoryItemImageComboBoxThree;
            this.layoutViewColumn6.FieldName = "CustomCode";
            this.layoutViewColumn6.LayoutViewField = this.layoutViewField_layoutViewColumn6;
            this.layoutViewColumn6.Name = "layoutViewColumn6";
            this.layoutViewColumn6.OptionsColumn.AllowEdit = false;
            this.layoutViewColumn6.OptionsColumn.AllowFocus = false;
            this.layoutViewColumn6.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.layoutViewColumn6.OptionsColumn.AllowIncrementalSearch = false;
            this.layoutViewColumn6.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.layoutViewColumn6.OptionsColumn.AllowMove = false;
            this.layoutViewColumn6.OptionsColumn.AllowSize = false;
            this.layoutViewColumn6.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.layoutViewColumn6.OptionsColumn.ReadOnly = true;
            this.layoutViewColumn6.OptionsColumn.ShowCaption = false;
            this.layoutViewColumn6.OptionsFilter.AllowAutoFilter = false;
            this.layoutViewColumn6.OptionsFilter.AllowFilter = false;
            this.layoutViewColumn6.OptionsFilter.ImmediateUpdateAutoFilter = false;
            // 
            // repositoryItemImageComboBoxThree
            // 
            this.repositoryItemImageComboBoxThree.AutoHeight = false;
            this.repositoryItemImageComboBoxThree.MaxLength = 25;
            this.repositoryItemImageComboBoxThree.Name = "repositoryItemImageComboBoxThree";
            this.repositoryItemImageComboBoxThree.SmallImages = this.imageListIndicatorControl;
            // 
            // layoutViewField_layoutViewColumn6
            // 
            this.layoutViewField_layoutViewColumn6.EditorPreferredWidth = 68;
            this.layoutViewField_layoutViewColumn6.ImageToTextDistance = 2;
            this.layoutViewField_layoutViewColumn6.Location = new System.Drawing.Point(103, 0);
            this.layoutViewField_layoutViewColumn6.MaxSize = new System.Drawing.Size(0, 34);
            this.layoutViewField_layoutViewColumn6.MinSize = new System.Drawing.Size(77, 34);
            this.layoutViewField_layoutViewColumn6.Name = "layoutViewField_layoutViewColumn6";
            this.layoutViewField_layoutViewColumn6.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 0, 2, 2);
            this.layoutViewField_layoutViewColumn6.Size = new System.Drawing.Size(77, 42);
            this.layoutViewField_layoutViewColumn6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutViewField_layoutViewColumn6.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 2, 2);
            this.layoutViewField_layoutViewColumn6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutViewField_layoutViewColumn6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewField_layoutViewColumn6.TextToControlDistance = 0;
            this.layoutViewField_layoutViewColumn6.TextVisible = false;
            this.layoutViewField_layoutViewColumn6.TrimClientAreaToControl = false;
            // 
            // layoutViewCard3
            // 
            this.layoutViewCard3.CustomizationFormText = "layoutViewTemplateCard";
            this.layoutViewCard3.ExpandButtonLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutViewCard3.GroupBordersVisible = false;
            this.layoutViewCard3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutViewField_layoutViewColumn5,
            this.layoutViewField_layoutViewColumn6});
            this.layoutViewCard3.Name = "layoutViewCard3";
            this.layoutViewCard3.OptionsItemText.TextToControlDistance = 0;
            this.layoutViewCard3.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutViewCard3.Text = "TemplateCard";
            // 
            // layoutViewField1
            // 
            this.layoutViewField1.EditorPreferredWidth = 98;
            this.layoutViewField1.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField1.Name = "layoutViewField1";
            this.layoutViewField1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutViewField1.Size = new System.Drawing.Size(107, 25);
            this.layoutViewField1.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutViewField1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutViewField1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewField1.TextToControlDistance = 0;
            this.layoutViewField1.TextVisible = false;
            // 
            // layoutViewField2
            // 
            this.layoutViewField2.EditorPreferredWidth = 68;
            this.layoutViewField2.Location = new System.Drawing.Point(107, 0);
            this.layoutViewField2.Name = "layoutViewField2";
            this.layoutViewField2.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutViewField2.Size = new System.Drawing.Size(77, 25);
            this.layoutViewField2.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutViewField2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutViewField2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewField2.TextToControlDistance = 0;
            this.layoutViewField2.TextVisible = false;
            // 
            // layoutViewField3
            // 
            this.layoutViewField3.EditorPreferredWidth = 98;
            this.layoutViewField3.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField3.Name = "layoutViewField3";
            this.layoutViewField3.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutViewField3.Size = new System.Drawing.Size(107, 25);
            this.layoutViewField3.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutViewField3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutViewField3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewField3.TextToControlDistance = 0;
            this.layoutViewField3.TextVisible = false;
            // 
            // layoutViewField4
            // 
            this.layoutViewField4.EditorPreferredWidth = 68;
            this.layoutViewField4.Location = new System.Drawing.Point(107, 0);
            this.layoutViewField4.Name = "layoutViewField4";
            this.layoutViewField4.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutViewField4.Size = new System.Drawing.Size(77, 25);
            this.layoutViewField4.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutViewField4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutViewField4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewField4.TextToControlDistance = 0;
            this.layoutViewField4.TextVisible = false;
            // 
            // layoutViewField5
            // 
            this.layoutViewField5.EditorPreferredWidth = 98;
            this.layoutViewField5.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField5.Name = "layoutViewField5";
            this.layoutViewField5.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutViewField5.Size = new System.Drawing.Size(107, 25);
            this.layoutViewField5.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutViewField5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutViewField5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewField5.TextToControlDistance = 0;
            this.layoutViewField5.TextVisible = false;
            // 
            // layoutViewField6
            // 
            this.layoutViewField6.EditorPreferredWidth = 68;
            this.layoutViewField6.Location = new System.Drawing.Point(107, 0);
            this.layoutViewField6.Name = "layoutViewField6";
            this.layoutViewField6.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutViewField6.Size = new System.Drawing.Size(77, 25);
            this.layoutViewField6.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutViewField6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutViewField6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewField6.TextToControlDistance = 0;
            this.layoutViewField6.TextVisible = false;
            // 
            // layoutViewCard1
            // 
            this.layoutViewCard1.CustomizationFormText = "layoutViewTemplateCard";
            this.layoutViewCard1.ExpandButtonLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutViewCard1.GroupBordersVisible = false;
            this.layoutViewCard1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutViewField_layoutViewColumn1,
            this.layoutViewField_layoutViewColumn2});
            this.layoutViewCard1.Name = "layoutViewCard1";
            this.layoutViewCard1.OptionsItemText.TextToControlDistance = 0;
            this.layoutViewCard1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 0, 2, 2);
            this.layoutViewCard1.Text = "TemplateCard";
            // 
            // layoutViewField_layoutViewColumn1
            // 
            this.layoutViewField_layoutViewColumn1.EditorPreferredWidth = 192;
            this.layoutViewField_layoutViewColumn1.ImageToTextDistance = 2;
            this.layoutViewField_layoutViewColumn1.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_layoutViewColumn1.MaxSize = new System.Drawing.Size(200, 0);
            this.layoutViewField_layoutViewColumn1.MinSize = new System.Drawing.Size(170, 41);
            this.layoutViewField_layoutViewColumn1.Name = "layoutViewField_layoutViewColumn1";
            this.layoutViewField_layoutViewColumn1.Size = new System.Drawing.Size(200, 41);
            this.layoutViewField_layoutViewColumn1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutViewField_layoutViewColumn1.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutViewField_layoutViewColumn1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutViewField_layoutViewColumn1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewField_layoutViewColumn1.TextToControlDistance = 0;
            this.layoutViewField_layoutViewColumn1.TextVisible = false;
            // 
            // layoutViewField_layoutViewColumn2
            // 
            this.layoutViewField_layoutViewColumn2.EditorPreferredWidth = 150;
            this.layoutViewField_layoutViewColumn2.ImageToTextDistance = 2;
            this.layoutViewField_layoutViewColumn2.Location = new System.Drawing.Point(200, 0);
            this.layoutViewField_layoutViewColumn2.MaxSize = new System.Drawing.Size(75, 0);
            this.layoutViewField_layoutViewColumn2.MinSize = new System.Drawing.Size(70, 24);
            this.layoutViewField_layoutViewColumn2.Name = "layoutViewField_layoutViewColumn2";
            this.layoutViewField_layoutViewColumn2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 2, 2, 2);
            this.layoutViewField_layoutViewColumn2.Size = new System.Drawing.Size(154, 41);
            this.layoutViewField_layoutViewColumn2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutViewField_layoutViewColumn2.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutViewField_layoutViewColumn2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutViewField_layoutViewColumn2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewField_layoutViewColumn2.TextToControlDistance = 0;
            this.layoutViewField_layoutViewColumn2.TextVisible = false;
            // 
            // IndicatorDashBoardControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.groupBoxOne);
            this.Controls.Add(this.groupBoxThree);
            this.Controls.Add(this.groupBoxTwo);
            this.Name = "IndicatorDashBoardControl";
            this.Size = new System.Drawing.Size(612, 340);
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxOne)).EndInit();
            this.groupBoxOne.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlOne)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBoxOne)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxTwo)).EndInit();
            this.groupBoxTwo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTwo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBoxTwo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxThree)).EndInit();
            this.groupBoxThree.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlThree)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBoxThree)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupBoxOne;
        private System.Windows.Forms.ImageList imageListIndicatorControl;
        private DevExpress.XtraEditors.GroupControl groupBoxTwo;
        private DevExpress.XtraEditors.GroupControl groupBoxThree;
        private DevExpress.XtraGrid.GridControl gridControlOne;
        private DevExpress.XtraGrid.Views.Layout.LayoutView layoutView1;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBoxOne;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn1;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn2;
        private DevExpress.XtraGrid.GridControl gridControlTwo;
        private DevExpress.XtraGrid.Views.Layout.LayoutView layoutView2;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn3;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField1;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn4;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBoxTwo;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField2;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField3;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField4;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField5;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField6;
        private DevExpress.XtraGrid.GridControl gridControlThree;
        private DevExpress.XtraGrid.Views.Layout.LayoutView layoutView3;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn5;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn6;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBoxThree;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit3;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn3;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn4;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewCard layoutViewCard2;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn5;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn6;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewCard layoutViewCard3;
        private DevExpress.Utils.ToolTipController toolTipController;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn1;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn2;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewCard layoutViewCard1;
    }
}
