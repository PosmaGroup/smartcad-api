namespace SmartCadGuiCommon.Controls
{
    partial class IndicatorGroupDisaptchPanelControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControlindicatorDispatch = new DevExpress.XtraLayout.LayoutControl();
            this.operatorIndicatorControl21 = new OperatorIndicatorControl2();
            this.incidentIndicatorControl2 = new IncidentIndicatorControl();
            this.dispatchOrderImprtantStatusIndicatorControl1 = new DispatchOrderImprtantStatusIndicatorControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemOperatorIndicator = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemDispatchOrder = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemIncidentIndicator = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlindicatorDispatch)).BeginInit();
            this.layoutControlindicatorDispatch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOperatorIndicator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDispatchOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIncidentIndicator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControlindicatorDispatch
            // 
            this.layoutControlindicatorDispatch.Controls.Add(this.operatorIndicatorControl21);
            this.layoutControlindicatorDispatch.Controls.Add(this.incidentIndicatorControl2);
            this.layoutControlindicatorDispatch.Controls.Add(this.dispatchOrderImprtantStatusIndicatorControl1);
            this.layoutControlindicatorDispatch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlindicatorDispatch.Location = new System.Drawing.Point(0, 0);
            this.layoutControlindicatorDispatch.Name = "layoutControlindicatorDispatch";
            this.layoutControlindicatorDispatch.Root = this.layoutControlGroup1;
            this.layoutControlindicatorDispatch.Size = new System.Drawing.Size(1270, 345);
            this.layoutControlindicatorDispatch.TabIndex = 7;
            this.layoutControlindicatorDispatch.Text = "layoutControl1";
            // 
            // operatorIndicatorControl21
            // 
            this.operatorIndicatorControl21.Location = new System.Drawing.Point(2, 2);
            this.operatorIndicatorControl21.LookAndFeel.SkinName = "Blue";
            this.operatorIndicatorControl21.LookAndFeel.UseDefaultLookAndFeel = false;
            this.operatorIndicatorControl21.Name = "operatorIndicatorControl21";
            this.operatorIndicatorControl21.Size = new System.Drawing.Size(630, 169);
            this.operatorIndicatorControl21.TabIndex = 5;
            // 
            // incidentIndicatorControl2
            // 
            this.incidentIndicatorControl2.Location = new System.Drawing.Point(2, 175);
            this.incidentIndicatorControl2.LookAndFeel.SkinName = "Blue";
            this.incidentIndicatorControl2.LookAndFeel.UseDefaultLookAndFeel = false;
            this.incidentIndicatorControl2.Name = "incidentIndicatorControl2";
            this.incidentIndicatorControl2.Size = new System.Drawing.Size(630, 168);
            this.incidentIndicatorControl2.TabIndex = 1;
            // 
            // dispatchOrderImprtantStatusIndicatorControl1
            // 
            this.dispatchOrderImprtantStatusIndicatorControl1.Location = new System.Drawing.Point(636, 2);
            this.dispatchOrderImprtantStatusIndicatorControl1.LookAndFeel.SkinName = "Blue";
            this.dispatchOrderImprtantStatusIndicatorControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.dispatchOrderImprtantStatusIndicatorControl1.Name = "dispatchOrderImprtantStatusIndicatorControl1";
            this.dispatchOrderImprtantStatusIndicatorControl1.Size = new System.Drawing.Size(632, 169);
            this.dispatchOrderImprtantStatusIndicatorControl1.TabIndex = 6;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemOperatorIndicator,
            this.layoutControlItemDispatchOrder,
            this.layoutControlItemIncidentIndicator,
            this.emptySpaceItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1270, 345);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItemOperatorIndicator
            // 
            this.layoutControlItemOperatorIndicator.Control = this.operatorIndicatorControl21;
            this.layoutControlItemOperatorIndicator.CustomizationFormText = "layoutControlItemOperatorIndicator";
            this.layoutControlItemOperatorIndicator.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemOperatorIndicator.Name = "layoutControlItemOperatorIndicator";
            this.layoutControlItemOperatorIndicator.Size = new System.Drawing.Size(634, 173);
            this.layoutControlItemOperatorIndicator.Text = "layoutControlItemOperatorIndicator";
            this.layoutControlItemOperatorIndicator.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemOperatorIndicator.TextToControlDistance = 0;
            this.layoutControlItemOperatorIndicator.TextVisible = false;
            // 
            // layoutControlItemDispatchOrder
            // 
            this.layoutControlItemDispatchOrder.Control = this.dispatchOrderImprtantStatusIndicatorControl1;
            this.layoutControlItemDispatchOrder.CustomizationFormText = "layoutControlItemDispatchOrder";
            this.layoutControlItemDispatchOrder.Location = new System.Drawing.Point(634, 0);
            this.layoutControlItemDispatchOrder.Name = "layoutControlItemDispatchOrder";
            this.layoutControlItemDispatchOrder.Size = new System.Drawing.Size(636, 173);
            this.layoutControlItemDispatchOrder.Text = "layoutControlItemDispatchOrder";
            this.layoutControlItemDispatchOrder.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemDispatchOrder.TextToControlDistance = 0;
            this.layoutControlItemDispatchOrder.TextVisible = false;
            // 
            // layoutControlItemIncidentIndicator
            // 
            this.layoutControlItemIncidentIndicator.Control = this.incidentIndicatorControl2;
            this.layoutControlItemIncidentIndicator.CustomizationFormText = "layoutControlItemIncidentIndicator";
            this.layoutControlItemIncidentIndicator.Location = new System.Drawing.Point(0, 173);
            this.layoutControlItemIncidentIndicator.Name = "layoutControlItemIncidentIndicator";
            this.layoutControlItemIncidentIndicator.Size = new System.Drawing.Size(634, 172);
            this.layoutControlItemIncidentIndicator.Text = "layoutControlItemIncidentIndicator";
            this.layoutControlItemIncidentIndicator.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemIncidentIndicator.TextToControlDistance = 0;
            this.layoutControlItemIncidentIndicator.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(634, 173);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(636, 172);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // IndicatorGroupDisaptchPanelControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControlindicatorDispatch);
            this.LookAndFeel.SkinName = "Blue";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "IndicatorGroupDisaptchPanelControl";
            this.Size = new System.Drawing.Size(1270, 345);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlindicatorDispatch)).EndInit();
            this.layoutControlindicatorDispatch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOperatorIndicator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDispatchOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIncidentIndicator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal IncidentIndicatorControl incidentIndicatorControl2;
        internal OperatorIndicatorControl2 operatorIndicatorControl21;
        internal DispatchOrderImprtantStatusIndicatorControl dispatchOrderImprtantStatusIndicatorControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControlindicatorDispatch;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemOperatorIndicator;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDispatchOrder;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemIncidentIndicator;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;

    }
}
