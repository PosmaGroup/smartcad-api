using System.Windows.Forms;
namespace SmartCadGuiCommon.Controls
{
    partial class DateIntervalsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.radioButtonDaily = new System.Windows.Forms.RadioButton();
            this.dateNavigator1 = new DevExpress.XtraScheduler.DateNavigator();
            this.radioButtonMonthly = new System.Windows.Forms.RadioButton();
            this.radioButtonScheduler = new System.Windows.Forms.RadioButton();
            this.styleController1 = new DevExpress.XtraEditors.StyleController(this.components);
            this.comboBoxEditEnum = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutControlDateIntervals = new DevExpress.XtraLayout.LayoutControl();
            this.timeEditBegin = new DevExpress.XtraEditors.TimeEdit();
            this.timeEditEnd = new DevExpress.XtraEditors.TimeEdit();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radioButtonDayMonthly = new System.Windows.Forms.RadioButton();
            this.radioButtonTheMonthly = new System.Windows.Forms.RadioButton();
            this.panelOptions = new System.Windows.Forms.Panel();
            this.radioButtonRotative = new System.Windows.Forms.RadioButton();
            this.radioButtonWeekly = new System.Windows.Forms.RadioButton();
            this.labelEvery = new System.Windows.Forms.Label();
            this.radioButtonWeekday = new System.Windows.Forms.RadioButton();
            this.spinEditDayDaily = new DevExpress.XtraEditors.SpinEdit();
            this.dateEditTo = new DevExpress.XtraEditors.DateEdit();
            this.dateEditFrom = new DevExpress.XtraEditors.DateEdit();
            this.labelFree = new System.Windows.Forms.Label();
            this.spinEditHoursFree = new DevExpress.XtraEditors.SpinEdit();
            this.radioButtonEveryDaily = new System.Windows.Forms.RadioButton();
            this.labelWorkingDay = new System.Windows.Forms.Label();
            this.spinEditHoursToWork = new DevExpress.XtraEditors.SpinEdit();
            this.spinEditWeekWeekly = new DevExpress.XtraEditors.SpinEdit();
            this.checkSund = new DevExpress.XtraEditors.CheckEdit();
            this.checkMon = new DevExpress.XtraEditors.CheckEdit();
            this.checkTues = new DevExpress.XtraEditors.CheckEdit();
            this.checkSat = new DevExpress.XtraEditors.CheckEdit();
            this.checkWed = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditThur = new DevExpress.XtraEditors.CheckEdit();
            this.spinEditMonth1 = new DevExpress.XtraEditors.SpinEdit();
            this.checkFrid = new DevExpress.XtraEditors.CheckEdit();
            this.comboBoxEditDays = new DevExpress.XtraEditors.ComboBoxEdit();
            this.spinEditDay = new DevExpress.XtraEditors.SpinEdit();
            this.spinEditMonth = new DevExpress.XtraEditors.SpinEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItemFrom = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTo = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupDaily = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemDaysDaily = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemEveryDaily = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupWeekly = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemWeeksWeekly = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupMonthly = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemEveryMonthly = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemEveryMonthly1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemMonthMonthly1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemMonthMonthly = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator1 = new DevExpress.XtraLayout.SimpleSeparator();
            this.emptySpaceItemHeader = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupScheduler = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemScheduler = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupRotative = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItemHoursFree = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemHoursWork = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemEnd = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemBegin = new DevExpress.XtraLayout.LayoutControlItem();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dateNavigator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.styleController1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditEnum.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlDateIntervals)).BeginInit();
            this.layoutControlDateIntervals.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.timeEditBegin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeEditEnd.Properties)).BeginInit();
            this.panel1.SuspendLayout();
            this.panelOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditDayDaily.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditHoursFree.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditHoursToWork.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditWeekWeekly.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkSund.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkMon.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkTues.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkSat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkWed.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditThur.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditMonth1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkFrid.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditDays.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditDay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditMonth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupDaily)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDaysDaily)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemEveryDaily)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupWeekly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWeeksWeekly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupMonthly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemEveryMonthly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemEveryMonthly1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemMonthMonthly1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemMonthMonthly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItemHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupScheduler)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemScheduler)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupRotative)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHoursFree)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHoursWork)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBegin)).BeginInit();
            this.SuspendLayout();
            // 
            // radioButtonDaily
            // 
            this.radioButtonDaily.Location = new System.Drawing.Point(3, 34);
            this.radioButtonDaily.Name = "radioButtonDaily";
            this.radioButtonDaily.Size = new System.Drawing.Size(96, 25);
            this.radioButtonDaily.TabIndex = 6;
            this.radioButtonDaily.TabStop = true;
            this.radioButtonDaily.Text = "radioButtonDaily";
            this.radioButtonDaily.UseVisualStyleBackColor = true;
            this.radioButtonDaily.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // dateNavigator1
            // 
            this.dateNavigator1.DateTime = new System.DateTime(2008, 9, 25, 0, 0, 0, 0);
            this.dateNavigator1.HotDate = null;
            this.dateNavigator1.Location = new System.Drawing.Point(112, 69);
            this.dateNavigator1.LookAndFeel.SkinName = "Black";
            this.dateNavigator1.MinimumSize = new System.Drawing.Size(364, 167);
            this.dateNavigator1.Name = "dateNavigator1";
            this.dateNavigator1.Size = new System.Drawing.Size(367, 178);
            this.dateNavigator1.TabIndex = 5;
            this.dateNavigator1.EditDateModified += new System.EventHandler(this.dateNavigator1_EditDateModified);
            this.dateNavigator1.CustomDrawDayNumberCell += new DevExpress.XtraEditors.Calendar.CustomDrawDayNumberCellEventHandler(this.dateNavigator1_CustomDrawDayNumberCell);
            this.dateNavigator1.MouseHover += new System.EventHandler(this.dateNavigator1_MouseHover);
            // 
            // radioButtonMonthly
            // 
            this.radioButtonMonthly.Location = new System.Drawing.Point(3, 96);
            this.radioButtonMonthly.Name = "radioButtonMonthly";
            this.radioButtonMonthly.Size = new System.Drawing.Size(96, 25);
            this.radioButtonMonthly.TabIndex = 11;
            this.radioButtonMonthly.TabStop = true;
            this.radioButtonMonthly.Text = "radioButtonMonthly";
            this.radioButtonMonthly.UseVisualStyleBackColor = true;
            this.radioButtonMonthly.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // radioButtonScheduler
            // 
            this.radioButtonScheduler.Location = new System.Drawing.Point(3, 3);
            this.radioButtonScheduler.Name = "radioButtonScheduler";
            this.radioButtonScheduler.Size = new System.Drawing.Size(96, 25);
            this.radioButtonScheduler.TabIndex = 4;
            this.radioButtonScheduler.TabStop = true;
            this.radioButtonScheduler.Text = "radioButtonScheduler";
            this.radioButtonScheduler.UseVisualStyleBackColor = true;
            this.radioButtonScheduler.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // styleController1
            // 
            this.styleController1.LookAndFeel.SkinName = "Lilian";
            this.styleController1.LookAndFeel.UseDefaultLookAndFeel = false;
            // 
            // comboBoxEditEnum
            // 
            this.comboBoxEditEnum.Location = new System.Drawing.Point(166, 436);
            this.comboBoxEditEnum.MinimumSize = new System.Drawing.Size(70, 20);
            this.comboBoxEditEnum.Name = "comboBoxEditEnum";
            this.comboBoxEditEnum.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditEnum.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEditEnum.Size = new System.Drawing.Size(71, 20);
            this.comboBoxEditEnum.StyleController = this.layoutControlDateIntervals;
            this.comboBoxEditEnum.TabIndex = 12;
            this.comboBoxEditEnum.EditValueChanged += new System.EventHandler(this.RaiseParameters_Change);
            this.comboBoxEditEnum.Enter += new System.EventHandler(this.comboBoxEditEnum_Enter);
            // 
            // layoutControlDateIntervals
            // 
            this.layoutControlDateIntervals.Controls.Add(this.timeEditBegin);
            this.layoutControlDateIntervals.Controls.Add(this.timeEditEnd);
            this.layoutControlDateIntervals.Controls.Add(this.panel1);
            this.layoutControlDateIntervals.Controls.Add(this.panelOptions);
            this.layoutControlDateIntervals.Controls.Add(this.labelEvery);
            this.layoutControlDateIntervals.Controls.Add(this.radioButtonWeekday);
            this.layoutControlDateIntervals.Controls.Add(this.spinEditDayDaily);
            this.layoutControlDateIntervals.Controls.Add(this.dateEditTo);
            this.layoutControlDateIntervals.Controls.Add(this.dateEditFrom);
            this.layoutControlDateIntervals.Controls.Add(this.labelFree);
            this.layoutControlDateIntervals.Controls.Add(this.spinEditHoursFree);
            this.layoutControlDateIntervals.Controls.Add(this.dateNavigator1);
            this.layoutControlDateIntervals.Controls.Add(this.radioButtonEveryDaily);
            this.layoutControlDateIntervals.Controls.Add(this.labelWorkingDay);
            this.layoutControlDateIntervals.Controls.Add(this.spinEditHoursToWork);
            this.layoutControlDateIntervals.Controls.Add(this.spinEditWeekWeekly);
            this.layoutControlDateIntervals.Controls.Add(this.checkSund);
            this.layoutControlDateIntervals.Controls.Add(this.checkMon);
            this.layoutControlDateIntervals.Controls.Add(this.checkTues);
            this.layoutControlDateIntervals.Controls.Add(this.checkSat);
            this.layoutControlDateIntervals.Controls.Add(this.checkWed);
            this.layoutControlDateIntervals.Controls.Add(this.checkEditThur);
            this.layoutControlDateIntervals.Controls.Add(this.spinEditMonth1);
            this.layoutControlDateIntervals.Controls.Add(this.checkFrid);
            this.layoutControlDateIntervals.Controls.Add(this.comboBoxEditDays);
            this.layoutControlDateIntervals.Controls.Add(this.spinEditDay);
            this.layoutControlDateIntervals.Controls.Add(this.spinEditMonth);
            this.layoutControlDateIntervals.Controls.Add(this.comboBoxEditEnum);
            this.layoutControlDateIntervals.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlDateIntervals.Location = new System.Drawing.Point(0, 0);
            this.layoutControlDateIntervals.Name = "layoutControlDateIntervals";
            this.layoutControlDateIntervals.Root = this.layoutControlGroup2;
            this.layoutControlDateIntervals.Size = new System.Drawing.Size(484, 560);
            this.layoutControlDateIntervals.TabIndex = 26;
            this.layoutControlDateIntervals.Text = "layoutControl1";
            // 
            // timeEditBegin
            // 
            this.timeEditBegin.EditValue = new System.DateTime(2009, 7, 31, 0, 0, 0, 0);
            this.timeEditBegin.Location = new System.Drawing.Point(122, 5);
            this.timeEditBegin.Name = "timeEditBegin";
            this.timeEditBegin.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.timeEditBegin.Properties.DisplayFormat.FormatString = "HH:mm";
            this.timeEditBegin.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.timeEditBegin.Properties.EditFormat.FormatString = "HH:mm";
            this.timeEditBegin.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.timeEditBegin.Properties.Mask.EditMask = "HH:mm";
            this.timeEditBegin.Size = new System.Drawing.Size(56, 20);
            this.timeEditBegin.StyleController = this.layoutControlDateIntervals;
            this.timeEditBegin.TabIndex = 37;
            this.timeEditBegin.EditValueChanged += new System.EventHandler(this.timeEditBegin_EditValueChanged);
            this.timeEditBegin.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.timeEdit_EditValueChanging);
            // 
            // timeEditEnd
            // 
            this.timeEditEnd.EditValue = new System.DateTime(2009, 8, 1, 0, 0, 0, 0);
            this.timeEditEnd.Location = new System.Drawing.Point(299, 5);
            this.timeEditEnd.Name = "timeEditEnd";
            this.timeEditEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.timeEditEnd.Properties.DisplayFormat.FormatString = "HH:mm";
            this.timeEditEnd.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.timeEditEnd.Properties.EditFormat.FormatString = "HH:mm";
            this.timeEditEnd.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.timeEditEnd.Properties.Mask.EditMask = "HH:mm";
            this.timeEditEnd.Size = new System.Drawing.Size(57, 20);
            this.timeEditEnd.StyleController = this.layoutControlDateIntervals;
            this.timeEditEnd.TabIndex = 36;
            this.timeEditEnd.EditValueChanged += new System.EventHandler(this.timeEditBegin_EditValueChanged);
            this.timeEditEnd.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.timeEdit_EditValueChanging);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radioButtonDayMonthly);
            this.panel1.Controls.Add(this.radioButtonTheMonthly);
            this.panel1.Location = new System.Drawing.Point(112, 403);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(52, 64);
            this.panel1.TabIndex = 32;
            // 
            // radioButtonDayMonthly
            // 
            this.radioButtonDayMonthly.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.radioButtonDayMonthly.Location = new System.Drawing.Point(3, 2);
            this.radioButtonDayMonthly.Name = "radioButtonDayMonthly";
            this.radioButtonDayMonthly.Size = new System.Drawing.Size(60, 25);
            this.radioButtonDayMonthly.TabIndex = 30;
            this.radioButtonDayMonthly.TabStop = true;
            this.radioButtonDayMonthly.Text = "Dia";
            this.radioButtonDayMonthly.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.radioButtonDayMonthly.UseVisualStyleBackColor = true;
            this.radioButtonDayMonthly.CheckedChanged += new System.EventHandler(this.RaiseParameters_Change);
            // 
            // radioButtonTheMonthly
            // 
            this.radioButtonTheMonthly.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.radioButtonTheMonthly.Location = new System.Drawing.Point(3, 35);
            this.radioButtonTheMonthly.Name = "radioButtonTheMonthly";
            this.radioButtonTheMonthly.Size = new System.Drawing.Size(60, 25);
            this.radioButtonTheMonthly.TabIndex = 31;
            this.radioButtonTheMonthly.TabStop = true;
            this.radioButtonTheMonthly.Text = "El";
            this.radioButtonTheMonthly.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.radioButtonTheMonthly.UseVisualStyleBackColor = true;
            this.radioButtonTheMonthly.CheckedChanged += new System.EventHandler(this.RaiseParameters_Change);
            // 
            // panelOptions
            // 
            this.panelOptions.Controls.Add(this.radioButtonScheduler);
            this.panelOptions.Controls.Add(this.radioButtonDaily);
            this.panelOptions.Controls.Add(this.radioButtonRotative);
            this.panelOptions.Controls.Add(this.radioButtonMonthly);
            this.panelOptions.Controls.Add(this.radioButtonWeekly);
            this.panelOptions.Location = new System.Drawing.Point(5, 37);
            this.panelOptions.Name = "panelOptions";
            this.panelOptions.Size = new System.Drawing.Size(101, 168);
            this.panelOptions.TabIndex = 28;
            // 
            // radioButtonRotative
            // 
            this.radioButtonRotative.Location = new System.Drawing.Point(3, 127);
            this.radioButtonRotative.Name = "radioButtonRotative";
            this.radioButtonRotative.Size = new System.Drawing.Size(96, 25);
            this.radioButtonRotative.TabIndex = 27;
            this.radioButtonRotative.TabStop = true;
            this.radioButtonRotative.Text = "radioButtonRotative";
            this.radioButtonRotative.UseVisualStyleBackColor = true;
            this.radioButtonRotative.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // radioButtonWeekly
            // 
            this.radioButtonWeekly.Location = new System.Drawing.Point(3, 65);
            this.radioButtonWeekly.Name = "radioButtonWeekly";
            this.radioButtonWeekly.Size = new System.Drawing.Size(96, 25);
            this.radioButtonWeekly.TabIndex = 26;
            this.radioButtonWeekly.TabStop = true;
            this.radioButtonWeekly.Text = "radioButtonWeekly";
            this.radioButtonWeekly.UseVisualStyleBackColor = true;
            this.radioButtonWeekly.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // labelEvery
            // 
            this.labelEvery.Location = new System.Drawing.Point(112, 312);
            this.labelEvery.Name = "labelEvery";
            this.labelEvery.Size = new System.Drawing.Size(46, 27);
            this.labelEvery.TabIndex = 27;
            this.labelEvery.Text = "Cada";
            this.labelEvery.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // radioButtonWeekday
            // 
            this.radioButtonWeekday.Location = new System.Drawing.Point(112, 282);
            this.radioButtonWeekday.Name = "radioButtonWeekday";
            this.radioButtonWeekday.Size = new System.Drawing.Size(367, 26);
            this.radioButtonWeekday.TabIndex = 29;
            this.radioButtonWeekday.TabStop = true;
            this.radioButtonWeekday.Text = "Cada dia de la semana";
            this.radioButtonWeekday.UseVisualStyleBackColor = true;
            this.radioButtonWeekday.CheckedChanged += new System.EventHandler(this.RaiseParameters_Change);
            // 
            // spinEditDayDaily
            // 
            this.spinEditDayDaily.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditDayDaily.Location = new System.Drawing.Point(174, 254);
            this.spinEditDayDaily.MinimumSize = new System.Drawing.Size(50, 20);
            this.spinEditDayDaily.Name = "spinEditDayDaily";
            this.spinEditDayDaily.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditDayDaily.Properties.Mask.EditMask = "##";
            this.spinEditDayDaily.Properties.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.spinEditDayDaily.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditDayDaily.Size = new System.Drawing.Size(52, 20);
            this.spinEditDayDaily.StyleController = this.layoutControlDateIntervals;
            this.spinEditDayDaily.TabIndex = 27;
            this.spinEditDayDaily.Enter += new System.EventHandler(this.spinEditDayDaily_Enter);
            // 
            // dateEditTo
            // 
            this.dateEditTo.EditValue = null;
            this.dateEditTo.Location = new System.Drawing.Point(295, 532);
            this.dateEditTo.Name = "dateEditTo";
            this.dateEditTo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dateEditTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditTo.Properties.ShowToday = false;
            this.dateEditTo.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditTo.Size = new System.Drawing.Size(51, 20);
            this.dateEditTo.StyleController = this.layoutControlDateIntervals;
            this.dateEditTo.TabIndex = 28;
            // 
            // dateEditFrom
            // 
            this.dateEditFrom.EditValue = null;
            this.dateEditFrom.Location = new System.Drawing.Point(121, 529);
            this.dateEditFrom.Name = "dateEditFrom";
            this.dateEditFrom.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dateEditFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFrom.Properties.ShowToday = false;
            this.dateEditFrom.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFrom.Size = new System.Drawing.Size(60, 20);
            this.dateEditFrom.StyleController = this.layoutControlDateIntervals;
            this.dateEditFrom.TabIndex = 26;
            this.dateEditFrom.EditValueChanged += new System.EventHandler(this.dateEditFrom_EditValueChanged);
            // 
            // labelFree
            // 
            this.labelFree.Location = new System.Drawing.Point(269, 471);
            this.labelFree.Name = "labelFree";
            this.labelFree.Size = new System.Drawing.Size(70, 27);
            this.labelFree.TabIndex = 27;
            this.labelFree.Text = "Descanso:";
            this.labelFree.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // spinEditHoursFree
            // 
            this.spinEditHoursFree.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditHoursFree.Location = new System.Drawing.Point(343, 474);
            this.spinEditHoursFree.MinimumSize = new System.Drawing.Size(50, 20);
            this.spinEditHoursFree.Name = "spinEditHoursFree";
            this.spinEditHoursFree.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditHoursFree.Properties.Mask.EditMask = "##";
            this.spinEditHoursFree.Properties.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.spinEditHoursFree.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditHoursFree.Size = new System.Drawing.Size(52, 20);
            this.spinEditHoursFree.StyleController = this.layoutControlDateIntervals;
            this.spinEditHoursFree.TabIndex = 35;
            // 
            // radioButtonEveryDaily
            // 
            this.radioButtonEveryDaily.Location = new System.Drawing.Point(112, 251);
            this.radioButtonEveryDaily.Name = "radioButtonEveryDaily";
            this.radioButtonEveryDaily.Size = new System.Drawing.Size(58, 27);
            this.radioButtonEveryDaily.TabIndex = 28;
            this.radioButtonEveryDaily.TabStop = true;
            this.radioButtonEveryDaily.Text = "Cada";
            this.radioButtonEveryDaily.UseVisualStyleBackColor = true;
            this.radioButtonEveryDaily.CheckedChanged += new System.EventHandler(this.RaiseParameters_Change);
            // 
            // labelWorkingDay
            // 
            this.labelWorkingDay.Location = new System.Drawing.Point(112, 471);
            this.labelWorkingDay.Name = "labelWorkingDay";
            this.labelWorkingDay.Size = new System.Drawing.Size(63, 27);
            this.labelWorkingDay.TabIndex = 26;
            this.labelWorkingDay.Text = "Jornada:";
            this.labelWorkingDay.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // spinEditHoursToWork
            // 
            this.spinEditHoursToWork.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditHoursToWork.Location = new System.Drawing.Point(179, 474);
            this.spinEditHoursToWork.MinimumSize = new System.Drawing.Size(50, 20);
            this.spinEditHoursToWork.Name = "spinEditHoursToWork";
            this.spinEditHoursToWork.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditHoursToWork.Properties.Mask.EditMask = "##";
            this.spinEditHoursToWork.Properties.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.spinEditHoursToWork.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditHoursToWork.Size = new System.Drawing.Size(51, 20);
            this.spinEditHoursToWork.StyleController = this.layoutControlDateIntervals;
            this.spinEditHoursToWork.TabIndex = 34;
            // 
            // spinEditWeekWeekly
            // 
            this.spinEditWeekWeekly.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditWeekWeekly.Location = new System.Drawing.Point(162, 312);
            this.spinEditWeekWeekly.MinimumSize = new System.Drawing.Size(50, 20);
            this.spinEditWeekWeekly.Name = "spinEditWeekWeekly";
            this.spinEditWeekWeekly.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditWeekWeekly.Properties.Mask.EditMask = "##";
            this.spinEditWeekWeekly.Properties.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.spinEditWeekWeekly.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditWeekWeekly.Size = new System.Drawing.Size(56, 20);
            this.spinEditWeekWeekly.StyleController = this.layoutControlDateIntervals;
            this.spinEditWeekWeekly.TabIndex = 33;
            // 
            // checkSund
            // 
            this.checkSund.Location = new System.Drawing.Point(292, 373);
            this.checkSund.Name = "checkSund";
            this.checkSund.Properties.Caption = "checkSund";
            this.checkSund.Size = new System.Drawing.Size(187, 19);
            this.checkSund.StyleController = this.layoutControlDateIntervals;
            this.checkSund.TabIndex = 32;
            // 
            // checkMon
            // 
            this.checkMon.Location = new System.Drawing.Point(112, 343);
            this.checkMon.Name = "checkMon";
            this.checkMon.Properties.Caption = "checkMon";
            this.checkMon.Size = new System.Drawing.Size(86, 19);
            this.checkMon.StyleController = this.layoutControlDateIntervals;
            this.checkMon.TabIndex = 26;
            // 
            // checkTues
            // 
            this.checkTues.Location = new System.Drawing.Point(202, 343);
            this.checkTues.Name = "checkTues";
            this.checkTues.Properties.Caption = "checkTues";
            this.checkTues.Size = new System.Drawing.Size(86, 19);
            this.checkTues.StyleController = this.layoutControlDateIntervals;
            this.checkTues.TabIndex = 27;
            // 
            // checkSat
            // 
            this.checkSat.Location = new System.Drawing.Point(202, 373);
            this.checkSat.Name = "checkSat";
            this.checkSat.Properties.Caption = "checkSat";
            this.checkSat.Size = new System.Drawing.Size(86, 19);
            this.checkSat.StyleController = this.layoutControlDateIntervals;
            this.checkSat.TabIndex = 31;
            // 
            // checkWed
            // 
            this.checkWed.Location = new System.Drawing.Point(292, 343);
            this.checkWed.Name = "checkWed";
            this.checkWed.Properties.Caption = "checkWed";
            this.checkWed.Size = new System.Drawing.Size(86, 19);
            this.checkWed.StyleController = this.layoutControlDateIntervals;
            this.checkWed.TabIndex = 28;
            // 
            // checkEditThur
            // 
            this.checkEditThur.Location = new System.Drawing.Point(382, 343);
            this.checkEditThur.Name = "checkEditThur";
            this.checkEditThur.Properties.Caption = "checkThur";
            this.checkEditThur.Size = new System.Drawing.Size(97, 19);
            this.checkEditThur.StyleController = this.layoutControlDateIntervals;
            this.checkEditThur.TabIndex = 29;
            // 
            // spinEditMonth1
            // 
            this.spinEditMonth1.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditMonth1.Location = new System.Drawing.Point(378, 433);
            this.spinEditMonth1.MinimumSize = new System.Drawing.Size(50, 20);
            this.spinEditMonth1.Name = "spinEditMonth1";
            this.spinEditMonth1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditMonth1.Properties.Mask.EditMask = "##";
            this.spinEditMonth1.Properties.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.spinEditMonth1.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditMonth1.Size = new System.Drawing.Size(58, 20);
            this.spinEditMonth1.StyleController = this.layoutControlDateIntervals;
            this.spinEditMonth1.TabIndex = 15;
            this.spinEditMonth1.EditValueChanged += new System.EventHandler(this.RaiseParameters_Change);
            this.spinEditMonth1.Enter += new System.EventHandler(this.comboBoxEditEnum_Enter);
            // 
            // checkFrid
            // 
            this.checkFrid.Location = new System.Drawing.Point(112, 373);
            this.checkFrid.Name = "checkFrid";
            this.checkFrid.Properties.Caption = "checkFrid";
            this.checkFrid.Size = new System.Drawing.Size(86, 19);
            this.checkFrid.StyleController = this.layoutControlDateIntervals;
            this.checkFrid.TabIndex = 30;
            // 
            // comboBoxEditDays
            // 
            this.comboBoxEditDays.Location = new System.Drawing.Point(244, 433);
            this.comboBoxEditDays.MinimumSize = new System.Drawing.Size(80, 20);
            this.comboBoxEditDays.Name = "comboBoxEditDays";
            this.comboBoxEditDays.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditDays.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEditDays.Size = new System.Drawing.Size(87, 20);
            this.comboBoxEditDays.StyleController = this.layoutControlDateIntervals;
            this.comboBoxEditDays.TabIndex = 13;
            this.comboBoxEditDays.EditValueChanged += new System.EventHandler(this.RaiseParameters_Change);
            this.comboBoxEditDays.Enter += new System.EventHandler(this.comboBoxEditEnum_Enter);
            // 
            // spinEditDay
            // 
            this.spinEditDay.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditDay.Location = new System.Drawing.Point(166, 406);
            this.spinEditDay.MinimumSize = new System.Drawing.Size(50, 20);
            this.spinEditDay.Name = "spinEditDay";
            this.spinEditDay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditDay.Properties.Mask.EditMask = "##";
            this.spinEditDay.Properties.MaxValue = new decimal(new int[] {
            31,
            0,
            0,
            0});
            this.spinEditDay.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditDay.Size = new System.Drawing.Size(52, 20);
            this.spinEditDay.StyleController = this.layoutControlDateIntervals;
            this.spinEditDay.TabIndex = 7;
            this.spinEditDay.EditValueChanged += new System.EventHandler(this.RaiseParameters_Change);
            this.spinEditDay.Enter += new System.EventHandler(this.spinEditDay_Enter);
            // 
            // spinEditMonth
            // 
            this.spinEditMonth.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditMonth.Location = new System.Drawing.Point(268, 403);
            this.spinEditMonth.MinimumSize = new System.Drawing.Size(50, 20);
            this.spinEditMonth.Name = "spinEditMonth";
            this.spinEditMonth.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditMonth.Properties.Mask.EditMask = "##";
            this.spinEditMonth.Properties.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.spinEditMonth.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditMonth.Size = new System.Drawing.Size(57, 20);
            this.spinEditMonth.StyleController = this.layoutControlDateIntervals;
            this.spinEditMonth.TabIndex = 9;
            this.spinEditMonth.EditValueChanged += new System.EventHandler(this.RaiseParameters_Change);
            this.spinEditMonth.Enter += new System.EventHandler(this.spinEditDay_Enter);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "Root";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem4,
            this.layoutControlItemFrom,
            this.layoutControlItemTo,
            this.layoutControlGroupDaily,
            this.layoutControlGroupWeekly,
            this.layoutControlGroupMonthly,
            this.emptySpaceItem1,
            this.emptySpaceItem6,
            this.layoutControlItem1,
            this.simpleSeparator1,
            this.emptySpaceItemHeader,
            this.layoutControlGroupScheduler,
            this.emptySpaceItem8,
            this.layoutControlGroupRotative,
            this.layoutControlItemEnd,
            this.layoutControlItemBegin});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup2.Size = new System.Drawing.Size(484, 560);
            this.layoutControlGroup2.Text = "Root";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(355, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(123, 24);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItemFrom
            // 
            this.layoutControlItemFrom.Control = this.dateEditFrom;
            this.layoutControlItemFrom.CustomizationFormText = "layoutControlItemFrom1";
            this.layoutControlItemFrom.Location = new System.Drawing.Point(0, 524);
            this.layoutControlItemFrom.Name = "layoutControlItemFrom";
            this.layoutControlItemFrom.Size = new System.Drawing.Size(180, 30);
            this.layoutControlItemFrom.Text = "layoutControlItemFrom";
            this.layoutControlItemFrom.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItemFrom.TextSize = new System.Drawing.Size(111, 13);
            this.layoutControlItemFrom.TextToControlDistance = 5;
            // 
            // layoutControlItemTo
            // 
            this.layoutControlItemTo.Control = this.dateEditTo;
            this.layoutControlItemTo.CustomizationFormText = "layoutControlItemTo1";
            this.layoutControlItemTo.Location = new System.Drawing.Point(180, 524);
            this.layoutControlItemTo.Name = "layoutControlItemTo";
            this.layoutControlItemTo.Padding = new DevExpress.XtraLayout.Utils.Padding(8, 5, 5, 5);
            this.layoutControlItemTo.Size = new System.Drawing.Size(168, 30);
            this.layoutControlItemTo.Text = "layoutControlItemTo";
            this.layoutControlItemTo.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItemTo.TextSize = new System.Drawing.Size(99, 13);
            this.layoutControlItemTo.TextToControlDistance = 5;
            // 
            // layoutControlGroupDaily
            // 
            this.layoutControlGroupDaily.CustomizationFormText = "layoutControlGroupDaily";
            this.layoutControlGroupDaily.GroupBordersVisible = false;
            this.layoutControlGroupDaily.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemDaysDaily,
            this.layoutControlItemEveryDaily,
            this.layoutControlItem35,
            this.emptySpaceItem2});
            this.layoutControlGroupDaily.Location = new System.Drawing.Point(107, 246);
            this.layoutControlGroupDaily.Name = "layoutControlGroupDaily";
            this.layoutControlGroupDaily.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupDaily.Size = new System.Drawing.Size(371, 61);
            this.layoutControlGroupDaily.Text = "layoutControlGroupDaily";
            // 
            // layoutControlItemDaysDaily
            // 
            this.layoutControlItemDaysDaily.Control = this.spinEditDayDaily;
            this.layoutControlItemDaysDaily.CustomizationFormText = "layoutControlItemDaysDaily";
            this.layoutControlItemDaysDaily.Location = new System.Drawing.Point(62, 0);
            this.layoutControlItemDaysDaily.MaxSize = new System.Drawing.Size(0, 31);
            this.layoutControlItemDaysDaily.MinSize = new System.Drawing.Size(90, 31);
            this.layoutControlItemDaysDaily.Name = "layoutControlItemDaysDaily";
            this.layoutControlItemDaysDaily.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 5, 5, 5);
            this.layoutControlItemDaysDaily.Size = new System.Drawing.Size(91, 31);
            this.layoutControlItemDaysDaily.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemDaysDaily.Text = "dia(s)";
            this.layoutControlItemDaysDaily.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItemDaysDaily.TextLocation = DevExpress.Utils.Locations.Right;
            this.layoutControlItemDaysDaily.TextSize = new System.Drawing.Size(27, 13);
            this.layoutControlItemDaysDaily.TextToControlDistance = 5;
            // 
            // layoutControlItemEveryDaily
            // 
            this.layoutControlItemEveryDaily.Control = this.radioButtonEveryDaily;
            this.layoutControlItemEveryDaily.CustomizationFormText = "layoutControlItemEveryDaily";
            this.layoutControlItemEveryDaily.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemEveryDaily.MaxSize = new System.Drawing.Size(62, 31);
            this.layoutControlItemEveryDaily.MinSize = new System.Drawing.Size(62, 31);
            this.layoutControlItemEveryDaily.Name = "layoutControlItemEveryDaily";
            this.layoutControlItemEveryDaily.Size = new System.Drawing.Size(62, 31);
            this.layoutControlItemEveryDaily.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemEveryDaily.Text = "layoutControlItemEveryDaily";
            this.layoutControlItemEveryDaily.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemEveryDaily.TextToControlDistance = 0;
            this.layoutControlItemEveryDaily.TextVisible = false;
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.Control = this.radioButtonWeekday;
            this.layoutControlItem35.CustomizationFormText = "layoutControlItem35";
            this.layoutControlItem35.Location = new System.Drawing.Point(0, 31);
            this.layoutControlItem35.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItem35.MinSize = new System.Drawing.Size(31, 30);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.Size = new System.Drawing.Size(371, 30);
            this.layoutControlItem35.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem35.Text = "layoutControlItem35";
            this.layoutControlItem35.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem35.TextToControlDistance = 0;
            this.layoutControlItem35.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(153, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(218, 31);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupWeekly
            // 
            this.layoutControlGroupWeekly.CustomizationFormText = "layoutControlGroupWeekly";
            this.layoutControlGroupWeekly.GroupBordersVisible = false;
            this.layoutControlGroupWeekly.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem36,
            this.layoutControlItemWeeksWeekly,
            this.layoutControlItem4,
            this.layoutControlItem10,
            this.layoutControlItem13,
            this.layoutControlItem12,
            this.layoutControlItem20,
            this.layoutControlItem19,
            this.layoutControlItem16,
            this.emptySpaceItem3});
            this.layoutControlGroupWeekly.Location = new System.Drawing.Point(107, 307);
            this.layoutControlGroupWeekly.Name = "layoutControlGroupWeekly";
            this.layoutControlGroupWeekly.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupWeekly.Size = new System.Drawing.Size(371, 91);
            this.layoutControlGroupWeekly.Text = "layoutControlGroupWeekly";
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.Control = this.labelEvery;
            this.layoutControlItem36.CustomizationFormText = "layoutControlItem36";
            this.layoutControlItem36.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem36.MaxSize = new System.Drawing.Size(50, 31);
            this.layoutControlItem36.MinSize = new System.Drawing.Size(50, 31);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(50, 31);
            this.layoutControlItem36.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem36.Text = "layoutControlItem36";
            this.layoutControlItem36.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem36.TextToControlDistance = 0;
            this.layoutControlItem36.TextVisible = false;
            // 
            // layoutControlItemWeeksWeekly
            // 
            this.layoutControlItemWeeksWeekly.Control = this.spinEditWeekWeekly;
            this.layoutControlItemWeeksWeekly.CustomizationFormText = "layoutControlItemWeeksWeekly";
            this.layoutControlItemWeeksWeekly.Location = new System.Drawing.Point(50, 0);
            this.layoutControlItemWeeksWeekly.Name = "layoutControlItemWeeksWeekly";
            this.layoutControlItemWeeksWeekly.Size = new System.Drawing.Size(130, 31);
            this.layoutControlItemWeeksWeekly.Text = "semana(s) el:";
            this.layoutControlItemWeeksWeekly.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItemWeeksWeekly.TextLocation = DevExpress.Utils.Locations.Right;
            this.layoutControlItemWeeksWeekly.TextSize = new System.Drawing.Size(65, 13);
            this.layoutControlItemWeeksWeekly.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.checkMon;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 31);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(90, 30);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(90, 30);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(90, 30);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.checkTues;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(90, 31);
            this.layoutControlItem10.MaxSize = new System.Drawing.Size(90, 30);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(90, 30);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(90, 30);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Text = "layoutControlItem10";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextToControlDistance = 0;
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.checkEditThur;
            this.layoutControlItem13.CustomizationFormText = "layoutControlItem13";
            this.layoutControlItem13.Location = new System.Drawing.Point(270, 31);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(101, 30);
            this.layoutControlItem13.Text = "layoutControlItem13";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextToControlDistance = 0;
            this.layoutControlItem13.TextVisible = false;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.checkWed;
            this.layoutControlItem12.CustomizationFormText = "layoutControlItem12";
            this.layoutControlItem12.Location = new System.Drawing.Point(180, 31);
            this.layoutControlItem12.MaxSize = new System.Drawing.Size(90, 30);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(90, 30);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(90, 30);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Text = "layoutControlItem12";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextToControlDistance = 0;
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.checkSund;
            this.layoutControlItem20.CustomizationFormText = "layoutControlItem20";
            this.layoutControlItem20.Location = new System.Drawing.Point(180, 61);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(191, 30);
            this.layoutControlItem20.Text = "layoutControlItem20";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem20.TextToControlDistance = 0;
            this.layoutControlItem20.TextVisible = false;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.checkSat;
            this.layoutControlItem19.CustomizationFormText = "layoutControlItem19";
            this.layoutControlItem19.Location = new System.Drawing.Point(90, 61);
            this.layoutControlItem19.MaxSize = new System.Drawing.Size(90, 30);
            this.layoutControlItem19.MinSize = new System.Drawing.Size(90, 30);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(90, 30);
            this.layoutControlItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem19.Text = "layoutControlItem19";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextToControlDistance = 0;
            this.layoutControlItem19.TextVisible = false;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.checkFrid;
            this.layoutControlItem16.CustomizationFormText = "layoutControlItem16";
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 61);
            this.layoutControlItem16.MaxSize = new System.Drawing.Size(90, 30);
            this.layoutControlItem16.MinSize = new System.Drawing.Size(90, 30);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(90, 30);
            this.layoutControlItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem16.Text = "layoutControlItem16";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem16.TextToControlDistance = 0;
            this.layoutControlItem16.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(180, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(191, 31);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupMonthly
            // 
            this.layoutControlGroupMonthly.CustomizationFormText = "layoutControlGroupMonthly";
            this.layoutControlGroupMonthly.GroupBordersVisible = false;
            this.layoutControlGroupMonthly.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemEveryMonthly,
            this.layoutControlItem5,
            this.layoutControlItemEveryMonthly1,
            this.layoutControlItemMonthMonthly1,
            this.layoutControlItemMonthMonthly,
            this.emptySpaceItem5,
            this.layoutControlItem2});
            this.layoutControlGroupMonthly.Location = new System.Drawing.Point(107, 398);
            this.layoutControlGroupMonthly.Name = "layoutControlGroupMonthly";
            this.layoutControlGroupMonthly.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupMonthly.Size = new System.Drawing.Size(371, 68);
            this.layoutControlGroupMonthly.Text = "layoutControlGroupMonthly";
            // 
            // layoutControlItemEveryMonthly
            // 
            this.layoutControlItemEveryMonthly.Control = this.spinEditDay;
            this.layoutControlItemEveryMonthly.CustomizationFormText = "layoutControlItemEveryMonthly";
            this.layoutControlItemEveryMonthly.Location = new System.Drawing.Point(56, 0);
            this.layoutControlItemEveryMonthly.Name = "layoutControlItemEveryMonthly";
            this.layoutControlItemEveryMonthly.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 5, 5, 5);
            this.layoutControlItemEveryMonthly.Size = new System.Drawing.Size(100, 30);
            this.layoutControlItemEveryMonthly.Text = "de cada";
            this.layoutControlItemEveryMonthly.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItemEveryMonthly.TextLocation = DevExpress.Utils.Locations.Right;
            this.layoutControlItemEveryMonthly.TextSize = new System.Drawing.Size(38, 13);
            this.layoutControlItemEveryMonthly.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.comboBoxEditEnum;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(56, 30);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 5, 5, 5);
            this.layoutControlItem5.Size = new System.Drawing.Size(76, 38);
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItemEveryMonthly1
            // 
            this.layoutControlItemEveryMonthly1.Control = this.comboBoxEditDays;
            this.layoutControlItemEveryMonthly1.CustomizationFormText = "layoutControlItemEveryMonthMonthly1";
            this.layoutControlItemEveryMonthly1.Location = new System.Drawing.Point(132, 30);
            this.layoutControlItemEveryMonthly1.Name = "layoutControlItemEveryMonthly1";
            this.layoutControlItemEveryMonthly1.Size = new System.Drawing.Size(134, 38);
            this.layoutControlItemEveryMonthly1.Text = "de cada";
            this.layoutControlItemEveryMonthly1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItemEveryMonthly1.TextLocation = DevExpress.Utils.Locations.Right;
            this.layoutControlItemEveryMonthly1.TextSize = new System.Drawing.Size(38, 13);
            this.layoutControlItemEveryMonthly1.TextToControlDistance = 5;
            // 
            // layoutControlItemMonthMonthly1
            // 
            this.layoutControlItemMonthMonthly1.Control = this.spinEditMonth1;
            this.layoutControlItemMonthMonthly1.CustomizationFormText = "layoutControlItemMonthMonthly1";
            this.layoutControlItemMonthMonthly1.Location = new System.Drawing.Point(266, 30);
            this.layoutControlItemMonthMonthly1.Name = "layoutControlItemMonthMonthly1";
            this.layoutControlItemMonthMonthly1.Size = new System.Drawing.Size(105, 38);
            this.layoutControlItemMonthMonthly1.Text = "mes(es)";
            this.layoutControlItemMonthMonthly1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItemMonthMonthly1.TextLocation = DevExpress.Utils.Locations.Right;
            this.layoutControlItemMonthMonthly1.TextSize = new System.Drawing.Size(38, 13);
            this.layoutControlItemMonthMonthly1.TextToControlDistance = 5;
            // 
            // layoutControlItemMonthMonthly
            // 
            this.layoutControlItemMonthMonthly.Control = this.spinEditMonth;
            this.layoutControlItemMonthMonthly.CustomizationFormText = "layoutControlItemMonthMonthly";
            this.layoutControlItemMonthMonthly.Location = new System.Drawing.Point(156, 0);
            this.layoutControlItemMonthMonthly.Name = "layoutControlItemMonthMonthly";
            this.layoutControlItemMonthMonthly.Size = new System.Drawing.Size(104, 30);
            this.layoutControlItemMonthMonthly.Text = "mes(es)";
            this.layoutControlItemMonthMonthly.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItemMonthMonthly.TextLocation = DevExpress.Utils.Locations.Right;
            this.layoutControlItemMonthMonthly.TextSize = new System.Drawing.Size(38, 13);
            this.layoutControlItemMonthMonthly.TextToControlDistance = 5;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(260, 0);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(111, 30);
            this.emptySpaceItem5.Text = "emptySpaceItem5";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.panel1;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(56, 68);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(56, 68);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.ShowInCustomizationForm = false;
            this.layoutControlItem2.Size = new System.Drawing.Size(56, 68);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 204);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(105, 320);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(348, 524);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(130, 30);
            this.emptySpaceItem6.Text = "emptySpaceItem6";
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.panelOptions;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(105, 180);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(105, 180);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 10, 2);
            this.layoutControlItem1.Size = new System.Drawing.Size(105, 180);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // simpleSeparator1
            // 
            this.simpleSeparator1.AllowHotTrack = false;
            this.simpleSeparator1.CustomizationFormText = "simpleSeparator1";
            this.simpleSeparator1.Location = new System.Drawing.Point(105, 24);
            this.simpleSeparator1.Name = "simpleSeparator1";
            this.simpleSeparator1.Size = new System.Drawing.Size(2, 500);
            this.simpleSeparator1.Text = "simpleSeparator1";
            // 
            // emptySpaceItemHeader
            // 
            this.emptySpaceItemHeader.AllowHotTrack = false;
            this.emptySpaceItemHeader.CustomizationFormText = "emptySpaceItem9";
            this.emptySpaceItemHeader.Location = new System.Drawing.Point(107, 24);
            this.emptySpaceItemHeader.MaxSize = new System.Drawing.Size(40, 40);
            this.emptySpaceItemHeader.MinSize = new System.Drawing.Size(40, 40);
            this.emptySpaceItemHeader.Name = "emptySpaceItemHeader";
            this.emptySpaceItemHeader.Size = new System.Drawing.Size(371, 40);
            this.emptySpaceItemHeader.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItemHeader.Text = "emptySpaceItemHeader";
            this.emptySpaceItemHeader.TextSize = new System.Drawing.Size(0, 0);
            this.emptySpaceItemHeader.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlGroupScheduler
            // 
            this.layoutControlGroupScheduler.CustomizationFormText = "layoutControlGroupScheduler";
            this.layoutControlGroupScheduler.GroupBordersVisible = false;
            this.layoutControlGroupScheduler.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemScheduler});
            this.layoutControlGroupScheduler.Location = new System.Drawing.Point(107, 64);
            this.layoutControlGroupScheduler.Name = "layoutControlGroupScheduler";
            this.layoutControlGroupScheduler.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupScheduler.Size = new System.Drawing.Size(371, 182);
            this.layoutControlGroupScheduler.Text = "layoutControlGroupScheduler";
            // 
            // layoutControlItemScheduler
            // 
            this.layoutControlItemScheduler.Control = this.dateNavigator1;
            this.layoutControlItemScheduler.CustomizationFormText = "layoutControlItemScheduler";
            this.layoutControlItemScheduler.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemScheduler.Name = "layoutControlItemScheduler";
            this.layoutControlItemScheduler.Size = new System.Drawing.Size(371, 182);
            this.layoutControlItemScheduler.Text = "layoutControlItemScheduler";
            this.layoutControlItemScheduler.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemScheduler.TextToControlDistance = 0;
            this.layoutControlItemScheduler.TextVisible = false;
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
            this.emptySpaceItem8.Location = new System.Drawing.Point(107, 497);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(371, 27);
            this.emptySpaceItem8.Text = "emptySpaceItem8";
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupRotative
            // 
            this.layoutControlGroupRotative.CustomizationFormText = "layoutControlGroupRotative";
            this.layoutControlGroupRotative.GroupBordersVisible = false;
            this.layoutControlGroupRotative.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem7,
            this.layoutControlItemHoursFree,
            this.layoutControlItem9,
            this.layoutControlItemHoursWork,
            this.layoutControlItem8});
            this.layoutControlGroupRotative.Location = new System.Drawing.Point(107, 466);
            this.layoutControlGroupRotative.Name = "layoutControlGroupRotative";
            this.layoutControlGroupRotative.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupRotative.Size = new System.Drawing.Size(371, 31);
            this.layoutControlGroupRotative.Text = "layoutControlGroupRotative";
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(322, 0);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(49, 31);
            this.emptySpaceItem7.Text = "emptySpaceItem7";
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItemHoursFree
            // 
            this.layoutControlItemHoursFree.Control = this.spinEditHoursFree;
            this.layoutControlItemHoursFree.CustomizationFormText = "layoutControlItemHoursFree";
            this.layoutControlItemHoursFree.Location = new System.Drawing.Point(231, 0);
            this.layoutControlItemHoursFree.Name = "layoutControlItemHoursFree";
            this.layoutControlItemHoursFree.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 5, 5, 5);
            this.layoutControlItemHoursFree.Size = new System.Drawing.Size(91, 31);
            this.layoutControlItemHoursFree.Text = "horas";
            this.layoutControlItemHoursFree.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItemHoursFree.TextLocation = DevExpress.Utils.Locations.Right;
            this.layoutControlItemHoursFree.TextSize = new System.Drawing.Size(27, 13);
            this.layoutControlItemHoursFree.TextToControlDistance = 5;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.labelFree;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(157, 0);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(74, 31);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(74, 31);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(74, 31);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItemHoursWork
            // 
            this.layoutControlItemHoursWork.Control = this.spinEditHoursToWork;
            this.layoutControlItemHoursWork.CustomizationFormText = "layoutControlItemHoursWork";
            this.layoutControlItemHoursWork.Location = new System.Drawing.Point(67, 0);
            this.layoutControlItemHoursWork.Name = "layoutControlItemHoursWork";
            this.layoutControlItemHoursWork.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 5, 5, 5);
            this.layoutControlItemHoursWork.Size = new System.Drawing.Size(90, 31);
            this.layoutControlItemHoursWork.Text = "horas";
            this.layoutControlItemHoursWork.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItemHoursWork.TextLocation = DevExpress.Utils.Locations.Right;
            this.layoutControlItemHoursWork.TextSize = new System.Drawing.Size(27, 13);
            this.layoutControlItemHoursWork.TextToControlDistance = 5;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.labelWorkingDay;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(67, 31);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(67, 31);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(67, 31);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItemEnd
            // 
            this.layoutControlItemEnd.Control = this.timeEditEnd;
            this.layoutControlItemEnd.CustomizationFormText = "layoutControlItemEnd";
            this.layoutControlItemEnd.Location = new System.Drawing.Point(177, 0);
            this.layoutControlItemEnd.Name = "layoutControlItemEnd";
            this.layoutControlItemEnd.Size = new System.Drawing.Size(178, 24);
            this.layoutControlItemEnd.Text = "layoutControlItemEnd";
            this.layoutControlItemEnd.TextSize = new System.Drawing.Size(113, 13);
            // 
            // layoutControlItemBegin
            // 
            this.layoutControlItemBegin.Control = this.timeEditBegin;
            this.layoutControlItemBegin.CustomizationFormText = "layoutControlItemBegin";
            this.layoutControlItemBegin.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemBegin.Name = "layoutControlItemBegin";
            this.layoutControlItemBegin.Size = new System.Drawing.Size(177, 24);
            this.layoutControlItemBegin.Text = "layoutControlItemBegin";
            this.layoutControlItemBegin.TextSize = new System.Drawing.Size(113, 13);
            // 
            // DateIntervalsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControlDateIntervals);
            this.Name = "DateIntervalsControl";
            this.Size = new System.Drawing.Size(484, 560);
            this.Load += new System.EventHandler(this.RaiseParameters_Change);
            ((System.ComponentModel.ISupportInitialize)(this.dateNavigator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.styleController1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditEnum.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlDateIntervals)).EndInit();
            this.layoutControlDateIntervals.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.timeEditBegin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeEditEnd.Properties)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panelOptions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spinEditDayDaily.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditHoursFree.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditHoursToWork.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditWeekWeekly.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkSund.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkMon.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkTues.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkSat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkWed.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditThur.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditMonth1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkFrid.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditDays.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditDay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditMonth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupDaily)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDaysDaily)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemEveryDaily)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupWeekly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWeeksWeekly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupMonthly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemEveryMonthly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemEveryMonthly1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemMonthMonthly1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemMonthMonthly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItemHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupScheduler)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemScheduler)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupRotative)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHoursFree)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHoursWork)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBegin)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RadioButton radioButtonDaily;
        private DevExpress.XtraScheduler.DateNavigator dateNavigator1;
        private System.Windows.Forms.RadioButton radioButtonMonthly;
        private System.Windows.Forms.RadioButton radioButtonScheduler;
        private DevExpress.XtraEditors.StyleController styleController1;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditEnum;
        private DevExpress.XtraEditors.SpinEdit spinEditMonth;
        private DevExpress.XtraEditors.SpinEdit spinEditDay;
        private DevExpress.XtraEditors.SpinEdit spinEditMonth1;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditDays;
        private System.Windows.Forms.ToolTip toolTip1;
        private RadioButton radioButtonWeekly;
        private DevExpress.XtraEditors.CheckEdit checkMon;
        private DevExpress.XtraEditors.CheckEdit checkTues;
        private DevExpress.XtraEditors.CheckEdit checkWed;
        private DevExpress.XtraEditors.CheckEdit checkEditThur;
        private DevExpress.XtraEditors.CheckEdit checkFrid;
        private DevExpress.XtraEditors.CheckEdit checkSat;
        private DevExpress.XtraEditors.CheckEdit checkSund;
        private DevExpress.XtraEditors.SpinEdit spinEditWeekWeekly;
        private RadioButton radioButtonRotative;
        private DevExpress.XtraEditors.SpinEdit spinEditHoursFree;
        private DevExpress.XtraEditors.SpinEdit spinEditHoursToWork;
        private Label labelFree;
        private Label labelWorkingDay;
        private DevExpress.XtraEditors.DateEdit dateEditTo;
        private DevExpress.XtraEditors.DateEdit dateEditFrom;
        private DevExpress.XtraLayout.LayoutControl layoutControlDateIntervals;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.SpinEdit spinEditDayDaily;
        private RadioButton radioButtonEveryDaily;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemEveryDaily;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDaysDaily;
        private RadioButton radioButtonWeekday;
        private Label labelEvery;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemWeeksWeekly;
        private RadioButton radioButtonDayMonthly;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemEveryMonthly;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemMonthMonthly;
        private RadioButton radioButtonTheMonthly;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemEveryMonthly1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemMonthMonthly1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemFrom;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTo;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupDaily;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupWeekly;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupMonthly;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private Panel panelOptions;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private Panel panel1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItemHeader;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupScheduler;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemScheduler;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupRotative;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemHoursFree;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemHoursWork;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.TimeEdit timeEditBegin;
        private DevExpress.XtraEditors.TimeEdit timeEditEnd;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemEnd;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemBegin;
    }
}
