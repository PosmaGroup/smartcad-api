using SmartCadControls;
using SmartCadControls.Controls;
namespace SmartCadGuiCommon.Controls
{
    partial class SelectUnSelectControlDevx
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.layoutControlMain = new DevExpress.XtraLayout.LayoutControl();
            this.simpleButtonExRemoveAll = new SimpleButtonEx();
            this.simpleButtonExRemove = new SimpleButtonEx();
            this.simpleButtonExAdd = new SimpleButtonEx();
            this.gridControlExSelected = new GridControlEx();
            this.gridViewExSelected = new GridViewEx();
            this.gridControlExNotSelected = new GridControlEx();
            this.gridViewExNotSelected = new GridViewEx();
            this.simpleButtonExAddAll = new SimpleButtonEx();
            this.layoutControlGroupMain = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemNotSelected = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemSelected = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlMain)).BeginInit();
            this.layoutControlMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExSelected)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExSelected)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExNotSelected)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExNotSelected)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemNotSelected)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSelected)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControlMain
            // 
            this.layoutControlMain.AllowCustomizationMenu = false;
            this.layoutControlMain.Appearance.DisabledLayoutGroupCaption.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControlMain.Appearance.DisabledLayoutGroupCaption.Options.UseForeColor = true;
            this.layoutControlMain.Appearance.DisabledLayoutItem.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControlMain.Appearance.DisabledLayoutItem.Options.UseForeColor = true;
            this.layoutControlMain.Controls.Add(this.simpleButtonExRemoveAll);
            this.layoutControlMain.Controls.Add(this.simpleButtonExRemove);
            this.layoutControlMain.Controls.Add(this.simpleButtonExAdd);
            this.layoutControlMain.Controls.Add(this.gridControlExSelected);
            this.layoutControlMain.Controls.Add(this.gridControlExNotSelected);
            this.layoutControlMain.Controls.Add(this.simpleButtonExAddAll);
            this.layoutControlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlMain.Location = new System.Drawing.Point(0, 0);
            this.layoutControlMain.Name = "layoutControlMain";
            this.layoutControlMain.Root = this.layoutControlGroupMain;
            this.layoutControlMain.Size = new System.Drawing.Size(827, 300);
            this.layoutControlMain.TabIndex = 0;
            this.layoutControlMain.Text = "layoutControlMain";
            // 
            // simpleButtonExRemoveAll
            // 
            this.simpleButtonExRemoveAll.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButtonExRemoveAll.Appearance.ForeColor = System.Drawing.Color.Black;
            this.simpleButtonExRemoveAll.Appearance.Options.UseFont = true;
            this.simpleButtonExRemoveAll.Appearance.Options.UseForeColor = true;
            this.simpleButtonExRemoveAll.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.simpleButtonExRemoveAll.Location = new System.Drawing.Point(392, 191);
            this.simpleButtonExRemoveAll.MinimumSize = new System.Drawing.Size(44, 22);
            this.simpleButtonExRemoveAll.Name = "simpleButtonExRemoveAll";
            this.simpleButtonExRemoveAll.Size = new System.Drawing.Size(44, 22);
            this.simpleButtonExRemoveAll.StyleController = this.layoutControlMain;
            this.simpleButtonExRemoveAll.TabIndex = 9;
            this.simpleButtonExRemoveAll.Text = "<<";
            this.simpleButtonExRemoveAll.Click += new System.EventHandler(this.simpleButtonExRemoveAll_Click);
            // 
            // simpleButtonExRemove
            // 
            this.simpleButtonExRemove.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButtonExRemove.Appearance.ForeColor = System.Drawing.Color.Black;
            this.simpleButtonExRemove.Appearance.Options.UseFont = true;
            this.simpleButtonExRemove.Appearance.Options.UseForeColor = true;
            this.simpleButtonExRemove.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.simpleButtonExRemove.Location = new System.Drawing.Point(392, 158);
            this.simpleButtonExRemove.MinimumSize = new System.Drawing.Size(44, 22);
            this.simpleButtonExRemove.Name = "simpleButtonExRemove";
            this.simpleButtonExRemove.Size = new System.Drawing.Size(44, 22);
            this.simpleButtonExRemove.StyleController = this.layoutControlMain;
            this.simpleButtonExRemove.TabIndex = 8;
            this.simpleButtonExRemove.Text = "<";
            this.simpleButtonExRemove.Click += new System.EventHandler(this.simpleButtonExRemove_Click);
            // 
            // simpleButtonExAdd
            // 
            this.simpleButtonExAdd.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButtonExAdd.Appearance.ForeColor = System.Drawing.Color.Black;
            this.simpleButtonExAdd.Appearance.Options.UseFont = true;
            this.simpleButtonExAdd.Appearance.Options.UseForeColor = true;
            this.simpleButtonExAdd.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.simpleButtonExAdd.Location = new System.Drawing.Point(392, 92);
            this.simpleButtonExAdd.MinimumSize = new System.Drawing.Size(44, 22);
            this.simpleButtonExAdd.Name = "simpleButtonExAdd";
            this.simpleButtonExAdd.Size = new System.Drawing.Size(44, 22);
            this.simpleButtonExAdd.StyleController = this.layoutControlMain;
            this.simpleButtonExAdd.TabIndex = 6;
            this.simpleButtonExAdd.Text = ">";
            this.simpleButtonExAdd.Click += new System.EventHandler(this.simpleButtonExAdd_Click);
            // 
            // gridControlExSelected
            // 
            this.gridControlExSelected.EnableAutoFilter = false;
            this.gridControlExSelected.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExSelected.Location = new System.Drawing.Point(447, 31);
            this.gridControlExSelected.MainView = this.gridViewExSelected;
            this.gridControlExSelected.Name = "gridControlExSelected";
            this.gridControlExSelected.Size = new System.Drawing.Size(375, 264);
            this.gridControlExSelected.TabIndex = 5;
            this.gridControlExSelected.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExSelected});
            this.gridControlExSelected.ViewTotalRows = false;
            // 
            // gridViewExSelected
            // 
            this.gridViewExSelected.AllowFocusedRowChanged = true;
            this.gridViewExSelected.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExSelected.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExSelected.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExSelected.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExSelected.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExSelected.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExSelected.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExSelected.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExSelected.EnablePreviewLineForFocusedRow = false;
            this.gridViewExSelected.GridControl = this.gridControlExSelected;
            this.gridViewExSelected.Name = "gridViewExSelected";
            this.gridViewExSelected.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExSelected.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExSelected.ViewTotalRows = false;
            this.gridViewExSelected.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridViewExSelected_SelectionChanged);
            this.gridViewExSelected.DoubleClick += new System.EventHandler(this.simpleButtonExRemove_Click);
            // 
            // gridControlExNotSelected
            // 
            this.gridControlExNotSelected.EnableAutoFilter = false;
            this.gridControlExNotSelected.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExNotSelected.Location = new System.Drawing.Point(6, 31);
            this.gridControlExNotSelected.MainView = this.gridViewExNotSelected;
            this.gridControlExNotSelected.Name = "gridControlExNotSelected";
            this.gridControlExNotSelected.Size = new System.Drawing.Size(375, 264);
            this.gridControlExNotSelected.TabIndex = 4;
            this.gridControlExNotSelected.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExNotSelected});
            this.gridControlExNotSelected.ViewTotalRows = false;
            // 
            // gridViewExNotSelected
            // 
            this.gridViewExNotSelected.AllowFocusedRowChanged = true;
            this.gridViewExNotSelected.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExNotSelected.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExNotSelected.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExNotSelected.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExNotSelected.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExNotSelected.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExNotSelected.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExNotSelected.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExNotSelected.EnablePreviewLineForFocusedRow = false;
            this.gridViewExNotSelected.GridControl = this.gridControlExNotSelected;
            this.gridViewExNotSelected.Name = "gridViewExNotSelected";
            this.gridViewExNotSelected.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExNotSelected.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExNotSelected.ViewTotalRows = false;
            this.gridViewExNotSelected.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridViewExNotSelected_SelectionChanged);
            this.gridViewExNotSelected.DoubleClick += new System.EventHandler(this.simpleButtonExAdd_Click);
            // 
            // simpleButtonExAddAll
            // 
            this.simpleButtonExAddAll.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButtonExAddAll.Appearance.ForeColor = System.Drawing.Color.Black;
            this.simpleButtonExAddAll.Appearance.Options.UseFont = true;
            this.simpleButtonExAddAll.Appearance.Options.UseForeColor = true;
            this.simpleButtonExAddAll.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.simpleButtonExAddAll.Location = new System.Drawing.Point(392, 125);
            this.simpleButtonExAddAll.MinimumSize = new System.Drawing.Size(44, 22);
            this.simpleButtonExAddAll.Name = "simpleButtonExAddAll";
            this.simpleButtonExAddAll.Size = new System.Drawing.Size(44, 22);
            this.simpleButtonExAddAll.StyleController = this.layoutControlMain;
            this.simpleButtonExAddAll.TabIndex = 7;
            this.simpleButtonExAddAll.Text = ">>";
            this.simpleButtonExAddAll.Click += new System.EventHandler(this.simpleButtonExAddAll_Click);
            // 
            // layoutControlGroupMain
            // 
            this.layoutControlGroupMain.CustomizationFormText = "layoutControlGroupMain";
            this.layoutControlGroupMain.GroupBordersVisible = false;
            this.layoutControlGroupMain.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemNotSelected,
            this.layoutControlItemSelected,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.emptySpaceItem1,
            this.emptySpaceItem2});
            this.layoutControlGroupMain.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupMain.Name = "layoutControlGroupMain";
            this.layoutControlGroupMain.Size = new System.Drawing.Size(827, 300);
            this.layoutControlGroupMain.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroupMain.Text = "layoutControlGroupMain";
            this.layoutControlGroupMain.TextVisible = false;
            // 
            // layoutControlItemNotSelected
            // 
            this.layoutControlItemNotSelected.Control = this.gridControlExNotSelected;
            this.layoutControlItemNotSelected.CustomizationFormText = "layoutControlItemNotSelected";
            this.layoutControlItemNotSelected.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemNotSelected.MinSize = new System.Drawing.Size(111, 56);
            this.layoutControlItemNotSelected.Name = "layoutControlItemNotSelected";
            this.layoutControlItemNotSelected.Size = new System.Drawing.Size(386, 300);
            this.layoutControlItemNotSelected.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemNotSelected.Text = "layoutControlItemNotSelected";
            this.layoutControlItemNotSelected.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItemNotSelected.TextSize = new System.Drawing.Size(145, 20);
            // 
            // layoutControlItemSelected
            // 
            this.layoutControlItemSelected.Control = this.gridControlExSelected;
            this.layoutControlItemSelected.CustomizationFormText = "layoutControlItemSelected";
            this.layoutControlItemSelected.Location = new System.Drawing.Point(441, 0);
            this.layoutControlItemSelected.MinSize = new System.Drawing.Size(111, 56);
            this.layoutControlItemSelected.Name = "layoutControlItemSelected";
            this.layoutControlItemSelected.Size = new System.Drawing.Size(386, 300);
            this.layoutControlItemSelected.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemSelected.Text = "layoutControlItemSelected";
            this.layoutControlItemSelected.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItemSelected.TextSize = new System.Drawing.Size(145, 20);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.simpleButtonExAdd;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(386, 86);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(55, 33);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(55, 33);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(55, 33);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.simpleButtonExAddAll;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(386, 119);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(55, 33);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(55, 33);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(55, 33);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.simpleButtonExRemove;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(386, 152);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(55, 33);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(55, 33);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(55, 33);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.simpleButtonExRemoveAll;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(386, 185);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(55, 33);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(55, 33);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(55, 33);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(386, 218);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(55, 82);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(386, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(55, 86);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // SelectUnSelectControlDevx
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControlMain);
            this.Name = "SelectUnSelectControlDevx";
            this.Size = new System.Drawing.Size(827, 300);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlMain)).EndInit();
            this.layoutControlMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExSelected)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExSelected)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExNotSelected)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExNotSelected)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemNotSelected)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSelected)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControlMain;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupMain;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemNotSelected;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemSelected;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        internal SimpleButtonEx simpleButtonExRemoveAll;
        internal SimpleButtonEx simpleButtonExRemove;
        internal SimpleButtonEx simpleButtonExAdd;
        internal GridControlEx gridControlExSelected;
        internal GridControlEx gridControlExNotSelected;
        internal SimpleButtonEx simpleButtonExAddAll;
        internal GridViewEx gridViewExSelected;
        internal GridViewEx gridViewExNotSelected;

    }
}
