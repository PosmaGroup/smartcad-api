﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using Smartmatic.SmartCad.Service;
using System.Net;

using System.ServiceModel;
using System.IO;
using System.Reflection;
using System.Collections;
using System.Xml;
using System.Diagnostics;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.Utils;
using System.Runtime.Serialization;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList;
using DevExpress.XtraEditors.Controls;
using SmartCadCore.Common;
using SmartCadCore.ClientData;
using SmartCadControls.SyncBoxes;
using SmartCadCore.Core;
using SmartCadControls.Controls;
using SmartCadControls.Util;
using SmartCadGuiCommon.Util;
using SmartCadGuiCommon.Services;
using SmartCadControls;
using SmartCadCore.Enums;
using Smartmatic.SmartCad.Map;
using Smartmatic.SmartCad.Vms;

namespace SmartCadGuiCommon.Controls
{
    public partial class MapLayoutDevX : UserControl
    {
        #region Fields

        public MessageBalloon messageBallon;
        private object obj = new object();

        private CamerasRibbonForm camerasRibbonForm;

        private Thread searchThread;
        private Thread waitThread;
        private WaitForm waitForm;
        private ImageSearchState setVisibleImageSearch = ImageSearchState.NotPainted;
		private IList<AddressClientData> searchAddress;
        //Aqui se guarda  la informacion sobre los layers sobre los cuales se hacen las busquedas
        //es un mapeo entre el nombre del layer y las columnas que estan configuradas para ser buscables.
        Dictionary<string, IList> layersInfo = new Dictionary<string,IList>();
        //aqui se precalcula el query que se realiza con los layers y la informacion de layersInfo
        Dictionary<string, string> layersQuery;
        //grid control de los resultados de busqueda.
        private GridControlSynBox searchSyncBox;

        private DateTime syncServerDateTime;
        private TimeSpan sinceLoggedIn;
        private const int GLOBAL_TIMER_PERIOD = 10000;
        private readonly TimeSpan ADD_TIMESPAN;

        private UnitClientData auxSelectedUnit;
        private IncidentClientData auxSelectedInc;
        
        //taza de cambio de los iconos cuando crecen o decrecen
        public const int RATE_SIZE_ICON_CHANGE = 3;

        private SkinEngine skinEngine;
        private int xxaux;
        private int yyaux;
        //aqui se aloja la informacion que viene de primer nivel, sobre la busqueda.
        private AddressClientData addressData;
        
        private double viewChangeOldValue;
        //Tamano inicial para el zoom mas afuera.
        private static double bitmapIconSize = 5;

        private System.Drawing.Rectangle bounds;
                
        //variable para controlar que no se busque 2 veces seguidas.
        private bool searching = false;
		private object searchingSync = new object();
		double totalDistance = 0;

        MessageForm notFoundResults;
		

        //LM: Variable for ClientConfiguration
        private ConfigurationClientData config;

		#endregion

        #region Constructor
        
        public MapLayoutDevX()
        {
            InitializeComponent();
            LoadLanguage();
            ADD_TIMESPAN = new TimeSpan(0, 0, 0, 0, GLOBAL_TIMER_PERIOD);

            this.searchAddress = new List<AddressClientData>();

			this.textEditZone.KeyDown += new KeyEventHandler(textBoxEx_KeyDown);

            LoadMap();

            SetSkin();
            SetComboBoxSearchTypeValues();

            //LM: Load of the Configuration of the server
            config = ServerServiceClient.GetInstance().GetConfiguration();
            
        }

        private void LoadMap()
        {
            if (SmartCadConfiguration.SmartCadSection.ServerElement.Map.Local.ToLower() == bool.TrueString.ToLower())
            {
                UpdateMaps();
            }
            panelControl1.Controls.Remove(mapControlEx1);
            mapControlEx1 = MapControlEx.GetInstance(SmartCadConfiguration.SmartCadSection.ServerElement.Map.Dll);
            panelControl1.Controls.Add(mapControlEx1);

            //Designer
            defaultToolTipController1.SetAllowHtmlText(mapControlEx1, DevExpress.Utils.DefaultBoolean.Default);
            mapControlEx1.Dock = System.Windows.Forms.DockStyle.Fill;
            mapControlEx1.ShowDistance = false;
            mapControlEx1.Distance = 0;
            mapControlEx1.TabIndex = 0;
            mapLayersControl.MapControlEx = mapControlEx1;

            mapControlEx1.Initialized += new EventHandler(mapControlEx1_Initialized);
        }

        #endregion

        void LoadLanguage()
        {
            layoutControlItem1.Text = ResourceLoader.GetString2("AddressZone") + ": ";
            layoutControlGroupSearch.Text = ResourceLoader.GetString2("SearchTitle");
			layoutControlGroupVisualization.Text = ResourceLoader.GetString2("Visualization");
            layoutControlItemSearchType.Text = ResourceLoader.GetString2("SearchBy") + " :";
            buttonCancel.Text = ResourceLoader.GetString2("");
            buttonSearch.Text = ResourceLoader.GetString2("");

            buttonCancel.ToolTip = ResourceLoader.GetString2("$Message.Cancel");
            buttonSearch.ToolTip = ResourceLoader.GetString2("Search");
            dockPanelTools.Text = ResourceLoader.GetString2("SearchingToolsMaps");
            layoutControlItem1.Text = ResourceLoader.GetString2("UrbSector");
        }

        #region ServerService Delegates Calls

        /// <summary>
        /// este metodo recibe la informacion de otros programas, este metodo
        /// procesa la informacion que proviene de primer nivel, despacho o administracion
        /// de forma tal que mapas haga la accion correspondiente, hay que destacar que si se
        /// trata de acceder a las tablas dinamicas desde la ejecucion de este hilo o delegado
        /// mapas no motrara las tablas y saldran nulas.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void serverServiceClient_GisAction(object sender, GisActionEventArgs e)
        {
            try
            {
                if (e is SearchGeoAddressEventArgs)
                {
                    SearchGeoAddressEventArgs geoEvent = e as SearchGeoAddressEventArgs;

                    if (!string.IsNullOrEmpty(geoEvent.GeoAddress.Zone) ||
                        !string.IsNullOrEmpty(geoEvent.GeoAddress.Street) ||
                        !string.IsNullOrEmpty(geoEvent.GeoAddress.More) ||
                        !string.IsNullOrEmpty(geoEvent.GeoAddress.Reference))
                    {
                        FormUtil.InvokeRequired(this, delegate
                        {
                            buttonCancel_Click(null, null);
                            this.CleanControl();

                            //AutoSelecting the Address search type.
                            comboBoxSearchType.SelectedIndex = 0;

                            this.AddressData = new AddressClientData(
                                geoEvent.GeoAddress.Zone,
                                geoEvent.GeoAddress.Street,
                                geoEvent.GeoAddress.Reference,
                                geoEvent.GeoAddress.More,
                                false);
                            this.RaiseBackGroundSearch();
                        });
                    }
                    else
                    {
                        FormUtil.InvokeRequired(this, delegate
                        {
                            buttonCancel_Click(null, null);
                            this.CleanControl();
                        });
                    }
                }
                else if (e is SendGeoPointEventArgs)
                {
                    SendGeoPointEventArgs ea = e as SendGeoPointEventArgs;
                    CurrentShapeType = ea.Type;
                    //CurrentZoneName = ea.ZoneCode;

                    FormUtil.InvokeRequired(mapControlEx1, delegate
                    {
                        this.CleanControl();
                        this.AddressData = new AddressClientData(
                            ea.Zone,
                            ea.Street,
                            "",
                            "", ea.IsSynchronize);
                        if ((string.IsNullOrEmpty(ea.Zone) && string.IsNullOrEmpty(ea.Street)) == false)
                            this.buttonSearch.PerformClick();
                        if (ea.IsSynchronize == true)
                        {
                            GeoPoint point = ea.Point;
                            if (point.Lat != 0 && point.Lon != 0)
                                mapControlEx1.CenterMap(6656);

                        }
                    });
                }
            }
            catch (GisException gis) 
            {
                mapLayersControl.MapControlEx.ClearTables();
                FormUtil.InvokeRequired(this,
                    delegate
                    {
                        MessageForm.Show(gis.Message, MessageFormType.Error);
                    });
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        #endregion Calls

        #region Form Delegate calls

        void mapControlEx1_Initialized(object sender, EventArgs e)
        {
            try
            {
                syncServerDateTime = ServerServiceClient.GetInstance().GetTime();
                sinceLoggedIn = TimeSpan.Zero;

                ApplicationServerService.GisAction += new EventHandler<GisActionEventArgs>(serverServiceClient_GisAction);
                this.mapControlEx1.ViewChanged += new MapControlEx.ViewChangedEventHandler(Map_ViewChangedEvent);
                this.mapControlEx1.FeatureSelected += new MapControlEx.FeatureSelectedEventHandler(Tools_FeatureSelected);
                this.mapControlEx1.DrawEvent += new MapControlEx.DrawEventHandler(map_DrawEvent);
                this.mapControlEx1.ShowBallonEvent += new MapControlEx.ShowBallonEventHandler(ShowBallonEvent);
                this.mapControlEx1.MouseMove += new MouseEventHandler(mapControl_MouseMove);
                this.mapControlEx1.ShowSuperToolTipEvent += new MapControlEx.ShowSuperToolTipEventHandler(ShowSuperToolTip);

                searchSyncBox = new GridControlSynBox(gridControlSearchResult);
                gridControlSearchResult.Type = typeof(GridSearchMapData);

                mapLayersControl.CreateMapsLayersTreeView();
                mapLayersControl.CreateMapLayersSmartCad("MapsPrincipal");

                mapLayersControl.MapControlEx.ReloadMap();
                BuildConfigurationSearchPanel();
            }
            catch (CommunicationObjectFaultedException)
            {
                throw;
            }
            catch (FaultException ex)
            {
                if (ex.Code != null && ex.Code.Name != null
                    && ex.Code.Name.Equals(typeof(DatabaseServerDownException).Name))
                {
                    throw new CommunicationObjectFaultedException(ex.Message, ex);
                }
                else
                {
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                SplashForm.CloseSplash();
            }
        }

        /// <summary>
        /// Metodo principal de mapas, es el que carga toda la data, los layers
        /// estaticos y los layers dinamicos, ademas de inicializar todos los eventos de las
        /// herramientas que son utilizadas en mapas.
        /// </summary>
        public void LoadData()
        {
            this.mapLayersControl.GlobalApplicationPreference = GlobalApplicationPreference;
            mapControlEx1.InitializeMaps();
        }

        /// <summary>
        /// coloca los valores en el combo box de busqueda
        /// </summary>
        private void SetComboBoxSearchTypeValues()
        {
            ComboBoxItemCollection coll = this.comboBoxSearchType.Properties.Items; 
            coll.BeginUpdate();
            try
            {
                coll.Add(ResourceLoader.GetString2("Address"));
                coll.Add(ResourceLoader.GetString2("Units"));
                coll.Add(ResourceLoader.GetString2("INCIDENTS"));
                coll.Add(ResourceLoader.GetString2("Structs"));
            }
            finally
            {
                coll.EndUpdate();
            }
            this.comboBoxSearchType.SelectedIndex = 0;
        }

        /// <summary>
        /// Este metodo es para realizar acciones como cambiar el icono de 
        /// la manito del pan a apretada, y ademas sirve para acomodar
        /// las cosas mediante pasa la accion de matener el pan presionado
        /// con el mouse
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void panMapToolEx_BeforeMouseDown(object sender, MouseEventArgs e)
        {
            //chequeo de la pantalla del ballon.
            IntPtr handle = Win32.LoadCursorFromFile(SmartCadConfiguration.ClosedHandCursorFilename);
            mapControlEx1.SetToolUseDefaultCursor(false,new Cursor(handle));
            //if (messageBallon != null && messageBallon.SetVisible)
            //{
            //    messageBallon.Hide(true);
            //}
            if(toolTipController1.Active)
                toolTipController1.HideHint();
            mapControlEx1.ChangeAddPositionToolState(AddPositionToolState.Moving);
        }

        /// <summary>
        /// Cuando termina de utilizar el pan
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void panMapToolEx_MouseUp(object sender, MouseEventArgs e)
        {
            IntPtr handle = Win32.LoadCursorFromFile(SmartCadConfiguration.OpenHandCursorFilename);
            this.mapControlEx1.SetToolUseDefaultCursor(false,new Cursor(handle));
            //repintado del ballon
            //if (messageBallon != null && messageBallon.SetVisible)
            //{
            //    Map_ViewChangedEvent(null, null);
            //}
            if (toolTipController1.Active)
                toolTipController1.HideHint();
            mapControlEx1.ChangeAddPositionToolState((AddPositionToolState.Normal));
        }

        /// <summary>
        /// Metodo que se ejecuta cada vez que el mapa
        /// cambia de vista, o se refresca. Aqui se calcula
        /// el nuevo tamano de los iconos ademas de que se calcula
        /// si hay nuevos cambios en los iconos pintados.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Map_ViewChangedEvent(object sender, ViewChangedEventArgs e)
        {
            //UpdateBallon();
            if (toolTipController1.Active)
                toolTipController1.HideHint();
        }


        /// <summary>
        /// Metodo importante...
        /// Cada vez que una persona selecciona algo con la herramienta de seleccionar
        /// entra en este metodo. Es importante que usen el codigo que esta aqui
        /// para iterar o buscar el elemento seleccionado.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Tools_FeatureSelected(object sender, FeatureSelectedEventArgs e)
        {
            //para cambiar el estilo de color de la figura seleccionanda en el mapa.
            //if (messageBallon != null)
            //{
            //    messageBallon.Dispose();
            //    messageBallon.SetVisible = false;

            //}
            if (toolTipController1.Active)
                toolTipController1.HideHint();

            //Se redirecciona el evento al mapControlEx pero se le pasa el handler para manejarlo aqui
            mapControlEx1.ToolFeatureSelected(new EventHandler(toolstripmenuitem_Click),e, ref toolTipController1);
        }
        

        void toolstripmenuitem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            ShowBallon((BallonData)item.Tag);
        }

        /// <summary>
        /// Aqui es donde se muestra la informacion.
        /// </summary>
        /// <param name="ballon"></param>
        private void ShowBallon(BallonData ballon)
        {
            if (ballon.Type == this.mapLayersControl.MapControlEx.DynTableUnitsName)
            {
                #region Units
                /*UnitClientData unit = null;

                IList list = ServerServiceClient.GetInstance().SearchClientObjects(
                    SmartCadHqls.GetCustomHql(SmartCadHqls.GetUnitByCode, ballon.Code));

                if (list != null && list.Count > 0)
                {
                    unit = (UnitClientData)list[0];
                    System.Windows.Forms.Label l = new System.Windows.Forms.Label();
                    l.Location = ballon.Position;
                    l.Size = new Size(0, 0);
                    this.mapControlEx1.Controls.Add(l);
                    auxSelectedUnit = unit;

                    messageBallon = new MessageBalloon();
                    messageBallon.Parent = l;
                    messageBallon.Title = unit.CustomCode;

                    messageBallon.Lat = ballon.Lat;
                    messageBallon.Lon = ballon.Lon;

                    messageBallon.TitleIcon = TooltipIcon.Info;

                    string distance = "0";
                    if (unit.DispatchOrder != null)
                    {
                        IncidentClientData inc = (IncidentClientData)ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetIncidentByCode, unit.DispatchOrder.IncidentCode))[0];
                        GeoPoint incPoint = new GeoPoint(inc.Address.Lon, inc.Address.Lat);
                        GeoPoint unitPoint = new GeoPoint(unit.Lon, unit.Lat);
                        IList<GeoPoint> listUnit = new List<GeoPoint>();
                        listUnit.Add(unitPoint);
                        IList<double> listRes = GisServiceUtil.GetDistances(incPoint, listUnit);
                        distance = listRes[0].ToString("0.00");
                    }

                    //string tooltipUnit2 = ResourceLoader.GetString2("MessageBallonMapsUnit");

                    string tooltipUnit = "Department type: {0}\r\nZone: {1}\r\nStation: {2}\r\n\r\nType: {3}\r\nStatus: {4}\r\nPortable: {5}\r\nDistance (Km): {6}\r\nLocated on: {7}";
                    tooltipUnit = String.Format(tooltipUnit, unit.DepartmentStation.DepartmentZone.DepartmentType.Name, unit.DepartmentStation.DepartmentZone.Name, unit.DepartmentStation.Name, unit.Type.Name, unit.Status.FriendlyName, unit.IdRadio, distance, unit.CoordinatesDate.ToString());
                    messageBallon.Text = tooltipUnit;

                    messageBallon.Align = BalloonAlignment.TopMiddle;
                    messageBallon.CenterStem = true;
                    FormUtil.InvokeRequired(this, new MethodInvoker(delegate
                    {
                        messageBallon.Show();
                    }));

                    this.mapControlEx1.Controls.Remove(l);
                }*/
                MapObjectUnit unit = mapLayersControl.TableUnits.Get(int.Parse(ballon.Code));

                if (unit != null)
                {
                    LabelControl l = new LabelControl();
                    l.Location = new System.Drawing.Point((int)ballon.Lon, (int)ballon.Lat);
                    l.Size = new Size(0, 0);

                    toolTipController1.Active = true;
                    toolTipController1.InitialDelay = 100;
                    toolTipController1.ReshowDelay = 3000;
                    toolTipController1.CloseOnClick = DefaultBoolean.True;

                    SuperToolTip sTooltip2 = new SuperToolTip();
                    ToolTipControllerShowEventArgs args = new ToolTipControllerShowEventArgs();
                    args.ToolTipLocation = ToolTipLocation.BottomLeft;

                    ToolTipTitleItem titleItem1 = new ToolTipTitleItem();
                    titleItem1.Text = unit.Fields["CustomCode"].ToString();

                    string distance = "0";
                    if (unit.Fields.ContainsKey("IncidentCode") && (int)unit.Fields["IncidentCode"] >= 0)
                    {
                        IncidentClientData inc = (IncidentClientData)ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetIncidentByCode, unit.Fields["IncidentCode"]))[0];
                        GeoPoint incPoint = new GeoPoint(inc.Address.Lon, inc.Address.Lat);
                        GeoPoint unitPoint = new GeoPoint(unit.Position.Lon, unit.Position.Lat);
                        IList<GeoPoint> listUnit = new List<GeoPoint>();
                        listUnit.Add(unitPoint);
                        IList<double> listRes = GisServiceUtil.GetDistances(incPoint, listUnit);
                        distance = listRes[0].ToString("0.00");
                    }

                    ToolTipItem item = new ToolTipItem();
                    string tooltipUnit = "Department type: {0}\r\nZone: {1}\r\nStation: {2}\r\n\r\nType: {3}\r\nStatus: {4}\r\nPortable: {5}\r\nDistance (Km): {6}\r\nLocated on: {7}";
                    tooltipUnit = String.Format(tooltipUnit, unit.Fields["DepartmentTypeName"], unit.Fields["DepartmentZoneName"], unit.Fields["DepartmentStationName"], unit.Fields["TypeName"], unit.Fields["StatusFriendlyName"], unit.Fields["Portable"], distance, unit.Fields["CoordinatesDate"]);
                    item.Text = tooltipUnit;

                    ToolTipSeparatorItem sepItem = new ToolTipSeparatorItem();

                    sTooltip2.Items.Add(titleItem1);
                    sTooltip2.Items.Add(sepItem);
                    sTooltip2.Items.Add(item);
                    args.SuperTip = sTooltip2;
                    args.SelectedControl = l;
                    toolTipController1.SetSuperTip(l, sTooltip2);
                    toolTipController1.ShowHint(args);
                }
                #endregion
            }
            else if (ballon.Type == this.mapLayersControl.MapControlEx.DynTableIncidentsName)
            {
                #region Incidents
                //IList list = ServerServiceClient.GetInstance().SearchClientObjects(
                //    SmartCadHqls.GetCustomHql(SmartCadHqls.GetIncidentByCode, ballon.Code));

                //if (list != null && list.Count > 0)
                //{
                //    IncidentClientData incident = (IncidentClientData)list[0];

                //    System.Windows.Forms.Label l = new System.Windows.Forms.Label();
                //    l.Location = ballon.Position;
                //    l.Size = new Size(0, 0);
                //    this.mapControlEx1.Controls.Add(l);

                //    auxSelectedInc = incident;

                //    messageBallon = new MessageBalloon();

                //    messageBallon.Parent = l;
                //    messageBallon.Title = incident.CustomCode;

                //    messageBallon.Lat = ballon.Lat;
                //    messageBallon.Lon = ballon.Lon;

                //    messageBallon.TitleIcon = TooltipIcon.Info;


                //    //string tooltipUnit = ResourceLoader.GetString2("MessageBallonMapsIncident");
                //    string tooltipUnit = "Estatus: {0}\r\nFecha de inicio: {1}\r\n";
                //    tooltipUnit = String.Format(tooltipUnit, incident.Status.FriendlyName, incident.StartDate.ToString());
                //    messageBallon.Text = tooltipUnit;

                //    messageBallon.Align = BalloonAlignment.TopMiddle;
                //    messageBallon.CenterStem = true;
                //    messageBallon.UseAbsolutePositioning = true;
                //    messageBallon.Show();

                //    this.mapControlEx1.Controls.Remove(l);

                //}

                MapObjectIncident incident = mapLayersControl.TableIncidents.Get(int.Parse(ballon.Code));

                if (incident != null)
                {
                    LabelControl l = new LabelControl();
                    l.Location = new System.Drawing.Point((int)ballon.Lon, (int)ballon.Lat);
                    l.Size = new Size(0, 0);

                    toolTipController1.Active = true;
                    toolTipController1.InitialDelay = 100;
                    toolTipController1.ReshowDelay = 3000;
                    toolTipController1.CloseOnClick = DefaultBoolean.True;

                    SuperToolTip sTooltip2 = new SuperToolTip();
                    ToolTipControllerShowEventArgs args = new ToolTipControllerShowEventArgs();
                    args.ToolTipLocation = ToolTipLocation.BottomLeft;

                    ToolTipTitleItem titleItem1 = new ToolTipTitleItem();
                    titleItem1.Text = incident.Fields["CustomCode"].ToString();

                    ToolTipItem item = new ToolTipItem();
                    string tooltipText = "Status: {0}\r\nStart Date: {1}\r\n";
                    tooltipText = String.Format(tooltipText, incident.Fields["StatusFriendlyName"], incident.Fields["StartDate"]);
                    item.Text = tooltipText;

                    ToolTipSeparatorItem sepItem = new ToolTipSeparatorItem();

                    sTooltip2.Items.Add(titleItem1);
                    sTooltip2.Items.Add(sepItem);
                    sTooltip2.Items.Add(item);
                    args.SuperTip = sTooltip2;
                    args.SelectedControl = l;
                    toolTipController1.SetSuperTip(l, sTooltip2);
                    toolTipController1.ShowHint(args);
                }
                #endregion
            }
            else if (ballon.Type == this.mapLayersControl.MapControlEx.DynTablePostName)
            {
                #region Structs
                StructClientData structClientData = null;

                IList list = ServerServiceClient.GetInstance().SearchClientObjects(
                    SmartCadHqls.GetCustomHql(SmartCadHqls.GetStructsByCode, ballon.Code));

                if (list != null && list.Count > 0)
                {
                    structClientData = (StructClientData)list[0];
                    System.Drawing.Point point = ballon.Position;
                    System.Windows.Forms.ContextMenu menu = new ContextMenu();

                    IList cameraList = ServerServiceClient.GetInstance().SearchClientObjects(
                    SmartCadHqls.GetCustomHql(SmartCadHqls.GetCamerasByStructNameAndOperatorCode,
                        structClientData.Name, 
                        ServerServiceClient.GetInstance().OperatorClient.Code));
                    int index;
                    foreach (CameraClientData camera in cameraList)
                    {
                        MenuItem item = new MenuItem();
                        item.Text = camera.Name;
                        item.Name = camera.CustomId;
                        index = menu.MenuItems.Add(item);
                        menu.MenuItems[index].Click += new EventHandler(StructCamera_Click);
                    }
                    //menu.Show(mapControl, point);
                    menu.Show(mapLayersControl.MapControlEx, point);
                }
                #endregion
            }
        }

        private void UpdateBallon()
        {
            if (messageBallon != null && messageBallon.SetVisible == true)
            {
                System.Drawing.Point clientPoint;
                clientPoint = this.mapControlEx1.GetRelativePoint(new GeoPoint(messageBallon.Lon, messageBallon.Lat));

                if (this.mapControlEx1.IsVisible(clientPoint))
                {
                    System.Windows.Forms.Label l = new System.Windows.Forms.Label();
                    l.Location = new System.Drawing.Point(clientPoint.X, clientPoint.Y);

                    xxaux = clientPoint.X;
                    yyaux = clientPoint.Y;

                    l.Size = new Size(0, 0);
                    this.mapControlEx1.Controls.Add(l);

                    messageBallon.Parent = l;
                    messageBallon.Show();
                    this.mapControlEx1.Controls.Remove(l);
                }
                else
                {
                    messageBallon.SetVisible = false;
                    messageBallon.Hide();
                }
            }
        }

        private void ShowSuperToolTip(object sender, ShowSuperToolTipEventArgs e)
        {
            if (e.ObjectType.Equals(typeof(DepartmentZoneClientData)))
            {
                LabelControl l = new LabelControl();
                l.Location = new System.Drawing.Point((int)e.Position.Lon, (int)e.Position.Lat);
                l.Size = new Size(0, 0);

                toolTipController1.Active = true;
                toolTipController1.InitialDelay = 500;
                toolTipController1.ReshowDelay = 5000;
                SuperToolTip sTooltip2 = new SuperToolTip();
                // Create an object to initialize the SuperToolTip.
                ToolTipControllerShowEventArgs args = new ToolTipControllerShowEventArgs();
                DepartmentZoneClientData zone = new DepartmentZoneClientData();
                zone.Code = e.ObjectCode;// (int)featureCollection[0]["ZoneCode"];
                zone = (DepartmentZoneClientData)ServerServiceClient.GetInstance().RefreshClient(zone);


                ToolTipTitleItem titleItem1 = new ToolTipTitleItem();
                titleItem1.Text = zone.Name;

                args.ToolTipLocation = ToolTipLocation.BottomLeft;
                ToolTipItem item1 = new ToolTipItem();
                item1.Text = zone.Name;

                ToolTipSeparatorItem sepItem = new ToolTipSeparatorItem();


                ToolTipItem dep = new ToolTipItem();
                dep.AllowHtmlText = DefaultBoolean.True;
                dep.Text = "<b>" + ResourceLoader.GetString2("DepartmentType") + " :" + "</b> " + zone.DepartmentType.Name;

                sTooltip2.Items.Add(titleItem1);
                sTooltip2.Items.Add(sepItem);
                sTooltip2.Items.Add(dep);

                args.SuperTip = sTooltip2;
                args.SelectedControl = l;
                toolTipController1.SetSuperTip(l, sTooltip2);
                toolTipController1.ShowHint(args);
            }
            else if (e.ObjectType.Equals(typeof(DepartmentStationClientData)))
            {
                LabelControl l = new LabelControl();
                l.Location = new System.Drawing.Point((int)e.Position.Lon, (int)e.Position.Lat);
                l.Size = new Size(0, 0);

                toolTipController1.Active = true;
                toolTipController1.InitialDelay = 500;
                toolTipController1.ReshowDelay = 5000;
                SuperToolTip sTooltip2 = new SuperToolTip();
                // Create an object to initialize the SuperToolTip.
                ToolTipControllerShowEventArgs args = new ToolTipControllerShowEventArgs();
                DepartmentStationClientData stn = new DepartmentStationClientData();
                stn.Code = e.ObjectCode;
                stn = (DepartmentStationClientData)ServerServiceClient.GetInstance().RefreshClient(stn);


                ToolTipTitleItem titleItem1 = new ToolTipTitleItem();
                titleItem1.Text = stn.Name;

                args.ToolTipLocation = ToolTipLocation.BottomLeft;
                ToolTipItem item1 = new ToolTipItem();
                item1.Text = stn.Name;

                ToolTipSeparatorItem sepItem = new ToolTipSeparatorItem();

                ToolTipItem dep = new ToolTipItem();
                dep.AllowHtmlText = DefaultBoolean.True;
                dep.Text = "<b>" + ResourceLoader.GetString2("DepartmentType") + " : </b>" + stn.DepartamentTypeName + "<br>" +
                    "<b>" + ResourceLoader.GetString2("DepartmentZone") + " : </b>" + stn.DepartmentZone.Name;

                sTooltip2.Items.Add(titleItem1);
                sTooltip2.Items.Add(sepItem);
                sTooltip2.Items.Add(dep);

                args.SuperTip = sTooltip2;
                args.SelectedControl = l;
                toolTipController1.SetSuperTip(l, sTooltip2);
                toolTipController1.ShowHint(args);

                //Refresh();//this.mapControl.Refresh();
            }
            else if (e.ObjectType.Equals(typeof(MapColumnElement)) && e.Fields.Count > 0)
            {
                LabelControl l = new LabelControl();
                l.Location = new System.Drawing.Point((int)e.Position.Lon, (int)e.Position.Lat);
                l.Size = new Size(0, 0);

                toolTipController1.Active = true;
                toolTipController1.InitialDelay = 500;
                toolTipController1.ReshowDelay = 5000;
                SuperToolTip sTooltip2 = new SuperToolTip();
                // Create an object to initialize the SuperToolTip.

                ToolTipControllerShowEventArgs args = new ToolTipControllerShowEventArgs();
                args.ToolTipLocation = ToolTipLocation.BottomLeft;

                ToolTipTitleItem titleItem1 = new ToolTipTitleItem();
                titleItem1.Text = e.Fields[0];

                ToolTipSeparatorItem sepItem = new ToolTipSeparatorItem();

                sTooltip2.Items.Add(titleItem1);
                sTooltip2.Items.Add(sepItem);
                for (int i = 1; i < e.Fields.Count; i++)
                {
                    ToolTipItem item = new ToolTipItem();
                    item.AllowHtmlText = DefaultBoolean.True;
                    item.Text = e.Fields[i];

                    sTooltip2.Items.Add(item);
                }
                args.SuperTip = sTooltip2;
                args.SelectedControl = l;
                toolTipController1.SetSuperTip(l, sTooltip2);
                toolTipController1.ShowHint(args);

                //Refresh();
            }
        }

        private void StructCamera_Click(object sender, EventArgs e)
        {
            //if (camerasRibbonForm == null || camerasRibbonForm.IsDisposed == true)
            //    camerasRibbonForm = new CamerasRibbonForm();
            //camerasRibbonForm.Activate();

            CameraClientData camera = ServerServiceClient.SearchClientObject(
                      SmartCadHqls.GetCustomHql(
                      SmartCadHqls.GetCamerasByCustomId,
                      (sender as MenuItem).Name)) as CameraClientData;

            CameraCctvAppForm cameraCctvAppForm = new CameraCctvAppForm(camera, PlayModes.Live);
            cameraCctvAppForm.Text = camera.Name;
            cameraCctvAppForm.CameraType = camera.Type.Name;
            cameraCctvAppForm.Ip = camera.Ip;
            cameraCctvAppForm.Login = camera.Login;
            cameraCctvAppForm.Password = camera.Password;
            cameraCctvAppForm.StreamType = camera.StreamType;
            cameraCctvAppForm.FrameRate = camera.FrameRate;
            cameraCctvAppForm.Resolution = camera.Resolution;
            //cameraCctvAppForm.MdiParent = camerasRibbonForm;
            cameraCctvAppForm.MaximizeBox = false;

            
            //camerasRibbonForm.Show();


            if (cameraCctvAppForm.IsDisposed == false)
                cameraCctvAppForm.Show();
            //else
               // camerasRibbonForm.Close();
        }  
        #endregion

        #region Properties

        public TreeViewUnits TableTreeViewUnits { get { return mapLayersControl.TreeViewUnits; } }

		public TreeViewIncidents TableTreeViewIncidents { get { return mapLayersControl.TreeViewIncidents; } }

		public TreeViewStructs TableTreeViewStructs { get { return mapLayersControl.TreeViewStructs; } }

		public TreeViewZones TableTreeViewZones { get { return mapLayersControl.TreeViewZones; } }

		public TreeViewStations TableTreeViewStations { get { return mapLayersControl.TreeViewStations; } }

		public TreeViewRoutes TableTreeViewRoutes { get { return mapLayersControl.TreeViewRoutes; } }
		
		public List<TreeListNodeExType> PermissionsUser { get { return mapLayersControl.PermissionsUser; } }

		public ServerServiceClient ServerServiceClient { get; set; }

        public bool TrustedConnection { get; set; }

        public NetworkCredential NetworkCredential { get; set; }

        public ServiceHost MapService { get; set; }

        public ImageSearchState SetVisibleImageSearch
        {
            get
            {
                return setVisibleImageSearch;
            }
            set
            {
                setVisibleImageSearch = value;
            }
        }

		public IDictionary<string, ApplicationPreferenceClientData> GlobalApplicationPreference { get; set; }

        public System.Drawing.Rectangle TempBounds
        {
            set
            {
                bounds = value;
            }
        }

        public ShapeType CurrentShapeType
        {
            get
            {
                return mapLayersControl.CurrentShapeType;
            }
            set
            {
				mapLayersControl.CurrentShapeType = value;
            }
        }
        		
		public AddressClientData AddressData
        {
            get
            {
                if (addressData == null)
                {
                    addressData = new AddressClientData();
                }
                return addressData;
            }
            set
            {
                addressData = value;
                ////FormUtil.InvokeRequired(this, delegate
                ////{
                ////    this.CleanControl();
                ////});
            }
        }

       /* public Map Map
        {
            get
            {
				if (map == null)
					return mapControl.Map;
				return map;
            }
            set
            {
                if (map != null)
                    this.map.DrawEvent -= new MapDrawEventHandler(map_DrawEvent);

                map = value;

                if (map != null)
                    this.map.DrawEvent += new MapDrawEventHandler(map_DrawEvent);
            }
        } */

        public static double BitmapIconSize 
        {
            get
            {
                return bitmapIconSize;
            }
            set
            {
                bitmapIconSize = value;
            }
        }

		public GeoPoint LaspointInRoute
		{
			get
			{
				return mapLayersControl.LaspointInRoute;
			}
			set
			{
				mapLayersControl.LaspointInRoute = value;
			}
		}
		#endregion

        #region Control Calls

        private static void SaveStreamToFile(Stream stream, string filePath)
        {
            FileStream outstream = File.Open(filePath, FileMode.Create, FileAccess.Write);

            CopyStream(stream, outstream);

            outstream.Close();

            stream.Close();
        }

        private static void CopyStream(Stream instream, Stream outstream)
        {
            const int bufferLen = 1024 * 1024;

            byte[] buffer = new byte[bufferLen];

            int count = 0;

            while ((count = instream.Read(buffer, 0, bufferLen)) > 0)
            {
                outstream.Write(buffer, 0, count);
                outstream.Flush();
            }
        }

        private void CompareAndUpdateFiles(FileServerServiceClient fileServerServiceClient, string oldXml, string newXml)
        {
            Dictionary<string, int> oldFiles = new Dictionary<string, int>();
            Dictionary<string, int> newFiles = new Dictionary<string, int>();

            XmlDocument doc;
            XmlElement root;

            doc = new XmlDocument();
            doc.Load(oldXml);

            root = doc.DocumentElement;

            foreach (XmlNode node in root)
            {
                if (node.Name == "file")
                {
                    string name = node.Attributes["name"].Value;
                    int version = int.Parse(node.Attributes["version"].Value);

                    oldFiles.Add(name, version);
                }
            }

            doc = new XmlDocument();
            doc.Load(newXml);

            root = doc.DocumentElement;

            foreach (XmlNode node in root)
            {
                if (node.Name == "file")
                {
                    string name = node.Attributes["name"].Value;
                    int version = int.Parse(node.Attributes["version"].Value);

                    newFiles.Add(name, version);
                }
            }

            foreach (string name in newFiles.Keys)
            {
                if ((oldFiles.ContainsKey(name) == false) || 
                    (oldFiles[name] < newFiles[name]) || 
                    (File.Exists(Path.Combine(SmartCadConfiguration.DistFolder, "Maps/" + name)) == false))
                {
                    Stream stream = fileServerServiceClient.GetFile("Maps/" + name);

                    if (stream != null)
                    {
//#if DEBUG
                       // SaveStreamToFile(stream, Path.Combine(SmartCadConfiguration.DistFolder, "Maps/dev" + name));
//#else
                        SaveStreamToFile(stream, Path.Combine(SmartCadConfiguration.DistFolder, "Maps/" + name));
//#endif
                    }
                }
            }

            File.Delete(Path.Combine(SmartCadConfiguration.DistFolder, "Maps/clientfiles.xml"));

            File.Move(
                Path.Combine(SmartCadConfiguration.DistFolder, "Maps/newclientfiles.xml"),
                Path.Combine(SmartCadConfiguration.DistFolder, "Maps/clientfiles.xml"));
        }

        private void UpdateMaps()
        {
#if !DEBUG
            FileServerServiceClient fileServerServiceClient = FileServerServiceClient.CreateInstance();

            Stream stream = fileServerServiceClient.GetFile("Maps/serverfiles.xml");

            SaveStreamToFile(stream, Path.Combine(SmartCadConfiguration.DistFolder, "Maps/newclientfiles.xml"));

            CompareAndUpdateFiles(fileServerServiceClient,
                Path.Combine(SmartCadConfiguration.DistFolder, "Maps/clientfiles.xml"),
                Path.Combine(SmartCadConfiguration.DistFolder, "Maps/newclientfiles.xml"));
#endif
        }

        /// <summary>
        /// Construye las cosas para el panel de busqueda
        /// </summary>
		private void BuildConfigurationSearchPanel()
        {
            LayerCollection layers = SmartCadConfiguration.SmartCadSection.Maps[mapControlEx1.CurrentMap].Layers;

            foreach (LayerElement elem in layers)
            {
                if (bool.TrueString.ToLower() == elem.Search.ToLower())
                {
                    layersInfo.Add(elem.Name, GetColumnsSearchable(elem.Columns));
                }
            }
            this.mapControlEx1.BuildPreQuerys(layersInfo);
        }

        private List<MapColumnElement> GetColumnsSearchable(ColumnsCollection Columns)
        {
            List<MapColumnElement> ret = new List<MapColumnElement>();
            foreach (MapColumnElement col in Columns)
            {
                if (bool.TrueString.ToLower() == col.Search.ToLower())
                {
                    ret.Add(col);
                }
            }
            return ret;
        }

		private void SetSkin()
        {
            try
            {
                string skinFile = SmartCadConfiguration.SkinsFolder + "/Clasic.xml";

                skinEngine = SkinEngine.Load(skinFile);

                ProfessionalColorTable colorTable = new CustomToolStripColors(
                    skinEngine.Colors["ToolStripGradientBegin"],
                    skinEngine.Colors["ToolStripGradientMiddle"],
                    skinEngine.Colors["ToolStripGradientEnd"],

                    skinEngine.Colors["MenuStripGradientBegin"],
                    skinEngine.Colors["MenuStripGradientEnd"],

                    skinEngine.Colors["StatusStripGradientBegin"],
                    skinEngine.Colors["StatusStripGradientEnd"]
                );

                skinEngine.AddElement("background", this);
                skinEngine.ApplyChanges();
            }
            catch (Exception ex)
            {
                MessageForm.Show(ResourceLoader.GetString2("Error"), ResourceLoader.GetString2("Error cargando skin."), ex);
            }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            syncServerDateTime = syncServerDateTime.AddMilliseconds((double)GLOBAL_TIMER_PERIOD);
            sinceLoggedIn = sinceLoggedIn.Add(ADD_TIMESPAN);            
        }

        private bool Searching
        {
            get
            {
                lock (searchingSync)
                {
                    return searching;
                }
            }
            set
            {
                lock (searchingSync)
                {
                    searching = value;
                }
            }
        }
        /// <summary>
        /// Metodo principal de busqueda de direcciones.
        /// </summary>
        /// <param name="geoAddress"></param>
        private void SearchAddress(GeoAddress geoAddress)
        {
            try
            {
                this.searchAddress.Clear();
                IList<GeoAddress> geoAddressCollection = new List<GeoAddress>();

                geoAddressCollection = mapControlEx1.Search(geoAddress);

                if (geoAddressCollection != null)
                {
                    AddressClientData address = null;

                    foreach (GeoAddress tempGeoAddres in geoAddressCollection)
                    {
                        address = new AddressClientData(
                            tempGeoAddres.Zone,
                            tempGeoAddres.Street,
                            tempGeoAddres.Reference,
                            tempGeoAddres.More,
                            true);
                        address.FullText = tempGeoAddres.FullText;
                        address.Lon = tempGeoAddres.Point.Lon;
                        address.Lat = tempGeoAddres.Point.Lat;

                        this.searchAddress.Add(address);
                    }

                    FormUtil.InvokeRequired(this, delegate
                    {
                        this.AddAddressPoints(searchAddress);
                    });
                }

                FormUtil.InvokeRequired(this, delegate
                {
                    this.textEditZone.Text = CreateStringSearch(geoAddress);
                });
            }
            catch (Exception ex) { MessageForm.Show(ex.ToString(),MessageFormType.Error); }
        }

        private string CreateStringSearch(GeoAddress geoAddress)
        {
            List<string> sb = new List<string>();
            if (!string.IsNullOrEmpty(geoAddress.Zone))
                sb.Add(geoAddress.Zone);
            if (!string.IsNullOrEmpty(geoAddress.Street))
                sb.Add(geoAddress.Street);
            if (!string.IsNullOrEmpty(geoAddress.Reference))
                sb.Add(geoAddress.Reference);
            if (!string.IsNullOrEmpty(geoAddress.More))
                sb.Add(geoAddress.More);
            if (!string.IsNullOrEmpty(geoAddress.FullText))
                sb.Add(geoAddress.FullText);
            return string.Join(",", sb.ToArray());
        }


        public void SearchAddress()
        {
            GeoAddress geoAddress = new GeoAddress();
            geoAddress.Zone = this.AddressData.Zone;
            geoAddress.Street = this.AddressData.Street;
            geoAddress.Reference = this.AddressData.Reference;
            geoAddress.FullText = this.AddressData.FullText;

            SearchAddress(geoAddress);
        }
        #endregion

        #region Dialogs Call

        private void ShowWaitDialog(string waitMessage)
        {
            if (this.waitThread == null)
            {
                ParameterizedThreadStart threadStart = new ParameterizedThreadStart(this.RaiseWaitMessage);
                this.waitThread = new Thread(threadStart);
                this.waitThread.IsBackground = true;
                this.waitThread.SetApartmentState(ApartmentState.STA);
                this.waitThread.Start(waitMessage);
            }
        }

        private void CloseWaitDialog()
        {
            try
            {
                this.waitForm.BeginInvoke(new SimpleDelegate(delegate { this.waitForm.Close(); }));
            }
            catch
            {
            }
        }

        private void RaiseWaitMessage(object message)
        {
            this.waitForm = new WaitForm();
            this.waitForm.Message = message as string;
            Application.Run(this.waitForm);
        }

        #endregion

        private void mapControl_MouseMove(object sender, MouseEventArgs e)
        {
            GeoPoint dpo;
            if (auxSelectedUnit == null)
                dpo = new GeoPoint(0, 0);
            else
                dpo = new GeoPoint(auxSelectedUnit.Lon, auxSelectedUnit.Lat);

            GeoPoint dpo2;
            if (auxSelectedInc == null)
                dpo2 = new GeoPoint(0, 0);
            else
                dpo2 = new GeoPoint(auxSelectedInc.Address.Lon, auxSelectedInc.Address.Lat);

            this.mapControlEx1.GetRelativePoint(dpo); 
            this.mapControlEx1.GetRelativePoint(dpo2);
        }

        /// <summary>
        /// Evento click del boton buscar.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSearch_Click(object sender, EventArgs e)
        {
            if (sender.Equals(this.buttonSearch))
            {
                this.AddressData = new AddressClientData(this.textEditZone.Text.Trim(), false);
                this.RaiseBackGroundSearch();
            }
            else if (sender.Equals(this.buttonCancel))
            {
                this.AddressData = null;
                CleanControl();
                ClearPositionFlag();
            }
        }

        private void RaiseBackGroundSearch()
        {
            if (!Searching)
            {
                Searching = true;

                Image imageT = ResourceLoader.GetImage("$Image.WaitForSearch");
                string methodToCall = "SearchAddress";
                if (comboBoxSearchType.SelectedIndex == 1)
                {
                    methodToCall = "SearchUnits";
                }
                else if (comboBoxSearchType.SelectedIndex == 2)
                {
                    methodToCall = "SearchIncidents";
                }
                else if (comboBoxSearchType.SelectedIndex == 3)
                {
                    methodToCall = "SearchStructs";
                }

                BackgroundProcessForm processForm = new BackgroundProcessForm(ResourceLoader.GetString2("Searching"),
                    imageT, this,
                    new MethodInfo[1] { GetType().GetMethod(methodToCall, BindingFlags.Public | BindingFlags.Instance) },
                    new object[1][] { new object[0] { } });

                processForm.CanThrowError = false;
                processForm.CanCancel = false;
                processForm.FormClosed += new FormClosedEventHandler(processForm_FormClosed);
                processForm.ShowDialog();

                if (gridControlSearchResult.DataSource == null || ((IList)gridControlSearchResult.DataSource).Count == 0)
                {
                    FormUtil.InvokeRequired(this, delegate
                    {
                        if (notFoundResults == null || notFoundResults.IsDisposed == true)
                        {
                            notFoundResults = new MessageForm(ResourceLoader.GetString2("ResultsHaveNotBeenFound"), MessageFormType.Information);
                            if (notFoundResults.ShowDialog() == DialogResult.OK)
                                notFoundResults.Close();
                        }
                    });

                }
                else
                {
                    FormUtil.InvokeRequired(this, delegate
                    {
                        if (notFoundResults != null && notFoundResults.IsDisposed == false)
                            notFoundResults.Close();
                    });

                    //Centra el mapa en el primer resultado
                    GridSearchBasicMapData gsmd = (GridSearchBasicMapData)((IList)gridControlSearchResult.DataSource)[0];
                    if (gsmd != null)
                    {
                        this.mapControlEx1.CenterMapInPoint(new GeoPoint(gsmd.Address.Lon, gsmd.Address.Lat));
                        //puse esto para googlemaps (no afecta a mapXtreme) TODO: unificar manera de trabajar. AA
                        //this.mapControlEx1.InsertTempObject(new MapPoint(0, new GeoPoint(gsmd.Address.Lon, gsmd.Address.Lat), ""), mapControlEx1.DynTableTemp);
                    }
                }

                Searching = false;
            }
        }

        void processForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Searching = false;
        }

        /// <summary>
        /// Buscar unidades
        /// </summary>
        public void SearchUnits() 
        {
            string text = this.AddressData.FullText;
            if (mapLayersControl.TableUnits != null)
            {
                IList unitsInMap = mapLayersControl.TableUnits.SearchUnitInMap(text);
                SetDataSourceSearch(unitsInMap);
            }
        }

        private void SetDataSourceSearch(IList list)
        {
            lock (obj)
            {
                if (list != null && list.Count>0)
                {
                    IList source = new ArrayList();
                    if (list[0] is KeyValuePair<UnitClientData, AddressClientData>)
                    {
                        foreach (KeyValuePair<UnitClientData, AddressClientData> pair in list)
                        {
                            source.Add(new GridSearchUnitMapData(pair.Key, pair.Value));
                        }
                    }
                    else if (list[0] is StructClientData)
                    {
                        foreach (StructClientData client in list)
                        {
                            source.Add(new GridSearchStructMapData(client));
                        }
                    } 
                    else if (list[0] is IncidentClientData)
                    {
                        foreach (IncidentClientData client in list)
                        {
                            source.Add(new GridSearchIncidentMapData(client));
                        }
                    }
                
                    FormUtil.InvokeRequired(this, delegate
                    {
                        gridControlSearchResult.BeginUpdate();
                        gridControlSearchResult.DataSource = source;
                        gridControlSearchResult.EndUpdate();
                    });
                }
            }
        }

        /// <summary>
        /// Busqueda de Incidentes
        /// </summary>
        public void SearchIncidents()
        {
            string text = this.AddressData.FullText;
            if (!string.IsNullOrEmpty(text))
            {
                IList list = mapLayersControl.TableIncidents.SearchIncidents(text);
                SetDataSourceSearch(list);
            }
            else
            {
                SetDataSourceSearch(new  List<IncidentClientData>());
            }
        }

        /// <summary>
        /// Busqueda de postes
        /// </summary>
        public void SearchStructs()
        {
            string text = this.AddressData.FullText;
            if (!string.IsNullOrEmpty(text))
            {
                IList list = mapLayersControl.TableStructs.SearchStructs(text);
                SetDataSourceSearch(list);
            }
            else
            {
                SetDataSourceSearch(new List<GridSearchStructMapData>());
            }
        }

        private void ClearPositionFlag()
        {
            if (CurrentShapeType != ShapeType.None)
            {
                UserApplicationClientData application = (CurrentShapeType == ShapeType.Incident) ?
                        UserApplicationClientData.FirstLevel :
                        UserApplicationClientData.Administration;

                ApplicationServiceClient.Current(application).SendGeoPoint(new GeoPoint(0, 0));
            }
            mapControlEx1.Refresh();
        }

        public void AddAddressPoints(IList<AddressClientData> addressList)
        {
            lock (obj)
            {
                if (addressList != null)
                {
                    List<GridSearchMapData> listAdd = addressList.Cast<AddressClientData>().ToList().ConvertAll<GridSearchMapData>(
                        delegate(AddressClientData add)
                        {
                            return new GridSearchMapData(add);
                        });

                    gridControlSearchResult.BeginUpdate();
                    gridControlSearchResult.DataSource = listAdd;
                    gridControlSearchResult.EndUpdate();
                }
            }
        }

        public void CleanControl()
        {
            FormUtil.InvokeRequired(this, delegate
            {
                this.textEditZone.Text = "";
                this.gridControlSearchResult.BeginUpdate();
                this.gridControlSearchResult.ClearData();
                this.gridControlSearchResult.EndUpdate();

                this.mapControlEx1.ClearActions(MapActions.ClearTempTable);
            });
        }

        void textBoxEx_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.buttonSearch_Click(buttonSearch, null);
            }
        }

        /// <summary>
        /// Evento que maneja el pintado del mapa.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void map_DrawEvent(object sender, DrawEventArgs e)
        {
            if (gridControlSearchResult.SelectedItems.Count > 0)
            {
                AddressClientData address = (gridControlSearchResult.SelectedItems[0] as GridSearchBasicMapData).Address;

                if (address != null && this.mapControlEx1 != null)
                {
                    mapControlEx1.MapDraw(new GeoPoint(address.Lon, address.Lat), setVisibleImageSearch);
                    if (setVisibleImageSearch != ImageSearchState.Moving)
                        setVisibleImageSearch = ImageSearchState.Normal;
                }
            }
        }

        private void ShowBallonEvent(object sender, BallonData ballon)
        {
            ShowBallon(ballon);
        }

        private string GetSetOfStationsActives(List<int> listStations)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("(");
            for (int i = 0; i < listStations.Count; i++)
            {
                if (i > 0)
                    sb.Append("," + listStations[i]);
                else
                    sb.Append(listStations[i]);
            }
            sb.Append(")");
            return sb.ToString();
        }
        /// <summary>
        /// Captura las Estaciones que estan prendidas.
        /// </summary>
        /// <param name="root"></param>
        /// <param name="list"></param>
        private void GetStationOns(TreeListNode root, List<int> list)
        {
            for (int i = 0; i < root.Nodes.Count; i++)
            {
                if (root.Nodes[i].Tag != null && root.Nodes[i].CheckState != CheckState.Unchecked)
                {
                    if (!root.Nodes[i].HasChildren)
                        list.Add((int)root.Nodes[i].Tag);
                    GetStationOns(root.Nodes[i], list);
                }

            }
        }

        private void gridViewSeachResult_Click(object sender, System.EventArgs e)
        {
            if ((sender as GridViewEx).FocusedRowHandle >= 0)
            {
                GridSearchBasicMapData gsmd = (GridSearchBasicMapData)gridViewSeachResult.GetRow((sender as GridViewEx).FocusedRowHandle);
                if (gsmd != null)
                {
                    this.mapControlEx1.CenterMapInPoint(new GeoPoint(gsmd.Address.Lon, gsmd.Address.Lat));
                    //puse esto para googlemaps (no afecta a mapXtreme) TODO: unificar manera de trabajar. AA
                    //this.mapControlEx1.InsertTempObject(new MapPoint(0,new GeoPoint(gsmd.Address.Lon, gsmd.Address.Lat),""),mapControlEx1.DynTableTemp);
                }
            }

        }

        private void gridViewSeachResult_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle >= 0)
            {
                GridSearchBasicMapData gsmd = (GridSearchBasicMapData)gridViewSeachResult.GetRow(e.FocusedRowHandle);
                if (gsmd != null)
                {
                    this.mapControlEx1.CenterMapInPoint(new GeoPoint(gsmd.Address.Lon, gsmd.Address.Lat));
                    //puse esto para googlemaps (no afecta a mapXtreme) TODO: unificar manera de trabajar. AA
                    //this.mapControlEx1.InsertTempObject(new MapPoint(0, new GeoPoint(gsmd.Address.Lon, gsmd.Address.Lat), ""), mapControlEx1.DynTableTemp);
                }
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.textEditZone.Text = "";
            gridControlSearchResult.BeginUpdate();
            gridControlSearchResult.ClearData();
            gridViewSeachResult.ActiveFilterString = "";
            gridControlSearchResult.EndUpdate();

            this.mapControlEx1.ClearActions(MapActions.ClearTempTable);
        }

        private void buttonDoStuffWithMaps_Click(object sender, EventArgs e)
        {
            mapLayersControl.MapControlEx.ClearActions(MapActions.ClearTempTable);
        }

        /// <summary>
        /// Manejo del evento para el combo box de tipo de busqueda.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBoxSearchType_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBoxSearchType.SelectedIndex)
            { 
                case 0:
                    gridControlSearchResult.Type = typeof(GridSearchMapData);
                    break;
                case 1:
                    gridControlSearchResult.Type = typeof(GridSearchUnitMapData);
                    break;
                case 2:
                    gridControlSearchResult.Type = typeof(GridSearchIncidentMapData);
                    break;
                case 3:
                    gridControlSearchResult.Type = typeof(GridSearchStructMapData);
                    break;
            }
            gridViewSeachResult.OptionsView.ColumnAutoWidth = true;

            CleanControl();
        }

        private void textEditZone_EditValueChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textEditZone.Text) == false)
                enableSearchsButtons(true);
            else
                enableSearchsButtons(false);
        }

        private void enableSearchsButtons(bool enable)
        {
            buttonCancel.Enabled = enable;
            buttonSearch.Enabled = enable;
        }
	}
}
