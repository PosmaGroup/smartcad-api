﻿using SmartCadControls.Controls;
namespace SmartCadGuiCommon.Controls
{
	partial class MapLayersControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            DevExpress.XtraTreeList.FilterCondition filterCondition2 = new DevExpress.XtraTreeList.FilterCondition();
            this.treeListColumnType = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.treeListSmartCad = new TreeListEx();
            this.treeListColumn2 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListMaps = new TreeListEx();
            this.treeListColumn1 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupMaps = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupSmartCad = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeListSmartCad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListMaps)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupMaps)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupSmartCad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // treeListColumnType
            // 
            this.treeListColumnType.Caption = "Type";
            this.treeListColumnType.FieldName = "Type";
            this.treeListColumnType.MinWidth = 16;
            this.treeListColumnType.Name = "treeListColumnType";
            this.treeListColumnType.OptionsColumn.AllowEdit = false;
            this.treeListColumnType.OptionsColumn.AllowMove = false;
            this.treeListColumnType.OptionsColumn.AllowSize = false;
            this.treeListColumnType.OptionsColumn.AllowSort = false;
            this.treeListColumnType.OptionsColumn.FixedWidth = true;
            this.treeListColumnType.OptionsColumn.ReadOnly = true;
            this.treeListColumnType.OptionsColumn.ShowInCustomizationForm = false;
            this.treeListColumnType.Width = 2;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.treeListSmartCad);
            this.layoutControl1.Controls.Add(this.treeListMaps);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(325, 313);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // treeListSmartCad
            // 
            this.treeListSmartCad.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn2,
            this.treeListColumnType});
            filterCondition2.Column = this.treeListColumnType;
            filterCondition2.Condition = DevExpress.XtraTreeList.FilterConditionEnum.Equals;
            filterCondition2.Value1 = "IncidentRoot";
            this.treeListSmartCad.FilterConditions.AddRange(new DevExpress.XtraTreeList.FilterCondition[] {
            filterCondition2});
            this.treeListSmartCad.Location = new System.Drawing.Point(7, 184);
            this.treeListSmartCad.Name = "treeListSmartCad";
            this.treeListSmartCad.OptionsBehavior.AllowIndeterminateCheckState = true;
            this.treeListSmartCad.OptionsBehavior.EnableFiltering = true;
            this.treeListSmartCad.OptionsMenu.EnableColumnMenu = false;
            this.treeListSmartCad.OptionsMenu.EnableFooterMenu = false;
            this.treeListSmartCad.OptionsPrint.PrintFilledTreeIndent = true;
            this.treeListSmartCad.OptionsPrint.PrintHorzLines = false;
            this.treeListSmartCad.OptionsPrint.PrintPageHeader = false;
            this.treeListSmartCad.OptionsPrint.PrintReportFooter = false;
            this.treeListSmartCad.OptionsPrint.PrintVertLines = false;
            this.treeListSmartCad.OptionsView.ShowCheckBoxes = true;
            this.treeListSmartCad.OptionsView.ShowColumns = false;
            this.treeListSmartCad.OptionsView.ShowHorzLines = false;
            this.treeListSmartCad.OptionsView.ShowIndicator = false;
            this.treeListSmartCad.Size = new System.Drawing.Size(311, 122);
            this.treeListSmartCad.TabIndex = 14;
            this.treeListSmartCad.AfterExpand += new DevExpress.XtraTreeList.NodeEventHandler(this.treeListMaps_AfterExpand);
            this.treeListSmartCad.AfterCollapse += new DevExpress.XtraTreeList.NodeEventHandler(this.treeListSmartCad_AfterCollapse);
            this.treeListSmartCad.BeforeCheckNode += new DevExpress.XtraTreeList.CheckNodeEventHandler(this.treeListSmartCad_BeforeCheckNode);
            this.treeListSmartCad.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.treeListSmartCad_AfterCheckNode);
            this.treeListSmartCad.CompareNodeValues += new DevExpress.XtraTreeList.CompareNodeValuesEventHandler(this.treeListSmartCad_CompareNodeValues);
            this.treeListSmartCad.SizeChanged += new System.EventHandler(this.treeListSmartCad_SizeChanged);
            // 
            // treeListColumn2
            // 
            this.treeListColumn2.Caption = "treeListColumn2";
            this.treeListColumn2.FieldName = "treeListColumn2";
            this.treeListColumn2.MinWidth = 32;
            this.treeListColumn2.Name = "treeListColumn2";
            this.treeListColumn2.OptionsColumn.AllowEdit = false;
            this.treeListColumn2.OptionsColumn.AllowMove = false;
            this.treeListColumn2.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.treeListColumn2.OptionsColumn.ReadOnly = true;
            this.treeListColumn2.OptionsColumn.ShowInCustomizationForm = false;
            this.treeListColumn2.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.treeListColumn2.Visible = true;
            this.treeListColumn2.VisibleIndex = 0;
            this.treeListColumn2.Width = 91;
            // 
            // treeListMaps
            // 
            this.treeListMaps.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.treeListMaps.Appearance.FocusedCell.Options.UseBackColor = true;
            this.treeListMaps.Appearance.FocusedRow.BackColor = System.Drawing.Color.White;
            this.treeListMaps.Appearance.FocusedRow.Options.UseBackColor = true;
            this.treeListMaps.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn1});
            this.treeListMaps.Location = new System.Drawing.Point(7, 27);
            this.treeListMaps.Name = "treeListMaps";
            this.treeListMaps.OptionsBehavior.AllowIndeterminateCheckState = true;
            this.treeListMaps.OptionsMenu.EnableColumnMenu = false;
            this.treeListMaps.OptionsMenu.EnableFooterMenu = false;
            this.treeListMaps.OptionsPrint.PrintFilledTreeIndent = true;
            this.treeListMaps.OptionsPrint.PrintHorzLines = false;
            this.treeListMaps.OptionsPrint.PrintPageHeader = false;
            this.treeListMaps.OptionsPrint.PrintReportFooter = false;
            this.treeListMaps.OptionsPrint.PrintVertLines = false;
            this.treeListMaps.OptionsView.ShowCheckBoxes = true;
            this.treeListMaps.OptionsView.ShowColumns = false;
            this.treeListMaps.OptionsView.ShowHorzLines = false;
            this.treeListMaps.OptionsView.ShowIndicator = false;
            this.treeListMaps.Size = new System.Drawing.Size(311, 123);
            this.treeListMaps.TabIndex = 13;
            this.treeListMaps.AfterExpand += new DevExpress.XtraTreeList.NodeEventHandler(this.treeListMaps_AfterCollapse);
            this.treeListMaps.AfterCollapse += new DevExpress.XtraTreeList.NodeEventHandler(this.treeListMaps_AfterCollapse);
            this.treeListMaps.BeforeCheckNode += new DevExpress.XtraTreeList.CheckNodeEventHandler(this.treeListMaps_BeforeCheckNode);
            this.treeListMaps.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.treeListMaps_AfterCheckNode);
            this.treeListMaps.CompareNodeValues += new DevExpress.XtraTreeList.CompareNodeValuesEventHandler(this.treeListMaps_CompareNodeValues);
            this.treeListMaps.SizeChanged += new System.EventHandler(this.treeListMaps_SizeChanged);
            // 
            // treeListColumn1
            // 
            this.treeListColumn1.Caption = "treeListColumn1";
            this.treeListColumn1.FieldName = "treeListColumn1";
            this.treeListColumn1.MinWidth = 32;
            this.treeListColumn1.Name = "treeListColumn1";
            this.treeListColumn1.OptionsColumn.AllowEdit = false;
            this.treeListColumn1.OptionsColumn.AllowMove = false;
            this.treeListColumn1.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.treeListColumn1.OptionsColumn.AllowSort = false;
            this.treeListColumn1.OptionsColumn.ReadOnly = true;
            this.treeListColumn1.OptionsColumn.ShowInCustomizationForm = false;
            this.treeListColumn1.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.treeListColumn1.Visible = true;
            this.treeListColumn1.VisibleIndex = 0;
            this.treeListColumn1.Width = 91;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupMaps,
            this.layoutControlGroupSmartCad});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(325, 313);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroupMaps
            // 
            this.layoutControlGroupMaps.CustomizationFormText = "layoutControlGroupMaps";
            this.layoutControlGroupMaps.ExpandButtonLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroupMaps.ExpandButtonVisible = true;
            this.layoutControlGroupMaps.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem10});
            this.layoutControlGroupMaps.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupMaps.Name = "layoutControlGroupMaps";
            this.layoutControlGroupMaps.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupMaps.Size = new System.Drawing.Size(325, 157);
            this.layoutControlGroupMaps.Text = "Caracas";
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.treeListMaps;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(315, 127);
            this.layoutControlItem10.Text = "layoutControlItem10";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextToControlDistance = 0;
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlGroupSmartCad
            // 
            this.layoutControlGroupSmartCad.CustomizationFormText = "layoutControlGroupSmartCad";
            this.layoutControlGroupSmartCad.ExpandButtonLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroupSmartCad.ExpandButtonVisible = true;
            this.layoutControlGroupSmartCad.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem11});
            this.layoutControlGroupSmartCad.Location = new System.Drawing.Point(0, 157);
            this.layoutControlGroupSmartCad.Name = "layoutControlGroupSmartCad";
            this.layoutControlGroupSmartCad.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupSmartCad.Size = new System.Drawing.Size(325, 156);
            this.layoutControlGroupSmartCad.Text = "Mapas";
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.treeListSmartCad;
            this.layoutControlItem11.CustomizationFormText = "layoutControlItem11";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(315, 126);
            this.layoutControlItem11.Text = "layoutControlItem11";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextToControlDistance = 0;
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.treeListSmartCad;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(209, 53);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.treeListMaps;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 53);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(209, 31);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // MapLayersControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Name = "MapLayersControl";
            this.Size = new System.Drawing.Size(325, 313);
            this.Load += new System.EventHandler(this.MapLayersControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeListSmartCad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListMaps)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupMaps)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupSmartCad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private DevExpress.XtraLayout.LayoutControl layoutControl1;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupMaps;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupSmartCad;
		public TreeListEx treeListSmartCad;
		public TreeListEx treeListMaps;
		private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn1;
		private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn2;
		private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumnType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
	}
}
