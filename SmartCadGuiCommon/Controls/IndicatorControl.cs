using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraCharts;
using System.Collections;
using Smartmatic.SmartCad.Service;
using System.Threading;
using SmartCadCore.Common;
using SmartCadCore.ClientData;

using SmartCadCore.Core;


namespace SmartCadGuiCommon.Controls
{
    public enum ExtraSerieTypes
    {
        UpTreshold,
        DownTreshold,
        Trend
    }
    public partial class IndicatorControl : DevExpress.XtraEditors.XtraUserControl
    {
        private int indicatorCodeSelected = -1;
        private bool upTresholdSerieVisible = false;
        private bool downTresholdSerieVisible = false;
        private bool trendSerieVisible = false;
        private IList dataResult = null;
        private bool clear = false;
        private XYDiagram Diagram
        {
            get
            {
                return this.chartControl1.Diagram as XYDiagram;
            }
        }
        public IndicatorControl()
        {            
            InitializeComponent();
            this.chartControl1.Series[1].Visible = trendSerieVisible;
            this.chartControl1.Series[2].Visible = upTresholdSerieVisible;
            this.chartControl1.Series[3].Visible = downTresholdSerieVisible;
            this.indicatorGridControl1.FocusedRowChangedEvent +=
                new IndicatorGridFocusedRowChangedEventHandler(indicatorGridControl1_FocusedRowChangedEvent);
            Diagram.EnableZooming = true;
            Diagram.EnableScrolling = true;

            LoadLanguage();
        }

        private void LoadLanguage()
        {
            chartControl1.Series["Trend"].LegendText = ResourceLoader.GetString2("Trend");
            chartControl1.Series["Calls"].LegendText = ResourceLoader.GetString2("Calls");
            chartControl1.Series["UpTreshold"].LegendText = ResourceLoader.GetString2("UpTreshold");
            chartControl1.Series["DownTreshold"].LegendText = ResourceLoader.GetString2("DownTreshold");
        }
        private void indicatorGridControl1_FocusedRowChangedEvent(object sender,
             IndicatorGridFocusedRowChangedEventArgs e)
        {

            this.dockPanel1.Text = string.Empty;

            if (e.List.Count > 0)
            {
                indicatorCodeSelected = (e.List[0] as IndicatorResultClientData).IndicatorCode;

            }
            if (clear == false)
            {
                clear = e.Clear;
            }
            if (clear == true)
            {
                clear = false;
                this.chartControl1.Series[0].LegendText = string.Empty;
                this.chartControl1.Series[0].Points.Clear();
                if (upTresholdSerieVisible == true)
                {
                    this.chartControl1.Series[2].Points.Clear();
                }
                if (downTresholdSerieVisible == true)
                {
                    this.chartControl1.Series[3].Points.Clear();
                }
                if (e.List.Count > 0)
                {
                    this.chartControl1.Series.BeginUpdate();
                    this.chartControl1.Series[0].LegendText =
                        (e.List[0] as IndicatorResultClientData).IndicatorName;


                    foreach (IndicatorResultClientData ind in e.List)
                    {
                        foreach (IndicatorResultValuesClientData data in ind.Values)
                        {
                            if ((data.IndicatorClassName == IndicatorClassClientData.System.Name) ||
                                        ((data.IndicatorClassName == IndicatorClassClientData.Group.Name) &&
                                        (data.CustomCode == ServerServiceClient.GetInstance().OperatorClient.Code))
                                        ||
                                        (data.IndicatorClassName == IndicatorClassClientData.Department.Name)&&
                                        (data.CustomCode == ((this.ParentForm as IndicatorsForm).MdiParent as SupervisionForm).SelectedDepartment.Code))
                            {
                                SeriesPoint sp1 = null;
                                if (data.MeasureUnit == "%")
                                {
                                    sp1 = new SeriesPoint(data.Date, new double[] { 
                                            (Convert.ToDouble(data.ResultValue)*100) });
                                }
                                else
                                {
                                    sp1 = new SeriesPoint(data.Date, new double[] { 
                                            Convert.ToDouble(data.ResultValue)});
                                }
                                FormUtil.InvokeRequired(this.chartControl1, delegate
                                {
                                    this.chartControl1.Series[0].Points.Add(sp1);
                                });
                                Thread.Sleep(10);
                                //if (trendSerieVisible == true)
                                //{
                                //    this.chartControl1.Series[1].Points.Clear();
                                //    ShowExtraResultSeries(ExtraSerieTypes.Trend, true, data.Date);
                                //}
                                if (upTresholdSerieVisible == true)
                                {
                                    //this.chartControl1.Series[2].Points.Clear();
                                    ShowExtraResultSeries(ExtraSerieTypes.UpTreshold, true, data.Date);
                                }
                                Thread.Sleep(10);
                                if (downTresholdSerieVisible == true)
                                {
                                    //this.chartControl1.Series[3].Points.Clear();
                                    ShowExtraResultSeries(ExtraSerieTypes.DownTreshold, true, data.Date);
                                }
                                Thread.Sleep(10);
                            }
                        }
                    }
                    this.chartControl1.Series.EndUpdate();
                }
            }
            else
            {
                if (this.chartControl1.Series[0].Points.Count > 0)
                {
                    IndicatorResultValuesClientData data =
                        ((e.List[(e.List.Count - 1)] as IndicatorResultClientData).Values[0] as
                        IndicatorResultValuesClientData);
                    if ((data.IndicatorClassName == IndicatorClassClientData.System.Name) ||
                                ((data.IndicatorClassName == IndicatorClassClientData.Group.Name) &&
                                (data.CustomCode == ServerServiceClient.GetInstance().OperatorClient.Code))
                                ||
                                (data.IndicatorClassName == IndicatorClassClientData.Department.Name) &&
                                (data.CustomCode == ((this.ParentForm as IndicatorsForm).MdiParent as SupervisionForm).SelectedDepartment.Code))
                    {
                        SeriesPoint sp1 = null;
                        if (data.MeasureUnit == "%")
                        {
                            sp1 = new SeriesPoint(data.Date, new double[] { 
                                    (Convert.ToDouble(data.ResultValue)*100) });
                        }
                        else
                        {
                            sp1 = new SeriesPoint(data.Date, new double[] { 
                                    Convert.ToDouble(data.ResultValue)});
                        }
                        if (this.chartControl1.Series[0].Points.Count == 60)
                        {
                            this.chartControl1.Series[0].Points.RemoveAt(0);
                        }
                        this.chartControl1.Series[0].Points.Add(sp1);
                        Thread.Sleep(10);
                        //if (trendSerieVisible == true)
                        //{
                        //    this.chartControl1.Series[1].Points.Clear();
                        //    ShowExtraResultSeries(ExtraSerieTypes.Trend, true, data.Date);
                        //}
                        if (upTresholdSerieVisible == true)
                        {
                            //this.chartControl1.Series[2].Points.Clear();
                            ShowExtraResultSeries(ExtraSerieTypes.UpTreshold, true, data.Date);
                        }
                        Thread.Sleep(10);
                        if (downTresholdSerieVisible == true)
                        {
                            //this.chartControl1.Series[3].Points.Clear();
                            ShowExtraResultSeries(ExtraSerieTypes.DownTreshold, true, data.Date);
                        }
                        Thread.Sleep(10);
                    }
                }
            }
            dataResult = this.chartControl1.Series[0].Points;
            if (trendSerieVisible == true)
            {
                this.chartControl1.Series[1].Points.Clear();
                ShowExtraResultSeries(ExtraSerieTypes.Trend, true, DateTime.MinValue);
            }
        }
        public void ShowExtraResultSeries(ExtraSerieTypes extraSerieTypes, bool visible, DateTime OneHourLater)
        {

            if ((indicatorCodeSelected >= 0) && (OneHourLater != DateTime.MinValue))
            {
                DateTime now = ServerServiceClient.GetInstance().GetTime();

                IList indicators = ServerServiceClient.GetInstance().SearchClientObjects(
                             SmartCadHqls.GetCustomHql(SmartCadHqls.GetIndicatorByForeCastTimeAndByIndicatorCode,
                            now.ToString(ApplicationUtil.DataBaseFormattedDate),
                            OneHourLater.ToString(ApplicationUtil.DataBaseFormattedDate),
                            indicatorCodeSelected));

                if ((indicators.Count > 0) && (extraSerieTypes != ExtraSerieTypes.Trend))
                {
                    IndicatorGroupForecastClientData generalForecast = null;
                    IndicatorGroupForecastClientData currentSpecificForecast = null;
                    ArrayList specificsForecast = new ArrayList();

                    this.chartControl1.Series.BeginUpdate();

                    //Ocultar series que no se dean ver
                    if ((extraSerieTypes == ExtraSerieTypes.UpTreshold) && (visible == false))
                    {
                        this.chartControl1.Series[2].Points.Clear();
                    }
                    else if ((extraSerieTypes == ExtraSerieTypes.DownTreshold) && (visible == false))
                    {
                        this.chartControl1.Series[3].Points.Clear();

                    }
                    else if ((extraSerieTypes == ExtraSerieTypes.Trend) && (visible == false))
                    {
                        this.chartControl1.Series[1].Points.Clear();
                    }

                    //Separar grupos generales de especficos
                    foreach (IndicatorGroupForecastClientData indicatorGroupForecastClientData in indicators)
                    {
                        if (indicatorGroupForecastClientData.General == false)
                        {
                            specificsForecast.Add(indicatorGroupForecastClientData);
                        }
                        else
                        {
                            generalForecast = indicatorGroupForecastClientData;
                        }
                    }

                    //Colocar los ltimos 60 puntos en la grfica
                    //for (int i = 0; i < 60; i++)
                    {
                        //Si es general o no hay especfico
                        if ((specificsForecast.Count == 0) ||
                           (OneHourLater < (specificsForecast[0] as IndicatorGroupForecastClientData).Start))
                        {
                            if (extraSerieTypes == ExtraSerieTypes.UpTreshold)
                            {
                                SeriesPoint sp = new SeriesPoint(OneHourLater, new double[] { Convert.ToDouble(
                                    (generalForecast.ForecastValues[0] as IndicatorForecastClientData).High) });
                                FormUtil.InvokeRequired(this.chartControl1, delegate
                                {
                                    this.chartControl1.Series[2].Points.Add(sp);
                                });
                                this.chartControl1.Series[2].Visible = visible;

                            }
                            else if (extraSerieTypes == ExtraSerieTypes.DownTreshold)
                            {
                                SeriesPoint sp = new SeriesPoint(OneHourLater, new double[] { Convert.ToDouble(
                                    (generalForecast.ForecastValues[0] as IndicatorForecastClientData).Low) });
                                FormUtil.InvokeRequired(this.chartControl1, delegate
                                {
                                    this.chartControl1.Series[3].Points.Add(sp);
                                });
                                this.chartControl1.Series[3].Visible = visible;

                            }
                        }
                        //si es especfico
                        else if (OneHourLater < (specificsForecast[0] as IndicatorGroupForecastClientData).End)
                        {
                            currentSpecificForecast = (specificsForecast[0] as
                                IndicatorGroupForecastClientData);

                            if (extraSerieTypes == ExtraSerieTypes.UpTreshold)
                            {
                                SeriesPoint sp = new SeriesPoint(OneHourLater, new double[] { Convert.ToDouble(
                                    (currentSpecificForecast.ForecastValues[0] as 
                                    IndicatorForecastClientData).High) });
                                FormUtil.InvokeRequired(this.chartControl1, delegate
                                {
                                    this.chartControl1.Series[2].Points.Add(sp);
                                });
                                this.chartControl1.Series[2].Visible = visible;

                            }
                            else if (extraSerieTypes == ExtraSerieTypes.DownTreshold)
                            {
                                SeriesPoint sp = new SeriesPoint(OneHourLater, new double[] { Convert.ToDouble(
                                    (currentSpecificForecast.ForecastValues[0] as 
                                    IndicatorForecastClientData).Low) });
                                FormUtil.InvokeRequired(this.chartControl1, delegate
                                {
                                    this.chartControl1.Series[3].Points.Add(sp);
                                });
                                this.chartControl1.Series[3].Visible = visible;

                            }
                        }


                        //Si termino el perodo de un especfico
                        else if (OneHourLater > (specificsForecast[0] as IndicatorGroupForecastClientData).End)
                        {
                            specificsForecast.RemoveAt(0);
                            //i -= 1;
                        }

                        //OneHourLater = OneHourLater.AddMinutes(5);
                    }
                    trendSerieVisible = this.chartControl1.Series[1].Visible;
                    upTresholdSerieVisible = this.chartControl1.Series[2].Visible;
                    downTresholdSerieVisible = this.chartControl1.Series[3].Visible;

                    this.chartControl1.Series.EndUpdate();


                }
            }
            else if (extraSerieTypes == ExtraSerieTypes.Trend)
            {               
                CalculateTrend();
                this.chartControl1.Series[1].Visible = trendSerieVisible = visible;
            }
            else if (OneHourLater == DateTime.MinValue)
            {
                clear = true;
                if (extraSerieTypes == ExtraSerieTypes.UpTreshold)
                {
                    this.chartControl1.Series[2].Visible = visible;
                    upTresholdSerieVisible = visible;
                }
                if (extraSerieTypes == ExtraSerieTypes.DownTreshold)
                {
                    this.chartControl1.Series[3].Visible = visible;
                    downTresholdSerieVisible = visible;
                }
            }



        }

        private void CalculateTrend()
        {
           
            if (dataResult != null)
            {                
                IList currentDataResult = new ArrayList();                
                double mediaY = 0;
                double mediaX = 0;
                List<double> XiXm = new List<double>();
                List<double> YiYm = new List<double>();
                List<double> XY = new List<double>();
                List<double> X2 = new List<double>();
                double sumXY = 0;
                double sumX2 = 0;
                double b = 0;
                double a = 0;                
                //Get mediaX and mediaY           
                #region mediaX and mediaY                
                for (int i = 0; i < dataResult.Count; i++)
                {
                    SeriesPoint point = (SeriesPoint)dataResult[i];
                    currentDataResult.Add(point.Values[0]);
                    mediaY += point.Values[0];
                    mediaX += (i + 1);                    
                }
                mediaY = mediaY / dataResult.Count;
                mediaX = mediaX / dataResult.Count;
                #endregion mediaX and mediaY
                //Get XiXm and YiYm
                #region XiXm and YiYm                
                for (int i = 0; i < currentDataResult.Count; i++)
                {
                    YiYm.Add((double)currentDataResult[i] - mediaY);
                    XiXm.Add((i+1) - mediaX);
                }
                #endregion XiXm and YiYm
                //Get XY mediaXY and X2 mediaX2
                #region XY and X2
                for (int i = 0; i < currentDataResult.Count; i++)
                {
                    XY.Add(XiXm[i] * YiYm[i]);
                    X2.Add(XiXm[i] * XiXm[i]);
                    sumXY += XY[i];
                    sumX2 += X2[i];
                }
                #endregion XY and X2
                b = ((sumXY / currentDataResult.Count) / (sumX2 / currentDataResult.Count));
                a = mediaY - (b * mediaX);
                //Get trend
                #region trend                
                for (int i = 0; i < dataResult.Count; i++)
                {
                    SeriesPoint point = (SeriesPoint)dataResult[i];
                    SeriesPoint sp1 = new SeriesPoint(point.DateTimeArgument, new double[] { (a + (b * (i + 1))) });
                    FormUtil.InvokeRequired(this.chartControl1, delegate
                                           {
                                               this.chartControl1.Series[1].Points.Add(sp1);
                                           });
                    Thread.Sleep(10);
                }
                #endregion trend
            }
        }
    }
}
