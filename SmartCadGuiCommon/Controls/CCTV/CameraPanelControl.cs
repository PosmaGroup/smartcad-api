﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Smartmatic.SmartCad.Service;
using System.Threading;
using DevExpress.XtraLayout;

using SmartCadCore.ClientData;
using Smartmatic.SmartCad.Vms;
using SmartCadGuiCommon.Classes;
using VideoOS.Platform.Client;
using VideoOS.Platform;
using System.Diagnostics;
using VideoOS.Platform.SDK.Platform;
using System.Net;
using VideoOS.Platform.Messaging;
using SmartCadCore.Common;
using System.Reflection;
using SmartCadGuiCommon.Util;

namespace SmartCadGuiCommon.Controls
{
    public partial class CameraPanelControl : UserControl, IPlayBackPanel//DevExpress.XtraEditors.XtraUserControl, IPlayBackPanel
    {
        public event SelectionChanged SelectedChanged;

        public string PanelName { get; set; }
        public VmsControlEx vmsControl { get; set; }
        public CameraClientData Camera { get; set; }
        public DateTime StartPlayBackTime { get; set; }
        public DateTime EndPlayBackTime { get; set; }
        public PlayModes Mode { get; set; }
        private bool selected = false;
        private Thread connectThread = null;
        private ConfigurationClientData config = null;
        public static ImageViewerControl imageViewerControl;
        public event EventHandler Connected;
        private Thread demoThread = null;
        public static System.Windows.Forms.ProgressBar progressBar = new System.Windows.Forms.ProgressBar();
        public static LayoutControl layoutGlobal;
        private delegate void SetControlPropertyThreadSafeDelegate(Control control, string propertyName, object propertyValue);
        public static VmsSingletonUtil VmsSingleton = new VmsSingletonUtil();
        public static List<VmsControlEx> vmsControlList = new List<VmsControlEx>();
        public static VmsSingletonPlaybackUtil VmsPlayback = new VmsSingletonPlaybackUtil();
        public static List<Form> unpinCameras = new List<Form>();


        public static void SetControlPropertyThreadSafe(Control control, string propertyName, object propertyValue)
        {
            if (control.InvokeRequired)
            {
                control.Invoke(new SetControlPropertyThreadSafeDelegate
                (SetControlPropertyThreadSafe),
                new object[] { control, propertyName, propertyValue });
            }
            else
            {
                control.GetType().InvokeMember(
                    propertyName,
                    BindingFlags.SetProperty,
                    null,
                    control,
                    new object[] { propertyValue });
            }
        }
        public CameraPanelControl()
        {
            InitializeComponent();
            this.Text = "";
            this.Disposed += new EventHandler(CameraPanelControl_Disposed);
        }

        public CameraPanelControl(CameraClientData camera, PlayModes mode, ConfigurationClientData config)
            : this(camera, mode, config, true)
        {
        }

        public CameraPanelControl(CameraClientData camera, PlayModes mode, ConfigurationClientData config, int multiple)
            : this(camera, mode, config, true, multiple)
        {
        }

        void vmsControl_DateTimeValueChanged(DateTime dateTime)
        {
            //if (DateTimeValueChanged != null)

            //    this.DateTimeValueChanged(dateTime);

            //    DateTimeValueChanged(dateTime);
            if (dateTime > EndPlayBackTime)
                vmsControl.SetPlaybackDate(StartPlayBackTime);

        }

        public CameraPanelControl(CameraClientData camera, PlayModes mode, ConfigurationClientData config, bool unping)
            : this()
        {
            System.GC.Collect();
            this.config = config;

            Camera = camera;
            Mode = mode;
            if (Mode != PlayModes.Live)
            {
                barManager1.ShowCloseButton = false;
                this.barButtonUnping.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                barAndDockingController1.LookAndFeel.SkinName = "Blue";
            }
            this.Name = Camera.Name;
            this.barStaticText.Caption = Camera.Name;
            this.CreateControl();


            ConfigurationClientData configuration = ServerServiceClient.GetInstance().GetConfiguration();
            vmsControl = VmsControlEx.GetInstance(config.VmsDllName);

            vmsControl.Dock = DockStyle.Fill;
            this.Controls.Add(vmsControl);


            if (Camera.CustomId != "")
            {

                vmsControl.Initialize(Camera.CustomId, configuration.VmsLogin, configuration.VmsPassword,
                                        configuration.VmsServerIp + ":" + configuration.VmsPort);

                vmsControl.Click += new EventHandler(vmsControl_Click);
                vmsControl.DateTimeValueChanged += new VmsControlEx.DateTimeValueChangedEventHandler(vmsControl_DateTimeValueChanged);


                this.Invoke((MethodInvoker)delegate
                {
                    // this.initViewer();


                    vmsControl.Connect(0);
                });
                vmsControl.Playmode(mode);

                //Control.Connect();
                if (Connected != null)
                    Connected(null, null);
                #region Extra Code

                #endregion


            }

            barButtonUnping.Visibility = unping ? DevExpress.XtraBars.BarItemVisibility.Always :
                    DevExpress.XtraBars.BarItemVisibility.Never;
        }

        public void CameraPanelControlTemplates()
        {
            vmsControl.ReleaseConnectionsTemplate();
        }

        public void ClearVMSControl()
        {
            //vmsControl = VmsSingleton.GetInstance(config);

            //foreach (var vms in vmsControlList)
            //{
            //    vms.clearVms();
            //}
            //vmsControlList = new List<VmsControlEx>();
            vmsControl = VmsControlEx.GetInstance(config.VmsDllName);
        }

        public void ClearVMSControlPlayback()
        {
           // vmsControl = VmsPlayback.GetInstance(config);
            vmsControl = VmsPlayback.GetInstance(config);
        }


        public void CameraPanelControlTemplates(VmsControlEx vms)
        {
            vms.ReleaseConnectionsTemplate();
        }


        public CameraPanelControl(CameraClientData camera, PlayModes mode, ConfigurationClientData config, bool unping, int multiple)
            : this()
        {
            System.GC.Collect();
            this.config = config;

            Camera = camera;
            Mode = mode;
            if (Mode != PlayModes.Live)
            {
                barManager1.ShowCloseButton = false;
                this.barButtonUnping.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                barAndDockingController1.LookAndFeel.SkinName = "Blue";
            }
            this.Name = Camera.Name;
            this.barStaticText.Caption = Camera.Name;
            this.CreateControl();


            ConfigurationClientData configuration = ServerServiceClient.GetInstance().GetConfiguration();
            vmsControl = VmsControlEx.GetInstance(config.VmsDllName);// VmsSingleton.GetInstance(config); 


            vmsControl.Dock = DockStyle.Fill;
            this.Controls.Add(vmsControl);


            if (Camera.CustomId != "")
            {
                //vmsControl.SetTemplate(multiple);

                vmsControl.Initialize(Camera.CustomId, configuration.VmsLogin, configuration.VmsPassword,
                                        configuration.VmsServerIp + ":" + configuration.VmsPort);

                vmsControl.Click += new EventHandler(vmsControl_Click);
                vmsControl.DateTimeValueChanged += new VmsControlEx.DateTimeValueChangedEventHandler(vmsControl_DateTimeValueChanged);


                this.Invoke((MethodInvoker)delegate
                {
                    // this.initViewer();


                    vmsControl.Connect(multiple);
                });
                vmsControl.Playmode(mode);

                //Control.Connect();
                if (Connected != null)
                    Connected(null, null);
                #region Extra Code

                #endregion

            }

            barButtonUnping.Visibility = unping ? DevExpress.XtraBars.BarItemVisibility.Always :
                    DevExpress.XtraBars.BarItemVisibility.Never;
        }

        public CameraPanelControl(CameraClientData camera, PlayModes mode, ConfigurationClientData config, bool unping, DateTime startDate, DateTime endDate, VmsSingletonPlaybackUtil VmsControlPlayback)
            : this()
        {
            System.GC.Collect();
            this.config = config;
            Camera = camera;
            Mode = mode;
            if (Mode != PlayModes.Live)
            {
                barManager1.ShowCloseButton = false;
                this.barButtonUnping.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                barAndDockingController1.LookAndFeel.SkinName = "Blue";
            }
            this.Name = Camera.Name;
            this.barStaticText.Caption = Camera.Name;
            this.CreateControl();


            ConfigurationClientData configuration = ServerServiceClient.GetInstance().GetConfiguration();
            vmsControl = VmsControlEx.GetInstance(config.VmsDllName);//VmsControlPlayback.GetInstance(config); // VmsSingleton.GetInstance(config);// 

            

            //vmsControl.startDate = startDate;

            vmsControl.Dock = DockStyle.Fill;
            this.Controls.Add(vmsControl);


            if (Camera.CustomId != "")
            {

                vmsControl.Initialize(Camera.CustomId, configuration.VmsLogin, configuration.VmsPassword,
                                        configuration.VmsServerIp + ":" + configuration.VmsPort);

                vmsControl.Click += new EventHandler(vmsControl_Click);
                vmsControl.DateTimeValueChanged += vmsControl_DateTimeValueChanged;

                

                this.Invoke((MethodInvoker)delegate
                {
                    // this.initViewer();


                    vmsControl.Connect(0);
                });

          //      vmsControl.SetPlaybackEndDate(endDate);
           //     vmsControl.SetPlaybackDate(startDate);
                vmsControl.Playmode(mode);

                //Control.Connect();
                if (Connected != null)
                    Connected(null, null);
                #region Extra Code

                #endregion


            }

            barButtonUnping.Visibility = unping ? DevExpress.XtraBars.BarItemVisibility.Always :
                    DevExpress.XtraBars.BarItemVisibility.Never;
        }

        public CameraPanelControl(CameraClientData camera, PlayModes mode, ConfigurationClientData config, bool unping, DateTime startDate, DateTime endDate, VmsSingletonUtil VmsControl)
            : this()
        {
            System.GC.Collect();
            this.config = config;
            // this.StartPlayBackTime = startDate;
            this.EndPlayBackTime = endDate;
            Camera = camera;
            Mode = mode;
            if (Mode != PlayModes.Live)
            {
                barManager1.ShowCloseButton = false;
                this.barButtonUnping.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                barAndDockingController1.LookAndFeel.SkinName = "Blue";
            }
            this.Name = Camera.Name;
            this.barStaticText.Caption = Camera.Name;
            this.CreateControl();


            ConfigurationClientData configuration = ServerServiceClient.GetInstance().GetConfiguration();
            vmsControl = VmsControl.GetInstance(config); //VmsControlEx.GetInstance(config.VmsDllName); // 

        //    vmsControl.startDate = startDate;

            vmsControl.Dock = DockStyle.Fill;
            this.Controls.Add(vmsControl);


            if (Camera.CustomId != "")
            {

                vmsControl.Initialize(Camera.CustomId, configuration.VmsLogin, configuration.VmsPassword,
                                        configuration.VmsServerIp + ":" + configuration.VmsPort);

                vmsControl.Click += new EventHandler(vmsControl_Click);
                vmsControl.DateTimeValueChanged += vmsControl_DateTimeValueChanged;



                this.Invoke((MethodInvoker)delegate
                {
                    // this.initViewer();


                    vmsControl.Connect(0);
                });

             //   vmsControl.EndPlayBackDate = endDate;
             //   vmsControl.SetPlaybackEndDate(endDate);
            //    vmsControl.SetPlaybackDate(startDate);
                vmsControl.Playmode(mode);

            //    VmsSingleton = new VmsSingletonUtil();

                //Control.Connect();
                if (Connected != null)
                    Connected(null, null);
                #region Extra Code

                #endregion


            }

            barButtonUnping.Visibility = unping ? DevExpress.XtraBars.BarItemVisibility.Always :
                    DevExpress.XtraBars.BarItemVisibility.Never;
        }




        public void DisposePanelControl()
        {
            this.Controls.Clear();
        }


        public CameraPanelControl(CameraClientData camera, PlayModes mode, ConfigurationClientData config, bool unping, DateTime startDate)
            : this()
        {
            System.GC.Collect();
            this.config = config;

            Camera = camera;
            Mode = mode;
            if (Mode != PlayModes.Live)
            {
                barManager1.ShowCloseButton = false;
                this.barButtonUnping.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                barAndDockingController1.LookAndFeel.SkinName = "Blue";
            }
            this.Name = Camera.Name;
            this.barStaticText.Caption = Camera.Name;
            this.CreateControl();


            ConfigurationClientData configuration = ServerServiceClient.GetInstance().GetConfiguration();
            vmsControl = VmsControlEx.GetInstance(config.VmsDllName);
            vmsControl.startDate = startDate;
            vmsControl.SetPlaybackDate(startDate);
            vmsControl.Dock = DockStyle.Fill;
            this.Controls.Add(vmsControl);


            if (Camera.CustomId != "")
            {

                vmsControl.Initialize(Camera.CustomId, configuration.VmsLogin, configuration.VmsPassword,
                                        configuration.VmsServerIp + ":" + configuration.VmsPort);
               
                vmsControl.Click += new EventHandler(vmsControl_Click);
                vmsControl.DateTimeValueChanged += new VmsControlEx.DateTimeValueChangedEventHandler(vmsControl_DateTimeValueChanged);

                vmsControl.Playmode(mode);
                this.Invoke((MethodInvoker)delegate
                {
                    // this.initViewer();


                    vmsControl.Connect(0);
                });

                //Control.Connect();
                if (Connected != null)
                    Connected(null, null);
                #region Extra Code

                #endregion


            }

            barButtonUnping.Visibility = unping ? DevExpress.XtraBars.BarItemVisibility.Always :
                    DevExpress.XtraBars.BarItemVisibility.Never;
        }

        public void SetPlayMode(PlayModes playMode)
        {
            //vmsControl.Dock = DockStyle.Fill;
            //this.Show();
            vmsControl.Playmode(playMode);
        }

        void CameraPanelControl_Disposed(object sender, EventArgs e)
        {
            if (connectThread != null) connectThread.Abort();

            if (selected == true && SelectedChanged != null)
                SelectedChanged(null);
        }

        void vmsControl_Click(object sender, EventArgs e)
        {
            if (this.barAndDockingController1.LookAndFeel.ActiveSkinName != "Blue")
            {
                barAndDockingController1.LookAndFeel.SkinName = "Blue";
                selected = true;
                if (this.ParentForm != null)
                {
                    this.ParentForm.AccessibleDescription = "true";
                }
                if (SelectedChanged != null)
                    SelectedChanged(this);
            }
        }

        public void DeSelect()
        {
            barAndDockingController1.LookAndFeel.SkinName = "Office 2007 Silver";
            selected = false;
            if (this.ParentForm != null)
            {
                this.ParentForm.AccessibleDescription = "false";
            }
        }

        private void barManager1_CloseButtonClick(object sender, EventArgs e)
        {
            if (Parent.Parent is Form && Parent.Parent.Text == "Camera")
                Parent.Parent.Dispose();
            else
            {
                //this.Hide();
                vmsControl.Disconnect();
                this.Dispose();

                CctvFrontClientFormDevX.cameraVisualizeForm.SelectedCamera = null;
                
            }
        }

        private void barButtonUnping_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form floatingForm = new Form();
            floatingForm.FormBorderStyle = FormBorderStyle.SizableToolWindow;
            floatingForm.Margin = new System.Windows.Forms.Padding(0);
            LayoutControl layout = new LayoutControl();
            layout.Margin = new System.Windows.Forms.Padding(0);

            LayoutControlItem dragItem = new LayoutControlItem();
            dragItem.Name = Camera.Name;
            dragItem.Text = "";
            dragItem.TextVisible = false;
            dragItem.Padding = new DevExpress.XtraLayout.Utils.Padding(0);
            dragItem.ParentName = "Customization";
            dragItem.Control = new CameraPanelControl(Camera, Mode, config, false, 1);

            if(SelectedChanged!=null)
                ((CameraPanelControl)dragItem.Control).SelectedChanged += (SelectionChanged)SelectedChanged.GetInvocationList()[0];
           
            layout.AddItem(dragItem);
            layout.GetGroupAtPoint(new Point(1, 1)).GroupBordersVisible = false;
            layout.GetGroupAtPoint(new Point(1, 1)).Padding = new DevExpress.XtraLayout.Utils.Padding(0);
            layoutGlobal = layout;
            floatingForm.Size = new System.Drawing.Size(500, 500);
            floatingForm.StartPosition = FormStartPosition.CenterScreen;
            layout.Dock = DockStyle.Fill;
            floatingForm.Controls.Add(layout);
            floatingForm.Text = "Camera";    
            floatingForm.AccessibleName = Camera.Name;
            floatingForm.Show();

            unpinCameras.Add(floatingForm);
        }

        public List<Form> unpinList()
        {
            if(unpinCameras.Count > 0)
            return unpinCameras;

            return new List<Form>();
        }

        public void ClearunpinList()
        {
            unpinCameras = new List<Form>(); 
        }

        public void AddunpinList(Form unpingForm)
        {
            unpinCameras.Add(unpingForm);
        }

        internal void PTZMove(int X, int Y, int Z)
        {
            vmsControl.PTZMove(X, Y, Z);
        }

        #region IPlayBackPanel Members

        public event VmsControlEx.DateTimeValueChangedEventHandler DateTimeValueChanged;
        private bool unping;

        public double PlaySpeed
        {
            set { vmsControl.SetSpeedPlayback(value); } 
        }

        public int Volume
        {
            set { vmsControl.volume(value); }
        }

        public bool Mute
        {
            set { vmsControl.mute(value); }
        }

        public int PlayDirection
        {
            set { vmsControl.SetPlayDirection(value); }
        }

        public DateTime EndDate
        {
            set { EndPlayBackTime = value;
            vmsControl.SetPlaybackEndDate(value);
            }
        }

        public DateTime StartDate
        {
            set {
                StartPlayBackTime = value;
                vmsControl.SetPlaybackDate(value); }

        }

        public void StopPlayBack()
        {
           vmsControl.StopPlayback();
        }

        public void StartPlayBack()
        {
           vmsControl.StartPlayBack();
        }

        public void ExportVideo(DateTime startDate, DateTime endDate, string path, int format, int timestamp)
        {
            vmsControl.ExportVideo(startDate, endDate, path, format, timestamp);
         
         //   progressBar.Show();
        }

        public void ClearControlPanel()
        {
            this.Controls.Clear();
        }

        public void InitStopPlayback()
        {
            vmsControl.InitStopPlayback();
        }


        //public void ClearVmsControl()
        //{
        //    vmsControl.Dispose();
        //    vmsControl.Disconnect();
        //    vmsControl = null;
        //}


        #endregion
    }
}
