namespace SmartCadGuiCommon.Controls
{
    partial class EmergencyIndicatorControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraCharts.Series series1 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.PieSeriesLabel pieSeriesLabel1 = new DevExpress.XtraCharts.PieSeriesLabel();
            DevExpress.XtraCharts.PiePointOptions piePointOptions1 = new DevExpress.XtraCharts.PiePointOptions();
            DevExpress.XtraCharts.PieSeriesView pieSeriesView1 = new DevExpress.XtraCharts.PieSeriesView();
            DevExpress.XtraCharts.Series series2 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.PieSeriesLabel pieSeriesLabel2 = new DevExpress.XtraCharts.PieSeriesLabel();
            DevExpress.XtraCharts.PiePointOptions piePointOptions2 = new DevExpress.XtraCharts.PiePointOptions();
            DevExpress.XtraCharts.PieSeriesView pieSeriesView2 = new DevExpress.XtraCharts.PieSeriesView();
            DevExpress.XtraCharts.PieSeriesLabel pieSeriesLabel3 = new DevExpress.XtraCharts.PieSeriesLabel();
            DevExpress.XtraCharts.PieSeriesView pieSeriesView3 = new DevExpress.XtraCharts.PieSeriesView();
            this.chartControl1 = new DevExpress.XtraCharts.ChartControl();
            this.emergencyGridIndicatorControl1 = new EmergencyGridIndicatorControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemEmergencyGrid = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemChart = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemEmergencyGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemChart)).BeginInit();
            this.SuspendLayout();
            // 
            // chartControl1
            // 
            this.chartControl1.Location = new System.Drawing.Point(349, 3);
            this.chartControl1.Name = "chartControl1";
            pieSeriesLabel1.LineVisible = true;
            pieSeriesLabel1.Visible = false;
            series1.Label = pieSeriesLabel1;
            piePointOptions1.ValueNumericOptions.Format = DevExpress.XtraCharts.NumericFormat.Percent;
            piePointOptions1.ValueNumericOptions.Precision = 0;
            series1.LegendPointOptions = piePointOptions1;
            series1.Name = "Series 1";
            series1.SynchronizePointOptions = false;
            pieSeriesView1.RuntimeExploding = false;
            series1.View = pieSeriesView1;
            pieSeriesLabel2.LineVisible = true;
            pieSeriesLabel2.Visible = false;
            series2.Label = pieSeriesLabel2;
            piePointOptions2.ValueNumericOptions.Format = DevExpress.XtraCharts.NumericFormat.Percent;
            piePointOptions2.ValueNumericOptions.Precision = 0;
            series2.LegendPointOptions = piePointOptions2;
            series2.Name = "Series 2";
            series2.SynchronizePointOptions = false;
            pieSeriesView2.RuntimeExploding = false;
            series2.View = pieSeriesView2;
            this.chartControl1.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series1,
        series2};
            pieSeriesLabel3.LineVisible = true;
            this.chartControl1.SeriesTemplate.Label = pieSeriesLabel3;
            pieSeriesView3.RuntimeExploding = false;
            this.chartControl1.SeriesTemplate.View = pieSeriesView3;
            this.chartControl1.Size = new System.Drawing.Size(344, 187);
            this.chartControl1.TabIndex = 4;
            // 
            // emergencyGridIndicatorControl1
            // 
            this.emergencyGridIndicatorControl1.DataSource = null;
            this.emergencyGridIndicatorControl1.Location = new System.Drawing.Point(3, 3);
            this.emergencyGridIndicatorControl1.Name = "emergencyGridIndicatorControl1";
            this.emergencyGridIndicatorControl1.Size = new System.Drawing.Size(343, 187);
            this.emergencyGridIndicatorControl1.TabIndex = 3;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Appearance.DisabledLayoutGroupCaption.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControl1.Appearance.DisabledLayoutGroupCaption.Options.UseForeColor = true;
            this.layoutControl1.Appearance.DisabledLayoutItem.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControl1.Appearance.DisabledLayoutItem.Options.UseForeColor = true;
            this.layoutControl1.Controls.Add(this.emergencyGridIndicatorControl1);
            this.layoutControl1.Controls.Add(this.chartControl1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(695, 192);
            this.layoutControl1.TabIndex = 5;
            this.layoutControl1.Text = "layoutControlEmergengyIndicator";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemEmergencyGrid,
            this.layoutControlItemChart});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(695, 192);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItemEmergencyGrid
            // 
            this.layoutControlItemEmergencyGrid.Control = this.emergencyGridIndicatorControl1;
            this.layoutControlItemEmergencyGrid.CustomizationFormText = "layoutControlItemEmergencyGrid";
            this.layoutControlItemEmergencyGrid.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemEmergencyGrid.Name = "layoutControlItemEmergencyGrid";
            this.layoutControlItemEmergencyGrid.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItemEmergencyGrid.Size = new System.Drawing.Size(346, 190);
            this.layoutControlItemEmergencyGrid.Text = "layoutControlItemEmergencyGrid";
            this.layoutControlItemEmergencyGrid.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemEmergencyGrid.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemEmergencyGrid.TextToControlDistance = 0;
            this.layoutControlItemEmergencyGrid.TextVisible = false;
            // 
            // layoutControlItemChart
            // 
            this.layoutControlItemChart.Control = this.chartControl1;
            this.layoutControlItemChart.CustomizationFormText = "layoutControlItemChart";
            this.layoutControlItemChart.Location = new System.Drawing.Point(346, 0);
            this.layoutControlItemChart.Name = "layoutControlItemChart";
            this.layoutControlItemChart.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItemChart.Size = new System.Drawing.Size(347, 190);
            this.layoutControlItemChart.Text = "layoutControlItemChart";
            this.layoutControlItemChart.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemChart.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemChart.TextToControlDistance = 0;
            this.layoutControlItemChart.TextVisible = false;
            // 
            // EmergencyIndicatorControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "EmergencyIndicatorControl";
            this.Size = new System.Drawing.Size(695, 192);
            ((System.ComponentModel.ISupportInitialize)(pieSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemEmergencyGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemChart)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal EmergencyGridIndicatorControl emergencyGridIndicatorControl1;
        internal DevExpress.XtraCharts.ChartControl chartControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemEmergencyGrid;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemChart;
    }
}
