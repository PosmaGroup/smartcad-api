using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Smartmatic.SmartCad.Service;
using System.Collections;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraTab;
using DevExpress.XtraEditors;
using System.Reflection;
using System.ServiceModel;
using SmartCadCore.ClientData;
using SmartCadCore.Core;

using SmartCadCore.Common;


namespace SmartCadGuiCommon.Controls
{
    public partial class IndicatorDashboardPreference : XtraForm
    {
        const int MAX_NUMBER_INDICATOR = 18;
        //private int maxTypeIndicatorsSelected = 0;
        private IList indicators = null;
        private string indicatorClassSelected = string.Empty;
        private SupervisionForm parentForm;
		IList<IndicatorClassDashboardClientData> preferences = new BindingList<IndicatorClassDashboardClientData>();

        public IndicatorDashboardPreference(SupervisionForm parent)
        {
            InitializeComponent();
            parentForm = parent;
            this.FormClosing += new FormClosingEventHandler(IndicatorDashboardPreference_FormClosing);
        }

        void IndicatorDashboardPreference_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (parentForm != null)
                parentForm.SupervisionCommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(IndicatorDashboardPreference_SupervisionCommittedChanges);
        }

        private IndicatorDashboardPreferenceClass CurrentIndicatorDashbardPreference
        {
            get
            {
                string name = this.tabbedControlGroupPreferences.SelectedTabPage.Name;

                if (name == this.layoutControlGroupSystem.Name)
                    return indicatorDashboardPreferenceClassSystem;
                else if (name == layoutControlGroupDepartment.Name)
                    return indicatorDashboardPreferenceClass1;
                else if (name == layoutControlGroupMyGroup.Name)
                    return indicatorDashboardPreferenceClass2;
                else
                    return indicatorDashboardPreferenceClass3;                        

                }
        }

        private void IndicatorDashboardPreference_Load(object sender, EventArgs e)
        {
            LoadLanguage();
            LoadData();
            LoadEvents();
            indicatorClassSelected = this.tabbedControlGroupPreferences.SelectedTabPage.Text;
            this.radioGroupSupervisor.SelectedIndex = 0;
            
        }

        private void LoadLanguage()
        {
            this.Text = ResourceLoader.GetString2("IndicatorPanel");
            this.radioGroupSupervisor.Properties.Items[0].Description = ResourceLoader.GetString2("FirstLevelSupervisor");
            this.radioGroupSupervisor.Properties.Items[1].Description = ResourceLoader.GetString2("DispatchSupervisor");
            this.layoutControlGroupSystem.Text = ResourceLoader.GetString2("System");
            this.layoutControlGroupDepartment.Text = ResourceLoader.GetString2("Department");
            this.layoutControlGroupMyGroup.Text = ResourceLoader.GetString2("Group");
            this.layoutControlGroupOperator.Text = ResourceLoader.GetString2("Operator");
            this.simpleButtonAccept.Text = ResourceLoader.GetString2("Accept");
            this.simpleButtonCancel.Text = ResourceLoader.GetString2("Cancel");
            this.simpleButtonApply.Text = ResourceLoader.GetString2("Apply");

            indicatorDashboardPreferenceClassSystem.layoutControlItemIndicatorType.Text = ResourceLoader.GetString2("IndicatorTypes") + ":";
            indicatorDashboardPreferenceClassSystem.layoutControlItemDescription.Text = ResourceLoader.GetString2("IndicatorDescriptionSelected");
            indicatorDashboardPreferenceClassSystem.layoutControlItemIndicator.Text = ResourceLoader.GetString2("Indicators") + ":";

            indicatorDashboardPreferenceClass1.layoutControlItemIndicatorType.Text = ResourceLoader.GetString2("IndicatorTypes") + ":";
            indicatorDashboardPreferenceClass1.layoutControlItemDescription.Text = ResourceLoader.GetString2("IndicatorDescriptionSelected");
            indicatorDashboardPreferenceClass1.layoutControlItemIndicator.Text = ResourceLoader.GetString2("Indicators") + ":";

            indicatorDashboardPreferenceClass2.layoutControlItemIndicatorType.Text = ResourceLoader.GetString2("IndicatorTypes") + ":";
            indicatorDashboardPreferenceClass2.layoutControlItemDescription.Text = ResourceLoader.GetString2("IndicatorDescriptionSelected");
            indicatorDashboardPreferenceClass2.layoutControlItemIndicator.Text = ResourceLoader.GetString2("Indicators") + ":";

            indicatorDashboardPreferenceClass3.layoutControlItemIndicatorType.Text = ResourceLoader.GetString2("IndicatorTypes") + ":";
            indicatorDashboardPreferenceClass3.layoutControlItemDescription.Text = ResourceLoader.GetString2("IndicatorDescriptionSelected");
            indicatorDashboardPreferenceClass3.layoutControlItemIndicator.Text = ResourceLoader.GetString2("Indicators") + ":";

        }
            
        

        private void LoadData()
        {
            indicators = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetIndicatorClassDashboardsWithClassesByDashboard);
        }

        private void LoadEvents()
        {
            parentForm.SupervisionCommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(IndicatorDashboardPreference_SupervisionCommittedChanges);

            indicatorDashboardPreferenceClassSystem.comboBoxEditIndicatorType.SelectedValueChanged += new EventHandler(comboBoxEditIndicatorType_SelectedValueChanged);
            indicatorDashboardPreferenceClassSystem.listBoxControlIndicators.MouseDoubleClick += new MouseEventHandler(listBoxControlIndicators_MouseDoubleClick);
            indicatorDashboardPreferenceClassSystem.listBoxControlIndicators.ItemCheck += new DevExpress.XtraEditors.Controls.ItemCheckEventHandler(listBoxControlIndicators_ItemCheck);
            indicatorDashboardPreferenceClassSystem.listBoxControlIndicators.ItemChecking += new ItemCheckingEventHandler(listBoxControlIndicators_ItemChecking);
            indicatorDashboardPreferenceClassSystem.listBoxControlIndicators.SelectedValueChanged += new EventHandler(listBoxControlIndicators_SelectedValueChanged);

            indicatorDashboardPreferenceClass1.comboBoxEditIndicatorType.SelectedValueChanged += new EventHandler(comboBoxEditIndicatorType_SelectedValueChanged);
            indicatorDashboardPreferenceClass1.listBoxControlIndicators.MouseDoubleClick += new MouseEventHandler(listBoxControlIndicators_MouseDoubleClick);
            indicatorDashboardPreferenceClass1.listBoxControlIndicators.ItemCheck += new DevExpress.XtraEditors.Controls.ItemCheckEventHandler(listBoxControlIndicators_ItemCheck);
            indicatorDashboardPreferenceClass1.listBoxControlIndicators.ItemChecking += new ItemCheckingEventHandler(listBoxControlIndicators_ItemChecking);
            indicatorDashboardPreferenceClass1.listBoxControlIndicators.SelectedValueChanged += new EventHandler(listBoxControlIndicators_SelectedValueChanged);

            indicatorDashboardPreferenceClass2.comboBoxEditIndicatorType.SelectedValueChanged += new EventHandler(comboBoxEditIndicatorType_SelectedValueChanged);
            indicatorDashboardPreferenceClass2.listBoxControlIndicators.MouseDoubleClick += new MouseEventHandler(listBoxControlIndicators_MouseDoubleClick);
            indicatorDashboardPreferenceClass2.listBoxControlIndicators.ItemCheck += new DevExpress.XtraEditors.Controls.ItemCheckEventHandler(listBoxControlIndicators_ItemCheck);
            indicatorDashboardPreferenceClass2.listBoxControlIndicators.ItemChecking += new ItemCheckingEventHandler(listBoxControlIndicators_ItemChecking);
            indicatorDashboardPreferenceClass2.listBoxControlIndicators.SelectedValueChanged += new EventHandler(listBoxControlIndicators_SelectedValueChanged);

            indicatorDashboardPreferenceClass3.comboBoxEditIndicatorType.SelectedValueChanged += new EventHandler(comboBoxEditIndicatorType_SelectedValueChanged);
            indicatorDashboardPreferenceClass3.listBoxControlIndicators.MouseDoubleClick += new MouseEventHandler(listBoxControlIndicators_MouseDoubleClick);
            indicatorDashboardPreferenceClass3.listBoxControlIndicators.ItemCheck += new DevExpress.XtraEditors.Controls.ItemCheckEventHandler(listBoxControlIndicators_ItemCheck);
            indicatorDashboardPreferenceClass3.listBoxControlIndicators.ItemChecking += new ItemCheckingEventHandler(listBoxControlIndicators_ItemChecking);
            indicatorDashboardPreferenceClass3.listBoxControlIndicators.SelectedValueChanged += new EventHandler(listBoxControlIndicators_SelectedValueChanged);
        }

        void listBoxControlIndicators_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            
        }

        void IndicatorDashboardPreference_SupervisionCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    try
                    {
                        if (e.Objects[0] is IndicatorClassDashboardClientData)
                        {
                            IndicatorClassDashboardClientData icd = (IndicatorClassDashboardClientData)e.Objects[0];
                            if (indicators.Contains(icd) == true)
                            {
                                indicators[indicators.IndexOf(icd)] = icd;
                                CheckedListBoxItemCollection col = CurrentIndicatorDashbardPreference.listBoxControlIndicators.Items;
                                int index = col.IndexOf(icd);
                                if (index > -1)
                                {
                                    IndicatorClassDashboardClientData itemValue =
                                        (IndicatorClassDashboardClientData)((CheckedListBoxItem)col[index]).Value;
                                    if (itemValue.ShowDashboard != icd.ShowDashboard)
                                    {
                                        MessageForm.Show(ResourceLoader.GetString2("UpdateFormIndicatorDashboardPreferenceData", icd.IndicatorName), MessageFormType.Information);
                                        bool checkState = icd.ShowDashboard == true;
                                        col[index] = new CheckedListBoxItem(icd, checkState);
                                    }
                                }
                            }
                        }
                    }
                    catch
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        void listBoxControlIndicators_ItemCheck(object sender, DevExpress.XtraEditors.Controls.ItemCheckEventArgs e)
        {
            CheckedListBoxControl clc = (CheckedListBoxControl)sender;
            if (clc.SelectedItem != null)
            {
                IndicatorClassDashboardClientData icd = (IndicatorClassDashboardClientData)((CheckedListBoxItem)clc.SelectedItem).Value;
                icd.ShowDashboard = e.State == CheckState.Checked ? true : false;
                clc.Items[clc.SelectedIndex] = new CheckedListBoxItem(icd, e.State);
				if(preferences.Contains(icd) == false)
					preferences.Add(icd);
                if (simpleButtonApply.Enabled == false)
                    simpleButtonApply.Enabled = true;
            }
        }

        void listBoxControlIndicators_ItemChecking(object sender, ItemCheckingEventArgs e)
        {
            CheckedListBoxControl clc = (CheckedListBoxControl)sender;
            if (clc.SelectedItem != null)
            {
                if (e.OldValue != e.NewValue && e.NewValue == CheckState.Checked)
                {
                    bool canBeChecked = GetCanBeChecked();
                    if (!canBeChecked)
                    {
                        MessageForm.Show(ResourceLoader.GetString2("MaxNumberIndicatorSelected"), MessageFormType.Information);
                        e.Cancel = true;
                    }
                }
            }
        }

        void listBoxControlIndicators_SelectedValueChanged(object sender, EventArgs e)
        {
            CheckedListBoxControl clc = (CheckedListBoxControl)sender;
            if (clc.SelectedItem != null)
            {
                IndicatorClassDashboardClientData icd = (IndicatorClassDashboardClientData)((CheckedListBoxItem)clc.SelectedItem).Value;
                CurrentIndicatorDashbardPreference.textBoxExIndicatorDescription.Text = icd.IndicatorDescription;
            }
        }

        void comboBoxEditIndicatorType_SelectedValueChanged(object sender, EventArgs e)
        {
            ComboBoxEdit cbe = (ComboBoxEdit)sender;
            if (cbe.SelectedItem != null)
            {
                CheckedListBoxItemCollection col = CurrentIndicatorDashbardPreference.listBoxControlIndicators.Items;
                col.Clear();
                col.BeginUpdate();
                foreach (IndicatorClassDashboardClientData item in indicators)
                {
                    if (item.IndicatorTypeFriendlyName == CurrentIndicatorDashbardPreference.comboBoxEditIndicatorType.SelectedItem.ToString() &&
                        item.ClassFriendlyName == indicatorClassSelected)
                    {
                        col.Add(item, item.ShowDashboard);
                    }
                }
                col.EndUpdate();
                CurrentIndicatorDashbardPreference.textBoxExIndicatorDescription.Text = string.Empty;
				if (simpleButtonApply.Enabled == true && preferences.Count == 0)
                    simpleButtonApply.Enabled = false;
                if (CurrentIndicatorDashbardPreference.comboBoxEditIndicatorType.SelectedIndex != -1 &&
                    CurrentIndicatorDashbardPreference.listBoxControlIndicators.Items.Count > 0)
                {
                    CurrentIndicatorDashbardPreference.listBoxControlIndicators.SelectedIndex = 0;
                }
            }
        }

        void radioGroupSupervisor_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadIndicatorTypes(radioGroupSupervisor.SelectedIndex == 0);
          
            if (radioGroupSupervisor.SelectedIndex == 0) { 
                if (this.tabbedControlGroupPreferences.TabPages.Contains(this.layoutControlGroupDepartment) == true)
                   tabbedControlGroupPreferences.RemoveTabPage(this.layoutControlGroupDepartment);
            }else{
                if (this.tabbedControlGroupPreferences.TabPages.Contains(this.layoutControlGroupDepartment) == false)
                    this.tabbedControlGroupPreferences.InsertTabPage(1, layoutControlGroupDepartment);
            }

            this.tabbedControlGroupPreferences.SelectedTabPageIndex = 0;
           
        }



        void simpleButtonApply_Click(object sender, EventArgs e)
        {
            if (simpleButtonApply.Enabled == true)
            {
                Save();
                if (((SimpleButton)sender).Name == simpleButtonApply.Name && preferences.Count == 0)
                    simpleButtonApply.Enabled = false;
            }
            if (((SimpleButton)sender).Name == simpleButtonAccept.Name)
                DialogResult = DialogResult.OK;
        }

        private void Clear()
        {
            CurrentIndicatorDashbardPreference.textBoxExIndicatorDescription.Clear();
            CurrentIndicatorDashbardPreference.listBoxControlIndicators.Items.Clear();
            CurrentIndicatorDashbardPreference.comboBoxEditIndicatorType.Properties.Items.Clear();
            CurrentIndicatorDashbardPreference.comboBoxEditIndicatorType.SelectedIndex = -1;
            //maxTypeIndicatorsSelected = 0;
        }

        private int CalculateIndicatorTypeSelected()
        {
            Dictionary<string, int> totals = new Dictionary<string, int>();
            foreach (IndicatorClassDashboardClientData item in indicators)
            {
                if (item.IndicatorTypeFriendlyName != CurrentIndicatorDashbardPreference.comboBoxEditIndicatorType.SelectedItem.ToString() &&
                    item.ClassFriendlyName == indicatorClassSelected &&
                    item.ShowDashboard == true)
                {
                    if (totals.ContainsKey(item.IndicatorTypeFriendlyName) == false)
                    {
                        totals.Add(item.IndicatorTypeFriendlyName, 1);
                    }
                    else
                    {
                        totals[item.IndicatorTypeFriendlyName] += 1;
                    }
                }
            }
            int max = 0;
            foreach (int value in totals.Values)
            {
                if (value > max)
                    max = value;
            }
            return max;
        }

        private bool GetCanBeChecked()
        {
            int callCols = 0;
            int pnCols = 0;
            int dispCols = 0;
            int currentItems = 0;

            Dictionary<string, int> totals = new Dictionary<string, int>();
            totals.Add(IndicatorTypeClientData.FirstLevel.FriendlyName, 0);
            totals.Add(IndicatorTypeClientData.Calls.FriendlyName, 0);
            totals.Add(IndicatorTypeClientData.Dispatch.FriendlyName, 0);
            foreach (IndicatorClassDashboardClientData item in indicators)
            {
                if (item.ClassFriendlyName == indicatorClassSelected &&
                    item.ShowDashboard == true &&
                    item.IndicatorTypeFriendlyName != CurrentIndicatorDashbardPreference.comboBoxEditIndicatorType.SelectedItem.ToString())
                {
                    totals[item.IndicatorTypeFriendlyName] += 1;
                    if (totals[item.IndicatorTypeFriendlyName] > 0 &&
                        totals[item.IndicatorTypeFriendlyName] <= 6)
                    {
                        if (item.IndicatorTypeFriendlyName == IndicatorTypeClientData.FirstLevel.FriendlyName)
                            pnCols = 1;
                        else if (item.IndicatorTypeFriendlyName == IndicatorTypeClientData.Calls.FriendlyName)
                            callCols = 1;
                        else if (item.IndicatorTypeFriendlyName == IndicatorTypeClientData.Dispatch.FriendlyName)
                            dispCols = 1;
                    }
                    else if (totals[item.IndicatorTypeFriendlyName] > 6 &&
                        totals[item.IndicatorTypeFriendlyName] <= 12)
                    {
                        if (item.IndicatorTypeFriendlyName == IndicatorTypeClientData.FirstLevel.FriendlyName)
                            pnCols = 2;
                        else if (item.IndicatorTypeFriendlyName == IndicatorTypeClientData.Calls.FriendlyName)
                            callCols = 2;
                        else if (item.IndicatorTypeFriendlyName == IndicatorTypeClientData.Dispatch.FriendlyName)
                            dispCols = 2;
                    }
                    else if (totals[item.IndicatorTypeFriendlyName] > 12 &&
                        totals[item.IndicatorTypeFriendlyName] <= 18)
                    {
                        if (item.IndicatorTypeFriendlyName == IndicatorTypeClientData.FirstLevel.FriendlyName)
                            pnCols = 3;
                        else if (item.IndicatorTypeFriendlyName == IndicatorTypeClientData.Calls.FriendlyName)
                            callCols = 3;
                        else if (item.IndicatorTypeFriendlyName == IndicatorTypeClientData.Dispatch.FriendlyName)
                            dispCols = 3;
                    }
                }
                else if (item.ClassFriendlyName == indicatorClassSelected &&
                    item.ShowDashboard == true &&
                    item.IndicatorTypeFriendlyName == CurrentIndicatorDashbardPreference.comboBoxEditIndicatorType.SelectedItem.ToString())
                {
                    currentItems++;
                }
            }

            int available = 0;
            if (indicatorClassSelected == IndicatorClassClientData.System.FriendlyName)
                available = MAX_NUMBER_INDICATOR - 6 * (pnCols + callCols + dispCols) - currentItems;
            else if (radioGroupSupervisor.SelectedIndex == 0)
                available = MAX_NUMBER_INDICATOR - 6 * (pnCols + callCols) - currentItems;
            else if (radioGroupSupervisor.SelectedIndex == 1)
                available = MAX_NUMBER_INDICATOR - 6 * (dispCols) - currentItems;
            
            return available > 0;

        }

        private void LoadIndicatorTypes(bool firstLevel)
        {
            Clear();
            ComboBoxItemCollection col = CurrentIndicatorDashbardPreference.comboBoxEditIndicatorType.Properties.Items;
            col.Clear();
            col.BeginUpdate();
            foreach (IndicatorClassDashboardClientData icd in indicators)
            {
                if (col.Contains(icd.IndicatorTypeFriendlyName) == false &&
                   ((firstLevel == true && (icd.IndicatorTypeName == IndicatorTypeClientData.Calls.Name || icd.IndicatorTypeName == IndicatorTypeClientData.FirstLevel.Name)) ||
                   (firstLevel == false && (icd.IndicatorTypeName == IndicatorTypeClientData.Dispatch.Name))) &&
                   ((indicatorClassSelected == IndicatorClassClientData.Department.FriendlyName && icd.IndicatorTypeName == IndicatorTypeClientData.Dispatch.Name) ||
                    (indicatorClassSelected != IndicatorClassClientData.Department.FriendlyName)))
                {
                    col.Add(icd.IndicatorTypeFriendlyName);
                }
            }
            col.EndUpdate();
            if (col.Count > 0)
            {
                CurrentIndicatorDashbardPreference.comboBoxEditIndicatorType.SelectedIndex = 0;
            }
            if (CurrentIndicatorDashbardPreference.comboBoxEditIndicatorType.SelectedIndex != -1 &&
                CurrentIndicatorDashbardPreference.listBoxControlIndicators.Items.Count > 0)
            {
                CurrentIndicatorDashbardPreference.listBoxControlIndicators.SelectedIndex = 0;
            }
        }

        private void Save()
        {
            try
            {
                BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[1] { GetType().GetMethod("Save_Help", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[0] });
                processForm.CanThrowError = true;
                processForm.ShowDialog();
            }
            catch (FaultException ex)
            {
                LoadData();
                comboBoxEditIndicatorType_SelectedValueChanged(CurrentIndicatorDashbardPreference.comboBoxEditIndicatorType, new EventArgs());
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, MessageFormType.Error);
            }
            catch (Exception ex)
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, ex);
            }
        }

        private void Save_Help()
        {
            foreach (IndicatorClassDashboardClientData item in preferences)
            {
                ServerServiceClient.GetInstance().SaveOrUpdateClientData(item);
            }
			preferences = new BindingList<IndicatorClassDashboardClientData>();
        }

        private void tabbedControlGroupPreferences_SelectedPageChanged(object sender, DevExpress.XtraLayout.LayoutTabPageChangedEventArgs e)
        {
            indicatorClassSelected = e.Page.Text;
            if (simpleButtonApply.Enabled == true && preferences.Count == 0)
                simpleButtonApply.Enabled = false;
            LoadIndicatorTypes(radioGroupSupervisor.SelectedIndex == 0);
    
        }
    }
}