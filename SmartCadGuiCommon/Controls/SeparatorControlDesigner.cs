﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.Design;
using SelectionRulesEnum = System.Windows.Forms.Design.SelectionRules;

namespace SmartCadGuiCommon.Controls
{
    /// <summary>
    /// Summary description for SeparatorControlDesigner.
    /// </summary>
    public class SeparatorControlDesigner : ControlDesigner
    {
        public override SelectionRulesEnum SelectionRules
        {
            get { return (base.SelectionRules & (~SelectionRulesEnum.BottomSizeable) & (~SelectionRulesEnum.TopSizeable)); }
        }
    }
}
