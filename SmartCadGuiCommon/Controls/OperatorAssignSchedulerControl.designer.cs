namespace SmartCadGuiCommon.Controls
{
    partial class OperatorAssignSchedulerControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OperatorAssignSchedulerControl));
            DevExpress.XtraScheduler.TimeRuler timeRuler3 = new DevExpress.XtraScheduler.TimeRuler();
            DevExpress.XtraScheduler.TimeRuler timeRuler4 = new DevExpress.XtraScheduler.TimeRuler();
            this.barManagerScheduler = new DevExpress.XtraBars.BarManager(this.components);
            this.viewNavigatorBar1 = new DevExpress.XtraScheduler.UI.ViewNavigatorBar();
            this.viewNavigatorBackwardItem1 = new DevExpress.XtraScheduler.UI.ViewNavigatorBackwardItem();
            this.viewNavigatorForwardItem1 = new DevExpress.XtraScheduler.UI.ViewNavigatorForwardItem();
            this.viewNavigatorTodayItem1 = new DevExpress.XtraScheduler.UI.ViewNavigatorTodayItem();
            this.viewNavigatorZoomInItem1 = new DevExpress.XtraScheduler.UI.ViewNavigatorZoomInItem();
            this.viewNavigatorZoomOutItem1 = new DevExpress.XtraScheduler.UI.ViewNavigatorZoomOutItem();
            this.viewSelectorBar1 = new DevExpress.XtraScheduler.UI.ViewSelectorBar();
            this.viewSelectorItem2 = new DevExpress.XtraScheduler.UI.ViewSelectorItem();
            this.viewSelectorItem3 = new DevExpress.XtraScheduler.UI.ViewSelectorItem();
            this.viewSelectorItem4 = new DevExpress.XtraScheduler.UI.ViewSelectorItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.hideContainerRight = new DevExpress.XtraBars.Docking.AutoHideContainer();
            this.dockPanelScheduler = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.layoutControlDock = new DevExpress.XtraLayout.LayoutControl();
            this.labelControlTotally = new DevExpress.XtraEditors.LabelControl();
            this.labelControlPartially = new DevExpress.XtraEditors.LabelControl();
            this.labelControlNotSupervised = new DevExpress.XtraEditors.LabelControl();
            this.dateNavigator = new DevExpress.XtraScheduler.DateNavigator();
            this.schedulerControlDefault = new DevExpress.XtraScheduler.SchedulerControl();
            this.schedulerStorageDefault = new DevExpress.XtraScheduler.SchedulerStorage(this.components);
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupLegend = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.viewSelectorItem1 = new DevExpress.XtraScheduler.UI.ViewSelectorItem();
            this.viewNavigatorScheduler = new DevExpress.XtraScheduler.UI.ViewNavigator(this.components);
            this.viewSelectorScheduler = new DevExpress.XtraScheduler.UI.ViewSelector(this.components);
            this.printingSystem = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.printableComponentLink = new DevExpress.XtraPrinting.PrintableComponentLink(this.components);

            this.layoutControlItemPerformance = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.barManagerScheduler)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.hideContainerRight.SuspendLayout();
            this.dockPanelScheduler.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlDock)).BeginInit();
            this.layoutControlDock.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateNavigator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerControlDefault)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorageDefault)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupLegend)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewNavigatorScheduler)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewSelectorScheduler)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem)).BeginInit();
            this.SuspendLayout();
            // 
            // barManagerScheduler
            // 
            this.barManagerScheduler.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.viewNavigatorBar1,
            this.viewSelectorBar1});
            this.barManagerScheduler.DockControls.Add(this.barDockControlTop);
            this.barManagerScheduler.DockControls.Add(this.barDockControlBottom);
            this.barManagerScheduler.DockControls.Add(this.barDockControlLeft);
            this.barManagerScheduler.DockControls.Add(this.barDockControlRight);
            this.barManagerScheduler.DockManager = this.dockManager1;
            this.barManagerScheduler.Form = this;
            this.barManagerScheduler.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.viewNavigatorBackwardItem1,
            this.viewNavigatorForwardItem1,
            this.viewNavigatorTodayItem1,
            this.viewNavigatorZoomInItem1,
            this.viewNavigatorZoomOutItem1,
            this.viewSelectorItem1,
            this.viewSelectorItem2,
            this.viewSelectorItem3,
            this.viewSelectorItem4});
            this.barManagerScheduler.MaxItemId = 20;
            // 
            // viewNavigatorBar1
            // 
            this.viewNavigatorBar1.DockCol = 0;
            this.viewNavigatorBar1.DockRow = 0;
            this.viewNavigatorBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.viewNavigatorBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.viewNavigatorBackwardItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.viewNavigatorForwardItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.viewNavigatorTodayItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.viewNavigatorZoomInItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.viewNavigatorZoomOutItem1)});
            this.viewNavigatorBar1.OptionsBar.AllowQuickCustomization = false;
            this.viewNavigatorBar1.OptionsBar.DrawDragBorder = false;
            // 
            // viewNavigatorBackwardItem1
            // 
            this.viewNavigatorBackwardItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("viewNavigatorBackwardItem1.Glyph")));
            this.viewNavigatorBackwardItem1.GroupIndex = 1;
            this.viewNavigatorBackwardItem1.Id = 10;
            this.viewNavigatorBackwardItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("viewNavigatorBackwardItem1.LargeGlyph")));
            this.viewNavigatorBackwardItem1.Name = "viewNavigatorBackwardItem1";
            // 
            // viewNavigatorForwardItem1
            // 
            this.viewNavigatorForwardItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("viewNavigatorForwardItem1.Glyph")));
            this.viewNavigatorForwardItem1.GroupIndex = 1;
            this.viewNavigatorForwardItem1.Id = 11;
            this.viewNavigatorForwardItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("viewNavigatorForwardItem1.LargeGlyph")));
            this.viewNavigatorForwardItem1.Name = "viewNavigatorForwardItem1";
            // 
            // viewNavigatorTodayItem1
            // 
            this.viewNavigatorTodayItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("viewNavigatorTodayItem1.Glyph")));
            this.viewNavigatorTodayItem1.GroupIndex = 1;
            this.viewNavigatorTodayItem1.Id = 12;
            this.viewNavigatorTodayItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("viewNavigatorTodayItem1.LargeGlyph")));
            this.viewNavigatorTodayItem1.Name = "viewNavigatorTodayItem1";
            // 
            // viewNavigatorZoomInItem1
            // 
            this.viewNavigatorZoomInItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("viewNavigatorZoomInItem1.Glyph")));
            this.viewNavigatorZoomInItem1.GroupIndex = 1;
            this.viewNavigatorZoomInItem1.Id = 13;
            this.viewNavigatorZoomInItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("viewNavigatorZoomInItem1.LargeGlyph")));
            this.viewNavigatorZoomInItem1.Name = "viewNavigatorZoomInItem1";
            // 
            // viewNavigatorZoomOutItem1
            // 
            this.viewNavigatorZoomOutItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("viewNavigatorZoomOutItem1.Glyph")));
            this.viewNavigatorZoomOutItem1.GroupIndex = 1;
            this.viewNavigatorZoomOutItem1.Id = 14;
            this.viewNavigatorZoomOutItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("viewNavigatorZoomOutItem1.LargeGlyph")));
            this.viewNavigatorZoomOutItem1.Name = "viewNavigatorZoomOutItem1";
            // 
            // viewSelectorBar1
            // 
            this.viewSelectorBar1.DockCol = 1;
            this.viewSelectorBar1.DockRow = 0;
            this.viewSelectorBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.viewSelectorBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.viewSelectorItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.viewSelectorItem3),
            new DevExpress.XtraBars.LinkPersistInfo(this.viewSelectorItem4)});
            this.viewSelectorBar1.Offset = 153;
            this.viewSelectorBar1.OptionsBar.AllowQuickCustomization = false;
            this.viewSelectorBar1.OptionsBar.DrawDragBorder = false;
            // 
            // viewSelectorItem2
            // 
            this.viewSelectorItem2.Glyph = ((System.Drawing.Image)(resources.GetObject("viewSelectorItem2.Glyph")));
            this.viewSelectorItem2.GroupIndex = 1;
            this.viewSelectorItem2.Id = 16;
            this.viewSelectorItem2.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("viewSelectorItem2.LargeGlyph")));
            this.viewSelectorItem2.Name = "viewSelectorItem2";
            this.viewSelectorItem2.SchedulerViewType = DevExpress.XtraScheduler.SchedulerViewType.WorkWeek;
            // 
            // viewSelectorItem3
            // 
            this.viewSelectorItem3.Glyph = ((System.Drawing.Image)(resources.GetObject("viewSelectorItem3.Glyph")));
            this.viewSelectorItem3.GroupIndex = 1;
            this.viewSelectorItem3.Id = 17;
            this.viewSelectorItem3.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("viewSelectorItem3.LargeGlyph")));
            this.viewSelectorItem3.Name = "viewSelectorItem3";
            this.viewSelectorItem3.SchedulerViewType = DevExpress.XtraScheduler.SchedulerViewType.Week;
            // 
            // viewSelectorItem4
            // 
            this.viewSelectorItem4.Checked = true;
            this.viewSelectorItem4.Glyph = ((System.Drawing.Image)(resources.GetObject("viewSelectorItem4.Glyph")));
            this.viewSelectorItem4.GroupIndex = 1;
            this.viewSelectorItem4.Id = 18;
            this.viewSelectorItem4.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("viewSelectorItem4.LargeGlyph")));
            this.viewSelectorItem4.Name = "viewSelectorItem4";
            this.viewSelectorItem4.SchedulerViewType = DevExpress.XtraScheduler.SchedulerViewType.Month;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(872, 31);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 700);
            this.barDockControlBottom.Size = new System.Drawing.Size(872, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 31);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 669);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(872, 31);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 669);
            // 
            // dockManager1
            // 
            this.dockManager1.AutoHideContainers.AddRange(new DevExpress.XtraBars.Docking.AutoHideContainer[] {
            this.hideContainerRight});
            this.dockManager1.Form = this;
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // hideContainerRight
            // 
            this.hideContainerRight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.hideContainerRight.Controls.Add(this.dockPanelScheduler);
            this.hideContainerRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.hideContainerRight.Location = new System.Drawing.Point(853, 31);
            this.hideContainerRight.Name = "hideContainerRight";
            this.hideContainerRight.Size = new System.Drawing.Size(19, 669);
            // 
            // dockPanelScheduler
            // 
            this.dockPanelScheduler.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.dockPanelScheduler.Appearance.Options.UseBackColor = true;
            this.dockPanelScheduler.Controls.Add(this.dockPanel1_Container);
            this.dockPanelScheduler.Dock = DevExpress.XtraBars.Docking.DockingStyle.Right;
            this.dockPanelScheduler.ID = new System.Guid("bbe7d245-c2d9-42c5-9777-3c4e2c67b3d0");
            this.dockPanelScheduler.Location = new System.Drawing.Point(0, 0);
            this.dockPanelScheduler.Name = "dockPanelScheduler";
            this.dockPanelScheduler.Options.AllowDockBottom = false;
            this.dockPanelScheduler.Options.AllowDockFill = false;
            this.dockPanelScheduler.Options.AllowDockTop = false;
            this.dockPanelScheduler.Options.AllowFloating = false;
            this.dockPanelScheduler.Options.FloatOnDblClick = false;
            this.dockPanelScheduler.Options.ShowCloseButton = false;
            this.dockPanelScheduler.Options.ShowMaximizeButton = false;
            this.dockPanelScheduler.OriginalSize = new System.Drawing.Size(200, 200);
            this.dockPanelScheduler.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Right;
            this.dockPanelScheduler.SavedIndex = 0;
            this.dockPanelScheduler.Size = new System.Drawing.Size(200, 669);
            this.dockPanelScheduler.Text = "Calendario";
            this.dockPanelScheduler.Visibility = DevExpress.XtraBars.Docking.DockVisibility.AutoHide;
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.layoutControlDock);
            this.dockPanel1_Container.Location = new System.Drawing.Point(4, 23);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(192, 642);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // layoutControlDock
            // 
            this.layoutControlDock.Controls.Add(this.labelControlTotally);
            this.layoutControlDock.Controls.Add(this.labelControlPartially);
            this.layoutControlDock.Controls.Add(this.labelControlNotSupervised);
            this.layoutControlDock.Controls.Add(this.dateNavigator);
            this.layoutControlDock.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlDock.Location = new System.Drawing.Point(0, 0);
            this.layoutControlDock.Name = "layoutControlDock";
            this.layoutControlDock.Root = this.layoutControlGroup1;
            this.layoutControlDock.Size = new System.Drawing.Size(192, 642);
            this.layoutControlDock.TabIndex = 0;
            this.layoutControlDock.Text = "layoutControl1";
            // 
            // labelControlTotally
            // 
            this.labelControlTotally.Location = new System.Drawing.Point(24, 590);
            this.labelControlTotally.Name = "labelControlTotally";
            this.labelControlTotally.Size = new System.Drawing.Size(63, 13);
            this.labelControlTotally.StyleController = this.layoutControlDock;
            this.labelControlTotally.TabIndex = 9;
            this.labelControlTotally.Text = "labelControl3";
            // 
            // labelControlPartially
            // 
            this.labelControlPartially.Location = new System.Drawing.Point(24, 573);
            this.labelControlPartially.Name = "labelControlPartially";
            this.labelControlPartially.Size = new System.Drawing.Size(63, 13);
            this.labelControlPartially.StyleController = this.layoutControlDock;
            this.labelControlPartially.TabIndex = 8;
            this.labelControlPartially.Text = "labelControl2";
            // 
            // labelControlNotSupervised
            // 
            this.labelControlNotSupervised.Location = new System.Drawing.Point(24, 556);
            this.labelControlNotSupervised.Name = "labelControlNotSupervised";
            this.labelControlNotSupervised.Size = new System.Drawing.Size(63, 13);
            this.labelControlNotSupervised.StyleController = this.layoutControlDock;
            this.labelControlNotSupervised.TabIndex = 7;
            this.labelControlNotSupervised.Text = "labelControl1";
            // 
            // dateNavigator
            // 
            this.dateNavigator.HotDate = null;
            this.dateNavigator.Location = new System.Drawing.Point(12, 12);
            this.dateNavigator.Name = "dateNavigator";
            this.dateNavigator.SchedulerControl = this.schedulerControlDefault;
            this.dateNavigator.Size = new System.Drawing.Size(168, 503);
            this.dateNavigator.TabIndex = 6;
            // 
            // schedulerControlDefault
            // 
            this.schedulerControlDefault.ActiveViewType = DevExpress.XtraScheduler.SchedulerViewType.Month;
            this.schedulerControlDefault.Dock = System.Windows.Forms.DockStyle.Fill;
            this.schedulerControlDefault.Location = new System.Drawing.Point(0, 31);
            this.schedulerControlDefault.MenuManager = this.barManagerScheduler;
            this.schedulerControlDefault.Name = "schedulerControlDefault";
            this.schedulerControlDefault.OptionsCustomization.AllowAppointmentDelete = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControlDefault.OptionsCustomization.AllowAppointmentDrag = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControlDefault.OptionsCustomization.AllowAppointmentDragBetweenResources = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControlDefault.OptionsCustomization.AllowAppointmentMultiSelect = false;
            this.schedulerControlDefault.OptionsCustomization.AllowAppointmentResize = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControlDefault.OptionsCustomization.AllowInplaceEditor = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControlDefault.Size = new System.Drawing.Size(853, 669);
            this.schedulerControlDefault.Start = new System.DateTime(2009, 7, 13, 0, 0, 0, 0);
            this.schedulerControlDefault.Storage = this.schedulerStorageDefault;
            this.schedulerControlDefault.TabIndex = 4;
            this.schedulerControlDefault.Text = "schedulerControl1";
            this.schedulerControlDefault.Views.DayView.TimeRulers.Add(timeRuler3);
            this.schedulerControlDefault.Views.WorkWeekView.TimeRulers.Add(timeRuler4);
            this.schedulerControlDefault.SelectionChanged += new System.EventHandler(this.schedulerControlDefault_SelectionChanged);
            this.schedulerControlDefault.InitAppointmentDisplayText += new DevExpress.XtraScheduler.AppointmentDisplayTextEventHandler(this.schedulerControlDefault_InitAppointmentDisplayText);
            this.schedulerControlDefault.PreparePopupMenu += new DevExpress.XtraScheduler.PreparePopupMenuEventHandler(this.schedulerControlDefault_PreparePopupMenu);
            this.schedulerControlDefault.EditAppointmentFormShowing += new DevExpress.XtraScheduler.AppointmentFormEventHandler(this.schedulerControlDefault_EditAppointmentFormShowing);
            // 
            // schedulerStorageDefault
            // 
            this.schedulerStorageDefault.Appointments.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentCustomFieldMapping("OperatorCode", "", DevExpress.XtraScheduler.FieldValueType.Integer));
            this.schedulerStorageDefault.Appointments.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentCustomFieldMapping("WorkShiftCode", "", DevExpress.XtraScheduler.FieldValueType.Integer));
            this.schedulerStorageDefault.Appointments.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentCustomFieldMapping("ScheduleCode", "", DevExpress.XtraScheduler.FieldValueType.Integer));
            this.schedulerStorageDefault.Appointments.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentCustomFieldMapping("IsSupervisor", "", DevExpress.XtraScheduler.FieldValueType.Boolean));
            this.schedulerStorageDefault.Appointments.Labels.Add(new DevExpress.XtraScheduler.AppointmentLabel(System.Drawing.Color.Wheat, "None", "&None"));
            this.schedulerStorageDefault.Appointments.Labels.Add(new DevExpress.XtraScheduler.AppointmentLabel(System.Drawing.Color.LightBlue, "Partially", "&Partially"));
            this.schedulerStorageDefault.Appointments.Labels.Add(new DevExpress.XtraScheduler.AppointmentLabel(System.Drawing.Color.LightGreen, "Totally", "&Totally"));
            this.schedulerStorageDefault.Appointments.Statuses.Add(new DevExpress.XtraScheduler.AppointmentStatus(DevExpress.XtraScheduler.AppointmentStatusType.Free, System.Drawing.Color.CornflowerBlue, "WorkShift", "&WorkShift"));
            this.schedulerStorageDefault.Appointments.Statuses.Add(new DevExpress.XtraScheduler.AppointmentStatus(DevExpress.XtraScheduler.AppointmentStatusType.Tentative, System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(204)))), ((int)(((byte)(111))))), "ExtraTime", "&ExtraTime"));
            this.schedulerStorageDefault.FilterAppointment += new DevExpress.XtraScheduler.PersistentObjectCancelEventHandler(this.schedulerStorageDefault_FilterAppointment);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlGroupLegend,
            this.splitterItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(192, 642);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dateNavigator;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(172, 507);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroupLegend
            // 
            this.layoutControlGroupLegend.CustomizationFormText = "layoutControlGroupLegend";
            this.layoutControlGroupLegend.ExpandButtonMode = DevExpress.Utils.Controls.ExpandButtonMode.Inverted;
            this.layoutControlGroupLegend.ExpandButtonVisible = true;
            this.layoutControlGroupLegend.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.emptySpaceItem1});
            this.layoutControlGroupLegend.Location = new System.Drawing.Point(0, 512);
            this.layoutControlGroupLegend.Name = "layoutControlGroupLegend";
            this.layoutControlGroupLegend.Size = new System.Drawing.Size(172, 110);
            this.layoutControlGroupLegend.Text = "layoutControlGroupLegend";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.labelControlNotSupervised;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(148, 17);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.labelControlPartially;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 17);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(148, 17);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.labelControlTotally;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 34);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(148, 17);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 51);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(148, 15);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.CustomizationFormText = "splitterItem1";
            this.splitterItem1.Location = new System.Drawing.Point(0, 507);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(172, 5);
            // 
            // viewSelectorItem1
            // 
            this.viewSelectorItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("viewSelectorItem1.Glyph")));
            this.viewSelectorItem1.GroupIndex = 1;
            this.viewSelectorItem1.Id = 15;
            this.viewSelectorItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("viewSelectorItem1.LargeGlyph")));
            this.viewSelectorItem1.Name = "viewSelectorItem1";
            this.viewSelectorItem1.SchedulerViewType = DevExpress.XtraScheduler.SchedulerViewType.Day;
            // 
            // viewNavigatorScheduler
            // 
            this.viewNavigatorScheduler.BarManager = this.barManagerScheduler;
            this.viewNavigatorScheduler.SchedulerControl = this.schedulerControlDefault;
            // 
            // viewSelectorScheduler
            // 
            this.viewSelectorScheduler.BarManager = this.barManagerScheduler;
            this.viewSelectorScheduler.SchedulerControl = this.schedulerControlDefault;
            // 
            // printingSystem
            // 
            this.printingSystem.Links.AddRange(new object[] {
            this.printableComponentLink});
            // 
            // printableComponentLink
            // 
            this.printableComponentLink.Component = this.schedulerControlDefault;
            // 
            // 
            // 
            this.printableComponentLink.ImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("printableComponentLink1.ImageCollection.ImageStream")));
            this.printableComponentLink.PrintingSystem = this.printingSystem;
            this.printableComponentLink.PrintingSystemBase = this.printingSystem;
            // 
            // OperatorAssignSchedulerControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.schedulerControlDefault);
            this.Controls.Add(this.hideContainerRight);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "OperatorAssignSchedulerControl";
            this.Size = new System.Drawing.Size(872, 700);
            ((System.ComponentModel.ISupportInitialize)(this.barManagerScheduler)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.hideContainerRight.ResumeLayout(false);
            this.dockPanelScheduler.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlDock)).EndInit();
            this.layoutControlDock.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateNavigator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerControlDefault)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorageDefault)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupLegend)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewNavigatorScheduler)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewSelectorScheduler)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManagerScheduler;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraScheduler.UI.ViewNavigator viewNavigatorScheduler;
        private DevExpress.XtraScheduler.UI.ViewSelector viewSelectorScheduler;
        private DevExpress.XtraScheduler.UI.ViewNavigatorBar viewNavigatorBar1;
        private DevExpress.XtraScheduler.UI.ViewNavigatorBackwardItem viewNavigatorBackwardItem1;
        private DevExpress.XtraScheduler.UI.ViewNavigatorForwardItem viewNavigatorForwardItem1;
        private DevExpress.XtraScheduler.UI.ViewNavigatorTodayItem viewNavigatorTodayItem1;
        private DevExpress.XtraScheduler.UI.ViewNavigatorZoomInItem viewNavigatorZoomInItem1;
        private DevExpress.XtraScheduler.UI.ViewNavigatorZoomOutItem viewNavigatorZoomOutItem1;
        private DevExpress.XtraScheduler.UI.ViewSelectorBar viewSelectorBar1;
        private DevExpress.XtraScheduler.UI.ViewSelectorItem viewSelectorItem1;
        private DevExpress.XtraScheduler.UI.ViewSelectorItem viewSelectorItem2;
        private DevExpress.XtraScheduler.UI.ViewSelectorItem viewSelectorItem3;
        private DevExpress.XtraScheduler.UI.ViewSelectorItem viewSelectorItem4;
        public DevExpress.XtraScheduler.SchedulerControl schedulerControlDefault;
        private DevExpress.XtraScheduler.SchedulerStorage schedulerStorageDefault;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelScheduler;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraLayout.LayoutControl layoutControlDock;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraScheduler.DateNavigator dateNavigator;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.LabelControl labelControlTotally;
        private DevExpress.XtraEditors.LabelControl labelControlPartially;
        private DevExpress.XtraEditors.LabelControl labelControlNotSupervised;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupLegend;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraBars.Docking.AutoHideContainer hideContainerRight;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem;
        public DevExpress.XtraPrinting.PrintableComponentLink printableComponentLink;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemPerformance;
    }
}
