﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraLayout;
using System.ComponentModel;
using DevExpress.XtraEditors;
using System.Drawing;
using SmartCadGuiCommon.SyncBoxes;
using SmartCadCore.Common;


namespace SmartCadGuiCommon.Controls
{
    public class TextBoxExProperties : ControlExProperties
    {
        public TextBoxExProperties(Control con,LayoutControlItem ite, GridAnswerData gdata)
            :base(con, ite, gdata)
        {
        }

        public TextBoxExProperties(Control con, LayoutControlItem ite)
            : base(con, ite)
        {
            ((TextBox)Control).MaxLength = 255;
        }

        [MyDisplayNameAttribute("PropertyGrid.Multiline")]
        [BrowsableAttribute(true)]
        public bool Multiline
        {
            get { return ((TextBox)Control).Multiline; }
            set 
            {
                if (value == true)
                {
                    this.LayoutItem.SizeConstraintsType = SizeConstraintsType.Custom;
                    this.LayoutItem.MaxSize = new Size(0, 0);
                }   
                else
                {
                    this.LayoutItem.SizeConstraintsType = SizeConstraintsType.Default;
                    
                }
                this.LayoutItem.Update();
                ((TextBox)Control).Multiline = value; 
            }
        }
    }

    public class NumericProperties : ControlExProperties
    {
        public NumericProperties(Control con, LayoutControlItem ite, GridAnswerData gdata)
            : base(con, ite, gdata)
        {
        }

        public NumericProperties(Control con, LayoutControlItem ite)
            : base(con, ite)
        {
        }

        [MyDisplayNameAttribute("Minimum")]
        [BrowsableAttribute(true)]
        public decimal Minimum
        {
            get { return ((SpinEdit)Control).Properties.MinValue; }
            set
            {
                ((SpinEdit)Control).Properties.MinValue = value;
            }
        }

        [MyDisplayNameAttribute("Maximum")]
        [BrowsableAttribute(true)]
        public decimal Maximum
        {
            get { return ((SpinEdit)Control).Properties.MaxValue; }
            set
            {
                ((SpinEdit)Control).Properties.MaxValue = value;
            }
        }

        [MyDisplayNameAttribute("PropertyGrid.Text")]
        [BrowsableAttribute(true)]
        public bool ShowText
        {
            get
            {
                return this.LayoutItem.TextVisible;
            }
            set
            {
                LayoutItem.TextVisible = value;

            }
        }
    }

    public class ControlExProperties
    {
        [BrowsableAttribute(false)]
        public GridAnswerData GridData { get; set; }

        public ControlExProperties(Control con, LayoutControlItem ite, GridAnswerData gdata)
        {
            this.Control = con;
            this.LayoutItem = ite;
            this.GridData = gdata;
        }

        public ControlExProperties(Control con, LayoutControlItem ite)
        {
            this.Control = con;
            this.LayoutItem = ite;
        }

        [BrowsableAttribute(false)]
        public Control Control { get; set; }

        [BrowsableAttribute(false)]
        public LayoutControlItem LayoutItem { get; set; }

        [MyDisplayNameAttribute("PropertyGrid.Text")]
        [BrowsableAttribute(true)]
        public string Text 
        {
            get
            {
                return this.LayoutItem.Text;
            }
            set
            {
                if (value == string.Empty)
                    this.LayoutItem.Text = string.Empty;
                else
                    this.LayoutItem.Text = value;
                
                if (GridData != null)
                    this.GridData.Description = value;
            }
        }       

        public static ControlExProperties GetProperties(LayoutControlItem item)
        {
            ControlExProperties properties = null;
            if (item.Control is TextBox)
                properties = new TextBoxExProperties(item.Control, item);
            else if (item.Control is SpinEdit)
                properties = new NumericProperties(item.Control, item);
            else
                properties = new ControlExProperties(item.Control, item);
            return properties;
        }
    }

    public class MyDisplayNameAttribute : DisplayNameAttribute
    {
        public MyDisplayNameAttribute(string key) : base(ResourceLoader.GetString2(key)) { }
    }

}
