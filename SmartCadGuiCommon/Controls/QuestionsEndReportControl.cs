using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraLayout;
using System.IO;
using System.Xml;
using System.Collections;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadControls.Controls;

namespace SmartCadGuiCommon.Controls
{
    public partial class QuestionsEndReportControl : DevExpress.XtraEditors.XtraUserControl
    {
        private DispatchReportClientData report;
        private IList questions = new ArrayList();
        private bool onlyRequired;
        private int controlMaxHeight = 0;

        public int ControlMaxHeight 
        {
            get { return controlMaxHeight; }
            set { controlMaxHeight = value; } 
        }

        public QuestionsEndReportControl()
        {
            InitializeComponent();
        }
        
        public QuestionsEndReportControl(IList questions, bool onlyRequired)
            : this()
        {
            this.report = null;
            this.questions = questions;
            this.onlyRequired = onlyRequired;
        }

        public QuestionsEndReportControl(IList questions, DispatchReportClientData report)
            : this()
        {
            this.report = report;
            this.questions = questions;
        }

        private void QuestionsEndReportControl_Load(object sender, EventArgs e)
        {
            PaintQuestions();
        }

        private void PaintQuestions()
        {
            LayoutControl layoutControl;
            LayoutControlItem item;
            layoutControlQuestionEndReport.BeginUpdate();

            if (report != null)
            {
                int lengthToCut = 4;
                layoutControlGroupQuestionEndReport.Text = ResourceLoader.GetString2("IncidentData") + " -> ";
                foreach (IncidentTypeClientData incType in report.IncidentTypes)
                {
                    layoutControlGroupQuestionEndReport.Text += incType.CustomCode + ", ";
                    lengthToCut = 2;
                }
                layoutControlGroupQuestionEndReport.Text = layoutControlGroupQuestionEndReport.Text.Substring(0, layoutControlGroupQuestionEndReport.Text.Length - lengthToCut);
            }
            else
            {
                if (onlyRequired == false)
                    layoutControlGroupQuestionEndReport.Text = ResourceLoader.GetString2("IncidentCommonData");
                else
                    layoutControlGroupQuestionEndReport.Text = ResourceLoader.GetString2("IncidentData");
            } 

            foreach (DispatchReportQuestionDispatchReportClientData reportQuestion in questions)
            {
                layoutControl = BuildLayoutControl();
                item = (LayoutControlItem)layoutControlQuestionEndReport.CreateLayoutItem(layoutControlGroupQuestionEndReport);

                item.AppearanceItemCaption.Font = new Font(item.AppearanceItemCaption.Font, FontStyle.Bold);
                if (reportQuestion.QuestionRequired == true)
                    item.Text = reportQuestion.QuestionText + " *";
                else
                    item.Text = reportQuestion.QuestionText;
                
                item.TextLocation = DevExpress.Utils.Locations.Top;
                item.Control = layoutControl;
                item.TextAlignMode = TextAlignModeItem.CustomSize;
                item.TextToControlDistance = 0;
                item.SizeConstraintsType = SizeConstraintsType.Default;
                MemoryStream ms = GetControlsInXML(reportQuestion.QuestionRender, layoutControl, reportQuestion.QuestionRequired);
                layoutControl.RestoreLayoutFromStream(ms);
                layoutControl.MaximumSize = CalcMaxSize(layoutControl);
                layoutControlGroupQuestionEndReport.AddItem(item);

                if (questions.Count == 1)
                ControlMaxHeight += layoutControl.MaximumSize.Height;
                else
                ControlMaxHeight += layoutControl.MaximumSize.Height;
               
            }
            layoutControlQuestionEndReport.EndUpdate();

        }

        private LayoutControl BuildLayoutControl() 
        {
            LayoutControl layout = new LayoutControl();
            layout.BeginInit();
            LayoutControlGroup group = new LayoutControlGroup();
            group.BeginInit();
            layout.AutoScroll = true;
            layout.Appearance.ControlDisabled.BackColor = System.Drawing.Color.White;
            layout.Appearance.ControlDisabled.Options.UseBackColor = true;
            layout.Appearance.DisabledLayoutGroupCaption.ForeColor = System.Drawing.Color.White;
            layout.Appearance.DisabledLayoutGroupCaption.Options.UseForeColor = true;
            layout.Appearance.DisabledLayoutItem.ForeColor = System.Drawing.Color.Black;
            layout.Appearance.DisabledLayoutItem.Options.UseForeColor = true;
            //layout.MinimumSize = new Size(0, 20);
            layout.AllowCustomizationMenu = false;
            group.GroupBordersVisible = true;
            layout.Root = group;
            group.EndInit();
            layout.BestFit();
            layout.EndInit();
            
            return layout;
        
        }
        private int i = 0;
        private Size CalcMaxSize(LayoutControl layout)
        {
            Size size = new Size();
            Dictionary<int, int> controlsByTop = new Dictionary<int, int>();
            foreach (Control c in layout.Controls)
            {
                int h = 0;
                if (c is TextBox && ((TextBox)c).Multiline == true)
                {
                    h = 150;
                    i++;
                }
                else if (c is TextBox || c is SpinEdit || c is CheckEdit)
                {
                    h = c.Height + 25;
                    i++;
                }
                else if (c is RadioButton)
                {
                    h = c.Height + 35;
                }
                if (h > 0)
                {
                    if (controlsByTop.ContainsKey(c.Top) == true)
                        controlsByTop[c.Top] = (Math.Max(controlsByTop[c.Top], h));
                    else
                        controlsByTop.Add(c.Top, h);
                }
            }
            foreach (int h in controlsByTop.Values)
                size.Height += h;
            
            return size;
        }

        public event ControlValue_Changed ControlValueChanged;

        public delegate void ControlValue_Changed(object sender, EventArgs e);

        private MemoryStream GetControlsInXML(string render, LayoutControl layout, bool isRequired)
        {
            byte[] byteArray = Encoding.UTF8.GetBytes(render);
            MemoryStream ms = new MemoryStream(byteArray);
            ms.Position = 0;
            XmlDocument doc = new XmlDocument();
            doc.Load(ms);
            XmlNode firstChild = doc.FirstChild;
            foreach (XmlNode node in firstChild.ChildNodes)
            {
                if (node.Name == "Control")
                {
                    Control control = (Control)Activator.CreateInstance(Type.GetType(node.Attributes["type"].Value));
                    control.Name = node.Attributes["name"].Value;
                    control.Parent = this;
                    control.Tag = node.Attributes["itemName"].Value;
                    control.Text = "";
                    if (control is TextBoxEx)
                    {
                        if (node.Attributes["multiline"] != null)
                        {
                            (control as TextBoxEx).Multiline = bool.Parse(node.Attributes["multiline"].Value);
                            (control as TextBoxEx).AcceptsReturn = (control as TextBoxEx).Multiline;
                            (control as TextBoxEx).ScrollBars = ScrollBars.Vertical;
                        }
                        (control as TextBoxEx).ContextMenu = null;
                        (control as TextBoxEx).TextChanged += new EventHandler(ControlValueChanged);
                    }
                    else if (control is SpinEdit)
                    {
                        if (node.Attributes["minimum"] != null)
                            (control as SpinEdit).Properties.MinValue = decimal.Parse(node.Attributes["minimum"].Value);

                        if (node.Attributes["maximum"] != null)
                            (control as SpinEdit).Properties.MaxValue = decimal.Parse(node.Attributes["maximum"].Value);

                        (control as SpinEdit).ValueChanged += new EventHandler(ControlValueChanged);
                    }
                    else if (control is RadioButton)
                    {
                        (control as RadioButton).CheckedChanged += new EventHandler(ControlValueChanged);
                    }
                    else if (control is CheckEdit)
                    {
                        (control as CheckEdit).CheckedChanged += new EventHandler(ControlValueChanged);
                    }
                    layout.Controls.Add(control);

                    if (reportControls.ContainsKey(control.Name) == false)
                    {
                        if (control is RadioButton || control is CheckEdit)
                            reportControls.Add(control.Name, false.ToString());
                        else
                            reportControls.Add(control.Name, string.Empty);
                    }
                    if (isRequired == true)
                        requiredControls.Add(control.Name);
                }
            }
            ms.Position = 0;
            return ms;
        }

        private IList requiredControls = new ArrayList();

        public IList RequiredControls
        {
            get
            {
                return requiredControls;
            }
            set
            { 
                requiredControls = value; 
            }
        }

        private Dictionary<string, string> reportControls = new Dictionary<string, string>();

        public Dictionary<string, string> ReportControls
        {
            get
            {
                return reportControls;
            }
            
        }

        //public Dictionary<string, string> GetReportControlWithAnswers() 
        //{
        //    Dictionary<string, string> result = new Dictionary<string, string>();

        //    foreach (Control layout in layoutControlQuestionEndReport.Controls)
        //    {
        //        if (layout is LayoutControl) {

        //            LayoutControl layoutControl = layout as LayoutControl;

        //            foreach (Control control in layoutControl.Controls)
        //            {
        //                if (control is TextBoxEx)
        //                    result.Add(control.Name, control.Text);
        //                else if (control is RadioButton)
        //                    result.Add(control.Name, ((RadioButton)control).Checked.ToString());
        //                else if (control is SpinEdit)
        //                    result.Add(control.Name, ((SpinEdit)control).Value.ToString());
        //                else if (control is CheckEdit)
        //                    result.Add(control.Name, ((CheckEdit)control).Checked.ToString());
        //            }

                
        //        }
                
        //    }

        //    return result;
        //}


        public bool SetAnswerToControl(string controlName, string answer)
        {
            foreach (Control layout in layoutControlQuestionEndReport.Controls)
            {
                if (layout is LayoutControl)
                {
                    LayoutControl layoutControl = layout as LayoutControl;

                    foreach (Control control in layoutControl.Controls)
                    {
                        if (control.Name == controlName)
                        {
                            if (control is TextBoxEx)
                                control.Text = answer;
                            else if (control is RadioButton)
                                ((RadioButton)control).Checked = bool.Parse(answer);
                            else if (control is SpinEdit)
                                ((SpinEdit)control).Value = decimal.Parse(answer);
                            else if (control is CheckEdit)
                                ((CheckEdit)control).Checked = bool.Parse(answer);

                            return true;

                        }
                    }
                }
            }

            return false;
        }

        public IList GetReportAnswers()
        {
            IList result = new ArrayList();
            EndingReportAnswerClientData reportAnswer;
            foreach (BaseLayoutItem baseLayoutItemParent in layoutControlQuestionEndReport.Items)
            {
                if (baseLayoutItemParent is LayoutControlItem)
                {
                    LayoutControlItem itemParent = (LayoutControlItem)baseLayoutItemParent;

                    if (itemParent.Control != null && itemParent.Control is LayoutControl)
                    {
                        LayoutControl layout = itemParent.Control as LayoutControl;

                        foreach (BaseLayoutItem baseLayoutItem in layout.Items)
                        {
                            if (baseLayoutItem is LayoutControlItem)
                            {
                                LayoutControlItem item = (LayoutControlItem)baseLayoutItem;
                                if (item.Control != null)
                                {
                                    reportAnswer = new EndingReportAnswerClientData();
                                    reportAnswer.QuestionText = itemParent.Text.Replace('*', ' ').Trim();
                                    reportAnswer.AnswerHeaderText = item.Text.Trim();

                                    if (item.Control is TextBoxEx && item.Control.Text.Trim() != string.Empty)
                                        reportAnswer.AnswerText = item.Control.Text.Trim();
                                    else if (item.Control is RadioButton && ((RadioButton)item.Control).Checked == true)
                                        reportAnswer.AnswerText = item.Text.Trim();
                                    else if (item.Control is SpinEdit)
                                        reportAnswer.AnswerText = ((SpinEdit)item.Control).Value.ToString();
                                    else if (item.Control is CheckEdit && ((CheckEdit)item.Control).Checked == true)
                                        reportAnswer.AnswerText = item.Text.Trim();

                                    if (reportAnswer.AnswerText != string.Empty && reportAnswer.AnswerText != null)
                                        result.Add(reportAnswer);
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }

        

    }


    public class ReportQuestionAnswerData 
    {
        private string controlName;
        private string questionText;
        private string answerText;

        public ReportQuestionAnswerData()
        {            
        }

        public string ControlName
        {
            get
            {
                return controlName;
            }
            set {
                controlName = value;
            }
        }

        public string QuestionText
        {
            get
            {
                return questionText;
            }
            set
            {
                questionText = value;

            }
        }

        public string AnswerText
        {
            get
            {
                return answerText;
            }
            set
            {
                answerText = value;
            }
        }
      
    }
}
