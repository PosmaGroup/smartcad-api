using System;
using System.Reflection;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.ServiceModel;
using System.Net.Sockets;

using Microsoft.Win32;

using SmartCadCore.Core;
using Smartmatic.SmartCad.Service;
using System.Net;
using SmartCadCore.Model;
using SmartCadCore.ClientData;
using DevExpress.XtraEditors;
using SmartCadCore.Common;
using SmartCadControls.Controls;

namespace SmartCadGuiCommon
{
    public class LoginForm : XtraForm
    {
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Panel panel;
        private SimpleButtonEx buttonOk;
        private SimpleButtonEx buttonCancel;
        private TextBoxEx textBoxPassword;
        private TextBoxEx textBoxLogin;
        private LabelEx labelPassword;
        private LabelEx labelExLogin;
        private new System.ComponentModel.IContainer components;
        private System.Windows.Forms.ToolTip toolTip;
        private UserApplicationClientData application;
        private NetworkCredential networkCredential;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel;
        private string plainPassword;

        public string Password
        {
            get
            {
                return this.plainPassword;
            }
        }

        public LoginForm(UserApplicationClientData application)
        {
            InitializeComponent();

            //textBoxExExtNumber.Enabled = (application == UserApplicationClientData.FirstLevel);

            HandleCreated += new EventHandler(LoginForm_HandleCreated);

            timerFocus = new System.Timers.Timer(300);
            timerFocus.AutoReset = true;
            timerFocus.Elapsed += new System.Timers.ElapsedEventHandler(timerFocus_Elapsed);
            this.application = application;
            LoadLanguage();
        }

        private void LoadLanguage()
        {
            pictureBox.Image = ResourceLoader.GetImage("$Image.LoginLogo");
            labelExLogin.Text = ResourceLoader.GetString2("$Message.Login") + ":";
            //labelExExtNumber.Text = ResourceLoader.GetString2("$Message.Extension") + ":";
            labelPassword.Text = ResourceLoader.GetString2("$Message.Password") + ":";
            buttonOk.Text = ResourceLoader.GetString2("Accept");
            buttonCancel.Text = ResourceLoader.GetString2("Cancel");
            Text =/* ResourceLoader.GetString2(this.Name) + " " + */application.FriendlyName;
        }

        void LoginForm_HandleCreated(object sender, EventArgs e)
        {
            Win32.SetForegroundWindow(Handle);
        }

        /// <summary>
        /// Limpiar los recursos que se esten utilizando.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Metodo necesario para admitir el Disenador, no se puede modificar
        /// el contenido del metodo con el editor de codigo.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginForm));
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.panel = new System.Windows.Forms.Panel();
            this.buttonOk = new SimpleButtonEx();
            this.buttonCancel = new SimpleButtonEx();
            this.textBoxPassword = new TextBoxEx();
            this.textBoxLogin = new TextBoxEx();
            this.labelPassword = new LabelEx();
            this.labelExLogin = new LabelEx();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.defaultLookAndFeel = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox
            // 
            this.pictureBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox.Location = new System.Drawing.Point(0, 0);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(330, 121);
            this.pictureBox.TabIndex = 0;
            this.pictureBox.TabStop = false;
            // 
            // panel
            // 
            this.panel.Controls.Add(this.buttonOk);
            this.panel.Controls.Add(this.buttonCancel);
            this.panel.Controls.Add(this.textBoxPassword);
            this.panel.Controls.Add(this.textBoxLogin);
            this.panel.Controls.Add(this.labelPassword);
            this.panel.Controls.Add(this.labelExLogin);
            this.panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel.Location = new System.Drawing.Point(0, 121);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(330, 118);
            this.panel.TabIndex = 0;
            // 
            // buttonOk
            // 
            this.buttonOk.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOk.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonOk.Appearance.Options.UseFont = true;
            this.buttonOk.Appearance.Options.UseForeColor = true;
            this.buttonOk.Enabled = false;
            this.buttonOk.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonOk.Location = new System.Drawing.Point(164, 80);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(75, 25);
            this.buttonOk.TabIndex = 6;
            this.buttonOk.Text = "Accept";
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCancel.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonCancel.Appearance.Options.UseFont = true;
            this.buttonCancel.Appearance.Options.UseForeColor = true;
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonCancel.Location = new System.Drawing.Point(245, 80);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 25);
            this.buttonCancel.TabIndex = 7;
            this.buttonCancel.Text = "Cancelar";
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.AllowDrop = true;
            this.textBoxPassword.AllowsLetters = true;
            this.textBoxPassword.AllowsNumbers = true;
            this.textBoxPassword.AllowsPunctuation = true;
            this.textBoxPassword.AllowsSeparators = true;
            this.textBoxPassword.AllowsSymbols = true;
            this.textBoxPassword.AllowsWhiteSpaces = false;
            this.textBoxPassword.ExtraAllowedChars = "";
            this.textBoxPassword.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxPassword.Location = new System.Drawing.Point(116, 47);
            this.textBoxPassword.MaxLength = 50;
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.NonAllowedCharacters = "\'";
            this.textBoxPassword.PasswordChar = '*';
            this.textBoxPassword.RegularExpresion = "";
            this.textBoxPassword.Size = new System.Drawing.Size(204, 20);
            this.textBoxPassword.TabIndex = 5;
            this.textBoxPassword.TextChanged += new System.EventHandler(this.AcceptButtonActivation);
            // 
            // textBoxLogin
            // 
            this.textBoxLogin.AllowsLetters = true;
            this.textBoxLogin.AllowsNumbers = true;
            this.textBoxLogin.AllowsPunctuation = true;
            this.textBoxLogin.AllowsSeparators = true;
            this.textBoxLogin.AllowsSymbols = true;
            this.textBoxLogin.AllowsWhiteSpaces = false;
            this.textBoxLogin.ExtraAllowedChars = "";
            this.textBoxLogin.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxLogin.Location = new System.Drawing.Point(116, 15);
            this.textBoxLogin.MaxLength = 50;
            this.textBoxLogin.Name = "textBoxLogin";
            this.textBoxLogin.NonAllowedCharacters = "";
            this.textBoxLogin.RegularExpresion = "";
            this.textBoxLogin.Size = new System.Drawing.Size(204, 20);
            this.textBoxLogin.TabIndex = 1;
            this.textBoxLogin.TextChanged += new System.EventHandler(this.AcceptButtonActivation);
            // 
            // labelPassword
            // 
            this.labelPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPassword.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelPassword.Location = new System.Drawing.Point(28, 47);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(68, 21);
            this.labelPassword.TabIndex = 4;
            this.labelPassword.Text = "Contrasena:";
            this.labelPassword.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelExLogin
            // 
            this.labelExLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExLogin.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExLogin.Location = new System.Drawing.Point(28, 15);
            this.labelExLogin.Name = "labelExLogin";
            this.labelExLogin.Size = new System.Drawing.Size(72, 22);
            this.labelExLogin.TabIndex = 0;
            this.labelExLogin.Text = "Identificador:";
            this.labelExLogin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // defaultLookAndFeel
            // 
            this.defaultLookAndFeel.LookAndFeel.SkinName = "Blue";
            // 
            // LoginForm
            // 
            this.AcceptButton = this.buttonOk;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(330, 239);
            this.Controls.Add(this.panel);
            this.Controls.Add(this.pictureBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LoginForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.LoginForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.panel.ResumeLayout(false);
            this.panel.PerformLayout();
            this.ResumeLayout(false);
            

        }
        #endregion

        private System.Timers.Timer timerFocus;

        private void LoginForm_Load(object sender, System.EventArgs e)
        {
            AcceptButtonActivation(null, null);

            Win32.LockSetForegroundWindow(Win32.LSFW_LOCK);

            //textBoxExExtNumber.Enabled = SmartCadConfiguration.SmartCadSection.ClientElement.TelephonyElement.Changeable && application == UserApplicationClientData.FirstLevel;
            //textBoxExExtNumber.Text = SmartCadConfiguration.SmartCadSection.ClientElement.TelephonyElement.Extension;
            timerFocus.Start();
            textBoxLogin.Focus();
        }

        private void buttonOk_Click(object sender, System.EventArgs e)
        {
            try
            {
                BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[] { this.GetType().GetMethod("CallLogin", BindingFlags.Instance | BindingFlags.NonPublic) }, new object[][] { new object[] { } });
                processForm.CanThrowError = true;
                processForm.ShowDialog();
                DialogResult = DialogResult.OK;
            }
            catch (FaultException ex)
            {
                ShowErrorMessage(ex);
            }
            catch (SocketException ex)
            {
                SmartLogger.Print(ex);
                NoServerError();
            }
        }

        private void ShowErrorMessage(FaultException ex)
        {
            MessageForm.Show(ResourceLoader.GetString2(ex.Message), MessageFormType.Error);
            textBoxPassword.Focus();
            textBoxPassword.SelectionStart = 0;
            textBoxPassword.SelectionLength = textBoxPassword.Text.Length;
        }

        private void NoServerError()
        {
            MessageForm.Show(ResourceLoader.GetString2("CheckServerConexion"), MessageFormType.Warning);
            textBoxPassword.Focus();
            textBoxPassword.SelectionStart = 0;
            textBoxPassword.SelectionLength = textBoxPassword.Text.Length;
        }

        private delegate bool textBox_Focus();

        private void timerFocus_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (textBoxLogin.Text == "" && textBoxPassword.Text == "")
            {
                if (this.InvokeRequired)
                    this.BeginInvoke(new textBox_Focus(textBoxLogin.Focus), new object[0] { });
                else
                    textBoxLogin.Focus();
                timerFocus.Stop();
            }
        }

        private void AcceptButtonActivation(object sender, System.EventArgs e)
        {
            if (textBoxLogin.Text == "" || textBoxPassword.Text == "")
                buttonOk.Enabled = false;
            else
                buttonOk.Enabled = true;
        }

        public string getLocalIP() { string localIP = ""; string strHostName = Dns.GetHostName(); IPHostEntry ipEntry = Dns.GetHostByName(strHostName); IPAddress[] addr = ipEntry.AddressList;  /* what if more than one instance exists? */  for (int i = 0; i < addr.Length; i++) { localIP = addr[i].ToString(); } return localIP; }

        private void CallLogin()
        {
            string login = textBoxLogin.Text.Trim();
            string password = textBoxPassword.Text.Trim();
            //string extNumber = textBoxExExtNumber.Text.Trim();
            this.plainPassword = password;
            this.networkCredential = new NetworkCredential(login, password, Environment.UserDomainName);

            serverService = ServerServiceClient.GetInstance();

            serverService.OpenSession(login, password, string.Empty, Dns.GetHostName(), application);
            try
            {
                if (application == UserApplicationClientData.FirstLevel)
                {
                    SmartCadConfiguration.Save();
                }
            }
            catch
            {
                serverService.CloseSession();
                ServerServiceClient.GetServerService();
                ServerServiceClient.GetInstance().SetApplication(application);
                throw;
            }
        }

        public void ValidateUser(string loginStr, string passwordStr)
        {
            try
            {
                this.textBoxLogin.Text = loginStr;
                this.textBoxPassword.Text = passwordStr;
                buttonOk_Click(null, null);
            }
            catch
            {
                throw;
            }
        }

        private ServerServiceClient serverService;

        public ServerServiceClient ServerService
        {
            get
            {
                return serverService;
            }
        }

        public NetworkCredential NetworkCredential
        {
            get
            {
                return networkCredential;
            }
        }

        //public string ExtNumber
        //{
        //    get
        //    {
        //        return textBoxExExtNumber.Text.Trim();
        //    }
        //}
    }
}
