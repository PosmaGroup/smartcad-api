﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Smartmatic.SmartCad.Vms;
using System.Threading;
using Smartmatic.SmartCad.Service;
using System.Net;
using System.Collections;
using System.IO;
using System.Reflection;
using SmartCadCore.ClientData;
using SmartCadControls;
using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadControls.Controls;
using SmartCadGuiCommon.Classes;

namespace SmartCadFirstLevel.Gui
{
    public partial class TwitterAlarmsForm : DevExpress.XtraEditors.XtraForm
    {
        public TwitterAlarmGridData SelectedAlarm { get; set; }
        public Dictionary<string, ApplicationPreferenceClientData> GlobalApplicationPreference { get; set; }
        public BindingList<TwitterAlarmGridData> AlarmsList { get; set; }
        public BindingList<TweetGridData> MentionsList { get; set; }

        /// <summary>
        /// Set the current selected camera. Use "camera" == null to clear selection.
        /// </summary>
        /// <param name="camera"></param>
        void SelectAlarm(TwitterAlarmGridData alarm)
        {
            if ((TwitterFrontClientFormDevX)this.MdiParent != null)
            {
                if (alarm == null)
                {
                    ((TwitterFrontClientFormDevX)this.MdiParent).barButtonItemStartRegIncident.Enabled = false;
                    ((TwitterFrontClientFormDevX)this.MdiParent).barButtonItemCancelAlarm.Enabled = false;
                    gridControlAlarmTweets.DataSource = null;
                }
                else
                {
                    ((TwitterFrontClientFormDevX)this.MdiParent).barButtonItemStartRegIncident.Enabled = true;
                    ((TwitterFrontClientFormDevX)this.MdiParent).barButtonItemCancelAlarm.Enabled = true;
                    gridControlAlarmTweets.DataSource = alarm.Tweets;
                }
            }
            SelectedAlarm = alarm;
        }

        void Panel_Disposed(object sender, EventArgs e)
        {
            SelectedAlarm = null;
        }

        public TwitterAlarmsForm()
        {
            InitializeComponent();
            InitGrid();

            this.AlarmsList.ListChanged += AlarmsList_ListChanged;
            this.Enter += TwitterAlarmsForm_Enter;

            ServerServiceClient.GetInstance().CommittedChanges += ServerService_CommittedChanges;
        }

        void TwitterAlarmsForm_Enter(object sender, EventArgs e)
        {
            if (AlarmsList.Count == 0)
            {
                (this.MdiParent as TwitterFrontClientFormDevX).barButtonItemStartRegIncident.Enabled = false;

            }
        }

        /// <summary>
        /// Initialize Grid Control EX of Alarms
        /// </summary>
        private void InitGrid()
        {
            gridControlAlarms.EnableAutoFilter = true;
            gridControlAlarms.Type = typeof(TwitterAlarmGridData);
            gridControlAlarms.ViewTotalRows = true;
            gridControlAlarms.AllowDrop = false;
            gridViewAlarms.ViewTotalRows = true;
            gridViewAlarms.BestFitColumns();
            gridViewAlarms.FocusedRowHandle = GridControlEx.AutoFilterRowHandle;
            gridViewAlarms.SelectRow(GridControlEx.AutoFilterRowHandle);


            gridControlSearchResults.AllowDrop = false;

            AlarmsList = new BindingList<TwitterAlarmGridData>();
            this.gridControlAlarms.DataSource = AlarmsList;
        }

        private void TwitterAlarmsForm_Load(object sender, EventArgs e)
        {
            LoadLanguage();
            MentionsList = TwitterSearch.GetMentions();
            gridControlMentions.DataSource = MentionsList;
        }

        DataTable GetData(int rows)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ID", typeof(int));
            dt.Columns.Add("Content", typeof(string));
            for (int i = 0; i < rows; i++)
            {
                dt.Rows.Add(i, "Content" + i.ToString());
            }
            return dt;
        } 

        public void LoadInitialData()
        {
            #region Loading ApplicationPreferences
            GlobalApplicationPreference = new Dictionary<string, ApplicationPreferenceClientData>();
            foreach (ApplicationPreferenceClientData preference in
                ServerServiceClient.GetInstance().SearchClientObjectsMaxRows(SmartCadHqls.GetAllApplicationPreferences, true))
            {
                GlobalApplicationPreference.Add(preference.Name, preference);
            }
            #endregion

            #region Load TwitterAlarms

            IList alarms = ServerServiceClient.GetInstance().SearchClientObjectsMaxRows(SmartCadHqls.GetCustomHql(
                SmartCadHqls.GetAlarmsTwitterByStatus, (int)AlarmTwitterClientData.AlarmStatus.NotTaken),false);
            if (alarms != null && alarms.Count > 0)
            {
                foreach (AlarmTwitterClientData client in alarms)
                {
                    AlarmsList.Add(new TwitterAlarmGridData(client));
                }
            }
            #endregion
        }

        private void AlarmsList_ListChanged(object sender, ListChangedEventArgs e)
        {
        }

        public void ServerService_CommittedChanges(object sender, CommittedObjectDataCollectionEventArgs args)
        {
            try
            {
                if (args != null && args.Objects != null & args.Objects.Count > 0)
                {
                    if (args.Objects[0] is AlarmTwitterClientData)
                    {
                        #region AlarmTwitterClientData
                        TwitterAlarmGridData alarm = new TwitterAlarmGridData((AlarmTwitterClientData)args.Objects[0]);
                        if (args.Action == CommittedDataAction.Save)
                        {
                            FormUtil.InvokeRequired(this, delegate
                            {
                                AlarmsList.Add(alarm);
                                if (AlarmsList.Count == 1)
                                {
                                    SelectAlarm(alarm);
                                }
                            });
                        }
                        if (args.Action == CommittedDataAction.Update)
                        {
                            FormUtil.InvokeRequired(this, delegate
                            {
                                if (AlarmsList.Contains(alarm))
                                {
                                    if (alarm.alarmClientData.Status != AlarmTwitterClientData.AlarmStatus.NotTaken)
                                    {
                                        AlarmsList.Remove(alarm);
                                    }
                                    else
                                    {
                                        AlarmsList[AlarmsList.IndexOf(alarm)] = alarm;
                                    }
                                }
                                else
                                {
                                    if (alarm.alarmClientData.Status == AlarmTwitterClientData.AlarmStatus.NotTaken)
                                    {
                                        AlarmsList.Add(alarm);
                                    }
                                }

                                if (SelectedAlarm == alarm) // Update interface
                                {
                                    SelectAlarm(alarm);
                                }
                            });
                        }
                        else if (args.Action == CommittedDataAction.Delete)
                        {
                            FormUtil.InvokeRequired(this, delegate
                            {
                                if (SelectedAlarm.Equals(alarm))
                                {
                                    SelectAlarm(null);
                                }
                                AlarmsList.Remove(alarm);
                            });
                        }
                        #endregion
                    }
                    else if(args.Objects[0] is TweetClientData)
                    {
                        #region TweetClientData (Mentions)
                        TweetClientData tweet = (TweetClientData)args.Objects[0];
                        if (args.Action == CommittedDataAction.Save)
                        {
                            FormUtil.InvokeRequired(this, delegate
                            {
                                MentionsList.Add(new TweetGridData(tweet));
                                gridControlMentions.Update();
                            });
                        }
                        if (args.Action == CommittedDataAction.Update)
                        {
                        }
                        else if (args.Action == CommittedDataAction.Delete)
                        {
                        }
                        #endregion
                    }
                }

            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private void LoadLanguage()
        {
            this.Text = ResourceLoader.GetString2("Alerts");

            barButtonItemPrint.Caption = ResourceLoader.GetString2("Print");
            barButtonItemRefresh.Caption = ResourceLoader.GetString2("Refresh");
            barButtonItemSave.Caption = ResourceLoader.GetString2("Save");
            ribbonPageGroupGeneralOptions.Text = ResourceLoader.GetString2("GeneralOptions");
            ribbonPage1.Text = ResourceLoader.GetString2("SocialNetworks");
            dockPanel2.Text = ResourceLoader.GetString2("CustomSearch");
            buttonSearch.Text = ResourceLoader.GetString2("Search");
            groupControlAlarms.Text = ResourceLoader.GetString2("Alerts");
            groupControlAlarmTweets.Text = ResourceLoader.GetString2("RelatedTweets");

            ribbonPageGroupTwitter.Text = ResourceLoader.GetString2("Twitter");
            barButtonItemTweet.Caption = ResourceLoader.GetString2("New Tweet");
            #region Footer
            //toolStripStatusLABEL_TEXT.Text = ResourceLoader.GetString2("User") + ":";
            //toolStripStatusLabelConnected.Text = ResourceLoader.GetString2("Connected") + ":";
            #endregion
        }

        private void GridViewAlarms_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (gridViewAlarms.GetFocusedRow() != null)
            {
                SelectAlarm((TwitterAlarmGridData)gridViewAlarms.GetFocusedRow());
            }
            else
            {
                SelectAlarm(null);
            }
        }

        private void gridViewSeachResults_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (gridViewSeachResults.GetFocusedRow() != null)
            {
                buttonNewAlert.Enabled = true;
            }
            else
            {
                buttonNewAlert.Enabled = false;
            }
        }

        private void toolStripMenuItemCancel_Click(object sender, EventArgs e)
        {
            gridViewAlarms.BeginUpdate();
            gridViewAlarms.DeleteSelectedRows();
            gridViewAlarms.EndUpdate();
        }

        public void TakeAlarm(TwitterAlarmGridData gridControlDataAlarm)
        {
            //In progress
            gridControlDataAlarm.alarmClientData.Status = AlarmTwitterClientData.AlarmStatus.InProcess;
            ServerServiceClient.GetInstance().SaveOrUpdateClientData(gridControlDataAlarm.alarmClientData);
        }

        public void ReleaseAlarm(TwitterAlarmGridData gridControlDataAlarm)
        {
            //New
            gridControlDataAlarm.alarmClientData.Status = AlarmTwitterClientData.AlarmStatus.NotTaken;
            ServerServiceClient.GetInstance().SaveOrUpdateClientData(gridControlDataAlarm.alarmClientData);
        }

        public void FisnishAlarm(TwitterAlarmGridData gridControlDataAlarm)
        {
            //Closed
            if (gridControlDataAlarm != null)
            {
                gridControlDataAlarm.alarmClientData.Status = AlarmTwitterClientData.AlarmStatus.Ended;
                ServerServiceClient.GetInstance().SaveOrUpdateClientData(gridControlDataAlarm.alarmClientData);
            }
        }

        public void CancelAlarm()
        {
            if (SelectedAlarm != null)
            {
                if (MessageForm.Show(ResourceLoader.GetString2("CancelTelemetryAlarm"), MessageFormType.Question) == DialogResult.Yes)
                {
                    AlarmsList.Remove(SelectedAlarm);
                    SelectedAlarm = null;
                }
            }
        }

        private void TwitterAlarmsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
            }
        }
        
        private static bool EnsureLogout()
        {
            DialogResult dialogResult = MessageForm.Show(ResourceLoader.GetString2("$Message.ExitApplication"), MessageFormType.Question);

            return dialogResult == DialogResult.Yes;
        }
        
        private void buttonSearch_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textSearch.Text)) return;

            try
            {
                Cursor = Cursors.WaitCursor;
                BackgroundProcessForm processForm = new BackgroundProcessForm(
                    ResourceLoader.GetString2("Searching"),
                    this,
                    new MethodInfo[1] {
                        GetType().GetMethod("BackgroundSearch", BindingFlags.NonPublic | BindingFlags.Instance) 
                    },
                    new object[1][] { new object[0] { } }
                );

                processForm.CanThrowError = true;
                processForm.ShowDialog();
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void BackgroundSearch()
        {
            List<TweetGridData> searchResults = TwitterSearch.GetSearchResults(textSearch.Text);
            FormUtil.InvokeRequired(this, delegate
            {
                gridControlSearchResults.BeginUpdate();
                gridControlSearchResults.DataSource = searchResults;
                gridControlSearchResults.EndUpdate();
                
                gridControlSearchResults.Update();
            });
        }

        private void buttonNewAlert_Click(object sender, EventArgs e)
        {
            TweetGridData grid = ((TweetGridData)gridViewSeachResults.GetFocusedRow());

            AlarmTwitterClientData newAlarm =  new AlarmTwitterClientData()
            {
                StartAlarm = DateTime.Now,
                Status = AlarmTwitterClientData.AlarmStatus.InProcess,
                Query = new AlarmQueryTwitterClientData()
                {
                    Name = ResourceLoader.GetString2("Live search result"),
                    Description = ResourceLoader.GetString2("Alarm generated manually from live search"),
                    TwitterQuery = textSearch.Text,
                    Type = 2
                },
                Tweets = new List<TweetClientData>(){ grid.tweet }
            };
            ServerServiceClient.GetInstance().SaveOrUpdateClientData(newAlarm);

           //AlarmsList.Add(new TwitterAlarmGridData(newAlarm));
        }

        private void textSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                buttonSearch_Click(null, null);
            }
        }

        private void barButtonItemTweet_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            PostTweetForm ptf = new PostTweetForm();
            ptf.ShowDialog();
        }

        private void gridControlSearchResults_Enter(object sender, EventArgs e)
        {
            gridControlSearchResults.RefreshDataSource();
        }

        private void gridControlSearchResults_Click(object sender, EventArgs e)
        {
            gridControlSearchResults.RefreshDataSource();
        }

        private void gridControlSearchResults_Leave(object sender, EventArgs e)
        {
            gridControlSearchResults.RefreshDataSource();
        }

        private void gridControlMentions_Click(object sender, EventArgs e)
        {
            gridControlMentions.RefreshDataSource();
        }

        private void gridControlMentions_Enter(object sender, EventArgs e)
        {
            gridControlMentions.RefreshDataSource();
        }

        private void gridControlMentions_Leave(object sender, EventArgs e)
        {
            gridControlMentions.RefreshDataSource();
        }

        private void gridControlAlarmTweets_Click(object sender, EventArgs e)
        {
            gridControlAlarmTweets.RefreshDataSource();
        }

        private void gridControlAlarmTweets_Enter(object sender, EventArgs e)
        {
            gridControlAlarmTweets.RefreshDataSource();
        }

        private void gridControlAlarmTweets_Leave(object sender, EventArgs e)
        {
            gridControlAlarmTweets.RefreshDataSource();
        }
        
    }







}