﻿using SmartCadControls;
using SmartCadControls.Controls;

namespace SmartCadFirstLevel.Gui
{
    partial class TwitterAlarmsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }


        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TwitterAlarmsForm));
            this.gridViewSeachResults = new DevExpress.XtraGrid.Views.Layout.LayoutView();
            this.layoutViewColumn7 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colAuthor2 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn8 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.repositoryItemMemoEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.layoutViewField_colContent1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn9 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colSince1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn12 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.repositoryItemPictureEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.layoutViewField_colProfileImage1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.gridControlSearchResults = new DevExpress.XtraGrid.GridControl();
            this.contextMenuStripEx1 = new ContextMenuStripEx(this.components);
            this.toolStripMenuItemCancel = new System.Windows.Forms.ToolStripMenuItem();
            this.layoutViewCard2 = new DevExpress.XtraGrid.Views.Layout.LayoutViewCard();
            this.item1 = new DevExpress.XtraLayout.SimpleSeparator();
            this.gridControlAlarmTweets = new DevExpress.XtraGrid.GridControl();
            this.gridViewTweets = new DevExpress.XtraGrid.Views.Layout.LayoutView();
            this.layoutViewColumn1 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colAuthor1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn2 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.layoutViewField2 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn3 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField3 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn10 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.repositoryItemPictureEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.layoutViewField_layoutViewColumn4 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewCard3 = new DevExpress.XtraGrid.Views.Layout.LayoutViewCard();
            this.item4 = new DevExpress.XtraLayout.SimpleSeparator();
            this.gridControlAlarms = new GridControlEx();
            this.gridViewAlarms = new GridViewEx();
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItemPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSave = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemTweet = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroupGeneralOptions = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupTwitter = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.repositoryItemMemoEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanel2 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel2_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.layoutControlSearch = new DevExpress.XtraLayout.LayoutControl();
            this.buttonNewAlert = new DevExpress.XtraEditors.SimpleButton();
            this.textSearch = new DevExpress.XtraEditors.TextEdit();
            this.buttonSearch = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.dockPanelMentions = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.gridControlMentions = new DevExpress.XtraGrid.GridControl();
            this.gridViewMentions = new DevExpress.XtraGrid.Views.Layout.LayoutView();
            this.layoutViewColumn5 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn6 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.repositoryItemMemoEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.layoutViewField_colContent3 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn11 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colSince3 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn4 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.repositoryItemPictureEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.layoutViewField_colProfileImage3 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewCard1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewCard();
            this.item5 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemMentions = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.groupControlAlarmTweets = new DevExpress.XtraEditors.GroupControl();
            this.groupControlAlarms = new DevExpress.XtraEditors.GroupControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.item2 = new DevExpress.XtraLayout.SimpleSeparator();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSeachResults)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colAuthor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colContent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colSince1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colProfileImage1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSearchResults)).BeginInit();
            this.contextMenuStripEx1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAlarmTweets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTweets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colAuthor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAlarms)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewAlarms)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanel2.SuspendLayout();
            this.dockPanel2_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlSearch)).BeginInit();
            this.layoutControlSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textSearch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            this.dockPanelMentions.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlMentions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMentions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colContent3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colSince3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colProfileImage3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemMentions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlAlarmTweets)).BeginInit();
            this.groupControlAlarmTweets.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlAlarms)).BeginInit();
            this.groupControlAlarms.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item2)).BeginInit();
            this.SuspendLayout();
            // 
            // gridViewSeachResults
            // 
            this.gridViewSeachResults.CardCaptionFormat = "{2}";
            this.gridViewSeachResults.CardHorzInterval = 0;
            this.gridViewSeachResults.CardMinSize = new System.Drawing.Size(316, 61);
            this.gridViewSeachResults.CardVertInterval = 6;
            this.gridViewSeachResults.Columns.AddRange(new DevExpress.XtraGrid.Columns.LayoutViewColumn[] {
            this.layoutViewColumn7,
            this.layoutViewColumn8,
            this.layoutViewColumn9,
            this.layoutViewColumn12});
            this.gridViewSeachResults.GridControl = this.gridControlSearchResults;
            this.gridViewSeachResults.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutViewField_colAuthor2});
            this.gridViewSeachResults.Name = "gridViewSeachResults";
            this.gridViewSeachResults.OptionsBehavior.AllowRuntimeCustomization = false;
            this.gridViewSeachResults.OptionsBehavior.Editable = false;
            this.gridViewSeachResults.OptionsBehavior.ScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Auto;
            this.gridViewSeachResults.OptionsCustomization.AllowFilter = false;
            this.gridViewSeachResults.OptionsCustomization.AllowSort = false;
            this.gridViewSeachResults.OptionsCustomization.ShowGroupCardCaptions = false;
            this.gridViewSeachResults.OptionsCustomization.ShowGroupCardIndents = false;
            this.gridViewSeachResults.OptionsCustomization.ShowGroupCards = false;
            this.gridViewSeachResults.OptionsCustomization.ShowGroupFields = false;
            this.gridViewSeachResults.OptionsCustomization.ShowGroupHiddenItems = false;
            this.gridViewSeachResults.OptionsCustomization.ShowGroupLayout = false;
            this.gridViewSeachResults.OptionsCustomization.ShowGroupLayoutTreeView = false;
            this.gridViewSeachResults.OptionsCustomization.ShowGroupView = false;
            this.gridViewSeachResults.OptionsCustomization.ShowSaveLoadLayoutButtons = false;
            this.gridViewSeachResults.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridViewSeachResults.OptionsFilter.AllowFilterEditor = false;
            this.gridViewSeachResults.OptionsMultiRecordMode.StretchCardToViewHeight = true;
            this.gridViewSeachResults.OptionsMultiRecordMode.StretchCardToViewWidth = true;
            this.gridViewSeachResults.OptionsView.AllowHotTrackFields = false;
            this.gridViewSeachResults.OptionsView.CardArrangeRule = DevExpress.XtraGrid.Views.Layout.LayoutCardArrangeRule.AllowPartialCards;
            this.gridViewSeachResults.OptionsView.ShowCardLines = false;
            this.gridViewSeachResults.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewSeachResults.OptionsView.ShowHeaderPanel = false;
            this.gridViewSeachResults.OptionsView.ViewMode = DevExpress.XtraGrid.Views.Layout.LayoutViewMode.Column;
            this.gridViewSeachResults.PaintStyleName = "Skin";
            this.gridViewSeachResults.TemplateCard = this.layoutViewCard2;
            this.gridViewSeachResults.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewSeachResults_FocusedRowChanged);
            // 
            // layoutViewColumn7
            // 
            this.layoutViewColumn7.Caption = "Author";
            this.layoutViewColumn7.FieldName = "Author";
            this.layoutViewColumn7.LayoutViewField = this.layoutViewField_colAuthor2;
            this.layoutViewColumn7.Name = "layoutViewColumn7";
            this.layoutViewColumn7.OptionsColumn.AllowEdit = false;
            // 
            // layoutViewField_colAuthor2
            // 
            this.layoutViewField_colAuthor2.EditorPreferredWidth = 316;
            this.layoutViewField_colAuthor2.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_colAuthor2.MaxSize = new System.Drawing.Size(0, 20);
            this.layoutViewField_colAuthor2.MinSize = new System.Drawing.Size(24, 20);
            this.layoutViewField_colAuthor2.Name = "layoutViewField_colAuthor2";
            this.layoutViewField_colAuthor2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutViewField_colAuthor2.Size = new System.Drawing.Size(314, 39);
            this.layoutViewField_colAuthor2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutViewField_colAuthor2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewField_colAuthor2.TextToControlDistance = 0;
            this.layoutViewField_colAuthor2.TextVisible = false;
            // 
            // layoutViewColumn8
            // 
            this.layoutViewColumn8.Caption = "Tweet";
            this.layoutViewColumn8.ColumnEdit = this.repositoryItemMemoEdit4;
            this.layoutViewColumn8.FieldName = "Content";
            this.layoutViewColumn8.LayoutViewField = this.layoutViewField_colContent1;
            this.layoutViewColumn8.Name = "layoutViewColumn8";
            this.layoutViewColumn8.OptionsColumn.AllowEdit = false;
            // 
            // repositoryItemMemoEdit4
            // 
            this.repositoryItemMemoEdit4.Name = "repositoryItemMemoEdit4";
            // 
            // layoutViewField_colContent1
            // 
            this.layoutViewField_colContent1.AllowHtmlStringInCaption = true;
            this.layoutViewField_colContent1.EditorPreferredWidth = 215;
            this.layoutViewField_colContent1.Location = new System.Drawing.Point(46, 0);
            this.layoutViewField_colContent1.Name = "layoutViewField_colContent1";
            this.layoutViewField_colContent1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutViewField_colContent1.Size = new System.Drawing.Size(215, 39);
            this.layoutViewField_colContent1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewField_colContent1.TextToControlDistance = 0;
            this.layoutViewField_colContent1.TextVisible = false;
            // 
            // layoutViewColumn9
            // 
            this.layoutViewColumn9.Caption = "Since";
            this.layoutViewColumn9.FieldName = "Since";
            this.layoutViewColumn9.LayoutViewField = this.layoutViewField_colSince1;
            this.layoutViewColumn9.Name = "layoutViewColumn9";
            this.layoutViewColumn9.OptionsColumn.AllowEdit = false;
            // 
            // layoutViewField_colSince1
            // 
            this.layoutViewField_colSince1.EditorPreferredWidth = 51;
            this.layoutViewField_colSince1.Location = new System.Drawing.Point(263, 0);
            this.layoutViewField_colSince1.MaxSize = new System.Drawing.Size(51, 22);
            this.layoutViewField_colSince1.MinSize = new System.Drawing.Size(51, 22);
            this.layoutViewField_colSince1.Name = "layoutViewField_colSince1";
            this.layoutViewField_colSince1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutViewField_colSince1.Size = new System.Drawing.Size(51, 39);
            this.layoutViewField_colSince1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutViewField_colSince1.TextLocation = DevExpress.Utils.Locations.Right;
            this.layoutViewField_colSince1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewField_colSince1.TextToControlDistance = 0;
            this.layoutViewField_colSince1.TextVisible = false;
            // 
            // layoutViewColumn12
            // 
            this.layoutViewColumn12.Caption = "ProfileImage";
            this.layoutViewColumn12.ColumnEdit = this.repositoryItemPictureEdit3;
            this.layoutViewColumn12.FieldName = "ProfileImage";
            this.layoutViewColumn12.LayoutViewField = this.layoutViewField_colProfileImage1;
            this.layoutViewColumn12.Name = "layoutViewColumn12";
            this.layoutViewColumn12.OptionsColumn.AllowEdit = false;
            // 
            // repositoryItemPictureEdit3
            // 
            this.repositoryItemPictureEdit3.Name = "repositoryItemPictureEdit3";
            // 
            // layoutViewField_colProfileImage1
            // 
            this.layoutViewField_colProfileImage1.EditorPreferredWidth = 42;
            this.layoutViewField_colProfileImage1.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_colProfileImage1.MaxSize = new System.Drawing.Size(65, 0);
            this.layoutViewField_colProfileImage1.MinSize = new System.Drawing.Size(1, 1);
            this.layoutViewField_colProfileImage1.Name = "layoutViewField_colProfileImage1";
            this.layoutViewField_colProfileImage1.Size = new System.Drawing.Size(46, 39);
            this.layoutViewField_colProfileImage1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutViewField_colProfileImage1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewField_colProfileImage1.TextToControlDistance = 0;
            this.layoutViewField_colProfileImage1.TextVisible = false;
            // 
            // gridControlSearchResults
            // 
            this.gridControlSearchResults.ContextMenuStrip = this.contextMenuStripEx1;
            this.gridControlSearchResults.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlSearchResults.Location = new System.Drawing.Point(4, 30);
            this.gridControlSearchResults.MainView = this.gridViewSeachResults;
            this.gridControlSearchResults.Name = "gridControlSearchResults";
            this.gridControlSearchResults.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoEdit4,
            this.repositoryItemPictureEdit3});
            this.gridControlSearchResults.Size = new System.Drawing.Size(332, 586);
            this.gridControlSearchResults.TabIndex = 34;
            this.gridControlSearchResults.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewSeachResults});
            this.gridControlSearchResults.Click += new System.EventHandler(this.gridControlSearchResults_Click);
            this.gridControlSearchResults.Enter += new System.EventHandler(this.gridControlSearchResults_Enter);
            this.gridControlSearchResults.Leave += new System.EventHandler(this.gridControlSearchResults_Leave);
            // 
            // contextMenuStripEx1
            // 
            this.contextMenuStripEx1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemCancel});
            this.contextMenuStripEx1.Name = "contextMenuStripEx1";
            this.contextMenuStripEx1.Size = new System.Drawing.Size(111, 26);
            // 
            // toolStripMenuItemCancel
            // 
            this.toolStripMenuItemCancel.Image = global::SmartCadGuiCommon.Properties.ResourcesGui.Delete_icon__1_;
            this.toolStripMenuItemCancel.Name = "toolStripMenuItemCancel";
            this.toolStripMenuItemCancel.Size = new System.Drawing.Size(110, 22);
            this.toolStripMenuItemCancel.Text = "Cancel";
            this.toolStripMenuItemCancel.Click += new System.EventHandler(this.toolStripMenuItemCancel_Click);
            // 
            // layoutViewCard2
            // 
            this.layoutViewCard2.CustomizationFormText = "TemplateCard";
            this.layoutViewCard2.ExpandButtonLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutViewCard2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutViewField_colContent1,
            this.layoutViewField_colSince1,
            this.item1,
            this.layoutViewField_colProfileImage1});
            this.layoutViewCard2.Name = "layoutViewCard2";
            this.layoutViewCard2.OptionsItemText.TextToControlDistance = 5;
            this.layoutViewCard2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutViewCard2.Text = "TemplateCard";
            // 
            // item1
            // 
            this.item1.AllowHotTrack = false;
            this.item1.CustomizationFormText = "item1";
            this.item1.Location = new System.Drawing.Point(261, 0);
            this.item1.Name = "item1";
            this.item1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.item1.Size = new System.Drawing.Size(2, 39);
            this.item1.Text = "item1";
            // 
            // gridControlAlarmTweets
            // 
            this.gridControlAlarmTweets.ContextMenuStrip = this.contextMenuStripEx1;
            this.gridControlAlarmTweets.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlAlarmTweets.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlAlarmTweets.Location = new System.Drawing.Point(2, 22);
            this.gridControlAlarmTweets.MainView = this.gridViewTweets;
            this.gridControlAlarmTweets.Name = "gridControlAlarmTweets";
            this.gridControlAlarmTweets.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPictureEdit1,
            this.repositoryItemMemoEdit1});
            this.gridControlAlarmTweets.Size = new System.Drawing.Size(298, 302);
            this.gridControlAlarmTweets.TabIndex = 35;
            this.gridControlAlarmTweets.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewTweets});
            this.gridControlAlarmTweets.Click += new System.EventHandler(this.gridControlAlarmTweets_Click);
            this.gridControlAlarmTweets.Enter += new System.EventHandler(this.gridControlAlarmTweets_Enter);
            this.gridControlAlarmTweets.Leave += new System.EventHandler(this.gridControlAlarmTweets_Leave);
            // 
            // gridViewTweets
            // 
            this.gridViewTweets.CardCaptionFormat = "{2}";
            this.gridViewTweets.CardHorzInterval = 0;
            this.gridViewTweets.CardMinSize = new System.Drawing.Size(316, 61);
            this.gridViewTweets.CardVertInterval = 6;
            this.gridViewTweets.Columns.AddRange(new DevExpress.XtraGrid.Columns.LayoutViewColumn[] {
            this.layoutViewColumn1,
            this.layoutViewColumn2,
            this.layoutViewColumn3,
            this.layoutViewColumn10});
            this.gridViewTweets.GridControl = this.gridControlAlarmTweets;
            this.gridViewTweets.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutViewField_colAuthor1});
            this.gridViewTweets.Name = "gridViewTweets";
            this.gridViewTweets.OptionsBehavior.AllowRuntimeCustomization = false;
            this.gridViewTweets.OptionsBehavior.Editable = false;
            this.gridViewTweets.OptionsBehavior.ScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Auto;
            this.gridViewTweets.OptionsCustomization.AllowFilter = false;
            this.gridViewTweets.OptionsCustomization.AllowSort = false;
            this.gridViewTweets.OptionsCustomization.ShowGroupCardCaptions = false;
            this.gridViewTweets.OptionsCustomization.ShowGroupCardIndents = false;
            this.gridViewTweets.OptionsCustomization.ShowGroupCards = false;
            this.gridViewTweets.OptionsCustomization.ShowGroupFields = false;
            this.gridViewTweets.OptionsCustomization.ShowGroupHiddenItems = false;
            this.gridViewTweets.OptionsCustomization.ShowGroupLayout = false;
            this.gridViewTweets.OptionsCustomization.ShowGroupLayoutTreeView = false;
            this.gridViewTweets.OptionsCustomization.ShowGroupView = false;
            this.gridViewTweets.OptionsCustomization.ShowSaveLoadLayoutButtons = false;
            this.gridViewTweets.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridViewTweets.OptionsFilter.AllowFilterEditor = false;
            this.gridViewTweets.OptionsMultiRecordMode.StretchCardToViewHeight = true;
            this.gridViewTweets.OptionsMultiRecordMode.StretchCardToViewWidth = true;
            this.gridViewTweets.OptionsView.AllowHotTrackFields = false;
            this.gridViewTweets.OptionsView.CardArrangeRule = DevExpress.XtraGrid.Views.Layout.LayoutCardArrangeRule.AllowPartialCards;
            this.gridViewTweets.OptionsView.ShowCardLines = false;
            this.gridViewTweets.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewTweets.OptionsView.ShowHeaderPanel = false;
            this.gridViewTweets.OptionsView.ViewMode = DevExpress.XtraGrid.Views.Layout.LayoutViewMode.Column;
            this.gridViewTweets.PaintStyleName = "Skin";
            this.gridViewTweets.TemplateCard = this.layoutViewCard3;
            // 
            // layoutViewColumn1
            // 
            this.layoutViewColumn1.Caption = "Author";
            this.layoutViewColumn1.CustomizationCaption = "Author";
            this.layoutViewColumn1.FieldName = "Author";
            this.layoutViewColumn1.LayoutViewField = this.layoutViewField_colAuthor1;
            this.layoutViewColumn1.Name = "layoutViewColumn1";
            this.layoutViewColumn1.OptionsColumn.AllowEdit = false;
            // 
            // layoutViewField_colAuthor1
            // 
            this.layoutViewField_colAuthor1.AllowHtmlStringInCaption = true;
            this.layoutViewField_colAuthor1.EditorPreferredWidth = 316;
            this.layoutViewField_colAuthor1.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_colAuthor1.MaxSize = new System.Drawing.Size(0, 20);
            this.layoutViewField_colAuthor1.MinSize = new System.Drawing.Size(24, 20);
            this.layoutViewField_colAuthor1.Name = "layoutViewField_colAuthor1";
            this.layoutViewField_colAuthor1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutViewField_colAuthor1.Size = new System.Drawing.Size(259, 22);
            this.layoutViewField_colAuthor1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutViewField_colAuthor1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewField_colAuthor1.TextToControlDistance = 0;
            this.layoutViewField_colAuthor1.TextVisible = false;
            // 
            // layoutViewColumn2
            // 
            this.layoutViewColumn2.Caption = "Tweet";
            this.layoutViewColumn2.ColumnEdit = this.repositoryItemMemoEdit1;
            this.layoutViewColumn2.FieldName = "Content";
            this.layoutViewColumn2.LayoutViewField = this.layoutViewField2;
            this.layoutViewColumn2.Name = "layoutViewColumn2";
            this.layoutViewColumn2.OptionsColumn.AllowEdit = false;
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            // 
            // layoutViewField2
            // 
            this.layoutViewField2.AllowHtmlStringInCaption = true;
            this.layoutViewField2.EditorPreferredWidth = 211;
            this.layoutViewField2.Location = new System.Drawing.Point(50, 0);
            this.layoutViewField2.Name = "layoutViewField2";
            this.layoutViewField2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutViewField2.Size = new System.Drawing.Size(211, 39);
            this.layoutViewField2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewField2.TextToControlDistance = 0;
            this.layoutViewField2.TextVisible = false;
            // 
            // layoutViewColumn3
            // 
            this.layoutViewColumn3.Caption = "Since";
            this.layoutViewColumn3.FieldName = "Since";
            this.layoutViewColumn3.LayoutViewField = this.layoutViewField3;
            this.layoutViewColumn3.Name = "layoutViewColumn3";
            this.layoutViewColumn3.OptionsColumn.AllowEdit = false;
            // 
            // layoutViewField3
            // 
            this.layoutViewField3.EditorPreferredWidth = 51;
            this.layoutViewField3.Location = new System.Drawing.Point(263, 0);
            this.layoutViewField3.MaxSize = new System.Drawing.Size(51, 22);
            this.layoutViewField3.MinSize = new System.Drawing.Size(51, 22);
            this.layoutViewField3.Name = "layoutViewField3";
            this.layoutViewField3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutViewField3.Size = new System.Drawing.Size(51, 39);
            this.layoutViewField3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutViewField3.TextLocation = DevExpress.Utils.Locations.Right;
            this.layoutViewField3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewField3.TextToControlDistance = 0;
            this.layoutViewField3.TextVisible = false;
            // 
            // layoutViewColumn10
            // 
            this.layoutViewColumn10.Caption = "ProfileImage";
            this.layoutViewColumn10.ColumnEdit = this.repositoryItemPictureEdit1;
            this.layoutViewColumn10.FieldName = "ProfileImage";
            this.layoutViewColumn10.LayoutViewField = this.layoutViewField_layoutViewColumn4;
            this.layoutViewColumn10.Name = "layoutViewColumn10";
            this.layoutViewColumn10.OptionsColumn.AllowEdit = false;
            // 
            // repositoryItemPictureEdit1
            // 
            this.repositoryItemPictureEdit1.Name = "repositoryItemPictureEdit1";
            // 
            // layoutViewField_layoutViewColumn4
            // 
            this.layoutViewField_layoutViewColumn4.EditorPreferredWidth = 46;
            this.layoutViewField_layoutViewColumn4.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_layoutViewColumn4.MaxSize = new System.Drawing.Size(65, 0);
            this.layoutViewField_layoutViewColumn4.MinSize = new System.Drawing.Size(1, 1);
            this.layoutViewField_layoutViewColumn4.Name = "layoutViewField_layoutViewColumn4";
            this.layoutViewField_layoutViewColumn4.Size = new System.Drawing.Size(50, 39);
            this.layoutViewField_layoutViewColumn4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutViewField_layoutViewColumn4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewField_layoutViewColumn4.TextToControlDistance = 0;
            this.layoutViewField_layoutViewColumn4.TextVisible = false;
            // 
            // layoutViewCard3
            // 
            this.layoutViewCard3.CustomizationFormText = "TemplateCard";
            this.layoutViewCard3.ExpandButtonLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutViewCard3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutViewField2,
            this.layoutViewField3,
            this.item4,
            this.layoutViewField_layoutViewColumn4});
            this.layoutViewCard3.Name = "layoutViewCard3";
            this.layoutViewCard3.OptionsItemText.TextToControlDistance = 5;
            this.layoutViewCard3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutViewCard3.Text = "TemplateCard";
            // 
            // item4
            // 
            this.item4.AllowHotTrack = false;
            this.item4.CustomizationFormText = "item1";
            this.item4.Location = new System.Drawing.Point(261, 0);
            this.item4.Name = "item4";
            this.item4.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.item4.Size = new System.Drawing.Size(2, 39);
            this.item4.Text = "item4";
            // 
            // gridControlAlarms
            // 
            this.gridControlAlarms.ContextMenuStrip = this.contextMenuStripEx1;
            this.gridControlAlarms.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlAlarms.EnableAutoFilter = false;
            this.gridControlAlarms.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlAlarms.Location = new System.Drawing.Point(2, 22);
            this.gridControlAlarms.MainView = this.gridViewAlarms;
            this.gridControlAlarms.Name = "gridControlAlarms";
            this.gridControlAlarms.Size = new System.Drawing.Size(298, 285);
            this.gridControlAlarms.TabIndex = 24;
            this.gridControlAlarms.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewAlarms});
            this.gridControlAlarms.ViewTotalRows = false;
            // 
            // gridViewAlarms
            // 
            this.gridViewAlarms.AllowFocusedRowChanged = true;
            this.gridViewAlarms.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewAlarms.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewAlarms.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewAlarms.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewAlarms.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewAlarms.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewAlarms.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewAlarms.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewAlarms.EnablePreviewLineForFocusedRow = false;
            this.gridViewAlarms.GridControl = this.gridControlAlarms;
            this.gridViewAlarms.Name = "gridViewAlarms";
            this.gridViewAlarms.OptionsBehavior.SmartVertScrollBar = false;
            this.gridViewAlarms.OptionsCustomization.AllowFilter = false;
            this.gridViewAlarms.OptionsDetail.AllowZoomDetail = false;
            this.gridViewAlarms.OptionsDetail.EnableMasterViewMode = false;
            this.gridViewAlarms.OptionsDetail.ShowDetailTabs = false;
            this.gridViewAlarms.OptionsDetail.SmartDetailExpand = false;
            this.gridViewAlarms.OptionsMenu.EnableColumnMenu = false;
            this.gridViewAlarms.OptionsMenu.EnableFooterMenu = false;
            this.gridViewAlarms.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridViewAlarms.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewAlarms.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gridViewAlarms.OptionsSelection.UseIndicatorForSelection = false;
            this.gridViewAlarms.OptionsView.ShowDetailButtons = false;
            this.gridViewAlarms.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewAlarms.OptionsView.ShowGroupPanel = false;
            this.gridViewAlarms.OptionsView.ShowIndicator = false;
            this.gridViewAlarms.ViewTotalRows = false;
            this.gridViewAlarms.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.GridViewAlarms_FocusedRowChanged);
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ApplicationButtonAnimationLength = 1;
            this.ribbonControl1.Dock = System.Windows.Forms.DockStyle.None;
            this.ribbonControl1.ExpandCollapseItem.AllowRightClickInMenu = false;
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.ExpandCollapseItem.Name = "";
            this.ribbonControl1.GroupAnimationLength = 1;
            this.ribbonControl1.ItemAnimationLength = 1;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.barButtonItemPrint,
            this.barButtonItemSave,
            this.barButtonItemRefresh,
            this.barButtonItemTweet});
            this.ribbonControl1.Location = new System.Drawing.Point(54, 495);
            this.ribbonControl1.MaxItemId = 29;
            this.ribbonControl1.MdiMergeStyle = DevExpress.XtraBars.Ribbon.RibbonMdiMergeStyle.Always;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.PageAnimationLength = 1;
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbonControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoEdit3});
            this.ribbonControl1.ShowExpandCollapseButton = DevExpress.Utils.DefaultBoolean.True;
            this.ribbonControl1.ShowToolbarCustomizeItem = false;
            this.ribbonControl1.Size = new System.Drawing.Size(823, 120);
            this.ribbonControl1.Toolbar.ShowCustomizeItem = false;
            this.ribbonControl1.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // barButtonItemPrint
            // 
            this.barButtonItemPrint.Caption = "ItemPrint";
            this.barButtonItemPrint.Enabled = false;
            this.barButtonItemPrint.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.Glyph")));
            this.barButtonItemPrint.Id = 6;
            this.barButtonItemPrint.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemPrint.Name = "barButtonItemPrint";
            // 
            // barButtonItemSave
            // 
            this.barButtonItemSave.Caption = "ItemSave";
            this.barButtonItemSave.Enabled = false;
            this.barButtonItemSave.Glyph = global::SmartCadGuiCommon.PrintRibbonControllerResources.RibbonPrintPreview_Save;
            this.barButtonItemSave.Id = 7;
            this.barButtonItemSave.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemSave.Name = "barButtonItemSave";
            // 
            // barButtonItemRefresh
            // 
            this.barButtonItemRefresh.Caption = "ItemRefresh";
            this.barButtonItemRefresh.Enabled = false;
            this.barButtonItemRefresh.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemRefresh.Glyph")));
            this.barButtonItemRefresh.Id = 8;
            this.barButtonItemRefresh.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemRefresh.Name = "barButtonItemRefresh";
            // 
            // barButtonItemTweet
            // 
            this.barButtonItemTweet.Caption = "barButtonItem1";
            this.barButtonItemTweet.Glyph = global::SmartCadGuiCommon.Properties.ResourcesGui.comment_edit_icon;
            this.barButtonItemTweet.Id = 27;
            this.barButtonItemTweet.Name = "barButtonItemTweet";
            this.barButtonItemTweet.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemTweet.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemTweet_ItemClick);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupGeneralOptions,
            this.ribbonPageGroupTwitter});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "ribbonPage1";
            // 
            // ribbonPageGroupGeneralOptions
            // 
            this.ribbonPageGroupGeneralOptions.AllowMinimize = false;
            this.ribbonPageGroupGeneralOptions.AllowTextClipping = false;
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemPrint);
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemSave);
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemRefresh);
            this.ribbonPageGroupGeneralOptions.Name = "ribbonPageGroupGeneralOptions";
            this.ribbonPageGroupGeneralOptions.ShowCaptionButton = false;
            this.ribbonPageGroupGeneralOptions.Text = "ribbonPageGroupGeneralOptions";
            this.ribbonPageGroupGeneralOptions.Visible = false;
            // 
            // ribbonPageGroupTwitter
            // 
            this.ribbonPageGroupTwitter.ItemLinks.Add(this.barButtonItemTweet);
            this.ribbonPageGroupTwitter.Name = "ribbonPageGroupTwitter";
            // 
            // repositoryItemMemoEdit3
            // 
            this.repositoryItemMemoEdit3.MaxLength = 140;
            this.repositoryItemMemoEdit3.Name = "repositoryItemMemoEdit3";
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel2,
            this.dockPanelMentions});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // dockPanel2
            // 
            this.dockPanel2.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.dockPanel2.Appearance.Options.UseBackColor = true;
            this.dockPanel2.Controls.Add(this.dockPanel2_Container);
            this.dockPanel2.Dock = DevExpress.XtraBars.Docking.DockingStyle.Right;
            this.dockPanel2.ID = new System.Guid("ac5c5b12-bccc-45a9-ace9-7ca83853e19b");
            this.dockPanel2.Location = new System.Drawing.Point(641, 0);
            this.dockPanel2.Name = "dockPanel2";
            this.dockPanel2.OriginalSize = new System.Drawing.Size(348, 200);
            this.dockPanel2.Size = new System.Drawing.Size(348, 647);
            this.dockPanel2.Text = "dockPanel2";
            // 
            // dockPanel2_Container
            // 
            this.dockPanel2_Container.Controls.Add(this.layoutControlSearch);
            this.dockPanel2_Container.Location = new System.Drawing.Point(4, 23);
            this.dockPanel2_Container.Name = "dockPanel2_Container";
            this.dockPanel2_Container.Size = new System.Drawing.Size(340, 620);
            this.dockPanel2_Container.TabIndex = 0;
            // 
            // layoutControlSearch
            // 
            this.layoutControlSearch.Controls.Add(this.buttonNewAlert);
            this.layoutControlSearch.Controls.Add(this.textSearch);
            this.layoutControlSearch.Controls.Add(this.gridControlSearchResults);
            this.layoutControlSearch.Controls.Add(this.buttonSearch);
            this.layoutControlSearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlSearch.Location = new System.Drawing.Point(0, 0);
            this.layoutControlSearch.Name = "layoutControlSearch";
            this.layoutControlSearch.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(308, 442, 250, 350);
            this.layoutControlSearch.Root = this.layoutControlGroup2;
            this.layoutControlSearch.Size = new System.Drawing.Size(340, 620);
            this.layoutControlSearch.TabIndex = 36;
            this.layoutControlSearch.Text = "layoutControl2";
            // 
            // buttonNewAlert
            // 
            this.buttonNewAlert.Enabled = false;
            this.buttonNewAlert.Location = new System.Drawing.Point(298, 4);
            this.buttonNewAlert.MaximumSize = new System.Drawing.Size(40, 22);
            this.buttonNewAlert.Name = "buttonNewAlert";
            this.buttonNewAlert.Size = new System.Drawing.Size(38, 22);
            this.buttonNewAlert.StyleController = this.layoutControlSearch;
            this.buttonNewAlert.TabIndex = 37;
            this.buttonNewAlert.Text = "+";
            this.buttonNewAlert.Click += new System.EventHandler(this.buttonNewAlert_Click);
            // 
            // textSearch
            // 
            this.textSearch.Location = new System.Drawing.Point(4, 4);
            this.textSearch.MenuManager = this.ribbonControl1;
            this.textSearch.Name = "textSearch";
            this.textSearch.Size = new System.Drawing.Size(206, 20);
            this.textSearch.StyleController = this.layoutControlSearch;
            this.textSearch.TabIndex = 36;
            this.textSearch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textSearch_KeyPress);
            // 
            // buttonSearch
            // 
            this.buttonSearch.Location = new System.Drawing.Point(214, 4);
            this.buttonSearch.MaximumSize = new System.Drawing.Size(80, 0);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(80, 22);
            this.buttonSearch.StyleController = this.layoutControlSearch;
            this.buttonSearch.TabIndex = 35;
            this.buttonSearch.Text = "simpleButton1";
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem5});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup2.Size = new System.Drawing.Size(340, 620);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridControlSearchResults;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(336, 590);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.buttonSearch;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(210, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(84, 26);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.textSearch;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(54, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(210, 26);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.buttonNewAlert;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(294, 0);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(42, 0);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(42, 26);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(42, 26);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // dockPanelMentions
            // 
            this.dockPanelMentions.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.dockPanelMentions.Appearance.Options.UseBackColor = true;
            this.dockPanelMentions.Controls.Add(this.dockPanel1_Container);
            this.dockPanelMentions.Dock = DevExpress.XtraBars.Docking.DockingStyle.Right;
            this.dockPanelMentions.ID = new System.Guid("c393ed65-f571-46fa-82a2-d3746463b28e");
            this.dockPanelMentions.Location = new System.Drawing.Point(310, 0);
            this.dockPanelMentions.Name = "dockPanelMentions";
            this.dockPanelMentions.OriginalSize = new System.Drawing.Size(331, 200);
            this.dockPanelMentions.Size = new System.Drawing.Size(331, 647);
            this.dockPanelMentions.Text = "Mentions";
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.layoutControl2);
            this.dockPanel1_Container.Location = new System.Drawing.Point(4, 23);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(323, 620);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.gridControlMentions);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.layoutControlGroup3;
            this.layoutControl2.Size = new System.Drawing.Size(323, 620);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // gridControlMentions
            // 
            this.gridControlMentions.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlMentions.Location = new System.Drawing.Point(4, 4);
            this.gridControlMentions.MainView = this.gridViewMentions;
            this.gridControlMentions.Name = "gridControlMentions";
            this.gridControlMentions.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoEdit2,
            this.repositoryItemPictureEdit2});
            this.gridControlMentions.Size = new System.Drawing.Size(315, 612);
            this.gridControlMentions.TabIndex = 34;
            this.gridControlMentions.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewMentions});
            this.gridControlMentions.Click += new System.EventHandler(this.gridControlMentions_Click);
            this.gridControlMentions.Enter += new System.EventHandler(this.gridControlMentions_Enter);
            this.gridControlMentions.Leave += new System.EventHandler(this.gridControlMentions_Leave);
            // 
            // gridViewMentions
            // 
            this.gridViewMentions.CardCaptionFormat = "{2}";
            this.gridViewMentions.CardHorzInterval = 0;
            this.gridViewMentions.CardMinSize = new System.Drawing.Size(316, 61);
            this.gridViewMentions.CardVertInterval = 6;
            this.gridViewMentions.Columns.AddRange(new DevExpress.XtraGrid.Columns.LayoutViewColumn[] {
            this.layoutViewColumn5,
            this.layoutViewColumn6,
            this.layoutViewColumn11,
            this.layoutViewColumn4});
            this.gridViewMentions.GridControl = this.gridControlMentions;
            this.gridViewMentions.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutViewField_layoutViewColumn1});
            this.gridViewMentions.Name = "gridViewMentions";
            this.gridViewMentions.OptionsBehavior.AllowRuntimeCustomization = false;
            this.gridViewMentions.OptionsBehavior.Editable = false;
            this.gridViewMentions.OptionsBehavior.ScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Auto;
            this.gridViewMentions.OptionsCustomization.AllowFilter = false;
            this.gridViewMentions.OptionsCustomization.AllowSort = false;
            this.gridViewMentions.OptionsCustomization.ShowGroupCardCaptions = false;
            this.gridViewMentions.OptionsCustomization.ShowGroupCardIndents = false;
            this.gridViewMentions.OptionsCustomization.ShowGroupCards = false;
            this.gridViewMentions.OptionsCustomization.ShowGroupFields = false;
            this.gridViewMentions.OptionsCustomization.ShowGroupHiddenItems = false;
            this.gridViewMentions.OptionsCustomization.ShowGroupLayout = false;
            this.gridViewMentions.OptionsCustomization.ShowGroupLayoutTreeView = false;
            this.gridViewMentions.OptionsCustomization.ShowGroupView = false;
            this.gridViewMentions.OptionsCustomization.ShowSaveLoadLayoutButtons = false;
            this.gridViewMentions.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridViewMentions.OptionsFilter.AllowFilterEditor = false;
            this.gridViewMentions.OptionsMultiRecordMode.StretchCardToViewHeight = true;
            this.gridViewMentions.OptionsMultiRecordMode.StretchCardToViewWidth = true;
            this.gridViewMentions.OptionsView.AllowHotTrackFields = false;
            this.gridViewMentions.OptionsView.CardArrangeRule = DevExpress.XtraGrid.Views.Layout.LayoutCardArrangeRule.AllowPartialCards;
            this.gridViewMentions.OptionsView.ShowCardLines = false;
            this.gridViewMentions.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewMentions.OptionsView.ShowHeaderPanel = false;
            this.gridViewMentions.OptionsView.ViewMode = DevExpress.XtraGrid.Views.Layout.LayoutViewMode.Column;
            this.gridViewMentions.PaintStyleName = "Skin";
            this.gridViewMentions.TemplateCard = this.layoutViewCard1;
            // 
            // layoutViewColumn5
            // 
            this.layoutViewColumn5.Caption = "Author";
            this.layoutViewColumn5.FieldName = "Author";
            this.layoutViewColumn5.LayoutViewField = this.layoutViewField_layoutViewColumn1;
            this.layoutViewColumn5.Name = "layoutViewColumn5";
            this.layoutViewColumn5.OptionsColumn.AllowEdit = false;
            // 
            // layoutViewField_layoutViewColumn1
            // 
            this.layoutViewField_layoutViewColumn1.EditorPreferredWidth = 316;
            this.layoutViewField_layoutViewColumn1.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_layoutViewColumn1.MaxSize = new System.Drawing.Size(0, 20);
            this.layoutViewField_layoutViewColumn1.MinSize = new System.Drawing.Size(24, 20);
            this.layoutViewField_layoutViewColumn1.Name = "layoutViewField_layoutViewColumn1";
            this.layoutViewField_layoutViewColumn1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutViewField_layoutViewColumn1.Size = new System.Drawing.Size(314, 39);
            this.layoutViewField_layoutViewColumn1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutViewField_layoutViewColumn1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewField_layoutViewColumn1.TextToControlDistance = 0;
            this.layoutViewField_layoutViewColumn1.TextVisible = false;
            // 
            // layoutViewColumn6
            // 
            this.layoutViewColumn6.Caption = "Tweet";
            this.layoutViewColumn6.ColumnEdit = this.repositoryItemMemoEdit2;
            this.layoutViewColumn6.FieldName = "Content";
            this.layoutViewColumn6.LayoutViewField = this.layoutViewField_colContent3;
            this.layoutViewColumn6.Name = "layoutViewColumn6";
            this.layoutViewColumn6.OptionsColumn.AllowEdit = false;
            // 
            // repositoryItemMemoEdit2
            // 
            this.repositoryItemMemoEdit2.Name = "repositoryItemMemoEdit2";
            // 
            // layoutViewField_colContent3
            // 
            this.layoutViewField_colContent3.AllowHtmlStringInCaption = true;
            this.layoutViewField_colContent3.EditorPreferredWidth = 213;
            this.layoutViewField_colContent3.Location = new System.Drawing.Point(48, 0);
            this.layoutViewField_colContent3.Name = "layoutViewField_colContent3";
            this.layoutViewField_colContent3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutViewField_colContent3.Size = new System.Drawing.Size(213, 39);
            this.layoutViewField_colContent3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewField_colContent3.TextToControlDistance = 0;
            this.layoutViewField_colContent3.TextVisible = false;
            // 
            // layoutViewColumn11
            // 
            this.layoutViewColumn11.Caption = "Since";
            this.layoutViewColumn11.FieldName = "Since";
            this.layoutViewColumn11.LayoutViewField = this.layoutViewField_colSince3;
            this.layoutViewColumn11.Name = "layoutViewColumn11";
            this.layoutViewColumn11.OptionsColumn.AllowEdit = false;
            // 
            // layoutViewField_colSince3
            // 
            this.layoutViewField_colSince3.EditorPreferredWidth = 51;
            this.layoutViewField_colSince3.Location = new System.Drawing.Point(263, 0);
            this.layoutViewField_colSince3.MaxSize = new System.Drawing.Size(51, 22);
            this.layoutViewField_colSince3.MinSize = new System.Drawing.Size(51, 22);
            this.layoutViewField_colSince3.Name = "layoutViewField_colSince3";
            this.layoutViewField_colSince3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutViewField_colSince3.Size = new System.Drawing.Size(51, 39);
            this.layoutViewField_colSince3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutViewField_colSince3.TextLocation = DevExpress.Utils.Locations.Right;
            this.layoutViewField_colSince3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewField_colSince3.TextToControlDistance = 0;
            this.layoutViewField_colSince3.TextVisible = false;
            // 
            // layoutViewColumn4
            // 
            this.layoutViewColumn4.Caption = "ProfileImage";
            this.layoutViewColumn4.ColumnEdit = this.repositoryItemPictureEdit2;
            this.layoutViewColumn4.FieldName = "ProfileImage";
            this.layoutViewColumn4.LayoutViewField = this.layoutViewField_colProfileImage3;
            this.layoutViewColumn4.Name = "layoutViewColumn4";
            this.layoutViewColumn4.OptionsColumn.AllowEdit = false;
            // 
            // repositoryItemPictureEdit2
            // 
            this.repositoryItemPictureEdit2.Name = "repositoryItemPictureEdit2";
            // 
            // layoutViewField_colProfileImage3
            // 
            this.layoutViewField_colProfileImage3.EditorPreferredWidth = 44;
            this.layoutViewField_colProfileImage3.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_colProfileImage3.MaxSize = new System.Drawing.Size(65, 0);
            this.layoutViewField_colProfileImage3.MinSize = new System.Drawing.Size(1, 1);
            this.layoutViewField_colProfileImage3.Name = "layoutViewField_colProfileImage3";
            this.layoutViewField_colProfileImage3.Size = new System.Drawing.Size(48, 39);
            this.layoutViewField_colProfileImage3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutViewField_colProfileImage3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewField_colProfileImage3.TextToControlDistance = 0;
            this.layoutViewField_colProfileImage3.TextVisible = false;
            // 
            // layoutViewCard1
            // 
            this.layoutViewCard1.CustomizationFormText = "TemplateCard";
            this.layoutViewCard1.ExpandButtonLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutViewCard1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutViewField_colContent3,
            this.layoutViewField_colSince3,
            this.item5,
            this.layoutViewField_colProfileImage3});
            this.layoutViewCard1.Name = "layoutViewCard1";
            this.layoutViewCard1.OptionsItemText.TextToControlDistance = 5;
            this.layoutViewCard1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutViewCard1.Text = "TemplateCard";
            // 
            // item5
            // 
            this.item5.AllowHotTrack = false;
            this.item5.CustomizationFormText = "item1";
            this.item5.Location = new System.Drawing.Point(261, 0);
            this.item5.Name = "item5";
            this.item5.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.item5.Size = new System.Drawing.Size(2, 39);
            this.item5.Text = "item5";
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemMentions});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup3.Size = new System.Drawing.Size(323, 620);
            this.layoutControlGroup3.Text = "layoutControlGroup3";
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlItemMentions
            // 
            this.layoutControlItemMentions.Control = this.gridControlMentions;
            this.layoutControlItemMentions.CustomizationFormText = "layoutControlItemMentions";
            this.layoutControlItemMentions.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemMentions.Name = "layoutControlItemMentions";
            this.layoutControlItemMentions.Size = new System.Drawing.Size(319, 616);
            this.layoutControlItemMentions.Text = "layoutControlItemMentions";
            this.layoutControlItemMentions.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemMentions.TextToControlDistance = 0;
            this.layoutControlItemMentions.TextVisible = false;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.groupControlAlarmTweets);
            this.layoutControl1.Controls.Add(this.groupControlAlarms);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(310, 647);
            this.layoutControl1.TabIndex = 30;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // groupControlAlarmTweets
            // 
            this.groupControlAlarmTweets.Controls.Add(this.gridControlAlarmTweets);
            this.groupControlAlarmTweets.Location = new System.Drawing.Point(4, 317);
            this.groupControlAlarmTweets.Name = "groupControlAlarmTweets";
            this.groupControlAlarmTweets.Size = new System.Drawing.Size(302, 326);
            this.groupControlAlarmTweets.TabIndex = 37;
            this.groupControlAlarmTweets.Text = "Related Tweets";
            // 
            // groupControlAlarms
            // 
            this.groupControlAlarms.Controls.Add(this.gridControlAlarms);
            this.groupControlAlarms.Location = new System.Drawing.Point(4, 4);
            this.groupControlAlarms.Name = "groupControlAlarms";
            this.groupControlAlarms.Size = new System.Drawing.Size(302, 309);
            this.groupControlAlarms.TabIndex = 36;
            this.groupControlAlarms.Text = "Alarms";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6,
            this.layoutControlItem4});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(310, 647);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.groupControlAlarms;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(306, 313);
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.groupControlAlarmTweets;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 313);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(306, 330);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // item2
            // 
            this.item2.AllowHotTrack = false;
            this.item2.CustomizationFormText = "item1";
            this.item2.Location = new System.Drawing.Point(261, 0);
            this.item2.Name = "item2";
            this.item2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.item2.Size = new System.Drawing.Size(2, 39);
            this.item2.Text = "item2";
            // 
            // TwitterAlarmsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(989, 647);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.dockPanelMentions);
            this.Controls.Add(this.dockPanel2);
            this.Controls.Add(this.ribbonControl1);
            this.Name = "TwitterAlarmsForm";
            this.Text = "Events";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TwitterAlarmsForm_FormClosing);
            this.Load += new System.EventHandler(this.TwitterAlarmsForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSeachResults)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colAuthor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colContent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colSince1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colProfileImage1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSearchResults)).EndInit();
            this.contextMenuStripEx1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAlarmTweets)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTweets)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colAuthor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAlarms)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewAlarms)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanel2.ResumeLayout(false);
            this.dockPanel2_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlSearch)).EndInit();
            this.layoutControlSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textSearch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            this.dockPanelMentions.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlMentions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMentions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colContent3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colSince3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colProfileImage3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemMentions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControlAlarmTweets)).EndInit();
            this.groupControlAlarmTweets.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControlAlarms)).EndInit();
            this.groupControlAlarms.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private GridControlEx gridControlAlarms;
        private GridViewEx gridViewAlarms;
        private ContextMenuStripEx contextMenuStripEx1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemCancel;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.BarButtonItem barButtonItemPrint;
        private DevExpress.XtraBars.BarButtonItem barButtonItemSave;
        private DevExpress.XtraBars.BarButtonItem barButtonItemRefresh;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupGeneralOptions;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel2;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel2_Container;
        private DevExpress.XtraGrid.GridControl gridControlSearchResults;
        private DevExpress.XtraLayout.LayoutControl layoutControlSearch;
        private DevExpress.XtraEditors.TextEdit textSearch;
        private DevExpress.XtraEditors.SimpleButton buttonSearch;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraGrid.Views.Layout.LayoutView gridViewSeachResults;
        private DevExpress.XtraGrid.GridControl gridControlAlarmTweets;
        private DevExpress.XtraGrid.Views.Layout.LayoutView gridViewTweets;
        private DevExpress.XtraEditors.GroupControl groupControlAlarmTweets;
        private DevExpress.XtraEditors.GroupControl groupControlAlarms;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.SimpleButton buttonNewAlert;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit3;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupTwitter;
        private DevExpress.XtraBars.BarButtonItem barButtonItemTweet;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelMentions;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraGrid.GridControl gridControlMentions;
        private DevExpress.XtraGrid.Views.Layout.LayoutView gridViewMentions;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemMentions;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn1;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn2;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn3;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn4;
        private DevExpress.XtraLayout.SimpleSeparator item2;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn10;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn7;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colAuthor2;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn8;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit4;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colContent1;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn9;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colSince1;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn12;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit3;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colProfileImage1;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewCard layoutViewCard2;
        private DevExpress.XtraLayout.SimpleSeparator item1;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn5;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn1;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn6;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit2;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colContent3;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn11;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colSince3;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit2;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colProfileImage3;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewCard layoutViewCard1;
        private DevExpress.XtraLayout.SimpleSeparator item5;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colAuthor1;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField2;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField3;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn4;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewCard layoutViewCard3;
        private DevExpress.XtraLayout.SimpleSeparator item4;
    }
}