using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Globalization;
using System.Drawing.Printing;
using System.Threading;
using System.Diagnostics;
using Smartmatic.SmartCad.Service;
using System.Reflection;

using System.Xml;
using System.Xml.Xsl;
using System.Net;
using System.ServiceModel;
using Microsoft.Win32;
using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadControls;
using SmartCadCore.ClientData;
using SmartCadControls.Filters;
using SmartCadControls.SyncBoxes;
using SmartCadControls.Controls;
using SmartCadGuiCommon.Services;
using SmartCadGuiCommon.Util;
using SmartCadCore.Enums;
using SmartCadFirstLevel.Gui.SyncBoxes;
using SmartCadGuiCommon.Enums;
using SmartCadGuiCommon.Classes;
using SmartCadGuiCommon;
using SmartCadGuiCommon.Controls;

namespace SmartCadFirstLevel.Gui
{
	public partial class DefaultTwitterFrontClientForm : DevExpress.XtraEditors.XtraForm
    {
        #region Fields
        private ServerServiceClient serverServiceClient;
        private TwitterReportClientData globalTwitterReport;
        private bool isActiveCall = false;
        private bool incidentAdded = false;
        private bool callEntering = false;
        private ImageList activeStepImages;
        private ImageList inactiveStepImages;
        private FilterBase incidentsCurrentFilter;
        private ToolStripMenuItem toolStripMenuItem;
        private SkinEngine skinEngine;
        private System.Threading.Timer answersUpdatedTimer;
        private EventArgs answersUpdatedTimerEventArgs;
        private Dictionary<int, IncidentTypeClientData> globalIncidentTypes;
        private Dictionary<int,DepartmentTypeClientData> globalDepartmentTypes;
        private Dictionary<string, IncidentNotificationPriorityClientData> globalNotificationPriorities;
        private IList globalIncidents;
        private Dictionary<string, ApplicationPreferenceClientData> globalApplicationPreference;
        private object syncAssociatedInfo;
        private IList updatedAnswers = new ArrayList();
        private XslCompiledTransform xslCompiledTransform;       
        private ConfigurationClientData configurationClient;        
        private GridControlSynBox incidentSyncBox;        
        private NetworkCredential networkCredential;          
        private int selectedIncidentCode = -1;        
        private System.Threading.Timer refreshIncidentReport;
        public string password;
        private object IEFooter;
        private string objectValue;
        public string ExtNumber;
        private const string SourceDirectory = "Software\\Microsoft\\Internet Explorer\\PageSetup";
        private bool searchByText = false;
        private bool popUpShow = true;       
        private object sync = new object();
        private ArrayList IncidentChanges = new ArrayList();
        private ArrayList QuestionChanges = new ArrayList();
		private ArrayList DepartmentTypeChanges = new ArrayList();
        private FrontClientStateEnum frontClientState = FrontClientStateEnum.None;
        private Dictionary<int, IncidentTypeClientData> incidentTypeActionsUpdate;
        private Dictionary<int, IncidentTypeClientData> incidentTypeActionDelete;
        private Dictionary<int, IncidentTypeClientData> incidentTypeActionAdd;
        private Dictionary<int, QuestionClientData> questionActionsUpdate;
        private Dictionary<int, QuestionClientData> questionActionDelete;
        private Dictionary<int, QuestionClientData> questionActionAdd;

        #endregion

        #region Properties       
        
        public FrontClientStateEnum FrontClientState
        {
            get
            {
                return this.frontClientState;
            }
            set
            {
                this.frontClientState = value;
                if (value == FrontClientStateEnum.WaitingForIncident)
                    SetFrontClientWaitingForCallState();
                else if (value == FrontClientStateEnum.RegisteringIncident)
                    SetFrontClientRegisteringCallState();                
            }
        }

        public IList GlobalIncidents
        {
            get
            {
                return globalIncidents;
            }
            set
            {
                globalIncidents = value;
            }
        }



        public TwitterReportClientData GlobalTwitterReport 
        {
            get
            {
                return globalTwitterReport;
            }
            set
            {
                globalTwitterReport = value;
            }
        }

        object syncGlobalIncidentTypes = new object();
        public Dictionary<int, IncidentTypeClientData> GlobalIncidentTypes
        {
            get
            {
                lock (syncGlobalIncidentTypes)
                {
                    return globalIncidentTypes;
                }
            }
            set
            {
                lock (sync)
                {
                    globalIncidentTypes = value;

                    foreach (IncidentTypeClientData itcd in incidentTypeActionAdd.Values)
                    {
                        if (globalIncidentTypes.ContainsKey(itcd.Code) == false)
                        {
                            if (itcd.NoEmergency == false)
                            {
                                globalIncidentTypes.Add(itcd.Code, itcd);
                            }
                        }
                    }
                    incidentTypeActionAdd.Clear();

                    foreach (IncidentTypeClientData itcd in incidentTypeActionsUpdate.Values)
                    {
                        if (globalIncidentTypes.ContainsKey(itcd.Code) == true)
                            globalIncidentTypes[itcd.Code] = itcd;
                    }
                    incidentTypeActionsUpdate.Clear();

                    foreach (IncidentTypeClientData itcd in incidentTypeActionDelete.Values)
                    {
                        if (globalIncidentTypes.ContainsKey(itcd.Code) == true)
                            globalIncidentTypes.Remove(itcd.Code);
                    }
                    incidentTypeActionDelete.Clear();

                    foreach (QuestionClientData qcd in questionActionAdd.Values)
                    {
                        AddQuestion(qcd);
                    }
                    questionActionAdd.Clear();

                    foreach (QuestionClientData qcd in questionActionsUpdate.Values)
                    {
                        UpdateQuestion(qcd);
                    }
                    questionActionsUpdate.Clear();

                    foreach (QuestionClientData qcd in questionActionDelete.Values)
                    {
                        DeleteQuestion(qcd);
                    }
                    questionActionDelete.Clear();

                    questionsControl.GlobalIncidentTypes = globalIncidentTypes;
                }
            }
        }

        public Dictionary<int, DepartmentTypeClientData> GlobalDepartmentTypes
        {
            get
            {
                return this.globalDepartmentTypes;
            }
            set
            {
                this.globalDepartmentTypes = value;
                this.departmentsInvolvedControl.departmentTypesControl.GlobalDepartmentTypes = globalDepartmentTypes;
            }
        }

        public Dictionary<string, IncidentNotificationPriorityClientData> GlobalNotificationPriorities
        {
            get
            {
                return this.globalNotificationPriorities;
            }
            set
            {
                
                this.globalNotificationPriorities = value;
                this.departmentsInvolvedControl.departmentTypesControl.GlobalNotificationPriorities = globalNotificationPriorities;
            }
        }

        public Dictionary<string,ApplicationPreferenceClientData> GlobalApplicationPreference
        {
            get
            {
                return globalApplicationPreference;
            }
            set
            {
                globalApplicationPreference = value;
            }
        }

        public ServerServiceClient ServerServiceClient
        {
            get
            {
                return serverServiceClient;
            }
            set
            {
                serverServiceClient = value;
            }
        }
              

        internal QuestionsControlDevX QuestionsTwitterControl
        {
            get
            {
                return this.questionsControl;
            }
        }

        public NetworkCredential NetworkCredential
        {
            get
            {
                return networkCredential;
            }

            set
            {
                networkCredential = value;
            }
        }

        public ConfigurationClientData ConfigurationClient
        {
            get
            {
                return configurationClient;
            }
            set
            {
                configurationClient = value;
            }
        }

        public string Password { get; set; }

        #endregion

        #region Constructors

        public DefaultTwitterFrontClientForm()
        {
            InitializeComponent();
            globalIncidents = new ArrayList();
            globalDepartmentTypes = new Dictionary<int, DepartmentTypeClientData>();
            globalNotificationPriorities = new Dictionary<string, IncidentNotificationPriorityClientData>();
            globalIncidentTypes = new Dictionary<int, IncidentTypeClientData>();
            
            syncAssociatedInfo = new object();
            refreshIncidentReport = new System.Threading.Timer(new TimerCallback(OnRefreshIncidentReportTimer));

            incidentTypeActionsUpdate = new Dictionary<int, IncidentTypeClientData>();
            incidentTypeActionDelete = new Dictionary<int, IncidentTypeClientData>();
            incidentTypeActionAdd = new Dictionary<int, IncidentTypeClientData>();
            questionActionsUpdate = new Dictionary<int, QuestionClientData>();
            questionActionDelete = new Dictionary<int, QuestionClientData>();
            questionActionAdd = new Dictionary<int, QuestionClientData>();

            ServerServiceClient.GetInstance().CommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(serverService_CommittedChanges);
        }

        #endregion

        public void LoadInitialData()
        {
            #region Loading Incident Types and questions
			IList clientIncidentTypes = ServerServiceClient.GetInstance().SearchClientObjectsMaxRows(
                SmartCadHqls.GetCustomHql(
                    SmartCadHqls.IncidentTypesWithDepartmentTypesQuestions, UserApplicationClientData.SocialNetworks.Code), true);
            foreach (IncidentTypeClientData incidentTypeClient in clientIncidentTypes)
            {
                if (globalIncidentTypes.ContainsKey(incidentTypeClient.Code))
                {
                    if (globalIncidentTypes[incidentTypeClient.Code].Version < incidentTypeClient.Version)
                    {
                        globalIncidentTypes[incidentTypeClient.Code] = incidentTypeClient;
                    }
                }
                else
                {
                    if (incidentTypeClient.NoEmergency == false)
                    {
                        globalIncidentTypes.Add(incidentTypeClient.Code, incidentTypeClient);
                    }
                }
            }
            this.GlobalIncidentTypes = globalIncidentTypes;
            #endregion

            #region Loading DepartmentTypes
            IList tempDepartmentTypes = ServerServiceClient.GetInstance().SearchClientObjects(
                     SmartCadHqls.GetDepartmentsTypeWithStationsAssociated, true);
            
            foreach (DepartmentTypeClientData departmentType in tempDepartmentTypes)
            {
                if (globalDepartmentTypes.ContainsKey(departmentType.Code))
                {
                    if (globalDepartmentTypes[departmentType.Code].Version < departmentType.Version)
                    {
                        globalDepartmentTypes[departmentType.Code] = departmentType;
                    }
                }
                else
                {
                    globalDepartmentTypes.Add(departmentType.Code, departmentType);
                }
            }
            this.GlobalDepartmentTypes = globalDepartmentTypes;
            #endregion

            #region Loading IncidentNotificationPriorities
            IList tempNotificationPriorities = ServerServiceClient.GetInstance().SearchClientObjectsMaxRows(
                SmartCadHqls.GetIncidentNotificationPriorities, true);            
            foreach (IncidentNotificationPriorityClientData notificationPriority in tempNotificationPriorities)
            {
                if (globalNotificationPriorities.ContainsKey(notificationPriority.CustomCode))
                {
                    if (globalNotificationPriorities[notificationPriority.CustomCode].Version < notificationPriority.Version)
                    {
                        globalNotificationPriorities[notificationPriority.CustomCode] = notificationPriority;
                    }
                }
                else
                {
                    globalNotificationPriorities.Add(notificationPriority.CustomCode, notificationPriority);
                }
            }
            GlobalNotificationPriorities = globalNotificationPriorities;
            #endregion

            #region Loading ApplicationPreferences
            GlobalApplicationPreference = new Dictionary<string, ApplicationPreferenceClientData>();
            foreach (ApplicationPreferenceClientData preference in 
                ServerServiceClient.GetInstance().SearchClientObjectsMaxRows(
                SmartCadHqls.GetAllApplicationPreferences, true))
            {
                GlobalApplicationPreference.Add(preference.Name, preference);
            }
            #endregion

            #region Loading Incidents
            IList incidentList = ServerServiceClient.GetInstance().SearchClientObjectsMaxRows(
                SmartCadHqls.GetCustomHql(
                SmartCadHqls.NewIncidentRuleEvaluate, UserApplicationClientData.SocialNetworks.Code), true);
            int index = -1;
            foreach (IncidentClientData incidentClientData in incidentList)
            {
                if (incidentClientData.SourceIncidentApp == SourceIncidentAppEnum.SocialNetworks)
                {
                    index = globalIncidents.IndexOf(incidentClientData);
                    if (index != -1 && (globalIncidents[index] as IncidentClientData).Version < incidentClientData.Version)
                    {
                        globalIncidents[index] = incidentClientData;
                    }
                    else if (index == -1)
                    {
                        globalIncidents.Add(incidentClientData);
                    }
                }
            }
            this.GlobalIncidents = globalIncidents;
            
            #endregion
        }

        private void DefaultTwitterFrontClientForm_Load(object sender, EventArgs e)
        {
            SetSkin();

            XmlReaderSettings xmlReaderSettings = new XmlReaderSettings() { ProhibitDtd = false };

            xslCompiledTransform = new XslCompiledTransform();
            xslCompiledTransform.Load(XmlReader.Create(SmartCadConfiguration.AlarmIncidentXslt, xmlReaderSettings));

            this.incidentInformationControl.defaultTwitterFrontClientForm = this;

            departmentsInvolvedControl.departmentTypesControl.Left = 
                (departmentsInvolvedControl.Size.Width - departmentsInvolvedControl.departmentTypesControl.Size.Width)/2;                      
            
            //Subscribing to events needed.
            questionsControl.AnswerUpdated += questionsControl_AnswerUpdated;

            questionsControl.IncidentTypesSelected += questionsTwitterControl_IncidentTypeSelected;
            this.departmentsInvolvedControl.departmentTypesControl.DepartmentTypesSelected += departmentTypesControl2_DepartmentTypesSelected;
            questionsControl.Enter += questionsTwitterControl_Enter;
            this.departmentsInvolvedControl.Enter += departmentsInvolvedControl_Enter;
            incidentInformationControl.Enter += callReceiverControl1_Enter;
            KeyDown += DefaultTwitterFrontClientForm_KeyDown;

			
            //Filling needed information...
            LoadStepImages();

            //Setting up the FrontClient 
            this.FrontClientState = FrontClientStateEnum.WaitingForIncident;
            ArrayList parameters = new ArrayList();
            parameters.Add("OPEN");      

            configurationClient = ServerServiceClient.GetInstance().GetConfiguration();
            answersUpdatedTimer = new System.Threading.Timer(OnAnswerUpdateTimer);
            CleanForm();
            questionsControl.removeIncident += questionsTwitterControl_removeIncident;
            departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.ClearSelection();
			LoadLanguage();
			questionsControl.clientMode = FrontClientMode.Lpr;

            barButtonItemStartRegIncident.LargeGlyph = ResourceLoader.GetImage("$Image.RecordOff");

			gridControlExIncidentList.Type = typeof(GridIncidentData);
			incidentSyncBox = new GridControlSynBox(gridControlExIncidentList);
			incidentSyncBox.Sync(globalIncidents);
			gridControlExIncidentList.ViewTotalRows = true;
			gridControlExIncidentList.EnableAutoFilter = true;
			gridViewExIncidentList.OptionsView.ShowAutoFilterRow = true;
			gridViewExIncidentList.ViewTotalRows = true;
			gridControlExIncidentList.AllowDrop = false;
            gridControlExIncidentList.CellToolTipNeeded += gridControlExIncidentList_CellToolTipNeeded;
            gridViewExIncidentList.SelectionWillChange += gridViewEx1_SelectionWillChange;
            gridViewExIncidentList.SingleSelectionChanged += gridViewExIncidentList_SingleSelectionChanged;
        }

        void departmentsInvolvedControl_Enter(object sender, EventArgs e)
        {
            if (this.FrontClientState != FrontClientStateEnum.WaitingForIncident)
            {
                ChangeCurrentStep(3, false);
            }
        }


		void gridControlExIncidentList_CellToolTipNeeded(object sender, DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventArgs e)
		{
			if (e.Info ==null)
			{
				DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo info = ((GridViewEx)gridControlExIncidentList.MainView).CalcHitInfo(e.ControlMousePosition);
				if (info.RowHandle > -1)
				{
					e.Info = new DevExpress.Utils.ToolTipControlInfo();
					e.Info.Object = gridControlExIncidentList;

					if (this.gridViewExIncidentList.GetRow(info.RowHandle) != null)
					{
						IncidentClientData incidentData = (IncidentClientData)((GridIncidentData)((IList)gridControlExIncidentList.DataSource)[info.RowHandle]).Tag;

						string toolTipText = "";

						if (incidentData != null)
						{
							foreach (IncidentTypeClientData incidentType in incidentData.IncidentTypes)
							{
								if (incidentType.NoEmergency != true)
                                    toolTipText = String.Format("{0}{1}, ", toolTipText, incidentType.FriendlyName);
							}
						}
						if (toolTipText.Trim() != "")
						{
							toolTipText = toolTipText.Substring(0, toolTipText.Length - 2);
						}
						e.Info.Text = toolTipText;
					}
				}
			}
		}

		private void LoadLanguage()
		{
			barButtonItemPrint.Caption = ResourceLoader.GetString2("Print");
			barButtonItemRefresh.Caption = ResourceLoader.GetString2("Refresh");
			barButtonItemSave.Caption = ResourceLoader.GetString2("Save"); 
			barButtonItemCancelIncident.Caption = ResourceLoader.GetString2("Cancel");
			barButtonItemCreateNewIncident.Caption = ResourceLoader.GetString2("Register");
			barButtonItemStartRegIncident.Caption = ResourceLoader.GetString2("New");
            barButtonItemCancelAlarm.Caption = ResourceLoader.GetString2("CancelAlert");
			ribbonPageGroupCallInfo.Text = ResourceLoader.GetString2("Incidents");
			ribbonPageGroupGeneralOptions.Text = ResourceLoader.GetString2("GeneralOptions");
			ribbonPage1.Text = ResourceLoader.GetString2("SocialNetworks");
            this.Text = ResourceLoader.GetString2("DefaultTwitterFrontClientFormDevX");
            layoutControlDefaultTwitterFrontClient.Text = ResourceLoader.GetString2("DefaultTwitterFrontClientFormDevX");
			layoutControlGroup3.Text = ResourceLoader.GetString2("IncidentList");
			layoutControlGroup4.Text = ResourceLoader.GetString2("IncidentDetails");
			dockPanel1.Text = ResourceLoader.GetString2("HeaderRegIncidents");

		}
       
        private void questionsTwitterControl_removeIncident(IList selectedIncidentTypes)
        {
            IncidentTypeDepartmentTypeClientData department;
            IncidentTypeClientData incidentType;
            ArrayList updateIncident = new ArrayList();
            foreach (IncidentTypeClientData incident in selectedIncidentTypes)
            {
                if (GlobalTwitterReport.IncidentTypesCodes != null)
                    GlobalTwitterReport.IncidentTypesCodes.Remove(incident.Code);
                               
            }
            for (int j = 0; j < departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.RowCount; j++)
            {
                if (departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.GetRowCellValue(j,"Prioridad") == null)
                    departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.SetRowCellValue(j," ",false);
            }
            if (globalTwitterReport.IncidentTypesCodes != null)
            {
                foreach (int incidentCode in GlobalTwitterReport.IncidentTypesCodes)
                {
                    for (int k = 0; k < this.questionsControl.SelectedIncidentTypes.Count; k++)
                    {
                        incidentType = (IncidentTypeClientData)this.questionsControl.SelectedIncidentTypes[k];
                        if (incidentType.Code == incidentCode)
                            updateIncident.Add(incidentType);
                    }
                }
            }
            foreach (IncidentTypeClientData incident in updateIncident)
            {
                for (int i = 0; i < incident.IncidentTypeDepartmentTypeClients.Count; i++)
                {
                    department = (IncidentTypeDepartmentTypeClientData)incident.IncidentTypeDepartmentTypeClients[i];
                    for (int j = 0; j < departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.RowCount; j++)
                    {
                        if (department.DepartmentType.Code == (((GridDepartmentTypeData)((IList)departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.DataSource)[j]).Tag as DepartmentTypeClientData).Code)
                        {
                            departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.SetRowCellValue(j," ", true);
                        }
                    }
                }
            }
			this.questionsControl.UpdateTextBoxSelectedIncidentTypes(updateIncident);
        }
		
        private void gridViewEx1_SelectionWillChange(object sender, EventArgs e)
        {
           
			barButtonItemSave.Enabled = false;
			barButtonItemPrint.Enabled = false;
			barButtonItemRefresh.Enabled = false;           
           
            if (textBoxExIncidentdetails.IsDisposed == false)
                textBoxExIncidentdetails.DocumentText = "<span style='HEIGHT:100%;FONT-SIZE: 10pt; FONT-FAMILY: Sans-Serif'>" + ResourceLoader.GetString2("NoIncidentSelected") + "</span>";                        
   
            FormUtil.InvokeRequired(this,
               delegate
               {
                   this.layoutControlGroup4.Text = ResourceLoader.GetString2("IncidentDetails");
               });
        }
        private void OnRefreshIncidentReport()
        {
            refreshIncidentReport.Change(1000, Timeout.Infinite);
        }
        private void OnRefreshIncidentReportTimer(object state)
        {
            refreshIncidentReport.Change(Timeout.Infinite, Timeout.Infinite);

            ThreadPool.QueueUserWorkItem(delegate
            {
                try
                {
                    RefreshIncidentReport();
                }
                catch (Exception ex)
                {
                    SmartLogger.Print(ex);
                }
            });
        }
        private void OnAnswerUpdateTimer(object state)
        {
            answersUpdatedTimer.Change(Timeout.Infinite, Timeout.Infinite);          
        }

        private void DefaultTwitterFrontClientForm_KeyDown(object sender, KeyEventArgs e)
        {
            //SHORTCUTS...
            if (e.Control == true && !(e.Modifiers == (Keys.Control | Keys.Alt)))
            {
                if ((e.KeyValue == 49 || e.KeyCode == Keys.NumPad1) && this.frontClientState != FrontClientStateEnum.WaitingForIncident)//GOES TO STEP ONE(1)...
                {
                    ChangeCurrentStep(1, true);
                }
                else if ((e.KeyValue == 50 || e.KeyCode == Keys.NumPad2) && this.frontClientState != FrontClientStateEnum.WaitingForIncident)//GOES TO STEP TWO(2)...
                {
                    ChangeCurrentStep(2, true);
                }
                else if ((e.KeyValue == 51 || e.KeyCode == Keys.NumPad3) && this.frontClientState != FrontClientStateEnum.WaitingForIncident)//GOES TO STEP THREE(3)...
                {
                    ChangeCurrentStep(3, true);
                }
                else if ((e.KeyValue == 52 || e.KeyCode == Keys.NumPad4) && this.frontClientState != FrontClientStateEnum.WaitingForIncident)//GOES TO STEP FOUR(4)...
                {
                    ChangeCurrentStep(4, true);
                }
                else if (e.KeyCode == Keys.A && this.frontClientState == FrontClientStateEnum.RegisteringIncident)//TO CHECK OR UNCHECK THE IS ANONIMOUS CALL CHECBOX
                {
                              
                }
                else if ((e.KeyCode == Keys.Enter || e.KeyCode == Keys.Space))
                {
                    e.SuppressKeyPress = true;
                }
            }
           
            else if (e.KeyCode == Keys.F1)//CALLS ONLINE HELP...
            {
				((TwitterFrontClientFormDevX)ParentForm).barButtonItemHelp_ItemClick(null, null);
            }
          
            else if (e.KeyCode == Keys.F6)//SETS THE SIMILAR INCIDENT FILTER...
            {
        
            }
            else if (e.KeyCode == Keys.F7)//SETS THE SAME NUMBER INCIDENT FILTER...
            {
                
            }
            else if (e.KeyCode == Keys.Enter && e.Shift == false && e.Control == false && e.Alt == false)
            {
                if (this.questionsControl.ContainsFocus && this.questionsControl.Active && !this.questionsControl.IsPanelIncidentTypeActive)
                {
                    this.questionsControl.SetNextQuestion();
                }
            }
            else if (e.KeyCode == Keys.F8)//SETS THE OPEN INCIDENTS FILTER...
            {
                
            }
            else if (e.KeyCode == Keys.F9)//CREATE A NEW INCIDENT WITH A NEW PHONE REPORTS...
            {
                if (FrontClientState != FrontClientStateEnum.WaitingForIncident)
                    barButtonItemCreateNewIncident_ItemClick(null, null);
            }           
            else if (sender == null && e.Alt == true && !(e.Modifiers == (Keys.Control | Keys.Alt))
               && e.KeyCode == Keys.F4 && this.Disposing == false)
            {
                this.Close();
            }        
        }       
       
        public void callReceiverControl1_Enter(object sender, EventArgs e)
        {
            if (this.FrontClientState != FrontClientStateEnum.WaitingForIncident)
            {
                ChangeCurrentStep(1, false);
            }
        }
       
        private void questionsTwitterControl_Enter(object sender, EventArgs e)
        {
            if (this.FrontClientState != FrontClientStateEnum.WaitingForIncident)
            {
                ChangeCurrentStep(2, false);
            }
        }
        private void LoadStepImages()
        {
            this.inactiveStepImages = new ImageList();
            this.activeStepImages = new ImageList();
            string imageName = "$Image.";
            for (int i = 1; i < 5; i++)
            {
                Image image = ResourceLoader.GetImage(imageName + i + "Encendido");
                if (activeStepImages.ImageSize.Height != image.Size.Height ||
                    activeStepImages.ImageSize.Width != image.Size.Width)
                {
                    activeStepImages.ImageSize = image.Size;
                }
                activeStepImages.Images.Add(image);
            }
            for (int i = 1; i < 5; i++)
            {
                Image image = ResourceLoader.GetImage(imageName + i + "Apagado");
                if (inactiveStepImages.ImageSize.Height != image.Size.Height ||
                    inactiveStepImages.ImageSize.Width != image.Size.Width)
                {
                    inactiveStepImages.ImageSize = image.Size;
                }
                inactiveStepImages.Images.Add(image);
            }
        }
        private void departmentTypesControl2_DepartmentTypesSelected(object sender, EventArgs e)
        {
            DepartmentTypesControl control = (DepartmentTypesControl)sender;
            if(this.GlobalTwitterReport != null)
				this.GlobalTwitterReport.ReportBaseDepartmentTypesClient = control.SelectedDepartmentTypesWithPriority;
        }               
        private bool ValidateQuestions(RequirementTypeClientEnum requirementType)
        {
            //returns true if there are invalid questions...
            bool toReturn = false;
            if (GlobalTwitterReport.IncidentTypesCodes != null)
            {
                int index = 0;
                while (toReturn != true && index < GlobalTwitterReport.IncidentTypesCodes.Count)
                {
                    IncidentTypeClientData incidentType = globalIncidentTypes[(int)GlobalTwitterReport.IncidentTypesCodes[index]];
                    foreach (IncidentTypeQuestionClientData incidentQuestion in incidentType.IncidentTypeQuestions)
                    {
                        if (incidentQuestion.Question != null)
                        {
                            if (incidentQuestion.Question.RequirementType == requirementType)
                            {
                                int questionIndex = 0;
                                bool found = false;
                                while ((questionIndex < this.questionsControl.QuestionsWithAnswers.Count) && (found != true))
                                {
                                    if (((ReportAnswerClientData)this.questionsControl.QuestionsWithAnswers[questionIndex]).QuestionCode.Equals(incidentQuestion.Question.Code) == true)
                                    {
                                        found = true;
                                    }
                                    questionIndex++;
                                }
                                if (found != true)
                                {
                                    toReturn = true;
                                }
                            }
                        }
                    }
                    index++;
                }
            }
            return toReturn;
        }   
        private bool ValidateRequiredQuestions()
        {
            return ValidateQuestions(RequirementTypeClientEnum.Required);
        }
        private bool ValidateIncidentTypes()
        {
            if (GlobalTwitterReport.IncidentTypesCodes != null && GlobalTwitterReport.IncidentTypesCodes.Count > 0)
                return false;
            else
                return true;
        }
        private bool ValidateDepartmentTypes()
        {
            bool toReturn = false;
            if (GlobalTwitterReport.ReportBaseDepartmentTypesClient != null)
            {
                int index = 0;
                while (toReturn != true && index < GlobalTwitterReport.ReportBaseDepartmentTypesClient.Count)
                {
                    ReportBaseDepartmentTypeClientData twitterReportDepartmentType =
                        GlobalTwitterReport.ReportBaseDepartmentTypesClient[index] as ReportBaseDepartmentTypeClientData;
                    if (twitterReportDepartmentType.PriorityCode == 0)
                    {
                        toReturn = true;
                    }
                    index++;
                }
            }
            else
            {
                GlobalTwitterReport.ReportBaseDepartmentTypesClient = new ArrayList();
            }
            return toReturn;
        }        
        private void UpdateQuestionsTwitterControl(ArrayList Changes)
        {
            if ((Changes != null) && (Changes.Count > 0))
            {
                if (Changes[0] is IncidentTypeClientData)
                {
                    this.questionsControl.UpdateIncidentTClientData(Changes);
                }
                else if (Changes[0] is QuestionClientData)
                {
                    this.questionsControl.UpdateIncidentQuestions(Changes);
                }
            }
        }
        private void UpdatedDepartmentInvolvedControl()
        {
            for (int i = 0; i < this.departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.RowCount; i++)
                if ((this.departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.GetRowCellValue(i,"Prioridad") == null)||
                    (this.departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.GetRowCellValue(i,"Prioridad").ToString() == string.Empty))
                    this.departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.SetRowCellValue(i," ",false);
        }        
        private bool ValidateInterface()
        {
            bool toReturn = true;
            
            if (ValidateChangedIncidentype() == false)
            {
                if (ValidateIncidentTypes() == false)
                {
                    DialogResult result = MessageForm.Show(ResourceLoader.GetString2("DeletedOrModifiedIncidentTipes"), MessageFormType.Question);
                    if (result == DialogResult.No)
                    {
                        popUpShow = false;
                        this.UpdateQuestionsTwitterControl(IncidentChanges);
                        UpdatedDepartmentInvolvedControl();
                        this.departmentsInvolvedControl.departmentTypesControl.IncidentTypes = IncidentChanges;
                        IncidentChanges.Clear();                      
                        toReturn = false;
                        this.questionsControl.SetFrontClientChangedIncidentTypeState();
                        this.questionsControl.Focus();
                    }
                }
                else
                {
                    MessageForm.Show(ResourceLoader.GetString2("DeletedIncidentTypes"), MessageFormType.Information);
                    toReturn = false;
                    this.questionsControl.SetFrontClientChangedIncidentTypeState();
                    this.questionsControl.Focus();
                }
            }
			  else if (ValidateChangedQuestions() == false)
            {
                DialogResult result = MessageForm.Show(ResourceLoader.GetString2("UpdatedRequiredQuestionsWishToContinue"), MessageFormType.Question);
                if (result == DialogResult.No)
                {
                    this.UpdateQuestionsTwitterControl(QuestionChanges);
                    QuestionChanges.Clear();
                    toReturn = false;
                    this.questionsControl.Focus();
                }
            }
            else if (ValidateRecommendedChangedQuestions() == false)
            {
                DialogResult result = MessageForm.Show(ResourceLoader.GetString2("UpdatedRequiredQuestions"), MessageFormType.Information);
                toReturn = false;
                this.questionsControl.PressedButtonOk();
                this.questionsControl.Focus();
            }
            else if (ValidateAddedQuestions() == false)
            {
                DialogResult result = MessageForm.Show(ResourceLoader.GetString2("NewRequiredQuestions"), MessageFormType.Information);
                toReturn = false;
                this.questionsControl.PressedButtonOk();
                this.questionsControl.Focus();
            }
            else if (ValidateTwitterCallInformation() == false)
            {
                DialogResult result = MessageForm.Show(ResourceLoader.GetString2("CctvBadIncident"), MessageFormType.Error);
                toReturn = false;                
                this.incidentInformationControl.Focus();
            }
			else if (ValidateDepartmentTypeChanges() == false)
			{
				if (ValidateDepartmentTypes() == false)
				{
					DialogResult dialog = MessageForm.Show(ResourceLoader.GetString2("DepartmentTypeUpdated"), MessageFormType.Question);
					if (dialog == DialogResult.No)
					{
						popUpShow = false;
						toReturn = false;
					}
					else if (GetSelectedDepartment().Count() == 0)
					{
						MessageForm.Show(ResourceLoader.GetString2("DepartmentTypeUpdatedCheckSelection"), MessageFormType.Error);
						popUpShow = false;
						toReturn = false;
					}
				}
			}
			if (toReturn == true)
            {
                if (ValidateIncidentTypes() == true)
                {
                    toReturn = false;
                    MessageForm.Show(ResourceLoader.GetString2("CctvReportWithoutIncidentType"), MessageFormType.Error);
                    this.questionsControl.SetFrontClientChangedIncidentTypeState();
                    this.questionsControl.Focus();
                    
                }
                else if (ValidateRequiredQuestions() == true)
                {
                    toReturn = false;
                    MessageForm.Show(ResourceLoader.GetString2("CctvRemainingNotAnsweredRequiredQuestions"), MessageFormType.Error);
                    this.questionsControl.Focus();
                    this.questionsControl.SelectNextNotAskedQuestion(new VistaButtonEx());
                }               
                else if (ValidateDepartmentTypes() == true)
                {
                    toReturn = false;
                    MessageForm.Show(ResourceLoader.GetString2("CctvDepartmentsWithoutPriority"), MessageFormType.Error);                   

                    this.departmentsInvolvedControl.departmentTypesControl.SetFocus("radioButtonExPriority1");
                }
				else if (ValidateTwitterCallInformation() == false)
				{
					DialogResult result = MessageForm.Show(ResourceLoader.GetString2("CctvBadIncident"), MessageFormType.Error);
					toReturn = false;
					this.incidentInformationControl.Focus();
				}              
            }
            if ((questionActionDelete.Count > 0) && (toReturn == true))
            {
                questionsControl.ShowPanelSelected();
            }
            return toReturn;
        }

		private bool ValidateDepartmentTypeChanges()
		{
			bool result = true;
			foreach (DepartmentTypeClientData item in DepartmentTypeChanges)
			{
				departmentsInvolvedControl.departmentTypesControl.DeleteDepartmentWithPriority(item);
				departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.BeginInit();
				departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.DeleteItem(new GridDepartmentTypeData(item));
				departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.EndInit();
				result = false;
			}
			DepartmentTypeChanges.Clear();
			return result;
		}

		private bool ValidateTwitterCallInformation()
        {
            if ((incidentInformationControl.CallerAddress == null) ||
                (incidentInformationControl.TweetOrAlarmName == "") )
                return false;
            else
                return true;
        }

        private bool ValidateChangedQuestions()
        {
            bool validate = true;
            if (GlobalTwitterReport.Answers != null)
            {
                for (int i = 0; i < GlobalTwitterReport.Answers.Count; i++)
                {
                    ReportAnswerClientData pracd = GlobalTwitterReport.Answers[i] as ReportAnswerClientData;
                    if (questionActionDelete.ContainsKey(pracd.QuestionCode) == true)
                    {
                        if (validate == true)
                            validate = false;

                        QuestionClientData deletedQuestion = questionActionDelete[pracd.QuestionCode];
                        GlobalTwitterReport.Answers.RemoveAt(i);
                        questionsControl.DeleteQuestion(deletedQuestion);
                        i--;
                    }
                    else
                    {
                        if (questionActionsUpdate.ContainsKey(pracd.QuestionCode) == true)
                        {
                            QuestionClientData updatedQuestion = questionActionsUpdate[pracd.QuestionCode];
                         
                            if (validate == true)
                                validate = false;

                            questionsControl.UpdateQuestion(updatedQuestion);
                        }
                    }
                }
            }
            return validate;
        }

        private bool ValidateRecommendedChangedQuestions()
        {
            bool validate = true;
            foreach (QuestionClientData qcd in questionActionsUpdate.Values)
            {
                foreach (IncidentTypeQuestionClientData itqcd in qcd.IncidentTypes)
                {
                    for (int i = 0; i < questionsControl.SelectedIncidentTypes.Count; i++)
                    {
                        IncidentTypeClientData incidentTypeClientData = questionsControl.SelectedIncidentTypes[i] as IncidentTypeClientData;
                        if (incidentTypeClientData.Code == itqcd.IncidentType.Code)
                        {
                            if (validate == true)
                            {
                                validate = false;
                                break;
                            }

                            questionsControl.UpdateQuestion(qcd);
                        }
                    }
                    if (validate == false)
                        break;
                }
                if (validate == false)
                    break;
            }
            if (validate == false)
            {
                questionActionsUpdate.Clear();
            }            
            return validate;
        }

        private bool ValidateAddedQuestions()
        {
            bool validate = true;
            foreach (QuestionClientData qcd in questionActionAdd.Values)
            {
                foreach (IncidentTypeQuestionClientData itqcd in qcd.IncidentTypes)
                {
                    for (int i = 0; i < questionsControl.SelectedIncidentTypes.Count; i++)
                    {
                        IncidentTypeClientData incidentTypeClientData = questionsControl.SelectedIncidentTypes[i] as IncidentTypeClientData;
                        if (incidentTypeClientData.Code == itqcd.IncidentType.Code)
                        {
                            if (validate == true)
                            {
                                validate = false;
                                break;
                            }
                        }
                    }
                    if (validate == false)
                        break;
                }
                if (validate == false)
                    break;
            }
            if (validate == false)
                questionActionAdd.Clear();
            return validate;
        }

        private bool ValidateChangedIncidentype()
        {
            bool validate = true;
            for (int i = 0; i < questionsControl.SelectedIncidentTypes.Count; i++)
            {
                IncidentTypeClientData incidentTypeClientData = questionsControl.SelectedIncidentTypes[i] as IncidentTypeClientData;
                if (GlobalIncidentTypes.ContainsKey(incidentTypeClientData.Code) == false)
                {
                    if (validate == true)
                        validate = false;

                    questionsControl.DeleteIncidentType(incidentTypeClientData);
                    CleanIncidentTypeInformationFromGlobalTwitterReport(incidentTypeClientData);
                }
                else
                {
                    IncidentTypeClientData updatedIncidentTypeClientData = GlobalIncidentTypes[incidentTypeClientData.Code] as IncidentTypeClientData;
                    if ((updatedIncidentTypeClientData.Version > incidentTypeClientData.Version) && (popUpShow == true))
                    {
                        if (validate == true)
                            validate = false;

                        questionsControl.UpdateIncidentType(updatedIncidentTypeClientData);
                    }
                }
            }
            return validate;
        }

        private void CleanIncidentTypeInformationFromGlobalTwitterReport(IncidentTypeClientData incidentTypeClientData)
        {
            //If the IncidentType has been deleted while using it. Before we save the phone report we need to 
            // delete the incident report and its answers from it.

            GlobalTwitterReport.IncidentTypesCodes.Remove(incidentTypeClientData.Code);
            for (int k = 0; GlobalTwitterReport.Answers != null && k < GlobalTwitterReport.Answers.Count; k++)
            {
                ReportAnswerClientData pracd = GlobalTwitterReport.Answers[k] as ReportAnswerClientData;
                bool deleteAnswer = true;

                foreach (IncidentTypeClientData itcd in questionsControl.SelectedIncidentTypes)
                {
                    foreach (IncidentTypeQuestionClientData prqt in itcd.IncidentTypeQuestions)
                    {
                        if (prqt.Question.Code == pracd.QuestionCode)
                        {
                            deleteAnswer = false;
                            break;
                        }
                    }
                    if (deleteAnswer == false)
                        break;
                }

                if (deleteAnswer == true)
                {
                    GlobalTwitterReport.Answers.RemoveAt(k);
                    k--;
                }
            }
        }
  
        private void ChangeCurrentStep(int stepNumber, bool needFocus)
        {
            switch (stepNumber)
            {
                case 0:                   
                    this.incidentInformationControl.Active = false;
                    this.questionsControl.Active = false;
                    this.departmentsInvolvedControl.Active = false;                  
                    break;
                case 1:                    
                    this.incidentInformationControl.Active = true;
                    if(needFocus == true)
                        this.incidentInformationControl.Focus();
                    this.questionsControl.Active = false;
                    this.departmentsInvolvedControl.Active = false;                    
                    break;
                case 2:                  
                    this.incidentInformationControl.Active = false;
                    this.questionsControl.Active = true;
                    if (needFocus == true)
                        this.questionsControl.Focus();
                    this.departmentsInvolvedControl.Active = false;                                        
                    break;
                case 3:                  
                    this.incidentInformationControl.Active = false;
                    this.questionsControl.Active = false;
                    this.departmentsInvolvedControl.Active = true;
                    if(needFocus == true)
                        this.departmentsInvolvedControl.Focus();                                        
                    break;
                case 4:                   
                    this.incidentInformationControl.Active = false;
                    this.questionsControl.Active = false;
                    this.departmentsInvolvedControl.Active = false;                                       
                    break;
            }
        }

        private void GenerateNewTwitterReport(object sender)
        {
            if (sender is TwitterAlarmGridData)
            {
                TwitterAlarmGridData twitterAlarm = (TwitterAlarmGridData)sender;

                this.GlobalTwitterReport = new TwitterReportClientData();
                this.GlobalTwitterReport.CustomCode = Guid.NewGuid().ToString();
                this.GlobalTwitterReport.OperatorLogin = ServerServiceClient.OperatorClient.Login;
                this.globalTwitterReport.AlarmTwitter = twitterAlarm.alarmClientData;

                // this.GlobalTwitterReport.AlarmLprClient = twitterAlarm.alarmClientData;
                incidentInformationControl.Enabled = true;
                incidentInformationControl.TweetOrAlarmName = twitterAlarm.alarmClientData.Query.Name;
            }
            else
            {
               
            }
            ChangeCurrentStep(1, true);

        }
            
        private void questionsTwitterControl_IncidentTypeSelected(object sender, EventArgs e)
        {
            QuestionsControlDevX questionCtrl = (QuestionsControlDevX)sender;
            IList selectedIncidentTypes = questionCtrl.SelectedIncidentTypes;
            if (HasIncidentTypesListChanged(selectedIncidentTypes) == true)
            {
                this.GlobalTwitterReport.IncidentTypesCodes = new ArrayList();
                foreach (IncidentTypeClientData incidentType in selectedIncidentTypes)
                    this.GlobalTwitterReport.IncidentTypesCodes.Add(incidentType.Code);
                if (selectedIncidentTypes.Count == 1 && (selectedIncidentTypes[0] as IncidentTypeClientData) != null &&
                    (selectedIncidentTypes[0] as IncidentTypeClientData).NoEmergency == true)
                    this.departmentsInvolvedControl.departmentTypesControl.CleanControl();
                else
                    this.departmentsInvolvedControl.departmentTypesControl.IncidentTypes = selectedIncidentTypes;
            }                   
            questionActionAdd.Clear();
            questionActionsUpdate.Clear();
            questionActionDelete.Clear();
        }

        private bool HasIncidentTypesListChanged(IList newList)
        {
            bool result = false;
            if (GlobalTwitterReport.IncidentTypesCodes != null)
            {
                if (newList.Count != GlobalTwitterReport.IncidentTypesCodes.Count)
                    result = true;
                else if (result == false)
                {
                    for (int i = 0; i < newList.Count && result == false; i++)
                    {
                        IncidentTypeClientData newIncidentType = newList[i] as IncidentTypeClientData;
                        if (GlobalTwitterReport.IncidentTypesCodes.Contains(newIncidentType.Code) == false)
                            result = true;
                    }
                }
            }
            else
                result = true;

            return result;
        }
        
		public void serverService_CommittedChanges(object sender, CommittedObjectDataCollectionEventArgs args)
        {
            try
            {
                if (args != null && args.Objects != null && args.Objects.Count > 0)
                {
                    if (args.Objects[0] is IncidentClientData)//if it's an incident...
                    {
                        if (((IncidentClientData)args.Objects[0]).Status.Name.Equals(IncidentStatusClientData.Closed.Name) == false
                            && args.Action != CommittedDataAction.Delete)
                        {
                            SmartCadContext context = args.Context as SmartCadContext;
                            bool select = false;
                            if (context != null)
                            {
                                if (ServerServiceClient.GetInstance().OperatorClient != null &&
                                    ServerServiceClient.GetInstance().OperatorClient.Login == context.Operator)
                                {
                                    select = true;
                                }
                            }
                            gridControlExIncidentList.AddOrUpdateItem(new GridIncidentData(args.Objects[0] as IncidentClientData), select);
                            
                            this.GlobalIncidents.Add(args.Objects[0] as IncidentClientData);
						}
						else if (((IncidentClientData)args.Objects[0]).Status.Name.Equals(IncidentStatusClientData.Closed.Name))
						{
							gridControlExIncidentList.DeleteItem(new GridIncidentData(args.Objects[0] as IncidentClientData));
							globalIncidents.Remove(args.Objects[0] as IncidentClientData);
						}
                    }
                    else if (args.Objects[0] is TwitterReportClientData)// if it's a phone report... gotta check if it is already added...
                    {
						IncidentClientData incidentData = new IncidentClientData();//null;
                        TwitterReportClientData twitterReport = args.Objects[0] as TwitterReportClientData;
						int index = ((IList)gridControlExIncidentList.DataSource).IndexOf(new GridIncidentData(incidentData));
						
						if(index>=0)
                        {
							incidentData = (IncidentClientData)((GridIncidentData)((IList)gridControlExIncidentList.DataSource)[index]).Tag;
                            bool found = false;
                            for (int i = 0; i < incidentData.TwitterReports.Count && found != true; i++)
                            {
                                TwitterReportClientData twitterReportTemp = incidentData.TwitterReports[i] as TwitterReportClientData;
                                if (twitterReportTemp.CustomCode == twitterReport.CustomCode)
                                {
                                    found = true;
                                    twitterReportTemp = twitterReport;
                                }
                            }
                            if (found == false)
                            {
                                foreach (int incidentTypeCode in twitterReport.IncidentTypesCodes)
                                {
                                    bool alreadyAdded = false;
                                    IncidentTypeClientData itcd = GlobalIncidentTypes[incidentTypeCode];
                                    foreach (IncidentTypeClientData incidentType in incidentData.IncidentTypes)
                                    {
                                        if (incidentType.Code == itcd.Code)
                                            alreadyAdded = true;
                                    }

                                    if (alreadyAdded == false)
                                        incidentData.IncidentTypes.Add(itcd);
                                }
                                incidentData.TwitterReports.Add(twitterReport);
                                incidentSyncBox.Sync(new GridIncidentData(incidentData), args.Action);
                            }
                        }
                    }
                    else if (args.Objects[0] is IncidentTypeClientData)
                    {
                        IncidentTypeClientData incidentTypeClientData = args.Objects[0] as IncidentTypeClientData;
                        if (args.Action == CommittedDataAction.Save)
                        {
                            if (incidentTypeClientData.NoEmergency == false)
                            {
                                AddIncindetType(incidentTypeClientData);
                            }
                        }
                        else if (args.Action == CommittedDataAction.Update)
                        {
                            popUpShow = true;

                            UpdateIncidentType(incidentTypeClientData);
                            for (int i = 0; i < IncidentChanges.Count; i++)
                            {
                                if ((IncidentChanges[i] as IncidentTypeClientData).Code == incidentTypeClientData.Code)
                                    IncidentChanges.RemoveAt(i);
                            }
                            IncidentChanges.Add(incidentTypeClientData);
                        }
                        else if (args.Action == CommittedDataAction.Delete)
                        {
                            DeleteIncidentType(incidentTypeClientData);
                        }
                        //Actualizar tipod de incident en datagrid.
                        FormUtil.InvokeRequired(this, delegate
                        {
                            gridControlExIncidentList.BeginUpdate();

                            if (gridControlExIncidentList.DataSource != null)
                            {
                                ArrayList list = new ArrayList(gridControlExIncidentList.DataSource as IList);
                                foreach (GridIncidentData item in list)
                                {
                                    IncidentClientData icd = item.Tag as IncidentClientData;
                                    for (int i = 0; i < icd.IncidentTypes.Count; i++)
                                    {
                                        IncidentTypeClientData itcd = icd.IncidentTypes[i] as IncidentTypeClientData;
                                        if (itcd.Code == incidentTypeClientData.Code)
                                        {
                                            icd.IncidentTypes[i] = incidentTypeClientData;
                                            int index = ((IList)gridControlExIncidentList.DataSource).IndexOf(new GridIncidentData(icd));
                                            if (index >= 0)
                                                ((IList)gridControlExIncidentList.DataSource)[index] = new GridIncidentData(icd);
                                            break;
                                        }
                                    }
                                }
                            }

                            gridControlExIncidentList.EndUpdate();

                        });
                    }
                    else if (args.Objects[0] is QuestionClientData)
                    {
                        QuestionClientData questionClientDara = args.Objects[0] as QuestionClientData;
                        if (args.Action == CommittedDataAction.Save)
                        {
                            AddQuestion(questionClientDara);
                        }
                        else if (args.Action == CommittedDataAction.Update)
                        {
                            UpdateQuestion(questionClientDara);
                            for (int i = 0; i < QuestionChanges.Count; i++)
                            {
                                if ((QuestionChanges[i] as QuestionClientData).Code == questionClientDara.Code)
                                    QuestionChanges.RemoveAt(i);
                            }
                            QuestionChanges.Add(questionClientDara);
                        }
                        else if (args.Action == CommittedDataAction.Delete)
                        {
                            DeleteQuestion(questionClientDara);
                        }
                    }
                    else if (args.Objects[0] is AlarmQueryTwitterClientData)
                    {
                        FormUtil.InvokeRequired(this, delegate
                        {
                            AlarmQueryTwitterClientData alarmQuery = (AlarmQueryTwitterClientData)args.Objects[0];

                            if (args.Action == CommittedDataAction.Save)
                            {

                            }
                            else if (args.Action == CommittedDataAction.Update)
                            {

                            }
                            else if (args.Action == CommittedDataAction.Delete)
                            {

                            }

                        });
                    }
                    else if (args.Objects[0] is ApplicationPreferenceClientData)
                    {
                        ApplicationPreferenceClientData preference = args.Objects[0] as ApplicationPreferenceClientData;
                        if (args.Action == CommittedDataAction.Update)
                        {
                            if (globalApplicationPreference.ContainsKey(preference.Name))
                            {
                                globalApplicationPreference[preference.Name] = preference;
                            }
                            else
                            {
                                globalApplicationPreference.Add(preference.Name, preference);
                            }
                        }
                    }
                    else if (args.Objects[0] is DepartmentTypeClientData)
                    {
                        #region  DepartmentTypeClientData
                        if (args.Action == CommittedDataAction.Delete)
                        {
                            DepartmentTypeClientData data =(DepartmentTypeClientData)args.Objects[0];
							GridDepartmentTypeData itemData = new GridDepartmentTypeData(data);
							if (GetSelectedDepartment().Contains(itemData))
								DepartmentTypeChanges.Add(data);
							else
							{
								FormUtil.InvokeRequired(this,
									  delegate
									  {
										  departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.BeginInit();
										  departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.DeleteItem(itemData);
										  departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.EndInit();
									  });
							}
                        }
                        else if (args.Action == CommittedDataAction.Save || args.Action == CommittedDataAction.Update)
                        {
                            GridDepartmentTypeData data = new GridDepartmentTypeData((DepartmentTypeClientData)args.Objects[0]);
                            FormUtil.InvokeRequired(this,
                                delegate
                                {
                                    departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.BeginInit();
                                    departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.AddOrUpdateItem(data);
                                    departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.EndInit();
                                });
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

		private IEnumerable<GridDepartmentTypeData> GetSelectedDepartment()
		{
			return departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.Items.Cast<GridDepartmentTypeData>().Where(
									dep => dep.DepartmentTypeCheck);
		}
               
        #region IncidentType Actions
        private void DeleteIncidentType(IncidentTypeClientData incidentTypeClientData)
        {
            if (GlobalIncidents != null && GlobalIncidentTypes.ContainsKey(incidentTypeClientData.Code) == true)
            {
                GlobalIncidentTypes.Remove(incidentTypeClientData.Code);
                if (FrontClientState == FrontClientStateEnum.WaitingForIncident)
                {
                    FormUtil.InvokeRequired(this, delegate
                    {
                        CleanForm(false);
                    });
                }
            }
            else if (incidentTypeActionDelete.ContainsKey(incidentTypeClientData.Code) == false)
            {
                incidentTypeActionDelete.Add(incidentTypeClientData.Code, incidentTypeClientData);
            }
        }
        private void UpdateIncidentType(IncidentTypeClientData incidentTypeClientData)
        {
            if (GlobalIncidents != null && GlobalIncidentTypes.ContainsKey(incidentTypeClientData.Code) == true)
            {
                GlobalIncidentTypes[incidentTypeClientData.Code] = incidentTypeClientData;
                if (FrontClientState == FrontClientStateEnum.WaitingForIncident && callEntering == false)
                {
                    FormUtil.InvokeRequired(this, delegate
                    {
                        CleanForm(false);
                    });
                }
            }
            else
            {
                if (incidentTypeActionsUpdate.ContainsKey(incidentTypeClientData.Code) == true)
                {
                    incidentTypeActionsUpdate[incidentTypeClientData.Code] = incidentTypeClientData;
                }
                else
                {
                    incidentTypeActionsUpdate.Add(incidentTypeClientData.Code, incidentTypeClientData);
                }
            }
        }
        private void AddIncindetType(IncidentTypeClientData incidentTypeClientData)
        {
            if (GlobalIncidents != null)
                if (incidentTypeClientData.NoEmergency == false)
                {
                    GlobalIncidentTypes.Add(incidentTypeClientData.Code, incidentTypeClientData);
                }
                //GlobalIncidentTypes.Add(incidentTypeClientData.Code, incidentTypeClientData);
            if (FrontClientState == FrontClientStateEnum.WaitingForIncident && callEntering == false)
            {
                FormUtil.InvokeRequired(this, delegate
                {
                    CleanForm(false);
                });
            }
            else
            {
                if (incidentTypeActionAdd.ContainsKey(incidentTypeClientData.Code) == false)
                {
                    incidentTypeActionAdd.Add(incidentTypeClientData.Code, incidentTypeClientData);
                }
            }
        }
        #endregion

        #region Question Actions
        private void DeleteQuestion(QuestionClientData questionClientData)
        {
            if (GlobalIncidents != null)
            {
                foreach (IncidentTypeQuestionClientData itqcd in questionClientData.IncidentTypes)
                {
                    if (GlobalIncidentTypes.ContainsKey(itqcd.IncidentType.Code) == true)
                    {
                        IncidentTypeClientData itcd = GlobalIncidentTypes[itqcd.IncidentType.Code] as IncidentTypeClientData;
                        for (int i = 0; i < itcd.IncidentTypeQuestions.Count; i++)
                        {
                            IncidentTypeQuestionClientData itqcd2 = itcd.IncidentTypeQuestions[i] as IncidentTypeQuestionClientData;
                            if (itqcd2.Code == itqcd.Code)
                            {
                                itcd.IncidentTypeQuestions.RemoveAt(i);
                                i--;
                            }
                        }
                    }
                }
            }
            if (FrontClientState == FrontClientStateEnum.WaitingForIncident)
            {
                FormUtil.InvokeRequired(this, delegate
                {
                    CleanForm(false);
                });
            }
            else
            {
                if (questionActionDelete.ContainsKey(questionClientData.Code) == false)
                {
                    questionActionDelete.Add(questionClientData.Code, questionClientData);
                }
            }
        }
        private void UpdateQuestion(QuestionClientData questionClientData)
        {
            if (GlobalIncidents != null)
            {
                foreach (IncidentTypeClientData itcd in GlobalIncidentTypes.Values)
                {
                    for (int i = 0; i < itcd.IncidentTypeQuestions.Count; i++)
                    {
                        IncidentTypeQuestionClientData itqcd = itcd.IncidentTypeQuestions[i] as IncidentTypeQuestionClientData;
                        if (itqcd.Question.Code == questionClientData.Code)
                        {
                            itcd.IncidentTypeQuestions.RemoveAt(i);
                            i--;
                        }
                    }
                }
                foreach (IncidentTypeQuestionClientData itqcd in questionClientData.IncidentTypes)
                {
                    if (GlobalIncidentTypes.ContainsKey(itqcd.IncidentType.Code) == true)
                    {
                        IncidentTypeClientData itcd = GlobalIncidentTypes[itqcd.IncidentType.Code] as IncidentTypeClientData;
                        itqcd.Question = questionClientData;
                        itcd.IncidentTypeQuestions.Add(itqcd);
                    }
                }
            }
            if (FrontClientState == FrontClientStateEnum.WaitingForIncident && callEntering == false)
            {
                FormUtil.InvokeRequired(this, delegate
                {
                     CleanForm(false);
                });
            }
            else
            {
                if (questionClientData.RequirementType == RequirementTypeClientEnum.Recommended && 
                    questionActionsUpdate.ContainsKey(questionClientData.Code) == false)
                {
                    questionActionsUpdate.Add(questionClientData.Code, questionClientData);
                }
                else
                {
                    if (questionClientData.RequirementType == RequirementTypeClientEnum.Recommended &&
                        questionActionsUpdate[questionClientData.Code] != null)
                        questionActionsUpdate[questionClientData.Code] = questionClientData;
                }
            }

            if (questionActionAdd.ContainsKey(questionClientData.Code))
            {
                questionActionAdd[questionClientData.Code] = questionClientData;
                questionActionsUpdate.Remove(questionClientData.Code);
            }
        }
        private void AddQuestion(QuestionClientData questionClientData)
        {
            foreach (IncidentTypeQuestionClientData itqcd in questionClientData.IncidentTypes)
            {
                if (GlobalIncidents != null && GlobalIncidentTypes.ContainsKey(itqcd.IncidentType.Code) == true)
                {
                    IncidentTypeClientData itcd = GlobalIncidentTypes[itqcd.IncidentType.Code] as IncidentTypeClientData;
                    itqcd.Question = questionClientData;
                    itcd.IncidentTypeQuestions.Add(itqcd);
                }
            }
            if (FrontClientState == FrontClientStateEnum.WaitingForIncident && callEntering == false)
            {
                FormUtil.InvokeRequired(this, delegate
                {
                    CleanForm(false);
                });
            }
            else
            {
                if (questionClientData.RequirementType == RequirementTypeClientEnum.Recommended &&
                    questionActionAdd.ContainsKey(questionClientData.Code) == false)
                {
                    questionActionAdd.Add(questionClientData.Code, questionClientData);
                }
                else
                {
                    if (questionClientData.RequirementType == RequirementTypeClientEnum.Recommended &&
                        questionActionAdd[questionClientData.Code] != null)
                        questionActionAdd[questionClientData.Code] = questionClientData;
                }
            }
        }

        private void questionsControl_AnswerUpdated(object sender, EventArgs e)
        {
            if (FrontClientState != FrontClientStateEnum.RegisteringIncident)
            {
                updatedAnswers = new ArrayList(questionsControl.QuestionsWithAnswers);
                foreach (ReportAnswerClientData twitterReportAnswer in updatedAnswers)
                {
                    twitterReportAnswer.ReportCode = GlobalTwitterReport.Code;
                }
            }
            else
            {
                this.GlobalTwitterReport.Answers = new ArrayList(questionsControl.QuestionsWithAnswers);
                foreach (ReportAnswerClientData twitterReportAnswer in GlobalTwitterReport.Answers)
                {
                    twitterReportAnswer.ReportCode = GlobalTwitterReport.Code;
                }
            }

            answersUpdatedTimerEventArgs = e;
            answersUpdatedTimer.Change(1000, Timeout.Infinite);
        }
        #endregion
               
        private IList CheckTwitterReportUpdates(IList newAnswers)
        {
            //No borrar, se utilizara despues.
            IList answersToAdd = new ArrayList();

            if (GlobalTwitterReport != null)
            {
                if (newAnswers != null)
                {
                    foreach (ReportAnswerClientData twitterReportAnswer in newAnswers)
                    {
                        bool found = false;
                        int i = 0;
                        while (found == false && i < GlobalTwitterReport.Answers.Count)
                        {
                            ReportAnswerClientData tempTwitterReportAnswer = GlobalTwitterReport.Answers[i] as ReportAnswerClientData;
                            if (tempTwitterReportAnswer.QuestionCode == twitterReportAnswer.QuestionCode)
                            {
                                found = true;
                            }
                            i++;
                        }
                        if (found == false)
                        {
                            twitterReportAnswer.ReportCode = GlobalTwitterReport.Code;
                            answersToAdd.Add(twitterReportAnswer);
                        }
                    }
                }
            }
            return answersToAdd;
        }

        private bool IsEmergencyIncident(TwitterReportClientData newTwitterReport)
        {
            bool result = true;
            if (newTwitterReport.IncidentTypesCodes.Count == 1)
            {
                IncidentTypeClientData incidentType = globalIncidentTypes[(int)newTwitterReport.IncidentTypesCodes[0]];
                if(incidentType.NoEmergency == true)
                    result = false;
            }
            return result;
        }

        private void GenerateNewIncident()
        {
            IncidentClientData incident = new IncidentClientData();
            incident.SourceIncidentApp = SourceIncidentAppEnum.SocialNetworks;
            incident.TwitterReports = new ArrayList();
			ApplicationPreferenceClientData prefix = globalApplicationPreference["SNIncidentCodePrefix"];
			ApplicationPreferenceClientData isSuffix = globalApplicationPreference["IsSNIncidentCodeSuffix"];
			incident.CustomCode = serverServiceClient.OperatorClient.Code + serverServiceClient.GetTime().ToString("yyMMddHHmmss");
			if(isSuffix.Value.Equals(false.ToString()))
				incident.CustomCode = prefix.Value +  incident.CustomCode;
			else
				incident.CustomCode+=prefix.Value;
            incident.Address = incidentInformationControl.CallerAddress;
           
            incident.IsEmergency = IsEmergencyIncident(GlobalTwitterReport);            
            if (incident.IsEmergency == true)
                incident.Status = IncidentStatusClientData.Open;
            else
                incident.Status = IncidentStatusClientData.Closed;                     
            foreach (ReportBaseDepartmentTypeClientData reportBaseDepartmentType in
                this.GlobalTwitterReport.ReportBaseDepartmentTypesClient)
            {
                reportBaseDepartmentType.ReportBaseCode = this.GlobalTwitterReport.Code;
            }
            if (GlobalTwitterReport.ReportBaseDepartmentTypesClient == null || GlobalTwitterReport.ReportBaseDepartmentTypesClient.Count == 0)
                incident.Status = IncidentStatusClientData.Closed;           

            this.GlobalTwitterReport.IncidentCode = incident.Code;
            if (GlobalTwitterReport.Answers == null)
                GlobalTwitterReport.Answers = new ArrayList();
            incident.TwitterReports.Add(this.GlobalTwitterReport);
            ServerServiceClient.GetInstance().SaveOrUpdateClientData(incident);
            updatedAnswers.Clear();
            EndRecordNewIncident();
            
        }

        private void EndRecordNewIncident()
        {
            //CHECKING FOR UNSAVED CHANGES IN PHONEREPORT...
            if (updatedAnswers.Count > 0)
            {
                if (EnsureUpdateTwitterReport() == true)
                {
                    GlobalTwitterReport.Answers = updatedAnswers;
                }
            }
            if (ValidateChangedQuestions() == false)
            {
                DialogResult dialogResult = MessageForm.Show(ResourceLoader.GetString2("QuestionsWereDeleted"), MessageFormType.Information);
            }           
            ServerServiceClient.GetInstance().FinalizeTwitterIncident(GlobalTwitterReport);          
            this.FrontClientState = FrontClientStateEnum.WaitingForIncident;
            //SEND CLEAR TO MAPS
            ApplicationServiceClient.Current(UserApplicationClientData.Map).InsertAddressDataInGis(
                new AddressClientData(string.Empty, string.Empty, string.Empty, string.Empty, false));
            questionActionsUpdate.Clear();
            questionActionDelete.Clear();
            questionActionAdd.Clear();
            CleanForm();         
        }  

        private void FillIncidentDetails(SelectedIncidentTaskResult selectedIncidentTask, bool sameIncident)
        {
            try
            {
                string strXml = ApplicationUtil.BuildHtml(selectedIncidentTask.Xml, xslCompiledTransform, "SNincident");
                strXml = ApplicationUtil.RecoverBlanks(strXml);

                FileStream fs = File.Open("incident_SN.html", FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite);
                byte[] byteXML = Encoding.Unicode.GetBytes(strXml);
                fs.Write(byteXML, 0, byteXML.Length);
                fs.Flush();
                fs.Close();
                FormUtil.InvokeRequired(this,
               delegate
               {
                   this.layoutControlGroup4.Text =  ResourceLoader.GetString2("IncidentDetails");
                   this.layoutControlGroup4.Text += ResourceLoader.GetString2("UpdatedAt");
                   this.layoutControlGroup4.Text += serverServiceClient.GetTime().ToString("G", ApplicationUtil.GetCurrentCulture());
               });
             
                FormUtil.InvokeRequired(textBoxExIncidentdetails, delegate
                {
                    textBoxExIncidentdetails.Navigate(new FileInfo("incident_SN.html").FullName, sameIncident); 
                });
            }
            catch(Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }        

        private void FillCallsAndDispatchs(SelectedIncidentTaskResult selectedIncidentTask)
        {
            if (selectedIncidentTask != null)
            {
                foreach (TwitterReportClientData phoneReportLight in selectedIncidentTask.TotalPhoneReports)
                {
					GridControlDataAssociatedCallData item = new GridControlDataAssociatedCallData(phoneReportLight);
                }
                foreach (DispatchOrderClientData dispatchOrderLight in selectedIncidentTask.TotalDispatchOrders)
                {
					GridControlDataAssociatedDispatchOrder item = new GridControlDataAssociatedDispatchOrder(dispatchOrderLight);
                }
            }
        }

        private string GetTwitterReportIncidentTypes(TwitterReportClientData twitterReport)
        {
            string incidentTypeStr = "";
            int counter = 0;
            int incTypeLength = twitterReport.IncidentTypesCodes.Count;
            foreach (int incidentTypeCode in twitterReport.IncidentTypesCodes)
            {
                incidentTypeStr += globalIncidentTypes[incidentTypeCode].CustomCode;
                counter++;
                if (counter < incTypeLength)
                    incidentTypeStr += ", ";

            }
            return incidentTypeStr;
        }
        
		private bool EnsureUpdateTwitterReport()
        {
            DialogResult dialogResult = MessageForm.Show(ResourceLoader.GetString2("ChangesOnReport"), MessageFormType.Question);

            return dialogResult == DialogResult.Yes;
        }
        
		private void CleanForm()
        {
            CleanForm(true);
        }

        private void CleanForm(bool clearSelection)
        {
            this.questionsControl.CleanControls();            
            this.incidentInformationControl.CleanControl();
            this.departmentsInvolvedControl.departmentTypesControl.CleanControl();
            this.incidentAdded = false;
            callEntering = false;        
            if (clearSelection == true)
				this.gridViewExIncidentList.ClearSelection();          
            
			this.GlobalTwitterReport = null;
            ChangeCurrentStep(0, false);
        }

        private void CheckButtons(ButtonEx pressedButton)
        {            
            pressedButton.BackColor = SystemColors.ButtonHighlight;
        } 
              
        private void buttonExOpenIncidentsFilter_Click(object sender, EventArgs e)
        {            
            ButtonEx pressedButton = (ButtonEx)sender;
            if (pressedButton.BackColor != SystemColors.ButtonHighlight)
            {
                searchByText = false;
                CheckButtons(pressedButton);              
                ArrayList parameters = new ArrayList();
                parameters.Add("OPEN_INCIDENTS");
                incidentsCurrentFilter.Parameters = parameters;
                incidentsCurrentFilter.Filter(true);

				if (gridControlExIncidentList.SelectedItems.Count == 0)
                    ClearTextBox();
            }
        }

        private void ClearTextBox()
        {
            layoutControlGroup4.Text = ResourceLoader.GetString2("IncidentDetails");
            textBoxExIncidentdetails.DocumentText = "<span style='HEIGHT:100%;FONT-SIZE: 10pt; FONT-FAMILY: Sans-Serif'>" + ResourceLoader.GetString2("NoIncidentSelected") + "</span>";            
			barButtonItemPrint.Enabled = false;
			barButtonItemSave.Enabled = false;
			barButtonItemRefresh.Enabled = false;
        }                
                     
        private void SelectedIncidentHelp(SelectedIncidentTask selectedIncidentTask, GeoPoint point)
        {
            if (point != null)
            {
                ApplicationServiceClient.Current(UserApplicationClientData.Map).DisplayGeoPointInGis(point);
            }
            if (selectedIncidentTask != null)
            {
                ArrayList tasks = new ArrayList();
                tasks.Add(selectedIncidentTask);
                bool sameIncident = false;
                if (selectedIncidentCode == (int)selectedIncidentTask.Parameters[0])
                    sameIncident = true;
                selectedIncidentCode = (int)selectedIncidentTask.Parameters[0];
                try
                {
                    IList result = serverServiceClient.RunTasks(tasks);
                    if (result != null && result.Count > 0 && (result[0] as IList) != null)
                    {
                        IList tempList = (IList)result[0];
                        if (tempList != null && tempList.Count > 0)
                        {
                            SelectedIncidentTaskResult selectedIncidentLightData = tempList[0] as SelectedIncidentTaskResult;
                            if (selectedIncidentLightData != null)
                            {
                                FillIncidentDetails(selectedIncidentLightData, sameIncident);
                                FillCallsAndDispatchs(selectedIncidentLightData);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    SmartLogger.Print(ex);
                }
            }
        }

        private void RefreshIncidentReport()
        {
            if (selectedIncidentCode != -1)
            {
                ArrayList parameters = new ArrayList();
                parameters.Add(selectedIncidentCode);
                SelectedIncidentTask selectedIncidentTask = new SelectedIncidentTask(parameters);
                ArrayList tasks = new ArrayList();
                tasks.Add(selectedIncidentTask);
                try
                {
                    IList result = serverServiceClient.RunTasks(tasks);
                    if (result != null && result.Count > 0 && (result[0] as IList) != null)
                    {
                        IList tempList = (IList)result[0];
                        if (tempList != null && tempList.Count > 0)
                        {
                            SelectedIncidentTaskResult selectedIncidentLightData = tempList[0] as SelectedIncidentTaskResult;
                            if (selectedIncidentLightData != null)
                            {
                                FillIncidentDetails(selectedIncidentLightData, true);
                                FillCallsAndDispatchs(selectedIncidentLightData);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    SmartLogger.Print(ex);
                }
            }
        }    
          
        private void SetSkin()
        {
            try
            {
                string skinFile = SmartCadConfiguration.SkinsFolder + "/Clasic.xml";
                skinEngine = SkinEngine.Load(skinFile);
                ProfessionalColorTable colorTable = new CustomToolStripColors(
                    skinEngine.Colors["ToolStripGradientBegin"],
                    skinEngine.Colors["ToolStripGradientMiddle"],
                    skinEngine.Colors["ToolStripGradientEnd"],
                    skinEngine.Colors["MenuStripGradientBegin"],
                    skinEngine.Colors["MenuStripGradientEnd"],
                    skinEngine.Colors["StatusStripGradientBegin"],
                    skinEngine.Colors["StatusStripGradientEnd"]
                );

                
                skinEngine.AddElement("Step1Control", this.incidentInformationControl);
                skinEngine.AddElement("Step2Control", this.questionsControl);
                skinEngine.AddElement("Step3LeftControl", this.departmentsInvolvedControl);               
                skinEngine.AddCompleteElement(this);
                skinEngine.ApplyChanges();
            }
            catch (Exception ex)
            {
                MessageForm.Show(ResourceLoader.GetString2("$Message.Error"), ResourceLoader.GetString2("ErrorLoadingSkin"), ex);
            }
        }

        private void DefaultLprFrontClientForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            ServerServiceClient.GetInstance().CommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(serverService_CommittedChanges);
            
            ServerServiceClient.GetInstance().CloseSession();
        }

        private void SetFrontClientWaitingForCallState()
        {
            FormUtil.InvokeRequired(this, () =>
            {
                this.incidentInformationControl.FrontClientState = FrontClientStateEnum.WaitingForIncident;
                this.questionsControl.FrontClientState = FrontClientStateEnum.WaitingForIncident;
                this.departmentsInvolvedControl.FrontClientState = FrontClientStateEnum.WaitingForIncident;
                // this.barButtonItemStartRegIncident.Enabled = true;
                this.barButtonItemCreateNewIncident.Enabled = false;
                this.barButtonItemCancelIncident.Enabled = false;
            });
        }

        private void SetFrontClientRegisteringCallState()
        {
            FormUtil.InvokeRequired(this, delegate
            {
                this.incidentInformationControl.FrontClientState = FrontClientStateEnum.RegisteringIncident;
                this.questionsControl.FrontClientState = FrontClientStateEnum.RegisteringIncident;
                this.departmentsInvolvedControl.FrontClientState = FrontClientStateEnum.RegisteringIncident;
                this.barButtonItemStartRegIncident.Enabled = false;
                this.barButtonItemCreateNewIncident.Enabled = true;
				this.barButtonItemCancelIncident.Enabled = true;
               
            }); 
        }    
   
        private void dataGridTwitterPrevIncidents_KeyDown(object sender, KeyEventArgs e)
        {          
            if (e.KeyCode == Keys.F5)
            {
                barButtonItemRefresh_ItemClick(null, null);
            }
            else
            {
                DefaultTwitterFrontClientForm_KeyDown(null, e);
            }
        }

        private void textBoxExIncidentdetails_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            KeyEventArgs keyEvent = new KeyEventArgs(e.KeyData);
            DefaultTwitterFrontClientForm_KeyDown(null, keyEvent);
        }
        
		private void questionAnswerTextControl_Enter(object sender, EventArgs e)
        {
            if (this.FrontClientState != FrontClientStateEnum.WaitingForIncident)
                ChangeCurrentStep(3, false);
        }              
        
		private void toolStripMenuItemHelpOnLine_Click(object sender, EventArgs e)
        {
            try
            {
                ApplicationUtil.LaunchHelp(SmartCadConfiguration.DistFolder + "/Help/SmartCad.chm");
            }
            catch (Exception ex)
            {
                MessageForm.Show(ResourceLoader.GetString2("$Message.HelpNotInstalled"), ex);
            }
        }    
     
        private void questionAnswerTextControl_QuestionAnswerTextControlPreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            KeyEventArgs keyEvent = new KeyEventArgs(e.KeyData);
            DefaultTwitterFrontClientForm_KeyDown(null, keyEvent);
        }
        
        private void frontClientButton_MouseLeave(object sender, EventArgs e)
        {
            toolTipMain.Hide((Button)sender);
        }

        private void contextMenuStripIncidents_Opening(object sender, CancelEventArgs e)
        {
			if (gridControlExIncidentList.SelectedItems.Count > 0)
                barButtonItemRefresh.Enabled = true;
            else
            {
                e.Cancel = true;
                barButtonItemRefresh.Enabled = false;
            }
        }
                
        private void twitterCallInformationControl1_SendGeoPointStruct(object sender, GeoPointEventArgs e)
        {
            ApplicationServiceClient.Current(UserApplicationClientData.Map).DisplayGeoPointInGis(e.Point);
        }

		public void barButtonItemStartRegIncident_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			GenerateNewTwitterReport(sender);
			this.FrontClientState = FrontClientStateEnum.RegisteringIncident;
		}

		public void barButtonItemCreateNewIncident_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			if (ValidateInterface() == true)
			{
                this.Show();
				this.questionsControl.Focus();
				GenerateNewIncident();
				CleanForm();
				departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.ClearSelection();
                this.FrontClientState = FrontClientStateEnum.WaitingForIncident;

                ((TwitterFrontClientFormDevX)this.MdiParent).FisnishAlarm();
			}
		}

        public void barButtonItemCancelIncident_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			DialogResult dialog = MessageForm.Show("CancelIncidentRegistrationCCTV", MessageFormType.WarningQuestion);
			if (dialog.Equals(DialogResult.No))
			{
				return;
			}
			updatedAnswers.Clear();
			CleanForm();
			departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.ClearSelection();
            this.FrontClientState = FrontClientStateEnum.WaitingForIncident;

            ((TwitterFrontClientFormDevX)this.MdiParent).ReleaseAlarm();
			//barButtonItemStartRegIncident.LargeGlyph = ResourceLoader.GetImage("$Image.RecordOff");
		}

		private void barButtonItemPrint_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
            textBoxExIncidentdetails.ShowPrintDialog();
		}

		private void barButtonItemSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			textBoxExIncidentdetails.ShowSaveAsDialog();
		}

		private void barButtonItemRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			if (gridControlExIncidentList.SelectedItems.Count > 0)
                OnRefreshIncidentReport();
		}

		private void dockPanel1_DockChanged(object sender, EventArgs e)
		{
			if (dockPanel1.Visible == true && this.Width < this.ParentForm.MinimumSize.Width + dockPanel1.Width)
				this.ParentForm.Width += dockPanel1.Width;
		}
		
		void gridViewExIncidentList_SingleSelectionChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
		{
			this.selectedIncidentCode = -1;
			if (gridControlExIncidentList.SelectedItems.Count > 0)
            {
				IncidentClientData selectedIncident = ((GridIncidentData)gridControlExIncidentList.SelectedItems[0]).Tag as IncidentClientData;
                
                ArrayList parameters = new ArrayList();
                parameters.Add(selectedIncident.Code);
                parameters.Add(selectedIncident.IncidentTypes);

                SelectedIncidentTask selectedIncidentTask = new SelectedIncidentTask(parameters);
                ArrayList tasks = new ArrayList();
                tasks.Add(selectedIncidentTask);
                
                GeoPoint point = null;
                AddressClientData address = selectedIncident.Address;
                if (address != null && address.Lon != 0 && address.Lat != 0)
                {
                    point = new GeoPoint(address.Lon, address.Lat);
                }                
                ThreadPool.QueueUserWorkItem(delegate(object state)
                {
                    try
                    {                       
                        SelectedIncidentHelp(selectedIncidentTask, point);
                    }
                    catch (Exception ex)
                    {
                        MessageForm.Show(ex.Message, ex);
                    }
                });

				barButtonItemSave.Enabled = true;
				barButtonItemPrint.Enabled = true;
				barButtonItemRefresh.Enabled = true;
            }
            else
            {
				barButtonItemSave.Enabled = false;
				barButtonItemPrint.Enabled = false;
				barButtonItemRefresh.Enabled = false;                                        
                textBoxExIncidentdetails.DocumentText = "<span style='HEIGHT:100%;FONT-SIZE: 10pt; FONT-FAMILY: Sans-Serif'>" + ResourceLoader.GetString2("NoIncidentSelected") + "</span>";                                
            }
		}

		private void DefaultLprFrontClientFormDevX_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
            }
            else if (FrontClientState == FrontClientStateEnum.WaitingForIncident && EnsureLogout())
            {
                incidentInformationControl.Enter -= callReceiverControl1_Enter;
                ServerServiceClient.GetInstance().CommittedChanges -= serverService_CommittedChanges;
                ServerServiceClient.GetInstance().CommittedChanges -= ((TwitterFrontClientFormDevX)this.MdiParent).FrontClientFormDevX_CommittedChanges;
                this.Dispose(true);
            }
            else
            {
                e.Cancel = true;
            }

        }

        private static bool EnsureLogout()
        {
            DialogResult dialogResult = MessageForm.Show(ResourceLoader.GetString2("$Message.ExitApplication"), MessageFormType.Question);

            return dialogResult == DialogResult.Yes;
        }
    }    
}
