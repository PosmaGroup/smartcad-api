using Microsoft.Win32;
using SmartCadControls;
using SmartCadControls.Controls;
using SmartCadCore.Enums;
using SmartCadGuiCommon.Controls;
using System.IO;

namespace SmartCadFirstLevel.Gui
{
    partial class DefaultTwitterFrontClientForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private new System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();

                RegistryKey regkey;
               
                try
                {
                    DirectoryInfo source = new DirectoryInfo(SourceDirectory);

                    //Determine whether the source directory exists.
                    if (!source.Exists)
                        regkey = Registry.Users;
                    else
                        regkey = Registry.CurrentUser;

                    // Restore the Print Page Setup properties
                    regkey = regkey.OpenSubKey(@SourceDirectory, true);
                    if (regkey != null)
                    {
                        // Restore the footer values.			
                        regkey.SetValue("footer", IEFooter.ToString());
                    }
                }
                catch
                { }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DefaultTwitterFrontClientForm));
            this.toolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.saveFileDialogMain = new System.Windows.Forms.SaveFileDialog();
            this.printDialogMain = new System.Windows.Forms.PrintDialog();
            this.contextMenuStripIncidents = new ContextMenuStripEx(this.components);
            this.imageListTabs = new System.Windows.Forms.ImageList(this.components);
            this.textBoxExIncidentdetails = new SearchableWebBrowser();
            this.layoutControlDefaultTwitterFrontClient = new DevExpress.XtraLayout.LayoutControl();
            this.incidentInformationControl = new TwitterCallInformationControl();
            this.questionsControl = new QuestionsControlDevX();
            this.departmentsInvolvedControl = new DepartmentsInvolvedControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemCallInformation = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemQuestionControl = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemDepartmentsInvolved = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanel1 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.layoutControlDockPanel = new DevExpress.XtraLayout.LayoutControl();
            this.gridControlExIncidentList = new GridControlEx();
            this.gridViewExIncidentList = new GridViewEx();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemIncidentList = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemIncidentDetails = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItemPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSave = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemStartRegIncident = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemCreateNewIncident = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemCancelIncident = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemCancelAlarm = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroupCallInfo = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupGeneralOptions = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlDefaultTwitterFrontClient)).BeginInit();
            this.layoutControlDefaultTwitterFrontClient.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCallInformation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemQuestionControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartmentsInvolved)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanel1.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlDockPanel)).BeginInit();
            this.layoutControlDockPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExIncidentList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExIncidentList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIncidentList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIncidentDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            this.SuspendLayout();
            // 
            // printDialogMain
            // 
            this.printDialogMain.UseEXDialog = true;
            // 
            // contextMenuStripIncidents
            // 
            this.contextMenuStripIncidents.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.contextMenuStripIncidents.Name = "contextMenuStripIncidents";
            this.contextMenuStripIncidents.Size = new System.Drawing.Size(61, 4);
            this.contextMenuStripIncidents.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStripIncidents_Opening);
            // 
            // imageListTabs
            // 
            this.imageListTabs.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageListTabs.ImageSize = new System.Drawing.Size(16, 16);
            this.imageListTabs.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // textBoxExIncidentdetails
            // 
            this.textBoxExIncidentdetails.AllowWebBrowserDrop = false;
            this.textBoxExIncidentdetails.DocumentText = "<HTML></HTML>\0";
            this.textBoxExIncidentdetails.IsWebBrowserContextMenuEnabled = true;
            this.textBoxExIncidentdetails.Location = new System.Drawing.Point(10, 312);
            this.textBoxExIncidentdetails.Margin = new System.Windows.Forms.Padding(0);
            this.textBoxExIncidentdetails.Name = "textBoxExIncidentdetails";
            this.textBoxExIncidentdetails.Size = new System.Drawing.Size(403, 381);
            this.textBoxExIncidentdetails.TabIndex = 5;
            this.textBoxExIncidentdetails.WebBrowserShortcutsEnabled = false;
            this.textBoxExIncidentdetails.XmlText = "<HTML></HTML>\0";
            this.textBoxExIncidentdetails.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.textBoxExIncidentdetails_PreviewKeyDown);
            // 
            // layoutControlDefaultTwitterFrontClient
            // 
            this.layoutControlDefaultTwitterFrontClient.AllowCustomizationMenu = false;
            this.layoutControlDefaultTwitterFrontClient.Controls.Add(this.incidentInformationControl);
            this.layoutControlDefaultTwitterFrontClient.Controls.Add(this.questionsControl);
            this.layoutControlDefaultTwitterFrontClient.Controls.Add(this.departmentsInvolvedControl);
            this.layoutControlDefaultTwitterFrontClient.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlDefaultTwitterFrontClient.Location = new System.Drawing.Point(0, 0);
            this.layoutControlDefaultTwitterFrontClient.LookAndFeel.SkinName = "Blue";
            this.layoutControlDefaultTwitterFrontClient.LookAndFeel.UseDefaultLookAndFeel = false;
            this.layoutControlDefaultTwitterFrontClient.MinimumSize = new System.Drawing.Size(1, 1);
            this.layoutControlDefaultTwitterFrontClient.Name = "layoutControlDefaultTwitterFrontClient";
            this.layoutControlDefaultTwitterFrontClient.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(439, 313, 250, 350);
            this.layoutControlDefaultTwitterFrontClient.Root = this.layoutControlGroup1;
            this.layoutControlDefaultTwitterFrontClient.Size = new System.Drawing.Size(345, 770);
            this.layoutControlDefaultTwitterFrontClient.TabIndex = 43;
            this.layoutControlDefaultTwitterFrontClient.Text = "layoutControl1";
            // 
            // incidentInformationControl
            // 
            this.incidentInformationControl.FrontClientState = FrontClientStateEnum.WaitingForIncident;
            this.incidentInformationControl.Location = new System.Drawing.Point(5, 5);
            this.incidentInformationControl.MinimumSize = new System.Drawing.Size(466, 155);
            this.incidentInformationControl.Name = "incidentInformationControl";
            this.incidentInformationControl.Size = new System.Drawing.Size(583, 155);
            this.incidentInformationControl.TabIndex = 12;
            this.incidentInformationControl.TweetOrAlarmName = "";
            // 
            // questionsControl
            // 
            this.questionsControl.ActiveAnswerTextColor = System.Drawing.Color.Empty;
            this.questionsControl.ActiveQuestionTextColor = System.Drawing.Color.Empty;
            this.questionsControl.AskedStatusColor = System.Drawing.Color.Empty;
            this.questionsControl.AskingStatusColor = System.Drawing.Color.Empty;
            this.questionsControl.CctvClientState = CctvClientStateEnum.WaitingForCctvIncident;
            this.questionsControl.DisabledButtonColor = System.Drawing.Color.Empty;
            this.questionsControl.FrontClientState = FrontClientStateEnum.WaitingForIncident;
            this.questionsControl.GlobalIncidentTypes = null;
            this.questionsControl.InactiveAnswerTextColor = System.Drawing.Color.Empty;
            this.questionsControl.InactiveQuestionTextColor = System.Drawing.Color.Empty;
            this.questionsControl.IncidentTypeLabelBorderColor = System.Drawing.Color.Empty;
            this.questionsControl.Location = new System.Drawing.Point(5, 162);
            this.questionsControl.MinimumSize = new System.Drawing.Size(583, 286);
            this.questionsControl.Name = "questionsControl";
            this.questionsControl.NamePressedButton = null;
            this.questionsControl.NotAskedStatusColor = System.Drawing.Color.Empty;
            this.questionsControl.QuestionsWithAnswers = null;
            this.questionsControl.SelectedIncidentTypes = null;
            this.questionsControl.Size = new System.Drawing.Size(583, 301);
            this.questionsControl.TabIndex = 13;
            // 
            // departmentsInvolvedControl
            // 
            this.departmentsInvolvedControl.FrontClientState = FrontClientStateEnum.WaitingForIncident;
            this.departmentsInvolvedControl.Location = new System.Drawing.Point(5, 473);
            this.departmentsInvolvedControl.Name = "departmentsInvolvedControl";
            this.departmentsInvolvedControl.Size = new System.Drawing.Size(583, 275);
            this.departmentsInvolvedControl.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemCallInformation,
            this.layoutControlItemQuestionControl,
            this.layoutControlItemDepartmentsInvolved,
            this.splitterItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(593, 753);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItemCallInformation
            // 
            this.layoutControlItemCallInformation.Control = this.incidentInformationControl;
            this.layoutControlItemCallInformation.CustomizationFormText = "layoutControlItemCallInformation";
            this.layoutControlItemCallInformation.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemCallInformation.MaxSize = new System.Drawing.Size(0, 157);
            this.layoutControlItemCallInformation.MinSize = new System.Drawing.Size(470, 157);
            this.layoutControlItemCallInformation.Name = "layoutControlItemCallInformation";
            this.layoutControlItemCallInformation.Size = new System.Drawing.Size(587, 157);
            this.layoutControlItemCallInformation.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemCallInformation.Text = "layoutControlItemCallInformation";
            this.layoutControlItemCallInformation.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemCallInformation.TextToControlDistance = 0;
            this.layoutControlItemCallInformation.TextVisible = false;
            // 
            // layoutControlItemQuestionControl
            // 
            this.layoutControlItemQuestionControl.Control = this.questionsControl;
            this.layoutControlItemQuestionControl.CustomizationFormText = "layoutControlItemQuestionControl";
            this.layoutControlItemQuestionControl.Location = new System.Drawing.Point(0, 157);
            this.layoutControlItemQuestionControl.MinSize = new System.Drawing.Size(587, 290);
            this.layoutControlItemQuestionControl.Name = "layoutControlItemQuestionControl";
            this.layoutControlItemQuestionControl.Size = new System.Drawing.Size(587, 305);
            this.layoutControlItemQuestionControl.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemQuestionControl.Text = "layoutControlItemQuestionControl";
            this.layoutControlItemQuestionControl.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemQuestionControl.TextToControlDistance = 0;
            this.layoutControlItemQuestionControl.TextVisible = false;
            // 
            // layoutControlItemDepartmentsInvolved
            // 
            this.layoutControlItemDepartmentsInvolved.Control = this.departmentsInvolvedControl;
            this.layoutControlItemDepartmentsInvolved.CustomizationFormText = "layoutControlItemDepartmentsInvolved";
            this.layoutControlItemDepartmentsInvolved.Location = new System.Drawing.Point(0, 468);
            this.layoutControlItemDepartmentsInvolved.Name = "layoutControlItemDepartmentsInvolved";
            this.layoutControlItemDepartmentsInvolved.Size = new System.Drawing.Size(587, 279);
            this.layoutControlItemDepartmentsInvolved.Text = "layoutControlItemDepartmentsInvolved";
            this.layoutControlItemDepartmentsInvolved.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemDepartmentsInvolved.TextToControlDistance = 0;
            this.layoutControlItemDepartmentsInvolved.TextVisible = false;
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.CustomizationFormText = "splitterItem1";
            this.splitterItem1.Location = new System.Drawing.Point(0, 462);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(587, 6);
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel1});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // dockPanel1
            // 
            this.dockPanel1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.dockPanel1.Appearance.Options.UseBackColor = true;
            this.dockPanel1.Controls.Add(this.dockPanel1_Container);
            this.dockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Right;
            this.dockPanel1.ID = new System.Guid("dcc1b68c-a8dd-4c08-822e-42893f1d3a8a");
            this.dockPanel1.Location = new System.Drawing.Point(345, 0);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.Options.AllowDockBottom = false;
            this.dockPanel1.Options.AllowDockFill = false;
            this.dockPanel1.Options.AllowDockLeft = false;
            this.dockPanel1.Options.AllowDockTop = false;
            this.dockPanel1.Options.AllowFloating = false;
            this.dockPanel1.Options.FloatOnDblClick = false;
            this.dockPanel1.Options.ShowCloseButton = false;
            this.dockPanel1.Options.ShowMaximizeButton = false;
            this.dockPanel1.OriginalSize = new System.Drawing.Size(407, 200);
            this.dockPanel1.Size = new System.Drawing.Size(407, 770);
            this.dockPanel1.Text = "dockPanel1";
            this.dockPanel1.DockChanged += new System.EventHandler(this.dockPanel1_DockChanged);
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.layoutControlDockPanel);
            this.dockPanel1_Container.Location = new System.Drawing.Point(3, 29);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(401, 738);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // layoutControlDockPanel
            // 
            this.layoutControlDockPanel.AllowCustomizationMenu = false;
            this.layoutControlDockPanel.Controls.Add(this.gridControlExIncidentList);
            this.layoutControlDockPanel.Controls.Add(this.textBoxExIncidentdetails);
            this.layoutControlDockPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlDockPanel.Location = new System.Drawing.Point(0, 0);
            this.layoutControlDockPanel.Name = "layoutControlDockPanel";
            this.layoutControlDockPanel.Root = this.layoutControlGroup2;
            this.layoutControlDockPanel.Size = new System.Drawing.Size(401, 738);
            this.layoutControlDockPanel.TabIndex = 7;
            this.layoutControlDockPanel.Text = "layoutControl2";
            // 
            // gridControlExIncidentList
            // 
            this.gridControlExIncidentList.EnableAutoFilter = false;
            this.gridControlExIncidentList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExIncidentList.Location = new System.Drawing.Point(10, 31);
            this.gridControlExIncidentList.MainView = this.gridViewExIncidentList;
            this.gridControlExIncidentList.Name = "gridControlExIncidentList";
            this.gridControlExIncidentList.Size = new System.Drawing.Size(403, 246);
            this.gridControlExIncidentList.TabIndex = 19;
            this.gridControlExIncidentList.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExIncidentList});
            this.gridControlExIncidentList.ViewTotalRows = false;
            // 
            // gridViewExIncidentList
            // 
            this.gridViewExIncidentList.AllowFocusedRowChanged = true;
            this.gridViewExIncidentList.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExIncidentList.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExIncidentList.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExIncidentList.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExIncidentList.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExIncidentList.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExIncidentList.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExIncidentList.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExIncidentList.EnablePreviewLineForFocusedRow = false;
            this.gridViewExIncidentList.GridControl = this.gridControlExIncidentList;
            this.gridViewExIncidentList.Name = "gridViewExIncidentList";
            this.gridViewExIncidentList.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExIncidentList.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExIncidentList.ViewTotalRows = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3,
            this.layoutControlGroup4,
            this.emptySpaceItem2});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup2.Size = new System.Drawing.Size(423, 721);
            this.layoutControlGroup2.Text = "Root";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.ExpandButtonMode = DevExpress.Utils.Controls.ExpandButtonMode.Inverted;
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemIncidentList});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup3.Size = new System.Drawing.Size(417, 281);
            this.layoutControlGroup3.Text = "layoutControlGroup3";
            // 
            // layoutControlItemIncidentList
            // 
            this.layoutControlItemIncidentList.Control = this.gridControlExIncidentList;
            this.layoutControlItemIncidentList.CustomizationFormText = "layoutControlItemIncidentList";
            this.layoutControlItemIncidentList.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemIncidentList.Name = "layoutControlItemIncidentList";
            this.layoutControlItemIncidentList.Size = new System.Drawing.Size(407, 250);
            this.layoutControlItemIncidentList.Text = "layoutControlItemIncidentList";
            this.layoutControlItemIncidentList.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemIncidentList.TextToControlDistance = 0;
            this.layoutControlItemIncidentList.TextVisible = false;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "layoutControlGroup4";
            this.layoutControlGroup4.ExpandButtonMode = DevExpress.Utils.Controls.ExpandButtonMode.Inverted;
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemIncidentDetails});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 281);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup4.Size = new System.Drawing.Size(417, 416);
            this.layoutControlGroup4.Text = "layoutControlGroup4";
            // 
            // layoutControlItemIncidentDetails
            // 
            this.layoutControlItemIncidentDetails.Control = this.textBoxExIncidentdetails;
            this.layoutControlItemIncidentDetails.CustomizationFormText = "layoutControlItemIncidentDetails";
            this.layoutControlItemIncidentDetails.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemIncidentDetails.Name = "layoutControlItemIncidentDetails";
            this.layoutControlItemIncidentDetails.Size = new System.Drawing.Size(407, 385);
            this.layoutControlItemIncidentDetails.Text = "layoutControlItemIncidentDetails";
            this.layoutControlItemIncidentDetails.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemIncidentDetails.TextToControlDistance = 0;
            this.layoutControlItemIncidentDetails.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 697);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(417, 18);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(417, 18);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(417, 18);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.Dock = System.Windows.Forms.DockStyle.None;
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.ExpandCollapseItem.Name = "";
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.barButtonItemPrint,
            this.barButtonItemSave,
            this.barButtonItemRefresh,
            this.barButtonItemStartRegIncident,
            this.barButtonItemCreateNewIncident,
            this.barButtonItemCancelIncident,
            this.barButtonItemCancelAlarm});
            this.ribbonControl1.Location = new System.Drawing.Point(12, 44);
            this.ribbonControl1.MaxItemId = 19;
            this.ribbonControl1.MdiMergeStyle = DevExpress.XtraBars.Ribbon.RibbonMdiMergeStyle.Always;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbonControl1.ShowToolbarCustomizeItem = false;
            this.ribbonControl1.Size = new System.Drawing.Size(521, 119);
            this.ribbonControl1.Toolbar.ShowCustomizeItem = false;
            this.ribbonControl1.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // barButtonItemPrint
            // 
            this.barButtonItemPrint.Caption = "ItemPrint";
            this.barButtonItemPrint.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.Glyph")));
            this.barButtonItemPrint.Id = 6;
            this.barButtonItemPrint.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemPrint.Name = "barButtonItemPrint";
            this.barButtonItemPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPrint_ItemClick);
            // 
            // barButtonItemSave
            // 
            this.barButtonItemSave.Caption = "ItemSave";
            this.barButtonItemSave.Glyph = global::SmartCadGuiCommon.PrintRibbonControllerResources.RibbonPrintPreview_Save;
            this.barButtonItemSave.Id = 7;
            this.barButtonItemSave.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemSave.Name = "barButtonItemSave";
            this.barButtonItemSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemSave_ItemClick);
            // 
            // barButtonItemRefresh
            // 
            this.barButtonItemRefresh.Caption = "ItemRefresh";
            this.barButtonItemRefresh.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemRefresh.Glyph")));
            this.barButtonItemRefresh.Id = 8;
            this.barButtonItemRefresh.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemRefresh.Name = "barButtonItemRefresh";
            this.barButtonItemRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemRefresh_ItemClick);
            // 
            // barButtonItemStartRegIncident
            // 
            this.barButtonItemStartRegIncident.Caption = "ItemStartRegIncident";
            this.barButtonItemStartRegIncident.Enabled = false;
            this.barButtonItemStartRegIncident.Id = 12;
            this.barButtonItemStartRegIncident.LargeGlyph = global::SmartCadGuiCommon.Properties.ResourcesGui.StartRegistry;
            this.barButtonItemStartRegIncident.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemStartRegIncident.Name = "barButtonItemStartRegIncident";
            this.barButtonItemStartRegIncident.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemStartRegIncident.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemStartRegIncident_ItemClick);
            // 
            // barButtonItemCreateNewIncident
            // 
            this.barButtonItemCreateNewIncident.Caption = "ItemCreateNewIncident";
            this.barButtonItemCreateNewIncident.Id = 13;
            this.barButtonItemCreateNewIncident.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemCreateNewIncident.LargeGlyph")));
            this.barButtonItemCreateNewIncident.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemCreateNewIncident.Name = "barButtonItemCreateNewIncident";
            this.barButtonItemCreateNewIncident.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemCreateNewIncident.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemCreateNewIncident_ItemClick);
            // 
            // barButtonItemCancelIncident
            // 
            this.barButtonItemCancelIncident.Caption = "ItemCancelIncident";
            this.barButtonItemCancelIncident.Id = 15;
            this.barButtonItemCancelIncident.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemCancelIncident.LargeGlyph")));
            this.barButtonItemCancelIncident.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemCancelIncident.Name = "barButtonItemCancelIncident";
            this.barButtonItemCancelIncident.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemCancelIncident.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemCancelIncident_ItemClick);
            // 
            // barButtonItemCancelAlarm
            // 
            this.barButtonItemCancelAlarm.Caption = "ItemCancelAlarm";
            this.barButtonItemCancelAlarm.Enabled = false;
            this.barButtonItemCancelAlarm.Id = 18;
            this.barButtonItemCancelAlarm.LargeGlyph = global::SmartCadGuiCommon.Properties.ResourcesGui.Cancelar_alarma;
            this.barButtonItemCancelAlarm.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemCancelAlarm.Name = "barButtonItemCancelAlarm";
            this.barButtonItemCancelAlarm.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupCallInfo,
            this.ribbonPageGroupGeneralOptions});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "ribbonPage1";
            // 
            // ribbonPageGroupCallInfo
            // 
            this.ribbonPageGroupCallInfo.AllowMinimize = false;
            this.ribbonPageGroupCallInfo.AllowTextClipping = false;
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barButtonItemStartRegIncident);
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barButtonItemCancelAlarm);
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barButtonItemCreateNewIncident, true);
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barButtonItemCancelIncident);
            this.ribbonPageGroupCallInfo.Name = "ribbonPageGroupCallInfo";
            this.ribbonPageGroupCallInfo.ShowCaptionButton = false;
            this.ribbonPageGroupCallInfo.Text = "ribbonPageGroupCallInfo";
            // 
            // ribbonPageGroupGeneralOptions
            // 
            this.ribbonPageGroupGeneralOptions.AllowMinimize = false;
            this.ribbonPageGroupGeneralOptions.AllowTextClipping = false;
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemPrint);
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemSave);
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemRefresh);
            this.ribbonPageGroupGeneralOptions.Name = "ribbonPageGroupGeneralOptions";
            this.ribbonPageGroupGeneralOptions.ShowCaptionButton = false;
            this.ribbonPageGroupGeneralOptions.Text = "ribbonPageGroupGeneralOptions";
            this.ribbonPageGroupGeneralOptions.Visible = false;
            // 
            // DefaultTwitterFrontClientForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(752, 770);
            this.Controls.Add(this.ribbonControl1);
            this.Controls.Add(this.layoutControlDefaultTwitterFrontClient);
            this.Controls.Add(this.dockPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.LookAndFeel.SkinName = "Blue";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DefaultTwitterFrontClientForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DefaultLprFrontClientFormDevX_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.DefaultLprFrontClientForm_FormClosed);
            this.Load += new System.EventHandler(this.DefaultTwitterFrontClientForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlDefaultTwitterFrontClient)).EndInit();
            this.layoutControlDefaultTwitterFrontClient.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCallInformation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemQuestionControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartmentsInvolved)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanel1.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlDockPanel)).EndInit();
            this.layoutControlDockPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExIncidentList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExIncidentList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIncidentList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIncidentDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            this.ResumeLayout(false);

        }

  
        #endregion

        private System.Windows.Forms.ToolTip toolTipMain;
        private System.Windows.Forms.SaveFileDialog saveFileDialogMain;
        private System.Windows.Forms.PrintDialog printDialogMain;
        private System.Windows.Forms.ImageList imageListTabs;
        private ContextMenuStripEx contextMenuStripIncidents;
        private SearchableWebBrowser textBoxExIncidentdetails;
        private DevExpress.XtraLayout.LayoutControl layoutControlDefaultTwitterFrontClient;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
		private DevExpress.XtraBars.Docking.DockManager dockManager1;
		private DevExpress.XtraBars.Docking.DockPanel dockPanel1;
		private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
		private DevExpress.XtraLayout.LayoutControl layoutControlDockPanel;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemIncidentDetails;
		private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
		private DevExpress.XtraBars.BarButtonItem barButtonItemPrint;
		private DevExpress.XtraBars.BarButtonItem barButtonItemSave;
		private DevExpress.XtraBars.BarButtonItem barButtonItemRefresh;
		public DevExpress.XtraBars.BarButtonItem barButtonItemStartRegIncident;
		public DevExpress.XtraBars.BarButtonItem barButtonItemCreateNewIncident;
        private DevExpress.XtraBars.BarButtonItem barButtonItemCancelIncident;
		private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupCallInfo;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupGeneralOptions;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
		private GridControlEx gridControlExIncidentList;
		private GridViewEx gridViewExIncidentList;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemIncidentList;
        public TwitterCallInformationControl incidentInformationControl;
        private QuestionsControlDevX questionsControl;
        private DepartmentsInvolvedControl departmentsInvolvedControl;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemCallInformation;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemQuestionControl;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDepartmentsInvolved;
        public DevExpress.XtraBars.BarButtonItem barButtonItemCancelAlarm; 
    }
}
