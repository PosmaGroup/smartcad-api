﻿namespace SmartCadFirstLevel.Gui
{
    partial class PostTweetForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.simpleButtonTweet = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.textEditMessage = new DevExpress.XtraEditors.MemoEdit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditMessage.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButtonTweet
            // 
            this.simpleButtonTweet.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButtonTweet.Enabled = false;
            this.simpleButtonTweet.Location = new System.Drawing.Point(214, 12);
            this.simpleButtonTweet.LookAndFeel.SkinName = "Office 2010 Silver";
            this.simpleButtonTweet.LookAndFeel.UseDefaultLookAndFeel = false;
            this.simpleButtonTweet.Name = "simpleButtonTweet";
            this.simpleButtonTweet.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonTweet.TabIndex = 0;
            this.simpleButtonTweet.Text = "Tweet";
            this.simpleButtonTweet.Click += new System.EventHandler(this.simpleButtonTweet_Click);
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(214, 41);
            this.simpleButtonCancel.LookAndFeel.SkinName = "Office 2010 Silver";
            this.simpleButtonCancel.LookAndFeel.UseDefaultLookAndFeel = false;
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonCancel.TabIndex = 1;
            this.simpleButtonCancel.Text = "Cancel";
            this.simpleButtonCancel.Click += new System.EventHandler(this.simpleButtonCancel_Click);
            // 
            // textEditMessage
            // 
            this.textEditMessage.Location = new System.Drawing.Point(7, 10);
            this.textEditMessage.Name = "textEditMessage";
            this.textEditMessage.Properties.MaxLength = 140;
            this.textEditMessage.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.textEditMessage.Size = new System.Drawing.Size(196, 78);
            this.textEditMessage.TabIndex = 2;
            this.textEditMessage.EditValueChanged += new System.EventHandler(this.textEditMessage_EditValueChanged);
            this.textEditMessage.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textEditMessage_KeyPress);
            // 
            // PostTweetForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonCancel;
            this.ClientSize = new System.Drawing.Size(301, 96);
            this.Controls.Add(this.textEditMessage);
            this.Controls.Add(this.simpleButtonCancel);
            this.Controls.Add(this.simpleButtonTweet);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.LookAndFeel.SkinName = "Caramel";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PostTweetForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "What\'s happening?";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.FindVehicleForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.textEditMessage.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButtonTweet;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private DevExpress.XtraEditors.MemoEdit textEditMessage;
    }
}