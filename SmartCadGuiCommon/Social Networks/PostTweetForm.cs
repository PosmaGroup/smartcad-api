﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Collections;
using Smartmatic.SmartCad.Service;
using System.Reflection;
using SmartCadCore.Common;
using SmartCadCore.Core;

namespace SmartCadFirstLevel.Gui
{
    public partial class PostTweetForm : DevExpress.XtraEditors.XtraForm
    {
        public PostTweetForm()
        {
            InitializeComponent();
        }

        private void FindVehicleForm_Load(object sender, EventArgs e)
        {
            LoadLanguage();
        }

        private void LoadLanguage()
        {
            this.Text = ResourceLoader.GetString2("What's happening");
            simpleButtonCancel.Text = ResourceLoader.GetString2("Cancel");
            simpleButtonTweet.Text = ResourceLoader.GetString2("Tweet");
        }

        private void textEditMessage_EditValueChanged(object sender, EventArgs e)
        {
            if (textEditMessage.Text.Trim() != "")
                simpleButtonTweet.Enabled = true;
            else
                simpleButtonTweet.Enabled = false;
        }

        private void textEditMessage_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                simpleButtonTweet_Click(null, null);
            }
        }

        private void simpleButtonTweet_Click(object sender, EventArgs e)
        {
            BackgroundProcessForm processForm = new BackgroundProcessForm(
                 ResourceLoader.GetString2("Tweeting"),
                 this,
                 new MethodInfo[1] {
                    GetType().GetMethod("BackgroundTweet", BindingFlags.NonPublic | BindingFlags.Instance) 
                },
                 new object[1][] { new object[] { new object[] { textEditMessage.Text.Trim() } } }
             );
            processForm.CanThrowError = true;
            processForm.ShowDialog();
        }

        private void BackgroundTweet(object[] objs)
        {
            string tweet = (string)objs[0];

            SocialNetworksEngine.SetStatus(tweet);
        }

        private void simpleButtonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}