﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadGuiCommon.Classes
{
    public class Author
    {
        public string Name { get; set; }
        public string Uri { get; set; }
        public string UserName { get; set; }

        public override string ToString()
        {
            return Name + " (@" + UserName + ")";
        }

    }
}
