using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Drawing;

using Smartmatic.SmartCad.Service;
using SmartCadCore.Common;
using SmartCadCore.ClientData;
using SmartCadGuiCommon.Controls;

namespace SmartCadGui.Classes
{
    public class QuestionsNaturalLanguage
    {
        public static void Write(CallInformationControl callInformationControl, IList selectedIncidentTypes, IList questionsWithAnswers, RichTextBox richTextBox)
        {
            ServerServiceClient serverServiceClient = ServerServiceClient.GetInstance();

            Font font = richTextBox.Font;
            Font boldFont = new Font(richTextBox.Font, FontStyle.Bold);
            Font underlinedFont = new Font(richTextBox.Font, FontStyle.Underline | FontStyle.Bold);

            richTextBox.SelectionFont = underlinedFont;
            richTextBox.AppendText(ResourceLoader.GetString2("CallId"));
            richTextBox.AppendText(Environment.NewLine);

            if (callInformationControl.LineTelephoneNumber != null && callInformationControl.LineTelephoneNumber != "")
            {
                richTextBox.SelectionFont = boldFont;
                richTextBox.AppendText(ResourceLoader.GetString2("Phone") + ": ");
                richTextBox.SelectionFont = font;
                richTextBox.AppendText(callInformationControl.LineTelephoneNumber);
                richTextBox.AppendText(Environment.NewLine);
            }

            if (callInformationControl.CallerNumber != null && callInformationControl.CallerNumber != "")
            {
                richTextBox.SelectionFont = boldFont;
                richTextBox.AppendText(ResourceLoader.GetString2("OtherPhone") + ": ");
                richTextBox.SelectionFont = font;
                richTextBox.AppendText(callInformationControl.CallerNumber);
                richTextBox.AppendText(Environment.NewLine);
            }

            richTextBox.SelectionFont = boldFont;
            richTextBox.AppendText(ResourceLoader.GetString2("Caller") + ": ");
            richTextBox.SelectionFont = font;
            if (callInformationControl.LineName != null && callInformationControl.LineName != "")
                richTextBox.AppendText(callInformationControl.LineName);
            else
                richTextBox.AppendText(ResourceLoader.GetString2("Anonymous"));
            richTextBox.AppendText(Environment.NewLine);

            if (callInformationControl.CallerAddress.Zone != null && callInformationControl.CallerAddress.Zone != "")
            {
                richTextBox.SelectionFont = boldFont;
                richTextBox.AppendText(ResourceLoader.GetString2("Zone") + ": ");
                richTextBox.SelectionFont = font;
                richTextBox.AppendText(callInformationControl.CallerAddress.Zone);
                richTextBox.AppendText(Environment.NewLine);
            }

            if (callInformationControl.CallerAddress.Street != null && callInformationControl.CallerAddress.Street != "")
            {
                richTextBox.SelectionFont = boldFont;
                richTextBox.AppendText(ResourceLoader.GetString2("Street") + ": ");
                richTextBox.SelectionFont = font;
                richTextBox.AppendText(callInformationControl.CallerAddress.Street);
                richTextBox.AppendText(Environment.NewLine);
            }

            if (callInformationControl.CallerAddress.Reference != null && callInformationControl.CallerAddress.Reference != "")
            {
                richTextBox.SelectionFont = boldFont;
                richTextBox.AppendText(ResourceLoader.GetString2("AddressReference") + ": ");
                richTextBox.SelectionFont = font;
                richTextBox.AppendText(callInformationControl.CallerAddress.Reference);
                richTextBox.AppendText(Environment.NewLine);
            }

            if (callInformationControl.CallerAddress.More != null && callInformationControl.CallerAddress.More != "")
            {
                richTextBox.SelectionFont = boldFont;
                richTextBox.AppendText(ResourceLoader.GetString2("More") + ": ");
                richTextBox.SelectionFont = font;
                richTextBox.AppendText(callInformationControl.CallerAddress.More);
                richTextBox.AppendText(Environment.NewLine);
            }

            if (selectedIncidentTypes != null && selectedIncidentTypes.Count > 0)
            {
                richTextBox.SelectionFont = boldFont;
                richTextBox.AppendText(ResourceLoader.GetString2("IncidentTypes") + ": ");
                richTextBox.SelectionFont = font;
                string incidents = "";
                foreach (IncidentTypeClientData incidentTypes in selectedIncidentTypes)
                {
                    incidents += incidentTypes.FriendlyName + " (" + incidentTypes.CustomCode + "), ";
                }
                incidents = incidents.Substring(0, incidents.Length - 2);
                richTextBox.AppendText(incidents);
                richTextBox.AppendText(Environment.NewLine);
            }

            if (questionsWithAnswers != null && questionsWithAnswers.Count > 0)
            {
                richTextBox.AppendText(Environment.NewLine);
                richTextBox.SelectionFont = underlinedFont;
                richTextBox.AppendText(ResourceLoader.GetString2("Questions"));
                richTextBox.AppendText(Environment.NewLine);
            }
            foreach (ReportAnswerClientData phoneReportAnswer in questionsWithAnswers)
            {
                richTextBox.SelectionFont = boldFont;
                
                richTextBox.AppendText(phoneReportAnswer.QuestionText);
                richTextBox.AppendText(Environment.NewLine);

                richTextBox.SelectionFont = font;

                foreach (PossibleAnswerAnswerClientData possibleAnswerAnswer in phoneReportAnswer.Answers)
                {
                    richTextBox.AppendText(possibleAnswerAnswer.TextAnswer);
                    richTextBox.AppendText(Environment.NewLine); 
                }

                richTextBox.AppendText(Environment.NewLine);
            }

            //richTextBox.Font = font;
            boldFont.Dispose();
            underlinedFont.Dispose();
        }
    }
}
