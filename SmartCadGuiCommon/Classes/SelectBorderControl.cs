using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using SmartCadCore.Common;

namespace SmartCadGuiCommon.Classes
{

    public class SelectBorderControl : IDisposable
    {
        #region "Private Controls"

        private ResizeBox topLeftBox;
        private ResizeBox topRightBox;
        private ResizeBox bottomLeftBox;
        private ResizeBox bottomRightBox;
        private bool showLines = false;

        private class ResizeBox : UserControl
        {
            public enum BoxPosition
            {
                TopLeft,
                TopRight,
                BottomLeft,
                BottomRight
            }

            public ResizeBox(BoxPosition position)
            {
                InitializeComponent();
                this.Position = position;
            }

            private System.ComponentModel.IContainer components = null;

            protected override void Dispose(bool disposing)
            {
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }

            private void InitializeComponent()
            {
                this.SuspendLayout();
                this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
                this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
                this.BackColor = System.Drawing.Color.White;
                this.BorderStyle = System.Windows.Forms.BorderStyle.None;
                this.BackgroundImage = ResourceLoader.GetImage("ResizeBox");
                this.BackgroundImageLayout = ImageLayout.Center;
                this.Name = "ResizeBox";
                this.Size = new System.Drawing.Size(7, 7);
                this.ResumeLayout(false);
            }

            private BoxPosition _Position = BoxPosition.TopLeft;
            public BoxPosition Position
            {
                get
                {
                    return _Position;
                }
                set
                {
                    _Position = value;
                }
            }


        }

        #endregion

        #region "Public Methods"

        public SelectBorderControl(Control target, Boolean showResizeBoxes)
        {
            this._Target = target;

            target.Parent.Paint += new PaintEventHandler(Parent_Paint);


            topLeftBox = new ResizeBox(ResizeBox.BoxPosition.TopLeft);
            topRightBox = new ResizeBox(ResizeBox.BoxPosition.TopRight);
            bottomLeftBox = new ResizeBox(ResizeBox.BoxPosition.BottomLeft);
            bottomRightBox = new ResizeBox(ResizeBox.BoxPosition.BottomRight);

            if (showResizeBoxes)
                ShowResizeBoxes();
        }

        public void Parent_Paint(object sender, PaintEventArgs e)
        {
            Pen pen = null;
            if (showLines)
                pen = new Pen(new SolidBrush(Color.DarkBlue), 1);
            else
                pen = new Pen(new SolidBrush(Color.Transparent), 1);
            Graphics g = e.Graphics;

            pen.DashStyle = System.Drawing.Drawing2D.DashStyle.DashDot;
            g.DrawRectangle(pen, new Rectangle(Target.Left - 5, Target.Top - 5, Target.Width + 9, Target.Height + 9));

        }

        public void ShowResizeBoxes()
        {

            PositionTopLeftBox();
            PositionTopRightBox();
            PositionBottomLeftBox();
            PositionBottomRightBox();
            Target.Parent.Controls.Add(topLeftBox);
            Target.Parent.Controls.Add(topRightBox);
            Target.Parent.Controls.Add(bottomLeftBox);
            Target.Parent.Controls.Add(bottomRightBox);


            Target.Parent.Paint += Parent_Paint;
            Target.Parent.Invalidate(new Rectangle(Target.Left - 6, Target.Top - 6, Target.Width + 12, Target.Height + 12));
            showLines = true;

        }

        public void HideResizeBoxes()
        {
            try
            {
                Target.Parent.Controls.Remove(topLeftBox);
                Target.Parent.Controls.Remove(topRightBox);
                Target.Parent.Controls.Remove(bottomLeftBox);
                Target.Parent.Controls.Remove(bottomRightBox);

                Target.Parent.Invalidate(new Rectangle(Target.Left - 6, Target.Top - 6, Target.Width + 12, Target.Height + 12));
                Target.Parent.Paint -= Parent_Paint;
                showLines = false;
            }
            catch { }

        }

        void IDisposable.Dispose()
        {
            HideResizeBoxes();
        }

        #endregion

        #region "Move Event Handlers"

        private Point mouseLocation;

        private void Boxes_MouseDown(object sender, MouseEventArgs e)
        {
            mouseLocation.X = e.X;
            mouseLocation.Y = e.Y;
        }


        #endregion

        #region "Positioning Commands"

        private void PositionTopLeftBox()
        {
            topLeftBox.Top = Target.Top - topLeftBox.Height - 1;
            topLeftBox.Left = Target.Left - topLeftBox.Width - 1;
        }


        private void PositionTopRightBox()
        {
            topRightBox.Top = Target.Top - topRightBox.Height - 1;
            topRightBox.Left = Target.Left + Target.Width + 1;
        }



        private void PositionBottomLeftBox()
        {
            bottomLeftBox.Top = Target.Top + Target.Height + 1;
            bottomLeftBox.Left = Target.Left - topLeftBox.Width - 1;
        }


        private void PositionBottomRightBox()
        {
            bottomRightBox.Top = Target.Top + Target.Height + 1;
            bottomRightBox.Left = Target.Left + Target.Width + 1;
        }

        #endregion

        #region "Properties"

        private Control _Target;
        public Control Target
        {
            get
            {
                if (_Target == null)
                    _Target = new Control();
                return _Target;
            }
        }

        #endregion
    }

}
