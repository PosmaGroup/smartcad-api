﻿using SmartCadControls;
using SmartCadControls.Controls;
using SmartCadCore.ClientData;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadGuiCommon.Classes
{
    public class TwitterAlarmGridData : GridControlData
    {
        DateTime startDate;
        public AlarmTwitterClientData alarmClientData { get; set; }
        BindingList<TweetGridData> tweetList = new BindingList<TweetGridData>();

        //public TwitterAlarmGridData()
        //{
        //}

        public TwitterAlarmGridData(AlarmTwitterClientData alarmClientData)
        : base(alarmClientData)
        {
            this.alarmClientData = alarmClientData;
            this.Name = alarmClientData.Query.Name;
            this.Description = alarmClientData.Query.Description;
            this.startDate = (DateTime)alarmClientData.StartAlarm;
            this.TweetsAmount = alarmClientData.Tweets.Count;
            foreach (TweetClientData tweet in alarmClientData.Tweets)
            {
                tweetList.Add(new TweetGridData(tweet));
            }
        }


        [GridControlRowInfoData(Visible = true, Searchable = true)]
        public string Name { get; set; }

        [GridControlRowInfoData(Visible = true, Searchable = true)]
        public string Description { get; set; }

        [GridControlRowInfoData(Visible = true, Searchable = true)]
        public string StartTime
        {
            get
            {
                return startDate.ToLongTimeString();
            }
        }

        [GridControlRowInfoData(Visible = true, Searchable = true)]
        public int TweetsAmount { get; set; }

        public BindingList<TweetGridData> Tweets
        {
            get
            {
                return tweetList;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj is TwitterAlarmGridData && (obj as TwitterAlarmGridData).alarmClientData.Code == this.alarmClientData.Code)
            {
                return true;
            }

            return false;
        }
    }
}
