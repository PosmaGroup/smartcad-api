﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Globalization;
using System.Net;
using System.IO;
using System.Collections;
using System.ComponentModel;
using SmartCadCore.Core;
using SmartCadCore.ClientData;
using SmartCadGuiCommon.Classes;

namespace SmartCadFirstLevel.Gui
{
    public class TwitterSearch
    {
        public static List<TweetGridData> GetSearchResults(string searchString)
        {
            List<TweetGridData> tweets = new List<TweetGridData>();

            try
            {
                List<Tweet> results = SocialNetworksEngine.GetSearchResults(searchString);
                
                foreach (Tweet tweet in results)
                {
                    tweets.Add(new TweetGridData(new TweetClientData()
                    {
                        Published = DateTime.ParseExact(tweet.created_at, "ddd MMM dd HH:mm:ss zzzz yyyy", CultureInfo.InvariantCulture),
                        AuthorName = tweet.user.name,
                        AuthorUserName = tweet.user.screen_name,
                        AuthorUri = tweet.user.url,
                        Link = tweet.textReferenceUrl,
                        Content = tweet.text,
                        CustomCode = tweet.id_str,
                        ImageUrl = tweet.user.profile_image_url
                    }));
                }
            }
            catch (Exception x)
            {
            }

            return tweets;
        }

        public static BindingList<TweetGridData> GetMentions()
        {
            BindingList<TweetGridData> tweets = new BindingList<TweetGridData>();

            try
            {
                List<Tweet> results = SocialNetworksEngine.GetMentions("");

                foreach (Tweet tweet in results)
                {
                    tweets.Add(new TweetGridData(new TweetClientData()
                    {
                        Published = DateTime.ParseExact(tweet.created_at, "ddd MMM dd HH:mm:ss zzzz yyyy", CultureInfo.InvariantCulture),
                        AuthorName = tweet.user.name,
                        AuthorUserName = tweet.user.screen_name,
                        AuthorUri = tweet.user.url,
                        Link = tweet.textReferenceUrl,
                        Content = tweet.text,
                        CustomCode = tweet.id_str,
                        ImageUrl = tweet.user.profile_image_url
                    }));
                    //if (long.Parse(tweets[tweets.Count - 1].Id) > since_id)
                    //{
                    //    since_id = long.Parse(tweets[tweets.Count - 1].Id);
                    //}
                }
            }
            catch (Exception x)
            {
            }

            return tweets;
        }

        public static void SetStatus(string tweet)
        {

            try
            {
                SocialNetworksEngine.SetStatus(tweet);
            }
            catch (Exception x)
            {
            }
        }
    }


}
