﻿using SmartCadCore.ClientData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadGuiCommon.Administration
{
    #region DataClass

    public class SensorTelemetryGridDataAdm
    {
        public SensorTelemetryGridDataAdm(SensorTelemetryThresholdClientData sensor)
        {
            SensorTelemetryThreshold = sensor;
        }

        public SensorTelemetryGridDataAdm()
        {
            SensorTelemetryThreshold = new SensorTelemetryThresholdClientData();
            SensorTelemetryThreshold.Low = -1;
            SensorTelemetryThreshold.High = -1;
            SensorTelemetryThreshold.SensorTelemetry = new SensorTelemetryClientData();
            SensorTelemetryThreshold.SensorTelemetry.Name = "sensor";
            SensorTelemetryThreshold.SensorTelemetry.Variable = "variable";

            SensorTelemetryThreshold.SensorTelemetry.Threshold = new List<SensorTelemetryThresholdClientData>();
            SensorTelemetryThreshold.SensorTelemetry.Threshold.Add(SensorTelemetryThreshold);
        }

        private void ReCreate()
        {
            SensorTelemetryClientData SensorTelemetry = new SensorTelemetryClientData();
            SensorTelemetry.Name = Sensor;
            SensorTelemetry.Variable = Variable;
            SensorTelemetry.Threshold = new List<SensorTelemetryThresholdClientData>();
            SensorTelemetry.Threshold.Add(SensorTelemetryThreshold);
            SensorTelemetryThreshold.SensorTelemetry = SensorTelemetry;
        }

        public SensorTelemetryThresholdClientData SensorTelemetryThreshold { get; set; }


        public string Variable
        {
            get
            {
                return SensorTelemetryThreshold.SensorTelemetry.Variable;
            }
            set
            {
                //This code was commented because it removes the code of the sensor telemetry. Jeem
                //if (SensorTelemetryThreshold.SensorTelemetry.Variable != value)
                //{
                //    SensorTelemetryThreshold.SensorTelemetry.Threshold.Remove(SensorTelemetryThreshold);
                //    ReCreate();
                //}
                SensorTelemetryThreshold.SensorTelemetry.Variable = value;
            }
        }

        public double High
        {
            get
            {
                return SensorTelemetryThreshold.High.Value;
            }
            set
            {
                SensorTelemetryThreshold.High = value;
            }
        }

        public double Low
        {
            get
            {
                return SensorTelemetryThreshold.Low.Value;
            }
            set
            {
                SensorTelemetryThreshold.Low = value;
            }
        }

        public string Sensor
        {
            get
            {
                return SensorTelemetryThreshold.SensorTelemetry.Name;
            }
            set
            {
                //This code was commented because it removes the code of the sensor telemetry. Jeem
                //if (SensorTelemetryThreshold.SensorTelemetry.Name != value)
                //{
                //    SensorTelemetryThreshold.SensorTelemetry.Threshold.Remove(SensorTelemetryThreshold);
                //    ReCreate();
                //}
                SensorTelemetryThreshold.SensorTelemetry.Name = value;
            }
        }


        public override bool Equals(object obj)
        {
            if (obj is SensorTelemetryGridDataAdm)
            {
                SensorTelemetryGridDataAdm data = (obj as SensorTelemetryGridDataAdm);

                if (data.Sensor == this.Sensor && data.Variable == this.Variable &&
                    data.Low == this.Low && data.High == this.High)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else if (obj is SensorTelemetryClientData)
            {
                SensorTelemetryClientData data = (obj as SensorTelemetryClientData);

                if (data.Name == this.Sensor && data.Variable == this.Variable)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
    #endregion
}
