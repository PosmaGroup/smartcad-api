using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using SmartCadCore.Core;


namespace SmartCadGuiCommon
{
    public partial class WaitForm : DevExpress.XtraEditors.XtraForm
    {
        private string message;

        //private Thread thread;

        public string Message
        {
            get
            {
                return message;
            }
            set
            {
                message = value;
                this.label1.Text = value;
            }
        }


        public WaitForm()
        {
            InitializeComponent();
        }

        private void WaitForm_Load(object sender, EventArgs e)
        {
            /*
            ThreadStart threadStart = new ThreadStart(this.Initilize);
            this.thread = new Thread(threadStart);
            this.thread.Start();
            */
           
        }

        private void WaitForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            /*
            this.thread.Interrupt();
             */
        }

        private void Initilize()
        {
            while (this.Visible)
            {
                FormUtil.InvokeRequired(this, new MethodInvoker(this.Increment));
                Thread.Sleep(500);
            }
        }

        private void Increment()
        {
            /*
            if (this.progressBar.Value + 7 >= this.progressBar.Maximum)
            {
                this.progressBar.Value = this.progressBar.Minimum;
            }

            this.progressBar.Value += 7;
             * */
        }
    }
}