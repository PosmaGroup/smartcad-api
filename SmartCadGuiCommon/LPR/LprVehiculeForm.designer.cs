using SmartCadControls;
using SmartCadCore.Common;
using DevExpress.XtraPrinting;
using SmartCadControls.Controls;
namespace SmartCadGuiCommon
{
    partial class LprVehiculeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LprAlerts));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabVehicule = new System.Windows.Forms.TabControl(); // Tabla de Alarmas
            this.tabDataloggerVehicule = new System.Windows.Forms.TabPage();
            this.gridControlVehiculeIn = new SmartCadControls.GridControlEx();
            this.gridViewDataloggerVehicule = new SmartCadControls.GridViewEx();
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItemPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSave = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.toolTipController1 = new DevExpress.Utils.ToolTipController(this.components);
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.DateEditEndDate = new DevExpress.XtraEditors.DateEdit();
            this.DateEditStartDate = new DevExpress.XtraEditors.DateEdit();
            this.pictureCar = new DevExpress.XtraEditors.PictureEdit();
            this.picturePlate = new DevExpress.XtraEditors.PictureEdit();
            this.groupPictureCarControl = new DevExpress.XtraEditors.GroupControl();
            this.groupInformationCar = new DevExpress.XtraEditors.GroupControl();
            this.imagePlate = new DevExpress.XtraEditors.GroupControl();
            this.informationUser = new DevExpress.XtraEditors.GroupControl();
            this.containerInformacionCar = new DevExpress.XtraBars.Docking.ControlContainer();
            this.containerVehicule = new DevExpress.XtraBars.Docking.DockPanel();
            this.textBoxPlate = new TextBoxEx();
            this.textBoxVehiclePlate = new TextBoxEx();
            this.textBoxVehicleModel = new TextBoxEx();
            this.textBoxVehicleYear = new TextBoxEx();
            this.textBoxVehicleColour = new TextBoxEx();
            this.textBoxUserName = new TextBoxEx();
            this.textBoxUserId = new TextBoxEx();
            this.textBoxUserPhone = new TextBoxEx();
            this.buttonVideo = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemPlate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemVehiclePlate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemVehicleModel = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemVehicleYear = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemVehicleColour = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemUserName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemUserId = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemUserPhone = new DevExpress.XtraLayout.LayoutControlItem();
            this.ribbonPageGroupGeneralOptions = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);


            this.tabVehicule.SuspendLayout();
            this.tabDataloggerVehicule.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlVehiculeIn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDataloggerVehicule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout(); 
            ((System.ComponentModel.ISupportInitialize)(this.groupPictureCarControl)).BeginInit();
            this.groupPictureCarControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupInformationCar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.informationUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagePlate)).BeginInit();
            this.groupPictureCarControl.SuspendLayout();
            this.containerInformacionCar.SuspendLayout();
            this.containerVehicule.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureCar.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picturePlate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditStartDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditStartDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();  
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit(); 
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit(); 
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPlate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemVehicleModel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemVehicleYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemVehicleColour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemUserId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemUserPhone)).BeginInit();
            this.SuspendLayout();
            // 
            // tabVehicule
            // Tabla de Vehiculos registrados 
            this.tabVehicule.Controls.Add(this.tabDataloggerVehicule);
            this.tabVehicule.Location = new System.Drawing.Point(4, 4);
            //this.tabVehicule.MinimumSize = new System.Drawing.Size(1350, 300); //Para mi
            this.tabVehicule.MinimumSize = new System.Drawing.Size(1600, 240); //Para ellos
            //this.tabVehicule.MaximumSize = new System.Drawing.Size(1600, 300); //Para ellos
            this.tabVehicule.Name = "tabVehicule";
            this.tabVehicule.SelectedIndex = 0;
            //this.tabVehicule.Size = new System.Drawing.Size(1650, 323);
            this.tabVehicule.TabIndex = 2;
            // 
            //tabDataloggerVehicule
            // 
            this.tabDataloggerVehicule.Controls.Add(this.gridControlVehiculeIn);
            this.tabDataloggerVehicule.Location = new System.Drawing.Point(4, 22);
            this.tabDataloggerVehicule.Name = "tabDataloggerVehicule";
            this.tabDataloggerVehicule.Padding = new System.Windows.Forms.Padding(2);
            this.tabDataloggerVehicule.Size = new System.Drawing.Size(554, 297);
            this.tabDataloggerVehicule.TabIndex = 1;
            this.tabDataloggerVehicule.Text = ResourceLoader.GetString2("DataloggerAlarms");
            this.tabDataloggerVehicule.UseVisualStyleBackColor = true;
            // 
            // gridControlVehiculeIn
            // 
            this.gridControlVehiculeIn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlVehiculeIn.EnableAutoFilter = false;
            this.gridControlVehiculeIn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlVehiculeIn.Location = new System.Drawing.Point(2, 2);
            this.gridControlVehiculeIn.MainView = this.gridViewDataloggerVehicule;
            this.gridControlVehiculeIn.Margin = new System.Windows.Forms.Padding(2);
            this.gridControlVehiculeIn.Name = "gridControlVehiculeIn";
            this.gridControlVehiculeIn.Size = new System.Drawing.Size(550, 293);
            this.gridControlVehiculeIn.TabIndex = 8;
            this.gridControlVehiculeIn.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDataloggerVehicule});
            this.gridControlVehiculeIn.ViewTotalRows = false;
            //ScrollBar vScrollBar1 = new VScrollBar();
            //vScrollBar1.Dock = DockStyle.Left;
            //this.gridControlVehiculeIn.Controls.Add(vScrollBar1);
            // 
            this.gridViewDataloggerVehicule.AllowFocusedRowChanged = true;
            this.gridViewDataloggerVehicule.Appearance.FocusedCell.BackColor = System.Drawing.Color.Transparent;
            this.gridViewDataloggerVehicule.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewDataloggerVehicule.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewDataloggerVehicule.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewDataloggerVehicule.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewDataloggerVehicule.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewDataloggerVehicule.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewDataloggerVehicule.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewDataloggerVehicule.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewDataloggerVehicule.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gridViewDataloggerVehicule.Appearance.Row.Options.UseBackColor = true;
            this.gridViewDataloggerVehicule.EnablePreviewLineForFocusedRow = false;
            this.gridViewDataloggerVehicule.GridControl = this.gridControlVehiculeIn;
            this.gridViewDataloggerVehicule.GroupFormat = "[#image]{1} {2}";
            this.gridViewDataloggerVehicule.Name = "gridViewDataloggerVehicule";
            this.gridViewDataloggerVehicule.OptionsBehavior.Editable = false;
            this.gridViewDataloggerVehicule.OptionsMenu.EnableColumnMenu = false;
            this.gridViewDataloggerVehicule.OptionsMenu.EnableFooterMenu = false;
            this.gridViewDataloggerVehicule.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridViewDataloggerVehicule.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewDataloggerVehicule.OptionsView.ShowAutoFilterRow = true;
            this.gridViewDataloggerVehicule.OptionsView.ShowDetailButtons = false;
            this.gridViewDataloggerVehicule.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewDataloggerVehicule.OptionsView.ShowGroupPanel = false;
            this.gridViewDataloggerVehicule.OptionsView.ShowIndicator = false;
            this.gridViewDataloggerVehicule.ViewTotalRows = false;
            //this.gridViewDataloggerVehicule.SelectionWillChange += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewExPreviousAlarms_SelectionWillChange);

            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.picturePlate);
            this.layoutControl1.Controls.Add(this.textBoxPlate);
            this.layoutControl1.AllowCustomizationMenu = false;
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(1, 1);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1590, 653);
            this.layoutControl1.TabIndex = 9;
            this.layoutControl1.Text = "layoutControl1";
            //
            // pictureCar
            //
            this.pictureCar.Size = new System.Drawing.Size(300, 380);
            this.pictureCar.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.pictureCar.Location = new System.Drawing.Point(10, 30);
            this.pictureCar.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            // 
            // groupPictureCarControl
            //
            //this.groupPictureCarControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupPictureCarControl.Controls.Add(this.pictureCar); 
            this.groupPictureCarControl.Location = new System.Drawing.Point(4, 250);
            this.groupPictureCarControl.Name = "groupPictureCarControl";
            this.groupPictureCarControl.Size = new System.Drawing.Size(320, 420);
            this.groupPictureCarControl.TabIndex = 11;
            //
            // groupInformationCar
            //
            this.groupInformationCar.Controls.Add(this.layoutControl2);
            //this.groupInformationCar.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupInformationCar.Location = new System.Drawing.Point(4, 3);
            this.groupInformationCar.Name = "groupInformationCar";
            this.groupInformationCar.Size = new System.Drawing.Size(620, 200);
            this.groupInformationCar.TabIndex = 11;
            //
            // informationUser
            //
            this.informationUser.Controls.Add(this.layoutControl3);
            //this.informationUser.Dock = System.Windows.Forms.DockStyle.None;
            this.informationUser.Location = new System.Drawing.Point(625, 3);
            this.informationUser.Name = "InformationCar";
            this.informationUser.Size = new System.Drawing.Size(620, 200);
            this.informationUser.TabIndex = 11;
            //
            // imagePlate
            //
            this.imagePlate.Location = new System.Drawing.Point(4, 205);
            this.imagePlate.Size = new System.Drawing.Size(620, 220);
            this.imagePlate.Controls.Add(this.layoutControl1);
            //
            // picturePlate
            //
            this.picturePlate.Size = new System.Drawing.Size(300, 100);
            this.picturePlate.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.picturePlate.Location = new System.Drawing.Point(10, 30);
            this.picturePlate.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            // 
            // containerInformacionCar
            // 
            this.containerInformacionCar.Controls.Add(this.groupInformationCar);
            this.containerInformacionCar.Controls.Add(this.informationUser);
            this.containerInformacionCar.Controls.Add(this.imagePlate);
            this.containerInformacionCar.Controls.Add(this.buttonVideo);
            //this.containerInformacionCar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.containerInformacionCar.Location = new System.Drawing.Point(330, 250);
            this.containerInformacionCar.Name = "containerInformacionCar";
            this.containerInformacionCar.Size = new System.Drawing.Size(1250, 420);
            //
            // containerVehicule
            //
            this.containerVehicule.Controls.Add(this.tabVehicule);
            this.containerVehicule.Controls.Add(this.groupPictureCarControl);
            this.containerVehicule.Controls.Add(this.containerInformacionCar);
            this.containerVehicule.Dock = DevExpress.XtraBars.Docking.DockingStyle.Fill;
            this.containerVehicule.Options.AllowDockBottom = false;
            this.containerVehicule.Options.AllowDockRight = false;
            this.containerVehicule.Options.AllowDockLeft = false;
            this.containerVehicule.Options.AllowDockTop = false;
            this.containerVehicule.Options.AllowFloating = false;
            this.containerVehicule.Options.FloatOnDblClick = false;
            this.containerVehicule.Options.ShowCloseButton = false;
            this.containerVehicule.Options.ShowMaximizeButton = false;
            this.containerVehicule.Size = new System.Drawing.Size(1590, 663);
            this.containerVehicule.Name = "containerVehicule";
            // 
            // textBoxPlate
            // 
            this.textBoxPlate.AllowsLetters = true;
            this.textBoxPlate.AllowsNumbers = true;
            this.textBoxPlate.AllowsPunctuation = true;
            this.textBoxPlate.AllowsSeparators = true;
            this.textBoxPlate.AllowsSymbols = true;
            this.textBoxPlate.AllowsWhiteSpaces = true;
            this.textBoxPlate.Enabled = false;
            this.textBoxPlate.ExtraAllowedChars = "";
            this.textBoxPlate.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxPlate.Location = new System.Drawing.Point(4, 150);
            this.textBoxPlate.MaxLength = 40;
            this.textBoxPlate.Name = "textBoxPlate";
            this.textBoxPlate.NonAllowedCharacters = "";
            this.textBoxPlate.RegularExpresion = "";
            this.textBoxPlate.Size = new System.Drawing.Size(39, 20);
            this.textBoxPlate.TabIndex = 47;
            //
            // buttonVideo
            //
            this.buttonVideo.Location = new System.Drawing.Point(740, 320);
            this.buttonVideo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonVideo.Enabled = false;
            //this.buttonVideo.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonGoto.Image")));
            //this.buttonVideo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.buttonVideo.Name = "buttonVideo";
            this.buttonVideo.Size = new System.Drawing.Size(80, 25);
            this.buttonVideo.TabIndex = 47;
            //this.buttonVideo.EnabledChanged += new System.EventHandler(this.simpleButtonGoto_EnabledChanged);
            //this.buttonVideo.Click += new System.EventHandler(this.simpleButtonGoto_Click);
            //
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.textBoxVehiclePlate);
            this.layoutControl2.Controls.Add(this.textBoxVehicleModel);
            this.layoutControl2.Controls.Add(this.textBoxVehicleYear);
            this.layoutControl2.Controls.Add(this.textBoxVehicleColour);
            this.layoutControl2.AllowCustomizationMenu = false;
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(1, 1);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(1590, 653);
            this.layoutControl2.TabIndex = 9;
            this.layoutControl2.Text = "layoutControl2";
            //
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.textBoxUserName);
            this.layoutControl3.Controls.Add(this.textBoxUserId);
            this.layoutControl3.Controls.Add(this.textBoxUserPhone);
            this.layoutControl3.AllowCustomizationMenu = false;
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(1, 1);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.layoutControlGroup3;
            this.layoutControl3.Size = new System.Drawing.Size(1590, 653);
            this.layoutControl3.TabIndex = 9;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // layoutControlItemPlate
            // 
            this.layoutControlItemPlate.Control = this.textBoxPlate;
            this.layoutControlItemPlate.CustomizationFormText = "layoutControlItemPlate";
            this.layoutControlItemPlate.Location = new System.Drawing.Point(3, 150);
            this.layoutControlItemPlate.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItemPlate.MinSize = new System.Drawing.Size(100, 30);
            this.layoutControlItemPlate.Name = "layoutControlItemPlate";
            this.layoutControlItemPlate.Size = new System.Drawing.Size(194, 30);
            this.layoutControlItemPlate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemPlate.Text = "layoutControlItemPlate";
            this.layoutControlItemPlate.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemPlate.TextSize = new System.Drawing.Size(140, 20);
            // 
            // layoutControlItemVehiclePlate
            // 
            this.layoutControlItemVehiclePlate.Control = this.textBoxVehiclePlate;
            this.layoutControlItemVehiclePlate.CustomizationFormText = "layoutControlItemVehiclePlate";
            this.layoutControlItemVehiclePlate.Location = new System.Drawing.Point(3, 4);
            this.layoutControlItemVehiclePlate.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItemVehiclePlate.MinSize = new System.Drawing.Size(100, 30);
            this.layoutControlItemVehiclePlate.Name = "layoutControlItemVehiclePlate";
            this.layoutControlItemVehiclePlate.Size = new System.Drawing.Size(194, 30);
            this.layoutControlItemVehiclePlate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemVehiclePlate.Text = "layoutControlItemVehiclePlate";
            this.layoutControlItemVehiclePlate.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemVehiclePlate.TextSize = new System.Drawing.Size(140, 20);
            // 
            // layoutControlItemVehicleModel
            // 
            this.layoutControlItemVehicleModel.Control = this.textBoxVehicleModel;
            this.layoutControlItemVehicleModel.CustomizationFormText = "layoutControlItemVehicleModel";
            this.layoutControlItemVehicleModel.Location = new System.Drawing.Point(3, 4);
            this.layoutControlItemVehicleModel.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItemVehicleModel.MinSize = new System.Drawing.Size(100, 30);
            this.layoutControlItemVehicleModel.Name = "layoutControlItemVehicleModel";
            this.layoutControlItemVehicleModel.Size = new System.Drawing.Size(194, 30);
            this.layoutControlItemVehicleModel.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemVehicleModel.Text = "layoutControlItemVehicleModel";
            this.layoutControlItemVehicleModel.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemVehicleModel.TextSize = new System.Drawing.Size(140, 20);
            // 
            // layoutControlItemVehicleYear
            // 
            this.layoutControlItemVehicleYear.Control = this.textBoxVehicleYear;
            this.layoutControlItemVehicleYear.CustomizationFormText = "layoutControlItemVehicleYear";
            this.layoutControlItemVehicleYear.Location = new System.Drawing.Point(3, 4);
            this.layoutControlItemVehicleYear.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItemVehicleYear.MinSize = new System.Drawing.Size(100, 30);
            this.layoutControlItemVehicleYear.Name = "layoutControlItemVehicleYear";
            this.layoutControlItemVehicleYear.Size = new System.Drawing.Size(194, 30);
            this.layoutControlItemVehicleYear.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemVehicleYear.Text = "layoutControlItemVehicleYear";
            this.layoutControlItemVehicleYear.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemVehicleYear.TextSize = new System.Drawing.Size(140, 20);
            // 
            // layoutControlItemVehicleColour
            // 
            this.layoutControlItemVehicleColour.Control = this.textBoxVehicleColour;
            this.layoutControlItemVehicleColour.CustomizationFormText = "layoutControlItemVehicleColour";
            this.layoutControlItemVehicleColour.Location = new System.Drawing.Point(3, 4);
            this.layoutControlItemVehicleColour.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItemVehicleColour.MinSize = new System.Drawing.Size(100, 30);
            this.layoutControlItemVehicleColour.Name = "layoutControlItemVehicleColour";
            this.layoutControlItemVehicleColour.Size = new System.Drawing.Size(194, 30);
            this.layoutControlItemVehicleColour.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemVehicleColour.Text = "layoutControlItemVehicleColour";
            this.layoutControlItemVehicleColour.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemVehicleColour.TextSize = new System.Drawing.Size(140, 20);
            // 
            // layoutControlItemUserName
            // 
            this.layoutControlItemUserName.Control = this.textBoxUserName;
            this.layoutControlItemUserName.CustomizationFormText = "layoutControlItemUserName";
            this.layoutControlItemUserName.Location = new System.Drawing.Point(3, 4);
            this.layoutControlItemUserName.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItemUserName.MinSize = new System.Drawing.Size(100, 30);
            this.layoutControlItemUserName.Name = "layoutControlItemUserName";
            this.layoutControlItemUserName.Size = new System.Drawing.Size(194, 30);
            this.layoutControlItemUserName.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemUserName.Text = "layoutControlItemUserName";
            this.layoutControlItemUserName.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemUserName.TextSize = new System.Drawing.Size(140, 20);
            // 
            // layoutControlItemUserId
            // 
            this.layoutControlItemUserId.Control = this.textBoxUserId;
            this.layoutControlItemUserId.CustomizationFormText = "layoutControlItemUserId";
            this.layoutControlItemUserId.Location = new System.Drawing.Point(3, 4);
            this.layoutControlItemUserId.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItemUserId.MinSize = new System.Drawing.Size(100, 30);
            this.layoutControlItemUserId.Name = "layoutControlItemUserId";
            this.layoutControlItemUserId.Size = new System.Drawing.Size(194, 30);
            this.layoutControlItemUserId.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemUserId.Text = "layoutControlItemUserId";
            this.layoutControlItemUserId.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemUserId.TextSize = new System.Drawing.Size(140, 20);
            // 
            // layoutControlItemUserPhone
            // 
            this.layoutControlItemUserPhone.Control = this.textBoxUserPhone;
            this.layoutControlItemUserPhone.CustomizationFormText = "layoutControlItemUserPhone";
            this.layoutControlItemUserPhone.Location = new System.Drawing.Point(3, 4);
            this.layoutControlItemUserPhone.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItemUserPhone.MinSize = new System.Drawing.Size(100, 30);
            this.layoutControlItemUserPhone.Name = "layoutControlItemUserPhone";
            this.layoutControlItemUserPhone.Size = new System.Drawing.Size(194, 30);
            this.layoutControlItemUserPhone.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemUserPhone.Text = "layoutControlItemUserPhone";
            this.layoutControlItemUserPhone.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemUserPhone.TextSize = new System.Drawing.Size(140, 20);
            // 
            // textBoxVehiclePlate
            // 
            this.textBoxVehiclePlate.AllowsLetters = true;
            this.textBoxVehiclePlate.AllowsNumbers = true;
            this.textBoxVehiclePlate.AllowsPunctuation = true;
            this.textBoxVehiclePlate.AllowsSeparators = true;
            this.textBoxVehiclePlate.AllowsSymbols = true;
            this.textBoxVehiclePlate.AllowsWhiteSpaces = true;
            this.textBoxVehiclePlate.Enabled = false;
            this.textBoxVehiclePlate.ExtraAllowedChars = "";
            this.textBoxVehiclePlate.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxVehiclePlate.Location = new System.Drawing.Point(4, 150);
            this.textBoxVehiclePlate.MaxLength = 40;
            this.textBoxVehiclePlate.Name = "textBoxVehiclePlate";
            this.textBoxVehiclePlate.NonAllowedCharacters = "";
            this.textBoxVehiclePlate.RegularExpresion = "";
            this.textBoxVehiclePlate.Size = new System.Drawing.Size(39, 20);
            this.textBoxVehiclePlate.TabIndex = 47;
            // 
            // textBoxVehicleModel
            // 
            this.textBoxVehicleModel.AllowsLetters = true;
            this.textBoxVehicleModel.AllowsNumbers = true;
            this.textBoxVehicleModel.AllowsPunctuation = true;
            this.textBoxVehicleModel.AllowsSeparators = true;
            this.textBoxVehicleModel.AllowsSymbols = true;
            this.textBoxVehicleModel.AllowsWhiteSpaces = true;
            this.textBoxVehicleModel.Enabled = false;
            this.textBoxVehicleModel.ExtraAllowedChars = "";
            this.textBoxVehicleModel.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxVehicleModel.Location = new System.Drawing.Point(4, 150);
            this.textBoxVehicleModel.MaxLength = 40;
            this.textBoxVehicleModel.Name = "textBoxVehicleModel";
            this.textBoxVehicleModel.NonAllowedCharacters = "";
            this.textBoxVehicleModel.RegularExpresion = "";
            this.textBoxVehicleModel.Size = new System.Drawing.Size(39, 20);
            this.textBoxVehicleModel.TabIndex = 47;
            // 
            // textBoxVehicleYear
            // 
            this.textBoxVehicleYear.AllowsLetters = true;
            this.textBoxVehicleYear.AllowsNumbers = true;
            this.textBoxVehicleYear.AllowsPunctuation = true;
            this.textBoxVehicleYear.AllowsSeparators = true;
            this.textBoxVehicleYear.AllowsSymbols = true;
            this.textBoxVehicleYear.AllowsWhiteSpaces = true;
            this.textBoxVehicleYear.Enabled = false;
            this.textBoxVehicleYear.ExtraAllowedChars = "";
            this.textBoxVehicleYear.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxVehicleYear.Location = new System.Drawing.Point(4, 150);
            this.textBoxVehicleYear.MaxLength = 40;
            this.textBoxVehicleYear.Name = "textBoxVehicleYear";
            this.textBoxVehicleYear.NonAllowedCharacters = "";
            this.textBoxVehicleYear.RegularExpresion = "";
            this.textBoxVehicleYear.Size = new System.Drawing.Size(39, 20);
            this.textBoxVehicleYear.TabIndex = 47;
            // 
            // textBoxVehicleColour
            // 
            this.textBoxVehicleColour.AllowsLetters = true;
            this.textBoxVehicleColour.AllowsNumbers = true;
            this.textBoxVehicleColour.AllowsPunctuation = true;
            this.textBoxVehicleColour.AllowsSeparators = true;
            this.textBoxVehicleColour.AllowsSymbols = true;
            this.textBoxVehicleColour.AllowsWhiteSpaces = true;
            this.textBoxVehicleColour.Enabled = false;
            this.textBoxVehicleColour.ExtraAllowedChars = "";
            this.textBoxVehicleColour.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxVehicleColour.Location = new System.Drawing.Point(4, 150);
            this.textBoxVehicleColour.MaxLength = 40;
            this.textBoxVehicleColour.Name = "textBoxVehicleColour";
            this.textBoxVehicleColour.NonAllowedCharacters = "";
            this.textBoxVehicleColour.RegularExpresion = "";
            this.textBoxVehicleColour.Size = new System.Drawing.Size(39, 20);
            this.textBoxVehicleColour.TabIndex = 47;
            // 
            // textBoxUserName
            // 
            this.textBoxUserName.AllowsLetters = true;
            this.textBoxUserName.AllowsNumbers = true;
            this.textBoxUserName.AllowsPunctuation = true;
            this.textBoxUserName.AllowsSeparators = true;
            this.textBoxUserName.AllowsSymbols = true;
            this.textBoxUserName.AllowsWhiteSpaces = true;
            this.textBoxUserName.Enabled = false;
            this.textBoxUserName.ExtraAllowedChars = "";
            this.textBoxUserName.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxUserName.Location = new System.Drawing.Point(4, 150);
            this.textBoxUserName.MaxLength = 40;
            this.textBoxUserName.Name = "textBoxUserName";
            this.textBoxUserName.NonAllowedCharacters = "";
            this.textBoxUserName.RegularExpresion = "";
            this.textBoxUserName.Size = new System.Drawing.Size(39, 20);
            this.textBoxUserName.TabIndex = 47;
            // 
            // textBoxUserId
            // 
            this.textBoxUserId.AllowsLetters = true;
            this.textBoxUserId.AllowsNumbers = true;
            this.textBoxUserId.AllowsPunctuation = true;
            this.textBoxUserId.AllowsSeparators = true;
            this.textBoxUserId.AllowsSymbols = true;
            this.textBoxUserId.AllowsWhiteSpaces = true;
            this.textBoxUserId.Enabled = false;
            this.textBoxUserId.ExtraAllowedChars = "";
            this.textBoxUserId.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxUserId.Location = new System.Drawing.Point(4, 150);
            this.textBoxUserId.MaxLength = 40;
            this.textBoxUserId.Name = "textBoxUserId";
            this.textBoxUserId.NonAllowedCharacters = "";
            this.textBoxUserId.RegularExpresion = "";
            this.textBoxUserId.Size = new System.Drawing.Size(39, 20);
            this.textBoxUserId.TabIndex = 47;
            // 
            // textBoxUserPhone
            // 
            this.textBoxUserPhone.AllowsLetters = true;
            this.textBoxUserPhone.AllowsNumbers = true;
            this.textBoxUserPhone.AllowsPunctuation = true;
            this.textBoxUserPhone.AllowsSeparators = true;
            this.textBoxUserPhone.AllowsSymbols = true;
            this.textBoxUserPhone.AllowsWhiteSpaces = true;
            this.textBoxUserPhone.Enabled = false;
            this.textBoxUserPhone.ExtraAllowedChars = "";
            this.textBoxUserPhone.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxUserPhone.Location = new System.Drawing.Point(4, 150);
            this.textBoxUserPhone.MaxLength = 40;
            this.textBoxUserPhone.Name = "textBoxuserPhone";
            this.textBoxUserPhone.NonAllowedCharacters = "";
            this.textBoxUserPhone.RegularExpresion = "";
            this.textBoxUserPhone.Size = new System.Drawing.Size(39, 20);
            this.textBoxUserPhone.TabIndex = 47;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemPlate});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(200, 623);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemVehiclePlate,
            this.layoutControlItemVehicleColour,
            this.layoutControlItemVehicleModel,
            this.layoutControlItemVehicleYear});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup2.Size = new System.Drawing.Size(620, 200);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemUserId,
            this.layoutControlItemUserName,
            this.layoutControlItemUserPhone});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup1";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup3.Size = new System.Drawing.Size(620, 200);
            this.layoutControlGroup3.Text = "layoutControlGroup1";
            this.layoutControlGroup3.TextVisible = false;

            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ApplicationButtonAnimationLength = 1;
            this.ribbonControl1.ExpandCollapseItem.AllowRightClickInMenu = false;
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.ExpandCollapseItem.Name = "";
            this.ribbonControl1.GroupAnimationLength = 1;
            this.ribbonControl1.ItemAnimationLength = 1;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.barButtonItemPrint,
            this.barButtonItemSave,
            this.barButtonItemRefresh});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 45;
            this.ribbonControl1.MdiMergeStyle = DevExpress.XtraBars.Ribbon.RibbonMdiMergeStyle.Always;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.PageAnimationLength = 1;
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbonControl1.ShowExpandCollapseButton = DevExpress.Utils.DefaultBoolean.True;
            this.ribbonControl1.ShowToolbarCustomizeItem = false;
            this.ribbonControl1.Size = new System.Drawing.Size(1174, 119);
            this.ribbonControl1.Toolbar.ShowCustomizeItem = false;
            this.ribbonControl1.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            //this.ribbonControl1.Controls.Add(vScrollBar1);
            // 
            // barButtonItemPrint
            // 
            this.barButtonItemPrint.Caption = "ItemPrint";
            this.barButtonItemPrint.Enabled = false;
            this.barButtonItemPrint.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.Glyph")));
            this.barButtonItemPrint.Id = 6;
            this.barButtonItemPrint.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemPrint.Name = "barButtonItemPrint";
            //this.barButtonItemPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPrint_ItemClick);
            // 
            // barButtonItemSave
            // 
            this.barButtonItemSave.Caption = "ItemSave";
            this.barButtonItemSave.Enabled = false;
            this.barButtonItemSave.Glyph = global::SmartCadGuiCommon.PrintRibbonControllerResources.RibbonPrintPreview_Save;
            this.barButtonItemSave.Id = 7;
            this.barButtonItemSave.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemSave.Name = "barButtonItemSave";
            //this.barButtonItemSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemSave_ItemClick);
            // 
            // barButtonItemRefresh
            // 
            this.barButtonItemRefresh.Caption = "ItemRefresh";
            this.barButtonItemRefresh.Enabled = false;
            this.barButtonItemRefresh.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemRefresh.Glyph")));
            this.barButtonItemRefresh.Id = 8;
            this.barButtonItemRefresh.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemRefresh.Name = "barButtonItemRefresh";
            //this.barButtonItemRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemRefresh_ItemClick);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupGeneralOptions});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "ribbonPage1";
            // 
            // ribbonPageGroupGeneralOptions
            // 
            this.ribbonPageGroupGeneralOptions.AllowMinimize = false;
            this.ribbonPageGroupGeneralOptions.AllowTextClipping = false;
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemPrint);
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemSave);
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemRefresh);
            this.ribbonPageGroupGeneralOptions.Name = "ribbonPageGroupGeneralOptions";
            this.ribbonPageGroupGeneralOptions.ShowCaptionButton = false;
            this.ribbonPageGroupGeneralOptions.Text = "ribbonPageGroupGeneralOptions";  
            // 
            // panelControl1
            // 
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(255, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(945, 671);
            this.panelControl1.TabIndex = 10;
            //this.panelControl1.Controls.Add(vScrollBar1);
            // 
            // DateEditEndDate
            // 
            this.DateEditEndDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DateEditEndDate.EditValue = null;
            this.DateEditEndDate.Location = new System.Drawing.Point(87, 58);
            this.DateEditEndDate.Name = "DateEditEndDate";
            this.DateEditEndDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.DateEditEndDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject6, "", null, null, false)});
            this.DateEditEndDate.Properties.DisplayFormat.FormatString = "MM/dd/yyyy HH:mm:ss.fff";
            this.DateEditEndDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.DateEditEndDate.Properties.EditFormat.FormatString = "MM/dd/yyyy HH:mm:ss.fff";
            this.DateEditEndDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.DateEditEndDate.Properties.Mask.EditMask = "MM/dd/yyyy HH:mm:ss.fff";
            this.DateEditEndDate.Properties.ShowDropDown = DevExpress.XtraEditors.Controls.ShowDropDown.Never;
            this.DateEditEndDate.Properties.ShowPopupShadow = false;
            this.DateEditEndDate.Properties.ShowToday = false;
            this.DateEditEndDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DateEditEndDate.Size = new System.Drawing.Size(146, 20);
            this.DateEditEndDate.TabIndex = 45;
            // 
            // DateEditStartDate
            // 
            this.DateEditStartDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DateEditStartDate.EditValue = null;
            this.DateEditStartDate.Location = new System.Drawing.Point(87, 32);
            this.DateEditStartDate.Name = "DateEditStartDate";
            this.DateEditStartDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.DateEditStartDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject7, "", null, null, false)});
            this.DateEditStartDate.Properties.DisplayFormat.FormatString = "MM/dd/yyyy HH:mm:ss.fff";
            this.DateEditStartDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.DateEditStartDate.Properties.EditFormat.FormatString = "MM/dd/yyyy HH:mm:ss.fff";
            this.DateEditStartDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.DateEditStartDate.Properties.Mask.EditMask = "MM/dd/yyyy HH:mm:ss.fff";
            this.DateEditStartDate.Properties.ShowDropDown = DevExpress.XtraEditors.Controls.ShowDropDown.Never;
            this.DateEditStartDate.Properties.ShowPopupShadow = false;
            this.DateEditStartDate.Properties.ShowToday = false;
            this.DateEditStartDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DateEditStartDate.Size = new System.Drawing.Size(146, 20);
            this.DateEditStartDate.TabIndex = 44;
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // CctvAlarmsHistoryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1174, 742);
            this.Controls.Add(this.containerVehicule);
            this.Controls.Add(this.ribbonControl1);
            this.Controls.Add(this.panelControl1);
            this.Name = "LprAlerts";
            this.Text = "LprAlerts";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LprAlerts_FormClosing);
            this.Load += new System.EventHandler(this.LprVehiculeForm_Load);
            this.ControlBox = false;
            this.tabVehicule.ResumeLayout(false);
            this.tabDataloggerVehicule.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlVehiculeIn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDataloggerVehicule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditEndDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditEndDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditStartDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditStartDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);    
            ((System.ComponentModel.ISupportInitialize)(this.groupPictureCarControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupInformationCar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.informationUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagePlate)).EndInit();
            this.containerInformacionCar.ResumeLayout(false);
            this.containerVehicule.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureCar.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picturePlate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit(); 
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPlate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemVehiclePlate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemVehicleModel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemVehicleYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemVehicleColour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemUserId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemUserPhone)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        public DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.BarButtonItem barButtonItemPrint;
        private DevExpress.XtraBars.BarButtonItem barButtonItemSave;
        private DevExpress.XtraBars.BarButtonItem barButtonItemRefresh;    
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupGeneralOptions;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        public DevExpress.XtraEditors.DateEdit DateEditEndDate;
        public DevExpress.XtraEditors.DateEdit DateEditStartDate; 
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.Utils.ToolTipController toolTipController1;
        public System.Windows.Forms.TabControl tabVehicule;
        private System.Windows.Forms.TabPage tabDataloggerVehicule;
        public GridControlEx gridControlVehiculeIn;
        public GridViewEx gridViewDataloggerVehicule;
        private DevExpress.XtraEditors.PictureEdit pictureCar;
        private DevExpress.XtraEditors.PictureEdit picturePlate;
        public DevExpress.XtraEditors.GroupControl groupPictureCarControl;
        public DevExpress.XtraEditors.GroupControl groupInformationCar;
        public DevExpress.XtraEditors.GroupControl informationUser;
        public DevExpress.XtraEditors.GroupControl imagePlate;
        private DevExpress.XtraBars.Docking.ControlContainer containerInformacionCar;
        private DevExpress.XtraBars.Docking.ControlContainer containerRegisterVehicule;
        private DevExpress.XtraBars.Docking.DockPanel containerVehicule;
        private TextBoxEx textBoxPlate;
        private TextBoxEx textBoxVehiclePlate;
        private TextBoxEx textBoxVehicleModel;
        private TextBoxEx textBoxVehicleYear;
        private TextBoxEx textBoxVehicleColour;
        private TextBoxEx textBoxUserName;
        private TextBoxEx textBoxUserId;
        private TextBoxEx textBoxUserPhone;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemPlate;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemVehiclePlate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemVehicleModel;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemVehicleYear;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemVehicleColour;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemUserName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemUserId;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemUserPhone;
        private DevExpress.XtraEditors.SimpleButton buttonVideo;
    }
}