using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using DevExpress.XtraTreeList;
using Smartmatic.SmartCad.Service;
using DevExpress.XtraLayout;
using System.Threading;
using System.Collections;
using System.IO;
using System.Text.RegularExpressions;
using System.Text;
using System.ComponentModel;
using DevExpress.XtraEditors.Repository;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadControls;
using Smartmatic.SmartCad.Vms;
using SmartCadControls.Controls;
using SmartCadGuiCommon.Classes;
using SmartCadGuiCommon.Controls;
using SmartCadControls.Interfaces;
using System.Drawing;

namespace SmartCadGuiCommon
{
    public partial class LprVehiculeForm : DevExpress.XtraEditors.XtraForm
    {
        #region Fields
        
        private ServerServiceClient serverServiceClient;
        public System.Threading.Timer joyTimer;
        private ConfigurationClientData config = null;
        private LprFrontClient lprFrontClient;

        #endregion

        #region Properties
        public ServerServiceClient ServerServiceClient
        {
            get
            {
                return serverServiceClient;
            }
            set
            {
                serverServiceClient = value;
            }
        }
        #endregion

        #region Constructor
        public LprVehiculeForm()
        {
            InitializeComponent();

            config = ServerServiceClient.GetInstance().GetConfiguration();

            lprFrontClient = new LprFrontClient();
        }
        #endregion

        private void LprVehiculeForm_Load(object sender, EventArgs e)
        {
            LoadLanguage();
            LoadInitialData();
        }

        /// <summary>
        /// Initialize Captions and Text messages for each controls in the form
        /// gets from the resources
        /// </summary>
        private void LoadLanguage()
        {
            Text = ResourceLoader.GetString2("Alerts");

            barButtonItemPrint.Caption = ResourceLoader.GetString2("Print");
            barButtonItemRefresh.Caption = ResourceLoader.GetString2("Refresh");
            barButtonItemSave.Caption = ResourceLoader.GetString2("Save");
            ribbonPageGroupGeneralOptions.Text = ResourceLoader.GetString2("GeneralOptions");
            ribbonPage1.Text = ResourceLoader.GetString2("LPR");
            //layoutControlItemPlate.Text = ResourceLoader.GetString2("Plate");
            //layoutControlItemUserId.Text = ResourceLoader.GetString2("ID");
            //layoutControlItemUserName.Text = ResourceLoader.GetString2("Name");
            //layoutControlItemUserPhone.Text = ResourceLoader.GetString2("Phone");
            //layoutControlItemVehicleColour.Text = ResourceLoader.GetString2("Colour");
            //layoutControlItemVehicleModel.Text = ResourceLoader.GetString2("Model");
            //layoutControlItemVehiclePlate.Text = ResourceLoader.GetString2("Plate");
            //layoutControlItemVehicleYear.Text = ResourceLoader.GetString2("Year");
            //groupPictureCarControl.Text = ResourceLoader.GetString2("Vehicule Picture");
            //groupInformationCar.Text = ResourceLoader.GetString2("Vehicule Data");
            //informationUser.Text = ResourceLoader.GetString2("Applicant Data");
            //imagePlate.Text = ResourceLoader.GetString2("Image Plate");
            //buttonVideo.Text = ResourceLoader.GetString2("View Video");        
        }
        
        public void LoadInitialData()
        {
            //Imagenes en formato bitmap, metafile, icon, JPEG, GIF, PNG or SVG.
            //pictureCar.Image = Image.FromFile(@"C:\Users\Posma-dev\Documents\imagenes de ricardo\fontawesome.png");
        }

        void LprAlerts_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
            }
            else
            {
                
            }
        }


    }







}
