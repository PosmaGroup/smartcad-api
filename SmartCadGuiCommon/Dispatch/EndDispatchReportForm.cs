using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using SmartCadCore.ClientData;
using DevExpress.XtraEditors.Repository;
using SmartCadCore.Core;
using DevExpress.XtraLayout;
using Smartmatic.SmartCad.Service;
using System.Collections;
using SmartCadCore.Common;
using DevExpress.XtraEditors.DXErrorProvider;
using System.Reflection;
using SmartCadCore.Enums;
using SmartCadControls.Controls;
using SmartCadControls;
using SmartCadGuiCommon.Controls;

namespace SmartCadGuiCommon
{
    public partial class EndDispatchReportForm : DevExpress.XtraEditors.XtraForm
    {
        private FormBehavior behavior;
        private IList reports = new ArrayList();
        private int BaseHeight;
        //private IList incidentTypes;
        private IList departmentTypes = new ArrayList();
        private DispatchOrderClientData dispatchOrder;
        private IList dispatchOrderList = new ArrayList();
        private EndingReportClientData endingReport;
        private Dictionary<string, string> controlAnswers = new Dictionary<string, string>();
        private ArrayList requiredControls = new ArrayList();
        private bool showWarningOnValidation = false;
        private GridViewExOfficerLeaderMode gridViewExOfficerLeaderMode;


        enum GridViewExOfficerLeaderMode
        {
            LoadOfficer,
            NewOfficer,
            NoOfficerSelected,
            OfficerNotInZoneOrStation,
            OfficerNotInDepartment,
            ValidOfficer
        };

        public EndDispatchReportForm()
        {
            InitializeComponent();
            InitializeDataGrid();
            LoadLanguage();
        }

        public EndDispatchReportForm(FormBehavior behavior, DispatchOrderClientData dispatchOrder)
            : this()
        {
            this.Behavior = behavior;
            // this.incidentTypes = GetIncidentTypes(dispatchOrder.IncidentCode);
            this.departmentTypes.Add(dispatchOrder.DepartmentType);
            this.reports = GetReportsByIncidentAndDepartmentType(dispatchOrder.IncidentCode, dispatchOrder.DepartmentType.Code);
            this.dispatchOrder = dispatchOrder;
            this.dispatchOrderList.Add(dispatchOrder);
        }

        public EndDispatchReportForm(FormBehavior behavior, IList dispatchOrders)
            : this()
        {
            this.Behavior = behavior;
            this.dispatchOrderList = dispatchOrders;
            this.dispatchOrder = dispatchOrderList[0] as DispatchOrderClientData;
            // this.incidentTypes = GetIncidentTypes(dispatchOrder.IncidentCode);
            this.departmentTypes.Add(dispatchOrder.DepartmentType);
            this.reports = GetReportsByIncidentAndDepartmentType(dispatchOrder.IncidentCode, dispatchOrder.DepartmentType.Code);
        }

        public EndDispatchReportForm(FormBehavior behavior, DispatchReportClientData report)
            : this()
        {
            this.Behavior = behavior;
            this.reports.Add(report);
            LoadRequiredQuestions();
        }

        public EndingReportClientData EndingReport
        {
            get
            {
                return endingReport;
            }
            set
            {
                endingReport = value;
            }
        }

        public Dictionary<string, string> ControlAnswers
        {
            get
            {
                return controlAnswers;
            }
            set
            {
                controlAnswers = value;
            }
        }

        public IList RequiredControls
        {
            get
            {
                return requiredControls;
            }

        }

        private IList GetIncidentTypes(int incidentCode)
        {
            IList result = new ArrayList();
            //result = (IList)ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetIncidentByCode, incidentCode));
            result = (IList)ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(@"SELECT incidentTypes 
                                                                                                            FROM ReportBaseData rb left join rb.SetIncidentTypes incidentTypes 
                                                                                                            WHERE rb.Incident.Code = {0} ORDER BY rb.Code ASC ", incidentCode));


            return result;

        }

        private IList GetReportsByIncidentAndDepartmentType(int incidentCode, int depTypeCode)
        {
            Dictionary<string, IList> querys = new Dictionary<string, IList>();
            querys["Reports"] = new ArrayList();

            querys["Reports"].Add(SmartCadHqls.GetCustomHql(SmartCadHqls.GetDispatchReportsWithQuestionsByIncTypeAndDepType, depTypeCode, incidentCode));

            querys["Reports"].Add(SmartCadHqls.GetCustomHql(SmartCadHqls.GetDispatchReportsWithIncidentTypesByIncTypeAndDepType, depTypeCode, incidentCode));

            querys["Reports"].Add(SmartCadHqls.GetCustomHql(SmartCadHqls.GetDispatchReportsByIncTypeAndDepType, depTypeCode, incidentCode));

            querys = ServerServiceClient.GetInstance().SearchClientObjectsMaxRowsMultiQuery(true, querys);

            return querys["Reports"];

        }

        private void LoadRequiredQuestions()
        {
            IList questions = new ArrayList();
            foreach (DispatchReportClientData report in reports)
            {
                if (report.Code != 0)
                {
                    questions = (IList)ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetRequiredDispatchReportQuestionDispatchReport, report.Code));

                    foreach (DispatchReportQuestionDispatchReportClientData reportQuestion in questions)
                        report.Questions.Add(reportQuestion);

                }
                else
                {
                    questions = GetRequiredQuestions();

                    foreach (DispatchReportQuestionDispatchReportClientData reportQuestion in questions)
                    {
                        report.Questions.Add(reportQuestion);
                    }
                }


            }
        }

        private IList GetRequiredQuestions()
        {
            IList result = new ArrayList();
            IList questions = (IList)ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetRequiredQuestionDispatchReport);
            DispatchReportQuestionDispatchReportClientData reportQuestion;
            foreach (QuestionDispatchReportClientData question in questions)
            {
                reportQuestion = new DispatchReportQuestionDispatchReportClientData();
                reportQuestion.QuestionCode = question.Code;
                reportQuestion.QuestionRender = question.Render;
                reportQuestion.QuestionRequired = question.Required;
                reportQuestion.QuestionText = question.Text;
                result.Add(reportQuestion);
            }

            return result;

        }

        private void SetHeaderInformation()
        {

            ClearLabels();
            labelControlDate.Text = ServerServiceClient.GetInstance().GetTime().ToString("MM/dd/yyyy HH:mm");


            if (behavior == FormBehavior.View)
            {
                foreach (DispatchReportClientData report in reports)
                {
                    foreach (IncidentTypeClientData incType in report.IncidentTypes)
                        labelControlIncidentType.Text += ", " + incType.ToString();

                    foreach (DepartmentTypeClientData depType in report.DepartmentTypes)
                        labelControlDepartmentType.Text += ", " + depType.Name;

                }

                if (labelControlDepartmentType.Text != string.Empty)
                    labelControlDepartmentType.Text = labelControlDepartmentType.Text.Substring(1);
                if (labelControlIncidentType.Text != string.Empty)
                    labelControlIncidentType.Text = labelControlIncidentType.Text.Substring(1);
            }
            else
            {
                foreach (DepartmentTypeClientData depType in this.departmentTypes)
                    labelControlDepartmentType.Text += ", " + depType.Name;

                if (this.reports.Count > 0)
                {
                    foreach (DispatchReportClientData report in this.reports)
                    {
                        foreach (IncidentTypeClientData incType in report.IncidentTypes)
                            labelControlIncidentType.Text += ", " + incType.ToString();

                    }
                }
                else
                {

                    labelControlIncidentType.Text = "";
                    labelControlIncidentType.AutoEllipsis = true;
                    foreach (IncidentTypeClientData incType in GetIncidentTypes(dispatchOrder.IncidentCode))
                    {
                        if (labelControlIncidentType.Text == "")
                        {
                            labelControlIncidentType.Text = " " + incType.ToString();
                        }
                        else {
                            labelControlIncidentType.Text = " " + labelControlIncidentType.Text + "," + incType.ToString();
                        }
                    }

                }


                if (labelControlDepartmentType.Text != string.Empty)
                    labelControlDepartmentType.Text = labelControlDepartmentType.Text.Substring(1);
                if (labelControlIncidentType.Text != string.Empty)
                    labelControlIncidentType.Text = labelControlIncidentType.Text.Substring(1);


                labelControlOperator.Text = dispatchOrder.DispatchOperatorName;
                labelControlIncident.Text = dispatchOrder.IncidentCustomCode;
                labelControlZone.Text = dispatchOrder.Unit.DepartmentStation.DepartmentZone.Name;
                labelControlStation.Text = dispatchOrder.Unit.DepartmentStation.Name;
                LoadOfficers(dispatchOrder.Code);
            }
        }

        private int LabelHeight(LabelControl label)
        {
            int width = TextRenderer.MeasureText(label.Text, label.Font).Width;

            int numberLines = (width / label.Width) + 1;

            return (numberLines * 13) + 10;
        }

        private void LoadOfficers(int dispatchOrderCode)
        {
            BindingList<GridControlDataUnitOfficerReport> dataSource = new BindingList<GridControlDataUnitOfficerReport>();

            string hql = @"SELECT officerAssigns FROM DispatchOrderData dispatchOrder 
                          left join dispatchOrder.Unit.SetOfficerAssigns officerAssigns
                          WHERE officerAssigns.DeletedId is null AND  dispatchOrder.Code IN (";

            foreach (DispatchOrderClientData dOrder in dispatchOrderList)
                hql += string.Format("{0},", dOrder.Code);

            hql = hql.Substring(0, hql.Length - 1);

            hql += string.Format(") AND ('{0}' BETWEEN officerAssigns.Start AND officerAssigns.End)", ApplicationUtil.GetDataBaseFormattedDate(ServerServiceClient.GetInstance().GetTimeFromDB()));

            IList officerAssigns = ServerServiceClient.GetInstance().SearchClientObjects(hql);

            foreach (UnitOfficerAssignHistoryClientData officerA in officerAssigns)
                dataSource.Add(new GridControlDataUnitOfficerReport(officerA));


            gridControlExOfficers.BeginInit();
            gridControlExOfficers.DataSource = dataSource;
            gridControlExOfficers.EndInit();

        }

        private void ClearLabels()
        {
            labelControlDate.Text = string.Empty;
            labelControlDepartmentType.Text = string.Empty;
            labelControlIncidentType.Text = string.Empty;
            labelControlOperator.Text = string.Empty;
            labelControlIncident.Text = string.Empty;
            labelControlStation.Text = string.Empty;
            labelControlZone.Text = string.Empty;
        }

        public FormBehavior Behavior
        {
            get
            {
                return behavior;
            }
            set
            {
                behavior = value;
            }
        }

        private Dictionary<int, IList> questionByReport = new Dictionary<int, IList>();
        private IList commonQuestions = new ArrayList();

        private void SearchCommonQuestion()
        {
            bool isCommon = false;

            for (int i = 0; i < reports.Count; i++)
            {
                DispatchReportClientData reportA = reports[i] as DispatchReportClientData;

                foreach (DispatchReportQuestionDispatchReportClientData reportQuestionA in reportA.Questions)
                {
                    isCommon = false;
                    for (int j = i + 1; j < reports.Count; j++)
                    {
                        DispatchReportClientData reportB = reports[j] as DispatchReportClientData;

                        foreach (DispatchReportQuestionDispatchReportClientData reportQuestionB in reportB.Questions)
                        {
                            if (reportQuestionA.QuestionCode == reportQuestionB.QuestionCode && ContainsQuestion(commonQuestions, reportQuestionA) == false)
                            {
                                commonQuestions.Add(reportQuestionA);
                                isCommon = true;
                                break;
                            }
                        }
                        if (isCommon == true)
                            break;
                    }

                    if (isCommon == false && ContainsQuestion(commonQuestions, reportQuestionA) == false)
                        AddQuestionToList(reportQuestionA);
                }
            }

            ((ArrayList)commonQuestions).Sort((IComparer)new ReportQuestionComparer());
        }

        private bool ContainsQuestion(IList list, DispatchReportQuestionDispatchReportClientData question)
        {
            foreach (DispatchReportQuestionDispatchReportClientData reportQuestion in list)
            {
                if (reportQuestion.QuestionCode == question.QuestionCode)
                    return true;
            }

            return false;
        }

        private void AddQuestionToList(DispatchReportQuestionDispatchReportClientData question)
        {
            IList notCommonQuestions;

            if (questionByReport.ContainsKey(question.ReportCode) == false)
            {
                notCommonQuestions = new ArrayList();
                notCommonQuestions.Add(question);
                questionByReport.Add(question.ReportCode, notCommonQuestions);
            }
            else
            {
                if (!QuestionsByReportContains(questionByReport[question.ReportCode], question.QuestionCode))
                {
                    notCommonQuestions = questionByReport[question.ReportCode];
                    notCommonQuestions.Add(question);
                    ((ArrayList)notCommonQuestions).Sort((IComparer)new ReportQuestionComparer());
                    questionByReport[question.ReportCode] = notCommonQuestions;
                }
            }

        }

        private bool QuestionsByReportContains(IList questions, int questionCode)
        {
            foreach (DispatchReportQuestionDispatchReportClientData question in questions)
            {
                if (questionCode == question.QuestionCode)
                {
                    return true;
                }
            }
            return false;
        }

        private void LoadQuestions()
        {
            QuestionsEndReportControl questionsControl;
            LayoutControlItem item;
            int height = 0;

            //Calculo la altura de las etiquetas de los tipos incidentes y organismos
            int labelIncHeight = LabelHeight(labelControlIncidentType);
            int labelDeptHeight = LabelHeight(labelControlDepartmentType);

            layoutControlItemIncidentType.SizeConstraintsType = SizeConstraintsType.Custom;
            layoutControlItemDepartmentType.SizeConstraintsType = SizeConstraintsType.Custom;

            layoutControlItemIncidentType.MaxSize = new Size(layoutControlItemIncidentType.Width, labelIncHeight);
            layoutControlItemDepartmentType.MaxSize = new Size(layoutControlItemDepartmentType.Width, labelDeptHeight);

            if (behavior == FormBehavior.View)
                BaseHeight = 390 + labelIncHeight + labelDeptHeight;
            else
                BaseHeight = 425 + labelIncHeight + labelDeptHeight;

            //Busco las preguntas comunes entre los distintos reportes.
            SearchCommonQuestion();

            layoutControlEndReport.BeginUpdate();
            layoutControlItemOfficerLeader.SizeConstraintsType = SizeConstraintsType.Custom;

            if (reports != null && reports.Count > 0)
            {
                foreach (DispatchReportClientData report in reports)
                {
                    height = 0;
                    if (questionByReport.ContainsKey(report.Code) == true || report.Code == 0)
                    {
                        questionsControl = new QuestionsEndReportControl(questionByReport[report.Code], report);
                        questionsControl.ControlValueChanged += new QuestionsEndReportControl.ControlValue_Changed(questionsControl_ControlValueChanged);
                        questionsControl.Dock = DockStyle.Fill;
                        item = (LayoutControlItem)layoutControlEndReport.CreateLayoutItem(layoutControlGroupEndReport);
                        item.TextVisible = false;
                        item.Padding = new DevExpress.XtraLayout.Utils.Padding(2);
                        layoutControlGroupEndReport.AddItem(item, layoutControlItemOfficerLeader, DevExpress.XtraLayout.Utils.InsertType.Bottom);
                        item.Control = questionsControl;
                        height += (questionByReport[report.Code].Count);
                        item.SizeConstraintsType = SizeConstraintsType.Custom;
                        item.MaxSize = new Size(0, questionsControl.ControlMaxHeight);
                        item.MinSize = new Size(0, questionsControl.ControlMaxHeight);
                        BaseHeight += questionsControl.ControlMaxHeight;
                        if (behavior != FormBehavior.Edit)
                            questionsControl.Enabled = false;
                    }
                }

                //Creo el control de preguntas para las comunes.
                if (commonQuestions.Count > 0)
                {
                    height = 0;
                    questionsControl = new QuestionsEndReportControl(commonQuestions, false);
                    questionsControl.ControlValueChanged += new QuestionsEndReportControl.ControlValue_Changed(questionsControl_ControlValueChanged);
                    item = (LayoutControlItem)layoutControlEndReport.CreateLayoutItem(layoutControlGroupEndReport);
                    item.TextVisible = false;
                    item.Padding = new DevExpress.XtraLayout.Utils.Padding(2);
                    layoutControlGroupEndReport.AddItem(item, layoutControlItemOfficerLeader, DevExpress.XtraLayout.Utils.InsertType.Bottom);
                    item.Control = questionsControl;
                    height += (commonQuestions.Count);
                    item.SizeConstraintsType = SizeConstraintsType.Custom;
                    item.MaxSize = new Size(0, questionsControl.ControlMaxHeight);
                    item.MinSize = new Size(0, questionsControl.ControlMaxHeight);
                    BaseHeight += questionsControl.ControlMaxHeight;

                    if (behavior != FormBehavior.Edit)
                        questionsControl.Enabled = false;

                }
            }
            else
            {
                height = 0;
                IList requiredQuestions = GetRequiredQuestions();

                if (requiredQuestions != null)
                {
                    questionsControl = new QuestionsEndReportControl(requiredQuestions, true);
                    questionsControl.ControlValueChanged += new QuestionsEndReportControl.ControlValue_Changed(questionsControl_ControlValueChanged);
                    item = (LayoutControlItem)layoutControlEndReport.CreateLayoutItem(layoutControlGroupEndReport);
                    item.TextVisible = false;
                    item.Padding = new DevExpress.XtraLayout.Utils.Padding(2);
                    layoutControlGroupEndReport.AddItem(item, layoutControlItemOfficerLeader, DevExpress.XtraLayout.Utils.InsertType.Bottom);
                    item.Control = questionsControl;
                    height += (requiredQuestions.Count);
                    item.SizeConstraintsType = SizeConstraintsType.Custom;
                    item.MaxSize = new Size(0, questionsControl.ControlMaxHeight);
                    item.MinSize = new Size(0, questionsControl.ControlMaxHeight);
                    BaseHeight += questionsControl.ControlMaxHeight;
                }
            }

            layoutControlEndReport.EndUpdate();

            if (BaseHeight > Screen.PrimaryScreen.WorkingArea.Height - 50)
                this.Size = new Size(this.Width + 20, Screen.PrimaryScreen.WorkingArea.Height - 50);
            else
                this.Size = new Size(this.Width, BaseHeight);

            this.Top = (Screen.PrimaryScreen.WorkingArea.Height - this.Size.Height) / 2;
        }

        private void questionsControl_ControlValueChanged(object sender, EventArgs e)
        {
            Control control = sender as Control;

            if (controlAnswers.ContainsKey(control.Name) == true)
            {
                if (control is TextBoxEx)
                    controlAnswers[control.Name] = control.Text.Trim();
                else if (control is SpinEdit)
                    controlAnswers[control.Name] = ((SpinEdit)control).Value.ToString();
                else if (control is RadioButton)
                    controlAnswers[control.Name] = ((RadioButton)control).Checked.ToString();
                else if (control is CheckEdit)
                    controlAnswers[control.Name] = ((CheckEdit)control).Checked.ToString();

            }

            CheckButtons();
        }

        private void InitializeDataGrid()
        {
            gridControlExOfficers.Type = typeof(GridControlDataUnitOfficerReport);
            gridControlExOfficers.ViewTotalRows = false;
            
            gridViewExOfficers.OptionsBehavior.Editable = true;
            gridViewExOfficers.OptionsView.ShowFooter = false;
            gridViewExOfficers.Columns["IsLeader"].OptionsColumn.AllowEdit = true;
            gridViewExOfficers.Columns["IsLeader"].OptionsColumn.AllowMove = false;

            gridViewExOfficers.Columns["Name"].OptionsColumn.AllowEdit = false;
            gridViewExOfficers.Columns["LastName"].OptionsColumn.AllowEdit = false;
            gridViewExOfficers.Columns["Badge"].OptionsColumn.AllowEdit = false;
            gridViewExOfficers.Columns["Unit"].OptionsColumn.AllowEdit = false;


            gridControlExOfficerLeader.Type = typeof(GridControlDataOfficerReport);
            gridViewExOfficerLeader.OptionsBehavior.Editable = true;
            gridViewExOfficerLeader.Columns["Name"].OptionsColumn.AllowEdit = false;
            gridViewExOfficerLeader.Columns["LastName"].OptionsColumn.AllowEdit = false;
            gridViewExOfficerLeader.Columns["Badge"].OptionsColumn.AllowEdit = true;

            OfficerClientData officerLeader = new OfficerClientData();
            gridControlExOfficerLeader.AddOrUpdateItem(new GridControlDataOfficerReport(officerLeader));

            gridViewExOfficerLeaderMode = GridViewExOfficerLeaderMode.LoadOfficer;
        }

        private void LoadLanguage()
        {
            Icon = ResourceLoader.GetIcon("$Icon.DispatchReport");
            this.Text = ResourceLoader.GetString2("EndReport");
            layoutControlItemDate.Text = ResourceLoader.GetString2("Date") + ":";
            layoutControlItemDepartmentType.Text = ResourceLoader.GetString2("DepartmentType") + ":";
            layoutControlItemIncident.Text = ResourceLoader.GetString2("Incident") + ":";
            layoutControlItemIncidentType.Text = ResourceLoader.GetString2("IncidentTypes") + ":";
            layoutControlItemOfficers.Text = ResourceLoader.GetString2("OfficersAssigned") + ":";
            layoutControlItemOperator.Text = ResourceLoader.GetString2("Operator") + ":";
            layoutControlGroupHeader.Text = ResourceLoader.GetString2("EndReportInfo");
            layoutControlItemOfficerLeader.Text = ResourceLoader.GetString2("OfficerLeader") + ": *";
            layoutControlItemZone.Text = ResourceLoader.GetString2("Zone") + ":";
            layoutControlItemStation.Text = ResourceLoader.GetString2("Station") + ":";
        }

        private void EndDispatchReportForm_Load(object sender, System.EventArgs e)
        {
            if (behavior != FormBehavior.View)
            {
                simpleButtonAccept.Text = ResourceLoader.GetString2("Accept");
                simpleButtonCancel.Text = ResourceLoader.GetString2("Cancel");
            }
            else
            {
                gridControlExOfficers.Enabled = false;
                gridControlExOfficerLeader.Enabled = false;
                layoutControlItemAccept.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                layoutControlItemCancel.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            }

            SetHeaderInformation();
            LoadQuestions();


            if (controlAnswers != null)
            {
                if (controlAnswers.Count > 0)
                    SetAnswers(controlAnswers);
                else
                    LoadControlsInformation();

            }

            LoadRequiredControls();

            if (dispatchOrder != null && dispatchOrder.EndingReport != null && dispatchOrder.EndingReport.OfficerCode > 0)
            {
                SetOfficerLeader(dispatchOrder.EndingReport.OfficerCode);
            }
        }

        private void LoadRequiredControls()
        {
            foreach (Control questionControl in layoutControlEndReport.Controls)
            {
                if (questionControl is QuestionsEndReportControl)
                {
                    requiredControls.AddRange(((QuestionsEndReportControl)questionControl).RequiredControls);
                }
            }
        }

        private void LoadControlsInformation()
        {
            foreach (Control questionControl in layoutControlEndReport.Controls)
            {
                if (questionControl is QuestionsEndReportControl)
                {
                    Dictionary<string, string> controls = ((QuestionsEndReportControl)questionControl).ReportControls;

                    foreach (string key in controls.Keys)
                    {
                        controlAnswers.Add(key, controls[key]);
                    }

                }
            }

        }

        private void SetOfficerLeader(int officerCode)
        {
            GridControlDataUnitOfficerReport gridDataLeader = null;
            OfficerClientData officer = null;

            gridControlExOfficers.BeginUpdate();
            foreach (GridControlDataUnitOfficerReport gridData in gridControlExOfficers.Items)
            {
                if (gridData.Code == officerCode)
                {
                    gridData.IsLeader = true;
                    gridDataLeader = gridData;
                    break;
                }
            }
            gridControlExOfficers.EndUpdate();

            if (gridDataLeader != null)
            {
                gridViewExOfficerLeader.Columns["Badge"].OptionsColumn.AllowEdit = false;
                gridControlExOfficerLeader.DataSource = null;
                officer = new OfficerClientData();
                officer.Badge = gridDataLeader.UnitOfficerAssign.Officer.Badge;
                officer.FirstName = gridDataLeader.UnitOfficerAssign.Officer.FirstName;
                officer.LastName = gridDataLeader.UnitOfficerAssign.Officer.LastName;
                officer.Birthday = gridDataLeader.UnitOfficerAssign.Officer.Birthday;
                officer.PersonID = gridDataLeader.UnitOfficerAssign.Officer.PersonID;
                officer.Code = gridDataLeader.UnitOfficerAssign.Officer.Code;
                gridControlExOfficerLeader.AddOrUpdateItem(new GridControlDataOfficerReport(officer));
            }
            else
            {
                officer = (OfficerClientData)ServerServiceClient.GetInstance().SearchClientObject(SmartCadHqls.GetCustomHql(SmartCadHqls.GetOfficerByCode, officerCode));

                if (officer != null)
                {

                    gridViewExOfficerLeader.Columns["Badge"].OptionsColumn.AllowEdit = true;
                    gridControlExOfficerLeader.DataSource = null;
                    gridControlExOfficerLeader.AddOrUpdateItem(new GridControlDataOfficerReport(officer));
                }
            }



            CheckButtons();

        }

        private void gridViewExOfficers_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            gridViewExOfficers.CellValueChanging -= new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(gridViewExOfficers_CellValueChanging);

            if (((bool)e.Value) == true)
            {
                gridViewExOfficers.BeginUpdate();
                GridControlDataUnitOfficerReport gridData;
                for (int i = 0; i < gridControlExOfficers.Items.Count; i++)
                {
                    gridData = gridControlExOfficers.Items[i] as GridControlDataUnitOfficerReport;
                    gridData.IsLeader = false;

                }

                gridData = gridViewExOfficers.GetRow(e.RowHandle) as GridControlDataUnitOfficerReport;
                gridData.IsLeader = true;
                gridViewExOfficers.EndUpdate();

                gridViewExOfficerLeader.Columns["Badge"].OptionsColumn.AllowEdit = false;
                gridControlExOfficerLeader.DataSource = null;
                OfficerClientData officer = new OfficerClientData();
                officer.Badge = gridData.UnitOfficerAssign.Officer.Badge;
                officer.FirstName = gridData.UnitOfficerAssign.Officer.FirstName;
                officer.LastName = gridData.UnitOfficerAssign.Officer.LastName;
                officer.Code = gridData.UnitOfficerAssign.Officer.Code;
                officer.Birthday = gridData.UnitOfficerAssign.Officer.Birthday;
                officer.PersonID = gridData.UnitOfficerAssign.Officer.PersonID;
                gridControlExOfficerLeader.AddOrUpdateItem(new GridControlDataOfficerReport(officer));
                gridViewExOfficerLeaderMode = GridViewExOfficerLeaderMode.ValidOfficer;
                gridViewExOfficerLeader.Appearance.SelectedRow.BackColor = Color.FromArgb(0x74, 0xB3, 0xE1);
                gridViewExOfficerLeader.Appearance.FocusedRow.BackColor = Color.FromArgb(0x74, 0xB3, 0xE1);
            }
            else
            {

                ClearGridViewExOfficerLeader();
                gridViewExOfficerLeader.Columns["Badge"].OptionsColumn.AllowEdit = true;

            }
            gridViewExOfficers.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(gridViewExOfficers_CellValueChanging);

            CheckButtons();
        }

        private void CheckButtons()
        {
            GridControlDataOfficerReport gridData = null;
            if (gridControlExOfficerLeader.Items.Count > 0)
                gridData = gridControlExOfficerLeader.Items[0] as GridControlDataOfficerReport;


            if (gridData != null && gridData.Code != 0 && RequiredQuestionsHasValue())
                simpleButtonAccept.Enabled = true;
            else
                simpleButtonAccept.Enabled = false;
            FormUtil.InvokeRequired(this, delegate
            {
                if (gridViewExOfficerLeaderMode == GridViewExOfficerLeaderMode.NewOfficer && RequiredQuestionsHasValue())
                {
                    if (gridData != null &&
                        !string.IsNullOrEmpty(gridData.Badge) &&
                        !string.IsNullOrEmpty(gridData.Name) &&
                        !string.IsNullOrEmpty(gridData.LastName) &&
                        !string.IsNullOrEmpty(gridData.PersonalId) &&
                        gridData.Name != ResourceLoader.GetString2("TypeHere") &&
                        gridData.LastName != ResourceLoader.GetString2("TypeHere") &&
                        gridData.PersonalId != ResourceLoader.GetString2("TypeHere"))
                        simpleButtonAccept.Enabled = true;
                }
            });

        }

        private bool RequiredQuestionsHasValue()
        {
            bool result = false;
            foreach (string rControlName in requiredControls)
            {
                if (controlAnswers.ContainsKey(rControlName) == true)
                {
                    if (controlAnswers[rControlName] != null && controlAnswers[rControlName] != string.Empty)
                        result = true;
                    else
                        return false;
                }

            }

            return result;

        }

        private void SaveNewOfficerLeader()
        {
            FormUtil.InvokeRequired(this, delegate
            {
                OfficerClientData officer = ((GridControlDataOfficerReport)gridControlExOfficerLeader.SelectedItems[0]).Officer;
                officer.DepartmentStation = this.dispatchOrder.Unit.DepartmentStation;
                officer.DepartmentType = this.dispatchOrder.Unit.DepartmentType;
                ((GridControlDataOfficerReport)gridControlExOfficerLeader.SelectedItems[0]).Code = ((OfficerClientData)ServerServiceClient.GetInstance().SaveOrUpdateClientDataWithReturn(officer)).Code;
            });

        }

        private void simpleButtonAccept_Click(object sender, EventArgs e)
        {
            if (gridViewExOfficerLeaderMode == GridViewExOfficerLeaderMode.NewOfficer)
            {
                try
                {
                    BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[1] { GetType().GetMethod("SaveNewOfficerLeader", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[0] });
                    processForm.CanThrowError = true;
                    processForm.ShowDialog();
                }
                catch (Exception ex)
                {
                    SmartLogger.Print(ex);
                    return;
                }
            }

            int officerCode = 0;
            this.endingReport = new EndingReportClientData();

            if (gridControlExOfficerLeader.Items.Count > 0)
                officerCode = ((GridControlDataOfficerReport)gridControlExOfficerLeader.Items[0]).Code;

            this.endingReport.OfficerCode = officerCode;
            this.endingReport.ClosingDispatchOperatorCode = ServerServiceClient.GetInstance().OperatorClient.Code;
            this.endingReport.Answers = GetReportAnswers();

        }

        private IList GetReportAnswers()
        {
            ArrayList result = new ArrayList();

            foreach (Control questionControl in layoutControlEndReport.Controls)
            {
                if (questionControl is QuestionsEndReportControl)
                {
                    result.AddRange(((QuestionsEndReportControl)questionControl).GetReportAnswers());
                }
            }

            return result;
        }

        // public int GetLeaderOfficer() 
        // {
        //foreach (GridControlDataUnitOfficerReport gridData in gridControlExOfficers.Items)
        //{
        //    if (gridData.IsLeader == true)
        //        return gridData.Code;
        //}

        //return -1;

        //}

        private void SetAnswers(Dictionary<string, string> controlAnswers)
        {
            ArrayList keysList = new ArrayList(controlAnswers.Keys);

            foreach (string key in keysList)
            {
                foreach (Control questionControl in layoutControlEndReport.Controls)
                {
                    if (questionControl is QuestionsEndReportControl)
                    {
                        bool found = ((QuestionsEndReportControl)questionControl).SetAnswerToControl(key, controlAnswers[key]);
                        if (found == true)
                            break;
                    }
                }

            }
        }

        void gridViewExOfficerLeader_InvalidValueException(object sender, DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs e)
        {

        }

        void ClearGridViewExOfficerLeader()
        {
            GridControlDataOfficerReport gridData = gridControlExOfficerLeader.SelectedItems[0] as GridControlDataOfficerReport;
            gridData.LastName = string.Empty;
            gridData.Name = string.Empty;
            gridData.Code = 0;
            gridData.Birthday = null;
            gridData.Badge = string.Empty;
            gridData.PersonalId = string.Empty;
            gridData.Officer = new OfficerClientData();
        }

        void SetGridViewExOfficerLeaderAllowEdit(bool value)
        {
            gridViewExOfficerLeader.Columns["Name"].OptionsColumn.AllowEdit = value;
            gridViewExOfficerLeader.Columns["LastName"].OptionsColumn.AllowEdit = value;
            gridViewExOfficerLeader.Columns["PersonalId"].OptionsColumn.AllowEdit = value;
            gridViewExOfficerLeader.Columns["Birthday"].OptionsColumn.AllowEdit = value;
        }

        void gridViewExOfficerLeader_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            if (e.Value != null && e.Value.ToString() != ResourceLoader.GetString2("TypeHere"))
            {
                if (gridViewExOfficerLeader.FocusedColumn.Name == "colBadge")
                {
                    gridViewExOfficerLeader.ClearColumnErrors();
                    if (!string.IsNullOrEmpty((string)e.Value))
                    {
                        OfficerClientData officer = (OfficerClientData)ServerServiceClient.GetInstance().SearchClientObject(SmartCadHqls.GetCustomHql(@"SELECT officer FROM OfficerData officer WHERE officer.Badge = '{0}'", e.Value.ToString()));
                        if (officer != null)
                        {
                            if (officer.DepartmentType.Equals(this.dispatchOrder.DepartmentType))
                            {
                                gridControlExOfficerLeader.DataSource = null;
                                gridControlExOfficerLeader.AddOrUpdateItem(new GridControlDataOfficerReport(officer));
                                if (!officer.DepartmentStation.DepartmentZone.Equals(dispatchOrder.Unit.DepartmentStation.DepartmentZone) || !officer.DepartmentStation.Equals(this.dispatchOrder.Unit.DepartmentStation))
                                {
                                    //not in the same zone or station as mobile unit
                                    gridViewExOfficerLeader.SetColumnError(gridViewExOfficerLeader.Columns["Badge"],
                                        ResourceLoader.GetString2("OfficerNotInZoneOrStation"),
                                        ErrorType.Warning);
                                    SetGridViewExOfficerLeaderAllowEdit(false);
                                    gridViewExOfficerLeaderMode = GridViewExOfficerLeaderMode.OfficerNotInZoneOrStation;
                                }
                                else
                                {
                                    gridViewExOfficerLeaderMode = GridViewExOfficerLeaderMode.ValidOfficer;
                                }
                            }
                            else
                            {
                                //not in the same department as mobile unit
                                ClearGridViewExOfficerLeader();
                                ((GridControlDataOfficerReport)gridControlExOfficerLeader.SelectedItems[0]).Badge = e.Value.ToString();
                                gridViewExOfficerLeader.SetColumnError(gridViewExOfficerLeader.Columns["Badge"],
                                    ResourceLoader.GetString2("OfficerNotInDepartmentType"),
                                    ErrorType.Critical);
                                SetGridViewExOfficerLeaderAllowEdit(false);
                                gridViewExOfficerLeaderMode = GridViewExOfficerLeaderMode.OfficerNotInDepartment;
                            }
                        }
                        else
                        {
                            //badge does not exist in database
                            GridControlDataOfficerReport gridData = ((GridControlDataOfficerReport)gridControlExOfficerLeader.SelectedItems[0]);
                            if (gridViewExOfficerLeaderMode != GridViewExOfficerLeaderMode.NewOfficer)
                            {
                                ClearGridViewExOfficerLeader();
                                gridData.Name = ResourceLoader.GetString2("TypeHere");
                                gridData.LastName = ResourceLoader.GetString2("TypeHere");
                                gridData.PersonalId = ResourceLoader.GetString2("TypeHere");
                                gridData.Birthday = DateTime.Now;
                            }
                            gridViewExOfficerLeader.SetColumnError(gridViewExOfficerLeader.Columns["Badge"],
                                ResourceLoader.GetString2("CreateNewOfficerLeader"),
                                ErrorType.Information);
                            gridViewExOfficerLeader.SetColumnError(gridViewExOfficerLeader.Columns["Name"],
                                ResourceLoader.GetString2("CreateNewOfficerLeader"),
                                ErrorType.Information);
                            gridViewExOfficerLeader.SetColumnError(gridViewExOfficerLeader.Columns["LastName"],
                                ResourceLoader.GetString2("CreateNewOfficerLeader"),
                                ErrorType.Information);
                            gridViewExOfficerLeader.SetColumnError(gridViewExOfficerLeader.Columns["PersonalId"],
                                ResourceLoader.GetString2("CreateNewOfficerLeader"),
                                ErrorType.Information);
                            gridViewExOfficerLeader.SetColumnError(gridViewExOfficerLeader.Columns["Birthday"],
                                ResourceLoader.GetString2("CreateNewOfficerLeader"),
                                ErrorType.Information);

                            gridData.Badge = e.Value.ToString();
                            gridViewExOfficerLeader.FocusedColumn = gridViewExOfficerLeader.Columns["Name"];
                            gridViewExOfficerLeader.FocusedRowHandle = gridViewExOfficerLeader.GetRowHandle(0);
                            SetGridViewExOfficerLeaderAllowEdit(true);
                            //force to invoke method to prevent delay in updating
                            this.BeginInvoke((MethodInvoker)delegate
                            {
                                gridViewExOfficerLeader.ShowEditor();
                                CheckActiveEditor();
                            });

                            gridViewExOfficerLeaderMode = GridViewExOfficerLeaderMode.NewOfficer;
                        }
                    }
                    else
                    {
                        ClearGridViewExOfficerLeader();
                        gridViewExOfficerLeaderMode = GridViewExOfficerLeaderMode.NoOfficerSelected;
                    }


                    //set colors for each modes
                    switch (gridViewExOfficerLeaderMode)
                    {
                        case GridViewExOfficerLeaderMode.NewOfficer:
                            gridViewExOfficerLeader.Appearance.SelectedRow.BackColor = Color.FromArgb(0xF7, 0xFA, 0xBE);
                            gridViewExOfficerLeader.Appearance.FocusedRow.BackColor = Color.FromArgb(0xF7, 0xFA, 0xBE);
                            break;

                        case GridViewExOfficerLeaderMode.OfficerNotInDepartment:
                        case GridViewExOfficerLeaderMode.NoOfficerSelected:
                        case GridViewExOfficerLeaderMode.OfficerNotInZoneOrStation:
                        case GridViewExOfficerLeaderMode.ValidOfficer:
                            gridViewExOfficerLeader.Appearance.SelectedRow.BackColor = Color.FromArgb(0x74, 0xB3, 0xE1);
                            gridViewExOfficerLeader.Appearance.FocusedRow.BackColor = Color.FromArgb(0x74, 0xB3, 0xE1);
                            break;

                        default:
                            gridViewExOfficerLeader.Appearance.SelectedRow.BackColor = Color.FromArgb(0x74, 0xB3, 0xE1);
                            gridViewExOfficerLeader.Appearance.FocusedRow.BackColor = Color.FromArgb(0x74, 0xB3, 0xE1);
                            break;
                    }
                }
            }
            CheckButtons();
        }

        void gridViewExOfficerLeader_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            CheckButtons();
        }

        void gridViewExOfficerLeader_ShownEditor(object sender, System.EventArgs e)
        {
            //force to invoke method to prevent delay in updating
            this.BeginInvoke((MethodInvoker)delegate
            {
                CheckActiveEditor();
            });
        }

        void gridViewExOfficerLeader_FocusedColumnChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedColumnChangedEventArgs e)
        {
            if (gridViewExOfficerLeaderMode == GridViewExOfficerLeaderMode.NewOfficer)
            {
                //force to invoke method to prevent delay in updating
                this.BeginInvoke((MethodInvoker)delegate
                     {
                         UpdateGridViewExOfficerLeaderCells();
                         gridViewExOfficerLeader.ShowEditor();
                         CheckActiveEditor();
                     });
            }
        }

        void gridControlExOfficerLeader_GotFocus(object sender, System.EventArgs e)
        {
            //force to invoke method to prevent delay in updating
            this.BeginInvoke((MethodInvoker)delegate
            {
                CheckActiveEditor();
            });
        }

        void UpdateGridViewExOfficerLeaderCells()
        {
            int rowHandle = gridViewExOfficerLeader.GetRowHandle(0);
            DevExpress.XtraGrid.Columns.GridColumn column;
            object value;

            column = gridViewExOfficerLeader.Columns["Name"];
            value = gridViewExOfficerLeader.GetRowCellValue(rowHandle, column);
            if (value != null)
            {
                if (value.ToString().Trim() == string.Empty)
                {
                    gridViewExOfficerLeader.SetRowCellValue(rowHandle, column, ResourceLoader.GetString2("TypeHere"));
                }
            }

            column = gridViewExOfficerLeader.Columns["LastName"];
            value = gridViewExOfficerLeader.GetRowCellValue(rowHandle, column);
            if (value != null)
            {
                if (value.ToString().Trim() == string.Empty)
                {
                    gridViewExOfficerLeader.SetRowCellValue(rowHandle, column, ResourceLoader.GetString2("TypeHere"));
                }
            }

            column = gridViewExOfficerLeader.Columns["PersonalId"];
            value = gridViewExOfficerLeader.GetRowCellValue(rowHandle, column);
            if (value != null)
            {
                if (value.ToString().Trim() == string.Empty)
                {
                    gridViewExOfficerLeader.SetRowCellValue(rowHandle, column, ResourceLoader.GetString2("TypeHere"));
                }
            }
        }

        void gridControlExOfficerLeader_Leave(object sender, System.EventArgs e)
        {
            //force to invoke method to prevent delay in updating
            this.BeginInvoke((MethodInvoker)delegate
            {
                UpdateGridViewExOfficerLeaderCells();
            });
        }

        void CheckActiveEditor()
        {
            if (gridViewExOfficerLeader.ActiveEditor != null)
            {
                if (gridViewExOfficerLeader.ActiveEditor.Text == "Type here...")
                {
                    gridViewExOfficerLeader.ActiveEditor.Text = "";
                }
            }
        }

        private void labelControlIncidentType_Click(object sender, EventArgs e)
        {

        }

        //void serverServiceClient_CommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        //{
        //    try
        //    {
        //        if (e.Objects != null && e.Objects.Count > 0)
        //        {
        //            lock (syncCommited)
        //            {
        //                try
        //                {
        //                    if (ServerServiceClient.GetInstance().OperatorClient == null)
        //                    { }
        //                }
        //                catch
        //                { }
        //                if (DispatchCommittedChanges != null)
        //                {
        //                    DispatchCommittedChanges(null, e);
        //                }

        //                if (e.Objects[0] is DepartmentStationAssignHistoryClientData)
        //                {
        //                    #region DepartmentStationAssignHistoryClientData
        //                    //DepartmentStationAssignHistoryClientData departmentStationAssignHistoryClientData = e.Objects[0] as DepartmentStationAssignHistoryClientData;


        //                    //foreach (KeyValuePair<string, List<OfficerClientData>> item in departmentStationAssignHistoryClientData.Units)
        //                    //{
        //                    //    UnitClientData unit = new UnitClientData();
        //                    //    unit.CustomCode = item.Key;

        //                    //    int index = gridControlUnits.Items.IndexOf(new GridControlDataUnit(unit));

        //                    //    if (index > -1)
        //                    //    {
        //                    //        GridControlDataUnit data = gridControlUnits.Items[index] as GridControlDataUnit;
        //                    //        UnitClientData ucd = data.Tag as UnitClientData;
        //                    //        ucd.OfficerAssigns = new ArrayList(item.Value);

        //                    //        unitSyncBox.Sync(new GridControlDataUnit(ucd), CommittedDataAction.Update);
        //                    //    }
        //                    //}
        //                    #endregion
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        SmartLogger.Print(ex);
        //    }
        //}

    }

    public class GridControlDataUnitOfficerReport : GridControlData
    {
        public UnitOfficerAssignHistoryClientData UnitOfficerAssign { get; set; }
        private bool isLeader;


        public GridControlDataUnitOfficerReport(UnitOfficerAssignHistoryClientData unitOfficerAssign)
            : base(unitOfficerAssign)
        {
            UnitOfficerAssign = unitOfficerAssign;
        }

        public int Code
        {
            get
            {
                return UnitOfficerAssign.Code;
            }
            set { }
        }

        [GridControlRowInfoData(Visible = true, RepositoryType = typeof(RepositoryItemCheckEdit), Searchable = false, Width = 80)]
        public bool IsLeader
        {
            get { return isLeader; }
            set { isLeader = value; }
        }

        [GridControlRowInfoData(Visible = true)]
        public string Badge
        {
            get
            {
                return UnitOfficerAssign.Officer.Badge;
            }

        }

        [GridControlRowInfoData(Visible = true)]
        public string Name
        {
            get
            {
                return UnitOfficerAssign.Officer.FirstName;
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string LastName
        {
            get
            {
                return UnitOfficerAssign.Officer.LastName;
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string Unit
        {
            get
            {
                return UnitOfficerAssign.Unit.CustomCode;
            }
        }

        [GridControlRowInfoData(Visible = false)]
        public DateTime? Birthday
        {
            get
            {
                return UnitOfficerAssign.Officer.Birthday;
            }
        }


        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataUnitOfficerReport)
            {
                result = UnitOfficerAssign.Code.Equals(((GridControlDataUnitOfficerReport)obj).UnitOfficerAssign.Code);
            }
            return result;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    public class GridControlDataOfficerReport : GridControlData
    {
        public OfficerClientData Officer { get; set; }

        private string badge;

        public GridControlDataOfficerReport(OfficerClientData officer)
            : base(officer)
        {
            Officer = officer;
        }

        public int Code
        {
            get
            {
                return Officer.Code;
            }
            set
            {
                Officer.Code = value;
            }
        }

        [GridControlRowInfoData(Visible = true, RepositoryType = typeof(RepositoryItemTextEdit))]
        public string Badge
        {
            get
            {
                return Officer.Badge;
            }
            set
            {
                Officer.Badge = value;
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string Name
        {
            get
            {
                return Officer.FirstName;
            }
            set
            {
                Officer.FirstName = value;
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string LastName
        {
            get
            {
                return Officer.LastName;
            }
            set
            {
                Officer.LastName = value;
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string PersonalId
        {
            get
            {
                return Officer.PersonID;
            }
            set
            {
                Officer.PersonID = value;
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public DateTime? Birthday
        {
            get
            {
                return Officer.Birthday;
            }
            set
            {
                Officer.Birthday = value;
            }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataOfficerReport)
            {
                result = Officer.Code.Equals(((GridControlDataOfficerReport)obj).Officer.Code);
            }
            return result;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    public class ReportQuestionComparer : IComparer
    {
        #region IComparer Members

        int IComparer.Compare(object x, object y)
        {
            DispatchReportQuestionDispatchReportClientData rq1 = (DispatchReportQuestionDispatchReportClientData)x;
            DispatchReportQuestionDispatchReportClientData rq2 = (DispatchReportQuestionDispatchReportClientData)y;

            return rq1.Order.CompareTo(rq2.Order);
        }

        #endregion
    }
}