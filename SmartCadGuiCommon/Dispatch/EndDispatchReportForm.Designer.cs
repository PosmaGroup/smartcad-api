using SmartCadControls;
namespace SmartCadGuiCommon
{
    partial class EndDispatchReportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControlEndReport = new DevExpress.XtraLayout.LayoutControl();
            this.labelControlStation = new DevExpress.XtraEditors.LabelControl();
            this.gridControlExOfficerLeader = new GridControlEx();
            this.gridViewExOfficerLeader = new GridViewEx();
            this.simpleButtonAccept = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.gridControlExOfficers = new GridControlEx();
            this.gridViewExOfficers = new GridViewEx();
            this.labelControlZone = new DevExpress.XtraEditors.LabelControl();
            this.labelControlIncident = new DevExpress.XtraEditors.LabelControl();
            this.labelControlIncidentType = new DevExpress.XtraEditors.LabelControl();
            this.labelControlDepartmentType = new DevExpress.XtraEditors.LabelControl();
            this.labelControlOperator = new DevExpress.XtraEditors.LabelControl();
            this.labelControlDate = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlGroupEndReport = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemCancel = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemAccept = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupHeader = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemOfficers = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemDepartmentType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemOperator = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemOfficerLeader = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemIncident = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemIncidentType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemZone = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemStation = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlEndReport)).BeginInit();
            this.layoutControlEndReport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExOfficerLeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExOfficerLeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExOfficers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExOfficers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupEndReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAccept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOfficers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartmentType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOperator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOfficerLeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIncident)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIncidentType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemZone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControlEndReport
            // 
            this.layoutControlEndReport.AllowCustomizationMenu = false;
            this.layoutControlEndReport.Appearance.ControlDisabled.BackColor = System.Drawing.Color.White;
            this.layoutControlEndReport.Appearance.ControlDisabled.ForeColor = System.Drawing.Color.Black;
            this.layoutControlEndReport.Appearance.ControlDisabled.Options.UseBackColor = true;
            this.layoutControlEndReport.Appearance.ControlDisabled.Options.UseForeColor = true;
            this.layoutControlEndReport.Appearance.DisabledLayoutGroupCaption.ForeColor = System.Drawing.Color.White;
            this.layoutControlEndReport.Appearance.DisabledLayoutGroupCaption.Options.UseForeColor = true;
            this.layoutControlEndReport.Appearance.DisabledLayoutItem.ForeColor = System.Drawing.Color.Black;
            this.layoutControlEndReport.Appearance.DisabledLayoutItem.Options.UseForeColor = true;
            this.layoutControlEndReport.Controls.Add(this.labelControlStation);
            this.layoutControlEndReport.Controls.Add(this.gridControlExOfficerLeader);
            this.layoutControlEndReport.Controls.Add(this.simpleButtonAccept);
            this.layoutControlEndReport.Controls.Add(this.simpleButtonCancel);
            this.layoutControlEndReport.Controls.Add(this.gridControlExOfficers);
            this.layoutControlEndReport.Controls.Add(this.labelControlZone);
            this.layoutControlEndReport.Controls.Add(this.labelControlIncident);
            this.layoutControlEndReport.Controls.Add(this.labelControlIncidentType);
            this.layoutControlEndReport.Controls.Add(this.labelControlDepartmentType);
            this.layoutControlEndReport.Controls.Add(this.labelControlOperator);
            this.layoutControlEndReport.Controls.Add(this.labelControlDate);
            this.layoutControlEndReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlEndReport.Location = new System.Drawing.Point(0, 0);
            this.layoutControlEndReport.Name = "layoutControlEndReport";
            this.layoutControlEndReport.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(71, 353, 601, 570);
            this.layoutControlEndReport.Root = this.layoutControlGroupEndReport;
            this.layoutControlEndReport.Size = new System.Drawing.Size(798, 361);
            this.layoutControlEndReport.TabIndex = 0;
            this.layoutControlEndReport.Text = "layoutControlEndReport";
            // 
            // labelControlStation
            // 
            this.labelControlStation.Location = new System.Drawing.Point(557, 83);
            this.labelControlStation.MinimumSize = new System.Drawing.Size(300, 0);
            this.labelControlStation.Name = "labelControlStation";
            this.labelControlStation.Size = new System.Drawing.Size(300, 13);
            this.labelControlStation.StyleController = this.layoutControlEndReport;
            this.labelControlStation.TabIndex = 14;
            this.labelControlStation.Text = "labelControlStation";
            // 
            // gridControlExOfficerLeader
            // 
            this.gridControlExOfficerLeader.EnableAutoFilter = false;
            this.gridControlExOfficerLeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExOfficerLeader.Location = new System.Drawing.Point(7, 281);
            this.gridControlExOfficerLeader.MainView = this.gridViewExOfficerLeader;
            this.gridControlExOfficerLeader.Name = "gridControlExOfficerLeader";
            this.gridControlExOfficerLeader.Size = new System.Drawing.Size(767, 60);
            this.gridControlExOfficerLeader.TabIndex = 12;
            this.gridControlExOfficerLeader.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExOfficerLeader});
            this.gridControlExOfficerLeader.ViewTotalRows = false;
            this.gridControlExOfficerLeader.GotFocus += new System.EventHandler(this.gridControlExOfficerLeader_GotFocus);
            this.gridControlExOfficerLeader.Leave += new System.EventHandler(this.gridControlExOfficerLeader_Leave);
            // 
            // gridViewExOfficerLeader
            // 
            this.gridViewExOfficerLeader.AllowFocusedRowChanged = true;
            this.gridViewExOfficerLeader.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExOfficerLeader.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExOfficerLeader.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExOfficerLeader.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExOfficerLeader.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExOfficerLeader.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExOfficerLeader.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExOfficerLeader.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExOfficerLeader.EnablePreviewLineForFocusedRow = false;
            this.gridViewExOfficerLeader.GridControl = this.gridControlExOfficerLeader;
            this.gridViewExOfficerLeader.Name = "gridViewExOfficerLeader";
            this.gridViewExOfficerLeader.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExOfficerLeader.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExOfficerLeader.OptionsView.ShowGroupPanel = false;
            this.gridViewExOfficerLeader.ViewTotalRows = false;
            this.gridViewExOfficerLeader.ShownEditor += new System.EventHandler(this.gridViewExOfficerLeader_ShownEditor);
            this.gridViewExOfficerLeader.FocusedColumnChanged += new DevExpress.XtraGrid.Views.Base.FocusedColumnChangedEventHandler(this.gridViewExOfficerLeader_FocusedColumnChanged);
            this.gridViewExOfficerLeader.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewExOfficerLeader_CellValueChanged);
            this.gridViewExOfficerLeader.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridViewExOfficerLeader_ValidatingEditor);
            this.gridViewExOfficerLeader.InvalidValueException += new DevExpress.XtraEditors.Controls.InvalidValueExceptionEventHandler(this.gridViewExOfficerLeader_InvalidValueException);
            // 
            // simpleButtonAccept
            // 
            this.simpleButtonAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButtonAccept.Enabled = false;
            this.simpleButtonAccept.Location = new System.Drawing.Point(619, 350);
            this.simpleButtonAccept.MinimumSize = new System.Drawing.Size(78, 32);
            this.simpleButtonAccept.Name = "simpleButtonAccept";
            this.simpleButtonAccept.Size = new System.Drawing.Size(78, 32);
            this.simpleButtonAccept.StyleController = this.layoutControlEndReport;
            this.simpleButtonAccept.TabIndex = 11;
            this.simpleButtonAccept.Text = "simpleButton2";
            this.simpleButtonAccept.Click += new System.EventHandler(this.simpleButtonAccept_Click);
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(701, 350);
            this.simpleButtonCancel.MinimumSize = new System.Drawing.Size(78, 32);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(78, 32);
            this.simpleButtonCancel.StyleController = this.layoutControlEndReport;
            this.simpleButtonCancel.TabIndex = 10;
            this.simpleButtonCancel.Text = "simpleButton1";
            // 
            // gridControlExOfficers
            // 
            this.gridControlExOfficers.EnableAutoFilter = true;
            this.gridControlExOfficers.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExOfficers.Location = new System.Drawing.Point(7, 133);
            this.gridControlExOfficers.MainView = this.gridViewExOfficers;
            this.gridControlExOfficers.Name = "gridControlExOfficers";
            this.gridControlExOfficers.Size = new System.Drawing.Size(767, 128);
            this.gridControlExOfficers.TabIndex = 9;
            this.gridControlExOfficers.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExOfficers});
            this.gridControlExOfficers.ViewTotalRows = true;
            // 
            // gridViewExOfficers
            // 
            this.gridViewExOfficers.AllowFocusedRowChanged = true;
            this.gridViewExOfficers.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExOfficers.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExOfficers.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExOfficers.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExOfficers.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewExOfficers.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewExOfficers.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExOfficers.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExOfficers.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExOfficers.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExOfficers.EnablePreviewLineForFocusedRow = false;
            this.gridViewExOfficers.GridControl = this.gridControlExOfficers;
            this.gridViewExOfficers.Name = "gridViewExOfficers";
            this.gridViewExOfficers.OptionsMenu.EnableFooterMenu = false;
            this.gridViewExOfficers.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExOfficers.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExOfficers.OptionsView.ShowFooter = true;
            this.gridViewExOfficers.OptionsView.ShowGroupPanel = false;
            this.gridViewExOfficers.ViewTotalRows = true;
            this.gridViewExOfficers.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewExOfficers_CellValueChanging);
            // 
            // labelControlZone
            // 
            this.labelControlZone.Location = new System.Drawing.Point(557, 66);
            this.labelControlZone.Name = "labelControlZone";
            this.labelControlZone.Size = new System.Drawing.Size(217, 13);
            this.labelControlZone.StyleController = this.layoutControlEndReport;
            this.labelControlZone.TabIndex = 13;
            this.labelControlZone.Text = "labelControlZone";
            // 
            // labelControlIncident
            // 
            this.labelControlIncident.Location = new System.Drawing.Point(213, 83);
            this.labelControlIncident.Name = "labelControlIncident";
            this.labelControlIncident.Size = new System.Drawing.Size(134, 13);
            this.labelControlIncident.StyleController = this.layoutControlEndReport;
            this.labelControlIncident.TabIndex = 8;
            this.labelControlIncident.Text = "labelControlIncident";
            // 
            // labelControlIncidentType
            // 
            this.labelControlIncidentType.AutoEllipsis = true;
            this.labelControlIncidentType.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControlIncidentType.Location = new System.Drawing.Point(557, 26);
            this.labelControlIncidentType.Name = "labelControlIncidentType";
            this.labelControlIncidentType.Size = new System.Drawing.Size(217, 36);
            this.labelControlIncidentType.StyleController = this.layoutControlEndReport;
            this.labelControlIncidentType.TabIndex = 7;
            this.labelControlIncidentType.Text = "labelControlIncidentType";
            this.labelControlIncidentType.Click += new System.EventHandler(this.labelControlIncidentType_Click);
            // 
            // labelControlDepartmentType
            // 
            this.labelControlDepartmentType.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControlDepartmentType.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControlDepartmentType.Location = new System.Drawing.Point(213, 100);
            this.labelControlDepartmentType.Name = "labelControlDepartmentType";
            this.labelControlDepartmentType.Size = new System.Drawing.Size(561, 13);
            this.labelControlDepartmentType.StyleController = this.layoutControlEndReport;
            this.labelControlDepartmentType.TabIndex = 6;
            this.labelControlDepartmentType.Text = "labelControlDepartmentType";
            // 
            // labelControlOperator
            // 
            this.labelControlOperator.Location = new System.Drawing.Point(213, 66);
            this.labelControlOperator.Name = "labelControlOperator";
            this.labelControlOperator.Size = new System.Drawing.Size(134, 13);
            this.labelControlOperator.StyleController = this.layoutControlEndReport;
            this.labelControlOperator.TabIndex = 5;
            this.labelControlOperator.Text = "labelControlOperator";
            // 
            // labelControlDate
            // 
            this.labelControlDate.Location = new System.Drawing.Point(213, 26);
            this.labelControlDate.Name = "labelControlDate";
            this.labelControlDate.Size = new System.Drawing.Size(134, 36);
            this.labelControlDate.StyleController = this.layoutControlEndReport;
            this.labelControlDate.TabIndex = 4;
            this.labelControlDate.Text = "labelControlDate";
            // 
            // layoutControlGroupEndReport
            // 
            this.layoutControlGroupEndReport.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroupEndReport.GroupBordersVisible = false;
            this.layoutControlGroupEndReport.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemCancel,
            this.layoutControlItemAccept,
            this.layoutControlGroupHeader,
            this.emptySpaceItem2});
            this.layoutControlGroupEndReport.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupEndReport.Name = "Root";
            this.layoutControlGroupEndReport.Size = new System.Drawing.Size(781, 384);
            this.layoutControlGroupEndReport.Text = "Root";
            this.layoutControlGroupEndReport.TextVisible = false;
            // 
            // layoutControlItemCancel
            // 
            this.layoutControlItemCancel.Control = this.simpleButtonCancel;
            this.layoutControlItemCancel.CustomizationFormText = "layoutControlCancel";
            this.layoutControlItemCancel.Location = new System.Drawing.Point(699, 348);
            this.layoutControlItemCancel.MinSize = new System.Drawing.Size(82, 26);
            this.layoutControlItemCancel.Name = "layoutControlItemCancel";
            this.layoutControlItemCancel.Size = new System.Drawing.Size(82, 36);
            this.layoutControlItemCancel.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemCancel.Text = "layoutControlItemCancel";
            this.layoutControlItemCancel.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemCancel.TextToControlDistance = 0;
            this.layoutControlItemCancel.TextVisible = false;
            // 
            // layoutControlItemAccept
            // 
            this.layoutControlItemAccept.Control = this.simpleButtonAccept;
            this.layoutControlItemAccept.CustomizationFormText = "layoutControlItemAccept";
            this.layoutControlItemAccept.Location = new System.Drawing.Point(617, 348);
            this.layoutControlItemAccept.MinSize = new System.Drawing.Size(82, 26);
            this.layoutControlItemAccept.Name = "layoutControlItemAccept";
            this.layoutControlItemAccept.Size = new System.Drawing.Size(82, 36);
            this.layoutControlItemAccept.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemAccept.Text = "layoutControlItemAccept";
            this.layoutControlItemAccept.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemAccept.TextToControlDistance = 0;
            this.layoutControlItemAccept.TextVisible = false;
            // 
            // layoutControlGroupHeader
            // 
            this.layoutControlGroupHeader.AppearanceGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupHeader.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroupHeader.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupHeader.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroupHeader.CustomizationFormText = "layoutControlGroupHeader";
            this.layoutControlGroupHeader.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemOfficers,
            this.layoutControlItemDepartmentType,
            this.layoutControlItemOperator,
            this.layoutControlItemDate,
            this.layoutControlItemOfficerLeader,
            this.layoutControlItemIncident,
            this.layoutControlItemIncidentType,
            this.layoutControlItemZone,
            this.layoutControlItemStation});
            this.layoutControlGroupHeader.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupHeader.Name = "layoutControlGroupHeader";
            this.layoutControlGroupHeader.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupHeader.Size = new System.Drawing.Size(781, 348);
            this.layoutControlGroupHeader.Text = "layoutControlGroupHeader";
            // 
            // layoutControlItemOfficers
            // 
            this.layoutControlItemOfficers.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutControlItemOfficers.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItemOfficers.Control = this.gridControlExOfficers;
            this.layoutControlItemOfficers.CustomizationFormText = "layoutControlItemOfficers";
            this.layoutControlItemOfficers.Location = new System.Drawing.Point(0, 91);
            this.layoutControlItemOfficers.MinSize = new System.Drawing.Size(158, 148);
            this.layoutControlItemOfficers.Name = "layoutControlItemOfficers";
            this.layoutControlItemOfficers.ShowInCustomizationForm = false;
            this.layoutControlItemOfficers.Size = new System.Drawing.Size(771, 148);
            this.layoutControlItemOfficers.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemOfficers.Text = "layoutControlItemOfficers";
            this.layoutControlItemOfficers.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItemOfficers.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItemOfficers.TextSize = new System.Drawing.Size(148, 13);
            this.layoutControlItemOfficers.TextToControlDistance = 3;
            // 
            // layoutControlItemDepartmentType
            // 
            this.layoutControlItemDepartmentType.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutControlItemDepartmentType.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItemDepartmentType.Control = this.labelControlDepartmentType;
            this.layoutControlItemDepartmentType.CustomizationFormText = "layoutControlItemDepartmentType";
            this.layoutControlItemDepartmentType.Location = new System.Drawing.Point(0, 74);
            this.layoutControlItemDepartmentType.MinSize = new System.Drawing.Size(220, 17);
            this.layoutControlItemDepartmentType.Name = "layoutControlItemDepartmentType";
            this.layoutControlItemDepartmentType.Size = new System.Drawing.Size(771, 17);
            this.layoutControlItemDepartmentType.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemDepartmentType.Text = "layoutControlItemDepartmentType";
            this.layoutControlItemDepartmentType.TextSize = new System.Drawing.Size(202, 13);
            // 
            // layoutControlItemOperator
            // 
            this.layoutControlItemOperator.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutControlItemOperator.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItemOperator.Control = this.labelControlOperator;
            this.layoutControlItemOperator.CustomizationFormText = "layoutControlItemOperator";
            this.layoutControlItemOperator.Location = new System.Drawing.Point(0, 40);
            this.layoutControlItemOperator.MinSize = new System.Drawing.Size(311, 17);
            this.layoutControlItemOperator.Name = "layoutControlItemOperator";
            this.layoutControlItemOperator.Size = new System.Drawing.Size(344, 17);
            this.layoutControlItemOperator.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemOperator.Text = "layoutControlItemOperator";
            this.layoutControlItemOperator.TextSize = new System.Drawing.Size(202, 13);
            // 
            // layoutControlItemDate
            // 
            this.layoutControlItemDate.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutControlItemDate.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItemDate.Control = this.labelControlDate;
            this.layoutControlItemDate.CustomizationFormText = "layoutControlItemDate";
            this.layoutControlItemDate.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemDate.MinSize = new System.Drawing.Size(290, 17);
            this.layoutControlItemDate.Name = "layoutControlItemDate";
            this.layoutControlItemDate.Size = new System.Drawing.Size(344, 40);
            this.layoutControlItemDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemDate.Text = "layoutControlItemDate";
            this.layoutControlItemDate.TextSize = new System.Drawing.Size(202, 13);
            // 
            // layoutControlItemOfficerLeader
            // 
            this.layoutControlItemOfficerLeader.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItemOfficerLeader.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItemOfficerLeader.Control = this.gridControlExOfficerLeader;
            this.layoutControlItemOfficerLeader.CustomizationFormText = "layoutControlItemOfficerLeader";
            this.layoutControlItemOfficerLeader.Location = new System.Drawing.Point(0, 239);
            this.layoutControlItemOfficerLeader.MinSize = new System.Drawing.Size(191, 80);
            this.layoutControlItemOfficerLeader.Name = "layoutControlItemOfficerLeader";
            this.layoutControlItemOfficerLeader.Size = new System.Drawing.Size(771, 80);
            this.layoutControlItemOfficerLeader.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemOfficerLeader.Text = "layoutControlItemOfficerLeader";
            this.layoutControlItemOfficerLeader.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItemOfficerLeader.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItemOfficerLeader.TextSize = new System.Drawing.Size(181, 13);
            this.layoutControlItemOfficerLeader.TextToControlDistance = 3;
            // 
            // layoutControlItemIncident
            // 
            this.layoutControlItemIncident.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutControlItemIncident.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItemIncident.Control = this.labelControlIncident;
            this.layoutControlItemIncident.CustomizationFormText = "layoutControlItemIncident";
            this.layoutControlItemIncident.Location = new System.Drawing.Point(0, 57);
            this.layoutControlItemIncident.MinSize = new System.Drawing.Size(306, 17);
            this.layoutControlItemIncident.Name = "layoutControlItemIncident";
            this.layoutControlItemIncident.Size = new System.Drawing.Size(344, 17);
            this.layoutControlItemIncident.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemIncident.Text = "layoutControlItemIncident";
            this.layoutControlItemIncident.TextSize = new System.Drawing.Size(202, 13);
            // 
            // layoutControlItemIncidentType
            // 
            this.layoutControlItemIncidentType.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutControlItemIncidentType.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItemIncidentType.Control = this.labelControlIncidentType;
            this.layoutControlItemIncidentType.CustomizationFormText = "layoutControlItemIncidentType";
            this.layoutControlItemIncidentType.Location = new System.Drawing.Point(344, 0);
            this.layoutControlItemIncidentType.MinSize = new System.Drawing.Size(330, 40);
            this.layoutControlItemIncidentType.Name = "layoutControlItemIncidentType";
            this.layoutControlItemIncidentType.Size = new System.Drawing.Size(427, 40);
            this.layoutControlItemIncidentType.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemIncidentType.Text = "layoutControlItemIncidentType";
            this.layoutControlItemIncidentType.TextSize = new System.Drawing.Size(202, 13);
            // 
            // layoutControlItemZone
            // 
            this.layoutControlItemZone.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutControlItemZone.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItemZone.Control = this.labelControlZone;
            this.layoutControlItemZone.CustomizationFormText = "layoutControlItemZone";
            this.layoutControlItemZone.Location = new System.Drawing.Point(344, 40);
            this.layoutControlItemZone.MinSize = new System.Drawing.Size(223, 17);
            this.layoutControlItemZone.Name = "layoutControlItemZone";
            this.layoutControlItemZone.Size = new System.Drawing.Size(427, 17);
            this.layoutControlItemZone.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemZone.Text = "layoutControlItemZone";
            this.layoutControlItemZone.TextSize = new System.Drawing.Size(202, 13);
            // 
            // layoutControlItemStation
            // 
            this.layoutControlItemStation.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutControlItemStation.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItemStation.Control = this.labelControlStation;
            this.layoutControlItemStation.CustomizationFormText = "layoutControlStation";
            this.layoutControlItemStation.Location = new System.Drawing.Point(344, 57);
            this.layoutControlItemStation.MinSize = new System.Drawing.Size(427, 17);
            this.layoutControlItemStation.Name = "layoutControlItemStation";
            this.layoutControlItemStation.Size = new System.Drawing.Size(427, 17);
            this.layoutControlItemStation.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemStation.Text = "layoutControlItemStation";
            this.layoutControlItemStation.TextSize = new System.Drawing.Size(202, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 348);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(0, 36);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 36);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(617, 36);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // EndDispatchReportForm
            // 
            this.AcceptButton = this.simpleButtonAccept;
            this.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.CancelButton = this.simpleButtonCancel;
            this.ClientSize = new System.Drawing.Size(798, 361);
            this.Controls.Add(this.layoutControlEndReport);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EndDispatchReportForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "EndDispatchReportForm";
            this.Load += new System.EventHandler(this.EndDispatchReportForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlEndReport)).EndInit();
            this.layoutControlEndReport.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExOfficerLeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExOfficerLeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExOfficers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExOfficers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupEndReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAccept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOfficers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartmentType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOperator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOfficerLeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIncident)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIncidentType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemZone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            this.ResumeLayout(false);

        }



        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControlEndReport;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupEndReport;
        private DevExpress.XtraEditors.LabelControl labelControlIncident;
        private DevExpress.XtraEditors.LabelControl labelControlDepartmentType;
        private DevExpress.XtraEditors.LabelControl labelControlOperator;
        private DevExpress.XtraEditors.LabelControl labelControlDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemOperator;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDepartmentType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemIncident;
        private DevExpress.XtraEditors.SimpleButton simpleButtonAccept;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private GridControlEx gridControlExOfficers;
        private GridViewEx gridViewExOfficers;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemOfficers;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemCancel;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemAccept;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupHeader;
        private GridControlEx gridControlExOfficerLeader;
        private GridViewEx gridViewExOfficerLeader;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemOfficerLeader;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.LabelControl labelControlStation;
        private DevExpress.XtraEditors.LabelControl labelControlZone;
        private DevExpress.XtraEditors.LabelControl labelControlIncidentType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemIncidentType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemZone;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemStation;
    }
}