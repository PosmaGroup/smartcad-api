using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Xml;

using Microsoft.Win32;
using SmartCadCore.Core;
using SmartCadCore.Common;

namespace SmartCadGuiCommon
{
    #region Class UspConfiguration Documentation
    /// <summary>
    /// TODO:
    /// </summary>
    /// <className>UspConfiguration</className>
    /// <author email="atorres@smartmatic.com">Alden Torres</author>
    /// <date>2006/03/30</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
	public class AboutForm : DevExpress.XtraEditors.XtraForm
	{
		private System.Windows.Forms.PictureBox pictureBox;
		private System.Windows.Forms.Label lbVersion;
		private System.Windows.Forms.Label lbCopyright;
		private System.Windows.Forms.Label lbReserved;
		private System.Windows.Forms.Label lbLicenceTitle;
		private System.Windows.Forms.Label lbCustomerName;
		private System.Windows.Forms.Label lcCustomerEnterprise;
		private System.Windows.Forms.Label lbSerial;
		private System.Windows.Forms.RichTextBox lbText;
		private System.Windows.Forms.RichTextBox lbInfo;
		
        /// <summary>
		/// Required designer variable.
		/// </summary>
		private new System.ComponentModel.Container components = null;

		public AboutForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
            LoadLanguage();
		}
		
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AboutForm));
			this.pictureBox = new System.Windows.Forms.PictureBox();
			this.lbText = new System.Windows.Forms.RichTextBox();
			this.lbVersion = new System.Windows.Forms.Label();
			this.lbCopyright = new System.Windows.Forms.Label();
			this.lbReserved = new System.Windows.Forms.Label();
			this.lbLicenceTitle = new System.Windows.Forms.Label();
			this.lbCustomerName = new System.Windows.Forms.Label();
			this.lcCustomerEnterprise = new System.Windows.Forms.Label();
			this.lbSerial = new System.Windows.Forms.Label();
			this.lbInfo = new System.Windows.Forms.RichTextBox();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
			this.SuspendLayout();
			// 
			// pictureBox
			// 
			this.pictureBox.Location = new System.Drawing.Point(0, 0);
			this.pictureBox.Name = "pictureBox";
			this.pictureBox.Size = new System.Drawing.Size(553, 226);
			this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pictureBox.TabIndex = 0;
			this.pictureBox.TabStop = false;
			// 
			// lbText
			// 
			this.lbText.BackColor = System.Drawing.Color.White;
			this.lbText.Location = new System.Drawing.Point(8, 320);
			this.lbText.Name = "lbText";
			this.lbText.ReadOnly = true;
			this.lbText.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
			this.lbText.Size = new System.Drawing.Size(256, 88);
			this.lbText.TabIndex = 1;
			this.lbText.Text = "Este programa hace uso de una extensa serie de librerias que son propiedad intele" +
				"ctual y comercial exclusiva de Smartmatic Corporation.";
			this.lbText.LinkClicked += new System.Windows.Forms.LinkClickedEventHandler(this.lbText_LinkClicked);
			// 
			// lbVersion
			// 
			this.lbVersion.Location = new System.Drawing.Point(8, 232);
			this.lbVersion.Name = "lbVersion";
			this.lbVersion.Size = new System.Drawing.Size(192, 16);
			this.lbVersion.TabIndex = 2;
			this.lbVersion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lbCopyright
			// 
			this.lbCopyright.Location = new System.Drawing.Point(8, 248);
			this.lbCopyright.Name = "lbCopyright";
			this.lbCopyright.Size = new System.Drawing.Size(248, 16);
			this.lbCopyright.TabIndex = 3;
			this.lbCopyright.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lbReserved
			// 
			this.lbReserved.Location = new System.Drawing.Point(8, 264);
			this.lbReserved.Name = "lbReserved";
			this.lbReserved.Size = new System.Drawing.Size(192, 16);
			this.lbReserved.TabIndex = 4;
			this.lbReserved.Text = "";
			this.lbReserved.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lbLicenceTitle
			// 
			this.lbLicenceTitle.Location = new System.Drawing.Point(190, 232);
			this.lbLicenceTitle.Name = "lbLicenceTitle";
			this.lbLicenceTitle.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbLicenceTitle.Size = new System.Drawing.Size(346, 16);
			this.lbLicenceTitle.TabIndex = 5;
			this.lbLicenceTitle.Text = "Licencia para la utilizacion de este producto ha sido otorgada a: ";
			this.lbLicenceTitle.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lbCustomerName
			// 
			this.lbCustomerName.Location = new System.Drawing.Point(280, 248);
			this.lbCustomerName.Name = "lbCustomerName";
			this.lbCustomerName.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			this.lbCustomerName.Size = new System.Drawing.Size(256, 16);
			this.lbCustomerName.TabIndex = 6;
			this.lbCustomerName.Text = "Customer name";
			this.lbCustomerName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lcCustomerEnterprise
			// 
			this.lcCustomerEnterprise.Location = new System.Drawing.Point(272, 264);
			this.lcCustomerEnterprise.Name = "lcCustomerEnterprise";
			this.lcCustomerEnterprise.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			this.lcCustomerEnterprise.Size = new System.Drawing.Size(264, 16);
			this.lcCustomerEnterprise.TabIndex = 7;
			this.lcCustomerEnterprise.Text = "Smartmatic Corp";
			this.lcCustomerEnterprise.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lbSerial
			// 
			this.lbSerial.Location = new System.Drawing.Point(188, 280);
			this.lbSerial.Name = "lbSerial";
			this.lbSerial.Size = new System.Drawing.Size(348, 16);
			this.lbSerial.TabIndex = 8;
			this.lbSerial.Text = "Serial: E3D8A22E-D267-4E08-9618-EA0174732E01";
			this.lbSerial.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lbInfo
			// 
			this.lbInfo.BackColor = System.Drawing.Color.White;
			this.lbInfo.Location = new System.Drawing.Point(276, 320);
			this.lbInfo.Name = "lbInfo";
			this.lbInfo.ReadOnly = true;
			this.lbInfo.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
			this.lbInfo.Size = new System.Drawing.Size(264, 88);
			this.lbInfo.TabIndex = 12;
			this.lbInfo.Text = resources.GetString("lbInfo.Text");
			// 
			// AboutForm
			// 
			this.Appearance.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Appearance.Options.UseFont = true;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 11);
			this.ClientSize = new System.Drawing.Size(546, 445);
			this.Controls.Add(this.lbInfo);
			this.Controls.Add(this.lbSerial);
			this.Controls.Add(this.lcCustomerEnterprise);
			this.Controls.Add(this.lbCustomerName);
			this.Controls.Add(this.lbLicenceTitle);
			this.Controls.Add(this.lbReserved);
			this.Controls.Add(this.lbCopyright);
			this.Controls.Add(this.lbVersion);
			this.Controls.Add(this.lbText);
			this.Controls.Add(this.pictureBox);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.LookAndFeel.SkinName = "Blue";
			this.LookAndFeel.UseDefaultLookAndFeel = false;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "AboutForm";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Acerca de SmartCAD";
			this.Load += new System.EventHandler(this.AboutForm_Load);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		#endregion

		private void lbText_LinkClicked(object sender, System.Windows.Forms.LinkClickedEventArgs e)
		{
			System.Diagnostics.Process.Start(e.LinkText);
		}

        private void LoadInfo()
        {
            try
            {
                lbLicenceTitle.Text = "";
                lbVersion.Text = ResourceLoader.GetString2("$Message.ApplicationName") + " " + SmartCadConfiguration.Version;
                lbCustomerName.Text = "";//SmartCadConfiguration.Customer;
                lbCopyright.Text = ResourceLoader.GetString("$Message.Copyright");
                lcCustomerEnterprise.Text = "";//SmartCadConfiguration.Corporation;
                lbSerial.Text = "";//ResourceLoader.GetString2("Serial: ") + SmartCadConfiguration.ProductCode;
            }
            catch (Exception ex)
            {
                MessageForm.Show(ResourceLoader.GetString2("ErrorLoadingInformation"), ex);
            }
        }

        private void AboutForm_Load(object sender, EventArgs e)
        {
            LoadInfo();
            this.pictureBox.Image = ResourceLoader.GetImage("$Image.Splash");
        }
        private void LoadLanguage()
        {
            this.lbReserved.Text = ResourceLoader.GetString2("AllRightsReserved");
        }
	}
}
