﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadGuiCommon.Enums
{
    #region Enums
    public enum InitializeMapActions
    {
        ActivateButton,
        ActivateTreeNode,
        ActivateShapeType
    }
    #endregion
}
