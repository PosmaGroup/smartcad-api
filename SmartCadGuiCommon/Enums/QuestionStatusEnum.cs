﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadGuiCommon.Enums
{
    public enum QuestionStatusEnum
    {
        NotAsked, Asking, Asked
    }
}
