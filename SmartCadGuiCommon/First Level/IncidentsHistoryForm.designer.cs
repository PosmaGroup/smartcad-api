

using SmartCadControls;
using SmartCadControls.Controls;

namespace SmartCadGuiCommon
{
    partial class IncidentsHistoryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanel1 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.searchFilterBar1 = new SearchFilterBar();
            this.dockPanelGrid = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel2_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.gridControlExPreviousIncidents = new GridControlEx();
            this.gridViewExPreviousIncidents = new GridViewEx();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.groupControlDetails = new DevExpress.XtraEditors.GroupControl();
            this.textBoxExIncidentdetails = new SearchableWebBrowser();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.groupControlPlayBack = new DevExpress.XtraTab.XtraTabControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanel1.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            this.dockPanelGrid.SuspendLayout();
            this.dockPanel2_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExPreviousIncidents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExPreviousIncidents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlDetails)).BeginInit();
            this.groupControlDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlPlayBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(795, 690);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel1,
            this.dockPanelGrid});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // dockPanel1
            // 
            this.dockPanel1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.dockPanel1.Appearance.Options.UseBackColor = true;
            this.dockPanel1.Controls.Add(this.dockPanel1_Container);
            this.dockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanel1.ID = new System.Guid("471da6ac-a016-472d-9140-2353b31e4409");
            this.dockPanel1.Location = new System.Drawing.Point(0, 0);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.Options.AllowDockFill = false;
            this.dockPanel1.Options.AllowDockTop = false;
            this.dockPanel1.Options.ShowCloseButton = false;
            this.dockPanel1.Options.ShowMaximizeButton = false;
            this.dockPanel1.OriginalSize = new System.Drawing.Size(233, 200);
            this.dockPanel1.Size = new System.Drawing.Size(233, 692);
            this.dockPanel1.Text = "Options";
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.searchFilterBar1);
            this.dockPanel1_Container.Location = new System.Drawing.Point(3, 29);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(227, 660);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // searchFilterBar1
            // 
            this.searchFilterBar1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchFilterBar1.Location = new System.Drawing.Point(0, 0);
            this.searchFilterBar1.Name = "searchFilterBar1";
            this.searchFilterBar1.Size = new System.Drawing.Size(227, 660);
            this.searchFilterBar1.TabIndex = 0;
            // 
            // dockPanelGrid
            // 
            this.dockPanelGrid.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.dockPanelGrid.Appearance.Options.UseBackColor = true;
            this.dockPanelGrid.Controls.Add(this.dockPanel2_Container);
            this.dockPanelGrid.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelGrid.ID = new System.Guid("fd96e0ca-f576-47b8-8598-de62ee6865c5");
            this.dockPanelGrid.Location = new System.Drawing.Point(233, 0);
            this.dockPanelGrid.Name = "dockPanelGrid";
            this.dockPanelGrid.Options.ShowCloseButton = false;
            this.dockPanelGrid.OriginalSize = new System.Drawing.Size(367, 200);
            this.dockPanelGrid.Size = new System.Drawing.Size(367, 692);
            this.dockPanelGrid.Text = "dockPanelGrid";
            // 
            // dockPanel2_Container
            // 
            this.dockPanel2_Container.Controls.Add(this.gridControlExPreviousIncidents);
            this.dockPanel2_Container.Location = new System.Drawing.Point(3, 29);
            this.dockPanel2_Container.Name = "dockPanel2_Container";
            this.dockPanel2_Container.Size = new System.Drawing.Size(361, 660);
            this.dockPanel2_Container.TabIndex = 0;
            // 
            // gridControlExPreviousIncidents
            // 
            this.gridControlExPreviousIncidents.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlExPreviousIncidents.EnableAutoFilter = true;
            this.gridControlExPreviousIncidents.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExPreviousIncidents.Location = new System.Drawing.Point(0, 0);
            this.gridControlExPreviousIncidents.MainView = this.gridViewExPreviousIncidents;
            this.gridControlExPreviousIncidents.Name = "gridControlExPreviousIncidents";
            this.gridControlExPreviousIncidents.Size = new System.Drawing.Size(361, 660);
            this.gridControlExPreviousIncidents.TabIndex = 4;
            this.gridControlExPreviousIncidents.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExPreviousIncidents});
            this.gridControlExPreviousIncidents.ViewTotalRows = true;
            this.gridControlExPreviousIncidents.CellToolTipNeeded += new DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventHandler(this.gridControlExPreviousIncidents_CellToolTipNeeded);
            // 
            // gridViewExPreviousIncidents
            // 
            this.gridViewExPreviousIncidents.AllowFocusedRowChanged = true;
            this.gridViewExPreviousIncidents.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExPreviousIncidents.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExPreviousIncidents.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExPreviousIncidents.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExPreviousIncidents.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewExPreviousIncidents.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewExPreviousIncidents.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExPreviousIncidents.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExPreviousIncidents.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExPreviousIncidents.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExPreviousIncidents.EnablePreviewLineForFocusedRow = false;
            this.gridViewExPreviousIncidents.GridControl = this.gridControlExPreviousIncidents;
            this.gridViewExPreviousIncidents.Name = "gridViewExPreviousIncidents";
            this.gridViewExPreviousIncidents.OptionsMenu.EnableFooterMenu = false;
            this.gridViewExPreviousIncidents.OptionsNavigation.UseTabKey = false;
            this.gridViewExPreviousIncidents.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExPreviousIncidents.OptionsView.ShowDetailButtons = false;
            this.gridViewExPreviousIncidents.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExPreviousIncidents.OptionsView.ShowFooter = true;
            this.gridViewExPreviousIncidents.ViewTotalRows = true;
            this.gridViewExPreviousIncidents.SelectionWillChange += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewExPreviousIncidents_SelectionWillChange);
            this.gridViewExPreviousIncidents.SingleSelectionChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewExPreviousIncidents_SingleSelectionChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(740, 637);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup1";
            this.layoutControlGroup2.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup2.Size = new System.Drawing.Size(740, 637);
            this.layoutControlGroup2.Text = "layoutControlGroup1";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(736, 633);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem3});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup3.Size = new System.Drawing.Size(702, 692);
            this.layoutControlGroup3.Text = "layoutControlGroup3";
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.groupControlDetails;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(698, 615);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // groupControlDetails
            // 
            this.groupControlDetails.Controls.Add(this.textBoxExIncidentdetails);
            this.groupControlDetails.Location = new System.Drawing.Point(4, 4);
            this.groupControlDetails.Name = "groupControlDetails";
            this.groupControlDetails.Size = new System.Drawing.Size(694, 611);
            this.groupControlDetails.TabIndex = 10;
            this.groupControlDetails.Text = "IncidentDetails";
            // 
            // textBoxExIncidentdetails
            // 
            this.textBoxExIncidentdetails.AllowWebBrowserDrop = false;
            this.textBoxExIncidentdetails.AutoSize = true;
            this.textBoxExIncidentdetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxExIncidentdetails.DocumentText = "<HTML></HTML>\0";
            this.textBoxExIncidentdetails.IsWebBrowserContextMenuEnabled = true;
            this.textBoxExIncidentdetails.Location = new System.Drawing.Point(2, 22);
            this.textBoxExIncidentdetails.Margin = new System.Windows.Forms.Padding(0);
            this.textBoxExIncidentdetails.Name = "textBoxExIncidentdetails";
            this.textBoxExIncidentdetails.Size = new System.Drawing.Size(690, 587);
            this.textBoxExIncidentdetails.TabIndex = 6;
            this.textBoxExIncidentdetails.WebBrowserShortcutsEnabled = false;
            this.textBoxExIncidentdetails.XmlText = "<HTML></HTML>\0";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.groupControlPlayBack;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 615);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(0, 73);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(104, 73);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(698, 73);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // groupControlPlayBack
            // 
            this.groupControlPlayBack.Location = new System.Drawing.Point(4, 619);
            this.groupControlPlayBack.Name = "groupControlPlayBack";
            this.groupControlPlayBack.Size = new System.Drawing.Size(694, 69);
            this.groupControlPlayBack.TabIndex = 4;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.groupControlPlayBack);
            this.layoutControl1.Controls.Add(this.groupControlDetails);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(600, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(972, 154, 205, 350);
            this.layoutControl1.Root = this.layoutControlGroup3;
            this.layoutControl1.Size = new System.Drawing.Size(702, 692);
            this.layoutControl1.TabIndex = 9;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // IncidentsHistoryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1302, 692);
            this.ControlBox = false;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.dockPanelGrid);
            this.Controls.Add(this.dockPanel1);
            this.Name = "IncidentsHistoryForm";
            this.Text = "IncidentsHistoryForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.IncidentsHistoryForm_FormClosing);
            this.Load += new System.EventHandler(this.IncidentsHistoryForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanel1.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            this.dockPanelGrid.ResumeLayout(false);
            this.dockPanel2_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExPreviousIncidents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExPreviousIncidents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlDetails)).EndInit();
            this.groupControlDetails.ResumeLayout(false);
            this.groupControlDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlPlayBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel1;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private SearchFilterBar searchFilterBar1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelGrid;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel2_Container;
        private GridControlEx gridControlExPreviousIncidents;
        private GridViewEx gridViewExPreviousIncidents;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraEditors.GroupControl groupControlDetails;
        private SearchableWebBrowser textBoxExIncidentdetails;
        private DevExpress.XtraTab.XtraTabControl groupControlPlayBack;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
    }
}