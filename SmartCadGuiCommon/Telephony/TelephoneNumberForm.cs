using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using SmartCadCore.Common;


namespace SmartCadGuiCommon
{
    public partial class TelephoneNumberForm : XtraForm
    {
        public TelephoneNumberForm()
        {
            InitializeComponent();
            LoadLanguage();
        }

        public string TelephoneNumber
        {
            get
            {
                return this.textBoxExTelephoneNumber.Text.Trim();
            }
        }

        protected void LoadLanguage()
        {
            buttonExAccept.Text = ResourceLoader.GetString2("TelephoneNumberForm.Accept");
            buttonExCancel.Text = ResourceLoader.GetString2("TelephoneNumberForm.Cancel");
            labelExTelephoneNumber.Text = ResourceLoader.GetString2("TelephoneNumberForm.labelExTelephoneNumber") + ":";
            this.Text = ResourceLoader.GetString2("TelephoneNumberForm");
        }

        private void textBoxExTelephoneNumber_TextChanged(object sender, EventArgs e)
        {
            if (this.textBoxExTelephoneNumber.Text.ToString().Trim().Length > 0)
                this.buttonExAccept.Enabled = true;
            else
                this.buttonExAccept.Enabled = false;
        }
    }
}