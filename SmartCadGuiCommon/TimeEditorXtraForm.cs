using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using SmartCadCore.Core;
using SmartCadCore.ClientData;
using SmartCadCore.Common;

namespace SmartCadGuiCommon
{
    public partial class TimeEditorXtraForm : DevExpress.XtraEditors.XtraForm
    {
        private TimeSpan timespan;

        public TimeSpan Timespan
        {
            get
            {
                return timespan;
            }
            set
            {
                timespan = value;
            }
        }

        public TimeEditorXtraForm(ApplicationPreferenceClientData preference)
        {
            InitializeComponent();
            LoadLanguage();
            switch (preference.MeasureUnit)
            {
                case "DaysUnit":
                    this.textEditDays.Text = preference.Value;
                    break;
                case "HoursUnit":
                    this.textEditHours.Text = preference.Value;
                    break;
                case "MinutesUnit":
                    this.textEditMinutes.Text = preference.Value;
                    break;
                case "SecondsUnit":
                    this.textEditSeconds.Text = preference.Value;
                    break;
                case "MillisecondsUnit":
                    this.textEditMilliseconds.Text = preference.Value;
                    break;                
            }
        }

        private void LoadLanguage()
        {
            this.labelControlDays.Text = ResourceLoader.GetString2("DaysUnit") + ":";
            this.labelControlHours.Text = ResourceLoader.GetString2("HoursUnit") + ":";
            this.labelControlMinutes.Text = ResourceLoader.GetString2("MinutesUnit") + ":";
            this.labelControlSeconds.Text = ResourceLoader.GetString2("SecondsUnit") + ":";
            this.labelControlMilliseconds.Text = ResourceLoader.GetString2("MillisecondsUnit") + ":";
            this.simpleButtonAccept.Text = ResourceLoader.GetString2("$Message.Accept");
            this.simpleButtonCancel.Text = ResourceLoader.GetString2("$Message.Cancel");
            this.Text = ResourceLoader.GetString2("TimeCalculator");
        }

        private void simpleButtonAccept_Click(object sender, EventArgs e)
        {
            timespan = TimeSpan.FromDays(Convert.ToDouble(textEditDays.Text));
            timespan = timespan.Add(TimeSpan.FromHours(Convert.ToDouble(textEditHours.Text)));
            timespan = timespan.Add(TimeSpan.FromMinutes(Convert.ToDouble(textEditMinutes.Text)));
            timespan = timespan.Add(TimeSpan.FromSeconds(Convert.ToDouble(textEditSeconds.Text)));
            timespan = timespan.Add(TimeSpan.FromMilliseconds(Convert.ToDouble(textEditMilliseconds.Text)));
        }

        private void textEditHours_EditValueChanged(object sender, EventArgs e)
        {

        }
    }
}