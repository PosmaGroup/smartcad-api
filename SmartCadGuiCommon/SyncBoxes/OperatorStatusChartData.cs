﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraCharts;
using SmartCadCore.ClientData;


namespace SmartCadGuiCommon.SyncBoxes
{
	class OperatorStatusChartData
	{
		public OperatorStatusChartData(DateTime start, DateTime end, string serie, OperatorClientData oper)
		{
			this.serie = serie;
			this.start = start;
			this.end = end;
			this.operatorName = oper.FirstName + " " + oper.LastName;
			this.operatorCode = oper.Code;
		}

        public OperatorStatusChartData(DateTime start, DateTime end, string serie, OperatorClientData oper, DateTime finished)
            :this(start,end, serie, oper)
        {
            this.finished = finished;
        }

		private int operatorCode;
		public int OperatorCode 
		{ 
			get
			{
				return operatorCode;
			}
			set
			{
				operatorCode = value;
			}
		}

		private string serie;

		public string Serie
		{
			get
			{
				return serie;
			}
			//set
			//{
			//    serie = value;
			//}
		}
		
		private string operatorName;

		public string OperatorName
		{
			get 
			{ 
				return operatorName; 
			}
			//set
			//{
			//    operatorName = value;
			//}
		}

		private DateTime start;

		public DateTime Start
		{
			get
			{
				return start;
			}
			//set
			//{
			//    start = value;
			//}
		}

		private DateTime end;

		public DateTime End
		{
			get
			{
				return end;
			}
			set
			{
				end = value;
			}
		}

        private DateTime finished = DateTime.MinValue;

        public DateTime Finished
        {
            get
            {
                return finished;
            }
            set
            {
                finished = value;
            }
        }

		public override bool Equals(object obj)
		{
			if (!(obj is OperatorStatusChartData))
				return false;
			OperatorStatusChartData data = ((OperatorStatusChartData)obj);
			if (data.serie.Equals(this.serie) && 
				data.operatorName.Equals(this.operatorName) &&
				data.start.Equals(this.start))
			{
				return true;
			}
			return false;
		}
	}
}
