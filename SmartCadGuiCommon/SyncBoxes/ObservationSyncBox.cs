using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using DevExpress.Data;
using SmartCadControls.Controls;
using SmartCadCore.ClientData;
using SmartCadControls;
using SmartCadControls.SyncBoxes;

namespace SmartCadGuiCommon.SyncBoxes
{
    public class OperatorObservationGridData: GridControlData
    {

        private OperatorObservationClientData observation;

        public OperatorObservationClientData Observation
        {
            get 
            { 
                return observation; 
            }
            set 
            { 
                observation = value; 
            }
        }


        public OperatorObservationGridData(OperatorObservationClientData obs)
        :base(obs){
            this.observation = obs;
        }

        [GridControlRowInfoData(Visible= true, Width = 150)]
        public string ObservationType
        {
            get { return observation.FriendlyNameObservationType; }
        }

        [GridControlRowInfoData(Visible = true, Width = 123,ColumnOrder = ColumnSortOrder.Descending)]
        public string DateTime 
        {
            get { return observation.Date.ToShortDateString() + " " + observation.Date.ToString("HH:mm"); }
        }

        [GridControlRowInfoData(Visible = true, Width = 160)]
        public string Supervisor
        {
            get { return observation.SupervisorName; }
        }

        public override bool Equals(object obj)
        {
            OperatorObservationGridData data = obj as OperatorObservationGridData;
            if (data != null)
            {
                return data.Observation.Code == this.Observation.Code;
            }

            return false;
        }
    }


    public class OperatorObservationGridData2 
    {

        private OperatorObservationClientData observation;

        public OperatorObservationClientData Observation
        {
            get
            {
                return observation;
            }
            set
            {
                observation = value;
            }
        }


        public OperatorObservationGridData2(OperatorObservationClientData obs)
        {
            this.observation = obs;
        }

        public string ObservationType
        {
            get { return observation.FriendlyNameObservationType; }
        }

        public string ShortDate
        {
            get { return observation.Date.ToShortDateString(); }
        }


        public string ShortTime
        {
            get { return observation.Date.ToString("HH:mm"); }
        }

        public string Supervisor
        {
            get { return observation.SupervisorName; }
        }
    }

    public class OperatorObservationSyncBox : SyncBox2
    {
        public override DataGridExData BuildGridData(object obj)
        {
            
            return null;
        }
    }

}
