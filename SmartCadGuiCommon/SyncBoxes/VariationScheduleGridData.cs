using SmartCadCore.Common;
using System;
using System.Collections.Generic;
using System.Text;


namespace SmartCadGuiCommon.SyncBoxes
{
    class VariationScheduleGridData
    {
        private int code;
        private int version;
        private DateTime date;
        private DateTime beginTime;
        private DateTime endTime;

        public DateTime BeginTime
        {
            get { return beginTime; }
            set { beginTime = value; }
        }

        public DateTime EndTime
        {
            get
            {
                if (endTime.Hour == 0 && endTime.Minute == 0)
                {
                    return endTime.AddDays(1).AddSeconds(-1);
                }
                else
                    return endTime;
            }
            set { endTime = value; }
        }

        public DateTime Date
        {
            get { return date; }      
            set { date = value; }      
        }

        public string DayOfWeek
        {
            get { return ResourceLoader.GetString2(date.DayOfWeek.ToString()); }
        }

        public int Code
        {
            get { return code; }
            set { code = value; }
        }

        public int Version
        {
            get { return version; }
            set { version = value; }
        }

        public override bool Equals(object obj)
        {
            if (obj is VariationScheduleGridData)
            {
                VariationScheduleGridData var = (VariationScheduleGridData)obj;
                if (var.Date == this.Date &&
                    var.BeginTime.TimeOfDay == this.BeginTime.TimeOfDay &&
                    var.EndTime.TimeOfDay == this.EndTime.TimeOfDay)
                    return true;
            }
            return false;
        }
    }
}
