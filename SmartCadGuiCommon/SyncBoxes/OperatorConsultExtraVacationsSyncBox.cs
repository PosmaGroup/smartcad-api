﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Smartmatic.SmartCad.Service;
using SmartCadCore.ClientData;
using SmartCadControls;
using SmartCadCore.Common;

namespace SmartCadGuiCommon.SyncBoxes
{
    public class OperatorConsultExtraVacationsGridData
    {
        public OperatorConsultExtraVacationsGridData(OperatorClientData ope)
        {
            this.Operator = ope;
            this.Visible = false;
        }

        public OperatorClientData Operator { get; set; }
		
		[GridControlRowInfoData(RepositoryType = typeof(DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit))]
        public bool Visible { get; set; }

		[GridControlRowInfoData(Searchable = true, GroupIndex = 0, Visible = false)]
		public string TypeOperator
		{
			get
			{
				if (Operator.IsSupervisor)
					return ResourceLoader.GetString2("Supervisor");
				else
					return ResourceLoader.GetString2("Operators");
				//else if (Operator.DepartmentTypes.Count > 0)
				//    return ResourceLoader.GetString2("Dispatch");
				//else
				//    return ResourceLoader.GetString2("FirstLevel");
			}
		}

		[GridControlRowInfoData(Searchable = true)]
		public string Name
        {
            get
            {
                return this.Operator.FirstName;
            }
            set { }

        }

		[GridControlRowInfoData(Searchable = true)]
		public string LastName
        {
            get
            {
                return this.Operator.LastName;
            }
            set
            { }

        }

        [GridControlRowInfoData(Searchable = true)]
        public string RoleName
        {
            get
            {
                return this.Operator.RoleFriendlyName;
            }
            set
            { }

        }

	

		public override bool Equals(object obj)
        {
            OperatorConsultExtraVacationsGridData other = obj as OperatorConsultExtraVacationsGridData;
            if (other != null)
            {
                return this.Operator.Code != other.Operator.Code;
            }
            return base.Equals(obj);
        }

    }
}
