﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Collections;
using Smartmatic.SmartCad.Service;
using DevExpress.XtraEditors.Repository;
using System.IO;
using SmartCadControls.Controls;
using SmartCadCore.ClientData;
using SmartCadControls;
using SmartCadCore.Common;


namespace SmartCadGuiCommon
{
    public class GridControlDataWSVSchedule : GridControlData
    {
        public GridControlDataWSVSchedule(WorkShiftScheduleVariationClientData schedule)
            : base(schedule)
        {
        }

        public WorkShiftScheduleVariationClientData ScheduleCient
        {
            get
            {
                return this.Tag as WorkShiftScheduleVariationClientData;
            }
        }

        [GridControlRowInfoData(Visible = false, Width = 100, Searchable = true)]
        public string Name
        {
            get
            {
                return ScheduleCient.WorkShiftVariationName;
            }
        }

        [GridControlRowInfoData(Visible = true, Width = 150, Searchable = true)]
        public string Day
        {
            get
            {
                return ResourceLoader.GetString2(ScheduleCient.Start.DayOfWeek.ToString());
            }
        }

        [GridControlRowInfoData(Visible = true, Width = 110, Searchable = true)]
        public string Date
        {
            get
            {
                return ScheduleCient.Start.ToShortDateString();
            }
        }

        [GridControlRowInfoData(Visible = true, Width = 105, Searchable = true)]
        public string Start
        {
            get
            {
                return ScheduleCient.Start.ToString("HH:mm");
            }
        }

        [GridControlRowInfoData(Visible = true, Width = 105, Searchable = true)]
        public string End
        {
            get
            {
                return ScheduleCient.End.ToString("HH:mm");
            }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataWSVSchedule)
            {
                result = ScheduleCient.Equals((obj as GridControlDataWSVSchedule).ScheduleCient);
            }
            return result;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
