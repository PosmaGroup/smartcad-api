using System;
using DevExpress.Data;
using SmartCadControls.Controls;
using SmartCadCore.ClientData;
using SmartCadControls;
using SmartCadCore.Common;

namespace SmartCadGuiCommon.SyncBoxes
{
    public class OperatorEvaluationGridData: GridControlData 
    {
        //private Color backColor;


        private OperatorEvaluationClientData operatorEvaluation;

        public OperatorEvaluationGridData(OperatorEvaluationClientData operEval)
            : base(operEval)
        {
            this.operatorEvaluation = operEval;
        }

        public OperatorEvaluationClientData OperatorEvaluation
        {
            get { return operatorEvaluation; }
            set { operatorEvaluation = value; }
        }

        [GridControlRowInfoData(Visible = true, Width = 140)]
        public string EvaluationName
        {
            get
            {
                return operatorEvaluation.EvaluationName;
            }
        }

        [GridControlRowInfoData(Visible = true, Width = 70)]
        public string Qualification
        {
            get
            {
                string res = "";
                switch (operatorEvaluation.Scale)
                {
                    case ClientEvaluationScale.OneToThree:
                        res = operatorEvaluation.Qualification.ToString()+ "/3";
                        break;
                    case ClientEvaluationScale.OneToFive:
                        res = operatorEvaluation.Qualification.ToString() + "/5";
                        break;
                    case ClientEvaluationScale.YesNo:
                        res = operatorEvaluation.Qualification.ToString() + "% " + ResourceLoader.GetString2("$Message.Yes");
                        break;
                }
                return res;
            }
        }

        [GridControlRowInfoData(Visible = true, Width = 88,ColumnOrder = ColumnSortOrder.Descending)]
        public DateTime Date
        {
            get
            {
                return operatorEvaluation.Date.Date;
            }
        }

        [GridControlRowInfoData(Visible = true, Width = 135)]
        public string Evaluator
        {
            get
            {
                return operatorEvaluation.EvaluatorFirstName+" "+operatorEvaluation.EvaluatorLastName;
            }
        }

        public override bool Equals(object obj)
        {
            OperatorEvaluationGridData data = obj as OperatorEvaluationGridData;
            if (data != null)
            {
                return data.OperatorEvaluation.Code == this.OperatorEvaluation.Code;
            }

            return false;
        }
    }
}
