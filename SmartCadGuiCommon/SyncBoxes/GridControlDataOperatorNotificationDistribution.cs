﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraEditors.Repository;
using System.Collections;
using Smartmatic.SmartCad.Service;
using SmartCadControls.Controls;
using SmartCadCore.ClientData;
using SmartCadControls;
using SmartCadCore.Common;

namespace SmartCadGuiCommon
{
    public class GridControlDataOperatorNotificationDistribution : GridControlData
    {
        private double[] percentageSchedule;

        public GridControlDataOperatorNotificationDistribution(OperatorClientData oper)
            : base(oper)
        {
            percentageSchedule = new double[2] { 0, 2 };
        }

        private static string supervisedApplicationName = "Dispatch";

        public OperatorClientData OperatorClient
        {
            get
            {
                return this.Tag as OperatorClientData;
            }
        }

        [GridControlRowInfoData(Visible = true, Width = 80, Searchable = true)]
        public string FullFirstName
        {
            get
            {
                return OperatorClient.FirstName;
            }
        }

        [GridControlRowInfoData(Visible = true, Width = 80, Searchable = true)]
        public string FullLastName
        {
            get
            {
                return OperatorClient.LastName;
            }
        }

        [GridControlRowInfoData(Visible = true, Width = 80, Searchable = true)]
        public string Status
        {
            get
            {
                SessionStatusHistoryClientData status = new SessionStatusHistoryClientData();
                status.StatusFriendlyName = ResourceLoader.GetString("MSG_NotConnected");
                if (OperatorClient.LastSessions != null)
                {
                    SessionHistoryClientData lastSession = GetLastSession(OperatorClient.LastSessions, supervisedApplicationName);
                    if (lastSession != null)
                    {
                        if (lastSession.IsLoggedIn.Value == true)
                        {
                            bool found = false;
                            DateTime minDate = DateTime.MinValue;
                            for (int i = 0; i < lastSession.StatusHistoryList.Count && found == false; i++)
                            {
                                SessionStatusHistoryClientData sshcd = (SessionStatusHistoryClientData)lastSession.StatusHistoryList[i];
                                if (sshcd.EndDateStatus == DateTime.MinValue)
                                {
                                    status = sshcd;
                                    found = true;
                                }
                                else if (sshcd.EndDateStatus > minDate)
                                {
                                    status = sshcd;
                                    minDate = status.EndDateStatus;
                                }
                            }
                        }
                    }
                }
                return status.StatusFriendlyName;
            }
        }

        [GridControlRowInfoData(Visible = true, Width = 80, Searchable = true)]
        public string Room
        {
            get
            {
                string room = string.Empty;
                if (OperatorClient.LastSessions != null)
                {
                    SessionHistoryClientData lastSession = GetLastSession(OperatorClient.LastSessions, supervisedApplicationName);
                    if (lastSession != null && lastSession.IsLoggedIn.Value == true)
                    {
                        room = lastSession.Room;
                    }
                }
                return room;
            }
        }

        [GridControlRowInfoData(Visible = true, Width = 80, Searchable = true)]
        public string SitNumber
        {
            get
            {
                string sit = string.Empty;
                if (OperatorClient.LastSessions != null)
                {
                    SessionHistoryClientData lastSession = GetLastSession(OperatorClient.LastSessions, supervisedApplicationName);
                    if (lastSession != null && lastSession.IsLoggedIn.Value == true)
                    {
                        sit = lastSession.NumberSeat;
                    }
                }
                return sit;
            }
        }

        [GridControlRowInfoData(Visible = true, Width = 80, Searchable = true)]
        public string Category
        {
            get
            {
                OperatorCategoryHistoryClientData category = new OperatorCategoryHistoryClientData();
                if (OperatorClient.CategoryList != null)
                {
                    bool found = false;
                    for (int i = 0; i < OperatorClient.CategoryList.Count && found == false; i++)
                    {
                        OperatorCategoryHistoryClientData ochcd = (OperatorCategoryHistoryClientData)OperatorClient.CategoryList[i];
                        if (ochcd.EndDate == DateTime.MinValue)
                        {
                            category = ochcd;
                            found = true;
                        }
                    }
                }
                return category.CategoryFriendlyName;
            }
        }

        private SessionHistoryClientData GetLastSession(IList sessions, string applicationName)
        {
            SessionHistoryClientData session = null;
            bool found = false;
            DateTime minDate = DateTime.MinValue;
            for (int i = 0; i < sessions.Count && found == false; i++)
            {
                SessionHistoryClientData temp = (SessionHistoryClientData)sessions[i];
                if (temp.UserApplication == applicationName)
                {
                    if (temp.EndDateLogin == DateTime.MinValue)
                    {
                        session = temp;
                        found = true;
                    }
                    else if (temp.EndDateLogin > minDate)
                    {
                        session = temp;
                        minDate = temp.EndDateLogin.Value;
                    }
                }
            }
            return session;
        }

        [GridControlRowInfoData(Visible = false, Width = 80, Searchable = true)]
        public string Supervisor
        {
            get
            {
                OperatorAssignClientData assign = GetCurrentAssignedSupervisor();
                if (assign != null)
                    return assign.SupFirstName + " " + assign.SupLastName;
                return string.Empty;
            }
        }

        private OperatorAssignClientData GetCurrentAssignedSupervisor()
        {
            OperatorAssignClientData assign = null;
            DateTime now = ServerServiceClient.GetInstance().GetTime();
            //DateTime time = ApplicationUtil.SCHEDULE_REFERENCE_DATE;
            //time = time.AddHours(now.Hour).AddMinutes(now.Minute).AddSeconds(now.Second).AddMilliseconds(now.Millisecond);
            if (OperatorClient.Supervisors != null && OperatorClient.WorkShifts != null && OperatorClient.WorkShifts.Count > 0)
            {
                for (int i = 0; i < OperatorClient.Supervisors.Count && assign == null; i++)
                {
                    OperatorAssignClientData temp = OperatorClient.Supervisors[i] as OperatorAssignClientData;
                    if ((int)now.DayOfWeek == (int)temp.StartDate.DayOfWeek && temp.StartDate <= now && now <= temp.EndDate)
                    {
                        assign = temp;
                    }
                }
            }
            return assign;
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataOperatorNotificationDistribution)
            {
                result = OperatorClient.Equals((obj as GridControlDataOperatorNotificationDistribution).OperatorClient);
            }
            return result;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
