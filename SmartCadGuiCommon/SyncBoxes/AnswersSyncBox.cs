using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Collections;
using SmartCadControls.Controls;
using SmartCadCore.ClientData;
using SmartCadControls;
using SmartCadCore.Common;


namespace SmartCadGuiCommon.SyncBoxes
{
    public class GridAnswerData : GridControlData
    {
        private QuestionPossibleAnswerClientData answer;
        private string key;
        public static DataGridEx DataGrid = null;


        

        public QuestionPossibleAnswerClientData Answer
        {
            get
            {
                return answer;
            }
            set
            {
                answer = value;
            }
        }

        public GridAnswerData(string objectName, QuestionPossibleAnswerClientData answer)
            : base(answer)
        {
            this.key = objectName;
            this.answer = answer;
        }        


        

        [GridControlRowInfoData(Searchable = true)]
        public string Description
        {
            get
            {
                return answer.Description;
            }
            set
            {
                answer.Description = value;    
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string Type
        {
            get
            {
                string type;
                
                if (answer.ControlName.StartsWith("rdbttn", StringComparison.OrdinalIgnoreCase)) 
                {
                    type = "radio button";
                }
                else if (answer.ControlName.StartsWith("chckdt", StringComparison.OrdinalIgnoreCase))
                {
                    type = "check box";
                }
                else if (answer.ControlName.StartsWith("txtbxx", StringComparison.OrdinalIgnoreCase))
                {
                    type = "text";
                }
                else if (answer.ControlName.StartsWith("textedit", StringComparison.OrdinalIgnoreCase))
                {
                    type = "text";
                }
                else
                {
                    type = "unknown";
                }
                return type;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string Procedure
        {
            get
            {
                if (string.IsNullOrEmpty(answer.Procedure) == true)
                {
                    return ResourceLoader.GetString2("$Message.No");
                }
                else
                {
                    return ResourceLoader.GetString2("$Message.Yes");
                }
            }
            set
            {
                answer.Procedure = value;
            }
        }

        public override bool Equals(object obj)
        {
            GridAnswerData aux = obj as GridAnswerData;
            if(aux != null)
                return this.key == aux.key;
            return false;
        }
    }

}
