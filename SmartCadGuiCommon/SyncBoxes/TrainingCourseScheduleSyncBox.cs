using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Reflection;
using Smartmatic.SmartCad.Service;
using SmartCadControls.Controls;
using SmartCadCore.ClientData;
using SmartCadControls;
using SmartCadCore.Common;


namespace SmartCadGuiCommon.SyncBoxes
{
    public class GridTrainingCourseScheduleData : GridControlData
    {
        private TrainingCourseScheduleClientData courseScheduleClient;

        public TrainingCourseScheduleClientData TrainingCourseScheduleClient
        {
            get
            {
                return courseScheduleClient;
            }
            set
            {
                courseScheduleClient = value;
            }
        }

        public GridTrainingCourseScheduleData(TrainingCourseScheduleClientData courseClient)
            : base(courseClient)
        {
            this.courseScheduleClient = courseClient;
        }

        [GridControlRowInfoData]
        public string Name
        {
            get
            {
                return courseScheduleClient.Name;
            }
        }

        [GridControlRowInfoData]
        public DateTime StartDate
        {
            get
            {
                return courseScheduleClient.Start;
            }
        }

        [GridControlRowInfoData]
        public DateTime EndDate
        {
            get
            {
                return courseScheduleClient.End;
            }
        }

        [GridControlRowInfoData]
        public string Trainer
        {
            get
            {
                return courseScheduleClient.Trainer;
            }
        }

        [GridControlRowInfoData]
        public string Status
        {
            get
            {
                return GetStatus();
            }
        }

        private string GetStatus()
        {
            string status = string.Empty;
            if (courseScheduleClient.RealStart > ServerServiceClient.GetInstance().GetTime() && courseScheduleClient.RealStart != DateTime.MaxValue)
            {
                status = ResourceLoader.GetString2("NotStarted");
            }
            else if (courseScheduleClient.RealEnd < ServerServiceClient.GetInstance().GetTime() && courseScheduleClient.RealEnd != DateTime.MinValue)
            {
                status = ResourceLoader.GetString2("Finished");
            }
            else
            {
                if (courseScheduleClient.RealStart != DateTime.MaxValue && courseScheduleClient.RealEnd != DateTime.MinValue)
                    status = ResourceLoader.GetString2("Started");
            }
            return status;
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridTrainingCourseScheduleData)
            {
                result = this.Name == ((GridTrainingCourseScheduleData)obj).Name &&
                         this.StartDate == ((GridTrainingCourseScheduleData)obj).StartDate &&
                         this.EndDate == ((GridTrainingCourseScheduleData)obj).EndDate;
            }
            return result;
        }
    }


    public class GridEditTrainingCourseScheduleData : DataGridExData
    {
        private TrainingCourseScheduleClientData courseScheduleClient;
        public static DataGridEx DataGrid = null;
        private Color backColor;

        public TrainingCourseScheduleClientData TrainingCourseScheduleClient
        {
            get
            {
                return courseScheduleClient;
            }
            set
            {
                courseScheduleClient = value;
            }
        }

        public GridEditTrainingCourseScheduleData(TrainingCourseScheduleClientData courseClient)
            : base(courseClient)
        {
            this.courseScheduleClient = courseClient;
        }




        public override Color BackColor
        {
            get
            {
                return backColor;
            }
            set
            {
                backColor = value;
            }
        }

        public override string Key
        {
            get
            {
                return courseScheduleClient.Code.ToString();
            }
            set
            {
            }
        }

        [DataGridExColumn(HeaderText = "Horario", Width = 155, ReadOnly=false)]
        public string Name
        {
            get
            {
                return courseScheduleClient.Name;
            }
            set {
                courseScheduleClient.Name = value;   
            }
        }

        [DataGridExColumn(HeaderText = "Fecha inicio", Type = typeof(DateTime), Column=typeof(CalendarDateColumn), Width = 90, ReadOnly=false)]
        public DateTime Start
        {
            get
            {
                return courseScheduleClient.Start;
            }
            set {

                courseScheduleClient.Start = value;
            }
        }

        [DataGridExColumn(HeaderText = "Fecha fin", Type = typeof(DateTime), Column = typeof(CalendarDateColumn), Width = 90, ReadOnly = false)]
        public DateTime End
        {
            get
            {
                return courseScheduleClient.End;
            }
            set 
            {
                courseScheduleClient.End = value;
            }
        }

        [DataGridExColumn(HeaderText = "Instructor", Width = 150, ReadOnly=false)]
        public string Trainer
        {
            get
            {
               return courseScheduleClient.Trainer;
               
            }
            set
            {
                courseScheduleClient.Trainer = value;
            }
        }
    }

    /*public class TrainingCourseScheduleSyncBox2 : SyncBox2
    {

        public override DataGridExData BuildGridData(object obj)
        {
            TrainingCourseScheduleClientData courseScheduleClient = obj as TrainingCourseScheduleClientData;

            GridTrainingCourseScheduleData gridData = new GridTrainingCourseScheduleData(courseScheduleClient);

            return gridData;
        }
    }*/
}
