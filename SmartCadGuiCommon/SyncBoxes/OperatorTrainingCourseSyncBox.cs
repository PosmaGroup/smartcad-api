using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using SmartCadControls;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadControls.SyncBoxes;


namespace SmartCadGuiCommon.SyncBoxes
{
    public class GridOperatorTrainingCourseData : DataGridExData
    {
        public enum ActionOnData
        {
            Approved,
            Failing,
            Pending
        }

        private OperatorTrainingCourseClientData operatorTrainingClient;
        public static DataGridEx DataGrid = null;
        private ActionOnData action;
        private Color backColor;
        private static readonly Image approvedImg = ResourceLoader.GetImage("$Image.Approved");
        private static readonly Image pendingImg = ResourceLoader.GetImage("$Image.Transparent");
        private static readonly Image failingImg = ResourceLoader.GetImage("$Image.Failing");
   

        public OperatorTrainingCourseClientData OperatorTrainingCourseClient
        {
            get
            {
                return operatorTrainingClient;
            }
            set
            {
                operatorTrainingClient = value;
            }
        }

        public GridOperatorTrainingCourseData(OperatorTrainingCourseClientData operatorTrainingClient)
            : base(operatorTrainingClient)
        {
            this.operatorTrainingClient = operatorTrainingClient;
    //        this.Action = action;
        }

        public override Color BackColor
        {
            get
            {
                return backColor;
            }
            set
            {
                backColor = value;
            }
        }

        public override string Key
        {
            get
            {
                return operatorTrainingClient.Code.ToString();
            }
            set
            {
            }
        }

        public ActionOnData Action
        {
            get
            {
                return action;
            }
            set
            {
                if (action != value)
                {
                    action = value;
                }
            }
        }

        [DataGridExColumn(HeaderText = "Operador", Width = 150)]
        public string OperatorName
        {
            get
            {
                return operatorTrainingClient.OperatorFirstName + " " + operatorTrainingClient.OperatorLastName;
            }
        }

        private Image GetImageOnAction(ActionOnData actionOnData)
        {
            Image imageToDisplay = pendingImg;
            return imageToDisplay;
        }
    }

    public class OperatorTrainingCourseSyncBox2: SyncBox2
    {
        public override DataGridExData BuildGridData(object obj)
        {
            OperatorTrainingCourseClientData operatorTrainingClient = obj as OperatorTrainingCourseClientData;

            GridOperatorTrainingCourseData gridData = new GridOperatorTrainingCourseData(operatorTrainingClient);

            return gridData;
        }
    }
}
