using System;
using System.Collections.Generic;
using System.Text;

using System.Drawing;
using System.Collections;
using SmartCadControls;
using SmartCadCore.ClientData;
using SmartCadControls.Controls;

namespace SmartCadGuiCommon
{
    public class GridIncidentData : GridControlData
    {
        private IncidentClientData incident;
        private bool visible = true;

        
        public GridIncidentData(IncidentClientData incident)
            : base(incident)
        {
            this.incident = incident;
        }

		[GridControlRowInfoData(Searchable = true)]
        public string AddressZone
        {
            get
            {
                return incident.Address.Zone;
            }
        }

		[GridControlRowInfoData(Searchable = true)]
        public string AddressStreet
        {
            get
            {
                return incident.Address.Street;
            }
        }

		[GridControlRowInfoData(Searchable = true)]
        public string Reference
        {
            get
            {
                return incident.Address.Reference;
            }
        }

		[GridControlRowInfoData(Searchable = true)]
        public string IncidentTypes
        {
            get
            {
                StringBuilder text = new StringBuilder();

                ArrayList incidentTypes = (ArrayList)incident.IncidentTypes;
                incidentTypes.Sort(new ClientDataCodeComparer());
                
                for (int index = 0; index < incident.IncidentTypes.Count; index++)
                {
                    IncidentTypeClientData incidentType = incident.IncidentTypes[index] as IncidentTypeClientData;

                    if (incidentType != null)
                    {
                       // if (incidentType.NoEmergency == false)
                        if (incidentType.NoEmergency == true)
                            text.Append("NE");
                        else
                            text.Append(incidentType.CustomCode);
                        if (index < (incident.IncidentTypes.Count - 1))
                        {
                            text.Append(", ");
                        }
                    }
                }
                if (text.Length > 1 && text[text.Length - 2] == ',')
                {
                    text = text.Replace(',', '.', text.Length - 2, 1);
                }
                else
                    text = text.Append(".");

                return text.ToString();
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string IncidentID 
        {
            get 
            {
                return incident.CustomCode;
            }
        
        }

        [GridControlRowInfoData(Searchable = true, DisplayFormat = "MM/dd/yyyy HH:mm")]
        public DateTime CallDate
        {
            get
            {
				return incident.StartDate;
            }
        }

		[GridControlRowInfoData(Searchable = true, ImageIndex = 0)]
        public int PhoneReportCount
        {
            get
            {
                return incident.PhoneReports.Count;
            }
        }

        
        /// <summary>
        /// List of phone numbers to check the same number filter.
        /// </summary>
        [GridControlRowInfoData(Visible=false)]
		public string LineNumber
		{
			get
			{
                List<string> lineNumbers = new List<string>();
				foreach (PhoneReportClientData report in incident.PhoneReports)
				{
					lineNumbers.Add(report.PhoneReportCallerClient.Telephone);
				}
				return ","+string.Join(",",lineNumbers.ToArray())+",";
			}
		}

		public string Status 
		{
			get
			{
				return incident.Status.CustomCode;
			}
		}

        [GridControlRowInfoData(Visible=false)]
        public bool Visible
        {
            get
            {
                return visible;
            }
            set
            {
                visible = value;   
            }
        }

		public override bool Equals(object obj)
		{
			if (obj is GridIncidentData == false)
				return false;
			GridIncidentData data = obj as GridIncidentData;
			if (data.incident.Code == incident.Code || data.incident.CustomCode.Equals(incident.CustomCode))
				return true;
			return false;
		}
    }

    public class GridCctvIncidentData : GridControlData
    {
        private IncidentClientData incident;
        private bool visible = true;


        [GridControlRowInfoData(Searchable = true)]
        public string Camara
        {
            get
            {
                return incident.IncidentCode.ToString();
            }
        }

        public GridCctvIncidentData(IncidentClientData incident)
            : base(incident)
        {
            this.incident = incident;
        }

        [GridControlRowInfoData(Searchable = true)]
        public string AddressZone
        {
            get
            {
                return incident.Address.Zone;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string AddressStreet
        {
            get
            {
                return incident.Address.Street;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string IncidentTypes
        {
            get
            {
                StringBuilder text = new StringBuilder();

                ArrayList incidentTypes = (ArrayList)incident.IncidentTypes;
                incidentTypes.Sort(new ClientDataCodeComparer());

                for (int index = 0; index < incident.IncidentTypes.Count; index++)
                {
                    IncidentTypeClientData incidentType = incident.IncidentTypes[index] as IncidentTypeClientData;

                    if (incidentType != null)
                    {
                        // if (incidentType.NoEmergency == false)
                        if (incidentType.NoEmergency == true)
                            text.Append("NE");
                        else
                            text.Append(incidentType.CustomCode);
                        if (index < (incident.IncidentTypes.Count - 1))
                        {
                            text.Append(", ");
                        }
                    }
                }
                if (text.Length > 1 && text[text.Length - 2] == ',')
                {
                    text = text.Replace(',', '.', text.Length - 2, 1);
                }
                else
                    text = text.Append(".");

                return text.ToString();
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string IncidentID
        {
            get
            {
                return incident.CustomCode;
            }

        }

        [GridControlRowInfoData(Searchable = true, DisplayFormat = "MM/dd/yyyy HH:mm")]
        public DateTime CallDate
        {
            get
            {
                return incident.StartDate;
            }
        }
        
        
        [GridControlRowInfoData(Searchable = true, ImageIndex = 0)]
        public int PhoneReportCount
        {
            get
            {
                return incident.PhoneReports.Count;
            }
        }

        /// <summary>
        /// List of phone numbers to check the same number filter.
        /// </summary>
        [GridControlRowInfoData(Visible = false)]
        public string LineNumber
        {
            get
            {
                List<string> lineNumbers = new List<string>();
                foreach (PhoneReportClientData report in incident.PhoneReports)
                {
                    lineNumbers.Add(report.PhoneReportCallerClient.Telephone);
                }
                return "," + string.Join(",", lineNumbers.ToArray()) + ",";
            }
        }

        public string Status
        {
            get
            {
                return incident.Status.CustomCode;
            }
        }

        [GridControlRowInfoData(Visible = false)]
        public bool Visible
        {
            get
            {
                return visible;
            }
            set
            {
                visible = value;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj is GridCctvIncidentData == false)
                return false;
            GridCctvIncidentData data = obj as GridCctvIncidentData;
            if (data.incident.Code == incident.Code || data.incident.CustomCode.Equals(incident.CustomCode))
                return true;
            return false;
        }
    }
}
