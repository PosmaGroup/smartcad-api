using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using SmartCadControls.Controls;
using SmartCadCore.ClientData;
using SmartCadControls;

namespace SmartCadGuiCommon.SyncBoxes
{
    public class OperatorCategoryHistoryGridData: GridControlData
    {

        public OperatorCategoryHistoryGridData(OperatorCategoryHistoryClientData var)
        :base(var)
        {
            this.operatorCategoryHistory = var;
        }

        private OperatorCategoryHistoryClientData operatorCategoryHistory;

        public OperatorCategoryHistoryClientData OperatorCategoryHistory
        {
            get { return operatorCategoryHistory; }
            set { operatorCategoryHistory = value; }
        }


        [GridControlRowInfoData(Visible = true, Width = 120)]
        public string Category 
        {
            get 
            {
                return this.operatorCategoryHistory.CategoryFriendlyName;
            }
        }

        [GridControlRowInfoData(Visible = true, Width = 88)]
        public string Since 
        {
            get 
            {
                return this.operatorCategoryHistory.StartDate.Value.ToShortDateString();
            }
        }

        
        public override bool Equals(object obj)
        {
            OperatorCategoryHistoryGridData data = obj as OperatorCategoryHistoryGridData;
            if (data != null)
            {
                return data.OperatorCategoryHistory.CategoryCode == this.OperatorCategoryHistory.CategoryCode;
            }

            return false;
        }
    }
}
