﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartCadControls;
using SmartCadCore.ClientData;
using SmartCadControls.Controls;

namespace SmartCadGuiCommon
{
    public class AlertsLprSyncBoxData : GridControlData
    {

        private LprAlertClientData lprAlerts;
        private bool visible = true;

        public AlertsLprSyncBoxData(LprAlertClientData lprAlerts)
            : base(lprAlerts)
        {
            this.lprAlerts = lprAlerts;
        }

        [GridControlRowInfoData(Searchable = true)]
        public string Camera
        {
            get
            {
                return lprAlerts.Camera.Ip;
            }
        }

        [GridControlRowInfoData(Searchable = true, DisplayFormat = "MM/dd/yyyy HH:mm:ss")]
        public DateTime Date
        {
            get
            {
                return lprAlerts.AlertDate;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public int Status
        {
            get
            {
                return lprAlerts.Status;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string Plate
        {
            get
            {
                return lprAlerts.Plate;
            }
        } 

        [GridControlRowInfoData(Visible = false)]
        public bool Visible
        {
            get
            {
                return visible;
            }
            set
            {
                visible = value;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj is AlertsLprSyncBoxData == false)
                return false;
            AlertsLprSyncBoxData data = obj as AlertsLprSyncBoxData;
            if (data.lprAlerts.Code == lprAlerts.Code || data.lprAlerts.Code.Equals(lprAlerts.Code))
                return true;
            return false;
        }
    }
}
