using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using SmartCadControls.Controls;
using SmartCadCore.ClientData;
using SmartCadControls;

namespace SmartCadGuiCommon.SyncBoxes
{
    public class OperatorTrainingCourseGridData: GridControlData 
    {

        private OperatorTrainingCourseClientData operatorTrainingCourse;
        //private Color backColor;

        public OperatorTrainingCourseGridData(OperatorTrainingCourseClientData operTraining)
            : base(operTraining)
        {
            this.operatorTrainingCourse = operTraining;
        }
       
        [GridControlRowInfoData(Visible = true, Width = 132)]
        public string CourseName 
        {
            get 
            {
                return operatorTrainingCourse.CourseName;
            }
        }

        [GridControlRowInfoData(Visible = true, DisplayFormat = "MM/dd/yyyy", Width = 85)]
        public DateTime StartDate
        {
            get
            {
                return operatorTrainingCourse.StarDate;
            }
        }

        [GridControlRowInfoData(Visible = true, DisplayFormat = "MM/dd/yyyy", Width = 85)]
        public DateTime EndDate
        {
            get
            {
                return operatorTrainingCourse.EndDate;
            }
        }

        [GridControlRowInfoData(Visible = true, Width = 130)]
        public string Trainer
        {
            get
            {
                return operatorTrainingCourse.CourseScheduleTrainer;
            }
        }

        
        public OperatorTrainingCourseClientData OperatorTrainingCourse
        {
            get { return operatorTrainingCourse; }
            set { operatorTrainingCourse = value; }
        }

        public override bool Equals(object obj)
        {
            OperatorTrainingCourseGridData data = obj as OperatorTrainingCourseGridData;
            if (data != null)
            {
                return data.OperatorTrainingCourse.Code == this.OperatorTrainingCourse.Code;
            }

            return false;
        }

    }
}
