using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using SmartCadControls;
using SmartCadCore.ClientData;
using SmartCadControls.SyncBoxes;
using SmartCadCore.Common;
using SmartCadControls.Filters;
using SmartCadCore.Core;


namespace SmartCadGuiCommon.SyncBoxes
{
    public class GridTrainingCourseData : DataGridExData
    {
        private TrainingCourseClientData courseClient;
        public static DataGridEx DataGrid = null;

        private Color backColor;

        public TrainingCourseClientData TrainingCourseClient
        {
            get
            {
                return courseClient;
            }
            set
            {
                courseClient = value;
            }
        }

        public GridTrainingCourseData(TrainingCourseClientData courseClient)
            : base(courseClient)
        {
            this.courseClient = courseClient;
        }

        public override Color BackColor
        {
            get
            {
                return backColor;
            }
            set
            {
                backColor = value;
            }
        }

        public override string Key
        {
            get
            {
                return courseClient.Code.ToString();
            }
            set
            {
            }
        }

        [DataGridExColumn(HeaderText = "Nombre", Width = 120)]
        public string Name
        {
            get
            {
                return courseClient.Name;
            }
        }

        [DataGridExColumn(HeaderText = "Objetivos", Width = 150)]
        public string Objective
        {
            get
            {
				return courseClient.Objective.Replace("\r", " ").Replace("\n", "");
            }
        }

        [DataGridExColumn(HeaderText = "Contenido", Width = 150)]
        public string Content
        {
            get
            {
				return courseClient.Content.Replace("\r", " ").Replace("\n","");
            }
        }

        [DataGridExColumn(HeaderText = "Contacto", Width = 100)]
        public string ContactPerson
        {
            get
            {
                return courseClient.ContactPerson;
            }
        }

        [DataGridExColumn(HeaderText = "Telefono", Width = 100)]
        public string PhoneContact
        {
            get
            {
                return courseClient.PhoneContactPerson;
            }
        }

        [DataGridExColumn(HeaderText = "En linea", Width = 75, Column = typeof(DataGridViewCheckBoxColumn))]
        public bool OnLine
        {
            get
            {
                if (string.IsNullOrEmpty(courseClient.Link))
                    return false;
                else
                    return true;
            }
        }
    }

    public class TrainingCourseSyncBox2 : SyncBox2
    {

        public override DataGridExData BuildGridData(object obj)
        {
            TrainingCourseClientData courseClient = obj as TrainingCourseClientData;

            GridTrainingCourseData gridData = new GridTrainingCourseData(courseClient);

            return gridData;
        }

        public override void Sync(object objectData, CommittedDataAction action, FilterBase filter)
        {
            DataGridExData gridData = BuildGridData(objectData);

            bool visible = true;

            switch (action)
            {
                case CommittedDataAction.Save:
                    {
                        if (filter != null)
                            visible = filter.Filter(objectData);

                        FormUtil.InvokeRequired(DataGrid, delegate
                        {
                            DataGrid.AddData(gridData, visible);
                            DataGrid.SelectData(gridData);
                        });
                    }
                    break;
                case CommittedDataAction.Update:
                    {
                        if (filter != null)
                            visible = filter.Filter(objectData);

                        if (DataGrid.ContainsData(gridData))
                        {
                            FormUtil.InvokeRequired(DataGrid, delegate
                            {
                                DataGrid.UpdateData(gridData, visible);
                            });
                        }
                        else
                        {
                            FormUtil.InvokeRequired(DataGrid, delegate
                            {
                                DataGrid.AddData(gridData, visible);
                            });
                        }
                    }
                    break;
                case CommittedDataAction.Delete:
                    if (DataGrid.ContainsData(gridData) == true)
                    {
                        FormUtil.InvokeRequired(DataGrid, delegate
                        {
                            DataGrid.RemoveData(gridData);
                        });
                    }
                    break;
            }
        }
    }
}
