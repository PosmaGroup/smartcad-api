namespace SmartCadGuiCommon
{
    partial class TimeEditorXtraForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.simpleButtonAccept = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.labelControlDays = new DevExpress.XtraEditors.LabelControl();
            this.labelControlHours = new DevExpress.XtraEditors.LabelControl();
            this.labelControlMinutes = new DevExpress.XtraEditors.LabelControl();
            this.labelControlSeconds = new DevExpress.XtraEditors.LabelControl();
            this.labelControlMilliseconds = new DevExpress.XtraEditors.LabelControl();
            this.textEditDays = new DevExpress.XtraEditors.TextEdit();
            this.textEditHours = new DevExpress.XtraEditors.TextEdit();
            this.textEditMinutes = new DevExpress.XtraEditors.TextEdit();
            this.textEditSeconds = new DevExpress.XtraEditors.TextEdit();
            this.textEditMilliseconds = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDays.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditHours.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditMinutes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditSeconds.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditMilliseconds.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButtonAccept
            // 
            this.simpleButtonAccept.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonAccept.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButtonAccept.Appearance.Options.UseFont = true;
            this.simpleButtonAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButtonAccept.Location = new System.Drawing.Point(105, 138);
            this.simpleButtonAccept.Name = "simpleButtonAccept";
            this.simpleButtonAccept.Size = new System.Drawing.Size(75, 25);
            this.simpleButtonAccept.TabIndex = 0;
            this.simpleButtonAccept.Text = "Accept";
            this.simpleButtonAccept.Click += new System.EventHandler(this.simpleButtonAccept_Click);
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButtonCancel.Appearance.Options.UseFont = true;
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(186, 138);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(75, 25);
            this.simpleButtonCancel.TabIndex = 1;
            this.simpleButtonCancel.Text = "Cancelar";
            // 
            // labelControlDays
            // 
            this.labelControlDays.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControlDays.Appearance.Options.UseFont = true;
            this.labelControlDays.Location = new System.Drawing.Point(12, 11);
            this.labelControlDays.Name = "labelControlDays";
            this.labelControlDays.Size = new System.Drawing.Size(26, 13);
            this.labelControlDays.TabIndex = 2;
            this.labelControlDays.Text = "Dias:";
            // 
            // labelControlHours
            // 
            this.labelControlHours.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControlHours.Appearance.Options.UseFont = true;
            this.labelControlHours.Location = new System.Drawing.Point(12, 34);
            this.labelControlHours.Name = "labelControlHours";
            this.labelControlHours.Size = new System.Drawing.Size(31, 13);
            this.labelControlHours.TabIndex = 3;
            this.labelControlHours.Text = "Horas:";
            // 
            // labelControlMinutes
            // 
            this.labelControlMinutes.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControlMinutes.Appearance.Options.UseFont = true;
            this.labelControlMinutes.Location = new System.Drawing.Point(12, 57);
            this.labelControlMinutes.Name = "labelControlMinutes";
            this.labelControlMinutes.Size = new System.Drawing.Size(40, 13);
            this.labelControlMinutes.TabIndex = 4;
            this.labelControlMinutes.Text = "Minutos:";
            // 
            // labelControlSeconds
            // 
            this.labelControlSeconds.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControlSeconds.Appearance.Options.UseFont = true;
            this.labelControlSeconds.Location = new System.Drawing.Point(12, 80);
            this.labelControlSeconds.Name = "labelControlSeconds";
            this.labelControlSeconds.Size = new System.Drawing.Size(51, 13);
            this.labelControlSeconds.TabIndex = 5;
            this.labelControlSeconds.Text = "Segundos:";
            // 
            // labelControlMilliseconds
            // 
            this.labelControlMilliseconds.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControlMilliseconds.Appearance.Options.UseFont = true;
            this.labelControlMilliseconds.Location = new System.Drawing.Point(12, 102);
            this.labelControlMilliseconds.Name = "labelControlMilliseconds";
            this.labelControlMilliseconds.Size = new System.Drawing.Size(64, 13);
            this.labelControlMilliseconds.TabIndex = 6;
            this.labelControlMilliseconds.Text = "Milisegundos:";
            // 
            // textEditDays
            // 
            this.textEditDays.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditDays.EditValue = "0";
            this.textEditDays.Location = new System.Drawing.Point(96, 9);
            this.textEditDays.Name = "textEditDays";
            this.textEditDays.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEditDays.Properties.Appearance.Options.UseFont = true;
            this.textEditDays.Properties.Appearance.Options.UseTextOptions = true;
            this.textEditDays.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditDays.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEditDays.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditDays.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.textEditDays.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditDays.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.textEditDays.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditDays.Properties.DisplayFormat.FormatString = "n0";
            this.textEditDays.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.textEditDays.Properties.EditFormat.FormatString = "n0";
            this.textEditDays.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.textEditDays.Properties.Mask.EditMask = "n0";
            this.textEditDays.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEditDays.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textEditDays.Properties.NullText = "0";
            this.textEditDays.Size = new System.Drawing.Size(165, 19);
            this.textEditDays.TabIndex = 7;
            // 
            // textEditHours
            // 
            this.textEditHours.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditHours.Location = new System.Drawing.Point(96, 32);
            this.textEditHours.Name = "textEditHours";
            this.textEditHours.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEditHours.Properties.Appearance.Options.UseFont = true;
            this.textEditHours.Properties.Appearance.Options.UseTextOptions = true;
            this.textEditHours.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditHours.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEditHours.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditHours.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.textEditHours.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditHours.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.textEditHours.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditHours.Properties.DisplayFormat.FormatString = "n0";
            this.textEditHours.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.textEditHours.Properties.EditFormat.FormatString = "n0";
            this.textEditHours.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.textEditHours.Properties.Mask.EditMask = "n0";
            this.textEditHours.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEditHours.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textEditHours.Properties.NullText = "0";
            this.textEditHours.Size = new System.Drawing.Size(165, 19);
            this.textEditHours.TabIndex = 8;
            // 
            // textEditMinutes
            // 
            this.textEditMinutes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditMinutes.Location = new System.Drawing.Point(96, 54);
            this.textEditMinutes.Name = "textEditMinutes";
            this.textEditMinutes.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEditMinutes.Properties.Appearance.Options.UseFont = true;
            this.textEditMinutes.Properties.Appearance.Options.UseTextOptions = true;
            this.textEditMinutes.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditMinutes.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEditMinutes.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditMinutes.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.textEditMinutes.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditMinutes.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.textEditMinutes.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditMinutes.Properties.DisplayFormat.FormatString = "n0";
            this.textEditMinutes.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.textEditMinutes.Properties.EditFormat.FormatString = "n0";
            this.textEditMinutes.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.textEditMinutes.Properties.Mask.EditMask = "n0";
            this.textEditMinutes.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEditMinutes.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textEditMinutes.Properties.NullText = "0";
            this.textEditMinutes.Size = new System.Drawing.Size(165, 19);
            this.textEditMinutes.TabIndex = 9;
            // 
            // textEditSeconds
            // 
            this.textEditSeconds.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditSeconds.Location = new System.Drawing.Point(96, 77);
            this.textEditSeconds.Name = "textEditSeconds";
            this.textEditSeconds.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEditSeconds.Properties.Appearance.Options.UseFont = true;
            this.textEditSeconds.Properties.Appearance.Options.UseTextOptions = true;
            this.textEditSeconds.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditSeconds.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEditSeconds.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditSeconds.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.textEditSeconds.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditSeconds.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.textEditSeconds.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditSeconds.Properties.DisplayFormat.FormatString = "n0";
            this.textEditSeconds.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.textEditSeconds.Properties.EditFormat.FormatString = "n0";
            this.textEditSeconds.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.textEditSeconds.Properties.Mask.EditMask = "n0";
            this.textEditSeconds.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEditSeconds.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textEditSeconds.Properties.NullText = "0";
            this.textEditSeconds.Size = new System.Drawing.Size(165, 19);
            this.textEditSeconds.TabIndex = 10;
            // 
            // textEditMilliseconds
            // 
            this.textEditMilliseconds.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditMilliseconds.Location = new System.Drawing.Point(96, 100);
            this.textEditMilliseconds.Name = "textEditMilliseconds";
            this.textEditMilliseconds.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEditMilliseconds.Properties.Appearance.Options.UseFont = true;
            this.textEditMilliseconds.Properties.Appearance.Options.UseTextOptions = true;
            this.textEditMilliseconds.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditMilliseconds.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEditMilliseconds.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditMilliseconds.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.textEditMilliseconds.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditMilliseconds.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.textEditMilliseconds.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditMilliseconds.Properties.DisplayFormat.FormatString = "n0";
            this.textEditMilliseconds.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.textEditMilliseconds.Properties.EditFormat.FormatString = "n0";
            this.textEditMilliseconds.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.textEditMilliseconds.Properties.Mask.EditMask = "n0";
            this.textEditMilliseconds.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEditMilliseconds.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textEditMilliseconds.Properties.NullText = "0";
            this.textEditMilliseconds.Size = new System.Drawing.Size(165, 19);
            this.textEditMilliseconds.TabIndex = 11;
            // 
            // TimeEditorXtraForm
            // 
            this.AcceptButton = this.simpleButtonAccept;
            this.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonCancel;
            this.ClientSize = new System.Drawing.Size(270, 171);
            this.Controls.Add(this.textEditMilliseconds);
            this.Controls.Add(this.textEditSeconds);
            this.Controls.Add(this.textEditMinutes);
            this.Controls.Add(this.textEditHours);
            this.Controls.Add(this.textEditDays);
            this.Controls.Add(this.labelControlMilliseconds);
            this.Controls.Add(this.labelControlSeconds);
            this.Controls.Add(this.labelControlMinutes);
            this.Controls.Add(this.labelControlHours);
            this.Controls.Add(this.labelControlDays);
            this.Controls.Add(this.simpleButtonCancel);
            this.Controls.Add(this.simpleButtonAccept);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "TimeEditorXtraForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "TimeEditorXtraForm";
            ((System.ComponentModel.ISupportInitialize)(this.textEditDays.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditHours.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditMinutes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditSeconds.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditMilliseconds.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButtonAccept;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private DevExpress.XtraEditors.LabelControl labelControlDays;
        private DevExpress.XtraEditors.LabelControl labelControlHours;
        private DevExpress.XtraEditors.LabelControl labelControlMinutes;
        private DevExpress.XtraEditors.LabelControl labelControlSeconds;
        private DevExpress.XtraEditors.LabelControl labelControlMilliseconds;
        private DevExpress.XtraEditors.TextEdit textEditDays;
        private DevExpress.XtraEditors.TextEdit textEditHours;
        private DevExpress.XtraEditors.TextEdit textEditMinutes;
        private DevExpress.XtraEditors.TextEdit textEditSeconds;
        private DevExpress.XtraEditors.TextEdit textEditMilliseconds;
    }
}