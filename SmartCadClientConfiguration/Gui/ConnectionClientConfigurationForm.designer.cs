
using SmartCadControls.Controls;

namespace SmartCadClientConfiguration.Gui
{
    partial class ConnectionClientConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private new System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConnectionClientConfigurationForm));
            this.buttonExAccept = new SimpleButtonEx();
            this.buttonExCancel = new SimpleButtonEx();
            this.tabControl = new DevExpress.XtraTab.XtraTabControl();
            this.tabPageConnection = new DevExpress.XtraTab.XtraTabPage();
            this.groupBoxConnection = new DevExpress.XtraEditors.GroupControl();
            this.labelExAddress = new DevExpress.XtraEditors.LabelControl();
            this.textBoxExAddress = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).BeginInit();
            this.tabControl.SuspendLayout();
            this.tabPageConnection.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxConnection)).BeginInit();
            this.groupBoxConnection.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxExAddress.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonExAccept
            // 
            this.buttonExAccept.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonExAccept.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExAccept.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonExAccept.Appearance.Options.UseFont = true;
            this.buttonExAccept.Appearance.Options.UseForeColor = true;
            this.buttonExAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonExAccept.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonExAccept.Location = new System.Drawing.Point(238, 107);
            this.buttonExAccept.Name = "buttonExAccept";
            this.buttonExAccept.Size = new System.Drawing.Size(75, 23);
            this.buttonExAccept.TabIndex = 1;
            this.buttonExAccept.Text = "Accept";
            this.buttonExAccept.Click += new System.EventHandler(this.buttonExAccept_Click);
            // 
            // buttonExCancel
            // 
            this.buttonExCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonExCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExCancel.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonExCancel.Appearance.Options.UseFont = true;
            this.buttonExCancel.Appearance.Options.UseForeColor = true;
            this.buttonExCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonExCancel.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonExCancel.Location = new System.Drawing.Point(319, 107);
            this.buttonExCancel.Name = "buttonExCancel";
            this.buttonExCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonExCancel.TabIndex = 2;
            this.buttonExCancel.Text = "Cancelar";
            // 
            // tabControl
            // 
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControl.Location = new System.Drawing.Point(5, 5);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedTabPage = this.tabPageConnection;
            this.tabControl.Size = new System.Drawing.Size(392, 95);
            this.tabControl.TabIndex = 0;
            this.tabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabPageConnection});
            this.tabControl.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tabControl_KeyDown);
            this.tabControl.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tabControl_KeyUp);
            // 
            // tabPageConnection
            // 
            this.tabPageConnection.Controls.Add(this.groupBoxConnection);
            this.tabPageConnection.Name = "tabPageConnection";
            this.tabPageConnection.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageConnection.Size = new System.Drawing.Size(387, 70);
            this.tabPageConnection.Text = "Conexion";
            // 
            // groupBoxConnection
            // 
            this.groupBoxConnection.Controls.Add(this.labelExAddress);
            this.groupBoxConnection.Controls.Add(this.textBoxExAddress);
            this.groupBoxConnection.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxConnection.Location = new System.Drawing.Point(3, 3);
            this.groupBoxConnection.Name = "groupBoxConnection";
            this.groupBoxConnection.Size = new System.Drawing.Size(381, 64);
            this.groupBoxConnection.TabIndex = 2;
            this.groupBoxConnection.Text = "Server";
            // 
            // labelExAddress
            // 
            this.labelExAddress.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExAddress.Location = new System.Drawing.Point(51, 35);
            this.labelExAddress.Name = "labelExAddress";
            this.labelExAddress.Size = new System.Drawing.Size(41, 13);
            this.labelExAddress.TabIndex = 0;
            this.labelExAddress.Text = "Address:";
            // 
            // textBoxExAddress
            // 
            this.textBoxExAddress.Location = new System.Drawing.Point(96, 32);
            this.textBoxExAddress.Name = "textBoxExAddress";
            this.textBoxExAddress.Size = new System.Drawing.Size(232, 20);
            this.textBoxExAddress.TabIndex = 0;
            // 
            // ConnectionClientConfigurationForm
            // 
            this.AcceptButton = this.buttonExAccept;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonExCancel;
            this.ClientSize = new System.Drawing.Size(402, 139);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.buttonExCancel);
            this.Controls.Add(this.buttonExAccept);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.LookAndFeel.SkinName = "Blue";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConnectionClientConfigurationForm";
            this.Padding = new System.Windows.Forms.Padding(5);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Setting Client Connections";
            this.Load += new System.EventHandler(this.ConnectionClientConfigurationForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.tabPageConnection.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxConnection)).EndInit();
            this.groupBoxConnection.ResumeLayout(false);
            this.groupBoxConnection.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxExAddress.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private SimpleButtonEx buttonExAccept;
        private SimpleButtonEx buttonExCancel;
        private DevExpress.XtraTab.XtraTabControl tabControl;
        private DevExpress.XtraTab.XtraTabPage tabPageConnection;
        private DevExpress.XtraEditors.LabelControl labelExAddress;
        private DevExpress.XtraEditors.TextEdit textBoxExAddress;
        private DevExpress.XtraEditors.GroupControl groupBoxConnection;
        //private DbConnectionControl dbConnectionControl;
    }
}