using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using SmartCadCore.Core;
using SmartCadCore.Common;

namespace SmartCadClientConfiguration.Gui
{
    public partial class ConnectionClientConfigurationForm : DevExpress.XtraEditors.XtraForm
    {
        public ConnectionClientConfigurationForm()
        {
            InitializeComponent();
        }

        private void ConnectionClientConfigurationForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.textBoxExAddress.Text = SmartCadConfiguration.SmartCadSection.ClientElement.ServerAddressElement.IP;
            }
            catch (Exception ex)
            {
                MessageForm.Show(ex.ToString(), MessageFormType.Error);
            }

            LoadLanguage();
        }

        private void LoadLanguage()
        {
            buttonExAccept.Text = ResourceLoader.GetString2("$Message.Accept");
            buttonExCancel.Text = ResourceLoader.GetString2("$Message.Cancel");
            tabPageConnection.Text = ResourceLoader.GetString2("Connection");
        }

        private void buttonExAccept_Click(object sender, EventArgs e)
        {
            SmartCadConfiguration.SmartCadSection.ClientElement.ServerAddressElement.IP = this.textBoxExAddress.Text.Trim();
            SmartCadConfiguration.Save();
            MessageForm.Show(ResourceLoader.GetString2("ChangesSavedSuccessfully"), MessageFormType.Information);
            DialogResult = DialogResult.OK;
        }

        private void tabControl_KeyDown(object sender, KeyEventArgs e)
        {
            EvalEnableOk();
        }

        private void tabControl_KeyUp(object sender, KeyEventArgs e)
        {
            EvalEnableOk();
        }

        private void EvalEnableOk()
        {
            bool enabled = textBoxExAddress.Text.Length != 0;
            buttonExAccept.Enabled = enabled;
        }
    }
}