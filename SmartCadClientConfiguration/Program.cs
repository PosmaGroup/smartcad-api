using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;

using SmartCadCore.Core;
using System.Security.Principal;
using System.Security.Permissions;
using SmartCadCore.Common;
using System.Globalization;

using SmartCadClientConfiguration.Gui;
using SmartCadGuiCommon;
using SmartCadGuiCommon.Util;

namespace SmartCadClientConfiguration
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                ApplicationUtil.ActivateProcess(Process.GetCurrentProcess());
                ApplicationGuiUtil.CheckErrorDetailButton();
                AppDomain.CurrentDomain.SetPrincipalPolicy(PrincipalPolicy.WindowsPrincipal);
                WindowsPrincipal myPrincipal = (WindowsPrincipal)Thread.CurrentPrincipal;


                SmartCadConfiguration.Load();

                ResourceLoader.UpdatedLanguage(new CultureInfo(
                    SmartCadConfiguration.SmartCadSection.CultureElement.CultureLanguageElement.Name 
                    + "-" +
                  SmartCadConfiguration.SmartCadSection.CultureElement.CultureCountryElement.Name));

                
                SplashForm.ShowSplash();

                Thread.Sleep(TimeSpan.FromSeconds(2));

                SplashForm.CloseSplash();

                if (myPrincipal.IsInRole(WindowsBuiltInRole.Administrator) == true)
                {
                    ConnectionClientConfigurationForm form = new ConnectionClientConfigurationForm();
                    form.ShowDialog();
                }
                else
                {
                    MessageForm.Show(ResourceLoader.GetString2("MustHaveAdministratorAccess"), MessageFormType.Error);
                }

            }
            catch (System.Security.SecurityException se)
            {
                MessageForm.Show(ResourceLoader.GetString2("MustHaveAdministratorAccess"), se);
            }
            catch (Exception ex)
            {
                SplashForm.CloseSplash();

                MessageForm.Show(ResourceLoader.GetString2("GenericErrorApplication"), ex);
            }
        }
    }
}