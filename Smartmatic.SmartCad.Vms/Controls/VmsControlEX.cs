﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.ServiceModel;
using System.Threading;
using System.Collections;
using DevExpress.Utils;
using System.Reflection;
using SmartCadCore.Common;
using VideoOS.Platform.Client;

namespace Smartmatic.SmartCad.Vms
{
    public enum PlayModes
    {
        Live,
        PlayBack_MultiSequences,
        PlayBack_SingleSequence,
        PlayBack_RepeatSequence
    }

    public partial class VmsControlEx : UserControl
    {

        #region Fields
        private static Type VmsType = null;

        private System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VmsControlEx));

        #region local enum

        /// <summary>
        /// Enum for the PTZ movement
        /// </summary>
        public enum PtzMoveRelativeTypes
        {
            Up = 0,
            UpRight = 1,
            Right = 2,
            DownRight = 3,
            Down = 4,
            DownLeft = 5,
            Left = 6,
            UpLeft = 7,
            Home = 8,
            ZoomIn = 9,
            ZoomOut = 10,
        }

        #endregion
        #endregion

        #region Properties

        public bool PTZ { get; set; }
        public bool audio { get; set; }
        public bool speaker { get; set; }
        public IList inputs { get; set; }
        public IList outputs { get; set; }
        public bool feed { get; set; }
        public DateTime PlaybackDate { get; set; }
        public DateTime EndPlayBackDate { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public string PlaybackCode { get; set; }

        #endregion

        #region Static Methods

        /// <summary>
        /// This create a new instance of the control with the current dll
        /// </summary>
        /// <param name="assemblyName">dll name</param>
        /// <returns>the control fully loaded</returns>
        public static VmsControlEx GetInstance(string assemblyName)
        {
            try
            {
                if (VmsType == null)
                {

                    Assembly assembly = Assembly.Load(assemblyName);

                    Type[] types = assembly.GetTypes();

                    foreach (Type type in types)
                    {
                        if (type.IsClass == true
                            && type.IsAbstract == false
                            && type.IsSubclassOf(typeof(VmsControlEx)))
                        {
                            VmsType = type;
                            break;
                        }
                    }
                }
                return (VmsControlEx)VmsType.GetConstructor(new Type[0] { }).Invoke(new object[0] { });
            }
            catch (Exception e)
            {
                SmartLogger.Print(e);
                return null;
            }
        }

        #endregion

        #region events
        public delegate void ChangeInput(object sender, IOArgs args);

        public delegate void ChangeOutput(object sender, IOArgs args);

        public delegate void DateTimeValueChangedEventHandler(DateTime dateTime);

        public virtual event ChangeInput CInput;

        public virtual event ChangeOutput COutput;

        public virtual event DateTimeValueChangedEventHandler DateTimeValueChanged;
        #endregion

        #region Constructors
        public VmsControlEx()
        {
            InitializeComponent();
        }

        #endregion

        private void simpleButtonOutput_Click(object sender, EventArgs e)
        {

        }

        private void timerplayback_Tick(object sender, EventArgs e)
        {

        }

        public void SAudio(bool play)
        {

        }

        #region virtual methods

        /// <summary>
        /// Continuos movement of the Camera
        /// </summary>
        /// <param name="X">Axis X</param>
        /// <param name="Y">Axis Y</param>
        /// <param name="Z">Axis Z</param>
        public virtual void PTZMove(int X, int Y, int Z)
        {
            // throw new NotImplementedException();
        }

        /// <summary>
        /// Relative movement of the camera
        /// </summary>
        /// <param name="dir">direction fo the movement</param>
        public virtual void Ptz_do(PtzMoveRelativeTypes dir)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Mute the audio in the video
        /// </summary>
        /// <param name="mute">bool, true mute and false unmute</param>
        public virtual void mute(bool mute)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Ajust the volume in the control
        /// </summary>
        /// <param name="volume">value for the volume</param>
        public virtual void volume(int volume)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Connect to the camera
        /// </summary>
        ///<param name="volume">value for the volume</param>
        /// <returns>0 Not connected, 1 Connected, 2 Error</returns>
        public virtual int Connect(int multiple)
        {
            throw new NotImplementedException();
        }

        public virtual ImageViewerControl instanceCameraPanelControl()
        {
            throw new NotImplementedException();
        }

        public virtual void ReleaseConnectionsTemplate()
        {
            throw new NotImplementedException();
        }

        public virtual bool ReleaseConnectionExport()
        {
            throw new NotImplementedException();
        }


        public virtual bool VerifyConnectionExport()
        {
            throw new NotImplementedException();
        }

        public virtual int TotalExported()
        {
            throw new NotImplementedException();
        }



        /// <summary>
        /// Connect to a specific camera
        /// </summary>
        /// <returns>0 Not connected, 1 Connected, 2 Error</returns>
        public virtual int Connect(string cameraName)
        {
            throw new NotImplementedException();
        }
    
        /// <summary>
        /// Disconnect
        /// </summary>
        public virtual void Disconnect()
        {
            //throw new NotImplementedException();
        }

        /// <summary>
        /// Initialize the control
        /// </summary>
        /// <param name="cameraName">Represent the camera name</param>
        /// <param name="login">User Login</param>
        /// <param name="password">User Password</param>
        /// <param name="serverIp">Ip for the VMS server</param>
        public virtual void Initialize(string cameraName, string login, string password, string serverIp)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Set template
        /// </summary>
        /// <param name="template">Represent the camera name</param>

        public virtual void SetTemplate(int template)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This allow the operator speak with the user
        /// </summary>
        /// <param name="speak">true speaking, false quiet</param>
        public virtual void Speaking(bool speak)
        {
            throw new NotImplementedException();
        }
        
        /// <summary>
        /// Trigger the selected Output
        /// </summary>
        /// <param name="status">True Activate, False Deactivate</param>
        /// <param name="OutputName">Name of the output</param>
        public virtual void TriggerOutput(bool status,string OutputName)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Change the play mode for the current camera
        /// </summary>
        /// <param name="mode">Play mode</param>
        public virtual void Playmode(PlayModes mode)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get the volume of the currente camera
        /// </summary>
        /// <returns>Volume value</returns>
        public virtual int GetVolume()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get id the current camera is muted
        /// </summary>
        /// <returns>Mute value</returns>
        public virtual bool GetMute()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get the current play mode for teh camera
        /// </summary>
        /// <returns>True Live, False Playback</returns>
        public virtual bool GetPlaymode()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Set the direction of the playback
        /// </summary>
        /// <param name="direction">0 forward, 1 backward</param>
        public virtual void SetPlayDirection(int direction)
        {
            throw new NotImplementedException();
        }

        public virtual void StartPlayBack()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get the current direction of the playback
        /// </summary>
        /// <returns>0 forward, 1 backward</returns>
        public virtual int GetPlayDirection()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Stop the live video 
        /// </summary>
        public virtual void StopLive()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Stop the playback
        /// </summary>
        public virtual void StopPlayback()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Change the current speed of the playback
        /// </summary>
        /// <param name="speed">value for the speed</param>
        public virtual void SetSpeedPlayback(double speed)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get the current speed of the playback
        /// </summary>
        /// <returns></returns>
        public virtual double GetSpeedPlayback()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Set the playback time
        /// </summary>
        /// <param name="PlaybackDAte">start time of the playback</param>
        public virtual void SetPlaybackDate(DateTime date)
        {
            throw new NotImplementedException();
        }

       
        public virtual void SetPlaybackEndDate(DateTime date)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get the current date of the playback
        /// </summary>
        /// <returns></returns>
        public virtual DateTime GetPlaybackDate()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Connect to the server to import the video of the current camera
        /// </summary>
        /// <param name="startDate">Start date for the export</param>
        /// <param name="endDate">End date for the export</param>
        /// <param name="path">Path to storage the video</param>
        /// <param name="format">0 AVI, 1 JPEG</param>
        /// <param name="timestamp">0 with Timestamp, 1 without</param>
        /// <returns>0 Not connected, 1 Connected</returns>
        public virtual int ExportVideo(DateTime startDate, DateTime endDate, string path,
            int format, int timestamp)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get the current camera name
        /// </summary>
        /// <returns>String with the camera name</returns>
        public virtual string GetCameraName()
        {
            throw new NotImplementedException();
        }

        public virtual void clearVms()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Stop the playback
        /// </summary>
        public virtual void InitStopPlayback()
        {
            throw new NotImplementedException();
        }
        #endregion

    }
    public class IOData
    {
        private String name;
        private int status;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int Status
        {
            get { return status; }
            set { status = value; }
        }
    }

    public class IOArgs : System.EventArgs
    {
        private IOData IoData;

        public IOArgs(IOData Data)
        {
            this.IoData = Data;        
        }

        public IOData IData
        {
            get { return IoData; }
            set { IoData = value; }
        }
    }

}

