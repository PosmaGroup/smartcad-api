<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:dt="http://xsltsl.org/date-time">
  <xsl:import href="xsltsl/stdlib.xsl"/>
  <xsl:output method="html"/>
  <!-- Global Parameters and Variables -->
  <!-- date format-->
  <xsl:param name="format" select="'%d/%m/%Y %H:%M:%S'"/>
  <!-- Section footer-->
  <xsl:variable name="section-footer">
    <tr>
      <td>&#160;</td>
      <td>
        <br/>
        <a class="enabled-link" onclick="goToObject('top');">Volver arriba</a>
        <br/>
        <br/>
      </td>
      <td>&#160;</td>
      <td>&#160;</td>
    </tr>
  </xsl:variable>

  <!-- BEGIN Base document-->
  <xsl:template match="/">
    <html>
      <head>
        <!-- 
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        -->
        <LINK REL="StyleSheet" HREF="style.css" TYPE="text/css"/>
        <script type="text/javascript" src="script.js" language="javascript"/>
        <title>SmartCAD</title>
      </head>
      <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <xsl:apply-templates/>
        </table>
      </body>
    </html>
  </xsl:template>
  <!-- END Base document-->

  <!-- BEGIN Incident-->
  <xsl:template match="incident">
    <tr>
      <div id="top"></div>
    </tr>
    <tr>
      <td width="5"/>
      <td width="875">
        <span class="header1">
          Incidente # <xsl:value-of select="@custom-code"/>
        </span>
        <a name="up" id="up"/>
        <br/>
        <xsl:choose>
          <xsl:when test="//phone-report">
            <a class="enabled-link" onclick="goToObject('phone-reports');">Llamadas</a>
            <br/>
          </xsl:when>
          <xsl:otherwise>
            <a class="disabled-link">No hay llamadas registradas</a>
            <br/>
          </xsl:otherwise>
        </xsl:choose>
        <xsl:choose>
          <xsl:when test="//incident-notification">
            <a class="enabled-link" onclick="goToObject('dispatch-orders');">Solicitudes de despacho</a>
            <br/>
          </xsl:when>
          <xsl:otherwise>
            <a class="disabled-link">No hay solicitudes de despacho registradas</a>
            <br/>
          </xsl:otherwise>
        </xsl:choose>
        <xsl:choose>
          <xsl:when test="//support-requests">
            <a class="enabled-link" onclick="goToObject('support');">Solicitudes de Apoyo</a>
            <br/>
          </xsl:when>
          <xsl:otherwise>
            <a class="disabled-link">No hay solicitudes de apoyo registradas</a>
            <br/>
          </xsl:otherwise>
        </xsl:choose>
        <xsl:choose>
          <xsl:when test="//incident-notes">
            <a class="enabled-link" onclick="goToObject('inc-notes');">Notas del incidente</a>
            <br/>
          </xsl:when>
          <xsl:otherwise>
            <a class="disabled-link">No hay notas del incidente registradas</a>
            <br/>
          </xsl:otherwise>
        </xsl:choose>
        <xsl:choose>
          <xsl:when test="//summary">
            <a class="enabled-link" onclick="goToObject('summary');">Resumen del incidente</a>
            <br/>
          </xsl:when>
          <xsl:otherwise>
            <a class="disabled-link">No hay resumen del incidente registrado</a>
            <br/>
          </xsl:otherwise>
        </xsl:choose>
        <br/>
        <br/>
      </td>
      <td>&#160;</td>
      <td width="5">&#160;</td>
    </tr>
    <xsl:apply-templates/>
  </xsl:template>
  <!-- END Incident-->

  <!-- BEGIN Phone reports-->
  <xsl:template match="phone-reports">
    <tr>
      <div id="phone-reports"/>
    </tr>
    <xsl:for-each select="phone-report">
      <tr bgcolor="C6D3D9">
        <td width="5" />
        <td>
          <span class="header1">
            Llamada # <xsl:number value="position()"/>
          </span>
          &#160;<xsl:call-template name="dt:format-date-time">
            <xsl:with-param name="xsd-date-time" select="@call-start"/>
            <xsl:with-param name="format" select="$format"/>
          </xsl:call-template>
        </td>
        <xsl:choose>
          <xsl:when test="@complete='0'">
            <td width="143">
              <span class="status-complete">Llamada completa</span>
            </td>
          </xsl:when>
          <xsl:otherwise>
            <td width="143">
              <span class="status-incomplete">Llamada inconclusa</span>
            </td>
          </xsl:otherwise>
        </xsl:choose>
        <td width="5">&#160;</td>
      </tr>
      <tr bgcolor="E4EAED">
        <td width="5">&#160;</td>
        <td>
          <br/>
          <span class="prop-name">Atendida por:</span>&#160;<xsl:value-of select="@attended-by"/><br/>
          <xsl:for-each select="caller">
            <span class="prop-name">Persona que llama:&#160;</span>
            <xsl:choose>
              <xsl:when test="string-length(@name)>0">
                <xsl:value-of select="@name"/>
              </xsl:when>
              <xsl:otherwise>Anónima</xsl:otherwise>
            </xsl:choose>
            <br/>
            <span class="prop-name">Teléfono</span>:&#160;<xsl:value-of select="@phone"/>
            <xsl:if test="string-length(@additional-phone)>0">
              /&#160;<xsl:value-of select="@additional-phone"/>
            </xsl:if>
            <br/>
          </xsl:for-each>
          <xsl:for-each select="address">
            <span class="prop-name">Dirección de la persona que llama:</span>
            <br/>
            <xsl:if test ="string-length(@zone)>0">
              Urb/Sector:&#160;<xsl:value-of select="@zone"/>.
              <br/>
            </xsl:if>
            <xsl:if test ="string-length(@street)>0">
              Cl/Av/Esq:&#160;<xsl:value-of select="@street"/>.
              <br/>
            </xsl:if>
            <xsl:if test ="string-length(@reference)>0">
              Casa/Edif:&#160;<xsl:value-of select="@reference"/>.
              <br/>
            </xsl:if>
            <xsl:if test ="string-length(@more)>0">
              Piso/Apto:&#160;<xsl:value-of select="@more"/>.
              <br/>
            </xsl:if>
          </xsl:for-each>          <span class="prop-name">Tipos de incidentes:</span>
          <xsl:for-each select="incident-types/incident-type">
            &#160;
            <xsl:variable name="incident-type-string">
              <xsl:value-of select="@friendly-name"/>&#160;(<xsl:value-of select="@custom-code"/>)
            </xsl:variable>
            <xsl:copy-of select ="normalize-space($incident-type-string)"/>
            <xsl:choose>
              <xsl:when test="position()=last()">.</xsl:when>
              <xsl:otherwise>,</xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
          <br/>
          <br/>
        </td>
        <td width="143">&#160;</td>
        <td width="5">&#160;</td>
      </tr>
      <tr>
        <td width="5">&#160;</td>
        <td>
          <br/>
          <span class="header2">Preguntas</span>
          <br/>
          <br/>
          <xsl:for-each select="question-set/question">
            <span class="prop-name">
              <xsl:value-of select="q"/>
            </span>
            <br/>
            <xsl:for-each select="a">
              <xsl:value-of select="@text"/>
              <xsl:if test="position()!=last()">,&#160;</xsl:if>
            </xsl:for-each>
            <br/>
          </xsl:for-each>
          <br/>
        </td>
        <td width="143">&#160;</td>
        <td width="5">&#160;</td>
      </tr>
      <tr bgcolor="E4EAED">
        <td width="5">&#160;</td>
        <td>
          <strong>
            <br/>
            <span class="header2">Solicitudes de despacho:</span>
            <br/>
            <br/>
          </strong>
          <xsl:for-each select="notifications/notification">
            &#160;<xsl:value-of
    select="@department-type-name"/>&#160;(<xsl:value-of
                select="@priority-custom-code"/>)<br/>
          </xsl:for-each>
          <br/>
        </td>
        <td width="143">&#160;</td>
        <td width="5">&#160;</td>
      </tr>
      <tr>
        <td>
          <br/>
        </td>
      </tr>
    </xsl:for-each>
    <xsl:copy-of select="$section-footer"/>
  </xsl:template>
  <!-- END Phone reports -->

  <!-- BEGIN Incident notifications-->
  <xsl:template match="incident-notifications">
    <tr>
      <div id="dispatch-orders"/>
    </tr>
    <xsl:for-each select="incident-notification">
      <tr bgcolor="C6D3D9">
        <td>&#160;</td>
        <td>
          <span class="header1">
            Solicitud de despacho # <xsl:number value="position()"/>
          </span>
        </td>
        <td>&#160;</td>
        <td>&#160;</td>
      </tr>
      <tr bgcolor="E4EAED">
        <td>&#160;</td>
        <td>
          <br/>
          <span class="prop-name">
            <xsl:value-of select="@department-type-name"/>
          </span>
          &#160;(<xsl:value-of select="@priority-custom-code"/>)&#160; <xsl:call-template name="dt:format-date-time">
            <xsl:with-param name="xsd-date-time" select="@creation-date"/>
            <xsl:with-param name="format" select="$format"/>
          </xsl:call-template>
          <br/>
          <xsl:for-each select="real-status">
           	<span class="prop-name">Estatus (actual):</span>&#160;<xsl:value-of select="@friendly-name"/>&#160;
            &#160; <xsl:call-template name="dt:format-date-time">
                <xsl:with-param name="xsd-date-time" select="@start"/>
                <xsl:with-param name="format" select="$format"/>               
              </xsl:call-template>&#160;
          <xsl:if test="@custom_code='CANCELLED'">
              <br/>
              <span class="prop-name">Motivo de cancelación:</span>&#160;
              <xsl:value-of select="@status_detail"/>
              <br/>
            </xsl:if>
         </xsl:for-each>
          <xsl:choose>
            <xsl:when test="status-set/status[2]">
              <a onclick="switchMenu('exp{position()}');" title="Mostrar historia">
                <span class="enabled-link">(Histórico)</span>
              </a><br/>
              <div style="display:none" id="exp{position()}">
                <br/>

                <table width="60%" border="0" cellspacing="0" cellpadding="0">
                  <tr bgcolor="C6D3D9">
                    <td width="20"/>
                    <td class="prop-name">Estatus</td>
					<td class="prop-name">Operador</td>
                    <td class="prop-name">Fecha</td>
                  </tr>
                  <xsl:for-each select="status-set/status">
                    <xsl:if test="position()!=last()">
                      <tr>
                        <xsl:attribute name="class">
                          <xsl:if test="position() mod 2">even-sub-row</xsl:if>
                        </xsl:attribute>
                        <td/>
                        <td>
                          &#160;<xsl:value-of select="@status"/>&#160;
                        </td>
						<td>
                          &#160;<xsl:value-of select="@complete-name"/>&#160;
                        </td>
                        <td>
                          &#160; <xsl:call-template name="dt:format-date-time">
                            <xsl:with-param name="xsd-date-time" select="@start-date"/>
                            <xsl:with-param name="format" select="$format"/>
                          </xsl:call-template>
                        </td>
                      </tr>
                    </xsl:if>
                  </xsl:for-each>
                </table>
                <br/>
              </div>
            </xsl:when>
            <xsl:otherwise>
              <br/>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:for-each select="units">
            <span class="prop-name">Cantidad de unidades móviles asignadas:</span>&#160;<xsl:value-of select="@total-units"/>
            <br/>
          </xsl:for-each>
          <br/>
        </td>
        <td>&#160;</td>
        <td>&#160;</td>
      </tr>
      <xsl:for-each select="units/dispatch-order">
        <tr>
          <td>&#160;</td>
          <td>
            <br/>
            <span class="header2">
              Unidad móvil # <xsl:number value="position()"/>
            </span>
            <br/>
            <br/>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="30">&#160;</td>
                <td>
                  <span class="prop-name">
                    <xsl:value-of select="@unit"/><br/> Despacho:
                  </span>&#160; <xsl:call-template
  name="dt:format-date-time">
                    <xsl:with-param name="xsd-date-time" select="@start-date"/>
                    <xsl:with-param name="format" select="$format"/>
                  </xsl:call-template>
                  <span class="prop-name">
                    <br/> Llegada:
                  </span>&#160; <xsl:call-template
  name="dt:format-date-time">
                    <xsl:with-param name="xsd-date-time" select="@arrival-date"/>
                    <xsl:with-param name="format" select="$format"/>
                  </xsl:call-template>
                  <span class="prop-name">
                    <br/> Finalización:
                  </span>
                  <xsl:if test="@end-date">
                    &#160; <xsl:call-template name="dt:format-date-time">
                      <xsl:with-param name="xsd-date-time" select="@end-date"/>
                      <xsl:with-param name="format" select="$format"/>
                    </xsl:call-template>
                  </xsl:if>
                  <span class="prop-name">
                    <br/> Oficiales:
                  </span>
                  <xsl:for-each select="officers/officer">
                    &#160;<xsl:value-of select="@badge"
                  />&#160;<xsl:value-of select="@complete-name"/>.
                  </xsl:for-each>
                  <xsl:for-each select="notes/note">
                    <br/>
                    <br/>
                    <span class="prop-name">
                      Nota&#160;<xsl:number value="position()"/>
                    </span>
                    &#160; <xsl:call-template name="dt:format-date-time">
                      <xsl:with-param name="xsd-date-time" select="@creation-date"/>
                      <xsl:with-param name="format" select="$format"/>
                    </xsl:call-template>
                    <br/>
                    <span class="prop-name">Operador:</span>&#160;<xsl:value-of select="@operator"/><br/>
                    <span class="prop-name">Detalle:</span>&#160;<xsl:value-of select="@detail"/>
                  </xsl:for-each>
                  <br/><br/>
                </td>
              </tr>
              <xsl:for-each select="ending-report">                
                <xsl:if test="question-set">
                  <tr>
                    <td>&#160;</td>
                    <td>
                      <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="E4EAED">
                        <tr>
                          <td width="30">&#160;</td>
                          <td align="center">
                            <br/>
                            <span class="prop-name">Reporte de finalización</span>
                            <br/>
                            <br/>
                          </td>
                        </tr>
                        <tr>
                          <td width="30">&#160;</td>
                          <td>
                            <span class="prop-name">Oficial a cargo:</span>&#160;<xsl:value-of select="@officerFullName"/><br/><br/>
                          </td>
                        </tr>
                        <tr>
                          <td width="30">&#160;</td>
                          <td>
                            <span class="prop-name">Datos del incidente</span>
                            <br/>
                            <br/>
                          </td>
                        </tr>
                        <tr>
                          <td width="30">&#160;</td>
                          <td>
                            <xsl:for-each select="question-set/question">
                              <span class="prop-name">
                                &#160;&#160;&#160;<xsl:value-of select="@text"/>:&#160;
                              </span>
                              <br/>
                              <xsl:for-each select="answer">
                                &#160;&#160;&#160;<xsl:value-of select="@text"/>
                                <xsl:if test="position()!=last()">,&#160;</xsl:if>
                              </xsl:for-each>
                              <br/>
                            </xsl:for-each>
                            <br/>
                            </td>
                        </tr>
                      </table>
                    </td>
                    <td>&#160;</td>
                    <td>&#160;</td>
                  </tr>
                </xsl:if>             
              </xsl:for-each>              
            </table>
          </td>
        </tr>
      </xsl:for-each>
        <xsl:for-each select="notif-ending-report">
      <tr>        
        <td>&#160;</td>
        <td>
          <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="E4EAED">
            <tr>
              <td width="30">&#160;</td>
              <td align="center">
                <br/>
                  <span class="prop-name">Reporte de finalización</span>
                <br/>
                <br/>
              </td>
            </tr>
            <tr>
              <td width="30">&#160;</td>
              <td>
                <span class="prop-name">Oficial a cargo:</span>&#160;<xsl:value-of select="@officerFullName"/><br/><br/>
              </td>
            </tr>
            <tr>
              <td width="30">&#160;</td>
              <td>
                <span class="prop-name">Datos del incidente</span>
                <br/>
                <br/>
              </td>
            </tr>
            <tr>
              <td width="30">&#160;</td>
              <td>
                <xsl:for-each select="question-set/question">
                  <span class="prop-name">
                    &#160;&#160;&#160;<xsl:value-of select="@text"/>:&#160;
                  </span>
                  <br/>
                  <xsl:for-each select="answer">
                    &#160;&#160;&#160;<xsl:value-of select="@text"/>
                    <xsl:if test="position()!=last()">,&#160;</xsl:if>
                  </xsl:for-each>
                  <br/>
                </xsl:for-each>
                <br/>
              </td>              
            </tr>
          </table>
        </td>       
      </tr>
      <tr>
        <td>&#160;</td>
        <td>&#160;</td>
      </tr>
      </xsl:for-each>
      
      <tr>
        <td>&#160;</td>
        <td>
          <span class="prop-name">
            Realizado por:&#160;<xsl:value-of select="@firstly-assigned"/>
          </span>
          <br/>
          <span class="prop-name">
            Cerrado por:&#160;<xsl:value-of select="@notification-closed-by"/>
          </span>
          <br/>
        </td>
        <td>&#160;</td>
        <td>&#160;</td>
      </tr>
      <xsl:copy-of select="$section-footer"/>
    </xsl:for-each>
  </xsl:template>
  <!--END Incident notifications-->

  <!-- BEGIN Incident notes-->
  <xsl:template match="incident-notes">
    <tr>
      <div id="inc-notes"/>
    </tr>
    <xsl:for-each select="incident-note">
      <tr bgcolor="C6D3D9">
        <td>&#160;</td>
        <td>
          <span class="header1">
            Nota del incidente # <xsl:number value="position()"/>
          </span>
        </td>
        <td>&#160;</td>
        <td>&#160;</td>
      </tr>
      <tr bgcolor="E4EAED">
        <td>&#160;</td>
        <td>
          <span class="prop-name">
            <br/>Nota&#160;
          </span>
          <xsl:call-template name="dt:format-date-time">
            <xsl:with-param name="xsd-date-time" select="@creation-date"/>
            <xsl:with-param name="format" select="$format"/>
          </xsl:call-template><br/>
          <span class="prop-name">Operador:</span>&#160;<xsl:value-of select="@user-login"/><br/>
          <br/>
        </td>
        <td>&#160;</td>
        <td>&#160;</td>
      </tr>
      <tr>
        <td>&#160;</td>
        <td>
          <br/>
          <span class="prop-name">Detalle:</span>&#160;<xsl:value-of select="@detail"/>
          <br/>
          <br/>
        </td>
        <td>&#160;</td>
        <td>&#160;</td>
      </tr>
    </xsl:for-each>
    <xsl:copy-of select="$section-footer"/>
  </xsl:template>
  <!-- END Incident notes-->

  <!-- BEGIN Support requests-->
  <xsl:template match="support-requests">
    <tr>
      <div id="support"/>
    </tr>
    <xsl:for-each select="support-request">
      <tr bgcolor="C6D3D9">
        <td>&#160;</td>
        <td>
          <span class="header1">
            Solicitud de apoyo # <xsl:number value="position()"/>
          </span>
        </td>
        <td>&#160;</td>
        <td>&#160;</td>
      </tr>
      <tr bgcolor="E4EAED">
        <td>&#160;</td>
        <td>
          <br/>
          <span class="prop-name">Solicitud de apoyo&#160;</span>
          <xsl:call-template name="dt:format-date-time">
            <xsl:with-param name="xsd-date-time" select="@creation-date"/>
            <xsl:with-param name="format" select="$format"/>
          </xsl:call-template>
          <br/>
          <span class="prop-name">Operador:</span>&#160;<xsl:value-of select="@attended-by"/>
          <br/>
          <xsl:for-each select="unit-petitioneer">
            <span class="prop-name">Requerido por:&#160;</span>
            <xsl:value-of select="@department"/>&#160;-&#160;
            <xsl:value-of select="@custom-code"/>&#160;
            <xsl:value-of select="@unit-type"/>
          </xsl:for-each>
          <br/>
          <span class="prop-name">Organismo(s) solicitado(s):&#160;</span>
          <br/>
          <xsl:for-each select="incident-notifications/incident-notification">
            &#160;<xsl:value-of select="@department-type-name"/>
            &#160;(<xsl:value-of select="@priority"/>)<br/>
          </xsl:for-each>
        </td>
        <td>&#160;</td>
        <td>&#160;</td>
      </tr>
      <tr>
        <td>&#160;</td>
        <td>
          <br/>
          <span class="prop-name">Observaciones:</span>:&#160;<xsl:value-of select="@details"/>
          <br/>
          <br/>
        </td>
        <td>&#160;</td>
        <td>&#160;</td>
      </tr>
    </xsl:for-each>
    <xsl:copy-of select="$section-footer"/>
  </xsl:template>
  <!-- END Support requests-->

  <!-- BEGIN Summary-->
  <xsl:template match="summary">
    <tr>
      <div id="summary"/>
    </tr>
    <tr bgcolor="C6D3D9">
      <td>&#160;</td>
      <td>
        <span class="header1">Resumen del incidente</span>
      </td>
      <td>&#160;</td>
      <td>&#160;</td>
    </tr>
    <tr bgcolor="E4EAED">
      <td>&#160;</td>
      <td>
        <br/>
        <xsl:for-each select="attention-time">
          <xsl:choose>
            <xsl:when test="@value=-1">
              El incidente no ha sido cerrado.
            </xsl:when>
            <xsl:otherwise>
              <span class="prop-name">Tiempo de atención:</span>&#160;
              <xsl:value-of select="@value"/>&#160;minuto(s).
            </xsl:otherwise>
          </xsl:choose>
        </xsl:for-each>
        <br/>
        <span class="prop-name">Organismos involucrados:</span>
        <xsl:for-each select="involved-departments/department">
          &#160;<xsl:value-of select="@name"/>
          <xsl:choose>
            <xsl:when test="position()=last()">.</xsl:when>
            <xsl:otherwise>,</xsl:otherwise>
          </xsl:choose>
        </xsl:for-each>
        <br/>
        <span class="prop-name">Cantidad de unidades móviles asignadas:</span>
        &#160;<xsl:value-of select="total-assigned-units"/>
        <br/>
        <span class="prop-name">Tipos de unidades móviles:</span>
        <xsl:for-each select="unit-types/unit-type">
          &#160;<xsl:value-of select="@name"/>
          <xsl:choose>
            <xsl:when test="position()=last()">.</xsl:when>
            <xsl:otherwise>,</xsl:otherwise>
          </xsl:choose>
        </xsl:for-each>
        <br/>
      </td>
      <td>&#160;</td>
      <td>&#160;</td>
    </tr>
    <xsl:copy-of select="$section-footer"/>
    <tr>
      <td>&#160;</td>
      <td>&#160;</td>
      <td>&#160;</td>
    </tr>
  </xsl:template>
  <!-- END Summary-->

</xsl:stylesheet>
