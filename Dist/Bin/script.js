function switchMenu(obj) {
	var el = document.getElementById(obj);
	if ( el.style.display != "none" ) {
		el.style.display = 'none';
	}
	else {
		el.style.display = '';
	}
}

function goToObject(obj) {
	var el = document.getElementById(obj).scrollIntoView();
}