IF NOT EXISTS(select* from sysobjects where name= 'history')
BEGIN
	CREATE TABLE [dbo].[history](
		[id] [varchar](250) NOT NULL,
		[type] [varchar](250) NOT NULL,
		[date] [datetime] NULL,
		[frame] [varchar](1200)
	) ON [PRIMARY]
END

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[history]') AND name = N'IX_HISTORY')
CREATE NONCLUSTERED INDEX [IX_HISTORY] ON [dbo].[history] 
(
	[id] ASC,
	[type] ASC,
	[date] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
