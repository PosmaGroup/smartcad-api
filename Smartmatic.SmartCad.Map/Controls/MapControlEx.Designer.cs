﻿
using SmartCadCore.Common;
namespace Smartmatic.SmartCad.Map
{
    partial class MapControlEx
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.zoomTrackBarControl1 = new DevExpress.XtraEditors.ZoomTrackBarControl();
            this.ruler = new Ruler();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.zoomTrackBarControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zoomTrackBarControl1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // zoomTrackBarControl1
            // 
            this.zoomTrackBarControl1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.zoomTrackBarControl1.EditValue = -5;
            this.zoomTrackBarControl1.Location = new System.Drawing.Point(3, 3);
            this.zoomTrackBarControl1.Name = "zoomTrackBarControl1";
            this.zoomTrackBarControl1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.zoomTrackBarControl1.Properties.Appearance.Options.UseBackColor = true;
            this.zoomTrackBarControl1.Properties.LookAndFeel.SkinName = "The Asphalt World";
            this.zoomTrackBarControl1.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.zoomTrackBarControl1.Properties.Maximum = 5;
            this.zoomTrackBarControl1.Properties.Minimum = -5;
            this.zoomTrackBarControl1.Properties.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.zoomTrackBarControl1.Properties.ScrollThumbStyle = DevExpress.XtraEditors.Repository.ScrollThumbStyle.Bar;
            this.zoomTrackBarControl1.Properties.UseParentBackground = true;
            this.zoomTrackBarControl1.Size = new System.Drawing.Size(23, 236);
            this.zoomTrackBarControl1.TabIndex = 5;
            this.zoomTrackBarControl1.Value = -5;
            // 
            // ruler
            // 
            this.ruler.Appearance.BackColor = System.Drawing.Color.White;
            this.ruler.Appearance.BackColor2 = System.Drawing.Color.White;
            this.ruler.Appearance.Options.UseBackColor = true;
            this.ruler.AutoSize = true;
            this.ruler.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ruler.Location = new System.Drawing.Point(0, 608);
            this.ruler.LookAndFeel.UseDefaultLookAndFeel = false;
            this.ruler.Name = "ruler";
            this.ruler.Size = new System.Drawing.Size(642, 25);
            this.ruler.TabIndex = 7;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Location = new System.Drawing.Point(32, 16);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(598, 560);
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            // 
            // MapControlEx
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.ruler);
            this.Controls.Add(this.zoomTrackBarControl1);
            this.Name = "MapControlEx";
            this.Size = new System.Drawing.Size(642, 633);
            ((System.ComponentModel.ISupportInitialize)(this.zoomTrackBarControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zoomTrackBarControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public DevExpress.XtraEditors.ZoomTrackBarControl zoomTrackBarControl1;
        public Ruler ruler;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}
