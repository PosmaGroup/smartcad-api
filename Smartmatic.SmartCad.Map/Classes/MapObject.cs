﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.Drawing;
using SmartCadCore.Common;
using SmartCadCore.ClientData;


namespace Smartmatic.SmartCad.Map
{
    public class MapObject
    {
        public int Code { get; set; }
        public virtual bool Drawable { get; set; }
        public IDictionary<string, object> Fields { get; set; }

        public MapObject(int code, params IDictionary<string, object>[] args)
        {
            Code = code;
            Fields = (args.Length > 0 ? args[0] : new Dictionary<string, object>());
            Drawable = false;
        }

        // override object.Equals
        public override bool Equals(object obj)
        {
            return (obj != null && GetType() == obj.GetType() && ((MapObject)obj).Code == Code);
        }
    }

    public class MapPolygon : MapObject //poligono abierto
	{
        public override bool Drawable { get { return Geometry.Count > 0;  } }
        public List<GeoPoint> Geometry { get; set; }
        public Color Color { get; set; }
        public int LinePattern = 2;
        public int FillPattern = 17;

        public MapPolygon(int code, Color color)
        :base(code)
        {
            Color = color;
            Geometry = new List<GeoPoint>();
        }
	}

    public class MapPoint: MapObject
	{
        public override bool Drawable { get { return Position != null; } }
        public GeoPoint Position { get; set; }
        public string IconName { get; set; }
        public string ParentName;

        public MapPoint(int code, GeoPoint position, string iconName)
        :base(code)
        {
            Position = position;
            IconName = (iconName ?? "");
        }
	}

    public class MapCurve : MapObject //poligono cerrado
    {
        public override bool Drawable { get { return Geometry.Count > 0; } }
        public List<GeoPoint> Geometry { get; set; }
        public Color Color { get; set; }

        public MapCurve(int code, Color color)
            : base(code)
        {
            Color = color;
            Geometry = new List<GeoPoint>();
        }
    }



    public class MapObjectUnit : MapPoint
    {
        public MapObjectUnit(UnitClientData data)
            : base(data.Code, !data.IsSynchronized ? null : new GeoPoint(data.Lon, data.Lat), data.IconName)
        {
            IDictionary<string, object> args = new Dictionary<string, object>();
            args.Add("CustomCode", data.CustomCode);
            args.Add("DepartmentTypeCode", (data.DepartmentType != null) ? data.DepartmentType.Code : 0);
            args.Add("DepartmentZoneCode", (data.DepartmentStation != null && data.DepartmentStation.DepartmentZone != null) ? data.DepartmentStation.DepartmentZone.Code : 0);
            args.Add("DepartmentStationCode", (data.DepartmentStation != null) ? data.DepartmentStation.Code : 0);
            args.Add("TypeCode", (data.Type != null) ? data.Type.Code : 0);
            args.Add("IncidentCode", (data.DispatchOrder != null) ? data.DispatchOrder.IncidentCode : -1);
            args.Add("DepartmentTypeName", (data.DepartmentType != null) ? data.DepartmentType.Name : string.Empty);
            args.Add("DepartmentZoneName", (data.DepartmentStation != null && data.DepartmentStation.DepartmentZone != null) ? data.DepartmentStation.DepartmentZone.Name : string.Empty);
            args.Add("DepartmentStationName", (data.DepartmentStation != null) ? data.DepartmentStation.Name : string.Empty);
            args.Add("TypeName", (data.Type != null) ? data.Type.Name : string.Empty);
            args.Add("StatusFriendlyName", (data.Status != null) ? data.Status.FriendlyName : string.Empty);
            args.Add("Portable", data.IdRadio);
            args.Add("CoordinatesDate", data.CoordinatesDate.ToString());

            Fields = args;
        }

        /// <summary>
        /// Build an empty object with properties names
        /// </summary>
        public MapObjectUnit()
        :base(0,null,"")
        {
            IDictionary<string, object> args = new Dictionary<string, object>();
            args.Add("CustomCode", string.Empty);
            args.Add("DepartmentTypeCode", 0);
            args.Add("DepartmentZoneCode", 0);
            args.Add("DepartmentStationCode", 0);
            args.Add("TypeCode", 0);
            args.Add("IncidentCode", 0);
            args.Add("DepartmentTypeName", "");
            args.Add("DepartmentZoneName", "");
            args.Add("DepartmentStationName", "");
            args.Add("TypeName", "");
            args.Add("StatusFriendlyName", "");
            args.Add("Portable", "");
            args.Add("CoordinatesDate", "");

            Fields = args;
        }
    }
    
    public class MapObjectIncident : MapPoint 
    {
        public MapObjectIncident(IncidentClientData data)
            : base(data.Code, !data.Address.IsSynchronized ? null : new GeoPoint(data.Address.Lon, data.Address.Lat), data.IconName)
        {
            IDictionary<string, object> args = new Dictionary<string, object>();
            args.Add("CustomCode",data.CustomCode);
            args.Add("IncidentTypeName", (data.IncidentTypes != null && data.IncidentTypes.Count > 1) ? ((IncidentTypeClientData)data.IncidentTypes[0]).Name : string.Empty);
            args.Add("StatusFriendlyName", (data.Status!=null) ? data.Status.FriendlyName : string.Empty);
            args.Add("StartDate",(data.StartDate!=null) ? data.StartDate.ToString() : string.Empty);
            
            Fields = args;
        }

        /// <summary>
        /// Build an empty object with properties names
        /// </summary>
        public MapObjectIncident()
            : base(0, null, "")
        {
            IDictionary<string, object> args = new Dictionary<string, object>();
            args.Add("CustomCode", string.Empty);
            args.Add("IncidentTypeName", string.Empty);
            args.Add("StatusFriendlyName", string.Empty);
            args.Add("StartDate", string.Empty);


            Fields = args;
        }
    }
    
    public class MapObjectStruct : MapPoint
    {
        public MapObjectStruct(StructClientData data)
            : base(data.Code, !data.IsSynchronized ? null : new GeoPoint(data.Lon, data.Lat), "estructuras.png")
        {
            IDictionary<string, object> args = new Dictionary<string, object>();
            args.Add("Name", data.Name);
            if (data.CctvZone != null)
            {
                args.Add("CctvZoneCode", data.CctvZone.Code);
            }
            else
            {
                args.Add("CctvZoneCode", 0);
            }

            Fields = args;
        }

        /// <summary>
        /// Build an empty object with properties names
        /// </summary>
        public MapObjectStruct()
            : base(0, null, "")
        {
            IDictionary<string, object> args = new Dictionary<string, object>();
            args.Add("Name", string.Empty);
            args.Add("CctvZoneCode", 0);

            Fields = args;
        }
    }

    public class MapObjectBusStop : MapPoint
    {
        public MapObjectBusStop(RouteAddressClientData data, int RouteCode, string iconName)
            : base(data.Code, new GeoPoint(data.Lon, data.Lat), iconName)
        {
            IDictionary<string, object> args = new Dictionary<string, object>();
            args.Add("Name", data.Name);
            args.Add("ParentCode", RouteCode);

            Fields = args;
        }

        /// <summary>
        /// Build an empty object with properties names
        /// </summary>
        public MapObjectBusStop()
            : base(0, null, "")
        {
            IDictionary<string, object> args = new Dictionary<string, object>();
            args.Add("Name", string.Empty);
            args.Add("ParentCode", 0);

            Fields = args;
        }
    }
    
    public class MapObjectZone : MapPolygon
    {
        public MapObjectZone(DepartmentZoneClientData data)
        : base(data.Code,data.DepartmentType.Color)
        {
            IDictionary<string, object> args = new Dictionary<string, object>();
            args.Add("Name", data.Name);
            args.Add("DepartmentTypeCode", data.DepartmentType.Code);

            Fields = args;
            Geometry = GetPointsSorted(data).ToList();                 
            //this.Type = MapPolygon.PolygonType.Area;
        }

        /// <summary>
        /// Build an empty object with properties names
        /// </summary>
        public MapObjectZone()
            : base(0,Color.Black)
        {
            IDictionary<string, object> args = new Dictionary<string, object>();
            args.Add("Name", string.Empty);
            args.Add("DepartmentTypeCode", 0);

            Fields = args;
            Geometry = new List<GeoPoint>();
            //this.Type = MapPolygon.PolygonType.Area;
        }
        
        private GeoPoint[] GetPointsSorted(DepartmentZoneClientData dep)
        {
            List<DepartmentZoneAddressClientData> listAddss = dep.DepartmentZoneAddress.Cast<DepartmentZoneAddressClientData>().ToList();

            listAddss.Sort(delegate(DepartmentZoneAddressClientData a, DepartmentZoneAddressClientData b)
            {
                return Int32.Parse(a.PointNumber) - Int32.Parse(b.PointNumber);
            });

            GeoPoint[] points = listAddss.ConvertAll<GeoPoint>(delegate(DepartmentZoneAddressClientData toConv)
            {
                return new GeoPoint(toConv.Lon, toConv.Lat);
            }).ToArray();
            return points;
        }
    }
    
    public class MapObjectStation : MapPolygon
    {
        public MapObjectStation(DepartmentStationClientData data)
            : base(data.Code, data.DepartmentZone.DepartmentType.Color)
        {
            IDictionary<string, object> args = new Dictionary<string, object>();
            args.Add("Name", data.Name);
            args.Add("DepartmentTypeCode", data.DepartamentTypeCode);

            Fields = args;
            Geometry = GetPointsSorted(data).ToList();
            LinePattern = 5;
            FillPattern = 15;
            //this.Type = MapPolygon.PolygonType.Area;
        }

        /// <summary>
        /// Build an empty object with properties names
        /// </summary>
        public MapObjectStation()
            : base(0,Color.Black)
        {
            IDictionary<string, object> args = new Dictionary<string, object>();
            args.Add("Name", string.Empty);
            args.Add("DepartmentTypeCode", 0);

            Fields = args;
            Geometry = new List<GeoPoint>();
            //this.Type = MapPolygon.PolygonType.Area;
        }
        
        private GeoPoint[] GetPointsSorted(DepartmentStationClientData dep)
        {
            List<DepartmentStationAddressClientData> listAddss = dep.DepartmentStationAddress.Cast<DepartmentStationAddressClientData>().ToList();

            listAddss.Sort(delegate(DepartmentStationAddressClientData a, DepartmentStationAddressClientData b)
            {
                return Int32.Parse(a.PointNumber) - Int32.Parse(b.PointNumber);
            });

            GeoPoint[] points = listAddss.ConvertAll<GeoPoint>(delegate(DepartmentStationAddressClientData toConv)
            {
                return new GeoPoint(toConv.Lon, toConv.Lat);
            }).ToArray();
            return points;
        }
    }

    public class MapObjectRoute : MapCurve
    {
        public List<MapObjectBusStop> Stops { get; set; }

        public MapObjectRoute(RouteClientData data)
            : base(data.Code, data.Department.Color)
        {
            IDictionary<string, object> args = new Dictionary<string, object>();
            args.Add("Name", data.Name);
            args.Add("ParentCode", data.Department.Code);

            Fields = args;
            Geometry = GetPointsSorted(data).ToList();  
            //this.Type = MapPolygon.PolygonType.Line;
        }

        /// <summary>
        /// Build an empty object with properties names
        /// </summary>
        public MapObjectRoute()
            : base(0,Color.Black)
        {
            IDictionary<string, object> args = new Dictionary<string, object>();
            args.Add("Name", string.Empty);
            args.Add("ParentCode", 0);

            Fields = args;
            Geometry = new List<GeoPoint>();
            //this.Type = MapPolygon.PolygonType.Area;
        }

        private GeoPoint[] GetPointsSorted(RouteClientData route)
        {
            List<RouteAddressClientData> listAddss = route.RouteAddress.Cast<RouteAddressClientData>().ToList();

            listAddss.Sort(delegate(RouteAddressClientData a, RouteAddressClientData b)
            {
                return a.PointNumber - b.PointNumber;
            });

            Stops = new List<MapObjectBusStop>();
            GeoPoint[] points = listAddss.ConvertAll<GeoPoint>(delegate(RouteAddressClientData toConv)
            {
                if (!string.IsNullOrEmpty(toConv.Name))
                {
                    toConv.Code = int.Parse(route.Code.ToString() + toConv.Code.ToString());
                    Stops.Add(new MapObjectBusStop(toConv, route.Code, route.Image));
                }
                return new GeoPoint(toConv.Lon, toConv.Lat);
            }).ToArray();
            return points;
        }
    }

    public class MapObjectTrack : MapCurve
    {
        public MapObjectTrack(UnitClientData data, Color color, List<GeoPoint> points)
            : base(data.Code, color)
        {
            IDictionary<string, object> args = new Dictionary<string, object>();
            args.Add("CustomCode", data.CustomCode);

            Fields = args;
            Geometry = points;  
        }

        /// <summary>
        /// Build an empty object with properties names
        /// </summary>
        public MapObjectTrack()
            : base(0, Color.Black)
        {
            IDictionary<string, object> args = new Dictionary<string, object>();
            args.Add("CustomCode", string.Empty);

            Fields = args;
            Geometry = new List<GeoPoint>();
        }
    }

    public class MapObjectUnitFollow : MapPoint
    {
       public MapObjectUnitFollow(UnitClientData data)
            : base(data.Code, !data.IsSynchronized ? null : new GeoPoint(data.Lon, data.Lat), data.IconName)
        {
            IDictionary<string, object> args = new Dictionary<string, object>();
            args.Add("CustomCode", data.CustomCode);

            Fields = args;
        }

        /// <summary>
        /// Build an empty object with properties names
        /// </summary>
        public MapObjectUnitFollow()
        :base(0,null,"")
        {
            IDictionary<string, object> args = new Dictionary<string, object>();
            args.Add("CustomCode", string.Empty);

            Fields = args;
        }
    }

    public class MapObjectUnitTrack: MapPoint
    {
        public MapObjectUnitTrack(UnitClientData data)
            : base(data.Code, !data.IsSynchronized ? null : new GeoPoint(data.Lon, data.Lat), data.IconName)
        {
            IDictionary<string, object> args = new Dictionary<string, object>();
            args.Add("CustomCode", data.CustomCode);

            Fields = args;
        }

        /// <summary>
        /// Build an empty object with properties names
        /// </summary>
        public MapObjectUnitTrack()
            : base(0, null, "")
        {
            IDictionary<string, object> args = new Dictionary<string, object>();
            args.Add("CustomCode", string.Empty);

            Fields = args;
        }
    }

    public class MapObjectAlarm : MapPoint
    {
        public MapObjectAlarm(int code, double lon, double lat, string name, int severity)
            : base(code, new GeoPoint(lon, lat), (severity == 1) ? "Alarm1.bmp" : (severity == 2) ? "Alarm2.bmp" : "Alarm3.bmp")
        {
            IDictionary<string, object> args = new Dictionary<string, object>();
            args.Add("Name", name);
            args.Add("Severity", severity);

            Fields = args;
        }

        /// <summary>
        /// Build an empty object with properties names
        /// </summary>
        public MapObjectAlarm()
            : base(0, null, "")
        {
            IDictionary<string, object> args = new Dictionary<string, object>();
            args.Add("Name", string.Empty);
            args.Add("Severity", 1);

            Fields = args;
        }
    }
}
