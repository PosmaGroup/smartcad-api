﻿using SmartCadCore.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smartmatic.SmartCad.Map.Util
{
    public class Line
    {
        public GeoPoint PointFirst { get; set; }
        public GeoPoint PointSecond { get; set; }
        public GeoPoint Intersection { get; private set; }
        public double K { get; private set; }
        public double N { get; private set; }

        public Line() { }
        public Line(GeoPoint first, GeoPoint second)
        {
            PointFirst = first;
            PointSecond = second;
            CalculateKoeficients();
        }

        private void CalculateKoeficients()
        {
            K = (PointFirst.Lat - PointSecond.Lat) / (PointFirst.Lon - PointSecond.Lon);
            N = PointFirst.Lat - (K * PointFirst.Lon);
        }

        public static GeoPoint GetIntersection(Line ln1, Line ln2)
        {
            try
            {
                double x = (ln1.N - ln2.N) / (ln2.K - ln1.K);
                double y = ln2.K * x + ln2.N;
                return new GeoPoint((double)x, (double)y);
            }
            catch
            {
                return null;
            }
        }

        public override string ToString()
        {
            return string.Format("y = {0} * x + {1}", K, N);
        }
    }
}
