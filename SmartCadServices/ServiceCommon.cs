﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Channels;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Xml.Linq;
using System.Reflection;
using System.Diagnostics;
using System.Threading;
using System.Collections;

/// <summary>
/// Common classes that are used by the communication services. Client or Server.
/// </summary>
namespace Smartmatic.SmartCad.Service
{
    /// <summary>
    /// Indicate to the service that a custom behavior would occur.
    /// Extract from the message send by the client the current session information.
    /// </summary>
    public class SessionStateContextDispatchBehaviorAttribute : Attribute, IEndpointBehavior, IServiceBehavior, IDispatchMessageInspector
    {
        #region IDispatchMessageInspector Members

        public object AfterReceiveRequest(ref Message request, System.ServiceModel.IClientChannel channel, System.ServiceModel.InstanceContext instanceContext)
        {
            // add context to operation context
            // create business context from incoming message headers
            OperationContext.Current.Extensions.Add(new SessionStateContextExtension());

            int headerPos = request.Headers.FindHeader(SessionStateContextExtension.HEADER_NAME, SessionStateContextExtension.HEADER_NAMESPACE);

            if (headerPos == -1)
            { }
            //    throw new FaultException(string.Format("Message header missing: {0}", SessionStateContextExtension.HEADER_NAME));
            else
                SessionStateContextExtension.Current.Session = request.Headers.GetHeader<SessionState>(SessionStateContextExtension.HEADER_NAME, SessionStateContextExtension.HEADER_NAMESPACE);

            return null;
        }

        public void BeforeSendReply(ref Message reply, object correlationState)
        {

        }

        #endregion

        #region IEndpointBehavior Members

        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        { }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        { }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
            endpointDispatcher.DispatchRuntime.MessageInspectors.Add(new SessionStateContextDispatchBehaviorAttribute());
        }

        public void Validate(ServiceEndpoint endpoint)
        {
        }

        #endregion

        #region IServiceBehavior Members

        public void AddBindingParameters(ServiceDescription serviceDescription, System.ServiceModel.ServiceHostBase serviceHostBase, System.Collections.ObjectModel.Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, System.ServiceModel.ServiceHostBase serviceHostBase)
        {
            foreach (ChannelDispatcher dispatcher in serviceHostBase.ChannelDispatchers)
            {
                // TODO: only add for application endpoints, not mex
                foreach (EndpointDispatcher epDispatcher in dispatcher.Endpoints)
                {
                    epDispatcher.DispatchRuntime.MessageInspectors.Add(new SessionStateContextDispatchBehaviorAttribute());
                }
            }
        }

        public void Validate(ServiceDescription serviceDescription, System.ServiceModel.ServiceHostBase serviceHostBase)
        {
        }

        #endregion
    }

    /// <summary>
    /// Extend the current operation context in order to add the neccesary information about the session.
    /// </summary>
    public class SessionStateContextExtension : IExtension<OperationContext>
    {
        public const string HEADER_NAME = "Session";
        public const string HEADER_NAMESPACE = "http://Session";

        public SessionState Session { get; set; }

        public static SessionStateContextExtension Current
        {
            get { return OperationContext.Current.Extensions.Find<SessionStateContextExtension>(); }
        }

        public MessageHeader CreateMessageHeader()
        {
            return MessageHeader.CreateHeader(HEADER_NAME, HEADER_NAMESPACE, Session);
        }

        public static MessageHeader CreateMessageHeader(SessionState session)
        {
            return MessageHeader.CreateHeader(HEADER_NAME, HEADER_NAMESPACE, session);

        }

        #region IExtension<OperationContext> Members

        public void Attach(OperationContext owner)
        {
        }

        public void Detach(OperationContext owner)
        {
        }

        #endregion
    }

    /// <summary>
    /// Extend the client behavior in order to add to each outgoing message to the server the session information.
    /// </summary>
    public class SessionStateClientBehavior : IEndpointBehavior, IClientMessageInspector
    {
        public SessionStateClientBehavior()
        {
        }

        public SessionStateClientBehavior(SessionState session)
        {
            Session = session;
        }

        public SessionState Session { get; set; }

        #region IClientMessageInspector Members

        public void AfterReceiveReply(ref Message reply, object correlationState)
        {

        }

        public object BeforeSendRequest(ref Message request, System.ServiceModel.IClientChannel channel)
        {
            foreach (MessageHeader header in channel.GetAutoHeaders())
            {
                request.Headers.Add(header);
            }
            return null;
        }

        #endregion

        #region IEndpointBehavior Members

        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {

        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            clientRuntime.MessageInspectors.Add(new SessionStateClientBehavior());
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
        }

        public void Validate(ServiceEndpoint endpoint)
        {

        }

        #endregion

    }

    /// <summary>
    /// Extension class for the IContextChannel
    /// </summary>
    public static class AutoHeaderExtension
    {
        class AutoHeaderContextExtension : Dictionary<XName, MessageHeader>, IExtension<IContextChannel>
        {
            public void Attach(IContextChannel owner)
            {
            }

            public void Detach(IContextChannel owner)
            {
            }
        }

        public static void AddAutoHeader(this IContextChannel proxy, string name, string ns, object value)
        {
            AutoHeaderContextExtension extension = proxy.Extensions.Find<AutoHeaderContextExtension>();
            if (extension == null)
            {
                extension = new AutoHeaderContextExtension();
                proxy.Extensions.Add(extension);
            }
            extension[XName.Get(name, ns)] = MessageHeader.CreateHeader(name, ns, value);
        }

        public static IEnumerable<MessageHeader> GetAutoHeaders(this IContextChannel proxy)
        {
            AutoHeaderContextExtension extension = proxy.Extensions.Find<AutoHeaderContextExtension>();
            if (extension == null)
            {
                return Enumerable.Empty<MessageHeader>();
            }
            return extension.Values;
        }

        public static void RemoveAutoHeader(this IContextChannel proxy, string name, string ns)
        {
            AutoHeaderContextExtension extension = proxy.Extensions.Find<AutoHeaderContextExtension>();
            if (extension != null)
            {
                extension.Remove(XName.Get(name, ns));
            }
        }
    }

    /// <summary>
    /// This class holds the information about the operator session.
    /// </summary>
    [Serializable]
    public class SessionState
    {
        public string Application { get; set; }
        public OperatorSessionState User { get; set; }
        public string Computer { get; set; }
    }

    /// <summary>
    /// Light operator data for the SessionState
    /// </summary>
    [Serializable]
    public class OperatorSessionState
    {
        public int Code { get; set; }
        public string Login { get; set; }
        public int Role { get; set; }
    }

    public class Variables
    {
        public static int Retries = 5;
    }

    /// <summary>
    /// Base proxy for conexion against a server.
    /// Check that the call to the server has been donde successfully.
    /// </summary>
    public class ServerProxy<K> where K : class
    {
        #region Fields
        /// <summary>
        /// Information about the session that is going to be passed to the server at every call.
        /// </summary>
        SessionState session = null;
        SeverProxyFactory<K> factory = null;
        #endregion


        public ServerProxy()
        {
        }

        public ServerProxy(SessionState session)
            : this()
        {
            this.session = session;
        }

        public SessionState Session
        {
            get
            {
                return session;
            }
        }

        #region Client Load Balacing and High Avalaibility
        /// <summary>
        /// Method that fix the server if one of them is not responding.
        /// All the methods that call the server should call this one.
        /// </summary>
        /// <typeparam name="T">Type to return, object if void.</typeparam>
        /// <param name="args">Arguments from the method that has to be called.</param>
        /// <returns>The result of the method called.</returns>
        private T CallChannel<T>(params object[] args)
        {
            T result = default(T);
            bool successfull = false;
            int serverTries = 0;
            while (successfull == false)
            {
                K channel = null;
                try
                {
                    if (factory.Channels.Count > 0)
                    {
                        channel = factory.ChangeConnection();

                        StackTrace callStack = new StackTrace(); // The call stack
                        StackFrame frame = callStack.GetFrame(1); // The frame that called me
                        MethodBase method = frame.GetMethod(); // The method that called me

                        //Create the Type array to get the method.
                        ParameterInfo[] parameters = method.GetParameters();
                        Type[] methodTypes = new Type[parameters.Length];

                        for (int parametersIndex = 0; parametersIndex < parameters.Length; parametersIndex++)
                        {
                            ParameterInfo parameterInfo = parameters[parametersIndex];
                            methodTypes[parametersIndex] = parameterInfo.ParameterType;
                        }
                        //We obtain the method that we want to call.
                        MethodInfo methodInfo = channel.GetType().GetMethod(method.Name, methodTypes);

                        //Add the session to each call to the server.
                        ((IContextChannel)channel).AddAutoHeader(SessionStateContextExtension.HEADER_NAME, SessionStateContextExtension.HEADER_NAMESPACE, session);

                        if (args.Length > 0 && methodInfo.ReturnType != typeof(void))
                            result = (T)methodInfo.Invoke(channel, args);
                        else if (args.Length > 0 && methodInfo.ReturnType == typeof(void))
                            methodInfo.Invoke(channel, args);
                        else if (args.Length == 0 && methodInfo.ReturnType != typeof(void))
                            result = (T)methodInfo.Invoke(channel, null);
                        else if (args.Length == 0 && methodInfo.ReturnType == typeof(void))
                            methodInfo.Invoke(channel, null);
                        successfull = true;
                    }
                    else
                    {
                        serverTries++;
                        if (serverTries >= Variables.Retries)
                            throw new EndpointNotFoundException();
                    }
                }
                catch
                {

                    factory.RemoveChannel(channel);
                    serverTries++;
                    if (serverTries >= Variables.Retries)
                        throw;
                }
                Thread.Sleep(100);
            }
            return result;
        }
        #endregion
    }

    /// <summary>
    /// Factory that would create and use the connection to the server.
    /// </summary>
    /// <typeparam name="T">Interface to connect to the server.</typeparam>
    public class SeverProxyFactory<T>
    {
        #region Events

        public static event EventHandler<EventArgs> CommittedChanges;
        public static event EventHandler<EventArgs> GisAction;

        #endregion

        #region Fields
        /// <summary>
        /// Channel Factory
        /// </summary>
        static ChannelFactory<T> factory = null;
        static List<IEndpointBehavior> behaviors = new List<IEndpointBehavior>();
        static ServiceHost host = null;
        static List<string> channelsIDS = new List<string>();
        static List<T> channels = new List<T>();
        static Thread thread = null;
        static List<string> servers = new List<string>();
        static object syncChannels = new object();
        static bool randomChannel = false;
        static int channelIndex = 0;
        static Random random = new Random();
        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="checkChannels">Check if the conexions are active.</param>
        public SeverProxyFactory(
            bool checkChannels, 
            List<IEndpointBehavior> behaviors)
        {
            CreateFactory(behaviors);
            AddServers();
            if (checkChannels == true)
            {
                thread = new Thread(new ThreadStart(CheckChannels));
                thread.Name = "Check Channels";
                thread.Start();
            }
        }

        public SeverProxyFactory()
            : this(false, new List<IEndpointBehavior>())
        { }

        /// <summary>
        /// Create the factory that is going to be used to create the conexions to the server.
        /// </summary>
        private void CreateFactory(List<IEndpointBehavior> behaviors)
        {
            factory = new ChannelFactory<T>(new NetTcpBinding(SecurityMode.None));
            foreach (IEndpointBehavior behavior in behaviors)
            {
                factory.Endpoint.Behaviors.Add(behavior);
            }
            factory.Open();
        }

        /// <summary>
        /// Create the host for the client, perform as a server, and could receive notifications.
        /// </summary>
        public void AddHost(Type receivingType, Binding binding, string address)
        {
            host = new ServiceHost(typeof(SeverProxyFactory<T>));
            host.AddServiceEndpoint(receivingType, binding, address);
            host.Open();
        }

        /// <summary>
        /// Loads all the servers that the client can connect.
        /// </summary>
        private void AddServers()
        {
            foreach (string uri in servers)
            {
                try
                {
                    if (channelsIDS.Contains(uri) == false && ((randomChannel == false && channelsIDS.Count == 0) || (randomChannel == true)))
                    {
                        T service = TryConnection(uri);
                        AddChannel(service);
                    }
                }
                catch
                { }
            }
        }

        /// <summary>
        /// Check if the conexion to the server are faulted or closed are remove it from the list.
        /// </summary>
        private void CheckChannels()
        {
            while (true)
            {
                AddServers();
                foreach (T channel in channels)
                {
                    if (Channel(channel).State != CommunicationState.Opened)
                    {
                        RemoveChannel(channel);
                    }
                }
                Thread.Sleep(1000);
            }
        }

        /// <summary>
        /// Try the connection to the server.
        /// </summary>
        /// <param name="uri">Address of the server.</param>
        /// <returns></returns>
        private T TryConnection(string uri)
        {
            //Create connection to the next server.
            T channel = factory.CreateChannel(new EndpointAddress(uri));
            //Try connection to the new server.
            try
            {
                Channel(channel).Open();
                return channel;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Remove a channel that is unreachable.
        /// </summary>
        /// <param name="channel">The channel that is unreachable.</param>
        public void RemoveChannel(T channel)
        {
            if (channel != null)
            {
                lock (syncChannels)
                {
                    try
                    {
                        //Check in which state is the channel to close if properly.
                        if (Channel(channel).State == CommunicationState.Faulted)
                            Channel(channel).Abort();
                        else if (Channel(channel).State != CommunicationState.Closed)
                            Channel(channel).Close();
                    }
                    catch
                    { }

                    //Get the id of the channel to remove it from the list.
                    string id = Channel(channel).RemoteAddress.Uri.AbsoluteUri;
                    if (channelsIDS.Contains(id) == true)
                    {
                        channelsIDS.Remove(id);
                        channels.Remove(channel);
                    }
                }
            }
        }

        /// <summary>
        /// Add a channel to the list of channels.
        /// </summary>
        /// <param name="channel">A chanel or server to connect.</param>
        private void AddChannel(T channel)
        {
            lock (syncChannels)
            {
                string id = Channel(channel).RemoteAddress.Uri.AbsoluteUri;
                if (channelsIDS.Contains(id) == false)
                {
                    channelsIDS.Add(id);
                    channels.Add(channel);
                }
            }
        }

        /// <summary>
        /// Update the server index.
        /// </summary>
        private int UpdateServerIndex()
        {
            channelIndex = (channelIndex + 1) % Channels.Count;
            return channelIndex;
        }

        /// <summary>
        /// Change the connection to a random server.
        /// If there is 1 connection it does not do anything.
        /// If there is 2 connection it just change the server to the other.
        /// If there is more than two servers is generate the random number.
        /// </summary>
        public T ChangeConnection()
        {
            int index = 0;
            if (randomChannel == true)
            {
                //Server lenght greater than 2, generate the random number.
                if (channels.Count > 2)
                {
                    index = random.Next(0, channels.Count * 100) / 100;
                }
                //Server lenght is 2, changed to the other server.
                else
                {
                    index = UpdateServerIndex();
                }
            }
            return GetChannel(index);
        }

        /// <summary>
        /// Get the channel by the given index.
        /// </summary>
        /// <param name="index">Index of the channel.</param>
        /// <returns>The channel or service connection.</returns>
        private T GetChannel(int index)
        {
            lock (syncChannels)
            {
                return channels[index];
            }
        }

        /// <summary>
        /// Convert the interface of the server to IClientChannel.
        /// </summary>
        /// <param name="channel"></param>
        /// <returns></returns>
        private IClientChannel Channel(T channel)
        {
            return (IClientChannel)channel;
        }

        /// <summary>
        /// Get the list of conexion available to comunicae with the server.
        /// </summary>
        public List<T> Channels
        {
            get
            {
                lock (syncChannels)
                {
                    return channels;
                }
            }
        }

        /// <summary>
        /// Close all the conexion(s) to the server(s).
        /// </summary>
        public void Close()
        {
            try
            {
                if (thread != null)
                    thread.Abort();
            }
            catch
            { }
            foreach (T channel in channels)
            {
                try
                {
                    Channel(channel).Close();
                }
                catch
                { }
            }
            if (factory != null && factory.State != CommunicationState.Closed)
                factory.Abort();
            if (host != null && host.State != CommunicationState.Closed)
                host.Close();
        }
    }
}
