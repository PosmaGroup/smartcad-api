using System;
using System.Collections.Generic;
using System.Text;
using System.ServiceModel;
using System.Collections;

using System.IO;


using System.Net.Security;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadCore.Model;

namespace Smartmatic.SmartCad.Service
{
    //[ServiceContract(SessionMode = SessionMode.Required, CallbackContract = typeof(IServerServiceCallback), ProtectionLevel = ProtectionLevel.None)]
    [ServiceContract(SessionMode = SessionMode.Allowed, ProtectionLevel = ProtectionLevel.None)]
    public interface IServerService
    {
        #region Session
        [OperationContract( )]
        OperatorClientData OpenSession(string login, string password, string extNumber, string computerName, UserApplicationClientData application);

        [OperationContract()]
        void CloseSession();
        #endregion

        [OperationContract( )]
        bool CheckAccessClient(UserResourceClientData userResource, UserActionClientData userAction, UserApplicationClientData userApplication, bool throwException);

        #region SaveOrUpdate
        [OperationContract( )]
		void SaveOrUpdateClientData(IList list);

        [OperationContract(Name = "SaveOrUpdateClientDataList")]
        object SaveOrUpdateClientData(ClientData obj, bool withReturn);

        #endregion

        #region Languages
        [OperationContract()]
        byte[] CheckLanguages(string language);

        #endregion

        [OperationContract( )]
		void SendClientData(ClientData clientData);

		[OperationContract( )]
		void SendClientDataList(IList list);


		[OperationContract()]
		List<ClientData> GetHistory(DateTime startDate, DateTime endDate, string deviceType, string deviceID, int min, int max);

		[OperationContract( )]
		void DeleteClientObject(ClientData clientData);

        [OperationContract()]
        void DeleteAlarmObject(ClientData clientData);

		[OperationContract( )]
		void DeleteClientObjectCollection(IList list);

        [OperationContract( )]
        IList RunTasks(IList tasks);

        [OperationContract( )]
        void FinalizeCall(PhoneReportClientData phoneReport);

        [OperationContract( )]
        void FinalizeCctvIncident(CctvReportClientData cctvReport);

        [OperationContract()]
        bool CheckVirtualCall();

        [OperationContract()]
        void FinalizeAlarmIncident(AlarmReportClientData alarmReport);

        [OperationContract()]
        void FinalizeAlarmLprIncident(AlarmLprReportClientData alarmReport);

        [OperationContract()]
        void FinalizeTwitterIncident(TwitterReportClientData twitterReport);

        [OperationContract( )]
        ICollection InitializeLazy(ObjectData obj, string property);

        [OperationContract( )]
        DateTime GetTime();

        [OperationContract( )]
        void SetOperatorStatus(OperatorStatusClientData status);

        [OperationContract( )]
        double CalculateAmountTimeAvailableByStatusWithPause(string operatorStatusCustomCode);

        [OperationContract( )]
        void Ping();

        [OperationContract( )]
        ConfigurationClientData GetConfiguration();

        [OperationContract()]
        bool SetGPSOutputs(string idGPS, IList<int> outputs, bool state);

        [OperationContract(Name = "SetOutputsByTime")]
        bool SetGPSOutputs(string idGPS, IList<int> outputs, bool state, int seconds, int times);

        [OperationContract()]
        bool DevicePing( ServerServiceClient.Device deviceToPing );

        [OperationContract( )]
        IList SearchClientObjects(Type clientDataType);

        [OperationContract( )]
        ClientData SearchClientObject(string hql);

        [OperationContract()]
        ClientData SearchClientObjectByBytes(string hql, int offset, int length);

        [OperationContract(Name = "SearchClientObjectByObject")]
        ClientData SearchClientObject(ClientData obj);

        [OperationContract(Name = "SearchClientObjectsHql")]
        IList SearchClientObjects(string hql);

        [OperationContract(Name = "SearchClientObjectsHqlBool")]
        IList SearchClientObjects(string hql, bool InitializeCollections);

        [OperationContract( )]
        IList SearchClientObjectsMaxRows(string hql, int startIndex, int maxRows);

        [OperationContract( )]
        object SearchBasicObjects(string hql);

        [OperationContract( )]
        object SearchBasicObject(string hql);

        [OperationContract()]
        bool ActivateApplication(string application, string macAddress);

        [OperationContract()]
        IList GetActiveApplications();

        [OperationContract()]
        IList GetInstalledApplicationsForWorkStation(string macAddress);

        [OperationContract()]
        bool RemoveInstalledApplication(string application, string macAddress);

        [OperationContract( )]
        void UpdatePhoneReportClient(PhoneReportClientData phoneReportClient);

        [OperationContract( )]
        void UpdateCctvReportClient(CctvReportClientData cctvReportClient);

        [OperationContract()]
        void UpdateAlarmReportClient(AlarmReportClientData alarmReportClient);

        [OperationContract()]
        void UpdateAlarmLprReportClient(AlarmLprReportClientData alarmReportClient);

        [OperationContract( )]
        ClientData RefreshClient(ClientData clientData);

        [OperationContract(Name = "RefreshClientWithBool")]
        ClientData RefreshClient(ClientData clientData, bool initializeCollections);

        [OperationContract( )]
        void AssignIncidentNotificationToManualSupervisor(IncidentNotificationClientData notification);

        [OperationContract( )]
        bool AssignIncidentNotificationToOperator(IncidentNotificationClientData selectedNotification, OperatorClientData selectedOperator);

        [OperationContract( )]
        IList GetSupervisedOperators(string applicationName, int departmentCode);

        [OperationContract( )]
        IList GetOperatorsWorkingNowByAppByWorkShift(string applicationName, int workshifCode);

        [OperationContract( )]
        bool CheckOperatorClientAccess(OperatorClientData oper, string supervisedApplicationName);

        [OperationContract( )]
        object OperatorScheduleManagerMethod(string methodName, IList args);

        [OperationContract( )]
        void SetSupervisedApplicationName(string supervisedAppName);

		[OperationContract( )]
		bool CheckGeneralSupervisor(int roleCode);

		[OperationContract( )]
		bool CheckSupervisor(int roleCode);

		[OperationContract( )]
		bool CheckApplicationSupervisor(int roleCode, string app);

        [OperationContract( )]
        bool CheckFirstLevelOperator(int roleCode);

        [OperationContract( )]
        bool CheckDispatchOperator(int roleCode);

        [OperationContract( )]
        bool CheckUserAccess(int roleCode, string accessName);

        [OperationContract()]
        IList<string> GetIncidentCodeInRatio(GeoPoint point, double ratio, DistanceUnit distanceUnit);

		[OperationContract( )]
		DepartmentStationClientData GetDepartmentStationForIncidentNotification(IncidentNotificationClientData notification);

		[OperationContract()]
		RouteAddressClientData GetLastStop(UnitClientData unit,RouteClientData route);

		[OperationContract()]
		TimeSpan GetAverageStopTime(UnitClientData selectedPosition, WorkShiftRouteClientData wsr);

		[OperationContract()]
        void RequestReassignIncidentNotifications();

		[OperationContract()]
		Dictionary<string, IList> SearchClientObjectsMaxRowsMultiQuery(int startIndex, int maxRows, Dictionary<string, IList> querys);

		[OperationContract()]
		IList GetRunTimePoints(UnitClientData selectedPosition, WorkShiftScheduleVariationClientData schedule);

		[OperationContract()]
		double GetAverageSpeed(UnitClientData selectedPosition, WorkShiftScheduleVariationClientData schedule);
    }
}
