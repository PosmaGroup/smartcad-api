/*using System;
using System.Collections.Generic;
using System.Text;

using System.ServiceModel;
using System.ServiceModel.Channels;
using Smartmatic.SmartCad.Data;
using Smartmatic.SmartCad.Core;
using System.ServiceModel.Description;
using System.Runtime.Serialization;
using System.Collections;
using System.Threading;
using System.ServiceModel.PeerResolvers;

namespace Smartmatic.SmartCad.Service
{
    public abstract class ServiceNotification<T, U> where U : T
    {
        Uri address;
        ServiceHost host;
        ServiceNotificationType type = ServiceNotificationType.Both;
        ChannelFactory<U> factory;
        T channel;
        protected PeerMessagePropagationFilter filter;

        public ServiceNotification(ServiceNotificationType type, Uri address)
            : this(type, address, null)
        {}

        public ServiceNotification(ServiceNotificationType type, Uri address, PeerMessagePropagationFilter filter)
        {
            this.address = address;
            this.type = type;
            this.filter = filter;
        }

        public T Call
        {
            get
            {
                return channel;
            }
        }

        protected abstract CustomBinding Binding
        {
            get;
        }

        public void Start()
        {
            if (type == ServiceNotificationType.Both || type == ServiceNotificationType.Client)
            {
                host = new ServiceHost(this);
                host.AddServiceEndpoint(typeof(T), Binding, address);
                foreach (ServiceEndpoint ep in host.Description.Endpoints)
                {
                    foreach (OperationDescription op in ep.Contract.Operations)
                    {
                        DataContractSerializerOperationBehavior dataContractBehavior =
                           op.Behaviors.Remove<DataContractSerializerOperationBehavior>()
                                as DataContractSerializerOperationBehavior;

                        op.Behaviors.Add(new NetDataContractSerializerOperationBehavior(op));
                    }
                }
                host.Open();
            }

            if (type == ServiceNotificationType.Both || type == ServiceNotificationType.Server)
            {
                factory = new ChannelFactory<U>(Binding, new EndpointAddress(address));
                foreach (OperationDescription op in factory.Endpoint.Contract.Operations)
                {
                    DataContractSerializerOperationBehavior dataContractBehavior =
                       op.Behaviors.Remove<DataContractSerializerOperationBehavior>()
                            as DataContractSerializerOperationBehavior;

                    op.Behaviors.Add(new NetDataContractSerializerOperationBehavior(op));
                }
                channel = factory.CreateChannel();
                ((IClientChannel)channel).OperationTimeout = new TimeSpan(0, 0, 10);

                if (filter != null)
                {
                    PeerNode myPeerNode = ((IChannel)channel).GetProperty<PeerNode>();
                    myPeerNode.MessagePropagationFilter = filter;
                }
            }
        }

        public void Stop()
        {
            if (type == ServiceNotificationType.Both || type == ServiceNotificationType.Client)
            {
                if (host != null)
                    host.Close();
            }

            if (type == ServiceNotificationType.Both || type == ServiceNotificationType.Server)
            {
                if (channel != null)
                    ((IChannel)channel).Close();
                if (factory != null)
                    factory.Close();
            }
        }
    }

    #region Service Notification Peer To Peer
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class ServiceNotificationDuplex : ServiceNotification<IServiceNotification, IServiceNotificationClient>, IServiceNotification
    {
        public event EventHandler<CommittedObjectDataCollectionEventArgs> CommittedChanges;
        public event EventHandler<GisActionEventArgs> GisAction;
        public event EventHandler<GisActionEventArgs> GisActionResponse;
        public event EventHandler<PingActionEventArgs> PingNotificationResponse;
        private CustomBinding binding;
        private string login;
        private string application;

        public string Login
        {
            get
            {
                return this.login;
            }
            set
            {
                this.login = value;
                if (this.filter != null)
                {
                    (this.filter as ApplicationMessagePropagationFilter).Login = this.login;
                }                
            }
        }

        public ServiceNotificationDuplex(ServiceNotificationType type)
            :base(type, new Uri(SmartCadConfiguration.CLIENT_NOTIFICATION))
        {}

        public ServiceNotificationDuplex(UserApplicationData application)
            : base(ServiceNotificationType.Both, new Uri(SmartCadConfiguration.CLIENT_NOTIFICATION), new ApplicationMessagePropagationFilter(application.Name))
        {}

        public ServiceNotificationDuplex(UserApplicationData application, string login)
            : base(ServiceNotificationType.Both, new Uri(SmartCadConfiguration.CLIENT_NOTIFICATION), new ApplicationMessagePropagationFilter(application.Name, login))
        {
            this.Login = login;
        }

        protected override CustomBinding Binding
        {
            get 
            { 
                if (binding == null)
                    binding = BindingBuilder.GetNotificationBinding((this.filter as ApplicationMessagePropagationFilter).ApplicationName == UserApplicationData.Server.Name);
                return binding;
            }
        }

        #region IServiceNotification Members

        void IServiceNotification.UpdateInformation(ClientNotification notification)
        {
            ThreadPool.QueueUserWorkItem(delegate(object state)
            {
                if (CommittedChanges != null)
                {
                    CommittedChanges(null, notification.Data);
                }
            });
        }

        void IServiceNotification.ShareInformationGis(GisNotification gisNotification)
        {
            ThreadPool.QueueUserWorkItem(delegate(object state)
            {
                if (GisAction != null)
                {
                    GisAction(null, gisNotification.Data);
                }
            });
        }

        void IServiceNotification.Ping(PingNotification pingNotification)
        {
            PingNotification pn = new PingNotification(UserApplicationData.Server.Name);
            pn.FromApplication = pingNotification.Application;
            pn.FromLogin = pingNotification.Login;
            pn.Instance = pingNotification.Instance;
            Call.PingResponse(pn);
        }

        void IServiceNotification.PingResponse(PingNotification pingNotification)
        {
            if (PingNotificationResponse != null)
            {
                PingNotificationResponse(null, new PingActionEventArgs(pingNotification.FromApplication, pingNotification.FromLogin, pingNotification.Instance));
            }
        }

        #endregion
    }

    public interface IServiceNotificationClient : IServiceNotification, IClientChannel
    { }

    [ServiceContract]
    public interface IServiceNotification
    {
        [OperationContract(IsOneWay = true)]
        void UpdateInformation(ClientNotification notification);

        [OperationContract(IsOneWay = true)]
        void ShareInformationGis(GisNotification gisNotification);

        [OperationContract(IsOneWay = true)]
        void Ping(PingNotification pingNotification);

        [OperationContract(IsOneWay = true)]
        void PingResponse(PingNotification pingNotification);
    }

    [MessageContract]
    public class ApplicationNotification
    {
        [MessageHeader(Name = "Application")]
        readonly string application;
        [MessageHeader(Name = "Login")]
        readonly string login;


        public ApplicationNotification(string application)
            :this(application, null)
        {}

        public ApplicationNotification(string application, string login)
        {
            this.application = application;
            this.login = login;
        }

        public ApplicationNotification()
        { }

        public string Application
        {
            get { return application; }
        }

        public string Login
        {
            get { return login; }
        }
    }

    [MessageContract]
    public class ClientNotification : ApplicationNotification
    {
        [MessageBodyMember]
        CommittedObjectDataCollectionEventArgs data;
        
        public CommittedObjectDataCollectionEventArgs Data
        {
            get { return data; }
            set { data = value; }
        }

        public ClientNotification(string application)
            : this(application, null)
        {}

        public ClientNotification(string application, string login)
            : base(application, login)
        {}

        public ClientNotification()
        { }
    }

    [MessageContract]
    public class PingNotification : ApplicationNotification
    {
        [MessageBodyMember]
        int instance;
        [MessageBodyMember]
        string application;
        [MessageBodyMember]
        string login;
        
        public int Instance
        {
            get { return instance; }
            set { instance = value; }
        }

        public string FromApplication
        {
            get { return application; }
            set { application = value; }
        }

        public string FromLogin
        {
            get { return login; }
            set { login = value; }
        }

        public PingNotification(string application, string login)
            : base(application, login)
        {}

        public PingNotification(string application)
            : base(application)
        { }

        public PingNotification()
        { }
    }

    [MessageContract]
    public class GisNotification : ApplicationNotification
    {
        [MessageBodyMember]
        GisActionEventArgs data;

        public GisActionEventArgs Data
        {
            get { return data; }
            set { data = value; }
        }

        public GisNotification(string application)
            : this(application, null)
        { }

        public GisNotification(string application, string login)
            : base(application, login)
        { }

        public GisNotification()
        { }
    }

    [MessageContract]
    public abstract class Response<T>
    {
        [MessageBodyMember]
        T result;

        public T Result
        {
            get { return result; }
            set { result = value; }
        }

        public Response(T result)
        {
            this.result = result;
        }

        public Response()
        { }
    }



    public enum ServiceNotificationType
    { 
        Client,
        Server,
        Both
    }

    public class ApplicationMessagePropagationFilter : PeerMessagePropagationFilter
    {
        private string application;
        private string login;

        public string ApplicationName
        {
            get
            {
                return this.application;
            }
            set
            {
                this.application = value;
            }
        }

        public string Login
        {
            get
            {
                return this.login;
            }
            set
            {
                this.login = value;
            }
        }


        public ApplicationMessagePropagationFilter(string application)
            : this(application,null)
        {}

        public ApplicationMessagePropagationFilter(string application, string login)
        {
            this.application = application;
            this.login = login;
        }

        public override PeerMessagePropagation ShouldMessagePropagate(System.ServiceModel.Channels.Message message, PeerMessageOrigination origination)
        {
            PeerMessagePropagation destination = PeerMessagePropagation.Remote;
            string recipientApplication = null;
            string recipientLogin = null;
            for (int i = 0; i < message.Headers.Count && (recipientApplication == null || recipientLogin == null); i++)
            {
                MessageHeaderInfo mhi = message.Headers[i];
                if (mhi.Name == "Application")
                {
                    recipientApplication = message.Headers.GetHeader<string>(i);
                }
                else if (mhi.Name == "Login")
                {
                    recipientLogin = message.Headers.GetHeader<string>(i);
                }
            }
            if (recipientApplication == null)
            {
                destination = PeerMessagePropagation.None;
            }
            else if (((this.application != null && recipientApplication == this.application) && 
                (this.login != null && recipientLogin != null && recipientLogin == this.login)) ||
                (this.application != null && recipientApplication == this.application && recipientApplication == UserApplicationData.Server.Name))
            {
                destination = PeerMessagePropagation.Local;
            }
            else if (recipientApplication == this.application &&
                recipientLogin == null)
            {
                destination = PeerMessagePropagation.LocalAndRemote;
            }
            else
            {
                destination = PeerMessagePropagation.Remote;
            }
            return destination;
        }
    }
    #endregion
}
*/