using System;
using System.Collections.Generic;
using System.Text;
using System.ServiceModel;
using System.Reflection;
using System.Collections;
using System.Security.Principal;
using System.Threading;
using System.Collections.Specialized;
using System.IO;
using System.Net.Sockets;
using System.ServiceModel.Channels;

using NAudio.Wave;
using SmartCadCore.Core;
using SmartCadCore.Common;

namespace Smartmatic.SmartCad.Service
{
    #region Class FileServerService Documentation
    /// <summary>
    /// 
    /// </summary>
    /// <className>FileServerService</className>
    /// <author email="atorres@smartmatic.com">Alden Torres</author>
    /// <date>2007/05/18</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class FileServerService : IFileServerService
    {
        public FileServerService()
        {
        }

        public static ServiceHost Host()
        {
            String address = ApplicationUtil.BuildServiceUrl(
                "net.tcp",
                "localhost",
                SmartCadConfiguration.FILE_SERVER_SERVICE_PORT,
                SmartCadConfiguration.FILE_SERVER_SERVICE_ADDRESS);

            Binding binding = BindingBuilder.GetNetTcpBindingStreamed();

            ServiceHost host = ServiceHostBuilder.ConfigureHost(typeof(FileServerService), typeof(IFileServerService), binding, address);

            return host;
        }

        #region IFileServerService Members

        public Stream GetFile(string fileName)
        {
            try
            {
                fileName = Path.Combine(SmartCadConfiguration.DistFolder, fileName);

                FileStream fileStream = File.Open(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);

                return fileStream;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return null;
            }
        }

        public void SendFile(FileSend upload)
        {
            byte[] bytes = ReadToEnd(upload.Data);
            WaveFileWriter file = new WaveFileWriter(upload.Filename, new WaveFormat(8000, 16, 2));
            file.Write(bytes, 0, bytes.Length);
            
            file.Close();
            file.Dispose();
        }

        #endregion

        private void HandleException(Exception ex)
        {
            SmartLogger.Print(ex);

            throw ApplicationUtil.NewFaultException(ex);
        }

        public static byte[] ReadToEnd(System.IO.Stream stream)
        {
            long originalPosition = stream.Position;

            byte[] readBuffer = new byte[4096];

            int totalBytesRead = 0;
            int bytesRead;

            while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0)
            {
                totalBytesRead += bytesRead;

                if (totalBytesRead == readBuffer.Length)
                {
                    int nextByte = stream.ReadByte();
                    if (nextByte != -1)
                    {
                        byte[] temp = new byte[readBuffer.Length * 2];
                        Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
                        Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
                        readBuffer = temp;
                        totalBytesRead++;
                    }
                }
            }

            byte[] buffer = readBuffer;
            if (readBuffer.Length != totalBytesRead)
            {
                buffer = new byte[totalBytesRead];
                Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
            }
            return buffer;
        }
    }
}
