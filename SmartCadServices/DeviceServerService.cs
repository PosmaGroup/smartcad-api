﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Collections;
using NHibernate.Mapping;
using System.Threading;
using Iesi.Collections;
using System.IO;
using SmartCadCore.Core;
using SmartCadCore.Model;
using SmartCadCore.Common;
using SmartCadCore.ClientData;

namespace Smartmatic.SmartCad.Service
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class DeviceServerService : IDeviceServerService
    {
        public event EventHandler<CommittedObjectDataCollectionEventArgs> ArriveData;
        private static DeviceServerService deviceServerService;
        private ServiceHost host;
        private static IDictionary<string, IDictionary<string, string>> incoming = new Dictionary<string, IDictionary<string, string>>();
        private static object sync = new object();
        private static object syncToWrite = new object();
        private static Thread threadGPS = null;
        private enum IncomingOperation
        {
            Save,
            Read,
            Delete
        }

        private DeviceServerService()
        { }

        public static DeviceServerService Current
        {
            get
            {
                if (deviceServerService == null)
                {
                    deviceServerService = new DeviceServerService();
                    deviceServerService.OpenHost();
                }
                return deviceServerService;
            }
        }
        #region Host

        private void OpenHost()
        {
            if (host == null)
            {
                try
                {
                    String address = ApplicationUtil.BuildServiceUrl(
                        "net.tcp",
                        "localhost",
                        SmartCadConfiguration.SmartCadSection.DeviceServer.Port,
                        "Devices");

                    NetTcpBinding binding = new NetTcpBinding(SecurityMode.None);
                    binding.ReaderQuotas.MaxDepth = 1024;
                    binding.ReaderQuotas.MaxArrayLength = 524288;
                    binding.MaxReceivedMessageSize = 5242880;
                    binding.ReaderQuotas.MaxStringContentLength = 1572864;

                    host = ServiceHostBuilder.ConfigureSimpleHost(typeof(DeviceServerService), typeof(IDeviceServerService), binding, address);
                    host.Open();

                    //Here will be started all devices threads
                    threadGPS = new Thread(new ThreadStart(UpdateAllGPS));
                    threadGPS.Name = "UpdateAllGPS";
                    threadGPS.Start();

                    DeviceAlertService.Start();
                    SmartLogger.Print(ResourceLoader.GetString2("DeviceServerStarted"));
                }
                catch (Exception ex)
                {
                    SmartLogger.Print(ex);
                }

            }
        }

        public void Close()
        {
            try
            {
                if (host != null)
                    host.Close();
                if (threadGPS != null)
                    threadGPS.Abort();
                DeviceAlertService.Stop();
                SmartLogger.Print(ResourceLoader.GetString2("DeviceServerStoped"));
            }
            catch { }
        }

        #endregion

        #region General Methods

        private static string OperationToQueue(IncomingOperation operation, string type)
        {
            return OperationToQueue(null, type, operation);
        }

        private static string OperationToQueue(string data, string type, IncomingOperation operation)
        {
            switch (operation)
            {
                case IncomingOperation.Save:
                    //The lock is in the method. AA
                    InsertOrReplace(data, type);
                    break;
                case IncomingOperation.Read:
                    lock (sync)
                    {
                        if (incoming.Count > 0 && incoming.ContainsKey(type) && incoming[type].Count > 0)
                            data = incoming[type].First().Value;
                        else
                            data = null;
                    }
                    break;
                case IncomingOperation.Delete:
                    lock (sync)
                    {
                        if (incoming.Count > 0 && incoming.ContainsKey(type) && incoming[type].Count > 0)
                        {
                            data = incoming[type].First().Value;
                            incoming[type].Remove(incoming[type].First());
                        }
                        else
                            data = null;
                    }
                    break;
                default:
                    break;
            }
            return data;
        }

        /// <summary>
        /// Insert of replace every data frame that comes from the parser.
        /// Each frame is check by its code and type to check if an update has arrived.
        /// </summary>
        /// <param name="data">Parser incoming data</param>
        /// <param name="type">Type of incoming data</param>
        private static void InsertOrReplace(string data, string type)
        {
            lock (sync)
            {
                /// We check the First array position since this has the code of the data.
                string dataCode = data.Split('|')[1];
                if (incoming.ContainsKey(type))
                {
                    if (incoming[type].ContainsKey(dataCode))
                        incoming[type][data.Split('|')[1]] = data;
                    else
                        incoming[type].Add(dataCode, data);
                }
                else
                {
                    IDictionary<string,string> newlist = new Dictionary<string,string>();
                    newlist.Add(dataCode,data);
                    incoming.Add(type, newlist);
                }
            }
        }

        #endregion
                    
        #region IDeviceServerService Members

        void IDeviceServerService.Ping()
        {
            return;
        }

        void IDeviceServerService.SendData(object obj)
        {
            try
            {
                OperationToQueue(obj.ToString(), obj.ToString().Split('|')[0], IncomingOperation.Save);
                if (Current.ArriveData != null)
                {
                    Current.ArriveData(null, new CommittedObjectDataCollectionEventArgs(obj, CommittedDataAction.None, null));
                }
            }
            catch { }
        }

        #endregion

        #region GPS Methods

        private static void UpdateAllGPS()
        {
            //Update
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US", true);
            while (true)
            {
                try
                {
                    IList gpsList = new ArrayList();
                    Dictionary<int, AlarmSensorTelemetryData> alarms = new Dictionary<int, AlarmSensorTelemetryData>();
                    IList dataloggerValues = new ArrayList();
                    Dictionary<string, AlarmLprData> lprAlarms = new Dictionary<string, AlarmLprData>();
                    for (int i = 0; i < SmartCadConfiguration.SmartCadSection.AvlSync.MaxUnitsUpdated; i++)
                    {
                        try
                        {
                            #region GPS
                            {
                                string GPS = (string)OperationToQueue(IncomingOperation.Read, "GPS");
                                if (GPS != null)
                                {
                                    #region GPS
                                    gpsList.Add(new GPSClientData(GPS));
                                    OperationToQueue(IncomingOperation.Delete, "GPS");
                                    #endregion
                                }
                                else
                                {
                                    //SmartLogger.Print(ResourceLoader.GetString2("UnitArriveWithoutIDGPS"));
                                }

                            }
                            #endregion

                            #region Telemetry
                            {
                                string Telemetry = (string)OperationToQueue(IncomingOperation.Read, "Datalogger");
                                if (Telemetry != null)
                                {
                                    DataloggerClientData dataloggerClientData = new DataloggerClientData(Telemetry);

                                        
                                    dataloggerValues.Add(dataloggerClientData);
                                    #region DataLogger
                                    //Example data: Datalogger|1|20101005 04:54:10|rain_mm=0.23|battery=0.15|DT=0.231

                                    try
                                    {
                                        foreach (SensorTelemetryClientData sensorTelemetryClientData in dataloggerClientData.SensorTelemetrys)
                                        {
                                            SensorTelemetryData sensor = (SensorTelemetryData)SmartCadDatabase.SearchObjectData(
                                                @"SELECT sensor FROM SensorTelemetryData sensor
                                                WHERE sensor.Datalogger.CustomCode = " + dataloggerClientData.CustomCode +
                                                     " AND sensor.Variable = '" + sensorTelemetryClientData.Variable + "'");

                                            if (sensor != null)
                                            {
                                                sensor.Value = sensorTelemetryClientData.Value.Value;
                                                if (sensor.ValueDate.HasValue == false ||
                                                     sensorTelemetryClientData.ValueDate > sensor.ValueDate)
                                                {
                                                   // sensor.ValueDate = sensorTelemetryClientData.ValueDate.Value;
                                                    SmartCadDatabase.SaveOrUpdate(sensor);
                                                }

                                                if (CheckAlarm(sensor))
                                                {
                                                    ObjectData objectData = SmartCadDatabase.SearchObjectData(
                                                     @"SELECT alarm FROM AlarmSensorTelemetryData alarm
                                                    WHERE alarm.SensorTelemetry.Code = " + sensor.Code +
                                                         " AND alarm.EndAlarm is null");

                                                    if (objectData == null)
                                                    {
                                                        objectData = SmartCadDatabase.SearchObjectData(
                                                             @"SELECT alarm FROM AlarmReportData alarm
                                                            WHERE alarm.AlarmSensorTelemetry.SensorTelemetry.Code = " + sensor.Code +
                                                                 " AND alarm.Incident.EndDate is null");

                                                        AlarmSensorTelemetryData alarm = new AlarmSensorTelemetryData()
                                                        {
                                                            SensorTelemetry = sensor,
                                                            StartAlarm = sensorTelemetryClientData.ValueDate,
                                                            Value = sensor.Value.Value
                                                        };

                                                        if (objectData == null)
                                                        {
                                                            if (alarms.ContainsKey(sensor.Code) == false)
                                                                alarms.Add(sensor.Code, alarm);
                                                        }
                                                        else
                                                        {
                                                            AlarmReportData alarmReportOld = (AlarmReportData)objectData;

                                                            AlarmReportData alarmReportNew = new AlarmReportData();

                                                            ApplicationPreferenceData preference = SmartCadDatabase.SearchBasicObject(
                                                                                                    SmartCadHqls.GetCustomHql(SmartCadHqls.GetApplicationPreferenceByName, "AlarmUpdateValue"))
                                                                                                    as ApplicationPreferenceData;

                                                            alarm.EndAlarm = alarm.StartAlarm;
                                                            alarm.Operator = alarmReportOld.Operator;

                                                            alarmReportNew.AlarmSensorTelemetry = alarm;
                                                            if (SmartCadDatabase.IsInitialize(alarmReportOld.SetIncidentTypes) == false)
                                                                SmartCadDatabase.InitializeLazy(alarmReportOld, alarmReportOld.SetIncidentTypes);
                                                            alarmReportNew.SetIncidentTypes = alarmReportOld.SetIncidentTypes;
                                                            alarmReportNew.MultipleOrganisms = alarmReportOld.MultipleOrganisms;
                                                            alarmReportNew.Operator = alarmReportOld.Operator;
                                                            if (SmartCadDatabase.IsInitialize(alarmReportOld.ReportBaseDepartmentTypes) == false)
                                                                SmartCadDatabase.InitializeLazy(alarmReportOld, alarmReportOld.ReportBaseDepartmentTypes);
                                                            alarmReportNew.ReportBaseDepartmentTypes = new ArrayList();
                                                            foreach (ReportBaseDepartmentTypeData rbdtOld in alarmReportOld.ReportBaseDepartmentTypes)
                                                            {
                                                                ReportBaseDepartmentTypeData rbdtNew =
                                                                    new ReportBaseDepartmentTypeData(
                                                                        rbdtOld.DepartmentType,
                                                                        rbdtOld.Priority,
                                                                        alarmReportNew);
                                                                alarmReportNew.ReportBaseDepartmentTypes.Add(rbdtNew);
                                                            }
                                                            alarmReportNew.Incident = alarmReportOld.Incident;
                                                            alarmReportNew.CustomCode = Guid.NewGuid().ToString();

                                                            if (SmartCadDatabase.IsInitialize(alarmReportOld.SetAnswers) == false)
                                                                SmartCadDatabase.InitializeLazy(alarmReportOld, alarmReportOld.SetAnswers);
                                                            alarmReportNew.SetAnswers = new ListSet();
                                                            foreach (AlarmReportAnswerData answerOld in alarmReportOld.SetAnswers)
                                                            {
                                                                if (answerOld.Question.RequirementType == RequirementTypeEnum.Required &&
                                                                    answerOld.Question.CustomCode == "CCTV")
                                                                {
                                                                    AlarmReportAnswerData answerNew = new AlarmReportAnswerData();
                                                                    answerNew.AlarmReport = alarmReportNew;
                                                                    answerNew.Question = answerOld.Question;
                                                                    answerNew.SetAnswers = new ListSet();

                                                                    if (SmartCadDatabase.IsInitialize(answerOld.SetAnswers) == false)
                                                                        SmartCadDatabase.InitializeLazy(answerOld, answerOld.SetAnswers);

                                                                    foreach (PossibleAnswerAnswerData possibleAnswerOld in answerOld.SetAnswers)
                                                                    {
                                                                        PossibleAnswerAnswerData possibleAnswerNew = new PossibleAnswerAnswerData();
                                                                        possibleAnswerNew.AlarmReportAnswer = answerNew;
                                                                        possibleAnswerNew.PossibleAnswer = possibleAnswerOld.PossibleAnswer;
                                                                        possibleAnswerNew.TextAnswer = preference.Value;
                                                                        answerNew.SetAnswers.Add(possibleAnswerNew);
                                                                    }
                                                                    alarmReportNew.SetAnswers.Add(answerNew);
                                                                }
                                                            }
                                                            SmartCadDatabase.SaveOrUpdate(alarmReportNew);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        SmartLogger.Print("Datalogger data arrived with errors. Data='" + Telemetry + "'");
                                    }
                                    OperationToQueue(IncomingOperation.Delete, "Datalogger");
                                    #endregion
                                }
                                else
                                {
                                    //SmartLogger.Print(ResourceLoader.GetString2("UnitArriveWithoutIDGPS"));
                                }
                            }
                            #endregion

                            #region LPR
                            {
                                string lprFrame = (string)OperationToQueue(IncomingOperation.Read, "LPR");
                                if (lprFrame != null)
                                {
                                    string[] lprInfo = lprFrame.Split('|');
                                    if (!lprAlarms.ContainsKey(lprInfo[5]))
                                    {
                                        VehicleRequestData request = (VehicleRequestData)SmartCadDatabase.SearchObjectData(
                                            SmartCadHqls.GetCustomHql(SmartCadHqls.GetVehicleRequestByPlate, lprInfo[5]));
                                        if (request != null)
                                        {
                                            LprData lpr = (LprData)SmartCadDatabase.SearchObjectData(
                                            SmartCadHqls.GetCustomHql(SmartCadHqls.GetLprByName, lprInfo[1]));
                                            if (lpr != null)
                                            {
                                                //COMPRUEBA SI EXISTE UNA ALERTA YA GENERADA CON LA MISMA PLACA,
                                                //CAPTURADA POR EL MISMO LPR Y CON LA MISMA SOLICITUD Y EN CASO DE EXISTIR
                                                //LA ACTUALIZA

                                                AlarmLprData alarm = (AlarmLprData)SmartCadDatabase.SearchObjectData(
                                                SmartCadHqls.GetCustomHql(SmartCadHqls.GetAlarmLprByPlateRequestAndLpr,
                                                    lprInfo[5], request.Code, lpr.Code));

                                                if (alarm != null)// && DateTime.Now.Subtract(alarm.StartDate) < new TimeSpan(0, 1, 0))
                                                {
                                                    //AQUI DEBERIA ACTUALIZARSE UN CAMPO "ULTIMA FECHA VISTO"
                                                    //PARA TENER TANTO LA PRIMERA VEZ Q SE VIO EN ESE LUGAR COMO LA ULTIMA
                                                    break;
                                                }

                                                byte[] bWImageBuffer = null;
                                                byte[] colorImageBuffer = null;

                                                if (lprInfo[3] != "")
                                                {
                                                    try
                                                    {
                                                        string[] auxImage = lprInfo[3].Split(',');
                                                        colorImageBuffer = new byte[auxImage.Length];

                                                        for (int k = 0; k < auxImage.Length; k++)
                                                        {
                                                            colorImageBuffer[k] = byte.Parse(auxImage[k]);
                                                        }
                                                    }
                                                    catch
                                                    {
                                                    }
                                                }
                                                if (lprInfo[4] != "")
                                                {
                                                    try
                                                    {
                                                        string[] auxImage = lprInfo[4].Split(',');
                                                        bWImageBuffer = new byte[auxImage.Length];

                                                        for (int k = 0; k < auxImage.Length; k++)
                                                        {
                                                            bWImageBuffer[k] = byte.Parse(auxImage[k]);
                                                        }
                                                    }
                                                    catch
                                                    {
                                                    }
                                                }


                                                //GENERATE ALARM
                                                AlarmLprData alarmLpr = new AlarmLprData()
                                                {
                                                    Plate = lprInfo[5],
                                                    VehicleRequest = request,
                                                    Lpr = lpr,
                                                    StartDate = DateTime.Now,
                                                    BWImage = bWImageBuffer,
                                                    ColorImage = colorImageBuffer
                                                };

                                                lprAlarms.Add(lprInfo[5], alarmLpr);
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion
                        }
                        catch (Exception ex)
                        { SmartLogger.Print(ex); }
                    }
                    Thread.Sleep(SmartCadConfiguration.SmartCadSection.AvlSync.Rate);
                    if (gpsList.Count > 0)
                        UpdateGPS(gpsList);
                    if (alarms.Count > 0)
                        UpdateTelemetryAlarms(alarms.Values.ToList<AlarmSensorTelemetryData>());
                    if (dataloggerValues.Count > 0)
                        SmartCadDatabase.InvokeCommitedChangedEvent(dataloggerValues, CommittedDataAction.Update, null);
                    if (lprAlarms.Count > 0)
                    {
                        UpdateLprAlarms(lprAlarms.Values.ToList<AlarmLprData>());
                        SmartCadDatabase.InvokeCommitedChangedEvent(lprAlarms, CommittedDataAction.Update, null);
                    }
                }
                catch (Exception ex)
                { SmartLogger.Print(ex); }
            }
        }

        private static bool CheckAlarm(SensorTelemetryData sensor)
        {
            bool result = false;
            
            foreach (SensorTelemetryThresholdData sensorBound in sensor.Thresholds)
            {
                if (sensorBound.High != null && sensorBound.Low != null)
                {
                    if (sensor.Value.Value <= sensorBound.Low || sensor.Value.Value >= sensorBound.High)
                        result = true;
                }
                else if (sensorBound.High == null)
                {
                    if (sensor.Value.Value >= sensorBound.Low)
                        result = true;
                }
                else if (sensorBound.Low == null)
                {
                    if (sensor.Value.Value <= sensorBound.High)
                        result = true;
                }
            }
            return result;
        }

        private static void UpdateGPS(IList gpsList)
        {
            int i = 0;
            while (i < gpsList.Count)
            {
                GPSClientData gps = (GPSClientData)gpsList[i];
                string sql = "UPDATE GPS SET LONGITUDE = " + gps.Lon +
                                ",LATITUDE = " + gps.Lat +
                                ",ALTITUDE = " + gps.Alt +
                                ",SPEED = " + gps.Speed +
                                ",SATELLITES = " + gps.Satellites +
                                ",HEADING = " + gps.Heading +
                                ",SYNCHRONIZED = 1, COORDINATES_DATE = '" + gps.CoordinatesDate.ToString(ApplicationUtil.DataBaseFormattedDate) +
                                "' WHERE PARENT_CODE in (SELECT CODE FROM DEVICE WHERE NAME = '" + gps.Name + "')";

                lock (syncToWrite)
                {
                    try
                    {
                        SmartCadDatabase.UpdateSqlWithOutValidator(sql);
                        Object code = SmartCadDatabase.SearchBasicObject("SELECT unit.Code FROM UnitData unit WHERE unit.GPS.Name = '" + gps.Name + "'");
                        if (code != null)
                        {
                            gps.UnitCode = (int)code;
                        }
                        else
                        {
                            gpsList.RemoveAt(i);
                            i--;
                        }
                    }
                    catch (Exception ex)
                    {
                        //SmartLogger.Print(ex);
                        gpsList.RemoveAt(i);
                        i--;
                    }
                }
                i++;
            }

            if (gpsList.Count > 0)
                SmartCadDatabase.InvokeCommitedChangedEvent(gpsList, CommittedDataAction.Update, null);
        }

        private static void UpdateTelemetryAlarms(List<AlarmSensorTelemetryData> alarms)
        {
            lock (syncToWrite)
            {
                try
                {

                    SmartCadDatabase.SaveOrUpdateCollection(alarms, null);
                }
                catch { }
            }
        }

        private static void UpdateLprAlarms(List<AlarmLprData> alarms)
        {
            lock (syncToWrite)
            {
                try
                {

                    SmartCadDatabase.SaveOrUpdateCollection(alarms, null);
                }
                catch { }
            }
        }
        #endregion 
    }

    public static class DeviceAlertService
    {
        private static Thread checkDataLessAlert;
        private static bool runCheckDataLessAlert;
		private static List<AlertNotificationData> notEndedAlerts = new List<AlertNotificationData>();
		private static Dictionary<string, ApplicationPreferenceData> appPreference = new Dictionary<string, ApplicationPreferenceData>();
		
		public static void Start()
		{
			DeviceServerService.Current.ArriveData += new EventHandler<CommittedObjectDataCollectionEventArgs>(Current_ArriveData);
			ServiceHelper.AlertNotificationUpdate += new EventHandler<CommittedDataEventArgs>(ServiceHelper_AlertNotificationUpdate);
			runCheckDataLessAlert = true;
			Dictionary<string, IList> querys = new Dictionary<string, IList>();
			querys["AlertNotificationData"] = new ArrayList();
			querys["AlertNotificationData"].Add(SmartCadHqls.GetCustomHql(
									"Select alert From AlertNotificationData alert Where alert.Status != {0}",
									(int)AlertNotificationData.AlertStatus.Ended));			
			querys["apps"] = new ArrayList();
			querys["apps"].Add(SmartCadHqls.GetAllApplicationPreferences);
			querys = SmartCadDatabase.SearchObjects(0,int.MaxValue,querys,UserApplicationData.Map,false);
			notEndedAlerts = querys["AlertNotificationData"].Cast<AlertNotificationData>().ToList();

			foreach (ApplicationPreferenceData item in querys["apps"])
				appPreference.Add(item.Name, item);
			
			checkDataLessAlert = new Thread(new ThreadStart(CheckDataLessAlert));
			checkDataLessAlert.Start();
		}

		static void ServiceHelper_AlertNotificationUpdate(object sender, CommittedDataEventArgs e)
		{
			if (e.ObjectData is AlertNotificationData)
			{
				AlertNotificationData and = e.ObjectData as AlertNotificationData;
				int index = notEndedAlerts.IndexOf(and);
				if (index >= 0)
					if (and.Status != AlertNotificationData.AlertStatus.Ended)
						notEndedAlerts[index] = and;
					else
						notEndedAlerts.RemoveAt(index);
				else if (and.Status != AlertNotificationData.AlertStatus.Ended)
					notEndedAlerts.Add(and);
			}
			else if (e.ObjectData is ApplicationPreferenceData)
			{
				ApplicationPreferenceData preference = e.ObjectData as ApplicationPreferenceData;
				if (appPreference.ContainsKey(preference.Name))
					appPreference[preference.Name] = preference;				
			}
		}

        public static void Stop()
        {
            DeviceServerService.Current.ArriveData -= new EventHandler<CommittedObjectDataCollectionEventArgs>(Current_ArriveData);
            runCheckDataLessAlert = false;
            try
            {
                checkDataLessAlert.Abort();
            }
            catch { }
        }
        
        static void Current_ArriveData(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                string[] frame = e.Objects[0].ToString().Split('|');
                if (frame[0] == "GPS")
                {
                    GPSUnitData gpsUnit = new GPSUnitData(e.Objects[0].ToString());
                    UnitData unit = SmartCadDatabase.SearchObjectData(
                                        SmartCadHqls.GetCustomHql(
                                            SmartCadHqls.GetUnitByGpsNameWithUnitAlerts,
                                            gpsUnit.idGPS)) as UnitData;
                    if (unit != null)
                    {
                        if (SmartCadDatabase.IsInitialize(unit.WorkShiftRoutes) == false)
                            SmartCadDatabase.InitializeLazy(unit, unit.WorkShiftRoutes);

                        UnitAlertData unitAlert = null;
                        if ((unitAlert = CheckSpeedLimit(unit, gpsUnit)) != null)
                            CreateAlert(unitAlert);
                        if ((unitAlert = CheckExceededTimeOnStopOrUnknownStop(unit, gpsUnit, true)) != null)
                            CreateAlert(unitAlert);
                        if ((unitAlert = CheckExceededTimeOnStopOrUnknownStop(unit, gpsUnit, false)) != null)
                            CreateAlert(unitAlert);
                        if ((unitAlert = CheckIOAlert(unit, gpsUnit)) != null)
                            CreateAlert(unitAlert, gpsUnit);
                        if ((unitAlert = CheckUnitInOrOutOfRoute(unit, gpsUnit, true)) != null)
                            CreateAlert(unitAlert);
                        if ((unitAlert = CheckUnitInOrOutOfRoute(unit, gpsUnit, false)) != null)
                            CreateAlert(unitAlert);
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

		private static UnitAlertData CheckUnitInOrOutOfRoute(UnitData unit, GPSUnitData gpsUnit, bool inRoute)
		{
			UnitAlertData unitAlert = null;
			try
			{
                IEnumerable<UnitAlertData> alerts = null; 
                if (inRoute == true)
                {
                    alerts = unit.Alerts.Cast<UnitAlertData>().Where(uAlert => uAlert.Alert.CustomCode.Equals("IN_ROUTE"));                    
                }
                else
                {
                    alerts = unit.Alerts.Cast<UnitAlertData>().Where(uAlert => uAlert.Alert.CustomCode.Equals("OFF_ROUTE"));                    
                }
                if (alerts.Count<UnitAlertData>() > 0)
                {
                    unitAlert = alerts.ElementAt(0);
                    if (GetExistingAlert(unitAlert) != null || unitAlert.Active == false)
                        return null;
                }
                else
                {
                    return null;
                }
			}
			catch
			{
				unitAlert = null;
			}
			if (unit.WorkShiftRoutes.Count > 0)
			{
				WorkShiftRouteData route = ServiceUtil.GetCurrentRoute(unit);
				if (route != null && (inRoute == false || inRoute != route.InRoute))
				{
					if (SmartCadDatabase.IsInitialize(route.Route.RouteAddress) == false)
						SmartCadDatabase.InitializeLazy(route.Route, route.Route.RouteAddress);
					
					if (route.Route.Type == RouteData.RouteType.Area)
					{
						List<GeoPoint> list = route
												.Route
												.RouteAddress
												.Cast<RouteAddressData>()
												.Select<RouteAddressData,GeoPoint>(add => new GeoPoint(add.Address.Lon,add.Address.Lat))
												.ToList();
						
						bool checkValue = ApplicationUtil.PointInPolygon(new GeoPoint(unit.GPS.Longitude, unit.GPS.Latitude), list);
						if (checkValue == true && inRoute == true)
						{
							route.InRoute = inRoute;
							SmartCadDatabase.SaveOrUpdate(route);
							return unitAlert;
						}
						else if (checkValue == true && inRoute == false)
							return null;
						
					}
                    
					for (int i = 0; i < route.Route.RouteAddress.Count - 1; i++)
					{
						RouteAddressData p1 = route.Route.RouteAddress.Cast<RouteAddressData>().ElementAt(i);
						RouteAddressData p2 = route.Route.RouteAddress.Cast<RouteAddressData>().ElementAt(i + 1);

						bool checkValue = ServiceUtil.CheckUnitInside2PointsRoute(unit, p1, p2, double.Parse(appPreference["ToleranceForRoutes"].Value));
						if (checkValue == true &&
							inRoute == true)
						{
							route.InRoute = inRoute;
							SmartCadDatabase.SaveOrUpdate(route);
							return unitAlert;
						}
						else if (checkValue == true && inRoute == false)
							return null;
					}
					if (inRoute == false)
					{
						route.InRoute = inRoute;
						SmartCadDatabase.SaveOrUpdate(route);
						return unitAlert;
					}
				}
			}
			return null;
		}

		private static AlertNotificationData GetExistingAlert(UnitAlertData unitAlert)
		{
			for (int i = 0; i < notEndedAlerts.Count; i++)
			{
				AlertNotificationData al = notEndedAlerts[i];
				if (al.UnitAlert.Code == unitAlert.Code)
					return al;
			}
			return null;
		}

		private static UnitAlertData CheckIOAlert(UnitData unit, GPSUnitData gpsUnit)
		{
			UnitAlertData unitAlert = null;
			try
			{
				unitAlert = unit.Alerts.Cast<UnitAlertData>().Where(uAlert => uAlert.Alert.CustomCode.Equals("GPS_I/O")).ElementAt(0);
				if (unitAlert.Active == false)
					return null;
			}
			catch
			{
				return null;
			}
			if (gpsUnit.inputs.Count > 0 ||
				gpsUnit.outputs.Count > 0)
				return unitAlert;
			return null;
		}		

		private static UnitAlertData CheckSpeedLimit(UnitData unit, GPSUnitData gpsUnit)
		{
			UnitAlertData unitAlert = null;
			try
			{
				unitAlert = unit.Alerts.Cast<UnitAlertData>().Where(uAlert => uAlert.Alert.CustomCode.Equals("SPEED_LIMIT")).ElementAt(0);
                if (GetExistingAlert(unitAlert) != null || unitAlert.Active == false)
                    return null;
			}
			catch
			{
				return null;
			}
			if (unitAlert != null && unit != null && unit.Velocity < gpsUnit.speed)
				return unitAlert;

			return null;
		}

		private static UnitAlertData CheckExceededTimeOnStopOrUnknownStop(UnitData unit, GPSUnitData gpsUnit, bool exceeded)
		{
			UnitAlertData unitAlert = null;
			try
			{
                if (exceeded == true)
					unitAlert = unit.Alerts.Cast<UnitAlertData>().Where(uAlert => uAlert.Alert.CustomCode.Equals("STOP_TIME_LIMIT")).ElementAt(0);
				else
					unitAlert = unit.Alerts.Cast<UnitAlertData>().Where(uAlert => uAlert.Alert.CustomCode.Equals("UNSPECIFIED_STOP")).ElementAt(0);
                
				if (GetExistingAlert(unitAlert) != null || unitAlert.Active == false)
                    return null;
			}
			catch
			{
				return null;
			}
			if (unit.WorkShiftRoutes.Count > 0)
			{
				WorkShiftRouteData route = ServiceUtil.GetCurrentRoute(unit);
				if (route != null)
				{
                    int minTime = exceeded ? route.Route.StopTime * 60 + route.Route.StopTimeTol * 60 : route.Route.StopTime * 60;
                    int maxTime = exceeded ? minTime + 1 : route.Route.StopTime * 60 + route.Route.StopTimeTol * 60;
					GPSUnitData result = ParserServiceClient.Current.CheckPointForStop(gpsUnit, minTime, maxTime);
					
                    if (result!=null)
					{
						if (SmartCadDatabase.IsInitialize(route.Route.RouteAddress) == false)
							SmartCadDatabase.InitializeLazy(route.Route, route.Route.RouteAddress);

						RouteAddressData address = ServiceUtil.GetNearestStop(result.lat, result.lon, route.Route.RouteAddress);
						if (address == null)
							return null;
						
						double distance = GeoCodeCalc.CalcDistance(address.Address.Lat, address.Address.Lon, result.lat, result.lon);
						bool valueToCheck = distance < double.Parse(appPreference["ToleranceForStopApproximation"].Value) / 1000;
						if (valueToCheck && exceeded)
							return unitAlert;
						else if (!valueToCheck && !exceeded)
							return unitAlert;
					}
				}
			}
			return null;
		}

        public static void CheckDataLessAlert()
        {
            while (runCheckDataLessAlert)
            {
				if (notEndedAlerts.Count > 0)
					for(int i=0;i<notEndedAlerts.Count;i++)
					{
						AlertNotificationData alert = notEndedAlerts.ElementAt(i);
						if (alert.Status == AlertNotificationData.AlertStatus.Ended)
						{
							notEndedAlerts.Remove(alert);
							i--;
						}
					}

				IList list = SmartCadDatabase.SearchObjects(
								SmartCadHqls.GetCustomHql(
									"Select unitAlert From UnitAlertData unitAlert Where unitAlert.Alert.CustomCode = '{0}'",
									"WITHOUT_COMMUNICATION"));

				foreach (UnitAlertData unitAlert in list)
				{
					if (unitAlert.Unit.GPS != null)
					{
						double minOnSilence = SmartCadDatabase.GetTimeFromBD().Subtract(unitAlert.Unit.GPS.CoordinatesDate).TotalMinutes;
					
						if (minOnSilence > double.Parse(appPreference["MaxWaitTimeForCommunicationWithGPS"].Value) && (GetExistingAlert(unitAlert) == null))
							CreateAlert(unitAlert);
					}
				}
				Thread.Sleep(10000);	
            }
        }

		private static void CreateAlert(UnitAlertData unitAlert, GPSUnitData gpsUnit)
		{
			AlertNotificationData alert = new AlertNotificationData();
			alert.Date = SmartCadDatabase.GetTimeFromBD();
			alert.Status = AlertNotificationData.AlertStatus.NotTaken;
			alert.UnitAlert = unitAlert;
			notEndedAlerts.Add(alert);
			if (SmartCadDatabase.IsInitialize(unitAlert.Unit.GPS.Sensors) == false)
				SmartCadDatabase.InitializeLazy(unitAlert.Unit.GPS, unitAlert.Unit.GPS.Sensors);

			foreach (int o in gpsUnit.outputs)
			{
				foreach (GPSPinSensorData pinSensor in unitAlert.Unit.GPS.Sensors)
				{
					if (pinSensor.Pin.Number == o &&
						(pinSensor.Pin.Type == GPSPinData.PinType.Output ||
						pinSensor.Pin.Type == GPSPinData.PinType.Input_Output) &&
						pinSensor.Sensor.Type == SensorData.SensorType.Output)
					{
						alert.Sensor = pinSensor.Sensor;
						if (ExistsAlert(alert))
							SmartCadDatabase.SaveOrUpdate(alert);
					}
				}
			}
			foreach (int i in gpsUnit.inputs)
			{
				foreach (GPSPinSensorData pinSensor in unitAlert.Unit.GPS.Sensors)
				{
					if (pinSensor.Pin.Number == i &&
						(pinSensor.Pin.Type == GPSPinData.PinType.Input ||
						pinSensor.Pin.Type == GPSPinData.PinType.Input_Output) &&
						pinSensor.Sensor.Type == SensorData.SensorType.Input)
					{
						alert.Sensor = pinSensor.Sensor;
						if (ExistsAlert(alert))
							SmartCadDatabase.SaveOrUpdate(alert);
					}
				}
			}
		}

		private static bool ExistsAlert(AlertNotificationData alert)
		{
			foreach (AlertNotificationData al in notEndedAlerts)
			{
				if (al.UnitAlert.Code == alert.UnitAlert.Code &&
					al.Sensor != null && al.Sensor.Code == alert.Sensor.Code)
					return true;
			}
			return false;
		}

		private static void CreateAlert(UnitAlertData unitAlert)
		{
            try
            {
                AlertNotificationData alert = GetExistingAlert(unitAlert);
                if (alert == null)
                {
                    alert = new AlertNotificationData();
                    alert.Date = SmartCadDatabase.GetTimeFromBD();
                    alert.Status = AlertNotificationData.AlertStatus.NotTaken;
                    alert.UnitAlert = unitAlert;
                    notEndedAlerts.Add(alert);
                    SmartCadDatabase.SaveOrUpdate(alert);
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
		}
    }
}
