﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.Net.Security;

namespace Smartmatic.SmartCad.Service
{
    [ServiceContract(ProtectionLevel = ProtectionLevel.None)]
    public interface IDeviceServerService
    {
        [OperationContract()]
        void Ping();

        [OperationContract()]
        void SendData(object obj);
    }
}

