using System;
using System.Collections.Generic;
using System.Text;

using System.Collections;

using System.Threading;

using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using NHibernate;

using System.IO;
using NAudio.Wave;
using SmartCadCore.Model;
using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadCore.Statistic;
using SmartCadCore.ClientData;
using SmartCadCore.Core.Util;

namespace Smartmatic.SmartCad.Service
{
    public class ServiceHelper : IDisposable
    {
        private const int PING_FAILED_TIMES = 3;
        private int committedChangesSleep = 0;
        private Thread threadStartCheckOperators;
        private Thread threadStartCheckOperatorsCheck = null;
        private Thread threadStartCheckAssignedIncidentNotification = null;
        private Timer timerCheckWorkingOperators;
        private Dictionary<string, Dictionary<string, ClientServiceData>> serverServiceLogIn = null;
        private object syncserverServiceLogIn = new object();
        private int MAX_NOTIFICATIONS = 2;
        private bool processingOperators = false;
        public static event EventHandler<CommittedDataEventArgs> AlertNotificationUpdate;
        private FileSystemWatcher watcher = null;
        AsteriskStatWrapper AsteriskWrapper = new AsteriskStatWrapper();
        public string ConnectionString;

        public void Start()
        {
            SmartLogger.Print(ResourceLoader.GetString2("AdjustingDataBase"));

            serverServiceLogIn = new Dictionary<string, Dictionary<string, ClientServiceData>>();

            FixDatabase();

            committedChangesSleep = SmartCadConfiguration.SmartCadSection.ServerElement.InternalServerAddressElement.CommittedChangedSleep;

            SmartCadDatabase.CommittedChanges += new EventHandler<CommittedDataEventArgs>(SmartCadDatabase_CommittedChanges);

            threadStartCheckOperatorsCheck = new Thread(new ThreadStart(RunCheckLoggedOperators));
            threadStartCheckOperatorsCheck.Start();

            threadStartCheckOperators = new Thread(new ParameterizedThreadStart(StartCreatingClientConexions));
            threadStartCheckOperators.Name = "StartCreatingClientConexions";
            threadStartCheckOperators.Start(true);

            StartCheckWorkingOperators();

            threadStartCheckAssignedIncidentNotification = new Thread(new ThreadStart(RunCheckAssignedIncidentNotifications));
            threadStartCheckAssignedIncidentNotification.Start();
            StatisticsConfiguration.Load();
            StartTelephonyStatManager();

            //File watcher to monitor the file coming from the clients (Audio recordings, etc)
            watcher = new FileSystemWatcher();
            watcher.Path = Environment.CurrentDirectory;
            watcher.NotifyFilter = NotifyFilters.CreationTime | NotifyFilters.LastAccess | NotifyFilters.LastWrite;
            watcher.Filter = "*.wav";
            watcher.Created += OnChanged;
            watcher.Changed += OnChanged;
            watcher.EnableRaisingEvents = true;
        }

        public void Stop()
        {
            try
            {
                SmartCadDatabase.CommittedChanges -= new EventHandler<CommittedDataEventArgs>(SmartCadDatabase_CommittedChanges);
                NortelRTDWrapper.Stop();
                AvayaStatWrapper.Stop();

                threadStartCheckOperatorsCheck.Abort();
                threadStartCheckOperators.Abort();
                threadStartCheckAssignedIncidentNotification.Abort();
                timerCheckWorkingOperators.Dispose();

                watcher.EnableRaisingEvents = false;
                watcher.Dispose();
            }
            catch
            { }
        }

        private void OnChanged(object source, FileSystemEventArgs e)
        {
            try
            {
                ApplicationPreferenceData preference = SmartCadDatabase.SearchBasicObject(
                SmartCadHqls.GetCustomHql(SmartCadHqls.GetApplicationPreferenceByName, "VoiceRecordingPath")) as ApplicationPreferenceData;

                String fileName = DateTime.Now.ToString("yyyyMMdd_HHmmss") + "_" + e.Name;
                FileInfo fi = new FileInfo(e.FullPath);
                fi.MoveTo(preference.Value + fileName);

                PhoneReportAudioData phad = new PhoneReportAudioData()
                {
                    CustomCode = e.Name.Replace(".wav", ""),
                    FileName = fileName,
                    StorageFolder = preference.Value
                };
                //long count = (long)SmartCadDatabase.SearchBasicObject("select count(*) from PhoneReportAudioData data where data.CustomCode = '" + phad.CustomCode + "'");
                //if (count == 0)

                SmartCadDatabase.SaveOrUpdate(phad);
                fi = null;
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }


        private void StartTelephonyStatManager()
        {
            if (SmartCadConfiguration.SmartCadSection.ServerElement.TelephonyServerStatisticElement.ServerType == TelephonyServerTypes.Nortel.ToString())
            {
                if (string.IsNullOrEmpty(SmartCadConfiguration.SmartCadSection.ServerElement.TelephonyServerStatisticElement.Server) == true ||
                    string.IsNullOrEmpty(SmartCadConfiguration.SmartCadSection.ServerElement.TelephonyServerStatisticElement.ServerLogin) == true ||
                    string.IsNullOrEmpty(SmartCadConfiguration.SmartCadSection.ServerElement.TelephonyServerStatisticElement.ServerPassword) == true ||
                    string.IsNullOrEmpty(SmartCadConfiguration.SmartCadSection.ServerElement.TelephonyServerStatisticElement.Script) == true)
                    return;
            }
            ThreadPool.QueueUserWorkItem(new WaitCallback(StartStatisticWrapper), null);
        }

        private void StartStatisticWrapper(object obj)
        {
            bool run = false;

            if (SmartCadConfiguration.SmartCadSection.ServerElement.TelephonyServerStatisticElement.ServerType ==
                TelephonyServerTypes.Nortel.ToString())
            {
                while (run == false)
                {
                    try
                    {
                        string[] parameters = new string[] 
                        { 
                            SmartCadConfiguration.SmartCadSection.ServerElement.TelephonyServerStatisticElement.Server, 
                            SmartCadConfiguration.SmartCadSection.ServerElement.TelephonyServerStatisticElement.ServerLogin, 
                            SmartCadConfiguration.SmartCadSection.ServerElement.TelephonyServerStatisticElement.ServerPassword, 
                            "5",
                            SmartCadConfiguration.SmartCadSection.ServerElement.TelephonyServerStatisticElement.Script,
                            ((int)NRTD_Values.NIrtd_INTRVL_APPL).ToString(),
                            ((int)NRTD_Values.NIrtd_APPL_APPL_ID).ToString(), 
                            ((int)NRTD_Values.NIrtd_APPL_CALLS_OFFER).ToString(),
                            ((int)NRTD_Values.NIrtd_APPL_CALLS_ANS).ToString(),
                            ((int)NRTD_Values.NIrtd_APPL_CALLS_ANS_AFT_THRESHOLD).ToString(),
                            ((int)NRTD_Values.NIrtd_APPL_CALLS_ABAN).ToString(),
                            ((int)NRTD_Values.NIrtd_APPL_CALLS_ABAN_AFT_THRESHOLD).ToString(),
                            ((int)NRTD_Values.NIrtd_APPL_CALLS_WAITING).ToString(),
                            ((int)NRTD_Values.NIrtd_APPL_MAX_WAITING_TIME).ToString()
                        };

                        uint result = NortelRTDWrapper.Start(parameters);
                        if (result == 0)
                        {
                            SmartLogger.Print(ResourceLoader.GetString2("TelephonyDBStarted"));
                            run = true;
                        }
                        else
                        {
                            Thread.Sleep(10000);
                            SmartLogger.Print(ResourceLoader.GetString2("TelephonyDBError"));
                        }
                    }
                    catch
                    {
                        Thread.Sleep(10000);
                        SmartLogger.Print(ResourceLoader.GetString2("TelephonyDBError"));
                        try
                        {
                            NortelRTDWrapper.Stop();
                        }
                        catch { }
                    }
                }
            }
            else if (SmartCadConfiguration.SmartCadSection.ServerElement.TelephonyServerStatisticElement.ServerType ==
                TelephonyServerTypes.Genesys.ToString())
            {

                while (run == false)
                {
                    if (GenesysStatWrapper.IsNotInitiated())
                    {
                        if (string.IsNullOrEmpty(SmartCadConfiguration.SmartCadSection.ServerElement.TelephonyServerStatisticElement.Server) ||
                            string.IsNullOrEmpty(SmartCadConfiguration.SmartCadSection.ServerElement.TelephonyServerStatisticElement.Port) ||
                            string.IsNullOrEmpty(SmartCadConfiguration.SmartCadSection.ServerElement.TelephonyServerStatisticElement.ServerLogin) ||
                            string.IsNullOrEmpty(SmartCadConfiguration.SmartCadSection.ServerElement.TelephonyServerStatisticElement.ServiceName))
                        {
                            SmartLogger.Print("Statistic layer is not properly configured. Check statistic server configuration.");
                        }
                        else
                        {
                            try
                            {
                                GenesysStatWrapper.Init(
                                    SmartCadConfiguration.SmartCadSection.ServerElement.TelephonyServerStatisticElement.Server,
                                    SmartCadConfiguration.SmartCadSection.ServerElement.TelephonyServerStatisticElement.Port,
                                    SmartCadConfiguration.SmartCadSection.ServerElement.TelephonyServerStatisticElement.ServerLogin,
                                    SmartCadConfiguration.SmartCadSection.ServerElement.TelephonyServerStatisticElement.ServerPassword,
                                    SmartCadConfiguration.SmartCadSection.ServerElement.TelephonyServerStatisticElement.ServiceName);

                                SmartLogger.Print(ResourceLoader.GetString2("Connected To Gis sucessfully"));
                                run = true;
                            }
                            catch (Exception ex)
                            {
                                SmartLogger.Print("Connection to GIS was not succesful. Trying again.");
                                Thread.Sleep(10000);
                            }
                        }
                    }
                }
            }
            else if (SmartCadConfiguration.SmartCadSection.ServerElement.TelephonyServerStatisticElement.ServerType ==
                TelephonyServerTypes.Avaya.ToString())
            {

                while (run == false)
                {
                    if (AvayaStatWrapper.IsInitiated() == false)
                    {
                        TelephonyServerStatisticElement telephonyConfig = SmartCadConfiguration.SmartCadSection.ServerElement.TelephonyServerStatisticElement;

                        if (string.IsNullOrEmpty(telephonyConfig.Script))
                        {
                            SmartLogger.Print("Statistic layer is not properly configured. Check statistic server configuration.");
                            break;
                        }
                        else
                        {
                            try
                            {
                                //Starts Historical Statistics
                                AvayaStatWrapper.Init(telephonyConfig.Script);
                                SmartLogger.Print(ResourceLoader.GetString2("Connected to Statistics successfully"));
                                run = true;
                            }
                            catch (Exception ex)
                            {
                                SmartLogger.Print(ex.Message);//"Connection to GIS was not succesful. Trying again.");
                                AvayaStatWrapper.Stop();
                                Thread.Sleep(5000);
                            }
                        }
                    }
                }
            }
            else if (SmartCadConfiguration.SmartCadSection.ServerElement.TelephonyServerStatisticElement.ServerType ==
                TelephonyServerTypes.Asterisk.ToString())
            {

                while (run == false)
                {
                    if (AsteriskStatWrapper.IsInitiated() == false)
                    {
                        TelephonyServerStatisticElement telephonyConfig = SmartCadConfiguration.SmartCadSection.ServerElement.TelephonyServerStatisticElement;

                        if (string.IsNullOrEmpty(telephonyConfig.Script))
                        {
                            SmartLogger.Print("Statistic layer is not properly configured. Check statistic server configuration.");
                            break;
                        }
                        else
                        {
                            try
                            {
                                //Starts Historical Statistics
                                string script = SmartCadConfiguration.SmartCadSection.ServerElement.TelephonyServerStatisticElement.Script;
                                String[] substrings = script.Split(';');
                                String[] port = substrings[3].Split('=');
                                String[] server = substrings[1].Split('=');
                                ConnectionString = "server=" + server[1] + "; " + substrings[6] + "; " + substrings[5] + "; port=" + port[1] + "; " + substrings[7] + "; ";
                                AsteriskStatWrapper.Init(ConnectionString);
                                SmartLogger.Print(ResourceLoader.GetString2("Connected to Statistics successfully"));
                                run = true;
                            }
                            catch (Exception ex)
                            {
                                SmartLogger.Print(ex.Message);//"Connection to GIS was not succesful. Trying again.");
                                AvayaStatWrapper.Stop();
                                Thread.Sleep(5000);
                            }
                        }
                    }
                }
            }
        }

        private void GetDueTimeAndPeriod(ref int dueTime, ref int period)
        {
            ApplicationPreferenceData preference = SmartCadDatabase.SearchBasicObject(
                SmartCadHqls.GetCustomHql(SmartCadHqls.GetApplicationPreferenceByName, "TimeCycleCheckOperators"))
                as ApplicationPreferenceData;
            if (preference != null)
            {
                period = Convert.ToInt32(preference.Value);
                dueTime = 30000;
            }
            else
            {
                dueTime = 30000;
                period = 60000 * 5;
            }
        }

        private void StartCheckWorkingOperators()
        {
            int dueTime = 0;
            int period = 90000;
            // GetDueTimeAndPeriod(ref dueTime, ref period);
            timerCheckWorkingOperators = ApplicationUtil.RecurrentExecuteMethod(
                new SimpleDelegate(delegate
                {
                    if (processingOperators == false)
                    {
                        processingOperators = true;
                        bool checkLoginMode = ServiceUtil.GetLogInMode();
                        ArrayList allOperators = null;

                        long result = 0;
                        if (checkLoginMode == true)
                        {
                            //Devuelve el n�mero de horarios de trabajo que acaban de comenzar o finalizar.
                            result = (long)SmartCadDatabase.SearchBasicObject(
                                                    SmartCadHqls.GetCustomHql(
                                                    SmartCadHqls.GetCountWorkShiftByRange,
                                                    ApplicationUtil.GetDataBaseFormattedDate(
                                                    DateTime.Now.AddMilliseconds(-period * 2)),
                                                    ApplicationUtil.GetDataBaseFormattedDate(DateTime.Now)));

                            if (result == 0)
                            {
                                //Devuelve el n�mero de asignaciones que acaban de comenzar o finalizar.
                                result = (long)SmartCadDatabase.SearchBasicObject(
                                                   SmartCadHqls.GetCustomHql(SmartCadHqls.GetCountOperatorAssignByRange,
                                                   ApplicationUtil.GetDataBaseFormattedDate(
                                                   DateTime.Now.AddMilliseconds(-period * 2)),
                                                   ApplicationUtil.GetDataBaseFormattedDate(DateTime.Now)));
                            }

                            if (result > 0)
                            {
                                allOperators = new ArrayList(OperatorScheduleManager.GetAllOperatorsNotWorkingNow());
                                ArrayList operatorsWorking = new ArrayList(OperatorScheduleManager.GetAllOperatorsWorkingNow(false));
                                foreach (int operatorCode in operatorsWorking)
                                {
                                    OperatorData userAccount = new OperatorData();
                                    userAccount.Code = operatorCode;
                                    userAccount = SmartCadDatabase.RefreshObject(userAccount) as OperatorData;
                                    if (userAccount != null && allOperators.Contains(userAccount) == false)
                                    {
                                        allOperators.Add(userAccount);
                                    }
                                    Thread.Sleep(150);
                                }

                                SmartCadDatabase_CommittedChanges(null, new CommittedDataEventArgs(new OperatorData(), CommittedDataAction.None, allOperators));

                            }
                        }

                        processingOperators = false;
                    }
                }),
                    null, null, dueTime, period);
        }

        private void RunCheckAssignedIncidentNotifications()
        {
            ApplicationPreferenceData preferenceMaxNotificationsToReassign = null;
            ApplicationPreferenceData preferenceTimeIntervalToRunReassignment = null;
            int sleepTime = 1000;
            double timeIntervalForReassignments = 1; // 1 min default            
            while (true)
            {
                try
                {
                    preferenceMaxNotificationsToReassign = SmartCadDatabase.SearchObject<ApplicationPreferenceData>(
                            SmartCadHqls.GetCustomHql(SmartCadHqls.GetApplicationPreferenceByName, "MaxNotificationsToReassign"));
                    Thread.Sleep(sleepTime);
                    MAX_NOTIFICATIONS = int.Parse(preferenceMaxNotificationsToReassign.Value);

                    preferenceTimeIntervalToRunReassignment = SmartCadDatabase.SearchObject<ApplicationPreferenceData>(
                            SmartCadHqls.GetCustomHql(SmartCadHqls.GetApplicationPreferenceByName, "TimeIntervalToRunReassignments"));
                    Thread.Sleep(sleepTime);
                    timeIntervalForReassignments = double.Parse(preferenceTimeIntervalToRunReassignment.Value);
                }
                catch
                {
                    MAX_NOTIFICATIONS = 2;
                }
                IList notifications = SmartCadDatabase.SearchObjects(SmartCadHqls.GetActiveIncidentNotificationsNotAssignedToSupervisor);
                if (notifications.Count > 0)
                {
                    ServiceUtil.AssignIncidentNotificationsToAutomaticSupervisor(notifications);
                }
                ServiceUtil.AssignIncidentNotificationToOperators(MAX_NOTIFICATIONS);
                Thread.Sleep(TimeSpan.FromMinutes(timeIntervalForReassignments));
                //SmartLogger.Print("Assigning");
            }
        }

        private void RunCheckLoggedOperators()
        {
            while (true)
            {
                try
                {
                    ArrayList applications = new ArrayList();
                    lock (syncserverServiceLogIn)
                    {
                        if (serverServiceLogIn.Keys.Count > 0)
                            applications.AddRange(serverServiceLogIn.Keys);
                    }
                    foreach (string app in applications)
                    {
                        if (serverServiceLogIn[app] != null && serverServiceLogIn[app].Keys.Count > 0)
                        {
                            ArrayList operators = new ArrayList(serverServiceLogIn[app].Keys);

                            foreach (string ope in operators)
                            {
                                int pingFailedTimes = 0;
                                bool pingResult = false;
                                while (pingFailedTimes < PING_FAILED_TIMES && pingResult == false)
                                {
                                    try
                                    {
                                        if (serverServiceLogIn.ContainsKey(app) == true &&
                                            serverServiceLogIn[app].ContainsKey(ope) == true)
                                        {
                                            serverServiceLogIn[app][ope].ClientService.OnPing();
                                        }
                                        pingResult = true;
                                    }
                                    catch (TimeoutException)
                                    {
                                        pingFailedTimes++;
                                    }
                                    catch (Exception ex)
                                    {
                                        pingFailedTimes++;
                                        //SmartLogger.Print(ex);
                                    }
                                    finally
                                    {
                                    }
                                }

                                if (pingFailedTimes == PING_FAILED_TIMES)
                                {
                                    try
                                    {
                                        SmartLogger.Print(ResourceLoader.GetString2("FailedPing", ope, app));
                                        lock (syncserverServiceLogIn)
                                        {
                                            if (serverServiceLogIn.ContainsKey(app) == true &&
                                                serverServiceLogIn[app].ContainsKey(ope) == true)
                                            {
                                                CloseSession(serverServiceLogIn[app][ope].Session);
                                                EndCloseReport(serverServiceLogIn[app][ope].Session);
                                            }
                                        }
                                    }
                                    catch (Exception newEx)
                                    {
                                        SmartLogger.Print(newEx);
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception exception)
                {
                    //SmartLogger.Print(exception);
                }
                Thread.Sleep(5000);
            }
        }

        #region Client Conexions
        /// <summary>
        /// Add every X time the sessions of the operators connected.
        /// </summary>
        private void StartCreatingClientConexions(object run)
        {
            do
            {
                try
                {
                    //Get all current session histories.
                    //This would be added to the list of session per applications.
                    IList sessions = ServiceUtil.GetAllCurrentSessionHistory();

                    foreach (SessionHistoryData session in sessions)
                    {
                        try
                        {
                            AddOrUpdateServerServiceLogIn(session);
                        }
                        catch
                        {
                            CloseSession(session);
                        }
                    }
                    Thread.Sleep(2500);
                }
                catch (Exception ex)
                {
                    SmartLogger.Print(ex);
                }
            }
            while ((bool)run);
        }

        /// <summary>
        /// Add each session to the list of session per applications
        /// </summary>
        /// <param name="session">Session of an operator in an application.</param>
        public void AddOrUpdateServerServiceLogIn(SessionHistoryData session)
        {
            lock (syncserverServiceLogIn)
            {
                if (serverServiceLogIn.ContainsKey(session.UserApplication.Name) == false)
                {
                    Dictionary<string, ClientServiceData> aux = new Dictionary<string, ClientServiceData>();
                    ClientServiceData service = new ClientServiceData() { ClientService = ClientService.GetClientService(session), Session = session };
                    aux.Add(session.UserAccount.Login, service);
                    serverServiceLogIn.Add(session.UserApplication.Name, aux);
                }
                else
                {
                    Dictionary<string, ClientServiceData> aux = serverServiceLogIn[session.UserApplication.Name];
                    if (aux.ContainsKey(session.UserAccount.Login) == false)
                    {
                        ClientServiceData service = new ClientServiceData() { ClientService = ClientService.GetClientService(session), Session = session };
                        aux.Add(session.UserAccount.Login, service);
                    }
                    else
                    {
                        //This code was added to update the SuppervisedApplicationName 
                        //when a general supervisor change from FirstLevel to Dispatch or vice-versa. AA
                        //The method is called from SetSupervisedApplicationName in ServerService.
                        aux[session.UserAccount.Login].Session = session;
                    }
                }
            }
        }

        /// <summary>
        /// Remove the session from the list of session by application
        /// </summary>
        /// <param name="serverService">Session of an operator in an application.</param>
        private void RemoveServiceLogIn(SessionHistoryData session)
        {
            RemoveServiceLogIn(session.UserApplication.Name, session.UserAccount.Login);
        }

        /// <summary>
        /// Remove the session from the list of session by application
        /// </summary>
        /// <param name="applicationName">Application name.</param>
        /// <param name="login">Login of the user.</param>
        private void RemoveServiceLogIn(string applicationName, string login)
        {
            if (ConnectedServiceLogIn(applicationName, login) == true)
            {
                lock (syncserverServiceLogIn)
                {
                    try
                    {
                        if (serverServiceLogIn[applicationName][login].ClientService.State == CommunicationState.Opened)
                            serverServiceLogIn[applicationName][login].ClientService.Close();
                    }
                    catch { }
                    serverServiceLogIn[applicationName].Remove(login);
                }
            }
            //else
            //{
            //    SmartLogger.Print(ResourceLoader.GetString2("$Message.ErrorLogin"));
            //}
        }

        /// <summary>
        /// Check if the current session is in the list of sessions per application
        /// </summary>
        /// <param name="serverService">Session of an operator in an application.</param>
        /// <returns>True if it is on the list, otherwise false.</returns>
        private bool ConnectedServiceLogIn(SessionHistoryData serverService)
        {
            return ConnectedServiceLogIn(serverService.UserApplication.Name, serverService.UserAccount.Login);
        }

        /// <summary>
        /// Check if the current session is in the list of sessions per application.
        /// </summary>
        /// <param name="applicationName">Application name to check.</param>
        /// <param name="login">Operator login to check.</param>
        /// <returns>True if it is on the list, otherwise false.</returns>
        private bool ConnectedServiceLogIn(string applicationName, string login)
        {
            lock (syncserverServiceLogIn)
            {
                bool result = false;
                if (serverServiceLogIn.ContainsKey(applicationName) == true)
                {
                    if (serverServiceLogIn[applicationName].ContainsKey(login) == true)
                    {
                        result = true;
                    }
                }
                return result;
            }
        }
        #endregion

        /// <summary>
        /// Verify that a department meets the condition to be sent to an specific application.
        /// Condition: Dispachable and that posses at least one Station.
        /// </summary>
        /// <param name="departmentTypeCode"></param>
        /// <returns></returns>
        private long CheckDepartmentConditions(int departmentTypeCode)
        {
            long sendCctvAndFrontClient = 0;
            object result = SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(
                 SmartCadHqls.CheckDepartmentTypeHasUnitsAssociated, departmentTypeCode));
            if (result != null)
            {
                sendCctvAndFrontClient = (long)result;
            }
            return sendCctvAndFrontClient;
        }

        #region Committed Changes
        void SmartCadDatabase_CommittedChanges(object sender, CommittedDataEventArgs e)
        {
            try
            {
                CommittedObjectDataCollectionEventArgs args;
                SmartCadContext context = null;
                if (e.Data != null && e.Data is SmartCadContext)
                {
                    context = (SmartCadContext)e.Data;
                }
                if (e.ObjectData == null && ((e.Data is IList && (e.Data as IList).Count == 0) || e.Data == null))
                {
                    return;
                }

                //This is when list are sent.
                if (e.ObjectData == null && e.Data is IList && (e.Data as IList).Count > 0)
                {
                    if (e.Data is IList && (e.Data as IList)[0] is GPSClientData)
                    {
                        args = new CommittedObjectDataCollectionEventArgs(e.Data as IList, e.Action, false, context);
                        OnCommittedChanges(null, args, UserApplicationData.Map.Name, UserApplicationData.Dispatch.Name);
                    }
                    else if (e.Data is IList && (e.Data as IList)[0] is DataloggerClientData)
                    {
                        args = new CommittedObjectDataCollectionEventArgs(e.Data as IList, e.Action, false, context);
                        OnCommittedChanges(null, args, UserApplicationData.Alarm.Name);
                    }
                    else if (e.Data is IList && (e.Data as IList)[0] is IndicatorGroupForecastData)
                    {
                        //This case is when we are replacing or restoring and general forecast.
                        //This case all of the forecast has to be general
                        IndicatorGroupForecastData forecast = (e.Data as IList)[0] as IndicatorGroupForecastData;
                        args = new CommittedObjectDataCollectionEventArgs(
                            SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(
                            SmartCadHqls.GetIndicatorGroupForecastGeneral,
                            forecast.General.ToString().ToLower(),
                            ApplicationUtil.GetDataBaseFormattedDate(SmartCadDatabase.GetTimeFromBD().AddMinutes(-5)))),
                            e.Action,
                            false, context);
                        OnCommittedChanges(null, args, UserApplicationData.Supervision.Name);
                    }
                    else if (e.Data is IList && (e.Data as IList).Count > 0 && (e.Data as IList)[0] is ApplicationPreferenceData)
                    {
                        args = new CommittedObjectDataCollectionEventArgs(e.Data as IList, e.Action, false, context);
                        OnCommittedChanges(null, args, UserApplicationData.Supervision.Name,
                            UserApplicationData.Administration.Name, UserApplicationData.Cctv.Name,
                            UserApplicationData.Dispatch.Name, UserApplicationData.FirstLevel.Name, UserApplicationData.Alarm.Name);
                    }
                    else if (e.Data is IList && (e.Data as IList).Count > 0 && (e.Data as IList)[0] is AlarmSensorTelemetryData)
                    {
                        args = new CommittedObjectDataCollectionEventArgs(e.Data as IList, e.Action, false, context);
                        OnCommittedChanges(null, args, UserApplicationData.Alarm.Name);
                    }
                    else if (e.Data is IList && (e.Data as IList).Count > 0 && (e.Data as IList)[0] is AlarmLprData)
                    {
                        args = new CommittedObjectDataCollectionEventArgs(e.Data as IList, e.Action, false, context);
                        OnCommittedChanges(null, args, UserApplicationData.Alarm.Name, UserApplicationData.Lpr.Name);
                    }
                    else
                    {
                        args = new CommittedObjectDataCollectionEventArgs(e.Data as IList, e.Action, false, context);
                        OnCommittedChanges(null, args, UserApplicationData.Supervision.Name);
                    }
                }
                //This is when clientdata are sent instead of objectdata
                else if (e.Data != null && e.ObjectData == null)
                {
                    if (e.Data is ApplicationClientData)
                    {
                        args = new CommittedObjectDataCollectionEventArgs(e.Data, e.Action, context);
                        OnCommittedChanges(null, args, ((ApplicationClientData)e.Data).ToApplications);
                    }
                }

                else if (e.ObjectData.GetType().Name == typeof(IncidentNotificationData).Name)
                {
                    if (e.Action == CommittedDataAction.Update)
                    {
                        args = new CommittedObjectDataCollectionEventArgs(e.ObjectData, e.Action, context);
                        OnCommittedChanges(null, args, UserApplicationData.Dispatch.Name, UserApplicationData.FirstLevel.Name,
                            UserApplicationData.Map.Name, UserApplicationData.Supervision.Name, UserApplicationData.Cctv.Name);
                    }
                }
                else if (e.ObjectData.GetType().Name == typeof(OperatorData).Name && e.Data is IList && (e.Data as IList).Count > 0)
                {
                    args = new CommittedObjectDataCollectionEventArgs(e.Data as IList, CommittedDataAction.None, true, context);
                    OnCommittedChanges(null, args, UserApplicationData.Supervision.Name);
                }
                else if (e.ObjectData.GetType().Name == typeof(DispatchOrderData).Name)
                {
                    args = new CommittedObjectDataCollectionEventArgs(e.ObjectData, e.Action, context);
                    OnCommittedChanges(null, args, UserApplicationData.Dispatch.Name, UserApplicationData.Supervision.Name);
                }
                else if (e.ObjectData.GetType().Name == typeof(SupportRequestReportData).Name)
                {
                    args = new CommittedObjectDataCollectionEventArgs(e.ObjectData, e.Action, context);
                    OnCommittedChanges(null, args, UserApplicationData.Dispatch.Name);
                }
                else if (e.ObjectData.GetType().Name == typeof(UserProfileData).Name ||
                    e.ObjectData.GetType().Name == typeof(UserRoleData).Name)
                {
                    args = new CommittedObjectDataCollectionEventArgs(e.ObjectData, e.Action, context);
                    OnCommittedChanges(null, args, UserApplicationData.Administration.Name);
                }
                else if (e.ObjectData.GetType().Name == typeof(QuestionData).Name)
                {
                    args = new CommittedObjectDataCollectionEventArgs(e.ObjectData, e.Action, context);
                    OnCommittedChanges(null, args, UserApplicationData.Administration.Name, UserApplicationData.FirstLevel.Name);
                }
                else if (e.ObjectData.GetType().Name == typeof(OfficerData).Name ||
                         e.ObjectData.GetType().Name == typeof(DepartmentStationAssignHistoryData).Name ||
                         e.ObjectData.GetType().Name == typeof(QuestionDispatchReportData).Name ||
                         e.ObjectData.GetType().Name == typeof(DispatchReportData).Name)
                {
                    args = new CommittedObjectDataCollectionEventArgs(e.ObjectData, e.Action, context);
                    OnCommittedChanges(null, args, UserApplicationData.Administration.Name, UserApplicationData.Dispatch.Name);
                }
                else if (e.ObjectData.GetType().Name == typeof(OperatorStatusData).Name)
                {
                    args = new CommittedObjectDataCollectionEventArgs(e.ObjectData, e.Action, context);
                    OnCommittedChanges(null, args, UserApplicationData.Dispatch.Name, UserApplicationData.FirstLevel.Name, UserApplicationData.Administration.Name, UserApplicationData.Supervision.Name);
                }
                else if (e.ObjectData.GetType().Name == typeof(UnitData).Name ||
                         e.ObjectData.GetType().Name == typeof(UnitTypeData).Name)
                {
                    args = new CommittedObjectDataCollectionEventArgs(e.ObjectData, e.Action, context);
                    OnCommittedChanges(null, args, UserApplicationData.Dispatch.Name, UserApplicationData.Map.Name, UserApplicationData.Administration.Name);

                    //Chequea de que organismo pueden ser generadas notificaciones.
                    if (e.ObjectData.GetType().Name == typeof(UnitData).Name)
                    {
                        if (e.Action == CommittedDataAction.Save)
                        {
                            if (CheckDepartmentConditions((e.ObjectData as UnitData).DepartmentType.Code) == 1)
                            {
                                DepartmentTypeData departmentType = (e.ObjectData as UnitData).DepartmentType;
                                args = new CommittedObjectDataCollectionEventArgs(departmentType, CommittedDataAction.Save, context);
                                OnCommittedChanges(null, args,
                                    UserApplicationData.FirstLevel.Name,
                                    UserApplicationData.Cctv.Name,
                                    UserApplicationData.Dispatch.Name,
                                    UserApplicationData.Administration.Name);
                            }
                        }
                        else if (e.Action == CommittedDataAction.Delete)
                        {
                            if (CheckDepartmentConditions((e.ObjectData as UnitData).DepartmentType.Code) == 0)
                            {
                                DepartmentTypeData departmentType = (e.ObjectData as UnitData).DepartmentType;
                                args = new CommittedObjectDataCollectionEventArgs(departmentType, CommittedDataAction.Delete, context);
                                OnCommittedChanges(null, args,
                                    UserApplicationData.FirstLevel.Name,
                                    UserApplicationData.Cctv.Name,
                                    UserApplicationData.Dispatch.Name,
                                    UserApplicationData.Administration.Name);
                            }
                        }
                        else if (e.Action == CommittedDataAction.Update)
                        {
                            if (CheckDepartmentConditions((e.ObjectData as UnitData).DepartmentType.Code) == 1)
                            {
                                DepartmentTypeData departmentType = (e.ObjectData as UnitData).DepartmentType;
                                args = new CommittedObjectDataCollectionEventArgs(departmentType, CommittedDataAction.Save, context);
                                OnCommittedChanges(null, args,
                                    UserApplicationData.FirstLevel.Name,
                                    UserApplicationData.Cctv.Name,
                                    UserApplicationData.Dispatch.Name,
                                    UserApplicationData.Administration.Name);
                            }

                            //TODO: Esto debe ser una sola llamada a los clientes.
                            IList departmentTypeList = SmartCadDatabase.SearchObjects(
                                SmartCadHqls.GetDepartmentsTypeWithoutStationsAssociated);
                            foreach (DepartmentTypeData dept in departmentTypeList)
                            {
                                args = new CommittedObjectDataCollectionEventArgs(dept, CommittedDataAction.Delete, context);
                                OnCommittedChanges(null, args,
                                    UserApplicationData.FirstLevel.Name,
                                    UserApplicationData.Cctv.Name,
                                    UserApplicationData.Dispatch.Name,
                                    UserApplicationData.Administration.Name);
                                Thread.Sleep(100);
                            }
                        }
                    }
                }
                else if (e.ObjectData.GetType().Name == typeof(GPSData).Name ||
                         e.ObjectData.GetType().Name == typeof(RouteData).Name)
                {
                    args = new CommittedObjectDataCollectionEventArgs(e.ObjectData, e.Action, context);
                    OnCommittedChanges(null, args, UserApplicationData.Map.Name, UserApplicationData.Administration.Name);
                }
                else if (e.ObjectData.GetType().Name == typeof(IncidentTypeData).Name)
                {
                    args = new CommittedObjectDataCollectionEventArgs(e.ObjectData, e.Action, context);
                    OnCommittedChanges(null, args, UserApplicationData.Dispatch.Name, UserApplicationData.FirstLevel.Name, UserApplicationData.Map.Name, UserApplicationData.Administration.Name, UserApplicationData.Cctv.Name);
                }
                else if (e.ObjectData.GetType().Name == typeof(IncidentData).Name)
                {
                    if (e.Action == CommittedDataAction.Save ||
                        e.Action == CommittedDataAction.Update)
                    {
                        IncidentData incidentData = e.ObjectData as IncidentData;
                        if (incidentData.IsEmergency == true && incidentData.Status.Name != IncidentStatusData.Closed.Name)
                        {
                            args = new CommittedObjectDataCollectionEventArgs(e.ObjectData, e.Action, context);
                            if (((e.ObjectData) as IncidentData).SourceIncidentApp == UserApplicationData.FirstLevel)
                            {
                                OnCommittedChanges(null, args, UserApplicationData.FirstLevel.Name, UserApplicationData.Dispatch.Name, UserApplicationData.Map.Name, UserApplicationData.Supervision.Name);
                            }
                            else if (((e.ObjectData) as IncidentData).SourceIncidentApp == UserApplicationData.Cctv)
                            {
                                OnCommittedChanges(null, args, UserApplicationData.Cctv.Name, UserApplicationData.Dispatch.Name, UserApplicationData.Map.Name, UserApplicationData.Supervision.Name);
                            }
                            else if (((e.ObjectData) as IncidentData).SourceIncidentApp == UserApplicationData.Alarm)
                            {
                                OnCommittedChanges(null, args, UserApplicationData.Alarm.Name, UserApplicationData.Dispatch.Name, UserApplicationData.Map.Name, UserApplicationData.Supervision.Name);
                            }
                        }
                    }
                    if (e.Action == CommittedDataAction.Delete)
                    {
                        args = new CommittedObjectDataCollectionEventArgs(e.ObjectData, e.Action, context);
                        OnCommittedChanges(null, args, UserApplicationData.Map.Name);
                    }
                }
                else if (e.ObjectData.GetType().Name == typeof(PhoneReportData).Name)
                {
                    if (e.Action == CommittedDataAction.Save ||
                        e.Action == CommittedDataAction.Update)
                    {
                        if ((e.ObjectData as PhoneReportData).Incident.IsEmergency == true &&
                            (e.ObjectData as PhoneReportData).Incident.Status.Name != IncidentStatusData.Closed.Name)
                        {
                            args = new CommittedObjectDataCollectionEventArgs(e.ObjectData, e.Action, context);

                            OnCommittedChanges(null, args, UserApplicationData.FirstLevel.Name, UserApplicationData.Dispatch.Name, UserApplicationData.Map.Name, UserApplicationData.Supervision.Name);
                        }
                    }
                }
                else if (e.ObjectData.GetType().Name == typeof(SessionHistoryData).Name ||
                    e.ObjectData.GetType().Name == typeof(SessionStatusHistoryData).Name)
                {
                    SessionHistoryData sessionHistoryData = null;
                    if (e.ObjectData is SessionStatusHistoryData)
                    {
                        sessionHistoryData = ((SessionStatusHistoryData)e.ObjectData).Session;
                    }
                    else
                    {
                        sessionHistoryData = e.ObjectData as SessionHistoryData;
                    }

                    if (sessionHistoryData.UserApplication == UserApplicationData.Dispatch && e.ObjectData is SessionHistoryData)
                    {
                        if (!sessionHistoryData.IsLoggedIn.Value)
                        {
                            ServiceUtil.AssignIncidentNotificationToOperators(MAX_NOTIFICATIONS);
                        }
                    }

                    args = new CommittedObjectDataCollectionEventArgs(sessionHistoryData.UserAccount, e.Action, context);
                    OnCommittedChanges(null, args, UserApplicationData.Supervision.Name, UserApplicationData.Dispatch.Name, UserApplicationData.FirstLevel.Name);
                    args = new CommittedObjectDataCollectionEventArgs(sessionHistoryData, CommittedDataAction.None, context);
                    OnCommittedChanges(null, args, UserApplicationData.Dispatch.Name, UserApplicationData.Supervision.Name);
                }
                else if (e.ObjectData.GetType().Name == typeof(WorkShiftVariationData).Name)
                {
                    WorkShiftVariationData ws = (WorkShiftVariationData)e.ObjectData;
                    if (ws.ObjectType == WorkShiftVariationData.ObjectRelatedType.Operators)
                    {
                        args = new CommittedObjectDataCollectionEventArgs(e.ObjectData, e.Action, context);
                        OnCommittedChanges(null, args, UserApplicationData.Supervision.Name);
                    }
                    else if (ws.ObjectType == WorkShiftVariationData.ObjectRelatedType.Units)
                    {
                        args = new CommittedObjectDataCollectionEventArgs(e.ObjectData, e.Action, context);
                        OnCommittedChanges(null, args, UserApplicationData.Administration.Name);
                    }
                }
                else if (e.ObjectData.GetType().Name == typeof(EvaluationData).Name ||
                       e.ObjectData.GetType().Name == typeof(OperatorData).Name ||
                   e.ObjectData.GetType().Name == typeof(RoomSeatData).Name ||
                   e.ObjectData.GetType().Name == typeof(RoomData).Name ||
                    e.ObjectData.GetType().Name == typeof(OperatorCategoryData).Name)
                {
                    args = new CommittedObjectDataCollectionEventArgs(e.ObjectData, e.Action, context);
                    OnCommittedChanges(null, args, UserApplicationData.Administration.Name, UserApplicationData.Supervision.Name);
                }
                else if (e.ObjectData.GetType().Name == typeof(CamerasTemplateData).Name)
                {
                    args = new CommittedObjectDataCollectionEventArgs(e.ObjectData, e.Action, context);
                    OnCommittedChanges(null, args, UserApplicationData.Administration.Name, UserApplicationData.Cctv.Name);

                }
                else if (e.ObjectData.GetType().Name == typeof(TelephonyConfigurationData).Name ||
                    e.ObjectData.GetType().Name == typeof(RadioConfigurationData).Name)
                {
                    args = new CommittedObjectDataCollectionEventArgs(e.ObjectData, e.Action, context);
                    OnCommittedChanges(null, args, UserApplicationData.Administration.Name, UserApplicationData.FirstLevel.Name, UserApplicationData.Dispatch.Name);
                }
                else if (e.ObjectData.GetType().Name == typeof(StructData).Name ||
                    e.ObjectData.GetType().Name == typeof(CameraData).Name ||
                    e.ObjectData.GetType().Name == typeof(LprData).Name ||
                    e.ObjectData.GetType().Name == typeof(CctvZoneData).Name ||
                    e.ObjectData.GetType().Name == typeof(DataloggerData).Name ||
                    e.ObjectData.GetType().Name == typeof(AlarmSensorTelemetryData).Name ||
                    e.ObjectData.GetType().Name == typeof(AlarmLprData).Name)
                {
                    args = new CommittedObjectDataCollectionEventArgs(e.ObjectData, e.Action, context);
                    OnCommittedChanges(null, args, UserApplicationData.Administration.Name,
                        UserApplicationData.Map.Name,
                        UserApplicationData.Cctv.Name,
                        UserApplicationData.Lpr.Name,
                        UserApplicationData.Alarm.Name);
                }
                else if (e.ObjectData is AlarmTwitterData || e.ObjectData is TweetData)
                {
                    args = new CommittedObjectDataCollectionEventArgs(e.ObjectData, e.Action, context);
                    OnCommittedChanges(null, args, UserApplicationData.SocialNetworks.Name);
                }
                else if
                    (e.ObjectData.GetType().Name == typeof(IndicatorResultData).Name ||
                    e.ObjectData.GetType().Name == typeof(TrainingCourseData).Name ||
                    e.ObjectData.GetType().Name == typeof(TrainingCourseScheduleData).Name ||
                    e.ObjectData.GetType().Name == typeof(OperatorTrainingCourseData).Name ||
                    e.ObjectData.GetType().Name == typeof(OperatorObservationData).Name ||
                    e.ObjectData.GetType().Name == typeof(OperatorCategoryHistoryData).Name ||
                    e.ObjectData.GetType().Name == typeof(OperatorEvaluationData).Name ||
                    e.ObjectData.GetType().Name == typeof(IndicatorData).Name ||
                    e.ObjectData.GetType().Name == typeof(OperatorAssignData).Name ||
                    e.ObjectData.GetType().Name == typeof(IndicatorGroupForecastData).Name ||
                    e.ObjectData.GetType().Name == typeof(IndicatorClassDashboardData).Name ||
                    e.ObjectData.GetType().Name == typeof(SupervisorCloseReportData).Name)
                {
                    args = new CommittedObjectDataCollectionEventArgs(e.ObjectData, e.Action, context);
                    OnCommittedChanges(null, args, UserApplicationData.Supervision.Name);
                }
                else if (e.ObjectData.GetType().Name == typeof(DepartmentStationData).Name)
                {
                    args = new CommittedObjectDataCollectionEventArgs(e.ObjectData, e.Action, context);
                    OnCommittedChanges(null, args, UserApplicationData.Supervision.Name,
                        UserApplicationData.Administration.Name, UserApplicationData.Cctv.Name,
                        UserApplicationData.Dispatch.Name, UserApplicationData.FirstLevel.Name,
                        UserApplicationData.Map.Name);
                }
                else if (e.ObjectData.GetType().Name == typeof(DepartmentTypeData).Name)
                {
                    if (e.Action == CommittedDataAction.Save)
                    {
                        args = new CommittedObjectDataCollectionEventArgs(e.ObjectData, e.Action, context);
                        OnCommittedChanges(null, args, UserApplicationData.Supervision.Name,
                            UserApplicationData.Administration.Name, UserApplicationData.Map.Name);
                    }
                    else
                    {
                        if (((DepartmentTypeData)e.ObjectData).Dispatch == false)
                        {
                            args = new CommittedObjectDataCollectionEventArgs(e.ObjectData, CommittedDataAction.Delete, context);
                            OnCommittedChanges(null, args, UserApplicationData.FirstLevel.Name, UserApplicationData.Cctv.Name);

                            args = new CommittedObjectDataCollectionEventArgs(e.ObjectData, CommittedDataAction.Update, context);
                            OnCommittedChanges(null, args, UserApplicationData.Administration.Name, UserApplicationData.Map.Name);
                        }
                        else
                        {
                            args = new CommittedObjectDataCollectionEventArgs(e.ObjectData, e.Action, context);

                            if (CheckDepartmentConditions((e.ObjectData as DepartmentTypeData).Code) == 1)
                            {
                                OnCommittedChanges(null, args, UserApplicationData.Supervision.Name,
                                  UserApplicationData.Administration.Name, UserApplicationData.Dispatch.Name,
                                  UserApplicationData.FirstLevel.Name, UserApplicationData.Cctv.Name, UserApplicationData.Map.Name);
                            }
                            else
                            {
                                OnCommittedChanges(null, args, UserApplicationData.Supervision.Name,
                                    UserApplicationData.Administration.Name, UserApplicationData.Map.Name);
                            }
                        }
                    }
                }
                else if (e.ObjectData.GetType().Name == typeof(ApplicationPreferenceData).Name ||
                                e.ObjectData.GetType().Name == typeof(DepartmentZoneData).Name ||
                                e.ObjectData.GetType().Name == typeof(RankData).Name ||
                                e.ObjectData.GetType().Name == typeof(PositionData).Name)
                {
                    ThreadPool.QueueUserWorkItem(delegate(object state)
                    {
                        if (AlertNotificationUpdate != null)
                        {
                            AlertNotificationUpdate(null, new CommittedDataEventArgs(e.ObjectData, e.Action));
                        }
                    });
                    args = new CommittedObjectDataCollectionEventArgs(e.ObjectData, e.Action, context);
                    OnCommittedChanges(null, args, UserApplicationData.Supervision.Name,
                        UserApplicationData.Administration.Name, UserApplicationData.Cctv.Name,
                        UserApplicationData.Dispatch.Name, UserApplicationData.FirstLevel.Name,
                        UserApplicationData.Map.Name, UserApplicationData.Alarm.Name);
                }
                else if (e.ObjectData is AlertNotificationData)
                {
                    ThreadPool.QueueUserWorkItem(delegate(object state)
                    {
                        if (AlertNotificationUpdate != null)
                        {
                            AlertNotificationUpdate(null, new CommittedDataEventArgs(e.ObjectData, e.Action));
                        }
                    });
                    args = new CommittedObjectDataCollectionEventArgs(e.ObjectData, e.Action, context);
                    OnCommittedChanges(null, args, UserApplicationData.Map.Name);
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private void OnCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e, params string[] applications)
        {
            try
            {
                foreach (string application in applications)
                {
                    if (serverServiceLogIn.ContainsKey(application) == true && serverServiceLogIn[application].Count > 0)
                    {
                        UserApplicationData app = UserApplicationData.GetUserApplicationByName(application);
                        ClientData clientClientData = null;
                        IList clientDataList = null;

                        if (e.Objects.Count > 0 && e.Objects[0] is ObjectData)
                        {
                            if (e.UpdatingWorkshift == false && e.Objects.Count > 1)
                            {
                                clientDataList = ApplyConversion(e.Objects, app);
                            }
                            else if (e.UpdatingWorkshift == false)
                            {
                                clientClientData = ApplyConversion(e.Objects[0] as ObjectData, app);
                            }
                            else
                            {
                                clientDataList = ApplyConversion(e.Objects, app);
                            }
                        }
                        else if (e.Objects.Count > 0 && e.Objects[0] is ClientData)
                        {
                            clientDataList = e.Objects;
                        }
                        else
                            continue;

                        //Hacer if para determinar si hay que procesar el grupo de objetos o uno slo(OperatorData)
                        //Puede que este conversion no vaya ac

                        CommittedObjectDataCollectionEventArgs eventArgs = new CommittedObjectDataCollectionEventArgs(null, e.Action, e.Context);

                        bool callClient = false;
                        if (e.Objects[0] is IncidentNotificationData)
                        {
                            #region IncidentNotificationData
                            IncidentNotificationData incidentNotification = e.Objects[0] as IncidentNotificationData;

                            if (application == UserApplicationData.Dispatch.Name)
                            {
                                callClient = true;
                                eventArgs.Objects.Add(clientClientData);
                            }
                            else if (application == UserApplicationData.FirstLevel.Name)
                            {
                                if (incidentNotification.ReportBase.Incident.Status.Name == IncidentStatusData.Closed.Name)
                                {
                                    callClient = true;
                                    eventArgs = new CommittedObjectDataCollectionEventArgs(null, CommittedDataAction.Delete, e.Context);
                                    eventArgs.Objects.Add(clientClientData);
                                }
                            }
                            else if (application == UserApplicationData.Map.Name)
                            {
                                if (incidentNotification.ReportBase.Incident.Status.Name == IncidentStatusData.Closed.Name)
                                {
                                    callClient = true;
                                    eventArgs = new CommittedObjectDataCollectionEventArgs(null, CommittedDataAction.Delete, e.Context);
                                    eventArgs.Objects.Add(clientClientData);
                                }
                            }
                            else if (application == UserApplicationData.Supervision.Name)
                            {
                                callClient = true;
                                eventArgs = new CommittedObjectDataCollectionEventArgs(null, e.Action, e.Context);
                                eventArgs.Objects.Add(clientClientData);
                            }
                            else if (application == UserApplicationData.Cctv.Name)
                            {
                                if (incidentNotification.ReportBase.Incident.Status.Name == IncidentStatusData.Closed.Name)
                                {
                                    callClient = true;
                                    eventArgs = new CommittedObjectDataCollectionEventArgs(null, CommittedDataAction.Delete, e.Context);
                                    eventArgs.Objects.Add(clientClientData);
                                }
                            }
                            #endregion
                        }
                        else if (e.Objects[0] is DispatchOrderData)
                        {
                            #region DispatchOrderData
                            DispatchOrderData dispatchOrderData = e.Objects[0] as DispatchOrderData;
                            if (application == UserApplicationData.Dispatch.Name ||
                                application == UserApplicationData.Supervision.Name)
                            {
                                IncidentNotificationData incidentNotification =
                                     SmartCadDatabase.SearchObject<IncidentNotificationData>(SmartCadHqls.GetCustomHql(SmartCadHqls.GetIncidentNotificationByCustomCode, dispatchOrderData.IncidentNotification.CustomCode));

                                callClient = true;
                                if (application == UserApplicationData.Dispatch.Name)
                                {
                                    eventArgs.Objects.Add(clientClientData);
                                }
                                eventArgs.Objects.Add(IncidentNotificationConversion.ToClient(incidentNotification, app));
                            }
                            #endregion
                        }
                        else if (e.Objects[0] is PhoneReportData)
                        {
                            #region PhoneReportData
                            PhoneReportData phoneReportData = e.Objects[0] as PhoneReportData;
                            if (application == UserApplicationData.Dispatch.Name ||
                                application == UserApplicationData.FirstLevel.Name)
                            {
                                callClient = true;
                                eventArgs = new CommittedObjectDataCollectionEventArgs(
                                    null, CommittedDataAction.Update, e.Context);
                                eventArgs.Objects.Add(clientClientData);
                            }
                            else if (application == UserApplicationData.Map.Name)
                            {
                                IncidentData inc = phoneReportData.Incident;
                                clientClientData = ApplyConversion(inc, app);
                                callClient = true;
                                eventArgs.Objects.Add(clientClientData);
                            }
                            else
                            {
                                callClient = true;
                                eventArgs.Objects.Add(clientClientData);
                            }
                            #endregion
                        }
                        else if (e.Objects[0] is DepartmentStationAssignHistoryData)
                        {
                            #region DepartmentStationAssignHistoryData
                            DepartmentStationAssignHistoryData data = e.Objects[0] as DepartmentStationAssignHistoryData;
                            DateTime time = SmartCadDatabase.GetTimeFromBD();
                            if (data.Start >= time && data.End > time)
                            {
                                callClient = true;
                                eventArgs.Objects.Add(clientClientData);
                            }
                            else if (time >= data.End)
                            {
                                callClient = true;
                                eventArgs = new CommittedObjectDataCollectionEventArgs(
                                    null, CommittedDataAction.Delete, e.Context);
                                eventArgs.Objects.Add(clientClientData);
                            }
                            #endregion
                        }
                        else if (e.Objects[0] is SessionStatusHistoryData)
                        {
                            #region SessionStatusHistoryData
                            if (application == UserApplicationData.Supervision.Name)
                            {
                                callClient = true;
                                eventArgs.Objects.Add(clientClientData);
                            }
                            #endregion
                        }
                        else if (e.Objects[0] is SessionHistoryData)
                        {
                            #region SessionStatusHistoryData
                            if (application == UserApplicationData.Supervision.Name ||
                                application == UserApplicationData.Dispatch.Name ||
                                application == UserApplicationData.FirstLevel.Name)
                            {
                                callClient = true;
                                eventArgs.Objects.Add(clientClientData);
                            }
                            #endregion
                        }
                        else if (e.Objects[0] is OperatorData && e.Objects.Count > 1)
                        {
                            callClient = true;
                        }
                        else
                        {
                            callClient = true;
                            if (clientDataList != null && clientDataList.Count > 0)
                            {
                                eventArgs.Objects = clientDataList;
                            }
                            else
                                eventArgs.Objects.Add(clientClientData);
                        }

                        if (callClient == true)
                        {
                            ArrayList loggedUsers = new ArrayList(serverServiceLogIn[application].Keys);
                            foreach (string ope in loggedUsers)
                            {
                                if (serverServiceLogIn[application].ContainsKey(ope) == true)
                                {
                                    try
                                    {
                                        if (e.UpdatingWorkshift == true)
                                        {
#if (DEBUG)
                                            Console.WriteLine("Start - " + DateTime.Now.ToString(ApplicationUtil.DataBaseFormattedDate));
#endif
                                            if ((app.Name == UserApplicationData.Supervision.Name) &&
                                                ServiceUtil.CheckSupervisor(serverServiceLogIn[application][ope].Session.UserAccount.Role.Code) == true)
                                            {
                                                ClientServiceData currentServer = serverServiceLogIn[application][ope];
                                                bool userIsGeneralsupervisor = ServiceUtil.CheckGeneralSupervisor(currentServer.Session.UserAccount.Role);
                                                bool userIsFirstLevelSupervisor = ServiceUtil.CheckApplicationSupervisor(currentServer.Session.UserAccount.Role, UserApplicationData.FirstLevel.Name) && !userIsGeneralsupervisor;
                                                bool userIsDispatchSupervisor = ServiceUtil.CheckApplicationSupervisor(currentServer.Session.UserAccount.Role, UserApplicationData.Dispatch.Name) && !userIsGeneralsupervisor;
                                                bool checkLoginMode = ServiceUtil.GetLogInMode();

                                                foreach (OperatorLightClientData operador in clientDataList)
                                                {
                                                    //Checks if the operator has access in the specified supervisedApplication
                                                    bool operatorCanBeSupervised = ServiceUtil.CheckUserApplication(operador.RoleCode, currentServer.Session.SupervisedApplication);

                                                    if (operador.IsSupervisor == false)
                                                    {
                                                        UserApplicationData supervisedApp = currentServer.Session.SupervisedApplication == UserApplicationData.Dispatch.Name ? UserApplicationData.Dispatch : UserApplicationData.FirstLevel;
                                                        if (checkLoginMode == true)
                                                        {
                                                            bool operatorShoulBeConnected = OperatorScheduleManager.IsWorkingNow(operador.Code);
                                                            OperatorData operatorCurrentSupervisor = OperatorScheduleManager.GetCurrentSupervisor(operador.Code, supervisedApp);

                                                            if (userIsGeneralsupervisor && operatorCanBeSupervised
                                                                && (operatorShoulBeConnected || operador.IsLogged))
                                                            {
                                                                operador.Delete = false;
                                                            }
                                                            else if ((userIsFirstLevelSupervisor || userIsDispatchSupervisor)
                                                                && operatorCanBeSupervised == true && operatorShoulBeConnected
                                                                && operatorCurrentSupervisor != null && operatorCurrentSupervisor.Code == currentServer.Session.UserAccount.Code)
                                                            {
                                                                operador.Delete = false;
                                                            }
                                                            else
                                                            {
                                                                operador.Delete = true;
                                                            }
                                                        }
                                                        else //CheckLoginMode is disabled
                                                        {
                                                            if (operatorCanBeSupervised == true)//Only checks if operator is from Dispatch or First Level
                                                            {
                                                                if (userIsGeneralsupervisor)
                                                                {
                                                                    operador.Delete = false;
                                                                }
                                                                else
                                                                {
                                                                    operador.Delete = true;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                operador.Delete = true;
                                                            }
                                                        }

                                                    }
                                                    else //if operator is supervisor, he must be deleted.
                                                    {
                                                        operador.Delete = true;
                                                    }
                                                }
                                                eventArgs = new CommittedObjectDataCollectionEventArgs(clientDataList, CommittedDataAction.None, true, e.Context);
#if (DEBUG)
                                                Console.WriteLine("End - " + DateTime.Now.ToString(ApplicationUtil.DataBaseFormattedDate));
#endif
                                            }
                                        }

                                        if (serverServiceLogIn.ContainsKey(application) && serverServiceLogIn[application].ContainsKey(ope))
                                        {
                                            serverServiceLogIn[application][ope].ClientService.OnCommittedChanges(sender, eventArgs);
                                        }

                                    }
                                    catch (Exception ex)
                                    {
                                        if (clientClientData != null && ope != null && app != null)
                                        {
                                            SmartLogger.Print(ResourceLoader.GetString2("ClientCallBackError", clientClientData.GetType().Name, ope, app.FriendlyName));
                                        }
                                        SmartLogger.Print(ex);
                                        lock (syncserverServiceLogIn)
                                        {
                                            if (serverServiceLogIn.ContainsKey(app.Name) == true &&
                                                serverServiceLogIn[app.Name].ContainsKey(ope) == true)
                                            {
                                                CloseSession(serverServiceLogIn[app.Name][ope].Session);
                                            }
                                        }
                                    }
                                }
                                if (committedChangesSleep > 0)
                                    Thread.Sleep(committedChangesSleep);
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private IList ApplyConversion(IList objects, UserApplicationData app)
        {
            IList result = new ArrayList();
            if (objects[0] is OperatorData && app.Name == UserApplicationData.Supervision.Name)
            {
                foreach (OperatorData oper in objects)
                {
                    OperatorLightClientData operLight = new OperatorLightClientData();
                    operLight.Code = oper.Code;
                    operLight.Version = oper.Version;
                    operLight.DispatchAccess = ServiceUtil.CheckDispatchOperator(oper.Role.Code);
                    operLight.FirstLevelAccess = ServiceUtil.CheckFirstLevelOperator(oper.Role.Code);
                    operLight.RoleCode = oper.Role.Code;
                    operLight.IsSupervisor = ServiceUtil.CheckSupervisor(oper.Role.Code);
                    operLight.CurrentWorkShiftName = OperatorScheduleManager.GetCurrentWorkShiftName(oper.Code);

                    bool logged = false;
                    if (operLight.DispatchAccess)
                    {
                        double operatorConnected = Convert.ToDouble(SmartCadDatabase.SearchBasicObject(
                        SmartCadHqls.GetCustomHql(
                           SmartCadHqls.IsLoggedOperatorInApplication,
                           operLight.Code, true.ToString().ToLower(),
                           UserApplicationData.Dispatch.Code)));
                        logged = operatorConnected > 0;
                    }
                    else
                    {
                        double operatorConnected = Convert.ToDouble(SmartCadDatabase.SearchBasicObject(
                        SmartCadHqls.GetCustomHql(
                           SmartCadHqls.IsLoggedOperatorInApplication,
                           operLight.Code, true.ToString().ToLower(),
                           UserApplicationData.FirstLevel.Code)));
                        logged = operatorConnected > 0;
                    }
                    operLight.IsLogged = logged;
                    result.Add(operLight);
                }
            }
            else
            {
                foreach (ObjectData objectData in objects)
                {
                    result.Add(ApplyConversion(objectData, app));
                }
            }
            return result;
        }

        private ClientData ApplyConversion(ObjectData objectData, UserApplicationData app)
        {
            if (objectData is IncidentNotificationData)
            {
                IncidentNotificationData incidentNotification = objectData as IncidentNotificationData;

                if (app.Name == UserApplicationData.Dispatch.Name)
                {
                    return IncidentNotificationConversion.ToClient(incidentNotification, app);
                }
                else if (app.Name == UserApplicationData.FirstLevel.Name)
                {
                    if (incidentNotification.ReportBase.Incident.Status.Name == IncidentStatusData.Closed.Name)
                    {
                        return IncidentConversion.ToClient(incidentNotification.ReportBase.Incident, UserApplicationData.FirstLevel);
                    }
                }
                else if (app.Name == UserApplicationData.Cctv.Name)
                {
                    if (incidentNotification.ReportBase.Incident.Status.Name == IncidentStatusData.Closed.Name)
                    {
                        return IncidentConversion.ToClient(incidentNotification.ReportBase.Incident, UserApplicationData.Cctv);
                    }
                }
                else if (app.Name == UserApplicationData.Map.Name)
                {
                    if (incidentNotification.ReportBase.Incident.Status.Name == IncidentStatusData.Closed.Name)
                    {
                        return IncidentConversion.ToClient(incidentNotification.ReportBase.Incident, UserApplicationData.Map);
                    }
                }
                else if (app.Name == UserApplicationData.Supervision.Name)
                {
                    return IncidentNotificationConversion.ToClient(incidentNotification, app);
                }
            }
            else if (objectData is DispatchOrderData)
            {
                DispatchOrderData dispatchOrderData = objectData as DispatchOrderData;
                if (app.Name == UserApplicationData.Dispatch.Name)
                {
                    dispatchOrderData.Unit = (UnitData)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(SmartCadHqls.GetUnitByCode, dispatchOrderData.Unit.Code));
                    dispatchOrderData.Unit.DispatchOrder = dispatchOrderData;

                    return DispatchOrderConversion.ToClient(dispatchOrderData, app);
                }
                else if (app.Name == UserApplicationData.Supervision.Name)
                {
                    return DispatchOrderConversion.ToClient(dispatchOrderData, app);
                }
            }
            else if (objectData is PhoneReportData)
            {
                PhoneReportData phoneReportData = objectData as PhoneReportData;
                if (app.Name == UserApplicationData.FirstLevel.Name)
                {
                    return PhoneReportConversion.ToClient(phoneReportData, UserApplicationData.FirstLevel);
                }
                else if (app.Name == UserApplicationData.Dispatch.Name)
                {
                    return IncidentConversion.ToClient(phoneReportData.Incident, UserApplicationData.Dispatch);
                }
                else if (app.Name == UserApplicationData.Supervision.Name)
                {
                    return IncidentConversion.ToClient(phoneReportData.Incident, UserApplicationData.Supervision);
                }
                else
                {
                    return PhoneReportConversion.ToClient(phoneReportData, app);
                }
            }
            else if (objectData is IncidentTypeData)
            {
                if (app == UserApplicationData.FirstLevel)
                {
                    IncidentTypeData incidentTypeData = (IncidentTypeData)objectData;
                    if (SmartCadDatabase.IsInitialize(incidentTypeData.SetIncidentTypeQuestions) == false)
                        SmartCadDatabase.InitializeLazy(objectData, incidentTypeData.SetIncidentTypeQuestions);
                    if (SmartCadDatabase.IsInitialize(incidentTypeData.SetIncidentTypeDepartmentTypes) == false)
                        SmartCadDatabase.InitializeLazy(objectData, incidentTypeData.SetIncidentTypeDepartmentTypes);
                }
                return Conversion.ToClient(objectData, app);
            }
            else
            {
                return Conversion.ToClient(objectData, app);
            }
            Console.WriteLine("Convertion File not exists for type " + objectData.GetType().Name);
            return null;
        }
        #endregion

        #region Init Server
        /// <summary>
        /// Check that the database is function correctly to starup.
        /// </summary>
        private void FixDatabase()
        {
            try
            {
                StartCreatingClientConexions(false);

                LogoutAllOperators();

                AssignAllIncidentNotificationsToAutomaticSupervisor();

                AssignAllIncidentNotificationsStatusHistory();

                FinalizeSupervisorReports();

                ClearOperatorAlarms();
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
                throw ex;
            }
        }

        /// <summary>
        /// Clear the operators from the alarm that had being registered by an operator.
        /// </summary>
        private void ClearOperatorAlarms()
        {
            IList alarms = SmartCadDatabase.SearchObjects(
               SmartCadHqls.GetAllAlarmSensorTelemetrysStarted);

            foreach (AlarmSensorTelemetryData alarm in alarms)
            {
                alarm.Operator = null;
                SmartCadDatabase.SaveOrUpdate(alarm);
            }
        }

        /// <summary>
        /// Check which operators have to be logged out.
        /// </summary>
        public void LogoutAllOperators()
        {
            //Update all the operator session to logged out.
            IList uaads = ServiceUtil.GetAllCurrentSessionHistory();
            foreach (SessionHistoryData session in uaads)
            {
                if (ConnectedServiceLogIn(session) == false)
                {
                    SmartCadAudit.Audit(session.UserApplication, session.UserAccount.Login, session.UserAccount.Code, "CloseSession", null, OperatorActions.Logout);
                    session.IsLoggedIn = false;
                    session.EndDateLogin = SmartCadDatabase.GetTimeFromBD();
                    SmartCadDatabase.SaveOrUpdate(session, ServiceUtil.CreateContext(session.UserAccount.Login, session.UserApplication.Name));
                }
            }
        }

        /// <summary>
        /// Assign al incident notifications to the automatic supervisor.
        /// This is perform at the start of the servers.
        /// </summary>
        public void AssignAllIncidentNotificationsToAutomaticSupervisor()
        {
            try
            {
                //TODO: Cambios temporales para hacer que el servidor levante mas rapido.
                NHibernate.ISession session = SmartCadDatabase.NewSession();

                IList incidentNotifications = SmartCadDatabase.SearchObjects(SmartCadHqls.GetActiveIncidentNotificationsOrInManualSupervisor);

                ITransaction transaction = session.BeginTransaction();
                foreach (IncidentNotificationData incidentNotification in incidentNotifications)
                {
                    if (ConnectedServiceLogIn(UserApplicationData.Dispatch.Name, incidentNotification.DispatchOperator.Login) == false ||
                        incidentNotification.Status == IncidentNotificationStatusData.ManualSupervisor)
                    {
                        incidentNotification.Status = IncidentNotificationStatusData.AutomaticSupervisor;
                        incidentNotification.DispatchOperator = OperatorData.InternalSupervisor;

                        SmartCadDatabase.UpdateObject(session, incidentNotification);
                    }
                }
                transaction.Commit();
                SmartCadDatabase.CloseSession(session);
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        /// <summary>
        /// Close status histories from incident notification reassigned to automatic supervisor.
        /// This should be done in the validator.
        /// </summary>
        public void AssignAllIncidentNotificationsStatusHistory()
        {
            try
            {
                //TODO: Cambios temporales para hacer que el servidor levante mas rapido.
                NHibernate.ISession session = SmartCadDatabase.NewSession();

                IList incidentNotificationStatusHistories = SmartCadDatabase.SearchObjects("select insh from IncidentNotificationStatusHistoryData insh where insh.End is null and insh.Status.CustomCode != 'AUTOMATIC_SUPERVISOR' and insh.Status.CustomCode != 'MANUAL_SUPERVISOR' and insh.Status.CustomCode != 'NEW'");

                ITransaction transaction = session.BeginTransaction();
                foreach (IncidentNotificationStatusHistoryData incidentNotificationStatusHistory in incidentNotificationStatusHistories)
                {
                    if (ConnectedServiceLogIn(UserApplicationData.Dispatch.Name, incidentNotificationStatusHistory.UserAccount.Login) == false)
                    {
                        incidentNotificationStatusHistory.End = SmartCadDatabase.GetTimeFromBD();
                        SmartCadDatabase.UpdateObject(session, incidentNotificationStatusHistory);
                    }
                }
                transaction.Commit();
                SmartCadDatabase.CloseSession(session);

                NHibernate.ISession session1 = SmartCadDatabase.NewSession();

                ITransaction transaction1 = session1.BeginTransaction();
                foreach (IncidentNotificationStatusHistoryData incidentNotificationStatusHistory in incidentNotificationStatusHistories)
                {
                    if (ConnectedServiceLogIn(UserApplicationData.Dispatch.Name, incidentNotificationStatusHistory.UserAccount.Login) == false)
                    {
                        IncidentNotificationStatusHistoryData temp = new IncidentNotificationStatusHistoryData();
                        temp.Start = SmartCadDatabase.GetTimeFromBD();
                        temp.Status = IncidentNotificationStatusData.AutomaticSupervisor;
                        temp.UserAccount = OperatorData.InternalSupervisor;
                        temp.IncidentNotification = incidentNotificationStatusHistory.IncidentNotification;
                        SmartCadDatabase.SaveObject(session1, temp);
                    }
                }
                transaction1.Commit();
                SmartCadDatabase.CloseSession(session1);
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        /// <summary>
        /// Finalize any supervisor report that were lest open when the server was down.
        /// </summary>
        public void FinalizeSupervisorReports()
        {
            IList list = SmartCadDatabase.SearchObjects("select data from SupervisorCloseReportData data where data.Finished = false");
            foreach (SupervisorCloseReportData data in list)
            {
                data.Finished = true;
                SmartCadDatabase.SaveOrUpdate(data);
            }
        }

        #endregion

        public void CloseSession(SessionHistoryData session)
        {
            if (session.UserApplication.Name == UserApplicationData.Dispatch.Name)
            {
                ServiceUtil.AssignIncidentNotificationsToAutomaticSupervisor(session.UserAccount.Login);
            }
            else if (session.UserApplication.Name.Equals(UserApplicationData.Map.Name))
            {
                ServiceUtil.UnAssignAlertNotificationFromThisOperator(session.UserAccount);
            }
            bool logout = false;
            while (logout == false)
            {
                try
                {
                    ServiceUtil.LogoutOperator(session.UserAccount.Login, session.UserApplication.Name);
                    logout = true;
                }
                catch (Exception ex)
                {
                    //SmartLogger.Print(ex);
                    logout = false;
                }
            }
            RemoveServiceLogIn(session);
        }

        public void EndCloseReport(SessionHistoryData session)
        {
            SupervisorCloseReportData report = SmartCadDatabase.SearchObject<SupervisorCloseReportData>(
                SmartCadHqls.GetCustomHql(
                SmartCadHqls.GetCurrentCloseReportBySessionWithoutMessages, session.Code));

            if (report != null && report.Finished == false)
            {
                report.Finished = true;
                SmartCadDatabase.SaveOrUpdate(report);
            }
        }

        #region IDisposable

        private bool disposed = false;

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    // Dispose managed resources.
                    SmartCadDatabase.CommittedChanges -= new EventHandler<CommittedDataEventArgs>(SmartCadDatabase_CommittedChanges);
                    //NortelRTDWrapper.Stop();
                    AvayaStatWrapper.Stop();
                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.

                disposed = true;
            }
        }

        ~ServiceHelper()
        {
            Dispose(false);
        }

        #endregion


    }

    public class ClientServiceData
    {
        public SessionHistoryData Session { get; set; }
        public ClientService ClientService { get; set; }
    }
}
