using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Security;
using System.Security.Cryptography.X509Certificates;
using System.IdentityModel.Selectors;
using System.IdentityModel.Policy;
using System.Threading;
using System.IO;
using SmartCadCore.Core;


namespace Smartmatic.SmartCad.Service
{
    public class FileServerServiceClient : ClientBase<IFileServerService>, IFileServerService
    {
        public FileServerServiceClient(Binding binding, EndpointAddress remoteAddress)
            : base(binding, remoteAddress)
        {
        }

        public static FileServerServiceClient CreateInstance()
        {
            string url = ApplicationUtil.BuildServiceUrl(
                "net.tcp",
                SmartCadConfiguration.SmartCadSection.ClientElement.ServerAddressElement.IP,
                SmartCadConfiguration.FILE_SERVER_SERVICE_PORT,
                SmartCadConfiguration.FILE_SERVER_SERVICE_ADDRESS);

            EndpointAddress address = new EndpointAddress(url);

            Binding binding = BindingBuilder.GetNetTcpBindingStreamed();

            FileServerServiceClient client = new FileServerServiceClient(binding, address);

            foreach (OperationDescription op in client.Endpoint.Contract.Operations)
            {
                DataContractSerializerOperationBehavior dataContractBehavior =
                   op.Behaviors.Remove<DataContractSerializerOperationBehavior>()
                        as DataContractSerializerOperationBehavior;

                op.Behaviors.Add(new NetDataContractSerializerOperationBehavior(op));
            }

            return client;
        }

        #region IFileServerService Members

        public Stream GetFile(string fileName)
        {
            return base.Channel.GetFile(fileName);
        }

        void IFileServerService.SendFile(FileSend fileSend)
        {
            base.Channel.SendFile(fileSend);
        }

        public void SendFile(Stream data, string fileName)
        {
            FileSend fs = new FileSend() { Data = data, Filename = fileName, Length = int.Parse(data.Length.ToString()) };
            ((IFileServerService)this).SendFile(fs);
        }
        #endregion
    }
}
