﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smartmatic.SmartCad.VideoAnalitica.Objects
{
    public class EventVideo
    {
        #region Fields

        private long objectId;

        private DateTime eventTimeValue;

        private int eventTimeMilliseconds; 

        private DateTime? birthTimeValue;

        private int? birtTimeMilliseconds;

        private DateTime? deathTimeValue;

        private int? deathTimeMilliseconds;

        private int totalObjectInstances;

        private IList<VideoInstance> videoInstances;
        
        #endregion

        #region Properties

        public virtual long ObjectId
        {
            get { return objectId; }
            set { objectId = value; }
        }

        public virtual DateTime EventTimeValue
        {
            get { return eventTimeValue; }
            set { eventTimeValue = value; }
        }

        public virtual int EventTimeMilliseconds
        {
            get { return eventTimeMilliseconds; }
            set { eventTimeMilliseconds = value; }
        }

        public virtual DateTime? BirthTimeValue
        {
            get { return birthTimeValue; }
            set { birthTimeValue = value; }
        }

        public virtual int? BirtTimeMilliseconds
        {
            get { return birtTimeMilliseconds; }
            set { birtTimeMilliseconds = value; }
        }

        public virtual DateTime? DeathTimeValue
        {
            get { return deathTimeValue; }
            set { deathTimeValue = value; }
        }

        public virtual int? DeathTimeMilliseconds
        {
            get { return deathTimeMilliseconds; }
            set { deathTimeMilliseconds = value; }
        }

        public virtual int TotalObjectInstances
        {
            get { return totalObjectInstances; }
            set { totalObjectInstances = value; }
        }


        public virtual IList<VideoInstance> VideoInstances
        {
            get { return videoInstances; }
            set { videoInstances = value; }
        }
 
        #endregion

    }
}
