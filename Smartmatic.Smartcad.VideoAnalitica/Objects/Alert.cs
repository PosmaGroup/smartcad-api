﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smartmatic.SmartCad.VideoAnalitica.Objects
{
    public class Alert
    { 
        public uint id {get;set;}
        public DateTime date { get; set; }
        public uint sensorId { get; set; }
        public uint ruleId { get; set; }

    }
}
