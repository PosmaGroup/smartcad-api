﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smartmatic.SmartCad.VideoAnalitica.Objects
{
    public class Rule
    {
        private uint id;

        private string guid;

        public uint ID
        {
            get { return id; }
            set { id = value; }
        }

        public string GUID
        {
            get { return guid; }
            set { guid = value; }
        }
    }
}
