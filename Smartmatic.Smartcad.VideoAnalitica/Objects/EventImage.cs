﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smartmatic.SmartCad.VideoAnalitica.Objects
{
    public class EventImage
    {
        private uint idEvent;

        public uint ID_EVENT
        {
            get { return idEvent; }
            set { idEvent = value; }
        }
        private byte[] image;

        public byte[] IMAGE
        {
            get { return image; }
            set { image = value; }
        }
    }
}
