﻿using Iesi.Collections;
using NHibernate.Mapping.Attributes;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    #region Class TwitterReportData Documentation

    #endregion
    [Serializable]
    [JoinedSubclass(Table = "TWITTER_REPORT", NameType = typeof(TwitterReportData), ExtendsType = typeof(ReportBaseData), Lazy = false)]
    public class TwitterReportData : ReportBaseData
    {
        #region Fields

        private int parentCode;
        private ISet setAnswers;
        private DateTime? finishedIncidentTime;
        private AlarmTwitterData alarmTwitter;

        #endregion

        #region Constructors

        public TwitterReportData()
        {
        }
        public TwitterReportData(ISet answers)
        {
            this.setAnswers = answers;
        }

        #endregion

        #region Properties

        [Key(0, Column = "PARENT_CODE", ForeignKey = "FK_TWITTER_REPORT_REPORT_BASE_CODE")]
        public virtual int ParentCode
        {
            get
            {
                return parentCode;
            }
            set
            {
                parentCode = value;
            }
        }

        [Property(Column = "FINISHED_INCIDENT_TIME", TypeType = typeof(DateTime))]
        public virtual DateTime? FinishedIncidentTime
        {
            get
            {
                return finishedIncidentTime;
            }
            set
            {
                finishedIncidentTime = value;
            }
        }

        [Set(0, Name = "SetAnswers", Table = "TWITTER_REPORT_ANSWER", Cascade = "all-delete-orphan",
            Lazy = CollectionLazy.True/*, Inverse = true*/, Fetch = CollectionFetchMode.Subselect)]
        [Key(1, Column = "TWITTER_REPORT_CODE"/*, ForeignKey = "FK_TWITTER_REPORT_TWITTER_REPORT_ANSWER_CODE"*/)]
        [OneToMany(2, ClassType = typeof(TwitterReportAnswerData))]
        public ISet SetAnswers
        {
            get
            {
                return setAnswers;
            }
            set
            {
                setAnswers = value;
            }
        }

        [ManyToOne(ClassType = typeof(AlarmTwitterData), Column = "ALARM_CODE",
           ForeignKey = "FK_TWITTER_REPORT_ALARM_CODE", NotNull = true, Cascade = "none")]
        public AlarmTwitterData AlarmTwitter
        {
            get
            {
                return alarmTwitter;
            }
            set
            {
                alarmTwitter = value;
            }
        }

        #endregion

        #region doc

        #endregion doc

    }
}
