﻿using NHibernate.Mapping.Attributes;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(AlarmQueryTwitterData), Table = "ALARM_QUERY_TWITTER", Lazy = false, BatchSize = 1000, Where = "DELETED_ID IS NULL")]
    public class AlarmQueryTwitterData : ObjectData
    {
        [Property(0, Unique = true, TypeType = typeof(string))]
        [Column(1, Name = "TWITTER_QUERY", UniqueKey = "UK_ALARM_QUERY_TWITTER", NotNull = true)]
        public virtual string TwitterQuery
        {
            get;
            set;
        }

        [Property(Column = "NAME", NotNull = true)]
        public virtual string Name
        {
            get;
            set;
        }

        [Property(Column = "DESCRIPTION", NotNull = true)]
        public virtual string Description
        {
            get;
            set;
        }

        [Property(Column = "TWEETS_AMOUNT", NotNull = true)]
        public virtual int TweetsAmount
        {
            get;
            set;
        }

        /// <summary>
        /// Time in seconds
        /// </summary>
        [Property(Column = "TIME_INTERVAL", NotNull = true)]
        public virtual int TimeInterval
        {
            get;
            set;
        }


        /// <summary>
        /// Type (1=preDefined or 2=liveSearch)
        /// </summary>
        [Property(Column = "TYPE", NotNull = true)]
        public virtual int Type
        {
            get;
            set;
        }

        [InitialData(PropertyName = "Name", PropertyValue = "AlarmQueryTwitterData")]
        public static readonly UserResourceData Resource;


        public override string ToString()
        {
            return Description;
        }

        public override bool Equals(object obj)
        {
            try
            {
                if (obj.GetType() == typeof(AlarmQueryTwitterData))
                {
                    AlarmQueryTwitterData alarmQuery = (AlarmQueryTwitterData)obj;
                    if (this.Code == alarmQuery.Code)
                    {
                        return true;
                    }
                }
            }
            catch { }
            return false;
        }
    }
}
