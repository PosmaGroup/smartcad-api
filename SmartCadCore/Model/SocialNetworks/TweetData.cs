﻿using NHibernate.Mapping.Attributes;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(TweetData), Table = "TWEET", Lazy = false, BatchSize = 100, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_TWEET")]
    public class TweetData : ObjectData, IComparable<TweetData>
    {
        private string customCode;
        private AlarmTwitterData alarm;

        [Property(0, Unique = true)]
        [Column(1, Name = "CUSTOM_CODE", UniqueKey = "UK_TWEET", NotNull = true)]
        public virtual string CustomCode
        {
            get
            {
                return customCode;
            }
            set
            {
                customCode = value;
            }
        }

        [ManyToOne(ClassType = typeof(AlarmTwitterData), Column = "ALARM_TWITTER_CODE",
         ForeignKey = "FK_ALARM_TWITTER_TWEET_CODE", Cascade = "save-update", UniqueKey = "UK_TWEET")]
        public AlarmTwitterData Alarm
        {
            get
            {
                return alarm;
            }
            set
            {
                alarm = value;
            }
        }

        [Property(0, TypeType = typeof(DateTime))]
        [Column(1, Name = "PUBLISHED", NotNull = true)]
        public virtual DateTime Published
        {
            get;
            set;
        }

        [Property(Column = "AUTHOR_USERNAME", NotNull = true)]
        public virtual string AuthorUserName
        {
            get;
            set;
        }

        [Property(Column = "CONTENT", NotNull = true, Length = 10240)]
        public virtual string Content
        {
            get;
            set;
        }

        [Property(Column = "AUTHOR_NAME", NotNull = true)]
        public virtual string AuthorName
        {
            get;
            set;
        }

        [Property(Column = "AUTHOR_URI")]
        public virtual string AuthorUri
        {
            get;
            set;
        }

        [Property(Column = "IMAGE_URL", NotNull = true)]
        public virtual string ImageUrl
        {
            get;
            set;
        }

        [Property(Column = "LINK", NotNull = true)]
        public virtual string Link
        {
            get;
            set;
        }

        [InitialData(PropertyName = "Name", PropertyValue = "TwittData")]
        public static readonly UserResourceData Resource;


        public override string ToString()
        {
            return Content;
        }

        public override bool Equals(object obj)
        {
            try
            {
                if (obj.GetType() == typeof(TweetData))
                {
                    TweetData twitt = (TweetData)obj;
                    if (this.CustomCode == twitt.CustomCode)
                    {
                        return true;
                    }
                }
            }
            catch { }
            return false;
        }
        public override int GetHashCode()
        {
            return unchecked((int)long.Parse(customCode));
        }

        #region IComparable Members

        public int CompareTo(TweetData twitt)
        {
            if (twitt == null) return 1;
            return Published.CompareTo(twitt.Published);
        }

        #endregion
    }
}
