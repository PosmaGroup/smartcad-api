﻿using Iesi.Collections;
using NHibernate.Mapping.Attributes;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    #region Class TwitterReportAnswerData Documentation
    /// <summary>
    /// This class represents the setAnswers of a cctv report
    /// </summary>
    /// <className>TwitterReportAnswerData</className>
    /// <author email="fsilva@smartmatic.com">Fernando Silva</author>
    /// <date>2007/01/30</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion

    [Serializable]
    [Class(NameType = typeof(TwitterReportAnswerData), Table = "TWITTER_REPORT_ANSWER", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_TWITTER_REPORT_ANSWER_QUESTION_TWITTER_REPORT_CODE")]
    public class TwitterReportAnswerData : ObjectData
    {
        #region Fields

        private QuestionData question;
        private ISet setAnswers;
        private TwitterReportData twitterReport;

        #endregion

        #region Constructor

        public TwitterReportAnswerData()
        {
        }



        public TwitterReportAnswerData(QuestionData question, QuestionPossibleAnswerData answer,
            string answerText)
        {
            this.question = question;
            PossibleAnswerAnswerData paad = new PossibleAnswerAnswerData();
            paad.TwitterReportAnswer = this;
            paad.PossibleAnswer = answer;
            paad.TextAnswer = answerText;
            this.SetAnswers.Add(paad);
            //this.answer = answer;
        }

        public TwitterReportAnswerData(QuestionData question, string answer)
        {
            this.question = question;
            PossibleAnswerAnswerData paad = new PossibleAnswerAnswerData();
            //this.answer = answer;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Alarm report question
        /// </summary>
        [ManyToOne(0, ClassType = typeof(QuestionData), ForeignKey = "FK_TWITTER_REPORT_ANSWER_QUESTION_CODE", Unique = true, Cascade = "none")]
        [Column(1, Name = "TWITTER_REPORT_ANSWER_QUESTION_CODE", UniqueKey = "UK_TWITTER_REPORT_ANSWER_QUESTION_TWITTER_REPORT_CODE", NotNull = true, Index = "IX_TWITTER_REPORT_ANSWER_QUESTION_TWITTER_REPORT")]
        public virtual QuestionData Question
        {
            get
            {
                return question;
            }
            set
            {
                question = value;
            }
        }

        [Set(0, Name = "SetAnswers", Table = "POSSIBLE_ANSWER_ANSWER_DATA", Cascade = "save-update",
            Lazy = CollectionLazy.True/*, Inverse = true*/, Fetch = CollectionFetchMode.Subselect)]
        [Key(1, Column = "TWITTER_REPORT_ANSWER_CODE"/*, ForeignKey = "FK_POSSIBLE_ANSWER_ANSWER_TWITTER_REPORT_ANSWER"*/)]
        [OneToMany(2, ClassType = typeof(PossibleAnswerAnswerData))]
        public ISet SetAnswers
        {
            get
            {
                return setAnswers;
            }
            set
            {
                setAnswers = value;
            }
        }

        /// <summary>
        /// ALARM report parent of this answer
        /// </summary>
        [ManyToOne(0, ClassType = typeof(TwitterReportData), ForeignKey = "FK_TWITTER_REPORT_TWITTER_REPORT_ANSWER_CODE", Unique = true, Cascade = "none")]
        [Column(1, Name = "TWITTER_REPORT_CODE", UniqueKey = "UK_TWITTER_REPORT_ANSWER_QUESTION_TWITTER_REPORT_CODE", NotNull = true, Index = "IX_TWITTER_REPORT_ANSWER_QUESTION_TWITTER_REPORT")]
        public virtual TwitterReportData TwitterReport
        {
            get
            {
                return twitterReport;
            }
            set
            {
                twitterReport = value;
            }
        }
        #endregion

        public override bool Equals(object obj)
        {

            if ((question.Code != (obj as TwitterReportAnswerData).Question.Code) &&
                (question.Text != (obj as TwitterReportAnswerData).Question.Text) &&
                (question.ShortText != (obj as TwitterReportAnswerData).Question.ShortText))
                return false;
            else
                return true;
        }
    }
}
