using System;
using System.Text;
using System.Drawing;
using ColorBase = System.Drawing.Color;

using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(DispatchOrderStatusData), Table = "DISPATCH_ORDER_STATUS", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_DISPATCH_ORDER_STATUS_CUSTOM_CODE")]
    [AttributeIdentifier("DELETED_ID_2", Value = "UK_DISPATCH_ORDER_STATUS_FRIENDLY_NAME")]
    public class DispatchOrderStatusData : ObjectData, INamedObjectData, IImmutableObjectData
    {
        #region Fields

        private string name;
        private string friendlyName;

        private Color color;

        private byte[] image;
        private string customCode;
        private bool? immutable;
        private ComponentColorData componentColor;

        #endregion

        #region Constructors

        public DispatchOrderStatusData()
        {
        }

        public DispatchOrderStatusData(string name, Color color, byte[] image, bool allowDelete)
        {
            this.name = name;
            this.color = color;
            this.image = image;
            this.immutable = allowDelete;
        }

        #endregion

        #region Properties

        [Property(Column = "NAME")]
        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "FRIENDLY_NAME", UniqueKey = "UK_DISPATCH_ORDER_STATUS_FRIENDLY_NAME", NotNull = true)]
        public virtual string FriendlyName
        {
            get
            {
                return friendlyName;
            }
            set
            {
                friendlyName = value;
            }
        }

        //[Property(Column = "COLOR", TypeType = typeof(Color))]
        public virtual Color Color
        {
            get
            {
                if (ComponentColor != null)
                    return ColorBase.FromArgb(ComponentColor.ColorA.Value, ComponentColor.ColorR.Value, ComponentColor.ColorG.Value, ComponentColor.ColorB.Value);
                else
                    return Color.White;
            }
            set
            {
                componentColor = new ComponentColorData(value);
            }
        }

        [ComponentProperty(ComponentType = typeof(ComponentColorData))]
        public ComponentColorData ComponentColor
        {
            get
            {
                return componentColor;
            }
            set
            {
                componentColor = value;
                color = ColorBase.FromArgb(value.ColorA.Value, value.ColorR.Value, value.ColorG.Value, value.ColorB.Value);
            }
        }


        [Property(Column = "IMAGE", Type = "BinaryBlob", Length = int.MaxValue)]
        public virtual byte[] Image
        {
            get
            {
                return image;
            }
            set
            {
                image = value;
            }
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "CUSTOM_CODE", UniqueKey = "UK_DISPATCH_ORDER_STATUS_CUSTOM_CODE", NotNull = true)]
        public virtual string CustomCode
        {
            get
            {
                return customCode;
            }
            set
            {
                customCode = value;
            }
        }

        #region IImmutableObjectData Members

        [Property(Column = "IMMUTABLE", NotNull = false, TypeType = typeof(bool))]
        public virtual bool? Immutable
        {
            get
            {
                return immutable;
            }
            set
            {
                immutable = value;
            }
        }

        #endregion

        #endregion

        #region InitialData

        [InitialData(PropertyName = "CustomCode", PropertyValue = "DISPATCHED")]
        public static DispatchOrderStatusData Dispatched;

        [InitialData(PropertyName = "CustomCode", PropertyValue = "CANCELLED")]
        public static DispatchOrderStatusData Cancelled;

        [InitialData(PropertyName = "CustomCode", PropertyValue = "CLOSED")]
        public static DispatchOrderStatusData Closed;

        [InitialData(PropertyName = "CustomCode", PropertyValue = "DELAYED")]
        public static DispatchOrderStatusData Delayed;

        [InitialData(PropertyName = "CustomCode", PropertyValue = "UNIT_ONSCENE")]
        public static DispatchOrderStatusData UnitOnScene;

        #endregion
        public override string ToString()
        {
            string text = "";
            if (this.Name != null)
            {
                text = this.Name;
            }
            return text;
        }
    }
}