﻿using System;
using System.Linq;
using System.Text;
using NHibernate.Mapping.Attributes;
using System.Collections;

namespace SmartCadCore.Model
{
    #region Class DispatchReportData Documentation
    /// <summary>
    /// This class represents a dispatch report.
    /// </summary>
    /// <className>DispatchReportData</className>
    /// <author email="fsilva@smartmatic.com">Fernando Silva</author>
    /// <date>2007/01/30</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    [Serializable]
    [Class(NameType = typeof(DispatchReportData), Table = "DISPATCH_REPORT", Lazy = false, BatchSize = 100, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_DISPATCH_REPORT_NAME")]
    public class DispatchReportData : ObjectData
    {
        /// <summary>
        /// Name of the dispatch report.
        /// </summary>
        [Property(0, Unique = true)]
        [Column(1, Name = "NAME", NotNull = true, UniqueKey = "UK_DISPATCH_REPORT_NAME")]
        public string Name { get; set; }

        /// <summary>
        /// List of incident types of the dispatch report.
        /// </summary>
        [Bag(0, Name = "IncidentTypes", Table = "DISPATCH_REPORT_INCIDENT_TYPE", Cascade = "none", Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect)]
        [Key(1, Column = "DISPATCH_REPORT_CODE", ForeignKey = "FK_DISPATCH_REPORT_INCIDENT_TYPE_CODE")]
        [ManyToMany(2, ClassType = typeof(IncidentTypeData), Column = "INCIDENT_TYPE_CODE", ForeignKey = "FK_INCIDENT_TYPE_DISPATCH_REPORT_CODE")]
        public IList IncidentTypes { get; set; }
        /// <summary>
        /// List of department types of the dispatch report.
        /// </summary>
        [Bag(0, Name = "DepartmentTypes", Table = "DISPATCH_REPORT_DEPARTMENT_TYPE", Cascade = "none", Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect)]
        [Key(1, Column = "DISPATCH_REPORT_CODE", ForeignKey = "FK_DISPATCH_REPORT_DEPARTMENT_TYPE_CODE")]
        [ManyToMany(2, ClassType = typeof(DepartmentTypeData), Column = "DEPARTMENT_TYPE_CODE", ForeignKey = "FK_DEPARTMENT_TYPE_DISPATCH_REPORT_CODE")]
        public IList DepartmentTypes { get; set; }
       
        
        /// <summary>
        /// List of question of the dispatch report.
        /// </summary>
        [Bag(0, Name = "Questions", Table = "DISPATCH_REPORT_QUESTION_DISPATCH_REPORT", Cascade = "save-update", Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect, Where= "DELETED_ID IS NULL")]
        [Key(1, Column = "DISPATCH_REPORT_CODE"/*, ForeignKey = "FK_DISPATCH_REPORT_QUESTION_DISPATCH_REPORT_DISPATCH_REPORT_CODE"*/)]
        [OneToMany(2, ClassType = typeof(DispatchReportQuestionDispatchReportData))]
        public IList Questions { get; set; }
      
        [InitialData(PropertyName = "Name", PropertyValue = "DispatchReportData")]
        public static readonly UserResourceData Resource;   
    }
}
