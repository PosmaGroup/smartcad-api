﻿using Iesi.Collections;
using NHibernate.Mapping.Attributes;
using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(ReportBaseData), Table = "REPORT_BASE", Lazy = false, BatchSize = 100, Where = "DELETED_ID IS NULL", Polymorphism = PolymorphismType.Explicit)]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_REPORT_BASE_CUSTOM_CODE")]
    public class ReportBaseData : ObjectData
    {
        #region Fields
        private IncidentData incident;
        private IList incidentNotifications;
        private ISet setIncidentTypes;
        private IList reportBaseDepartmentTypes;
        private bool? multipleOrganisms;
        private OperatorData operatorData;
        private string customCode;
        #endregion

        #region Properties

        [ManyToOne(0, ClassType = typeof(IncidentData), ForeignKey = "FK_INCIDENT_REPORT_BASE_CODE", Cascade = "none")]
        [Column(1, Name = "INCIDENT_CODE", NotNull = true, Index = "IX_REPORT_BASE_INCIDENT_OPERATOR")]
        public virtual IncidentData Incident
        {
            get
            {
                return incident;
            }
            set
            {
                incident = value;
            }
        }

        [Bag(0, Name = "IncidentNotifications", Table = "INCIDENT_NOTIFICATIONS", Cascade = "all-delete-orphan",
           Lazy = CollectionLazy.True, Inverse = true, Fetch = CollectionFetchMode.Subselect)]
        [Key(1, Column = "REPORT_BASE_CODE"/*, ForeignKey = "FK_INCIDENT_NOTIFICATION_REPORT_BASE_CODE"*/)]
        [OneToMany(2, ClassType = typeof(IncidentNotificationData))]
        public virtual IList IncidentNotifications
        {
            get
            {
                return incidentNotifications;
            }
            set
            {
                incidentNotifications = value;
            }
        }

        [Set(0, Name = "SetIncidentTypes", Table = "REPORT_BASE_INCIDENT_TYPE", Cascade = "none", Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect)]
        [Key(1, ForeignKey = "FK_INCIDENT_TYPE_REPORT_BASE_CODE")]
        [Column(2, Name = "REPORT_BASE_CODE", NotNull = true, Index = "IX_REPORT_BASE_INCIDENT_TYPE")]
        [ManyToMany(3, ClassType = typeof(IncidentTypeData), ForeignKey = "FK_REPORT_BASE_INCIDENT_TYPE_CODE")]
        [Column(4, Name = "INCIDENT_TYPE_CODE", NotNull = true, Index = "IX_REPORT_BASE_INCIDENT_TYPE")]
        public ISet SetIncidentTypes
        {
            get
            {
                return setIncidentTypes;
            }
            set
            {
                setIncidentTypes = value;
            }
        }

        [Bag(0, Name = "ReportBaseDepartmentTypes", Table = "REPORT_BASE_DEPARTMENT_TYPE", Cascade = "all-delete-orphan",
           Lazy = CollectionLazy.True, Inverse = true, Fetch = CollectionFetchMode.Subselect)]
        [Key(1, Column = "REPORT_BASE_CODE"/*, ForeignKey = "FK_REPORT_BASE_DEPARTMENT_TYPE_REPORT_BASE_CODE"*/)]
        [OneToMany(2, ClassType = typeof(ReportBaseDepartmentTypeData))]
        public virtual IList ReportBaseDepartmentTypes
        {
            get
            {
                return reportBaseDepartmentTypes;
            }
            set
            {
                reportBaseDepartmentTypes = value;
            }
        }

        [Property(Column = "MULTIPLE_ORGANISMS", TypeType = typeof(bool))]
        public virtual bool? MultipleOrganisms
        {
            get
            {
                return multipleOrganisms;
            }
            set
            {
                multipleOrganisms = value;
            }
        }

        [ManyToOne(0, ClassType = typeof(OperatorData), ForeignKey = "FK_OPERATOR_REPORT_BASE_CODE", Cascade = "none")]
        [Column(1, Name = "OPERATOR_CODE", NotNull = true, Index = "IX_REPORT_BASE_INCIDENT_OPERATOR")]
        public virtual OperatorData Operator
        {
            get
            {
                return operatorData;
            }
            set
            {
                operatorData = value;
            }
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "CUSTOM_CODE", UniqueKey = "UK_REPORT_BASE_CUSTOM_CODE", NotNull = true)]
        public virtual string CustomCode
        {
            get
            {
                return customCode;
            }
            set
            {
                customCode = value;
            }
        }
        #endregion
    }
}
