using System;
using System.Collections;
using System.Text;
using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    #region Class SupportRequestData Documentation
    /// <summary>
    /// This class represents a support request made by any unit.
    /// </summary>
    /// <className>SupportRequestReportData</className>
    /// <author email="jynojosa@smartmatic.com">Julio Ynojosa</author>
    /// <date>2007/05/03</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion

    [Serializable]
    //[Class(Table = "SUPPORT_REQUEST_REPORT", Lazy = false, Where = "DELETED_ID IS NULL")]
    [JoinedSubclass(Table = "SUPPORT_REQUEST_REPORT", NameType = typeof(SupportRequestReportData), ExtendsType = typeof(ReportBaseData), Lazy = false)]
    public class SupportRequestReportData : ReportBaseData
    {
        #region Enums
        /// <summary>
        /// Enumerates the properties that this class has.
        /// </summary>
        public enum Properties
        {
            Unit,
            Comment,
            CreationDate
        }

        #endregion

        #region Fields

        private int parentCode;
        private UnitData unit;
        private string comment;
        public DateTime? creationDate;
        private ReportBaseData sourceReportBase;

        #endregion

        #region Properties

        [Key(0, Column = "PARENT_CODE", ForeignKey = "FK_SUPPORT_REQUEST_REPORT_REPORT_BASE_CODE")]
        public virtual int ParentCode
        {
            get
            {
                return parentCode;
            }
            set
            {
                parentCode = value;
            }
        }

        [ManyToOne(ClassType = typeof(UnitData), Column = "UNIT_CODE",
            ForeignKey = "FK_UNIT_SUPPORT_REQUEST_CODE", Cascade = "none")]
        public virtual UnitData Unit
        {
            get
            {
                return unit;
            }
            set
            {
                unit = value;
            }
        }

        [Property(Column = "COMMENT", Length = 1000)]
        public virtual string Comment
        {
            get
            {
                return comment;
            }
            set
            {
                comment = value;
            }
        }

        [Property(Column = "CREATION_DATE", TypeType = typeof(DateTime))]
        public virtual DateTime? CreationDate
        {
            get
            {
                return creationDate;
            }
            set
            {
                creationDate = value;
            }
        }

        [ManyToOne(ClassType = typeof(ReportBaseData), Column = "REPORT_SOURCE_CODE",
            ForeignKey = "FK_SUPPORT_REQUEST_REPORT_SOURCE_REPORT_BASE_CODE", Cascade = "none")]
        public ReportBaseData SourceReportBase
        {
            get
            {
                return sourceReportBase;
            }
            set
            {
                sourceReportBase = value;
            }
        }
        #endregion
    }
}
