﻿using Iesi.Collections;
using NHibernate.Mapping.Attributes;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{ 
    #region Class UserAccountApplicationData Documentation
    /// <summary>
    /// This class represents the M-N relation between UserAccount and UserApplication.
    /// It was mapped in this way to keep track about all logins made by an user its durations.
    /// </summary>
    /// <className>UserAccountApplicationData</className>
    /// <author email="jynojosa@smartmatic.com">Julio Ynojosa</author>
    /// <date>2006/04/06</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    [Serializable]
    [Class(NameType = typeof(SessionHistoryData), Table = "SESSION_HISTORY", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_SESSION_HISTORY")]
    public class SessionHistoryData : ObjectData
    {
        #region Fields

        /// <summary>
        /// Primary key
        /// </summary>

        private OperatorData userAccount;
        
        private UserApplicationData userApplication;
        
        private DateTime? startDateLogin;
        
        private DateTime? endDateLogin;
        
        private bool? isLoggedIn;

        private ISet sessionHistoryStatusList;

        private string computerName;
        
        private string supervisedApplication;
	
        #endregion

        #region Properties

        /// <summary>
        /// User application that is or was logged
        /// </summary>
        [ManyToOne(0, ClassType = typeof(UserApplicationData),
         ForeignKey = "FK_SESSION_HISTORY_USER_APPLICATION_CODE", Cascade = "none")]
        [Column(1, Name = "USER_APPLICATION_CODE", NotNull = false, UniqueKey = "UK_SESSION_HISTORY")]
        public virtual UserApplicationData UserApplication
        {
            get 
            { 
                return userApplication; 
            }
            set 
            { 
                userApplication = value; 
            }
        }

        /// <summary>
        /// User account who is logged in.
        /// </summary>
        [ManyToOne(ClassType = typeof(OperatorData),
             ForeignKey = "FK_SESSION_HISTORY_USER_ACCOUNT_CODE",
             Insert = true, Update = false, Cascade = "none")]
        [Column(1, Name = "USER_ACCOUNT_CODE", NotNull = false, UniqueKey = "UK_SESSION_HISTORY")]
        public virtual OperatorData UserAccount
        {
            get 
            { 
                return userAccount; 
            }
            set 
            { 
                userAccount = value; 
            }
        }

        /// <summary>
        /// Start date of login
        /// </summary>
        [Property(Name = "StartDateLogin", Column = "START_DATE_LOGIN", TypeType = typeof(DateTime))]
        public virtual DateTime? StartDateLogin
        {
            get 
            { 
                return startDateLogin; 
            }
            set 
            { 
                startDateLogin = value; 
            }
        }

        /// <summary>
        /// End date of login
        /// </summary>
        [Property(0, Name = "EndDateLogin", TypeType = typeof(DateTime))]
        [Column(1, Name = "END_DATE_LOGIN", UniqueKey = "UK_SESSION_HISTORY")]
        public virtual DateTime? EndDateLogin
        {
            get 
            { 
                return endDateLogin; 
            }
            set 
            { 
                endDateLogin = value; 
            }
        }

        /// <summary>
        /// Indicates if the user is logged
        /// </summary>
        [Property(Name = "IsLoggedIn", Column = "IS_LOGGED_IN", TypeType = typeof(bool))]
        public virtual bool? IsLoggedIn
        {
            get
            {
                return isLoggedIn;
            }
            set
            {
                isLoggedIn = value;
            }
        }

        [Set(0, Name = "SessionHistoryStatusList", Table = "SESSION_STATUS_HISTORY", Cascade = "save-update",
           Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect, Inverse = true, Where = "DELETED_ID IS NULL")]
        [Key(1, Column = "SESSION_HISTORY_CODE"/*, ForeignKey = "FK_SESSION_HISTORY_SESSION_STATUS_HISTORY_CODE"*/)]
        [OneToMany(2, ClassType = typeof(SessionStatusHistoryData))]
        public ISet SessionHistoryStatusList
        {
            get
            {
                return sessionHistoryStatusList;
            }
            set
            {
                sessionHistoryStatusList = value;
            }
        }

        [Property(Name = "ComputerName", Column = "COMPUTER_NAME", TypeType = typeof(string))]
        public virtual string ComputerName
        {
            get { return computerName; }
            set { computerName = value; }
        }

        [Property(Name = "SupervisedApplication", Column = "SUPERVISED_APPLIATION", TypeType = typeof(string))]
        public virtual string SupervisedApplication
        {
            get { return supervisedApplication; }
            set { supervisedApplication = value; }
        }
        #endregion

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is SessionHistoryData)
            {
                SessionHistoryData shd = (SessionHistoryData)obj;
                result = this.UserApplication == shd.UserApplication &&
                         this.UserAccount == shd.UserAccount &&
                         this.StartDateLogin == shd.StartDateLogin;
            }
            return result;
        }
    }
}
