﻿using NHibernate.Mapping.Attributes;
using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(DispatchOrderData), Table = "DISPATCH_ORDER", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_DISPATCH_ORDER_CUSTOM_CODE")]
    [AttributeIdentifier("DELETED_ID_2", Value = "UK_DISPATCH_ORDER_UNIT")]
    public class DispatchOrderData : ObjectData
    {
        #region Fields

        private DateTime? startDate;

        private DateTime? arrivalDate;

        private DateTime? endDate;

        private string comment;

        private IncidentNotificationData incidentNotification;

        private UnitData unit;

        private DispatchOrderStatusData status;

        private string customCode;

        private TimeSpan? expectedTime;

        private IList notes;

        private EndingReportData endingReport;

        #endregion

        #region Constructors

        public DispatchOrderData()
        {
        }

        public DispatchOrderData(
            DateTime startDate,
            DateTime endDate,
            string comment,
            IncidentNotificationData incidentNotification,
            UnitData unit,
            DispatchOrderStatusData status)
        {
            this.startDate = startDate;
            this.endDate = endDate;
            this.comment = comment;
            this.incidentNotification = incidentNotification;
            this.unit = unit;
            this.status = status;
        }

        #endregion

        #region Properties

        [Property(0, TypeType = typeof(DateTime))]
        [Column(1, Name = "START_DATE", NotNull = true)]
        public virtual DateTime? StartDate
        {
            get
            {
                return startDate;
            }
            set
            {
                startDate = value;
            }
        }

        [Property(0, TypeType = typeof(DateTime))]
        [Column(1, Name = "ARRIVAL_DATE")]
        public virtual DateTime? ArrivalDate
        {
            get
            {
                return arrivalDate;
            }
            set
            {
                arrivalDate = value;
            }
        }

        [Property(0, TypeType = typeof(DateTime))]
        [Column(1, Name = "END_DATE", UniqueKey = "UK_DISPATCH_ORDER_UNIT")]
        public virtual DateTime? EndDate
        {
            get
            {
                return endDate;
            }
            set
            {
                endDate = value;
            }
        }

        [Property(0)]
        [Column(1, Name = "COMMENT")]
        public virtual string Comment
        {
            get
            {
                return comment;
            }
            set
            {
                comment = value;
            }
        }

        [ManyToOne(0, ClassType = typeof(IncidentNotificationData), ForeignKey = "FK_INCIDENT_NOTIFICATION_DISPATCH_ORDERS_CODE", Cascade = "none")]
        [Column(1, Name = "INCIDENT_NOTIFICATION_CODE", NotNull = true, Index = "IX_DISPATCH_ORDER_INCIDENT_NOTIFICATION")]
        public virtual IncidentNotificationData IncidentNotification
        {
            get
            {
                return incidentNotification;
            }
            set
            {
                incidentNotification = value;
            }
        }

        [ManyToOne(0, ClassType = typeof(UnitData), ForeignKey = "FK_UNIT_DISPATCH_ORDERS_CODE")]
        [Column(1, Name = "UNIT_CODE", UniqueKey = "UK_DISPATCH_ORDER_UNIT")]
        public virtual UnitData Unit
        {
            get
            {
                return unit;
            }
            set
            {
                unit = value;
            }
        }

        [ManyToOne(ClassType = typeof(DispatchOrderStatusData), Column = "DISPATCH_ORDER_STATUS_CODE",
            ForeignKey = "FK_DISPATCH_ORDER_DISPATCH_ORDER_STATUS_CODE")]
        public virtual DispatchOrderStatusData Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "CUSTOM_CODE", UniqueKey = "UK_DISPATCH_ORDER_CUSTOM_CODE", NotNull = true)]
        public virtual string CustomCode
        {
            get
            {
                return customCode;
            }
            set
            {
                customCode = value;
            }
        }

        [Property(Column = "EXPECTED_TIME", NotNull = true, TypeType = typeof(TimeSpan))]
        public virtual TimeSpan? ExpectedTime
        {
            get
            {
                return expectedTime;
            }
            set
            {
                expectedTime = value;
            }
        }

        [Bag(0, Name = "Notes", Table = "DISPATCH_ORDER_NOTE", Cascade = "save-update",
           Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect, Inverse = true)]
        [Key(1, Column = "DISPATCH_ORDER_CODE"/*, ForeignKey = "FK_DISPATCH_ORDER_NOTE_CODE"*/)]
        [OneToMany(2, ClassType = typeof(DispatchOrderNoteData))]
        public virtual IList Notes
        {
            get
            {
                return notes;
            }
            set
            {
                notes = value;
            }
        }

        [ComponentProperty(ComponentType = typeof(EndingReportData))]
        public virtual EndingReportData EndingReport
        {
            get
            {
                return endingReport;
            }
            set
            {
                endingReport = value;
            }
        }

        /*[Property(0)]
        [Column(1, Name = "INDEX_CODE", NotNull = true)]
        public int Index
        {
            get
            {
                return index;
            }
            set
            {
                index = value;
            }
        }*/

        public string TimeInAttention
        {
            get
            {
                string result = "";
                if (this.ArrivalDate != null)
                {
                    DateTime endDate = DateTime.Now;
                    if (this.EndDate != null)
                        endDate = this.EndDate.Value;
                    TimeSpan timespan = TimeSpan.Zero;
                    timespan = endDate - this.ArrivalDate.Value;
                    result = timespan.Hours.ToString().PadLeft(2, '0');
                    result += ":" + timespan.Minutes.ToString().PadLeft(2, '0');
                    result += ":" + timespan.Seconds.ToString().PadLeft(2, '0');
                }
                return result;
            }
        }

        #endregion
    }
}
