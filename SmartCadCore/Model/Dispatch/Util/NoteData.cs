﻿using NHibernate.Mapping.Attributes;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    #region Class NoteData Documentation
    /// <summary>
    /// This class represents an generic note.
    /// </summary>
    /// <className>NoteData</className>
    /// <author email="jynojosa@smartmatic.com">Julio Ynojosa</author>
    /// <date>2007/02/07</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    [Serializable]
    public class NoteData : ObjectData
    {
        #region Fields

        private string completeUserName;

        private string userLogin;

        private string detail;

        private DateTime? creationDate;

        #endregion

        #region Properties

        [Property(Column = "{{completeUserName}}")]
        [AttributeIdentifier("completeUserName", Value = "COMPLETE_USER_NAME")]
        public virtual string CompleteUserName
        {
            get
            {
                return completeUserName;
            }
            set
            {
                completeUserName = value;
            }
        }

        [Property(Column = "{{userLogin}}")]
        [AttributeIdentifier("userLogin", Value = "USER_LOGIN")]
        public virtual string UserLogin
        {
            get
            {
                return userLogin;
            }
            set
            {
                userLogin = value;
            }
        }

        [Property(Column = "{{detail}}", Length = 16384)]
        [AttributeIdentifier("detail", Value = "DETAIL")]
        public virtual string Detail
        {
            get
            {
                return detail;
            }
            set
            {
                detail = value;
            }
        }

        [Property(Column = "{{creationDate}}", TypeType = typeof(DateTime))]
        [AttributeIdentifier("creationDate", Value = "CREATION_DATE")]
        public virtual DateTime? CreationDate
        {
            get
            {
                return creationDate;
            }
            set
            {
                creationDate = value;
            }
        }
        #endregion
    }

    [Serializable]
    [Class(NameType = typeof(IncidentNoteData), Table = "INCIDENT_NOTE", Lazy = false, Where = "DELETED_ID IS NULL")]
    public sealed class IncidentNoteData : NoteData
    {
        private IncidentData incident;

        [ManyToOne(ClassType = typeof(IncidentData), Column = "INCIDENT_CODE",
            ForeignKey = "FK_INCIDENT_INCIDENT_NOTE_CODE", NotNull = true, Cascade = "none")]
        public IncidentData Incident
        {
            get
            {
                return incident;
            }
            set
            {
                incident = value;
            }
        }
    }

    [Serializable]
    [Class(NameType = typeof(DispatchOrderNoteData), Table = "DISPATCH_ORDER_NOTE", Lazy = false, Where = "DELETED_ID IS NULL")]
    public sealed class DispatchOrderNoteData : NoteData
    {
        private DispatchOrderNoteTypeData type;
        private DispatchOrderData dispatchOrder;

        [ManyToOne(ClassType = typeof(DispatchOrderNoteTypeData), Column = "DISPATCH_ORDER_NOTE_TYPE",
           ForeignKey = "FK_DISPATCH_ORDER_NOTE_TYPE_CODE", NotNull = false, Cascade = "none")]
        public DispatchOrderNoteTypeData Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }
        }

        [ManyToOne(ClassType = typeof(DispatchOrderData), Column = "DISPATCH_ORDER_CODE",
           ForeignKey = "FK_DISPATCH_ORDER_NOTE_CODE", NotNull = true, Cascade = "none")]
        public DispatchOrderData DispatchOrder
        {
            get
            {
                return dispatchOrder;
            }
            set
            {
                dispatchOrder = value;
            }
        }

    }

    [Serializable]
    [Class(NameType = typeof(UnitNoteData), Table = "UNIT_NOTE", Lazy = false, Where = "DELETED_ID IS NULL")]
    public sealed class UnitNoteData : NoteData
    {
        private UnitData unit;
        private DispatchOrderData dispatchOrder;

        [ManyToOne(ClassType = typeof(UnitData), Column = "UNIT_CODE",
            ForeignKey = "FK_UNIT_UNIT_NOTE_CODE", NotNull = true, Cascade = "none")]
        public UnitData Unit
        {
            get
            {
                return unit;
            }
            set
            {
                unit = value;
            }
        }

        [ManyToOne(0, ClassType = typeof(DispatchOrderData), ForeignKey = "FK_DISPATCH_ORDER_UNIT_NOTE_CODE", Cascade = "none")]
        [Column(1, Name = "DISPATCH_ORDER_CODE", NotNull = true, Index = "IX_UNIT_NOTE_DISPATCH_ORDER")]
        public DispatchOrderData DispatchOrder
        {
            get
            {
                return dispatchOrder;
            }
            set
            {
                dispatchOrder = value;
            }
        }
    }
}
