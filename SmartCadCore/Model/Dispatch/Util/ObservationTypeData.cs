using System;
using System.Text;
using NHibernate.Mapping.Attributes;
using System.Collections;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(ObservationTypeData), Table = "OBSERVATION_TYPE", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_OBSERVATION_TYPE_NAME")]
    [AttributeIdentifier("DELETED_ID_2", Value = "UK_OBSERVATION_TYPE_FRIENDLY_NAME")]
    public class ObservationTypeData : ObjectData, INamedObjectData
    {
        #region Fields

        private string name;

        private string friendlyName;

		private IList userAccess;

        #endregion

        #region Properties

        [Property(0, Unique = true)]
        [Column(1, Name = "NAME", UniqueKey = "UK_OBSERVATION_TYPE_NAME", NotNull = true)]
        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "FRIENDLY_NAME", UniqueKey = "UK_OBSERVATION_TYPE_FRIENDLY_NAME", NotNull = true)]
        public virtual string FriendlyName
        {
            get
            {
                return friendlyName;
            }
            set
            {
                friendlyName = value;
            }
        }

		[Bag(0, Name = "UserAccess", Table = "OBSERVATION_TYPE_USER_ACCESS", Cascade = "none",
            Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect)]
		[Key(1, Column = "OBSERVATION_TYPE_CODE", ForeignKey = "FK_OBSERVATION_TYPE_OBSERVATION_TYPE_USER_ACCESS_CODE")]
		[ManyToMany(2, ClassType = typeof(UserAccessData), Column = "USER_ACCESS_CODE", ForeignKey = "FK_OBSERVATION_TYPE_USER_ACCESS_CODE")]
		public IList UserAccess
		{
			get
			{
				return userAccess;
			}
			set
			{
				userAccess = value;
			}
		}
        #endregion

        #region Overriden methods

        public override string ToString()
        {
            return this.FriendlyName;
        }

        #endregion
    }
}
