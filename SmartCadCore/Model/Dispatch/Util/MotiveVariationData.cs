﻿using NHibernate.Mapping.Attributes;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(MotiveVariationData), Table = "MOTIVE_VARIATION", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_MOTIVE_VARIATION_NAME")]
    public class MotiveVariationData : ObjectData, INamedObjectData
    {
        #region Fields

        private string name;
        private bool type;

        #endregion

        #region Constructors

        public MotiveVariationData()
        {
        }


        #endregion

        #region Properties


        [Property(0, Unique = true)]
        [Column(1, Name = "NAME", UniqueKey = "UK_MOTIVE_VARIATION_NAME", NotNull = true)]
        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        //is true when schedule is a extra time, otherwise is false.
        [Property(0, Unique = true)]
        [Column(2, Name = "TYPE", NotNull = true, UniqueKey = "UK_MOTIVE_VARIATION_NAME")]
        public virtual bool Type
        {
            get { return type; }
            set { type = value; }
        }


        #endregion

        public override string ToString()
        {
            string text = "";

            if (this.Name != null)
            {
                text = this.Name;
            }

            return text;
        }
    }
}
