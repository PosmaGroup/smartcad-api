﻿using System;
using System.Linq;
using System.Text;
using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(DispatchReportQuestionDispatchReportData), Table = "DISPATCH_REPORT_QUESTION_DISPATCH_REPORT", Lazy = false, Where = "DELETED_ID IS NULL")]
    public class DispatchReportQuestionDispatchReportData: ObjectData
    {
        #region Properties
        [ManyToOne(ClassType = typeof(QuestionDispatchReportData), Column = "QUESTION_DISPATCH_REPORT_CODE",
            Cascade = "none", ForeignKey = "FK_DISPATCH_REPORT_QUESTION_DISPATCH_REPORT_QUESTION_DISPATCH_REPORT_CODE")]
        public QuestionDispatchReportData Question
        {
            get;
            set;
        }

        [ManyToOne(Column = "DISPATCH_REPORT_CODE", ClassType = typeof(DispatchReportData),
            ForeignKey = "FK_DISPATCH_REPORT_QUESTION_DISPATCH_REPORT_DISPATCH_REPORT_CODE", Cascade = "none")]
        public DispatchReportData Report
        {
            get;
            set;
        }

        [Property(Column = "QUESTION_ORDER", NotNull = true)]
        public int Order
        {
            get;
            set;
        }


        #endregion


        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is DispatchReportQuestionDispatchReportData)
            {
                result = this.Code == ((DispatchReportQuestionDispatchReportData)obj).Code;
            }
            return result;
        }
    }

}
