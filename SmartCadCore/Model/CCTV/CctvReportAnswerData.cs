using System;
using System.Collections;
using System.Text;

using NHibernate.Mapping.Attributes;
using Iesi.Collections;

namespace SmartCadCore.Model
{
    #region Class CctvReportAnswerData Documentation
    /// <summary>
    /// This class represents the setAnswers of a cctv report
    /// </summary>
    /// <className>CctvReportAnswerData</className>
    /// <author email="fsilva@smartmatic.com">Fernando Silva</author>
    /// <date>2007/01/30</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
 
    [Serializable]
    [Class(NameType = typeof(CctvReportAnswerData), Table = "CCTV_REPORT_ANSWER", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_CCTV_REPORT_ANSWER_QUESTION_CCTV_REPORT_CODE")]
    public class CctvReportAnswerData : ObjectData
    {
        #region Fields

        private QuestionData question;
        private ISet setAnswers;
        private CctvReportData cctvReport;

        #endregion       
       
        #region Constructor

        public CctvReportAnswerData()
        {
        }

       

        public CctvReportAnswerData(QuestionData question, QuestionPossibleAnswerData answer, 
            string answerText)
        {
            this.question = question;
            PossibleAnswerAnswerData paad = new PossibleAnswerAnswerData();
            paad.CctvReportAnswer = this;
            paad.PossibleAnswer = answer;
            paad.TextAnswer = answerText;
            this.SetAnswers.Add(paad);
            //this.answer = answer;
        }

        public CctvReportAnswerData(QuestionData question, string answer)
        {
            this.question = question;
            PossibleAnswerAnswerData paad = new PossibleAnswerAnswerData();
            //this.answer = answer;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Cctv report question
        /// </summary>
        [ManyToOne(0, ClassType = typeof(QuestionData), ForeignKey = "FK_CCTV_REPORT_ANSWER_QUESTION_CODE", Unique = true, Cascade = "none")]
        [Column(1, Name = "CCTV_REPORT_ANSWER_QUESTION_CODE", UniqueKey = "UK_CCTV_REPORT_ANSWER_QUESTION_CCTV_REPORT_CODE", NotNull = true, Index = "IX_CCTV_REPORT_ANSWER_QUESTION_CCTV_REPORT")]
        public virtual QuestionData Question
        {
            get
            {
                return question;
            }
            set
            {
                question = value;
            }
        }

        [Set(0, Name = "SetAnswers", Table = "POSSIBLE_ANSWER_ANSWER_DATA", Cascade = "save-update",
            Lazy = CollectionLazy.True/*, Inverse = true*/, Fetch = CollectionFetchMode.Subselect)]
        [Key(1, Column = "CCTV_REPORT_ANSWER_CODE"/*, ForeignKey = "FK_POSSIBLE_ANSWER_ANSWER_CCTV_REPORT_ANSWER"*/)]
        [OneToMany(2, ClassType = typeof(PossibleAnswerAnswerData))]
        public ISet SetAnswers
        {
            get
            {
                return setAnswers;
            }
            set
            {
                setAnswers = value;
            }
        } 

        /// <summary>
        /// CCTV report parent of this answer
        /// </summary>
        [ManyToOne(0, ClassType = typeof(CctvReportData), ForeignKey = "FK_CCTV_REPORT_CCTV_REPORT_ANSWER_CODE", Unique = true, Cascade = "none")]
        [Column(1, Name = "CCTV_REPORT_CODE", UniqueKey = "UK_CCTV_REPORT_ANSWER_QUESTION_CCTV_REPORT_CODE", NotNull = true, Index = "IX_CCTV_REPORT_ANSWER_QUESTION_CCTV_REPORT")]
        public virtual CctvReportData CctvReport
        {
            get
            {
                return cctvReport;
            }
            set
            {
                cctvReport = value;
            }
        }
        #endregion

        public override bool Equals(object obj)
        {

            if ((question.Code != (obj as CctvReportAnswerData).Question.Code)&&
                (question.Text != (obj as CctvReportAnswerData).Question.Text) &&
                (question.ShortText != (obj as CctvReportAnswerData).Question.ShortText))
                return false;
            else
                return true;
        }
    }
}
