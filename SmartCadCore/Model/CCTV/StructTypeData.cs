using System;
using System.Text;
using NHibernate.Mapping.Attributes;
using Iesi.Collections;


namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(StructTypeData), Table = "STRUCT_TYPE", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_STRUCT_TYPE")]
    public class StructTypeData : ObjectData
    {
        private string name = string.Empty;
      
        [Property(0, Unique = true)]
        [Column(1, Name = "NAME", UniqueKey = "UK_STRUCT_TYPE", NotNull = true)]
        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public override string ToString()
        {
            return this.Name;
        }

        public override bool Equals(object obj)
        {
            try
            {
                if (obj.GetType() == typeof(StructTypeData))
                {
                    StructTypeData str = (StructTypeData)obj;
                    if (this.Name == str.Name)
                    {
                        return true;
                    }
                }
            }
            catch { }
            return false;
        }

        [InitialData(PropertyName = "Name", PropertyValue = "StructTypeData")]
        public static readonly UserResourceData Resource;
    }
}
