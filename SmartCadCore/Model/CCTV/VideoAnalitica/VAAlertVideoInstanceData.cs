﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(VAAlertVideoInstanceData), Table = "VA_ALERT_VIDEO_INSTANCE", Lazy = false, BatchSize = 1000)]
    public class VAAlertVideoInstanceData : ObjectData
    {

        #region Fields


        private long objectId;

        public enum TypeObjectEnum
        {
            ObjectInstanceOnEvent,
            ObjectInstanceOnBestVIew,
            ObjectInstance
        }

        private VAAlertCamData alert;

        private TypeObjectEnum typeObject;

        private DateTime timestampValue;

        private int timestampMilliseconds;

        private float boundingBoxLeft;

        private float boundingBoxTop;

        private float boundingBoxRight;

        private float boundingBoxButtom;

        private float footX;

        private float footY;


        #endregion

        #region Properties

        [Property(0)]
        [Column(1, Name = "OBJECT_ID")]
        public virtual long ObjectId
        {
            get { return objectId; }
            set { objectId = value; }
        }

        [Property(0, TypeType = typeof(DateTime))]
        [Column(1, Name = "TIMESTAMP_VALUE")]
        public virtual DateTime TimestampValue
        {
            get { return timestampValue; }
            set { timestampValue = value; }
        }

        [Property(0)]
        [Column(1, Name = "TIMESTAMP_MILLISECONDS")]
        public virtual int TimestampMilliseconds
        {
            get { return timestampMilliseconds; }
            set { timestampMilliseconds = value; }
        }

        [Property(0)]
        [Column(1, Name = "BOUNDING_BOX_LEFT")]
        public virtual float BoundingBoxLeft
        {
            get { return boundingBoxLeft; }
            set { boundingBoxLeft = value; }
        }

        [Property(0)]
        [Column(1, Name = "BOUNDING_BOX_TOP")]
        public virtual float BoundingBoxTop
        {
            get { return boundingBoxTop; }
            set { boundingBoxTop = value; }
        }

        [Property(0)]
        [Column(1, Name = "BOUNDING_BOX_RIGHT")]
        public virtual float BoundingBoxRight
        {
            get { return boundingBoxRight; }
            set { boundingBoxRight = value; }
        }

        [Property(0)]
        [Column(1, Name = "BOUNDING_BOX_BUTTOM")]
        public virtual float BoundingBoxButtom
        {
            get { return boundingBoxButtom; }
            set { boundingBoxButtom = value; }
        }

        [Property(0)]
        [Column(1, Name = "FOOT_X")]
        public virtual float FootX
        {
            get { return footX; }
            set { footX = value; }
        }

        [Property(0)]
        [Column(1, Name = "FOOT_Y")]
        public virtual float FootY
        {
            get { return footY; }
            set { footY = value; }
        }

        [Property(0, TypeType = typeof(TypeObjectEnum))]
        [Column(1, Name = "TYPE_OBJECT")]
        public virtual TypeObjectEnum TypeObject
        {
            get { return typeObject; }
            set { typeObject = value; }
        }

         [ManyToOne(0, ClassType = typeof(VAAlertCamData), ForeignKey = "FK_VA_ALERT_VIDEO_INSTANCE_ALERT_CAM", Cascade = "none", Column = "ALERT_CODE")]    
        [Column(1, Name = "ALERT_CODE")]
        public virtual VAAlertCamData Alert
        {
            get { return alert; }
            set { alert = value; }


        }

        #endregion

         #region InitialData

         [InitialData(PropertyName = "Name", PropertyValue = "VAAlertVideoInstanceData")]
         public static readonly UserResourceData Resource;
         #endregion
    }
}
