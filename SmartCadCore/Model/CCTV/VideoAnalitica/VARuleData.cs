using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(VARuleData), Table = "VA_RULE", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_VA_RULE_SAVI_CODE")]
    public class VARuleData : ObjectData
    {

        #region Fields

        private string name;
        private string saviCode;
        #endregion

        #region Properties

        [Property(0)]
        [Column(1, Name = "NAME", NotNull = true)]
        public virtual string Name
        {
            get { return name; }
            set { name = value; }
        }


        [Property(0, Unique = true)]
        [Column(1, Name = "SAVI_CODE", UniqueKey = "UK_VA_RULE_SAVI_CODE", NotNull = true)]
        public virtual string SaviCode
        {
            get { return saviCode; }
            set { saviCode = value; }
        }
        

        public override string ToString()
        {
            return name;
        }
     
        public override bool Equals(object obj)
        {
            if (obj.GetType() == typeof(VARuleData))
            {
                VARuleData stc = (VARuleData)obj;
                if (this.Code == stc.Code)
                {
                    return true;
                }
            }
            return false;
        }
        #endregion

        #region InitialData

        [InitialData(PropertyName = "Name", PropertyValue = "VARuleData")]
        public static readonly UserResourceData Resource;
        #endregion
    }
}
