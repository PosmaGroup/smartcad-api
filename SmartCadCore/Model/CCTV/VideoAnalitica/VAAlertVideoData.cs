﻿using Iesi.Collections;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    [Serializable]
    [Component()]
    public class VAAlertVideoData
    {

        #region Fields

        private long objectId;

        private DateTime eventTimeValue;

        private int eventTimeMilliseconds; 

        private DateTime? birthTimeValue;

        private int? birtTimeMilliseconds;

        private DateTime? deathTimeValue;

        private int? deathTimeMilliseconds;

        private int totalObjectInstances;

        private ISet videoInstances;



        #endregion

        #region Properties
        [Property(0) ]
        [Column(1, Name = "OBJECT_ID", NotNull = true)]
        public virtual long ObjectId
        {
            get { return objectId; }
            set { objectId = value; }
        }

        [Property(0, TypeType = typeof(DateTime))]
        [Column(1, Name = "EVENT_TIME_VALUE", NotNull = true)]
        public virtual DateTime EventTimeValue
        {
            get { return eventTimeValue; }
            set { eventTimeValue = value; }
        }

        [Property(0)]
        [Column(1, Name = "EVENT_TIME_MILLISECONDS", NotNull = true)]
        public virtual int EventTimeMilliseconds
        {
            get { return eventTimeMilliseconds; }
            set { eventTimeMilliseconds = value; }
        }


        [Property(0, TypeType = typeof(DateTime))]
        [Column(1, Name = "BIRTH_TIME_VALUE", NotNull = false)]
        public virtual DateTime? BirthTimeValue
        {
            get { return birthTimeValue; }
            set { birthTimeValue = value; }
        }

        [Property(0)]
        [Column(1, Name = "BIRTH_TIME_MILLISECONDS", NotNull = false)]
        public virtual int? BirtTimeMilliseconds
        {
            get { return birtTimeMilliseconds; }
            set { birtTimeMilliseconds = value; }
        }

        [Property(0, TypeType = typeof(DateTime))]
        [Column(1, Name = "DEATH_TIME_VALUE", NotNull = false)]
        public virtual DateTime? DeathTimeValue
        {
            get { return deathTimeValue; }
            set { deathTimeValue = value; }
        }

        [Property(0)]
        [Column(1, Name = "DEATH_TIME_MILLISECONDS", NotNull = false)]
        public virtual int? DeathTimeMilliseconds
        {
            get { return deathTimeMilliseconds; }
            set { deathTimeMilliseconds = value; }
        }

        [Property(0)]
        [Column(1, Name = "TOTAL_OBJET_INSTANCES", NotNull = true)]
        public virtual int TotalObjectInstances
        {
            get { return totalObjectInstances; }
            set { totalObjectInstances = value; }
        }

        [Set(0, Name = "VideoInstances", Table = "VA_ALERT_VIDEO_INSTANCE", Cascade = "save-update",
            Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect)]
        [Key(1, Column = "ALERT_CODE")]
        [OneToMany(2, ClassType = typeof(VAAlertVideoInstanceData))]
        public ISet VideoInstances
        {
            get { return videoInstances; }
            set { videoInstances = value; }
        }
        
        #endregion

    }
}
