using System;
using System.Text;
using NHibernate.Mapping.Attributes;
using Iesi.Collections;
using System.Collections;

namespace SmartCadCore.Model
{
    [Serializable]
    [JoinedSubclass(Table = "VIDEO_DEVICE", NameType = typeof(VideoDeviceData), ExtendsType = typeof(DeviceData), Lazy = false)]
    public class VideoDeviceData : DeviceData
    {      
         private StructData myStruct;
         private int parentCode;

         [ManyToOne(ClassType = typeof(StructData), Column = "STRUCT_CODE",
           ForeignKey = "FK_STRUCT_VIDEO_DEVICE_CODE", Cascade = "none")]       
        public StructData Struct
        {
            get
            {
                return myStruct;
            }
            set
            {
                myStruct = value;
            }
        }


         [Key(0, Column = "PARENT_CODE", ForeignKey = "FK_DEVICE_CODE")]
         public virtual int ParentCode
         {
             get
             {
                 return parentCode;
             }
             set
             {
                 parentCode = value;
             }
         }

        [InitialData(PropertyName = "Name", PropertyValue = "VideoDeviceData")]
        public static readonly UserResourceData Resource;
        public override string ToString()
        {
            return Name;
        }
        public override bool Equals(object obj)
        {
            try
            {
                if (obj.GetType() == typeof(VideoDeviceData))
                {
                    VideoDeviceData cam = (VideoDeviceData)obj;
                    if (this.Name == cam.Name)
                    {
                        return true;
                    }
                }
            }
            catch { }
            return false;
        }
    }

}
