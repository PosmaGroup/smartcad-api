using System;
using System.Collections;
using System.Text;

using NHibernate.Mapping.Attributes;
using Iesi.Collections;

namespace SmartCadCore.Model
{
    #region Class CctvReportData Documentation
    /// <summary>
    /// This class represents the cctv report.
    /// </summary>
    /// <className>CctvReportData</className>
    /// <author email="fsilva@smartmatic.com">Fernando Silva</author>
    /// <date>2007/01/30</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    [Serializable]
    [JoinedSubclass(Table = "CCTV_REPORT", NameType = typeof(CctvReportData), ExtendsType = typeof(ReportBaseData), Lazy = false)]
    public class CctvReportData : ReportBaseData
    {
        #region Fields

        private int parentCode;   
        private ISet setAnswers;       
        //private DateTime? statrtIncidentCameraTime;
        //private DateTime? finishedIncidentCameraTime;

        #endregion

        #region Constructors
        
        public CctvReportData()
        {
        }
        public CctvReportData(
            CameraData camera, DateTime startDate,        
            ISet answers)
        {
            this.Camera = camera;
            this.StartDate = startDate;
            this.setAnswers = answers;
        }

        #endregion

        #region Properties

        [Key(0, Column = "PARENT_CODE", ForeignKey = "FK_CCTV_REPORT_REPORT_BASE_CODE")]
        public virtual int ParentCode
        {
            get
            {
                return parentCode;
            }
            set
            {
                parentCode = value;
            }
        }     
               
        [Set(0, Name = "SetAnswers", Table = "CCTV_REPORT_ANSWER", Cascade = "all-delete-orphan",
            Lazy = CollectionLazy.True/*, Inverse = true*/, Fetch = CollectionFetchMode.Subselect)]
        [Key(1, Column = "CCTV_REPORT_CODE"/*, ForeignKey = "FK_CCTV_REPORT_CCTV_REPORT_ANSWER_CODE"*/)]
        [OneToMany(2, ClassType = typeof(CctvReportAnswerData))]
        public ISet SetAnswers
        {
            get
            {
                return setAnswers;
            }
            set
            {
                setAnswers = value;
            }
        }

        /// <summary>
        /// Asociated Camera.
        /// </summary>
        [ManyToOne(ClassType = typeof(CameraData), Column = "CAMERA_CODE",
            ForeignKey = "FK_CCTV_REPORT_CAMERA_CODE", NotNull = true, Cascade = "none")]
        public CameraData Camera { get; set; }

        /// <summary>
        /// Indicates when the event was generated.
        /// </summary>
        [Property(0, TypeType = typeof(DateTime))]
        [Column(1, Name = "START_DATE")]
        public DateTime StartDate { get; set; }


        #endregion

        #region doc
        //private bool? incomplete;
        //private PhoneReportCallerData caller;
        //private DateTime? registeredCallTime;
        //private DateTime? receivedCallTime;
        //private DateTime? pickedUpCallTime;
        //[Property(Column = "REGISTERED_CALL_TIME", TypeType = typeof(DateTime))]
        //public virtual DateTime? RegisteredCallTime
        //{
        //    get
        //    {
        //        return registeredCallTime;
        //    }
        //    set
        //    {
        //        registeredCallTime = value;
        //    }
        //}

        //[Property(Column = "RECEIVED_CALL_TIME", TypeType = typeof(DateTime))]
        //public virtual DateTime? ReceivedCallTime
        //{
        //    get
        //    {
        //        return receivedCallTime;
        //    }
        //    set
        //    {
        //        receivedCallTime = value;
        //    }
        //}

        //[Property(Column = "HANGED_UP_CALL_TIME", TypeType = typeof(DateTime))]
        //public virtual DateTime? HangedUpCallTime
        //{
        //    get
        //    {
        //        return hangedUpCallTime;
        //    }
        //    set
        //    {
        //        hangedUpCallTime = value;
        //    }
        //}

        //[Property(Column = "INCOMPLETE", TypeType = typeof(bool))]
        //public virtual bool? Incomplete
        //{
        //    get
        //    {
        //        return incomplete;
        //    }
        //    set
        //    {
        //        incomplete = value;
        //    }
        //}
        //[ComponentProperty(ComponentType = typeof(PhoneReportCallerData))]
        //public virtual PhoneReportCallerData Caller
        //{
        //    get
        //    {
        //        return caller;
        //    }
        //    set
        //    {
        //        caller = value;
        //    }
        //}
        #endregion doc
      
    }

   
}
