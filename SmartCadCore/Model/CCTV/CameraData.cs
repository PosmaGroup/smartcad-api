using System;
using System.Text;
using NHibernate.Mapping.Attributes;
using Iesi.Collections;
using System.Collections;

namespace SmartCadCore.Model
{    
    [Serializable]
    //[Class(Table = "CAMERA", Lazy = false,, Where = "DELETED_ID IS NULL"]
    [JoinedSubclass(Table = "CAMERA", NameType = typeof(CameraData), ExtendsType = typeof(VideoDeviceData), Lazy = false)]
    //[AttributeIdentifier("DELETED_ID_1", Value = "UK_CAMERA_NAME")]
    //#if !DEBUG
    //[AttributeIdentifier("DELETED_ID_2", Value = "UK_CAMERA_IP")]
    //#endif    
    public class CameraData : VideoDeviceData, INamedObjectData
    {
        //private StructData myStruct;

        private CameraTypeData type;
        private ConnectionTypeData connectionType;
        private int parentCode;
        private string customId;
        private string resolution;
        private string streamType;
        private string frameRate;

        [Key(0, Column = "PARENT_CODE", ForeignKey = "FK_CAMERA_VIDEO_DEVICE_CODE")]
        public virtual int ParentCode
        {
            get
            {
                return parentCode;
            }
            set
            {
                parentCode = value;
            }
        }

        [Property(Column = "CUSTOM_ID")]
        public string CustomId
        {
            get
            {
                return customId;
            }
            set
            {
                customId = value;
            }
        }

        [ManyToOne(Column = "CAMERA_TYPE", ClassType = typeof(CameraTypeData), 
            ForeignKey = "FK_STRUCT_CAMERA_TYPE", NotNull = true, Cascade = "none")]
        public CameraTypeData Type
        {
            get
            {
                return type;
            }
            set
            {               
                type = value;
            }
        }

        [ManyToOne(Column = "CONNECTION_TYPE", 
            ClassType = typeof(ConnectionTypeData), ForeignKey = "FK_STRUCT_CONNECTION_TYPE", NotNull = true, Cascade = "none")]
        public ConnectionTypeData ConnectionType
        {
            get
            {
                return connectionType;
            }
            set
            {
                connectionType = value;
            }
        }

        [Property(Column = "RESOLUTION", NotNull = true)]
        public string Resolution
        {
            get
            {
                return resolution;
            }
            set
            {
                resolution = value;
            }
        }
        [Property(Column = "STREAMTYPE", NotNull = true)]
        public string StreamType
        {
            get
            {
                return streamType;
            }
            set
            {
                if (value != "")
                streamType = value;
            }
        }
        [Property(Column = "FRAMERATE", NotNull = true)]
        public string FrameRate
        {
            get
            {
                return frameRate;
            }
            set
            {
                
                frameRate = value;
            }
        }

        [InitialData(PropertyName = "Name", PropertyValue = "CameraData")] 
        public static readonly UserResourceData Resource;


        public override string ToString()
        {
            return Name;
        }

        public override bool Equals(object obj)
        {
            try
            {
                if (obj.GetType() == typeof(CameraData))
                {
                    CameraData cam = (CameraData)obj;
                    if (this.Name == cam.Name)
                    {
                        return true;
                    }
                }
            }
            catch { }
            return false;
        }
    }

}
