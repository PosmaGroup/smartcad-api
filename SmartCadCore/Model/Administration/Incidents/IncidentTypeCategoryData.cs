using System;
using System.Collections;
using System.Text;
using NHibernate.Mapping.Attributes;


namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(IncidentTypeCategoryData), Table = "INCIDENT_TYPE_CATEGORY", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_INCIDENT_TYPE_CATEGORY_NAME")]
    [AttributeIdentifier("DELETED_ID_2", Value = "UK_INCIDENT_TYPE_CATEGORY_CUSTOM_CODE")]
    public class IncidentTypeCategoryData : ObjectData, INamedObjectData
    {
        #region Fields

        private string customCode;

        private string name;

        private IList incidentTypes;

        #endregion

        #region Constructors

        public IncidentTypeCategoryData()
        { }

        public IncidentTypeCategoryData(string name, string customCode)
        {
            this.name = name;
            this.customCode = customCode;
        }

        #endregion

        #region Properties

        [Property(0, Unique = true)]
        [Column(1, Name = "CUSTOM_CODE", UniqueKey = "UK_INCIDENT_TYPE_CATEGORY_CUSTOM_CODE", NotNull = true)]
        public string CustomCode
        {
            get
            { 
                return customCode; 
            }
            set
            {
                customCode = value; 
            }
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "NAME", UniqueKey = "UK_INCIDENT_TYPE_CATEGORY_NAME", NotNull = true)]
        public string Name
        {
            get 
            {
                return name; 
            }
            set 
            {
                name = value; 
            }
        }

        [Bag(0, Name = "IncidentTypes", Table = "INCIDENT_TYPE_INCIDENT_TYPE_CATEGORY", Cascade = "none",
            Lazy = CollectionLazy.True, Inverse = true, Fetch = CollectionFetchMode.Subselect)]
        [Key(1, Column = "INCIDENT_TYPE_CATEGORY_CODE"/*, ForeignKey = "FK_INCIDENT_TYPE_INCIDENT_TYPE_CATEGORY_CODE"*/)]
        [ManyToMany(2, ClassType = typeof(IncidentTypeData), Column = "INCIDENT_TYPE_CODE", ForeignKey = "FK_INCIDENT_TYPE_CATEGORY_INCIDENT_TYPE_CODE")]
        public IList IncidentTypes
        {
            get
            {
                return incidentTypes;
            }
            set
            {
                incidentTypes = value;
            }
        }
        #endregion

        #region InitialData

        [InitialData(PropertyName = "CustomCode", PropertyValue = "PHONECALL")]
        public static IncidentTypeCategoryData PhoneCall;

        [InitialData(PropertyName = "CustomCode", PropertyValue = "CAMERA")]
        public static IncidentTypeCategoryData Camera;

        #endregion
    }
}
