﻿using NHibernate.Mapping.Attributes;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(IncidentNotificationStatusHistoryData), Table = "INCIDENT_NOTIFICATION_STATUS_HISTORY", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("UK_START", Value = "UK_INCIDENT_NOTIFICATION_HISTORY")]
    [AttributeIdentifier("UK_END", Value = "UK_INCIDENT_NOTIFICATION_HISTORY")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_INCIDENT_NOTIFICATION_HISTORY")]
    [AttributeIdentifier("IX_START", Value = "IX_INCIDENT_NOTIFICATION_STATUS_HISTORY_START")]
    [AttributeIdentifier("UK_END1", Value = "UK_INCIDENT_NOTIFICATION_HISTORY_USER_ACCOUNT")]
    [AttributeIdentifier("UK_START1", Value = "UK_INCIDENT_NOTIFICATION_HISTORY_USER_ACCOUNT")]
    [AttributeIdentifier("DELETED_ID_2", Value = "UK_INCIDENT_NOTIFICATION_HISTORY_USER_ACCOUNT")]
    public class IncidentNotificationStatusHistoryData : BaseSessionHistory
    {
        #region Fields

        private OperatorData userAccount;

        private IncidentNotificationData incidentNotification;

        private IncidentNotificationStatusData status;

        private string detail;

        #endregion

        #region Properties

        /// <summary>
        /// User account who is logged in.
        /// </summary>

        [ManyToOne(0, ClassType = typeof(OperatorData), Insert = true, Update = false,
           ForeignKey = "FK_INCIDENT_NOTIFICATION_STATUS_HISTORY_USER_ACCOUNT_CODE", Cascade = "none")]
        [Column(1, Name = "USER_ACCOUNT_CODE", NotNull = true, UniqueKey = "UK_INCIDENT_NOTIFICATION_HISTORY")]
        public virtual OperatorData UserAccount
        {
            get
            {
                return userAccount;
            }
            set
            {
                userAccount = value;
            }
        }

        [ManyToOne(0, ClassType = typeof(IncidentNotificationData),
          ForeignKey = "FK_INCIDENT_NOTIFICATION_STATUS_HISTORY_INCIDENT_NOTIFICATION_CODE", Cascade = "none")]
        [Column(1, Name = "INCIDENT_NOTIFICATION_CODE", NotNull = true,
            UniqueKey = "UK_INCIDENT_NOTIFICATION_HISTORY",
            Index = "IX_INCIDENT_NOTIFICATION_STATUS_HISTORY_INCIDENT_NOTIFICATION")]
        [Column(2, Name = "INCIDENT_NOTIFICATION_CODE", NotNull = true,
           UniqueKey = "UK_INCIDENT_NOTIFICATION_HISTORY_USER_ACCOUNT")]
        public IncidentNotificationData IncidentNotification
        {
            get
            {
                return incidentNotification;
            }
            set
            {
                incidentNotification = value;
            }
        }

        [ManyToOne(0, ClassType = typeof(IncidentNotificationStatusData),
         ForeignKey = "FK_INCIDENT_NOTIFICATION_STATUS_HISTORY_INCIDENT_NOTIFICATION_STATUS_CODE", Cascade = "none")]
        [Column(1, Name = "INCIDENT_NOTIFICATION_STATUS_CODE", NotNull = true, UniqueKey = "UK_INCIDENT_NOTIFICATION_HISTORY")]
        [Column(2, Name = "INCIDENT_NOTIFICATION_STATUS_CODE", NotNull = true, UniqueKey = "UK_INCIDENT_NOTIFICATION_HISTORY_USER_ACCOUNT")]
        public virtual IncidentNotificationStatusData Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }

        [Property(Column = "DETAIL", Length = 1024, NotNull = false)]
        public string Detail
        {
            get
            {
                return detail;
            }
            set
            {
                detail = value;
            }
        }

        #endregion
    }
}
