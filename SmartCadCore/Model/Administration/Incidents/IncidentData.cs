﻿using Iesi.Collections;
using NHibernate.Mapping.Attributes;
using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    #region Class IncidentData Documentation
    /// <summary>
    /// This class represents an incident.
    /// </summary>
    /// <className>IncidentData</className>
    /// <author email="fsilva@smartmatic.com">Fernando Silva</author>
    /// <date>2007/01/30</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    [Serializable]
    [Class(NameType = typeof(IncidentData), Table = "INCIDENT", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_INCIDENT_CUSTOM_CODE")]
    public class IncidentData : ObjectData
    {
        #region Fields

        private ISet setReportBaseList;
        private DateTime? startDate;
        private DateTime? endDate;
        private IList incidentNotifications;
        private IncidentStatusData status;
        private IncidentAddressData address;
        private string customCode;
        private IList incidentTypes;
        private IList notes;
        private bool? isEmergency;


        #endregion

        #region Constructors

        public IncidentData()
        {
        }

        public IncidentData(
            ISet reportBaseList,
            DateTime startDate,
            DateTime endDate,
            IList incidentNotifications,
            IncidentStatusData status)
        {
            this.setReportBaseList = reportBaseList;
            this.startDate = startDate;
            this.endDate = endDate;
            this.incidentNotifications = incidentNotifications;
            this.status = status;

        }

        #endregion

        #region Properties


        [Set(0, Name = "SetReportBaseList", Table = "REPORT_BASE", Cascade = "all-delete-orphan",
            Lazy = CollectionLazy.True/*, Inverse = true*/, Fetch = CollectionFetchMode.Subselect)]
        [Key(1, Column = "INCIDENT_CODE"/*, ForeignKey = "FK_INCIDENT_REPORT_BASE_CODE"*/)]
        [OneToMany(2, ClassType = typeof(ReportBaseData))]
        public ISet SetReportBaseList
        {
            get
            {
                return setReportBaseList;
            }
            set
            {
                setReportBaseList = value;
            }
        }

        [Property(0, TypeType = typeof(DateTime))]
        [Column(1, Name = "START_DATE", NotNull = true)]
        public virtual DateTime? StartDate
        {
            get
            {
                return startDate;
            }
            set
            {
                startDate = value;
            }
        }

        [Property(0, TypeType = typeof(DateTime))]
        [Column(1, Name = "END_DATE", Index = "IX_INCIDENT_END_DATE")]
        public virtual DateTime? EndDate
        {
            get
            {
                return endDate;
            }
            set
            {
                endDate = value;
            }
        }

        public virtual IList IncidentNotifications
        {
            get
            {
                return incidentNotifications;
            }
            set
            {
                incidentNotifications = value;
            }
        }

        [ManyToOne(ClassType = typeof(IncidentStatusData), Column = "INCIDENT_STATUS_CODE",
            ForeignKey = "FK_INCIDENT_INCIDENT_STATUS_CODE", NotNull = true, Cascade = "none")]
        public virtual IncidentStatusData Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }

        [ComponentProperty(ComponentType = typeof(IncidentAddressData))]
        public virtual IncidentAddressData Address
        {
            get
            {
                return address;
            }
            set
            {
                address = value;
            }
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "CUSTOM_CODE", UniqueKey = "UK_INCIDENT_CUSTOM_CODE", NotNull = true)]
        public virtual string CustomCode
        {
            get
            {
                return customCode;
            }
            set
            {
                customCode = value;
            }
        }

        public virtual IList IncidentTypes
        {
            get
            {
                return this.incidentTypes;
            }
            set
            {
                this.incidentTypes = value;
            }
        }

        [Bag(0, Name = "Notes", Table = "INCIDENT_NOTE", Cascade = "save-update",
           Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect)]
        [Key(1, Column = "INCIDENT_CODE"/*, ForeignKey = "FK_INCIDENT_INCIDENT_NOTE_CODE"*/)]
        [OneToMany(2, ClassType = typeof(IncidentNoteData))]
        public virtual IList Notes
        {
            get
            {
                return notes;
            }
            set
            {
                notes = value;
            }
        }

        [Property(0, TypeType = typeof(bool))]
        [Column(1, Name = "IS_EMERGENCY", NotNull = true)]
        public virtual bool? IsEmergency
        {
            get
            {
                return this.isEmergency;
            }
            set
            {
                this.isEmergency = value;
            }
        }

        private OperatorData createOperator;
        [ManyToOne(ClassType = typeof(OperatorData), Column = "CREATE_OPERATOR_CODE", Insert = true, Update = false,
            ForeignKey = "FK_INCIDENT_CREATE_OPERATOR_CODE", NotNull = false, Cascade = "none")]
        public virtual OperatorData CreateOperator
        {
            get
            {
                return createOperator;
            }
            set
            {
                createOperator = value;
            }
        }

        private OperatorData closeOperator;
        [ManyToOne(ClassType = typeof(OperatorData), Column = "CLOSE_OPERATOR_CODE",
           ForeignKey = "FK_INCIDENT_CLOSE_OPERATOR_CODE", NotNull = false, Cascade = "none")]
        public virtual OperatorData CloseOperator
        {
            get
            {
                return closeOperator;
            }
            set
            {
                closeOperator = value;
            }
        }

        private UserApplicationData sourceIncidentApp;

        [ManyToOne(ClassType = typeof(UserApplicationData), Column = "APP_CODE",
           ForeignKey = "FK_APP_INCIDENT_CODE", Cascade = "none")]
        public UserApplicationData SourceIncidentApp
        {
            get
            {
                return sourceIncidentApp;
            }
            set
            {
                sourceIncidentApp = value;
            }
        }

        #endregion
    }


}
