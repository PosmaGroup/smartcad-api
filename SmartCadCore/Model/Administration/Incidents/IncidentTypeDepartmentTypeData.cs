﻿using NHibernate.Mapping.Attributes;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(IncidentTypeDepartmentTypeData), Table = "INCIDENT_TYPE_DEPARTMENT_TYPE", Lazy = false, Where = "DELETED_ID IS NULL")]
    public class IncidentTypeDepartmentTypeData : ObjectData
    {
        #region Fields
        private IncidentTypeData incidentType;
        private DepartmentTypeData departmentType;
        #endregion

        #region Properties
        [ManyToOne(ClassType = typeof(DepartmentTypeData), Column = "DEPARTMENT_TYPE_CODE",
            Cascade = "none", ForeignKey = "FK_INCIDENT_TYPE_DEPARTMENT_TYPE_DEPARTMENT_TYPE_CODE")]
        public virtual DepartmentTypeData DepartmentType
        {
            get
            {
                return departmentType;
            }
            set
            {
                departmentType = value;
            }
        }

        [ManyToOne(ClassType = typeof(IncidentTypeData), Column = "INCIDENT_TYPE_CODE",
            Cascade = "none", ForeignKey = "FK_INCIDENT_TYPE_DEPARTMENT_TYPE_INCIDENT_TYPE_CODE")]
        public virtual IncidentTypeData IncidentType
        {
            get
            {
                return incidentType;
            }
            set
            {
                incidentType = value;
            }
        }

        /*[Property(0)]
        [Column(1, Name = "INDEX_CODE")]
        public int Index
        {
            get
            {
                return index;
            }
            set
            {
                index = value;
            }
        }*/

        #endregion
    }
}
