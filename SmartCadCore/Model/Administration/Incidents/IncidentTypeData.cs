﻿using Iesi.Collections;
using NHibernate.Mapping.Attributes;
using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    #region Class IncidentType Documentation
    /// <summary>
    /// This class represents a incident type
    /// </summary>
    /// <className>IncidentType</className>
    /// <author email="fsilva@smartmatic.com">Fernando Silva</author>
    /// <date>2007/01/30</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    [Serializable]
    [Class(NameType = typeof(IncidentTypeData), Table = "INCIDENT_TYPE", Lazy = false, BatchSize = 100, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_INCIDENT_TYPE_NAME")]
    [AttributeIdentifier("DELETED_ID_2", Value = "UK_INCIDENT_TYPE_FRIENDLY_NAME")]
    public class IncidentTypeData : ObjectData, INamedObjectData
    {
        #region Enums

        /// <summary>
        /// Enumerates the properties that this class has.
        /// </summary>
        public enum Properties
        {
            Name,
            FriendlyName,
            CustomCode,
            Exclusive,
            Procedure,
            IncidentTypeDepartmentTypes,
            IncidentTypeQuestions,
            NoEmergency,
            IconName
        }

        #endregion

        #region Constructors

        public IncidentTypeData()
        {
        }

        public IncidentTypeData(string customCode, string name)
        {
            this.CustomCode = customCode;
            this.Name = name;
        }

        #endregion

        #region Properties

        /// <summary>
        /// The custom code of the incident type
        /// </summary>
        [Property(0/*, Unique = true*/)]
        [Column(1, Name = "CUSTOM_CODE",/* UniqueKey = "UK_INCIDENT_TYPE_CUSTOM_CODE",*/ NotNull = true)]
        public virtual string CustomCode { get; set; }

        /// <summary>
        /// Name of the incident type
        /// </summary>
        [Property(0, Unique = true)]
        [Column(1, Name = "NAME", UniqueKey = "UK_INCIDENT_TYPE_NAME", NotNull = true)]
        public virtual string Name { get; set; }

        [Property(0, Unique = true)]
        [Column(1, Name = "FRIENDLY_NAME", UniqueKey = "UK_INCIDENT_TYPE_FRIENDLY_NAME", NotNull = true)]
        public virtual string FriendlyName { get; set; }

        [Property(Column = "EXCLUSIVE", TypeType = typeof(bool))]
        public bool? Exclusive { get; set; }

        [Property(Column = "NO_EMERGENCY", TypeType = typeof(bool))]
        public bool? NoEmergency { get; set; }

        [Property(0, Column = "EMERGENCY_PROCEDURE", Length = 16384)]
        public virtual string Procedure { get; set; }

        [Property(0, Column = "ICON_NAME")]
        public string IconName { get; set; }


        [Set(0, Name = "SetIncidentTypeDepartmentTypes", Table = "INCIDENT_TYPE_DEPARTMENT_TYPE", Cascade = "save-update",
           Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect, Where = "DELETED_ID IS NULL")]
        [Key(1, Column = "INCIDENT_TYPE_CODE"/*, ForeignKey = "FK_INCIDENT_TYPE_DEPARTMENT_TYPE_INCIDENT_TYPE_CODE"*/)]
        [OneToMany(2, ClassType = typeof(IncidentTypeDepartmentTypeData))]
        public ISet SetIncidentTypeDepartmentTypes { get; set; }

        [Set(0, Name = "SetIncidentTypeQuestions", Table = "INCIDENT_TYPE_QUESTION", Cascade = "save-update",
          Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect, Where = "DELETED_ID IS NULL")]
        [Key(1, Column = "INCIDENT_TYPE_CODE"/*, ForeignKey = "FK_INCIDENT_TYPE_QUESTION_INCIDENT_TYPE_CODE"*/)]
        [OneToMany(2, ClassType = typeof(IncidentTypeQuestionData))]
        public ISet SetIncidentTypeQuestions { get; set; }

        [Bag(0, Name = "IncidentTypeCategories", Table = "INCIDENT_TYPE_INCIDENT_TYPE_CATEGORY", Cascade = "none",
           Lazy = CollectionLazy.True/*, Inverse = true*/, Fetch = CollectionFetchMode.Subselect)]
        [Key(1, Column = "INCIDENT_TYPE_CODE"/*, ForeignKey = "FK_INCIDENT_TYPE_CATEGORY_INCIDENT_TYPE_CODE"*/)]
        [ManyToMany(2, ClassType = typeof(IncidentTypeCategoryData), Column = "INCIDENT_TYPE_CATEGORY_CODE", ForeignKey = "FK_INCIDENT_TYPE_INCIDENT_TYPE_CATEGORY_CODE")]
        public virtual IList IncidentTypeCategories { get; set; }

        #endregion

        [InitialData(PropertyName = "Name", PropertyValue = "IncidentTypeData")]
        public static readonly UserResourceData Resource;

        public override string ToString()
        {
            string ToReturn = this.FriendlyName + " (";
            if (this.NoEmergency == true)
                ToReturn = ToReturn + "NE";
            else
                ToReturn = ToReturn + this.CustomCode;
            ToReturn = ToReturn + ")";

            if (this.Exclusive == true)
            {
                ToReturn = ToReturn + " (-) ";
            }
            return ToReturn;
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is IncidentTypeData)
            {
                result = this.Code == ((IncidentTypeData)obj).Code;
            }
            return result;
        }
    }

    public class IncidentTypeComparer : IComparer
    {
        #region IComparer Members

        public int Compare(object x, object y)
        {
            IncidentTypeData it1 = (IncidentTypeData)x;
            IncidentTypeData it2 = (IncidentTypeData)y;

            return it1.FriendlyName.CompareTo(it2.FriendlyName);
        }

        #endregion
    }
}
