﻿using NHibernate.Mapping.Attributes;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(IncidentTypeQuestionData), Table = "INCIDENT_TYPE_QUESTION", Lazy = false, Where = "DELETED_ID IS NULL")]
    public class IncidentTypeQuestionData : ObjectData
    {
        #region Fields
        private IncidentTypeData incidentType;
        private QuestionData question;
        #endregion

        #region Properties
        [ManyToOne(Column = "INCIDENT_TYPE_CODE", ClassType = typeof(IncidentTypeData), ForeignKey = "FK_INCIDENT_TYPE_QUESTION_INCIDENT_TYPE_CODE", Cascade = "none")]
        public virtual IncidentTypeData IncidentType
        {
            get
            {
                return incidentType;
            }
            set
            {
                incidentType = value;
            }
        }


        [ManyToOne(Column = "QUESTION_CODE", ClassType = typeof(QuestionData), ForeignKey = "FK_INCIDENT_TYPE_QUESTION_QUESTION_CODE", Cascade = "none")]
        public virtual QuestionData Question
        {
            get
            {
                return question;
            }
            set
            {
                question = value;
            }
        }

        /*[Property(0)]
        [Column(1, Name = "INDEX_CODE")]
        public int Index
        {
            get
            {
                return index;
            }
            set
            {
                index = value;
            }
        }*/
        #endregion

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is IncidentTypeQuestionData)
            {
                IncidentTypeQuestionData aux = (IncidentTypeQuestionData)obj;
                result = (this.Code == aux.Code) || (this.Question.Code == aux.Question.Code && this.IncidentType.Code == aux.IncidentType.Code);
            }
            return result;
        }
    }
}
