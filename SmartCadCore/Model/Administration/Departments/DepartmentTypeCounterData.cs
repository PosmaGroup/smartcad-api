﻿using System;
using System.Linq;
using System.Text;
using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(DepartmentTypeCounterData), Table = "DEPARTMENT_TYPE_COUNTER", Lazy = false, BatchSize = 1000, Where = "DELETED_ID IS NULL")]
    public class DepartmentTypeCounterData: ObjectData
    {
        private int value;
        private DateTime? restartDate;


        [Property(Column = "RESTART_DATE", NotNull = true)]
        public DateTime? RestartDate
        {
            get
            {
                return restartDate;
            }
            set
            {
                restartDate = value;
            }
        }

        [Property(Column = "VALUE", NotNull = true)]
        public int Value
        {
            get
            {
                return value;
            }
            set
            {
                this.value = value;
            }
        }
    }
}
