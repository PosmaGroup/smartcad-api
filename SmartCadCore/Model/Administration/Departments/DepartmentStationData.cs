﻿using Iesi.Collections;
using NHibernate.Mapping.Attributes;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(DepartmentStationData), Table = "DEPARTMENT_STATION", Lazy = false, BatchSize = 1000, Where = "DELETED_ID IS NULL")]
    public class DepartmentStationData : ObjectData, INamedObjectData
    {
        #region Constructors

        public DepartmentStationData()
        {
        }

        #endregion

        #region Properties

        [Property(Column = "CUSTOM_CODE")]
        public virtual string CustomCode { get; set; }

        [Property(Column = "NAME", NotNull = true)]
        public virtual string Name { get; set; }

        [ManyToOne(ClassType = typeof(DepartmentZoneData), Column = "DEPARTMENT_ZONE_CODE",
            ForeignKey = "FK_DEPARTMENT_ZONE_DEPARTMENT_STATION_CODE", NotNull = true, Cascade = "none", Fetch = FetchMode.Join)]
        public virtual DepartmentZoneData DepartmentZone { get; set; }

        [Set(0, Name = "DepartmentStationAddres", Table = "DEPARTMENT_STATION_ADDRESS", Cascade = "save-update",
         Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect, Inverse = true, Where = "DELETED_ID IS NULL")]
        [Key(1, Column = "STATION_CODE"/*, ForeignKey = "FK_DEPARTMENT_STATION_ADDRESS_CODE"*/)]
        [OneToMany(2, ClassType = typeof(DepartmentStationAddressData))]
        public ISet DepartmentStationAddres { get; set; }

        [Property(Column = "TELEPHONE")]
        public virtual string Telephone { get; set; }

        #endregion

        public override string ToString()
        {
            return this.Name;
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is DepartmentStationData)
            {
                result = this.Code == ((DepartmentStationData)obj).Code;
            }
            return result;
        }

        #region Initial data

        [InitialData(PropertyName = "Name", PropertyValue = "DepartmentStationData")]
        public static readonly UserResourceData Resource;

        #endregion
    }
}
