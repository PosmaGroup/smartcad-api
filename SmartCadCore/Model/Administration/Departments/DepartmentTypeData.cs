using System;

using NHibernate.Mapping.Attributes;
using System.Collections;

using System.Drawing;
using ColorBase = System.Drawing.Color;
using Iesi.Collections;

namespace SmartCadCore.Model
{
    #region Class DepartmentData Documentation
    /// <summary>
    /// This class represents a dispatch deparment.
    /// </summary>
    /// <className>DepartmentData</className>
    /// <author email="fsilva@smartmatic.com">Fernando Silva</author>
    /// <date>2007/01/30</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    [Serializable]
    [Class(NameType = typeof(DepartmentTypeData), Table = "DEPARTMENT_TYPE", Lazy = false, BatchSize = 1000, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_DEPARTMENT_TYPE_NAME")]
    public class DepartmentTypeData : ObjectData, INamedObjectData
    {
        public enum Frequency
        {
            None = 0,
            Daily = 1,
            Weekly = 2,
            Fortnightly = 3,
            Monthly = 4,
            Annual = 5
		}

		#region Constructors

		public DepartmentTypeData()
        {
        }

        public DepartmentTypeData(string name)
        {
            this.Name = name;
            this.Color = ColorBase.Black;
		}

		#endregion

		#region Fields

		ComponentColorData componentColor;

		#endregion

		#region Properties

		[Property(0, Unique = true)]
		[Column(1, Name = "NAME", UniqueKey = "UK_DEPARTMENT_TYPE_NAME", NotNull = true)]
		public virtual string Name { get; set; }

		[Set(0, Name = "DepartmentZones", Table = "DEPARTMENT_ZONE", Cascade = "all-delete-orphan",
            Lazy = CollectionLazy.True, Inverse = true, Fetch = CollectionFetchMode.Subselect, Where = "DELETED_ID IS NULL")]
        [Key(1, Column = "DEPARTMENT_TYPE_CODE"/*, ForeignKey = "FK_DEPARTMENT_ZONE_DEPARTMENT_TYPE_CODE"*/)]
        [OneToMany(2, ClassType = typeof(DepartmentZoneData))]
        public virtual ISet DepartmentZones { get; set; }

        [Bag(0, Name = "Alerts", Table = "DEPARTMENT_TYPE_ALERT", Cascade = "save-update",
                 Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect, Where = "DELETED_ID IS NULL")]
        [Key(1, Column = "DEPARTMENT_TYPE_CODE"/*, ForeignKey = "FK_DEPARTMENT_TYPE_ALERT_DEPARTMENT_TYPE_CODE"*/)]
        [OneToMany(2, ClassType = typeof(DepartmentTypeAlertData))]
		public IList Alerts { get; set; }

        [Property(Column = "DISPATCH", NotNull = true, TypeType = typeof(bool))]
		public bool? Dispatch { get; set; }        

        [Property(Column = "RESTART_FREQUENCY", NotNull = true)]
		public Frequency RestartFrequency { get; set; }

        [Property(Column = "PATTERN")]
		public string Pattern { get; set; }

        [ManyToOne(ForeignKey = "FK_DEPARTMENT_TYPE_DEPARTMENT_TYPE_COUNTER_CODE", ClassType = typeof(DepartmentTypeCounterData),
            Cascade = "none", Column = "DEPARTMENT_TYPE_COUNTER_CODE",
			NotNull = false, Fetch = FetchMode.Join)]
		public DepartmentTypeCounterData Counter { get; set; }
           
        public virtual Color Color
        {
            get
            {
                if (ComponentColor != null)
                    return ColorBase.FromArgb(ComponentColor.ColorA.Value, ComponentColor.ColorR.Value, ComponentColor.ColorG.Value, ComponentColor.ColorB.Value);
                else
                    return Color.White;
            }
            set
            {
                componentColor = new ComponentColorData(value);
            }
        }
       
        [ComponentProperty(ComponentType = typeof(ComponentColorData))]
        public ComponentColorData ComponentColor
        {
            get
            {
                return componentColor;
            }
            set
            {
                componentColor = value;
                Color = ColorBase.FromArgb(value.ColorA.Value, value.ColorR.Value, value.ColorG.Value, value.ColorB.Value);
            }
		}
		#endregion

		#region Overrides

		public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is DepartmentTypeData)
            {
                result = this.Code == ((DepartmentTypeData)obj).Code;
            }
            return result;
        }

        public override string ToString()
        {
            return this.Name;
		}
		#endregion

		#region Initial data

		[InitialData(PropertyName = "Name", PropertyValue = "DepartmentTypeData")]
        public static readonly UserResourceData Resource;

        #endregion
    }

    #region Comparers
    public class DepartmentTypeNameComparer : IComparer
    {
        #region IComparer Members

        public int Compare(object x, object y)
        {
            DepartmentTypeData dt1 = (DepartmentTypeData)x;
            DepartmentTypeData dt2 = (DepartmentTypeData)y;

            return dt1.Name.CompareTo(dt2.Name);
        }

        #endregion
    }
    #endregion
}
