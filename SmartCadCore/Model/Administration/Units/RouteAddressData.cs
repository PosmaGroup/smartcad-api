﻿using NHibernate.Mapping.Attributes;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(RouteAddressData), Table = "ROUTE_ADDRESS", Lazy = false, Where = "DELETED_ID IS NULL")]
    public class RouteAddressData : ObjectData
    {
        [ComponentProperty(ComponentType = typeof(MultipleAddressData))]
        public MultipleAddressData Address { get; set; }

        [Property(1, Unique = false)]
        [Column(2, Name = "NAME", NotNull = false)]
        public string Name { get; set; }

        [ManyToOne(Column = "ROUTE_CODE", ClassType = typeof(RouteData), ForeignKey = "FK_ROUTE_ADDRESS_CODE",
            Cascade = "none")]
        public RouteData Route { get; set; }

        [Property(0, Unique = false)]
        [Column(1, Name = "POINT_NUMBER", NotNull = true)]
        public int PointNumber { get; set; }

        [InitialData(PropertyName = "Name", PropertyValue = "RouteAddressData")]
        public static readonly UserResourceData Resource;

        public override bool Equals(object obj)
        {
            if (obj is RouteAddressData)
                return false;
            RouteAddressData ra = (RouteAddressData)obj;
            if ((this.Address.Lon == ra.Address.Lon) &&
                (this.Address.Lat == ra.Address.Lat))
                return true;
            return false;
        }
    }
}
