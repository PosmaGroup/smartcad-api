﻿using NHibernate.Mapping.Attributes;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(UnitAlertData), Table = "UNIT_ALERT", Lazy = false, Where = "DELETED_ID IS NULL")]
    public class UnitAlertData : ObjectData
    {
        public enum AlertPriority
        {
            Low = 0,
            Medium = 1,
            High = 2
        }

        #region Properties

        [ManyToOne(ClassType = typeof(AlertData), Column = "ALERT_CODE",
            Cascade = "none", ForeignKey = "FK_UNIT_ALERT_ALERT_CODE")]
        public virtual AlertData Alert { get; set; }

        [ManyToOne(Column = "UNIT_CODE", ClassType = typeof(UnitData),
            ForeignKey = "FK_UNIT_ALERT_UNIT_CODE", Cascade = "none")]
        public virtual UnitData Unit { get; set; }

        [Property(0, Type = "int", Unique = false)]
        [Column(1, Name = "PRIORITY", NotNull = true)]
        public AlertPriority Priority { get; set; }

        [Property(Column = "MAX_VALUE", NotNull = false)]
        public int MaxValue { get; set; }

        [Property(Column = "ACTIVE", NotNull = true, TypeType = typeof(bool))]
        public bool Active { get; set; }

        #endregion
    }
}
