﻿using NHibernate.Mapping.Attributes;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(UnitStatusHistoryData), Table = "UNIT_STATUS_HISTORY", Lazy = false, Where = "DELETED_ID IS NULL AND END_DATE IS NULL")]
    [AttributeIdentifier("UK_START", Value = "UK_UNIT_STATUS_HISTORY")]
    [AttributeIdentifier("UK_END", Value = "UK_UNIT_STATUS_HISTORY")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_UNIT_STATUS_HISTORY")]
    public class UnitStatusHistoryData : BaseSessionHistory
    {
        #region Fields

        private UnitData unit;

        private OperatorData userAccount;

        private UnitStatusData status;

        #endregion

        #region Properties

        [ManyToOne(0, ClassType = typeof(UnitData), ForeignKey = "FK_UNIT_STATUS_HISTORY_UNIT_CODE", Cascade = "none")]
        [Column(1, Name = "UNIT_CODE", NotNull = true, UniqueKey = "UK_UNIT_STATUS_HISTORY")]
        public virtual UnitData Unit
        {
            get
            {
                return unit;
            }
            set
            {
                unit = value;
            }
        }

        /// <summary>
        /// User account who is logged in.
        /// </summary>

        [ManyToOne(0, ClassType = typeof(OperatorData), Insert = true, Update = false,
            ForeignKey = "FK_UNIT_STATUS_HISTORY_USER_ACCOUNT_CODE", Cascade = "none")]
        [Column(1, Name = "USER_ACCOUNT_CODE", NotNull = true, UniqueKey = "UK_UNIT_STATUS_HISTORY")]
        public virtual OperatorData UserAccount
        {
            get
            {
                return userAccount;
            }
            set
            {
                userAccount = value;
            }
        }

        [ManyToOne(0, ClassType = typeof(UnitStatusData),
           ForeignKey = "FK_UNIT_STATUS_HISTORY_UNIT_STATUS_CODE", Cascade = "none")]
        [Column(1, Name = "UNIT_STATUS_CODE", NotNull = true, UniqueKey = "UK_UNIT_STATUS_HISTORY")]
        public virtual UnitStatusData Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }

        #endregion
    }
}
