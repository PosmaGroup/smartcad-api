using System;
using System.Text;
using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    #region Class UnitEndingReportTypeData Documentation
    /// <summary>
    /// This class represents a incident type
    /// </summary>
    /// <className>UnitEndingReportTypeData</className>
    /// <author email="jynojosa@smartmatic.com">Julio Ynojosa</author>
    /// <date>2007/04/04</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    [Serializable]
    [Class(NameType = typeof(UnitEndingReportTypeData), Table = "UNIT_ENDING_REPORT_TYPE", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_UNIT_ENDING_REPORT_TYPE_CUSTOM_CODE")]
    public class UnitEndingReportTypeData : ObjectData
    {
        #region Fields
        private string name;
        private string customCode;
        private string friendlyName; 
        #endregion

        #region Properties
        [Property(Column = "NAME", NotNull = true)]
        public virtual string Name
        {
            get { return name; }
            set { name = value; }
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "CUSTOM_CODE", UniqueKey = "UK_UNIT_ENDING_REPORT_TYPE_CUSTOM_CODE", NotNull = true)]
        public virtual string CustomCode
        {
            get { return customCode; }
            set { customCode = value; }
        }

        [Property(Column = "FRIENDLY_NAME", NotNull = true)]
        public virtual string FriendlyName
        {
            get { return friendlyName; }
            set { friendlyName = value; }
        } 
        #endregion

        public override string ToString()
        {
            return this.FriendlyName;
        }

        #region InitialData
        [InitialData(PropertyName = "CustomCode", PropertyValue = "FALSE_ALARM")]
        public static UnitEndingReportTypeData FalseAlarmUnitEndingReportType;
        #endregion
    }
}
