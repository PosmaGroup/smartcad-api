﻿using System;
using System.Linq;
using System.Text;
using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    /// <summary>
    /// This class is used to store the audio of the call
    /// </summary>
    [Serializable]
    [Class(NameType = typeof(PhoneReportAudioData), Table = "PHONE_REPORT_AUDIO", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_PHONE_REPORT_AUDIO_FILENAME")]
    public class PhoneReportAudioData : ObjectData
    {
        #region Properties
        [Property(0, Unique = true)]
        [Column(1, Name = "CUSTOM_CODE", NotNull = true)]
        public string CustomCode { get; set; }

        [Property(Column = "FILE_NAME", TypeType = typeof(string), NotNull=true,  UniqueKey = "UK_PHONE_REPORT_AUDIO_FILENAME")]
        public string FileName { get; set; }

        [Property(Column = "STORAGE_FOLDER", TypeType = typeof(string))]
        public string StorageFolder{ get; set; }
        #endregion
    }
}
