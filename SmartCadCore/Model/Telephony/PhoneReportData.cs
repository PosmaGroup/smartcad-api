﻿using Iesi.Collections;
using NHibernate.Mapping.Attributes;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    #region Class PhoneReportData Documentation
    /// <summary>
    /// This class represents the phone report.
    /// </summary>
    /// <className>PhoneReportData</className>
    /// <author email="fsilva@smartmatic.com">Fernando Silva</author>
    /// <date>2007/01/30</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    [Serializable]
    [JoinedSubclass(Table = "PHONE_REPORT", NameType = typeof(PhoneReportData), ExtendsType = typeof(ReportBaseData), Lazy = false)]
    public class PhoneReportData : ReportBaseData
    {
        #region Fields

        private int parentCode;
        private bool? incomplete;
        private PhoneReportLineData line;
        private PhoneReportCallerData caller;
        private ISet setAnswers;
        private DateTime? registeredCallTime;
        private DateTime? receivedCallTime;
        private DateTime? pickedUpCallTime;
        private DateTime? hangedUpCallTime;
        private DateTime? finishedReportTime;

        #endregion

        #region Constructors

        public PhoneReportData()
        {
        }

        public PhoneReportData(
            PhoneReportLineData line,
            PhoneReportCallerData caller,
            ISet answers)
        {
            this.line = line;
            this.caller = caller;
            this.setAnswers = answers;
        }

        #endregion

        #region Properties

        [Key(0, Column = "PARENT_CODE", ForeignKey = "FK_PHONE_REPORT_REPORT_BASE_CODE")]
        public virtual int ParentCode
        {
            get
            {
                return parentCode;
            }
            set
            {
                parentCode = value;
            }
        }

        [Property(Column = "FINISHED_REPORT_TIME", TypeType = typeof(DateTime))]
        public virtual DateTime? FinishedReportTime
        {
            get
            {
                return finishedReportTime;
            }
            set
            {
                finishedReportTime = value;
            }
        }

        [Property(Column = "REGISTERED_CALL_TIME", TypeType = typeof(DateTime))]
        public virtual DateTime? RegisteredCallTime
        {
            get
            {
                return registeredCallTime;
            }
            set
            {
                registeredCallTime = value;
            }
        }

        [Property(Column = "RECEIVED_CALL_TIME", TypeType = typeof(DateTime))]
        public virtual DateTime? ReceivedCallTime
        {
            get
            {
                return receivedCallTime;
            }
            set
            {
                receivedCallTime = value;
            }
        }

        [Property(Column = "PICKED_UP_CALL_TIME", TypeType = typeof(DateTime))]
        public virtual DateTime? PickedUpCallTime
        {
            get
            {
                return pickedUpCallTime;
            }
            set
            {
                pickedUpCallTime = value;
            }
        }

        [Property(Column = "HANGED_UP_CALL_TIME", TypeType = typeof(DateTime))]
        public virtual DateTime? HangedUpCallTime
        {
            get
            {
                return hangedUpCallTime;
            }
            set
            {
                hangedUpCallTime = value;
            }
        }

        [Property(Column = "INCOMPLETE", TypeType = typeof(bool))]
        public virtual bool? Incomplete
        {
            get
            {
                return incomplete;
            }
            set
            {
                incomplete = value;
            }
        }

        [ComponentProperty(ComponentType = typeof(PhoneReportLineData))]
        public virtual PhoneReportLineData Line
        {
            get
            {
                return line;
            }
            set
            {
                line = value;
            }
        }

        [ComponentProperty(ComponentType = typeof(PhoneReportCallerData))]
        public virtual PhoneReportCallerData Caller
        {
            get
            {
                return caller;
            }
            set
            {
                caller = value;
            }
        }

        [Set(0, Name = "SetAnswers", Table = "PHONE_REPORT_ANSWER", Cascade = "all-delete-orphan",
            Lazy = CollectionLazy.True/*, Inverse = true*/, Fetch = CollectionFetchMode.Subselect, Where = "DELETED_ID IS NULL")]
        [Key(1, Column = "PHONE_REPORT_CODE"/*, ForeignKey = "FK_PHONE_REPORT_PHONE_REPORT_ANSWER_CODE"*/)]
        [OneToMany(2, ClassType = typeof(PhoneReportAnswerData))]
        public ISet SetAnswers
        {
            get
            {
                return setAnswers;
            }
            set
            {
                setAnswers = value;
            }
        }

        #endregion


    }
}
