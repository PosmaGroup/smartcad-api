﻿using NHibernate.Mapping.Attributes;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(PublicLineData), Table = "PUBLIC_LINE", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_PUBLIC_LINE_TELEPHONE")]
    public class PublicLineData : ObjectData
    {
        #region Fields

        private string telephone;

        private string name;

        private PublicLineAddressData address;

        private bool _public;

        private string callerName;

        private PublicLineCallerAddressData callerAddress;

        private DateTime? updated = null;

        #endregion

        #region Constructors

        public PublicLineData()
        {
        }

        public PublicLineData(string name, string telephone, PublicLineAddressData address)
        {
            this.name = name;
            this.telephone = telephone;
            this.address = address;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Telephone number of the phone line calling. Provided by the Telephone Company.
        /// </summary>
        [Property(0, Unique = true)]
        [Column(1, Name = "TELEPHONE", UniqueKey = "UK_PUBLIC_LINE_TELEPHONE", NotNull = true)]
        public virtual string Telephone
        {
            get
            {
                return telephone;
            }
            set
            {
                telephone = value;
            }
        }

        /// <summary>
        /// Owner's name of the phone line calling. Provided by the Telephone Company.
        /// </summary>
        [Property(0)]
        [Column(1, Name = "NAME")]
        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        /// <summary>
        /// Address of the phone number calling. Provided by the Telephone Company.
        /// </summary>
        [ComponentProperty(ComponentType = typeof(PublicLineAddressData))]
        public virtual PublicLineAddressData Address
        {
            get
            {
                return address;
            }
            set
            {
                address = value;
            }
        }

        /// <summary>
        /// Indicate if the number is a public phone. Provided by the Telephone Company.
        /// </summary>
        [Property(0, Unique = true)]
        [Column(1, Name = "PUBLIC_PHONE")]
        public virtual bool Public
        {
            get
            {
                return _public;
            }
            set
            {
                _public = value;
            }
        }

        /// <summary>
        /// Last person who called from this number.
        /// </summary>
        [Property(0)]
        [Column(1, Name = "CALLER_NAME")]
        public virtual string CallerName
        {
            get
            {
                return callerName;
            }
            set
            {
                callerName = value;
            }
        }

        /// <summary>
        /// Address of the Last person who called from this number.
        /// </summary>
        [ComponentProperty(ComponentType = typeof(PublicLineCallerAddressData))]
        public virtual PublicLineCallerAddressData CallerAddress
        {
            get
            {
                return callerAddress;
            }
            set
            {
                callerAddress = value;
            }
        }

        /// <summary>
        /// Date from the last update made to the record.
        /// </summary>
        [Property(0)]
        [Column(1, Name = "UPDATED")]
        public virtual DateTime? Updated
        {
            get
            {
                return updated;
            }
            set
            {
                updated = value;
            }
        }
        #endregion
    }
}
