﻿using Iesi.Collections;
using NHibernate.Mapping.Attributes;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    #region Class PhoneReportAnswerData Documentation
    /// <summary>
    /// This class represents the setAnswers of a phone report
    /// </summary>
    /// <className>PhoneReportAnswerData</className>
    /// <author email="fsilva@smartmatic.com">Fernando Silva</author>
    /// <date>2007/01/30</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion

    [Serializable]
    [Class(NameType = typeof(PhoneReportAnswerData), Table = "PHONE_REPORT_ANSWER", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_PHONE_REPORT_ANSWER_QUESTION_PHONE_REPORT_CODE")]
    public class PhoneReportAnswerData : ObjectData
    {
        #region Fields

        private QuestionData question;

        private ISet setAnswers;

        private PhoneReportData phoneReport;

        #endregion



        #region Constructor

        public PhoneReportAnswerData()
        {
        }



        public PhoneReportAnswerData(QuestionData question, QuestionPossibleAnswerData answer,
            string answerText)
        {
            this.question = question;
            PossibleAnswerAnswerData paad = new PossibleAnswerAnswerData();
            paad.PhoneReportAnswer = this;
            paad.PossibleAnswer = answer;
            paad.TextAnswer = answerText;
            this.SetAnswers.Add(paad);
            //this.answer = answer;
        }

        public PhoneReportAnswerData(QuestionData question, string answer)
        {
            this.question = question;
            PossibleAnswerAnswerData paad = new PossibleAnswerAnswerData();
            //this.answer = answer;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Phone report question
        /// </summary>
        [ManyToOne(0, ClassType = typeof(QuestionData), ForeignKey = "FK_PHONE_REPORT_ANSWER_QUESTION_CODE", Unique = true, Cascade = "none")]
        [Column(1, Name = "PHONE_REPORT_ANSWER_QUESTION_CODE", UniqueKey = "UK_PHONE_REPORT_ANSWER_QUESTION_PHONE_REPORT_CODE", NotNull = true, Index = "IX_PHONE_REPORT_ANSWER_QUESTION_PHONE_REPORT")]
        public virtual QuestionData Question
        {
            get
            {
                return question;
            }
            set
            {
                question = value;
            }
        }

        [Set(0, Name = "SetAnswers", Table = "POSSIBLE_ANSWER_ANSWER_DATA", Cascade = "save-update",
            Lazy = CollectionLazy.True/*, Inverse = true*/, Fetch = CollectionFetchMode.Subselect)]
        [Key(1, Column = "PHONE_REPORT_ANSWER_CODE"/*, ForeignKey = "FK_POSSIBLE_ANSWER_ANSWER_PHONE_REPORT_ANSWER"*/)]
        [OneToMany(2, ClassType = typeof(PossibleAnswerAnswerData))]
        public ISet SetAnswers
        {
            get
            {
                return setAnswers;
            }
            set
            {
                setAnswers = value;
            }
        }

        /// <summary>
        /// Phone report parent of this answer
        /// </summary>
        [ManyToOne(0, ClassType = typeof(PhoneReportData), ForeignKey = "FK_PHONE_REPORT_PHONE_REPORT_ANSWER_CODE", Unique = true, Cascade = "none")]
        [Column(1, Name = "PHONE_REPORT_CODE", UniqueKey = "UK_PHONE_REPORT_ANSWER_QUESTION_PHONE_REPORT_CODE", NotNull = true, Index = "IX_PHONE_REPORT_ANSWER_QUESTION_PHONE_REPORT")]
        public virtual PhoneReportData PhoneReport
        {
            get
            {
                return phoneReport;
            }
            set
            {
                phoneReport = value;
            }
        }
        #endregion

        public override bool Equals(object obj)
        {

            if ((question.Code != (obj as PhoneReportAnswerData).Question.Code) &&
                (question.Text != (obj as PhoneReportAnswerData).Question.Text) &&
                (question.ShortText != (obj as PhoneReportAnswerData).Question.ShortText))
                return false;
            else
                return true;
        }
    }
}
