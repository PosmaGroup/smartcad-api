﻿using NHibernate.Mapping.Attributes;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{/// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class GeoPointAddressData
    {
        #region Fields

        private double lon;
        private double lat;
        private bool isSynchronized;

        #endregion

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        public GeoPointAddressData()
        {
        }



        #endregion

        #region Properties


        /// <summary>
        /// 
        /// </summary>
        [Property(0, TypeType = typeof(double))]
        [Column(1, Name = "{{LON}}", Index = "{{IX_LON}}")]
        [AttributeIdentifier("LON", Value = "LON")]
        [AttributeIdentifier("IX_LON", Value = "IX_LON")]
        public virtual double Lon
        {
            get
            {
                return lon;
            }
            set
            {
                lon = value;
            }
        }


        [Property(0, TypeType = typeof(double))]
        [Column(1, Name = "{{LAT}}", NotNull = false, Index = "{{IX_LAT}}")]
        [AttributeIdentifier("LAT", Value = "LAT")]
        [AttributeIdentifier("IX_LAT", Value = "IX_LAT")]
        public virtual double Lat
        {
            get
            {
                return lat;
            }
            set
            {
                lat = value;
            }
        }

        [Property(Column = "{{ADD_SYNCHRONIZED}}")]
        [AttributeIdentifier("ADD_SYNCHRONIZED", Value = "ADD_SYNCHRONIZED")]
        public virtual bool IsSynchronized
        {
            get
            {
                return isSynchronized;
            }

            set
            {
                isSynchronized = value;
            }
        }

        #endregion

    }

    [Serializable]
    public class AddressData : GeoPointAddressData
    {
        #region Fields

        private string state;
        private string town;
        private string zone;
        private string street;
        private string reference;
        private string more;
        private bool isSynchronized;

        #endregion

        #region Constructors

        public AddressData()
        {
        }

        public AddressData(double lon, double lat)
        {
            this.Lat = lat;
            this.Lon = lon;
        }

        public AddressData(string zone, string street, string reference, string more)
        {
            this.zone = zone;
            this.street = street;
            this.reference = reference;
            this.more = more;
        }

        #endregion

        #region Properties

        [Property(Column = "{{STATE}}")]
        [AttributeIdentifier("STATE", Value = "STATE")]
        public virtual string State
        {
            get
            {
                return state;
            }
            set
            {
                state = value;
            }
        }

        [Property(Column = "{{TOWN}}")]
        [AttributeIdentifier("TOWN", Value = "TOWN")]
        public virtual string Town
        {
            get
            {
                return town;
            }
            set
            {
                town = value;
            }
        }

        [Property(Column = "{{ZONE}}")]
        [AttributeIdentifier("ZONE", Value = "ZONE")]
        public virtual string Zone
        {
            get
            {
                return zone;
            }
            set
            {
                zone = value;
            }
        }

        [Property(Column = "{{STREET}}")]
        [AttributeIdentifier("STREET", Value = "STREET")]
        public virtual string Street
        {
            get
            {
                return street;
            }
            set
            {
                street = value;
            }
        }

        [Property(Column = "{{REFERENCE}}")]
        [AttributeIdentifier("REFERENCE", Value = "REFERENCE")]
        public virtual string Reference
        {
            get
            {
                return reference;
            }
            set
            {
                reference = value;
            }
        }

        [Property(Column = "{{MORE}}")]
        [AttributeIdentifier("MORE", Value = "MORE")]
        public virtual string More
        {
            get
            {
                return more;
            }
            set
            {
                more = value;
            }
        }

        #endregion

        #region Overrides

        public override string ToString()
        {
            return "[" + this.zone + "][" + this.street + "][" + this.reference + "][" + this.more + "]";
        }

        #endregion
    }

    [Serializable]
    [Component()]
    [AttributeIdentifier("LON", Value = "LON")]
    [AttributeIdentifier("IX_LON", Value = "IX_LON")]
    [AttributeIdentifier("IX_LAT", Value = "IX_LAT")]
    public class IncidentAddressData : AddressData
    {
        #region Constructors

        public IncidentAddressData()
        {
        }

        public IncidentAddressData(AddressData address)
        {
            if (address != null)
            {
                this.Lon = address.Lon;
                this.Lat = address.Lat;
                this.More = address.More;
                this.Reference = address.Reference;
                this.Street = address.Street;
                this.Zone = address.Zone;
                this.State = address.State;
                this.Town = address.Town;
                this.IsSynchronized = address.IsSynchronized;
            }
            else
                throw new ArgumentNullException("address");
        }

        #endregion
    }

    [Serializable]
    [Component()]
    [AttributeIdentifier("ZONE", Value = "LINE_ZONE")]
    [AttributeIdentifier("STREET", Value = "LINE_STREET")]
    [AttributeIdentifier("REFERENCE", Value = "LINE_REFERENCE")]
    [AttributeIdentifier("MORE", Value = "LINE_MORE")]
    [AttributeIdentifier("LON", Value = "LINE_LON")]
    [AttributeIdentifier("LAT", Value = "LINE_LAT")]
    [AttributeIdentifier("ADD_SYNCHRONIZED", Value = "LINE_ADD_SYNCHRONIZED")]
    [AttributeIdentifier("TOWN", Value = "LINE_TOWN")]
    [AttributeIdentifier("STATE", Value = "LINE_STATE")]
    public sealed class PhoneReportLineAddressData : AddressData
    {
        #region Constructors

        public PhoneReportLineAddressData(AddressData address)
        {
            this.Lon = address.Lon;
            this.Lat = address.Lat;
            this.More = address.More;
            this.Reference = address.Reference;
            this.Street = address.Street;
            this.Zone = address.Zone;
        }

        public PhoneReportLineAddressData()
        {
        }

        #endregion
    }

    [Serializable]
    [Component(0)]
    [AttributeIdentifier("ZONE", Value = "CALLER_ZONE")]
    [AttributeIdentifier("STREET", Value = "CALLER_STREET")]
    [AttributeIdentifier("REFERENCE", Value = "CALLER_REFERENCE")]
    [AttributeIdentifier("MORE", Value = "CALLER_MORE")]
    [AttributeIdentifier("LON", Value = "CALLER_LON")]
    [AttributeIdentifier("LAT", Value = "CALLER_LAT")]
    [AttributeIdentifier("ADD_SYNCHRONIZED", Value = "CALLER_ADD_SYNCHRONIZED")]
    [AttributeIdentifier("TOWN", Value = "CALLER_TOWN")]
    [AttributeIdentifier("STATE", Value = "CALLER_STATE")]
    public sealed class PhoneReportCallerAddressData : AddressData
    {
        #region Constructors
        public PhoneReportCallerAddressData(AddressData address)
        {
            this.Lon = address.Lon;
            this.Lat = address.Lat;
            this.More = address.More;
            this.Reference = address.Reference;
            this.Street = address.Street;
            this.Zone = address.Zone;
            this.Zone = address.Zone;
        }

        public PhoneReportCallerAddressData() { }

        #endregion
    }

    [Serializable]
    [Component(0)]
    [AttributeIdentifier("ZONE", Value = "LINE_ZONE")]
    [AttributeIdentifier("STREET", Value = "LINE_STREET")]
    [AttributeIdentifier("REFERENCE", Value = "LINE_REFERENCE")]
    [AttributeIdentifier("MORE", Value = "LINE_MORE")]
    [AttributeIdentifier("LON", Value = "LINE_LON")]
    [AttributeIdentifier("LAT", Value = "LINE_LAT")]
    [AttributeIdentifier("ADD_SYNCHRONIZED", Value = "LINE_ADD_SYNCHRONIZED")]
    [AttributeIdentifier("TOWN", Value = "LINE_TOWN")]
    [AttributeIdentifier("STATE", Value = "LINE_STATE")]
    public sealed class PublicLineAddressData : AddressData
    {
        #region Constructors

        public PublicLineAddressData(AddressData address)
        {
            this.Lon = address.Lon;
            this.Lat = address.Lat;
            this.More = address.More;
            this.Reference = address.Reference;
            this.Street = address.Street;
            this.Zone = address.Zone;
            this.State = address.State;
            this.Town = address.Town;
        }

        public PublicLineAddressData() { }

        #endregion
    }

    [Serializable]
    [Component(0)]
    [AttributeIdentifier("ZONE", Value = "CALLER_ZONE")]
    [AttributeIdentifier("STREET", Value = "CALLER_STREET")]
    [AttributeIdentifier("REFERENCE", Value = "CALLER_REFERENCE")]
    [AttributeIdentifier("MORE", Value = "CALLER_MORE")]
    [AttributeIdentifier("LON", Value = "CALLER_LON")]
    [AttributeIdentifier("LAT", Value = "CALLER_LAT")]
    [AttributeIdentifier("ADD_SYNCHRONIZED", Value = "CALLER_ADD_SYNCHRONIZED")]
    [AttributeIdentifier("TOWN", Value = "CALLER_TOWN")]
    [AttributeIdentifier("STATE", Value = "CALLER_STATE")]
    public sealed class PublicLineCallerAddressData : AddressData
    {
        #region Constructors

        public PublicLineCallerAddressData(AddressData address)
        {
            this.Lon = address.Lon;
            this.Lat = address.Lat;
            this.More = address.More;
            this.Reference = address.Reference;
            this.Street = address.Street;
            this.Zone = address.Zone;
            this.State = address.State;
            this.Town = address.Town;
        }

        public PublicLineCallerAddressData() { }

        #endregion
    }


    [Serializable]
    [Component()]
    [AttributeIdentifier("STREET", Value = "CCTV_STREET")]
    [AttributeIdentifier("LON", Value = "CCTV_LON")]
    [AttributeIdentifier("LAT", Value = "CCTV_LAT")]
    [AttributeIdentifier("ADD_SYNCHRONIZED", Value = "CCTV_ADD_SYNCHRONIZED")]
    public sealed class StructAddressData : AddressData
    {
        #region Constructors
        public StructAddressData(AddressData address)
        {
            this.Lon = address.Lon;
            this.Lat = address.Lat;
            this.More = address.More;
            this.Reference = address.Reference;
            this.Street = address.Street;
            this.Zone = address.Zone;
            this.State = address.State;
            this.Town = address.Town;
        }

        public StructAddressData() { }

        #endregion
    }

    [Serializable]
    [Component()]
    [AttributeIdentifier("LON", Value = "MULTIPLE_LON")]
    [AttributeIdentifier("LAT", Value = "MULTIPLE_LAT")]
    [AttributeIdentifier("ADD_SYNCHRONIZED", Value = "MULTIPLE_ADD_SYNCHRONIZED")]
    public sealed class MultipleAddressData : GeoPointAddressData
    {
        #region Constructors
        public MultipleAddressData(AddressData address)
        {
            this.Lon = address.Lon;
            this.Lat = address.Lat;
            //this.IsSynchronized = address.IsSynchronized;
        }

        public MultipleAddressData() { }

        #endregion
    }

    [Serializable]
    [Component()]
    [AttributeIdentifier("LON", Value = "LON")]
    [AttributeIdentifier("LAT", Value = "LAT")]
    [AttributeIdentifier("ADD_SYNCHRONIZED", Value = "ADD_SYNCHRONIZED")]
    public sealed class ObjectPositionAddressData : GeoPointAddressData
    {

    }
}
