using System;
using System.Text;

namespace SmartCadCore.Model
{
    public class PhoneReportQuestionTypeData : ObjectData
    {
        private PhoneReportQuestionTypeControlType controlType;
        private string text;

        public PhoneReportQuestionTypeData()
        {}

        public PhoneReportQuestionTypeData(PhoneReportQuestionTypeControlType controlType, string text)
        {
            this.controlType = controlType;
            this.text = text;
        }
        
        public PhoneReportQuestionTypeControlType ControlType
        {
            get
            {
                return controlType;
            }
            set
            {
                controlType = value;
            }
        }

        public string Text
        {
            get
            {
                return text;
            }
            set
            {
                text = value;
            }
        }
    }

    public enum PhoneReportQuestionTypeControlType
    {
        TextBox = 0,
        RadioButton = 1,
        CheckBox = 2
    }
}
