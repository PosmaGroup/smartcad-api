﻿using System;
using System.Linq;
using System.Text;
using NHibernate.Mapping.Attributes;


namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(TelephonyConfigurationData), Table = "TELEPHONY_CONFIGURATION", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_TELEPHONY_CONFIGURATION_EXTENSION")]
    [AttributeIdentifier("DELETED_ID_2", Value = "UK_TELEPHONY_CONFIGURATION_COMPUTER")]
    public class TelephonyConfigurationData : ObjectData
    {
        #region Properties
        [Property(0, Unique = true)]
        [Column(1, Name = "EXTENSION", UniqueKey = "UK_TELEPHONY_CONFIGURATION_EXTENSION", NotNull = true)]
        public int Extension { get; set; }
        [Property(0)]
        [Column(1, Name = "PASSWORD", NotNull = true)]
        public string Password { get; set; }
        [Property(0)]
        [Column(1, Name = "COMPUTER", NotNull = true, UniqueKey = "UK_TELEPHONY_CONFIGURATION_COMPUTER")]
        public string Computer { get; set; }
        [Property(0)]
        [Column(1, Name = "VIRTUAL", NotNull = true)]
        public bool Virtual { get; set; }
        #endregion

        [InitialData(PropertyName = "Name", PropertyValue = "TelephonyConfigurationData")]
        public static readonly UserResourceData Resource;

        #region Overrides

        public override string ToString()
        {
            return this.Extension.ToString();
        }

        #endregion
    }
}
