using System;
using System.Text;
using NHibernate.Mapping.Attributes;
using Iesi.Collections;
using System.Collections;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(SensorTelemetryData), Table = "SENSOR_TELEMETRY", Lazy = false, BatchSize = 1000, Where = "DELETED_ID IS NULL")]
    public class SensorTelemetryData : ObjectData, INamedObjectData
    {
        private string name;
        private string variable;
        private DataloggerData datalogger;
        private ISet thresholds;

        [ManyToOne(ClassType = typeof(DataloggerData), Column = "DATALOGGER_CODE",
          ForeignKey = "FK_DATALOGGER_SENSOR_TELEMETRY_CODE", Cascade = "none")]
        public DataloggerData Datalogger
        {
            get
            {
                return datalogger;
            }
            set
            {
                datalogger = value;
            }
        }

        [Property(Column = "VARIABLE", NotNull = false)]
        public string Variable
        {
            get
            {
                return variable;
            }
            set
            {
                if (value != "")
                    variable = value;
            }
        }

        [Property(Column = "NAME", NotNull = true)]
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                if (value != "")
                    name = value;
            }
        }

        [Property(Column = "VALUE")]
        public double? Value
        {
            get;
            set;
        }

        [Property(Column = "VALUE_DATE")]
        public DateTime? ValueDate
        {
            get;
            set;
        }
        
        [Set(0, Name = "Thresholds", Table = "SENSOR_TELEMETRY_THRESHOLD", Cascade = "save-update",
            Lazy = CollectionLazy.False, Fetch = CollectionFetchMode.Subselect)]
        [Key(1, Column = "SENSOR_TELEMETRY_CODE")]
        [OneToMany(2, ClassType = typeof(SensorTelemetryThresholdData))]
        public ISet Thresholds
        {
            get
            {
                return thresholds;
            }
            set
            {
                thresholds = value;
            }
        }

        [InitialData(PropertyName = "Name", PropertyValue = "SensorTelemetryData")]
        public static readonly UserResourceData Resource;


        public override string ToString()
        {
            return Name;
        }

        public override bool Equals(object obj)
        {
            try
            {
                if (obj.GetType() == typeof(SensorTelemetryData))
                {
                    SensorTelemetryData cam = (SensorTelemetryData)obj;
                    if (this.Name == cam.Name)
                    {
                        return true;
                    }
                }
            }
            catch { }
            return false;
        }
    }

}
