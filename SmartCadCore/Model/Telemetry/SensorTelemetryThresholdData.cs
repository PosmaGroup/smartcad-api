﻿using System;
using System.Linq;
using System.Text;
using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(SensorTelemetryThresholdData), Table = "SENSOR_TELEMETRY_THRESHOLD", Lazy = false, BatchSize = 1000, Where = "DELETED_ID IS NULL")]
    public class SensorTelemetryThresholdData : ObjectData
	{

        private SensorTelemetryData sensor;
        private double? high;
        private double? low;

        [ManyToOne(ClassType = typeof(SensorTelemetryData), Column = "SENSOR_TELEMETRY_CODE",
          ForeignKey = "FK_SENSOR_TELEMETRY_SENSOR_TELEMETRY_THRESHOLD_CODE", Cascade = "none")]
        [Column(1, Name = "SENSOR_TELEMETRY_CODE", NotNull = true)]
        public SensorTelemetryData Sensor
        {
            get
            {
                return sensor;
            }
            set
            {
                sensor = value;
            }
        }

        [Property(Column = "HIGH", NotNull = false)]
        public double? High
        {
            get
            {
                return high;
            }
            set 
            {
                high = value;
            }
        }

        [Property(Column = "LOW", NotNull = false)]
        public double? Low
        {
            get
            {
                return low;
            }
            set
            {
                low = value;
            }
        }

        public override bool Equals(object obj)
        {
            bool result =false;
            if (obj is SensorTelemetryThresholdData)
            {
                SensorTelemetryThresholdData threshold = (SensorTelemetryThresholdData)obj;
                if ((this.Code > 0 && this.Code == threshold.Code) ||
                    (this.High == threshold.High && this.Low == threshold.Low && 
                    this.Sensor.Name == threshold.Sensor.Name && this.Sensor.Variable == threshold.Sensor.Variable))
                    result = true;
            }
            return result;
        }
	}
}
