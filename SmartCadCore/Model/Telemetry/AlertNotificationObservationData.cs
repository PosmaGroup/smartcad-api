﻿using System;
using System.Linq;
using System.Text;
using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
	[Serializable]
    [Class(NameType = typeof(AlertNotificationObservationData), Table = "ALERT_NOTIFICATION_OBSERVATION", Lazy = false, Where = "DELETED_ID IS NULL")]
	public class AlertNotificationObservationData : ObjectData
	{
		#region Constructor
		public AlertNotificationObservationData()
		{

		}
		#endregion

		#region Properties

		[Property(Column = "TEXT")]
		public string Text { get; set; }

		[Property(0, Type = "DateTime")]
		[Column(1, Name = "DATE", NotNull = true)]
		public DateTime Date { get; set; }

		[ManyToOne(Column = "OPERATOR_CODE", ClassType = typeof(OperatorData),
			ForeignKey = "FK_ALERT_NOTIFICATION_OBSERVATION_OPERATOR_CODE", Cascade = "none")]
		public OperatorData Operator { get; set; }

		[ManyToOne(Column = "ALERT_NOTIFICATION_CODE", ClassType = typeof(AlertNotificationData), ForeignKey = "FK_ALERT_NOTIFICATION_OBSERVATION_CODE",
			Cascade = "none")]
		public AlertNotificationData AlertNotification { get; set; }

		#endregion

		#region Overrides

		public override bool Equals(object obj)
		{
			if (obj is AlertNotificationObservationData == false)
			{
				return false;
			}
			AlertNotificationObservationData obs = obj as AlertNotificationObservationData;
			if (obs.Code == Code)
			{
				return true;
			}
			return false;
		}

		#endregion
	}
}
