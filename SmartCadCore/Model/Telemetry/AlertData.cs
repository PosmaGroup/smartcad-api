﻿using System;
using System.Linq;
using System.Text;
using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(AlertData), Table = "ALERT", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_ALERT_CUSTOM_CODE")]
    [AttributeIdentifier("DELETED_ID_2", Value = "UK_ALERT_NAME")]
    public class AlertData : ObjectData, INamedObjectData
    {
        public AlertData()
        {
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "CUSTOM_CODE", UniqueKey = "UK_ALERT_CUSTOM_CODE", NotNull = true)]
        public string CustomCode
        {
            get;
            set;
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "NAME", UniqueKey = "UK_ALERT_NAME", NotNull = true)]
        public string Name
        {
            get;
            set;
        }

        [Property(Column = "MEASURE_UNIT")]
        public string Measure
        {
            get;
            set;
        }

        [InitialData(PropertyName = "Name", PropertyValue = "AlertData")]
        public static readonly UserResourceData Resource;

        #region Overrides

        public override string ToString()
        {
            return this.Name;
        }

        #endregion

    }
}
