using System;
using System.Text;
using NHibernate.Mapping.Attributes;
using Iesi.Collections;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(LprWayData), Table = "LPR_WAY", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_LPR_WAY")]
    public class LprWayData : ObjectData
    {
        private string name = string.Empty;

        [Property(0, Unique = true)]
        [Column(1, Name = "NAME", UniqueKey = "UK_LPR_WAY", NotNull = true)]
        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public override string ToString()
        {
            return this.Name;
        }

        [InitialData(PropertyName = "Name", PropertyValue = "LprWayData")]
        public static readonly UserResourceData Resource;
    }
}
