using System;
using System.Text;
using NHibernate.Mapping.Attributes;
using Iesi.Collections;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(LprTypeData), Table = "LPR_TYPE", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_LPR_TYPE")]   
    public class LprTypeData : ObjectData
    {
        private string name = string.Empty;

        [Property(0, Unique = true)]
        [Column(1, Name = "NAME", UniqueKey = "UK_LPR_TYPE", NotNull = true)]
        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public override string ToString()
        {
            return this.Name;
        }

        [InitialData(PropertyName = "Name", PropertyValue = "LprTypeData")]
        public static readonly UserResourceData Resource;
    }
}
