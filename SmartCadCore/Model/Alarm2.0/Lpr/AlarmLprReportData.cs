using System;
using System.Collections;
using System.Text;

using NHibernate.Mapping.Attributes;
using Iesi.Collections;

namespace SmartCadCore.Model
{
    #region Class AlarmLprReportData Documentation
    /// <summary>
    /// This class represents the cctv report.
    /// </summary>
    /// <className>AlarmLprReportData</className>
    /// <author email="fsilva@smartmatic.com">Fernando Silva</author>
    /// <date>2007/01/30</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    [Serializable]
    [JoinedSubclass(Table = "ALARM_LPR_REPORT", NameType = typeof(AlarmLprReportData), ExtendsType = typeof(ReportBaseData), Lazy = false)]
    public class AlarmLprReportData : ReportBaseData
    {
        #region Fields

        private int parentCode;       
        private AlarmLprReportLprData lpr;
        private ISet setAnswers;       
        private DateTime? finishedIncidentLprTime;
        private AlarmLprData alarmLpr;

        #endregion

        #region Constructors
        
        public AlarmLprReportData()
        {
        }
        public AlarmLprReportData(
            AlarmLprReportLprData lpr,           
            ISet answers)
        {
            this.lpr = lpr;            
            this.setAnswers = answers;
        }

        #endregion

        #region Properties

        [Key(0, Column = "PARENT_CODE", ForeignKey = "FK_ALARM_LPR_REPORT_REPORT_BASE_CODE")]
        public virtual int ParentCode
        {
            get
            {
                return parentCode;
            }
            set
            {
                parentCode = value;
            }
        }

        [Property(Column = "FINISHED_INCIDENT_CAMERA_TIME", TypeType = typeof(DateTime))]
        public virtual DateTime? FinishedIncidentLprTime
        {
            get
            {
                return finishedIncidentLprTime;
            }
            set
            {
                finishedIncidentLprTime = value;
            }
        }              
       
        [Set(0, Name = "SetAnswers", Table = "ALARM_LPR_REPORT_ANSWER", Cascade = "all-delete-orphan",
            Lazy = CollectionLazy.True/*, Inverse = true*/, Fetch = CollectionFetchMode.Subselect)]
        [Key(1, Column = "ALARM_LPR_REPORT_CODE"/*, ForeignKey = "FK_ALARM_LPR_REPORT_ALARM_LPR_REPORT_ANSWER_CODE"*/)]
        [OneToMany(2, ClassType = typeof(AlarmLprReportAnswerData))]
        public ISet SetAnswers
        {
            get
            {
                return setAnswers;
            }
            set
            {
                setAnswers = value;
            }
        }

        [ManyToOne(ClassType = typeof(AlarmLprData), Column = "ALARM_CODE",
           ForeignKey = "FK_LPR_REPORT_ALARM_CODE", NotNull = true, Cascade = "none")]
        public AlarmLprData AlarmLpr
        {
            get
            {
                return alarmLpr;
            }
            set
            {
                alarmLpr = value;
            }
        }

        #endregion

        #region doc
        //private bool? incomplete;
        //private PhoneReportCallerData caller;
        //private DateTime? registeredCallTime;
        //private DateTime? receivedCallTime;
        //private DateTime? pickedUpCallTime;
        //[Property(Column = "REGISTERED_CALL_TIME", TypeType = typeof(DateTime))]
        //public virtual DateTime? RegisteredCallTime
        //{
        //    get
        //    {
        //        return registeredCallTime;
        //    }
        //    set
        //    {
        //        registeredCallTime = value;
        //    }
        //}

        //[Property(Column = "RECEIVED_CALL_TIME", TypeType = typeof(DateTime))]
        //public virtual DateTime? ReceivedCallTime
        //{
        //    get
        //    {
        //        return receivedCallTime;
        //    }
        //    set
        //    {
        //        receivedCallTime = value;
        //    }
        //}

        //[Property(Column = "HANGED_UP_CALL_TIME", TypeType = typeof(DateTime))]
        //public virtual DateTime? HangedUpCallTime
        //{
        //    get
        //    {
        //        return hangedUpCallTime;
        //    }
        //    set
        //    {
        //        hangedUpCallTime = value;
        //    }
        //}

        //[Property(Column = "INCOMPLETE", TypeType = typeof(bool))]
        //public virtual bool? Incomplete
        //{
        //    get
        //    {
        //        return incomplete;
        //    }
        //    set
        //    {
        //        incomplete = value;
        //    }
        //}
        //[ComponentProperty(ComponentType = typeof(PhoneReportCallerData))]
        //public virtual PhoneReportCallerData Caller
        //{
        //    get
        //    {
        //        return caller;
        //    }
        //    set
        //    {
        //        caller = value;
        //    }
        //}
        #endregion doc
      
    }

   
}
