using System;
using System.Text;
using NHibernate.Mapping.Attributes;
using Iesi.Collections;

using System.Collections;

namespace SmartCadCore.Model
{
    [Serializable]    
    [JoinedSubclass(Table = "DATA_LOGGER", NameType = typeof(DataloggerData), ExtendsType = typeof(VideoDeviceData), Lazy = false)]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_DATALOGGER_CUSTOM_CODE")]
    public class DataloggerData : VideoDeviceData, INamedObjectData
    {
        private int parentCode;
        private ISet sensorTelemetrys;

        [Set(0, Name = "SensorTelemetrys", Table = "SENSOR_TELEMETRY", Cascade = "save-update",
            Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect)]
        [Key(1, Column = "DATALOGGER_CODE"/*, ForeignKey = "FK_STRUCT_VIDEO_DEVICE_CODE"*/)]
        [OneToMany(2, ClassType = typeof(SensorTelemetryData))]
        public ISet SensorTelemetrys
        {
            get
            {
                return sensorTelemetrys;
            }
            set
            {
                sensorTelemetrys = value;
            }
        }


        [Key(0, Column = "PARENT_CODE", ForeignKey = "FK_SENSOR_TELEMETRY_DEVICE_CODE")]
        public virtual int ParentCode
        {
            get
            {
                return parentCode;
            }
            set
            {
                parentCode = value;
            }
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "CUSTOM_CODE", UniqueKey = "UK_DATALOGGER_CUSTOM_CODE", NotNull = true)]
        public int CustomCode { get; set; }

        
       
       

        [InitialData(PropertyName = "Name", PropertyValue = "DataloggerData")]
        public static readonly UserResourceData Resource;


        public override string ToString()
        {
            return Name;
        }

        public override bool Equals(object obj)
        {
            try
            {
                if (obj.GetType() == typeof(DataloggerData))
                {
                    DataloggerData cam = (DataloggerData)obj;
                    if (this.Name == cam.Name)
                    {
                        return true;
                    }
                }
            }
            catch { }
            return false;
        }
    }

}
