using System;

using System.Text;
using NHibernate.Mapping.Attributes;
using Iesi.Collections;
using System.Collections;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(AlarmLprData), Table = "ALARM_LPR", Lazy = false, BatchSize = 1000, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_ALARM_LPR")]
    public class AlarmLprData : ObjectData
    {
        public enum AlarmStatus
        {
            NotTaken = 0,
            InProcess,
            Ended
        }

        [ManyToOne(0, ClassType = typeof(VehicleRequestData), ForeignKey = "FK_REQUEST_ALARM_CODE", Cascade = "none")]
        [Column(1, Name = "REQUEST_CODE", UniqueKey = "UK_ALARM_LPR", NotNull = true)]
        public VehicleRequestData VehicleRequest
        {
            get;
            set;
        }


        [ManyToOne(0, ClassType = typeof(LprData), ForeignKey = "FK_LPR_ALARM_LPR_CODE", Cascade = "none")]
        [Column(1, Name = "LPR_CODE", UniqueKey = "UK_ALARM_LPR", NotNull = true)]
        public LprData Lpr
        {
            get;
            set;
        }

        [Property(0, Unique = true, TypeType = typeof(DateTime))]
        [Column(1, Name = "START_DATE", UniqueKey = "UK_ALARM_LPR", NotNull = true)]
        public virtual DateTime StartDate
        {
            get;
            set;
        }

        [Property(0, TypeType = typeof(DateTime))]
        [Column(1, Name = "END_DATE")]
        public virtual DateTime? EndDate
        {
            get;
            set;
        }

        [Property(0, TypeType = typeof(AlarmStatus))]
        [Column(1, Name = "ALARM_STATUS", NotNull = true)]
        public virtual AlarmStatus Status
        {
            get;
            set;
        }

        [Property(Column = "COLOR_IMAGE", NotNull = false, Length = 131072)]
        public byte[] ColorImage
        {
            get;
            set;
        }
        [Property(Column = "BW_IMAGE", NotNull = false, Length = 131072)]
        public byte[] BWImage
        {
            get;
            set;
        }
        [Property(Column = "PLATE", NotNull = true)]
        public string Plate
        {
            get;
            set;
        }

       
        [InitialData(PropertyName = "Name", PropertyValue = "AlarmLprData")]
        public static readonly UserResourceData Resource;


        public override string ToString()
        {
            return Lpr.Name;
        }

        public override bool Equals(object obj)
        {
            try
            {
                if (obj.GetType() == typeof(AlarmLprData))
                {
                    AlarmLprData cam = (AlarmLprData)obj;
                    if (this.VehicleRequest.Code == cam.VehicleRequest.Code)
                    {
                        return true;
                    }
                }
            }
            catch { }
            return false;
        }
    }

}
