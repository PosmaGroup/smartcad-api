using System;
using System.Text;
using NHibernate.Mapping.Attributes;
using Iesi.Collections;
using System.Collections;

namespace SmartCadCore.Model
{    
    [Serializable]
    [Class(NameType = typeof(AlarmLprRecordData), Table = "ALARM_LPR_RECORD", Lazy = false, Where = "DELETED_ID IS NULL")]    
    public class AlarmLprRecordData :ObjectData
    {      
      
        private int parentCode;       
        

        private string requestFor;
        private DepartmentTypeData requestByDepartment;
        private LprData myLpr;
        private string requestByOfficcer;
        private string plate;
        private string brand;
        private string model;
        private string color;

        private string details;
        private DateTime dateAndTime;

        [Key(0, Column = "PARENT_CODE", ForeignKey = "FK_ALARM_LPR_RECORD_CODE")]
        public virtual int ParentCode
        {
            get
            {
                return parentCode;
            }
            set
            {
                parentCode = value;
            }
        }
        [ManyToOne(ClassType = typeof(LprData), Column = "LPR_CODE",
           ForeignKey = "FK_LPR_ALARM_LPR_RECORDE_CODE", Cascade = "none")]
        public LprData Lpr
        {
            get
            {
                return myLpr;
            }
            set
            {
                myLpr = value;
            }
        }

        [Property(Column = "REQUEST_FOR", NotNull =true)]
        public string RequestFor
        {
            get
            {
                return requestFor;
            }
            set
            {
                requestFor = value;
            }
        }

        [ManyToOne(Column = "REQUEST_BY_DEPARTMENT", ClassType = typeof(DepartmentTypeData), 
            ForeignKey = "FK_DEPARTMENT_ALARM_LPR_RECORD", NotNull = true, Cascade = "none")]
        public DepartmentTypeData RequestByDepartment
        {
            get
            {
                return requestByDepartment;
            }
            set
            {
                requestByDepartment = value;
            }
        }


        [Property(Column = "REQUEST_BY_OFFICCER", NotNull = true)]
        public string RequestByOfficcer
        {
            get
            {
                return requestByOfficcer;
            }
            set
            {
                requestByOfficcer = value;
            }
        }
        [Property(Column = "PLATE", NotNull = true)]
        public string Plate
        {
            get
            {
                return plate;
            }
            set
            {
                plate = value;
            }
        }
        [Property(Column = "BRAND", NotNull = false)]
        public string Brand
        {
            get
            {
                return brand;
            }
            set
            {
                brand = value;
            }
        }
        [Property(Column = "MODEL", NotNull = false)]
        public string Model
        {
            get
            {
                return model;
            }
            set
            {
                model = value;
            }
        }
        [Property(Column = "COLOR", NotNull = false)]
        public string Color
        {
            get
            {
                return color;
            }
            set
            {
                color = value;
            }
        }
        [Property(Column = "DETAILS", NotNull = false)]
        public string Details
        {
            get
            {
                return details;
            }
            set
            {
                details = value;
            }
        }
        [Property(Column = "DATE_AND_TIME", NotNull = true)]
        public DateTime DateAndTime
        {
            get
            {
                return dateAndTime;
            }
            set
            {
                dateAndTime = value;
            }
        }


        [InitialData(PropertyName = "Name", PropertyValue = "AlarmLprRecordData")] 
        public static readonly UserResourceData Resource;


       
        

        public override bool Equals(object obj)
        {
            try
            {
                if (obj.GetType() == typeof(AlarmLprRecordData))
                {
                    AlarmLprRecordData cam = (AlarmLprRecordData)obj;
                   if ((this.plate == cam.Plate) && (this.dateAndTime == cam.DateAndTime))
                    {
                        return true;
                    }
                }
            }
            catch { }
            return false;
        }
    }

}
