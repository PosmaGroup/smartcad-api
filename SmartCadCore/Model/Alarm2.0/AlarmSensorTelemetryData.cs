using System;
using System.Text;
using NHibernate.Mapping.Attributes;
using Iesi.Collections;
using System.Collections;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(AlarmSensorTelemetryData), Table = "ALARM_SENSOR_TELEMETRY", Lazy = false, BatchSize = 1000, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_ALARM_SENSOR_TELEMETRY")]
    public class AlarmSensorTelemetryData : ObjectData
    {
        [ManyToOne(0, ClassType = typeof(SensorTelemetryData), ForeignKey = "FK_SENSOR_TELEMETRY_ALARM_CODE", Cascade = "none")]
        [Column(1, Name = "SENSOR_TELEMETRY_CODE", UniqueKey = "UK_ALARM_SENSOR_TELEMETRY", NotNull = true)]
        public SensorTelemetryData SensorTelemetry
        {
            get;
            set;
        }

        [Property(0, Unique = true, TypeType = typeof(DateTime))]
        [Column(1, Name = "START_DATE", NotNull = true)]
        public virtual DateTime? StartAlarm
        {
            get;
            set;
        }

        [Property(0, Unique = true, TypeType = typeof(DateTime))]
        [Column(1, Name = "END_DATE", UniqueKey = "UK_ALARM_SENSOR_TELEMETRY")]
        public virtual DateTime? EndAlarm
        {
            get;
            set;
        }

        [Property(0, Unique = true, TypeType = typeof(string))]
        [Column(1, Name = "OBSERVATION", Length = 1000)]
        public virtual string Observation
        {
            get;
            set;
        }

        [Property(Column = "CURRENT_VALUE", NotNull = true)]
        public double Value
        {
            get;
            set;
        }

        [ManyToOne(0, ClassType = typeof(OperatorData),
            ForeignKey = "FK_OPERATOR_OPERATOR_ALARM_SENSOR_TELEMETRY_CODE", Cascade = "none")]
        [Column(1, Name = "OPERATOR_CODE")]
        public OperatorData Operator
        {
            get;
            set;
        }


        [InitialData(PropertyName = "Name", PropertyValue = "AlarmSensorTelemetryData")]
        public static readonly UserResourceData Resource;


        public override string ToString()
        {
            return SensorTelemetry.Code + "-" + SensorTelemetry.Name;
        }

        public override bool Equals(object obj)
        {
            try
            {
                if (obj.GetType() == typeof(AlarmSensorTelemetryData))
                {
                    AlarmSensorTelemetryData cam = (AlarmSensorTelemetryData)obj;
                    if (this.SensorTelemetry.Code == cam.SensorTelemetry.Code)
                    {
                        return true;
                    }
                }
            }
            catch { }
            return false;
        }
    }

}
