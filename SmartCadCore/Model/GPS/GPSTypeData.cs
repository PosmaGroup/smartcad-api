﻿using NHibernate.Mapping.Attributes;
using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(GPSTypeData), Table = "GPS_TYPE", Lazy = false, Where = "DELETED_ID IS NULL")]
    public class GPSTypeData : ObjectData
    {
        private string brand = string.Empty;
        private string model = string.Empty;
        private IList pines;

        [Property(0, Column = "BRAND", NotNull = true)]
        public string Brand
        {
            get
            {
                return brand;
            }
            set
            {
                brand = value;
            }
        }

        [Property(0, Column = "MODEL", NotNull = true)]
        public string Model
        {
            get
            {
                return model;
            }
            set
            {
                model = value;
            }
        }

        [Bag(0, Name = "Pines", Table = "GPS_PIN", Cascade = "save-update",
                Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect, Where = "DELETED_ID IS NULL")]
        [Key(1, Column = "GPS_PIN_CODE", ForeignKey = "FK_GPS_TYPE_GPS_PIN_CODE")]
        [OneToMany(2, ClassType = typeof(GPSPinData))]
        public IList Pines
        {
            get
            {
                return pines;
            }
            set
            {
                pines = value;
            }

        }

        public override string ToString()
        {
            return this.Brand + " - " + this.Model;
        }

    }
}
