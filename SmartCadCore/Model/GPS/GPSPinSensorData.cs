﻿using NHibernate.Mapping.Attributes;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(GPSPinSensorData), Table = "GPS_PIN_SENSOR", Lazy = false, Where = "DELETED_ID IS NULL")]
    public class GPSPinSensorData : ObjectData
    {

        #region Properties

        [ManyToOne(ClassType = typeof(SensorData), Column = "SENSOR_CODE",
            Cascade = "none", ForeignKey = "FK_SENSOR_GPS_PIN_SENSOR_CODE")]
        public SensorData Sensor
        {
            get;
            set;
        }

        [ManyToOne(Column = "GPS_PIN_CODE", ClassType = typeof(GPSPinData),
            ForeignKey = "FK_GPS_PIN_SENSOR_GPS_PIN_CODE", Cascade = "none")]
        public GPSPinData Pin
        {
            get;
            set;
        }

        [ManyToOne(Column = "GPS_CODE", ClassType = typeof(GPSData),
            ForeignKey = "FK_GPS_PIN_SENSOR_GPS_PARENT_CODE", Cascade = "none")]
        public GPSData GPS
        {
            get;
            set;
        }


        #endregion


        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GPSPinSensorData)
            {
                result = this.Code == ((GPSPinSensorData)obj).Code;
            }
            return result;
        }
    }
}
