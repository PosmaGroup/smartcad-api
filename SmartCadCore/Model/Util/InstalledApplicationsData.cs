﻿using NHibernate.Mapping.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model.Util
{
    [Serializable]
    [Class(NameType = typeof(InstalledApplicationData), Table = "INSTALLEDAPPLICATION", Lazy = false, BatchSize = 1000)]
    public class InstalledApplicationData : ObjectData, INamedObjectData
    {
        public InstalledApplicationData() 
        {
        }

        #region Properties
        [Property(Column = "NAME")]
        public  string Name { get; set; }

        [Property(Column = "STATUS")]
        public virtual bool Status { get; set; }
        #endregion

        [InitialData(PropertyName = "Name", PropertyValue = "InstalledApplicationData")]
        public static readonly UserResourceData Resource;

        public override string ToString()
        {
            string text = "";

            if (this.Name != null)
                text += this.Name;

            return text;
        }

    }
}
