using System;
using System.Collections;
using System.Text;
using System.Runtime.Serialization;
using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    #region Class UserResourceData Documentation
    /// <summary>
    /// This class represents all the resources or components that are 
    /// part of the application.
    /// </summary>
    /// <className>UserResourceData</className>
    /// <author email="jynojosa@smartmatic.com">Julio Ynojosa</author>
    /// <date>2006/12/01</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    [Serializable]
    [Class(NameType = typeof(UserResourceData), Table = "USER_RESOURCE", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_USER_RESOURCE_NAME")]
    [AttributeIdentifier("DELETED_ID_2", Value = "UK_USER_RESOURCE_FRIENDLY_NAME")]
    public class UserResourceData : ObjectData, INamedObjectData
    {
        #region Enums
        
        public enum Properties
        {
            Name,
            FriendlyName,
            Actions
        }

        #endregion

        #region Fields
        
        private string name;

        private string friendlyName;

        private IList actions;

        #endregion

        #region Constructors

        public UserResourceData()
        {
        }

        public UserResourceData(string name, string friendlyName, IList actions)
        {
            this.Name = name;
            this.FriendlyName = friendlyName;
            this.Actions = actions;
        }

        #endregion

        #region Properties
        
        [Property(0, Unique = true)]
        [Column(1, Name = "NAME", UniqueKey = "UK_USER_RESOURCE_NAME", NotNull = true)]
        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "FRIENDLY_NAME", UniqueKey = "UK_USER_RESOURCE_FRIENDLY_NAME", NotNull = true)]
        public virtual string FriendlyName
        {
            get
            {
                return friendlyName;
            }
            set
            {
                friendlyName = value;
            }
        }

        [Bag(0, Name = "Actions", Table = "USER_PERMISSION", Cascade = "none", Inverse = true, Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect)]
        [Key(1, Column = "USER_RESOURCE_CODE"/*, ForeignKey = "FK_USER_RESOURCE_CODE"*/)]
        [ManyToMany(2, ClassType = typeof(UserActionData), Column = "USER_ACTION_CODE", ForeignKey = "FK_USER_ACTION_CODE")]
        public virtual IList Actions
        {
            get
            {
                return actions;
            }
            set
            {
                actions = value;
            }
        }
        #endregion

        #region Overrides

        public override string ToString()
        {
            return this.FriendlyName;
        }
        
        #endregion

        #region InitialData
		[InitialData(PropertyName = "Name", PropertyValue = "UserApplicationData")]
		public static UserResourceData Application;
		[InitialData(PropertyName = "Name", PropertyValue = "FirstLevelSupervisorName")]
		public static UserResourceData FirstLevelSupervisor;
		[InitialData(PropertyName = "Name", PropertyValue = "DispatchSupervisorName")]
		public static UserResourceData DispatchSupervisor;
		[InitialData(PropertyName = "Name", PropertyValue = "GeneralSupervisorName")]
		public static UserResourceData GeneralSupervisor;
		#endregion
    }
}
