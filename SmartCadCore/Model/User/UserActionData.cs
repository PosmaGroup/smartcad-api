using System;
using System.Text;
using System.Runtime.Serialization;

using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    #region Class UserActionData Documentation
    /// <summary>
    /// This class represents all the information an user account must have to be 
    /// included in the system.
    /// </summary>
    /// <className>UserActionData</className>
    /// <author email="jynojosa@smartmatic.com">Julio Ynojosa</author>
    /// <date>2006/12/01</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    [Serializable]
    [Class(NameType = typeof(UserActionData), Table = "USER_ACTION", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_USER_ACTION_NAME")]
    [AttributeIdentifier("DELETED_ID_2", Value = "UK_USER_ACTION_FRIENDLY_NAME")]
    public class UserActionData : ObjectData, INamedObjectData
    {
        #region Constructors

        public UserActionData()
        {
        }

        public UserActionData(string name, string friendlyName)
        {
            this.Name = name;
            this.FriendlyName = friendlyName;
        }

        #endregion

        #region Enums
        
        public enum Properties
        {
            Name,
            FriendlyName
        }

        #endregion

        #region Fields
        
        private string name;

        private string friendlyName;

        #endregion

        #region Properties

        [Property(0, Unique = true)]
        [Column(1, Name = "NAME", UniqueKey = "UK_USER_ACTION_NAME", NotNull = true)]
        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "FRIENDLY_NAME", UniqueKey = "UK_USER_ACTION_FRIENDLY_NAME", NotNull = true)]
        public virtual string FriendlyName
        {
            get
            {
                return friendlyName;
            }
            set
            {
                friendlyName = value;
            }
        }

        #endregion

        #region Overriden methods

        public override string ToString()
        {
            return this.FriendlyName;
        } 

        #endregion

        #region Initial Data
        [InitialData(PropertyName = "Name", PropertyValue = "Basic")]
        public static readonly UserActionData Basic;
        [InitialData(PropertyName = "Name", PropertyValue = "Search")]
        public static readonly UserActionData Search;
        [InitialData(PropertyName = "Name", PropertyValue = "Insert")]
        public static readonly UserActionData Insert;
        [InitialData(PropertyName = "Name", PropertyValue = "Delete")]
        public static readonly UserActionData Delete;
        [InitialData(PropertyName = "Name", PropertyValue = "Update")]
        public static readonly UserActionData Update;
        [InitialData(PropertyName = "Name", PropertyValue = "Duplicate")]
        public static readonly UserActionData Duplicate;

        #endregion
    }
}
