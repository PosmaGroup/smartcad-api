﻿using NHibernate.Mapping.Attributes;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    #region Class UserPermissionData Documentation
    /// <summary>
    /// This class represents the actions that really applies to the resources
    /// in the application.
    /// </summary>
    /// <className>UserPermissionData</className>
    /// <author email="jynojosa@smartmatic.com">Julio Ynojosa</author>
    /// <date>2006/12/01</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    [Serializable]
    [Class(NameType = typeof(UserPermissionData), Table = "USER_PERMISSION", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_USER_PERMISSION_NAME")]
    public class UserPermissionData : ObjectData, INamedObjectData
    {
        #region Enums

        public enum Properties
        {
            Name,
            UserResource,
            UserAction
        }

        #endregion

        #region Fields

        private string name;


        private UserResourceData userResource;


        private UserActionData userAction;

        #endregion

        #region Constructors

        public UserPermissionData()
        {
        }

        public UserPermissionData(string name,
            UserResourceData userResource, UserActionData userAction)
        {
            this.Name = name;
            this.UserResource = userResource;
            this.UserAction = userAction;
        }

        #endregion

        #region Properties

        [Property(0, Unique = true)]
        [Column(1, Name = "NAME", UniqueKey = "UK_USER_PERMISSION_NAME", NotNull = true)]
        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        [ManyToOne(ClassType = typeof(UserResourceData), Column = "USER_RESOURCE_CODE",
            ForeignKey = "FK_USER_RESOURCE_CODE", Cascade = "none")]
        public virtual UserResourceData UserResource
        {
            get
            {
                return userResource;
            }
            set
            {
                userResource = value;
            }
        }

        [ManyToOne(ClassType = typeof(UserActionData), Column = "USER_ACTION_CODE",
            ForeignKey = "FK_USER_ACTION_CODE", Cascade = "none")]
        public virtual UserActionData UserAction
        {
            get
            {
                return userAction;
            }
            set
            {
                userAction = value;
            }
        }

        #endregion
    }
}
