using System;
using System.Collections;
using System.Text;
using System.Runtime.Serialization;

using NHibernate.Mapping.Attributes;
using Iesi.Collections;

namespace SmartCadCore.Model
{
    #region Class UserApplicationData Documentation
    /// <summary>
    /// This class represents data of applications which are integrating the entire
    /// SmartCad system.
    /// </summary>
    /// <className>UserApplicationData</className>
    /// <author email="jynojosa@smartmatic.com">Julio Ynojosa</author>
    /// <date>2006/12/01</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    [Serializable]
    [Class(NameType = typeof(UserApplicationData), Table = "USER_APPLICATION", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_USER_APPLICATION_NAME")]
    [AttributeIdentifier("DELETED_ID_2", Value = "UK_USER_APPLICATION_FRIENDLY_NAME")]
    public class UserApplicationData : ObjectData, INamedObjectData
    {
        public UserApplicationData()
        {
        }

        public UserApplicationData(string name, string friendlyName)
        {
            this.Name = name;
            this.FriendlyName = friendlyName;
        }

        #region Enums

        public enum Properties
        {
            Name,
            FriendlyName
        }

        #endregion

        #region Properties
        
        private string name;

        [Property(0, Unique = true)]
        [Column(1, Name = "NAME", UniqueKey = "UK_USER_APPLICATION_NAME", NotNull = true)]
        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        private string friendlyName;

        [Property(0, Unique = true)]
        [Column(1, Name = "FRIENDLY_NAME", UniqueKey = "UK_USER_APPLICATION_FRIENDLY_NAME", NotNull = true)]
        public virtual string FriendlyName
        {
            get
            {
                return friendlyName;
            }
            set
            {
                friendlyName = value;
            }
        }

        private IList accesses;

        [Bag(0, Name = "Accesses", Table = "USER_ACCESS", Cascade = "none", Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect)]
        [Key(1, Column = "USER_APPLICATION_CODE"/*, ForeignKey = "FK_USER_ACCESS_APPLICATION_CODE"*/)]
        [OneToMany(2, ClassType = typeof(UserAccessData))]
        public virtual IList Accesses
        {
            get
            {
                return accesses;
            }
            set
            {
                accesses = value;
            }
        }

        private IList evaluations;

        [Bag(0, Name = "Evaluations", Table = "EVALUATION_USER_APPLICATION", Cascade = "save-update",
          Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect, Where = "DELETED_ID IS NULL")]
        [Key(1, Column = "USER_APPLICATION_CODE"/*, ForeignKey = "FK_EVALUATION_USER_APPLICATION_USER_APPLICATION_CODE"*/)]
        [OneToMany(2, ClassType = typeof(EvaluationUserApplicationData))]
        public IList Evaluations
        {
            get
            {
                return evaluations;
            }
            set
            {
                evaluations = value;
            }
        }

        #endregion

        public override string ToString()
        {
            return this.FriendlyName;
        }

        [InitialData(PropertyName = "Name", PropertyValue = "FirstLevel")]
        public static UserApplicationData FirstLevel;

        [InitialData(PropertyName = "Name", PropertyValue = "Dispatch")]
        public static UserApplicationData Dispatch;

        [InitialData(PropertyName = "Name", PropertyValue = "Map")]
        public static UserApplicationData Map;

        [InitialData(PropertyName = "Name", PropertyValue = "Administration")]
        public static UserApplicationData Administration;

        [InitialData(PropertyName = "Name", PropertyValue = "Report")]
        public static UserApplicationData Report;

        [InitialData(PropertyName = "Name", PropertyValue = "Server")]
        public static UserApplicationData Server;

        [InitialData(PropertyName = "Name", PropertyValue = "Supervision")]
        public static UserApplicationData Supervision;

        [InitialData(PropertyName = "Name", PropertyValue = "Alarm")]
        public static UserApplicationData Alarm;

        [InitialData(PropertyName = "Name", PropertyValue = "Cctv")]
        public static UserApplicationData Cctv;

        [InitialData(PropertyName = "Name", PropertyValue = "Lpr")]
        public static UserApplicationData Lpr;

        [InitialData(PropertyName = "Name", PropertyValue = "SocialNetworks")]
        public static UserApplicationData SocialNetworks;

        public static UserApplicationData GetUserApplicationByName(string name)
        {
            if (FirstLevel.Name == name)
                return FirstLevel;
            else if (Dispatch.Name == name)
                return Dispatch;
            else if (Map.Name == name)
                return Map;
            else if (Administration.Name == name)
                return Administration;
            else if (Report.Name == name)
                return Report;
            else if (Server.Name == name)
                return Server;
            else if (Supervision.Name == name)
                return Supervision;
            else if (Cctv.Name == name)
                return Cctv;
            else if (Alarm.Name == name)
                return Alarm;
            //else if (Lpr.Name == name)
            //    return Lpr;
            //else if (SocialNetworks.Name == name)
            //    return SocialNetworks;
            else
                return null;
        }

        public static int GetUserApplicationPortByName(string name)
        {
            if (FirstLevel.Name == name)
                return 2020;
            else if (Dispatch.Name == name)
                return 2021;
            else if (Map.Name == name)
                return 2022;
            else if (Administration.Name == name)
                return 2023;
            else if (Report.Name == name)
                return 2024;
            else if (Supervision.Name == name)
                return 2025;
            else if (Cctv.Name == name)
                return 2026;
            else if (Alarm.Name == name)
                return 2027;
            //else if (Lpr.Name == name)
            //    return 2028;
            //else if (SocialNetworks.Name == name)
            //    return 2029;
            else
                return 0;
        }

        [InitialData(PropertyName = "Name", PropertyValue = "UserApplicationData")]
        public static readonly UserResourceData Resource;

        public override bool Equals(object obj)
        {
            UserApplicationData app = (obj as UserApplicationData);
            if (app.name == this.name)
                return true;
            else
                return false;
        }
    }
}
