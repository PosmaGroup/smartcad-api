﻿using NHibernate.Mapping.Attributes;
using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    #region Class UserRoleData Documentation
    /// <summary>
    /// This class represents the roles an user can play in the application.
    /// </summary>
    /// <className>UserRoleData</className>
    /// <author email="jynojosa@smartmatic.com">Julio Ynojosa</author>
    /// <date>2006/12/01</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    [Serializable]
    [Class(NameType = typeof(UserRoleData), Table = "USER_ROLE", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_USER_ROLE_NAME")]
    [AttributeIdentifier("DELETED_ID_2", Value = "UK_USER_ROLE_FRIENDLY_NAME")]
    public class UserRoleData : ObjectData, INamedObjectData, IImmutableObjectData
    {
        #region Contructors

        public UserRoleData()
        {
        }

        public UserRoleData(string name, string friendlyName, string description, IList profiles)
        {
            this.Name = name;
            this.FriendlyName = friendlyName;
            this.Description = description;
            this.Profiles = profiles;
        }

        #endregion

        #region Enums

        public enum Properties
        {
            Name,
            FriendlyName,
            Description,
            Profiles
        }

        #endregion

        #region Fields

        private string name;
        private string friendlyName;
        private string description;
        private bool? immutable;

        private IList profiles;

        #endregion

        #region Properties

        [Property(0, Unique = true)]
        [Column(1, Name = "NAME", UniqueKey = "UK_USER_ROLE_NAME", NotNull = true)]
        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "FRIENDLY_NAME", UniqueKey = "UK_USER_ROLE_FRIENDLY_NAME", NotNull = true)]
        public virtual string FriendlyName
        {
            get
            {
                return friendlyName;
            }
            set
            {
                friendlyName = value;
            }
        }

        [Property(Column = "DESCRIPTION")]
        public virtual string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }


        [Property(Column = "IMMUTABLE", NotNull = true, TypeType = typeof(bool))]
        public bool? Immutable
        {
            get
            {
                return immutable;
            }
            set
            {
                immutable = value;
            }
        }

        [Bag(0, Name = "Profiles", Table = "USER_ROLE_PROFILE", Cascade = "none", Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect)]
        [Key(1, Column = "USER_ROLE_CODE", ForeignKey = "FK_USER_ROLE_CODE")]
        [ManyToMany(2, ClassType = typeof(UserProfileData), Column = "USER_PROFILE_CODE", ForeignKey = "FK_USER_ROLE_USER_PROFILE_CODE")]
        public virtual IList Profiles
        {
            get
            {
                return profiles;
            }
            set
            {
                profiles = value;
            }
        }


        #endregion

        #region Overrides

        public override string ToString()
        {
            return this.FriendlyName;
        }

        #endregion

        #region Initial Data

        [InitialData(PropertyName = "Name", PropertyValue = "UserRoleData")]
        public static readonly UserResourceData Resource;

        [InitialData(PropertyName = "Name", PropertyValue = "Developer")]
        public static readonly UserRoleData Developer;

        #endregion
    }
}
