using System;
using System.Collections;
using System.Text;
using System.Runtime.Serialization;

using NHibernate.Mapping.Attributes;

using Iesi.Collections;

namespace SmartCadCore.Model
{
    #region Class UserResourceData Documentation
    /// <summary>
    /// </summary>
    /// <className>UserResourceData</className>
    /// <author email="atorres@smartmatic.com">Alden Torres</author>
    /// <date>2007/02/15</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    [Serializable]
    [Class(NameType = typeof(UserProfileData), Table = "USER_PROFILE", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_USER_PROFILE_NAME")]
    [AttributeIdentifier("DELETED_ID_2", Value = "UK_USER_PROFILE_FRIENDLY_NAME")]
    public class UserProfileData : ObjectData, INamedObjectData, IImmutableObjectData
    {
        public UserProfileData()
        {
        }

        public UserProfileData(string name, string friendlyName, string description)
        {
            this.Name = name;
            this.FriendlyName = friendlyName;
            this.Description = description;
                
        }

        #region Enums

        public enum Properties
        {
            Name,
            FriendlyName,
            Description,
            Accesses
        }

        #endregion

        #region Properties
        
        private string name;
        
        [Property(0, Unique = true)]
        [Column(1, Name = "NAME", UniqueKey = "UK_USER_PROFILE_NAME", NotNull = true)]
        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }
        
        private string friendlyName;

        [Property(0, Unique = true)]
        [Column(1, Name = "FRIENDLY_NAME", UniqueKey = "UK_USER_PROFILE_FRIENDLY_NAME", NotNull = true)]
        public virtual string FriendlyName
        {
            get
            {
                return friendlyName;
            }
            set
            {
                friendlyName = value;
            }
        }
        
        private string description;

        [Property(Column = "DESCRIPTION")]
        public virtual string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }

        private bool? immutable;

        [Property(Column = "IMMUTABLE", NotNull = true, TypeType = typeof(bool))]
        public bool? Immutable
        {
            get
            {
                return immutable;
            }
            set
            {
                immutable = value;
            }
        }

        private ISet accesses;

        [Set(0, Name = "Accesses", Table = "USER_PROFILE_ACCESS", Cascade = "none", Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect)]
        [Key(1, Column = "USER_PROFILE_CODE", ForeignKey = "FK_USER_PROFILE_CODE")]
        [ManyToMany(2, ClassType = typeof(UserAccessData), Column = "USER_ACCESS_CODE", ForeignKey = "FK_USER_PROFILE_USER_ACCESS_CODE")]
        public virtual ISet Accesses
        {
            get
            {
                return accesses;
            }
            set
            {
                accesses = value;
            }
        }

        #endregion

        [InitialData(PropertyName = "Name", PropertyValue = "UserProfileData")]
        public static readonly UserResourceData Resource;

        public override string ToString()
        {
            return this.FriendlyName;
        }
    }
}
