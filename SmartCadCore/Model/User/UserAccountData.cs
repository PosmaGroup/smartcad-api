﻿using Iesi.Collections;
using NHibernate.Mapping.Attributes;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    #region Class UserAccountData Documentation
    /// <summary>
    /// This class represents all the information an user account must have to be 
    /// included in the system.
    /// </summary>
    /// <className>UserAccountData</className>
    /// <author email="jynojosa@smartmatic.com">Julio Ynojosa</author>
    /// <date>2006/12/01</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    [Serializable]
    //[Class(Table = "USER_ACCOUNT", Lazy = false)]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_USER_ACCOUNT_LOGIN")]
    [AttributeIdentifier("DELETED_ID_2", Value = "UK_USER_ACCOUNT_PERSON_ID")]
    // [AttributeIdentifier("DELETED_ID_2", Value = "UK_USER_PERSON_ID")]
    public abstract class UserAccountData : ObjectData
    {
        #region Enums

        /// <summary>
        /// Enumerates the properties that this class has.
        /// </summary>
        public enum Properties
        {
            Login,
            Password,
            FirstName,
            LastName,
            PersonId,
            Email,
            Address,
            Role,
            UserAccountApplicationList,
            Picture
        }

        #endregion

        #region Constants

        /// <summary>
        /// It offers the minimum length a passwword should be.
        /// </summary>
        public const int MinimunPasswordLength = 6;

        #endregion

        #region Fields

        private string login;
        private string password;
        private string firstName;
        private string lastName;
        private string personId;
        private string email;
        private string address;
        private string telephone;
        private UserRoleData role;
        private ISet sessions;
        private bool? windows;
        private byte[] picture;

        #endregion

        #region Properties
        /// <summary>
        /// User account login
        /// </summary>
        [Property(0, Unique = true)]
        [Column(1, Name = "LOGIN", UniqueKey = "UK_USER_ACCOUNT_LOGIN", NotNull = true)]
        public virtual string Login
        {
            get
            {
                return login;
            }
            set
            {
                login = value;
            }
        }

        [Property(Column = "PASSWORD")]
        public virtual string Password
        {
            get
            {
                return password;
            }
            set
            {
                password = value;
            }
        }

        [Property(Column = "FIRSTNAME")]
        public virtual string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                firstName = value;
            }
        }

        [Property(Column = "LASTNAME")]
        public virtual string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                lastName = value;
            }
        }

        [ManyToOne(ClassType = typeof(UserRoleData), Column = "USER_ROLE_CODE",
            ForeignKey = "FK_USER_ACCOUNT_ROLE_CODE", Cascade = "none")]
        public virtual UserRoleData Role
        {
            get
            {
                return role;
            }
            set
            {
                role = value;
            }
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "PERSON_ID", UniqueKey = "UK_USER_ACCOUNT_PERSON_ID", NotNull = true)]
        public virtual string PersonId
        {
            get
            {
                return personId;
            }

            set
            {
                personId = value;
            }
        }

        [Property(Column = "EMAIL")]
        public virtual string Email
        {
            get
            {
                return email;
            }
            set
            {
                email = value;
            }
        }

        [Property(Column = "ADDRESS")]
        public virtual string Address
        {
            get
            {
                return address;
            }
            set
            {
                address = value;
            }
        }

        [Property(Column = "TELEPHONE")]
        public virtual string Telephone
        {
            get
            {
                return telephone;
            }
            set
            {
                telephone = value;
            }
        }

        [Property(Column = "PICTURE", TypeType = typeof(byte[]), Length = 8001)]
        public byte[] Picture
        {
            get
            {
                return picture;
            }
            set
            {
                picture = value;
            }
        }

        /// <summary>
        /// Manages the list of all logins made by the user.
        /// </summary>
        [Set(0, Name = "Sessions", Table = "SESSION_HISTORY", Cascade = "all-delete-orphan",
         Lazy = CollectionLazy.True, Inverse = true, Fetch = CollectionFetchMode.Subselect)]
        [Key(1, Column = "USER_ACCOUNT_CODE"/*, ForeignKey = "FK_SESSION_HISTORY_USER_ACCOUNT_CODE"*/)]
        [OneToMany(2, ClassType = typeof(SessionHistoryData))]
        public virtual ISet Sessions
        {
            get
            {
                return sessions;
            }
            set
            {
                sessions = value;
            }
        }

        [Property(Column = "WINDOWS", TypeType = typeof(bool))]
        public bool? Windows
        {
            get
            {
                return windows;
            }
            set
            {
                windows = value;
            }
        }

        #endregion

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is OperatorData)
            {
                result = this.Login == ((OperatorData)obj).Login;
            }
            return result;
        }
    }
}
