using System;
using System.Text;
using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{

    [Serializable]
    [Class(NameType = typeof(IndicatorResultValuesData), Table = "INDICATOR_RESULT_VALUES", Lazy = false, Where = "DELETED_ID IS NULL")]
    public class IndicatorResultValuesData : ObjectData
    {
        private IndicatorResultData result;
        private int threshold;
        private int trend;
        private string _value;
        private string description;
        
        private IndicatorClassData indicatorClass;
        private int customCode;
        private string information;

        [ManyToOne(0, ClassType = typeof(IndicatorResultData), ForeignKey = "FK_INDICATOR_RESULT_VALUES_INDICATOR_RESULT_CODE", Cascade = "none")]
        [Column(1, Name = "INDICATOR_RESULT_CODE", NotNull = true, Index = "IX_INDICATOR_RESULT_CODE")]
        public IndicatorResultData Result
        {
            get { return result; }
            set { result = value; }
        }

        [ManyToOne(0, ClassType = typeof(IndicatorClassData), ForeignKey = "FK_INDICATOR_RESULT_INDICATOR_CLASS_CODE", Cascade = "none")]
        [Column(1, Name = "INDICATOR_CLASS_CODE", NotNull = true, Index = "IX_INDICATOR_CLASS_CODE")]
        public IndicatorClassData IndicatorClass
        {
            get { return indicatorClass; }
            set { indicatorClass = value; }
        }

        [Property(0)]
        [Column(1, Name = "CUSTOM_CODE", NotNull = true)]
        public int CustomCode
        {
            get { return customCode; }
            set { customCode = value; }
        }

        public string Information
        {
            get { return information; }
            set { information = value; }
        }

        [Property(0)]
        [Column(1, Name = "THRESHOLD")]
        public int Threshold
        {
            get { return threshold; }
            set { threshold = value; }
        }

        [Property(0)]
        [Column(1, Name = "TREND")]
        public int Trend
        {
            get { return trend; }
            set { trend = value; }
        }

        [Property(0)]
        [Column(1, Name = "VALUE", NotNull = true, Length = 16384)]
        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }

        [Property(0)]
        [Column(1, Name = "DESCRIPTION", Length = 16384)]
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is IndicatorResultValuesData)
            { 
                IndicatorResultValuesData compare = (IndicatorResultValuesData)obj;
                result = this.Result == compare.Result &&
                         this.Value == compare.Value; 
            }
            return result;
        }
    }
}
