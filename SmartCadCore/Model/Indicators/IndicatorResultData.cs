using System;
using System.Text;
using NHibernate.Mapping.Attributes;
using System.Collections;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(IndicatorResultData), Table = "INDICATOR_RESULT", Lazy = false, Where = "DELETED_ID IS NULL")]
    public class IndicatorResultData : ObjectData
    {
        private IndicatorData indicator;
        private DateTime time;
        private IList result;

        [ManyToOne(0, Unique = true, ClassType = typeof(IndicatorData), ForeignKey = "FK_INDICATOR_RESULT_INDICATOR_CODE", Cascade = "none")]
        [Column(1, Name = "INDICATOR_CODE", NotNull = true, Index = "IX_INDICATOR_CODE")]
        public IndicatorData Indicator
        { 
            get { return indicator;}
            set { indicator = value;}
        }

        [Property(0, Unique = true, TypeType = typeof(DateTime))]
        [Column(1, Name = "TIME", NotNull = true)]
        public DateTime Time
        {
            get { return time; }
            set { time = value; }
        }

        [Bag(0, Table = "INDICATOR_RESULT_VALUES", Cascade = "save-update",
            Lazy = CollectionLazy.True, Inverse = true, Fetch = CollectionFetchMode.Subselect)]
        [Key(1, Column = "INDICATOR_RESULT_CODE"/*, ForeignKey = "FK_INDICATOR_RESULT_VALUES_INDICATOR_RESULT_CODE"*/)]
        [OneToMany(2, ClassType = typeof(IndicatorResultValuesData))]
        public IList Result
        {
            get { return result; }
            set { result = value; }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is IndicatorResultData)
            {
                IndicatorResultData compare = (IndicatorResultData)obj;
                result = this.Indicator == compare.Indicator &&
                         this.Time == compare.Time;
            }
            return result;
        }
    }
}
