﻿using NHibernate.Mapping.Attributes;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(IndicatorTypeData), Table = "INDICATOR_TYPE", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_INDICATOR_TYPE_NAME")]
    [AttributeIdentifier("DELETED_ID_2", Value = "UK_INDICATOR_TYPE_FRIENDLY_NAME")]
    public class IndicatorTypeData : ObjectData, INamedObjectData
    {
        private string name;
        private string friendlyName;
        private string description;
        private ApplicationType? applicationType;

        [Property(0, Unique = true)]
        [Column(1, Name = "NAME", NotNull = true, UniqueKey = "UK_INDICATOR_TYPE_NAME")]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "FRIENDLY_NAME", UniqueKey = "UK_INDICATOR_TYPE_FRIENDLY_NAME", NotNull = true)]
        public virtual string FriendlyName
        {
            get
            {
                return friendlyName;
            }
            set
            {
                friendlyName = value;
            }
        }

        [Property(0, Column = "DESCRIPTION")]
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        [Property(Column = "APPLICATION_TYPE", TypeType = typeof(ApplicationType))]
        public ApplicationType? ApplicationType
        {
            get
            {
                return applicationType;
            }
            set
            {
                applicationType = value;
            }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is IndicatorTypeData)
            {
                result = this.Name == ((IndicatorTypeData)obj).Name;
            }
            return result;
        }

        [InitialData(PropertyName = "Name", PropertyValue = "CALLS")]
        public static IndicatorTypeData Calls;

        [InitialData(PropertyName = "Name", PropertyValue = "FIRSTLEVEL")]
        public static IndicatorTypeData FirstLevel;

        [InitialData(PropertyName = "Name", PropertyValue = "DISPATCH")]
        public static IndicatorTypeData Dispatch;
    }

    public enum ApplicationType
    {
        FIRST_LEVEL,
        DISPATCH
    }
}
