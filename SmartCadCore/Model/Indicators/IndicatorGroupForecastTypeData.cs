﻿using System;
using System.Text;
using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{

    [Serializable]
    [Class(NameType = typeof(IndicatorGroupForecastTypeData), Table = "INDICATOR_GROUP_FORECAST_TYPE", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_INDICATOR_GROUP_FORECAST_TYPE")]
    public class IndicatorGroupForecastTypeData : ObjectData
    {

        private string name;
        private string friendlyName;

        [Property(0, Unique = true)]
        [Column(1, Name = "NAME", UniqueKey = "UK_INDICATOR_GROUP_FORECAST_TYPE", NotNull = true)]
        public string Name 
        { 
            get 
            {
                return name;
            } 
            set
            {
                name = value;
            } 
        }

        [Property(0)]
        [Column(1, Name = "FRIENDLY_NAME")]
        public string FriendlyName
        {
            get
            {
                return friendlyName;
            }
            set
            {
                friendlyName = value;
            }
        }

    }
}
