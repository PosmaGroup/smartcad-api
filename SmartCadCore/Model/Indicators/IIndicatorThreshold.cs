using System;
using System.Text;

namespace SmartCadCore.Model
{
    public interface IIndicatorThreshold
    {
        int Low
        {
            get;
            set;
        }
        int High
        {
            get;
            set;
        }
        PositivePattern Pattern
        {
            get;
            set;
        }
    }
}
