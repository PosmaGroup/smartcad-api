using System;
using System.Text;
using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(IndicatorClassDashboardData), Table = "INDICATOR_CLASS_DASHBOARD", Lazy = false, Where = "DELETED_ID IS NULL")]
    public class IndicatorClassDashboardData : ObjectData
    {
        IndicatorData indicator;
        IndicatorClassData indicatorClass;
        bool showDashboard = false;

        public IndicatorClassDashboardData()
        { }

        public IndicatorClassDashboardData(IndicatorClassData indicatorClass, IndicatorData indicator)
        {
            this.indicatorClass = indicatorClass;
            this.indicator = indicator;
        }

        [ManyToOne(0, ClassType = typeof(IndicatorData), ForeignKey = "FK_INDICATOR_INDICATOR_CLASS_DASHBOARD_CODE", Cascade = "none")]
        [Column(1, Name = "INDICATOR_CODE", NotNull = true)]
        public IndicatorData Indicator
        {
            get { return indicator; }
            set { indicator = value; }
        }

        [ManyToOne(0, ClassType = typeof(IndicatorClassData), ForeignKey = "FK_INDICATOR_CLASS_INDICATOR_CLASS_DASHBOARD_CODE", Cascade = "none")]
        [Column(1, Name = "INDICATOR_CLASS_CODE", NotNull = true)]
        public IndicatorClassData IndicatorClass
        {
            get { return indicatorClass; }
            set { indicatorClass = value; }
        }

        [Property(0, Column = "SHOW_DASHBOARD")]
        public bool ShowDashboard
        {
            get { return showDashboard; }
            set { showDashboard = value; }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is IndicatorClassDashboardData)
            { 
                IndicatorClassDashboardData icdd = (IndicatorClassDashboardData)obj;
                result = this.Indicator == icdd.Indicator && this.IndicatorClass == icdd.IndicatorClass;
            }
            return result;
        }
        public override int GetHashCode()
        {
            return this.indicator.Code;
        }
    }
}
