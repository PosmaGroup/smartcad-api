using System;
using System.Text;

using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(IndicatorClassData), Table = "INDICATOR_CLASS", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_INDICATOR_CLASS_NAME")]
    [AttributeIdentifier("DELETED_ID_2", Value = "UK_INDICATOR_CLASS_FRIENDLY_NAME")]
    public class IndicatorClassData : ObjectData, INamedObjectData
    {
        private string name;
        private string friendlyName;
        private string description;

        [Property(0, Unique = true)]
        [Column(1, Name = "NAME", NotNull = true, UniqueKey = "UK_INDICATOR_CLASS_NAME")]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "FRIENDLY_NAME", UniqueKey = "UK_INDICATOR_CLASS_FRIENDLY_NAME", NotNull = true)]
        public virtual string FriendlyName
        {
            get
            {
                return friendlyName;
            }
            set
            {
                friendlyName = value;
            }
        }

        [Property(0, Column = "DESCRIPTION")]
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is IndicatorClassData)
            {
                result = this.Name == ((IndicatorClassData)obj).Name;
            }
            return result;
        }

        [InitialData(PropertyName = "Name", PropertyValue = "SYSTEM")]
        public static IndicatorClassData System;

        [InitialData(PropertyName = "Name", PropertyValue = "GROUP")]
        public static IndicatorClassData Group;

        [InitialData(PropertyName = "Name", PropertyValue = "DEPARTMENT")]
        public static IndicatorClassData Department;

        [InitialData(PropertyName = "Name", PropertyValue = "OPERATOR")]
        public static IndicatorClassData Operator;
    }
}
