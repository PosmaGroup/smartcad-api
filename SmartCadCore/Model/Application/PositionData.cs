﻿using NHibernate.Mapping.Attributes;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    #region Class PositionData Documentation
    /// <summary>
    /// Clase que representa a un cargo
    /// </summary>
    /// <className>PositionData</className>
    /// <author email="jynojosa@smartmatic.com">Julio Ynojosa</author>
    /// <author email="atorres@smartmatic.com">Alden Torres</author>
    /// <date>2006/03/30</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    [Serializable]
    [Class(NameType = typeof(PositionData), Table = "POSITION", Lazy = false, BatchSize = 1000, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_POSITION_NAME")]
    public class PositionData : ObjectData
    {
        #region Enums
        public enum Properties
        {
            Name,
            Description
        }
        #endregion

        #region Properties

        private string name;

        [Property(0, Unique = true)]
        [Column(1, Name = "NAME", UniqueKey = "UK_POSITION_NAME", NotNull = true)]
        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        private string description;

        [Property(Column = "DESCRIPTION")]
        public virtual string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }

        private DepartmentTypeData departmentType;

        [ManyToOne(0, ClassType = typeof(DepartmentTypeData),
            ForeignKey = "FK_POSITION_DEPARTMENT_TYPE_CODE", Cascade = "none", Fetch = FetchMode.Join)]
        [Column(1, Name = "DEPARTMENT_TYPE_CODE", NotNull = true, UniqueKey = "UK_POSITION_NAME")]
        public virtual DepartmentTypeData DepartmentType
        {
            get
            {
                return departmentType;
            }
            set
            {
                departmentType = value;
            }
        }


        #endregion

        #region Initial Data
        [InitialData(PropertyName = "Name", PropertyValue = "PositionData")]
        public static readonly UserResourceData Resource;
        #endregion

        #region Overrides

        public override string ToString()
        {
            return this.Name;
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is PositionData)
            {
                result = this.Code == ((PositionData)obj).Code;
            }
            return result;
        }

        #endregion
    }
}
