using System;
using System.Text;
using System.Drawing;
using ColorBase = System.Drawing.Color;
using NHibernate.Mapping.Attributes;


namespace SmartCadCore.Model
{
    [Serializable]
    [Component()]
    [AttributeIdentifier("COLORA", Value = "COLORA")]
    [AttributeIdentifier("COLORR", Value = "COLORR")]
    [AttributeIdentifier("COLORG", Value = "COLORG")]
    [AttributeIdentifier("COLORB", Value = "COLORB")]
    public class ComponentColorData
    {
            private int? colorA;
            private int? colorR;
            private int? colorG;
            private int? colorB;

            [Property(Column = "{{COLORA}}")]
            [AttributeIdentifier("COLORA", Value = "COLORA")]
            public int? ColorA
            {
                get { return colorA; }
                set { colorA = value; }
            }

            [Property(Column = "{{COLORR}}")]
            [AttributeIdentifier("COLORR", Value = "COLORR")]
            public int? ColorR
            {
                get { return colorR; }
                set { colorR = value; }
            }

            [Property(Column = "{{COLORG}}")]
            [AttributeIdentifier("COLORG", Value = "COLORG")]
            public int? ColorG
            {
                get { return colorG; }
                set { colorG = value; }
            }

            [Property(Column = "{{COLORB}}")]
            [AttributeIdentifier("COLORB", Value = "COLORB")]
            public int? ColorB
            {
                get { return colorB; }
                set { colorB = value; }
            }

            public ComponentColorData()
                : this(Color.White)
            { }

            public ComponentColorData(Color value)
            {
                colorA = Convert.ToInt32(value.A);
                colorR = Convert.ToInt32(value.R);
                colorG = Convert.ToInt32(value.G);
                colorB = Convert.ToInt32(value.B);
            }
    }
}
