using System;
using System.Text;

namespace SmartCadCore.Model
{
    public interface IImmutableObjectData
    {
        bool? Immutable
        {
            get;
            set;
        }
    }
}
