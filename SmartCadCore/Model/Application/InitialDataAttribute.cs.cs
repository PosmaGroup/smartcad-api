using System;
using System.Text;

namespace SmartCadCore.Model
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
    public class InitialDataAttribute : Attribute
    {
        private string propertyName;

        public string PropertyName
        {
            get
            {
                return propertyName;
            }
            set
            {
                propertyName = value;
            }
        }

        private string propertyValue;

        public string PropertyValue
        {
            get
            {
                return propertyValue;
            }
            set
            {
                propertyValue = value;
            }
        }
    }
}
