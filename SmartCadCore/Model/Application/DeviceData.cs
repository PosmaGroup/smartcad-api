using System;
using System.Text;
using NHibernate.Mapping.Attributes;
using Iesi.Collections;

using System.Collections;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(DeviceData), Table = "DEVICE", Lazy = false, BatchSize = 1000, Where = "DELETED_ID IS NULL")]
    //[AttributeIdentifier("DELETED_ID_1", Value = "UK_DEVICE_NAME")]
    public class DeviceData : ObjectData, INamedObjectData
    {

        private string ip;
        private string port;
        private string ipServer;
        private string login;
        private string password;
        private string name;
        private IList operators;



        //[Property(0, Unique = false)]
        //[Column(1, Name = "NAME", UniqueKey = "UK_DEVICE_NAME", NotNull = true)]
        [Property(Column = "NAME", NotNull = false)]
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                if (value != "")
                    name = value;
            }
        }
        [Property(Column = "PORT", NotNull = false)]
        public string Port
        {
            get
            {
                return port;
            }
            set
            {
                if (value != "")
                    port = value;
            }
        }
        [Property(Column = "IPSERVER", NotNull = false)]
        public string IpServer
        {
            get
            {
                return ipServer;
            }
            set
            {
                ipServer = value;
            }
        }



        [Property(Column = "IP", NotNull = false)]
        public string Ip
        {
            get
            {
                return ip;
            }
            set
            {
                ip = value;
            }
        }


        [Property(Column = "LOGIN", NotNull = false)]
        public string Login
        {
            get
            {
                return login;
            }
            set
            {
                if (value != "")
                    login = value;
            }
        }
        [Property(Column = "PASSWORD", NotNull = false)]
        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                if (value != "")
                    password = value;
            }
        }


        [Bag(0, Name = "Operators", Table = "OPERATOR_DEVICE", Cascade = "save-update",
            Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect, Where = "DELETED_ID IS NULL")]
        [Key(1, Column = "DEVICE_CODE"/*, ForeignKey = "FK_OPERATOR_DEVICE_DEVICE_CODE"*/)]
        [OneToMany(2, ClassType = typeof(OperatorDeviceData))]
        public IList Operators
        {
            get
            {
                return operators;
            }
            set
            {
                operators = value;
            }
        }


        [InitialData(PropertyName = "Name", PropertyValue = "DeviceData")]
        public static readonly UserResourceData Resource;
        public override string ToString()
        {
            return name;
        }
        public override bool Equals(object obj)
        {
            try
            {
                if (obj.GetType() == typeof(DeviceData))
                {
                    DeviceData cam = (DeviceData)obj;
                    if (this.Name == cam.Name)
                    {
                        return true;
                    }
                }
            }
            catch { }
            return false;
        }
    }

}
