﻿using System;
using System.Linq;
using System.Text;
using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
	[Serializable]
	public class ObjectPositionHistoryData:ObjectData
	{
		private DateTime date;
		private ObjectPositionAddressData geoPointAddress;

		[Property(Name = "Date")]
		public DateTime Date 
		{
			get
			{
				return date;
			}
			set
			{
				date = value;
			}
		}

		[ComponentProperty(ComponentType = typeof(ObjectPositionAddressData))]
		public ObjectPositionAddressData GeoPointAddress 
		{
			get
			{
				return geoPointAddress;
			}
			set
			{
				geoPointAddress = value;
			}
		}
	}
}
