using System;
using System.Text;
using NHibernate.Mapping.Attributes;
using Iesi.Collections;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(ConnectionTypeData), Table = "CONNECTION_TYPE", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_CONNECTION_TYPE")]
    public class ConnectionTypeData : ObjectData
    {
        private string name = string.Empty;

        [Property(0, Unique = true)]
        [Column(1, Name = "NAME", UniqueKey = "UK_CONNECTION_TYPE", NotNull = true)]
        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public override string ToString()
        {
            return this.Name;
        }

        [InitialData(PropertyName = "Name", PropertyValue = "ConnectionTypeData")]
        public static readonly UserResourceData Resource;
    }
}
