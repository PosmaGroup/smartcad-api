﻿using NHibernate.Mapping.Attributes;
using System;
using System.Collections;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    #region Class ObjectData Documentation
    /// <summary>
    /// Clase base de data.
    /// </summary>
    /// <className>ObjectData</className>
    /// <author email="fsilva@smartmatic.com">Fernando Silva</author>
    /// <author email="atorres@smartmatic.com">Alden Torres</author>
    ///  <author email="jynojosa@smartmatic.com">Julio Ynojosa</author>
    /// <date>2006/03/30</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    [Serializable]
    public class ObjectData : ICloneable
    {
        #region Properties

        private int code;
        [Id(0, Name = "Code", Column = "{{CODE}}", TypeType = typeof(int))]
        [Generator(1, Class = "identity")]
        [AttributeIdentifier("CODE", Value = "CODE")]
        public virtual int Code
        {
            get
            {
                return code;
            }
            set
            {
                code = value;
            }
        }

        /// <summary>
        /// Indica si el objeto ha sido borrado logicamente. Esta propiedad
        /// aplica a los objetos de clases que poseean el atributo 
        /// ReactivableAttribute
        /// </summary>
        public virtual bool? Deleted
        {
            get
            {
                return DeletedId != null;
            }
        }

        private Guid? deletedId;

        /// <summary>
        /// Indica el codigo de eliminado cuando el objeto ha sido 
        /// borrado logicamente. Esta propiedad aplica a los objetos de 
        /// clases que poseean el atributo ReactivableAttribute.
        /// </summary>
        [Property(0, Name = "DeletedId", Unique = true, NotNull = false)]
        [Column(1, Name = "DELETED_ID", UniqueKey = "{{DELETED_ID_1}}")]
        [Column(2, Name = "DELETED_ID", UniqueKey = "{{DELETED_ID_2}}")]
        [Column(2, Name = "DELETED_ID", UniqueKey = "{{DELETED_ID_3}}")]
        [Column(2, Name = "DELETED_ID", UniqueKey = "{{DELETED_ID_4}}")]
        [Column(2, Name = "DELETED_ID", UniqueKey = "{{DELETED_ID_5}}")]
        [Column(2, Name = "DELETED_ID", UniqueKey = "{{DELETED_ID_6}}")]
        [Column(2, Name = "DELETED_ID", UniqueKey = "{{DELETED_ID_7}}")]
        [Column(2, Name = "DELETED_ID", UniqueKey = "{{DELETED_ID_8}}")]
        [Column(2, Name = "DELETED_ID", UniqueKey = "{{DELETED_ID_9}}")]
        [Column(2, Name = "DELETED_ID", UniqueKey = "{{DELETED_ID_10}}")]
        [AttributeIdentifier("DELETED_ID_1", Value = "DELETED_ID_1")]
        [AttributeIdentifier("DELETED_ID_2", Value = "DELETED_ID_2")]
        [AttributeIdentifier("DELETED_ID_3", Value = "DELETED_ID_3")]
        [AttributeIdentifier("DELETED_ID_4", Value = "DELETED_ID_4")]
        [AttributeIdentifier("DELETED_ID_5", Value = "DELETED_ID_5")]
        [AttributeIdentifier("DELETED_ID_6", Value = "DELETED_ID_6")]
        [AttributeIdentifier("DELETED_ID_7", Value = "DELETED_ID_7")]
        [AttributeIdentifier("DELETED_ID_8", Value = "DELETED_ID_8")]
        [AttributeIdentifier("DELETED_ID_9", Value = "DELETED_ID_9")]
        [AttributeIdentifier("DELETED_ID_10", Value = "DELETED_ID_10")]
        public virtual Guid? DeletedId
        {
            get
            {
                return deletedId;
            }
            set
            {
                deletedId = value;
            }
        }

        private int version;

        [Version(Column = "VERSION", Name = "Version", UnsavedValue = "undefined")]
        public int Version
        {
            get
            {
                return version;
            }
            set
            {
                version = value;
            }
        }

        private bool initializeCollections;

        public bool InitializeCollections
        {
            get
            {
                return initializeCollections;
            }
            set
            {
                initializeCollections = value;
            }
        }
        #endregion

        #region Overrides

        public override bool Equals(object obj)
        {

            bool result = false;
            ObjectData objectData = obj as ObjectData;
            if (objectData != null)
            {
                if (/*this.Code == objectData.Code && */EqualsUniqueFields(objectData) == true)
                    result = true;
            }
            return result;

        }

        public override int GetHashCode()
        {
            return this.GetType().GetHashCode() * 100000 + this.Code + base.GetHashCode();
        }

        public static bool operator ==(ObjectData obj1, ObjectData obj2)
        {
            bool result = false;
            if (Object.Equals(obj1, null) && Object.Equals(obj2, null))
                result = true;
            else if (Object.Equals(obj1, null) || Object.Equals(obj2, null))
                result = false;
            else
                result = obj1.Equals(obj2);
            return result;
        }

        public static bool operator !=(ObjectData obj1, ObjectData obj2)
        {
            return !(obj1 == obj2);
        }

        #endregion

        #region Methods

        private bool EqualsUniqueFields(ObjectData objectData)
        {
            bool result = true;

            if (this.GetType().Name == objectData.GetType().Name)
            {
                PropertyInfo[] properties = this.GetType().GetProperties();

                foreach (PropertyInfo propertyInfo in properties)
                {
                    //object[] attributes = propertyInfo.GetCustomAttributes(typeof(ColumnAttribute), false);
                    object[] attributes = propertyInfo.GetCustomAttributes(typeof(PropertyAttribute), false);

                    //foreach (ColumnAttribute propertyAttribute in attributes)
                    foreach (PropertyAttribute propertyAttribute in attributes)
                    {
                        //if (string.IsNullOrEmpty(propertyAttribute.UniqueKey) == false)
                        if (propertyAttribute.Unique == true)
                        {
                            object valueThis = propertyInfo.GetValue(this, null);
                            object valueObject = propertyInfo.GetValue(objectData, null);

                            if (valueThis == null)
                                result = (valueObject == null) && result;
                            else
                                result = valueThis.Equals(valueObject) && result;
                            //return valueThis.Equals(valueObject);

                            if (!result)
                                return result;
                        }
                    }
                }
            }
            else
                result = false;
            return result;
        }
        #endregion

        #region Private Members

        private object Clone(object objectData)
        {
            object result = null;

            Type type = objectData.GetType();
            ConstructorInfo constructorInfo = type.GetConstructor(new Type[] { });

            result = constructorInfo.Invoke(new object[] { });
            Type resultType = result.GetType();
            foreach (PropertyInfo property in type.GetProperties())
            {
                PropertyInfo resultProperty = resultType.GetProperty(property.Name);
                object resultSetValue = null;
                if (property.PropertyType.IsSubclassOf(typeof(IList)) == true)
                {
                    resultSetValue = Clone((IList)property.GetValue(objectData, null));
                }
                else if (property.PropertyType.IsSubclassOf(typeof(ObjectData)) == true)
                {
                    resultSetValue = Clone((ObjectData)property.GetValue(objectData, null));
                    resultProperty.SetValue(result, Clone((ObjectData)property.GetValue(objectData, null)), null);
                }
                else
                {
                    resultSetValue = property.GetValue(objectData, null);
                }
                resultProperty.SetValue(result, resultSetValue, null);
            }
            return result;
        }

        private IList Clone(IList objectList)
        {
            ArrayList resultList = new ArrayList();
            try
            {
                foreach (object objectData in objectList)
                {
                    resultList.Add(Clone(objectData));
                }
            }
            catch
            {
                resultList = new ArrayList(objectList);
            }
            return resultList;
        }
        #endregion

        #region ICloneable Members

        public object Clone()
        {
            return Clone(this);
        }

        #endregion

        #region IComparer
        public IComparer IComparerByField(object field)
        {
            return new IComparerByField(field);
        }
        #endregion

        #region Public Static
        public static string GetPropertyName(object obj, object property)
        {
            if (obj == null)
                throw new ArgumentNullException("obj");

            if (property == null)
                throw new ArgumentNullException("property");

            bool propertyFounded = false;
            string propertyName = string.Empty;

            PropertyInfo[] properties = obj.GetType().GetProperties();

            int index = 0;

            while (index < properties.Length && !propertyFounded)
            {
                if (properties[index].PropertyType.IsInstanceOfType(property))
                {
                    MethodInfo method = properties[index].GetGetMethod();

                    if (method.Invoke(obj, null) == property)
                    {
                        propertyName = properties[index].Name;
                        propertyFounded = true;
                    }
                }

                index++;
            }

            return propertyName;
        }
        #endregion
    }

    public class IComparerByField : IComparer
    {
        private object fieldToCompare;
        private object subFieldToCompare;

        #region Constructors
        public IComparerByField(object fieldToCompare)
        {
            this.fieldToCompare = fieldToCompare;
        }

        public IComparerByField(object fieldToCompare, object subFieldToCompare)
            : this(fieldToCompare)
        {
            this.subFieldToCompare = subFieldToCompare;
        }
        #endregion

        #region IComparer Members

        public int Compare(object x, object y)
        {
            Type type = fieldToCompare.GetType();
            if (type.IsSubclassOf(typeof(int)) == true)
            {
                int valueX = (int)x.GetType().GetProperty(ObjectData.GetPropertyName(x, fieldToCompare)).GetValue(x, null);
                int valueY = (int)x.GetType().GetProperty(ObjectData.GetPropertyName(y, fieldToCompare)).GetValue(y, null);
                return valueX.CompareTo(y);
            }
            else if (type.IsSubclassOf(typeof(string)) == true)
            {
                string valueX = (string)x.GetType().GetProperty(ObjectData.GetPropertyName(x, fieldToCompare)).GetValue(x, null);
                string valueY = (string)x.GetType().GetProperty(ObjectData.GetPropertyName(y, fieldToCompare)).GetValue(y, null);
                return ((new CaseInsensitiveComparer()).Compare(valueX, valueY));
            }
            else if (type.IsSubclassOf(typeof(DateTime)) == true)
            {
                DateTime valueX = (DateTime)x.GetType().GetProperty(ObjectData.GetPropertyName(x, fieldToCompare)).GetValue(x, null);
                DateTime valueY = (DateTime)x.GetType().GetProperty(ObjectData.GetPropertyName(y, fieldToCompare)).GetValue(y, null);
                return valueX.CompareTo(valueY);
            }
            else if (type.IsSubclassOf(typeof(ObjectData)) == true && subFieldToCompare != null)
            {
                IComparerByField comparer = new IComparerByField(subFieldToCompare);
                return comparer.Compare(x, y);
            }
            else
            {
                return x.ToString().CompareTo(y.ToString());
            }
        }

        #endregion
    }
}
