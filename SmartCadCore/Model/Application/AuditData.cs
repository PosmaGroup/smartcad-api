using System;
using System.Text;

using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    #region Class AuditData Documentation
    /// <summary>
    /// TODO:
    /// </summary>
    /// <className>AuditData</className>
    /// <author email="atorres@smartmatic.com">Alden Torres</author>
    /// <date>2006/04/06</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    [Serializable]
    [Class(NameType = typeof(AuditData), Table = "AUDIT", Lazy = false, Where = "DELETED_ID IS NULL")]
    public class AuditData : ObjectData
    {
        public AuditData()
        {
        }

        public AuditData(string action, string login, 
            UserApplicationData userApplication, DateTime dateTime)
        {
            this.action = action;
            this.login = login;
            this.userApplication = userApplication.FriendlyName;
            this.dateTime = dateTime;
        }

        public AuditData(string action, string login,
            UserResourceData userResource, UserActionData userAction,
            UserApplicationData userApplication,
            DateTime dateTime)
            : this(action, login, userApplication, dateTime)
        {
            this.userResource = userResource.FriendlyName;
            this.userAction = userAction.FriendlyName;
            this.userApplication = userApplication.FriendlyName;
        }

        public AuditData(string action, string login, 
            UserApplicationData userApplication, string typeName, 
            string message, DateTime dateTime)
            : this(action, login, userApplication, dateTime)
        {
            this.typeName = typeName;
            this.message = message;
        }

        #region Fields

        private string action;
        public string login;
        private string operatorCode;
        private string userResource;
        private string userAction;
        private string userApplication;
        private string typeName;
        private string message;
        private int m_code;
        private string m_table;
        private DateTime? dateTime;

        #endregion

        [Property(Column = "ACTION", NotNull = true)]
        public string Action
        {
            set
            {
                action = value;
            }
            get
            {
                return action;
            }
        }

        [Property(Column = "LOGIN", NotNull = true)]
        public string Login
        {
            get
            {
                return login;
            }
            set
            {
                login = value;
            }
        }

        [Property(Column = "OPERATOR_CODE", NotNull = true)] 
        public string OperatorCode
        {
            get { return operatorCode; }
            set { operatorCode = value; }
        }

        [Property(Column = "USER_RESOURCE")]
        public string UserResource
        {
            get
            {
                return userResource;
            }
            set
            {
                userResource = value;
            }
        }

        [Property(Column = "USER_ACTION")]
        public string UserAction
        {
            get
            {
                return userAction;
            }
            set
            {
                userAction = value;
            }
        }

        [Property(Column = "USER_APPLICATION")]
        public string UserApplication
        {
            get
            {
                return userApplication;
            }
            set
            {
                userApplication = value;
            }
        }

        [Property(Column = "TYPE_NAME")]
        public string TypeName
        {
            get
            {
                return typeName;
            }
            set
            {
                typeName = value;
            }
        }

        [Property(Column = "MESSAGE", Length = 512)]
        public string Message
        {
            get
            {
                return message;
            }
            set
            {
                message = value;
            }
        }

        [Property(Column = "DATE_TIME", NotNull = true, TypeType = typeof(DateTime))]
        public DateTime? DateTime
        {
            get 
            { 
                return dateTime; 
            }
            set 
            { 
                dateTime = value; 
            }
        }

        [Property(Column = "OBJECT_CODE",NotNull = false)]
        public int ObjectCode
        {
            get { return m_code; }
            set { m_code = value; }
        }

        [Property(Column = "OBJECT_TABLE",NotNull = false)]
        public string ObjectTable
        {
            get { return m_table; }
            set { m_table = value; }
        }
	
	
    }
}
