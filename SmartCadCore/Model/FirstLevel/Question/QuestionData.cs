﻿using Iesi.Collections;
using NHibernate.Mapping.Attributes;
using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    #region Class QuestionData Documentation
    /// <summary>
    /// This class represents a possible question of a phone report
    /// </summary>
    /// <className>QuestionData</className>
    /// <author email="fsilva@smartmatic.com">Fernando Silva</author>
    /// <date>2007/01/30</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    [Serializable]
    [Class(NameType = typeof(QuestionData), Table = "QUESTION", Lazy = false, BatchSize = 100, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_QUESTION_CUSTOM_CODE")]
    [AttributeIdentifier("DELETED_ID_2", Value = "UK_QUESTION_SHORT_TEXT")]
    [AttributeIdentifier("DELETED_ID_3", Value = "UK_QUESTION_TEXT")]
    public class QuestionData : ObjectData
    {
        #region Enums
        /// <summary>
        /// Enumerates the properties that this class has.
        /// </summary>
        public enum Properties
        {
            ShortText,
            Text,
            RequirementType,
            CustomCode,
            PossibleAnswers,
            RenderMethod,
            IsClosedQuestion
        }

        #endregion

        #region Fields

        private string shortText;
        private string text;
        private RequirementTypeEnum? requirementType;
        private string customCode;
        private ISet setPossibleAnswers;
        private string renderMethod;
        private ISet incidentTypes;
        private IList app;
        #endregion

        #region Constructors

        public QuestionData()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// The short question of a phone report.
        /// </summary>
        [Property(0, Unique = true)]
        [Column(1, Name = "SHORT_TEXT", NotNull = true, UniqueKey = "UK_QUESTION_SHORT_TEXT")]
        public virtual string ShortText
        {
            get
            {
                return shortText;
            }
            set
            {
                shortText = value;
            }
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "CUSTOM_CODE", UniqueKey = "UK_QUESTION_CUSTOM_CODE", NotNull = true)]
        public virtual string CustomCode
        {
            get
            {
                return customCode;
            }
            set
            {
                customCode = value;
            }
        }

        /// <summary>
        /// A posible question of the phone report
        /// </summary>
        [Property(0, Unique = true)]
        [Column(1, Name = "TEXT", NotNull = true, UniqueKey = "UK_QUESTION_TEXT")]
        public virtual string Text
        {
            get
            {
                return text;
            }
            set
            {
                text = value;
            }
        }




        /// <summary>
        /// Indicate that the question is mandatory.
        /// </summary>
        [Property(0, TypeType = typeof(RequirementTypeEnum))]
        [Column(1, Name = "REQUIREMENT_TYPE", NotNull = true)]
        public virtual RequirementTypeEnum? RequirementType
        {
            get
            {
                return requirementType;
            }
            set
            {
                requirementType = value;
            }
        }

        [Set(0, Name = "SetPossibleAnswers", Table = "QUESTION_POSSIBLE_ANSWER", Cascade = "all-delete-orphan",
            Lazy = CollectionLazy.True, Inverse = true, Fetch = CollectionFetchMode.Subselect, Where = "DELETED_ID IS NULL")]
        [Key(1, Column = "INCIDENT_TYPE_QUESTION_CODE"/*, ForeignKey = "FK_INCIDENT_TYPE_POSSIBLE_ANSWER_QUESTION"*/)]
        [OneToMany(2, ClassType = typeof(QuestionPossibleAnswerData))]
        public ISet SetPossibleAnswers
        {
            get
            {
                return setPossibleAnswers;
            }
            set
            {
                setPossibleAnswers = value;
            }
        }

        /// <summary>
        /// The short question of a phone report.
        /// </summary>
        [Property(0)]
        [Column(1, Name = "RENDER_METHOD", Length = 65535)]
        public virtual string RenderMethod
        {
            get
            {
                return renderMethod;
            }
            set
            {
                renderMethod = value;
            }
        }

        [Set(0, Name = "IncidentTypes", Table = "INCIDENT_TYPE_QUESTION", Cascade = "save-update",
           Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect, Inverse = true, Where = "DELETED_ID IS NULL")]
        [Key(1, Column = "QUESTION_CODE"/*, ForeignKey = "FK_INCIDENT_TYPE_QUESTION_QUESTION_CODE"*/)]
        [OneToMany(2, ClassType = typeof(IncidentTypeQuestionData))]
        public virtual ISet IncidentTypes
        {
            get
            {
                return incidentTypes;
            }
            set
            {
                incidentTypes = value;
            }
        }

        [Bag(0, Name = "App", Table = "QUESTION_USER_APPLICATION", Cascade = "none",
            Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect)]
        [Key(1, Column = "QUESTION_CODE", ForeignKey = "FK_APP_QUESTION_CODE")]
        [ManyToMany(2, ClassType = typeof(UserApplicationData), Column = "USER_APPLICATION_CODE", ForeignKey = "FK_QUESTION_APP_CODE")]
        public IList App
        {
            get
            {
                return app;
            }
            set
            {
                app = value;
            }
        }

        #endregion

        [InitialData(PropertyName = "Name", PropertyValue = "QuestionData")]
        public static readonly UserResourceData Resource;
    }

    public enum RequirementTypeEnum
    {
        None = 0,
        Recommended = 1,
        Required = 2
    }
}
