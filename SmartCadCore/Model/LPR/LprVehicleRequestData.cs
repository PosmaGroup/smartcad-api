using System;
using System.Text;
using NHibernate.Mapping.Attributes;
using Iesi.Collections;
using System.Collections;

namespace SmartCadCore.Model
{    
    [Serializable]
    [Class(NameType = typeof(LprVehicleRequestData), Table = "LPR_VEHICLE_REQUEST", Lazy = false, Where = "DELETED_ID IS NULL")]
    public class LprVehicleRequestData : ObjectData
    {      
        private int parentCode;

        private LprOwnerData requestFor;
        
        private DepartmentTypeData requestByDepartment;
        private string requestByOfficcer;
        private string plate;
        private string brand;
        private string model;
        private string color;

        private string details;
        private DateTime dateAndTime;

        [Key(0, Column = "PARENT_CODE", ForeignKey = "FK_LPR_VEHICLE_REQUEST_CODE")]
        public virtual int ParentCode
        {
            get
            {
                return parentCode;
            }
            set
            {
                parentCode = value;
            }
        }

        [Property(Column = "REQUEST_FOR", NotNull =true)]
        public LprOwnerData RequestFor
        {
            get
            {
                return requestFor;
            }
            set
            {
                requestFor = value;
            }
        }

        [ManyToOne(Column = "REQUEST_BY_DEPARTMENT", ClassType = typeof(DepartmentTypeData),
            ForeignKey = "FK_LPR_DEPARTMENT_VEHICLE_REQUEST", NotNull = true, Cascade = "none")]
        public DepartmentTypeData RequestByDepartment
        {
            get
            {
                return requestByDepartment;
            }
            set
            {
                requestByDepartment = value;
            }
        }


        [Property(Column = "REQUEST_BY_OFFICCER", NotNull = true)]
        public string RequestByOfficcer
        {
            get
            {
                return requestByOfficcer;
            }
            set
            {
                requestByOfficcer = value;
            }
        }
        [Property(Column = "PLATE", NotNull = true)]
        public string Plate
        {
            get
            {
                return plate;
            }
            set
            {
                plate = value;
            }
        }
        [Property(Column = "BRAND", NotNull = false)]
        public string Brand
        {
            get
            {
                return brand;
            }
            set
            {
                brand = value;
            }
        }
        [Property(Column = "MODEL", NotNull = false)]
        public string Model
        {
            get
            {
                return model;
            }
            set
            {
                model = value;
            }
        }
        [Property(Column = "COLOR", NotNull = false)]
        public string Color
        {
            get
            {
                return color;
            }
            set
            {
                color = value;
            }
        }
        [Property(Column = "DETAILS", NotNull = false)]
        public string Details
        {
            get
            {
                return details;
            }
            set
            {
                details = value;
            }
        }
        [Property(Column = "DATE_AND_TIME", NotNull = true)]
        public DateTime DateAndTime
        {
            get
            {
                return dateAndTime;
            }
            set
            {
                dateAndTime = value;
            }
        }


        [InitialData(PropertyName = "Name", PropertyValue = "LprVehicleRequestData")] 
        public static readonly UserResourceData Resource;


        public override bool Equals(object obj)
        {
            try
            {
                if (obj.GetType() == typeof(LprVehicleRequestData))
                {
                    LprVehicleRequestData cam = (LprVehicleRequestData)obj;
                   if ((this.plate == cam.Plate) && (this.dateAndTime == cam.DateAndTime))
                    {
                        return true;
                    }
                }
            }
            catch { }
            return false;
        }
    }

}
