﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Mapping.Attributes;
using Iesi.Collections;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(LprOwnerData), Table = "LPR_OWNER", Lazy = false, BatchSize = 1000, Where = "DELETED_ID IS NULL")]
    public class LprOwnerData : ObjectData
    {
        #region fields

        private String name;

        private String last_name;

        private int id;

        private String address;

        private int phone;

        #endregion

        #region propierties

        [Property(0)]
        [Column(1, Name = "NAME", NotNull = false)]
        public virtual string Name
        {
            get { return name; }
            set { name = value; }
        }

        [Property(0)]
        [Column(1, Name = "LAST_NAME", NotNull = false)]
        public virtual string Last_name
        {
            get { return last_name; }
            set { last_name = value; }
        }

        [Property(0)]
        [Column(1, Name = "ID", NotNull = true)]
        public virtual int Id
        {
            get { return id; }
            set { id = value; }
        }

        [Property(0)]
        [Column(1, Name = "ADDRESS", NotNull = false)]
        public virtual string Address
        {
            get { return address; }
            set { address = value; }
        }

        [Property(0)]
        [Column(1, Name = "PHONE", NotNull = true)]
        public virtual int Phone
        {
            get { return phone; }
            set { phone = value; }
        }
        #endregion
    }
}
