﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Mapping.Attributes;
using Iesi.Collections;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(LprAlertData), Table = "LPR_ALERT", Lazy = false, BatchSize = 1000, Where = "DELETED_ID IS NULL")]
    public class LprAlertData: ObjectData
    {
        #region fields

        private CameraData camera;

        private DateTime alertDate;

        private int status;

        private int externalId;

        private String plate;

        private String vehiculeCode;

        #endregion

        #region propierties

        [ManyToOne(0, ClassType = typeof(CameraData), ForeignKey = "FK_LPR_ALERT_CAM_CAMERA", Cascade = "none", Column = "CAMERA_CODE")]
        [Column(1, Name = "CAMERA_CODE", NotNull = true)]
        public virtual CameraData Camera
        {
            get { return camera; }
            set { camera = value; }
        }

        [Property(0, TypeType = typeof(DateTime))]
        [Column(1, Name = "ALERT_DATE")]
        public virtual DateTime AlertDate
        {
            get { return alertDate; }
            set { alertDate = value; }
        }

        [Property(0)]
        [Column(1, Name = "STATUS", NotNull = false)]
        public virtual int Status
        {
            get { return status; }
            set { status = value; }
        }

        [Property(0)]
        [Column(1, Name = "EXTERNAL_ID", NotNull = true)]
        public virtual int ExternalId
        {
            get { return externalId; }
            set { externalId = value; }
        }

        [Property(0)]
        [Column(1, Name = "PLATE", NotNull = true)]
        public virtual String Plate
        {
            get { return plate; }
            set { plate = value; }
        }

        [Property(0)]
        [Column(1, Name = "VEHICULE_CODE", NotNull = true)]
        public virtual String VehiculeCode
        {
            get { return vehiculeCode; }
            set { vehiculeCode = value; }
        }

        #endregion

        #region InitialData

        [InitialData(PropertyName = "Name", PropertyValue = "LprAlertData")]
        public static readonly UserResourceData Resource;

        #endregion
    }
}
