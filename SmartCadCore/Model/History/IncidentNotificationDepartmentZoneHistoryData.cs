using System;
using System.Text;

using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(IncidentNotificationDepartmentStationHistoryData), Table = "INCIDENT_NOTIFICATION_DEPARTMENT_STATION_HISTORY", Lazy = false, Where = "DELETED_ID IS NULL AND END_DATE IS NULL")]
    [AttributeIdentifier("UK_START", Value = "UK_INCIDENT_NOTIFICATION_DEPARTMENT_STATION_HISTORY")]
    [AttributeIdentifier("UK_END", Value = "UK_INCIDENT_NOTIFICATION_DEPARTMENT_STATION_HISTORY")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_INCIDENT_NOTIFICATION_DEPARTMENT_STATION_HISTORY")]
    [AttributeIdentifier("IX_START", Value = "IX_INCIDENT_NOTIFICATION_DEPARTMENT_STATION_HISTORY_START")]
    public class IncidentNotificationDepartmentStationHistoryData : BaseSessionHistory
    {
        #region Fields
        private OperatorData userAccount;

        private IncidentNotificationData incidentNotification;

        private DepartmentStationData departmentStation;
        #endregion

        #region Properties

        /// <summary>
        /// User account who is logged in.
        /// </summary>

        [ManyToOne(0, ClassType = typeof(OperatorData), Insert = true, Update = false,
            ForeignKey = "FK_INCIDENT_NOTIFICATION_DEPARTMENT_STATION_HISTORY_USER_ACCOUNT_CODE", Cascade = "none")]
        [Column(1, Name = "USER_ACCOUNT_CODE", NotNull = true, UniqueKey = "UK_INCIDENT_NOTIFICATION_DEPARTMENT_STATION_HISTORY")]
        public virtual OperatorData UserAccount
        {
            get
            {
                return userAccount;
            }
            set
            {
                userAccount = value;
            }
        }

        [ManyToOne(0, ClassType = typeof(IncidentNotificationData),
        ForeignKey = "FK_INCIDENT_NOTIFICATION_DEPARTMENT_STATION_HISTORY_INCIDENT_NOTIFICATION_CODE", Cascade = "none")]
        [Column(1, Name = "INCIDENT_NOTIFICATION_CODE", NotNull = true, UniqueKey = "UK_INCIDENT_NOTIFICATION_DEPARTMENT_STATION_HISTORY")]
        public IncidentNotificationData IncidentNotification
        {
            get
            {
                return incidentNotification;
            }
            set
            {
                incidentNotification = value;
            }
        }

        [ManyToOne(0, ClassType = typeof(DepartmentStationData),
       ForeignKey = "FK_INCIDENT_NOTIFICATION_DEPARTMENT_STATION_HISTORY_DEPARTMENT_STATION_CODE", Cascade = "none")]
        [Column(1, Name = "DEPARTMENT_STATION_CODE", NotNull = true, UniqueKey = "UK_INCIDENT_NOTIFICATION_DEPARTMENT_STATION_HISTORY")]
        public virtual DepartmentStationData DepartmentStation
        {
            get
            {
                return departmentStation;
            }
            set
            {
                departmentStation = value;
            }
        }
        #endregion
    }
}
