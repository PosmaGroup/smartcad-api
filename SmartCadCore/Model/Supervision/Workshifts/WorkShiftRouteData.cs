﻿using NHibernate.Mapping.Attributes;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(WorkShiftRouteData), Table = "WORK_SHIFT_ROUTE", Lazy = false, Where = "DELETED_ID IS NULL")]
    public class WorkShiftRouteData : ObjectData
    {
        #region Constructor

        public WorkShiftRouteData()
            : base()
        {
            this.InRoute = true;
        }

        #endregion

        #region Properties

        [ManyToOne(ClassType = typeof(WorkShiftVariationData), Column = "WORK_SHIFT_CODE",
            Cascade = "none", ForeignKey = "FK_WORK_SHIFT_ROUTE_WORK_SHIFT_CODE")]
        public WorkShiftVariationData WorkShift
        {
            get;
            set;
        }

        [ManyToOne(Column = "ROUTE_CODE", ClassType = typeof(RouteData),
            ForeignKey = "FK_WORK_SHIFT_ROUTE_ROUTE_CODE", Cascade = "none")]
        public RouteData Route
        {
            get;
            set;
        }

        [ManyToOne(Column = "UNIT_CODE", ClassType = typeof(UnitData),
        ForeignKey = "FK_WORK_SHIFT_ROUTE_UNIT_CODE", Cascade = "none")]
        public UnitData Unit
        {
            get;
            set;
        }

        [Property(Column = "INROUTE", NotNull = true, TypeType = typeof(bool))]
        public bool InRoute { get; set; }

        #endregion

        #region Overrides

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is WorkShiftRouteData)
            {
                result = this.Code == ((WorkShiftRouteData)obj).Code;
            }
            return result;
        }

        #endregion
    }
}
