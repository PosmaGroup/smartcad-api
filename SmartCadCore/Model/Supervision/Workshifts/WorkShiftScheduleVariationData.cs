﻿using NHibernate.Mapping.Attributes;
using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(WorkShiftScheduleVariationData), Table = "WORK_SHIFT_SCHEDULE_VARIATION", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("UK_START", Value = "")]
    [AttributeIdentifier("UK_END", Value = "")]
    public class WorkShiftScheduleVariationData : BaseSessionHistory
    {
        private WorkShiftVariationData workShiftVariation;
        private IList supervisedOperators;

        [ManyToOne(ClassType = typeof(WorkShiftVariationData), Column = "WORK_SHIFT_VARIATION_CODE",
       Cascade = "none", ForeignKey = "FK_WORK_SHIFT_VARIATION_WORK_SHIFT_SCHEDULE_VARIATION_CODE")]
        public WorkShiftVariationData WorkShiftVariation
        {
            get
            {
                return workShiftVariation;
            }
            set
            {
                workShiftVariation = value;
            }
        }


        public override bool Equals(object obj)
        {

            bool result = false;
            ObjectData data = obj as ObjectData;
            if (data != null)
            {
                if (this.Code == data.Code)
                    result = true;
            }
            return result;

        }

    }
}
