using System;
using System.Text;
using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    [Serializable]
    [Component()]
    public class WeekDaysComponentData
    {
        private bool sunday;
        private bool monday;
        private bool tuesday;
        private bool wednesday;
        private bool thursday;
        private bool friday;
        private bool saturday;

        public WeekDaysComponentData()
        { }

        [Property(Column = "{{SUNDAY}}")]
        [AttributeIdentifier("SUNDAY", Value = "SUNDAY")]
        public bool Sunday
        {
            get { return sunday; }
            set { sunday = value; }
        }

        [Property(Column = "{{MONDAY}}")]
        [AttributeIdentifier("MONDAY", Value = "MONDAY")]
        public bool Monday
        {
            get { return monday; }
            set { monday = value; }
        }

        [Property(Column = "{{TUESDAY}}")]
        [AttributeIdentifier("TUESDAY", Value = "TUESDAY")]
        public bool Tuesday
        {
            get { return tuesday; }
            set { tuesday = value; }
        }

        [Property(Column = "{{WEDNESDAY}}")]
        [AttributeIdentifier("WEDNESDAY", Value = "WEDNESDAY")]
        public bool Wednesday
        {
            get { return wednesday; }
            set { wednesday = value; }
        }

        [Property(Column = "{{THURSDAY}}")]
        [AttributeIdentifier("THURSDAY", Value = "THURSDAY")]
        public bool Thursday
        {
            get { return thursday; }
            set { thursday = value; }
        }

        [Property(Column = "{{FRIDAY}}")]
        [AttributeIdentifier("FRIDAY", Value = "FRIDAY")]
        public bool Friday
        {
            get { return friday; }
            set { friday = value; }
        }

        [Property(Column = "{{SATURDAY}}")]
        [AttributeIdentifier("SATURDAY", Value = "SATURDAY")]
        public bool Saturday
        {
            get { return saturday; }
            set { saturday = value; }
        }
    }
}
