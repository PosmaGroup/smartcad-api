﻿using NHibernate.Mapping.Attributes;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(SessionStatusHistoryData), Table = "SESSION_STATUS_HISTORY", Lazy = false, Where = "DELETED_ID IS NULL")]
    public class SessionStatusHistoryData : ObjectData
    {
        #region Fields
        private SessionHistoryData session;
        private OperatorStatusData status;
        private DateTime? start;
        private DateTime? end;
        #endregion

        #region Constructors
        public SessionStatusHistoryData()
        { }
        #endregion

        #region Properties

        [ManyToOne(ClassType = typeof(SessionHistoryData), Column = "SESSION_HISTORY_CODE",
            ForeignKey = "FK_SESSION_HISTORY_SESSION_STATUS_HISTORY_CODE", NotNull = true, Cascade = "none")]
        public SessionHistoryData Session
        {
            get
            {
                return session;
            }
            set
            {
                session = value;
            }
        }

        [ManyToOne(ClassType = typeof(OperatorStatusData), Column = "OPERATOR_STATUS_CODE",
            ForeignKey = "FK_OPERATOR_STATUS_OPERATOR_STATUS_HISTORY_CODE", NotNull = true, Cascade = "none")]
        public OperatorStatusData Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }

        [Property(Column = "START_DATE", NotNull = true, TypeType = typeof(DateTime))]
        public DateTime? StartDate
        {
            get
            {
                return start;
            }
            set
            {
                start = value;
            }
        }

        [Property(Column = "END_DATE", TypeType = typeof(DateTime))]
        public DateTime? EndDate
        {
            get
            {
                return end;
            }
            set
            {
                end = value;
            }
        }
        #endregion
    }
}
