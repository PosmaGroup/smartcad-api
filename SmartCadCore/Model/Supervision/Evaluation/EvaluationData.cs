
using Iesi.Collections;
using NHibernate.Mapping.Attributes;
using System;
using System.Collections;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(EvaluationData), Table = "EVALUATION", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_2", Value = "UK_EVALUATION_NAME")]
    public class EvaluationData : ObjectData, INamedObjectData
    {
        #region Enums

        public enum Properties
        {
            Name,
            UserRoleCode,
            DepartamentTypeCode,
            OperatorCategory,
            Scale,
            Description,
            Questions
        }

        public enum EvaluationScales
        {
            OneToThree,
            OneToFive,
            YesNo
        }

        #endregion

        #region Fields

        private string name;
        private OperatorCategoryData operatorCategory;
        private EvaluationScales scale;
        private string description;    

        #endregion

        #region Constructors

        public EvaluationData()
        {
        }

        public EvaluationData(string name)
        {
            this.name = name;
        }

        #endregion

        #region Properties

        [Property(0, Unique = true)]
        [Column(1, Name = "NAME", UniqueKey = "UK_EVALUATION_NAME", NotNull = true)]
        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        [ManyToOne(ClassType = typeof(OperatorCategoryData), Column = "OPERATOR_CATEGORY",
           ForeignKey = "FK_EVALUATION_OPERATOR_CATEGORY", NotNull = true, Cascade = "none")]
        public OperatorCategoryData OperatorCategory
        {
            get
            {
                return operatorCategory;
            }
            set
            {
                operatorCategory = value;
            }
        }

        [Property(0, Unique = false)]
        [Column(1, Name = "SCALE", NotNull = false)]
        public virtual EvaluationScales Scale
        {
            get
            {
                return scale;
            }
            set
            {
                scale = value;
            }
        }

        [Property(0, Unique = false)]
        [Column(1, Name = "DESCRIPTION", NotNull = false)]
        public virtual string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }


        private ISet setEvaluationUserApplications;

        [Set(0, Name = "SetEvaluationUserApplications", Table = "EVALUATION_USER_APLICATION", Cascade = "save-update",
          Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect, Where = "DELETED_ID IS NULL")]
        [Key(1, Column = "EVALUATION_CODE"/*, ForeignKey = "FK_EVALUATION_USER_APPLICATION_EVALUATION_CODE"*/)]
        [OneToMany(2, ClassType = typeof(EvaluationUserApplicationData))]
        public ISet SetEvaluationUserApplications
        {
            get
            {
                return setEvaluationUserApplications;
            }
            set
            {
                setEvaluationUserApplications = value;
            }
        }

        private ISet setEvaluationDepartamentsType;

        [Set(0, Name = "SetEvaluationDepartamentsType", Table = "EVALUATION_DEPARTAMENT_TYPE", Cascade = "save-update",
         Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect, Where = "DELETED_ID IS NULL")]
        [Key(1, Column = "EVALUATION_CODE"/*, ForeignKey = "FK_EVALUATION_DEPARTAMENT_TYPE_EVALUATION_CODE"*/)]
        [OneToMany(2, ClassType = typeof(EvaluationDepartmentTypeData))]
        public ISet SetEvaluationDepartamentsType
        {
            get
            {
                return setEvaluationDepartamentsType;
            }
            set
            {
                setEvaluationDepartamentsType = value;
            }
        }

        private IList setQuestions;

        [Bag(0, Name = "SetQuestions", Table = "EVALUATION_QUESTION", Cascade = "save-update",
         Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect, Where = "DELETED_ID IS NULL")]
        [Key(1, Column = "EVALUATION_CODE", ForeignKey = "FK_EVALUATION_QUESTION_CODE")]
        [OneToMany(2, ClassType = typeof(EvaluationQuestionData))]
        public IList SetQuestions
        {
            get
            {
                return setQuestions;
            }
            set
            {
                setQuestions = value;
            }
        }

        #endregion

        #region Methods
     
        public override string ToString()
        {
            string text = "";

            if (this.Name != null)
            {
                text = this.Name;
            }

            return text;
        }

        #endregion

        #region Resource
        [InitialData(PropertyName = "Name", PropertyValue = "EvaluationData")]
        public static readonly UserResourceData Resource;
        #endregion
    }
}
