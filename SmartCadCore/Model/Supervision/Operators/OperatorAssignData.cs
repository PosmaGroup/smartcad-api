﻿using NHibernate.Mapping.Attributes;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    #region Class OperatorAssignData Documentation
    /// <summary>
    /// This class represents all the information an user account must have to be 
    /// included in the system.
    /// </summary>
    /// <className>OperatorAssignData</className>
    /// <author email="jynojosa@smartmatic.com">Julio Ynojosa</author>
    /// <date>2006/12/01</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion

    [Serializable]
    [Class(NameType = typeof(OperatorAssignData), Table = "OPERATOR_ASSIGN", Lazy = false, Where = "DELETED_ID IS NULL")]
    public class OperatorAssignData : ObjectData
    {
        #region Enums

        /// <summary>
        /// Enumerates the properties that this class has.
        /// </summary>
        public enum Properties
        {
            Operator,
            Supervisor,
            StartDate,
            EndDate,
            Day
        }

        #endregion

        #region Constants

        #endregion

        #region Fields

        private OperatorData supervisedOperator;
        private OperatorData supervisor;
        private DateTime startDate;
        private DateTime endDate;
        private WorkShiftScheduleVariationData supervisedScheduleVariation;
        private WorkShiftScheduleVariationData supervisorScheduleVariation;

        #endregion

        #region Constructor

        public OperatorAssignData()
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// User account login
        /// </summary>
        [ManyToOne(ClassType = typeof(OperatorData), Column = "OPERATOR_CODE", Cascade = "none",
            ForeignKey = "FK_OPERATOR_ASSIGN_OPERATOR_CODE")]
        public virtual OperatorData SupervisedOperator
        {
            get
            {
                return supervisedOperator;
            }
            set
            {
                supervisedOperator = value;
            }
        }

        [ManyToOne(ClassType = typeof(OperatorData), Column = "SUPERVISOR_CODE", Cascade = "none",
           ForeignKey = "FK_OPERATOR_ASSIGN_SUPERVISOR_CODE")]
        public virtual OperatorData Supervisor
        {
            get
            {
                return supervisor;
            }
            set
            {
                supervisor = value;
            }
        }

        [Property(Column = "START_DATE")]
        public virtual DateTime StartDate
        {
            get
            {
                return startDate;
            }
            set
            {
                startDate = value;
            }
        }

        [Property(Column = "END_DATE")]
        public virtual DateTime EndDate
        {
            get
            {
                return endDate;
            }
            set
            {
                endDate = value;
            }
        }


        [ManyToOne(ClassType = typeof(WorkShiftScheduleVariationData), Column = "WORK_SHIFT_SCHEDULE_VARIATION_CODE", Cascade = "none",
           ForeignKey = "FK_OPERATOR_ASSIGN_WORK_SHIFT_SCHEDULE_VARIATION_CODE")]
        public virtual WorkShiftScheduleVariationData SupervisedScheduleVariation
        {
            get
            {
                return supervisedScheduleVariation;
            }
            set
            {
                supervisedScheduleVariation = value;
            }
        }

        [ManyToOne(ClassType = typeof(WorkShiftScheduleVariationData), Column = "WORK_SHIFT_SCHEDULE_VARIATION_SUPERVISOR_CODE", Cascade = "none",
          ForeignKey = "FK_OPERATOR_ASSIGN_WORK_SHIFT_SCHEDULE_VARIATION_SUPERVISOR_CODE")]
        public virtual WorkShiftScheduleVariationData SupervisorScheduleVariation
        {
            get
            {
                return supervisorScheduleVariation;
            }
            set
            {
                supervisorScheduleVariation = value;
            }
        }

        #endregion

        public override bool Equals(Object obj)
        {
            if ((obj is OperatorAssignData) == false)
                return false;

            OperatorAssignData oad = ((OperatorAssignData)obj);
            if (this.EndDate == oad.EndDate &&
                this.StartDate == oad.StartDate &&
                this.SupervisedOperator.Code == oad.SupervisedOperator.Code &&
                this.Supervisor.Code == oad.Supervisor.Code &&
                this.SupervisedScheduleVariation.Code == oad.SupervisedScheduleVariation.Code &&
                this.SupervisorScheduleVariation.Code == oad.SupervisorScheduleVariation.Code)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
