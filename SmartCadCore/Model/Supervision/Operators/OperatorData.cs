﻿using Iesi.Collections;
using NHibernate.Mapping.Attributes;
using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(OperatorData), Table = "OPERATOR", Lazy = false, Where = "DELETED_ID IS NULL")]
    public class OperatorData : UserAccountData
    {
        #region Fields

        private string agentId;
        private string agentPassword;
        private IList departmentTypes;
        private DateTime? birthday;
        private IList currentIncidentNotifications;
        private OperatorData supervisor;
        private IList operators;
        private IList supervisors;
        private ISet categoryHistoryList;
        private IList devices;
        private IList workShifts;

        #endregion

        #region Constructors

        public OperatorData()
        {
        }

        #endregion

        #region Properties


        [Property(Column = "AGENTID")]
        public virtual string AgentID
        {
            get
            {
                return agentId;
            }
            set
            {
                agentId = value;
            }
        }

        [Property(Column = "AGENTPASSWORD")]
        public virtual string AgentPassword
        {
            get
            {
                return agentPassword;
            }
            set
            {
                agentPassword = value;
            }
        }

        [Bag(0, Name = "DepartmentTypes", Table = "OPERATOR_DEPARTMENT_TYPE", Cascade = "persist", Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect)]
        [Key(1, Column = "OPERATOR_CODE", ForeignKey = "FK_OPERATOR_DEPARTMENT_TYPE_CODE")]
        [ManyToMany(2, ClassType = typeof(DepartmentTypeData), Column = "DEPARTMENT_TYPE_CODE", ForeignKey = "FK_DEPARTMENT_TYPE_OPERATOR_CODE")]
        public virtual IList DepartmentTypes
        {
            get
            {
                return departmentTypes;
            }
            set
            {
                departmentTypes = value;
            }
        }

        [Property(0, TypeType = typeof(DateTime))]
        [Column(1, Name = "BIRTHDAY", NotNull = true)]
        public virtual DateTime? Birthday
        {
            get
            {
                return birthday;
            }
            set
            {
                birthday = value;
            }
        }



        [Bag(0, Name = "WorkShifts", Table = "WORK_SHIFT_VARIATION_OPERATOR", Cascade = "none", Lazy = CollectionLazy.True, Inverse = true, Fetch = CollectionFetchMode.Subselect, Where = "DELETED_ID IS NULL")]
        [Key(1, Column = "OPERATOR_CODE"/*, ForeignKey = "FK_WORK_SHIFT_VARIATION_OPERATOR_CODE"*/)]
        [OneToMany(2, ClassType = typeof(WorkShiftOperatorData))]
        public virtual IList WorkShifts
        {
            get
            {
                return workShifts;
            }
            set
            {
                workShifts = value;
            }
        }

        [Bag(0, Table = "OPERATOR", Cascade = "none",
          Lazy = CollectionLazy.True, Inverse = true, Fetch = CollectionFetchMode.Subselect,
           Where = "DELETED_ID IS NULL")]
        [Key(1, Column = "SUPERVISOR_CODE"/*, ForeignKey = "FK_OPERATOR_ASSIGN_SUPERVISOR_CODE"*/)]
        [OneToMany(2, ClassType = typeof(OperatorAssignData))]
        public IList Operators
        {
            get { return operators; }
            set { operators = value; }
        }


        [Bag(0, Table = "OPERATOR", Cascade = "none",
        Lazy = CollectionLazy.True, Inverse = true, Fetch = CollectionFetchMode.Subselect,
        Where = "DELETED_ID IS NULL")]
        [Key(1, Column = "OPERATOR_CODE"/*, ForeignKey = "FK_OPERATOR_ASSIGN_OPERATOR_CODE"*/)]
        [OneToMany(2, ClassType = typeof(OperatorAssignData))]
        public IList Supervisors
        {
            get { return supervisors; }
            set { supervisors = value; }

        }

        [Set(0, Name = "CategoryHistoryList", Table = "OPERATOR_CATEGORY_HISTORY",
            Cascade = "save-update", Lazy = CollectionLazy.True,
            Fetch = CollectionFetchMode.Subselect, Where = "DELETED_ID IS NULL")]
        [Key(1, Column = "USER_ACCOUNT_CODE"/*,
           ForeignKey = "FK_OPERATOR_OPERATOR_CATEGORY_HISTORY_CODE"*/)]
        [OneToMany(3, ClassType = typeof(OperatorCategoryHistoryData))]
        public virtual ISet CategoryHistoryList
        {
            get
            {
                return categoryHistoryList;
            }
            set
            {
                categoryHistoryList = value;
            }
        }

        [Bag(0, Name = "Devices", Table = "OPERATOR_DEVICE", Cascade = "save-update",
         Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect, Where = "DELETED_ID IS NULL")]
        [Key(1, Column = "OPERATOR_CODE"/*, ForeignKey = "FK_OPERATOR_DEVICE_OPERATOR_CODE"*/)]
        [OneToMany(2, ClassType = typeof(OperatorDeviceData))]
        public virtual IList Devices
        {
            get
            {
                return devices;
            }
            set
            {
                devices = value;
            }
        }

        #endregion

        [InitialData(PropertyName = "Name", PropertyValue = "UserAccountData")]
        public static readonly UserResourceData Resource;

        [InitialData(PropertyName = "Login", PropertyValue = "supervisor66666666666666666666666666666666666666666666666666")]
        public static readonly OperatorData InternalSupervisor;

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is OperatorData)
            {
                result = this.Login == ((OperatorData)obj).Login;
            }
            return result;
        }

        public override string ToString()
        {
            string ToReturn = this.LastName + ", " + this.FirstName;
            if (this.Role != null && this.Role.Name.Contains("Supervisor") == true)
                ToReturn = ToReturn + " (S)";

            return ToReturn;
        }
    }
}
