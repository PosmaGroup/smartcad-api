using System;
using System.Text;
using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(OperatorEvaluationQuestionAnswerData), Table = "EVALUATION_QUESTION_ANSWER", Lazy = false, Where = "DELETED_ID IS NULL")]
    public class OperatorEvaluationQuestionAnswerData : ObjectData
    {
        private OperatorEvaluationData operatorEvaluation;
        private string evaluationQuestion;
        private int answer;

        [Property(0, Unique = true)]
        [Column(1, Name = "ANSWER", NotNull = true)]
        public int Answer
        {
            get { return answer; }
            set { answer = value; }
        }

        [Property(Column = "EVALUATION_QUESTION")]
        public string EvaluationQuestion
        {
            get { return evaluationQuestion; }
            set { evaluationQuestion = value; }
        }

        [ManyToOne( ClassType = typeof(OperatorEvaluationData),
         ForeignKey = "FK_OPERATOR_EVALUATION_EVALUATION_QUESTION_ANSWER_CODE", Cascade = "none")]
        [Column(1, Name = "OPERATOR_EVALUATION_CODE")]
        public OperatorEvaluationData OperatorEvaluation
        {
            get { return operatorEvaluation; }
            set { operatorEvaluation = value; }
        }
    }
}
