using System;
using System.Text;
using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(OperatorTrainingCourseData), Table = "OPERATOR_TRAINING_COURSE", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_OPERATOR_TRAINING_COURSE")]
    public class OperatorTrainingCourseData : ObjectData
    {
        private OperatorData operatorData;
        private TrainingCourseScheduleData trainingCourseSchedule;

        [ManyToOne(0, ClassType = typeof(TrainingCourseScheduleData),
            ForeignKey = "FK_TRAINING_COURSE_SCHEDULE_OPERATOR_TRAINING_COURSE_CODE", Cascade = "none")]
        [Column(1, Name = "TRAINING_COURSE_SCHEDULE_CODE", NotNull = true,
          UniqueKey = "UK_OPERATOR_TRAINING_COURSE")]
        public TrainingCourseScheduleData TrainingCourseSchedule
        {
            get { return trainingCourseSchedule; }
            set { trainingCourseSchedule = value; }
        }

        [ManyToOne(0, ClassType = typeof(OperatorData),
            ForeignKey = "FK_OPERATOR_OPERATOR_TRAINING_COURSE_CODE", Cascade = "none")]
        [Column(1, Name = "OPERATOR_CODE", NotNull = true,
          UniqueKey = "UK_OPERATOR_TRAINING_COURSE")]
        public OperatorData Operator
        {
            get { return operatorData; }
            set { operatorData = value; }
        }
    }
}
