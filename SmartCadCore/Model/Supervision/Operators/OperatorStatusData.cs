using System;
using System.Text;
using NHibernate.Mapping.Attributes;
using ColorBase = System.Drawing.Color;
using System.Drawing;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(OperatorStatusData), Table = "OPERATOR_STATUS", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_OPERATOR_STATUS_NAME")]
    [AttributeIdentifier("DELETED_ID_2", Value = "UK_OPERATOR_STATUS_FRIENDLY_NAME")]
    [AttributeIdentifier("DELETED_ID_3", Value = "UK_OPERATOR_STATUS_CUSTOM_CODE")]
    public class OperatorStatusData : ObjectData, INamedObjectData, IImmutableObjectData
    {
        #region Enums

        /// <summary>
        /// Enumerates the properties that this class has.
        /// </summary>
        public enum Properties
        {
            Name,
            FriendlyName,
            CustomCode,
            Order,
            NotReady,
            Image,
            Porcentage,
            Tolerance,
            Color
        }

        #endregion

        #region Fields

        private string name;
        private string friendlyName;
        private string customCode;
        private int? order;
        private bool? notReady;
        private byte[] image;
        private bool? immutable;
        private int? porcentage;
        private int? tolerance;
        private Color color;
        private ComponentColorData componentColor;


        #endregion

        #region Constructors

        public OperatorStatusData()
        {
        }

        public OperatorStatusData(string name, string customCode)
        {
            this.name = name;
            this.customCode = customCode;
        }

        #endregion

        #region Properties
        
        [Property(0, Unique = true)]
        [Column(1, Name = "NAME", UniqueKey = "UK_OPERATOR_STATUS_NAME", NotNull = true)]
        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "FRIENDLY_NAME", UniqueKey = "UK_OPERATOR_STATUS_FRIENDLY_NAME", NotNull = true)]
        public virtual string FriendlyName
        {
            get
            {
                return friendlyName;
            }
            set
            {
                friendlyName = value;
            }
        }

        [Property(Column = "IMAGE", Type = "BinaryBlob", Length = int.MaxValue)]
        public virtual byte[] Image
        {
            get
            {
                return image;
            }
            set
            {
                image = value;
            }
        }
        
        [Property(0, Unique = true)]
        [Column(1, Name = "CUSTOM_CODE", UniqueKey = "UK_OPERATOR_STATUS_CUSTOM_CODE", NotNull = true)]
        public virtual string CustomCode
        {
            get
            {
                return customCode;
            }
            set
            {
                customCode = value;
            }
        }

        [Property(Column = "STATUS_ORDER", TypeType = typeof(int), NotNull = true)]
        public virtual int? Order
        {
            get
            {
                return order;
            }
            set
            {
                order = value;
            }
        }

        [Property(Column = "NOT_READY", TypeType = typeof(bool), NotNull = true)]
        public bool? NotReady
        {
            get
            {
                return notReady;
            }
            set
            {
                notReady = value;
            }
        }
       
        [Property(Column = "IMMUTABLE", NotNull = true, TypeType = typeof(bool))]
        public bool? Immutable
        {
            get
            {
                return immutable;
            }
            set
            {
                immutable = value;
            }
        }

        [Property(Column = "BREAK_PORCENTAGE", TypeType = typeof(int))]
        public int? Porcentage
        {
            get
            {
                return porcentage;
            }
            set
            {
                porcentage = value;
            }
        }

        [Property(Column = "BREAK_TOLERANCE", TypeType = typeof(int))]
        public int? Tolerance
        {
            get
            {
                return tolerance;
            }
            set
            {
                tolerance = value;
            }
        }

        public Color Color
        {
            get
            {
                return color;
            }
            set
            {
                color = value;
                ComponentColor = new ComponentColorData(value);
            }
        }

        [ComponentProperty(ComponentType = typeof(ComponentColorData))]
        public ComponentColorData ComponentColor
        {
            get
            {
                return componentColor;
            }
            set
            {
                componentColor = value;
                color = ColorBase.FromArgb(value.ColorA.Value, value.ColorR.Value, value.ColorG.Value, value.ColorB.Value);
            }
        }

        public bool IsPause()
        {
            return Porcentage > 0 && Tolerance >= 0;
        }

        #endregion

        #region InitialData

        [InitialData(PropertyName = "CustomCode", PropertyValue = "READY")]
        public static OperatorStatusData Ready;

        [InitialData(PropertyName = "CustomCode", PropertyValue = "BUSY")]
        public static OperatorStatusData Busy;

        [InitialData(PropertyName = "CustomCode", PropertyValue = "BATHROOM")]
        public static OperatorStatusData Bathroom;

        [InitialData(PropertyName = "CustomCode", PropertyValue = "TRAINING")]
        public static OperatorStatusData Training;

        [InitialData(PropertyName = "CustomCode", PropertyValue = "REST")]
        public static OperatorStatusData Rest;

        [InitialData(PropertyName = "CustomCode", PropertyValue = "REUNION")]
        public static OperatorStatusData Reunion;

        [InitialData(PropertyName = "CustomCode", PropertyValue = "ABSENT")]
        public static OperatorStatusData Absent;

        [InitialData(PropertyName = "Name", PropertyValue = "OperatorStatusData")]
        public static readonly UserResourceData Resource;
        
        #endregion

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is OperatorStatusData)
            {
                result = this.Name == ((OperatorStatusData)obj).Name;
            }
            return result;
        }

        public override string ToString()
        {
            string text = "";

            if (this.Name != null)
            {
                text = this.Name;
            }

            return text;
        }
    }
}
