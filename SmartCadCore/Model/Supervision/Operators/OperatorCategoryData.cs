using System;
using System.Text;
using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(OperatorCategoryData), Table = "OPERATOR_CATEGORY", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_OPERATOR_CATEGORY_LEVEL")]
    [AttributeIdentifier("DELETED_ID_2", Value = "UK_OPERATOR_CATEGORY_NAME")]
    [AttributeIdentifier("DELETED_ID_3", Value = "UK_OPERATOR_CATEGORY_FRIENDLY_NAME")]
    public class OperatorCategoryData : ObjectData, INamedObjectData
    {
        #region Enums

        /// <summary>
        /// Enumerates the properties that this class has.
        /// </summary>
        public enum Properties
        {
            Level,
            Name,
            FriendlyName,
            Description
        }

        #endregion

        #region Fields

        private int? level;
        private string name;
        private string friendlyName;
        private string description;

        #endregion

        #region Constructors

        public OperatorCategoryData()
        {
        }

        public OperatorCategoryData(int level, string name)
        {
            this.level = level;
            this.name = name;
        }

        #endregion

        #region Properties

        [Property(0, Unique = true)]
        [Column(1, Name = "LEVEL", UniqueKey = "UK_OPERATOR_CATEGORY_LEVEL", NotNull = true)]
        public virtual int? Level
        {
            get
            {
                return level;
            }
            set
            {
                level = value;
            }
        }

        [Property(0, Unique = true)]
        [Column(2, Name = "NAME", UniqueKey = "UK_OPERATOR_CATEGORY_NAME", NotNull = true)]
        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        [Property(0, Unique = true)]
        [Column(3, Name = "FRIENDLY_NAME", UniqueKey = "UK_OPERATOR_CATEGORY_FRIENDLY_NAME" , NotNull = false)]
        public virtual string FriendlyName
        {
            get
            {
                return friendlyName;
            }
            set
            {
                friendlyName = value;
            }
        }

        [Property(0, Unique = false)]
        [Column(4, Name = "DESCRIPTION", NotNull = false)]
        public virtual string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }

        #endregion

        public override string ToString()
        {
            string text = "";

            if (this.friendlyName != null)
            {
                text = this.friendlyName;
            }

            return text;
        }

        #region InitialData

        [InitialData(PropertyName = "Name", PropertyValue = "OnTraining")]
        public static OperatorCategoryData OnTraining;

        #endregion
    }
}
