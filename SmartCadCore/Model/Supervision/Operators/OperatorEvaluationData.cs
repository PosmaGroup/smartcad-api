using System;
using System.Text;
using NHibernate.Mapping.Attributes;
using System.Collections;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(OperatorEvaluationData), Table = "OPERATOR_EVALUATION", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_OPERATOR_EVALUATION")]
    public class OperatorEvaluationData : ObjectData
    {
        private int qualification;
        private DateTime date;
        private OperatorData evaluator;
        private OperatorData operatorData;
        private IList questionAnswers;
        private string evaluationName;
        private EvaluationData.EvaluationScales scale;
        private string operatorCategory;
        private string role;

        [Bag(0, Name = "QuestionAnswers", Table = "EVALUATION_QUESTION_ANSWER", Cascade = "save-update",
           Lazy = CollectionLazy.True, Inverse = true, Fetch = CollectionFetchMode.Subselect, Where = "DELETED_ID IS NULL")]
        [Key(1, Column = "OPERATOR_EVALUATION_CODE"/*, ForeignKey = "FK_OPERATOR_EVALUATION_EVALUATION_QUESTION_ANSWER_CODE"*/)]
        [OneToMany(2, ClassType = typeof(OperatorEvaluationQuestionAnswerData))]
        public IList QuestionAnswers
        {
            get { return questionAnswers; }
            set { questionAnswers = value; }
        }

        [Property(Column = "EVALUATION_NAME")]
        public string EvaluationName 
        {
            get 
            {
                return evaluationName;
            }
            set 
            {
                evaluationName = value;
            }
        }

        

        [Property(Column = "ROLE_NAME")]
        public string RoleOperator
        {
            get { return role; }
            set { role = value; }
        }

        [Property(Column = "CATEGORY_NAME")]
        public string OperatorCategory
        {
            get { return operatorCategory; }
            set { operatorCategory = value; }
        }
	

        [Property(Column = "SCALE")]
        public EvaluationData.EvaluationScales Scale 
        {
            get 
            {
                return scale;
            }
            set 
            {
                scale = value;
            }
        }

        [ManyToOne(0, ClassType = typeof(OperatorData),
            ForeignKey = "FK_OPERATOR_EVALUATION_OPERATOR_CODE", Cascade = "none")]
        [Column(1, Name = "OPERATOR_CODE", NotNull = true,
           UniqueKey = "UK_OPERATOR_EVALUATION")]
        public OperatorData Operator
        {
            get { return operatorData; }
            set { operatorData = value; }
        }

        [ManyToOne(0, ClassType = typeof(OperatorData),
            ForeignKey = "FK_OPERATOR_EVALUATION_EVALUATOR_CODE", Cascade = "none")]
        [Column(1, Name = "EVALUATOR_CODE", NotNull = true,
           UniqueKey = "UK_OPERATOR_EVALUATION")]
        public OperatorData Evaluator
        {
            get { return evaluator; }
            set { evaluator = value; }
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "DATE", UniqueKey = "UK_OPERATOR_EVALUATION", NotNull = true)]
        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        [Property(0)]
        [Column(1, Name = "QUALIFICATION", NotNull = true)]
        public int Qualification
        {
            get { return qualification; }
            set { qualification = value; }
        }
    }
}
