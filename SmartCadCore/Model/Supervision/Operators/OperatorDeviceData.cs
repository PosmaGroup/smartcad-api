﻿using NHibernate.Mapping.Attributes;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(OperatorDeviceData), Table = "OPERATOR_DEVICE", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("UK_START", Value = "UK_OPERATOR_DEVICE")]
    [AttributeIdentifier("UK_END", Value = "UK_OPERATOR_DEVICE")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_OPERATOR_DEVICE")]
    [AttributeIdentifier("IX_START", Value = "IX_OPERATOR_DEVICE_START")]
    public class OperatorDeviceData : BaseSessionHistory
    {
        #region Fields
        private OperatorData user;
        private DeviceData device;
        #endregion

        #region Properties

        [ManyToOne(ClassType = typeof(OperatorData),
            Cascade = "none", ForeignKey = "FK_OPERATOR_DEVICE_OPERATOR_CODE")]
        [Column(1, Name = "OPERATOR_CODE", UniqueKey = "UK_OPERATOR_DEVICE")]
        public virtual OperatorData User
        {
            get
            {
                return user;
            }
            set
            {
                user = value;
            }
        }

        [ManyToOne(ClassType = typeof(DeviceData),
          Cascade = "none", ForeignKey = "FK_OPERATOR_DEVICE_DEVICE_CODE")]
        [Column(1, Name = "DEVICE_CODE", UniqueKey = "UK_OPERATOR_DEVICE")]
        public virtual DeviceData Device
        {
            get
            {
                return device;
            }
            set
            {
                device = value;
            }
        }
        #endregion

        public override bool Equals(object obj)
        {
            try
            {
                if (obj.GetType() == typeof(OperatorDeviceData))
                {
                    OperatorDeviceData operDevice = (OperatorDeviceData)obj;

                    if ((this.User == null || operDevice.User != null && operDevice.User.Code == this.User.Code) &&
                        (this.Device == null || operDevice.Device.Code == this.Device.Code) &&
                        (this.Start == null || operDevice.Start == this.Start) &&
                        (this.End == null || operDevice.End == this.End))
                    {
                        return true;
                    }
                }
            }
            catch { }
            return false;
        }

        public override string ToString()
        {
            return this.device.Name;
        }
    }
}
