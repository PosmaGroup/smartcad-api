using System;
using System.Text;
using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(TrainingCourseSchedulePartsData), Table = "TRAINING_COURSE_SCHEDULE_PARTS", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("IX_START", Value = "IX_TRAINING_COURSE_SCHEDULE_PARTS")]
    [AttributeIdentifier("UK_START", Value = "")]
    [AttributeIdentifier("UK_END", Value = "")]
    public class TrainingCourseSchedulePartsData : BaseSessionHistory
    {
        private string room;
        private TrainingCourseScheduleData trainingCourseSchedule;

        [ManyToOne(0, ClassType = typeof(TrainingCourseScheduleData),
           ForeignKey = "FK_TRAINING_COURSE_SCHEDULE_TRAINING_COURSE_SCHEDULE_PARTS_CODE", Cascade = "none")]
        [Column(1, Name = "TRAINING_COURSE_SCHEDULE_CODE", NotNull = true)]
        public TrainingCourseScheduleData TrainingCourseSchedule
        {
            get { return trainingCourseSchedule; }
            set { trainingCourseSchedule = value; }
        }

        [Property(0)]
        [Column(1, Name = "ROOM", NotNull = true)]
        public string Room
        {
            get { return room; }
            set { room = value; }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is TrainingCourseSchedulePartsData)
            {
                if (this.Code == ((TrainingCourseSchedulePartsData)obj).Code)
                    result = true;
            }
            return result;
        }
    }
}
