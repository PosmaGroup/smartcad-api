using System;
using System.Text;
using NHibernate.Mapping.Attributes;
using System.Collections;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(SupervisorCloseReportData), Table = "CLOSE_REPORT", Lazy = false, Where = "DELETED_ID IS NULL")]
    public class SupervisorCloseReportData : ObjectData
    {
        private SessionHistoryData session;
        private IList messages;
        private IList indicators;
        private bool finished = false;

        [Bag(0, Name = "Messages", Table = "CLOSE_REPORT_MESSAGE", Cascade = "save-update",
            Lazy = CollectionLazy.True, Inverse = true, Fetch = CollectionFetchMode.Subselect)]
        [Key(1, Column = "CLOSE_REPORT_CODE"/*, ForeignKey = "FK_CLOSE_REPORT_CLOSE_REPORT_MESSAGE_CODE"*/)]
        [OneToMany(2, ClassType = typeof(SupervisorCloseReportMessageData))]
        public IList Messages
        {
            get { return messages; }
            set { messages = value; }
        }

        /// <summary>
        /// Relationship is Many to Many because it is an history, this is not to be updated or deleted.
        /// </summary>
        [Bag(0, Name = "Indicators", Table = "CLOSE_REPORT_INDICATOR", Cascade = "none",
            Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect)]
        [Key(1, Column = "CLOSE_REPORT_CODE", ForeignKey = "FK_CLOSE_REPORT_INDICATOR_CODE")]
        [ManyToMany(2, ClassType = typeof(IndicatorClassDashboardData), Column = "INDICATOR_CODE", ForeignKey = "FK_INDICATOR_CLOSE_REPORT_CODE")]
        public IList Indicators
        {
            get { return indicators; }
            set { indicators = value; }
        }

        [ManyToOne(0, ClassType = typeof(SessionHistoryData), ForeignKey = "FK_CLOSE_REPORT_SESSION_HISTORY_CODE", Cascade = "none")]
        [Column(1, Name = "SESSION_HISTORY_CODE", NotNull = true)]
        public SessionHistoryData Session
        {
            get { return session; }
            set { session = value; }
        }

        [Property(0, TypeType = typeof(bool))]
        [Column(1, Name = "FINISHED")]
        public bool Finished
        {
            get { return finished; }
            set { finished = value; }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is SupervisorCloseReportData)
            {
                result = this.Session == ((SupervisorCloseReportData)obj).Session;
            }
            return result;
        }
    }
}
