﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Resources;
using System.Threading;
using SmartCadCore.Common;

namespace SmartCadCore.Core
{
	/// <summary>
	/// Loads application resources for intenacionalization and customization.
	/// </summary>
	/// <remarks>
	/// TODO: Esta clase hay que mejorarla. Los metodos estaticos deben invocarse de una instancia estatica de la clase
	/// para que asi se puedan crear instancias que puedan usar archivos de recursos distintos.
	/// </remarks>
	public class ResourceMapLoader
	{
		/// <summary>
		/// Holds the resource manager that will be used in order to access the SmartBank resource file.
		/// </summary>
		private static ResourceManager rm;

		/// <summary>
		/// Creates a ClientResourceLoader object
		/// </summary>
		public ResourceMapLoader()
		{
		}

		/// <summary>
		/// Creates a ClientResourceLoader and instantiates a ResourceManager assigning it to the rm field.
		/// </summary>
        static ResourceMapLoader()
		{
            try
            {
                CultureInfo ci = new CultureInfo(Win32.GetUserDefaultLCID());
                Thread.CurrentThread.CurrentUICulture = ci;
                if (!ci.IsNeutralCulture)
                {
                    Thread.CurrentThread.CurrentCulture = ci;
                }
                rm = new ResourceManager("SmartCadCore.MapResources.Resources", Assembly.Load("SmartCadCore.MapResources"));
                rm.IgnoreCase = true;
            }
            catch
            {
            }
		}


		/// <summary>
		/// Retrieves a string resource.
		/// </summary>
		/// <param name="label">Resource name</param>
		/// <returns>A string resource value</returns>
		public static string GetString(string label)
		{
			string res="";						
			if (rm != null)
			{
				try
				{
					if (label!=null && label!="")
					{
						res=rm.GetString(label);
						if (res!=null && res.StartsWith("$"))
							res=rm.GetString(res);						
					}
				}
				catch (Exception ex)
				{
					throw ex;
				}
			}
			return res;
		}

		/// <summary>
		/// Retrieves a string resource, and replace part of it with args
		/// </summary>
		/// <param name="label">Resource name</param>
		/// <param name="args">Objects that would be place in the string</param>
		/// <returns>A string resource value</returns>
		public static string GetString(string label, params object[] args)
		{
			StringWriter writer = new StringWriter();
			writer.Write(GetString(label) == null ? "" : GetString(label), args);
			return writer.ToString();
		}

		/// <summary>
		/// Retrieve an object resource. the resource type doesn't matter.
		/// </summary>
		/// <param name="label">Resource name</param>
		/// <returns>A object resource value</returns>
		public static object GetObject(string label)
		{
			object res = null;
			if (rm != null)
			{
				try
				{
					string index = rm.GetString(label);
					res = rm.GetObject(index);
				}
				catch (Exception ex)
				{
					//throw new SBException(ex.Message);   
					throw ex;
				}
			}
			return res;
		}

		/// <summary>
		/// Sets the case sensitivity to true or false
		/// </summary>
		/// <param name="flag">Case sensititivity flag value</param>
		public static void SetIgnoreCase(bool flag)
		{
			if (rm != null)
			{
				try
				{
					rm.IgnoreCase = flag;
				}
				catch (Exception ex)
				{
					//throw new SBException(ex.Message);   
					throw ex;
				}
			}
		}

		/// <summary>
		/// method GetImage used to retrieve images required for the application
		/// </summary>
		/// <param name="index">string</param>
		/// <returns>Image</returns>
		public static Image GetImage(string index)
		{
			Image image = null;
			if (rm != null)
			{
				try
				{
					string imgPath = GetString(index);
					if (imgPath != null && imgPath != "")
					{
                        image = Image.FromStream(Assembly.Load("SmartCadCore.MapResources").GetManifestResourceStream(imgPath));
					}
				}
				catch
				{
				}
			}
			return image;
		}

		/// <summary>
		/// this method retrieve the icon specified in index
		/// </summary>
		/// <param name="index">string</param>
		/// <returns>Icon</returns>
		public static Icon GetIcon(string index)
		{
			Icon icon = null;
			if (rm != null)
			{
				try
				{
					string iconName = GetString(index);
					if (iconName != null && iconName != "")
					{
                        icon = new Icon(Assembly.Load("SmartCadCore.MapResources").GetManifestResourceStream(iconName));
					}
				}
				catch
				{
				}
			}
			return icon;
		}

		public static string GetString2(string pseudoKey)
		{
			string s = GetString(pseudoKey);

			if (s == null || s == "")
				return pseudoKey;
			else
				return s;
		}

		public static string GetString2(string pseudoKey, params string[] args)
		{
			string s = GetString2(pseudoKey);
			
			StringWriter writer = new StringWriter();
			writer.Write(s, args);
			return writer.ToString();
		}

        public static byte[] GetByteArrayFromIcon(string index)
        {
            Icon icon = GetIcon(index);
            byte[] result = null;
            MemoryStream stream = new MemoryStream();
            icon.Save(stream);
            result = stream.GetBuffer();
            return result;
        }

        public static byte[] GetByteArrayFromImage(string index)
        {
            Image image = GetImage(index);
            byte[] result = null;
            MemoryStream stream = new MemoryStream();
            image.Save(stream, image.RawFormat);
            result = stream.GetBuffer();
            return result;
        }

	}
}
