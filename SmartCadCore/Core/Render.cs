using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Xml.Serialization;
using System.IO;

namespace SmartCadCore.Core
{
    [XmlRoot("render")]
    public class Render
    {
        ArrayList groups = new ArrayList();

        [XmlElement("group", typeof(Group))]
        public ArrayList Groups
        {
            get
            {
                return groups;
            }
            set
            {
                groups = value;
            }
        }


        public static Render FromString(string p)
        {
            Render retVal = new Render();
            XmlSerializer ser = new XmlSerializer(typeof(Render));
            try
            {
                if (p != null)
                {
                    retVal = ser.Deserialize(new StringReader(p.Trim())) as Render;
                }
            }
            catch { }
            return retVal;
        }

        public static string FromRender(Render render)
        {
            XmlSerializer serializer = new XmlSerializer(render.GetType());
            StringWriter sw = new StringWriter();
            try
            {
                serializer.Serialize(sw, render);
            }
            catch
            { }
            return sw.GetStringBuilder().ToString();
        }
    }

    public class GenericControl
    {
        public const string GUITYPE_TEXTBOX = "freeText";
        public const string GUITYPE_RADIOBUTTON = "radioButton";
        public const string GUITYPE_CHECKBOX = "checkBox";
        public const string GUITYPE_TEXT = "text";
        public const string GUITYPE_PICTUREBOX = "pictureBox";

        private string name, id, width, height, x, y, image;

        private string possibleAnswerId, guitype;

        private string handler;

        private string text;
        private string toolTip;

        [XmlAttribute("type")]
        public string GuiType
        {
            get
            {
                return guitype;

            }
            set
            {
                guitype = value;
            }
        }
        private bool multiLine = false;
        [XmlAttribute("multiline")]
        public bool MultiLine
        {
            get
            {
                return multiLine;

            }
            set
            {
                multiLine= value;
            }
        }

        [XmlAttribute("possibleAnswerId")]
        public string PossibleAnswerId
        {
            get
            {
                return possibleAnswerId;
            }
            set
            {
                possibleAnswerId = value;
            }
        }


        [XmlAttribute("name")]
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        [XmlAttribute("id")]
        public string Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }

        [XmlAttribute("width")]
        public string Width
        {
            get
            {
                return width;
            }
            set
            {
                width = value;
            }
        }

        [XmlAttribute("height")]
        public string Height
        {
            get
            {
                return height;
            }
            set
            {
                height = value;
            }
        }

        [XmlAttribute("x")]
        public string X
        {
            get
            {
                return x;
            }
            set
            {
                x = value;
            }
        }

        [XmlAttribute("y")]
        public string Y
        {
            get
            {
                return y;
            }
            set
            {
                y = value;
            }
        }

        [XmlAttribute("handler")]
        public string Handler
        {
            get
            {
                return handler;

            }
            set
            {
                handler = value;
            }
        }

        [XmlAttribute("image")]
        public string Image
        {
            get
            {
                return image;
            }

            set
            {
                image = value;
            }
        }

        [XmlAttribute("text")]
        public string Text
        {
            get
            {
                return text;

            }
            set
            {
                text = value;
            }
        }

        [XmlAttribute("toolTip")]
        public string ToolTip
        {
            get
            {
                return toolTip;

            }
            set
            {
                toolTip = value;
            }
        }
    }

    [XmlRoot("guiElement")]
    public class GuiElement
    {
        private int order;
        private ArrayList groups = new ArrayList();

        [XmlAttribute("order")]
        public int Order
        {
            get
            {
                return order;
            }
            set
            {
                order = value;
            }
        }


    }

    [XmlRoot("group")]
    public class Group
    {
        private ArrayList element;

        [XmlElement("element", typeof(Element))]
        public ArrayList Element
        {
            get
            {
                return element;

            }
            set
            {
                element = value;
            }
        }
    }



    [XmlRoot("element")]
    public class Element : GenericControl
    {

    }

    public class ElementRoom : GenericControl 
    {
        int code;

        [XmlAttribute("code")]
        public int Code
        {
            get
            {
                return code;
            }
            set
            {
                code = value;
            }
        }
    }
}
