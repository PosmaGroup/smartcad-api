﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smartmatic.SmartCad.VideoAnalitica;
using Smartmatic.SmartCad.VideoAnalitica.Objects;
using Smartmatic.SmartCad.VideoAnalitica.AgentVI;
using System.Threading;
using SmartCadCore.Model;
using System.Collections.Generic;
using Iesi.Collections;

namespace SmartCadCore.Core
{
    public class SmartCadVideoAnalitica
    {
        private static VideoAnaliticaManagerAbstract videoAnaliticaManager = null;

        private static System.Threading.Timer timer;
        private static void Init(VideoAnaliticaCommunicationElement videoAnaliticaElements)
        {
            videoAnaliticaManager = new AgentViManager(videoAnaliticaElements.ServerIP, videoAnaliticaElements.ServerPort, videoAnaliticaElements.ServerUser, videoAnaliticaElements.ServerPassword);
        }

        private static bool existRule(IList<VARuleData> ruleData, Rule rule)
        {
            foreach (VARuleData vaRuleData in ruleData)
            {
                if (vaRuleData.SaviCode == rule.GUID)
                {
                    return true;
                }
            }
            return false;
        }
        private static VARuleData[] findRules()
        {
          
            ArrayList rulesData = new ArrayList(SmartCadDatabase.SearchObjects(new VARuleData()));
           
            VARuleData[] answer = new VARuleData[rulesData.Count];
            rulesData.CopyTo(answer);
            
            
            return answer;
            
        }

        private static VARuleData findRule(VARuleData[] vaRuleData, uint ruleID){
            Rule rule = videoAnaliticaManager.getRule(ruleID);
            foreach (VARuleData vaRule in vaRuleData)
            {
                if (rule.GUID == vaRule.SaviCode)
                {
                    return vaRule;
                }
            }
            return null;
        }

        private static VAAlertCamData[] findAlertsByRules(VARuleData[] rules)
        {
            IList<VAAlertCamData> vaAlertsCamData = new List<VAAlertCamData>();
            try
            {
                IList<string> rulesGuid = new List<string>();
                foreach (VARuleData rule in rules)
                {
                    rulesGuid.Add(rule.SaviCode);
                }
                Alert[] alerts = videoAnaliticaManager.getAlertsByRule(rulesGuid.ToArray());
                
                foreach (Alert alert in alerts)
                {
                    VideoSensor videoSensor = videoAnaliticaManager.getVideoSensor(alert.sensorId);

                    CameraData cameraDB = (CameraData)SmartCadDatabase.SearchObjectData(SmartCadHqls.GetCustomHql(
                                            SmartCadHqls.GetCamerasByCustomId, videoSensor.VMS_ID));

                    VAAlertCamData vaAlert = new VAAlertCamData();
                    vaAlert.Camera = cameraDB;
                    vaAlert.Camera.CustomId = videoSensor.VMS_ID;
                    vaAlert.AlertDate = alert.date;
                    vaAlert.Rule = findRule(rules, alert.ruleId);
                    vaAlert.ExternalId = (int)alert.id;
                    vaAlertsCamData.Add(vaAlert);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return vaAlertsCamData.ToArray();
        }

        private static VAAlertVideoData findVideo(VAAlertCamData alert)
        {
            VAAlertVideoData videoSave = new VAAlertVideoData();
            try
            {
                EventVideo eventVideo = videoAnaliticaManager.getVideoByEvent((uint)alert.ExternalId);                
                videoSave.ObjectId = eventVideo.ObjectId;
                videoSave.EventTimeValue = eventVideo.EventTimeValue;
                videoSave.EventTimeMilliseconds = eventVideo.EventTimeMilliseconds;
                videoSave.BirthTimeValue = eventVideo.BirthTimeValue;
                videoSave.BirtTimeMilliseconds = eventVideo.DeathTimeMilliseconds;
                videoSave.DeathTimeValue = eventVideo.DeathTimeValue;
                videoSave.DeathTimeMilliseconds = eventVideo.DeathTimeMilliseconds;
                videoSave.TotalObjectInstances = eventVideo.TotalObjectInstances;
                videoSave.VideoInstances = new HashedSet();
                if (eventVideo.VideoInstances != null)
                {
                    foreach (VideoInstance videoInstance in eventVideo.VideoInstances)
                    {
                        VAAlertVideoInstanceData alertVideoInstance = new VAAlertVideoInstanceData();
                        alertVideoInstance.ObjectId = videoInstance.ObjectId;
                        alertVideoInstance.TypeObject = (VAAlertVideoInstanceData.TypeObjectEnum)videoInstance.TypeObject;
                        alertVideoInstance.TimestampValue = videoInstance.TimestampValue;
                        alertVideoInstance.TimestampMilliseconds = videoInstance.TimestampMilliseconds;
                        alertVideoInstance.BoundingBoxLeft = videoInstance.BoundingBoxLeft;
                        alertVideoInstance.BoundingBoxTop = videoInstance.BoundingBoxTop;
                        alertVideoInstance.BoundingBoxRight = videoInstance.BoundingBoxRight;
                        alertVideoInstance.BoundingBoxRight = videoInstance.BoundingBoxRight;
                        alertVideoInstance.FootX = videoInstance.FootX;
                        alertVideoInstance.FootY = videoInstance.FootY;
                        alertVideoInstance.Alert = alert;
                        videoSave.VideoInstances.Add(alertVideoInstance);
                    }
                   
                } 
            }
            catch (Exception e)
            {
                Console.WriteLine(alert.ExternalId);
                Console.WriteLine(e.Message);
            }
            return videoSave;
        }

        private static void saveAlerts(VAAlertCamData[] alerts)
        {
           SmartCadDatabase.SaveObjectList<VAAlertCamData>(alerts);
           
        }
        private static void deleteAlerts()
        {
            videoAnaliticaManager.deleteAlerts();
        }


        private static void ActualizarAlertas(object state)
        {
            try
            {
                VARuleData[] rules = findRules();
                VAAlertCamData[] alerts = findAlertsByRules(rules);
                IList<VAAlertVideoData> videos = new List<VAAlertVideoData>();
                 foreach (VAAlertCamData alert in alerts)
                 {
                     alert.VideoAlert = findVideo(alert);                
                 }
                 if (alerts.Length > 0)
                 {
                     saveAlerts(alerts);
                     deleteAlerts();
                 }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
           
            
        }
      
        public static void Start()
        {
           Init(SmartCadConfiguration.VideoAnaliticaProperties);


           var autoEvent = new AutoResetEvent(false);
           timer  = new System.Threading.Timer(new TimerCallback(ActualizarAlertas), autoEvent, 0, 10000);
           GC.KeepAlive(timer);
        
        //   ActualizarAlertas(null);

        }
    }
}
