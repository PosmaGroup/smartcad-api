using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;

namespace SmartCadCore.Core.Validator
{
    class OperatorCameraValidator : ObjectDataValidator<OperatorDeviceData>
    {
        public override void AfterDelete(NHibernate.ISession session, OperatorDeviceData dataObject)
        {
            if (dataObject.User.DeletedId != null)
            {
                dataObject.DeletedId = dataObject.User.DeletedId;
            }
            else
            {
                dataObject.DeletedId = Guid.NewGuid();
            }
        }
    }
}

                    