using System;
using System.Collections;
using System.Text;

using SmartCadCore.Model;
using Iesi.Collections;

namespace SmartCadCore.Core.Validator
{
    public class SupportRequestReportDataValidator: ObjectDataValidator<SupportRequestReportData>
    {
        private void AddStatusHistory(IncidentNotificationData dataObject, OperatorData oper)
        {
            IncidentNotificationStatusHistoryData temp = new IncidentNotificationStatusHistoryData();
            temp.Start = SmartCadDatabase.GetTimeFromBD();
            temp.Status = dataObject.Status;
            temp.UserAccount = oper;
            temp.IncidentNotification = dataObject;
            dataObject.StatusHistoryList = new ArrayList();
            dataObject.StatusHistoryList.Add(temp);
        } 

        public override void BeforeSave(NHibernate.ISession session, SupportRequestReportData dataObject)
        {
            dataObject.IncidentNotifications = new ArrayList();
            DateTime datetime = SmartCadDatabase.GetTimeFromBD();
            dataObject.CreationDate = datetime;
            dataObject.MultipleOrganisms = true;
            dataObject.CustomCode = Guid.NewGuid().ToString();
            IncidentData incident = SmartCadDatabase.SearchObject<IncidentData>(dataObject.Incident);

            foreach (ReportBaseDepartmentTypeData departmentType in dataObject.ReportBaseDepartmentTypes)
            {
                if (ServiceUtil.DuplicatedNotification(departmentType, incident) == false)
                {
                    IncidentNotificationData notification = new IncidentNotificationData();
                    notification.Comment = string.Empty;
                    notification.CustomCode = Guid.NewGuid().ToString();
                    notification.FriendlyCustomCode = ServiceUtil.GenerateNotificationCustomCode(departmentType.DepartmentType, dataObject.Operator.Code);
                    notification.DepartmentType = departmentType.DepartmentType;
                    notification.ReportBase = dataObject;
                    notification.Priority = departmentType.Priority;
                    notification.CreationDate = datetime;
                    notification.Status = IncidentNotificationStatusData.New;
                    notification.SetDispatchOrders = new HashedSet();
                    AddStatusHistory(notification, dataObject.Operator);
                    dataObject.IncidentNotifications.Add(notification);
                }
            }
            UpdateAllIncidentNotifications(session, dataObject.Incident);
        }

        private void UpdateAllIncidentNotifications(NHibernate.ISession session, IncidentData incident)
        {
            SmartCadDatabase.InitializeLazy(incident, incident.SetReportBaseList);
            foreach (ReportBaseData reportBase in incident.SetReportBaseList)
            {
                CheckNewInformation(session, reportBase);
            }
        }

        private void CheckNewInformation(NHibernate.ISession session, ReportBaseData phoneReport)
        {
            SmartCadDatabase.InitializeLazy(phoneReport, phoneReport.IncidentNotifications);
            foreach (IncidentNotificationData incidentNotification in phoneReport.IncidentNotifications)
            {
                if (incidentNotification.Status != IncidentNotificationStatusData.Cancelled &&
                    incidentNotification.Status != IncidentNotificationStatusData.Closed &&
                    incidentNotification.Status != IncidentNotificationStatusData.AutomaticSupervisor &&
                    incidentNotification.Status != IncidentNotificationStatusData.ManualSupervisor &&
                    incidentNotification.Status != IncidentNotificationStatusData.New)
                {
                    incidentNotification.Status = IncidentNotificationStatusData.Updated;
                    SmartCadDatabase.UpdateObject(incidentNotification);
                }
            }
        }

        private bool Contains(IList deptTypeList, ReportBaseDepartmentTypeData deptType)
        {
            bool result = true;
            for (int i = 0; i < deptTypeList.Count && result != true; i++)
            {
                ReportBaseDepartmentTypeData deptTypeTemp = deptTypeList[i] as ReportBaseDepartmentTypeData;
                if (deptTypeTemp.DepartmentType.Name == deptType.DepartmentType.Name)
                {
                    result = false;
                }
            }
            return result;
        }
    }
}
