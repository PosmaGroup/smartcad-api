using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using System.Collections;
using System.ServiceModel;
using SmartCadCore.Common;

namespace SmartCadCore.Core.Validator
{
    public class UserProfileDataValidator : ObjectDataValidator<UserProfileData>
    {
        public override void BeforeDelete(NHibernate.ISession session, UserProfileData dataObject)
        {
            try
            {
                dataObject = SmartCadDatabase.RefreshObject(session, dataObject) as UserProfileData;
            }
            catch (Exception)
            {
                throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2("DeleteUserProfileData"),dataObject.FriendlyName));
            }

            if ((bool)dataObject.Immutable)
                throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2("ImmutableUserProfileDelete"), dataObject.FriendlyName));


            string queryHQL = SmartCadHqls.GetCustomHql(SmartCadHqls.CountUserRolesByUserProfile, dataObject.Code);
                
            ArrayList objectList = (ArrayList)SmartCadDatabase.SearchObjects(session, queryHQL);
            if (objectList.Count > 0)
            {
                long count = (long)objectList[0];
                if(count > 0)
                    throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString("FK_USER_ROLE_USER_PROFILE_CODE"),dataObject.FriendlyName));
            }
            dataObject.DeletedId = Guid.NewGuid();
        }

        public override void AfterDelete(NHibernate.ISession session, UserProfileData dataObject)
        {
            
        }
    }
}
