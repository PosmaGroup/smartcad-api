using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using SmartCadCore.Common;

namespace SmartCadCore.Core.Validator
{
    public class TrainingCourseScheduleValidator : ObjectDataValidator<TrainingCourseScheduleData>
    {
        public override void AfterLoad(NHibernate.ISession session, System.Collections.IList dataObject)
        {
            foreach (TrainingCourseScheduleData schedule in dataObject)
            {
                GetRealDates(session, schedule);
            }
        }

        public override void AfterSave(NHibernate.ISession session, TrainingCourseScheduleData dataObject)
        {
            GetRealDates(session, dataObject);
        }

        public override void BeforeUpdate(NHibernate.ISession session, TrainingCourseScheduleData dataObject)
        {
            if (dataObject.RealEnd > SmartCadDatabase.GetTimeFromBD())
            {
                long count = (long)SmartCadDatabase.SearchObject(
                    session,
                    SmartCadHqls.GetCustomHql(
                    SmartCadHqls.GetCountTrainingCourseScheduleEvaluatingorEvaluated,
                    dataObject.Code,
                    ApplicationUtil.GetDataBaseFormattedDate(
                    SmartCadDatabase.GetTimeFromBD())));
                //     if (count > 0 && dataObject.Start < SmartCadDatabase.GetTimeFromBD())
                //        throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("TrainingCourseScheduleCanNotBeUpdated", dataObject.Name));

                if (dataObject.Start > SmartCadDatabase.GetTimeFromBD())
                {
                    TrainingCourseScheduleData dataObjectAux = new TrainingCourseScheduleData();
                    dataObjectAux.Code = dataObject.Code;
                    dataObjectAux = (TrainingCourseScheduleData)SmartCadDatabase.RefreshObject(dataObjectAux);

                    if (SmartCadDatabase.IsInitialize(dataObjectAux.Parts) == false)
                        SmartCadDatabase.InitializeLazy(dataObjectAux, dataObjectAux.Parts);

                    foreach (TrainingCourseSchedulePartsData part in dataObjectAux.Parts)
                    {
                        if (dataObject.Parts.Contains(part) == false)
                            SmartCadDatabase.DeleteObject(session, part, false);
                    }
                }
            }
        }

        public override void BeforeDelete(NHibernate.ISession session, TrainingCourseScheduleData dataObject)
        {
            long count = (long)SmartCadDatabase.SearchObject(
                session,
                SmartCadHqls.GetCustomHql(
                SmartCadHqls.GetCountTrainingCourseScheduleEvaluatingorEvaluated,
                dataObject.Code,
                ApplicationUtil.GetDataBaseFormattedDate(
                SmartCadDatabase.GetTimeFromBD())));
            if (count > 0 && dataObject.RealStart < SmartCadDatabase.GetTimeFromBD())
                throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("TrainingCourseScheduleCanNotBeDeleted", dataObject.Name));

            if (SmartCadDatabase.IsInitialize(dataObject.Parts) == false)
                SmartCadDatabase.InitializeLazy(dataObject, dataObject.Parts);
            foreach (TrainingCourseSchedulePartsData part in dataObject.Parts)
            {
                SmartCadDatabase.DeleteObject(session, part, false);
            }
        }

        private void GetRealDates(NHibernate.ISession session, TrainingCourseScheduleData schedule)
        {
            object realStart;
            object realEnd;

            realStart = SmartCadDatabase.SearchObject(session, SmartCadHqls.GetCustomHql(SmartCadHqls.GetRealStartDateTrainingCourseSchedule, schedule.Code));

            if (realStart != null)
                schedule.RealStart = (DateTime)realStart;

            realEnd = SmartCadDatabase.SearchObject(session, SmartCadHqls.GetCustomHql(SmartCadHqls.GetRealEndDateTrainingCourseSchedule, schedule.Code));

            if (realEnd != null)
                schedule.RealEnd = (DateTime)realEnd;
        }
    }
}
