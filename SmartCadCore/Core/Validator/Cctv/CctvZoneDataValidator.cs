using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using System.Collections;
using SmartCadCore.Common;



namespace SmartCadCore.Core.Validator
{
    public class CctvZoneDataValidator : ObjectDataValidator<CctvZoneData>
    {
        public override void BeforeDelete(NHibernate.ISession session, CctvZoneData dataObject)
        {
            CheckIncidentOpen(dataObject, "DeleteCCTVZoneIncidentOpen");
        }

        public override void AfterDelete(NHibernate.ISession session, CctvZoneData dataObject)
        {
            string queryHQL = SmartCadHqls.GetCustomHql(SmartCadHqls.GetStructsByCctvZoneCode, dataObject.Code);

            IList objectList = SmartCadDatabase.SearchObjects(session, queryHQL);
            foreach (StructData myStruct in objectList)
            {
                myStruct.Zone = null;

                if (SmartCadDatabase.IsInitialize(myStruct.Devices) == false)
                {
                    SmartCadDatabase.InitializeLazy(session, myStruct, myStruct.Devices);
                }
                foreach (VideoDeviceData device in myStruct.Devices)
                {
                    device.Struct = null;
                    SmartCadDatabase.UpdateObject(session, device);
                }    

                SmartCadDatabase.UpdateObject(session, myStruct);
            }
        }

        public override void BeforeUpdate(NHibernate.ISession session, CctvZoneData dataObject)
        {
            CctvZoneData dataObjectAux = new CctvZoneData();
            dataObjectAux.Code = dataObject.Code;
            dataObjectAux = (CctvZoneData)SmartCadDatabase.RefreshObject(dataObjectAux);

            CheckIncidentOpen(dataObjectAux, "ModifyCCTVZoneIncidentOpen");

            string queryHQL = SmartCadHqls.GetCustomHql(SmartCadHqls.GetStructsByCctvZoneCode, dataObject.Code);

            IList objectList = SmartCadDatabase.SearchObjects(queryHQL);
            IList structs = new ArrayList(dataObject.Structs);

            foreach (StructData myStruct in objectList)
            {
                if (structs.Contains(myStruct) == false)
                {
                    myStruct.Zone = null;

                    if (SmartCadDatabase.IsInitialize(myStruct.Devices) == false)
                    {
                        SmartCadDatabase.InitializeLazy(myStruct, myStruct.Devices);
                    }
                    foreach (CameraData camera in myStruct.Devices)
                    {
                        camera.Struct = null;
                        SmartCadDatabase.UpdateObject(session, camera);
                    }
                    foreach (DataloggerData sensor in myStruct.Devices)
                    {
                        sensor.Struct = null;
                        SmartCadDatabase.UpdateObject(session, sensor);
                    }

                    SmartCadDatabase.UpdateObject(session, myStruct);
                }
            }
        }

        private void CheckIncidentOpen(CctvZoneData dataObject, string message)
        {
            string hql = @"select count(*) 
                            from CctvReportData report, CameraData camera 
                            where report.Camera.Name = camera.Name and camera.Struct.Zone.Code = {0} and report.Incident.EndDate is null";
            long count = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(hql, dataObject.Code));
            hql = @"select count(*) 
                    from AlarmReportData report, SensorTelemetryData sensor
                    where report.AlarmSensorTelemetry.SensorTelemetry.Name = sensor.Name and sensor.Datalogger.Struct.Zone.Code = {0} and report.Incident.EndDate is null";

            count += (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(hql, dataObject.Code));
            if (count > 0)
                throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2(message), dataObject.Name));
        }
    }
}
