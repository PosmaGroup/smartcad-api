﻿using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using System.Collections;
using SmartCadCore.Common;

namespace SmartCadCore.Core.Validator
{
    public class CameraValidator : ObjectDataValidator<CameraData>
    {

        public override void BeforeDelete(NHibernate.ISession session, CameraData dataObject)
        {
            try
            {
                dataObject = SmartCadDatabase.RefreshObject(session, dataObject) as CameraData;
            }
            catch (Exception)
            {
                throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2("DeleteCameraData"), dataObject.Name));
            }

            CheckIncidentOpen(dataObject, "DeleteCamaraIncidentOpen");

            IList list = (IList)SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorDeviceByDeviceCode, dataObject.Code));
            if (list.Count > 0){
                foreach (OperatorDeviceData operCamara in list)
	            {
                    SmartCadDatabase.DeleteObject(session, operCamara, false);
                }
            }            
        }

        public override void BeforeUpdate(NHibernate.ISession session, CameraData dataObject)
        {
            CameraData dataObjectAux = new CameraData();
            dataObjectAux.Code = dataObject.Code;
            dataObjectAux = (CameraData)SmartCadDatabase.RefreshObject(dataObjectAux);

            CheckIncidentOpen(dataObjectAux, "ModifyCamaraIncidentOpen");
            CheckName(dataObject);
        }

        public override void BeforeSave(NHibernate.ISession session, CameraData dataObject)
        {
            CheckName(dataObject);
        }

        private void CheckIncidentOpen(CameraData dataObject, string message)
        {
            string hql = "select count(*) from CctvReportData report, CameraData camera where report.Camera.Name = camera.Name and camera.Code = {0} and report.Incident.EndDate is null";
            long count = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(hql, dataObject.Code));
            if (count > 0)
                throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2(message), dataObject.Name));
        }

        private void CheckName(CameraData dataObject)
        {
            string hql = @"SELECT objectData FROM CameraData objectData WHERE objectData.Name = '{0}'";
            CameraData camera = (CameraData)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(hql, dataObject.Name));
            if ((camera != null) && (camera.Code != dataObject.Code))
            {
                throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2("UK_CAMERA_CUSTOM_CODE"), dataObject.Name));
            }
            hql = @"SELECT objectData FROM CameraData objectData WHERE objectData.Ip = '{0}'";
            camera = (CameraData)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(hql, dataObject.Ip));
            if ((camera != null) && (camera.Code != dataObject.Code))
            {
                throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2("UK_CAMERA_IP"), dataObject.Name));
            }

        }
    }
}

