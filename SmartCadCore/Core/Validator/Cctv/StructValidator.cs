using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using System.Collections;
using SmartCadCore.Common;

namespace SmartCadCore.Core.Validator
{
    public class StructValidator : ObjectDataValidator<StructData>
    {
       
        public override void BeforeDelete(NHibernate.ISession session, StructData dataObject)
        {
            CheckIncidentOpen(dataObject, "DeleteCCTVStructIncidentOpen");

            try
            {
                dataObject = SmartCadDatabase.RefreshObject(session, dataObject) as StructData;
            }
            catch (Exception)
            {
                throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2("DeleteUnitData"),dataObject.Name));
            }
            
            if (SmartCadDatabase.IsInitialize(dataObject.Devices) == false)
            {
                SmartCadDatabase.InitializeLazy(session, dataObject, dataObject.Devices);
            }           
            foreach (VideoDeviceData device in dataObject.Devices)
            {
                device.Struct = null;
                SmartCadDatabase.UpdateObject(session, device);               
            }            
        }
        public override void BeforeUpdate(NHibernate.ISession session, StructData dataObject)
        {
            StructData dataObjectAux = new StructData();
            dataObjectAux.Code = dataObject.Code;
            dataObjectAux = (StructData)SmartCadDatabase.RefreshObject(dataObjectAux);

            CheckIncidentOpen(dataObjectAux, "ModifyCCTVStructIncidentOpen");

            if (SmartCadDatabase.IsInitialize(dataObject.Devices) == false)
            {
                SmartCadDatabase.InitializeLazy(session, dataObject, dataObject.Devices);
            }      
            foreach (VideoDeviceData device in dataObject.Devices)
            {
                device.Struct = dataObject;
                SmartCadDatabase.UpdateObject(session, device);
            }            
        }

        private void CheckIncidentOpen(StructData dataObject, string message)
        {
            string hql = "select count(*) from CctvReportData report, CameraData camera where report.Camera.Name = camera.Name and camera.Struct.Code = {0} and report.Incident.EndDate is null";
            long count = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(hql, dataObject.Code));
//            hql = @"select count(*) 
//                    from AlarmReportData report, SensorTelemetryData sensor 
//                    where report.SensorTelemetry.Name = sensor.Name 
//                        and sensor.Struct.Code = {0} 
//                        and report.Incident.EndDate is null";
//            count += (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(hql, dataObject.Code));
            if (count > 0)
                throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2(message), dataObject.Name));

        }
    }
}

