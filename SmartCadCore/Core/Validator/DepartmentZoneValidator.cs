using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using System.Collections;
using SmartCadCore.Common;

namespace SmartCadCore.Core.Validator
{
    public class DepartmentZoneValidator : ObjectDataValidator<DepartmentZoneData>
    {
        public override void BeforeDelete(NHibernate.ISession session, DepartmentZoneData dataObject)
        {
            try
            {
                dataObject = SmartCadDatabase.RefreshObject(session, dataObject) as DepartmentZoneData;
            }
            catch
            {
                throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2("DeleteDepartmentZoneData"), dataObject.Name));
            }
            CheckThereNotDeptStations(dataObject, true);
            foreach (DepartmentZoneAddressData address in dataObject.DepartmentZoneAddres)
            {
                if (address.DeletedId == null)
                {
                    address.DeletedId = Guid.NewGuid();                   
                    SmartCadDatabase.UpdateObject(session, address);
                }
            }

            IList stations =
                SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetAllDepartmentStationByZoneCode, dataObject.Code));


            foreach (DepartmentStationData depSta in stations)
            {
                foreach (DepartmentStationAddressData depAddress in depSta.DepartmentStationAddres)
                {

                    depAddress.DeletedId = new Guid();
                    SmartCadDatabase.UpdateObject(session, depAddress);
                }

            }
        }

        public override void BeforeUpdate(NHibernate.ISession session, DepartmentZoneData dataObject)
        {
            CheckCustomCode(dataObject);
            CheckDepartmentTypeChanged(dataObject);

            if (dataObject.SetDepartmentStations.Count > 0)
            {
                string message = ResourceLoader.GetString2("MessageCanNotChangeZone");
                throw new DatabaseObjectException(dataObject, string.Format(message, dataObject.Name));
            }

            if (dataObject.DepartmentZoneAddres.Count == 0 || CkeckZoneAddressChanged(dataObject) == true)
            {
                IList addresses =
                    SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetDepartmentZoneAddressByZoneCode, dataObject.Code));

                foreach (DepartmentZoneAddressData address in addresses)
                {
                    SmartCadDatabase.DeleteObject(address, address);
                }
            }
        }

        private bool CkeckZoneAddressChanged(DepartmentZoneData dataObject)
        {
            //Evaluate the firt address to check if have code 0.
            foreach (DepartmentZoneAddressData address in dataObject.DepartmentZoneAddres)
            {
                if (address.Code == 0)
                    return true;
                break;
            }
            return false;
        }

        private bool CheckDepartmentTypeChanged(DepartmentZoneData dataObject)
        {
            bool departmentTypeWasChanged = false;
            string hql = SmartCadHqls.GetCustomHql(
                SmartCadHqls.CheckDepartmentTypeChanged, dataObject.Code, dataObject.DepartmentType.Code);

            long counter = (long)SmartCadDatabase.SearchBasicObject(hql);
            if (counter != 0)//DepartmentType is being changed
            {
                CheckThereNotDeptStations(dataObject, false);
                departmentTypeWasChanged = true;
            }
            return departmentTypeWasChanged;
        }

        public override void AfterSave(NHibernate.ISession session, DepartmentZoneData dataObject)
        {
            CheckCustomCode(dataObject);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataObject"></param>
        /// <param name="deleting">when true indicates if zone is being deleted so no data can be related to this.
        /// When false, is because we are trying to change the departmentType to given zone.</param>
        private void CheckThereNotDeptStations(DepartmentZoneData dataObject, bool deleting)
        {
            long counter = (long)SmartCadDatabase.SearchBasicObject(
                    SmartCadHqls.GetCustomHql(SmartCadHqls.GetCountDepartmentStationsByDepartmentZoneCode, dataObject.Code));
            if (counter > 0)
            {
                string message = string.Empty;
                if (deleting)
                {
                    message = ResourceLoader.GetString2("FK_DEPARTMENT_ZONE");
                }
                else
                {
                    message = ResourceLoader.GetString2("MessageCanNotChangeDepartmentType");
                }
                throw new DatabaseObjectException(dataObject, string.Format(message, dataObject.Name));
            }
        }

        private static void CheckCustomCode(DepartmentZoneData dataObject)
        {
            if (!string.IsNullOrEmpty(dataObject.CustomCode))
            {
                string hql = SmartCadHqls.GetCustomHql(
                    SmartCadHqls.GetCountDepartmentZoneByCustomCode, dataObject.CustomCode, 
                    dataObject.Code, dataObject.DepartmentType.Code);
                ArrayList list = (ArrayList)SmartCadDatabase.SearchBasicObjects(hql);
                if (list.Count > 0)
                {
                    long count = (long)list[0];
                    if (count > 0)
                    {
                        throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("UK_DEPARTMENT_ZONE_CUSTOM_CODE"));
                    }
                }

            }            
        }     
    }
}

