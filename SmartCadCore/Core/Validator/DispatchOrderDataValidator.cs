using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using NHibernate;
using SmartCadCore.Common;

namespace SmartCadCore.Core.Validator
{
    public class DispatchOrderDataValidator : ObjectDataValidator<DispatchOrderData>
    {
        public override void BeforeSave(NHibernate.ISession session, DispatchOrderData dataObject)
        {
            dataObject.StartDate = SmartCadDatabase.GetTimeFromBD();
            UnitData unit = new UnitData();
            unit.Code = dataObject.Unit.Code;
            unit.CustomCode = dataObject.Unit.CustomCode;
            //Search last version of this unit
            unit = SmartCadDatabase.RefreshObject(unit) as UnitData;
            IncidentNotificationData notification = new IncidentNotificationData();
            notification.Code = dataObject.IncidentNotification.Code;
            notification = SmartCadDatabase.RefreshObject(notification) as IncidentNotificationData;
            if (notification.Status.CustomCode == IncidentNotificationStatusData.Cancelled.CustomCode ||
                notification.Status.CustomCode == IncidentNotificationStatusData.Closed.CustomCode)
            {
                throw new DatabaseObjectException(notification, ResourceLoader.GetString("MSG_NotificationClosedError"));
            }
            if (unit.Status != UnitStatusData.Available)
            {
                throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("UK_DISPATCH_ORDER_UNIT"));
            }
            else
            {
                if (dataObject.Status.Equals(DispatchOrderStatusData.Dispatched))
                {
                    if (unit.DepartmentType.Name.Equals(dataObject.IncidentNotification.DepartmentType.Name) == false)
                    {
                        throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("NoUnitAssignInvalidDepartamentType"));
                    }
                    dataObject.Notes = new ArrayList();
                    dataObject.ArrivalDate = dataObject.StartDate.Value.Add(dataObject.ExpectedTime.Value);
                    dataObject.Notes.Add(GetNote(dataObject, DispatchOrderNoteTypeData.Creation));
                    //set assign status to unit
                    dataObject.Unit.Status = UnitStatusData.Assigned;
                    //save unit with assign status
                    SmartCadDatabase.UpdateObject(session, dataObject.Unit);
                    AddStatusHistory(session, dataObject, unit);
                    //set Dispatched status to dispatch order
                    IncidentNotificationData tempNotification = new IncidentNotificationData();
                    tempNotification.Code = dataObject.IncidentNotification.Code;
                    tempNotification = (IncidentNotificationData)SmartCadDatabase.RefreshObject(tempNotification);
                    if ((tempNotification.Version != dataObject.IncidentNotification.Version ||
                        tempNotification.Status.Name != dataObject.IncidentNotification.Status.Name) &&
                        tempNotification.DispatchOperator != dataObject.IncidentNotification.DispatchOperator)
                    {
                        StaleObjectStateException staleObjectException =
                            new StaleObjectStateException(typeof(IncidentNotificationData).Name, tempNotification.Code);
                        throw new DatabaseStaleObjectException(tempNotification,
                            SmartCadDatabase.GetConcurrencyErrorByObject(tempNotification), staleObjectException);
                    }
                    if (tempNotification.Status == IncidentNotificationStatusData.Assigned ||
                        tempNotification.Status == IncidentNotificationStatusData.Pending)
                    {
                        dataObject.IncidentNotification.Status = IncidentNotificationStatusData.InProgress;
                        SmartCadDatabase.UpdateObjectWithValidator(session, dataObject.IncidentNotification);
                    }
                    
                }
                else if (dataObject.Status.Equals(DispatchOrderStatusData.UnitOnScene))
                {
                    if (unit.DepartmentType.Name.Equals(dataObject.IncidentNotification.DepartmentType.Name) == false)
                    {
                        throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("NoUnitAssignInvalidDepartamentType"));
                    }
                    dataObject.Notes = new ArrayList();
                    dataObject.Notes.Add(GetNote(dataObject, DispatchOrderNoteTypeData.CurrentProcedure));
                    dataObject.ArrivalDate = SmartCadDatabase.GetTimeFromBD();
                    dataObject.Unit.Status = UnitStatusData.OnScene;
                    dataObject.ExpectedTime = new TimeSpan(0, 0, 1);
                    SmartCadDatabase.UpdateObject(session, dataObject.Unit);
                    AddStatusHistory(session, dataObject, unit);
                    if (dataObject.IncidentNotification.Status == IncidentNotificationStatusData.Assigned ||
                        dataObject.IncidentNotification.Status == IncidentNotificationStatusData.Pending)
                    {
                        dataObject.IncidentNotification.Status = IncidentNotificationStatusData.InProgress;
                        SmartCadDatabase.UpdateObjectWithValidator(session, dataObject.IncidentNotification);
                    }
                }
                CheckAttentionStatusIncidentNotification(session, dataObject);
            }
        }

        private DispatchOrderNoteData GetNote(DispatchOrderData dispatchOrder, DispatchOrderNoteTypeData type)
        {
            DispatchOrderNoteData note = new DispatchOrderNoteData();
            note.CompleteUserName = dispatchOrder.IncidentNotification.DispatchOperator.FirstName + " " +
                dispatchOrder.IncidentNotification.DispatchOperator.LastName;
            note.Detail = DispatchOrderNoteTypeData.CurrentProcedure.FriendlyName;
            note.UserLogin = dispatchOrder.IncidentNotification.DispatchOperator.Login;
            note.CreationDate = SmartCadDatabase.GetTimeFromBD();
            note.Type = type;
            note.DispatchOrder = dispatchOrder;
            return note;
        }

        private void AddStatusHistory(NHibernate.ISession session,
            DispatchOrderData dispatchOrder, UnitData refreshedUnit)
        {
            if (dispatchOrder.Unit.Status.Equals(refreshedUnit.Status) == false)
            {
                UnitStatusHistoryData temp = SmartCadDatabase.SearchObject(session,
                    SmartCadHqls.GetCustomHql(SmartCadHqls.GetCurrentUnitStatusHistory, dispatchOrder.Unit.Code))
                    as UnitStatusHistoryData;
                if (temp != null)
                {
                    temp.End = SmartCadDatabase.GetTimeFromBD();
                    SmartCadDatabase.UpdateObject(session, temp);
                }

                temp = new UnitStatusHistoryData();
                temp.Start = SmartCadDatabase.GetTimeFromBD();
                temp.Status = dispatchOrder.Unit.Status;
                temp.UserAccount = dispatchOrder.IncidentNotification.DispatchOperator;
                temp.Unit = dispatchOrder.Unit;
                SmartCadDatabase.SaveObject(session, temp);
            }
        }

        public override void BeforeUpdate(NHibernate.ISession session, DispatchOrderData dataObject)
        {
            UnitStatusData unitStatus = dataObject.Unit.Status;
            UnitData refreshedUnit = new UnitData();
            refreshedUnit.Code = dataObject.Unit.Code;
            refreshedUnit = SmartCadDatabase.RefreshObject(refreshedUnit) as UnitData;
            
            IncidentNotificationData tempNotification = new IncidentNotificationData();
            tempNotification.Code = dataObject.IncidentNotification.Code;
            tempNotification = (IncidentNotificationData)SmartCadDatabase.RefreshObject(tempNotification);
            if ((tempNotification.Version != dataObject.IncidentNotification.Version ||
                        tempNotification.Status.Name != dataObject.IncidentNotification.Status.Name) &&
                        tempNotification.DispatchOperator != dataObject.IncidentNotification.DispatchOperator)
            {
                StaleObjectStateException staleObjectException =
                    new StaleObjectStateException(typeof(IncidentNotificationData).Name, tempNotification.Code);
                throw new DatabaseStaleObjectException(tempNotification,
                    SmartCadDatabase.GetConcurrencyErrorByObject(tempNotification), staleObjectException);
            }

            if (dataObject.Status == DispatchOrderStatusData.Closed)
            {
                dataObject.Unit.DispatchOrder = null;
                dataObject.Unit.Status = unitStatus;
                SmartCadDatabase.UpdateObject(session, dataObject.Unit);
                AddStatusHistory(session, dataObject, refreshedUnit);
                dataObject.EndDate = SmartCadDatabase.GetTimeFromBD();
            }
            else if (dataObject.Status == DispatchOrderStatusData.Cancelled)
            {
                dataObject.EndDate = SmartCadDatabase.GetTimeFromBD();
                dataObject.Unit.DispatchOrder = null;
                dataObject.Unit.Status = unitStatus;
                SmartCadDatabase.UpdateObject(session, dataObject.Unit);
                DispatchOrderNoteData note = dataObject.Notes[0] as DispatchOrderNoteData;
                SmartCadDatabase.SaveObject(session, note);
                dataObject.Notes = null;
                AddStatusHistory(session, dataObject, refreshedUnit);
                CheckAttentionStatusIncidentNotification(session, dataObject);
            }                
            else if (dataObject.Status == DispatchOrderStatusData.UnitOnScene)
            {
                dataObject.ArrivalDate = SmartCadDatabase.GetTimeFromBD();
                dataObject.ExpectedTime = dataObject.ArrivalDate - dataObject.StartDate;
                dataObject.Unit.Status = UnitStatusData.OnScene;
                SmartCadDatabase.UpdateObject(session, dataObject.Unit);
                AddStatusHistory(session, dataObject, refreshedUnit);
                CheckAttentionStatusIncidentNotification(session, dataObject);
            }
            else if (dataObject.Status == DispatchOrderStatusData.Delayed)
            {
                dataObject.Unit.Status = UnitStatusData.Delayed;
                SmartCadDatabase.UpdateObject(session, dataObject.Unit);
                AddStatusHistory(session, dataObject, refreshedUnit);

                if (dataObject.Notes.Count > 0)
                {
                    DispatchOrderNoteData note = dataObject.Notes[0] as DispatchOrderNoteData;
                    SmartCadDatabase.SaveObject(session, note);
                    dataObject.Notes = null;
                }

                if (dataObject.IncidentNotification.Status != IncidentNotificationStatusData.Delayed)
                {
                    dataObject.IncidentNotification.Status = IncidentNotificationStatusData.Delayed;
                    SmartCadDatabase.UpdateObjectWithValidator(session, dataObject.IncidentNotification);
                }
            }
            else if (dataObject.Status == DispatchOrderStatusData.Dispatched)
            {
                dataObject.ArrivalDate = SmartCadDatabase.GetTimeFromBD().Add(dataObject.ExpectedTime.Value);
                CheckAttentionStatusIncidentNotification(session, dataObject);
                dataObject.Unit.Status = UnitStatusData.Assigned;
                SmartCadDatabase.UpdateObject(session, dataObject.Unit);
                AddStatusHistory(session, dataObject, refreshedUnit);
            }
        }

        private void CheckAttentionStatusIncidentNotification(ISession session,
            DispatchOrderData originalDispatchOrder)
        {
            bool inAttention = false;
            IncidentNotificationData incidentNotificationFromDB = (IncidentNotificationData)SmartCadDatabase.SearchObjects(
                SmartCadHqls.GetCustomHql(SmartCadHqls.GetIncidentNotificationByCustomCode, originalDispatchOrder.IncidentNotification.CustomCode))[0];

            SmartCadDatabase.InitializeLazy(incidentNotificationFromDB, incidentNotificationFromDB.SetDispatchOrders);
            IList dispatchOrderList = new ArrayList(incidentNotificationFromDB.SetDispatchOrders);
            for (int i = 0; i < dispatchOrderList.Count 
                && inAttention == false; i++)
            {
                DispatchOrderData dispatchOrder = (DispatchOrderData)dispatchOrderList[i];
                if (dispatchOrder.Status == DispatchOrderStatusData.Delayed && 
                    dispatchOrder != originalDispatchOrder)
                {
                    inAttention = true;
                }
            }
            if (incidentNotificationFromDB.Status == IncidentNotificationStatusData.Delayed ||
                incidentNotificationFromDB.Status == IncidentNotificationStatusData.Updated )
            {
                if (inAttention == false)
                {
                    originalDispatchOrder.IncidentNotification.Status = IncidentNotificationStatusData.InProgress;
                    SmartCadDatabase.UpdateObject(originalDispatchOrder.IncidentNotification);
                }
            }            
        }
    }
}
