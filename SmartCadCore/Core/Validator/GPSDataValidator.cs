﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.Model;
using System.Collections;
using SmartCadCore.Common;

namespace SmartCadCore.Core.Validator
{
    public class GPSDataValidator : ObjectDataValidator<GPSData>
    {
        public override void BeforeDelete(NHibernate.ISession session, GPSData dataObject)
        {
            try
            {
                dataObject = SmartCadDatabase.RefreshObject(session, dataObject) as GPSData;
            }
            catch (Exception)
            {
                throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("DeleteGPSDataWithName", dataObject.Name));
            }
            
            if (SmartCadDatabase.IsInitialize(dataObject.Sensors) == false)
                SmartCadDatabase.InitializeLazy(dataObject, dataObject.Sensors);
            if (dataObject.Sensors != null)
            {
                foreach (GPSPinSensorData sensor in dataObject.Sensors)
                    SmartCadDatabase.DeleteObject(session, sensor, false);
            }

            if (dataObject.Unit != null) 
            {
                UnitData unit = dataObject.Unit;
                unit.GPS = null;
                SmartCadDatabase.SaveObject(session, unit);
            }
        }

        public override void BeforeSave(NHibernate.ISession session, GPSData dataObject)
        {
            CheckUniqueName(dataObject);
        }

        public override void BeforeUpdate(NHibernate.ISession session, GPSData dataObject)
        {
            CheckUniqueName(dataObject);

            GPSData dataObjectAux = new GPSData();
            dataObjectAux.Code = dataObject.Code;
            dataObjectAux = (GPSData)SmartCadDatabase.RefreshObject(dataObjectAux);

            if (SmartCadDatabase.IsInitialize(dataObjectAux.Sensors) == false)
                SmartCadDatabase.InitializeLazy(dataObjectAux, dataObjectAux.Sensors);
            foreach (GPSPinSensorData sensor in dataObjectAux.Sensors)
            {
                if (dataObject.Sensors.Contains(sensor) == false)
                    SmartCadDatabase.DeleteObject(session, sensor, false);
                
            }
        }

        private void CheckUniqueName(GPSData gps) 
        {
            string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetCountGPSByName, gps.Code, gps.Name);
            
            ArrayList list = (ArrayList)SmartCadDatabase.SearchBasicObjects(hql);
            if (list.Count > 0)
            {
                long count = (long)list[0];
                if (count > 0)
                {
                    throw new DatabaseObjectException(gps, ResourceLoader.GetString2("UK_GPS_NAME"));
                }
            }
        
        }

        public override void AfterDelete(NHibernate.ISession session, GPSData dataObject)
        {

            IList unitList = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetUnitByGPSCode, dataObject.Code));

            foreach (UnitData unitData in unitList)
            {

                unitData.DeviceType = 0;
                unitData.GPS = null;
                SmartCadDatabase.UpdateObject(session, unitData);

            }
        
        }


    }
}
