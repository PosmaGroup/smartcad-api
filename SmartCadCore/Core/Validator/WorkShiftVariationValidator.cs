using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using System.Collections;
using SmartCadCore.Common;

namespace SmartCadCore.Core.Validator
{
    public class WorkShiftVariationValidator : ObjectDataValidator<WorkShiftVariationData>
    {
        public override void BeforeDelete(NHibernate.ISession session, WorkShiftVariationData dataObject)
        {
            try
            {
                dataObject = SmartCadDatabase.RefreshObject(session, dataObject) as WorkShiftVariationData;
            }
            catch (Exception)
            {
                if (dataObject.Type == WorkShiftVariationData.WorkShiftType.TimeOff)
                    throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2("DeleteVacationData"), dataObject.Name));
                else
                    throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2("DeleteWorkShiftData"), dataObject.Name));

            }

            if (dataObject.ObjectType == WorkShiftVariationData.ObjectRelatedType.Operators)
            {
                if (SmartCadDatabase.IsInitialize(dataObject.Schedules) == false)
                    SmartCadDatabase.InitializeLazy(dataObject, dataObject.Schedules);
                             

                if (dataObject.Schedules.Count > 0)
                {
                    foreach (WorkShiftScheduleVariationData schedule in dataObject.Schedules)
                    {
                        //Validate that the action cannot be perform if an operator is connected.
                        {
                            if (schedule.Start < DateTime.Now && schedule.End > DateTime.Now)
                            {
                                string hql = @"select count(*) 
                                           from SessionHistoryData data left join 
                                                data.SessionHistoryStatusList statuses
                                           where data.IsLoggedIn = true and
                                                 statuses.EndDate is null and
                                                 data.UserAccount.Code in 
                                            (SELECT ObjectData.SupervisedOperator.Code 
                                             FROM OperatorAssignData ObjectData 
                                             WHERE (ObjectData.SupervisedScheduleVariation.Code = {0} or
                                                    ObjectData.SupervisorScheduleVariation.Code = {0}) and
                                                  '{1}' between ObjectData.StartDate and ObjectData.EndDate)";

                                hql = SmartCadHqls.GetCustomHql(hql, schedule.Code, ApplicationUtil.GetDataBaseFormattedDate(DateTime.Now));

                                bool connected = (long)SmartCadDatabase.SearchBasicObject(hql) > 0;
                                if (connected)
                                    throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("DeleteWorkTimeOperatorConnectedData", dataObject.Name));
                            }
                        }

                        IList operAssigns = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql("SELECT ObjectData FROM OperatorAssignData ObjectData WHERE ObjectData.SupervisedScheduleVariation.Code = {0} OR ObjectData.SupervisorScheduleVariation.Code = {0}", schedule.Code));

                        if (operAssigns != null)
                        {
                            foreach (OperatorAssignData operAssign in operAssigns)
                            {
                                SmartCadDatabase.DeleteObject(session, operAssign, true);
                            }
                        }

                        SmartCadDatabase.DeleteObject(session, schedule, false);
                    }

                 
                }
            }
            else if (dataObject.ObjectType == WorkShiftVariationData.ObjectRelatedType.Units)
            {
                IList routeAssigns = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql("SELECT ObjectData FROM WorkShiftRouteData ObjectData WHERE ObjectData.WorkShift.Code = {0}", dataObject.Code));

                if (routeAssigns != null)
                {
                    foreach (WorkShiftRouteData route in routeAssigns)
                    {
                        SmartCadDatabase.InitializeLazy(route.Unit, route.Unit.WorkShiftRoutes);
                        route.Unit.WorkShiftRoutes.Remove(route);
                        SmartCadDatabase.UpdateObject(session, route.Unit);

                        SmartCadDatabase.DeleteObject(session, route, true);
                    }
                }
            }
        }

        public override void BeforeSave(NHibernate.ISession session, WorkShiftVariationData dataObject)
        {
            //Check if there are any WorkShift with the same name.
            CheckUniqueName(dataObject);          
        }

        public override void BeforeUpdate(NHibernate.ISession session, WorkShiftVariationData dataObject)
        {
            //Check if there are any WorkShift with the same name.
            CheckUniqueName(dataObject);

            IList operators = null;
            string HQL = string.Empty;

            //Initialize operator from the Database
            if (SmartCadDatabase.IsInitialize(dataObject.Operators) == false)
                dataObject.Operators = (IList)SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorByWorkShiftVariationCode, dataObject.Code));
            operators = dataObject.Operators;
            
            //Check if there are any collisions in the Database.
            if (operators != null && operators.Count > 0)
            {
                OperatorScheduleManager.ExistCollisionInDB(operators, dataObject);
            }

            IList deletedAssigns = new ArrayList();

            //Obtain the object that is in the database.
            WorkShiftVariationData dataObjectAux = new WorkShiftVariationData();
            dataObjectAux.Code = dataObject.Code;
            dataObjectAux = SmartCadDatabase.RefreshObject(dataObjectAux) as WorkShiftVariationData;
                 
            //Initialize the Schedule of the object previosly loaded.
            if (SmartCadDatabase.IsInitialize(dataObjectAux.Schedules) == false)
                SmartCadDatabase.InitializeLazy(dataObjectAux, dataObjectAux.Schedules);

            //Check if the deleted schedules contain any assignment.
            //Delete the assignment that are contained in those schedules.
            if (SmartCadDatabase.IsInitialize(dataObject.Schedules) == true)
            {
                foreach (WorkShiftScheduleVariationData schedule in dataObjectAux.Schedules)
                {
                    if (dataObject.Schedules.Contains(schedule) == false)
                    {
                        //Validate that the action cannot be perform if an operator is connected.
                        {
                            if (schedule.Start < DateTime.Now && schedule.End > DateTime.Now)
                            {
                                string hql = @"select count(*) 
                                               from SessionHistoryData data left join 
                                                    data.SessionHistoryStatusList statuses
                                               where data.IsLoggedIn = true and
                                                     statuses.EndDate is null and
                                                     data.UserAccount.Code in 
                                                (SELECT ObjectData.SupervisedOperator.Code 
                                                 FROM OperatorAssignData ObjectData 
                                                 WHERE (ObjectData.SupervisedScheduleVariation.Code = {0} OR ObjectData.SupervisorScheduleVariation.Code = {0}) and
                                                       '{1}' between ObjectData.StartDate and ObjectData.EndDate)";

                                hql = SmartCadHqls.GetCustomHql(hql, schedule.Code, ApplicationUtil.GetDataBaseFormattedDate(DateTime.Now));

                                bool connected = (long)SmartCadDatabase.SearchBasicObject(hql) > 0;
                                if (connected)
                                    throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("DeleteCurrentScheduleWorkTimeData"));
                            }
                        }

                        IList operAssigns = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql("SELECT ObjectData FROM OperatorAssignData ObjectData WHERE ObjectData.SupervisedScheduleVariation.Code = {0} OR ObjectData.SupervisorScheduleVariation.Code = {0}", schedule.Code));

                        if (operAssigns != null)
                        {
                            foreach (OperatorAssignData operAssign in operAssigns)
                            {
                                if (deletedAssigns.Contains(operAssign.Code) == false)
                                {
                                    SmartCadDatabase.DeleteObject(session, operAssign, false);
                                    //operAssign.DeletedId = new Guid();
                                    deletedAssigns.Add(operAssign.Code);
                                }
                            }
                        }
                        SmartCadDatabase.DeleteObject(session, schedule, false);
                    }
                }
            }

            //Initialize the operators of the object previosly loaded.
            if (SmartCadDatabase.IsInitialize(dataObjectAux.Operators) == false)
                SmartCadDatabase.InitializeLazy(dataObjectAux, dataObjectAux.Operators);

            //We check that both the operators of the previosly loaded object and the
            //schedules of the updated objects are loaded.
            if (SmartCadDatabase.IsInitialize(dataObjectAux.Operators) == true &&
                SmartCadDatabase.IsInitialize(dataObject.Schedules) == true)
            {
                //Check for those operators that are deleted
                foreach (WorkShiftOperatorData oper in dataObjectAux.Operators)
                {
                    if (dataObject.Operators.Contains(oper) == false)
                    {

                        //Validate that the action cannot be perform if an operator is connected.
                        {
							string hql = SmartCadHqls.GetSessionHistoryCountInWorkShiftVariationByOperatorCode;

                            hql = SmartCadHqls.GetCustomHql(hql, oper.Code, ApplicationUtil.GetDataBaseFormattedDate(DateTime.Now), dataObject.Code);
                            bool connected = (long)SmartCadDatabase.SearchBasicObject(hql) > 0;
                            if (connected)
                                throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("DeleteOperatorCurrentScheduleWorkTimeData", oper.ToString()));
                        }
                        
                        if (SmartCadDatabase.IsInitialize(oper.Operator.Operators) == false)
                            SmartCadDatabase.InitializeLazy(oper.Operator, oper.Operator.Operators);

                        if (SmartCadDatabase.IsInitialize(oper.Operator.Supervisors) == false)
                            SmartCadDatabase.InitializeLazy(oper.Operator, oper.Operator.Supervisors);

                        ArrayList allAssign = new ArrayList();
                        if (SmartCadDatabase.IsInitialize(oper.Operator.Operators) == true)
                            allAssign.AddRange(oper.Operator.Operators);
                        if (SmartCadDatabase.IsInitialize(oper.Operator.Supervisors) == true)
                            allAssign.AddRange(oper.Operator.Supervisors);

                        foreach (OperatorAssignData operAssign in allAssign)
                        {
                            if ((operAssign.SupervisedScheduleVariation != null &&
                                dataObject.Schedules.Contains(operAssign.SupervisedScheduleVariation) == true) ||
                                (operAssign.SupervisorScheduleVariation != null &&
                                 dataObject.Schedules.Contains(operAssign.SupervisorScheduleVariation) == true))
                            {
                                if (deletedAssigns.Contains(operAssign.Code) == false)
                                {
                                    //operAssign.DeletedId = Guid.NewGuid();
                                    SmartCadDatabase.DeleteObject(session, operAssign, true);
                                    deletedAssigns.Add(operAssign.Code);
                                }
                            }
                        }
                        //oper.DeletedId = Guid.NewGuid();
                        SmartCadDatabase.DeleteObject(session, oper, true);
                    }
                }

                //Check if operator in vacation have assignmets, and resolve those conflicts.
                if (dataObject.Type == WorkShiftVariationData.WorkShiftType.TimeOff && dataObject.Operators != null && dataObject.Operators.Count > 0)
                {
                    IList conflicts = new ArrayList();
                    IList assignsToSave = new ArrayList();
                    IList assignsToDelete = new ArrayList();

                    string hql = "SELECT assign FROM OperatorAssignData assign WHERE (";
                    
                    foreach (WorkShiftScheduleVariationData schedule in dataObject.Schedules)
                    {
                        hql += string.Format("(assign.StartDate between '{0}' and '{1}' OR " +
                                                "assign.EndDate between '{0}' and '{1}' OR " +
                                                "'{0}' between assign.StartDate and assign.EndDate OR " +
                                                "'{1}' between assign.StartDate and assign.EndDate) OR", ApplicationUtil.GetDataBaseFormattedDate(schedule.Start.Value), ApplicationUtil.GetDataBaseFormattedDate(schedule.End.Value));
                    }

                    hql = hql.Substring(0, hql.Length - 2);

                    hql += ") AND (assign.SupervisedOperator.Code IN (";

                    foreach (WorkShiftOperatorData oper in dataObject.Operators)
                        hql += string.Format("{0},", oper.Operator.Code);

                    hql = hql.Substring(0, hql.Length - 1);

                    hql += ") OR assign.Supervisor.Code IN ( ";

                    foreach (WorkShiftOperatorData oper in dataObject.Operators)
                        hql += string.Format("{0},", oper.Operator.Code);

                    hql = hql.Substring(0, hql.Length - 1) + "))";


                    conflicts = (IList)SmartCadDatabase.SearchObjects(hql);

                    object[] list = ResolveAssignsConflicts(dataObject.Schedules, conflicts);

                    assignsToSave = (IList)list[0];
                    assignsToDelete = (IList)list[1];


                    foreach (OperatorAssignData assign in assignsToDelete)
                        SmartCadDatabase.DeleteObject(session, assign, true);

                    foreach (OperatorAssignData assign in assignsToSave)
                        SmartCadDatabase.SaveOrUpdate(session, assign);


                }

            }
        }

        public override void AfterDelete(NHibernate.ISession session, WorkShiftVariationData dataObject)
        {

            string queryHQL = SmartCadHqls.GetCustomHql(SmartCadHqls.GetCustomHql(SmartCadHqls.WorkShiftVariationOperatorsByWorkShiftCode, dataObject.Code));
            IList operators = SmartCadDatabase.SearchObjects(session, queryHQL);
            
            foreach (WorkShiftOperatorData oper in operators)
            {
                oper.DeletedId = dataObject.DeletedId;
                SmartCadDatabase.UpdateObject(session, oper);
            }

        }


        private void CheckUniqueName(WorkShiftVariationData ws)
        {
            string HQL = string.Empty;

            if (ws.ObjectType == WorkShiftVariationData.ObjectRelatedType.Operators &&
                (ws.Type == WorkShiftVariationData.WorkShiftType.ExtraTime || ws.Type == WorkShiftVariationData.WorkShiftType.WorkShift))
            {
                HQL = SmartCadHqls.GetCustomHql(SmartCadHqls.GetCountWSVByNameTypeObjectType, ws.Code, ws.Name, ws.ObjectType.GetHashCode(), 0, 1);
            }
            else
            {
                HQL = SmartCadHqls.GetCustomHql(SmartCadHqls.GetCountWSVByNameTypeObjectType, ws.Code, ws.Name, ws.ObjectType.GetHashCode(), ws.Type.GetHashCode(), ws.Type.GetHashCode());
            }

            long result = (long)SmartCadDatabase.SearchBasicObject(HQL);

            if (result > 0)
            {
                throw new DatabaseObjectException(ws, "UK_WORK_SHIFT_VARIATION_NAME");
            }        
        }


        private object[] ResolveAssignsConflicts(IList schedules, IList conflicts)
        {
            object[] result = new object[2];
            IList assignsToSave = new ArrayList();
            IList assignsToDelete = new ArrayList();
            OperatorAssignData operAssign;

            foreach (WorkShiftScheduleVariationData sc in schedules)
            {
                foreach (OperatorAssignData oldAssign in conflicts)
                {
                    if (sc.Start.Value.Date == oldAssign.StartDate.Date)
                    {
                        if (sc.Start <= oldAssign.StartDate && sc.End < oldAssign.EndDate)
                        {
                            oldAssign.StartDate = sc.End.Value;
                            assignsToSave.Add(oldAssign);
                        }
                        else if (sc.Start > oldAssign.StartDate && sc.End >= oldAssign.EndDate)
                        {
                            oldAssign.EndDate = sc.Start.Value;
                            assignsToSave.Add(oldAssign);
                        }
                        else if (sc.Start > oldAssign.StartDate && sc.End < oldAssign.EndDate)
                        {
                            //Creo una nueva asignación similar a la vieja, pero modificandole la fecha fin.
                            operAssign = new OperatorAssignData();
                            operAssign.SupervisedOperator = oldAssign.SupervisedOperator;
                            operAssign.SupervisedScheduleVariation = oldAssign.SupervisedScheduleVariation;
                            operAssign.SupervisorScheduleVariation = oldAssign.SupervisorScheduleVariation;
                            operAssign.StartDate = oldAssign.StartDate;
                            operAssign.EndDate = sc.Start.Value;
                            operAssign.Supervisor = oldAssign.Supervisor;

                            //Modifico la fecha fin de la asignación vieja
                            oldAssign.StartDate = sc.End.Value;
                            assignsToSave.Add(operAssign);
                            assignsToSave.Add(oldAssign);

                        }
                        else if (sc.Start.Value <= oldAssign.StartDate && sc.End.Value >= oldAssign.EndDate)
                            assignsToDelete.Add(oldAssign);
                    }
                }
            
            }

            result[0] = assignsToSave;
            result[1] = assignsToDelete;


            return result;
        }

        /// <summary>
        /// Validates that the current schedule operators are not connected.
        /// </summary>
        /// <param name="schedule">Schedule to check</param>
        /// <param name="hql">Query to perform.</param>
        /// <returns>True if the current operator, schedule or workshift can be deleted. Otherwise false.</returns>
        private bool CheckCurrentScheduleOperatorsConnected(WorkShiftScheduleVariationData schedule, string hql)
        {
            bool result = false;
            if (schedule.Start < DateTime.Now && DateTime.Now < schedule.End)
            {
                bool connected = (long)SmartCadDatabase.SearchBasicObject(hql) > 0;
                result = connected;
            }
            return result;
        }
    }
}

