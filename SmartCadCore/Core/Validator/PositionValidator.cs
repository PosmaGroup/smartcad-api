﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.Model;
using System.Collections;
using SmartCadCore.Common;

namespace SmartCadCore.Core.Validator
{
    public class PositionValidator : ObjectDataValidator<PositionData>
    {
        public override void BeforeDelete(NHibernate.ISession session, PositionData dataObject)
        {
            try
            {
                dataObject = SmartCadDatabase.RefreshObject(session, dataObject) as PositionData;
            }
            catch (Exception)
            {
                throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2("DeletePositionData"), (dataObject.Name + " " + dataObject.Description)));
            }
        }

        public override void BeforeUpdate(NHibernate.ISession session, PositionData dataObject)
        {

            IList list = (IList)SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetPosition, dataObject.Code));

            foreach (PositionData dataObjectTemp in list)
            {

                if (dataObjectTemp.DepartmentType.Code != dataObject.DepartmentType.Code)
                {

                    IList officerList = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetOfficersWithPosition, dataObject.Code));

                    foreach (OfficerData officer in officerList)
                    {
                        officer.Position = null;
                        SmartCadDatabase.UpdateObject(session, officer);
                    }

                }

                break;

            }


        }

        public override void AfterDelete(NHibernate.ISession session, PositionData dataObject)
        {
            IList officerList = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetOfficersWithPosition, dataObject.Code));
            foreach (OfficerData officer in officerList)
            {
                officer.Position = null;
                SmartCadDatabase.UpdateObject(session, officer);
            }
        }
    }
}
