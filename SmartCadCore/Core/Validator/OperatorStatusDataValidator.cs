using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using System.Collections;
using SmartCadCore.Common;

namespace SmartCadCore.Core.Validator
{
    public class OperatorStatusDataValidator : ObjectDataValidator<OperatorStatusData>
    {
        public override void BeforeSave(NHibernate.ISession session, OperatorStatusData dataObject)
        {
            if (dataObject.Order == null)
            {
                dataObject.Order = SmartCadDatabase.SearchObjects(new OperatorStatusData()).Count + 1;
            }
            if (dataObject.Immutable == null)
            {
                dataObject.Immutable = false;
            }
        }

        public override void BeforeDelete(NHibernate.ISession session, OperatorStatusData dataObject)
        {
            try
            {
                dataObject = SmartCadDatabase.RefreshObject(session, dataObject) as OperatorStatusData;
            }
            catch (Exception)
            {
                throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("DeleteOperatorStatusData"));
            }

            string ready = ResourceLoader.GetString("OperatorStatusImmutableReady");
            string busy =  ResourceLoader.GetString("OperatorStatusImmutableBusy");
               
            if (dataObject.Name.CompareTo(ready) == 0)
                throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("DeletesOperatorStatusImmutableReady"));
            else if (dataObject.Name.CompareTo(busy) == 0)
                throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("DeletesOperatorStatusImmutableNotReady"));


        }

        public override void AfterDelete(NHibernate.ISession session, OperatorStatusData dataObject)
        {

            IList result = SmartCadDatabase.SearchObjects(session, SmartCadHqls.GetCustomHql(SmartCadHqls.GetLoggedInOperatorUsingOperatorStatus, dataObject.Name));

            foreach (SessionStatusHistoryData ssh in result)
            {
                if (ssh.Status.NotReady == true)
                    ssh.Status = OperatorStatusData.Busy;
                else
                    ssh.Status = OperatorStatusData.Ready;

                SmartCadDatabase.UpdateObject(session, ssh);
            }
        }

    }
}
