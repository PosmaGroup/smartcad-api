﻿using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using System.Collections;
using SmartCadCore.Common;

namespace SmartCadCore.Core.Validator
{
    public class DataloggerValidator : ObjectDataValidator<DataloggerData>
    {
        public override void BeforeDelete(NHibernate.ISession session, DataloggerData dataObject)
        {
            try
            {
                dataObject = SmartCadDatabase.RefreshObject(session, dataObject) as DataloggerData;
            }
            catch (Exception)
            {
                throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2("DeleteSensorTelemetryData"), dataObject.Name));
            }

            CheckIncidentOpen(dataObject);
            CheckAlarmActive(dataObject);

            IList list = (IList)SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(
                SmartCadHqls.GetSensorTelemetrysAndThresholdsByDataloggerName, dataObject.Name));
            if (list.Count > 0){
                foreach (SensorTelemetryData sensorTelemetry in list)
	            {
                    foreach (SensorTelemetryThresholdData threshold in sensorTelemetry.Thresholds)
                    {
                        SmartCadDatabase.DeleteObject(session, threshold, false);
                    }
                    SmartCadDatabase.DeleteObject(session, sensorTelemetry, false);
                }
            }            
        }

        public override void BeforeUpdate(NHibernate.ISession session, DataloggerData dataObject)
        {
            DataloggerData dataObjectAux = new DataloggerData();
            dataObjectAux.Code = dataObject.Code;
            dataObjectAux = (DataloggerData)SmartCadDatabase.RefreshObject(dataObjectAux);

            CheckIncidentOpen(dataObjectAux);
            CheckAlarmActive(dataObjectAux);
            
            CheckName(dataObject);
            CheckBounds(dataObject);
         
           if (SmartCadDatabase.IsInitialize(dataObjectAux.SensorTelemetrys) == false)
                            SmartCadDatabase.InitializeLazy(dataObjectAux, dataObjectAux.SensorTelemetrys);
           if (SmartCadDatabase.IsInitialize(dataObjectAux.SensorTelemetrys) == true)
           {
               foreach (SensorTelemetryData senAux in dataObjectAux.SensorTelemetrys)
               {
                   bool foundSensor = false;
                   foreach (SensorTelemetryData sen in dataObject.SensorTelemetrys)
                   {
                       if (senAux.Code == sen.Code)
                       {
                           foundSensor = true;
                           foreach (SensorTelemetryThresholdData thresholdAux in senAux.Thresholds)
                           {
                               bool foundThreshold = false;
                               foreach (SensorTelemetryThresholdData threshold in sen.Thresholds)
                               {
                                   if (thresholdAux.Equals(threshold))
                                   {
                                       foundThreshold = true;
                                       break;
                                   }
                               }
                               if (foundThreshold == false)
                               {
                                   CheckAlarms(thresholdAux.Sensor);
                                   SmartCadDatabase.DeleteObject(session, thresholdAux, false);
                               }
                           }
                           break;
                       }
                   }

                   if (foundSensor == false)
                   {
                       CheckAlarms(senAux);
                       SmartCadDatabase.DeleteObject(session, senAux, false);
                   }
               }
           }
        }


        private void CheckBounds(DataloggerData dataObject)
        {
           foreach (SensorTelemetryData sensor in dataObject.SensorTelemetrys)
            {
                foreach (SensorTelemetryThresholdData threshold1 in sensor.Thresholds)
                {
                    double? high = threshold1.High;
                    double? low = threshold1.Low;
                    int thresholdCode = threshold1.Code;

                    foreach (SensorTelemetryThresholdData threshold2 in sensor.Thresholds)
                    {
                        if (thresholdCode == 0 || thresholdCode != threshold2.Code)
                        {
                            if ((high > threshold2.Low && high < threshold2.High) || 
                                (low > threshold2.Low && low < threshold2.High))
                            {
                                throw new DatabaseObjectException(dataObject,
                                    string.Format(ResourceLoader.GetString2("ThresholdsError"), low, high, threshold2.Low, threshold2.High ));
                            }
                        }
                    }
                }
            }
        }
        public override void BeforeSave(NHibernate.ISession session, DataloggerData dataObject)
        {
            CheckName(dataObject);
            CheckBounds(dataObject);
          
        }

        private void CheckIncidentOpen(DataloggerData dataObject)
        {
            string hql = "select count(*) from AlarmReportData report where report.AlarmSensorTelemetry.SensorTelemetry.Datalogger.Code = {0} and report.Incident.EndDate is null";
            long count = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(hql, dataObject.Code));
            if (count > 0)
                throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2("AlarmsActiveError"), dataObject.Name));
        }

        private void CheckAlarmActive(DataloggerData datalogger)
        {
            string hql = "select count(*) from AlarmSensorTelemetryData alarm where alarm.SensorTelemetry.Datalogger.Code = {0} and alarm.EndAlarm is null";
            long count = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(hql, datalogger.Code));
            if (count > 0)
                throw new DatabaseObjectException(datalogger, string.Format(ResourceLoader.GetString2("AlarmsActiveError"), datalogger.Name));
        }

        private void CheckName(DataloggerData dataObject)
        {
            string hql = @"SELECT objectData FROM DataloggerData objectData WHERE objectData.Name = '{0}'";
            DataloggerData datalogger = (DataloggerData)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(hql, dataObject.Name));
            if ((datalogger != null) && (datalogger.Code != dataObject.Code))
            {
                throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2("UK_SENSOR_CUSTOM_CODE"), dataObject.Name));
            }
        }

        private void CheckAlarms(SensorTelemetryData sensor)
        {
            string hql = "select count(*) from AlarmSensorTelemetryData alarm where alarm.SensorTelemetry.Code = {0} and alarm.EndAlarm is null";
            long count = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(hql, sensor.Code));
            if (count > 0)
                throw new DatabaseObjectException(sensor, string.Format(ResourceLoader.GetString2("AlarmsActiveError"), sensor.Name));
        }
    }
}

