﻿using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using System.Collections;
using SmartCadCore.Common;

namespace SmartCadCore.Core.Validator
{
    public class SensorTelemetryValidator : ObjectDataValidator<DataloggerData>
    {

        public override void BeforeDelete(NHibernate.ISession session, DataloggerData dataObject)
        {
            try
            {
                dataObject = SmartCadDatabase.RefreshObject(session, dataObject) as DataloggerData;
            }
            catch (Exception)
            {
                throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2("DeleteSensorTelemetryData"), dataObject.Name));
            }

            CheckIncidentOpen(dataObject, "DeleteSensorTelemetryIncidentOpen");

            IList list = (IList)SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorDeviceByDeviceCode, dataObject.Code));
            if (list.Count > 0){
                foreach (OperatorDeviceData operSensorTelemetry in list)
	            {
                    SmartCadDatabase.DeleteObject(session, operSensorTelemetry, false);
                }
            }            
        }

        public override void BeforeUpdate(NHibernate.ISession session, DataloggerData dataObject)
        {
            DataloggerData dataObjectAux = new DataloggerData();
            dataObjectAux.Code = dataObject.Code;
            dataObjectAux = (DataloggerData)SmartCadDatabase.RefreshObject(dataObjectAux);

            CheckIncidentOpen(dataObjectAux, "ModifySensorTelemetryIncidentOpen");
            CheckName(dataObject);
        }

        public override void BeforeSave(NHibernate.ISession session, DataloggerData dataObject)
        {
            CheckName(dataObject);
        }

        private void CheckIncidentOpen(DataloggerData dataObject, string message)
        {
            string hql = "select count(*) from AlarmReportData report, SensorTelemetryData sensorTelemetry where report.SensorTelemetry.Name = sensorTelemetry.Name and sensorTelemetry.Code = {0} and report.Incident.EndDate is null";
            long count = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(hql, dataObject.Code));
            if (count > 0)
                throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2(message), dataObject.Name));
        }

        private void CheckName(DataloggerData dataObject)
        {
            string hql = @"SELECT objectData FROM SensorTelemetryData objectData WHERE objectData.Name = '{0}'";
            DataloggerData sensorTelemetry = (DataloggerData)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(hql, dataObject.Name));
            if ((sensorTelemetry != null) && (sensorTelemetry.Code != dataObject.Code))
            {
                throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2("UK_SENSOR_CUSTOM_CODE"), dataObject.Name));
            }
            //hql = @"SELECT objectData FROM SensorTelemetryData objectData WHERE objectData.Ip = '{0}'";
            //sensorTelemetry = (SensorTelemetryData)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(hql, dataObject.Ip));
            //if ((sensorTelemetry != null) && (sensorTelemetry.Code != dataObject.Code))
            //{
            //    throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2("UK_CAMERA_IP"), dataObject.Name));
            //}

        }
    }
}

