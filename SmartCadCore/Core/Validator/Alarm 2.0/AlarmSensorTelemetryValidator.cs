﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.Model;
using SmartCadCore.Common;

namespace SmartCadCore.Core.Validator
{
    public class AlarmSensorTelemetryValidator : ObjectDataValidator<AlarmSensorTelemetryData>
    {
        public override void BeforeUpdate(NHibernate.ISession session, AlarmSensorTelemetryData dataObject)
        {
            long result = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql("Select count(*) from AlarmSensorTelemetryData data where data.Code = {0} and data.Operator is not null", dataObject.Code));
            if (result > 0 && dataObject.Operator != null && dataObject.EndAlarm == null)
                throw new DatabaseObjectException(dataObject,
                                    string.Format(ResourceLoader.GetString2("AlarmAlreadyTaken")));
        }
    }
}
