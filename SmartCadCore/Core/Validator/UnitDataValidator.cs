using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using System.Collections;
using SmartCadCore.Common;

namespace SmartCadCore.Core.Validator
{
    public class UnitDataValidator : ObjectDataValidator<UnitData>
    {
        public override void AfterLoad(NHibernate.ISession session, System.Collections.IList dataObject)
        {
            foreach (UnitData unit in dataObject)
            {
                if (unit.Status == UnitStatusData.Assigned || 
                    unit.Status == UnitStatusData.Delayed ||
                    unit.Status == UnitStatusData.OnScene)
                {
                    unit.DispatchOrder = GetUnitDispatchedOrder(session, unit);
                }
            }
        }

        private DispatchOrderData GetUnitDispatchedOrder(NHibernate.ISession session, UnitData unit)
        {
            DispatchOrderData dispatchOrder = null;
            IList dispatchOrders = SmartCadDatabase.SearchObjects(session, SmartCadHqls.GetCustomHql(
                                        SmartCadHqls.GetCurrentUnitDispatchOrder, unit.CustomCode));
            if (dispatchOrders.Count == 1)
            {
                dispatchOrder = dispatchOrders[0] as DispatchOrderData;
            }
            return dispatchOrder;
        }

        public override void BeforeSave(NHibernate.ISession session, UnitData dataObject)
        {
            CheckIdsGpsRadio(dataObject);
            CheckUnitYear(dataObject);
            AddStatusHistory(session, dataObject, null, true);           
        }

        private static void CheckIdsGpsRadio(UnitData dataObject)
        {
            if (dataObject.GPS != null)
            {
                string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetUnitByGps, dataObject.GPS.Code, dataObject.Code);
                
                ArrayList list = (ArrayList)SmartCadDatabase.SearchBasicObjects(hql);
                if (list.Count > 0)
                {
                    long count = (long)list[0];
                    if (count > 0)
                    {
                        throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("UK_UNIT_ID_GPS"));
                    }
                }

            }
            if (!string.IsNullOrEmpty(dataObject.IdRadio))
            {
                string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetUnitByIdRadio, dataObject.IdRadio, dataObject.Code);
                ArrayList list = (ArrayList)SmartCadDatabase.SearchBasicObjects(hql);
                if (list.Count > 0)
                {
                    long count = (long)list[0];
                    if (count > 0)
                    {
                        throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("UK_UNIT_ID_RADIO"));
                    }
                }
            }
        }

        private void AddStatusHistory(NHibernate.ISession session,
            UnitData dataObject, UnitData dataObjectAux, bool firstSaved)
        {
            if (dataObjectAux == null ||
                dataObject.Status.Equals(dataObjectAux.Status) == false)
            {
                UnitStatusHistoryData temp = null;
                if (dataObject.Code != 0)
                {
                    temp = SmartCadDatabase.SearchBasicObject(
                        SmartCadHqls.GetCustomHql(SmartCadHqls.GetCurrentUnitStatusHistory, dataObject.Code))
                        as UnitStatusHistoryData;
                }
                if (temp != null)
                {
                    temp.End = SmartCadDatabase.GetTimeFromBD();
                    SmartCadDatabase.UpdateObject(session, temp);
                }
                temp = new UnitStatusHistoryData();
                temp.Start = SmartCadDatabase.GetTimeFromBD();
                temp.Status = dataObject.Status;
                temp.UserAccount = dataObject.DispatchOperator;
                temp.Unit = dataObject;
                if (firstSaved == true)
                {
                    dataObject.StatusHistoryList = new ArrayList();
                    dataObject.StatusHistoryList.Add(temp);
                }
                else
                {
                    SmartCadDatabase.SaveObject(session, temp);
                }
            }
        }

        public override void BeforeUpdate(NHibernate.ISession session, UnitData dataObject)
        {
            CheckIdsGpsRadio(dataObject);
            UnitData dataObjectAux = new UnitData();
            dataObjectAux.Code = dataObject.Code;
            dataObjectAux = SmartCadDatabase.RefreshObject(dataObjectAux) as UnitData;
            
			if (dataObject.Status.Name == dataObjectAux.Status.Name && UnitHasCurrentAssings(dataObject))
                throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("NoUpdateUnitOfficersAssigned"));

            if (UnitHasDispacth(dataObjectAux) == true)
                throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("NoUpdateUnitAttendingIncident"));

            CheckChangesUnitStatus(session, dataObject, dataObjectAux);
            CheckChangesUnitOfficerAssing(session, dataObject, dataObjectAux);
            CheckUnitYear(dataObject);
            //Update the map position information with that on the database.
            UpdateUnitPosition(dataObject, dataObjectAux);
            AddStatusHistory(session, dataObject, dataObjectAux, false);

            if (SmartCadDatabase.IsInitialize(dataObjectAux.WorkShiftRoutes) == false)
                SmartCadDatabase.InitializeLazy(dataObjectAux, dataObjectAux.WorkShiftRoutes);

            if (SmartCadDatabase.IsInitialize(dataObject.WorkShiftRoutes) == false)
                SmartCadDatabase.InitializeLazy(dataObject, dataObject.WorkShiftRoutes);

            foreach (WorkShiftRouteData wsRoute in dataObjectAux.WorkShiftRoutes)
                if (dataObject.WorkShiftRoutes.Contains(wsRoute) == false)
                    SmartCadDatabase.DeleteObject(session, wsRoute, false);
        }        

        private void UpdateUnitPosition(UnitData dataObject, UnitData dataObjectAux)
        {        
            if (dataObject.GPS != null && dataObjectAux.GPS != null)
            {
                dataObject.GPS.IsSynchronized = dataObjectAux.GPS.IsSynchronized;
                dataObject.GPS.CoordinatesDate = dataObjectAux.GPS.CoordinatesDate;
                dataObject.GPS.Longitude = dataObjectAux.GPS.Longitude;
                dataObject.GPS.Latitude = dataObjectAux.GPS.Latitude;
            }
        }

        private void CheckChangesUnitOfficerAssing(NHibernate.ISession session, UnitData dataObject, UnitData refreshObject)
        {

            if (dataObject.DepartmentStation.Code == refreshObject.DepartmentStation.Code)
            {//implica que no cambio la estacion.
                //chequeamos el numero de puestos ya que pudo haberlos cambiado
                //entonces hay que chequear si la cantidad nueva corresponde
                //con las asignaciones existentes.
                int seats = refreshObject.Capacity.Value;
                int newSeats = dataObject.Capacity.Value;
                if (newSeats >= seats)
                    return;//no necesitamos validar mas nada, asi que nos vamos del metodo.
                else
                {
                    //validamos que los nuevos puestos sirvan.
                    if (CheckNewSeats(newSeats, refreshObject.Code) == false)
                    {//Informamos al cliente de la inconsistencia.
                        throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("NoUpdateUnitInvalidUnitSeats"));
                    }
                }
            }
            else
            {// si cambio el organismo,zona o estacion
                //Borramos todo ya que el cliente le pregunto al usuario si estaba de acuerdo en borrar
                //las asignaciones.
                DeleteUnitOfficerAssing(session, dataObject.Code);
            }
        }

        private void CheckChangesUnitStatus(NHibernate.ISession session, UnitData dataObject, UnitData refreshObject)
        {

            if (dataObject.Status == refreshObject.Status)
            {
                if (UnitHasActiveAlerts(refreshObject.Code) == true)
                    throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("NoUpdateUnitActiveAlerts"));
                else
                    return;
            }
            else
                return;

        }

        private void DeleteUnitOfficerAssing(NHibernate.ISession session, int unitCode)
        {
            string DateNHibernateFormat = "yyyyMMdd HH:mm";
            string date = SmartCadDatabase.GetTimeFromBD().ToString(DateNHibernateFormat);

            string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetUnitOfficerAssignsByUnitCode, unitCode.ToString(), date);
            int stationCode = 0;
            ArrayList list = (ArrayList)SmartCadDatabase.SearchObjects(hql);

            if (list.Count >= 1)
                stationCode = ((UnitOfficerAssignHistoryData)(list[0])).Officer.DepartmentStation.Code;
            else
                return;

            SmartCadDatabase.DeleteObjectCollection(list,null);

            //Vemos que asignaciones quedaron vacias despues de elminar estas.

            hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetDepartmentStationHistoryAssing, stationCode.ToString(), date);
            list = (ArrayList)SmartCadDatabase.SearchObjects(hql);

            foreach (DepartmentStationAssignHistoryData var in list)
            {
                SmartCadDatabase.InitializeLazy(var, var.UnitOfficerAssign);
                if (var.UnitOfficerAssign.Count == 0)
                    SmartCadDatabase.DeleteObject(session,var,false);
            }
        }

        private void DeletePreviousUnitOfficerAssing(NHibernate.ISession session, int unitCode)
        {
            string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetUnitAssignsByUnitCode, unitCode.ToString());
            
            ArrayList list = (ArrayList)SmartCadDatabase.SearchObjects(hql);

            if (list.Count <= 0)
                return;

            foreach (UnitOfficerAssignHistoryData var in list)
            {
                SmartCadDatabase.DeleteObject(var, null);
            }

        }

        private bool CheckNewSeats(int newSeats, int unitCode)
        {
            
            string DateNHibernateFormat = "yyyyMMdd HH:mm";
            string date = SmartCadDatabase.GetTimeFromBD().ToString(DateNHibernateFormat);

            string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetUnitOfficerAssignsByUnitCode, unitCode.ToString(), date);

            ArrayList list = (ArrayList)SmartCadDatabase.SearchObjects(hql);
            Dictionary<string, int> assingSeats = new Dictionary<string, int>();

            foreach (UnitOfficerAssignHistoryData var in list)
            {
                string start = var.Start.Value.ToString(DateNHibernateFormat);
                string end = var.Start.Value.ToString(DateNHibernateFormat);
                if (!assingSeats.ContainsKey(var.Unit.CustomCode))
                {
                    assingSeats.Add(var.Unit.CustomCode, 1);
                }
                else
                    assingSeats[var.Unit.CustomCode]++;

            }

            foreach (KeyValuePair<string, int> pair in assingSeats)
            {
                if (pair.Value > newSeats)
                    return false;
            }
            return true;
        }

        private bool UnitHasDispacth(UnitData refreshObject)
        {
            if (refreshObject.Status.Name == UnitStatusData.Assigned.Name ||
                refreshObject.Status.Name == UnitStatusData.Delayed.Name ||
                refreshObject.Status.Name == UnitStatusData.OnScene.Name)
                return true;
            return false;
        }

        public override void AfterSave(NHibernate.ISession session, UnitData dataObject)
        {
			if (SmartCadDatabase.IsInitialize(dataObject.DepartmentType.Alerts) == false)
				SmartCadDatabase.InitializeLazy(dataObject.DepartmentType, dataObject.DepartmentType.Alerts);

			foreach (DepartmentTypeAlertData item in dataObject.DepartmentType.Alerts)
			{
				UnitAlertData unitAlert = new UnitAlertData();
				unitAlert.Active = true;
				unitAlert.Alert = item.Alert;
				unitAlert.MaxValue = item.MaxValue;
				unitAlert.Priority = (UnitAlertData.AlertPriority)(int)item.Priority;
				unitAlert.Unit = dataObject;
				SmartCadDatabase.SaveOrUpdate(session, unitAlert);
			}
			//TODO: Gustavo. Ingresar en todos los mapas.
        }

        public override void AfterUpdate(NHibernate.ISession session, UnitData dataObject)
        {
            //TODO: GUSTAVO. Actualizar unidad
        }

        public override void BeforeDelete(NHibernate.ISession session, UnitData dataObject)
        {
            try
            {
                dataObject = SmartCadDatabase.RefreshObject(session, dataObject) as UnitData;
            }
            catch (Exception)
            {
                throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2("DeleteUnitData"),dataObject.CustomCode));
            }            
            string name = dataObject.Status.Name; 
            
            if(name == UnitStatusData.Assigned.Name || name == UnitStatusData.Delayed.Name || name == UnitStatusData.OnScene.Name)
                throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("NoDeleteOfficerAssignedUnit",dataObject.CustomCode));
            else if (UnitHasCurrentAssings(dataObject))
                throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("ErrorDeleteOfficerAssignedUnit",dataObject.CustomCode));
            else  if (UnitHasActiveAlerts(dataObject.Code) == true)
                throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("NoDeleteUnitActiveAlerts", dataObject.CustomCode));

            if (SmartCadDatabase.IsInitialize(dataObject.SetIncidentTypes) == false)
            {
                SmartCadDatabase.InitializeLazy(session, dataObject, dataObject.SetIncidentTypes);
            }
            dataObject.SetIncidentTypes.Clear();



            if (SmartCadDatabase.IsInitialize(dataObject.WorkShiftRoutes) == false)
                SmartCadDatabase.InitializeLazy(dataObject, dataObject.WorkShiftRoutes);
            if (dataObject.WorkShiftRoutes != null)
            {
                foreach (WorkShiftRouteData wsRoute in dataObject.WorkShiftRoutes)
                    SmartCadDatabase.DeleteObject(session, wsRoute, false);

            }

            //Elimina las alertas asociadas a la unidad que quiero eliminar
            if (SmartCadDatabase.IsInitialize(dataObject.Alerts) == false)
                SmartCadDatabase.InitializeLazy(dataObject, dataObject.Alerts);
            if (dataObject.Alerts != null)
            {
                foreach (UnitAlertData alert in dataObject.Alerts)
                    SmartCadDatabase.DeleteObject(session, alert, false);

            }

        }

        public override void AfterDelete(NHibernate.ISession session, UnitData dataObject)
        {
            //TODO: Borrar todas las asignaciones de una unidad.
            DeleteUnitOfficerAssing(session, dataObject.Code);
        }

        private void CheckUnitYear(UnitData dataObject)
        {
            if (dataObject.Year.HasValue == true && dataObject.Year < 1900)
                throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("InvalidUnitYear"));
        }

        private bool UnitHasCurrentAssings(UnitData selectedUnit)
        {            
            string DateNHibernateFormat = "yyyyMMdd HH:mm";
            string date = SmartCadDatabase.GetTimeFromBD().ToString(DateNHibernateFormat);

			string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetCurrentUnitOfficerAssignsByUnitCodeCount, selectedUnit.Code, date);
            return (long)SmartCadDatabase.SearchBasicObject(hql) != 0;
        }

        private bool UnitHasActiveAlerts(int unitCode)
        {
            string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetAlertNotificationNotEndedByUnitCount, unitCode);
            return (long)SmartCadDatabase.SearchBasicObject(hql) != 0;          
        }
    }
}
