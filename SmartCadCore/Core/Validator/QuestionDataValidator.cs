using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using System.Collections;
using SmartCadCore.Common;

namespace SmartCadCore.Core.Validator
{
    public class QuestionDataValidator : ObjectDataValidator<QuestionData>
    {
        public override void AfterLoad(NHibernate.ISession session, System.Collections.IList dataObject)
        {
            //base.AfterLoad(session, dataObject);
        }

        public override void BeforeDelete(NHibernate.ISession session, QuestionData dataObject)
        {
            try
            {
                dataObject = SmartCadDatabase.RefreshObject(session, dataObject) as QuestionData;
            }
            catch (Exception)
            {
                throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2("DeleteQuestionData"),dataObject.Text));
            }

            if (dataObject.RequirementType == RequirementTypeEnum.Required)
                throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("DeleteRequiredQuestion"));


            dataObject.DeletedId = Guid.NewGuid();

            if (SmartCadDatabase.IsInitialize(dataObject.SetPossibleAnswers) == false)
            {
                SmartCadDatabase.InitializeLazy(session, dataObject, dataObject.SetPossibleAnswers);
            }
            foreach (QuestionPossibleAnswerData var in dataObject.SetPossibleAnswers)
            {
                SmartCadDatabase.DeleteObject(session, var,false);
            }
            if (SmartCadDatabase.IsInitialize(dataObject.IncidentTypes) == false)
            {
                SmartCadDatabase.InitializeLazy(session, dataObject, dataObject.IncidentTypes);
            }
            foreach (IncidentTypeQuestionData var in dataObject.IncidentTypes)
            {
                DeleteQuestionIncidentType(session, var);
            }
            //Como en questiodata no hay referencia directa a los
            //phonereport hay que buscar la lista de phonereport
            //en los cuales esta la pregunta y luego que se tienen
            //los phone report hay que buscar las preguntas asociadas
            //a ese phone report, eliminar esta y mantener los indices
            // de PhoneReportAnswer

            //Bucamos los phoneReport en los cuales
            //esta esta pregunta.
            string queryHQL = SmartCadHqls.GetCustomHql(SmartCadHqls.GetPhoneReportAnswerDataByQuestionCode, dataObject.Code);
            IList objectList = SmartCadDatabase.SearchObjects(session, queryHQL);
            foreach (PhoneReportAnswerData var in objectList)
            {
                //ahora buscamos los phone report con su lista de preguntas
                queryHQL = SmartCadHqls.GetCustomHql(SmartCadHqls.GetPhoneReportAnswerDataByPhoneReportCode, var.PhoneReport.Code);
                IList objectList2 = SmartCadDatabase.SearchObjects(session, queryHQL);
                //procedemos a borrar la pregunta en cuestion.
                foreach (PhoneReportAnswerData prad in objectList2)
                {
                    if (prad.Question.Code == dataObject.Code)
                    {
                        prad.DeletedId = Guid.NewGuid();
                        SmartCadDatabase.UpdateObject(session, prad);
                        break;
                    }
                }
            }
        }

        public override void BeforeUpdate(NHibernate.ISession session, QuestionData dataObject)
        {

            //Chequeamos nemonico y nombre
            CheckNameAndNemonic(dataObject);
            
            //Buscamos el objeto viejo en base de datos.
            //QuestionData dataObjectAux = new QuestionData();
            //dataObjectAux.Code = dataObject.Code;
            //dataObjectAux = (QuestionData)SmartCadDatabase.RefreshObject(dataObjectAux);
            //SmartCadDatabase.InitializeLazy(dataObjectAux, dataObjectAux.IncidentTypes);
            

        }

        private void CheckNameAndNemonic(QuestionData obj)
        {
            string hql = "SELECT ObjectData FROM QuestionData ObjectData WHERE ObjectData.Code != {0} AND ( ObjectData.Text = '{1}' OR ObjectData.ShortText = '{2}')";

            hql = SmartCadHqls.GetCustomHql(hql, obj.Code, obj.Text, obj.ShortText);

            IList list = SmartCadDatabase.SearchObjects(hql);
            
            if (list.Count > 0)
            {
                QuestionData q = (QuestionData)list[0];
                if (q.Text.ToLower() == obj.Text.ToLower())
                {
                    throw new DatabaseObjectException(obj, ResourceLoader.GetString2("UK_QUESTION_TEXT"));
                }
                else
                {
                    throw new DatabaseObjectException(obj, ResourceLoader.GetString2("UK_QUESTION_SHORT_TEXT"));
                }
            }
                

        }

        private void DeleteQuestionIncidentType(NHibernate.ISession session, IncidentTypeQuestionData var)
        {
            string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetIncidentTypeQuestionDataByIncidentTypeCode, var.IncidentType.Code);
            IList list = SmartCadDatabase.SearchObjects(session, hql); 
            foreach (IncidentTypeQuestionData itq in list)
            {
                if (itq.Question.Code == var.Question.Code)
                {
                    itq.DeletedId = Guid.NewGuid();
                    itq.Question = var.Question;
                    SmartCadDatabase.UpdateObject(session, itq);
                    break;
                }
            }
        }

        public override void BeforeSave(NHibernate.ISession session, QuestionData dataObject)
        {
            CheckNameAndNemonic(dataObject);
        }
    }
}