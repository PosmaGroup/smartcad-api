using System;
using System.Collections.Generic;
using System.Text;

using NHibernate;
using SmartCadCore.Model;
using SmartCadCore.Core;
using System.Collections;
using SmartCadCore.Common;
namespace SmartCadCore.Core.Validator
{
    public class OperatorDataValidator : ObjectDataValidator<OperatorData>
    {
        public override void BeforeSave(ISession session, OperatorData dataObject)
        {
            CheckAgentID(dataObject);
            //if (!dataObject.Windows.Value)
            //{
            //    if (string.IsNullOrEmpty(dataObject.Password))
            //        dataObject.Password = "";
            //    dataObject.Password = CryptoUtil.Hash(dataObject.Password);
            //}
        }

        public override void BeforeUpdate(ISession session, OperatorData dataObject)
        {
            //this.BeforeSave(session, dataObject);
            CheckAgentID(dataObject);
            SessionHistoryData uuad = SmartCadDatabase.SearchObject<SessionHistoryData>(
                                            SmartCadHqls.GetCustomHql(
                                                SmartCadHqls.GetUserSessions,
                                                dataObject.Code));

            if (uuad != null)
                throw new Exception(ResourceLoader.GetString2("NoDeleteUserLoggedIn", uuad.UserAccount.Login));

			OperatorData BDOper = new OperatorData();
			BDOper.Code = dataObject.Code;
			BDOper = ((OperatorData)SmartCadDatabase.RefreshObject(BDOper));
			if (BDOper.Role.Code != ((OperatorData)dataObject).Role.Code)
			{
				IList Operators = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorsAssignForSupervisor, dataObject.Code));
				foreach (OperatorAssignData item in Operators)
				{
					SmartCadDatabase.DeleteObject(session, item,true);
				}
				IList Supervisors = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetSupervisorsByOperatorCode, dataObject.Code));
				foreach (OperatorAssignData item in Supervisors)
				{
					SmartCadDatabase.DeleteObject(session, item,true);
				}
			}
			else 
			{
				if (SmartCadDatabase.IsInitialize(BDOper.DepartmentTypes) == false)
					SmartCadDatabase.InitializeLazy(BDOper, BDOper.DepartmentTypes);
				if (SmartCadDatabase.IsInitialize(dataObject.DepartmentTypes) == false)
					SmartCadDatabase.InitializeLazy(dataObject,dataObject.DepartmentTypes);

				foreach (DepartmentTypeData department in BDOper.DepartmentTypes)
				{
					if (SmartCadDatabase.IsInitialize(dataObject.DepartmentTypes) == true && dataObject.DepartmentTypes.Contains(department) == false)
					{
						IList list= SmartCadDatabase.SearchObjects((SmartCadHqls.GetCustomHql(SmartCadHqls.GetSupervisorsOfGivenDepartment, BDOper.Code, department.Name)));
						SmartCadDatabase.DeleteObjectCollection(list, null);
					}
				}
			}
            
        }

        public override void BeforeDelete(ISession session, OperatorData dataObject)
        {
            try
            {
                dataObject = SmartCadDatabase.RefreshObject(session, dataObject) as OperatorData;
            }
            catch (Exception)
            {
                throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2("DeleteOperatorData"),(dataObject.FirstName + " " + dataObject.LastName)));
            }

            if (dataObject.FirstName == "Administrador")
                throw new DatabaseObjectException(dataObject, ResourceLoader.GetString("SuperUserDelete"));

        
            string queryHQL =
                " SELECT ObjectData.Code FROM SessionHistoryData ObjectData " +
                " JOIN ObjectData.UserAccount User" +
                " WHERE " +
                " User.Code = '" + dataObject.Code + "' " +
                " AND ObjectData.IsLoggedIn = '1'";
            IList objectList = SmartCadDatabase.SearchObjects(session, queryHQL);
            if (objectList.Count > 0)
                throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString("UserLoggedIn"),(dataObject.FirstName + " " + dataObject.LastName)));
            else 
            {
                dataObject.DeletedId = Guid.NewGuid();
            }

            if (SmartCadDatabase.IsInitialize(dataObject.Devices) == false)
                SmartCadDatabase.InitializeLazy(dataObject, dataObject.Devices);

            foreach (OperatorDeviceData cam in dataObject.Devices)
            {
                SmartCadDatabase.DeleteObject(session, cam,false);
            }

			IList list = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorsTrainingCourseByOperatorCode, dataObject.Code));

            foreach (OperatorTrainingCourseData operTraining in list)
            {
                SmartCadDatabase.DeleteObject(session, operTraining, true);				
            }
			
			//SmartCadDatabase.UpdateObject(session, dataObject);

        }

        public override void AfterDelete(ISession session, OperatorData dataObject)
        {
            string queryHQL =
                " SELECT ObjectData FROM SessionHistoryData ObjectData " +
                " JOIN ObjectData.UserAccount User" +
                " WHERE " +
                " User.Code = '" + dataObject.Code + "' ";
            IQuery query = session.CreateQuery(queryHQL);
            IList objectList = query.List();
            foreach (SessionHistoryData var in objectList)
            {
                var.DeletedId = Guid.NewGuid();
                SmartCadDatabase.DeleteObject(session, var,false);
            }

			IList list = null;
            queryHQL = "SELECT assigns FROM OperatorAssignData assigns WHERE assigns.SupervisedOperator.Code = {0} OR assigns.Supervisor.Code={0}";
			list = session.CreateQuery(SmartCadHqls.GetCustomHql(queryHQL, dataObject.Code)).List();
			foreach (OperatorAssignData opAss in list)
			{
				SmartCadDatabase.DeleteObject(session, opAss, true);
			}

            list.Clear();
            list = session.CreateQuery(SmartCadHqls.GetCustomHql(SmartCadHqls.WorkShiftVariationOperatorsByOperatorCode, dataObject.Code)).List();

            foreach (WorkShiftOperatorData var in list)
            {
                SmartCadDatabase.DeleteObject(session, var, false);
            }		    			
        }


        private static void CheckAgentID(OperatorData dataObject)
        {
            if (!string.IsNullOrEmpty(dataObject.AgentID))
            {
                string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorByAgentID, dataObject.AgentID, dataObject.Code);
                
                ArrayList list = (ArrayList)SmartCadDatabase.SearchBasicObjects(hql);
                if (list.Count > 0)
                {
                    long count = (long)list[0];
                    if (count > 0)
                    {
                        throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("UK_USER_ACCOUNT_AGENTID"));
                    }
                }

            }
        }
    }
}
