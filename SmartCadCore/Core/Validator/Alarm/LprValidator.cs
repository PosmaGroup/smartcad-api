﻿using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using System.Collections;
using SmartCadCore.Common;


namespace SmartCadCore.Core.Validator
{
    public class LprValidator : ObjectDataValidator<LprData>
    {

        public override void BeforeDelete(NHibernate.ISession session, LprData dataObject)
        {
            try
            {
                dataObject = SmartCadDatabase.RefreshObject(session, dataObject) as LprData;
            }
            catch (Exception)
            {
                throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2("DeleteLprData"), dataObject.Name));
            }
           
            IList list = (IList)SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorDeviceByDeviceCode, dataObject.Code));
            if (list.Count > 0){
                foreach (OperatorDeviceData operLpr in list)
	            {
                    SmartCadDatabase.DeleteObject(session, operLpr, false);
                }
            }            
        }
        public override void BeforeSave(NHibernate.ISession session, LprData dataObject)
        {
            CheckName(dataObject);
        }

        public override void BeforeUpdate(NHibernate.ISession session, LprData dataObject)
        {
            CheckName(dataObject);
        }
        private void CheckName(LprData dataObject)
        {
            string hql = @"SELECT objectData FROM LprData objectData WHERE objectData.Name = '{0}'";
            LprData lpr = (LprData)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(hql, dataObject.Name));
            if ((lpr != null) && (lpr.Code != dataObject.Code))
            {
                throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2("UK_LPR_CUSTOM_CODE"), dataObject.Name));
            }
            hql = @"SELECT objectData FROM LprData objectData WHERE objectData.Ip = '{0}'";
            lpr = (LprData)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(hql, dataObject.Ip));
            if ((lpr != null) && (lpr.Code != dataObject.Code))
            {
                throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2("UK_LPR_IP"), dataObject.Name));
            }

        }

    }
}

