﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.Model;
using System.Collections;
using SmartCadCore.Common;

namespace SmartCadCore.Core.Validator
{
    public class QuestionDispatchReportDataValidator : ObjectDataValidator<QuestionDispatchReportData>
    {
        public override void BeforeDelete(NHibernate.ISession session, QuestionDispatchReportData dataObject)
        {
            try
            {
                dataObject = SmartCadDatabase.RefreshObject(session, dataObject) as QuestionDispatchReportData;
            }
            catch (Exception)
            {
                throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2("DeleteQuestionDispatchReportData"), dataObject.Text));
            }

            
            if (dataObject.Required == true)
            {
                throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2("DeleteRequiredQuestionDispatchReport"), dataObject.Text));
            }

            //Search and delete QuestionsReports associated to the questions
            string hql = SmartCadHqls.GetCustomHql(@"SELECT data FROM DispatchReportQuestionDispatchReportData data WHERE data.Question.Code = {0}", dataObject.Code);

            IList qReports = SmartCadDatabase.SearchObjects(hql);

            if (qReports != null && qReports.Count > 0)
                SmartCadDatabase.DeleteObjectCollection(qReports, null);

        }
    }
}
