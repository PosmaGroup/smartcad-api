using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using System.Collections;
using NHibernate;
using SmartCadCore.Common;

namespace SmartCadCore.Core.Validator
{
    class RoomSeatDataValidator : ObjectDataValidator<RoomSeatData>
    {
        public override void BeforeSave(ISession session, RoomSeatData dataObject)
        {
            ValidateComputerName(dataObject);
            
        }

        public override void BeforeUpdate(ISession session, RoomSeatData dataObject)
        {
            ValidateComputerName(dataObject);

        }

        private void ValidateComputerName(RoomSeatData dataObject)
        {
            string sql = SmartCadSQL.GetCustomSQL(SmartCadHqls.GetRoomsSeatByComputerName, dataObject.ComputerName);
            IList seats = SmartCadDatabase.SearchObjects(sql);
            if (seats.Count > 0 && ((RoomSeatData)seats[0]).Code != dataObject.Code)
            {
                throw new Exception(ResourceLoader.GetString2("UK_ROOM_SEAT_COMPUTER_NAME", dataObject.ComputerName));
            }
        }
    }
}
