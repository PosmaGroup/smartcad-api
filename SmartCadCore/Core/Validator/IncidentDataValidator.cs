using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using System.Collections;
using System.Threading;

namespace SmartCadCore.Core.Validator
{
    public class IncidentDataValidator : ObjectDataValidator<IncidentData>
    {
        private void AddStatusHistory(IncidentNotificationData dataObject, OperatorData oper)
        {
            IncidentNotificationStatusHistoryData temp = new IncidentNotificationStatusHistoryData();
            temp.Start = SmartCadDatabase.GetTimeFromBD();
            temp.Status = dataObject.Status;
            temp.UserAccount = oper;
            temp.IncidentNotification = dataObject;
            dataObject.StatusHistoryList = new ArrayList();
            dataObject.StatusHistoryList.Add(temp);
        }  

        public override void BeforeSave(NHibernate.ISession session, IncidentData dataObject)
        {            

            //SmartLogger.Print("Begin");            
            dataObject.StartDate = SmartCadDatabase.GetTimeFromBD();
            if (dataObject.SetReportBaseList != null)
            {
                ArrayList incidentNotifications = new ArrayList();
                foreach (ReportBaseData reportBase in dataObject.SetReportBaseList)
                {
                    if (reportBase is PhoneReportData)
                    {
                        #region PhoneReportData
                        PhoneReportData phoneReport = reportBase as PhoneReportData;
                        if (dataObject.CreateOperator == null)
                            dataObject.CreateOperator = phoneReport.Operator;
                        phoneReport.RegisteredCallTime = SmartCadDatabase.GetTimeFromBD();
                        //Fix problem with PickUpCallTime being higher that the registered.
                        if (phoneReport.RegisteredCallTime < phoneReport.PickedUpCallTime)
                            phoneReport.PickedUpCallTime = phoneReport.RegisteredCallTime.Value.AddSeconds(-10);
                        phoneReport.IncidentNotifications = new ArrayList();
                        phoneReport.MultipleOrganisms = phoneReport.ReportBaseDepartmentTypes.Count > 1;
                        foreach (ReportBaseDepartmentTypeData departmentType in phoneReport.ReportBaseDepartmentTypes)
                        {
                            if (ServiceUtil.DuplicatedNotification(departmentType, incidentNotifications) == false)
                            {
                                IncidentNotificationData notification = new IncidentNotificationData();
                                notification.Comment = string.Empty;
                                notification.CustomCode = Guid.NewGuid().ToString();
                                notification.FriendlyCustomCode = ServiceUtil.GenerateNotificationCustomCode(departmentType.DepartmentType, phoneReport.Operator.Code);
                                notification.DepartmentType = departmentType.DepartmentType;
                                notification.ReportBase = phoneReport;
                                notification.Priority = departmentType.Priority;
                                notification.CreationDate = SmartCadDatabase.GetTimeFromBD();
                                notification.Status = IncidentNotificationStatusData.New;
                                AddStatusHistory(notification, phoneReport.Operator);
                                phoneReport.IncidentNotifications.Add(notification);
                                incidentNotifications.Add(notification);
                            }
                        }

                        //Esto se hace para forzar el aumento de la versi�n del incidentType
                        foreach (IncidentTypeData type in phoneReport.SetIncidentTypes)
                            session.Lock(type, NHibernate.LockMode.Force);
                        #endregion
                    }
                    else if (reportBase is CctvReportData)
                    {
                        #region CctvReportData
                        CctvReportData cctvReport = reportBase as CctvReportData;
                        if (dataObject.CreateOperator == null)
                            dataObject.CreateOperator = cctvReport.Operator;
                        //cctvReport.RegisteredCallTime = SmartCadDatabase.GetTimeFromBD();
                        cctvReport.IncidentNotifications = new ArrayList();
                        cctvReport.MultipleOrganisms = cctvReport.ReportBaseDepartmentTypes.Count > 1;

                        foreach (ReportBaseDepartmentTypeData departmentType in cctvReport.ReportBaseDepartmentTypes)
                        {
                            if (ServiceUtil.DuplicatedNotification(departmentType, incidentNotifications) == false)
                            {
                                IncidentNotificationData notification = new IncidentNotificationData();
                                notification.Comment = string.Empty;
                                notification.CustomCode = Guid.NewGuid().ToString();
                                notification.FriendlyCustomCode = ServiceUtil.GenerateNotificationCustomCode(departmentType.DepartmentType, cctvReport.Operator.Code);
                                notification.DepartmentType = departmentType.DepartmentType;
                                notification.ReportBase = cctvReport;
                                notification.Priority = departmentType.Priority;
                                notification.CreationDate = SmartCadDatabase.GetTimeFromBD();
                                notification.Status = IncidentNotificationStatusData.New;
                                AddStatusHistory(notification, cctvReport.Operator);
                                cctvReport.IncidentNotifications.Add(notification);
                                incidentNotifications.Add(notification);
                            }
                        }


                        //Esto se hace para forzar el aumento de la versi�n del incidentType
                        foreach (IncidentTypeData type in cctvReport.SetIncidentTypes)
                            session.Lock(type, NHibernate.LockMode.Force);
                        #endregion
                    }

                    else if (reportBase is AlarmReportData)
                    {
                        #region AlarmReportData
                        AlarmReportData alarmReport = reportBase as AlarmReportData;
                        if (dataObject.CreateOperator == null)
                            dataObject.CreateOperator = alarmReport.Operator;                        
                        alarmReport.IncidentNotifications = new ArrayList();
                        alarmReport.MultipleOrganisms = alarmReport.ReportBaseDepartmentTypes.Count > 1;

                        foreach (ReportBaseDepartmentTypeData departmentType in alarmReport.ReportBaseDepartmentTypes)
                        {
                            if (ServiceUtil.DuplicatedNotification(departmentType, incidentNotifications) == false)
                            {
                                IncidentNotificationData notification = new IncidentNotificationData();
                                notification.Comment = string.Empty;
                                notification.CustomCode = Guid.NewGuid().ToString();
                                notification.FriendlyCustomCode = ServiceUtil.GenerateNotificationCustomCode(departmentType.DepartmentType, alarmReport.Operator.Code);
                                notification.DepartmentType = departmentType.DepartmentType;
                                notification.ReportBase = alarmReport;
                                notification.Priority = departmentType.Priority;
                                notification.CreationDate = SmartCadDatabase.GetTimeFromBD();
                                notification.Status = IncidentNotificationStatusData.New;
                                AddStatusHistory(notification, alarmReport.Operator);
                                alarmReport.IncidentNotifications.Add(notification);
                                incidentNotifications.Add(notification);
                            }
                        }


                        //Esto se hace para forzar el aumento de la versi�n del incidentType
                        foreach (IncidentTypeData type in alarmReport.SetIncidentTypes)
                            session.Lock(type, NHibernate.LockMode.Force);
                        #endregion
                    }
                    else if (reportBase is AlarmLprReportData)
                    {
                        #region AlarmLprReportData
                        AlarmLprReportData alarmReport = reportBase as AlarmLprReportData;
                        if (dataObject.CreateOperator == null)
                            dataObject.CreateOperator = alarmReport.Operator;
                        alarmReport.IncidentNotifications = new ArrayList();
                        alarmReport.MultipleOrganisms = alarmReport.ReportBaseDepartmentTypes.Count > 1;

                        foreach (ReportBaseDepartmentTypeData departmentType in alarmReport.ReportBaseDepartmentTypes)
                        {
                            if (ServiceUtil.DuplicatedNotification(departmentType, incidentNotifications) == false)
                            {
                                IncidentNotificationData notification = new IncidentNotificationData();
                                notification.Comment = string.Empty;
                                notification.CustomCode = Guid.NewGuid().ToString();
                                notification.FriendlyCustomCode = ServiceUtil.GenerateNotificationCustomCode(departmentType.DepartmentType, alarmReport.Operator.Code);
                                notification.DepartmentType = departmentType.DepartmentType;
                                notification.ReportBase = alarmReport;
                                notification.Priority = departmentType.Priority;
                                notification.CreationDate = SmartCadDatabase.GetTimeFromBD();
                                notification.Status = IncidentNotificationStatusData.New;
                                AddStatusHistory(notification, alarmReport.Operator);
                                alarmReport.IncidentNotifications.Add(notification);
                                incidentNotifications.Add(notification);
                            }
                        }


                        //Esto se hace para forzar el aumento de la versi�n del incidentType
                        foreach (IncidentTypeData type in alarmReport.SetIncidentTypes)
                            session.Lock(type, NHibernate.LockMode.Force);
                        #endregion
                    }
                    else if (reportBase is TwitterReportData)
                    {
                        #region TwitterReportData
                        TwitterReportData twitterReport = reportBase as TwitterReportData;
                        if (dataObject.CreateOperator == null)
                            dataObject.CreateOperator = twitterReport.Operator;
                        twitterReport.IncidentNotifications = new ArrayList();
                        twitterReport.MultipleOrganisms = twitterReport.ReportBaseDepartmentTypes.Count > 1;

                        foreach (ReportBaseDepartmentTypeData departmentType in twitterReport.ReportBaseDepartmentTypes)
                        {
                            if (ServiceUtil.DuplicatedNotification(departmentType, incidentNotifications) == false)
                            {
                                IncidentNotificationData notification = new IncidentNotificationData();
                                notification.Comment = string.Empty;
                                notification.CustomCode = Guid.NewGuid().ToString();
                                notification.FriendlyCustomCode = ServiceUtil.GenerateNotificationCustomCode(departmentType.DepartmentType, twitterReport.Operator.Code);
                                notification.DepartmentType = departmentType.DepartmentType;
                                notification.ReportBase = twitterReport;
                                notification.Priority = departmentType.Priority;
                                notification.CreationDate = SmartCadDatabase.GetTimeFromBD();
                                notification.Status = IncidentNotificationStatusData.New;
                                AddStatusHistory(notification, twitterReport.Operator);
                                twitterReport.IncidentNotifications.Add(notification);
                                incidentNotifications.Add(notification);
                            }
                        }


                        //Esto se hace para forzar el aumento de la versi�n del incidentType
                        foreach (IncidentTypeData type in twitterReport.SetIncidentTypes)
                            session.Lock(type, NHibernate.LockMode.Force);
                        #endregion
                    }
                }
                incidentNotifications.Clear();
            }
            if (dataObject.Status == IncidentStatusData.Closed)
            {
                dataObject.EndDate = dataObject.StartDate;
                dataObject.CloseOperator = dataObject.CreateOperator;
            }
            //SmartLogger.Print("End");
            
        }

        private bool ContainsDepartmentType(IList deptTypeList, ReportBaseDepartmentTypeData deptType)
        {
            bool result = true;
            for (int i = 0; i < deptTypeList.Count && result != true; i++)
            {
                ReportBaseDepartmentTypeData deptTypeTemp = deptTypeList[i] as ReportBaseDepartmentTypeData;
                if (deptTypeTemp.DepartmentType.Name == deptType.DepartmentType.Name)
                {
                    result = false;
                }
            }
            return result;
        }

        private bool NeedNotification(ReportBaseDepartmentTypeData departmentType, PhoneReportData phoneReport)
        {
            return ContainsDepartmentType(phoneReport.ReportBaseDepartmentTypes, departmentType);
        }

        public override void AfterSave(NHibernate.ISession session, IncidentData dataObject)
        {
            if (!(dataObject.Address.Lon != 0 && dataObject.Status.Code != IncidentStatusData.Closed.Code))
            {
                dataObject.Address.IsSynchronized = false;
                dataObject.Address.Lat = 0.0;
                dataObject.Address.Lon = 0.0;

                session.Save(dataObject);
            }
        }

        public override void AfterUpdate(NHibernate.ISession session, IncidentData dataObject)
        {
            if (dataObject.Address.Lon != 0)
            {
                IDictionary<string, string> properties = new Dictionary<string, string>();
                properties.Add("LABEL_TEXT", dataObject.CustomCode);
                properties.Add("STATUS", dataObject.Status.CustomCode);

            }
        }
    }
}
