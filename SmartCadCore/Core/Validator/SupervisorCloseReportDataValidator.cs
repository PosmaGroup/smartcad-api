﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.Model;
using System.Collections;

namespace SmartCadCore.Core.Validator
{
    public class SupervisorCloseReportDataValidator : ObjectDataValidator<SupervisorCloseReportData>
    {
        public override void BeforeUpdate(NHibernate.ISession session, SupervisorCloseReportData dataObject)
        {
            if (dataObject.Finished == true)
            {
                IList indicatorClassDashboards = (IList)SmartCadDatabase.SearchBasicObjects(SmartCadHqls.GetIndicatorCodeFromIndicatorInDashBoard);
                dataObject.Indicators = new ArrayList();
                foreach(object[] data in indicatorClassDashboards)
                {
                    dataObject.Indicators.Add(new IndicatorClassDashboardData() { Code = (int)data[0], Version = (int)data[1] });
                }
            }
        }
    }
}
