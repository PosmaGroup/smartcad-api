using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using System.Collections;
using SmartCadCore.Common;

namespace SmartCadCore.Core.Validator
{
    public class UserRoleDataValidator : ObjectDataValidator<UserRoleData>
    {
        public override void BeforeDelete(NHibernate.ISession session, UserRoleData dataObject)
        {
            try
            {
                dataObject = SmartCadDatabase.RefreshObject(session, dataObject) as UserRoleData;
            }
            catch (Exception)
            {
                throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2("DeleteUserRoleData"),dataObject.FriendlyName));
            }

            if ((bool)dataObject.Immutable)
                throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2("ImmutableUserRoleDelete"), dataObject.FriendlyName));

            string queryHQL ="SELECT ope FROM OperatorData ope left join ope.Role rol WHERE rol.Code = {0}";
            IList objectList = SmartCadDatabase.SearchObjects(session, SmartCadHqls.GetCustomHql(queryHQL, dataObject.Code));
            if (objectList.Count > 0)
                 throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString("FK_USER_ACCOUNT_ROLE_CODE"),dataObject.FriendlyName));
            
           
        }
    }
}
