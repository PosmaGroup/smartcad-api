using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using System.Collections;
using Iesi.Collections;
using System.Threading;
using SmartCadCore.Common;

namespace SmartCadCore.Core.Validator
{
    public class IncidentTypeDataValidator : ObjectDataValidator<IncidentTypeData>
    {
        public override void BeforeDelete(NHibernate.ISession session, IncidentTypeData dataObject)
        {
            /*Aqui se est� usando esta version de SearchObjects porque ejecuta el AfterLoad de IncidentData,
             * el cual carga los tipos de incidente asociados al incidente .By jynojosa 2007/07/18 */
            //Si solamente se necesita chequear los abiertos, las proximas lineas no son necesareas.
            /*try
            {
                dataObject = SmartCadDatabase.RefreshObject(session, dataObject) as IncidentTypeData;
            }
            catch (Exception)
            {
                throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2("DeleteIncidentTypeData"), dataObject.FriendlyName));
            }*/

            //   CheckOpenIncident(dataObject);
        }

        public override void AfterDelete(NHibernate.ISession session, IncidentTypeData dataObject)
        {
            CheckDispatchReport(dataObject);
            CheckOpenIncident(dataObject);
          
            string queryHQL = SmartCadHqls.GetCustomHql(SmartCadHqls.GetIncidentTypeDepartmentTypeDataByIncidentTypeCode, dataObject.Code);    
            IList objectList = SmartCadDatabase.SearchObjects(queryHQL);
            
            foreach (IncidentTypeDepartmentTypeData var in objectList) 
            {
                var.DeletedId = dataObject.DeletedId;
                SmartCadDatabase.UpdateObject(session, var);
            }
            
            queryHQL = SmartCadHqls.GetCustomHql(SmartCadHqls.GetIncidentTypeQuestionDataByIncidentTypeCode, dataObject.Code);
            objectList = SmartCadDatabase.SearchObjects(queryHQL);
            foreach (IncidentTypeQuestionData var in objectList)
            {
                var.DeletedId = dataObject.DeletedId;
                SmartCadDatabase.UpdateObject(session, var);
            }
            
            //Unidades
            queryHQL = SmartCadHqls.GetCustomHql(SmartCadHqls.GetUnitsIncidentType, dataObject.Code);
            objectList = SmartCadDatabase.SearchObjects(queryHQL);

            foreach (UnitData unit in objectList)
            {
                session.Lock(unit, NHibernate.LockMode.None);
                unit.SetIncidentTypes.Remove(dataObject);
                SmartCadDatabase.UpdateObject(session, unit);
            }
            
            //Tipos de Unidades.
            queryHQL = SmartCadHqls.GetCustomHql(SmartCadHqls.GetTypeUnitIncidentType, dataObject.Code);
            objectList = SmartCadDatabase.SearchObjects(queryHQL);
            foreach (UnitTypeData unitType in objectList)
            {
                session.Lock(unitType, NHibernate.LockMode.None);
                unitType.IncidentTypes.Remove(dataObject);
                SmartCadDatabase.UpdateObject(session, unitType);
            }                        
        }


        /// <summary>
        /// Check that an incident is open with the given incident type.
        /// </summary>
        /// <param name="dataObject">Incident type to check.</param>
        private void CheckOpenIncident(IncidentTypeData dataObject)
        {
            string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.CheckIncidentTypeCustomCodeInIncidentOpen, dataObject.Code);
            long objectList = (long)SmartCadDatabase.SearchBasicObject(hql);
            if (objectList > 0)
            {
                throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2("OpenIncidentsOnType"), dataObject.FriendlyName));
            }
        }

        /// <summary>
        /// Check that an dispatch report with the given incident type.
        /// </summary>
        /// <param name="dataObject">Incident type to check.</param>
        private void CheckDispatchReport(IncidentTypeData dataObject)
        {
            string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetCountDispatchReportByIncidentTypeCode, dataObject.Code);
            
            long objectList = (long)SmartCadDatabase.SearchBasicObject(hql);
            if (objectList > 0)
            {
                throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2("IncidentTypeHasDispatchReportAssociated"), dataObject.FriendlyName));
            }
        }


        public override void BeforeSave(NHibernate.ISession session, IncidentTypeData dataObject)
        {
            if (!dataObject.NoEmergency.Value)
            {
                CheckCustomCode(dataObject);
                AddIncidentTypeQuestionWhere(dataObject);
                
            }
            //The where question for CCTV is always added for no emergency or emergency.
            AddIncidentTypeQuestionCCTV(dataObject);
        }

        private static void CheckCustomCode(IncidentTypeData dataObject)
        {
            string queryHQL = SmartCadHqls.GetCustomHql(SmartCadHqls.CountIncidentTypesCustomCode, dataObject.Code, dataObject.CustomCode);
            ArrayList list = (ArrayList)SmartCadDatabase.SearchBasicObjects(queryHQL);
            if (list.Count > 0)
            {
                long count = (long)list[0];
                if (count > 0)
                {
                    throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("UK_INCIDENT_TYPE_CUSTOM_CODE"));
                }
            }
        }

        public override void AfterUpdate(NHibernate.ISession session, IncidentTypeData dataObject)
        {
            string codes = GetIncidentsCodes(dataObject);
            if (!string.IsNullOrEmpty(codes))
            {
                string image = "";
                if (string.IsNullOrEmpty(dataObject.IconName))
                    image = ResourceLoader.GetString("DefaultIconIncidentType") + ".BMP";
                else
                {
                    image = dataObject.IconName;
                    //TODO: GUSTAVO. Actualizar los iconos.
                }
            }  
        }

        private string GetIncidentsCodes(IncidentTypeData dataObject)
        {
            StringBuilder incCodes = new StringBuilder();
            //TODO: Este hql tiene el name de IncidentStatusData OPEN cableado, cambiarlo si es posible
            string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetCodesIncidentsByIncidentType,dataObject.Code);
            ArrayList list = (ArrayList)SmartCadDatabase.SearchBasicObjects(hql);
         
            if (list.Count > 0)
            {
                bool first = true;
                incCodes.Append("(");
                //este diccionario lo uso para ver si hay
                //codigos repetidos de forma de no meterlos 2 veces.
                Dictionary<int, bool> incRep = new Dictionary<int, bool>();
                foreach (int var in list) 
                {
                    if (!incRep.ContainsKey(var))
                    {
                        incRep.Add(var, true);
                        if (first)
                        {
                            first = false;
                            incCodes.Append("'" + var + "'");
                        }
                        else
                        {
                            incCodes.Append(",'" + var + "'");
                        }
                    }
                }
                incCodes.Append(")");
            }
            return incCodes.ToString();
        }

        public override void BeforeUpdate(NHibernate.ISession session, IncidentTypeData dataObject)
        {
            if (!dataObject.NoEmergency.Value)
                CheckCustomCode(dataObject);
     
            //hay que chequear si antes era de emergencia y ahora no.
            IncidentTypeData aux = SmartCadDatabase.SearchObject<IncidentTypeData>(SmartCadHqls.GetCustomHql(SmartCadHqls.GetIncidentTypeByCode,dataObject.Code));

            if(aux.NoEmergency.Value == false && dataObject.NoEmergency.Value == true)
            {
                //chequeamos que no tenga la pregunta donde asociada
                bool pass = HaveWhereAsociated(dataObject);
                if (pass)
                {
                    //borramos la asociacion entre este incidente y la pregunta donde
                    DeleteIncidentTypeQuestion(dataObject);
                }
            }
            if (aux.NoEmergency.Value == true && dataObject.NoEmergency.Value == false) 
            { 
                //le asociamos la pregunta donde
                AddIncidentTypeQuestionWhere(dataObject);
            }
        }
        /// <summary>
        /// Por ahora no se usa.
        /// </summary>
        /// <param name="dataObject"></param>
        private void DeleteIncidentTypeQuestion(IncidentTypeData dataObject)
        {
            string hql = "SELECT objectData FROM IncidentTypeQuestionData objectData WHERE objectData.IncidentType.Code = '" + dataObject.Code + "' AND objectData.Question.CustomCode = 'WHERE'";
            IncidentTypeQuestionData obj = SmartCadDatabase.SearchObject<IncidentTypeQuestionData>(hql);
            obj.DeletedId = Guid.NewGuid();
            SmartCadDatabase.UpdateObject<IncidentTypeQuestionData>(obj);
        }

        /// <summary>
        /// Por ahora no se usa.
        /// </summary>
        /// <param name="dataObject"></param>
        /// <returns></returns>
        private bool HaveWhereAsociated(IncidentTypeData dataObject)
        {
            string hql = "SELECT count(objectData.Code) FROM IncidentTypeQuestionData objectData WHERE objectData.IncidentType.Code = '"+dataObject.Code+"' AND objectData.Question.CustomCode = WHERE";
            ArrayList list = (ArrayList)SmartCadDatabase.SearchBasicObjects(hql);
            long count = (long)list[0];
            if (count > 0) 
            {
                return true;
            }
            return false;
        }

        private void AddIncidentTypeQuestionWhere(IncidentTypeData dataObject)
        {
            QuestionData q = SmartCadDatabase.SearchObject<QuestionData>(SmartCadHqls.GetCustomHql(SmartCadHqls.GetQuestionByCustomCode, "WHERE"));
            IncidentTypeQuestionData iq = new IncidentTypeQuestionData();
            iq.Code = 0;
            iq.IncidentType = dataObject;
            iq.Question = q;
            if (dataObject.SetIncidentTypeQuestions == null)
                dataObject.SetIncidentTypeQuestions = new HashedSet();
            dataObject.SetIncidentTypeQuestions.Add(iq);
        }

        private void AddIncidentTypeQuestionCCTV(IncidentTypeData dataObject)
        {
            QuestionData q = SmartCadDatabase.SearchObject<QuestionData>(SmartCadHqls.GetCustomHql(SmartCadHqls.GetQuestionByCustomCode, "CCTV"));
            IncidentTypeQuestionData iq = new IncidentTypeQuestionData();
            iq.Code = 0;
            iq.IncidentType = dataObject;
            iq.Question = q;
            if (dataObject.SetIncidentTypeQuestions == null)
                dataObject.SetIncidentTypeQuestions = new HashedSet();
            dataObject.SetIncidentTypeQuestions.Add(iq);
        }
    }
}
