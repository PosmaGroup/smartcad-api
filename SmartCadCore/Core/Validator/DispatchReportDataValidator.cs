﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.Model;
using System.Collections;
using SmartCadCore.Common;

namespace SmartCadCore.Core.Validator
{
    public class DispatchReportDataValidator: ObjectDataValidator<DispatchReportData>
    {
        public override void BeforeDelete(NHibernate.ISession session, DispatchReportData dataObject)
        {
            try
            {
                dataObject = SmartCadDatabase.RefreshObject(session, dataObject) as DispatchReportData;
            }
            catch (Exception)
            {
                throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2("DeleteDispatchReportData"), dataObject.Name));
            }

            if (SmartCadDatabase.IsInitialize(dataObject.Questions) == false)
                SmartCadDatabase.InitializeLazy(dataObject, dataObject.Questions);
            if (dataObject.Questions != null)
            {
                foreach (DispatchReportQuestionDispatchReportData question in dataObject.Questions)
                    SmartCadDatabase.DeleteObject(session, question, false);

            }    
        }

        public override void BeforeUpdate(NHibernate.ISession session, DispatchReportData dataObject)
        {
            CheckOtherReports(dataObject);
            DispatchReportData dataObjectAux = new DispatchReportData();
            dataObjectAux.Code = dataObject.Code;
            dataObjectAux = (DispatchReportData)SmartCadDatabase.RefreshObject(dataObjectAux);

            if (SmartCadDatabase.IsInitialize(dataObjectAux.Questions) == false)
                SmartCadDatabase.InitializeLazy(dataObjectAux, dataObjectAux.Questions);
            foreach (DispatchReportQuestionDispatchReportData reportQuestion in dataObjectAux.Questions)
            {
                if (dataObject.Questions.Contains(reportQuestion) == false)
                {
                    if (reportQuestion.Question.Required == true)
                    {
                        reportQuestion.Order = dataObject.Questions.Count;
                        dataObject.Questions.Add(reportQuestion);
                    }
                    else
                        SmartCadDatabase.DeleteObject(session, reportQuestion, false);
                }
            }
        }

        public override void BeforeSave(NHibernate.ISession session, DispatchReportData dataObject)
        {
            CheckOtherReports(dataObject);

            ArrayList list = GetRequiredQuestion(dataObject);

            foreach (DispatchReportQuestionDispatchReportData question in list)
                dataObject.Questions.Add(question);
            
        }

        private ArrayList GetRequiredQuestion(DispatchReportData dataObject) 
        {
            ArrayList list = new ArrayList();
            ArrayList questions = new ArrayList();
            DispatchReportQuestionDispatchReportData reportQuestion;
            
            list = (ArrayList)SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetRequiredQuestionDispatchReport));

            foreach (QuestionDispatchReportData question in list)
            {
                reportQuestion = new DispatchReportQuestionDispatchReportData();
                reportQuestion.Question = question;
                reportQuestion.Report = dataObject;
                reportQuestion.Order = dataObject.Questions.Count;

                questions.Add(reportQuestion);
            }

            return questions;
        }

        private void  CheckOtherReports(DispatchReportData dataObject)
        {
            // Check if exists other report for the same incidentType and departmentType
            foreach (DepartmentTypeData dep in dataObject.DepartmentTypes)
            {
                foreach (IncidentTypeData incidentType in dataObject.IncidentTypes)
                {
                    string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetDispatchReportByIncidentTypeAndDepartmentType, dataObject.Code, dep.Code, incidentType.Code);
                    ArrayList list = (ArrayList)SmartCadDatabase.SearchBasicObjects(hql);
                    if (list.Count > 0)
                    {
                        long count = (long)list[0];
                        if (count > 0)
                        {
                            throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("ExistOtherReportForIncidentType", incidentType.FriendlyName, dep.Name));
                        }
                    }
                }

            }
        }

      
    }
}
