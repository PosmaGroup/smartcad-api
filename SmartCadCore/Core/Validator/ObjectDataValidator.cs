using System;
using System.Collections;
using System.Text;

using NHibernate;
using System.Reflection;
using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Core.Validator
{
    public abstract class ObjectDataValidator<T>
    {
        public virtual void BeforeDelete(ISession session, T dataObject)
        {
        }

        public virtual void AfterDelete(ISession session, T dataObject)
        {
        }

        public virtual void BeforeSave(ISession session, T dataObject)
        {
            CheckNullProperties(dataObject);
        }

        public virtual void AfterSave(ISession session, T dataObject)
        {
        }

        public virtual void BeforeLoad(ISession session, T dataObject)
        {
        }

        public virtual void AfterLoad(ISession session, IList dataObject)
        {
        }

        public virtual void BeforeUpdate(ISession session, T dataObject)
        {
            CheckNullProperties(dataObject);
        }

        public virtual void AfterUpdate(ISession session, T dataObject)
        {
        }

        private void CheckNullProperties(T dataObject)
        {
            Type dataObjectType = typeof(T);
            foreach (PropertyInfo pi in dataObjectType.GetProperties())
            {
                object[] propertyAttributes = pi.GetCustomAttributes(typeof(PropertyAttribute), false);
                if (propertyAttributes.Length > 0)
                {
                    PropertyAttribute prop = (PropertyAttribute) propertyAttributes[0];
                    if (prop.NotNull == true && pi.GetValue(dataObject, null) == null)
                        throw new NullReferenceException();

                    object[] columnAttributes = pi.GetCustomAttributes(typeof(ColumnAttribute), false);
                    if (columnAttributes.Length > 0)
                    {
                        ColumnAttribute col = (ColumnAttribute) columnAttributes[0];
                        if (col.NotNull == true && pi.GetValue(dataObject, null) == null)
                            throw new NullReferenceException();
                    }
                }
            }
        }
    }
}
