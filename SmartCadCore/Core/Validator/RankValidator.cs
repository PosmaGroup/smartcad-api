﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.Model;
using System.Collections;
using SmartCadCore.Common;

namespace SmartCadCore.Core.Validator
{
    public class RankValidator : ObjectDataValidator<RankData>
    {
        public override void BeforeDelete(NHibernate.ISession session, RankData dataObject)
        {
            try
            {
                dataObject = SmartCadDatabase.RefreshObject(session, dataObject) as RankData;
            }
            catch (Exception)
            {
                throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2("DeleteRankData2"), (dataObject.Name + " " + dataObject.Description)));
            }
        }

        public override void BeforeUpdate(NHibernate.ISession session, RankData dataObject)
        {

            IList list = (IList)SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetRank, dataObject.Code));

            foreach (RankData dataObjectTemp in list)
            {
                
                        if (dataObjectTemp.DepartmentType.Code != dataObject.DepartmentType.Code)
                        {

                            IList result = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetOfficersWithRank, dataObject.Code));

                            foreach (OfficerData officer in result)
                            {
                                officer.Rank = null;
                                SmartCadDatabase.UpdateObject(session, officer);
                            }
            
                        }

                        break; 

            }
            
            
        }

        public override void AfterDelete(NHibernate.ISession session, RankData dataObject)
        {
            
            IList officerList = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetOfficersWithRank, dataObject.Code));

            foreach (OfficerData officer in officerList)
            {
                officer.Rank = null;
                SmartCadDatabase.UpdateObject(session, officer);
            }

        }
    }
}
