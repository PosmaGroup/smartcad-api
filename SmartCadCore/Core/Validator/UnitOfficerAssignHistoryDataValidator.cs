using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using System.Collections;
using SmartCadCore.Common;


namespace SmartCadCore.Core.Validator
{
    public class UnitOfficerAssignHistoryDataValidator : ObjectDataValidator<UnitOfficerAssignHistoryData>
    {
        public override void BeforeDelete(NHibernate.ISession session, UnitOfficerAssignHistoryData dataObject)
        {
            DateTime now = SmartCadDatabase.GetTimeFromBD();
            if (dataObject.Start <= now && now < dataObject.End)
            {
                throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("NoDeleteUnitAssignAfterBegining"));
            }
            else
            {
                try
                {
                    dataObject = SmartCadDatabase.RefreshObject(session, dataObject) as UnitOfficerAssignHistoryData;
                }
                catch (Exception)
                {
                    //throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("DeleteDepartmentStationAssignHistoryData"));
                }
             }
        }
    }
}
