using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using System.Collections;
using SmartCadCore.Common;

namespace SmartCadCore.Core.Validator
{
    public class OperatorTrainingCourseValidator : ObjectDataValidator<OperatorTrainingCourseData>
    {

        public override void BeforeSave(NHibernate.ISession session, OperatorTrainingCourseData dataObject)
        {
           if (dataObject.Operator == null)
               throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("OperatorCantBeAssigned"));


           if (ValidateOperatorTrainingCourse(dataObject) == false)
                throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("OperatorHasTrainingCourseScheduled", dataObject.Operator.FirstName + " " + dataObject.Operator.LastName));
        }

        private bool ValidateOperatorTrainingCourse(OperatorTrainingCourseData dataObject)
        {
            if (SmartCadDatabase.IsInitialize(dataObject.TrainingCourseSchedule.Parts) == false)
                SmartCadDatabase.InitializeLazy(dataObject.TrainingCourseSchedule, dataObject.TrainingCourseSchedule.Parts);

            IList list = (IList)SmartCadDatabase.SearchObjects(
                            SmartCadHqls.GetCustomHql(SmartCadHqls.GetTrainingCourseScheduleByOperatorCode,
                            dataObject.Operator.Code,
                            ApplicationUtil.GetDataBaseFormattedDate(SmartCadDatabase.GetTimeFromBD()),
                            ApplicationUtil.GetDataBaseFormattedDate(dataObject.TrainingCourseSchedule.Start.Value),
                            ApplicationUtil.GetDataBaseFormattedDate(dataObject.TrainingCourseSchedule.End.Value)
                            ));
            if (list != null && list.Count > 0)
            {
                foreach (TrainingCourseScheduleData operSchedule in list)
                {
                    if (ValidateSchedule(operSchedule, dataObject.TrainingCourseSchedule) == false)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        private bool ValidateSchedule(TrainingCourseScheduleData oldSchedule, TrainingCourseScheduleData newSchedule)
        {
            //if (SmartCadDatabase.IsInitialize(oldSchedule.Parts) == false)
            //     SmartCadDatabase.InitializeLazy(oldSchedule, newSchedule.Parts);

            foreach (TrainingCourseSchedulePartsData oldPart in oldSchedule.Parts)
            {
                foreach (TrainingCourseSchedulePartsData newPart in newSchedule.Parts)
                {
                    if (oldPart.Code != newPart.Code)
                    {
                        if (!((oldPart.Start < newPart.Start && oldPart.End < newPart.Start) || (oldPart.Start > newPart.End && oldPart.End > newPart.Start)))
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }
    }
}
