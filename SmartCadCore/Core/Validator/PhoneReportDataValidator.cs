using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using System.Collections;
using NHibernate;
using Iesi.Collections;

namespace SmartCadCore.Core.Validator
{
    public class PhoneReportDataValidator : ObjectDataValidator<PhoneReportData>
    {
        private void AddStatusHistory(IncidentNotificationData dataObject, OperatorData oper)
        {
            IncidentNotificationStatusHistoryData temp = new IncidentNotificationStatusHistoryData();
            temp.Start = SmartCadDatabase.GetTimeFromBD();
            temp.Status = dataObject.Status;
            temp.UserAccount = oper;
            temp.IncidentNotification = dataObject;
            dataObject.StatusHistoryList = new ArrayList();
            dataObject.StatusHistoryList.Add(temp);
        }

        public override void BeforeSave(NHibernate.ISession session, PhoneReportData dataObject)
        {
            dataObject.IncidentNotifications = new ArrayList();
            dataObject.RegisteredCallTime = SmartCadDatabase.GetTimeFromBD();
            foreach(ReportBaseDepartmentTypeData departmentType in dataObject.ReportBaseDepartmentTypes)
            {
                if (ServiceUtil.DuplicatedNotification(departmentType, dataObject.Incident) == false)
                {
                    IncidentNotificationData notification = new IncidentNotificationData();
                    notification.Comment = string.Empty;
                    notification.CustomCode = Guid.NewGuid().ToString();
                    notification.FriendlyCustomCode = ServiceUtil.GenerateNotificationCustomCode(departmentType.DepartmentType, dataObject.Operator.Code);
                    notification.DepartmentType = departmentType.DepartmentType;
                    notification.ReportBase = dataObject;
                    notification.Priority = departmentType.Priority;
                    notification.CreationDate = SmartCadDatabase.GetTimeFromBD();
                    notification.Status = IncidentNotificationStatusData.New;
                    notification.SetDispatchOrders = new HashedSet();
                    AddStatusHistory(notification, dataObject.Operator);
                    dataObject.IncidentNotifications.Add(notification);
                }
            }
            bool multipleOrganisms = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(SmartCadHqls.IncidentMultipleOrganisms, dataObject.Incident.Code)) + dataObject.IncidentNotifications.Count > 1;
            dataObject.MultipleOrganisms = multipleOrganisms;
            //UpdateAllIncidentNotifications(session, dataObject.Incident, multipleOrganisms);
            SmartCadDatabase.UpdateSqlWithOutValidator(session, SmartCadSQL.GetCustomSQL(SmartCadSQL.UpdateIncidentNotificationLastStatusHistoryByIncidentCode, dataObject.Incident.Code));
            SmartCadDatabase.UpdateSqlWithOutValidator(session, 
                SmartCadSQL.GetCustomSQL(
                    SmartCadSQL.InsertIncidentNotificationLastStatusHistoryByIncidentCode, 
                    dataObject.Operator.Code,
                    IncidentNotificationStatusData.Updated.Code,
                    dataObject.Incident.Code));
            SmartCadDatabase.UpdateSqlWithOutValidator(session, SmartCadSQL.GetCustomSQL(SmartCadSQL.UpdateIncidentNotificationStatusByIncidentCode, dataObject.Incident.Code));
            SmartCadDatabase.UpdateSqlWithOutValidator(session, SmartCadSQL.GetCustomSQL(SmartCadSQL.UpdateReportBaseMultipleOrganismsByIncidentCode, Convert.ToInt16(multipleOrganisms), dataObject.Incident.Code));
        }

        /*private void UpdateAllIncidentNotifications(NHibernate.ISession session, IncidentData incident, bool multipleOrganisms)
        {
            SmartCadDatabase.InitializeLazy(incident, incident.SetReportBaseList);
            foreach (ReportBaseData reportBase in incident.SetReportBaseList)
            {
                CheckNewInformation(session, reportBase, true);

                SmartCadDatabase.UpdateSqlWithOutValidator(session, "UPDATE REPORT_BASE SET MULTIPLE_ORGANISMS = '" + multipleOrganisms.ToString() + "' WHERE CODE = " + reportBase.Code);
            }
        }*/

        public override void BeforeUpdate(NHibernate.ISession session, PhoneReportData dataObject)
        {
            SmartCadDatabase.UpdateSqlWithOutValidator(session, SmartCadSQL.GetCustomSQL(SmartCadSQL.UpdateIncidentNotificationLastStatusHistoryByIncidentCode, dataObject.Incident.Code));
            SmartCadDatabase.UpdateSqlWithOutValidator(session,
                SmartCadSQL.GetCustomSQL(
                    SmartCadSQL.InsertIncidentNotificationLastStatusHistoryByIncidentCode,
                    dataObject.Operator.Code,
                    IncidentNotificationStatusData.Updated.Code,
                    dataObject.Incident.Code));
            SmartCadDatabase.UpdateSqlWithOutValidator(session, SmartCadSQL.GetCustomSQL(SmartCadSQL.UpdateIncidentNotificationStatusByIncidentCode, dataObject.Incident.Code));
            //CheckNewInformation(session, dataObject, false);
        }

        /*private void CheckNewInformation(NHibernate.ISession session, ReportBaseData phoneReport, bool save)
        {
            IList incidentNotifications = new ArrayList();
            if (save == true)
            {
                SmartCadDatabase.InitializeLazy(phoneReport, phoneReport.IncidentNotifications);
                incidentNotifications = phoneReport.IncidentNotifications;
            }
            else
            {
                incidentNotifications = SmartCadDatabase.SearchObjects("select incidentNotification from IncidentNotificationData incidentNotification where incidentNotification.ReportBase.Code = " + phoneReport.Code);
            }

            foreach (IncidentNotificationData incidentNotification in incidentNotifications)
            {
                if (incidentNotification.Status != IncidentNotificationStatusData.Cancelled &&
                    incidentNotification.Status != IncidentNotificationStatusData.Closed &&
                    incidentNotification.Status != IncidentNotificationStatusData.Supervisor &&
                    incidentNotification.Status != IncidentNotificationStatusData.New)
                {
                    incidentNotification.Status = IncidentNotificationStatusData.Updated;
                    SmartCadDatabase.UpdateObject(session, incidentNotification);
                }
            }
        }*/
    }
}
