using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using System.Collections;
using SmartCadCore.Common;

namespace SmartCadCore.Core.Validator
{
    public class DepartmentStationValidator : ObjectDataValidator<DepartmentStationData>
    {
        public override void BeforeDelete(NHibernate.ISession session, DepartmentStationData dataObject)
        {
            try
            {
                dataObject = SmartCadDatabase.RefreshObject(session, dataObject) as DepartmentStationData;
            }
            catch (Exception)
            {
                throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2("DeleteDepartmentStationData"), (dataObject.Name + " " + dataObject.CustomCode)));
            }
            CheckNotAssociatedObjects(dataObject, true);
            foreach (DepartmentStationAddressData address in dataObject.DepartmentStationAddres)
            {
                if (address.DeletedId == null)
                {
                    address.DeletedId = Guid.NewGuid();
                    SmartCadDatabase.UpdateObject(session, address);
                }
            }
        }

        public override void BeforeUpdate(NHibernate.ISession session, DepartmentStationData dataObject)
        {
            CheckCustomCode(dataObject);
            CheckNameIsUnique(dataObject);
            bool zoneWasChanged = CheckDepartmentZoneChanged(dataObject);

            if (dataObject.DepartmentStationAddres.Count == 0 || CkeckStationAddressChanged(dataObject) == true)
            {
                IList addresses =
                            SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetDepartmentStationAddressByStationCode, dataObject.Code));

                foreach (DepartmentStationAddressData address in addresses)
                {
                    SmartCadDatabase.DeleteObject(address, address);
                }
            }
        }

        private bool CkeckStationAddressChanged(DepartmentStationData dataObject)
        {
            //Evaluate the firt address to check if have code 0.
            foreach (DepartmentStationAddressData address in dataObject.DepartmentStationAddres)
            {
                if (address.Code == 0)
                    return true;
                break;
            }
            return false;
        }

        private static void CheckNotAssociatedObjects(DepartmentStationData dataObject, bool deleting)
        {
            object checker = SmartCadDatabase.SearchBasicObject(
                    SmartCadHqls.GetCustomHql(SmartCadHqls.CheckAssociatedObjectsToDepartmentStation, dataObject.Code));

            if (checker != null)
            {
                string message = string.Empty;
                if (deleting)
                {
                    message = ResourceLoader.GetString2("FK_DEPARTMENT_STATION");
                }
                else
                {
                    message = ResourceLoader.GetString2("MessageCanNotChangeDepartmentZone");
                }
                throw new DatabaseObjectException(dataObject, string.Format(message, dataObject.Name));
            }
        }

        private bool CheckDepartmentZoneChanged(DepartmentStationData dataObject)
        {
            bool zoneWasChanged = false;
            string hql = SmartCadHqls.GetCustomHql(
                SmartCadHqls.CheckDepartmentZoneChanged, dataObject.Code, dataObject.DepartmentZone.Code);

            long counter = (long)SmartCadDatabase.SearchBasicObject(hql);
            if (counter != 0)//DepartmentType is being changed
            {
                zoneWasChanged = true;
                CheckNotAssociatedObjects(dataObject, false);
            }
            return zoneWasChanged;
        }

        public override void BeforeSave(NHibernate.ISession session, DepartmentStationData dataObject)
        {
            CheckCustomCode(dataObject);
            CheckNameIsUnique(dataObject);
        }

        public override void AfterSave(NHibernate.ISession session, DepartmentStationData dataObject)
        {
            
        }

        private static void CheckCustomCode(DepartmentStationData dataObject)
        {
            if (!string.IsNullOrEmpty(dataObject.CustomCode))
            {
                string hql = SmartCadHqls.GetCustomHql(
                    SmartCadHqls.GetCountDepartmentStationByCustomCode, dataObject.CustomCode, 
                    dataObject.Code, dataObject.DepartmentZone.DepartmentType.Code);
                ArrayList list = (ArrayList)SmartCadDatabase.SearchBasicObjects(hql);
                if (list.Count > 0)
                {
                    long count = (long)list[0];
                    if (count > 0)
                    {
                        throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("UK_DEPARTMENT_STATION_CUSTOM_CODE"));
                    }
                }
            }
        }

        private static void CheckNameIsUnique(DepartmentStationData dataObject)
        {
            string hql = SmartCadHqls.GetCustomHql(
                    SmartCadHqls.GetCountDepartmentStationByNameByDepartmentType, dataObject.Name,
                    dataObject.Code, dataObject.DepartmentZone.DepartmentType.Code);
            ArrayList list = (ArrayList)SmartCadDatabase.SearchBasicObjects(hql);
            if (list.Count > 0)
            {
                long count = (long)list[0];
                if (count > 0)
                {
                    throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("UK_DEPARTMENT_STATION_NAME"));
                }
            }
        }
    }
}

