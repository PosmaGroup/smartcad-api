using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using System.Collections;
using SmartCadCore.Common;

namespace SmartCadCore.Core.Validator
{
    public class DepartmentStationAssignHistoryDataValidator : ObjectDataValidator<DepartmentStationAssignHistoryData>
    {
        public override void BeforeSave(NHibernate.ISession session, DepartmentStationAssignHistoryData dataObject)
        {
            //CheckAssignedPeriod(session, dataObject);
        }

        public override void BeforeUpdate(NHibernate.ISession session, DepartmentStationAssignHistoryData dataObject)
        {
            //CheckAssignedPeriod(session, dataObject);
        }

        public override void AfterDelete(NHibernate.ISession session, DepartmentStationAssignHistoryData dataObject)
        {
            foreach (UnitOfficerAssignHistoryData var in dataObject.UnitOfficerAssign)
            {
                if (var.DeletedId == null) 
                {
                    var.DeletedId =  Guid.NewGuid();
                }
                    
            }
        }

        private void CheckAssignedPeriod(NHibernate.ISession session, DepartmentStationAssignHistoryData dataObject)
        {
            foreach (UnitOfficerAssignHistoryData unitOfficerAssign in dataObject.UnitOfficerAssign)
            {
                if (CheckOfficerAssign(unitOfficerAssign.Officer, unitOfficerAssign.Start.Value, unitOfficerAssign.End.Value) == true)
                {
                    throw new Exception(ResourceLoader.GetString2("OfficerAssignedAtSameTime", unitOfficerAssign.Officer.Badge));
                }
            }
        }

        private bool CheckOfficerAssign(OfficerData officerData, DateTime start, DateTime end)
        {
            bool result = false;
            for(int i = 0; i < officerData.UnitsAssigned.Count && result == false; i++)
            {
                UnitOfficerAssignHistoryData unitOfficerAssign = (UnitOfficerAssignHistoryData) officerData.UnitsAssigned[i];
                if ((start >= unitOfficerAssign.Start && start <= unitOfficerAssign.End) ||
                    (end >= unitOfficerAssign.Start && end <= unitOfficerAssign.End))
                {
                    result = true;
                }
            }
            return result;
        }

        public override void BeforeDelete(NHibernate.ISession session, DepartmentStationAssignHistoryData dataObject)
        {
            DateTime now = SmartCadDatabase.GetTimeFromBD();
            SmartCadDatabase.InitializeLazy(dataObject, dataObject.UnitOfficerAssign);
            if ( (dataObject.UnitOfficerAssign.Count != 0) && (dataObject.Start <= now && now < dataObject.End) )
                throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("NoDeleteUnitAssignAfterBegining"));

            try
            {
                dataObject = SmartCadDatabase.RefreshObject(session, dataObject) as DepartmentStationAssignHistoryData;
            }
            catch (Exception)
            {
                throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("DeleteDepartmentStationAssignHistoryData"));
            }
        }
    }
}
