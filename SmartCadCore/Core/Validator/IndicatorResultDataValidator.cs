﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.Model;
using System.Collections;

namespace SmartCadCore.Core.Validator
{
    public class IndicatorResultDataValidator : ObjectDataValidator<IndicatorResultData>
    {
        public override void AfterLoad(NHibernate.ISession session, System.Collections.IList dataObject)
        {


            try
            {
                StringBuilder operators = new StringBuilder();
                StringBuilder departments = new StringBuilder();
                foreach (IndicatorResultData result in dataObject)
                {
                    if (SmartCadDatabase.IsInitialize(result.Result) == true)
                    {
                        foreach (IndicatorResultValuesData value in result.Result)
                        {
                            if (value.IndicatorClass == IndicatorClassData.Operator ||
                                value.IndicatorClass == IndicatorClassData.Group)
                            {
                                operators.Append(value.CustomCode + ",");
                            }
                            else if (value.IndicatorClass == IndicatorClassData.Department)
                            {
                                departments.Append(value.CustomCode + ",");
                            }
                        }
                    }
                }

                if (operators.Length > 0)
                {
                    operators.Remove(operators.Length - 1, 1);
                }
                if (departments.Length > 0)
                {
                    departments.Remove(departments.Length - 1, 1);
                }
                IList operatorsInfo = null;
                IList departmentsInfo = null;
                if (operators.Length > 0)
                {
                    operatorsInfo = (IList)SmartCadDatabase.SearchBasicObjects(SmartCadHqls.GetCustomHql("select data.Code, data.FirstName, data.LastName from OperatorData data where Code in ({0})", operators.ToString()));
                }
                if (departments.Length > 0)
                {
                    departmentsInfo = (IList)SmartCadDatabase.SearchBasicObjects(SmartCadHqls.GetCustomHql("select data.Code, data.Name from DepartmentTypeData data where Code in ({0})", departments.ToString()));
                }
                foreach (IndicatorResultData result in dataObject)
                {
                    if (SmartCadDatabase.IsInitialize(result.Result) == true)
                    {
                        foreach (IndicatorResultValuesData value in result.Result)
                        {
                            if (value.IndicatorClass == IndicatorClassData.Operator ||
                                value.IndicatorClass == IndicatorClassData.Group)
                            {
                                try
                                {
                                    if (operatorsInfo != null)
                                    {
                                        object[] information = GetInformation(value.CustomCode, operatorsInfo);
                                        value.Information = (information[1] as string) + " " + (information[2] as string);
                                    }
                                }
                                catch (Exception ee)
                                {
                                    Console.WriteLine(ee.ToString());
                                }
                            }
                            else if (value.IndicatorClass == IndicatorClassData.Department)
                            {
                                if (departmentsInfo != null)
                                {
                                    object[] information = GetInformation(value.CustomCode, departmentsInfo);
                                    value.Information = (information[1] as string);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public object[] GetInformation(int code, IList info)
        {
            object[] result = new object[] { 0, "", "" };

            foreach (object[] item in info)
            {
                if (((int)item[0]) == code)
                {
                    result = item;
                    break;
                }
            }
            return result;
        }
    }
}
