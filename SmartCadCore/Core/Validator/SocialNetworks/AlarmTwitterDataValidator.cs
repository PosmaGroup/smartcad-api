﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.Model;
using System.Collections;
using SmartCadCore.Common;

namespace SmartCadCore.Core.Validator
{
    public class AlarmTwitterDataValidator : ObjectDataValidator<AlarmTwitterData>
    {

        public override void BeforeSave(NHibernate.ISession session, AlarmTwitterData dataObject)
        {
            if (dataObject.AlarmQuery != null && dataObject.AlarmQuery.Code == 0)
            {
                AlarmQueryTwitterData oldQuery = (AlarmQueryTwitterData)SmartCadDatabase.SearchObjectData(SmartCadHqls.GetCustomHql(
                    SmartCadHqls.GetAlarmQueryTwitterByQuery,dataObject.AlarmQuery.TwitterQuery));

                if (oldQuery != null)
                {
                    dataObject.AlarmQuery = oldQuery;
                }
                else
                {
                    dataObject.AlarmQuery = (AlarmQueryTwitterData)SmartCadDatabase.SaveOrUpdate(dataObject.AlarmQuery);
                }
            }
        }
    }
}
