using System;
using System.Collections.Generic;
using System.Text;

using NHibernate;
using SmartCadCore.Model;
using SmartCadCore.Core;
using System.Collections;

namespace SmartCadCore.Core.Validator
{
    #region Class UserAccountDataValidator Documentation
    /// <summary>
    /// TODO:
    /// </summary>
    /// <className>UserAccountDataValidator</className>
    /// <author email="fsilva@smartmatic.com">Fernando Silva</author>
    /// <author email="atorres@smartmatic.com">Alden Torres</author>
    /// <author email="jynojosa@smartmatic.com">Julio Ynojosa</author>
    /// <date>2006/06/14</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    public class UserAccountDataValidator : ObjectDataValidator<UserAccountData>
    {
        public override void BeforeSave(ISession session, UserAccountData dataObject)
        {
            dataObject.Password = CryptoUtil.Hash(dataObject.Password);
        }

        public override void BeforeUpdate(ISession session, UserAccountData dataObject)
        {
            this.BeforeSave(session, dataObject);
        }

        public override void BeforeDelete(ISession session, UserAccountData dataObject)
        {
            //string queryHQL =
            //    " SELECT Count(ObjectData.Code) FROM SessionHistoryData ObjectData " +
            //    " JOIN ObjectData.UserAccount User"+
            //    " WHERE " +
            //    " User.Coder = '"+dataObject.Code+"' "+
            //    " AND ObjectData.IsLoggedIn = '1'";
            //IList objectList = SmartCadDatabase.SearchObjects(session, queryHQL);
            //if (objectList.Count > 0)
            //    throw new ArgumentException("ESE SE�OR ESTA LOEGADO");

        }
    }
}
