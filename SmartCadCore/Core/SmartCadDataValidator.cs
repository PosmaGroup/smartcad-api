using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

using NHibernate;

using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    #region Class UspDataValidator Documentation
    /// <summary>
    /// TODO:
    /// </summary>
    /// <className>UspDataValidator</className>
    /// <author email="fsilva@smartmatic.com">Fernando Silva</author>
    /// <author email="atorres@smartmatic.com">Alden Torres</author>
    /// <author email="jynojosa@smartmatic.com">Julio Ynojosa</author>
    /// <date>2006/07/07</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    public class SmartCadDataValidator
    {
        #region Class ValidatorType Documentation
        /// <summary>
        /// TODO:
        /// </summary>
        /// <className>ValidatorType</className>
        /// <author email="fsilva@smartmatic.com">Fernando Silva</author>
        /// <date>2006/07/07</date>
        /// <copyRight>Smartmatic copyright</copyRight>
        #endregion
        private enum ValidatorType
        {
            BeforeSave,
            AfterSave,
            BeforeDelete,
            AfterDelete,
            BeforeLoad,
            AfterLoad,
            BeforeUpdate,
            AfterUpdate
        }

        internal static readonly string DataValidatorAssembly = "SmartCadCore.Core.Validator";
        internal static readonly string ModelAssembly = "SmartCadCore.Model";
        
        private static Assembly dataValidator;
        private static Hashtable DataValidators = new Hashtable();
        private static bool isInit = false;

        internal static bool IsInit
        {
            get
            {
                return isInit;
            }
        }

        public static void Init()
        {
            try
            {
                if (IsInit == false)
                {
                    dataValidator = Assembly.GetExecutingAssembly();
                    Type[] types = dataValidator.GetTypes();
                    var cont = 0;
                    foreach (Type type in types)
                    {
                        
                        if (type.Namespace == DataValidatorAssembly || type.Namespace == ModelAssembly)
                        {
                           
                                if (type.IsClass == true
                                    && type.IsAbstract == false )
                                {
                                    ConstructorInfo className = type.GetConstructor(new Type[0] { });
                                    if (className != null)
                                    { 
                                        object classObject = className.Invoke(new object[0] { });
                                        Hashtable ValidatorTypes = new Hashtable();
                                        foreach (string name in Enum.GetNames(typeof(ValidatorType)))
                                        {
                                            MethodInfo mi = type.GetMethod(name);
                                            InvokeInformation ii = new InvokeInformation(classObject, mi);
                                            ValidatorTypes.Add(name, ii);
                                        }
                                        if (type.BaseType.GetGenericArguments().Length > 0 && !DataValidators.Contains(type.BaseType.GetGenericArguments()[0].FullName))
                                            DataValidators.Add(type.BaseType.GetGenericArguments()[0].FullName, ValidatorTypes);
                                    }
                                }                            
                        }
                        cont++;
                    }

                    isInit = true;
                }
            }
            catch (Exception ex)
            {
            }
        }
        
        private static void CheckValidator(ISession session, object objectData, ValidatorType type)
        {
            if (IsInit == false)
                Init();

            if (objectData is IList && ((IList)objectData).Count > 0)
            {
                ObjectData internalObjectData = ((IList)objectData)[0] as ObjectData;
                if (internalObjectData != null)
                {
                    CallValidatorMethod(session, objectData, internalObjectData, type);
                }
            }
            else if ((objectData is Nullable) == false && (objectData is IList) == false)
            {
                CallValidatorMethod(session, objectData, (ObjectData)objectData, type);
            }
        }

        private static void CallValidatorMethod(ISession session, object objectData, ObjectData internalObjectData, ValidatorType type)
        {
            if (DataValidators[internalObjectData.GetType().FullName] != null)
            {
                Hashtable dataValidators = (Hashtable)DataValidators[internalObjectData.GetType().FullName];
                InvokeInformation invokeInformation = (InvokeInformation)dataValidators[type.ToString()];

                try
                {
                    invokeInformation.MethodInfo.Invoke(invokeInformation.ClassObject, new object[2] { session, objectData });
                }
                catch (TargetInvocationException ex)
                {
                    throw new DatabaseObjectException(null, ex.InnerException.Message, ex.InnerException);
                }
                catch (Exception ex)
                {
                    throw new DatabaseObjectException(null, ex.Message, ex);
                }
            }
        }
        
        internal static void BeforeSave(ISession session, ObjectData objectData)
        {
            CheckValidator(session, objectData, ValidatorType.BeforeSave);
        }

        internal static void AfterSave(ISession session, ObjectData objectData)
        {
            CheckValidator(session, objectData, ValidatorType.AfterSave);
        }

        internal static void BeforeDelete(ISession session, ObjectData objectData)
        {
            CheckValidator(session, objectData, ValidatorType.BeforeDelete);
        }

        internal static void AfterDelete(ISession session, ObjectData objectData)
        {
            CheckValidator(session, objectData, ValidatorType.AfterDelete);
        }

        internal static void BeforeLoad(ISession session, ObjectData objectData)
        {
            CheckValidator(session, objectData, ValidatorType.BeforeLoad);
        }

        internal static void AfterLoad(ISession session, IList objectData)
        {
            CheckValidator(session, objectData, ValidatorType.AfterLoad);
        }

        internal static void BeforeUpdate(ISession session, ObjectData objectData)
        {
            CheckValidator(session, objectData, ValidatorType.BeforeUpdate);
        }

        internal static void AfterUpdate(ISession session, ObjectData objectData)
        {
            CheckValidator(session, objectData, ValidatorType.AfterUpdate);
        }
    }

    #region Class InvokeInformation Documentation
    /// <summary>
    /// TODO:
    /// </summary>
    /// <className>InvokeInformation</className>
    /// <author email="fsilva@smartmatic.com">Fernando Silva</author>
    /// <date>2006/07/07</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    internal class InvokeInformation
    {
        private object classObject;
        private MethodInfo methodObject;

        public InvokeInformation(object classObject, MethodInfo methodObject)
        {
            this.classObject = classObject;
            this.methodObject = methodObject;
        }

        public object ClassObject
        {
            get
            {
                return classObject;
            }
        }

        public MethodInfo MethodInfo
        {
            get
            {
                return methodObject;
            }
        }
    }
}
