using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using System.Collections;

namespace SmartCadCore.Core.Indicators
{
    public class D21IndicatorValuesCalculator : IndicatorValuesCalculator
    {
        protected override string GetIndicator()
        {
            return "D21";
        }

        protected override void Calculate()
        {    
            CalculateSystem();
            CalculateGroup();
        }

        private void CalculateSystem()
        {

            #region new
            string sql = @"SELECT	CONVERT(FLOAT,DISPONIBLE.DISPONIBLEPN)/ CONVERT(FLOAT,TOTAL.TOTALOPER)
FROM	(
		SELECT		COUNT(SEH.USER_ACCOUNT_CODE) AS DISPONIBLEPN
		FROM		USER_APPLICATION AS USA
					,SESSION_HISTORY AS SEH
					,SESSION_STATUS_HISTORY AS STH
		WHERE		SEH.USER_APPLICATION_CODE = 3
		AND			SEH.END_DATE_LOGIN IS NULL
		AND			SEH.IS_LOGGED_IN = 'TRUE'
		AND			SEH.CODE = STH.SESSION_HISTORY_CODE
		AND			STH.OPERATOR_STATUS_CODE > 2
		AND			STH.END_DATE IS NULL
		) AS DISPONIBLE,
		(
		SELECT		CASE COUNT(SEH.USER_ACCOUNT_CODE) WHEN 0 THEN 1 ELSE COUNT(SEH.USER_ACCOUNT_CODE) END AS TOTALOPER
		FROM		USER_APPLICATION AS USA
					,SESSION_HISTORY AS SEH
		WHERE		SEH.USER_APPLICATION_CODE = 3
		AND			SEH.END_DATE_LOGIN IS NULL
		AND			SEH.IS_LOGGED_IN = 'TRUE'
		AND			SEH.USER_ACCOUNT_CODE <> 1
		) AS TOTAL
";

            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);

            SaveIndicatorsValues(IndicatorClassData.System, double.Parse(data[0].ToString()));

            #endregion new

            #region doc
            //            string hql = @"select count(*) 
            //                           from SessionHistoryData data inner join 
            //                                data.SessionHistoryStatusList statuses
            //                           where data.IsLoggedIn = true and
            //                                 data.UserApplication.Code = {0} and
            //                                 statuses.Status.Code != {1} and
            //                                 statuses.Status.NotReady = {2} and
            //                                 statuses.EndDate is null";
            //            double operatorsNotReady = (long)SmartCadDatabase.SearchBasicObject(
            //                                                SmartCadHqls.GetCustomHql(
            //                                                    hql,
            //                                                    UserApplicationData.Dispatch.Code,
            //                                                    OperatorStatusData.Busy.Code,
            //                                                    true.ToString().ToLower()));

            //            double operatorsWorkingNow = (long)SmartCadDatabase.SearchBasicObject(
            //                                    SmartCadHqls.GetCustomHql(
            //                                        SmartCadHqls.GetCountLoggedOperatorsByApplication,
            //                                        true.ToString().ToLower(),
            //                                        UserApplicationData.Dispatch.Code));

            //            double result = 0;

            //            if (operatorsNotReady > 0 && operatorsWorkingNow > 0)
            //                result = operatorsNotReady / operatorsWorkingNow;

            //            SaveIndicatorsValues(IndicatorClassData.System, result);
            #endregion doc
        }

        private void CalculateGroup()
        {
            #region new
            string sql = @"SELECT		CONVERT(FLOAT,TOTALGRUPO.TOTALNODISGRUPO)/ CONVERT(FLOAT,CASE TOTAL.TOTALONLINE WHEN 0 THEN 1 ELSE TOTAL.TOTALONLINE END) 
AS PORCENTAJE, 
TOTAL.SUPERONLINE AS SUPERVISOR
FROM 
			(			
			SELECT		COUNT(DISTINCT OPA.OPERATOR_CODE) AS TOTALONLINE, OPE.CODE AS SUPERONLINE
			FROM		OPERATOR AS OPE
						,OPERATOR_ASSIGN AS OPA
						,SESSION_HISTORY AS SEH
			WHERE		OPA.DELETED_ID IS NULL
			
			AND			(
							(
							
							GETDATE()
							BETWEEN OPA.START_DATE AND OPA.END_DATE
							)						
						)
			AND			OPE.CODE = OPA.SUPERVISOR_CODE
			AND			OPA.OPERATOR_CODE = SEH.USER_ACCOUNT_CODE
			AND			SEH.IS_LOGGED_IN = 'TRUE'
			AND			SEH.END_DATE_LOGIN IS NULL
			GROUP BY	OPE.CODE
			) AS TOTAL,
			(
			SELECT		COUNT(DISTINCT OPA.OPERATOR_CODE) AS TOTALNODISGRUPO, OPE.CODE AS SUPERNODIS
			FROM        OPERATOR_ASSIGN AS OPA
						,OPERATOR AS OPE
						,SESSION_HISTORY AS SEH
						,SESSION_STATUS_HISTORY AS STH
			WHERE		OPA.DELETED_ID IS NULL
			
			AND			(
							(
							
							GETDATE()
							BETWEEN OPA.START_DATE AND OPA.END_DATE
							)						
						)
			AND			OPA.SUPERVISOR_CODE = OPE.CODE
			AND			OPE.CODE <> 1
			AND			OPA.OPERATOR_CODE = SEH.USER_ACCOUNT_CODE
			AND			SEH.IS_LOGGED_IN = 'TRUE'
			AND			SEH.END_DATE_LOGIN IS NULL
			AND			SEH.USER_APPLICATION_CODE = 3
			AND			SEH.CODE = STH.SESSION_HISTORY_CODE
			AND			STH.END_DATE IS NULL
			AND			STH.OPERATOR_STATUS_CODE > 2
			GROUP BY	OPE.CODE                    
			) AS TOTALGRUPO
WHERE		TOTALGRUPO.SUPERNODIS=TOTAL.SUPERONLINE
";

            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);



            foreach (object[] item in data)
            {

                ObjectData ob = new ObjectData();
                ob.Code = int.Parse((item[1].ToString()));
                SaveIndicatorsValues(ob, IndicatorClassData.Department, double.Parse((item[0].ToString())));

            }
            #endregion new

            #region doc

            //IList supervisors = OperatorScheduleManager.GetSupervisorsWorkingNowByApplication("DispatchSupervisorName");            
            //foreach (int supervisorCode in supervisors)
            //{
            //    IList operators = OperatorScheduleManager.GetOperatorsBySupervisor(supervisorCode);

            //    double operatorsNotReadyNow = 0;

            //    double operatorsWorkingNow = 0;

            //    foreach (OperatorData operatorData in operators)
            //    {
            //        long operatorNotReady = (long)SmartCadDatabase.SearchBasicObject(
            //                                        SmartCadHqls.GetCustomHql(
            //                                        SmartCadHqls.IsLoggedInOperatorByApplicationOperatorNotStatus,
            //                                        operatorData.Code,
            //                                        UserApplicationData.Dispatch.Code,
            //                                        OperatorStatusData.Busy.Code,
            //                                        true.ToString().ToLower()));

            //        if (operatorNotReady > 0)
            //            operatorsNotReadyNow += 1;

            //        long operatorConnected = (long)SmartCadDatabase.SearchBasicObject(
            //                                    SmartCadHqls.GetCustomHql(
            //                                    SmartCadHqls.GetCountLoggedOperatorByApplication,
            //                                    true.ToString().ToLower(),
            //                                    UserApplicationData.Dispatch.Code,
            //                                    operatorData.Code));

            //        if (operatorConnected > 0)
            //            operatorsWorkingNow += 1;
            //    }

            //    double result = 0;

            //    if (operatorsNotReadyNow > 0 && operatorsWorkingNow > 0)
            //        result = operatorsNotReadyNow / operatorsWorkingNow;

            //    ObjectData objectData = new ObjectData();
            //    objectData.Code = supervisorCode;
            //    SaveIndicatorsValues(objectData, IndicatorClassData.Group, result);
            #endregion doc
        }

    }
}

