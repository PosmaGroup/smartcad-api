using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using SmartCadCore.Model; 

namespace SmartCadCore.Core.Indicators
{
    public class D42IndicatorValuesCalculator : IndicatorValuesCalculator
    {
        protected override string GetIndicator()
        {
            return "D42";
        }

        protected override void Calculate()
        {
            CalculateSystem();
            CalculateDepartment();
            CalculateGroup();
        }

        private void CalculateGroup()
        {
            #region new
            string sql = @" SELECT			OPE.CODE,AGRUPADO.TIPO AS TIPO, COUNT(AGRUPADO.CODIGO) TOTAL, 				
              (Select  status.Code  from INCIDENT_NOTIFICATION_STATUS status where status.CODE = 
														INOT.INCIDENT_NOTIFICATION_STATUS_CODE)AS STATUS
                           
									FROM			(
											
				                            SELECT
				                            INC_1.CODE AS CODIGO,
				                            TIPO = RTRIM(REPLACE(REPLACE(o.list,'<FRIENDLY_NAME>',''),'</FRIENDLY_NAME>',', '))
				                            FROM
				                            INCIDENT AS INC_1
				                            CROSS APPLY
				                            (
				                            SELECT		INTY.FRIENDLY_NAME
				                            FROM		INCIDENT AS INC
							                            ,REPORT_BASE AS REB
							                            ,REPORT_BASE_INCIDENT_TYPE AS RBIT
							                            ,INCIDENT_TYPE AS INTY
				                            WHERE		INC.IS_EMERGENCY = 'TRUE'
				                            AND			INC.CODE = REB.INCIDENT_CODE
				                            AND			REB.CODE = RBIT.REPORT_BASE_CODE
				                            AND			RBIT.INCIDENT_TYPE_CODE = INTY.CODE
				                            AND			INC.CODE = INC_1.CODE
				                            ORDER BY INTY.FRIENDLY_NAME
				                            FOR XML PATH('')
				                            ) o (list)
				                            WHERE INC_1.IS_EMERGENCY = 'TRUE'
				                            ) AS AGRUPADO

				                            ,REPORT_BASE AS REB
				                            ,INCIDENT_NOTIFICATION AS INOT				                          
											,OPERATOR_ASSIGN AS OPA
											,OPERATOR AS OPE
											
                            WHERE			INOT.REPORT_BASE_CODE = REB.CODE
                            AND				AGRUPADO.CODIGO = REB.INCIDENT_CODE                            
							AND			INOT.INCIDENT_NOTIFICATION_STATUS_CODE<>6
							AND			INOT.INCIDENT_NOTIFICATION_STATUS_CODE<>9                          
							AND			OPA.OPERATOR_CODE = INOT.USER_ACCOUNT_CODE
							AND			OPA.DELETED_ID IS NULL
							AND			(
									(				
									GETDATE()
									BETWEEN OPA.START_DATE AND OPA.END_DATE
									)			
										)
							AND			OPA.SUPERVISOR_CODE = OPE.CODE

                            GROUP BY		OPE.CODE, AGRUPADO.TIPO, INOT.INCIDENT_NOTIFICATION_STATUS_CODE
order by ope.code";




            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);

            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
            List<double> list = new List<double>();
            int supervisorCode = 0;
            string incidentTypes = "";
            double currentTotal = 0;
            Dictionary<int, double> departmentCodes = new Dictionary<int, double>();
            foreach (object[] item in data)
            {
                if (departmentCodes.ContainsKey(int.Parse(item[0].ToString())) == false)
                {
                    departmentCodes.Add(int.Parse(item[0].ToString()), int.Parse(item[2].ToString()));
                }
                else
                {
                    departmentCodes[int.Parse(item[0].ToString())] += int.Parse(item[2].ToString());
                }

            }

            foreach (object[] item in data)
            {
                if (incidentTypes != "" && incidentTypes !=
                   item[1].ToString().Substring(0, item[1].ToString().Length - 1))
                {

                    list[10] = list[0] + list[2] + list[4] + list[6] + list[8];
                    list[1] = (list[0] / list[10]) * 100;
                    list[3] = (list[2] / list[10]) * 100;
                    list[5] = (list[4] / list[10]) * 100;
                    list[7] = (list[6] / list[10]) * 100;
                    list[9] = (list[8] / list[10]) * 100;
                    list[11] = (list[10] * 100) / departmentCodes[supervisorCode];
                    result.Add(incidentTypes, list);
                    list = new List<double>();
                    currentTotal = 0;
                }

                if (supervisorCode != 0 && supervisorCode != int.Parse(item[0].ToString()))
                {
                    if (incidentTypes ==
                   item[1].ToString().Substring(0, item[1].ToString().Length - 1))
                    {
                        list[10] = list[0] + list[2] + list[4] + list[6] + list[8];
                        if (list[10] > 0)
                        {
                            list[1] = (list[0] / list[10]) * 100;
                            list[3] = (list[2] / list[10]) * 100;
                            list[5] = (list[4] / list[10]) * 100;
                            list[7] = (list[6] / list[10]) * 100;
                            list[9] = (list[8] / list[10]) * 100;
                            list[11] = (list[10] * 100) / departmentCodes[supervisorCode];
                            result.Add(incidentTypes, list);
                            currentTotal = 0;
                        }
                    }
                    ObjectData od1 = new ObjectData();
                    od1.Code = supervisorCode;
                    SaveIndicatorMultiplesValues(od1, IndicatorClassData.Group, result);

                    result.Clear();
                    incidentTypes = "";
                    list = new List<double>();

                }

                incidentTypes = item[1].ToString().Substring(0, item[1].ToString().Length - 1);
                currentTotal += (double.Parse(item[2].ToString()));
                supervisorCode = (int)item[0];
                if (list.Count == 0)
                {
                    list.Add(0);
                    list.Add(0);
                    list.Add(0);
                    list.Add(0);
                    list.Add(0);
                    list.Add(0);
                    list.Add(0);
                    list.Add(0);
                    list.Add(0);
                    list.Add(0);
                    list.Add(0);
                    list.Add(0);
                }
                if (item[3].ToString() == IncidentNotificationStatusData.Assigned.Code.ToString())
                {
                    list[0] += (double.Parse(item[2].ToString()));
                }
                else if (item[3].ToString() == IncidentNotificationStatusData.Pending.Code.ToString())
                {
                    list[2] += (double.Parse(item[2].ToString()));
                }
                else if (item[3].ToString() == IncidentNotificationStatusData.Delayed.Code.ToString())
                {
                    list[4] += (double.Parse(item[2].ToString()));
                }
                else if ((item[3].ToString() == IncidentNotificationStatusData.InProgress.Code.ToString()) ||
                    (item[3].ToString() == IncidentNotificationStatusData.ManualSupervisor.Code.ToString()) ||
                    (item[3].ToString() == IncidentNotificationStatusData.AutomaticSupervisor.Code.ToString()))
                {
                    list[6] += (double.Parse(item[2].ToString()));
                }
                else if (item[3].ToString() == IncidentNotificationStatusData.New.Code.ToString())
                {
                    list[8] += (double.Parse(item[2].ToString()));
                }
                if (item == data[data.Count - 1])
                {
                    list[10] = list[0] + list[2] + list[4] + list[6] + list[8];
                    list[1] = (list[0] / list[10]) * 100;
                    list[3] = (list[2] / list[10]) * 100;
                    list[5] = (list[4] / list[10]) * 100;
                    list[7] = (list[6] / list[10]) * 100;
                    list[9] = (list[8] / list[10]) * 100;
                    list[11] = (list[10] * 100) / departmentCodes[supervisorCode];
                    result.Add(incidentTypes, list);
                    ObjectData od1 = new ObjectData();
                    od1.Code = supervisorCode;
                    SaveIndicatorMultiplesValues(od1, IndicatorClassData.Group, result);
                }
            }
            #endregion new

            #region doc
            //                IList supervisors = OperatorScheduleManager.GetSupervisorsWorkingNowByApplication("DispatchSupervisorName");

            //                foreach (int supervisor in supervisors)
            //                {
            //                    Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
            //                    double totalElementsResult = 0;
            //                    IList operators = OperatorScheduleManager.GetOperatorsBySupervisor(supervisor);
            //                    foreach (OperatorData operatorData in operators)
            //                    {
            //                        string hql = @"select distinct data.Code, sit.FriendlyName 
            //                           from IncidentData data inner join
            //                                data.SetReportBaseList srl inner join
            //                                srl.SetIncidentTypes sit inner join
            //                                srl.IncidentNotifications inot
            //                           where inot.EndDate is null and 
            //                            inot.DispatchOperator.Code = {0}
            //                           order by data.Code, sit.FriendlyName";
            //                        IList incidentInfo = (IList)SmartCadDatabase.SearchObjects(
            //                            SmartCadHqls.GetCustomHql(
            //                            hql, operatorData.Code));

            //                        Dictionary<string, List<int>> incidentTypeNamesByIncident = new Dictionary<string, List<int>>();
            //                        string itnames = "";
            //                        int code = 0;
            //                        foreach (object[] item in incidentInfo)
            //                        {
            //                            if (code != 0 && code != int.Parse(item[0].ToString()))
            //                            {
            //                                if (incidentTypeNamesByIncident.ContainsKey(itnames) == false)
            //                                {
            //                                    List<int> codes = new List<int>();
            //                                    codes.Add(code);
            //                                    incidentTypeNamesByIncident.Add(itnames, codes);
            //                                }
            //                                else
            //                                {
            //                                    List<int> codes = incidentTypeNamesByIncident[itnames];
            //                                    codes.Add(code);
            //                                }
            //                                itnames = string.Empty;
            //                            }
            //                            code = int.Parse(item[0].ToString());
            //                            string friendlyName = item[1].ToString();
            //                            if (itnames.Contains(friendlyName) == false)
            //                            {
            //                                if (itnames.Length == 0)
            //                                {
            //                                    itnames = friendlyName;
            //                                }
            //                                else
            //                                {
            //                                    itnames = string.Concat(itnames, ", ", friendlyName);
            //                                }
            //                            }
            //                        }
            //                        if (itnames != string.Empty)
            //                        {
            //                            if (incidentTypeNamesByIncident.ContainsKey(itnames) == false)
            //                            {
            //                                List<int> codes = new List<int>();
            //                                codes.Add(code);
            //                                incidentTypeNamesByIncident.Add(itnames, codes);
            //                            }
            //                            else
            //                            {
            //                                List<int> codes = incidentTypeNamesByIncident[itnames];
            //                                codes.Add(code);
            //                            }
            //                        }
            //                        StringBuilder incidentStatus = new StringBuilder();
            //                        incidentStatus.Append("(");
            //                        incidentStatus.Append(IncidentNotificationStatusData.Assigned.Code);
            //                        incidentStatus.Append(", ");
            //                        incidentStatus.Append(IncidentNotificationStatusData.Pending.Code);
            //                        incidentStatus.Append(", ");
            //                        incidentStatus.Append(IncidentNotificationStatusData.Delayed.Code);
            //                        incidentStatus.Append(", ");
            //                        incidentStatus.Append(IncidentNotificationStatusData.InProgress.Code);
            //                        incidentStatus.Append(", ");
            //                        incidentStatus.Append(IncidentNotificationStatusData.New.Code);
            //                        incidentStatus.Append(")");

            //                        string hql3 = @"select  count(*)
            //                                    from IncidentNotificationData notif
            //                                    where notif.Status.Code in {0} and
            //                                     notif.DispatchOperator.Code = {1}";
            //                        totalElementsResult += Convert.ToDouble(SmartCadDatabase.SearchBasicObject(
            //                                   SmartCadHqls.GetCustomHql(
            //                                   hql3,
            //                                   incidentStatus, operatorData.Code)));
            //                        foreach (KeyValuePair<string, List<int>> kvp in incidentTypeNamesByIncident)
            //                        {

            //                            StringBuilder sb = new StringBuilder();
            //                            sb.Append("(");
            //                            foreach (int codeValue in kvp.Value)
            //                            {
            //                                if (sb.Length == 1)
            //                                {
            //                                    sb.Append(codeValue);
            //                                }
            //                                else
            //                                {
            //                                    sb.Append(", ");
            //                                    sb.Append(codeValue);
            //                                }
            //                            }
            //                            sb.Append(")");

            //                            string hql2 = @"select notif.Status.FriendlyName, count(*)
            //                                    from IncidentNotificationData notif
            //                                    inner join notif.ReportBase repoBase
            //                                    inner join repoBase.Incident inc
            //                                    where notif.EndDate is null and
            //                                        inc.Code in {0} and
            //                                    notif.Status.Code in {1} and 
            //                            notif.DispatchOperator.Code = {2}                                  
            //                                     group by notif.Status.FriendlyName";

            //                            incidentInfo = (IList)
            //                                SmartCadDatabase.SearchBasicObjects(
            //                                SmartCadHqls.GetCustomHql(hql2,
            //                                sb.ToString(), incidentStatus.ToString(), operatorData.Code));
            //                            List<double> list = new List<double>();
            //                            list.Add(0);
            //                            list.Add(0);
            //                            list.Add(0);
            //                            list.Add(0);
            //                            list.Add(0);
            //                            list.Add(0);
            //                            list.Add(0);
            //                            list.Add(0);
            //                            list.Add(0);
            //                            list.Add(0);
            //                            list.Add(0);
            //                            list.Add(0);

            //                            long totalRow = 0;
            //                            //Get totalRow
            //                            foreach (object[] item in incidentInfo)
            //                            {
            //                                totalRow += ((long)item[1]);
            //                            }
            //                            foreach (object[] item in incidentInfo)
            //                            {
            //                                if (item[0].ToString() == IncidentNotificationStatusData.Assigned.FriendlyName)
            //                                {
            //                                    list[0] = ((long)item[1]);
            //                                    if (totalRow > 0)
            //                                        list[1] = ((double)((long)item[1] * 100) / totalRow);
            //                                    else
            //                                        list[1] = 0;
            //                                }
            //                                else if (item[0].ToString() == IncidentNotificationStatusData.Pending.FriendlyName)
            //                                {
            //                                    list[2] = ((long)item[1]);
            //                                    if (totalRow > 0)
            //                                        list[3] = ((double)((long)item[1] * 100) / totalRow);
            //                                    else
            //                                        list[3] = 0;
            //                                }
            //                                else if (item[0].ToString() == IncidentNotificationStatusData.Delayed.FriendlyName)
            //                                {
            //                                    list[4] = ((long)item[1]);
            //                                    if (totalRow > 0)
            //                                        list[5] = ((double)((long)item[1] * 100) / totalRow);
            //                                    else
            //                                        list[5] = 0;
            //                                }
            //                                else if (item[0].ToString() == IncidentNotificationStatusData.InProgress.FriendlyName)
            //                                {
            //                                    list[6] = ((long)item[1]);
            //                                    if (totalRow > 0)
            //                                        list[7] = ((double)((long)item[1] * 100) / totalRow);
            //                                    else
            //                                        list[7] = 0;
            //                                }
            //                                else if (item[0].ToString() == IncidentNotificationStatusData.New.FriendlyName)
            //                                {
            //                                    list[8] = ((long)item[1]);
            //                                    if (totalRow > 0)
            //                                        list[9] = ((double)((long)item[1] * 100) / totalRow);
            //                                    else
            //                                        list[9] = 0;
            //                                }
            //                            }
            //                            list[10] = totalRow;
            //                            if (result.ContainsKey(kvp.Key) == false)
            //                            {
            //                                result.Add(kvp.Key, list);
            //                            }
            //                            else
            //                            {
            //                                List<double> auxList = new List<double>();
            //                                result.TryGetValue(kvp.Key, out auxList);
            //                                for (int i = 0; i < list.Count; i++)
            //                                {
            //                                    list[i] += auxList[i];
            //                                }
            //                                if (list[10] > 0)
            //                                {
            //                                    list[1] = (list[0] * 100) / list[10];
            //                                    list[3] = (list[2] * 100) / list[10];
            //                                    list[5] = (list[4] * 100) / list[10];
            //                                    list[7] = (list[6] * 100) / list[10];
            //                                    list[9] = (list[8] * 100) / list[10];
            //                                }
            //                                else
            //                                {
            //                                    list[1] = 0;
            //                                    list[3] = 0;
            //                                    list[5] = 0;
            //                                    list[7] = 0;
            //                                    list[9] = 0;
            //                                }


            //                                result.Remove(kvp.Key);
            //                                result.Add(kvp.Key, list);
            //                            }
            //                        }
            //                    }

            //                    foreach (KeyValuePair<string, List<double>> pair in result)
            //                    {
            //                        if (totalElementsResult > 0)
            //                            pair.Value[11] = (pair.Value[10] * 100) / totalElementsResult;
            //                        else
            //                            pair.Value[11] = 0;
            //                    }
            //                    ObjectData ob = new ObjectData();
            //                    ob.Code = supervisor;
            //                    SaveIndicatorMultiplesValues(ob, IndicatorClassData.Group, result);
            //                    result.Clear();
            //                    totalElementsResult = 0;
            //                }
            #endregion doc
        }
        private void CalculateDepartment()
        {

            #region new
            string sql = @"  SELECT			DETY.CODE AS ORGANISMO, AGRUPADO.TIPO AS TIPO, COUNT(AGRUPADO.CODIGO) TOTAL, 				
(SELECT		COUNT(INOT.INCIDENT_NOTIFICATION_STATUS_CODE) 	
				FROM		INCIDENT_NOTIFICATION AS INOT							
				WHERE		INOT.INCIDENT_NOTIFICATION_STATUS_CODE<>6
				AND			INOT.INCIDENT_NOTIFICATION_STATUS_CODE<>9				
				AND			DETY.CODE = INOT.DEPARTMENT_TYPE_CODE				
				) as TOTAL_INCIDENT_NOTIF,

              (Select  status.Code  from INCIDENT_NOTIFICATION_STATUS status where status.CODE = 
														INOT.INCIDENT_NOTIFICATION_STATUS_CODE)AS STATUS

                           
									FROM			(
				                            SELECT
				                            INC_1.CODE AS CODIGO,
				                            TIPO = RTRIM(REPLACE(REPLACE(o.list,'<FRIENDLY_NAME>',''),'</FRIENDLY_NAME>',', '))
				                            FROM
				                            INCIDENT AS INC_1
				                            CROSS APPLY
				                            (
				                            SELECT		INTY.FRIENDLY_NAME
				                            FROM		INCIDENT AS INC
							                            ,REPORT_BASE AS REB
							                            ,REPORT_BASE_INCIDENT_TYPE AS RBIT
							                            ,INCIDENT_TYPE AS INTY
				                            WHERE		INC.IS_EMERGENCY = 'TRUE'
				                            AND			INC.CODE = REB.INCIDENT_CODE
				                            AND			REB.CODE = RBIT.REPORT_BASE_CODE
				                            AND			RBIT.INCIDENT_TYPE_CODE = INTY.CODE
				                            AND			INC.CODE = INC_1.CODE
				                            ORDER BY INTY.FRIENDLY_NAME
				                            FOR XML PATH('')
				                            ) o (list)
				                            WHERE INC_1.IS_EMERGENCY = 'TRUE'
				                            ) AS AGRUPADO
				                            ,REPORT_BASE AS REB
				                            ,INCIDENT_NOTIFICATION AS INOT
				                            ,DEPARTMENT_TYPE AS DETY
                            WHERE			INOT.REPORT_BASE_CODE = REB.CODE
                            AND				AGRUPADO.CODIGO = REB.INCIDENT_CODE                            
							AND			INOT.INCIDENT_NOTIFICATION_STATUS_CODE<>6
							AND			INOT.INCIDENT_NOTIFICATION_STATUS_CODE<>9
                            AND				INOT.DEPARTMENT_TYPE_CODE = DETY.CODE
                            GROUP BY		DETY.CODE, AGRUPADO.TIPO, INOT.INCIDENT_NOTIFICATION_STATUS_CODE
							order by dety.code";




            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);

            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
            List<double> list = new List<double>();
            int departmentCode = 0;
            string incidentTypes = "";
            double currentTotal = 0;
            foreach (object[] item in data)
            {
                if (incidentTypes != "" && incidentTypes !=
                   item[1].ToString().Substring(0, item[1].ToString().Length - 1))
                {
                    
                    list[10] = list[0] + list[2] + list[4] + list[6] + list[8];
                    list[1] = (list[0] / list[10]) * 100;
                    list[3] = (list[2] / list[10]) * 100;
                    list[5] = (list[4] / list[10]) * 100;
                    list[7] = (list[6] / list[10]) * 100;
                    list[9] = (list[8] / list[10]) * 100;
                    list[11] = (list[10] * 100) / currentTotal;
                    result.Add(incidentTypes, list);
                    list = new List<double>();
                }

                if (departmentCode != 0 && departmentCode != int.Parse(item[0].ToString()))
                {
                    if (incidentTypes ==
                   item[1].ToString().Substring(0, item[1].ToString().Length - 1))
                    {
                        list[10] = list[0] + list[2] + list[4] + list[6] + list[8];
                        if (list[10] > 0)
                        {
                            list[1] = (list[0] / list[10]) * 100;
                            list[3] = (list[2] / list[10]) * 100;
                            list[5] = (list[4] / list[10]) * 100;
                            list[7] = (list[6] / list[10]) * 100;
                            list[9] = (list[8] / list[10]) * 100;
                            list[11] = (list[10] * 100) / currentTotal;
                            result.Add(incidentTypes, list);
                        }
                    }
                    ObjectData od1 = new ObjectData();
                    od1.Code = departmentCode;
                    SaveIndicatorMultiplesValues(od1, IndicatorClassData.Department, result);

                    result.Clear();
                    incidentTypes = "";
                    list = new List<double>();
                }

                incidentTypes = item[1].ToString().Substring(0, item[1].ToString().Length - 1);
                currentTotal = (double.Parse(item[3].ToString()));
                departmentCode = (int)item[0];
                if (list.Count == 0)
                {
                    list.Add(0);
                    list.Add(0);
                    list.Add(0);
                    list.Add(0);
                    list.Add(0);
                    list.Add(0);
                    list.Add(0);
                    list.Add(0);
                    list.Add(0);
                    list.Add(0);
                    list.Add(0);
                    list.Add(0);
                }
                if (item[4].ToString() == IncidentNotificationStatusData.Assigned.Code.ToString())
                {
                    list[0] += (double.Parse(item[2].ToString()));
                }
                else if (item[4].ToString() == IncidentNotificationStatusData.Pending.Code.ToString())
                {
                    list[2] += (double.Parse(item[2].ToString()));
                }
                else if (item[4].ToString() == IncidentNotificationStatusData.Delayed.Code.ToString())
                {
                    list[4] += (double.Parse(item[2].ToString()));
                }
                else if ((item[4].ToString() == IncidentNotificationStatusData.InProgress.Code.ToString())||
                    (item[4].ToString() == IncidentNotificationStatusData.ManualSupervisor.Code.ToString())||
                    (item[4].ToString() == IncidentNotificationStatusData.AutomaticSupervisor.Code.ToString()))
                {
                    list[6] += (double.Parse(item[2].ToString()));
                }
                else if (item[4].ToString() == IncidentNotificationStatusData.New.Code.ToString())
                {
                    list[8] += (double.Parse(item[2].ToString()));
                }
                if (item == data[data.Count - 1])
                {
                    list[10] = list[0] + list[2] + list[4] + list[6] + list[8];
                    list[1] = (list[0] / list[10]) * 100;
                    list[3] = (list[2] / list[10]) * 100;
                    list[5] = (list[4] / list[10]) * 100;
                    list[7] = (list[6] / list[10]) * 100;
                    list[9] = (list[8] / list[10]) * 100;
                    list[11] = (list[10] * 100) / currentTotal;
                    result.Add(incidentTypes, list);
                    ObjectData od1 = new ObjectData();
                    od1.Code = departmentCode;
                    SaveIndicatorMultiplesValues(od1, IndicatorClassData.Department, result);
                }
            }
            #endregion new

            #region doc
//            IList departmentCodes = (IList)SmartCadDatabase.SearchBasicObjects(
//   SmartCadHqls.GetDepartmentsTypeCodes);
//            foreach (int departmentCode in departmentCodes)
//            {
//                string hql = @"select distinct data.Code, sit.FriendlyName 
//                           from IncidentData data inner join
//                                data.SetReportBaseList srl inner join
//                                srl.SetIncidentTypes sit inner join
//                                srl.IncidentNotifications inot
//                           where inot.EndDate is null and 
//                            inot.DepartmentType.Code = {0}
//                           order by data.Code, sit.FriendlyName";
//                IList incidentInfo = (IList)SmartCadDatabase.SearchObjects(
//                    SmartCadHqls.GetCustomHql(
//                    hql, departmentCode));

//                Dictionary<string, List<int>> incidentTypeNamesByIncident = new Dictionary<string, List<int>>();
//                string itnames = "";
//                int code = 0;
//                foreach (object[] item in incidentInfo)
//                {
//                    if (code != 0 && code != int.Parse(item[0].ToString()))
//                    {
//                        if (incidentTypeNamesByIncident.ContainsKey(itnames) == false)
//                        {
//                            List<int> codes = new List<int>();
//                            codes.Add(code);
//                            incidentTypeNamesByIncident.Add(itnames, codes);
//                        }
//                        else
//                        {
//                            List<int> codes = incidentTypeNamesByIncident[itnames];
//                            codes.Add(code);
//                        }
//                        itnames = string.Empty;
//                    }
//                    code = int.Parse(item[0].ToString());
//                    string friendlyName = item[1].ToString();
//                    if (itnames.Contains(friendlyName) == false)
//                    {
//                        if (itnames.Length == 0)
//                        {
//                            itnames = friendlyName;
//                        }
//                        else
//                        {
//                            itnames = string.Concat(itnames, ", ", friendlyName);
//                        }
//                    }
//                }
//                if (itnames != string.Empty)
//                {
//                    if (incidentTypeNamesByIncident.ContainsKey(itnames) == false)
//                    {
//                        List<int> codes = new List<int>();
//                        codes.Add(code);
//                        incidentTypeNamesByIncident.Add(itnames, codes);
//                    }
//                    else
//                    {
//                        List<int> codes = incidentTypeNamesByIncident[itnames];
//                        codes.Add(code);
//                    }
//                }
//                StringBuilder incidentStatus = new StringBuilder();
//                incidentStatus.Append("(");
//                incidentStatus.Append(IncidentNotificationStatusData.Assigned.Code);
//                incidentStatus.Append(", ");
//                incidentStatus.Append(IncidentNotificationStatusData.Pending.Code);
//                incidentStatus.Append(", ");
//                incidentStatus.Append(IncidentNotificationStatusData.Delayed.Code);
//                incidentStatus.Append(", ");
//                incidentStatus.Append(IncidentNotificationStatusData.InProgress.Code);
//                incidentStatus.Append(", ");
//                incidentStatus.Append(IncidentNotificationStatusData.New.Code);
//                incidentStatus.Append(")");

//                string hql3 = @"select  count(*)
//                                    from IncidentNotificationData notif
//                                    where notif.Status.Code in {0} and
//                                    notif.DepartmentType.Code = {1}";
//                double totalDispatchOrderByStatus = Convert.ToDouble(SmartCadDatabase.SearchBasicObject(
//                           SmartCadHqls.GetCustomHql(
//                           hql3,
//                           incidentStatus, departmentCode)));
//                foreach (KeyValuePair<string, List<int>> kvp in incidentTypeNamesByIncident)
//                {

//                    StringBuilder sb = new StringBuilder();
//                    sb.Append("(");
//                    foreach (int codeValue in kvp.Value)
//                    {
//                        if (sb.Length == 1)
//                        {
//                            sb.Append(codeValue);
//                        }
//                        else
//                        {
//                            sb.Append(", ");
//                            sb.Append(codeValue);
//                        }
//                    }
//                    sb.Append(")");

//                    string hql2 = @"select notif.Status.FriendlyName, count(*)
//                                    from IncidentNotificationData notif
//                                    inner join notif.ReportBase repoBase
//                                    inner join repoBase.Incident inc
//                                    where notif.EndDate is null and
//                                        inc.Code in {0} and
//                                    notif.Status.Code in {1} and 
//                            notif.DepartmentType.Code = {2}                                  
//                                     group by notif.Status.FriendlyName";

//                    incidentInfo = (IList)
//                        SmartCadDatabase.SearchBasicObjects(
//                        SmartCadHqls.GetCustomHql(hql2,
//                        sb.ToString(), incidentStatus.ToString(), departmentCode));

//                    Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//                    List<double> list = new List<double>();
//                    list.Add(0);
//                    list.Add(0);
//                    list.Add(0);
//                    list.Add(0);
//                    list.Add(0);
//                    list.Add(0);
//                    list.Add(0);
//                    list.Add(0);
//                    list.Add(0);
//                    list.Add(0);
//                    list.Add(0);
//                    list.Add(0);

//                    long totalRow = 0;
//                    //Get totalRow
//                    foreach (object[] item in incidentInfo)
//                    {
//                        totalRow += ((long)item[1]);
//                    }
//                    foreach (object[] item in incidentInfo)
//                    {
//                        if (item[0].ToString() == IncidentNotificationStatusData.Assigned.FriendlyName)
//                        {
//                            list[0] = ((long)item[1]);
//                            if (totalRow > 0)
//                                list[1] = ((double)((long)item[1] * 100) / totalRow);
//                            else
//                                list[1] = 0;
//                        }
//                        else if (item[0].ToString() == IncidentNotificationStatusData.Pending.FriendlyName)
//                        {
//                            list[2] = ((long)item[1]);
//                            if (totalRow > 0)
//                                list[3] = ((double)((long)item[1] * 100) / totalRow);
//                            else
//                                list[3] = 0;
//                        }
//                        else if (item[0].ToString() == IncidentNotificationStatusData.Delayed.FriendlyName)
//                        {
//                            list[4] = ((long)item[1]);
//                            if (totalRow > 0)
//                                list[5] = ((double)((long)item[1] * 100) / totalRow);
//                            else
//                                list[5] = 0;
//                        }
//                        else if (item[0].ToString() == IncidentNotificationStatusData.InProgress.FriendlyName)
//                        {
//                            list[6] = ((long)item[1]);
//                            if (totalRow > 0)
//                                list[7] = ((double)((long)item[1] * 100) / totalRow);
//                            else
//                                list[7] = 0;
//                        }
//                        else if (item[0].ToString() == IncidentNotificationStatusData.New.FriendlyName)
//                        {
//                            list[8] = ((long)item[1]);
//                            if (totalRow > 0)
//                                list[9] = ((double)((long)item[1] * 100) / totalRow);
//                            else
//                                list[9] = 0;
//                        }
//                    }
//                    list[10] = totalRow;
//                    if (totalDispatchOrderByStatus > 0)
//                        list[11] = (totalRow * 100) / totalDispatchOrderByStatus;
//                    else
//                        list[11] = 0;
//                    result.Add(kvp.Key, list);
//                    ObjectData ob = new ObjectData();
//                    ob.Code = departmentCode;
//                    SaveIndicatorMultiplesValues(ob, IndicatorClassData.Department, result);
//                    result.Clear();
//                }
//            }
            #endregion doc
        }
        private void CalculateSystem()
        {
            #region new
            string sql = @"  SELECT			 AGRUPADO.TIPO AS TIPO, COUNT(AGRUPADO.CODIGO) TOTAL, 
				


(SELECT		COUNT(INOT.INCIDENT_NOTIFICATION_STATUS_CODE) 	
				FROM		INCIDENT_NOTIFICATION AS INOT							
				WHERE		INOT.INCIDENT_NOTIFICATION_STATUS_CODE<>6
				AND			INOT.INCIDENT_NOTIFICATION_STATUS_CODE<>9											
				) as TOTAL_INCIDENT_NOTIF,

              (Select  status.Code  from INCIDENT_NOTIFICATION_STATUS status where status.CODE = 
														INOT.INCIDENT_NOTIFICATION_STATUS_CODE)AS STATUS

                           
									FROM			(
				                            SELECT
				                            INC_1.CODE AS CODIGO,
				                            TIPO = RTRIM(REPLACE(REPLACE(o.list,'<FRIENDLY_NAME>',''),'</FRIENDLY_NAME>',', '))
				                            FROM
				                            INCIDENT AS INC_1
				                            CROSS APPLY
				                            (
				                            SELECT		INTY.FRIENDLY_NAME
				                            FROM		INCIDENT AS INC
							                            ,REPORT_BASE AS REB
							                            ,REPORT_BASE_INCIDENT_TYPE AS RBIT
							                            ,INCIDENT_TYPE AS INTY
				                            WHERE		INC.IS_EMERGENCY = 'TRUE'
				                            AND			INC.CODE = REB.INCIDENT_CODE
				                            AND			REB.CODE = RBIT.REPORT_BASE_CODE
				                            AND			RBIT.INCIDENT_TYPE_CODE = INTY.CODE
				                            AND			INC.CODE = INC_1.CODE
				                            ORDER BY INTY.FRIENDLY_NAME
				                            FOR XML PATH('')
				                            ) o (list)
				                            WHERE INC_1.IS_EMERGENCY = 'TRUE'
				                            ) AS AGRUPADO
				                            ,REPORT_BASE AS REB
				                            ,INCIDENT_NOTIFICATION AS INOT				                          
                            WHERE			INOT.REPORT_BASE_CODE = REB.CODE
                            AND				AGRUPADO.CODIGO = REB.INCIDENT_CODE                            
							AND			INOT.INCIDENT_NOTIFICATION_STATUS_CODE<>6
							AND			INOT.INCIDENT_NOTIFICATION_STATUS_CODE<>9                          
                            GROUP BY	AGRUPADO.TIPO, INOT.INCIDENT_NOTIFICATION_STATUS_CODE
                            order by  AGRUPADO.TIPO";




            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);

            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
            List<double> list = new List<double>();         
            string incidentTypes = "";
            double currentTotal = 0;
            foreach (object[] item in data)
            {
                if (incidentTypes != "" && incidentTypes !=
                   item[0].ToString().Substring(0, item[0].ToString().Length - 1))
                {
                    list[10] = list[0] + list[2] + list[4] + list[6] + list[8];
                    list[1] = (list[0] / list[10]) * 100;
                    list[3] = (list[2] / list[10]) * 100;
                    list[5] = (list[4] / list[10]) * 100;
                    list[7] = (list[6] / list[10]) * 100;
                    list[9] = (list[8] / list[10]) * 100;
                    list[11] = (list[10] * 100) / currentTotal;
                    result.Add(incidentTypes, list);
                    SaveIndicatorMultiplesValues(null, IndicatorClassData.System, result);
                    list = new List<double>();
                    result.Clear();
                    incidentTypes = "";
                }              
                incidentTypes = item[0].ToString().Substring(0, item[0].ToString().Length - 1);   
                currentTotal =(double.Parse(item[2].ToString()));
                if (list.Count == 0)
                {
                    list.Add(0);
                    list.Add(0);
                    list.Add(0);
                    list.Add(0);
                    list.Add(0);
                    list.Add(0);
                    list.Add(0);
                    list.Add(0);
                    list.Add(0);
                    list.Add(0);
                    list.Add(0);
                    list.Add(0);
                }
                if (item[3].ToString() == IncidentNotificationStatusData.Assigned.Code.ToString())
                {
                    list[0] += (double.Parse(item[1].ToString()));
                }
                else if (item[3].ToString() == IncidentNotificationStatusData.Pending.Code.ToString())
                {
                    list[2] += (double.Parse(item[1].ToString()));
                }
                else if (item[3].ToString() == IncidentNotificationStatusData.Delayed.Code.ToString())
                {
                    list[4] += (double.Parse(item[1].ToString()));
                }
                else if ((item[3].ToString() == IncidentNotificationStatusData.InProgress.Code.ToString())||
                    (item[3].ToString() == IncidentNotificationStatusData.AutomaticSupervisor.Code.ToString())||
                    (item[3].ToString() == IncidentNotificationStatusData.ManualSupervisor.Code.ToString()))
                {
                    list[6] += (double.Parse(item[1].ToString()));
                }
                else if (item[3].ToString() == IncidentNotificationStatusData.New.Code.ToString())
                {
                    list[8] += (double.Parse(item[1].ToString()));
                }
                if (item == data[data.Count - 1])
                {
                    list[10] = list[0] + list[2] + list[4] + list[6] + list[8];
                    list[1] = (list[0] / list[10]) * 100;
                    list[3] = (list[2] / list[10]) * 100;
                    list[5] = (list[4] / list[10]) * 100;
                    list[7] = (list[6] / list[10]) * 100;
                    list[9] = (list[8] / list[10]) * 100;
                    list[11] = (list[10] * 100) / currentTotal;
                    result.Add(incidentTypes, list);
                    SaveIndicatorMultiplesValues(null, IndicatorClassData.System, result);
                    list = new List<double>();
                    result.Clear();
                    incidentTypes = "";
                }

            }
            #endregion new

            #region doc
//            string hql = @"select distinct data.Code, sit.FriendlyName 
//                           from IncidentData data inner join
//                                data.SetReportBaseList srl inner join
//                                srl.SetIncidentTypes sit inner join
//                                srl.IncidentNotifications inot
//                           where inot.EndDate is null
//                           order by data.Code, sit.FriendlyName";

//            IList incidentInfo = (IList)
//                SmartCadDatabase.SearchBasicObjects(
//                SmartCadHqls.GetCustomHql(hql));

//            Dictionary<string, List<int>> incidentTypeNamesByIncident = new Dictionary<string, List<int>>();
//            string itnames = "";
//            int code = 0;
//            foreach (object[] item in incidentInfo)
//            {
//                if (code != 0 && code != int.Parse(item[0].ToString()))
//                {
//                    if (incidentTypeNamesByIncident.ContainsKey(itnames) == false)
//                    {
//                        List<int> codes = new List<int>();
//                        codes.Add(code);
//                        incidentTypeNamesByIncident.Add(itnames, codes);
//                    }
//                    else
//                    {
//                        List<int> codes = incidentTypeNamesByIncident[itnames];
//                        codes.Add(code);
//                    }
//                    itnames = string.Empty;
//                }
//                code = int.Parse(item[0].ToString());
//                string friendlyName = item[1].ToString();
//                if (itnames.Contains(friendlyName) == false)
//                {
//                    if (itnames.Length == 0)
//                    {
//                        itnames = friendlyName;
//                    }
//                    else
//                    {
//                        itnames = string.Concat(itnames, ", ", friendlyName);
//                    }
//                }
//            }
//            if (itnames != string.Empty)
//            {
//                if (incidentTypeNamesByIncident.ContainsKey(itnames) == false)
//                {
//                    List<int> codes = new List<int>();
//                    codes.Add(code);
//                    incidentTypeNamesByIncident.Add(itnames, codes);
//                }
//                else
//                {
//                    List<int> codes = incidentTypeNamesByIncident[itnames];
//                    codes.Add(code);
//                }
//            }                        
//            StringBuilder incidentStatus = new StringBuilder();
//            incidentStatus.Append("(");
//            incidentStatus.Append(IncidentNotificationStatusData.Assigned.Code);
//            incidentStatus.Append(", ");
//            incidentStatus.Append(IncidentNotificationStatusData.Pending.Code);
//            incidentStatus.Append(", ");
//            incidentStatus.Append(IncidentNotificationStatusData.Delayed.Code);
//            incidentStatus.Append(", ");
//            incidentStatus.Append(IncidentNotificationStatusData.InProgress.Code);
//            incidentStatus.Append(", ");
//            incidentStatus.Append(IncidentNotificationStatusData.New.Code);
//            incidentStatus.Append(")");

            
//            string hql3 = @"select  count(*)
//                                    from IncidentNotificationData notif
//                                    where notif.Status.Code in {0}";           
//            double totalDispatchOrderByStatus = Convert.ToDouble(SmartCadDatabase.SearchBasicObject(
//                       SmartCadHqls.GetCustomHql(
//                       hql3,
//                       incidentStatus)));
          

//            foreach (KeyValuePair<string, List<int>> kvp in incidentTypeNamesByIncident)
//            {

//                StringBuilder sb = new StringBuilder();
//                sb.Append("(");
//                foreach (int codeValue in kvp.Value)
//                {
//                    if (sb.Length == 1)
//                    {
//                        sb.Append(codeValue);
//                    }
//                    else
//                    {
//                        sb.Append(", ");
//                        sb.Append(codeValue);
//                    }
//                }
//                sb.Append(")");
               
//                string hql2 = @"select notif.Status.FriendlyName, count(*)
//                                    from IncidentNotificationData notif
//                                    inner join notif.ReportBase repoBase
//                                    inner join repoBase.Incident inc
//                                    where notif.EndDate is null and
//                                        inc.Code in {0} and
//                                    notif.Status.Code in {1}                                   
//                                     group by notif.Status.FriendlyName";

//                incidentInfo = (IList)
//                    SmartCadDatabase.SearchBasicObjects(
//                    SmartCadHqls.GetCustomHql(hql2,
//                    sb.ToString(), incidentStatus.ToString()));
               
//                Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//                List<double> list = new List<double>();
//                list.Add(0);
//                list.Add(0);
//                list.Add(0);
//                list.Add(0);
//                list.Add(0);
//                list.Add(0);
//                list.Add(0);
//                list.Add(0);
//                list.Add(0);
//                list.Add(0);
//                list.Add(0);
//                list.Add(0);
//                long totalRow = 0;
//                //Get totalRow
//                foreach (object[] item in incidentInfo)
//                {
//                 totalRow +=  ((long)item[1]);
//                }                
//                foreach (object[] item in incidentInfo)
//                {
//                    if (item[0].ToString() == IncidentNotificationStatusData.Assigned.FriendlyName)
//                    {
//                        list[0] = ((long)item[1]);
//                        if (totalRow > 0)
//                            list[1] = ((double)((long)item[1] * 100) / totalRow);
//                        else
//                            list[1] = 0;
//                    }
//                    else if (item[0].ToString() == IncidentNotificationStatusData.Pending.FriendlyName)
//                    {
//                        list[2] = ((long)item[1]);
//                        if (totalRow > 0)
//                            list[3] = ((double)((long)item[1] * 100) / totalRow);
//                        else
//                            list[3] = 0;
//                    }
//                    else if (item[0].ToString() == IncidentNotificationStatusData.Delayed.FriendlyName)
//                    {
//                        list[4] = ((long)item[1]);
//                        if (totalRow > 0)
//                            list[5] = ((double)((long)item[1] * 100) / totalRow);
//                        else
//                            list[5] = 0;
//                    }
//                    else if (item[0].ToString() == IncidentNotificationStatusData.InProgress.FriendlyName)
//                    {
//                        list[6] = ((long)item[1]);
//                        if (totalRow > 0)
//                            list[7] = ((double)((long)item[1] * 100) / totalRow);
//                        else
//                            list[7] = 0;
//                    }
//                    else if (item[0].ToString() == IncidentNotificationStatusData.New.FriendlyName)
//                    {
//                        list[8] = ((long)item[1]);
//                        if (totalRow > 0)
//                            list[9] = ((double)((long)item[1] * 100) / totalRow);
//                        else
//                            list[9] = 0;
//                    }
//                }
//                list[10] = totalRow;
//                if (totalDispatchOrderByStatus > 0)
//                    list[11] = (totalRow * 100) / totalDispatchOrderByStatus;
//                else
//                    list[11] = 0;
//                result.Add(kvp.Key, list);
//                SaveIndicatorMultiplesValues(null, IndicatorClassData.System, result);
//                result.Clear();
            //            }
            #endregion doc
        }
            
    }
}
