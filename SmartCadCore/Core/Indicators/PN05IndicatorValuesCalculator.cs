//using System;
//using System.Collections.Generic;
//using System.Text;
//using SmartCadCore.Model;
//using System.Collections;

//namespace SmartCadCore.Core.Indicators
//{
//    public class PN05IndicatorValuesCalculator : IndicatorValuesCalculator
//    {
//        protected override string GetIndicator()
//        {
//            return "PN05";
//        }

//        protected override void Calculate()
//        {
//            CalculateSystem();
//            CalculateGroup();
//        }


//        private void CalculateSystem()
//        {
//            #region new
//            string sqlConnected = @" SELECT		COUNT(SEH.USER_ACCOUNT_CODE) AS CANTIDAD
//				
//FROM        USER_APPLICATION AS USA
//			,SESSION_HISTORY AS SEH
//			,SESSION_STATUS_HISTORY AS STH
//			,OPERATOR_STATUS AS OPS
//WHERE		(
//			USA.CODE = 2
//			)
//AND			USA.CODE = SEH.USER_APPLICATION_CODE
//AND			SEH.CODE = STH.SESSION_HISTORY_CODE
//AND			STH.OPERATOR_STATUS_CODE = OPS.CODE
//AND			SEH.END_DATE_LOGIN IS NULL
//AND			STH.END_DATE IS NULL 
//AND         SEH.IS_LOGGED_IN = 1
//";

//            string sqlNoConnected = @" SELECT	COUNT(OPA.OPERATOR_CODE) AS CANTIDAD
//				
//FROM	OPERATOR_ASSIGN AS OPA
//		,OPERATOR AS OPE
//		,USER_ROLE AS USR
//		,USER_ROLE_PROFILE AS URP
//		,USER_PROFILE AS USP
//		,USER_PROFILE_ACCESS AS UPA
//		,USER_ACCESS AS USA
//		,USER_APPLICATION AS APP
//WHERE	OPA.DELETED_ID IS NULL
//AND		GETDATE()
//		BETWEEN OPA.START_DATE AND OPA.END_DATE
//AND		OPA.OPERATOR_CODE NOT IN (
//									SELECT	SEH.USER_ACCOUNT_CODE AS OPERATOR
//									FROM	SESSION_HISTORY AS SEH
//									WHERE	SEH.IS_LOGGED_IN = 1
//									)
//AND		OPE.CODE = OPA.OPERATOR_CODE
//AND		USR.CODE = OPE.USER_ROLE_CODE
//AND		URP.USER_ROLE_CODE = USR.CODE
//AND		USP.CODE = URP.USER_PROFILE_CODE
//AND		UPA.USER_PROFILE_CODE = USP.CODE
//AND		USA.CODE = UPA.USER_ACCESS_CODE
//AND		APP.CODE = USA.USER_APPLICATION_CODE
//AND		(
//		
//		APP.CODE = 2
//		)
//";

//            IList operatorsConnected = (IList)SmartCadDatabase.SearchBasicObjects(sqlConnected, false);
//            IList operatorsNoConnected = (IList)SmartCadDatabase.SearchBasicObjects(sqlNoConnected, false);

//            double totalOperatorsConnected = 0;
//            double totalOperatorsNoConnected = 0;
//            double totalOperators = 0;
//            double operatorsReady = 0;
//            List<double> resultsValues = new List<double>();
//            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();

//            foreach (int item in operatorsConnected)
//            {
//                totalOperatorsConnected += item;
//            }
//            foreach (int item in operatorsNoConnected)
//            {
//                totalOperatorsNoConnected += item;
//            }
//            totalOperators = totalOperatorsConnected + totalOperatorsNoConnected;
//            resultsValues.Add(totalOperatorsNoConnected);
//            if (totalOperators == 0)
//            {
//                resultsValues.Add(0);
//            }
//            else
//            {
//                resultsValues.Add((totalOperatorsNoConnected * 100) / totalOperators);
//            }
//            result.Add("NoConnectedOperators", resultsValues);
//            SaveIndicatorMultiplesValues(null, IndicatorClassData.System, result);

//            #endregion new

//            #region doc
////            double percentNotConnectedOperators = 0;
////            double totalOperatorsConnected = 0;
////            double totalOperatorsNotConnected = 0;
////            string hql = @"select count(*) 
////                         from SessionHistoryData data 
////                         where  data.UserApplication.Code = {0} and
////                         data.IsLoggedIn = {1}";

////            double firtsLevelOperatorsReady = (long)SmartCadDatabase.SearchBasicObject(
////                                                SmartCadHqls.GetCustomHql(
////                                                    hql,
////                                                    UserApplicationData.FirstLevel.Code,
////                                                    true.ToString().ToLower()));
            

////            double firtsLevelOperatorsWorkingNow = OperatorScheduleManager.GetOperatorsWorkingNowByApplication(
////                UserApplicationData.FirstLevel.Name, false).Count;
            

////            totalOperatorsConnected = firtsLevelOperatorsReady;
////            totalOperatorsNotConnected = (firtsLevelOperatorsWorkingNow) - 
////                (firtsLevelOperatorsReady);
////            if ((firtsLevelOperatorsWorkingNow) > 0)
////            {
////                percentNotConnectedOperators = (totalOperatorsNotConnected * 100) /
////                    (firtsLevelOperatorsWorkingNow);
////            }
////            else
////            {
////                percentNotConnectedOperators = 0;
////            }
////            if (totalOperatorsNotConnected < 0)
////            {
////                totalOperatorsNotConnected = 0;
////            }
////            percentNotConnectedOperators = Math.Round(percentNotConnectedOperators, 2);
////            List<double> resultsValues = new List<double>();
////            resultsValues.Add(totalOperatorsNotConnected);
////            resultsValues.Add(percentNotConnectedOperators);
////            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
////            result.Add("AvailableOperators", resultsValues);
////            SaveIndicatorMultiplesValues(null, IndicatorClassData.System, result);
//            #endregion doc
//        }

//        private void CalculateGroup()
//        {
//            #region new
//            string operatorsConnectedSql = @" SELECT		COUNT(SEH.USER_ACCOUNT_CODE) AS CANTIDAD						
//			,OPE.CODE AS SUPERVISOR
//FROM        USER_APPLICATION AS USA
//			,SESSION_HISTORY AS SEH
//			,SESSION_STATUS_HISTORY AS STH
//			,OPERATOR_STATUS AS OPS
//			,OPERATOR_ASSIGN AS OPA
//			,OPERATOR AS OPE
//WHERE		USA.CODE = SEH.USER_APPLICATION_CODE
//AND			USA.CODE = 2			
//AND			SEH.CODE = STH.SESSION_HISTORY_CODE
//AND			STH.OPERATOR_STATUS_CODE = OPS.CODE
//AND			SEH.END_DATE_LOGIN IS NULL
//AND			STH.END_DATE IS NULL 
//AND         SEH.IS_LOGGED_IN = 1
//AND			SEH.USER_ACCOUNT_CODE = OPA.OPERATOR_CODE
//AND			OPA.DELETED_ID IS NULL
//AND			(
//				(				
//				GETDATE()
//				BETWEEN OPA.START_DATE AND OPA.END_DATE
//				)			
//			)
//AND			OPA.SUPERVISOR_CODE = OPE.CODE
//GROUP BY	OPE.CODE
//order by OPE.CODE
//";

//            string operatorsNoConnectedSql = @" SELECT	count(distinct(OPA.OPERATOR_CODE)) AS CANTIDAD		
//		,OPE2.CODE AS SUPERVISOR
//FROM	OPERATOR_ASSIGN AS OPA
//		,OPERATOR AS OPE
//		,OPERATOR AS OPE2
//		,USER_ROLE AS USR
//		,USER_ROLE_PROFILE AS URP
//		,USER_PROFILE AS USP
//		,USER_PROFILE_ACCESS AS UPA
//		,USER_ACCESS AS USA
//		,USER_APPLICATION AS APP
//		,OPERATOR_DEPARTMENT_TYPE AS ODT
//		,DEPARTMENT_TYPE AS DETY
//WHERE	OPA.DELETED_ID IS NULL
//AND		GETDATE()
//		BETWEEN OPA.START_DATE AND OPA.END_DATE
//AND		OPA.OPERATOR_CODE NOT IN (
//									SELECT	SEH.USER_ACCOUNT_CODE AS OPERATOR
//									FROM	SESSION_HISTORY AS SEH
//									WHERE	SEH.IS_LOGGED_IN = 1
//									)
//AND		OPE.CODE = OPA.OPERATOR_CODE
//AND		OPE2.CODE = OPA.SUPERVISOR_CODE
//AND		USR.CODE = OPE.USER_ROLE_CODE
//AND		URP.USER_ROLE_CODE = USR.CODE
//AND		USP.CODE = URP.USER_PROFILE_CODE
//AND		UPA.USER_PROFILE_CODE = USP.CODE
//AND		USA.CODE = UPA.USER_ACCESS_CODE
//AND		APP.CODE = USA.USER_APPLICATION_CODE
//AND		APP.CODE = 2		
//AND		ODT.OPERATOR_CODE = OPA.OPERATOR_CODE
//AND		DETY.CODE = ODT.DEPARTMENT_TYPE_CODE
//GROUP By OPE2.CODE
//order By OPE2.CODE
//";

//            IList operatorsConnected = (IList)SmartCadDatabase.SearchBasicObjects(operatorsConnectedSql, false);
//            IList operatorsNoConnected = (IList)SmartCadDatabase.SearchBasicObjects(operatorsNoConnectedSql, false);

//            Dictionary<int, double> supervisorCodes = new Dictionary<int, double>();
//            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//            List<double> list = new List<double>();
//            foreach (object[] item in operatorsConnected)
//            {
//                if (supervisorCodes.ContainsKey(int.Parse(item[1].ToString())) == false)
//                {
//                    supervisorCodes.Add(int.Parse(item[1].ToString()), int.Parse(item[0].ToString()));
//                }

//            }
//            foreach (object[] item in operatorsNoConnected)
//            {

//                if (supervisorCodes.ContainsKey(int.Parse(item[1].ToString())) == false)
//                {
//                    supervisorCodes.Add(int.Parse(item[1].ToString()), int.Parse(item[0].ToString()));
//                }
//                else
//                {
//                    supervisorCodes[int.Parse(item[1].ToString())] += int.Parse(item[0].ToString());
//                }
//            }

//            foreach (KeyValuePair<int, double> pair in supervisorCodes)
//            {

//                foreach (object[] item in operatorsNoConnected)
//                {
//                    if (int.Parse(item[1].ToString()) == pair.Key)
//                    {
//                        ObjectData od1 = new ObjectData();
//                        od1.Code = int.Parse(item[1].ToString());
//                        list.Add(int.Parse(item[0].ToString()));
//                        list.Add((int.Parse(item[0].ToString()) * 100) / pair.Value);
//                        result.Add("operatorsNoConnected", list);
//                        SaveIndicatorMultiplesValues(od1, IndicatorClassData.Group, result);
//                        result.Clear();
//                        list = new List<double>();
//                        break;
//                    }
//                }
//            }
//            #endregion new

//            #region doc
//            //IList supervisors = OperatorScheduleManager.GetSupervisorsWorkingNowByApplication("FirstLevelSupervisorName");

//            //foreach (int supervisor in supervisors)
//            //{
//            //    IList operators = OperatorScheduleManager.GetOperatorsBySupervisor(supervisor);

//            //    double operatorsConnected = 0;
//            //    double operatorsNotConnected = 0;
//            //    double percetOperatorsNotConnected = 0;
//            //    double allOperators = 0;
//            //    foreach (OperatorData operatorData in operators)
//            //    {
//            //        double operatorConnected = Convert.ToDouble(SmartCadDatabase.SearchBasicObject(
//            //            SmartCadHqls.GetCustomHql(
//            //               SmartCadHqls.IsLoggedOperatorInApplication,
//            //               operatorData.Code, true.ToString().ToLower(),
//            //               UserApplicationData.FirstLevel.Code)));
//            //        allOperators += 1;
//            //        if (operatorConnected > 0)
//            //            operatorsConnected += 1;
//            //    }
//            //    operatorsNotConnected = allOperators - operatorsConnected;
//            //    if (allOperators > 0)
//            //    {
//            //        percetOperatorsNotConnected = (operatorsNotConnected * 100) / (allOperators);
//            //    }
//            //    else
//            //    {
//            //        percetOperatorsNotConnected = 0;
//            //    }
//            //    if (operatorsNotConnected < 0)
//            //    {
//            //        operatorsNotConnected = 0;
//            //    }
//            //    percetOperatorsNotConnected = Math.Round(percetOperatorsNotConnected, 2);
//            //    List<double> resultsValues = new List<double>();
//            //    resultsValues.Add(operatorsNotConnected);
//            //    resultsValues.Add(percetOperatorsNotConnected);
//            //    Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//            //    result.Add("NotConnectedOperators", resultsValues);
//            //    ObjectData objectdata = new ObjectData();
//            //    objectdata.Code = supervisor;
//            //    SaveIndicatorMultiplesValues(objectdata, IndicatorClassData.Group, result);
//            //}
//            #endregion doc
//        }
     
//    }
//}
