using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using System.Collections;

namespace SmartCadCore.Core.Indicators
{
    public class L07IndicatorValuesCalculator : IndicatorValuesCalculator
    {
        protected override string GetIndicator()
        {
            return "L07";
        }

        protected override void Calculate()
        {
            CalculateSystem();
            CalculateGroup();
            CalculateOperators();
        }

        private void CalculateSystem()
        {
            #region new
            string sql = @" SELECT 
		(
			CONVERT(FLOAT,
							(
								SELECT		COUNT(INC.CODE) AS LL_NE
								FROM		INCIDENT AS INC
								WHERE		INC.IS_EMERGENCY = 'FALSE'
								AND			INC.APP_CODE=2
								AND			CONVERT(DATETIME,CONVERT(CHAR(10),INC.START_DATE,101),101) = 
											CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
							)
					) 
		) / 
		(
			SELECT	CASE COUNT(PHR.PARENT_CODE) 
					WHEN 0 THEN 1
					ELSE COUNT(PHR.PARENT_CODE)
					END AS TOT_LL
			FROM	PHONE_REPORT AS PHR
			WHERE	CONVERT(DATETIME,CONVERT(CHAR(10),PHR.PICKED_UP_CALL_TIME,101),101)= 
					CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
		) AS PORCENTAJE";

            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);
            SaveIndicatorsValues(IndicatorClassData.System, double.Parse(data[0].ToString()));

            #endregion new

            #region doc
            //double res = 0;

            //double noEmergenciesCalls = (long)SmartCadDatabase.SearchBasicObject(
            //    SmartCadHqls.GetCustomHql(
            //    SmartCadHqls.GetAllCallsDateNoEmergency,
            //    ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today)));

            //double totalCalls = (long)SmartCadDatabase.SearchBasicObject(
            //    SmartCadHqls.GetCustomHql(
            //    SmartCadHqls.GetAmountAllCallsDate,
            //    ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today)));

            //if (noEmergenciesCalls > 0 && totalCalls > 0)
            //{
            //    res = noEmergenciesCalls / totalCalls;
            //}

            //SaveIndicatorsValues(IndicatorClassData.System, res);
            #endregion doc
        }

        private void CalculateOperators()
        {
            #region new
            string sql = @" SELECT	INDIVIDUAL.LL_NE/GENERAL.TOT_LL AS PORCENTAJE, INDIVIDUAL.OPERADOR
FROM
		(
			SELECT		CONVERT(FLOAT,COUNT(INC.CODE)) AS LL_NE
						,OPE.CODE AS OPERADOR
			FROM		INCIDENT AS INC
						,OPERATOR AS OPE
			WHERE		INC.IS_EMERGENCY = 'FALSE'
			AND			INC.APP_CODE=2
			AND			CONVERT(DATETIME,CONVERT(CHAR(10),INC.START_DATE,101),101) = 
						CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
			AND			INC.CREATE_OPERATOR_CODE = OPE.CODE
			GROUP BY OPE.CODE
		) AS INDIVIDUAL
		, 
		(
			SELECT		CASE COUNT(PHR.PARENT_CODE)
						WHEN 0 THEN 1
						ELSE COUNT(PHR.PARENT_CODE)
						END AS TOT_LL
						,OPE.CODE AS OPERADOR
			FROM		PHONE_REPORT AS PHR
						,REPORT_BASE AS REB
						,OPERATOR AS OPE
			WHERE		CONVERT(DATETIME,CONVERT(CHAR(10),PHR.PICKED_UP_CALL_TIME,101),101)= 
						CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
			AND			PHR.PARENT_CODE = REB.CODE
			AND			REB.OPERATOR_CODE = OPE.CODE
			GROUP BY	OPE.CODE


		) AS GENERAL
WHERE	INDIVIDUAL.OPERADOR = GENERAL.OPERADOR
";

            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);

            foreach (object[] item in data)
            {
                ObjectData od1 = new ObjectData();
                od1.Code = int.Parse(item[1].ToString());
                SaveIndicatorsValues(od1, IndicatorClassData.Operator,
                    double.Parse(item[0].ToString()));
            }
            #endregion new

            #region doc
            //IList operators = OperatorScheduleManager.GetOperatorsWorkingNowByApplication(UserApplicationData.FirstLevel.Name, false);
            //foreach (int operatorCode in operators)
            //{
            //    double res = 0;

            //    double noEmergenciesCalls = (long)SmartCadDatabase.SearchBasicObject(
            //        SmartCadHqls.GetCustomHql(
            //        SmartCadHqls.GetAllCallsDateNoEmergencyByOperator,
            //        ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today),
            //        operatorCode));

            //    double totalCalls = (long)SmartCadDatabase.SearchBasicObject(
            //        SmartCadHqls.GetCustomHql(
            //        SmartCadHqls.GetAmountAllCallsDateByOperator,
            //        ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today),
            //        operatorCode));

            //    if (noEmergenciesCalls > 0 && totalCalls > 0)
            //    {
            //        res = noEmergenciesCalls / totalCalls;
            //    }

            //    ObjectData objectData = new ObjectData();
            //    objectData.Code = operatorCode;
            //    SaveIndicatorsValues(objectData, IndicatorClassData.Operator, res);
            //}
            #endregion doc
        }

        private void CalculateGroup()
        {
            #region new
            string sql = @" SELECT	INDIVIDUAL.LL_NE/GENERAL.TOT_LL AS PORCENTAJE, INDIVIDUAL.SUPERVISOR
FROM
		(
			SELECT		CONVERT(FLOAT,COUNT(INC.CODE)) AS LL_NE
						,OPE.CODE AS SUPERVISOR
			FROM		INCIDENT AS INC
						,OPERATOR AS OPE
						,OPERATOR_ASSIGN AS OPA
			WHERE		INC.IS_EMERGENCY = 'FALSE'
			AND			INC.APP_CODE=2
			AND			CONVERT(DATETIME,CONVERT(CHAR(10),INC.START_DATE,101),101) = 
						CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
			AND			INC.CREATE_OPERATOR_CODE = OPA.OPERATOR_CODE			
			AND			OPA.DELETED_ID IS NULL
			AND			(
							(							
							INC.START_DATE
							BETWEEN OPA.START_DATE AND OPA.END_DATE
							)						
						)
			AND			OPA.SUPERVISOR_CODE = OPE.CODE
			GROUP BY OPE.CODE
		) AS INDIVIDUAL
		, 
		(
			SELECT		CASE COUNT(PHR.PARENT_CODE)
						WHEN 0 THEN 1
						ELSE COUNT(PHR.PARENT_CODE)
						END AS TOT_LL
						,OPE.CODE AS SUPERVISOR
			FROM		PHONE_REPORT AS PHR
						,REPORT_BASE AS REB
						,OPERATOR AS OPE
						,OPERATOR_ASSIGN AS OPA
			WHERE		CONVERT(DATETIME,CONVERT(CHAR(10),PHR.PICKED_UP_CALL_TIME,101),101)= 
						CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
			AND			PHR.PARENT_CODE = REB.CODE
			AND			REB.OPERATOR_CODE = OPA.OPERATOR_CODE			
			AND			OPA.DELETED_ID IS NULL
			AND			(
							(							
							PHR.PICKED_UP_CALL_TIME
							BETWEEN OPA.START_DATE AND OPA.END_DATE
							)						
						)
			AND			OPA.SUPERVISOR_CODE = OPE.CODE
			GROUP BY	OPE.CODE
		) AS GENERAL
WHERE	INDIVIDUAL.SUPERVISOR = GENERAL.SUPERVISOR
";

            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);
            foreach (object[] item in data)
            {
                ObjectData od1 = new ObjectData();
                od1.Code = int.Parse(item[1].ToString());
                SaveIndicatorsValues(od1, IndicatorClassData.Group,
                    double.Parse(item[0].ToString()));
            }
            #endregion new

            #region doc
        //    IList supervisors = OperatorScheduleManager.GetSupervisorsWorkingNowByApplication("FirstLevelSupervisorName");
        //    //Dictionary<int, double> results = new Dictionary<int, double>();
        //    foreach (int supervisorCode in supervisors)
        //    {
        //        //IList operators = OperatorScheduleManager.GetOperatorsBySupervisor(supervisorCode);
        //        IList operators = OperatorScheduleManager.GetAllDayOperatorsBySupervisor(supervisorCode);
        //        WorkShiftScheduleVariationData bshSupervisor =
        //            OperatorScheduleManager.GetCurrentWorkShiftSchedulesByOperator(supervisorCode, SmartCadDatabase.GetTimeFromBD().Date);

        //        double totalIncidentsNoEmergencyInGroup = 0;
        //        double totalIncidentsEmergencyInGroup = 0;

        //        foreach (OperatorData operatorData in operators)
        //        {
        //            //BaseSessionHistory bsh = 
        //            //    OperatorScheduleManager.GetOperatorAssignBySupervisorAndByDayRealHours(
        //            //    supervisorCode, 
        //            //    operatorData.Code);

        //            IList bshList =
        //               OperatorScheduleManager.GetAllDayOperatorAssignBySupervisorAndByDayRealHours(supervisorCode, operatorData.Code);
        //            foreach (OperatorAssignData bsh in bshList)
        //            {
        //                if ((bshSupervisor != null) &&
        //                     (((bsh.StartDate.TimeOfDay >= bshSupervisor.Start.Value.TimeOfDay) &&
        //                    (bsh.StartDate.TimeOfDay <= bshSupervisor.End.Value.TimeOfDay)) ||
        //                    ((bsh.EndDate.TimeOfDay <= bshSupervisor.End.Value.TimeOfDay) &&
        //                    (bsh.EndDate.TimeOfDay >= bshSupervisor.Start.Value.TimeOfDay))))
        //                {
        //                    long totalIncidentsNoEmergency = (long)SmartCadDatabase.SearchBasicObject(
        //                    SmartCadHqls.GetCustomHql(
        //                    SmartCadHqls.GetAmountAllIncidentBetweenDateByOperator,
        //                    ApplicationUtil.GetDataBaseFormattedDate(bsh.StartDate),
        //                    ApplicationUtil.GetDataBaseFormattedDate(bsh.EndDate),
        //                    operatorData.Code, false));

        //                    long totalIncidentsEmergency = (long)SmartCadDatabase.SearchBasicObject(
        //                    SmartCadHqls.GetCustomHql(
        //                    SmartCadHqls.GetAmountAllIncidentBetweenDateByOperator,
        //                    ApplicationUtil.GetDataBaseFormattedDate(bsh.StartDate),
        //                    ApplicationUtil.GetDataBaseFormattedDate(bsh.EndDate),
        //                    operatorData.Code, true));

        //                    totalIncidentsNoEmergencyInGroup += totalIncidentsNoEmergency;
        //                    totalIncidentsEmergencyInGroup += totalIncidentsEmergency;
        //                }
        //            }
        //        }

        //        ObjectData objectData = new ObjectData();
        //        objectData.Code = supervisorCode;
        //        if ((totalIncidentsNoEmergencyInGroup + totalIncidentsEmergencyInGroup) > 0)
        //        {
        //            totalIncidentsNoEmergencyInGroup = totalIncidentsNoEmergencyInGroup /
        //                (totalIncidentsNoEmergencyInGroup + totalIncidentsEmergencyInGroup);
        //        }
        //        else
        //        {
        //            totalIncidentsNoEmergencyInGroup = 0;
        //        }
        //        SaveIndicatorsValues(objectData, IndicatorClassData.Group, totalIncidentsNoEmergencyInGroup);

        //        //results.Add(supervisorCode, total);
        //    }

        //    //double totalCalls = (long)SmartCadDatabase.SearchObject(
        //    //                            SmartCadHqls.GetCustomHql(
        //    //                            SmartCadHqls.GetAmountAllCallsDate,
        //    //                            ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today)));


        //    //foreach (KeyValuePair<int, double> pair in results)
        //    //{
        //    //    double res = 0;

        //    //    if (pair.Value > 0 && totalCalls > 0)
        //    //        res = pair.Value / totalCalls;

        //    //    ObjectData objectData = new ObjectData();
        //    //    objectData.Code = pair.Key;
        //    //    SaveIndicatorsValues(objectData, IndicatorClassData.Group, res);
        //    //}
            #endregion doc
        }
        
    }
}
