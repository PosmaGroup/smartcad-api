using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using System.Collections;

namespace SmartCadCore.Core.Indicators
{
    public class D11IndicatorValuesCalculator : IndicatorValuesCalculator
    {
        protected override string GetIndicator()
        {
            return "D11";
        }
         protected override void Calculate()
        {
            CalculateSystemIndicators();
        }

         private void CalculateSystemIndicators()
         {

             #region new
             string sqlOpen = @" SELECT			COUNT(INOT.CODE)AS ABIERTAS
				, INOT.INCIDENT_NOTIFICATION_PRIORITY_CODE 
				AS PRIORIDAD
				,DETY.CODE AS DEPARTAMENTO_CODE
				,DETY.NAME AS DEPARTAMENTO_NAME
FROM			INCIDENT_NOTIFICATION AS INOT
				,DEPARTMENT_TYPE AS DETY
WHERE			INOT.END_DATE IS NULL
AND				INOT.DEPARTMENT_TYPE_CODE = DETY.CODE
GROUP BY		INOT.INCIDENT_NOTIFICATION_PRIORITY_CODE, DETY.CODE, DETY.NAME
order by DETY.CODE";

             string sqlClose = @" SELECT			COUNT(INOT.CODE)AS CERRADAS
				, INOT.INCIDENT_NOTIFICATION_PRIORITY_CODE 
				AS PRIORIDAD
				,DETY.CODE AS DEPARTAMENTO_CODE
				,DETY.NAME AS DEPARTAMENTO_NAME
FROM			INCIDENT_NOTIFICATION AS INOT
				,DEPARTMENT_TYPE AS DETY
WHERE			CONVERT(DATETIME,CONVERT(CHAR(10),INOT.END_DATE,101),101)= 
				CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
AND				INOT.DEPARTMENT_TYPE_CODE = DETY.CODE
GROUP BY		INOT.INCIDENT_NOTIFICATION_PRIORITY_CODE, DETY.CODE, DETY.NAME
order by DETY.CODE";




             IList IncidentOpen = (IList)SmartCadDatabase.SearchBasicObjects(sqlOpen, false);
             IList IncidentClose = (IList)SmartCadDatabase.SearchBasicObjects(sqlClose, false);


             Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
             List<double> resultsValues = new List<double>();
             Dictionary<string, int> departmentsCode = new Dictionary<string, int>();

             double openDispatchOrderHightPriority = 0;
             double closedDispatchOrderHightPriority = 0;
             double openDispatchOrderMediumPriority = 0;
             double closedDispatchOrderMediumPriority = 0;
             double openDispatchOrderLowPriority = 0;
             double closedDispatchOrderLowPriority = 0;
             double openDispatchOrderTotal = 0;
             double closedDispatchOrderTotal = 0;

             #region IncidentOpen
             int departmentCode = 0;
             string departmentName = "";
             foreach (object[] item in IncidentOpen)
             {
                 if (departmentCode != 0 && departmentCode != int.Parse(item[2].ToString()))
                 {
                     departmentsCode.Add(departmentName, departmentCode);
                     openDispatchOrderTotal = openDispatchOrderHightPriority +
                         openDispatchOrderMediumPriority + openDispatchOrderLowPriority;
                     resultsValues.Add(openDispatchOrderHightPriority);
                     resultsValues.Add(openDispatchOrderMediumPriority);
                     resultsValues.Add(openDispatchOrderLowPriority);
                     resultsValues.Add(openDispatchOrderTotal);
                     resultsValues.Add(0);
                     resultsValues.Add(0);
                     resultsValues.Add(0);
                     resultsValues.Add(0);
                     result.Add(departmentName, resultsValues);

                     resultsValues = new List<double>();

                     openDispatchOrderHightPriority = 0;
                     openDispatchOrderMediumPriority = 0;
                     openDispatchOrderLowPriority = 0;
                     openDispatchOrderTotal = 0;
                     openDispatchOrderTotal = 0;
                 }

                 if (double.Parse(item[1].ToString()) == 1)
                 {
                     openDispatchOrderHightPriority = double.Parse(item[0].ToString());
                 }
                 else if (double.Parse(item[1].ToString()) == 2)
                 {
                     openDispatchOrderMediumPriority = double.Parse(item[0].ToString());
                 }
                 else if (double.Parse(item[1].ToString()) == 3)
                 {
                     openDispatchOrderLowPriority = double.Parse(item[0].ToString());
                 }
                 departmentCode = (int)item[2];
                 departmentName = (string)item[3];

                 if (item == IncidentOpen[IncidentOpen.Count - 1])
                 {
                     openDispatchOrderTotal = openDispatchOrderHightPriority +
                         openDispatchOrderMediumPriority + openDispatchOrderLowPriority;
                     resultsValues.Add(openDispatchOrderHightPriority);
                     resultsValues.Add(openDispatchOrderMediumPriority);
                     resultsValues.Add(openDispatchOrderLowPriority);
                     resultsValues.Add(openDispatchOrderTotal);
                     resultsValues.Add(0);
                     resultsValues.Add(0);
                     resultsValues.Add(0);
                     resultsValues.Add(0);
                     result.Add(departmentName, resultsValues);
                     resultsValues = new List<double>();
                     departmentsCode.Add(departmentName, departmentCode);

                 }
             }
             #endregion IncidentOpen

             #region IncidentClose
             departmentCode = 0;
             departmentName = "";
             foreach (object[] item in IncidentClose)
             {
                 if (departmentCode != 0 && departmentCode != int.Parse(item[2].ToString()))
                 {
                     closedDispatchOrderTotal = closedDispatchOrderHightPriority +
                           closedDispatchOrderMediumPriority + closedDispatchOrderLowPriority;
                     if (departmentsCode.ContainsKey(departmentName) == false)
                     {
                         departmentsCode.Add(departmentName, departmentCode);
                         resultsValues.Clear();
                         resultsValues.Add(0);
                         resultsValues.Add(0);
                         resultsValues.Add(0);
                         resultsValues.Add(0);
                         result.Add(departmentName, resultsValues);

                         result[departmentName].Add(closedDispatchOrderHightPriority);
                         result[departmentName].Add(closedDispatchOrderMediumPriority);
                         result[departmentName].Add(closedDispatchOrderLowPriority);
                         result[departmentName].Add(closedDispatchOrderTotal);
                     }
                     else
                     {
                         result[departmentName][4] = closedDispatchOrderHightPriority;
                         result[departmentName][5] = closedDispatchOrderMediumPriority;
                         result[departmentName][6] = closedDispatchOrderLowPriority;
                         result[departmentName][7] = closedDispatchOrderTotal;
                     }

                     closedDispatchOrderHightPriority = 0;
                     closedDispatchOrderMediumPriority = 0;
                     closedDispatchOrderLowPriority = 0;
                     closedDispatchOrderTotal = 0;
                     closedDispatchOrderTotal = 0;
                 }

                 if (double.Parse(item[1].ToString()) == 1)
                 {
                     closedDispatchOrderHightPriority = double.Parse(item[0].ToString());
                 }
                 else if (double.Parse(item[1].ToString()) == 2)
                 {
                     closedDispatchOrderMediumPriority = double.Parse(item[0].ToString());
                 }
                 else if (double.Parse(item[1].ToString()) == 3)
                 {
                     closedDispatchOrderLowPriority = double.Parse(item[0].ToString());
                 }
                 departmentCode = (int)item[2];
                 departmentName = (string)item[3];

                 if (item == IncidentClose[IncidentClose.Count - 1])
                 {
                     closedDispatchOrderTotal = closedDispatchOrderHightPriority +
                         closedDispatchOrderMediumPriority + closedDispatchOrderLowPriority;
                     if (departmentsCode.ContainsKey(departmentName) == false)
                     {
                         departmentsCode.Add(departmentName, departmentCode);
                         resultsValues.Clear();
                         resultsValues.Add(0);
                         resultsValues.Add(0);
                         resultsValues.Add(0);
                         resultsValues.Add(0);
                         result.Add(departmentName, resultsValues);


                         result[departmentName].Add(closedDispatchOrderHightPriority);
                         result[departmentName].Add(closedDispatchOrderMediumPriority);
                         result[departmentName].Add(closedDispatchOrderLowPriority);
                         result[departmentName].Add(closedDispatchOrderTotal);
                     }
                     else
                     {
                         result[departmentName][4] = closedDispatchOrderHightPriority;
                         result[departmentName][5] = closedDispatchOrderMediumPriority;
                         result[departmentName][6] = closedDispatchOrderLowPriority;
                         result[departmentName][7] = closedDispatchOrderTotal;

                     }
                 }
             }
             #endregion IncidentClose

             foreach (KeyValuePair<string, List<double>> val in result)
             {
                 Dictionary<string, List<double>> resultVal = new Dictionary<string, List<double>>();
                 resultVal.Add(val.Key, val.Value);
                 ObjectData obj = new ObjectData();
                 obj.Code = departmentsCode[val.Key];


                 SaveIndicatorMultiplesValues(obj, IndicatorClassData.System, resultVal);
             }


             #endregion new

             #region doc
             //IList departmentTypes = SmartCadDatabase.SearchObjects(SmartCadHqls.GetDepartmentsType);
             //foreach (DepartmentTypeData department in departmentTypes)
             //{
             //    double openDispatchOrderHightPriority = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(
             //        SmartCadHqls.GetOpendDispatchOrder, department.Code, 1));

             //    double closedDispatchOrderHightPriority = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(
             //       SmartCadHqls.GetClosedDispatchOrderByDate,
             //       ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today),
             //       department.Code, 1));
             //    double openDispatchOrderMediumPriority = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(
             //        SmartCadHqls.GetOpendDispatchOrder, department.Code, 2));

             //    double closedDispatchOrderMediumPriority = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(
             //       SmartCadHqls.GetClosedDispatchOrderByDate,
             //       ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today),
             //       department.Code, 2));
             //    double openDispatchOrderLowPriority = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(
             //        SmartCadHqls.GetOpendDispatchOrder, department.Code, 3));

             //    double closedDispatchOrderLowPriority = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(
             //       SmartCadHqls.GetClosedDispatchOrderByDate,
             //       ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today),
             //       department.Code, 3));

             //    double openDispatchOrderTotal = openDispatchOrderHightPriority + openDispatchOrderMediumPriority + openDispatchOrderLowPriority;
             //    double closedDispatchOrderTotal = closedDispatchOrderHightPriority + closedDispatchOrderMediumPriority + closedDispatchOrderLowPriority;
             //    List<double> resultsValues = new List<double>();
             //    resultsValues.Add(openDispatchOrderHightPriority);
             //    resultsValues.Add(openDispatchOrderMediumPriority);
             //    resultsValues.Add(openDispatchOrderLowPriority);
             //    resultsValues.Add(openDispatchOrderTotal);
             //    resultsValues.Add(closedDispatchOrderHightPriority);
             //    resultsValues.Add(closedDispatchOrderMediumPriority);
             //    resultsValues.Add(closedDispatchOrderLowPriority);
             //    resultsValues.Add(closedDispatchOrderTotal);

             //    Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
             //    result.Add(department.Name, resultsValues);
             //    ObjectData obj = new ObjectData();
             //    obj.Code = department.Code;
             //    SaveIndicatorMultiplesValues(obj, IndicatorClassData.System, result);
             //}
             #endregion doc

         }


    }
}
