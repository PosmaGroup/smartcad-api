using System;
using System.Collections.Generic;
using System.Text;

using NHibernate.Mapping.Attributes;
using System.Collections;
using System.IO;
using SmartCadCore.Model;

namespace SmartCadCore.Core.Indicators
{
    public class L01IndicatorValuesCalculator : IndicatorValuesCalculator
    {
        private double pickedUpCallTimeByGroupTotalElements = 0;
        private double pickedUpCallTimeBySystemTotalValue = 0;

        protected override string GetIndicator()
        {
            return "L01";
        }

        protected override void Calculate()
        {            
            CalculateSystem();
            CalculateGroup();
            CalculateOperators();
        }

        private void CalculateSystem()
        {
            #region new
            string sql = @" SELECT	CONVERT(FLOAT,ISNULL(SUM(DATEDIFF(SS,PHR.RECEIVED_CALL_TIME,PHR.PICKED_UP_CALL_TIME)),0))
		/CASE COUNT(PHR.PICKED_UP_CALL_TIME) WHEN 0 THEN 1 ELSE COUNT(PHR.PICKED_UP_CALL_TIME) END AS TIEMPO_PROMEDIO
FROM	PHONE_REPORT AS PHR 
WHERE	CONVERT(DATETIME,CONVERT(CHAR(10),PHR.PICKED_UP_CALL_TIME,101),101)= 
					CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)";

            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);
            SaveIndicatorsValues(IndicatorClassData.System, double.Parse(data[0].ToString()));

            #endregion new


            #region doc
            //double res = 0;
            //IList phoneCallAvg = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(
            //    SmartCadHqls.GetPhoneReportsCurrentday,
            //    ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today)));

            //if (phoneCallAvg.Count != 0)
            //{
            //    foreach (PhoneReportData prd in phoneCallAvg)
            //    {
            //        TimeSpan ts = prd.PickedUpCallTime.Value.Subtract(prd.ReceivedCallTime.Value);
            //        res += ts.TotalSeconds;
            //    }
            //    res /= phoneCallAvg.Count;
            //}
            //SaveIndicatorsValues(IndicatorClassData.System, res);
            #endregion doc
        }

        private void CalculateGroup()
        {
            #region new
            string sql = @" SELECT		CONVERT(FLOAT,ISNULL(SUM(DATEDIFF(SS,PHR.RECEIVED_CALL_TIME,PHR.PICKED_UP_CALL_TIME)),0))
			/CASE COUNT(PHR.PICKED_UP_CALL_TIME) WHEN 0 THEN 1 ELSE COUNT(PHR.PICKED_UP_CALL_TIME) END AS TIEMPO_PROMEDIO
			,OPE1.CODE AS USUARIO
FROM		OPERATOR AS OPE,
            OPERATOR AS OPE1,
            OPERATOR_ASSIGN AS OPA,
            REPORT_BASE AS REB,
			PHONE_REPORT AS PHR
WHERE		OPE1.CODE = OPA.SUPERVISOR_CODE
AND			OPA.DELETED_ID IS NULL 
AND			(
				(				
				PHR.PICKED_UP_CALL_TIME
				BETWEEN OPA.START_DATE AND OPA.END_DATE
				)		
			)
AND			OPE.CODE = OPA.OPERATOR_CODE
AND			OPE.CODE = REB.OPERATOR_CODE
AND			REB.CODE = PHR.PARENT_CODE
AND         CONVERT(DATETIME,CONVERT(CHAR(10),PHR.PICKED_UP_CALL_TIME,101),101)= 
			CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
GROUP BY	OPE1.CODE
";

            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);
            foreach (object[] item in data)
            {
                ObjectData od1 = new ObjectData();
                od1.Code = int.Parse(item[1].ToString());
                SaveIndicatorsValues(od1, IndicatorClassData.Group,
                    double.Parse(item[0].ToString()));
            }
            #endregion new

            #region doc
            //IList supervisors = OperatorScheduleManager.GetSupervisorsWorkingNowByApplication("FirstLevelSupervisorName");
            //Dictionary<int, double> results = new Dictionary<int, double>();
            //foreach (int supervisorCode in supervisors)
            //{
            //    //IList operators = OperatorScheduleManager.GetOperatorsBySupervisor(supervisorCode);
            //    IList operators = OperatorScheduleManager.GetAllDayOperatorsBySupervisor(supervisorCode);
            //    WorkShiftScheduleVariationData bshSupervisor = 
            //        OperatorScheduleManager.GetCurrentWorkShiftSchedulesByOperator(supervisorCode, SmartCadDatabase.GetTimeFromBD().Date);

            //    double total = 0;
            //    double totalAmount = 0;

            //    foreach (OperatorData operatorData in operators)
            //    {
                    
            //        //BaseSessionHistory bshSupervisor = OperatorScheduleManager.GetOperatorAssignBySupervisorAndByDayRealHours(supervisorCode, operatorData.Code);                    
            //        IList bshList =
            //          OperatorScheduleManager.GetAllDayOperatorAssignBySupervisorAndByDayRealHours(supervisorCode, operatorData.Code);

            //        foreach (OperatorAssignData bsh in bshList)
            //        {
            //            if ((bshSupervisor != null) &&
            //                 (((bsh.StartDate.TimeOfDay >= bshSupervisor.Start.Value.TimeOfDay) &&
            //                (bsh.StartDate.TimeOfDay <= bshSupervisor.End.Value.TimeOfDay)) ||
            //                ((bsh.EndDate.TimeOfDay <= bshSupervisor.End.Value.TimeOfDay) &&
            //                (bsh.EndDate.TimeOfDay >= bshSupervisor.Start.Value.TimeOfDay))))
            //            {
            //                IList phoneCallAvg = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(
            //                                       SmartCadHqls.GetOperatorPhoneReportsBetweenCurrentday,
            //                                       operatorData.Code,
            //                                       ApplicationUtil.GetDataBaseFormattedDate(bsh.StartDate),
            //                                       ApplicationUtil.GetDataBaseFormattedDate(bsh.EndDate)));

            //                foreach (PhoneReportData call in phoneCallAvg)
            //                {
            //                    TimeSpan sub = call.PickedUpCallTime.Value - call.ReceivedCallTime.Value;
            //                    total += sub.TotalSeconds;
            //                }
            //                totalAmount += phoneCallAvg.Count;
            //            }
            //        }
            //    }

            //    double res = 0;

            //    if (total > 0 && totalAmount > 0)
            //    {
            //        res = total / totalAmount;
            //    }

            //    ObjectData objectData = new ObjectData();
            //    objectData.Code = supervisorCode;
            //    SaveIndicatorsValues(objectData, IndicatorClassData.Group, res);
            //}
            #endregion doc
        }

        private void CalculateOperators()
        {
            #region new
            string sql = @" SELECT		CONVERT(FLOAT,ISNULL(SUM(DATEDIFF(SS,PHR.RECEIVED_CALL_TIME,PHR.PICKED_UP_CALL_TIME)),0))
			/CASE COUNT(PHR.PICKED_UP_CALL_TIME) WHEN 0 THEN 1 ELSE COUNT(PHR.PICKED_UP_CALL_TIME) END AS TIEMPO_PROMEDIO
			, OPE.CODE AS USUARIO
FROM		REPORT_BASE AS REB,
			PHONE_REPORT AS PHR,
			OPERATOR AS OPE
WHERE		REB.CODE = PHR.PARENT_CODE
AND			REB.OPERATOR_CODE = OPE.CODE
AND			CONVERT(DATETIME,CONVERT(CHAR(10),PHR.PICKED_UP_CALL_TIME,101),101)= 
			CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
GROUP BY	OPE.CODE
";

            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);

            foreach (object[] item in data)
            {
                ObjectData od1 = new ObjectData();
                od1.Code = int.Parse(item[1].ToString());
                SaveIndicatorsValues(od1, IndicatorClassData.Operator,
                    double.Parse(item[0].ToString()));
            }
            #endregion new

            #region doc
            //IList operators = OperatorScheduleManager.GetOperatorsWorkingNowByApplication(UserApplicationData.FirstLevel.Name, false);
            //foreach (int operatorCode in operators)
            //{
            //    double res = 0;
            //    IList phoneCallAvg = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(
            //        SmartCadHqls.GetOperatorPhoneReportsCurrentday, 
            //        operatorCode,
            //        ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today)));

            //    if (phoneCallAvg.Count != 0)
            //    {
            //        foreach (PhoneReportData prd in phoneCallAvg)
            //        {
            //            TimeSpan ts = prd.PickedUpCallTime.Value.Subtract(prd.ReceivedCallTime.Value);
            //            res += ts.TotalSeconds;
            //        }
            //        res /= phoneCallAvg.Count;
            //    }

            //    ObjectData objectData = new ObjectData();
            //    objectData.Code = operatorCode;
            //    SaveIndicatorsValues(objectData, IndicatorClassData.Operator, res);
            //}
            #endregion doc
        }
    }
}
