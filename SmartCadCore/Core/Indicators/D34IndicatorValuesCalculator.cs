//using System;
//using System.Collections.Generic;
//using System.Text;
//using SmartCadCore.Model;
//using System.Collections;

//namespace SmartCadCore.Core.Indicators
//{
//    public class D34IndicatorValuesCalculator : IndicatorValuesCalculator
//    {
//        protected override string GetIndicator()
//        {
//            return "D34";
//        }

//        protected override void Calculate()
//        {
//            CalculateSystem();
//            CalculateGroup();
//            CalculateDepartment();
//        }

//        private void CalculateSystem()
//        {
//            #region new
//            string sql = @" SELECT		COUNT(SEH.USER_ACCOUNT_CODE) AS CANTIDAD
//			,OPS.CODE AS ESTADO			
//FROM        USER_APPLICATION AS USA
//			,SESSION_HISTORY AS SEH
//			,SESSION_STATUS_HISTORY AS STH
//			,OPERATOR_STATUS AS OPS
//WHERE		(
//			USA.CODE = 3
//			)
//AND			USA.CODE = SEH.USER_APPLICATION_CODE
//AND			SEH.CODE = STH.SESSION_HISTORY_CODE
//AND			STH.OPERATOR_STATUS_CODE = OPS.CODE
//AND			SEH.END_DATE_LOGIN IS NULL
//AND			STH.END_DATE IS NULL 
//AND         SEH.IS_LOGGED_IN = 1
//GROUP BY	OPS.CODE
//";

//            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);

//            double totalOperatorsConnected = 0;
//            double operatorsReady = 0;
//            List<double> resultsValues = new List<double>();
//            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();

//            foreach (object[] item in data)
//            {
//                totalOperatorsConnected += double.Parse(item[0].ToString());
//            }
//            foreach (object[] item in data)
//            {
//                if (double.Parse(item[1].ToString()) == OperatorStatusData.Busy.Code)
//                {
//                    operatorsReady = double.Parse(item[0].ToString());
//                    break;
//                }
//            }
//            resultsValues.Add(operatorsReady);
//            if (totalOperatorsConnected == 0)
//            {
//                resultsValues.Add(0);
//            }
//            else
//            {
//                resultsValues.Add((operatorsReady * 100) / totalOperatorsConnected);
//            }
//            result.Add("ReadyOperators", resultsValues);
//            SaveIndicatorMultiplesValues(null, IndicatorClassData.System, result);

//            #endregion new


//            #region doc
////            double percentAvailableOperators = 0;
////            string hql = @"select count(*) 
////                           from SessionHistoryData data inner join 
////                                data.SessionHistoryStatusList statuses
////                           where data.IsLoggedIn = true and
////                                 data.UserApplication.Code = {0} and
////                                 statuses.Status.Code = {1} and                                 
////                                 statuses.EndDate is null";
////            double operatorsBusy = (long)SmartCadDatabase.SearchBasicObject(
////                                                SmartCadHqls.GetCustomHql(
////                                                    hql,
////                                                    UserApplicationData.Dispatch.Code,
////                                                    OperatorStatusData.Busy.Code,
////                                                    false.ToString().ToLower()));

////            string hql2 = @"select count(*) 
////                         from SessionHistoryData data 
////                         where  data.UserApplication.Code = {0} and
////                         data.IsLoggedIn = {1}";

////            double dispatchOperatorsConnected = (long)SmartCadDatabase.SearchBasicObject(
////                                                SmartCadHqls.GetCustomHql(
////                                                    hql2,
////                                                    UserApplicationData.Dispatch.Code,
////                                                    true.ToString().ToLower()));



////            if ((dispatchOperatorsConnected) > 0)
////            {
////                percentAvailableOperators = (operatorsBusy * 100) /
////                    (dispatchOperatorsConnected);
////            }
////            else
////            {
////                percentAvailableOperators = 0;
////            }
////            percentAvailableOperators = Math.Round(percentAvailableOperators, 2);
////            List<double> resultsValues = new List<double>();
////            resultsValues.Add(operatorsBusy);
////            resultsValues.Add(percentAvailableOperators);
////            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
////            result.Add("AvailableOperators", resultsValues);
////            SaveIndicatorMultiplesValues(null, IndicatorClassData.System, result);

////            //SaveIndicatorsValues(IndicatorClassData.System, result);
//            #endregion doc
//        }

//        private void CalculateGroup()
//        {
//            #region new
//            string sql = @" SELECT		COUNT(SEH.USER_ACCOUNT_CODE) AS CANTIDAD
//			,OPS.CODE AS ESTADO			
//			,OPE.CODE AS SUPERVISOR
//FROM        USER_APPLICATION AS USA
//			,SESSION_HISTORY AS SEH
//			,SESSION_STATUS_HISTORY AS STH
//			,OPERATOR_STATUS AS OPS
//			,OPERATOR_ASSIGN AS OPA
//			,OPERATOR AS OPE
//WHERE		USA.CODE = SEH.USER_APPLICATION_CODE
//AND			(
//			USA.CODE = 2 
//			OR
//			USA.CODE = 3
//			)
//AND			SEH.CODE = STH.SESSION_HISTORY_CODE
//AND			STH.OPERATOR_STATUS_CODE = OPS.CODE
//AND			SEH.END_DATE_LOGIN IS NULL
//AND			STH.END_DATE IS NULL 
//AND         SEH.IS_LOGGED_IN = 1
//AND			SEH.USER_ACCOUNT_CODE = OPA.OPERATOR_CODE
//AND			OPA.DELETED_ID IS NULL
//AND			(
//				(				
//				GETDATE()
//				BETWEEN OPA.START_DATE AND OPA.END_DATE
//				)		
//			)
//AND			OPA.SUPERVISOR_CODE = OPE.CODE
//GROUP BY	OPS.CODE, OPE.CODE
//order by OPE.CODE
//";

//            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);

//            double totalOperatorsConnected = 0;
//            double operatorsReady = 0;


//            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//            List<double> resultsValues = new List<double>();
//            int supervisorCode = 0;

//            foreach (object[] item in data)
//            {
//                if (supervisorCode != 0 && supervisorCode != int.Parse(item[2].ToString()))
//                {
//                    resultsValues.Add(operatorsReady);
//                    if (totalOperatorsConnected == 0)
//                    {
//                        resultsValues.Add(0);
//                    }
//                    else
//                    {
//                        resultsValues.Add((operatorsReady * 100) / totalOperatorsConnected);
//                    }

//                    result.Add("ReadyOperators", resultsValues);

//                    ObjectData od1 = new ObjectData();
//                    od1.Code = supervisorCode;
//                    SaveIndicatorMultiplesValues(od1, IndicatorClassData.Group, result);
//                    result.Clear();
//                    resultsValues = new List<double>();
//                    totalOperatorsConnected = 0;
//                }
//                if (double.Parse(item[1].ToString()) == OperatorStatusData.Busy.Code)
//                {
//                    operatorsReady = double.Parse(item[0].ToString());
//                }
//                totalOperatorsConnected += double.Parse(item[0].ToString());
//                supervisorCode = (int)item[2];

//                if (item == data[data.Count - 1])
//                {
//                    resultsValues.Add(operatorsReady);
//                    if (totalOperatorsConnected == 0)
//                    {
//                        resultsValues.Add(0);
//                    }
//                    else
//                    {
//                        resultsValues.Add((operatorsReady * 100) / totalOperatorsConnected);
//                    }

//                    result.Add("ReadyOperators", resultsValues);

//                    ObjectData od1 = new ObjectData();
//                    od1.Code = supervisorCode;
//                    SaveIndicatorMultiplesValues(od1, IndicatorClassData.Group, result);
//                    result.Clear();
//                    resultsValues = new List<double>();
//                    totalOperatorsConnected = 0;
//                }
//            }
//            #endregion new

//            #region doc
//            //IList supervisors = OperatorScheduleManager.GetSupervisorsWorkingNowByApplication("DispatchSupervisorName");

//            //foreach (int supervisor in supervisors)
//            //{
//            //    IList operators = OperatorScheduleManager.GetOperatorsBySupervisor(supervisor);

//            //    double operatorsConnected = 0;
//            //    double operatorsBusy = 0;

//            //    double percetOperatorsBusy = 0;

//            //    foreach (OperatorData operatorData in operators)
//            //    {
//            //        double operatorReady = Convert.ToDouble(SmartCadDatabase.SearchBasicObject(
//            //            SmartCadHqls.GetCustomHql(
//            //            SmartCadHqls.IsLoggedInOperatorByApplicationOperatorStatus,
//            //            operatorData.Code,
//            //            UserApplicationData.Dispatch.Code,
//            //            OperatorStatusData.Busy.Code)));
//            //        if (operatorReady > 0)
//            //            operatorsBusy += 1;

//            //        double operatorConnected = Convert.ToDouble(SmartCadDatabase.SearchBasicObject(
//            //            SmartCadHqls.GetCustomHql(
//            //               SmartCadHqls.IsLoggedOperatorInApplication,
//            //               operatorData.Code, true.ToString().ToLower(),
//            //               UserApplicationData.Dispatch.Code)));

//            //        if (operatorConnected > 0)
//            //            operatorsConnected += 1;
//            //    }

//            //    if (operatorsConnected > 0)
//            //    {
//            //        percetOperatorsBusy = (operatorsBusy * 100) / (operatorsConnected);
//            //    }
//            //    else
//            //    {
//            //        percetOperatorsBusy = 0;
//            //    }
//            //    percetOperatorsBusy = Math.Round(percetOperatorsBusy, 2);
//            //    List<double> resultsValues = new List<double>();
//            //    resultsValues.Add(operatorsBusy);
//            //    resultsValues.Add(percetOperatorsBusy);
//            //    Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//            //    result.Add("BusyOperators", resultsValues);
//            //    ObjectData objectdata = new ObjectData();
//            //    objectdata.Code = supervisor;
//            //    SaveIndicatorMultiplesValues(objectdata, IndicatorClassData.Group, result);
//            //}
//            #endregion doc
//        }

//        //ArrayList departmentsAllZeroValue = new ArrayList() { 1, 2, 3, 4, 5, 6, 7, 8 };
//        private void CalculateDepartment()
//        {
//            #region new
//            string sql = @" SELECT		COUNT(SEH.USER_ACCOUNT_CODE) AS CANTIDAD
//			,OPS.CODE AS ESTADO
//			,DETY.CODE AS ORGANISMO
//FROM        USER_APPLICATION AS USA
//			,SESSION_HISTORY AS SEH
//			,SESSION_STATUS_HISTORY AS STH
//			,OPERATOR_STATUS AS OPS
//			,OPERATOR_DEPARTMENT_TYPE AS ODT
//			,DEPARTMENT_TYPE AS DETY
//WHERE		(USA.CODE = 2 OR USA.CODE = 3)
//AND			USA.CODE = SEH.USER_APPLICATION_CODE
//AND			SEH.CODE = STH.SESSION_HISTORY_CODE
//AND			STH.OPERATOR_STATUS_CODE = OPS.CODE
//AND			SEH.END_DATE_LOGIN IS NULL
//AND			STH.END_DATE IS NULL 
//AND         SEH.IS_LOGGED_IN = 1
//AND			SEH.USER_ACCOUNT_CODE = ODT.OPERATOR_CODE
//AND			ODT.DEPARTMENT_TYPE_CODE = DETY.CODE
//GROUP BY	OPS.CODE, DETY.CODE
//order by DETY.CODE
//
//";

//            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);

//            double totalOperatorsConnected = 0;
//            double operatorsReady = 0;


//            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//            List<double> resultsValues = new List<double>();
//            int departmentCode = 0;

//            foreach (object[] item in data)
//            {
//                if (departmentCode != 0 && departmentCode != int.Parse(item[2].ToString()))
//                {
//                    resultsValues.Add(operatorsReady);
//                    if (totalOperatorsConnected == 0)
//                    {
//                        resultsValues.Add(0);
//                    }
//                    else
//                    {
//                        resultsValues.Add((operatorsReady * 100) / totalOperatorsConnected);
//                    }

//                    result.Add("ReadyOperators", resultsValues);

//                    ObjectData od1 = new ObjectData();
//                    od1.Code = departmentCode;
//                    SaveIndicatorMultiplesValues(od1, IndicatorClassData.Department, result);
//                    result.Clear();
//                    resultsValues = new List<double>();
//                    totalOperatorsConnected = 0;
//                }
//                if (double.Parse(item[1].ToString()) == OperatorStatusData.Busy.Code)
//                {
//                    operatorsReady = double.Parse(item[0].ToString());
//                }
//                totalOperatorsConnected += double.Parse(item[0].ToString());
//                departmentCode = (int)item[2];

//                if (item == data[data.Count - 1])
//                {
//                    resultsValues.Add(operatorsReady);
//                    if (totalOperatorsConnected == 0)
//                    {
//                        resultsValues.Add(0);
//                    }
//                    else
//                    {
//                        resultsValues.Add((operatorsReady * 100) / totalOperatorsConnected);
//                    }

//                    result.Add("ReadyOperators", resultsValues);

//                    ObjectData od1 = new ObjectData();
//                    od1.Code = departmentCode;
//                    SaveIndicatorMultiplesValues(od1, IndicatorClassData.Department, result);
//                    result.Clear();
//                    resultsValues = new List<double>();
//                    totalOperatorsConnected = 0;
//                }
//            }
//            #endregion new

//            #region doc
//            //foreach (int item in departmentsAllZeroValue)
//            //{
//            //    Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//            //    List<double> resultsValues = new List<double>();
//            //    resultsValues.Add(0);
//            //    resultsValues.Add(0);
//            //    result.Add("Empty", resultsValues);
//            //    ObjectData od = new ObjectData();
//            //    od.Code = item;
//            //    SaveIndicatorMultiplesValues(od, IndicatorClassData.Department, result);
//            //}

//            //IList operatorConnected = SmartCadDatabase.SearchObjects(
//            //           SmartCadHqls.GetCustomHql(
//            //              SmartCadHqls.GetOperatorCodeByApp,
//            //              UserApplicationData.Dispatch.Code));
//            //Dictionary<int, int> operatorDepartmenCount = new Dictionary<int, int>();



//            //foreach (int code in operatorConnected)
//            //{
//            //    double operatorBusy = Convert.ToDouble(SmartCadDatabase.SearchBasicObject(
//            //          SmartCadHqls.GetCustomHql(
//            //          SmartCadHqls.IsLoggedInOperatorByApplicationOperatorStatus,
//            //          code,
//            //          UserApplicationData.Dispatch.Code,
//            //          OperatorStatusData.Busy.Code)));
//            //    if (operatorBusy > 0)
//            //    {
//            //        IList operatorDepartment = SmartCadDatabase.SearchObjects(
//            //                   SmartCadHqls.GetCustomHql(
//            //                      SmartCadHqls.GetOperatorDepartmentTypeByOperatorCode,
//            //                      code));
//            //        foreach (DepartmentTypeData department in operatorDepartment)
//            //        {
//            //            if (operatorDepartmenCount.ContainsKey(department.Code))
//            //            {
//            //                int departmentValue = 0;
//            //                operatorDepartmenCount.TryGetValue(department.Code, out departmentValue);
//            //                departmentValue += 1;
//            //                operatorDepartmenCount[department.Code] = departmentValue;
//            //            }
//            //            else
//            //            {
//            //                operatorDepartmenCount.Add(department.Code, 1);
//            //            }
//            //        }
//            //    }

//            //}
//            //DateTime now = SmartCadDatabase.GetTimeFromBD();
//            ////DateTime thisTime = ApplicationUtil.SCHEDULE_REFERENCE_DATE;
//            ////thisTime = thisTime.AddHours(now.Hour);
//            ////thisTime = thisTime.AddMinutes(now.Minute);
//            ////thisTime = thisTime.AddSeconds(now.Second);
//            ////thisTime = thisTime.AddMilliseconds(now.Millisecond);
//            //foreach (KeyValuePair<int, int> pair in operatorDepartmenCount)
//            //{
//            //    double operatorsConnectedByDepartment = 0;
//            //    double percentOperatorsBusy = 0;

//            //    IList operatorsWorkingNowByDepartment = SmartCadDatabase.SearchObjects(
//            //        SmartCadHqls.GetCustomHql(SmartCadHqls.GetSupervisorWorkingNowByDepartment,
//            //       ApplicationUtil.GetDataBaseFormattedDate(now),pair.Key));
//            //    foreach (OperatorData operatorData in operatorsWorkingNowByDepartment)
//            //    {
//            //        if (OperatorScheduleManager.IsSupervisor(operatorData.Code) == false)
//            //        {
//            //            double operatorDepartmentConnected = Convert.ToDouble(SmartCadDatabase.SearchBasicObject(
//            //           SmartCadHqls.GetCustomHql(
//            //              SmartCadHqls.IsLoggedOperatorInApplication,
//            //              operatorData.Code, true.ToString().ToLower(),
//            //              UserApplicationData.Dispatch.Code)));
//            //            if (operatorDepartmentConnected > 0)
//            //            {
//            //                operatorsConnectedByDepartment += 1;
//            //            }
//            //        }
//            //    }
//            //    if (operatorsConnectedByDepartment > 0)
//            //    {
//            //        percentOperatorsBusy = (pair.Value * 100) / operatorsConnectedByDepartment;
//            //    }
//            //    percentOperatorsBusy = Math.Round(percentOperatorsBusy, 2);
//            //    List<double> resultsValues = new List<double>();
//            //    resultsValues.Add(pair.Value);
//            //    resultsValues.Add(percentOperatorsBusy);
//            //    Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//            //    result.Add("BusyOperators", resultsValues);
//            //    ObjectData od = new ObjectData();
//            //    od.Code = pair.Key;
//            //    SaveIndicatorMultiplesValues(od, IndicatorClassData.Department, result);
//            //}
//            #endregion doc
//        }
//    }
//}
