//using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Collections;
//using SmartCadCore.Model;

//namespace SmartCadCore.Core.Indicators
//{
//    public class D49IndicatorValuesCalculator : IndicatorValuesCalculator
//    {
//        protected override string GetIndicator()
//        {
//            return "D49";
//        }

//        protected override void Calculate()
//        {
//            CalculateGroup();
//            CalculateDepartment();
//        }

//        private void CalculateGroup()
//        {
//            IList supervisors = OperatorScheduleManager.GetSupervisorsWorkingNowByApplication("DispatchSupervisorName");

//            foreach (int supervisor in supervisors)
//            {
//                IList operators = OperatorScheduleManager.GetAllDayOperatorsBySupervisor(supervisor);

//                WorkShiftScheduleVariationData bshSupervisor =
//                 OperatorScheduleManager.GetCurrentWorkShiftSchedulesByOperator(supervisor, SmartCadDatabase.GetTimeFromBD().Date);

//                double totalIncidentNotification = 0;
//                double totalCancelledIncidentNotification = 0;
//                double percentCancelledIncidentNotification = 0;
//                foreach (OperatorData operatorData in operators)
//                {
//                    IList bshList =
//                      OperatorScheduleManager.GetAllDayOperatorAssignBySupervisorAndByDayRealHours(supervisor, operatorData.Code);

//                    foreach (OperatorAssignData bsh in bshList)
//                    {
//                        if ((bshSupervisor != null) &&
//                             (((bsh.StartDate.TimeOfDay >= bshSupervisor.Start.Value.TimeOfDay) &&
//                            (bsh.StartDate.TimeOfDay <= bshSupervisor.End.Value.TimeOfDay)) ||
//                            ((bsh.EndDate.TimeOfDay <= bshSupervisor.End.Value.TimeOfDay) &&
//                            (bsh.EndDate.TimeOfDay >= bshSupervisor.Start.Value.TimeOfDay))))
//                        {
//                            IList incidentsNotification = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(
//                                SmartCadHqls.GetIncidentNotificationByEndDateByOperator,
//                                 operatorData.Code,
//                                 ApplicationUtil.GetDataBaseFormattedDate(bsh.StartDate),
//                                 ApplicationUtil.GetDataBaseFormattedDate(bsh.EndDate)));

//                            foreach (IncidentNotificationData incidentnotification in incidentsNotification)
//                            {
//                                if (incidentnotification.Status == IncidentNotificationStatusData.Cancelled)
//                                {
//                                    totalCancelledIncidentNotification += 1;
//                                }
//                                //else if (incidentnotification.Status == IncidentNotificationStatusData.Closed)
//                                //{
//                                //    if (incidentnotification.EndDate < DateTime.Today)
//                                //    {
//                                //        totalIncidentNotification -= 1;
//                                //    }
//                                //}
//                            }
//                            totalIncidentNotification += incidentsNotification.Count;
//                        }
//                    }
//                }

//                if (totalIncidentNotification > 0)
//                {
//                    percentCancelledIncidentNotification = (totalCancelledIncidentNotification * 100) / (totalIncidentNotification);
//                }
//                else
//                {
//                    percentCancelledIncidentNotification = 0;
//                }

//                List<double> resultsValues = new List<double>();
//                resultsValues.Add(totalCancelledIncidentNotification);
//                resultsValues.Add(percentCancelledIncidentNotification);
//                Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//                result.Add("CancelledIncidents", resultsValues);
//                ObjectData objectdata = new ObjectData();
//                objectdata.Code = supervisor;
//                SaveIndicatorMultiplesValues(objectdata, IndicatorClassData.Group, result);
//            }
//        }

//        ArrayList departmentsAllZeroValue = new ArrayList() { 1, 2, 3, 4, 5, 6, 7, 8 };
//        private void CalculateDepartment()
//        {
//            foreach (int item in departmentsAllZeroValue)
//            {
//                Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//                List<double> resultsValues = new List<double>();
//                resultsValues.Add(0);
//                resultsValues.Add(0);
//                result.Add("Empty", resultsValues);
//                ObjectData od = new ObjectData();
//                od.Code = item;
//                SaveIndicatorMultiplesValues(od, IndicatorClassData.Department, result);
//            }

//            IList IncidentNotificationByDepartment = (IList)SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(
//                  SmartCadHqls.GetIncidentNotificationCountByEndDateByDepartmentByStatus,
//                  IncidentNotificationStatusData.Cancelled.Code, ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today)));


//            double percentOpenIncidents = 0;

//            for (int i = 0; i < IncidentNotificationByDepartment.Count; i++)
//            {
//                Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//                List<double> resultsValues = new List<double>();

//                int currentDTCode = (int)((object[])IncidentNotificationByDepartment[i])[0];
//                object[] item = (object[])IncidentNotificationByDepartment[i];

//                double totalIncidents = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(
//                       SmartCadHqls.GetAllIncidentNotificationByDepartment, currentDTCode,
//                       ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today)));

//                if (totalIncidents > 0)
//                {
//                    percentOpenIncidents = (Convert.ToDouble(item[1]) * 100) / (totalIncidents);
//                }
//                else
//                {
//                    percentOpenIncidents = 0;
//                }

//                resultsValues.Add(Convert.ToDouble(item[1]));
//                resultsValues.Add(percentOpenIncidents);
//                result.Add("CancelledIncidentsByDepartment", resultsValues);
//                ObjectData od = new ObjectData();
//                od.Code = currentDTCode;
//                SaveIndicatorMultiplesValues(od, IndicatorClassData.Department, result);
//            }
//        }


     
//    }
//}
