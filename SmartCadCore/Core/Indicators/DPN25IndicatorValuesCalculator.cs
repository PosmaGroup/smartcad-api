using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using System.Collections;
using SmartCadCore.Common;

namespace SmartCadCore.Core.Indicators
{
    public class DPN25IndicatorValuesCalculator : IndicatorValuesCalculator
    {
        protected override string GetIndicator()
        {
            return "DPN25";
        }

        protected override void Calculate()
        {
            CalculateGroup();
        }

        private void CalculateGroup()
        {
            #region new
            string sql = @" SELECT		COUNT(INOT.CODE) AS CANTIDAD
			,INS.CODE AS ESTADO
			,OPE.CODE AS SUPERVISOR
FROM		INCIDENT_NOTIFICATION AS INOT
			,INCIDENT_NOTIFICATION_STATUS  AS INS
			,OPERATOR_ASSIGN AS OPA
			,OPERATOR AS OPE
WHERE		INOT.INCIDENT_NOTIFICATION_STATUS_CODE = INS.CODE
AND			(	
				CONVERT(DATETIME,CONVERT(CHAR(10),INOT.END_DATE, 101),101) = CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(), 101),101) 
				OR 
				INOT.END_DATE IS NULL
			)
AND			INOT.USER_ACCOUNT_CODE = OPA.OPERATOR_CODE
AND			OPA.DELETED_ID IS NULL
AND			(
				(				
				GETDATE()
				BETWEEN OPA.START_DATE AND OPA.END_DATE
				)
			)
AND			OPA.SUPERVISOR_CODE = OPE.CODE
GROUP BY	INS.CODE,OPE.CODE
order by OPE.CODE, INS.CODE
";

            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);

            double totalDispatchOrder = 0;
            double dispatchOrderOpen = 0;
            double dispatchOrderClosed = 0;

            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
            List<double> resultsValues = new List<double>();
            int supervisorCode = 0;


            foreach (object[] item in data)
            {
                if (supervisorCode != 0 && supervisorCode != int.Parse(item[2].ToString()))
                {
                    ObjectData od1 = new ObjectData();
                    od1.Code = supervisorCode;
                    resultsValues.Add(dispatchOrderOpen);
                    if (totalDispatchOrder == 0)
                    {
                        resultsValues.Add(0);
                    }
                    else
                    {
                        resultsValues.Add((dispatchOrderOpen * 100) / totalDispatchOrder);
                    }

                    result.Add(ResourceLoader.GetString2("IndicatorDPN17Name"), resultsValues);


                    SaveIndicatorMultiplesValues(od1, IndicatorClassData.Group, result);
                    result.Clear();
                    resultsValues = new List<double>();


                    resultsValues.Add(dispatchOrderClosed);
                    if (totalDispatchOrder == 0)
                    {
                        resultsValues.Add(0);
                    }
                    else
                    {
                        resultsValues.Add((dispatchOrderClosed * 100) / totalDispatchOrder);
                    }

                    result.Add(ResourceLoader.GetString2("IndicatorDPN18Name"), resultsValues);


                    SaveIndicatorMultiplesValues(od1, IndicatorClassData.Group, result);
                    result.Clear();
                    resultsValues = new List<double>();
                    totalDispatchOrder = 0;
                    dispatchOrderClosed = 0;
                    dispatchOrderOpen = 0;
                }
                if ((double.Parse(item[1].ToString()) == IncidentNotificationStatusData.Assigned.Code) ||
                    (double.Parse(item[1].ToString()) == IncidentNotificationStatusData.Delayed.Code) ||
                    (double.Parse(item[1].ToString()) == IncidentNotificationStatusData.InProgress.Code) ||
                    (double.Parse(item[1].ToString()) == IncidentNotificationStatusData.Pending.Code))
                {
                    dispatchOrderOpen = double.Parse(item[0].ToString());
                }
                else if ((double.Parse(item[1].ToString()) == IncidentNotificationStatusData.Cancelled.Code) ||
                    (double.Parse(item[1].ToString()) == IncidentNotificationStatusData.Closed.Code))
                {
                    dispatchOrderClosed = double.Parse(item[0].ToString());
                }
                totalDispatchOrder += double.Parse(item[0].ToString());
                supervisorCode = (int)item[2];

                if (item == data[data.Count - 1])
                {
                    ObjectData od1 = new ObjectData();
                    od1.Code = supervisorCode;
                    resultsValues.Add(dispatchOrderOpen);
                    if (totalDispatchOrder == 0)
                    {
                        resultsValues.Add(0);
                    }
                    else
                    {
                        resultsValues.Add((dispatchOrderOpen * 100) / totalDispatchOrder);
                    }

                    result.Add(ResourceLoader.GetString2("IndicatorDPN17Name"), resultsValues);


                    SaveIndicatorMultiplesValues(od1, IndicatorClassData.Group, result);
                    result.Clear();
                    resultsValues = new List<double>();


                    resultsValues.Add(dispatchOrderClosed);
                    if (totalDispatchOrder == 0)
                    {
                        resultsValues.Add(0);
                    }
                    else
                    {
                        resultsValues.Add((dispatchOrderClosed * 100) / totalDispatchOrder);
                    }

                    result.Add(ResourceLoader.GetString2("IndicatorDPN18Name"), resultsValues);


                    SaveIndicatorMultiplesValues(od1, IndicatorClassData.Group, result);
                    result.Clear();
                    resultsValues = new List<double>();
                    totalDispatchOrder = 0;
                    dispatchOrderClosed = 0;
                    dispatchOrderOpen = 0;
                }
            }
            #endregion new
            
        }   


    }
}

