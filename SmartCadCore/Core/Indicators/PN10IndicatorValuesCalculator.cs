using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using System.Collections;

namespace SmartCadCore.Core.Indicators
{
    public class PN10IndicatorValuesCalculator : IndicatorValuesCalculator
    {
        protected override string GetIndicator()
        {
            return "PN10";
        }

        protected override void Calculate()
        {            
            CalculateOperators();
        }

        private void CalculateOperators()
        {
            #region new
            string sql = @"DECLARE		@CURSOR_ESTADOS CURSOR;
DECLARE		@ESTADO NVARCHAR(2);
DECLARE		@OPE_CODE_ESTADO NVARCHAR(2000);
DECLARE		@ACUMULADO_ESTADO FLOAT;
DECLARE		@TOTAL_ESTADO_DISP FLOAT;

DECLARE		@CURSOR_TOTAL CURSOR;
DECLARE		@OPE_CODE_TOTAL NVARCHAR(2000);
DECLARE		@ACUMULADO_GENERAL FLOAT;

DECLARE		@CURSOR_PAUSAS_MAXIMOS CURSOR;
DECLARE		@ESTADO_MAXIMO NVARCHAR(2);
DECLARE		@OPE_CODE_MAXIMO NVARCHAR(2000);
DECLARE		@ACUMULADO_MAXIMO FLOAT;

DECLARE		@CURSOR_ENTRENAMIENTO CURSOR;
DECLARE		@OPE_CODE_ENTRENAMIENTO NVARCHAR(2000);
DECLARE		@ACUMULADO_ENTRENAMIENTO FLOAT;

DECLARE		@CURSOR_PERMISO CURSOR;
DECLARE		@OPE_CODE_PERMISO NVARCHAR(2000);
DECLARE		@ACUMULADO_PERMISO FLOAT;

DECLARE		@FECHA_DE_CALCULO DATETIME;
/* INICIALIZACION DE VARIABLES GLOBALES */
SET @TOTAL_ESTADO_DISP = 0;



/* FECHA PARA EL CALCULO */
SET @FECHA_DE_CALCULO = GETDATE();

IF OBJECT_ID('tempdb..#TEMPTABLE') IS NULL
     
 CREATE TABLE #TEMPTABLE
 (
ADHERENCIA FLOAT,
OPERADOR FLOAT 
)

/* DECLARACION DE CURSORES */

/* CURSOR CON TIEMPO DE CONEXION POR ESTADO AGRUPADO POR OPERADOR Y ESTADO*/
SET @CURSOR_ESTADOS = CURSOR STATIC LOCAL FOR 
									SELECT		CONVERT(FLOAT,SUM(DATEDIFF(SS,
																CASE
																	WHEN OPA.START_DATE > SSH.START_DATE
													                THEN OPA.START_DATE
																	ELSE SSH.START_DATE
																END
																,CASE
																	WHEN OPA.START_DATE > SSH.END_DATE
																	THEN OPA.START_DATE
																	ELSE ISNULL(SSH.END_DATE,CASE
																								WHEN OPA.END_DATE < @FECHA_DE_CALCULO
																								THEN OPA.END_DATE
																								ELSE @FECHA_DE_CALCULO
																							END )
																	END
																	)
																))AS ACUMULADO
																,OPE.CODE AS OPE_CODE
																,SSH.OPERATOR_STATUS_CODE AS STATUS
									FROM		OPERATOR AS OPE
												,SESSION_HISTORY AS SEH
												,SESSION_STATUS_HISTORY AS SSH
												,OPERATOR_ASSIGN AS OPA
									WHERE		OPE.DELETED_ID IS NULL
									AND			SEH.USER_ACCOUNT_CODE = OPE.CODE
									AND			SEH.DELETED_ID IS NULL
									AND			SEH.USER_APPLICATION_CODE IN (3,2)
									AND			SSH.SESSION_HISTORY_CODE = SEH.CODE
									AND			SSH.DELETED_ID IS NULL
									AND			OPA.OPERATOR_CODE = OPE.CODE
									AND			OPA.DELETED_ID IS NULL
									AND			(
													CONVERT(DATETIME,CONVERT(CHAR(10),OPA.START_DATE,101),101) = 
													CONVERT(DATETIME,CONVERT(CHAR(10),@FECHA_DE_CALCULO,101),101)
												)
									AND			(
													CONVERT(DATETIME,CONVERT(CHAR(10),SSH.END_DATE,101),101) = 
													CONVERT(DATETIME,CONVERT(CHAR(10),@FECHA_DE_CALCULO,101),101)
													OR
													SSH.END_DATE IS NULL
												)
									--AND			SSH.START_DATE>=OPA.START_DATE
									AND			SSH.START_DATE<=OPA.END_DATE	
									GROUP BY	SSH.OPERATOR_STATUS_CODE,OPE.CODE
									ORDER BY	OPE.CODE,SSH.OPERATOR_STATUS_CODE ASC

/* CURSOR CON TOTAL DE TURNO HASTA TIEMPO ACTUAL */
SET @CURSOR_TOTAL = CURSOR STATIC LOCAL FOR 
									SELECT	OPE.CODE AS OPE_CODE
											,CONVERT(FLOAT,SUM(DATEDIFF(SS
														,OPA.START_DATE
														,CASE 
															WHEN OPA.WORK_SHIFT_SCHEDULE_VARIATION_CODE IS NULL THEN 
																													(CASE
																														WHEN OPA.END_DATE > @FECHA_DE_CALCULO THEN
																															 @FECHA_DE_CALCULO
																														ELSE OPA.END_DATE 
																													 END)
															ELSE (CASE
																	WHEN OPA.END_DATE > @FECHA_DE_CALCULO THEN
																		 @FECHA_DE_CALCULO
																	ELSE OPA.END_DATE 
																 END
																)
														 END))) AS TOTAL_ACUMULADO
									FROM	OPERATOR AS OPE
											,OPERATOR_ASSIGN AS OPA
									WHERE	OPE.DELETED_ID IS NULL
									AND		OPE.CODE = OPA.OPERATOR_CODE
									AND		OPA.DELETED_ID IS NULL
									AND		CONVERT(DATETIME,CONVERT(CHAR(10),OPA.START_DATE,101),101) = 
											CONVERT(DATETIME,CONVERT(CHAR(10),@FECHA_DE_CALCULO,101),101)
									GROUP BY OPE.CODE	

/* CURSOR CON TIEMPO MAXIMO DE CADA PAUSA POR OPERADOR */
SET @CURSOR_PAUSAS_MAXIMOS = CURSOR STATIC LOCAL FOR 											
									SELECT	(TOTAL_PAUSA.TOTAL*PAUSA.PORCENTAJE/100)+((TOTAL_PAUSA.TOTAL*PAUSA.PORCENTAJE/100)*TOLERANCIA/100) AS TOTAL_PERMITIDO
											,PAUSA.PAUSA AS CODIGO_PAUSA
											,TOTAL_PAUSA.OPE_CODE AS OPE_CODE
									FROM 
									(
									SELECT	OPE.CODE AS OPE_CODE
											,CONVERT(FLOAT,SUM(DATEDIFF(SS
														,OPA.START_DATE
														,OPA.END_DATE
														))
												) AS TOTAL
									FROM	OPERATOR AS OPE
											,OPERATOR_ASSIGN AS OPA
									WHERE	OPE.DELETED_ID IS NULL
									AND		OPE.CODE = OPA.OPERATOR_CODE
									AND		OPA.DELETED_ID IS NULL
									AND		(
												(
														OPA.START_DATE
														<= @FECHA_DE_CALCULO
												AND		OPA.END_DATE
														>= @FECHA_DE_CALCULO
												)
											) 
									GROUP BY OPE.CODE
									) AS TOTAL_PAUSA
									,(
									SELECT		OPS.CODE AS PAUSA,OPS.BREAK_PORCENTAGE AS PORCENTAJE, OPS.BREAK_TOLERANCE AS TOLERANCIA
									FROM		OPERATOR_STATUS AS OPS
									WHERE		OPS.BREAK_PORCENTAGE <> 0 
									AND			OPS.DELETED_ID IS NULL
									) AS PAUSA
									ORDER BY TOTAL_PAUSA.OPE_CODE, PAUSA.PAUSA ASC											

/* CURSOR CON TIEMPO DE ENTRENAMIENTO POR CADA OPERADOR */
SET @CURSOR_ENTRENAMIENTO = CURSOR STATIC LOCAL FOR 											
									SELECT	ENTRENAMIENTO.TOTAL_ENTRENAMIENTO
											-
											SUM(CASE 
													WHEN ENTRENAMIENTO.TOTAL_ONLINE < 0 THEN 0
													ELSE ENTRENAMIENTO.TOTAL_ONLINE
												END) AS TOTAL_ENTRENAMIENTO
											,ENTRENAMIENTO.OPE_CODE_TRA AS OPE_CODE_TRA
									FROM	(SELECT		CONVERT(FLOAT,(DATEDIFF(SS,TCSP.START_DATE,
																	CASE 
																		WHEN TCSP.END_DATE > @FECHA_DE_CALCULO THEN @FECHA_DE_CALCULO
																		ELSE TCSP.END_DATE
																	END))
														)AS TOTAL_ENTRENAMIENTO
														,
														CONVERT(FLOAT,(
																		DATEDIFF(SS,
																					CASE 
																						WHEN SEH.START_DATE_LOGIN < TCSP.START_DATE THEN TCSP.START_DATE
																						ELSE SEH.START_DATE_LOGIN
																					END
																					,CASE 
																						WHEN ISNULL(SEH.END_DATE_LOGIN,@FECHA_DE_CALCULO) > TCSP.END_DATE THEN TCSP.END_DATE
																						ELSE ISNULL(SEH.END_DATE_LOGIN,@FECHA_DE_CALCULO)
																					END )))AS TOTAL_ONLINE
														,OTC.OPERATOR_CODE AS OPE_CODE_TRA
											FROM		OPERATOR_TRAINING_COURSE AS OTC,
														TRAINING_COURSE_SCHEDULE AS TCS,
														TRAINING_COURSE_SCHEDULE_PARTS AS TCSP,
														SESSION_HISTORY AS SEH,
														OPERATOR_ASSIGN AS OPA
											WHERE		OTC.DELETED_ID IS NULL
											AND			OTC.TRAINING_COURSE_SCHEDULE_CODE = TCS.CODE
											AND			TCS.DELETED_ID IS NULL
											AND			TCS.CODE = TCSP.TRAINING_COURSE_SCHEDULE_CODE
											AND			TCSP.DELETED_ID IS NULL
											AND			(TCSP.START_DATE  BETWEEN (OPA.START_DATE) AND (OPA.END_DATE)
											OR			TCSP.END_DATE BETWEEN (OPA.START_DATE) AND (OPA.END_DATE))
											AND			TCSP.START_DATE <= @FECHA_DE_CALCULO
											AND			SEH.USER_ACCOUNT_CODE = OTC.OPERATOR_CODE
											AND			SEH.DELETED_ID IS NULL
											AND			SEH.USER_APPLICATION_CODE IN (3,2)
											AND			CONVERT(DATETIME,CONVERT(CHAR(10),SEH.START_DATE_LOGIN,101),101) = 
														CONVERT(DATETIME,CONVERT(CHAR(10),@FECHA_DE_CALCULO,101),101)
											AND			OPA.OPERATOR_CODE = OTC.OPERATOR_CODE
											AND			OPA.DELETED_ID IS NULL
											) AS ENTRENAMIENTO
									GROUP BY ENTRENAMIENTO.TOTAL_ENTRENAMIENTO,ENTRENAMIENTO.OPE_CODE_TRA										

/* ABRIENDO CURSORES */
OPEN @CURSOR_TOTAL
OPEN @CURSOR_ESTADOS
OPEN @CURSOR_PAUSAS_MAXIMOS
OPEN @CURSOR_ENTRENAMIENTO

/* CALCULO DE ADHERENCIA */
FETCH FIRST FROM @CURSOR_TOTAL INTO @OPE_CODE_TOTAL,@ACUMULADO_GENERAL
				
WHILE @@FETCH_STATUS = 0
BEGIN
		FETCH FIRST FROM @CURSOR_ESTADOS INTO @ACUMULADO_ESTADO,@OPE_CODE_ESTADO,@ESTADO
				
		WHILE @@FETCH_STATUS = 0
		BEGIN
				IF (@OPE_CODE_TOTAL = @OPE_CODE_ESTADO)
				BEGIN
					IF (@ESTADO <= 2)--ESTADO DISPONIBLE(OCUPADO/DISPONIBLE)
					BEGIN
						SET @TOTAL_ESTADO_DISP = @TOTAL_ESTADO_DISP + @ACUMULADO_ESTADO
					END
					ELSE--OTROS ESTADOS
					BEGIN
						FETCH FIRST FROM @CURSOR_PAUSAS_MAXIMOS INTO @ACUMULADO_MAXIMO,@ESTADO_MAXIMO,@OPE_CODE_MAXIMO
				
						WHILE @@FETCH_STATUS = 0
						BEGIN
								IF (@OPE_CODE_TOTAL = @OPE_CODE_MAXIMO)
								BEGIN
									IF (@ESTADO = @ESTADO_MAXIMO)
									BEGIN
										IF(@ACUMULADO_ESTADO > @ACUMULADO_MAXIMO)
										BEGIN
											--SET @TOTAL_ESTADO_DISP = @TOTAL_ESTADO_DISP - (@ACUMULADO_ESTADO - @ACUMULADO_MAXIMO)
											SET @ACUMULADO_GENERAL = @ACUMULADO_GENERAL - @ACUMULADO_MAXIMO
										END
										ELSE
										BEGIN
											SET @ACUMULADO_GENERAL = @ACUMULADO_GENERAL - @ACUMULADO_ESTADO
										END
									END
								END
								FETCH NEXT FROM @CURSOR_PAUSAS_MAXIMOS INTO @ACUMULADO_MAXIMO,@ESTADO_MAXIMO,@OPE_CODE_MAXIMO
						END
					END
				END
				FETCH NEXT FROM @CURSOR_ESTADOS INTO @ACUMULADO_ESTADO,@OPE_CODE_ESTADO,@ESTADO
		END
		--CALCULO DE LOS ENTRENAMIENTOS
		
		FETCH FIRST FROM @CURSOR_ENTRENAMIENTO INTO @ACUMULADO_ENTRENAMIENTO,@OPE_CODE_ENTRENAMIENTO
				
		WHILE @@FETCH_STATUS = 0
		BEGIN
				IF (@OPE_CODE_TOTAL = @OPE_CODE_ENTRENAMIENTO)
				BEGIN
					--select @ACUMULADO_ENTRENAMIENTO AS ENTRENAMIENTO
					SET @TOTAL_ESTADO_DISP = @TOTAL_ESTADO_DISP + @ACUMULADO_ENTRENAMIENTO
				END
				FETCH NEXT FROM @CURSOR_ENTRENAMIENTO INTO @ACUMULADO_ENTRENAMIENTO,@OPE_CODE_ENTRENAMIENTO
		END
		
		
		--VERIFICACION DE LIMITES SUPERIOR E INFERIOR

		IF (@TOTAL_ESTADO_DISP <= 0)
		BEGIN
			SET @TOTAL_ESTADO_DISP = 0;
		END
		IF (@ACUMULADO_GENERAL <= 0)
		BEGIN
			SET @ACUMULADO_GENERAL = 1;
		END
		IF (@TOTAL_ESTADO_DISP > @ACUMULADO_GENERAL)
		BEGIN
			SET @TOTAL_ESTADO_DISP = @ACUMULADO_GENERAL;
		END
		--CALCULO DE LA ADHERRENCIA
		--SELECT ROUND((@TOTAL_ESTADO_DISP*100)/@ACUMULADO_GENERAL,2) AS ADHERENCIA, OPE.LOGIN AS OPERADOR FROM OPERATOR AS OPE WHERE OPE.CODE = @OPE_CODE_TOTAL



INSERT INTO #TEMPTABLE VALUES (ROUND((@TOTAL_ESTADO_DISP*100)/@ACUMULADO_GENERAL,2),CONVERT(FLOAT, @OPE_CODE_TOTAL))
		

		SET @TOTAL_ESTADO_DISP = 0;
		FETCH NEXT FROM @CURSOR_TOTAL INTO @OPE_CODE_TOTAL,@ACUMULADO_GENERAL
END
SELECT * FROM #TEMPTABLE
drop  table #TEMPTABLE
CLOSE @CURSOR_TOTAL
CLOSE @CURSOR_ESTADOS
CLOSE @CURSOR_PAUSAS_MAXIMOS
CLOSE @CURSOR_ENTRENAMIENTO
DEALLOCATE @CURSOR_TOTAL
DEALLOCATE @CURSOR_ESTADOS
DEALLOCATE @CURSOR_PAUSAS_MAXIMOS
";




            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);
            foreach (object[] item in data)
            {

                ObjectData objectData = new ObjectData();
                objectData.Code = int.Parse(item[1].ToString());
                SaveIndicatorsValues(objectData, IndicatorClassData.Operator, double.Parse(item[0].ToString())/100);
            }

            #endregion new


            #region doc
            //IList operators = OperatorScheduleManager.GetOperatorsWorkingNowByApplication(UserApplicationData.FirstLevel.Name, false);
            //foreach (int operatorCode in operators)
            //{
            //    double res = 0;
            //    double completeTime = 0;
            //    double tempRes = 0;
            //    double todayCompleteTime = 0;
            //    IList workShiftSchedules;

            //    //Calculate the amount of working time schedule in this day.
            //    completeTime = OperatorScheduleManager.GetOperatorTodaysWorkingTime(operatorCode);
            //    todayCompleteTime = OperatorScheduleManager.GetOperatorTodaysTotalTime(operatorCode);
            //    res = completeTime;

            //    //Substract all the time that the operator was in other statuses
            //    foreach (OperatorStatusData os in ServiceUtil.StatusesToCalculateOperatorComplianceSchedule())
            //    {
            //        workShiftSchedules = SmartCadDatabase.SearchObjects(
            //            SmartCadHqls.GetCustomHql(
            //                SmartCadHqls.GetSessionStatusHistorySpecificDateOperatorNotReadySpecificStatus,
            //                    ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today),
            //                    ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today.AddDays(1)),
            //                    operatorCode,
            //                    os.CustomCode,
            //                    UserApplicationData.FirstLevel.Code));
            //        tempRes = ServiceUtil.CalculateAmountSessionStatusHistory(workShiftSchedules);
            //        if (os.IsPause() == true)
            //        {
            //            if (tempRes <= ServiceUtil.CalculateBreakTimeOperatorStatusTime(todayCompleteTime, os))
            //            {
            //                completeTime -= tempRes;
            //            }
            //            else
            //            {
            //                completeTime -= ServiceUtil.CalculateBreakTimeOperatorStatusTime(todayCompleteTime, os);
            //            }

            //        }
            //        //res -= tempRes;
            //    }


            //    //Subtract the time the operator was not connected in the system in his workshift.                          
            //    res -= OperatorScheduleManager.GetOperatorTodaysNotWorkingTime(operatorCode);
            //    if (res < 0)
            //    {
            //        res = 0;
            //    }
            //    if (completeTime > 0)
            //        res = res / completeTime;
            //    if (res > 1)
            //        res = 1;
            //    ObjectData objectData = new ObjectData();
            //    objectData.Code = operatorCode;
            //    SaveIndicatorsValues(objectData, IndicatorClassData.Operator, res);
            //}
            #endregion doc
        }
    }
}
