//using System;
//using System.Collections.Generic;
//using System.Text;
//using SmartCadCore.Model;
//using System.Collections;

//namespace SmartCadCore.Core.Indicators
//{
//    public class PND16IndicatorValuesCalculator : IndicatorValuesCalculator
//    {
//        protected override string GetIndicator()
//        {
//            return "PND16";
//        }

//        protected override void Calculate()
//        {
//            CalculateSystemResult();
//        }
//        protected void CalculateSystemResult()
//        {
//            #region new
//            string sql = @" SELECT		COUNT(INC.CODE) AS CANTIDAD
//			,INCS.CODE AS ESTADO
//FROM		INCIDENT AS INC
//			,INCIDENT_STATUS AS INCS
//WHERE		(	CONVERT(DATETIME,CONVERT(CHAR(10),INC.END_DATE,101),101) = 
//				CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
//				OR
//				INC.END_DATE IS NULL
//			)
//AND			INC.INCIDENT_STATUS_CODE = INCS.CODE
//GROUP BY	INCS.CODE
//";

//            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);

//            double totalIncidentes = 0;
//            double closeIncidents = 0;
//            List<double> resultsValues = new List<double>();
//            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();

//            foreach (object[] item in data)
//            {
//                totalIncidentes += double.Parse(item[0].ToString());
//            }
//            foreach (object[] item in data)
//            {
//                if (double.Parse(item[1].ToString()) == IncidentStatusData.Closed.Code)
//                {
//                    closeIncidents = double.Parse(item[0].ToString());
//                    break;
//                }
//            }
//            resultsValues.Add(closeIncidents);
//            if (totalIncidentes == 0)
//            {
//                resultsValues.Add(0);
//            }
//            else
//            {
//                resultsValues.Add((closeIncidents * 100) / totalIncidentes);
//            }
//            result.Add("closeIncidents", resultsValues);
//            SaveIndicatorMultiplesValues(null, IndicatorClassData.System, result);

//            #endregion new

//            #region doc
//            //double closedIncidents = 0;
//            //double openIncidents = 0;
//            //double totalIncients = 0;            
//            //double percentClosedIncident = 0;
//            //Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();

//            //closedIncidents = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(
//            //  SmartCadHqls.GetClosedIncidentsCountByDate,
//            //  ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today),
//            //  ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today.AddDays(1)),
//            //  IncidentStatusData.Closed.Name));
          
            

            
//            //openIncidents = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(
//            //  SmartCadHqls.GetOpenIncidentsCount, IncidentStatusData.Open.Name));

//            //totalIncients = closedIncidents + openIncidents;
            
//            //if (totalIncients != 0)
//            //{
//            //    percentClosedIncident = (closedIncidents * 100) / totalIncients;
//            //}
//            //else
//            //{
//            //    percentClosedIncident = 0;
//            //}

//            //List<double> resultsValues = new List<double>();
//            //resultsValues.Add(closedIncidents);
//            //resultsValues.Add(percentClosedIncident);
//            //result.Add(ResourceLoader.GetString2("IndicatorPND16Name"), resultsValues);
//            //SaveIndicatorMultiplesValues(null, IndicatorClassData.System, result);
//            #endregion doc
//        }
       
//    }
//}
