//using System;
//using System.Collections.Generic;
//using System.Text;
//using SmartCadCore.Model;
//using System.Collections;

//namespace SmartCadCore.Core.Indicators
//{
//    public class PND22IndicatorValuesCalculator : IndicatorValuesCalculator
//    {
//        private enum EMERGENCYTYPE
//        {
//            FINISH,
//            NOFINISH,
//            NOEMERGENCY,
//            EMERGENCY
//        }
        
       
//        protected override string GetIndicator()
//        {
//            return "PND22";
//        }

//        protected override void Calculate()
//        {
//            CalculateSystemIndicators();
//            CalculateGroup();
//        }
//        private void CalculateSystemIndicators()
//        {            
//            #region new
//            string sql = @"SELECT		COUNT(REB.CODE) AS CANTIDAD
//			,CASE	PHR.INCOMPLETE
//					WHEN 0 THEN 'FINISH'
//					WHEN 1 THEN 'NOFINISH'
//			END AS ESTADO
//			,CASE	INC.IS_EMERGENCY
//					WHEN 0 THEN 'NOEMERGENCY'
//					WHEN 1 THEN 'EMERGENCY'
//			END AS EMERGENCIA
//FROM		PHONE_REPORT AS PHR
//			,REPORT_BASE AS REB
//			,INCIDENT AS INC
//WHERE		CONVERT(DATETIME,CONVERT(CHAR(10),PHR.PICKED_UP_CALL_TIME,101),101)= 
//			CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
//AND			PHR.PARENT_CODE = REB.CODE
//AND			REB.INCIDENT_CODE = INC.CODE
//GROUP BY	PHR.INCOMPLETE ,INC.IS_EMERGENCY";

//            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);


//            double totalIncidentes = 0;
//            double totalEmergencyIncidentes = 0;

//            List<double> resultsValues = new List<double>();
//            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();

//            foreach (object[] item in data)
//            {
//                totalIncidentes += double.Parse(item[0].ToString());
               
//                if (item[2].ToString() == EMERGENCYTYPE.EMERGENCY.ToString())
//                {
//                    totalEmergencyIncidentes += double.Parse(item[0].ToString());
//                }
//            }
//            resultsValues.Add(totalEmergencyIncidentes);
//            resultsValues.Add((totalEmergencyIncidentes*100) / totalIncidentes);
//            result.Add(ResourceLoader.GetString2("IndicatorPND22Name"), resultsValues);
//            SaveIndicatorMultiplesValues(null, IndicatorClassData.System, result);
//            result.Clear();
//            resultsValues.Clear();

//            foreach (object[] item in data)
//            {
//                resultsValues.Add(double.Parse(item[0].ToString()));

//                if (item[2].ToString() == EMERGENCYTYPE.EMERGENCY.ToString())
//                {
//                    if (totalEmergencyIncidentes > 0)
//                    {
//                        resultsValues.Add((double.Parse(item[0].ToString()) * 100) / totalEmergencyIncidentes);
//                    }
//                    else
//                    {
//                        resultsValues.Add(0);
//                    }
//                    if (item[1].ToString() == EMERGENCYTYPE.FINISH.ToString())
//                    {
//                        result.Add(ResourceLoader.GetString2("IndicatorL08Name"), resultsValues);
//                    }
//                    else
//                    {
//                        result.Add(ResourceLoader.GetString2("IndicatorL13Name"), resultsValues);
//                    }
//                }
//                else
//                {
//                    resultsValues.Add((double.Parse(item[0].ToString()) * 100) / totalIncidentes);
//                    result.Add(ResourceLoader.GetString2("IndicatorPND23Name"), resultsValues);
//                }
//                SaveIndicatorMultiplesValues(null, IndicatorClassData.System, result);
//                result.Clear();
//                resultsValues.Clear();                
//            }                      
//            #endregion new

//            #region doc
//            //double totalEmergencyCalls = 0;
//            //double percentValueEmergencyCalls = 0;

//            //IList phoneCallEmergency = SmartCadDatabase.SearchObjects(
//            //    SmartCadHqls.GetCustomHql(SmartCadHqls.GetPhoneReportsEmergencyCurrentday,
//            //    ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today),
//            //    ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today.AddDays(1))));

//            //IList phoneCallNoEmergency = SmartCadDatabase.SearchObjects(
//            //       SmartCadHqls.GetCustomHql(SmartCadHqls.GetPhoneReportsNoEmergencyCurrentday,
//            //       ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today),
//            //       ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today.AddDays(1))));


//            //if (phoneCallEmergency.Count != 0)
//            //{
//            //    totalEmergencyCalls = phoneCallEmergency.Count;
//            //    percentValueEmergencyCalls = (double)(phoneCallEmergency.Count * 100) /
//            //        (double)(phoneCallEmergency.Count + phoneCallNoEmergency.Count);
//            //}              
                
//            //    List<double> resultsValues = new List<double>();
//            //    resultsValues.Add(totalEmergencyCalls);
//            //    resultsValues.Add(percentValueEmergencyCalls);               
//            //    Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//            //    result.Add("Emergency", resultsValues);
//            //    SaveIndicatorMultiplesValues(null, IndicatorClassData.System, result);
//            #endregion doc

//        }


//        private void CalculateGroup()
//        {
//            #region new
//            string sql = @" SELECT		COUNT(REB.CODE) AS CANTIDAD
//			,CASE	PHR.INCOMPLETE
//					WHEN 0 THEN 'FINISH'
//					WHEN 1 THEN 'NOFINISH'
//			END AS ESTADO
//			,CASE	INC.IS_EMERGENCY
//						WHEN 0 THEN 'NOEMERGENCY'
//					WHEN 1 THEN 'EMERGENCY'
//			END AS EMERGENCIA
//			,OPE.CODE AS SUPERVISOR
//FROM		PHONE_REPORT AS PHR
//			,REPORT_BASE AS REB
//			,INCIDENT AS INC
//			,OPERATOR_ASSIGN AS OPA
//			,OPERATOR AS OPE
//WHERE		CONVERT(DATETIME,CONVERT(CHAR(10),PHR.PICKED_UP_CALL_TIME,101),101)= 
//			CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
//AND			PHR.PARENT_CODE = REB.CODE
//AND			REB.INCIDENT_CODE = INC.CODE
//AND			REB.OPERATOR_CODE = OPA.OPERATOR_CODE
//AND			OPA.DELETED_ID IS NULL
//AND			(
//				(				
//				PHR.PICKED_UP_CALL_TIME
//				BETWEEN OPA.START_DATE AND OPA.END_DATE
//				)		
//			)
//AND			OPA.SUPERVISOR_CODE = OPE.CODE
//GROUP BY	PHR.INCOMPLETE ,INC.IS_EMERGENCY, OPE.CODE
//order by OPE.CODE
//";

//            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);
//            double totalCalls = 0;
//            double emergencyCalls = 0;
//            double finishCall = 0;
//            double noFinishCall = 0;
//            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//            List<double> resultsValues = new List<double>();
//            int supervisorCode = 0;

//            foreach (object[] item in data)
//            {
//                if (supervisorCode != 0 && supervisorCode != int.Parse(item[3].ToString()))
//                {
//                    ObjectData od1 = new ObjectData();
//                    od1.Code = supervisorCode;                    

//                    resultsValues.Add(emergencyCalls);
//                    resultsValues.Add((emergencyCalls*100) / totalCalls);
//                    result.Add(ResourceLoader.GetString2("IndicatorPND22Name"), resultsValues);
//                    SaveIndicatorMultiplesValues(od1, IndicatorClassData.Group, result);
//                    resultsValues.Clear();
//                    result.Clear();

//                    resultsValues.Add(totalCalls - emergencyCalls);
//                    resultsValues.Add(((totalCalls - emergencyCalls) * 100) / totalCalls);
//                    result.Add(ResourceLoader.GetString2("IndicatorPND23Name"), resultsValues);
//                    SaveIndicatorMultiplesValues(od1, IndicatorClassData.Group, result);
//                    resultsValues.Clear();
//                    result.Clear();

//                    resultsValues.Add(finishCall);
//                    resultsValues.Add((finishCall * 100) / emergencyCalls);
//                    result.Add(ResourceLoader.GetString2("IndicatorL08Name"), resultsValues);
//                    SaveIndicatorMultiplesValues(od1, IndicatorClassData.Group, result);
//                    resultsValues.Clear();
//                    result.Clear();

//                    resultsValues.Add(noFinishCall);
//                    resultsValues.Add((noFinishCall * 100) / emergencyCalls);
//                    result.Add(ResourceLoader.GetString2("IndicatorL13Name"), resultsValues);
//                    SaveIndicatorMultiplesValues(od1, IndicatorClassData.Group, result);
//                    resultsValues.Clear();
//                    result.Clear();

//                    totalCalls = 0;
//                    emergencyCalls = 0;
//                    finishCall = 0;
//                    noFinishCall = 0;                    
//                }
//                if (item[2].ToString() == EMERGENCYTYPE.EMERGENCY.ToString())
//                {
//                    emergencyCalls += double.Parse(item[0].ToString());
//                    if (item[1].ToString() == EMERGENCYTYPE.FINISH.ToString())
//                    {
//                        finishCall += double.Parse(item[0].ToString());
//                    }
//                    else
//                    {
//                        noFinishCall += double.Parse(item[0].ToString());
//                    }
//                }
//                totalCalls += double.Parse(item[0].ToString());
//                supervisorCode = (int)item[3];


//                if (item == data[data.Count - 1])
//                {
//                    ObjectData od1 = new ObjectData();
//                    od1.Code = supervisorCode;

//                    resultsValues.Add(emergencyCalls);
//                    resultsValues.Add((emergencyCalls * 100) / totalCalls);
//                    result.Add(ResourceLoader.GetString2("IndicatorPND22Name"), resultsValues);
//                    SaveIndicatorMultiplesValues(od1, IndicatorClassData.Group, result);
//                    resultsValues.Clear();
//                    result.Clear();

//                    resultsValues.Add(totalCalls - emergencyCalls);
//                    resultsValues.Add(((totalCalls - emergencyCalls) * 100) / totalCalls);
//                    result.Add(ResourceLoader.GetString2("IndicatorPND23Name"), resultsValues);
//                    SaveIndicatorMultiplesValues(od1, IndicatorClassData.Group, result);
//                    resultsValues.Clear();
//                    result.Clear();

//                    resultsValues.Add(finishCall);
//                    resultsValues.Add((finishCall * 100) / emergencyCalls);
//                    result.Add(ResourceLoader.GetString2("IndicatorL08Name"), resultsValues);
//                    SaveIndicatorMultiplesValues(od1, IndicatorClassData.Group, result);
//                    resultsValues.Clear();
//                    result.Clear();

//                    resultsValues.Add(noFinishCall);
//                    resultsValues.Add((noFinishCall * 100) / emergencyCalls);
//                    result.Add(ResourceLoader.GetString2("IndicatorL13Name"), resultsValues);
//                    SaveIndicatorMultiplesValues(od1, IndicatorClassData.Group, result);
//                }
//            }



//            //foreach (object[] item in data)
//            //{
//            //    if (supervisorCode != 0 && supervisorCode != int.Parse(item[3].ToString()))
//            //    {
//            //        resultsValues.Add(emergencyCalls);
//            //        if (totalCalls == 0)
//            //        {
//            //            resultsValues.Add(0);
//            //        }
//            //        else
//            //        {
//            //            resultsValues.Add((emergencyCalls * 100) / totalCalls);
//            //        }

//            //        result.Add("emergencyCalls", resultsValues);

//            //        ObjectData od1 = new ObjectData();
//            //        od1.Code = supervisorCode;
//            //        SaveIndicatorMultiplesValues(od1, IndicatorClassData.Group, result);
//            //        result.Clear();
//            //        resultsValues = new List<double>();
//            //        totalCalls = 0;
//            //    }
//            //    if ((bool)item[1] == true)
//            //    {
//            //        emergencyCalls = double.Parse(item[0].ToString());
//            //    }
//            //    totalCalls += double.Parse(item[0].ToString());
//            //    supervisorCode = (int)item[3];

//            //    if (item == data[data.Count - 1])
//            //    {
//            //        resultsValues.Add(emergencyCalls);
//            //        if (totalCalls == 0)
//            //        {
//            //            resultsValues.Add(0);
//            //        }
//            //        else
//            //        {
//            //            resultsValues.Add((emergencyCalls * 100) / totalCalls);
//            //        }

//            //        result.Add("emergencyCalls", resultsValues);

//            //        ObjectData od1 = new ObjectData();
//            //        od1.Code = supervisorCode;
//            //        SaveIndicatorMultiplesValues(od1, IndicatorClassData.Group, result);
//            //        result.Clear();
//            //        resultsValues = new List<double>();
//            //        totalCalls = 0;
//            //    }
//            //}
//            #endregion new

//            #region doc
//            //IList supervisors = OperatorScheduleManager.GetSupervisorsWorkingNowByApplication("FirstLevelSupervisorName");

//            //foreach (int supervisor in supervisors)
//            //{
//            //    IList operators = OperatorScheduleManager.GetAllDayOperatorsBySupervisor(supervisor);
//            //    WorkShiftScheduleVariationData bshSupervisor =
//            //    OperatorScheduleManager.GetCurrentWorkShiftSchedulesByOperator(supervisor, SmartCadDatabase.GetTimeFromBD().Date);

//            //    double totalEmergencyCalls = 0;
//            //    double totalNoEmergencyCalls = 0;
//            //    double percentValueEmergencyCalls = 0;

               

//            //    foreach (OperatorData operatorData in operators)
//            //    {
//            //        //BaseSessionHistory bsh =
//            //        //OperatorScheduleManager.GetOperatorAssignBySupervisorAndByDayRealHours(
//            //        //supervisor,
//            //        //operatorData.Code);

//            //        IList bshList =
//            //          OperatorScheduleManager.GetAllDayOperatorAssignBySupervisorAndByDayRealHours(supervisor, operatorData.Code);
//            //        foreach (OperatorAssignData bsh in bshList)
//            //        {
//            //            if ((bshSupervisor != null) &&
//            //                (((bsh.StartDate.TimeOfDay >= bshSupervisor.Start.Value.TimeOfDay) &&
//            //               (bsh.StartDate.TimeOfDay <= bshSupervisor.End.Value.TimeOfDay)) ||
//            //               ((bsh.EndDate.TimeOfDay <= bshSupervisor.End.Value.TimeOfDay) &&
//            //               (bsh.EndDate.TimeOfDay >= bshSupervisor.Start.Value.TimeOfDay))))
//            //            {

//            //                IList phoneCallEmergency = SmartCadDatabase.SearchObjects(
//            //                        SmartCadHqls.GetCustomHql(
//            //                        SmartCadHqls.GetPhoneReportsEmergencyCurrentdayByOperator,
//            //                        operatorData.Code,
//            //                        ApplicationUtil.GetDataBaseFormattedDate(bsh.StartDate),
//            //                        ApplicationUtil.GetDataBaseFormattedDate(bsh.EndDate)));

//            //                IList phoneCallNoEmergency = SmartCadDatabase.SearchObjects(
//            //                       SmartCadHqls.GetCustomHql(
//            //                       SmartCadHqls.GetPhoneReportsNoEmergencyCurrentdayByOperator,
//            //                       operatorData.Code,
//            //                       ApplicationUtil.GetDataBaseFormattedDate(bsh.StartDate),
//            //                       ApplicationUtil.GetDataBaseFormattedDate(bsh.EndDate)));
//            //                totalEmergencyCalls += phoneCallEmergency.Count;
//            //                totalNoEmergencyCalls += phoneCallNoEmergency.Count;
//            //            }
//            //        }

//            //    }
//            //    if (totalEmergencyCalls + totalNoEmergencyCalls>0)
//            //    {
//            //        percentValueEmergencyCalls = (totalEmergencyCalls * 100) /
//            //            (totalEmergencyCalls + totalNoEmergencyCalls);
//            //    }
//            //    else
//            //    {
//            //        percentValueEmergencyCalls = 0;
//            //    }
//            //    List<double> resultsValues = new List<double>();
//            //    resultsValues.Add(totalEmergencyCalls);
//            //    resultsValues.Add(percentValueEmergencyCalls);
//            //    Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//            //    result.Add("Emergency", resultsValues);
//            //    ObjectData objectdata = new ObjectData();
//            //    objectdata.Code = supervisor;
//            //    SaveIndicatorMultiplesValues(objectdata , IndicatorClassData.Group, result);
//            //}
//            #endregion doc
//        }       
//    }
//}

