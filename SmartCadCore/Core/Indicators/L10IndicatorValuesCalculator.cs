using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using System.Collections;
using SmartCadCore.Statistic;

namespace SmartCadCore.Core.Indicators
{
    public class L10IndicatorValuesCalculator : IndicatorValuesCalculator
    {
        protected override string GetIndicator()
        {
            return "L10";
        }

        protected override void Calculate()
        {
            CalculateSystem();
            CalculateGroup();
            CalculateOperators();
        }

        private void CalculateSystem()
        {

            #region new
            string sql = @" SELECT	COUNT(PHR.PARENT_CODE) AS CANTIDAD
FROM	PHONE_REPORT AS PHR
WHERE	CONVERT(DATETIME,CONVERT(CHAR(10),PHR.PICKED_UP_CALL_TIME,101),101)= 
		CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)";


            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);
            SaveIndicatorsValues(IndicatorClassData.System, double.Parse(data[0].ToString()));

            #endregion new

            #region doc
            //try
            //{
            //    if (TelephonyServerStatisticElement.CurrentServerType == TelephonyServerTypes.Genesys)
            //    {
            //        double value = 0d;
            //        if (IndicatorValuesCalculator.GetStatisticRetriever() != null)
            //        {
            //            IndicatorValuesCalculator.GetStatisticRetriever().L10TotalAnsweredCalls();
            //            value = IndicatorValuesCalculator.GetStatisticRetriever().GetStatisticValue(StatisticRetriever.StatisticNames.L10TotalAnsweredCalls);                        
            //        }
            //        SaveIndicatorsValues(IndicatorClassData.System, value);
            //    }
            //    else if (TelephonyServerStatisticElement.CurrentServerType == TelephonyServerTypes.Nortel)
            //    {
            //        double res = 0;

            //        double totalCalls = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(
            //                SmartCadHqls.GetAmountAllCallsDate,
            //                ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today)));

            //        if (totalCalls > 0)
            //        {
            //            res = totalCalls;
            //        }

            //        SaveIndicatorsValues(IndicatorClassData.System, totalCalls);
            //    }
            //}
            //catch (Exception ex)
            //{
            //}
            #endregion doc

        }

        private void CalculateOperators()
        {
            #region new
            string sql = @" SELECT		COUNT(PHR.PARENT_CODE) AS CANTIDAD,OPE.CODE AS USUARIO
FROM		PHONE_REPORT AS PHR
			,REPORT_BASE AS REB
			,OPERATOR AS OPE
WHERE		CONVERT(DATETIME,CONVERT(CHAR(10),PHR.PICKED_UP_CALL_TIME,101),101)= 
			CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
AND			PHR.PARENT_CODE = REB.CODE
AND			REB.OPERATOR_CODE = OPE.CODE
GROUP BY	OPE.CODE
";

            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);

            foreach (object[] item in data)
            {
                ObjectData od1 = new ObjectData();
                od1.Code = int.Parse(item[1].ToString());
                SaveIndicatorsValues(od1, IndicatorClassData.Operator,
                    double.Parse(item[0].ToString()));
            }
            #endregion new

            #region doc
            //if (TelephonyServerStatisticElement.CurrentServerType == TelephonyServerTypes.Nortel ||
            //    TelephonyServerStatisticElement.CurrentServerType == TelephonyServerTypes.Genesys)
            //{
            //    IList operators = OperatorScheduleManager.GetOperatorsWorkingNowByApplication(UserApplicationData.FirstLevel.Name, false);
            //    foreach (int operatorCode in operators)
            //    {
            //        double res = 0;

            //        double totalCalls = (long)SmartCadDatabase.SearchBasicObject(
            //            SmartCadHqls.GetCustomHql(
            //            SmartCadHqls.GetAmountAllCallsDateByOperator,
            //            ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today),
            //            operatorCode));

            //        if (totalCalls > 0)
            //        {
            //            res = totalCalls;
            //        }

            //        ObjectData objectData = new ObjectData();
            //        objectData.Code = operatorCode;
            //        SaveIndicatorsValues(objectData, IndicatorClassData.Operator, res);
            //    }
            //}
            #endregion doc
        }

        private void CalculateGroup()
        {
            #region new
            string sql = @" SELECT		COUNT(PHR.PARENT_CODE) AS CANTIDAD,OPE.CODE AS USUARIO
FROM		PHONE_REPORT AS PHR
			,REPORT_BASE AS REB
			,OPERATOR AS OPE
			,OPERATOR_ASSIGN AS OPA
WHERE		CONVERT(DATETIME,CONVERT(CHAR(10),PHR.PICKED_UP_CALL_TIME,101),101)= 
			CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
AND			PHR.PARENT_CODE = REB.CODE
AND			REB.OPERATOR_CODE = OPA.OPERATOR_CODE
AND			OPA.DELETED_ID IS NULL
AND			(
				(				
				PHR.PICKED_UP_CALL_TIME
				BETWEEN OPA.START_DATE AND OPA.END_DATE
				)			
			)
AND			OPA.SUPERVISOR_CODE = OPE.CODE
GROUP BY	OPE.CODE
";

            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);
            foreach (object[] item in data)
            {
                ObjectData od1 = new ObjectData();
                od1.Code = int.Parse(item[1].ToString());
                SaveIndicatorsValues(od1, IndicatorClassData.Group,
                    double.Parse(item[0].ToString()));
            }
            #endregion new

            #region doc
            //IList supervisors = OperatorScheduleManager.GetSupervisorsWorkingNowByApplication("FirstLevelSupervisorName");
            //Dictionary<int, double> results = new Dictionary<int, double>();
            //foreach (int supervisorCode in supervisors)
            //{
            //    IList operators = OperatorScheduleManager.GetAllDayOperatorsBySupervisor(supervisorCode);
            //    WorkShiftScheduleVariationData bshSupervisor =
            //        OperatorScheduleManager.GetCurrentWorkShiftSchedulesByOperator(supervisorCode, SmartCadDatabase.GetTimeFromBD().Date);

            //    double total = 0;

            //    foreach (OperatorData operatorData in operators)
            //    {

            //        IList bshList =
            //           OperatorScheduleManager.GetAllDayOperatorAssignBySupervisorAndByDayRealHours(supervisorCode, operatorData.Code);

            //        foreach (OperatorAssignData bsh in bshList)
            //        {
            //            if ((bshSupervisor != null) &&
            //                (((bsh.StartDate.TimeOfDay >= bshSupervisor.Start.Value.TimeOfDay) &&
            //               (bsh.StartDate.TimeOfDay <= bshSupervisor.End.Value.TimeOfDay)) ||
            //               ((bsh.EndDate.TimeOfDay <= bshSupervisor.End.Value.TimeOfDay) &&
            //               (bsh.EndDate.TimeOfDay >= bshSupervisor.Start.Value.TimeOfDay))))
            //            {
            //                double totalCalls = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(
            //                                       SmartCadHqls.GetAmountAllCallsDateByOperatorByWorkShift,
            //                                       ApplicationUtil.GetDataBaseFormattedDate(bsh.StartDate),
            //                                       ApplicationUtil.GetDataBaseFormattedDate(bsh.EndDate),
            //                                      operatorData.Code));

            //                if (totalCalls > 0)
            //                {
            //                    total += totalCalls;
            //                }
            //            }
            //        }
            //    }

            //    ObjectData objectData = new ObjectData();
            //    objectData.Code = supervisorCode;
            //    SaveIndicatorsValues(objectData, IndicatorClassData.Group, total);
            //}
            #endregion doc
        }
    }
}
