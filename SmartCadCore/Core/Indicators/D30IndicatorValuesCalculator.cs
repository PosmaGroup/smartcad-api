using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using SmartCadCore.Model;

namespace SmartCadCore.Core.Indicators
{
    public class D30IndicatorValuesCalculator : IndicatorValuesCalculator
    {
        protected override string GetIndicator()
        {
            return "D30";
        }

        protected override void Calculate()
        {
            CalculateDepartment();
        }

        private void CalculateDepartment()
        {
            #region new
            string availablesSql = @" SELECT     COUNT(UNIT.CODE) AS outoforder, DEPARTMENT_ZONE.NAME AS ZONA
FROM         DEPARTMENT_STATION INNER JOIN
                      UNIT ON DEPARTMENT_STATION.CODE = UNIT.DEPARTMENT_STATION_CODE INNER JOIN
                      DEPARTMENT_ZONE ON DEPARTMENT_STATION.DEPARTMENT_ZONE_CODE = DEPARTMENT_ZONE.CODE
WHERE     (UNIT.UNIT_STATUS_CODE = 2)
GROUP BY DEPARTMENT_ZONE.NAME
";

            string totalSql = @" SELECT     COUNT(UNIT.CODE) AS TOTAL, DEPARTMENT_ZONE.NAME AS ZONA, DEPARTMENT_ZONE.DEPARTMENT_TYPE_CODE as code
FROM         DEPARTMENT_STATION INNER JOIN
                      UNIT ON DEPARTMENT_STATION.CODE = UNIT.DEPARTMENT_STATION_CODE INNER JOIN
                      DEPARTMENT_ZONE ON DEPARTMENT_STATION.DEPARTMENT_ZONE_CODE = DEPARTMENT_ZONE.CODE
GROUP BY DEPARTMENT_ZONE.NAME, DEPARTMENT_ZONE.DEPARTMENT_TYPE_CODE
";

            IList availableList = (IList)SmartCadDatabase.SearchBasicObjects(availablesSql, false);
            IList total = (IList)SmartCadDatabase.SearchBasicObjects(totalSql, false);

            //data[total,department_name, department_code, available]
            List<object[]> data = new List<object[]>();

            foreach (object[] itemTotal in total)
            {
                object[] currentItem = new object[4];
                currentItem[0] = itemTotal[0];
                currentItem[1] = itemTotal[1];
                currentItem[2] = itemTotal[2];

                foreach (object[] itemAvailable in availableList)
                {
                    if (itemAvailable[1].ToString() == currentItem[1].ToString())
                    {
                        currentItem[3] = itemAvailable[0];
                        data.Add(currentItem);
                        break;
                    }
                }
                if (currentItem[3] == null)
                {
                    currentItem[3] = 0;
                    data.Add(currentItem);
                }

            }

            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
            List<double> list = new List<double>();
            int departmentCode = 0;
            string estationName = "";
            foreach (object[] item in data)
            {
                if (departmentCode != 0 && departmentCode != int.Parse(item[2].ToString()))
                {

                    ObjectData od1 = new ObjectData();
                    od1.Code = departmentCode;
                    SaveIndicatorMultiplesValues(od1, IndicatorClassData.Department, result);
                    result.Clear();
                    estationName = "";
                    list = new List<double>();
                }


                estationName = item[1].ToString();
                list.Add(double.Parse(item[3].ToString()));
                list.Add(double.Parse(item[3].ToString()) / double.Parse(item[0].ToString()));
                result.Add(estationName, list);
                list = new List<double>();
                departmentCode = (int)item[2];

                if (item == data[data.Count - 1])
                {
                    ObjectData od1 = new ObjectData();
                    od1.Code = departmentCode;
                    SaveIndicatorMultiplesValues(od1, IndicatorClassData.Department, result);
                }
            }
            #endregion new

            #region doc
            //IList departmentCodes = (IList)SmartCadDatabase.SearchBasicObjects(
            //    SmartCadHqls.GetDepartmentsTypeCodes);
            //foreach (int departmentCode in departmentCodes)
            //{
            //    IList departmentStation = (IList)SmartCadDatabase.SearchBasicObjects(
            //        SmartCadHqls.GetCustomHql(
            //        SmartCadHqls.GetDepartmentZonesByDepartmentType,
            //        departmentCode));

            //    foreach (DepartmentZoneData dz in departmentStation)
            //    {
            //        double outoforder = (long)SmartCadDatabase.SearchBasicObject(
            //            SmartCadHqls.GetCustomHql(
            //            SmartCadHqls.GetAmountUnitDispatchGroupByDepartmentTypeDepartmentZone,
            //            UnitStatusData.OutOfOrder.Code,
            //            dz.Code));
            //        double totalUnits = (long)SmartCadDatabase.SearchBasicObject(
            //            SmartCadHqls.GetCustomHql(
            //            SmartCadHqls.GetAmountUnitGroupByDepartmentTypeDepartmentZone,
            //            dz.Code));
            //        Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
            //        if (outoforder > 0 && totalUnits > 0)
            //        {
            //            List<double> items = new List<double>();
            //            items.Add(outoforder);
            //            items.Add(outoforder / totalUnits);
            //            result.Add(dz.Name, items);
            //        }
            //        else
            //        {
            //            List<double> items = new List<double>();
            //            items.Add(0);
            //            items.Add(0);
            //            result.Add(dz.Name, items);
            //        }

            //        ObjectData od1 = new ObjectData();
            //        od1.Code = departmentCode;
            //        SaveIndicatorMultiplesValues(od1, IndicatorClassData.Department, result);
            //    }
            //}
            #endregion doc
        }
    }
}
