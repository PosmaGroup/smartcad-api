using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using SmartCadCore.Model;

namespace SmartCadCore.Core.Indicators
{
    public class D04IndicatorValuesCalculator : IndicatorValuesCalculator
    {
        protected override string GetIndicator()
        {
            return "D04";
        }

        protected override void Calculate()
        {
            CalculateSystem();
            //CalculateOperators();
            CalculateGroup();
            CalculateDepartment();
        }

        #region doc
//        private void CalculateOperators()
//        {
//            IList operators = OperatorScheduleManager.GetOperatorsWorkingNowByApplication(UserApplicationData.Dispatch.Name, false);
//            foreach (int od in operators)
//            {
//                Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();

//                string hql = @"select distinct data.Code, sit.FriendlyName 
//                           from IncidentData data inner join
//                                data.SetReportBaseList srl inner join
//                                srl.SetIncidentTypes sit inner join
//                                srl.IncidentNotifications inot inner join
//                                inot.StatusHistoryList shl
//                           where (shl.End > '{0}' or shl.End is null) and
//                                 shl.Status.Code = {1} and
//                                 shl.UserAccount.Code = {2}
//                           order by data.Code, sit.FriendlyName";

//                IList incidentInfo = (IList)
//                SmartCadDatabase.SearchBasicObjects(
//                SmartCadHqls.GetCustomHql(hql,
//                ApplicationUtil.GetDataBaseFormattedDate(SmartCadDatabase.GetTimeFromBD().Date),
//                IncidentNotificationStatusData.Pending.Code,
//                od));

//                Dictionary<string, List<int>> incidentTypeNamesByIncident = new Dictionary<string, List<int>>();
//                string itnames = "";
//                int code = 0;
//                foreach (object[] item in incidentInfo)
//                {
//                    if (code != 0 && code != int.Parse(item[0].ToString()))
//                    {
//                        if (incidentTypeNamesByIncident.ContainsKey(itnames) == false)
//                        {
//                            List<int> codes = new List<int>();
//                            codes.Add(code);
//                            incidentTypeNamesByIncident.Add(itnames, codes);
//                        }
//                        else
//                        {
//                            List<int> codes = incidentTypeNamesByIncident[itnames];
//                            codes.Add(code);
//                        }
//                        itnames = string.Empty;
//                    }

//                    code = int.Parse(item[0].ToString());
//                    string friendlyName = item[1].ToString();
//                    if (itnames.Contains(friendlyName) == false)
//                    {
//                        if (itnames.Length == 0)
//                        {
//                            itnames = friendlyName;
//                        }
//                        else
//                        {
//                            itnames = string.Concat(itnames, ", ", friendlyName);
//                        }
//                    }
//                }

//                if (itnames != string.Empty)
//                {
//                    if (incidentTypeNamesByIncident.ContainsKey(itnames) == false)
//                    {
//                        List<int> codes = new List<int>();
//                        codes.Add(code);
//                        incidentTypeNamesByIncident.Add(itnames, codes);
//                    }
//                    else
//                    {
//                        List<int> codes = incidentTypeNamesByIncident[itnames];
//                        codes.Add(code);
//                    }
//                }

//                foreach (KeyValuePair<string, List<int>> kvp in incidentTypeNamesByIncident)
//                {
//                    StringBuilder sb = new StringBuilder();
//                    sb.Append("(");
//                    foreach (int codeValue in kvp.Value)
//                    {
//                        if (sb.Length == 1)
//                        {
//                            sb.Append(codeValue);
//                        }
//                        else
//                        {
//                            sb.Append(", ");
//                            sb.Append(codeValue);
//                        }
//                    }
//                    sb.Append(")");

//                    string hql2 = @"select shl
//                                from IncidentNotificationStatusHistoryData shl
//                                where (shl.End > '{0}' or shl.End is null) and
//                                     shl.Status.Code = {1} and
//                                     shl.IncidentNotification.ReportBase.Incident.Code in {2} and
//                                     shl.UserAccount.Code = {3}";

//                    incidentInfo = (IList)
//                        SmartCadDatabase.SearchObjects(
//                        SmartCadHqls.GetCustomHql(hql2,
//                        ApplicationUtil.GetDataBaseFormattedDate(SmartCadDatabase.GetTimeFromBD().Date),
//                        IncidentNotificationStatusData.Pending.Code,
//                        sb.ToString(),
//                        od));

//                    double average = 0;

//                    foreach (IncidentNotificationStatusHistoryData inshd in incidentInfo)
//                    {
//                        TimeSpan sub = TimeSpan.Zero;
//                        if (inshd.End.HasValue == true)
//                            sub = inshd.End.Value - inshd.Start.Value;
//                        else
//                            sub = DateTime.Now - inshd.Start.Value;

//                        average += sub.TotalSeconds;
//                    }

//                    List<double> list = new List<double>();
//                    if (average > 0)
//                    {
//                        list.Add(Convert.ToDouble(average / incidentInfo.Count));
//                        result.Add(kvp.Key, list);

//                        ObjectData objectData = new ObjectData();
//                        objectData.Code = od;
//                        SaveIndicatorMultiplesValues(objectData, IndicatorClassData.Operator, result);
//                        result.Clear();
//                    }
//                }
//            }
//        }
        #endregion doc


        private void CalculateSystem()
        {

            #region new
            string sql = @" SELECT		SUM(DATEDIFF(SS	,INSH.START_DATE
							,ISNULL(INSH.END_DATE,GETDATE())))
							/COUNT(ISNULL(INSH.END_DATE,1)) AS TIEMPO
			,AGRUPADO.TIPO AS TIPO
FROM		REPORT_BASE AS REB
			,INCIDENT_NOTIFICATION AS INOT
			,INCIDENT_NOTIFICATION_STATUS_HISTORY AS INSH
			,(
				SELECT
				INC_1.CODE AS CODIGO,
				TIPO = RTRIM(REPLACE(REPLACE(o.list,'<FRIENDLY_NAME>',''),'</FRIENDLY_NAME>',' , '))
				FROM
				INCIDENT AS INC_1
				CROSS APPLY
				(
				SELECT		INTY.FRIENDLY_NAME
				FROM		INCIDENT AS INC
							,REPORT_BASE AS REB
							,REPORT_BASE_INCIDENT_TYPE AS RBIT
							,INCIDENT_TYPE AS INTY
				WHERE		INC.CODE = REB.INCIDENT_CODE
				AND			REB.CODE = RBIT.REPORT_BASE_CODE
				AND			RBIT.INCIDENT_TYPE_CODE = INTY.CODE
				AND			INC.CODE = INC_1.CODE
				ORDER BY INTY.FRIENDLY_NAME
				FOR XML PATH('')
				) o (list)
			) AS AGRUPADO
WHERE		AGRUPADO.CODIGO = REB.INCIDENT_CODE
AND			REB.CODE = INOT.REPORT_BASE_CODE
AND			INOT.CODE = INSH.INCIDENT_NOTIFICATION_CODE
AND			(	CONVERT(DATETIME,CONVERT(CHAR(10),INSH.END_DATE,101),101) = 
				CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
				OR			
				INSH.END_DATE IS NULL
			)
AND			INSH.INCIDENT_NOTIFICATION_STATUS_CODE = 7
GROUP BY	AGRUPADO.TIPO";




            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);

            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
            List<double> list = new List<double>();
            string incidentTypes = "";
            foreach (object[] item in data)
            {
                incidentTypes = item[1].ToString().Substring(0, item[1].ToString().Length - 1);
                list.Add(double.Parse((item[0].ToString())));
                result.Add(incidentTypes, list);
                SaveIndicatorMultiplesValues(null, IndicatorClassData.System, result);
                result.Clear();
                incidentTypes = "";
                list = new List<double>();
            }
            #endregion new

            #region doc 
//            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();

//            string hql = @"select distinct data.Code, sit.FriendlyName 
//                           from IncidentData data inner join
//                                data.SetReportBaseList srl inner join
//                                srl.SetIncidentTypes sit inner join
//                                srl.IncidentNotifications inot inner join
//                                inot.StatusHistoryList shl
//                           where (shl.End > '{0}' or shl.End is null) and
//                                 shl.Status.Code = {1}
//                           order by data.Code, sit.FriendlyName";

//            IList incidentInfo = (IList)
//                SmartCadDatabase.SearchBasicObjects(
//                SmartCadHqls.GetCustomHql(hql,
//                ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today),
//                IncidentNotificationStatusData.Pending.Code));

//            Dictionary<string, List<int>> incidentTypeNamesByIncident = new Dictionary<string, List<int>>();
//            string itnames = "";
//            int code = 0;
//            foreach (object[] item in incidentInfo)
//            {
//                if (code != 0 && code != int.Parse(item[0].ToString()))
//                {
//                    if (incidentTypeNamesByIncident.ContainsKey(itnames) == false)
//                    {
//                        List<int> codes = new List<int>();
//                        codes.Add(code);
//                        incidentTypeNamesByIncident.Add(itnames, codes);
//                    }
//                    else
//                    {
//                        List<int> codes = incidentTypeNamesByIncident[itnames];
//                        codes.Add(code);
//                    }
//                    itnames = string.Empty;
//                }

//                code = int.Parse(item[0].ToString());
//                string friendlyName = item[1].ToString();
//                if (itnames.Contains(friendlyName) == false)
//                {
//                    if (itnames.Length == 0)
//                    {
//                        itnames = friendlyName;
//                    }
//                    else
//                    {
//                        itnames = string.Concat(itnames, ", ", friendlyName);
//                    }
//                }
//            }

//            if (itnames != string.Empty)
//            {
//                if (incidentTypeNamesByIncident.ContainsKey(itnames) == false)
//                {
//                    List<int> codes = new List<int>();
//                    codes.Add(code);
//                    incidentTypeNamesByIncident.Add(itnames, codes);
//                }
//                else
//                {
//                    List<int> codes = incidentTypeNamesByIncident[itnames];
//                    codes.Add(code);
//                }
//            }

//            foreach (KeyValuePair<string, List<int>> kvp in incidentTypeNamesByIncident)
//            {
//                StringBuilder sb = new StringBuilder();
//                sb.Append("(");
//                foreach (int codeValue in kvp.Value)
//                {
//                    if (sb.Length == 1)
//                    {
//                        sb.Append(codeValue);
//                    }
//                    else
//                    {
//                        sb.Append(", ");
//                        sb.Append(codeValue);
//                    }
//                }
//                sb.Append(")");

//                string hql2 = @"select shl
//                                from IncidentNotificationStatusHistoryData shl
//                                where (shl.End > '{0}' or shl.End is null) and
//                                     shl.Status.Code = {1} and
//                                     shl.IncidentNotification.ReportBase.Incident.Code in {2}";

//                incidentInfo = (IList)
//                    SmartCadDatabase.SearchObjects(
//                    SmartCadHqls.GetCustomHql(hql2,
//                    ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today),
//                    IncidentNotificationStatusData.Pending.Code,
//                    sb.ToString()));

//                double average = 0;

//                foreach (IncidentNotificationStatusHistoryData inshd in incidentInfo)
//                {
//                    TimeSpan sub = TimeSpan.Zero;
//                    if (inshd.End.HasValue == true)
//                        sub = inshd.End.Value - inshd.Start.Value;
//                    else
//                        sub = DateTime.Now - inshd.Start.Value;

//                    average += sub.TotalSeconds;
//                }


//                List<double> list = new List<double>();
//                if (average > 0)
//                {
//                    list.Add(Convert.ToDouble(average / incidentInfo.Count));
//                    result.Add(kvp.Key, list);

//                    SaveIndicatorMultiplesValues(null, IndicatorClassData.System, result);
//                    result.Clear();
//                }
            //            }

            #endregion doc
        }


        private void CalculateGroup()
        {
            #region new
            string sql = @" SELECT		SUM(DATEDIFF(SS	,INSH.START_DATE
							,ISNULL(INSH.END_DATE,GETDATE())))
							/COUNT(ISNULL(INSH.END_DATE,1)) AS TIEMPO
			,AGRUPADO.TIPO AS TIPO
			,OPE.CODE AS SUPERVISOR
FROM		REPORT_BASE AS REB
			,INCIDENT_NOTIFICATION AS INOT
			,INCIDENT_NOTIFICATION_STATUS_HISTORY AS INSH
			,OPERATOR_ASSIGN AS OPA
			,OPERATOR AS OPE
			,(
				SELECT
				INC_1.CODE AS CODIGO,
				TIPO = RTRIM(REPLACE(REPLACE(o.list,'<FRIENDLY_NAME>',''),'</FRIENDLY_NAME>',' , '))
				FROM
				INCIDENT AS INC_1
				CROSS APPLY
				(
				SELECT		INTY.FRIENDLY_NAME
				FROM		INCIDENT AS INC
							,REPORT_BASE AS REB
							,REPORT_BASE_INCIDENT_TYPE AS RBIT
							,INCIDENT_TYPE AS INTY
				WHERE		INC.CODE = REB.INCIDENT_CODE
				AND			REB.CODE = RBIT.REPORT_BASE_CODE
				AND			RBIT.INCIDENT_TYPE_CODE = INTY.CODE
				AND			INC.CODE = INC_1.CODE
				ORDER BY INTY.FRIENDLY_NAME
				FOR XML PATH('')
				) o (list)
			) AS AGRUPADO
WHERE		AGRUPADO.CODIGO = REB.INCIDENT_CODE
AND			REB.CODE = INOT.REPORT_BASE_CODE
AND			INOT.CODE = INSH.INCIDENT_NOTIFICATION_CODE
AND			(	CONVERT(DATETIME,CONVERT(CHAR(10),INSH.END_DATE,101),101) = 
				CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
				OR			
				INSH.END_DATE IS NULL
			)
AND			INSH.INCIDENT_NOTIFICATION_STATUS_CODE = 7
AND			OPA.OPERATOR_CODE = INSH.USER_ACCOUNT_CODE
AND			OPA.DELETED_ID IS NULL
AND			OPE.CODE = OPA.SUPERVISOR_CODE
GROUP BY	AGRUPADO.TIPO,OPE.CODE
order by OPE.CODE
";




            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);

            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
            List<double> list = new List<double>();
            int supervisorCode = 0;
            string incidentTypes = "";
            foreach (object[] item in data)
            {
                if (supervisorCode != 0 && supervisorCode != int.Parse(item[2].ToString()))
                {

                    ObjectData od1 = new ObjectData();
                    od1.Code = supervisorCode;
                    SaveIndicatorMultiplesValues(od1, IndicatorClassData.Group, result);
                    result.Clear();
                    incidentTypes = "";
                    list = new List<double>();
                }

                incidentTypes = item[1].ToString().Substring(0, item[1].ToString().Length - 1);
                list.Add(double.Parse(item[0].ToString()));
                result.Add(incidentTypes, list);
                list = new List<double>();
                supervisorCode = (int)item[2];

                if (item == data[data.Count - 1])
                {
                    ObjectData od1 = new ObjectData();
                    od1.Code = supervisorCode;
                    SaveIndicatorMultiplesValues(od1, IndicatorClassData.Group, result);
                }
            }
            #endregion new

            #region doc
//            IList supervisors = OperatorScheduleManager.GetSupervisorsWorkingNowByApplication("DispatchSupervisorName");

//            foreach (int supervisor in supervisors)
//            {
//                Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//                //double totalElementsResult = 0;
//                IList operators = OperatorScheduleManager.GetAllDayOperatorsBySupervisor(supervisor);
//                WorkShiftScheduleVariationData bshSupervisor =
//                     OperatorScheduleManager.GetCurrentWorkShiftSchedulesByOperator(supervisor, SmartCadDatabase.GetTimeFromBD().Date);
//                ArrayList incidentInfoGroup = new ArrayList();
//                foreach (OperatorData operatorData in operators)
//                {
//                    //BaseSessionHistory bsh = OperatorScheduleManager.GetOperatorAssignBySupervisorAndByDayRealHours(
//                    //    supervisor, operatorData.Code);

//                     IList bshList =
//                      OperatorScheduleManager.GetAllDayOperatorAssignBySupervisorAndByDayRealHours(supervisor, operatorData.Code);

//                     foreach (OperatorAssignData bsh in bshList)
//                     {
//                         if ((bshSupervisor != null) &&
//                             (((bsh.StartDate.TimeOfDay >= bshSupervisor.Start.Value.TimeOfDay) &&
//                            (bsh.StartDate.TimeOfDay <= bshSupervisor.End.Value.TimeOfDay)) ||
//                            ((bsh.EndDate.TimeOfDay <= bshSupervisor.End.Value.TimeOfDay) &&
//                            (bsh.EndDate.TimeOfDay >= bshSupervisor.Start.Value.TimeOfDay))))
//                         {
//                             string hql = @"select distinct data.Code, sit.FriendlyName 
//                           from IncidentData data inner join
//                                data.SetReportBaseList srl inner join
//                                srl.SetIncidentTypes sit inner join
//                                srl.IncidentNotifications inot inner join
//                                inot.StatusHistoryList shl
//                           where ((shl.End > '{0}' and
//                                 shl.End <= '{1}') or shl.End is null) and
//                                 shl.Status.Code = {2} and
//                                 shl.UserAccount.Code = {3}
//                           order by data.Code, sit.FriendlyName";



//                             IList incidentInfo = (IList)
//                                 SmartCadDatabase.SearchBasicObjects(
//                                 SmartCadHqls.GetCustomHql(hql,
//                                 ApplicationUtil.GetDataBaseFormattedDate(bsh.StartDate),
//                                 ApplicationUtil.GetDataBaseFormattedDate(bsh.EndDate),
//                                 IncidentNotificationStatusData.Pending.Code,
//                                 operatorData.Code));
//                             incidentInfoGroup.AddRange(incidentInfo);
//                         }
//                     }
//                }
//                Dictionary<string, List<int>> incidentTypeNamesByIncident = new Dictionary<string, List<int>>();
//                string itnames = "";
//                int code = 0;
//                foreach (object[] item in incidentInfoGroup)
//                {
//                    if (code != 0 && code != int.Parse(item[0].ToString()))
//                    {
//                        if (incidentTypeNamesByIncident.ContainsKey(itnames) == false)
//                        {
//                            List<int> codes = new List<int>();
//                            codes.Add(code);
//                            incidentTypeNamesByIncident.Add(itnames, codes);
//                        }
//                        else
//                        {
//                            List<int> codes = incidentTypeNamesByIncident[itnames];
//                            codes.Add(code);
//                        }
//                        itnames = string.Empty;
//                    }

//                    code = int.Parse(item[0].ToString());
//                    string friendlyName = item[1].ToString();
//                    if (itnames.Contains(friendlyName) == false)
//                    {
//                        if (itnames.Length == 0)
//                        {
//                            itnames = friendlyName;
//                        }
//                        else
//                        {
//                            itnames = string.Concat(itnames, ", ", friendlyName);
//                        }
//                    }
//                }

//                if (incidentTypeNamesByIncident.ContainsKey(itnames) == false)
//                {
//                    List<int> codes = new List<int>();
//                    codes.Add(code);
//                    incidentTypeNamesByIncident.Add(itnames, codes);
//                }
//                else
//                {
//                    List<int> codes = incidentTypeNamesByIncident[itnames];
//                    codes.Add(code);
//                }

//                SaveGroup(supervisor, incidentTypeNamesByIncident);
//                incidentTypeNamesByIncident.Clear();

            //}
            #endregion doc
        }


            #region doc
//        private void SaveGroup(int supervisorCode, Dictionary<string, List<int>> incidentTypeNamesByIncident)
//        {
//            foreach (KeyValuePair<string, List<int>> kvp in incidentTypeNamesByIncident)
//            {
//                StringBuilder sb = new StringBuilder();
//                sb.Append("(");
//                foreach (int codeValue in kvp.Value)
//                {
//                    if (sb.Length == 1)
//                    {
//                        sb.Append(codeValue);
//                    }
//                    else
//                    {
//                        sb.Append(", ");
//                        sb.Append(codeValue);
//                    }
//                }
//                sb.Append(")");

//                string hql2 = @"select shl
//                                from IncidentNotificationStatusHistoryData shl
//                                where (shl.End > '{0}' or shl.End is null) and
//                                     shl.Status.Code = {1} and
//                                     shl.IncidentNotification.ReportBase.Incident.Code in {2}";

//                IList incidentInfo = (IList)
//                    SmartCadDatabase.SearchObjects(
//                    SmartCadHqls.GetCustomHql(hql2,
//                    ApplicationUtil.GetDataBaseFormattedDate(SmartCadDatabase.GetTimeFromBD().Date),
//                    IncidentNotificationStatusData.Pending.Code,
//                    sb.ToString()));

//                double average = 0;

//                foreach (IncidentNotificationStatusHistoryData inshd in incidentInfo)
//                {
//                    TimeSpan sub = TimeSpan.Zero;
//                    if (inshd.End.HasValue == true)
//                        sub = inshd.End.Value - inshd.Start.Value;
//                    else
//                        sub = DateTime.Now - inshd.Start.Value;

//                    average += sub.TotalSeconds;
//                }

//                List<double> list = new List<double>();
//                if (average > 0)
//                {
//                    Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//                    list.Add(Convert.ToDouble(average / incidentInfo.Count));
//                    result.Add(kvp.Key, list);

//                    ObjectData objectData = new ObjectData();
//                    objectData.Code = supervisorCode;
//                    SaveIndicatorMultiplesValues(objectData, IndicatorClassData.Group, result);
//                    result.Clear();
//                }
//            }
//        }
        #endregion doc


        private void CalculateDepartment()
        {
            #region new
            string sql = @" SELECT		DETY.CODE AS ORGANISMO, SUM(DATEDIFF(SS	,INSH.START_DATE
							,ISNULL(INSH.END_DATE,GETDATE())))
							/COUNT(ISNULL(INSH.END_DATE,1)) AS TIEMPO
			,AGRUPADO.TIPO AS TIPO
			
FROM		REPORT_BASE AS REB
			,INCIDENT_NOTIFICATION AS INOT
			,INCIDENT_NOTIFICATION_STATUS_HISTORY AS INSH
			,DEPARTMENT_TYPE AS DETY
			,(
				SELECT
				INC_1.CODE AS CODIGO,
				TIPO = RTRIM(REPLACE(REPLACE(o.list,'<FRIENDLY_NAME>',''),'</FRIENDLY_NAME>',' , '))
				FROM
				INCIDENT AS INC_1
				CROSS APPLY
				(
				SELECT		INTY.FRIENDLY_NAME
				FROM		INCIDENT AS INC
							,REPORT_BASE AS REB
							,REPORT_BASE_INCIDENT_TYPE AS RBIT
							,INCIDENT_TYPE AS INTY
				WHERE		INC.CODE = REB.INCIDENT_CODE
				AND			REB.CODE = RBIT.REPORT_BASE_CODE
				AND			RBIT.INCIDENT_TYPE_CODE = INTY.CODE
				AND			INC.CODE = INC_1.CODE
				ORDER BY INTY.FRIENDLY_NAME
				FOR XML PATH('')
				) o (list)
			) AS AGRUPADO
WHERE		AGRUPADO.CODIGO = REB.INCIDENT_CODE
AND			REB.CODE = INOT.REPORT_BASE_CODE
AND			INOT.DEPARTMENT_TYPE_CODE = DETY.CODE
AND			INOT.CODE = INSH.INCIDENT_NOTIFICATION_CODE
AND			(	CONVERT(DATETIME,CONVERT(CHAR(10),INSH.END_DATE,101),101) = 
				CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
				OR			
				INSH.END_DATE IS NULL
			)
AND			INSH.INCIDENT_NOTIFICATION_STATUS_CODE = 7
GROUP BY	AGRUPADO.TIPO,DETY.CODE
order by DETY.CODE
";




            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);

            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
            List<double> list = new List<double>();
            int departmentCode = 0;
            string incidentTypes = "";
            foreach (object[] item in data)
            {
                if (departmentCode != 0 && departmentCode != int.Parse(item[0].ToString()))
                {

                    ObjectData od1 = new ObjectData();
                    od1.Code = departmentCode;
                    SaveIndicatorMultiplesValues(od1, IndicatorClassData.Department, result);
                    result.Clear();
                    incidentTypes = "";
                    list = new List<double>();
                }

                incidentTypes = item[2].ToString().Substring(0, item[2].ToString().Length - 1);
                list.Add(double.Parse(item[1].ToString()));
                result.Add(incidentTypes, list);
                list = new List<double>();
                departmentCode = (int)item[0];

                if (item == data[data.Count - 1])
                {
                    ObjectData od1 = new ObjectData();
                    od1.Code = departmentCode;
                    SaveIndicatorMultiplesValues(od1, IndicatorClassData.Department, result);
                }
            }
            #endregion new

            #region doc

//            IList departmentCodes = (IList)SmartCadDatabase.SearchBasicObjects(
//                SmartCadHqls.GetDepartmentsTypeCodes);
//            foreach (int departmentCode in departmentCodes)
//            {
//                Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();


               

//                string hql = @"select distinct data.Code, sit.FriendlyName 
//                           from IncidentData data inner join
//                                data.SetReportBaseList srl inner join
//                                srl.SetIncidentTypes sit inner join
//                                srl.IncidentNotifications inot inner join
//                                inot.StatusHistoryList shl
//                           where (shl.End > '{0}'or shl.End is null) and
//                                 shl.Status.Code = {1} and
//                                inot.DepartmentType.Code = {2}
//                           order by data.Code, sit.FriendlyName";


//                IList incidentInfo = (IList)
//                    SmartCadDatabase.SearchBasicObjects(
//                    SmartCadHqls.GetCustomHql(hql,
//                    ApplicationUtil.GetDataBaseFormattedDate(SmartCadDatabase.GetTimeFromBD().Date),
//                    IncidentNotificationStatusData.Pending.Code,
//                    departmentCode));

//                Dictionary<string, List<int>> incidentTypeNamesByIncident = new Dictionary<string, List<int>>();
//                string itnames = "";
//                int code = 0;
//                foreach (object[] item in incidentInfo)
//                {
//                    if (code != 0 && code != int.Parse(item[0].ToString()))
//                    {
//                        if (incidentTypeNamesByIncident.ContainsKey(itnames) == false)
//                        {
//                            List<int> codes = new List<int>();
//                            codes.Add(code);
//                            incidentTypeNamesByIncident.Add(itnames, codes);
//                        }
//                        else
//                        {
//                            List<int> codes = incidentTypeNamesByIncident[itnames];
//                            codes.Add(code);
//                        }
//                        itnames = string.Empty;
//                    }

//                    code = int.Parse(item[0].ToString());
//                    string friendlyName = item[1].ToString();
//                    if (itnames.Contains(friendlyName) == false)
//                    {
//                        if (itnames.Length == 0)
//                        {
//                            itnames = friendlyName;
//                        }
//                        else
//                        {
//                            itnames = string.Concat(itnames, ", ", friendlyName);
//                        }
//                    }
//                }

//                if (incidentTypeNamesByIncident.ContainsKey(itnames) == false)
//                {
//                    List<int> codes = new List<int>();
//                    codes.Add(code);
//                    incidentTypeNamesByIncident.Add(itnames, codes);
//                }
//                else
//                {
//                    List<int> codes = incidentTypeNamesByIncident[itnames];
//                    codes.Add(code);
//                }

//                SaveDepartment(departmentCode, incidentTypeNamesByIncident);
//                incidentTypeNamesByIncident.Clear();
            //            }
            #endregion doc
        }

        #region doc
//        private void SaveDepartment(int departmentCode, Dictionary<string, List<int>> incidentTypeNamesByIncident)
//        {
//            foreach (KeyValuePair<string, List<int>> kvp in incidentTypeNamesByIncident)
//            {
//                StringBuilder sb = new StringBuilder();
//                sb.Append("(");
//                foreach (int codeValue in kvp.Value)
//                {
//                    if (sb.Length == 1)
//                    {
//                        sb.Append(codeValue);
//                    }
//                    else
//                    {
//                        sb.Append(", ");
//                        sb.Append(codeValue);
//                    }
//                }
//                sb.Append(")");

//                string hql2 = @"select shl
//                                from IncidentNotificationStatusHistoryData shl
//                                where (shl.End > '{0}' or shl.End is null) and
//                                     shl.Status.Code = {1} and
//                                     shl.IncidentNotification.ReportBase.Incident.Code in {2} and
//                                     shl.IncidentNotification.DepartmentType.Code = {3}";

//                IList incidentInfo = (IList)
//                    SmartCadDatabase.SearchObjects(
//                    SmartCadHqls.GetCustomHql(hql2,
//                    ApplicationUtil.GetDataBaseFormattedDate(SmartCadDatabase.GetTimeFromBD().Date),
//                    IncidentNotificationStatusData.Pending.Code,
//                    sb.ToString(), departmentCode));

//                double average = 0;

//                foreach (IncidentNotificationStatusHistoryData inshd in incidentInfo)
//                {
//                    TimeSpan sub = TimeSpan.Zero;
//                    if (inshd.End.HasValue == true)
//                        sub = inshd.End.Value - inshd.Start.Value;
//                    else
//                        sub = DateTime.Now - inshd.Start.Value;

//                    average += sub.TotalSeconds;
//                }

//                List<double> list = new List<double>();
//                if (average > 0)
//                {
//                    Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//                    list.Add(Convert.ToDouble(average / incidentInfo.Count));
//                    result.Add(kvp.Key, list);

//                    ObjectData objectData = new ObjectData();
//                    objectData.Code = departmentCode;
//                    SaveIndicatorMultiplesValues(objectData, IndicatorClassData.Department, result);
//                    result.Clear();
//                }
//            }
        //        }
        #endregion doc
    }
}
