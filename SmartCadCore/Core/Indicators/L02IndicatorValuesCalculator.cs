﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SmartCadCore.Model;
using SmartCadCore.Statistic;
using SmartCadCore.Core.Util;

namespace SmartCadCore.Core.Indicators
{
    class L02IndicatorValuesCalculator : IndicatorValuesCalculator
    {
        protected override string GetIndicator()
        {
            return "L02";
        }

        protected override void Calculate()
        {
            CalculateSystem();
        }

        private void CalculateSystem()
        {
            try
            {
                if (TelephonyServerStatisticElement.CurrentServerType == TelephonyServerTypes.Genesys)
                {
                    double value = GenesysStatWrapper.GetData(StatisticRetriever.StatisticNames.L02TotalCallsAbandoned);
                    SaveIndicatorsValues(IndicatorClassData.System, value);
                }
                else if (TelephonyServerStatisticElement.CurrentServerType == TelephonyServerTypes.Nortel)
                {
                    SaveIndicatorsValues(IndicatorClassData.System, NortelRTDWrapper.GetData((int)NRTD_Values.NIrtd_APPL_CALLS_ABAN));
                }
                else if (TelephonyServerStatisticElement.CurrentServerType == TelephonyServerTypes.Avaya)
                {
                    
                    SaveIndicatorsValues(IndicatorClassData.System,
                        AvayaStatWrapper.GetData(StatisticRetriever.StatisticNames.L02TotalCallsAbandoned));
                }
                else if (TelephonyServerStatisticElement.CurrentServerType == TelephonyServerTypes.Asterisk)
                {
                    AsteriskStatWrapper AsteriskWrapper = new AsteriskStatWrapper();
                    SaveIndicatorsValues(IndicatorClassData.System,
                        AsteriskStatWrapper.GetData(StatisticRetriever.StatisticNames.L02TotalCallsAbandoned));
                }
            }
            catch (Exception ex)
            {
            }

            /*SybaseConnection con = new SybaseConnection(SmartCadConfiguration.SmartCadSection.ServerElement.TelephonyServerElement.ConnectionString);
            con.Open();
            DateTime dt = DateTime.Today;
            string sql = "select SUM(CallsAbandoned) from iApplicationStat where year(Timestamp) = " + dt.ToString("yyyy") + " and month(Timestamp) = " + dt.ToString("MM") + " and day(Timestamp) = " + dt.ToString("dd") + " and ApplicationID = 1";
            SybaseCommand com = new SybaseCommand(sql, con);
            
            try
            {
                SybaseDataReader reader = com.ExecuteReader();
                while (reader.Read())
                {
                    double abandoned = reader.GetInt32(0);
                    SaveIndicatorsValues(IndicatorClassData.System, abandoned);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
            }

            com.Dispose();
            con.Close();
            con.Dispose();*/
        }
    }
}
