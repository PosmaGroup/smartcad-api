using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using SmartCadCore.Model;

namespace SmartCadCore.Core.Indicators
{
    public class D17IndicatorValuesCalculator : IndicatorValuesCalculator
    {
        protected override string GetIndicator()
        {
            return "D17";
        }

        protected override void Calculate()
        {
            CalculateDepartment();
        }

        private void CalculateDepartment()
        {

            #region new
            string sql = @"SELECT		COUNT(UNIT.CODE) AS CANTIDAD, DETY.CODE AS ORGANISMO
FROM		DEPARTMENT_STATION AS DEPS
			,INCIDENT_NOTIFICATION AS INOT
			,DISPATCH_ORDER AS DIOR
			,UNIT AS UNIT
			,DEPARTMENT_STATION AS DEPS1
			,DEPARTMENT_TYPE AS DETY
WHERE		INOT.DEPARTMENT_STATION_CODE = DEPS.CODE
AND			INOT.CODE = DIOR.INCIDENT_NOTIFICATION_CODE
AND			DIOR.UNIT_CODE = UNIT.CODE
AND			UNIT.DEPARTMENT_STATION_CODE = DEPS1.CODE
AND			DEPS.DEPARTMENT_ZONE_CODE <> DEPS1.DEPARTMENT_ZONE_CODE
AND			INOT.DEPARTMENT_TYPE_CODE = DETY.CODE
GROUP BY	DETY.CODE
";

            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);
            
            
                    
            foreach (object[] item in data)
            {                
           
                ObjectData ob = new ObjectData();
                ob.Code = int.Parse((item[1].ToString()));
                SaveIndicatorsValues(ob, IndicatorClassData.Department, double.Parse((item[0].ToString())));
             
            }
            #endregion new

            #region doc

            //IList departmentCodes = (IList)SmartCadDatabase.SearchBasicObjects(
            //    SmartCadHqls.GetDepartmentsTypeCodes);
            //foreach (int departmentCode in departmentCodes)
            //{
            //    double amount = (long)SmartCadDatabase.SearchBasicObject(
            //        SmartCadHqls.GetCustomHql(
            //        SmartCadHqls.GetAmountUnitDispatchByDifferentZoneGroupByDepartmentType,
            //        departmentCode));

            //    ObjectData od = new ObjectData();
            //    od.Code = departmentCode;
            //    SaveIndicatorsValues(od, IndicatorClassData.Department, amount);
            //}
            #endregion doc
        }
    }
}
