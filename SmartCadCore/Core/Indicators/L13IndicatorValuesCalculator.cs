//using System;
//using System.Collections.Generic;
//using System.Text;
//using SmartCadCore.Model;
//using System.Collections;

//namespace SmartCadCore.Core.Indicators
//{
//    public class L13IndicatorValuesCalculator : IndicatorValuesCalculator
//    {
//        protected override string GetIndicator()
//        {
//            return "L13";
//        }

//        protected override void Calculate()
//        {
//            CalculateSystemIndicators();
//            CalculateGroup();
//        }

//        private void CalculateSystemIndicators()
//        {
//            #region new
//            string sql = @"SELECT		COUNT(REB.CODE) AS CANTIDAD
//			,PHR.INCOMPLETE AS ESTADO
//FROM		PHONE_REPORT AS PHR
//			,REPORT_BASE AS REB
//			,INCIDENT AS INC
//WHERE		CONVERT(DATETIME,CONVERT(CHAR(10),PHR.PICKED_UP_CALL_TIME,101),101)= 
//			CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
//AND			PHR.PARENT_CODE = REB.CODE
//AND			REB.INCIDENT_CODE = INC.CODE
//AND         INC.IS_EMERGENCY = 1
//GROUP BY	PHR.INCOMPLETE ,INC.IS_EMERGENCY";

//            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);

//            double totalCalls = 0;
//            double Incomplete = 0;
//            List<double> resultsValues = new List<double>();
//            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();

//            foreach (object[] item in data)
//            {
//                totalCalls += double.Parse(item[0].ToString());
//            }
//            foreach (object[] item in data)
//            {
//                if ((bool)item[1] == true)
//                {
//                    Incomplete = double.Parse(item[0].ToString());
//                    break;
//                }
//            }
//            resultsValues.Add(Incomplete);
//            if (totalCalls == 0)
//            {
//                resultsValues.Add(0);
//            }
//            else
//            {
//                resultsValues.Add((Incomplete * 100) / totalCalls);
//            }
//            result.Add("Incomplete", resultsValues);
//            SaveIndicatorMultiplesValues(null, IndicatorClassData.System, result);

//            #endregion new

//            #region doc
//            //double percentValueIncompleteCalls = 0;
//            //double incompleteCalls = 0;
           
//            //IList phoneCallEmergency = SmartCadDatabase.SearchObjects(
//            //    SmartCadHqls.GetCustomHql(SmartCadHqls.GetPhoneReportsEmergencyCurrentday,
//            //    ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today),
//            //    ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today.AddDays(1))));


//            //foreach (PhoneReportData phoneReport in phoneCallEmergency)
//            //{
//            //    if (phoneReport.Incomplete == true)
//            //    {
//            //        incompleteCalls++;
//            //    }

//            //}
            
//            //if (phoneCallEmergency.Count != 0)
//            //{
//            //    percentValueIncompleteCalls = (incompleteCalls * 100) / (double)(phoneCallEmergency.Count);
//            //}
//            //    List<double> resultsValues = new List<double>();
//            //    resultsValues.Add(incompleteCalls);
//            //    resultsValues.Add(percentValueIncompleteCalls);
//            //    Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//            //    result.Add("IncompleteCalls", resultsValues);
//            //    SaveIndicatorMultiplesValues(null, IndicatorClassData.System, result);
//            #endregion doc


//        }

//        private void CalculateGroup()
//        {
//            #region new
//            string sql = @" SELECT		COUNT(REB.CODE) AS CANTIDAD
//			,PHR.INCOMPLETE AS ESTADO			
//			,OPE.CODE AS SUPERVISOR
//FROM		PHONE_REPORT AS PHR
//			,REPORT_BASE AS REB
//			,INCIDENT AS INC
//			,OPERATOR_ASSIGN AS OPA
//			,OPERATOR AS OPE
//WHERE		CONVERT(DATETIME,CONVERT(CHAR(10),PHR.PICKED_UP_CALL_TIME,101),101)= 
//			CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
//AND			PHR.PARENT_CODE = REB.CODE
//AND			REB.INCIDENT_CODE = INC.CODE
//AND         INC.IS_EMERGENCY = 1
//AND			REB.OPERATOR_CODE = OPA.OPERATOR_CODE
//AND			OPA.DELETED_ID IS NULL
//AND			(
//				(				
//				PHR.PICKED_UP_CALL_TIME
//				BETWEEN OPA.START_DATE AND OPA.END_DATE
//				)			
//			)
//AND			OPA.SUPERVISOR_CODE = OPE.CODE
//GROUP BY	PHR.INCOMPLETE, OPE.CODE
//order by ope.code
//";

//            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);

//            double totalCalls = 0;
//            double Incomplete = 0;


//            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//            List<double> resultsValues = new List<double>();
//            int supervisorCode = 0;

//            foreach (object[] item in data)
//            {
//                if (supervisorCode != 0 && supervisorCode != int.Parse(item[2].ToString()))
//                {
//                    resultsValues.Add(Incomplete);
//                    if (totalCalls == 0)
//                    {
//                        resultsValues.Add(0);
//                    }
//                    else
//                    {
//                        resultsValues.Add((Incomplete * 100) / totalCalls);
//                    }

//                    result.Add("Incomplete", resultsValues);

//                    ObjectData od1 = new ObjectData();
//                    od1.Code = supervisorCode;
//                    SaveIndicatorMultiplesValues(od1, IndicatorClassData.Group, result);
//                    result.Clear();
//                    resultsValues = new List<double>();
//                    totalCalls = 0;
//                }
//                if ((bool)item[1] == true)
//                {
//                    Incomplete = double.Parse(item[0].ToString());
//                }
//                totalCalls += double.Parse(item[0].ToString());
//                supervisorCode = (int)item[2];

//                if (item == data[data.Count - 1])
//                {
//                    resultsValues.Add(Incomplete);
//                    if (totalCalls == 0)
//                    {
//                        resultsValues.Add(0);
//                    }
//                    else
//                    {
//                        resultsValues.Add((Incomplete * 100) / totalCalls);
//                    }

//                    result.Add("Incomplete", resultsValues);

//                    ObjectData od1 = new ObjectData();
//                    od1.Code = supervisorCode;
//                    SaveIndicatorMultiplesValues(od1, IndicatorClassData.Group, result);
//                    result.Clear();
//                    resultsValues = new List<double>();
//                    totalCalls = 0;
//                }
//            }
//            #endregion new

//            #region doc
//            //IList supervisors = OperatorScheduleManager.GetSupervisorsWorkingNowByApplication("FirstLevelSupervisorName");

//            //foreach (int supervisor in supervisors)
//            //{
//            //    IList operators = OperatorScheduleManager.GetAllDayOperatorsBySupervisor(supervisor);
//            //    WorkShiftScheduleVariationData bshSupervisor =
//            //     OperatorScheduleManager.GetCurrentWorkShiftSchedulesByOperator(supervisor, SmartCadDatabase.GetTimeFromBD().Date);

//            //    double totalEmergencyCalls = 0;
//            //    double incompleteCalls = 0;
//            //    double percentValueIncompleteCalls = 0;

//            //    foreach (OperatorData operatorData in operators)
//            //    {
//            //        //BaseSessionHistory bsh =
//            //        //  OperatorScheduleManager.GetOperatorAssignBySupervisorAndByDayRealHours(
//            //        //  supervisor,
//            //        //  operatorData.Code);

//            //        IList bshList =
//            //          OperatorScheduleManager.GetAllDayOperatorAssignBySupervisorAndByDayRealHours(supervisor, operatorData.Code);
//            //        foreach (OperatorAssignData bsh in bshList)
//            //        {
//            //            if ((bshSupervisor != null) &&
//            //                (((bsh.StartDate.TimeOfDay >= bshSupervisor.Start.Value.TimeOfDay) &&
//            //               (bsh.StartDate.TimeOfDay <= bshSupervisor.End.Value.TimeOfDay)) ||
//            //               ((bsh.EndDate.TimeOfDay <= bshSupervisor.End.Value.TimeOfDay) &&
//            //               (bsh.EndDate.TimeOfDay >= bshSupervisor.Start.Value.TimeOfDay))))
//            //            {
//            //                IList phoneCallEmergency = SmartCadDatabase.SearchObjects(
//            //                        SmartCadHqls.GetCustomHql(
//            //                        SmartCadHqls.GetPhoneReportsEmergencyCurrentdayByOperator,
//            //                        operatorData.Code,
//            //                        ApplicationUtil.GetDataBaseFormattedDate(bsh.StartDate),
//            //                        ApplicationUtil.GetDataBaseFormattedDate(bsh.EndDate)));

//            //                foreach (PhoneReportData phoneReport in phoneCallEmergency)
//            //                {
//            //                    if (phoneReport.Incomplete == true)
//            //                    {
//            //                        incompleteCalls++;
//            //                    }
//            //                }
//            //                totalEmergencyCalls += phoneCallEmergency.Count;
//            //            }
//            //        }
//            //    }
//            //    if (totalEmergencyCalls > 0)
//            //    {
//            //        percentValueIncompleteCalls = (incompleteCalls * 100) /
//            //            (totalEmergencyCalls);
//            //    }
//            //    else
//            //    {
//            //        percentValueIncompleteCalls = 0;
//            //    }
//            //    List<double> resultsValues = new List<double>();
//            //    resultsValues.Add(incompleteCalls);
//            //    resultsValues.Add(percentValueIncompleteCalls);
//            //    Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//            //    result.Add("CompleteCalls", resultsValues);
//            //    ObjectData objectdata = new ObjectData();
//            //    objectdata.Code = supervisor;
//            //    SaveIndicatorMultiplesValues(objectdata, IndicatorClassData.Group, result);
//            //}
//            #endregion doc
//        }       
//    }
//}
