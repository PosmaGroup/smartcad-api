using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using System.Collections;
using NHibernate.Mapping;

namespace SmartCadCore.Core.Indicators
{
    public class D07IndicatorValuesCalculator : IndicatorValuesCalculator
    {
        ArrayList departmentsAllZeroValue = new ArrayList() { 1, 2, 3, 4, 5, 6, 7, 8 };
        //ArrayList departmentsZeroValue;
        protected override string GetIndicator()
        {
            return "D07";
        }

        protected override void Calculate()
        {
            CalculateDepartment();
        }


        private void CalculateDepartment()
        {
            string sql = @"SELECT			DETY.CODE AS ORGANISMO, AGRUPADO.TIPO AS TIPO, COUNT(AGRUPADO.CODIGO) TOTAL, 
                            ROUND(
                                  CAST(COUNT(AGRUPADO.CODIGO) AS FLOAT) /
                                        (SELECT COUNT(*) AS TO1
				                             FROM INCIDENT_NOTIFICATION INOT LEFT OUTER JOIN 
					                              DEPARTMENT_TYPE DT ON INOT.DEPARTMENT_TYPE_CODE = DT.CODE
				                             WHERE INOT.INCIDENT_NOTIFICATION_STATUS_CODE = 5 AND
					                               DT.CODE = DETY.CODE), 4) PERCENTAGE
                            FROM			(
				                            SELECT
				                            INC_1.CODE AS CODIGO,
				                            TIPO = RTRIM(REPLACE(REPLACE(o.list,'<FRIENDLY_NAME>',''),'</FRIENDLY_NAME>',', '))
				                            FROM
				                            INCIDENT AS INC_1
				                            CROSS APPLY
				                            (
				                            SELECT		INTY.FRIENDLY_NAME
				                            FROM		INCIDENT AS INC
							                            ,REPORT_BASE AS REB
							                            ,REPORT_BASE_INCIDENT_TYPE AS RBIT
							                            ,INCIDENT_TYPE AS INTY
				                            WHERE		INC.IS_EMERGENCY = 'TRUE'
				                            AND			INC.CODE = REB.INCIDENT_CODE
				                            AND			REB.CODE = RBIT.REPORT_BASE_CODE
				                            AND			RBIT.INCIDENT_TYPE_CODE = INTY.CODE
				                            AND			INC.CODE = INC_1.CODE
				                            ORDER BY INTY.FRIENDLY_NAME
				                            FOR XML PATH('')
				                            ) o (list)
				                            WHERE INC_1.IS_EMERGENCY = 'TRUE'
				                            ) AS AGRUPADO
				                            ,REPORT_BASE AS REB
				                            ,INCIDENT_NOTIFICATION AS INOT
				                            ,DEPARTMENT_TYPE AS DETY
                            WHERE			INOT.REPORT_BASE_CODE = REB.CODE
                            AND				AGRUPADO.CODIGO = REB.INCIDENT_CODE
                            AND				INOT.INCIDENT_NOTIFICATION_STATUS_CODE = 5
                            AND				INOT.DEPARTMENT_TYPE_CODE = DETY.CODE
                            GROUP BY		AGRUPADO.TIPO,DETY.CODE
                            order by DETY.CODE";

            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);
            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
            int departmentCode = 0;
            foreach (object[] item in data)
            {
                if (departmentCode != 0 && departmentCode != int.Parse(item[0].ToString()))
                {
                    ObjectData od1 = new ObjectData();
                    od1.Code = departmentCode;
                    SaveIndicatorMultiplesValues(od1, IndicatorClassData.Department, result);

                    result.Clear();
                }

                departmentCode = (int)item[0];
                List<double> list = new List<double>();
                list.Add(double.Parse(item[2].ToString()));
                list.Add(((double)item[3])*100);

                result.Add(item[1].ToString().Substring(0, item[1].ToString().Length - 1), list);
                if (item == data[data.Count - 1])
                {
                    ObjectData od1 = new ObjectData();
                    od1.Code = departmentCode;
                    SaveIndicatorMultiplesValues(od1, IndicatorClassData.Department, result);
                }
            }

            return;
        }
    }
}
