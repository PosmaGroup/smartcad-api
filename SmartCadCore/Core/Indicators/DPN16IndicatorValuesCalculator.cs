//using System;
//using System.Collections.Generic;
//using System.Text;
//using SmartCadCore.Model;
//using System.Collections;

//namespace SmartCadCore.Core.Indicators
//{
//    public class DPN16IndicatorValuesCalculator : IndicatorValuesCalculator
//    {
//        protected override string GetIndicator()
//        {
//            return "DPN16";
//        }

//        protected override void Calculate()
//        {
//           // CalculateSystemResult();
//           // CalculateGroup();
//            CalculateDepartment();
           
//        }
//        protected void CalculateSystemResult()
//        {
//            #region new
//            string sql = @" SELECT		COUNT(INC.CODE) AS CANTIDAD
//			,INCS.CODE AS ESTADO
//FROM		INCIDENT AS INC
//			,INCIDENT_STATUS AS INCS
//WHERE		(	CONVERT(DATETIME,CONVERT(CHAR(10),INC.END_DATE,101),101) = 
//				CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
//				OR
//				INC.END_DATE IS NULL
//			)
//AND			INC.INCIDENT_STATUS_CODE = INCS.CODE
//GROUP BY	INCS.CODE
//";

//            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);

//            double totalIncidentes = 0;
//            double closeIncidents = 0;
//            List<double> resultsValues = new List<double>();
//            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();

//            foreach (object[] item in data)
//            {
//                totalIncidentes += double.Parse(item[0].ToString());
//            }
//            foreach (object[] item in data)
//            {
//                if (double.Parse(item[1].ToString()) == IncidentStatusData.Closed.Code)
//                {
//                    closeIncidents = double.Parse(item[0].ToString());
//                    break;
//                }
//            }
//            resultsValues.Add(closeIncidents);
//            if (totalIncidentes == 0)
//            {
//                resultsValues.Add(0);
//            }
//            else
//            {
//                resultsValues.Add((closeIncidents * 100) / totalIncidentes);
//            }
//            result.Add("closeIncidents", resultsValues);
//            SaveIndicatorMultiplesValues(null, IndicatorClassData.System, result);

//            #endregion new

//            #region doc
//            //double closedIncidents = 0;
//            //double openIncidents = 0;
//            //double totalIncients = 0;            
//            //double percentClosedIncident = 0;
//            //Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();

//            //closedIncidents = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(
//            //  SmartCadHqls.GetClosedIncidentsCountByDate,
//            //  ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today),
//            //  ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today.AddDays(1)),
//            //  IncidentStatusData.Closed.Name));
          
            

            
//            //openIncidents = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(
//            //  SmartCadHqls.GetOpenIncidentsCount, IncidentStatusData.Open.Name));

//            //totalIncients = closedIncidents + openIncidents;
            
//            //if (totalIncients != 0)
//            //{
//            //    percentClosedIncident = (closedIncidents * 100) / totalIncients;
//            //}
//            //else
//            //{
//            //    percentClosedIncident = 0;
//            //}

//            //List<double> resultsValues = new List<double>();
//            //resultsValues.Add(closedIncidents);
//            //resultsValues.Add(percentClosedIncident);
//            //result.Add(ResourceLoader.GetString2("IndicatorDPN16Name"), resultsValues);
//            //SaveIndicatorMultiplesValues(null, IndicatorClassData.System, result);
//            #endregion doc
//        }

//        #region doc
//        private void CalculateGroup()
//        {
//            IList supervisors = OperatorScheduleManager.GetSupervisorsWorkingNowByApplication("DispatchSupervisorName");

//            foreach (int supervisor in supervisors)
//            {
//                IList operators = OperatorScheduleManager.GetAllDayOperatorsBySupervisor(supervisor);

//                WorkShiftScheduleVariationData bshSupervisor =
//                   OperatorScheduleManager.GetCurrentWorkShiftSchedulesByOperator(supervisor, SmartCadDatabase.GetTimeFromBD().Date);


//                double totalOpenIncidents = 0;
//                double totalCloseIncidents = 0;
//                double percentCloseIncidents = 0;

//                foreach (OperatorData operatorData in operators)
//                {
//                    IList bshList =
//                      OperatorScheduleManager.GetAllDayOperatorAssignBySupervisorAndByDayRealHours(supervisor, operatorData.Code);

//                    double openIncidents = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(
//                               SmartCadHqls.GetOpenIncidentsByOperator, IncidentStatusData.Open.Name, operatorData.Code));
//                    totalOpenIncidents += openIncidents;

//                    foreach (OperatorAssignData bsh in bshList)
//                    {
//                        if ((bshSupervisor != null) &&
//                            (((bsh.StartDate.TimeOfDay >= bshSupervisor.Start.Value.TimeOfDay) &&
//                           (bsh.StartDate.TimeOfDay <= bshSupervisor.End.Value.TimeOfDay)) ||
//                           ((bsh.EndDate.TimeOfDay <= bshSupervisor.End.Value.TimeOfDay) &&
//                           (bsh.EndDate.TimeOfDay >= bshSupervisor.Start.Value.TimeOfDay))))
//                        {                           
//                            double closeIncidents = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(
//                                SmartCadHqls.GetCloseIncidentsByOperatorByDate, 
//                                IncidentStatusData.Closed.Name, 
//                                operatorData.Code,
//                                ApplicationUtil.GetDataBaseFormattedDate(bsh.StartDate),
//                                ApplicationUtil.GetDataBaseFormattedDate(bsh.EndDate)));                            
//                            totalCloseIncidents += closeIncidents;
//                        }
//                    }
//                }

//                if (totalCloseIncidents + totalOpenIncidents > 0)
//                {
//                    percentCloseIncidents = (totalCloseIncidents * 100) / (totalOpenIncidents + totalCloseIncidents);
//                }
//                else
//                {
//                    percentCloseIncidents = 0;
//                }


//                List<double> resultsValues = new List<double>();
//                resultsValues.Add(totalCloseIncidents);
//                resultsValues.Add(percentCloseIncidents);
//                Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//                result.Add("CloseIncidents", resultsValues);
//                ObjectData objectdata = new ObjectData();
//                objectdata.Code = supervisor;
//                SaveIndicatorMultiplesValues(objectdata, IndicatorClassData.Group, result);
//            }
//        }
//        #endregion doc
//        ArrayList departmentsAllZeroValue = new ArrayList() { 1, 2, 3, 4, 5, 6, 7, 8 };
//        private void CalculateDepartment()
//        {
//            #region new
//            string openIncidentNotificationSql = @" SELECT		COUNT(INOT.CODE) AS ABIERTAS,DETY.CODE AS ORGANISMO
//FROM		INCIDENT_NOTIFICATION AS INOT
//			,DEPARTMENT_TYPE AS DETY
//WHERE		INOT.END_DATE IS NULL
//AND			INOT.DEPARTMENT_TYPE_CODE = DETY.CODE
//GROUP BY DETY.CODE
//order by DETY.CODE
//";

//            string closeIncidentNotificationSql = @" SELECT		COUNT(INOT.CODE) AS CERRADAS,DETY.CODE AS ORGANISMO
//FROM		INCIDENT_NOTIFICATION AS INOT
//			,DEPARTMENT_TYPE AS DETY
//WHERE		INOT.START_DATE IS NOT NULL
//AND			CONVERT(DATETIME,CONVERT(CHAR(10),INOT.END_DATE,101),101)= 
//			CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
//AND			INOT.DEPARTMENT_TYPE_CODE = DETY.CODE
//GROUP BY DETY.CODE
//order by DETY.CODE
//";

//            IList openList = (IList)SmartCadDatabase.SearchBasicObjects(openIncidentNotificationSql, false);
//            IList closeList = (IList)SmartCadDatabase.SearchBasicObjects(closeIncidentNotificationSql, false);

//            Dictionary<int, double> departmentCodes = new Dictionary<int, double>();
//            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//            List<double> list = new List<double>();
//            foreach (object[] item in openList)
//            {
//                if (departmentCodes.ContainsKey(int.Parse(item[1].ToString())) == false)
//                {
//                    departmentCodes.Add(int.Parse(item[1].ToString()), int.Parse(item[0].ToString()));
//                }

//            }
//            foreach (object[] item in closeList)
//            {

//                if (departmentCodes.ContainsKey(int.Parse(item[1].ToString())) == false)
//                {
//                    departmentCodes.Add(int.Parse(item[1].ToString()), int.Parse(item[0].ToString()));
//                }
//                else
//                {
//                    departmentCodes[int.Parse(item[1].ToString())] += int.Parse(item[0].ToString());
//                }
//            }

//            foreach (KeyValuePair<int, double> pair in departmentCodes)
//            {

//                foreach (object[] item in closeList)
//                {
//                    if (int.Parse(item[1].ToString()) == pair.Key)
//                    {
//                        ObjectData od1 = new ObjectData();
//                        od1.Code = int.Parse(item[1].ToString());
//                        list.Add(int.Parse(item[0].ToString()));
//                        list.Add((int.Parse(item[0].ToString()) * 100) / pair.Value);
//                        result.Add("closeIncidents", list);
//                        SaveIndicatorMultiplesValues(od1, IndicatorClassData.Department, result);
//                        result.Clear();
//                        list = new List<double>();
//                        break;
//                    }
//                }
//            }
//            #endregion new

//            #region doc
//            //foreach (int item in departmentsAllZeroValue)
//            //{
//            //    Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//            //    List<double> resultsValues = new List<double>();
//            //    resultsValues.Add(0);
//            //    resultsValues.Add(0);
//            //    result.Add("Empty", resultsValues);
//            //    ObjectData od = new ObjectData();
//            //    od.Code = item;
//            //    SaveIndicatorMultiplesValues(od, IndicatorClassData.Department, result);
//            //}

//            //IList IncidentNotificationByDepartment = (IList)SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(
//            //        SmartCadHqls.GetIncidentNotificationCountCloseByDepartment,
//            //        IncidentNotificationStatusData.Closed.Code,
//            //        IncidentNotificationStatusData.Cancelled.Code,
//            //        ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today)));

//            //double percentOpenIncidents = 0;

//            //for (int i = 0; i < IncidentNotificationByDepartment.Count; i++)
//            //{
//            //    Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//            //    List<double> resultsValues = new List<double>();

//            //    int currentDTCode = (int)((object[])IncidentNotificationByDepartment[i])[0];
//            //    object[] item = (object[])IncidentNotificationByDepartment[i];

//            //    double totalIncidents = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(
//            //           SmartCadHqls.GetAllIncidentNotificationByDepartmentCode, currentDTCode));

//            //    if (totalIncidents > 0)
//            //    {
//            //        percentOpenIncidents = (Convert.ToDouble(item[1]) * 100) / (totalIncidents);
//            //    }
//            //    else
//            //    {
//            //        percentOpenIncidents = 0;
//            //    }

//            //    resultsValues.Add(Convert.ToDouble(item[1]));
//            //    resultsValues.Add(percentOpenIncidents);
//            //    result.Add("CloseIncidentsByDepartment", resultsValues);
//            //    ObjectData od = new ObjectData();
//            //    od.Code = currentDTCode;
//            //    SaveIndicatorMultiplesValues(od, IndicatorClassData.Department, result);
//            //}
//            #endregion doc
//        }


       
       
//    }
//}
