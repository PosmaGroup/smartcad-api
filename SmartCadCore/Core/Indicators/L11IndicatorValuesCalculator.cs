﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using SmartCadCore.Model;
using SmartCadCore.Statistic;
using SmartCadCore.Core.Util;

namespace SmartCadCore.Core.Indicators
{
    public class L11IndicatorValuesCalculator : IndicatorValuesCalculator
    {
        protected override string GetIndicator()
        {
            return "L11";
        }

        protected override void Calculate()
        {
            CalculateSystem();
        }

        private void CalculateSystem()
        {
            try
            {
                if (TelephonyServerStatisticElement.CurrentServerType == TelephonyServerTypes.Genesys)
                {
                    double value = GenesysStatWrapper.GetData(StatisticRetriever.StatisticNames.L11TotalCallsAbandonedWR);                        
                    SaveIndicatorsValues(IndicatorClassData.System, value);
                }
                else if (TelephonyServerStatisticElement.CurrentServerType == TelephonyServerTypes.Nortel)
                {
                    try
                    {
                        double result = NortelRTDWrapper.GetData((int)NRTD_Values.NIrtd_APPL_CALLS_OFFER) - NortelRTDWrapper.GetData((int)NRTD_Values.NIrtd_APPL_CALLS_ANS);
                        SaveIndicatorsValues(IndicatorClassData.System, result);
                    }
                    catch (Exception ex)
                    {
                    }

                    /*double system = 0;
                    IList operators = OperatorScheduleManager.GetOperatorsWorkingNowByApplication(UserApplicationData.FirstLevel.Name, false);
                    foreach (int operatorCode in operators)
                    {
                        string agentID = (string)SmartCadDatabase.SearchObject(SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorAgentID, operatorCode));

                        SybaseConnection con = new SybaseConnection(SmartCadConfiguration.SmartCadSection.ServerElement.TelephonyServerElement.ConnectionString);
                        con.Open();
                        DateTime dt = DateTime.Today;
                        string sql = "select SUM(CallsOffered) - SUM(CallsAnswered) from iAgentPerformanceStat where year(Timestamp) = " + dt.ToString("yyyy") + " and month(Timestamp) = " + dt.ToString("MM") + " and day(Timestamp) = " + dt.ToString("dd") + " and AgentLogin = " + agentID;
                        SybaseCommand com = new SybaseCommand(sql, con);

                        try
                        {
                            SybaseDataReader reader = com.ExecuteReader();
                            while (reader.Read())
                            {
                                double notAnswered = reader.GetInt32(0);
                                system += notAnswered;
                                SaveIndicatorsValues(IndicatorClassData.System, notAnswered);
                            }
                            reader.Close();
                        }
                        catch (Exception ex)
                        {
                        }

                        com.Dispose();
                        con.Close();
                        con.Dispose();
                    }
                    SaveIndicatorsValues(IndicatorClassData.System, system);*/
                }
                else if (TelephonyServerStatisticElement.CurrentServerType == TelephonyServerTypes.Avaya)
                {
                   
                    SaveIndicatorsValues(IndicatorClassData.System,
                        AvayaStatWrapper.GetData(StatisticRetriever.StatisticNames.L11TotalCallsAbandonedWR));
                }
                else if (TelephonyServerStatisticElement.CurrentServerType == TelephonyServerTypes.Asterisk)
                {
                    AsteriskStatWrapper AsteriskWrapper = new AsteriskStatWrapper();
                    SaveIndicatorsValues(IndicatorClassData.System,
                        AsteriskStatWrapper.GetData(StatisticRetriever.StatisticNames.L11TotalCallsAbandonedWR));
                }
            }
            catch (Exception ex)
            {
            }
            
        }
    }
}
