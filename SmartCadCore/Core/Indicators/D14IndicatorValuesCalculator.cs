using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using System.Collections;

namespace SmartCadCore.Core.Indicators
{
    public class D14IndicatorValuesCalculator : IndicatorValuesCalculator
    {
        protected override string GetIndicator()
        {
            return "D14";
        }

        protected override void Calculate()
        {
            CalculateDepartment();
        }

        private void CalculateDepartment()
        {
            #region new
            string availablesSql = @" SELECT     COUNT(UNIT.CODE) AS Disponibles,DEPARTMENT_STATION.NAME AS ESTACION_NAME  
FROM         DEPARTMENT_STATION INNER JOIN
                      UNIT ON DEPARTMENT_STATION.CODE = UNIT.DEPARTMENT_STATION_CODE			
WHERE     (UNIT.UNIT_STATUS_CODE = 1)
GROUP BY   DEPARTMENT_STATION.NAME 
";

            string totalSql = @" SELECT     COUNT(UNIT.CODE) AS Totales,DEPARTMENT_STATION.NAME AS ESTACION,
zone.DEPARTMENT_TYPE_CODE as code

FROM         DEPARTMENT_STATION INNER JOIN
                      UNIT ON DEPARTMENT_STATION.CODE = UNIT.DEPARTMENT_STATION_CODE
						,DEPARTMENT_ZONE as zone
WHERE     (zone.CODE = DEPARTMENT_STATION.DEPARTMENT_ZONE_CODE)
GROUP BY   DEPARTMENT_STATION.NAME , zone.DEPARTMENT_TYPE_CODE 
order by zone.DEPARTMENT_TYPE_CODE 
";
            
            IList availableList = (IList)SmartCadDatabase.SearchBasicObjects(availablesSql, false);
            IList total = (IList)SmartCadDatabase.SearchBasicObjects(totalSql, false);
            
            //data[total,department_name, department_code, available]
            List<object[]> data = new List<object[]>();           

            foreach (object[] itemTotal in total)
            {                
                object[] currentItem = new object[4];
                currentItem[0] = itemTotal[0];
                currentItem[1] = itemTotal[1];
                currentItem[2] = itemTotal[2];
               
                foreach (object[] itemAvailable in availableList)
                {
                    if (itemAvailable[1].ToString() == currentItem[1].ToString())
                    {                       
                        currentItem[3] = itemAvailable[0];
                        data.Add(currentItem);
                        break;
                    }
                }
                if (currentItem[3] == null)
                {
                    currentItem[3] = 0;
                    data.Add(currentItem);                    
                }

            }

            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
            List<double> list = new List<double>();
            int departmentCode = 0;
            string estationName = "";
            foreach (object[] item in data)
            {
                if (departmentCode != 0 && departmentCode != int.Parse(item[2].ToString()))
                {

                    ObjectData od1 = new ObjectData();
                    od1.Code = departmentCode;
                    SaveIndicatorMultiplesValues(od1, IndicatorClassData.Department, result);
                    result.Clear();
                    estationName = "";
                    list = new List<double>();
                }

                
                    estationName = item[1].ToString();
                    list.Add(double.Parse(item[3].ToString()));
                    list.Add(double.Parse(item[3].ToString()) / double.Parse(item[0].ToString()));
                    result.Add(estationName, list);
                    list = new List<double>();
                    departmentCode = (int)item[2];
               
                if (item == data[data.Count - 1])
                {
                    ObjectData od1 = new ObjectData();
                    od1.Code = departmentCode;
                    SaveIndicatorMultiplesValues(od1, IndicatorClassData.Department, result);
                }
            }
            #endregion new

            #region doc
            //IList availables = (IList)SmartCadDatabase.SearchBasicObjects(
            //    SmartCadHqls.GetCustomHql(
            //    SmartCadHqls.GetUnitsByStatusGroupByDepartmentTypeDepartmentStation,
            //    UnitStatusData.Available.Code));
            //IList totalUnitsList = (IList)SmartCadDatabase.SearchBasicObjects(
            //    SmartCadHqls.GetCustomHql(
            //    SmartCadHqls.GetAmountUnitGroupByDepartmentTypeDepartmentStation));
            //Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
            //int departmentCode = 0;
            //string departmentStationName = string.Empty;
            //int availablePivot = 0;
            //for (int i = 0; i < totalUnitsList.Count; i++)
            //{
            //    object[] dt = (object[])totalUnitsList[i];
            //    if (departmentCode != 0 && departmentCode != int.Parse(dt[0].ToString()))
            //    {
            //        ObjectData od1 = new ObjectData();
            //        od1.Code = departmentCode;
            //        SaveIndicatorMultiplesValues(od1, IndicatorClassData.Department, result);

            //        result.Clear();
            //    }

            //    departmentCode = (int)dt[0];
            //    departmentStationName = (string)dt[1];
            //    long totalUnits = (long)dt[2];

            //    int departmentTypeAux = 0;
            //    string departmentStationAux = string.Empty;
            //    long available = 0;
            //    for (int j = availablePivot; j < availables.Count; j++)
            //    {
            //        object[] dt1 = (object[])availables[j];
            //        departmentTypeAux = (int)dt1[0];
            //        departmentStationAux = (string)dt1[1];
            //        available = (long)dt1[2];

            //        availablePivot = j + 1;

            //        if (departmentTypeAux == departmentCode && departmentStationAux == departmentStationName)
            //            j = availables.Count;
            //    }

            //    if (available > 0 && totalUnits > 0)
            //    {
            //        List<double> items = new List<double>();
            //        items.Add(available);
            //        items.Add(available / totalUnits);
            //        result.Add(departmentStationName, items);
            //    }
            //    else
            //    {
            //        List<double> items = new List<double>();
            //        items.Add(0);
            //        items.Add(0);
            //        result.Add(departmentStationName, items);
            //    }
            //}
            //}
            //}
            #endregion doc
        }
   
    }
}