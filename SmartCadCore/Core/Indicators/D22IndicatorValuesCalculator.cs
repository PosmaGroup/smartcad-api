//using System;
//using System.Collections.Generic;
//using System.Text;
//using SmartCadCore.Model;
//using System.Collections;

//namespace SmartCadCore.Core.Indicators
//{
//    public class D22IndicatorValuesCalculator : IndicatorValuesCalculator
//    {
//        protected override string GetIndicator()
//        {
//            return "D22";
//        }

//        protected override void Calculate()
//        {
//            CalculateSystem();
//            CalculateGroup();
//            CalculateDepartment();
//        }
//        private void CalculateSystem()
//        {
//            #region new all
//            string sqlConnected = @" SELECT		COUNT(SEH.USER_ACCOUNT_CODE) AS CANTIDAD
//			,OPS.CODE AS ESTADO		
//FROM        USER_APPLICATION AS USA
//			,SESSION_HISTORY AS SEH
//			,SESSION_STATUS_HISTORY AS STH
//			,OPERATOR_STATUS AS OPS
//WHERE		(
//			
//			USA.CODE = 3
//			)
//AND			USA.CODE = SEH.USER_APPLICATION_CODE
//AND			SEH.CODE = STH.SESSION_HISTORY_CODE
//AND			STH.OPERATOR_STATUS_CODE = OPS.CODE
//AND			SEH.END_DATE_LOGIN IS NULL
//AND			STH.END_DATE IS NULL 
//AND         SEH.IS_LOGGED_IN = 1
//GROUP BY	OPS.CODE,USA.FRIENDLY_NAME
//
//";

//            string sqlNoConnected = @" SELECT	COUNT(OPA.OPERATOR_CODE) AS CANTIDAD
//		,'No Conectado' AS ESTADOS	
//FROM	OPERATOR_ASSIGN AS OPA
//		,OPERATOR AS OPE
//		,USER_ROLE AS USR
//		,USER_ROLE_PROFILE AS URP
//		,USER_PROFILE AS USP
//		,USER_PROFILE_ACCESS AS UPA
//		,USER_ACCESS AS USA
//		,USER_APPLICATION AS APP
//WHERE	OPA.DELETED_ID IS NULL
//AND		GETDATE()
//		BETWEEN OPA.START_DATE AND OPA.END_DATE
//AND		OPA.OPERATOR_CODE NOT IN (
//									SELECT	SEH.USER_ACCOUNT_CODE AS OPERATOR
//									FROM	SESSION_HISTORY AS SEH
//									WHERE	SEH.IS_LOGGED_IN = 1
//									)
//AND		OPE.CODE = OPA.OPERATOR_CODE
//AND		USR.CODE = OPE.USER_ROLE_CODE
//AND		URP.USER_ROLE_CODE = USR.CODE
//AND		USP.CODE = URP.USER_PROFILE_CODE
//AND		UPA.USER_PROFILE_CODE = USP.CODE
//AND		USA.CODE = UPA.USER_ACCESS_CODE
//AND		APP.CODE = USA.USER_APPLICATION_CODE
//AND		(
//		
//		APP.CODE = 3
//		)
//";
//            IList operatorsConnected = (IList)SmartCadDatabase.SearchBasicObjects(sqlConnected, false);
//            IList operatorsNoConnected = (IList)SmartCadDatabase.SearchBasicObjects(sqlNoConnected, false);

//            double totalOperatorsConnected = 0;
//            double totalOperatorsNoConnected = 0;
//            double totalOperators = 0;
//            List<double> resultsValues = new List<double>();
//            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();

//            foreach (object[] item in operatorsConnected)
//            {
//                totalOperatorsConnected += int.Parse(item[0].ToString());
//            }
//            foreach (object[] item in operatorsNoConnected)
//            {
//                totalOperatorsNoConnected += int.Parse(item[0].ToString());
//            }
//            totalOperators = totalOperatorsConnected + totalOperatorsNoConnected;



//            resultsValues.Add(totalOperatorsConnected);
//            resultsValues.Add((totalOperatorsConnected / totalOperators) * 100);
//            result.Add(ResourceLoader.GetString2("IndicatorD22Name"), resultsValues);
//            SaveIndicatorMultiplesValues(null, IndicatorClassData.System, result);


//            result.Clear();
//            resultsValues.Clear();
//            resultsValues.Add(totalOperatorsNoConnected);
//            resultsValues.Add((totalOperatorsNoConnected / totalOperators) * 100);
//            result.Add(ResourceLoader.GetString2("IndicatorD23Name"), resultsValues);
//            SaveIndicatorMultiplesValues(null, IndicatorClassData.System, result);

//            result.Clear();
//            resultsValues.Clear();
//            foreach (object[] item in operatorsConnected)
//            {
//                resultsValues.Add(double.Parse(item[0].ToString()));
//                resultsValues.Add((double.Parse(item[0].ToString()) / totalOperatorsConnected) * 100);

//                if (int.Parse(item[1].ToString()) == OperatorStatusData.Absent.Code)
//                {
//                    result.Add(ResourceLoader.GetString2("IndicatorD35Name"), resultsValues);
//                }
//                else if (int.Parse(item[1].ToString()) == OperatorStatusData.Bathroom.Code)
//                {
//                    result.Add(ResourceLoader.GetString2("IndicatorD38Name"), resultsValues);
//                }
//                else if (int.Parse(item[1].ToString()) == OperatorStatusData.Busy.Code)
//                {
//                    result.Add(ResourceLoader.GetString2("IndicatorD34Name"), resultsValues);
//                }
//                else if (int.Parse(item[1].ToString()) == OperatorStatusData.Ready.Code)
//                {
//                    result.Add(ResourceLoader.GetString2("IndicatorD20Name"), resultsValues);
//                }
//                else if (int.Parse(item[1].ToString()) == OperatorStatusData.Rest.Code)
//                {
//                    result.Add(ResourceLoader.GetString2("IndicatorD37Name"), resultsValues);
//                }
//                else if (int.Parse(item[1].ToString()) == OperatorStatusData.Reunion.Code)
//                {
//                    result.Add(ResourceLoader.GetString2("IndicatorD36Name"), resultsValues);
//                }
//                SaveIndicatorMultiplesValues(null, IndicatorClassData.System, result);
//            }




//            #endregion new all
           
//            #region new
////            string sqlConnected = @" SELECT		COUNT(SEH.USER_ACCOUNT_CODE) AS CANTIDAD
////				
////FROM        USER_APPLICATION AS USA
////			,SESSION_HISTORY AS SEH
////			,SESSION_STATUS_HISTORY AS STH
////			,OPERATOR_STATUS AS OPS
////WHERE		(
////			USA.CODE = 3
////			)
////AND			USA.CODE = SEH.USER_APPLICATION_CODE
////AND			SEH.CODE = STH.SESSION_HISTORY_CODE
////AND			STH.OPERATOR_STATUS_CODE = OPS.CODE
////AND			SEH.END_DATE_LOGIN IS NULL
////AND			STH.END_DATE IS NULL 
////AND         SEH.IS_LOGGED_IN = 1
////";

////            string sqlNoConnected = @" SELECT	COUNT(OPA.OPERATOR_CODE) AS CANTIDAD
////				
////FROM	OPERATOR_ASSIGN AS OPA
////		,OPERATOR AS OPE
////		,USER_ROLE AS USR
////		,USER_ROLE_PROFILE AS URP
////		,USER_PROFILE AS USP
////		,USER_PROFILE_ACCESS AS UPA
////		,USER_ACCESS AS USA
////		,USER_APPLICATION AS APP
////WHERE	OPA.DELETED_ID IS NULL
////AND		GETDATE()
////		BETWEEN OPA.START_DATE AND OPA.END_DATE
////AND		OPA.OPERATOR_CODE NOT IN (
////									SELECT	SEH.USER_ACCOUNT_CODE AS OPERATOR
////									FROM	SESSION_HISTORY AS SEH
////									WHERE	SEH.IS_LOGGED_IN = 1
////									)
////AND		OPE.CODE = OPA.OPERATOR_CODE
////AND		USR.CODE = OPE.USER_ROLE_CODE
////AND		URP.USER_ROLE_CODE = USR.CODE
////AND		USP.CODE = URP.USER_PROFILE_CODE
////AND		UPA.USER_PROFILE_CODE = USP.CODE
////AND		USA.CODE = UPA.USER_ACCESS_CODE
////AND		APP.CODE = USA.USER_APPLICATION_CODE
////AND		(
////		
////		APP.CODE = 3
////		)
////";

////            IList operatorsConnected = (IList)SmartCadDatabase.SearchBasicObjects(sqlConnected, false);
////            IList operatorsNoConnected = (IList)SmartCadDatabase.SearchBasicObjects(sqlNoConnected, false);

////            double totalOperatorsConnected = 0;
////            double totalOperatorsNoConnected = 0;
////            double totalOperators = 0;
////            double operatorsReady = 0;
////            List<double> resultsValues = new List<double>();
////            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();

////            foreach (int item in operatorsConnected)
////            {
////                totalOperatorsConnected += item;
////            }
////            foreach (int item in operatorsNoConnected)
////            {
////                totalOperatorsNoConnected += item;
////            }
////            totalOperators = totalOperatorsConnected + totalOperatorsNoConnected;
////            resultsValues.Add(totalOperatorsConnected);
////            if (totalOperators == 0)
////            {
////                resultsValues.Add(0);
////            }
////            else
////            {
////                resultsValues.Add((totalOperatorsConnected * 100) / totalOperators);
////            }
////            result.Add("ConnectedOperators", resultsValues);
////            SaveIndicatorMultiplesValues(null, IndicatorClassData.System, result);

//            #endregion new

//            #region doc
////            double percentConnectedOperators = 0;
////            double totalOperatorsConnected = 0;
////            string hql = @"select count(*) 
////                         from SessionHistoryData data 
////                         where  data.UserApplication.Code = {0} and
////                         data.IsLoggedIn = {1}";

////            double dispatchOperatorsReady = (long)SmartCadDatabase.SearchBasicObject(
////                                                SmartCadHqls.GetCustomHql(
////                                                    hql,
////                                                    UserApplicationData.Dispatch.Code,
////                                                    true.ToString().ToLower()));


////            double dispatchOperatorsWorkingNow = OperatorScheduleManager.GetOperatorsWorkingNowByApplication(
////                UserApplicationData.Dispatch.Name, false).Count;


////            totalOperatorsConnected = dispatchOperatorsReady;
////            if ((dispatchOperatorsWorkingNow > 0))
////            {
////                percentConnectedOperators = (totalOperatorsConnected * 100) /
////                    (dispatchOperatorsWorkingNow);
////            }
////            else
////            {
////                percentConnectedOperators = 0;
////            }
////            percentConnectedOperators = Math.Round(percentConnectedOperators, 2);
////            List<double> resultsValues = new List<double>();
////            resultsValues.Add(totalOperatorsConnected);
////            resultsValues.Add(percentConnectedOperators);
////            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
////            result.Add("AvailableOperators", resultsValues);
////            SaveIndicatorMultiplesValues(null, IndicatorClassData.System, result);
//            #endregion doc
//        }


//        private void CalculateGroup()
//        {

//            #region new all
//            string operatorsConnectedSql = @" SELECT		COUNT(SEH.USER_ACCOUNT_CODE) AS CANTIDAD						
//			,OPS.CODE As STATUS
//			,OPE.CODE AS SUPERVISOR			
//FROM        USER_APPLICATION AS USA
//			,SESSION_HISTORY AS SEH
//			,SESSION_STATUS_HISTORY AS STH
//			,OPERATOR_STATUS AS OPS
//			,OPERATOR_ASSIGN AS OPA
//			,OPERATOR AS OPE
//WHERE		USA.CODE = SEH.USER_APPLICATION_CODE
//AND			USA.CODE = 3			
//AND			SEH.CODE = STH.SESSION_HISTORY_CODE
//AND			STH.OPERATOR_STATUS_CODE = OPS.CODE
//AND			SEH.END_DATE_LOGIN IS NULL
//AND			STH.END_DATE IS NULL 
//AND         SEH.IS_LOGGED_IN = 1
//AND			SEH.USER_ACCOUNT_CODE = OPA.OPERATOR_CODE
//AND			OPA.DELETED_ID IS NULL
//AND			(
//				(				
//				GETDATE()
//				BETWEEN OPA.START_DATE AND OPA.END_DATE
//				)			
//			)
//AND			OPA.SUPERVISOR_CODE = OPE.CODE
//GROUP BY	OPE.CODE, OPS.CODE
//order by OPE.CODE
//";

//            string operatorsNoConnectedSql = @"SELECT	count(distinct(OPA.OPERATOR_CODE)) AS CANTIDAD		
//		,OPE2.CODE AS SUPERVISOR		
//FROM	OPERATOR_ASSIGN AS OPA
//		,OPERATOR AS OPE
//		,OPERATOR AS OPE2
//		,USER_ROLE AS USR
//		,USER_ROLE_PROFILE AS URP
//		,USER_PROFILE AS USP
//		,USER_PROFILE_ACCESS AS UPA
//		,USER_ACCESS AS USA
//		,USER_APPLICATION AS APP		
//WHERE	OPA.DELETED_ID IS NULL
//AND		GETDATE()
//		BETWEEN OPA.START_DATE AND OPA.END_DATE
//AND		OPA.OPERATOR_CODE NOT IN (
//									SELECT	SEH.USER_ACCOUNT_CODE AS OPERATOR
//									FROM	SESSION_HISTORY AS SEH
//									WHERE	SEH.IS_LOGGED_IN = 1
//									)
//AND		OPE.CODE = OPA.OPERATOR_CODE
//AND		OPE2.CODE = OPA.SUPERVISOR_CODE
//AND		USR.CODE = OPE.USER_ROLE_CODE
//AND		URP.USER_ROLE_CODE = USR.CODE
//AND		USP.CODE = URP.USER_PROFILE_CODE
//AND		UPA.USER_PROFILE_CODE = USP.CODE
//AND		USA.CODE = UPA.USER_ACCESS_CODE
//AND		APP.CODE = USA.USER_APPLICATION_CODE
//AND		APP.CODE = 3		
//GROUP By OPE2.CODE
//order By OPE2.CODE 
//";

//            IList operatorsConnected = (IList)SmartCadDatabase.SearchBasicObjects(operatorsConnectedSql, false);
//            IList operatorsNoConnected = (IList)SmartCadDatabase.SearchBasicObjects(operatorsNoConnectedSql, false);

//            Dictionary<int, double> supervisorCodesTotalOperators = new Dictionary<int, double>();
//            Dictionary<int, double> supervisorCodesConnectedOperators = new Dictionary<int, double>();
//            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//            List<double> resultsValues = new List<double>();

//            foreach (object[] item in operatorsConnected)
//            {

//                if (supervisorCodesTotalOperators.ContainsKey(int.Parse(item[2].ToString())) == false)
//                {
//                    supervisorCodesTotalOperators.Add(int.Parse(item[2].ToString()), int.Parse(item[0].ToString()));
//                    supervisorCodesConnectedOperators.Add(int.Parse(item[2].ToString()), int.Parse(item[0].ToString()));
//                }
//                else
//                {
//                    supervisorCodesConnectedOperators[int.Parse(item[2].ToString())] += int.Parse(item[0].ToString());
//                    supervisorCodesTotalOperators[int.Parse(item[2].ToString())] += int.Parse(item[0].ToString());
//                }

//            }
//            foreach (object[] item in operatorsNoConnected)
//            {


//                if (supervisorCodesTotalOperators.ContainsKey(int.Parse(item[1].ToString())) == false)
//                {
//                    supervisorCodesTotalOperators.Add(int.Parse(item[1].ToString()), int.Parse(item[0].ToString()));
//                }
//                else
//                {
//                    supervisorCodesTotalOperators[int.Parse(item[1].ToString())] += int.Parse(item[0].ToString());
//                }
//            }

//            foreach (KeyValuePair<int, double> pair in supervisorCodesTotalOperators)
//            {
//                ObjectData od1 = new ObjectData();
//                od1.Code = pair.Key;

//                if (supervisorCodesConnectedOperators.ContainsKey(pair.Key) == true)
//                {
//                    resultsValues.Add(supervisorCodesConnectedOperators[pair.Key]);
//                    resultsValues.Add((supervisorCodesConnectedOperators[pair.Key] / pair.Value) * 100);
//                    result.Add(ResourceLoader.GetString2("IndicatorD22Name"), resultsValues);
//                }
//                else
//                {
//                    resultsValues.Add(0);
//                    resultsValues.Add(0);
//                    result.Add(ResourceLoader.GetString2("IndicatorD22Name"), resultsValues);
//                }

//                SaveIndicatorMultiplesValues(od1, IndicatorClassData.Group, result);
//                result.Clear();
//                resultsValues.Clear();

//                if (supervisorCodesConnectedOperators.ContainsKey(pair.Key) == true)
//                {
//                    double operatorsNoConnectedByGroup = pair.Value - supervisorCodesConnectedOperators[pair.Key];
//                    resultsValues.Add(operatorsNoConnectedByGroup);
//                    resultsValues.Add((operatorsNoConnectedByGroup / pair.Value) * 100);
//                    result.Add(ResourceLoader.GetString2("IndicatorD23Name"), resultsValues);
//                }
//                else
//                {
//                    resultsValues.Add(pair.Value);
//                    resultsValues.Add(100);
//                    result.Add(ResourceLoader.GetString2("IndicatorD23Name"), resultsValues);
//                }
//                SaveIndicatorMultiplesValues(od1, IndicatorClassData.Group, result);
//                result.Clear();
//                resultsValues.Clear();

//                foreach (object[] item in operatorsConnected)
//                {
//                    if (int.Parse(item[2].ToString()) == pair.Key)
//                    {
//                        resultsValues.Add(int.Parse(item[0].ToString()));
//                        resultsValues.Add((int.Parse(item[0].ToString()) * 100) /
//                            supervisorCodesConnectedOperators[int.Parse(item[2].ToString())]);
//                        if (int.Parse(item[1].ToString()) == OperatorStatusData.Absent.Code)
//                        {
//                            result.Add(ResourceLoader.GetString2("IndicatorD35Name"), resultsValues);
//                        }
//                        else if (int.Parse(item[1].ToString()) == OperatorStatusData.Bathroom.Code)
//                        {
//                            result.Add(ResourceLoader.GetString2("IndicatorD38Name"), resultsValues);
//                        }
//                        else if (int.Parse(item[1].ToString()) == OperatorStatusData.Busy.Code)
//                        {
//                            result.Add(ResourceLoader.GetString2("IndicatorD34Name"), resultsValues);
//                        }
//                        else if (int.Parse(item[1].ToString()) == OperatorStatusData.Ready.Code)
//                        {
//                            result.Add(ResourceLoader.GetString2("IndicatorD20Name"), resultsValues);
//                        }
//                        else if (int.Parse(item[1].ToString()) == OperatorStatusData.Rest.Code)
//                        {
//                            result.Add(ResourceLoader.GetString2("IndicatorD37Name"), resultsValues);
//                        }
//                        else if (int.Parse(item[1].ToString()) == OperatorStatusData.Reunion.Code)
//                        {
//                            result.Add(ResourceLoader.GetString2("IndicatorD36Name"), resultsValues);
//                        }
//                        SaveIndicatorMultiplesValues(od1, IndicatorClassData.Group, result);
//                        result.Clear();
//                        resultsValues = new List<double>();
//                    }
//                }
//            }
//            #endregion new all

//            #region new
////            string operatorsConnectedSql = @" SELECT		COUNT(SEH.USER_ACCOUNT_CODE) AS CANTIDAD						
////			,OPE.CODE AS SUPERVISOR
////FROM        USER_APPLICATION AS USA
////			,SESSION_HISTORY AS SEH
////			,SESSION_STATUS_HISTORY AS STH
////			,OPERATOR_STATUS AS OPS
////			,OPERATOR_ASSIGN AS OPA
////			,OPERATOR AS OPE
////WHERE		USA.CODE = SEH.USER_APPLICATION_CODE
////AND			USA.CODE = 3			
////AND			SEH.CODE = STH.SESSION_HISTORY_CODE
////AND			STH.OPERATOR_STATUS_CODE = OPS.CODE
////AND			SEH.END_DATE_LOGIN IS NULL
////AND			STH.END_DATE IS NULL 
////AND         SEH.IS_LOGGED_IN = 1
////AND			SEH.USER_ACCOUNT_CODE = OPA.OPERATOR_CODE
////AND			OPA.DELETED_ID IS NULL
////AND			(
////				(				
////				GETDATE()
////				BETWEEN OPA.START_DATE AND OPA.END_DATE
////				)			
////			)
////AND			OPA.SUPERVISOR_CODE = OPE.CODE
////GROUP BY	OPE.CODE
////order by OPE.CODE
////";

////            string operatorsNoConnectedSql = @" SELECT	count(distinct(OPA.OPERATOR_CODE)) AS CANTIDAD		
////		,OPE2.CODE AS SUPERVISOR
////FROM	OPERATOR_ASSIGN AS OPA
////		,OPERATOR AS OPE
////		,OPERATOR AS OPE2
////		,USER_ROLE AS USR
////		,USER_ROLE_PROFILE AS URP
////		,USER_PROFILE AS USP
////		,USER_PROFILE_ACCESS AS UPA
////		,USER_ACCESS AS USA
////		,USER_APPLICATION AS APP
////		,OPERATOR_DEPARTMENT_TYPE AS ODT
////		,DEPARTMENT_TYPE AS DETY
////WHERE	OPA.DELETED_ID IS NULL
////AND		GETDATE()
////		BETWEEN OPA.START_DATE AND OPA.END_DATE
////AND		OPA.OPERATOR_CODE NOT IN (
////									SELECT	SEH.USER_ACCOUNT_CODE AS OPERATOR
////									FROM	SESSION_HISTORY AS SEH
////									WHERE	SEH.IS_LOGGED_IN = 1
////									)
////AND		OPE.CODE = OPA.OPERATOR_CODE
////AND		OPE2.CODE = OPA.SUPERVISOR_CODE
////AND		USR.CODE = OPE.USER_ROLE_CODE
////AND		URP.USER_ROLE_CODE = USR.CODE
////AND		USP.CODE = URP.USER_PROFILE_CODE
////AND		UPA.USER_PROFILE_CODE = USP.CODE
////AND		USA.CODE = UPA.USER_ACCESS_CODE
////AND		APP.CODE = USA.USER_APPLICATION_CODE
////AND		APP.CODE = 3		
////AND		ODT.OPERATOR_CODE = OPA.OPERATOR_CODE
////AND		DETY.CODE = ODT.DEPARTMENT_TYPE_CODE
////GROUP By OPE2.CODE
////order By OPE2.CODE
////";

////            IList operatorsConnected = (IList)SmartCadDatabase.SearchBasicObjects(operatorsConnectedSql, false);
////            IList operatorsNoConnected = (IList)SmartCadDatabase.SearchBasicObjects(operatorsNoConnectedSql, false);

////            Dictionary<int, double> supervisorCodes = new Dictionary<int, double>();
////            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
////            List<double> list = new List<double>();
////            foreach (object[] item in operatorsConnected)
////            {
////                if (supervisorCodes.ContainsKey(int.Parse(item[1].ToString())) == false)
////                {
////                    supervisorCodes.Add(int.Parse(item[1].ToString()), int.Parse(item[0].ToString()));
////                }

////            }
////            foreach (object[] item in operatorsNoConnected)
////            {

////                if (supervisorCodes.ContainsKey(int.Parse(item[1].ToString())) == false)
////                {
////                    supervisorCodes.Add(int.Parse(item[1].ToString()), int.Parse(item[0].ToString()));
////                }
////                else
////                {
////                    supervisorCodes[int.Parse(item[1].ToString())] += int.Parse(item[0].ToString());
////                }
////            }

////            foreach (KeyValuePair<int, double> pair in supervisorCodes)
////            {

////                foreach (object[] item in operatorsConnected)
////                {
////                    if (int.Parse(item[1].ToString()) == pair.Key)
////                    {
////                        ObjectData od1 = new ObjectData();
////                        od1.Code = int.Parse(item[1].ToString());
////                        list.Add(int.Parse(item[0].ToString()));
////                        list.Add((int.Parse(item[0].ToString()) * 100) / pair.Value);
////                        result.Add("operatorsConnected", list);
////                        SaveIndicatorMultiplesValues(od1, IndicatorClassData.Group, result);
////                        result.Clear();
////                        list = new List<double>();
////                        break;
////                    }
////                }
////            }
//            #endregion new

//            #region doc
//            //IList supervisors = OperatorScheduleManager.GetSupervisorsWorkingNowByApplication("DispatchSupervisorName");

//            //foreach (int supervisor in supervisors)
//            //{
//            //    IList operators = OperatorScheduleManager.GetOperatorsBySupervisor(supervisor);

//            //    double operatorsConnected = 0;
//            //    double percetOperatorsConnected = 0;
//            //    double allOperators = 0;
//            //    foreach (OperatorData operatorData in operators)
//            //    {
//            //        double operatorConnected = Convert.ToDouble(SmartCadDatabase.SearchBasicObject(
//            //            SmartCadHqls.GetCustomHql(
//            //               SmartCadHqls.IsLoggedOperatorInApplication,
//            //               operatorData.Code, true.ToString().ToLower(),
//            //               UserApplicationData.Dispatch.Code)));
//            //        allOperators += 1;
//            //        if (operatorConnected > 0)
//            //            operatorsConnected += 1;
//            //    }

//            //    if (allOperators > 0)
//            //    {
//            //        percetOperatorsConnected = (operatorsConnected * 100) / (allOperators);
//            //    }
//            //    else
//            //    {
//            //        percetOperatorsConnected = 0;
//            //    }
//            //    percetOperatorsConnected = Math.Round(percetOperatorsConnected, 2);
//            //    List<double> resultsValues = new List<double>();
//            //    resultsValues.Add(operatorsConnected);
//            //    resultsValues.Add(percetOperatorsConnected);
//            //    Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//            //    result.Add("ConnectedOperators", resultsValues);
//            //    ObjectData objectdata = new ObjectData();
//            //    objectdata.Code = supervisor;
//            //    SaveIndicatorMultiplesValues(objectdata, IndicatorClassData.Group, result);
//            //}
//            #endregion doc
//        }


//        //ArrayList departmentsAllZeroValue = new ArrayList() { 1, 2, 3, 4, 5, 6, 7, 8 };
//        private void CalculateDepartment()
//        {

//            #region new all
//            string operatorsConnectedSql = @" SELECT		COUNT(SEH.USER_ACCOUNT_CODE) AS CANTIDAD
//			,OPS.CODE AS Estado
//			,DETY.CODE AS ORGANISMO
//FROM        USER_APPLICATION AS USA
//			,SESSION_HISTORY AS SEH
//			,SESSION_STATUS_HISTORY AS STH
//			,OPERATOR_STATUS AS OPS
//			,OPERATOR_DEPARTMENT_TYPE AS ODT
//			,DEPARTMENT_TYPE AS DETY
//WHERE		(USA.CODE = 3)
//AND			USA.CODE = SEH.USER_APPLICATION_CODE
//AND			SEH.CODE = STH.SESSION_HISTORY_CODE
//AND			STH.OPERATOR_STATUS_CODE = OPS.CODE
//AND			SEH.END_DATE_LOGIN IS  NULL
//AND			STH.END_DATE IS  NULL 
//AND         SEH.IS_LOGGED_IN = 1
//AND			SEH.USER_ACCOUNT_CODE = ODT.OPERATOR_CODE
//AND			ODT.DEPARTMENT_TYPE_CODE = DETY.CODE
//GROUP BY	DETY.CODE,OPS.CODE
//Order by DETY.CODE
//";

//            string operatorsNoConnectedSql = @" SELECT	COUNT(OPA.OPERATOR_CODE) AS CANTIDAD
//		--,'No Conectado' AS ESTADOS
//		,DETY.CODE AS ORGANISMO
//FROM	OPERATOR_ASSIGN AS OPA
//		,OPERATOR AS OPE
//		,USER_ROLE AS USR
//		,USER_ROLE_PROFILE AS URP
//		,USER_PROFILE AS USP
//		,USER_PROFILE_ACCESS AS UPA
//		,USER_ACCESS AS USA
//		,USER_APPLICATION AS APP
//		,OPERATOR_DEPARTMENT_TYPE AS ODT
//		,DEPARTMENT_TYPE AS DETY
//WHERE	OPA.DELETED_ID IS NULL
//AND		GETDATE()
//		BETWEEN OPA.START_DATE AND OPA.END_DATE
//AND		OPA.OPERATOR_CODE NOT IN (
//									SELECT	SEH.USER_ACCOUNT_CODE AS OPERATOR
//									FROM	SESSION_HISTORY AS SEH
//									WHERE	SEH.IS_LOGGED_IN = 1
//									)
//AND		OPE.CODE = OPA.OPERATOR_CODE
//AND		USR.CODE = OPE.USER_ROLE_CODE
//AND		URP.USER_ROLE_CODE = USR.CODE
//AND		USP.CODE = URP.USER_PROFILE_CODE
//AND		UPA.USER_PROFILE_CODE = USP.CODE
//AND		USA.CODE = UPA.USER_ACCESS_CODE
//AND		APP.CODE = USA.USER_APPLICATION_CODE
//AND		(APP.CODE = 3)
//AND		ODT.OPERATOR_CODE = OPA.OPERATOR_CODE
//AND		DETY.CODE = ODT.DEPARTMENT_TYPE_CODE
//GROUP BY DETY.CODE
//order by DETY.CODE
//";

//            IList operatorsConnected = (IList)SmartCadDatabase.SearchBasicObjects(operatorsConnectedSql, false);
//            IList operatorsNoConnected = (IList)SmartCadDatabase.SearchBasicObjects(operatorsNoConnectedSql, false);

//            Dictionary<int, double> supervisorCodesTotalOperators = new Dictionary<int, double>();
//            Dictionary<int, double> supervisorCodesConnectedOperators = new Dictionary<int, double>();
//            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//            List<double> resultsValues = new List<double>();

//            foreach (object[] item in operatorsConnected)
//            {

//                if (supervisorCodesTotalOperators.ContainsKey(int.Parse(item[2].ToString())) == false)
//                {
//                    supervisorCodesTotalOperators.Add(int.Parse(item[2].ToString()), int.Parse(item[0].ToString()));
//                    supervisorCodesConnectedOperators.Add(int.Parse(item[2].ToString()), int.Parse(item[0].ToString()));
//                }
//                else
//                {
//                    supervisorCodesConnectedOperators[int.Parse(item[2].ToString())] += int.Parse(item[0].ToString());
//                    supervisorCodesTotalOperators[int.Parse(item[2].ToString())] += int.Parse(item[0].ToString());
//                }

//            }
//            foreach (object[] item in operatorsNoConnected)
//            {


//                if (supervisorCodesTotalOperators.ContainsKey(int.Parse(item[1].ToString())) == false)
//                {
//                    supervisorCodesTotalOperators.Add(int.Parse(item[1].ToString()), int.Parse(item[0].ToString()));
//                }
//                else
//                {
//                    supervisorCodesTotalOperators[int.Parse(item[1].ToString())] += int.Parse(item[0].ToString());
//                }
//            }

//            foreach (KeyValuePair<int, double> pair in supervisorCodesTotalOperators)
//            {
//                ObjectData od1 = new ObjectData();
//                od1.Code = pair.Key;

//                if (supervisorCodesConnectedOperators.ContainsKey(pair.Key) == true)
//                {
//                    resultsValues.Add(supervisorCodesConnectedOperators[pair.Key]);
//                    resultsValues.Add((supervisorCodesConnectedOperators[pair.Key] / pair.Value) * 100);
//                    result.Add(ResourceLoader.GetString2("IndicatorD22Name"), resultsValues);
//                }
//                else
//                {
//                    resultsValues.Add(0);
//                    resultsValues.Add(0);
//                    result.Add(ResourceLoader.GetString2("IndicatorD22Name"), resultsValues);
//                }

//                SaveIndicatorMultiplesValues(od1, IndicatorClassData.Department, result);
//                result.Clear();
//                resultsValues.Clear();

//                if (supervisorCodesConnectedOperators.ContainsKey(pair.Key) == true)
//                {
//                    double operatorsNoConnectedByGroup = pair.Value - supervisorCodesConnectedOperators[pair.Key];
//                    resultsValues.Add(operatorsNoConnectedByGroup);
//                    resultsValues.Add((operatorsNoConnectedByGroup / pair.Value) * 100);
//                    result.Add(ResourceLoader.GetString2("IndicatorD23Name"), resultsValues);
//                }
//                else
//                {
//                    resultsValues.Add(pair.Value);
//                    resultsValues.Add(100);
//                    result.Add(ResourceLoader.GetString2("IndicatorD23Name"), resultsValues);
//                }
//                SaveIndicatorMultiplesValues(od1, IndicatorClassData.Department, result);
//                result.Clear();
//                resultsValues.Clear();

//                foreach (object[] item in operatorsConnected)
//                {
//                    if (int.Parse(item[2].ToString()) == pair.Key)
//                    {
//                        resultsValues.Add(int.Parse(item[0].ToString()));
//                        resultsValues.Add((int.Parse(item[0].ToString()) * 100) /
//                            supervisorCodesConnectedOperators[int.Parse(item[2].ToString())]);
//                        if (int.Parse(item[1].ToString()) == OperatorStatusData.Absent.Code)
//                        {
//                            result.Add(ResourceLoader.GetString2("IndicatorD35Name"), resultsValues);
//                        }
//                        else if (int.Parse(item[1].ToString()) == OperatorStatusData.Bathroom.Code)
//                        {
//                            result.Add(ResourceLoader.GetString2("IndicatorD38Name"), resultsValues);
//                        }
//                        else if (int.Parse(item[1].ToString()) == OperatorStatusData.Busy.Code)
//                        {
//                            result.Add(ResourceLoader.GetString2("IndicatorD34Name"), resultsValues);
//                        }
//                        else if (int.Parse(item[1].ToString()) == OperatorStatusData.Ready.Code)
//                        {
//                            result.Add(ResourceLoader.GetString2("IndicatorD20Name"), resultsValues);
//                        }
//                        else if (int.Parse(item[1].ToString()) == OperatorStatusData.Rest.Code)
//                        {
//                            result.Add(ResourceLoader.GetString2("IndicatorD37Name"), resultsValues);
//                        }
//                        else if (int.Parse(item[1].ToString()) == OperatorStatusData.Reunion.Code)
//                        {
//                            result.Add(ResourceLoader.GetString2("IndicatorD36Name"), resultsValues);
//                        }
//                        SaveIndicatorMultiplesValues(od1, IndicatorClassData.Department, result);
//                        result.Clear();
//                        resultsValues = new List<double>();
//                    }
//                }
//            }
            
//            #endregion new all

//            #region new
////            string operatorsConnectedSql = @" SSELECT		COUNT(SEH.USER_ACCOUNT_CODE) AS CANTIDAD
////			,OPS.CODE AS Estado
////			,DETY.CODE AS ORGANISMO
////FROM        USER_APPLICATION AS USA
////			,SESSION_HISTORY AS SEH
////			,SESSION_STATUS_HISTORY AS STH
////			,OPERATOR_STATUS AS OPS
////			,OPERATOR_DEPARTMENT_TYPE AS ODT
////			,DEPARTMENT_TYPE AS DETY
////WHERE		(USA.CODE = 3)
////AND			USA.CODE = SEH.USER_APPLICATION_CODE
////AND			SEH.CODE = STH.SESSION_HISTORY_CODE
////AND			STH.OPERATOR_STATUS_CODE = OPS.CODE
////AND			SEH.END_DATE_LOGIN IS  NULL
////AND			STH.END_DATE IS  NULL 
////AND         SEH.IS_LOGGED_IN = 1
////AND			SEH.USER_ACCOUNT_CODE = ODT.OPERATOR_CODE
////AND			ODT.DEPARTMENT_TYPE_CODE = DETY.CODE
////GROUP BY	DETY.CODE,OPS.CODE
////Order by DETY.CODE
////";

////            string operatorsNoConnectedSql = @" SELECT	COUNT(OPA.OPERATOR_CODE) AS CANTIDAD
////		--,'No Conectado' AS ESTADOS
////		,DETY.CODE AS ORGANISMO
////FROM	OPERATOR_ASSIGN AS OPA
////		,OPERATOR AS OPE
////		,USER_ROLE AS USR
////		,USER_ROLE_PROFILE AS URP
////		,USER_PROFILE AS USP
////		,USER_PROFILE_ACCESS AS UPA
////		,USER_ACCESS AS USA
////		,USER_APPLICATION AS APP
////		,OPERATOR_DEPARTMENT_TYPE AS ODT
////		,DEPARTMENT_TYPE AS DETY
////WHERE	OPA.DELETED_ID IS NULL
////AND		GETDATE()
////		BETWEEN OPA.START_DATE AND OPA.END_DATE
////AND		OPA.OPERATOR_CODE NOT IN (
////									SELECT	SEH.USER_ACCOUNT_CODE AS OPERATOR
////									FROM	SESSION_HISTORY AS SEH
////									WHERE	SEH.IS_LOGGED_IN = 1
////									)
////AND		OPE.CODE = OPA.OPERATOR_CODE
////AND		USR.CODE = OPE.USER_ROLE_CODE
////AND		URP.USER_ROLE_CODE = USR.CODE
////AND		USP.CODE = URP.USER_PROFILE_CODE
////AND		UPA.USER_PROFILE_CODE = USP.CODE
////AND		USA.CODE = UPA.USER_ACCESS_CODE
////AND		APP.CODE = USA.USER_APPLICATION_CODE
////AND		(APP.CODE = 3)
////AND		ODT.OPERATOR_CODE = OPA.OPERATOR_CODE
////AND		DETY.CODE = ODT.DEPARTMENT_TYPE_CODE
////GROUP BY DETY.CODE
////order by DETY.CODE
////";

////            IList operatorsConnected = (IList)SmartCadDatabase.SearchBasicObjects(operatorsConnectedSql, false);
////            IList operatorsNoConnected = (IList)SmartCadDatabase.SearchBasicObjects(operatorsNoConnectedSql, false);

////            Dictionary<int, double> departmentCodes = new Dictionary<int, double>();
////            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
////            List<double> list = new List<double>();
////            foreach (object[] item in operatorsConnected)
////            {
////                if (departmentCodes.ContainsKey(int.Parse(item[1].ToString())) == false)
////                {
////                    departmentCodes.Add(int.Parse(item[1].ToString()), int.Parse(item[0].ToString()));
////                }

////            }
////            foreach (object[] item in operatorsNoConnected)
////            {

////                if (departmentCodes.ContainsKey(int.Parse(item[1].ToString())) == false)
////                {
////                    departmentCodes.Add(int.Parse(item[1].ToString()), int.Parse(item[0].ToString()));
////                }
////                else
////                {
////                    departmentCodes[int.Parse(item[1].ToString())] += int.Parse(item[0].ToString());
////                }
////            }

////            foreach (KeyValuePair<int, double> pair in departmentCodes)
////            {

////                foreach (object[] item in operatorsConnected)
////                {
////                    if (int.Parse(item[1].ToString()) == pair.Key)
////                    {
////                        ObjectData od1 = new ObjectData();
////                        od1.Code = int.Parse(item[1].ToString());
////                        list.Add(int.Parse(item[0].ToString()));
////                        list.Add((int.Parse(item[0].ToString()) * 100) / pair.Value);
////                        result.Add("operatorsConnected", list);
////                        SaveIndicatorMultiplesValues(od1, IndicatorClassData.Department, result);
////                        result.Clear();
////                        list = new List<double>();
////                        break;
////                    }
////                }
////            }
//            #endregion new

//            #region doc
//            //foreach (int item in departmentsAllZeroValue)
//            //{
//            //    Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//            //    List<double> resultsValues = new List<double>();
//            //    resultsValues.Add(0);
//            //    resultsValues.Add(0);
//            //    result.Add("Empty", resultsValues);
//            //    ObjectData od = new ObjectData();
//            //    od.Code = item;
//            //    SaveIndicatorMultiplesValues(od, IndicatorClassData.Department, result);
//            //}   
           
//            //IList operatorConnected = SmartCadDatabase.SearchObjects(
//            //           SmartCadHqls.GetCustomHql(
//            //              SmartCadHqls.GetOperatorCodeByApp,
//            //              UserApplicationData.Dispatch.Code));
//            //Dictionary<int, int> operatorDepartmenCount = new Dictionary<int, int>();
           
           
            
//            //foreach (int code in operatorConnected)
//            //{
//            //    IList operatorDepartment = SmartCadDatabase.SearchObjects(
//            //               SmartCadHqls.GetCustomHql(
//            //                  SmartCadHqls.GetOperatorDepartmentTypeByOperatorCode,
//            //                  code));
//            //    foreach (DepartmentTypeData department in operatorDepartment)
//            //    {
//            //        if (operatorDepartmenCount.ContainsKey(department.Code))
//            //        {
//            //            int departmentValue=0;
//            //            operatorDepartmenCount.TryGetValue(department.Code, out departmentValue);
//            //            departmentValue += 1;
//            //            operatorDepartmenCount[department.Code] = departmentValue;
//            //        }
//            //        else
//            //        {
//            //            operatorDepartmenCount.Add(department.Code, 1);
//            //        }
//            //    }

//            //}
//            //DateTime now = SmartCadDatabase.GetTimeFromBD();
//            ////DateTime thisTime = ApplicationUtil.SCHEDULE_REFERENCE_DATE;
//            ////thisTime = thisTime.AddHours(now.Hour);
//            ////thisTime = thisTime.AddMinutes(now.Minute);
//            ////thisTime = thisTime.AddSeconds(now.Second);
//            ////thisTime = thisTime.AddMilliseconds(now.Millisecond);
//            //foreach (KeyValuePair<int, int> pair in operatorDepartmenCount)
//            //{
//            //    double operatorsWorkingNowDepartment = 0;
//            //    double percentOperatorsConnected = 0;
//            //    IList operatorsWorkingNowByDepartment = SmartCadDatabase.SearchObjects(
//            //        SmartCadHqls.GetCustomHql(SmartCadHqls.GetSupervisorWorkingNowByDepartment, 
//            //       ApplicationUtil.GetDataBaseFormattedDate(now),pair.Key));
//            //    foreach (OperatorData operatorData in operatorsWorkingNowByDepartment)
//            //    {
//            //        if (OperatorScheduleManager.IsSupervisor(operatorData.Code) == false)
//            //        {
//            //            operatorsWorkingNowDepartment += 1;
//            //        }
//            //    }
//            //    if (operatorsWorkingNowDepartment > 0)
//            //    {
//            //        percentOperatorsConnected = (pair.Value * 100) / operatorsWorkingNowDepartment;
//            //    }
//            //    percentOperatorsConnected = Math.Round(percentOperatorsConnected, 2);
//            //    List<double> resultsValues = new List<double>();
//            //    resultsValues.Add(pair.Value);
//            //    resultsValues.Add(percentOperatorsConnected);
//            //    Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//            //    result.Add("ConnectedOperators", resultsValues);
//            //    ObjectData od = new ObjectData();
//            //    od.Code = pair.Key;
//            //    SaveIndicatorMultiplesValues(od, IndicatorClassData.Department, result);
//            //}
//        #endregion doc        
//        }
//    }
//}
