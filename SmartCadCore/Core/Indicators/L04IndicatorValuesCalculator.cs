using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using System.Collections;

namespace SmartCadCore.Core.Indicators
{
    public class L04IndicatorValuesCalculator : IndicatorValuesCalculator
    {
        protected override string GetIndicator()
        {
            return "L04";
        }

        protected override void Calculate()
        {           
            CalculateSystem();
            CalculateGroup();
            CalculateOperators();
        }

        private void CalculateSystem()
        {
            #region new
            string sql = @"SELECT		CONVERT(FLOAT,ISNULL(SUM(DATEDIFF(SS,PHR.PICKED_UP_CALL_TIME, PHR.FINISHED_REPORT_TIME)),0))
			/CASE COUNT(PHR.FINISHED_REPORT_TIME) WHEN 0 THEN 1 ELSE COUNT(PHR.FINISHED_REPORT_TIME) END AS TIEMPO
FROM		PHONE_REPORT AS PHR
			,REPORT_BASE AS REB
			,REPORT_BASE_INCIDENT_TYPE RBIT
			,INCIDENT_TYPE IT
WHERE		PHR.PARENT_CODE = REB.CODE
AND			CONVERT(DATETIME,CONVERT(CHAR(10),PHR.FINISHED_REPORT_TIME,101),101)= 
			CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
AND			RBIT.INCIDENT_TYPE_CODE = IT.CODE
AND			RBIT.REPORT_BASE_CODE = REB.CODE
AND			IT.NO_EMERGENCY = 'TRUE'";

            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);
            SaveIndicatorsValues(IndicatorClassData.System, double.Parse(data[0].ToString()));

            #endregion new

            #region doc
            //double res = 0;

            //IList phoneCallAvg = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(
            //      SmartCadHqls.GetAllPhoneReportsNotEmergencyCurrentday,
            //      ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today),
            //      true.ToString().ToLower()));

            //if (phoneCallAvg.Count > 0)
            //{
            //    foreach (PhoneReportData prd in phoneCallAvg)
            //    {
            //        TimeSpan ts = prd.FinishedReportTime.Value.Subtract(prd.PickedUpCallTime.Value);
            //        res += ts.TotalSeconds;
            //    }
            //    res /= phoneCallAvg.Count;
            //}
            //SaveIndicatorsValues(IndicatorClassData.System, res);
            #endregion doc
        }

        private void CalculateOperators()
        {
            #region new
            string sql = @" SELECT		CONVERT(FLOAT,ISNULL(SUM(DATEDIFF(SS, PHR.PICKED_UP_CALL_TIME, PHR.FINISHED_REPORT_TIME)),0))
			/ CASE COUNT(PHR.FINISHED_REPORT_TIME) WHEN 0 THEN 1 ELSE COUNT(PHR.FINISHED_REPORT_TIME) END AS TIEMPO
			,OPE.CODE AS OPERATOR
FROM		PHONE_REPORT AS PHR
			,REPORT_BASE AS REB
			,REPORT_BASE_INCIDENT_TYPE RBIT
			,INCIDENT_TYPE IT
			,OPERATOR AS OPE
WHERE		PHR.PARENT_CODE = REB.CODE
AND			CONVERT(DATETIME,CONVERT(CHAR(10),PHR.FINISHED_REPORT_TIME,101),101)= 
			CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
AND			RBIT.INCIDENT_TYPE_CODE = IT.CODE
AND			RBIT.REPORT_BASE_CODE = REB.CODE
AND			IT.NO_EMERGENCY = 'TRUE'
AND         REB.OPERATOR_CODE = OPE.CODE
GROUP BY OPE.CODE
";

            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);

            foreach (object[] item in data)
            {
                ObjectData od1 = new ObjectData();
                od1.Code = int.Parse(item[1].ToString());
                SaveIndicatorsValues(od1, IndicatorClassData.Operator,
                    double.Parse(item[0].ToString()));
            }
            #endregion new

            #region doc
            //IList operators = OperatorScheduleManager.GetOperatorsWorkingNowByApplication(UserApplicationData.FirstLevel.Name, false);
            //foreach (int operatorCode in operators)
            //{
            //    double res = 0;
            //    IList phoneCallAvg = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(
            //      SmartCadHqls.GetOperatorPhoneReportsNotEmergencyCurrentday, 
            //      operatorCode,
            //      ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today),
            //      true.ToString().ToLower()));

            //    if (phoneCallAvg.Count > 0)
            //    {
            //        foreach (PhoneReportData prd in phoneCallAvg)
            //        {
            //            TimeSpan ts = prd.FinishedReportTime.Value.Subtract(prd.PickedUpCallTime.Value);
            //            res += ts.TotalSeconds;
            //        }
            //        res /= phoneCallAvg.Count;
            //    }

            //    ObjectData objectData = new ObjectData();
            //    objectData.Code = operatorCode;
            //    SaveIndicatorsValues(objectData, IndicatorClassData.Operator, res);
            //}
            #endregion doc
        }

        private void CalculateGroup()
        {
            #region new
            string sql = @" SELECT		CONVERT(FLOAT,ISNULL(SUM(DATEDIFF(SS, PHR.PICKED_UP_CALL_TIME, PHR.FINISHED_REPORT_TIME)),0))
			/ CASE COUNT(PHR.FINISHED_REPORT_TIME) WHEN 0 THEN 1 ELSE COUNT(PHR.FINISHED_REPORT_TIME) END AS TIEMPO
			,OPA.SUPERVISOR_CODE AS Supervisor
FROM		PHONE_REPORT AS PHR
			,REPORT_BASE AS REB
			,REPORT_BASE_INCIDENT_TYPE RBIT
			,INCIDENT_TYPE IT
			,OPERATOR AS OPE
			,OPERATOR_ASSIGN AS OPA
WHERE		PHR.PARENT_CODE = REB.CODE
AND			CONVERT(DATETIME,CONVERT(CHAR(10),PHR.FINISHED_REPORT_TIME,101),101)= 
			CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
AND			RBIT.INCIDENT_TYPE_CODE = IT.CODE
AND			RBIT.REPORT_BASE_CODE = REB.CODE
AND			IT.NO_EMERGENCY = 'TRUE'
AND         REB.OPERATOR_CODE = OPE.CODE
AND			OPA.OPERATOR_CODE = ope.code
GROUP BY OPA.SUPERVISOR_CODE
";

            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);
            foreach (object[] item in data)
            {
                ObjectData od1 = new ObjectData();
                od1.Code = int.Parse(item[1].ToString());
                SaveIndicatorsValues(od1, IndicatorClassData.Group,
                    double.Parse(item[0].ToString()));
            }
            #endregion new

            #region doc
            //IList supervisors = OperatorScheduleManager.GetSupervisorsWorkingNowByApplication("FirstLevelSupervisorName");
            //Dictionary<int, double> results = new Dictionary<int, double>();
            //foreach (int supervisorCode in supervisors)
            //{
            //    //IList operators = OperatorScheduleManager.GetOperatorsBySupervisor(supervisorCode);
            //    IList operators = OperatorScheduleManager.GetAllDayOperatorsBySupervisor(supervisorCode);
            //    WorkShiftScheduleVariationData bshSupervisor =
            //       OperatorScheduleManager.GetCurrentWorkShiftSchedulesByOperator(supervisorCode, SmartCadDatabase.GetTimeFromBD().Date);

            //    double total = 0;
            //    double totalAmount = 0;

            //    foreach (OperatorData operatorData in operators)
            //    {

            //        //BaseSessionHistory bsh = 
            //        //    OperatorScheduleManager.GetOperatorAssignBySupervisorAndByDayRealHours(supervisorCode, operatorData.Code);

            //        IList bshList =
            //           OperatorScheduleManager.GetAllDayOperatorAssignBySupervisorAndByDayRealHours(supervisorCode, operatorData.Code);
            //        foreach (OperatorAssignData bsh in bshList)
            //        {
            //            if ((bshSupervisor != null) &&
            //                (((bsh.StartDate.TimeOfDay >= bshSupervisor.Start.Value.TimeOfDay) &&
            //               (bsh.StartDate.TimeOfDay <= bshSupervisor.End.Value.TimeOfDay)) ||
            //               ((bsh.EndDate.TimeOfDay <= bshSupervisor.End.Value.TimeOfDay) &&
            //               (bsh.EndDate.TimeOfDay >= bshSupervisor.Start.Value.TimeOfDay))))
            //            {
            //                IList phoneCallAvg = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(
            //                                       SmartCadHqls.GetOperatorPhoneReportsNotEmergencyCurrentdayCurrentWorkShift,
            //                                       operatorData.Code,
            //                                       ApplicationUtil.GetDataBaseFormattedDate(bsh.StartDate),
            //                                       ApplicationUtil.GetDataBaseFormattedDate(bsh.EndDate),
            //                                       true.ToString().ToLower()));

            //                foreach (PhoneReportData call in phoneCallAvg)
            //                {
            //                    TimeSpan sub = call.FinishedReportTime.Value - call.PickedUpCallTime.Value;
            //                    total += sub.TotalSeconds;
            //                }
            //                totalAmount += phoneCallAvg.Count;
            //            }
            //        }
            //    }

            //    double res = 0;

            //    if (total > 0 && totalAmount > 0)
            //    {
            //        res = total / totalAmount;
            //    }

            //    ObjectData objectData = new ObjectData();
            //    objectData.Code = supervisorCode;
            //    SaveIndicatorsValues(objectData, IndicatorClassData.Group, res);
            //}
            #endregion doc
        }
    }
}
