using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using System.Collections;

namespace SmartCadCore.Core.Indicators
{
    public class D16IndicatorValuesCalculator : IndicatorValuesCalculator
    {
        protected override string GetIndicator()
        {
            return "D16";
        }

        protected override void Calculate()
        {
            CalculateDepartment();
        }

        private void CalculateDepartment()
        {
            #region new
            string sql = @" SELECT		COUNT(A.ESTACION) AS CANTIDAD, DETY.CODE AS ORGANISMO
FROM		(
				SELECT		DEPS.CODE AS ESTACION
							,DEPS.DEPARTMENT_ZONE_CODE AS ZONA
							,INOT.CODE AS INCIDENTE
							,INOT.DEPARTMENT_TYPE_CODE AS ORGANISMO
				FROM		UNIT AS UNIT
							,DEPARTMENT_STATION AS DEPS
							,DISPATCH_ORDER AS DIOR
							,INCIDENT_NOTIFICATION AS INOT
				WHERE		(CONVERT(DATETIME,CONVERT(CHAR(10),INOT.END_DATE,101),101) = 
													CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)OR INOT.END_DATE IS NULL)
				AND			INOT.CODE = DIOR.INCIDENT_NOTIFICATION_CODE
				AND			DIOR.UNIT_CODE = UNIT.CODE
				AND			UNIT.DEPARTMENT_STATION_CODE = DEPS.CODE
			) AS A
			,(
				SELECT		DEPS.CODE AS ESTACION
							,DEPS.DEPARTMENT_ZONE_CODE AS ZONA
							,INOT.CODE AS INCIDENTE
							,INOT.DEPARTMENT_TYPE_CODE AS ORGANISMO
				FROM		DEPARTMENT_STATION AS DEPS
							,INCIDENT_NOTIFICATION AS INOT
				WHERE		(CONVERT(DATETIME,CONVERT(CHAR(10),INOT.END_DATE,101),101) = 
													CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)OR INOT.END_DATE IS NULL)
				AND			INOT.DEPARTMENT_STATION_CODE = DEPS.CODE
            ) AS B 
            ,DEPARTMENT_TYPE AS DETY
WHERE		A.INCIDENTE = B.INCIDENTE 
AND         A.ESTACION <> B.ESTACION
AND			A.ORGANISMO = B.ORGANISMO
AND			A.ORGANISMO = DETY.CODE
GROUP BY	DETY.CODE
";

            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);

            foreach (object[] item in data)
            {
                ObjectData od1 = new ObjectData();
                od1.Code = (int)(item[1]);
                SaveIndicatorsValues(od1, IndicatorClassData.Department,
                    double.Parse(item[0].ToString()));
            }
            #endregion new

            #region doc
            //IList departmentCodes = (IList)SmartCadDatabase.SearchBasicObjects(
            //    SmartCadHqls.GetDepartmentsTypeCodes);
            //foreach (int departmentCode in departmentCodes)
            //{
            //    double amount = (long)SmartCadDatabase.SearchBasicObject(
            //        SmartCadHqls.GetCustomHql(
            //        SmartCadHqls.GetAmountUnitDispatchByDifferentStationGroupByDepartmentType,
            //        departmentCode));

            //    ObjectData od = new ObjectData();
            //    od.Code = departmentCode;
            //    SaveIndicatorsValues(od, IndicatorClassData.Department, amount);
            //}
            #endregion doc
        }
    }
}
