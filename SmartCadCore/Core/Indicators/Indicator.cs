using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Reflection;
using System.Collections;
using SmartCadCore.Core.Indicators;
using SmartCadCore.Statistic;
using SmartCadCore.Common;

namespace SmartCadCore.Core
{
    public class IndicatorEventArgs<T> : EventArgs
    {
        public IndicatorEventArgs(T result, DateTime time)
        {
            this.result = result;
            this.time = time;
        }
        
        private T result;
        private DateTime time;

        /// <summary>
        /// Time of the calculated result.
        /// </summary>
        public DateTime Time
        {
            get { return time; }
        }

        /// <summary>
        /// Result of the indicator in an specific time.
        /// </summary>
        public T Result
        {
            get { return result; }
        }
    }

    public class SmartCadIndicators
    {
        internal static readonly string DataIndicatorAssembly = "SmartCadCore.Core.Indicators";

        private static Assembly dataIndicatorAssembly;
        private static Dictionary<string, InvokeInformation> dataIndicators = new Dictionary<string, InvokeInformation>();
        private static bool isInit = false;
        private static Thread runThread = null;
        private static bool isRunning = false;

        internal static bool IsInit
        {
            get
            {
                return isInit;
            }
        }

        public static void Init()
        {
            try
            {
                if (IsInit == false)
                {
                    if (TelephonyServerStatisticElement.CurrentServerType ==
                        TelephonyServerTypes.Genesys)
                    {
                        StatisticsConfiguration.Load();
                    }
                    dataIndicatorAssembly = Assembly.GetExecutingAssembly();
                    Type[] types = dataIndicatorAssembly.GetTypes();
                    Array.Sort(types, new TypeComparer());
                    foreach (Type type in types)
                    {
                        if (type.IsClass == true
                            && type.IsAbstract == false
                            && type.Namespace == DataIndicatorAssembly
                            && type.BaseType == typeof(IndicatorValuesCalculator))
                        {
                            ConstructorInfo className = type.GetConstructor(new Type[0] { });
                            object classObject = className.Invoke(new object[0] { });

                            InvokeInformation ii = new InvokeInformation(classObject, type.GetMethod("Run", BindingFlags.Instance | BindingFlags.Public));
                            dataIndicators.Add(type.FullName, ii);
                        }
                    }

                    isInit = true;
                }
            }
            catch
            {
            }
        }

        public class TypeComparer : IComparer
        {
            #region IComparer Members

            public int Compare(object x, object y)
            {
                if (x is Type && y is Type)
                {
                    Type xVar = (Type)x;
                    Type yVar = (Type)y;
                    return xVar.Name.CompareTo(yVar.Name);
                }
                else if ((x is Type) == false)
                {
                    return -1;
                }
                else if ((y is Type) == false)
                {
                    return 1;
                }
                return 0;
            }

            #endregion
        }

        public static void Start()
        {
            if (SmartCadConfiguration.SmartCadSection.ServerElement.IndicatorsElement.SleepTime != -1)
            {
                runThread = new Thread(new ThreadStart(Run));
                runThread.Start();
            }
        }

        public static void Stop()
        {
            isRunning = false;
            IndicatorValuesCalculator.Stop();
        }

        private static void Run()
        {
            if (IsInit == false)
                Init();

            isRunning = true;

            while (isRunning == true)
            {
                ArrayList keys = new ArrayList(dataIndicators.Keys);
                DateTime STARTCYCLE = DateTime.Now;
                for (int i = 0; i < keys.Count && isRunning == true; i++)
                {
                    try
                    {
                        DateTime START = DateTime.Now;

                        string key = keys[i] as string;
                        InvokeInformation ii = dataIndicators[key];
                        ii.MethodInfo.Invoke(ii.ClassObject, null);
                        Thread.Sleep(SmartCadConfiguration.SmartCadSection.ServerElement.IndicatorsElement.SleepTime);
                        dataIndicators[key] = ii;
//#if DEBUG
//                        Console.WriteLine("Calculate: " + DateTime.Now.Subtract(TimeSpan.FromSeconds(2)).Subtract(START).TotalSeconds + " Indicator: " + key + "\n\n");
//#endif
                    }
                    catch (Exception ex)
                    {
                        SmartLogger.Print(ex);
                    }
                }
//#if DEBUG
//                Console.WriteLine("Calculate Indiators: " + DateTime.Now.Subtract(STARTCYCLE).TotalMinutes + "\n");
//#endif
            }
        }
    }
}
