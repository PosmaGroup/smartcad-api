using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using System.Collections;

namespace SmartCadCore.Core.Indicators
{
    public class PN14IndicatorValuesCalculator : IndicatorValuesCalculator
    {
        protected override string GetIndicator()
        {
            return "PN14";
        }

        protected override void Calculate()
        {           
            CalculateGroup();
            CalculateOperators();
        }

        private void CalculateOperators()
        {
            #region new
            string sql = @" SELECT		CONVERT(FLOAT,SUM(DATEDIFF(SS,PHR.RECEIVED_CALL_TIME,PHR.HANGED_UP_CALL_TIME)))
			/COUNT(PHR.RECEIVED_CALL_TIME) AS TIEMPO
			,OPE.CODE AS OPERADOR
FROM		PHONE_REPORT AS PHR
			,REPORT_BASE AS REB
			,OPERATOR AS OPE
WHERE		REB.CODE = PHR.PARENT_CODE
AND			CONVERT(DATETIME,CONVERT(CHAR(10),PHR.FINISHED_REPORT_TIME,101),101) = 
			CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
AND			OPE.CODE = REB.OPERATOR_CODE
GROUP BY	OPE.CODE
";

            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);

            foreach (object[] item in data)
            {
                ObjectData od1 = new ObjectData();
                od1.Code = int.Parse(item[1].ToString());
                SaveIndicatorsValues(od1, IndicatorClassData.Operator,
                    double.Parse(item[0].ToString()));
            }
            #endregion new


            #region doc
            //IList operators = OperatorScheduleManager.GetOperatorsWorkingNowByApplication(UserApplicationData.FirstLevel.Name, false);


            //foreach (int operatorCode in operators)
            //{

            //    double total = 0;
            //    double totalAmount = 0;

            //    IList phoneCallAvg = SmartCadDatabase.SearchObjects(
            //        SmartCadHqls.GetCustomHql(
            //        SmartCadHqls.GetOperatorPhoneReportsFinishedCurrentWorkShift,
            //        operatorCode,
            //         ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today),
            //         ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today.AddDays(1))));


            //    foreach (PhoneReportData prd in phoneCallAvg)
            //    {
            //        TimeSpan sub = prd.HangedUpCallTime.Value.Subtract(prd.ReceivedCallTime.Value);
            //        total += sub.TotalSeconds;
            //    }
            //    totalAmount += phoneCallAvg.Count;




            //    double res = 0;

            //    if (total > 0 && totalAmount > 0)
            //    {
            //        res = total / totalAmount;
            //    }

            //    ObjectData objectData = new ObjectData();
            //    objectData.Code = operatorCode;
            //    SaveIndicatorsValues(objectData, IndicatorClassData.Operator, res);
            //}
            #endregion doc

        }

        private void CalculateGroup()
        {
            #region new
            string sql = @" SELECT		CONVERT(FLOAT,SUM(DATEDIFF(SS,PHR.RECEIVED_CALL_TIME,PHR.HANGED_UP_CALL_TIME)))
			/COUNT(PHR.RECEIVED_CALL_TIME) AS TIEMPO
			,OPE.CODE AS OPERADOR
FROM		PHONE_REPORT AS PHR
			,REPORT_BASE AS REB
			,OPERATOR_ASSIGN AS OPA
			,OPERATOR AS OPE
WHERE		REB.CODE = PHR.PARENT_CODE
AND			CONVERT(DATETIME,CONVERT(CHAR(10),PHR.FINISHED_REPORT_TIME,101),101) = 
			CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
AND			REB.OPERATOR_CODE = OPA.OPERATOR_CODE
AND			OPA.DELETED_ID IS NULL
AND			(
				(				
				PHR.FINISHED_REPORT_TIME
				BETWEEN OPA.START_DATE AND OPA.END_DATE
				)			
			)
AND			OPA.SUPERVISOR_CODE = OPE.CODE
GROUP BY	OPE.CODE
";

            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);

            foreach (object[] item in data)
            {
                ObjectData od1 = new ObjectData();
                od1.Code = int.Parse(item[1].ToString());
                SaveIndicatorsValues(od1, IndicatorClassData.Group,
                    double.Parse(item[0].ToString()));
            }
            #endregion new

            #region doc
            //IList supervisors = OperatorScheduleManager.GetSupervisorsWorkingNowByApplication("FirstLevelSupervisorName");
            //Dictionary<int, double> results = new Dictionary<int, double>();
            //foreach (int supervisorCode in supervisors)
            //{
            //    IList operators = OperatorScheduleManager.GetAllDayOperatorsBySupervisor(supervisorCode);
            //    WorkShiftScheduleVariationData bshSupervisor =
            //      OperatorScheduleManager.GetCurrentWorkShiftSchedulesByOperator(supervisorCode, SmartCadDatabase.GetTimeFromBD().Date);


            //    double total = 0;
            //    double totalAmount = 0;

            //    foreach (OperatorData operatorData in operators)
            //    {

            //        //BaseSessionHistory bsh = OperatorScheduleManager.GetOperatorAssignBySupervisorAndByDayRealHours(supervisorCode, operatorData.Code);
            //        IList bshList =
            //          OperatorScheduleManager.GetAllDayOperatorAssignBySupervisorAndByDayRealHours(supervisorCode, operatorData.Code);
            //        foreach (OperatorAssignData bsh in bshList)
            //        {
            //            if ((bshSupervisor != null) &&
            //                 (((bsh.StartDate.TimeOfDay >= bshSupervisor.Start.Value.TimeOfDay) &&
            //                (bsh.StartDate.TimeOfDay <= bshSupervisor.End.Value.TimeOfDay)) ||
            //                ((bsh.EndDate.TimeOfDay <= bshSupervisor.End.Value.TimeOfDay) &&
            //                (bsh.EndDate.TimeOfDay >= bshSupervisor.Start.Value.TimeOfDay))))
            //            {


            //                IList phoneCallAvg = SmartCadDatabase.SearchObjects(
            //                    SmartCadHqls.GetCustomHql(
            //                    SmartCadHqls.GetOperatorPhoneReportsFinishedCurrentWorkShift,
            //                    operatorData.Code,
            //                    ApplicationUtil.GetDataBaseFormattedDate(bsh.StartDate),
            //                    ApplicationUtil.GetDataBaseFormattedDate(bsh.EndDate)));


            //                foreach (PhoneReportData prd in phoneCallAvg)
            //                {
            //                    TimeSpan sub = prd.HangedUpCallTime.Value.Subtract(prd.ReceivedCallTime.Value);
            //                    total += sub.TotalSeconds;
            //                }
            //                totalAmount += phoneCallAvg.Count;
            //            }
            //        }
            //    }

            //    double res = 0;

            //    if (total > 0 && totalAmount > 0)
            //    {
            //        res = total / totalAmount;
            //    }

            //    ObjectData objectData = new ObjectData();
            //    objectData.Code = supervisorCode;
            //    SaveIndicatorsValues(objectData, IndicatorClassData.Group, res);
            //}
            #endregion doc
        }
    }
}
