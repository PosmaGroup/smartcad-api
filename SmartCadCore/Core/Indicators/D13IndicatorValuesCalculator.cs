using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using System.Collections;

namespace SmartCadCore.Core.Indicators
{
    public class D13IndicatorValuesCalculator : IndicatorValuesCalculator
    {
        protected override string GetIndicator()
        {
            return "D13";
        }

        protected override void Calculate()
        {
            CalculateSystem();
            CalculateDepartment();
        }

        private void CalculateSystem()
        {
            #region new
            string sql = @" SELECT		COUNT(INOT.CODE) AS CANTIDAD
FROM		INCIDENT_NOTIFICATION AS INOT
WHERE		CONVERT(DATETIME,CONVERT(CHAR(10),INOT.END_DATE,101),101) = 
			CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)";

            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);
            SaveIndicatorsValues(IndicatorClassData.System, double.Parse(data[0].ToString()));
          
            #endregion new

            #region doc
            //double amount = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(SmartCadHqls.GetAmountIncidentNotificationEndedGroup, ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today)));
            //SaveIndicatorsValues(IndicatorClassData.System, amount);
            #endregion doc
        }

        private void CalculateDepartment()
        {
            #region new
            string sql = @" SELECT		COUNT(INOT.CODE) AS CANTIDAD, DEPT.CODE AS ORGANISMO
FROM		INCIDENT_NOTIFICATION AS INOT
			,DEPARTMENT_TYPE AS DEPT
WHERE		CONVERT(DATETIME,CONVERT(CHAR(10),INOT.END_DATE,101),101) = 
			CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
AND			INOT.DEPARTMENT_TYPE_CODE = DEPT.CODE
GROUP BY	DEPT.CODE
order by DEPT.CODE
";


            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);

            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
            List<double> list = new List<double>();
            
            foreach (object[] item in data)
            {
                ObjectData od1 = new ObjectData();
                od1.Code = (int)item[1];
                SaveIndicatorsValues(od1, IndicatorClassData.Department, double.Parse(item[0].ToString()));
            }
            #endregion new

            #region doc
            //IList departmentCodes = (IList)SmartCadDatabase.SearchBasicObjects(
            //    SmartCadHqls.GetDepartmentsTypeCodes);
            //foreach (int departmentCode in departmentCodes)
            //{
            //    double amount = (long)SmartCadDatabase.SearchBasicObject(
            //        SmartCadHqls.GetCustomHql(
            //        SmartCadHqls.GetAmountIncidentNotificationEndedGroupByDepartmentType, 
            //        ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today),
            //        departmentCode));

            //    ObjectData od = new ObjectData();
            //    od.Code = departmentCode;
            //    SaveIndicatorsValues(od, IndicatorClassData.Department, amount);
            //}
            #endregion doc
        }
    }
}
