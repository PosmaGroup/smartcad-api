using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using System.Collections;

namespace SmartCadCore.Core.Indicators
{
    public class D27IndicatorValuesCalculator : IndicatorValuesCalculator
    {
        protected override string GetIndicator()
        {
            return "D27";
        }

        protected override void Calculate()
        {
            CalculateDepartment();
        }

        private void CalculateDepartment()
        {

            #region new
            string sql = @"SELECT SUM(DATEDIFF(SS,ASIGNADO.FECHA_ASIG,ESCENA.FECHA_ESC))/COUNT(ESCENA.FECHA_ESC) AS TIEMPO, 
DEPARTMENT_STATION.NAME ESTACION,

(select DEPARTMENT_TYPE_CODE as DEP_TYPE_CODE_BY_ZONE
from DEPARTMENT_ZONE
where DEPARTMENT_ZONE.CODE = DEPARTMENT_STATION.DEPARTMENT_ZONE_CODE
)as DEP_TYPE_CODE

FROM
(	SELECT		UNIT_CODE AS CODIGO_ESC,START_DATE AS FECHA_ESC
	FROM		UNIT_STATUS_HISTORY
	WHERE		CONVERT(DATETIME,CONVERT(CHAR(10),START_DATE,101),101) = CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
	AND			UNIT_STATUS_HISTORY.UNIT_STATUS_CODE=4
)	AS ESCENA,




(
	SELECT		UNIT_CODE AS CODIGO_ASIG,START_DATE AS FECHA_ASIG
	FROM		UNIT_STATUS_HISTORY
	WHERE		UNIT_STATUS_HISTORY.UNIT_STATUS_CODE=3
)	AS ASIGNADO,
UNIT,
DEPARTMENT_STATION
WHERE			ESCENA.CODIGO_ESC=ASIGNADO.CODIGO_ASIG 
AND				ESCENA.CODIGO_ESC=UNIT.CODE 
AND				UNIT.DEPARTMENT_STATION_CODE=DEPARTMENT_STATION.CODE
GROUP BY		 DEPARTMENT_STATION.DEPARTMENT_ZONE_CODE, DEPARTMENT_STATION.NAME
order by DEP_TYPE_CODE";

            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);
            Dictionary<string, double> result = new Dictionary<string, double>();
            int departmentCode = 0;
            foreach (object[] item in data)
            {              
                departmentCode = (int)item[2];
                string station = item[1].ToString();
                double time = double.Parse(item[0].ToString());
                result.Add(station, time);
                ObjectData od1 = new ObjectData();
                od1.Code = departmentCode;
                SaveIndicatorMultiplesValues(od1, IndicatorClassData.Department, result);
                result.Clear();                
            }
            #endregion new

            #region doc


            //IList departmentCodes = (IList)SmartCadDatabase.SearchBasicObjects(
            //    SmartCadHqls.GetDepartmentsTypeCodes);
            //foreach (int departmentCode in departmentCodes)
            //{
            //    IList departmentStation = (IList)SmartCadDatabase.SearchBasicObjects(
            //        SmartCadHqls.GetCustomHql(
            //        SmartCadHqls.GetDepartmentStationCodeByDepartmentType,
            //        departmentCode));

            //    foreach (DepartmentStationData ds in departmentStation)
            //    {
            //        IList list = (IList)SmartCadDatabase.SearchBasicObjects(
            //            SmartCadHqls.GetCustomHql(
            //            SmartCadHqls.GetDispatchOrderArrivalGroupByDepartmentTypeDepartmentStation,
            //            ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today),
            //            ds.Code));
            //        Dictionary<string, double> result = new Dictionary<string, double>();
            //        if (list.Count > 0)
            //        {
            //            double average = 0;
            //            foreach (object[] tempClass in list)
            //            {
            //                DateTime arrival = (DateTime)tempClass[0];
            //                DateTime start = (DateTime)tempClass[1];
            //                TimeSpan substract = arrival.Subtract(start);
            //                average += substract.TotalSeconds;
            //            }

            //            if (average > 0)
            //            {
            //                average /= list.Count;
            //            }
            //            result.Add(ds.Name, average);
            //            ObjectData od1 = new ObjectData();
            //            od1.Code = departmentCode;
            //            SaveIndicatorMultiplesValues(od1, IndicatorClassData.Department, result);
            //        }
                //}
            //}
            #endregion doc
        }
    }
}
