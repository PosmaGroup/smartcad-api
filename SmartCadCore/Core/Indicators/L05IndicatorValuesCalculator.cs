﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.Model;
using SmartCadCore.Statistic;
using SmartCadCore.Core.Util;

namespace SmartCadCore.Core.Indicators
{
    public class L05IndicatorValuesCalculator : IndicatorValuesCalculator
    {
        protected override string GetIndicator()
        {
            return "L05";
        }

        protected override void Calculate()
        {
            try
            {
                if (TelephonyServerStatisticElement.CurrentServerType == TelephonyServerTypes.Genesys)
                {
                    double value = GenesysStatWrapper.GetData(StatisticRetriever.StatisticNames.L05CurrentCallsInQueue);
                    SaveIndicatorsValues(IndicatorClassData.System, value);
                }
                else if (TelephonyServerStatisticElement.CurrentServerType == TelephonyServerTypes.Nortel)
                {
                    //Nortel
                    SaveIndicatorsValues(IndicatorClassData.System, NortelRTDWrapper.GetData((int)NRTD_Values.NIrtd_APPL_CALLS_WAITING));
                }
                else if (TelephonyServerStatisticElement.CurrentServerType == TelephonyServerTypes.Avaya)
                {
                    
                    SaveIndicatorsValues(IndicatorClassData.System,
                        AvayaStatWrapper.GetData(StatisticRetriever.StatisticNames.L05CurrentCallsInQueue));
                }
                else if (TelephonyServerStatisticElement.CurrentServerType == TelephonyServerTypes.Asterisk)
                {
                    AsteriskStatWrapper AsteriskWrapper = new AsteriskStatWrapper();
                    SaveIndicatorsValues(IndicatorClassData.System,
                        AsteriskStatWrapper.GetData(StatisticRetriever.StatisticNames.L05CurrentCallsInQueue));
                }
            }
            catch (Exception ex)
            {
            }            
        }
    }
}
