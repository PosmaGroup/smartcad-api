using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using System.Collections;
using SmartCadCore.Common;

namespace SmartCadCore.Core.Indicators
{
    public class PND25IndicatorValuesCalculator : IndicatorValuesCalculator
    {

        private enum EMERGENCYTYPE
        {
            FINISH,
            NOFINISH,
            NOEMERGENCY,
            EMERGENCY
        }

        protected override string GetIndicator()
        {
            return "PND25";
        }

        protected override void Calculate()
        {
            CalculateSystemIndicators();
            CalculateGroup();
        }
        private void CalculateSystemIndicators()
        {
            #region new
            string emergencySql = @"SELECT		COUNT(REB.CODE) AS CANTIDAD
			,CASE	PHR.INCOMPLETE
					WHEN 0 THEN 'FINISH'
					WHEN 1 THEN 'NOFINISH'
			END AS ESTADO			
FROM		PHONE_REPORT AS PHR
			,REPORT_BASE AS REB
			,INCIDENT AS INC
WHERE		CONVERT(DATETIME,CONVERT(CHAR(10),PHR.PICKED_UP_CALL_TIME,101),101)= 
			CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
AND			PHR.PARENT_CODE = REB.CODE
AND			REB.INCIDENT_CODE = INC.CODE
AND         INC.IS_EMERGENCY = '1'
GROUP BY	PHR.INCOMPLETE ,INC.IS_EMERGENCY";

            string noEmergencySql = @"SELECT		COUNT(REB.CODE) AS CANTIDAD
			,CASE	PHR.INCOMPLETE
					WHEN 0 THEN 'FINISH'
					WHEN 1 THEN 'NOFINISH'
			END AS ESTADO
			,CASE	INC.IS_EMERGENCY
					WHEN 0 THEN 'NOEMERGENCY'
					WHEN 1 THEN 'EMERGENCY'
			END AS EMERGENCIA
FROM		PHONE_REPORT AS PHR
			,REPORT_BASE AS REB
			,REPORT_BASE_INCIDENT_TYPE REBIT
			,INCIDENT AS INC
			,INCIDENT_TYPE AS INCT
WHERE		CONVERT(DATETIME,CONVERT(CHAR(10),PHR.PICKED_UP_CALL_TIME,101),101)= 
			CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
AND			PHR.PARENT_CODE = REB.CODE
AND			REB.INCIDENT_CODE = INC.CODE
AND			REB.CODE = REBIT.REPORT_BASE_CODE
AND			REBIT.INCIDENT_TYPE_CODE = INCT.CODE
AND			INCT.NO_EMERGENCY = 'TRUE'
GROUP BY	PHR.INCOMPLETE ,INC.IS_EMERGENCY";
            //IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);

            //Finish , NoFinish
            IList emergency = (IList)SmartCadDatabase.SearchBasicObjects(emergencySql, false);
            
            //No Emergency Finish, noFinish Asociated To emercgency Calls Fisnish, noFInish
            IList noEmergency = (IList)SmartCadDatabase.SearchBasicObjects(noEmergencySql, false);



            double totalIncidentes = 0;
            double totalEmergencyIncidentes = 0;
            double totalNoEmergencyIncidentes = 0;
            double finishEmergencyCall = 0;
            double NofinishEmergencyCall = 0;
            List<double> resultsValues = new List<double>();
            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();

            foreach (object[] callNoEmergency in noEmergency)
            {
                totalNoEmergencyIncidentes += (int)callNoEmergency[0];
                if (callNoEmergency[2].ToString() == EMERGENCYTYPE.EMERGENCY.ToString())
                {
                    foreach (object[] callEmergency in emergency)
                    {
                        if (callEmergency[1].ToString() == callNoEmergency[1].ToString())
                        {
                            callEmergency[0] = (int)callEmergency[0] - (int)callNoEmergency[0];
                            break;
                        }
                    }
                
                }
            
            }

            foreach (object[] callEmergency in emergency)
            {
                totalEmergencyIncidentes += (int)callEmergency[0];
                if (callEmergency[1].ToString() == EMERGENCYTYPE.FINISH.ToString())
                {
                    finishEmergencyCall += (int)callEmergency[0];
                }
                else
                {
                    NofinishEmergencyCall += (int)callEmergency[0];
                }


            }

            totalIncidentes = totalNoEmergencyIncidentes + totalEmergencyIncidentes;


            SaveValues(totalIncidentes, totalEmergencyIncidentes, resultsValues, result, 
                ResourceLoader.GetString2("IndicatorPND22Name"), IndicatorClassData.System, -1);
            SaveValues(totalEmergencyIncidentes, finishEmergencyCall, resultsValues, result,
                ResourceLoader.GetString2("IndicatorL08Name"), IndicatorClassData.System, -1);
            SaveValues(totalEmergencyIncidentes, NofinishEmergencyCall, resultsValues, result,
                ResourceLoader.GetString2("IndicatorL13Name"), IndicatorClassData.System, -1);
            SaveValues(totalIncidentes, totalNoEmergencyIncidentes, resultsValues, result,
                ResourceLoader.GetString2("IndicatorPND23Name"), IndicatorClassData.System, -1);                                    
            #endregion new
            
        }

        private void SaveValues(double total, double amount,
            List<double> resultsValues, Dictionary<string, List<double>> result, string resource, IndicatorClassData indicatorClass, int code)
        {
            resultsValues.Add(amount);
            if (total > 0)
            {
                resultsValues.Add(Math.Round((amount * 100) / total, 2));
            }
            else
            {
                resultsValues.Add(0);
            }
            result.Add(resource, resultsValues);
            if (code < 0)
            {
                SaveIndicatorMultiplesValues(null, indicatorClass, result);
            }
            else
            {
                ObjectData obj = new ObjectData();
                obj.Code = code;
                SaveIndicatorMultiplesValues(obj, indicatorClass, result);
            }
            resultsValues.Clear();
            result.Clear();
        }

        private void CalculateGroup()
        {
            #region new          
            string emergencySql = @" SELECT		COUNT(REB.CODE) AS CANTIDAD
			,CASE	PHR.INCOMPLETE
					WHEN 0 THEN 'FINISH'
					WHEN 1 THEN 'NOFINISH'
			END AS ESTADO		
			,OPE.CODE AS SUPERVISOR
FROM		PHONE_REPORT AS PHR
			,REPORT_BASE AS REB
			,INCIDENT AS INC
			,OPERATOR_ASSIGN AS OPA
			,OPERATOR AS OPE
WHERE		CONVERT(DATETIME,CONVERT(CHAR(10),PHR.PICKED_UP_CALL_TIME,101),101)= 
			CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
AND			PHR.PARENT_CODE = REB.CODE
AND			REB.INCIDENT_CODE = INC.CODE
AND			REB.OPERATOR_CODE = OPA.OPERATOR_CODE
AND			OPA.DELETED_ID IS NULL
AND			(
				(				
				PHR.PICKED_UP_CALL_TIME
				BETWEEN OPA.START_DATE AND OPA.END_DATE
				)		
			)
AND			OPA.SUPERVISOR_CODE = OPE.CODE
AND         INC.IS_EMERGENCY = '1'
GROUP BY	PHR.INCOMPLETE ,INC.IS_EMERGENCY, OPE.CODE
order by OPE.CODE
";

            string noEmergencySql = @" SELECT		COUNT(REB.CODE) AS CANTIDAD
			,CASE	PHR.INCOMPLETE
					WHEN 0 THEN 'FINISH'
					WHEN 1 THEN 'NOFINISH'
			END AS ESTADO
			,CASE	INC.IS_EMERGENCY
						WHEN 0 THEN 'NOEMERGENCY'
					WHEN 1 THEN 'EMERGENCY'
			END AS EMERGENCIA
			,OPE.CODE AS SUPERVISOR
FROM		PHONE_REPORT AS PHR
			,REPORT_BASE AS REB
			,INCIDENT AS INC
			,OPERATOR_ASSIGN AS OPA
			,OPERATOR AS OPE			
			,REPORT_BASE_INCIDENT_TYPE REBIT			
			,INCIDENT_TYPE AS INCT
WHERE		CONVERT(DATETIME,CONVERT(CHAR(10),PHR.PICKED_UP_CALL_TIME,101),101)= 
			CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
AND			PHR.PARENT_CODE = REB.CODE
AND			REB.INCIDENT_CODE = INC.CODE
AND			REB.OPERATOR_CODE = OPA.OPERATOR_CODE
AND			OPA.DELETED_ID IS NULL
AND			(
				(				
				PHR.PICKED_UP_CALL_TIME
				BETWEEN OPA.START_DATE AND OPA.END_DATE
				)		
			)
AND			OPA.SUPERVISOR_CODE = OPE.CODE
AND			REB.CODE = REBIT.REPORT_BASE_CODE
AND			REBIT.INCIDENT_TYPE_CODE = INCT.CODE
AND			INCT.NO_EMERGENCY = 'TRUE'
GROUP BY	PHR.INCOMPLETE ,INC.IS_EMERGENCY, OPE.CODE
order by OPE.CODE
";


            //Finish , NoFinish
            IList emergency = (IList)SmartCadDatabase.SearchBasicObjects(emergencySql, false);

            //No Emergency Finish, noFinish Asociated To emercgency Calls Fisnish, noFInish
            IList noEmergency = (IList)SmartCadDatabase.SearchBasicObjects(noEmergencySql, false);



  
            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
              List<double> resultsValues = new List<double>();
            //department, type, amount
            Dictionary<int, Dictionary<string, int>> groupsCodeWithValues =
                new Dictionary<int, Dictionary<string, int>>();



            foreach (object[] item in emergency)
            {
                if (groupsCodeWithValues.ContainsKey(int.Parse(item[2].ToString())) == false)
                {
                    Dictionary<string, int> values = new Dictionary<string, int>();
                    values.Add(item[1].ToString(), int.Parse(item[0].ToString()));
                    groupsCodeWithValues.Add(int.Parse(item[2].ToString()), values);
                }
                else
                {
                    groupsCodeWithValues[int.Parse(item[2].ToString())].Add(item[1].ToString(), int.Parse(item[0].ToString()));
                }
            }

           
            foreach (object[] item in noEmergency)
            {
                int supervisorCode = 0;
               
                if (groupsCodeWithValues.ContainsKey(int.Parse(item[3].ToString())) == false)
                {
                    supervisorCode = int.Parse(item[3].ToString());
                    Dictionary<string, int> values = new Dictionary<string, int>();
                    values.Add(item[2].ToString(), int.Parse(item[0].ToString()));
                    groupsCodeWithValues.Add(int.Parse(item[3].ToString()), values);
                }
                else
                {
                    if ((supervisorCode != 0) && (supervisorCode == int.Parse(item[3].ToString())))
                    {

                        groupsCodeWithValues[int.Parse(item[3].ToString())][item[2].ToString()] += int.Parse(item[0].ToString());
                    }
                    else
                    {
                        supervisorCode = 0;


                        if (item[2].ToString() == EMERGENCYTYPE.EMERGENCY.ToString())
                        {
                            groupsCodeWithValues[int.Parse(item[3].ToString())][item[1].ToString()] -= int.Parse(item[0].ToString());
                        }
                       
                        if (groupsCodeWithValues[int.Parse(item[3].ToString())].ContainsKey(EMERGENCYTYPE.NOEMERGENCY.ToString()) == false)
                        {
                            groupsCodeWithValues[int.Parse(item[3].ToString())].Add(EMERGENCYTYPE.NOEMERGENCY.ToString(), int.Parse(item[0].ToString()));
                        }
                        else
                        {
                            groupsCodeWithValues[int.Parse(item[3].ToString())][EMERGENCYTYPE.NOEMERGENCY.ToString()] += int.Parse(item[0].ToString());
                        }
                         
                        
                    }
                }
            }

           
            foreach (KeyValuePair<int,Dictionary<string, int>> group in groupsCodeWithValues)
            {
                double totalIncidentes = 0;
                double totalEmergencyIncidentes = 0;
                double totalNoEmergencyIncidentes = 0;
                double finishEmergencyCall = 0;
                double NofinishEmergencyCall = 0;

                foreach (KeyValuePair<string, int> valuePair in group.Value)
                {

                    if (valuePair.Key == EMERGENCYTYPE.FINISH.ToString())
                    {
                        finishEmergencyCall = valuePair.Value;
                    }
                    else if (valuePair.Key == EMERGENCYTYPE.NOFINISH.ToString())
                    {
                        NofinishEmergencyCall = valuePair.Value;
                    }
                    else if (valuePair.Key == EMERGENCYTYPE.NOEMERGENCY.ToString())
                    {
                        totalNoEmergencyIncidentes = valuePair.Value;
                    }

                    totalEmergencyIncidentes = finishEmergencyCall + NofinishEmergencyCall;
                    totalIncidentes = totalNoEmergencyIncidentes + totalEmergencyIncidentes;
                }


                SaveValues(totalIncidentes, totalEmergencyIncidentes, resultsValues, result,
              ResourceLoader.GetString2("IndicatorPND22Name"), IndicatorClassData.Group, group.Key);
                SaveValues(totalEmergencyIncidentes, finishEmergencyCall, resultsValues, result,
                    ResourceLoader.GetString2("IndicatorL08Name"), IndicatorClassData.Group, group.Key);
                SaveValues(totalEmergencyIncidentes, NofinishEmergencyCall, resultsValues, result,
                    ResourceLoader.GetString2("IndicatorL13Name"), IndicatorClassData.Group, group.Key);
                SaveValues(totalIncidentes, totalNoEmergencyIncidentes, resultsValues, result,
                    ResourceLoader.GetString2("IndicatorPND23Name"), IndicatorClassData.Group, group.Key);
            
            }

            #endregion new
            
        }       
     
    }
}

