using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using System.Collections;

namespace SmartCadCore.Core.Indicators
{
    public class PN12IndicatorValuesCalculator : IndicatorValuesCalculator
    {
        protected override string GetIndicator()
        {
            return "PN12";
        }

        protected override void Calculate()
        {            
            CalculateOperators();
            CalculateGroup();
        }

        private void CalculateOperators()
        {
            #region new
            string sql = @" SELECT		CONVERT(FLOAT,ISNULL(SUM(DATEDIFF(SS,PHR.PICKED_UP_CALL_TIME, PHR.REGISTERED_CALL_TIME)),0))
			/CASE COUNT(PHR.REGISTERED_CALL_TIME) WHEN 0 THEN 1 ELSE COUNT(PHR.REGISTERED_CALL_TIME) END AS TIEMPO
			,OPE.CODE AS USUARIO
FROM		PHONE_REPORT AS PHR
			,REPORT_BASE AS REB
			,OPERATOR AS OPE
WHERE		CONVERT(DATETIME,CONVERT(CHAR(10),PHR.REGISTERED_CALL_TIME,101),101) = 
			CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
AND			PHR.PARENT_CODE = REB.CODE
AND			REB.OPERATOR_CODE = OPE.CODE
GROUP BY	OPE.CODE
";

            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);

            foreach (object[] item in data)
            {
                ObjectData od1 = new ObjectData();
                od1.Code = int.Parse(item[1].ToString());
                SaveIndicatorsValues(od1, IndicatorClassData.Operator,
                    double.Parse(item[0].ToString()));
            }
            #endregion new

            #region doc
            //IList operators = OperatorScheduleManager.GetOperatorsWorkingNowByApplication(UserApplicationData.FirstLevel.Name, false);
            //foreach (int operatorCode in operators)
            //{
            //    double res = 0;
            //    IList phoneCallAvg = SmartCadDatabase.SearchObjects(
            //        SmartCadHqls.GetCustomHql(
            //        SmartCadHqls.GetOperatorPhoneReportsRegisteredCurrentday,
            //        operatorCode,
            //        ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today)));

            //    if (phoneCallAvg.Count > 0)
            //    {
            //        foreach (PhoneReportData prd in phoneCallAvg)
            //        {
            //            TimeSpan ts = prd.RegisteredCallTime.Value.Subtract(prd.PickedUpCallTime.Value);
            //            res += ts.TotalSeconds;
            //        }
            //        res /= phoneCallAvg.Count;
            //    }

            //    ObjectData objectData = new ObjectData();
            //    objectData.Code = operatorCode;
            //    SaveIndicatorsValues(objectData, IndicatorClassData.Operator, res);
            //}
            #endregion doc
        }


        private void CalculateGroup()
        {
            #region new
            string sql = @" SELECT		CONVERT(FLOAT,ISNULL(SUM(DATEDIFF(SS,PHR.PICKED_UP_CALL_TIME, PHR.REGISTERED_CALL_TIME)),0))
			/CASE COUNT(PHR.REGISTERED_CALL_TIME) WHEN 0 THEN 1 ELSE COUNT(PHR.REGISTERED_CALL_TIME) END AS TIEMPO
			,OPE.CODE AS USUARIO
FROM		PHONE_REPORT AS PHR
			,REPORT_BASE AS REB
			,OPERATOR AS OPE
			,OPERATOR_ASSIGN AS OPA
WHERE		CONVERT(DATETIME,CONVERT(CHAR(10),PHR.REGISTERED_CALL_TIME,101),101) = 
			CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
AND			PHR.PARENT_CODE = REB.CODE
AND			REB.OPERATOR_CODE = OPA.OPERATOR_CODE
AND			OPA.DELETED_ID IS NULL
AND			(
				(			
				PHR.REGISTERED_CALL_TIME
				BETWEEN OPA.START_DATE AND OPA.END_DATE
				)			
			)
AND			OPA.SUPERVISOR_CODE = OPE.CODE
GROUP BY	OPE.CODE
";

            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);

            foreach (object[] item in data)
            {
                ObjectData od1 = new ObjectData();
                od1.Code = int.Parse(item[1].ToString());
                SaveIndicatorsValues(od1, IndicatorClassData.Group,
                    double.Parse(item[0].ToString()));
            }
            #endregion new

            #region doc
            //IList supervisors = OperatorScheduleManager.GetSupervisorsWorkingNowByApplication("FirstLevelSupervisorName");
            //Dictionary<int, double> results = new Dictionary<int, double>();
            //foreach (int supervisorCode in supervisors)
            //{
            //    IList operators = OperatorScheduleManager.GetAllDayOperatorsBySupervisor(supervisorCode);
            //    WorkShiftScheduleVariationData bshSupervisor =
            //       OperatorScheduleManager.GetCurrentWorkShiftSchedulesByOperator(supervisorCode, SmartCadDatabase.GetTimeFromBD().Date);

            //    double total = 0;
            //    double totalAmount = 0;

            //    foreach (OperatorData operatorData in operators)
            //    {

            //        //BaseSessionHistory bsh = OperatorScheduleManager.GetOperatorAssignBySupervisorAndByDayRealHours(supervisorCode, operatorData.Code);

            //          IList bshList =
            //          OperatorScheduleManager.GetAllDayOperatorAssignBySupervisorAndByDayRealHours(supervisorCode, operatorData.Code);
            //          foreach (OperatorAssignData bsh in bshList)
            //          {
            //              if ((bshSupervisor != null) &&
            //                (((bsh.StartDate.TimeOfDay >= bshSupervisor.Start.Value.TimeOfDay) &&
            //               (bsh.StartDate.TimeOfDay <= bshSupervisor.End.Value.TimeOfDay)) ||
            //               ((bsh.EndDate.TimeOfDay <= bshSupervisor.End.Value.TimeOfDay) &&
            //               (bsh.EndDate.TimeOfDay >= bshSupervisor.Start.Value.TimeOfDay))))
            //              {

            //                  IList phoneCallAvg = SmartCadDatabase.SearchObjects(
            //                      SmartCadHqls.GetCustomHql(
            //                      SmartCadHqls.GetOperatorPhoneReportsRegisteredCurrentWorkShift,
            //                      operatorData.Code,
            //                      ApplicationUtil.GetDataBaseFormattedDate(bsh.StartDate),
            //                      ApplicationUtil.GetDataBaseFormattedDate(bsh.EndDate)));


            //                  foreach (PhoneReportData prd in phoneCallAvg)
            //                  {
            //                      TimeSpan sub = prd.RegisteredCallTime.Value.Subtract(prd.PickedUpCallTime.Value);
            //                      total += sub.TotalSeconds;
            //                  }
            //                  totalAmount += phoneCallAvg.Count;
            //              }
            //          }
            //    }

            //    double res = 0;

            //    if (total > 0 && totalAmount > 0)
            //    {
            //        res = total / totalAmount;
            //    }

            //    ObjectData objectData = new ObjectData();
            //    objectData.Code = supervisorCode;
            //    SaveIndicatorsValues(objectData, IndicatorClassData.Group, res);
            //}

            #endregion doc
        }
    }
}
