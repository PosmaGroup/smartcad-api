//using System;
//using System.Collections.Generic;
//using System.Text;
//using SmartCadCore.Model;
//using System.Collections;

//namespace SmartCadCore.Core.Indicators
//{
//    public class PND17IndicatorValuesCalculator : IndicatorValuesCalculator
//    {
//        protected override string GetIndicator()
//        {
//            return "PND17";
//        }

//        protected override void Calculate()
//        {
//            CalculateSystemIndicators();
//        }

//        private void CalculateSystemIndicators()
//        {
//            IList departmentStation = SmartCadDatabase.SearchObjects(SmartCadHqls.GetDepartmentsStation);
//            foreach (DepartmentTypeData department in departmentStation)
//            {
//                double openDispatchOrderHightPriority = (long)SmartCadDatabase.SearchObject(SmartCadHqls.GetCustomHql(
//                    SmartCadHqls.GetOpendDispatchOrder, department.Code, 1));

//                double closedDispatchOrderHightPriority = (long)SmartCadDatabase.SearchObject(SmartCadHqls.GetCustomHql(
//                   SmartCadHqls.GetClosedDispatchOrderByDate,
//                   ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today),
//                   department.Code, 1));
//                double openDispatchOrderMediumPriority = (long)SmartCadDatabase.SearchObject(SmartCadHqls.GetCustomHql(
//                    SmartCadHqls.GetOpendDispatchOrder, department.Code, 2));

//                double closedDispatchOrderMediumPriority = (long)SmartCadDatabase.SearchObject(SmartCadHqls.GetCustomHql(
//                   SmartCadHqls.GetClosedDispatchOrderByDate,
//                   ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today),
//                   department.Code, 2));
//                double openDispatchOrderLowPriority = (long)SmartCadDatabase.SearchObject(SmartCadHqls.GetCustomHql(
//                    SmartCadHqls.GetOpendDispatchOrder, department.Code, 3));

//                double closedDispatchOrderLowPriority = (long)SmartCadDatabase.SearchObject(SmartCadHqls.GetCustomHql(
//                   SmartCadHqls.GetClosedDispatchOrderByDate,
//                   ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today),
//                   department.Code, 3));

//                double openDispatchOrderTotal = openDispatchOrderHightPriority + openDispatchOrderMediumPriority + openDispatchOrderLowPriority;
//                double closedDispatchOrderTotal = closedDispatchOrderHightPriority + closedDispatchOrderMediumPriority + closedDispatchOrderLowPriority;
//                List<double> resultsValues = new List<double>();
//                resultsValues.Add(openDispatchOrderHightPriority);
//                resultsValues.Add(openDispatchOrderMediumPriority);
//                resultsValues.Add(openDispatchOrderLowPriority);
//                resultsValues.Add(openDispatchOrderTotal);
//                resultsValues.Add(closedDispatchOrderHightPriority);
//                resultsValues.Add(closedDispatchOrderMediumPriority);
//                resultsValues.Add(closedDispatchOrderLowPriority);
//                resultsValues.Add(closedDispatchOrderTotal);

//                Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//                result.Add(department.Name, resultsValues);

//                SaveIndicatorMultiplesValues(department, IndicatorClassData.System, result);
//                //SaveIndicatorMultiplesValues(department, IndicatorClassData.Group, result);
//            }
//        }
       
      
//    }
//}
