using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using System.Threading;
using System.Collections;
using Iesi.Collections;
using SmartCadCore.Statistic;
using SmartCadCore.Common;


namespace SmartCadCore.Core.Indicators
{
   
    public abstract class IndicatorValuesCalculator
    {
        private static Timer timer = new Timer(new TimerCallback(SaveAll));
        private static StatisticRetriever statisticRetriever;
        static IndicatorValuesCalculator()
        {
            timer.Change(0, 10000);
        }

        public static void Stop()
        {
            try
            {
                if (timer != null)
                    timer.Dispose();
            }
            catch { }
        }

        protected DateTime lastRun = DateTime.MinValue;
        bool running = false;
        IndicatorData indicator = null;
        double lastValueSystem = 0;
        Dictionary<int, double> lastValueDepartment = new Dictionary<int, double>();
        Dictionary<int, double> lastValueGroup = new Dictionary<int, double>();
        Dictionary<int, double> lastValueOperator = new Dictionary<int, double>();
        double lastTrendSystem = 0;
        Dictionary<int, double> lastTrendDepartment = new Dictionary<int, double>();
        Dictionary<int, double> lastTrendGroup = new Dictionary<int, double>();
        Dictionary<int, double> lastTrendOperator = new Dictionary<int, double>();

        IndicatorResultData result = null;

        protected IndicatorData Indicator
        {
            get
            {
                LoadIndicator();
                return indicator;
            }
        }

        private void LoadIndicator()
        {
            if (indicator == null)
            {
                indicator = SmartCadDatabase.SearchObject<IndicatorData>(SmartCadHqls.GetCustomHql(SmartCadHqls.GetIndicatorByCustomCode, GetIndicator()));
            }
        }

        private DateTime LastRun
        {
            get
            {
                object result = SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(SmartCadHqls.GetLastDateTimeResultForIndicator, Indicator.Code));
                if (result != null)
                    lastRun = (DateTime)result;
                return lastRun;
            }
        }

        public void Run()
        {
            if (Indicator != null)
            {
                TimeSpan time = DateTime.Now.Subtract(LastRun);
                if ((LastRun == DateTime.MinValue || time.TotalSeconds > Indicator.Frecuency) && running == false)
                {
                    running = true;
                    CallCalculate();
                }
            }
        }

        private void CallCalculate()
        {
            try
            {
                LoadIndicator();
                Thread.Sleep(50);
                UpdateThreshold();
                Thread.Sleep(50);
                CreateResultData();
                Thread.Sleep(50);
                Calculate();
                Thread.Sleep(50);
                Save();
                Thread.Sleep(50);
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
            finally
            {
                running = false;
            }
        }

        private void CreateResultData()
        {
            result = new IndicatorResultData();
            result.Indicator = Indicator;
            result.Time = DateTime.Now;
            result.Result = new ArrayList();
        }

        private static List<IndicatorResultData> indicatorsResultDataList = new List<IndicatorResultData>();
        private static object syncindicatorsResultDataList = new object();
        private void Save()
        {
            IndicatorValuesCalculator.AddToSave(result);
        }

        private static void AddToSave(IndicatorResultData result)
        {
            lock (syncindicatorsResultDataList)
            {
                indicatorsResultDataList.Add(result);
            }
        }
        private static void SaveAll(object objectData)
        {
            SaveAll();
        }
        private static void SaveAll()
        {
            lock (syncindicatorsResultDataList)
            {
                try
                {
                    SmartCadDatabase.SaveObjectList<IndicatorResultData>(indicatorsResultDataList);
                }
                catch
                { }
                indicatorsResultDataList.Clear();
            }
        }

        private void UpdateThreshold()
        {
            indicator.General = SmartCadDatabase.SearchObject<IndicatorForecastData>(SmartCadHqls.GetCustomHql(SmartCadHqls.GetIndicatorForecastByIndicatorIdTypeCurrentDate, indicator.Code, true.ToString().ToLower(), ApplicationUtil.GetDataBaseFormattedDate(DateTime.Now)));
            indicator.Forecast = SmartCadDatabase.SearchObject<IndicatorForecastData>(SmartCadHqls.GetCustomHql(SmartCadHqls.GetIndicatorForecastByIndicatorIdTypeCurrentDate, indicator.Code, false.ToString().ToLower(), ApplicationUtil.GetDataBaseFormattedDate(DateTime.Now)));
        }

        protected void SaveIndicatorsValues(IndicatorClassData resultClass, double resultValue)
        {
            SaveIndicatorsValues(null, resultClass, resultValue);
        }

        protected void SaveIndicatorsValues(ObjectData od, IndicatorClassData resultClass, double resultValue)
        {
            SaveIndicatorsValues(od, resultClass, string.Empty, resultValue);
        }

        protected void SaveIndicatorsValues(ObjectData od, IndicatorClassData resultClass, string description, double resultValue)
        {
            Dictionary<string, double> results1 = new Dictionary<string, double>();

            results1.Add(description, resultValue);

            SaveIndicatorMultiplesValues(od, resultClass, results1);
        }

        protected void SaveIndicatorMultiplesValues(ObjectData od, IndicatorClassData resultClass, Dictionary<string, double> resultValues)
        {
            Dictionary<string, List<double>> results = new Dictionary<string, List<double>>();
            foreach (KeyValuePair<string, double> pair in resultValues)
            {
                List<double> tempResultValues = new List<double>();
                tempResultValues.Add(pair.Value);
                results.Add(pair.Key, tempResultValues);
            }
            SaveIndicatorMultiplesValues(od, resultClass, results);
        }

        protected void SaveIndicatorMultiplesValues(ObjectData od, IndicatorClassData resultClass, Dictionary<string, List<double>> resultValues)
        {
            foreach (KeyValuePair<string, List<double>> pair in resultValues)
            {
                IndicatorResultValuesData singleResultValue = new IndicatorResultValuesData();
                singleResultValue.IndicatorClass = resultClass;
                if (od != null)
                    singleResultValue.CustomCode = od.Code;
                if (string.IsNullOrEmpty(pair.Key) == false)
                    singleResultValue.Description = pair.Key;
                if (indicator.Threshold() == true && pair.Value.Count == 1)
                {
                    double res = pair.Value[0];
                    if (indicator.MeasureUnit == "%")
                    {
                        res = res * 100;
                    }
                    else if (indicator.MeasureUnit == "seg")
                    {
                        res = Math.Round(res, MidpointRounding.AwayFromZero);
                    }
                    if (resultClass.Code == IndicatorClassData.System.Code)
                    {
                        singleResultValue.Threshold = CalculateThreshold(res);
                        singleResultValue.Trend = CalculateTrend(res, singleResultValue.Threshold);
                    }
                    else
                    {
                        singleResultValue.Threshold = CalculateThreshold(res, resultClass, od.Code);
                        singleResultValue.Trend = CalculateTrend(res, singleResultValue.Threshold, resultClass, od.Code);
                    }

                    if (resultClass.Code == IndicatorClassData.System.Code)
                    {
                        lastValueSystem = res;
                    }
                    else if (resultClass.Code == IndicatorClassData.Department.Code)
                    {
                        if (lastValueDepartment.ContainsKey(od.Code) == true)
                            lastValueDepartment[od.Code] = res;
                        else
                            lastValueDepartment.Add(od.Code, res);
                    }
                    else if (resultClass.Code == IndicatorClassData.Group.Code)
                    {
                        if (lastValueGroup.ContainsKey(od.Code) == true)
                            lastValueGroup[od.Code] = res;
                        else
                            lastValueGroup.Add(od.Code, res);
                    }
                    else if (resultClass.Code == IndicatorClassData.Operator.Code)
                    {
                        if (lastValueOperator.ContainsKey(od.Code) == true)
                            lastValueOperator[od.Code] = res;
                        else
                            lastValueOperator.Add(od.Code, res);
                    }
                }

                singleResultValue.Value = ApplicationUtil.ConvertIndicatorResultValuesToString
                    (RoundResultValues(pair.Value));

                singleResultValue.Result = result;
                result.Result.Add(singleResultValue);
            }
        }   

        private List<double> RoundResultValues(List<double> resultValues)
        {
            List<double> roundedResults = new List<double>();

            foreach (double rv in resultValues)
            {
                double valueRv = rv;
                if (indicator.MeasureUnit == ResourceLoader.GetString2("%"))
                    valueRv = Math.Round(valueRv, 4, MidpointRounding.AwayFromZero);
                else if (indicator.MeasureUnit == ResourceLoader.GetString2("seg"))
                    valueRv = Math.Round(valueRv, MidpointRounding.AwayFromZero);
                else
                    valueRv = Math.Round(valueRv, 2, MidpointRounding.AwayFromZero);
                roundedResults.Add(valueRv);
            }
            return roundedResults;
        }

        private int CalculateTrend(double result, int threshold)
        {
            return CalculateTrend(result, threshold, null, 0);
        }

        private int CalculateTrend(double result, int thresholdResult, IndicatorClassData indicatorClass, int customCode)
        {
            IIndicatorThreshold threshold;
            double lastTrend = 0;
            double lastValue = 0;
            threshold = indicator.General;

            //If the indicator has a forecast we have to used instead of the values inside the indicator.
            if ((Indicator.Forecast != null && Indicator.Forecast.DepartmentType == null) ||
                (Indicator.Forecast != null &&
                 Indicator.Forecast.DepartmentType != null &&
                 indicatorClass != null &&
                 indicatorClass == IndicatorClassData.Department &&
                 indicator.Forecast.DepartmentType.Code == customCode))
            {
                threshold = indicator.Forecast;
            }

            //The last trend has to be initialize in the last value.
            if (indicatorClass == null)
            {
                lastTrend = lastTrendSystem;
                lastValue = lastValueSystem;
            }
            else if (indicatorClass.Code == IndicatorClassData.Department.Code)
            {
                if (lastTrendDepartment.ContainsKey(customCode) == true)
                    lastTrend = lastTrendDepartment[customCode];
                if (lastValueDepartment.ContainsKey(customCode) == true)
                    lastValue = lastValueDepartment[customCode];
            }
            else if (indicatorClass.Code == IndicatorClassData.Group.Code)
            {
                if (lastTrendGroup.ContainsKey(customCode) == true)
                    lastTrend = lastTrendGroup[customCode];
                if (lastValueGroup.ContainsKey(customCode) == true)
                    lastValue = lastValueGroup[customCode];
            }
            else if (indicatorClass.Code == IndicatorClassData.Operator.Code)
            {
                if (lastTrendOperator.ContainsKey(customCode) == true)
                    lastTrend = lastTrendOperator[customCode];
                if (lastValueOperator.ContainsKey(customCode) == true)
                    lastValue = lastValueOperator[customCode];
            }

            int trend = 1;
            if (thresholdResult == 3)
                trend = -1;

            if (threshold.Pattern == PositivePattern.Maintain)
            {
                int X = (threshold.High - threshold.Low) / 4;
                int realLow = threshold.Low + X;
                int realHigh = threshold.High - X;

                if (result > threshold.High || result < threshold.Low)
                {
                    if (lastValue > result)
                        trend = -2;
                    else if (lastValue < result)
                        trend = -3;
                    else if (lastTrend < 0)
                        trend = -1;
                }
                else if (result >= realLow && result <= realHigh)
                {
                    if (lastValue > result)
                        trend = 2;
                    else if (lastValue < result)
                        trend = 3;
                    else if (lastTrend < 0)
                        trend = 1;
                }
                else if (result > realHigh && result <= threshold.High)
                {
                    if (lastValue > result)
                        trend = 2;
                    else if (lastValue < result)
                        trend = -3;
                    else if (lastTrend > 0)
                        trend = 1;
                    else if (lastTrend < 0)
                        trend = -1;
                }
                else if (result >= threshold.Low && result < realLow)
                {
                    if (lastValue > result)
                        trend = -2;
                    else if (lastValue < result)
                        trend = 3;
                    else if (lastTrend > 0)
                        trend = 1;
                    else if (lastTrend < 0)
                        trend = -1;
                }
            }
            else
            {
                if (result == lastValue)
                {
                    if (lastTrend > 0)
                        trend = 1;
                    else if (lastTrend < 0)
                        trend = -1;
                }
                else if (lastValue > result && threshold.Pattern == PositivePattern.Increase)
                    trend = -2;
                else if (lastValue < result && threshold.Pattern == PositivePattern.Decrease)
                    trend = -3;
                else if (lastValue > result && threshold.Pattern == PositivePattern.Decrease)
                    trend = 2;
                else if (lastValue < result && threshold.Pattern == PositivePattern.Increase)
                    trend = 3;
            }

            //The newest trend has to be save.
            if (indicatorClass == null)
            {
                lastTrendSystem = trend;
            }
            else if (indicatorClass.Code == IndicatorClassData.Department.Code)
            {
                if (lastTrendDepartment.ContainsKey(customCode) == true)
                    lastTrendDepartment[customCode] = trend;
                else
                    lastTrendDepartment.Add(customCode, trend);
            }
            else if (indicatorClass.Code == IndicatorClassData.Group.Code)
            {
                if (lastTrendGroup.ContainsKey(customCode) == true)
                    lastTrendGroup[customCode] = trend;
                else
                    lastTrendGroup.Add(customCode, trend);
            }
            else if (indicatorClass.Code == IndicatorClassData.Operator.Code)
            {
                if (lastTrendOperator.ContainsKey(customCode) == true)
                    lastTrendOperator[customCode] = trend;
                else
                    lastTrendOperator.Add(customCode, trend);
            }
            
            return trend;
        }

        private int CalculateThreshold(double result)
        {
            return CalculateThreshold(result, null, 0);
        }

        //1 verde 2 amarillo  3 rojo
        private int CalculateThreshold(double result, IndicatorClassData indicatorClass, int departmentCode)
        {
            IIndicatorThreshold threshold;
            threshold = indicator.General;
            
            //If the indicator has a forecast we have to used instead of the values inside the indicator.
            if ((Indicator.Forecast != null && Indicator.Forecast.DepartmentType == null) ||
                (Indicator.Forecast != null && 
                 Indicator.Forecast.DepartmentType != null && 
                 indicatorClass != null && 
                 indicatorClass == IndicatorClassData.Department && 
                 indicator.Forecast.DepartmentType.Code == departmentCode))
            {
                threshold = indicator.Forecast;
            }

            int resultThreshold = 0;
            try
            {
                if (threshold.Pattern == PositivePattern.Maintain)
                {
                    if (result >= threshold.Low && result <= threshold.High)
                    {
                        resultThreshold = 1;
                    }
                    else
                    {
                        resultThreshold = 3;
                    }
                }
                else
                {
                    if (result >= threshold.Low && result <= threshold.High)
                    {
                        resultThreshold = 2;
                    }
                    else if (threshold.Low > result && threshold.Pattern == PositivePattern.Increase ||
                             threshold.High < result && threshold.Pattern == PositivePattern.Decrease)
                    {
                        resultThreshold = 3;
                    }
                    else if (threshold.Low > result && threshold.Pattern == PositivePattern.Decrease ||
                             threshold.High < result && threshold.Pattern == PositivePattern.Increase)
                    {
                        resultThreshold = 1;
                    }
                }
            }
            catch (InvalidCastException)
            { }
            return resultThreshold;
        }
        
        private bool ContainsClass(ISet list, IndicatorClassData classData)
        {
            bool returnValue = false;
            foreach (IndicatorClassDashboardData dashBoarData in list)
            {
                if (dashBoarData.IndicatorClass.Name == classData.Name)
                {
                    returnValue = true;
                }
            }

            return returnValue;
        }

        protected abstract void Calculate();

        protected virtual double CalculateOperatorValue(int supervisorCode, int operatorCode)
        {
            return 0;
        }

        protected virtual double CalculateGroupTotalElementsCount(int supervisorCode )
        {
            return -1;
        }
        protected virtual double CalculateGroupTotalElementsValue(int supervisorCode, int operatorCode)
        {
            return -1;
        }
        protected virtual double CalculateSystemTotalElementsCount()
        {
            return -1;
        }
        protected virtual double CalculateSystemTotalElementsValue(int supervisorCode, int operatorCode)
        {
            return -1;
        }


        protected virtual Dictionary<string, List<double>> CalculateIndicatorMultipleGroupTotalElementsCount(int supervisorCode)
        {
            return new Dictionary<string, List<double>>();
        }
        protected virtual Dictionary<string, List<double>> CalculateIndicatorMultipleGroupTotalElementsValue(int supervisorCode, 
            int operatorCode)
        {
            return new Dictionary<string, List<double>>();
        }
        protected virtual Dictionary<string, List<double>> CalculateIndicatorMultipleSystemTotalElementsCount()
        {
            return new Dictionary<string, List<double>>();
        }
        protected virtual Dictionary<string, List<double>> CalculateIndicatorMultipleSystemTotalElementsValue(int supervisorCode, 
            int operatorCode)
        {
            return new Dictionary<string, List<double>>();
        }

         
        protected virtual Dictionary<string, List<double>> CalculateOperatorMultipleValue(int operatorCode)
        {
            return new Dictionary<string,List<double>>();
        }
        protected abstract string GetIndicator();
    }
}
