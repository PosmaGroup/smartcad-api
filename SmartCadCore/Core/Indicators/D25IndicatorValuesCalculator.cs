using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using System.Collections;

namespace SmartCadCore.Core.Indicators
{
    public class D25IndicatorValuesCalculator : IndicatorValuesCalculator
    {
        protected override string GetIndicator()
        {
            return "D25";
        }

        protected override void Calculate()
        {
            CalculateSystem();
            CalculateDepartment();
        }

        private void CalculateSystem()
        {
            #region new
            string sql = @" SELECT		SUM(DO_MAX.MAXIMO)/COUNT(DO_MAX.MAXIMO) AS PROMEDIO, AGRUPADO.TIPO AS TIPO
FROM		REPORT_BASE AS REB
			,INCIDENT_NOTIFICATION AS INOT
			,(
				SELECT
				INC_1.CODE AS CODIGO,
				TIPO = RTRIM(REPLACE(REPLACE(o.list,'<FRIENDLY_NAME>',''),'</FRIENDLY_NAME>',' , '))
				FROM
				INCIDENT AS INC_1
				CROSS APPLY
				(
				SELECT		INTY.FRIENDLY_NAME
				FROM		INCIDENT AS INC
							,REPORT_BASE AS REB
							,REPORT_BASE_INCIDENT_TYPE AS RBIT
							,INCIDENT_TYPE AS INTY
				WHERE		INC.CODE = REB.INCIDENT_CODE
				AND			REB.CODE = RBIT.REPORT_BASE_CODE
				AND			RBIT.INCIDENT_TYPE_CODE = INTY.CODE
				AND			INC.CODE = INC_1.CODE
				ORDER BY INTY.FRIENDLY_NAME
				FOR XML PATH('')
				) o (list)
			) AS AGRUPADO
			,(
				SELECT		MAX(DATEDIFF(SS,DO.START_DATE,DO.END_DATE)) AS MAXIMO, DO.INCIDENT_NOTIFICATION_CODE AS CODE
				FROM		DISPATCH_ORDER AS DO
				WHERE		CONVERT(DATETIME,CONVERT(CHAR(10),DO.END_DATE,101),101) = CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
				GROUP BY	DO.INCIDENT_NOTIFICATION_CODE
			) AS DO_MAX
WHERE		AGRUPADO.CODIGO = REB.INCIDENT_CODE
AND			REB.CODE = INOT.REPORT_BASE_CODE
AND			INOT.CODE = DO_MAX.CODE
GROUP BY	AGRUPADO.TIPO
";


            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);

            
            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
            List<double> list = new List<double>();
            string incidentTypes = "";
            foreach (object[] item in data)
            {
                incidentTypes = item[1].ToString().Substring(0, item[1].ToString().Length - 1);
                list.Add(double.Parse((item[0].ToString())));              
                result.Add(incidentTypes, list);
                SaveIndicatorMultiplesValues(null, IndicatorClassData.System, result);
                result.Clear();
                incidentTypes = "";
                list = new List<double>();
            }
            #endregion new


            #region doc
//            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();

//            string hql = @"select distinct data.Code, sit.FriendlyName 
//                           from IncidentData data inner join
//                                data.SetReportBaseList srl inner join
//                                srl.SetIncidentTypes sit inner join
//                                srl.IncidentNotifications inot inner join
//                                inot.SetDispatchOrders dos
//                           where dos.EndDate > '{0}'
//                           order by data.Code, sit.FriendlyName";

//            IList incidentInfo = (IList)
//                SmartCadDatabase.SearchBasicObjects(
//                SmartCadHqls.GetCustomHql(hql,
//                ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today)));

//            Dictionary<string, List<int>> incidentTypeNamesByIncident = new Dictionary<string, List<int>>();
//            string itnames = "";
//            int code = 0;
//            foreach (object[] item in incidentInfo)
//            {
//                if (code != 0 && code != int.Parse(item[0].ToString()))
//                {
//                    if (incidentTypeNamesByIncident.ContainsKey(itnames) == false)
//                    {
//                        List<int> codes = new List<int>();
//                        codes.Add(code);
//                        incidentTypeNamesByIncident.Add(itnames, codes);
//                    }
//                    else
//                    {
//                        List<int> codes = incidentTypeNamesByIncident[itnames];
//                        codes.Add(code);
//                    }
//                    itnames = string.Empty;
//                }

//                code = int.Parse(item[0].ToString());
//                string friendlyName = item[1].ToString();
//                if (itnames.Contains(friendlyName) == false)
//                {
//                    if (itnames.Length == 0)
//                    {
//                        itnames = friendlyName;
//                    }
//                    else
//                    {
//                        itnames = string.Concat(itnames, ", ", friendlyName);
//                    }
//                }
//            }

//            if (itnames != string.Empty)
//            {
//                if (incidentTypeNamesByIncident.ContainsKey(itnames) == false)
//                {
//                    List<int> codes = new List<int>();
//                    codes.Add(code);
//                    incidentTypeNamesByIncident.Add(itnames, codes);
//                }
//                else
//                {
//                    List<int> codes = incidentTypeNamesByIncident[itnames];
//                    codes.Add(code);
//                }
//            }

//            foreach (KeyValuePair<string, List<int>> kvp in incidentTypeNamesByIncident)
//            {
//                StringBuilder sb = new StringBuilder();
//                sb.Append("(");
//                foreach (int codeValue in kvp.Value)
//                {
//                    if (sb.Length == 1)
//                    {
//                        sb.Append(codeValue);
//                    }
//                    else
//                    {
//                        sb.Append(", ");
//                        sb.Append(codeValue);
//                    }
//                }
//                sb.Append(")");

//                string hql2 = @"select data.IncidentNotification.ReportBase.Incident.Code, 
//                                       cast(max(data.EndDate - data.StartDate) as int)
//                                from DispatchOrderData data
//                                where data.EndDate > '{0}' and
//                                      data.IncidentNotification.ReportBase.Incident.Code in (2)
//                                group by data.IncidentNotification.ReportBase.Incident.Code";

//                incidentInfo = (IList)
//                    SmartCadDatabase.SearchBasicObjects(
//                    SmartCadHqls.GetCustomHql(hql2,
//                    ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today),
//                    sb.ToString()));

//                double average = 0;

//                foreach (object[] item in incidentInfo)
//                {
//                    average += (int)item[1];
//                }

//                List<double> list = new List<double>();
//                if (average > 0)
//                {
//                    list.Add(Convert.ToDouble(average / incidentInfo.Count));
//                    result.Add(kvp.Key, list);

//                    SaveIndicatorMultiplesValues(null, IndicatorClassData.System, result);
//                    result.Clear();
//                }
            //            }
            #endregion doc
        }

        private void CalculateDepartment()
        {
            #region new
            string sql = @" SELECT		SUM(DO_MAX.MAXIMO)/COUNT(DO_MAX.MAXIMO) AS PROMEDIO, AGRUPADO.TIPO AS TIPO, DET.CODE AS ORGANISMO
FROM		REPORT_BASE AS REB
			,INCIDENT_NOTIFICATION AS INOT
			,DEPARTMENT_TYPE AS DET
			,(
				SELECT
				INC_1.CODE AS CODIGO,
				TIPO = RTRIM(REPLACE(REPLACE(o.list,'<FRIENDLY_NAME>',''),'</FRIENDLY_NAME>',' , '))
				FROM
				INCIDENT AS INC_1
				CROSS APPLY
				(
				SELECT		INTY.FRIENDLY_NAME
				FROM		INCIDENT AS INC
							,REPORT_BASE AS REB
							,REPORT_BASE_INCIDENT_TYPE AS RBIT
							,INCIDENT_TYPE AS INTY
				WHERE		INC.CODE = REB.INCIDENT_CODE
				AND			REB.CODE = RBIT.REPORT_BASE_CODE
				AND			RBIT.INCIDENT_TYPE_CODE = INTY.CODE
				AND			INC.CODE = INC_1.CODE
				ORDER BY INTY.FRIENDLY_NAME
				FOR XML PATH('')
				) o (list)
			) AS AGRUPADO
			,(
				SELECT		MAX(DATEDIFF(SS,DO.START_DATE,DO.END_DATE)) AS MAXIMO, DO.INCIDENT_NOTIFICATION_CODE AS CODE
				FROM		DISPATCH_ORDER AS DO
				WHERE		CONVERT(DATETIME,CONVERT(CHAR(10),DO.END_DATE,101),101) = CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
				GROUP BY	DO.INCIDENT_NOTIFICATION_CODE
			) AS DO_MAX
WHERE		AGRUPADO.CODIGO = REB.INCIDENT_CODE
AND			REB.CODE = INOT.REPORT_BASE_CODE
AND			INOT.CODE = DO_MAX.CODE
AND			INOT.DEPARTMENT_TYPE_CODE = DET.CODE
GROUP BY	AGRUPADO.TIPO,DET.CODE
order by DET.CODE
";


            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);

            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
            List<double> list = new List<double>();
            int departmentCode = 0;
            string incidentTypes = "";
            foreach (object[] item in data)
            {
                if (departmentCode != 0 && departmentCode != int.Parse(item[2].ToString()))
                {

                    ObjectData od1 = new ObjectData();
                    od1.Code = departmentCode;
                    SaveIndicatorMultiplesValues(od1, IndicatorClassData.Department, result);
                    result.Clear();
                    incidentTypes = "";
                    list = new List<double>();
                }

                incidentTypes = item[1].ToString().Substring(0, item[1].ToString().Length - 1);
                list.Add(double.Parse(item[0].ToString()));                
                result.Add(incidentTypes, list);
                list = new List<double>();
                departmentCode = (int)item[2];

                if (item == data[data.Count - 1])
                {
                    ObjectData od1 = new ObjectData();
                    od1.Code = departmentCode;
                    SaveIndicatorMultiplesValues(od1, IndicatorClassData.Department, result);
                }
            }
            #endregion new

            #region doc
//            IList departmentCodes = (IList)SmartCadDatabase.SearchBasicObjects(
//                SmartCadHqls.GetDepartmentsTypeCodes);
//            foreach (int departmentCode in departmentCodes)
//            {
//                Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();

//                string hql = @"select distinct data.Code, sit.FriendlyName 
//                           from IncidentData data inner join
//                                data.SetReportBaseList srl inner join
//                                srl.SetIncidentTypes sit inner join
//                                srl.IncidentNotifications inot inner join
//                                inot.SetDispatchOrders dos
//                           where dos.EndDate > '{0}' and
//                                 inot.DepartmentType.Code = {1}
//                           order by data.Code, sit.FriendlyName";

//                IList incidentInfo = (IList)
//                    SmartCadDatabase.SearchBasicObjects(
//                    SmartCadHqls.GetCustomHql(hql,
//                    ApplicationUtil.GetDataBaseFormattedDate(SmartCadDatabase.GetTimeFromBD().Date),
//                    departmentCode));

//                Dictionary<string, List<int>> incidentTypeNamesByIncident = new Dictionary<string, List<int>>();
//                string itnames = "";
//                int code = 0;
//                foreach (object[] item in incidentInfo)
//                {
//                    if (code != 0 && code != int.Parse(item[0].ToString()))
//                    {
//                        if (incidentTypeNamesByIncident.ContainsKey(itnames) == false)
//                        {
//                            List<int> codes = new List<int>();
//                            codes.Add(code);
//                            incidentTypeNamesByIncident.Add(itnames, codes);
//                        }
//                        else
//                        {
//                            List<int> codes = incidentTypeNamesByIncident[itnames];
//                            codes.Add(code);
//                        }
//                        itnames = string.Empty;
//                    }

//                    code = int.Parse(item[0].ToString());
//                    string friendlyName = item[1].ToString();
//                    if (itnames.Contains(friendlyName) == false)
//                    {
//                        if (itnames.Length == 0)
//                        {
//                            itnames = friendlyName;
//                        }
//                        else
//                        {
//                            itnames = string.Concat(itnames, ", ", friendlyName);
//                        }
//                    }
//                }

//                if (incidentTypeNamesByIncident.ContainsKey(itnames) == false)
//                {
//                    List<int> codes = new List<int>();
//                    codes.Add(code);
//                    incidentTypeNamesByIncident.Add(itnames, codes);
//                }
//                else
//                {
//                    List<int> codes = incidentTypeNamesByIncident[itnames];
//                    codes.Add(code);
//                }

//                SaveDepartment(departmentCode, incidentTypeNamesByIncident);
//                incidentTypeNamesByIncident.Clear();
//            }
#endregion doc
        }

        #region doc
        private void SaveDepartment(int departmentCode, Dictionary<string, List<int>> incidentTypeNamesByIncident)
        {
            foreach (KeyValuePair<string, List<int>> kvp in incidentTypeNamesByIncident)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("(");
                foreach (int codeValue in kvp.Value)
                {
                    if (sb.Length == 1)
                    {
                        sb.Append(codeValue);
                    }
                    else
                    {
                        sb.Append(", ");
                        sb.Append(codeValue);
                    }
                }
                sb.Append(")");

                string hql2 = @"select inc.Code, 
                                       cast(max(data.EndDate - data.StartDate) as int)
                                from DispatchOrderData data 
                                inner join data.IncidentNotification notif
                                inner join notif.ReportBase repoBase
                                inner join repoBase.Incident inc
                                where data.EndDate > '{0}' and
                                      inc.Code in {1} and
                                      notif.DepartmentType.Code = {2}
                                group by inc.Code";

                IList incidentInfo = (IList)
                    SmartCadDatabase.SearchBasicObjects(
                    SmartCadHqls.GetCustomHql(hql2,
                    ApplicationUtil.GetDataBaseFormattedDate(SmartCadDatabase.GetTimeFromBD().Date),
                    sb.ToString(),
                    departmentCode));

                double average = 0;

                foreach (object[] item in incidentInfo)
                {
                    average += (int)item[1];
                }

                Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
                List<double> list = new List<double>();
                if (average > 0)
                {
                    list.Add(Convert.ToDouble(average / incidentInfo.Count));
                    result.Add(kvp.Key, list);

                    ObjectData od = new ObjectData();
                    od.Code = departmentCode;
                    SaveIndicatorMultiplesValues(od, IndicatorClassData.Department, result);
                    result.Clear();
                }
            }

        }
    #endregion doc
    }
}
