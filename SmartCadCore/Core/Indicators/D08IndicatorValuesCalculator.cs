using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using System.Collections;

namespace SmartCadCore.Core.Indicators
{
    public class D08IndicatorValuesCalculator : IndicatorValuesCalculator
    {
        protected override string GetIndicator()
        {
            return "D08";
        }

        protected override void Calculate()
        {
            CalculateSystem();
            CalculateDepartment();
        }

        private void CalculateDepartment()
        {
            #region new
            string sql = @" SELECT		DETY.CODE AS ORGANISMO, COUNT(DISTINCT AGRUPADO.CODIGO) AS CANTIDAD
,AGRUPADO.TIPO AS TIPO,


(SELECT		COUNT(INOT.INCIDENT_NOTIFICATION_STATUS_CODE) 	
				FROM		INCIDENT_NOTIFICATION AS INOT							
				WHERE		INOT.INCIDENT_NOTIFICATION_STATUS_CODE =5							
				AND			DETY.CODE = INOT.DEPARTMENT_TYPE_CODE				
				) as TOTAL_INCIDENT_NOTIF

FROM		REPORT_BASE AS REB
			,INCIDENT_NOTIFICATION AS INOT
			,DEPARTMENT_TYPE AS DETY
			,(
				SELECT
				INC_1.CODE AS CODIGO,
				TIPO = RTRIM(REPLACE(REPLACE(o.list,'<FRIENDLY_NAME>',''),'</FRIENDLY_NAME>',' , '))
				FROM
				INCIDENT AS INC_1
				CROSS APPLY
				(
				SELECT		INTY.FRIENDLY_NAME
				FROM		INCIDENT AS INC
							,REPORT_BASE AS REB
							,REPORT_BASE_INCIDENT_TYPE AS RBIT
							,INCIDENT_TYPE AS INTY
				WHERE		INC.IS_EMERGENCY = 1
				AND			INC.INCIDENT_STATUS_CODE = 1
				AND			INC.CODE = REB.INCIDENT_CODE
				AND			REB.CODE = RBIT.REPORT_BASE_CODE
				AND			RBIT.INCIDENT_TYPE_CODE = INTY.CODE
				AND			INC.CODE = INC_1.CODE
				ORDER BY INTY.FRIENDLY_NAME
				FOR XML PATH('')
				) o (list)
				WHERE	INC_1.IS_EMERGENCY = 1
				AND		INC_1.INCIDENT_STATUS_CODE = 1
			) AS AGRUPADO
WHERE		AGRUPADO.CODIGO = REB.INCIDENT_CODE
AND			REB.CODE = INOT.REPORT_BASE_CODE
AND			INOT.INCIDENT_NOTIFICATION_STATUS_CODE = 5
AND			INOT.DEPARTMENT_TYPE_CODE = DETY.CODE
GROUP BY	AGRUPADO.TIPO,DETY.CODE, INOT.DEPARTMENT_TYPE_CODE
order by DETY.CODE
";


            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);

            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
            List<double> list = new List<double>();
            int departmentCode = 0;
            string incidentTypes = "";
            foreach (object[] item in data)
            {
                if (departmentCode != 0 && departmentCode != int.Parse(item[0].ToString()))
                {

                    ObjectData od1 = new ObjectData();
                    od1.Code = departmentCode;
                    SaveIndicatorMultiplesValues(od1, IndicatorClassData.Department, result);
                    result.Clear();
                    incidentTypes = "";
                    list = new List<double>();
                }

                incidentTypes = item[2].ToString().Substring(0, item[2].ToString().Length - 1);
                list.Add(double.Parse(item[1].ToString()));
                list.Add(double.Parse(item[1].ToString()) / double.Parse(item[3].ToString()));
                result.Add(incidentTypes, list);
                list = new List<double>();
                departmentCode = (int)item[0];

                if (item == data[data.Count - 1])
                {
                    ObjectData od1 = new ObjectData();
                    od1.Code = departmentCode;
                    SaveIndicatorMultiplesValues(od1, IndicatorClassData.Department, result);
                }
            }
            #endregion new

            #region doc
            //            int totalIncidents = 0;
//            IList departmentCodes = (IList)SmartCadDatabase.SearchBasicObjects(
//                SmartCadHqls.GetDepartmentsTypeCodes);
//            foreach (int departmentCode in departmentCodes)
//            {
//                Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();

//                string hql = @"select distinct data.Code, sit.FriendlyName
//                           from IncidentNotificationData inot left join
//                                inot.ReportBase.Incident data left join
//                                data.SetReportBaseList srl left join
//                                srl.SetIncidentTypes sit
//                           where inot.StartDate is null and 
//                                 inot.DepartmentType.Code = {0}
//                           order by data.Code, sit.FriendlyName";

               

//                IList incidentInfo = (IList)SmartCadDatabase.SearchBasicObjects(
//                    SmartCadHqls.GetCustomHql(
//                    hql, departmentCode));

//                Dictionary<string, List<int>> incidentTypeNamesByIncident = new Dictionary<string, List<int>>();
//                string itnames = "";
//                int code = 0;
//                foreach (object[] item in incidentInfo)
//                {
//                    if (code != 0 && code != int.Parse(item[0].ToString()))
//                    {
//                        if (incidentTypeNamesByIncident.ContainsKey(itnames) == false)
//                        {
//                            List<int> codes = new List<int>();
//                            codes.Add(code);
//                            totalIncidents += 1;
//                            incidentTypeNamesByIncident.Add(itnames, codes);
//                        }
//                        else
//                        {
//                            List<int> codes = incidentTypeNamesByIncident[itnames];
//                            codes.Add(code);
//                            totalIncidents += 1;
//                        }
//                        itnames = string.Empty;
//                    }

//                    code = int.Parse(item[0].ToString());
//                    string friendlyName = item[1].ToString();
//                    if (itnames.Contains(friendlyName) == false)
//                    {
//                        if (itnames.Length == 0)
//                        {
//                            itnames = friendlyName;
//                        }
//                        else
//                        {
//                            itnames = string.Concat(itnames, ", ", friendlyName);
//                        }
//                    }
//                }

//                if (itnames != string.Empty)
//                {
//                    if (incidentTypeNamesByIncident.ContainsKey(itnames) == false)
//                    {
//                        List<int> codes = new List<int>();
//                        codes.Add(code);
//                        totalIncidents += 1;
//                        incidentTypeNamesByIncident.Add(itnames, codes);
//                    }
//                    else
//                    {
//                        List<int> codes = incidentTypeNamesByIncident[itnames];
//                        codes.Add(code);
//                        totalIncidents += 1;
//                    }
//                }

//                foreach (KeyValuePair<string, List<int>> kvp in incidentTypeNamesByIncident)
//                {
//                    List<double> list = new List<double>();
//                    list.Add(Convert.ToDouble(kvp.Value.Count));
//                    list.Add(Convert.ToDouble(kvp.Value.Count) /totalIncidents);                    
//                    result.Add(kvp.Key, list);

//                    DepartmentTypeData dtd = new DepartmentTypeData();
//                    dtd.Code = departmentCode;
//                    SaveIndicatorMultiplesValues(dtd, IndicatorClassData.Department, result);
//                    result.Clear();
//                }
//                totalIncidents = 0;
//                incidentTypeNamesByIncident.Clear();
            //            }
            #endregion doc
        }

        private void CalculateSystem()
        {

            #region new
            string sql = @" SELECT		COUNT(DISTINCT AGRUPADO.CODIGO) AS CANTIDAD, AGRUPADO.TIPO AS TIPO

FROM		REPORT_BASE AS REB
			,INCIDENT_NOTIFICATION AS INOT
			,(
				SELECT
				INC_1.CODE AS CODIGO,
				TIPO = RTRIM(REPLACE(REPLACE(o.list,'<FRIENDLY_NAME>',''),'</FRIENDLY_NAME>',' , '))
				FROM
				INCIDENT AS INC_1
				CROSS APPLY
				(
				SELECT		INTY.FRIENDLY_NAME
				FROM		INCIDENT AS INC
							,REPORT_BASE AS REB
							,REPORT_BASE_INCIDENT_TYPE AS RBIT
							,INCIDENT_TYPE AS INTY
				WHERE		INC.IS_EMERGENCY = 1
				AND			INC.INCIDENT_STATUS_CODE = 1
				AND			INC.CODE = REB.INCIDENT_CODE
				AND			REB.CODE = RBIT.REPORT_BASE_CODE
				AND			RBIT.INCIDENT_TYPE_CODE = INTY.CODE
				AND			INC.CODE = INC_1.CODE
				ORDER BY INTY.FRIENDLY_NAME
				FOR XML PATH('')
				) o (list)
				WHERE	INC_1.IS_EMERGENCY = 1
				AND		INC_1.INCIDENT_STATUS_CODE = 1
			) AS AGRUPADO
WHERE		AGRUPADO.CODIGO = REB.INCIDENT_CODE
AND			REB.CODE = INOT.REPORT_BASE_CODE
AND			INOT.INCIDENT_NOTIFICATION_STATUS_CODE = 5
GROUP BY AGRUPADO.TIPO
";


            

            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);

            double totalIncidents = 0;
            for (int i = 0; i < data.Count; i++)
            {
                object[] item = (object[])data[i];
                totalIncidents += double.Parse((item[0].ToString()));
            }

            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
            List<double> list = new List<double>();
            string incidentTypes = "";
            foreach (object[] item in data)
            {
                incidentTypes = item[1].ToString().Substring(0, item[1].ToString().Length - 1);
                list.Add(double.Parse((item[0].ToString())));
                list.Add(double.Parse((item[0].ToString())) / totalIncidents);
                result.Add(incidentTypes, list);
                SaveIndicatorMultiplesValues(null, IndicatorClassData.System, result);
                result.Clear();
                incidentTypes = "";
                list = new List<double>();
            }
            #endregion new


            #region doc
            //            int totalIncidents = 0;
//            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();

//            string hql = @"select distinct data.Code, sit.FriendlyName
//                           from IncidentNotificationData inot inner join
//                                inot.ReportBase.Incident data left join
//                                data.SetReportBaseList srl left join
//                                srl.SetIncidentTypes sit
//                           where inot.StartDate is null
//                           order by data.Code, sit.FriendlyName";

//            IList incidentInfo = (IList)SmartCadDatabase.SearchBasicObjects(
//                SmartCadHqls.GetCustomHql(
//                hql));

//            Dictionary<string, List<int>> incidentTypeNamesByIncident = new Dictionary<string, List<int>>();
//            string itnames = "";
//            int code = 0;
//            foreach (object[] item in incidentInfo)
//            {
//                if (code != 0 && code != int.Parse(item[0].ToString()))
//                {
//                    if (incidentTypeNamesByIncident.ContainsKey(itnames) == false)
//                    {
//                        List<int> codes = new List<int>();
//                        codes.Add(code);
//                        totalIncidents += 1;
//                        incidentTypeNamesByIncident.Add(itnames, codes);
                        
//                    }
//                    else
//                    {
//                        List<int> codes = incidentTypeNamesByIncident[itnames];
//                        codes.Add(code);
//                        totalIncidents += 1;
//                    }
//                    itnames = string.Empty;
//                }

//                code = int.Parse(item[0].ToString());
//                string friendlyName = item[1].ToString();
//                if (itnames.Contains(friendlyName) == false)
//                {
//                    if (itnames.Length == 0)
//                    {
//                        itnames = friendlyName;
//                    }
//                    else
//                    {
//                        itnames = string.Concat(itnames, ", ", friendlyName);
//                    }
//                }
//            }

//            if (itnames != string.Empty)
//            {
//                if (incidentTypeNamesByIncident.ContainsKey(itnames) == false)
//                {
//                    List<int> codes = new List<int>();
//                    codes.Add(code);
//                    totalIncidents += 1;
//                    incidentTypeNamesByIncident.Add(itnames, codes);
                    
//                }
//                else
//                {
//                    List<int> codes = incidentTypeNamesByIncident[itnames];
//                    codes.Add(code);
//                    totalIncidents += 1;                   
//                }
//            }

//            foreach (KeyValuePair<string, List<int>> kvp in incidentTypeNamesByIncident)
//            {                
//                List<double> list = new List<double>();
//                list.Add(Convert.ToDouble(kvp.Value.Count));
//                list.Add(Convert.ToDouble(kvp.Value.Count) / totalIncidents);
//                result.Add(kvp.Key, list);

//                SaveIndicatorMultiplesValues(null, IndicatorClassData.System, result);
//                result.Clear();
            //            }
            #endregion doc
        }
    }
}
