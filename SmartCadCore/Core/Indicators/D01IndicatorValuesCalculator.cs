using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using System.Collections;

namespace SmartCadCore.Core.Indicators
{
    public class D01IndicatorValuesCalculator : IndicatorValuesCalculator
    {
        protected override string GetIndicator()
        {
            return "D01";
        }

        protected override void Calculate()
        {
            CalculateSystem();
            CalculateDepartment();
            CalculateGroup();
        }

        private void CalculateSystem()
        {
            #region new
            string sql = @" SELECT		SUM(DATEDIFF(SS,INC.START_DATE,INOT.END_DATE))/COUNT(INOT.END_DATE) AS FECHA
			,AGRUPADO.TIPO AS NOMBRE
FROM
			INCIDENT_NOTIFICATION AS INOT
			,REPORT_BASE AS REB
			,INCIDENT AS INC
			,REPORT_BASE_INCIDENT_TYPE AS RBIT
			,INCIDENT_TYPE AS INCT
			,(
				SELECT
				INC_1.CODE AS CODIGO,
				TIPO = RTRIM(REPLACE(REPLACE(o.list,'<FRIENDLY_NAME>',''),'</FRIENDLY_NAME>',' , '))
				FROM
				INCIDENT AS INC_1
				CROSS APPLY
				(
				SELECT		INTY.FRIENDLY_NAME
				FROM		INCIDENT AS INC
							,REPORT_BASE AS REB
							,REPORT_BASE_INCIDENT_TYPE AS RBIT
							,INCIDENT_TYPE AS INTY
				WHERE		INC.CODE = REB.INCIDENT_CODE
				AND			REB.CODE = RBIT.REPORT_BASE_CODE
				AND			RBIT.INCIDENT_TYPE_CODE = INTY.CODE
				AND			INC.CODE = INC_1.CODE
				ORDER BY INTY.FRIENDLY_NAME
				FOR XML PATH('')
			) o (list)
			) AS AGRUPADO
WHERE		CONVERT(DATETIME,CONVERT(CHAR(10),INOT.END_DATE,101),101) = CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
AND			INOT.INCIDENT_NOTIFICATION_STATUS_CODE=6
AND			INOT.REPORT_BASE_CODE=REB.CODE
AND			REB.INCIDENT_CODE=INC.CODE
AND			REB.CODE=RBIT.REPORT_BASE_CODE
AND			RBIT.INCIDENT_TYPE_CODE=INCT.CODE
AND			AGRUPADO.CODIGO = INC.CODE
GROUP BY	AGRUPADO.TIPO";




            
            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);

            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
            List<double> list = new List<double>();          
            string incidentTypes = "";
            foreach (object[] item in data)
            {
                incidentTypes = item[1].ToString().Substring(0, item[1].ToString().Length - 1);
                list.Add(double.Parse((item[0].ToString())));
                result.Add(incidentTypes, list);
                SaveIndicatorMultiplesValues(null, IndicatorClassData.System, result);
                result.Clear();
                incidentTypes = "";
                list = new List<double>();
            }
            #endregion new


            #region doc
//            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();

////            string hql = @"select distinct data.Code, sit.FriendlyName 
////                           from IncidentData data left join
////                                data.SetReportBas   eList srl left join
////                                srl.SetIncidentTypes sit left join
////                                srl.IncidentNotifications inot left join
////                                inot.SetDispatchOrders dos
////                           where inot.EndDate > '{0}'
////                           order by data.Code, sit.FriendlyName";

//            string hql = @"select distinct data.Code, sit.FriendlyName 
//                           from IncidentData data left join
//                                data.SetReportBaseList srl left join
//                                srl.SetIncidentTypes sit left join
//                                srl.IncidentNotifications inot left join
//                                inot.SetDispatchOrders dos
//                           where inot.EndDate > '{0}'
//                           order by data.Code, sit.FriendlyName";


//            IList incidentInfo = (IList)
//                SmartCadDatabase.SearchBasicObjects(
//                SmartCadHqls.GetCustomHql(hql,
//                ApplicationUtil.GetDataBaseFormattedDate(SmartCadDatabase.GetTimeFromBD().Date)));

//            Dictionary<string, List<int>> incidentTypeNamesByIncident = new Dictionary<string, List<int>>();
//            string itnames = "";
//            int code = 0;
//            foreach (object[] item in incidentInfo)
//            {
//                if (code != 0 && code != int.Parse(item[0].ToString()))
//                {
//                    if (incidentTypeNamesByIncident.ContainsKey(itnames) == false)
//                    {
//                        List<int> codes = new List<int>();
//                        codes.Add(code);
//                        incidentTypeNamesByIncident.Add(itnames, codes);
//                    }
//                    else
//                    {
//                        List<int> codes = incidentTypeNamesByIncident[itnames];
//                        codes.Add(code);
//                    }
//                    itnames = string.Empty;
//                }

//                code = int.Parse(item[0].ToString());
//                string friendlyName = item[1].ToString();
//                if (itnames.Contains(friendlyName) == false)
//                {
//                    if (itnames.Length == 0)
//                    {
//                        itnames = friendlyName;
//                    }
//                    else
//                    {
//                        itnames = string.Concat(itnames, ", ", friendlyName);
//                    }
//                }
//            }

//            if (itnames != string.Empty)
//            {
//                if (incidentTypeNamesByIncident.ContainsKey(itnames) == false)
//                {
//                    List<int> codes = new List<int>();
//                    codes.Add(code);
//                    incidentTypeNamesByIncident.Add(itnames, codes);
//                }
//                else
//                {
//                    List<int> codes = incidentTypeNamesByIncident[itnames];
//                    codes.Add(code);
//                }
//            }

//            foreach (KeyValuePair<string, List<int>> kvp in incidentTypeNamesByIncident)
//            {
//                StringBuilder sb = new StringBuilder();
//                sb.Append("(");
//                foreach (int codeValue in kvp.Value)
//                {
//                    if (sb.Length == 1)
//                    {
//                        sb.Append(codeValue);
//                    }
//                    else
//                    {
//                        sb.Append(", ");
//                        sb.Append(codeValue);
//                    }
//                }
//                sb.Append(")");

//                string hql2 = @"select inc.Code, notif.EndDate, inc.StartDate
//                                    from DispatchOrderData data
//                                    inner join data.IncidentNotification notif
//                                    inner join notif.ReportBase repoBase
//                                    inner join repoBase.Incident inc
//                                    where notif.EndDate > '{0}' and
//                                        inc.Code in {1}
//                                    group by inc.Code, inc.StartDate, notif.EndDate";

//                incidentInfo = (IList)
//                    SmartCadDatabase.SearchBasicObjects(
//                    SmartCadHqls.GetCustomHql(hql2,
//                    ApplicationUtil.GetDataBaseFormattedDate(SmartCadDatabase.GetTimeFromBD().Date),
//                    sb.ToString()));

//                double average = 0;

//                foreach (object[] item in incidentInfo)
//                {
//                    DateTime end = (DateTime)item[1];
//                    DateTime start = (DateTime)item[2];
//                    TimeSpan sub = end - start;
//                    average += sub.TotalSeconds;
//                }


//                List<double> list = new List<double>();
//                if (average > 0)
//                {
//                    list.Add(Convert.ToDouble(average / incidentInfo.Count));
//                    result.Add(kvp.Key, list);

//                    SaveIndicatorMultiplesValues(null, IndicatorClassData.System, result);
//                    result.Clear();
//                }
            //}
            #endregion doc

        }

        private void CalculateDepartment()
        {
            #region new
            string sql = @" SELECT		DETY.CODE AS ORGANISMO, SUM(DATEDIFF(SS,INC.START_DATE,INOT.END_DATE))/COUNT(INOT.END_DATE) AS FECHA
			,AGRUPADO.TIPO AS NOMBRE
			
FROM
			INCIDENT_NOTIFICATION AS INOT
			,REPORT_BASE AS REB
			,INCIDENT AS INC
			,REPORT_BASE_INCIDENT_TYPE AS RBIT
			,INCIDENT_TYPE AS INCT
			,DEPARTMENT_TYPE AS DETY
			,(
				SELECT
				INC_1.CODE AS CODIGO,
				TIPO = RTRIM(REPLACE(REPLACE(o.list,'<FRIENDLY_NAME>',''),'</FRIENDLY_NAME>',' , '))
				FROM
				INCIDENT AS INC_1
				CROSS APPLY
				(
				SELECT		INTY.FRIENDLY_NAME
				FROM		INCIDENT AS INC
							,REPORT_BASE AS REB
							,REPORT_BASE_INCIDENT_TYPE AS RBIT
							,INCIDENT_TYPE AS INTY
				WHERE		INC.CODE = REB.INCIDENT_CODE
				AND			REB.CODE = RBIT.REPORT_BASE_CODE
				AND			RBIT.INCIDENT_TYPE_CODE = INTY.CODE
				AND			INC.CODE = INC_1.CODE
				ORDER BY INTY.FRIENDLY_NAME
				FOR XML PATH('')
				) o (list)
			) AS AGRUPADO
WHERE		CONVERT(DATETIME,CONVERT(CHAR(10),INOT.END_DATE,101),101) = CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
AND			INOT.INCIDENT_NOTIFICATION_STATUS_CODE=6
AND			INOT.DEPARTMENT_TYPE_CODE = DETY.CODE
AND			INOT.REPORT_BASE_CODE=REB.CODE
AND			REB.INCIDENT_CODE=INC.CODE
AND			REB.CODE=RBIT.REPORT_BASE_CODE
AND			RBIT.INCIDENT_TYPE_CODE=INCT.CODE
AND			AGRUPADO.CODIGO = INC.CODE
GROUP BY	DETY.CODE, AGRUPADO.TIPO
order by DETY.CODE";




            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);

            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
            List<double> list = new List<double>();
            int departmentCode = 0;
            string incidentTypes = "";
            foreach (object[] item in data)
            {                                          
                if (departmentCode != 0 && departmentCode != int.Parse(item[0].ToString()))
                {
                   
                    ObjectData od1 = new ObjectData();
                    od1.Code = departmentCode;
                    SaveIndicatorMultiplesValues(od1, IndicatorClassData.Department, result);
                    result.Clear();
                    incidentTypes = "";
                    list = new List<double>();
                }

                incidentTypes = item[2].ToString().Substring(0, item[2].ToString().Length - 1);
                list.Add(double.Parse(item[1].ToString()));
                result.Add(incidentTypes, list);
                list = new List<double>();
                departmentCode = (int)item[0];
                
                if (item == data[data.Count - 1])
                {                  
                    ObjectData od1 = new ObjectData();
                    od1.Code = departmentCode;
                    SaveIndicatorMultiplesValues(od1, IndicatorClassData.Department, result);
                }
            }
            #endregion new



            #region doc
            //            IList departmentCodes = (IList)SmartCadDatabase.SearchBasicObjects(
//                SmartCadHqls.GetDepartmentsTypeCodes);
//            foreach (int departmentCode in departmentCodes)
//            {
//                Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();

//                string hql = @"select distinct data.Code, sit.FriendlyName 
//                           from IncidentData data inner join
//                                data.SetReportBaseList srl inner join
//                                srl.SetIncidentTypes sit inner join
//                                srl.IncidentNotifications inot inner join
//                                inot.SetDispatchOrders dos
//                           where inot.EndDate > '{0}' and
//                                 inot.DepartmentType.Code = {1}
//                           order by data.Code, sit.FriendlyName";

//                IList incidentInfo = (IList)
//                    SmartCadDatabase.SearchBasicObjects(
//                    SmartCadHqls.GetCustomHql(hql,
//                    ApplicationUtil.GetDataBaseFormattedDate(SmartCadDatabase.GetTimeFromBD().Date),
//                    departmentCode));

//                Dictionary<string, List<int>> incidentTypeNamesByIncident = new Dictionary<string, List<int>>();
//                string itnames = "";
//                int code = 0;
//                foreach (object[] item in incidentInfo)
//                {
//                    if (code != 0 && code != int.Parse(item[0].ToString()))
//                    {
//                        if (incidentTypeNamesByIncident.ContainsKey(itnames) == false)
//                        {
//                            List<int> codes = new List<int>();
//                            codes.Add(code);
//                            incidentTypeNamesByIncident.Add(itnames, codes);
//                        }
//                        else
//                        {
//                            List<int> codes = incidentTypeNamesByIncident[itnames];
//                            codes.Add(code);
//                        }
//                        itnames = string.Empty;
//                    }

//                    code = int.Parse(item[0].ToString());
//                    string friendlyName = item[1].ToString();
//                    if (itnames.Contains(friendlyName) == false)
//                    {
//                        if (itnames.Length == 0)
//                        {
//                            itnames = friendlyName;
//                        }
//                        else
//                        {
//                            itnames = string.Concat(itnames, ", ", friendlyName);
//                        }
//                    }
//                }

//                if (incidentTypeNamesByIncident.ContainsKey(itnames) == false)
//                {
//                    List<int> codes = new List<int>();
//                    codes.Add(code);
//                    incidentTypeNamesByIncident.Add(itnames, codes);
//                }
//                else
//                {
//                    List<int> codes = incidentTypeNamesByIncident[itnames];
//                    codes.Add(code);
//                }

//                SaveDepartment(departmentCode, incidentTypeNamesByIncident);
//                incidentTypeNamesByIncident.Clear();
            //            }
            #endregion doc
        }

            #region doc
//        private void SaveDepartment(int departmentCode, Dictionary<string, List<int>> incidentTypeNamesByIncident)
//        {
//            foreach (KeyValuePair<string, List<int>> kvp in incidentTypeNamesByIncident)
//            {
//                StringBuilder sb = new StringBuilder();
//                sb.Append("(");
//                foreach (int codeValue in kvp.Value)
//                {
//                    if (sb.Length == 1)
//                    {
//                        sb.Append(codeValue);
//                    }
//                    else
//                    {
//                        sb.Append(", ");
//                        sb.Append(codeValue);
//                    }
//                }
//                sb.Append(")");




//                string hql2 = @"select inc.Code, notif.EndDate, inc.StartDate
//                                    from DispatchOrderData data
//                                    inner join data.IncidentNotification notif
//                                    inner join notif.ReportBase repoBase
//                                    inner join repoBase.Incident inc
//                                    where notif.EndDate > '{0}' and
//                                        inc.Code in {1} and
//                                        notif.DepartmentType.Code = {2}
//                                    group by inc.Code, inc.StartDate, notif.EndDate";


//                IList incidentInfo = (IList)
//                    SmartCadDatabase.SearchObjects(
//                    SmartCadHqls.GetCustomHql(hql2,
//                    ApplicationUtil.GetDataBaseFormattedDate(SmartCadDatabase.GetTimeFromBD().Date),
//                    sb.ToString(),
//                    departmentCode));

//                double average = 0;

//                foreach (object[] item in incidentInfo)
//                {
//                    DateTime end = (DateTime)item[1];
//                    DateTime start = (DateTime)item[2];
//                    TimeSpan sub = end - start;
//                    average += sub.TotalSeconds;
//                }


//                if (average > 0)
//                {
//                    Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//                    List<double> list = new List<double>();

//                    list.Add(Convert.ToDouble(average / incidentInfo.Count));
//                    result.Add(kvp.Key, list);

//                    ObjectData od = new ObjectData();
//                    od.Code = departmentCode;
//                    SaveIndicatorMultiplesValues(od, IndicatorClassData.Department, result);
//                    result.Clear();
//                }
//            }
//        }
        
#endregion doc

        private void CalculateGroup()
        {
            #region new
            string sql = @" SELECT		SUM(DATEDIFF(SS,INC.START_DATE,INOT.END_DATE))/COUNT(INOT.END_DATE) AS FECHA
			,AGRUPADO.TIPO AS NOMBRE
			,OPE.CODE AS SUPERVISOR
FROM		INCIDENT_NOTIFICATION AS INOT
			,REPORT_BASE AS REB
			,INCIDENT AS INC
			,REPORT_BASE_INCIDENT_TYPE AS RBIT
			,INCIDENT_TYPE AS INCT
			,(
				SELECT
				INC_1.CODE AS CODIGO,
				TIPO = RTRIM(REPLACE(REPLACE(o.list,'<FRIENDLY_NAME>',''),'</FRIENDLY_NAME>',' , '))
				FROM
				INCIDENT AS INC_1
				CROSS APPLY
				(
				SELECT		INTY.FRIENDLY_NAME
				FROM		INCIDENT AS INC
							,REPORT_BASE AS REB
							,REPORT_BASE_INCIDENT_TYPE AS RBIT
							,INCIDENT_TYPE AS INTY
				WHERE		INC.CODE = REB.INCIDENT_CODE
				AND			REB.CODE = RBIT.REPORT_BASE_CODE
				AND			RBIT.INCIDENT_TYPE_CODE = INTY.CODE
				AND			INC.CODE = INC_1.CODE
				ORDER BY INTY.FRIENDLY_NAME
				FOR XML PATH('')
			) o (list)
			) AS AGRUPADO
			,OPERATOR_ASSIGN AS OPA
			,OPERATOR AS OPE
WHERE		CONVERT(DATETIME,CONVERT(CHAR(10),INOT.END_DATE,101),101) = CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
AND			INOT.INCIDENT_NOTIFICATION_STATUS_CODE=6
AND			INOT.REPORT_BASE_CODE=REB.CODE
AND			REB.INCIDENT_CODE=INC.CODE
AND			REB.CODE=RBIT.REPORT_BASE_CODE
AND			RBIT.INCIDENT_TYPE_CODE=INCT.CODE
AND			AGRUPADO.CODIGO = INC.CODE
AND			OPA.OPERATOR_CODE = INOT.USER_ACCOUNT_CODE
AND			OPA.DELETED_ID IS NULL
--AND			OPA.DAY = DATEPART(WEEKDAY,GETDATE())
AND			(
				(
				/*OPA.WORK_SHIFT_SCHEDULE_VARIATION_CODE IS NULL
				AND*/
				INOT.END_DATE
				BETWEEN OPA.START_DATE AND OPA.END_DATE
				)
			/*OR
				(
				OPA.WORK_SHIFT_SCHEDULE_VARIATION_CODE IS NOT NULL
				AND
				INOT.END_DATE 
				BETWEEN	OPA.START_DATE AND OPA.END_DATE
				)*/
			)
AND			OPE.CODE = OPA.SUPERVISOR_CODE 
GROUP BY	AGRUPADO.TIPO, OPE.CODE
order by ope.code";




            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);

            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
            List<double> list = new List<double>();
            int supervisorCode = 0;
            string incidentTypes = "";
            foreach (object[] item in data)
            {
                if (supervisorCode != 0 && supervisorCode != int.Parse(item[2].ToString()))
                {

                    ObjectData od1 = new ObjectData();
                    od1.Code = supervisorCode;
                    SaveIndicatorMultiplesValues(od1, IndicatorClassData.Group, result);
                    result.Clear();
                    incidentTypes = "";
                    list = new List<double>();
                }

                incidentTypes = item[1].ToString().Substring(0, item[1].ToString().Length - 1);
                list.Add(double.Parse(item[0].ToString()));
                result.Add(incidentTypes, list);
                list = new List<double>();
                supervisorCode = (int)item[2];

                if (item == data[data.Count - 1])
                {
                    ObjectData od1 = new ObjectData();
                    od1.Code = supervisorCode;
                    SaveIndicatorMultiplesValues(od1, IndicatorClassData.Group, result);
                }
            }
            #endregion new

            #region doc
//            IList supervisors = OperatorScheduleManager.GetSupervisorsWorkingNowByApplication("DispatchSupervisorName");

//            foreach (int supervisor in supervisors)
//            {
//                Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//                //double totalElementsResult = 0;
//                IList operators = OperatorScheduleManager.GetAllDayOperatorsBySupervisor(supervisor);
//                WorkShiftScheduleVariationData bshSupervisor = 
//                    OperatorScheduleManager.GetCurrentWorkShiftSchedulesByOperator(supervisor, SmartCadDatabase.GetTimeFromBD().Date);
//                ArrayList incidentInfoGroup = new ArrayList();
//                foreach (OperatorData operatorData in operators)
//                {
//                    //BaseSessionHistory bsh = OperatorScheduleManager.GetOperatorAssignBySupervisorAndByDayRealHours(
//                    //    supervisor, operatorData.Code);

//                    IList bshList =
//                      OperatorScheduleManager.GetAllDayOperatorAssignBySupervisorAndByDayRealHours(supervisor, operatorData.Code);

//                    foreach (OperatorAssignData bsh in bshList)
//                    {
//                        if ((bshSupervisor != null) &&
//                            (((bsh.StartDate.TimeOfDay >= bshSupervisor.Start.Value.TimeOfDay) &&
//                           (bsh.StartDate.TimeOfDay <= bshSupervisor.End.Value.TimeOfDay)) ||
//                           ((bsh.EndDate.TimeOfDay <= bshSupervisor.End.Value.TimeOfDay) &&
//                           (bsh.EndDate.TimeOfDay >= bshSupervisor.Start.Value.TimeOfDay))))
//                        {
//                            string hql = @"select distinct data.Code, sit.FriendlyName 
//                           from IncidentData data inner join
//                                data.SetReportBaseList srl inner join
//                                srl.SetIncidentTypes sit inner join
//                                srl.IncidentNotifications inot inner join
//                                inot.SetDispatchOrders dos
//                           where inot.EndDate > '{0}' and
//                                 inot.EndDate <= '{1}' and    
//                                 inot.DispatchOperator.Code = {2}
//                           order by data.Code, sit.FriendlyName";

//                            IList incidentInfo = (IList)
//                                SmartCadDatabase.SearchBasicObjects(
//                                SmartCadHqls.GetCustomHql(hql,
//                                ApplicationUtil.GetDataBaseFormattedDate(bsh.StartDate),
//                                ApplicationUtil.GetDataBaseFormattedDate(bsh.EndDate),
//                                operatorData.Code));
//                            incidentInfoGroup.AddRange(incidentInfo);
//                        }
//                    }
//                }
//                    Dictionary<string, List<int>> incidentTypeNamesByIncident = new Dictionary<string, List<int>>();
//                    string itnames = "";
//                    int code = 0;
//                    foreach (object[] item in incidentInfoGroup)
//                    {
//                        if (code != 0 && code != int.Parse(item[0].ToString()))
//                        {
//                            if (incidentTypeNamesByIncident.ContainsKey(itnames) == false)
//                            {
//                                List<int> codes = new List<int>();
//                                codes.Add(code);
//                                incidentTypeNamesByIncident.Add(itnames, codes);
//                            }
//                            else
//                            {
//                                List<int> codes = incidentTypeNamesByIncident[itnames];
//                                codes.Add(code);
//                            }
//                            itnames = string.Empty;
//                        }

//                        code = int.Parse(item[0].ToString());
//                        string friendlyName = item[1].ToString();
//                        if (itnames.Contains(friendlyName) == false)
//                        {
//                            if (itnames.Length == 0)
//                            {
//                                itnames = friendlyName;
//                            }
//                            else
//                            {
//                                itnames = string.Concat(itnames, ", ", friendlyName);
//                            }
//                        }
//                    }

//                    if (incidentTypeNamesByIncident.ContainsKey(itnames) == false)
//                    {
//                        List<int> codes = new List<int>();
//                        codes.Add(code);
//                        incidentTypeNamesByIncident.Add(itnames, codes);
//                    }
//                    else
//                    {
//                        List<int> codes = incidentTypeNamesByIncident[itnames];
//                        codes.Add(code);
//                    }

//                    SaveGroup(supervisor, incidentTypeNamesByIncident);
//                    incidentTypeNamesByIncident.Clear();
                
//            }
            #endregion doc
        }
        
        
            #region doc
//        private void SaveGroup(int supervisorCode, Dictionary<string, List<int>> incidentTypeNamesByIncident)
//        {
//            foreach (KeyValuePair<string, List<int>> kvp in incidentTypeNamesByIncident)
//            {
//                StringBuilder sb = new StringBuilder();
//                sb.Append("(");
//                foreach (int codeValue in kvp.Value)
//                {
//                    if (sb.Length == 1)
//                    {
//                        sb.Append(codeValue);
//                    }
//                    else
//                    {
//                        sb.Append(", ");
//                        sb.Append(codeValue);
//                    }
//                }
//                sb.Append(")");




//                string hql2 = @"select inc.Code, notif.EndDate, inc.StartDate
//                                    from DispatchOrderData data
//                                    inner join data.IncidentNotification notif
//                                    inner join notif.ReportBase repoBase
//                                    inner join repoBase.Incident inc
//                                    where notif.EndDate > '{0}' and
//                                        inc.Code in {1} 
//                                    group by inc.Code, inc.StartDate, notif.EndDate";


//                IList incidentInfo = (IList)
//                    SmartCadDatabase.SearchObjects(
//                    SmartCadHqls.GetCustomHql(hql2,
//                    ApplicationUtil.GetDataBaseFormattedDate(SmartCadDatabase.GetTimeFromBD().Date),
//                    sb.ToString()));

//                double average = 0;

//                foreach (object[] item in incidentInfo)
//                {
//                    DateTime end = (DateTime)item[1];
//                    DateTime start = (DateTime)item[2];
//                    TimeSpan sub = end - start;
//                    average += sub.TotalSeconds;
//                }


//                if (average > 0)
//                {
//                    Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//                    List<double> list = new List<double>();

//                    list.Add(Convert.ToDouble(average / incidentInfo.Count));
//                    result.Add(kvp.Key, list);

//                    ObjectData od = new ObjectData();
//                    od.Code = supervisorCode;
//                    SaveIndicatorMultiplesValues(od, IndicatorClassData.Group, result);
//                    result.Clear();
//                }
//            }
//        }
        #endregion doc


    }
}
