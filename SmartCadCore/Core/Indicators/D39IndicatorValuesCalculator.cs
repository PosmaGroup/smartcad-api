//using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Collections;
//using SmartCadCore.Model;

//namespace SmartCadCore.Core.Indicators
//{
//    public class D39IndicatorValuesCalculator : IndicatorValuesCalculator
//    {
//        protected override string GetIndicator()
//        {
//            return "D39";
//        }

//        protected override void Calculate()
//        {
//            CalculateGroup();
//            CalculateDepartment();
//        }

//        private void CalculateGroup()
//        {
//            #region new
//            string sql = @" SELECT A.CANTIDAD AS CANTIDAD, ROUND(A.CANTIDAD*100/B.TOTAL,2) AS PORCENTAJE,
//		A.ESTADO AS ESTADO, A.SUPERVISOR AS SUPERVISOR
//FROM
//(SELECT		CONVERT(FLOAT,COUNT(INOT.CODE)) AS CANTIDAD
//			,INS.FRIENDLY_NAME AS ESTADO
//			,OPE.CODE AS SUPERVISOR
//FROM		INCIDENT_NOTIFICATION AS INOT
//			,INCIDENT_NOTIFICATION_STATUS  AS INS
//			,OPERATOR_ASSIGN AS OPA
//			,OPERATOR AS OPE
//WHERE		INOT.INCIDENT_NOTIFICATION_STATUS_CODE = INS.CODE
//AND			(	
//				CONVERT(DATETIME,CONVERT(CHAR(10),INOT.END_DATE, 101),101) = CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(), 101),101) 
//				OR 
//				INOT.END_DATE IS NULL
//			)
//AND			INOT.USER_ACCOUNT_CODE = OPA.OPERATOR_CODE
//AND			OPA.DELETED_ID IS NULL
//AND			(
//				GETDATE()
//				BETWEEN OPA.START_DATE AND OPA.END_DATE
//			)
//AND			OPA.SUPERVISOR_CODE = OPE.CODE
//GROUP BY	INS.FRIENDLY_NAME,OPE.CODE) AS A,
//(
//SELECT		COUNT(DISTINCT INOT.CODE) AS TOTAL
//			,OPE.CODE AS SUPERVISOR
//FROM		INCIDENT_NOTIFICATION AS INOT
//			,INCIDENT_NOTIFICATION_STATUS  AS INS
//			,OPERATOR_ASSIGN AS OPA
//			,OPERATOR AS OPE
//WHERE		(	
//				CONVERT(DATETIME,CONVERT(CHAR(10),INOT.END_DATE, 101),101) = CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(), 101),101) 
//				OR 
//				INOT.END_DATE IS NULL
//			)
//AND			INOT.USER_ACCOUNT_CODE = OPA.OPERATOR_CODE
//AND			OPA.DELETED_ID IS NULL
//AND			(
//				GETDATE()
//				BETWEEN OPA.START_DATE AND OPA.END_DATE
//			)
//AND			OPA.SUPERVISOR_CODE = OPE.CODE
//GROUP BY	OPE.CODE
//) AS B
//WHERE
//A.SUPERVISOR = B.SUPERVISOR
//ORDER BY A.SUPERVISOR
//";

//            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);

//            foreach (object[] item in data)
//            {
//                Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//                List<double> resultsValues = new List<double>();
//                resultsValues.Add(double.Parse(item[0].ToString()));
//                resultsValues.Add(double.Parse(item[1].ToString()));
//                result.Add(item[2].ToString(), resultsValues);
//                ObjectData od1 = new ObjectData();
//                od1.Code = int.Parse(item[3].ToString());
//                SaveIndicatorMultiplesValues(od1, IndicatorClassData.Group, result);
//            }
//            #endregion new

//            #region doc
//            //IList supervisors = OperatorScheduleManager.GetSupervisorsWorkingNowByApplication("DispatchSupervisorName");

//            //foreach (int supervisor in supervisors)
//            //{
//            //    IList operators = OperatorScheduleManager.GetAllDayOperatorsBySupervisor(supervisor);

//            //    WorkShiftScheduleVariationData bshSupervisor =
//            //       OperatorScheduleManager.GetCurrentWorkShiftSchedulesByOperator(supervisor, SmartCadDatabase.GetTimeFromBD().Date);


//            //    double totalIncidentNotification = 0;
//            //    double totalClosedIncidentNotification = 0;
//            //    double percentClosedIncidentNotification = 0;
//            //    foreach (OperatorData operatorData in operators)
//            //    {
//            //        IList bshList =
//            //          OperatorScheduleManager.GetAllDayOperatorAssignBySupervisorAndByDayRealHours(supervisor, operatorData.Code);

//            //        foreach (OperatorAssignData bsh in bshList)
//            //        {
//            //            if ((bshSupervisor != null) &&
//            //                (((bsh.StartDate.TimeOfDay >= bshSupervisor.Start.Value.TimeOfDay) &&
//            //               (bsh.StartDate.TimeOfDay <= bshSupervisor.End.Value.TimeOfDay)) ||
//            //               ((bsh.EndDate.TimeOfDay <= bshSupervisor.End.Value.TimeOfDay) &&
//            //               (bsh.EndDate.TimeOfDay >= bshSupervisor.Start.Value.TimeOfDay))))
//            //            {
//            //                IList incidentsNotification = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(
//            //                    SmartCadHqls.GetIncidentNotificationByEndDateByOperator,
//            //                     operatorData.Code,
//            //                     ApplicationUtil.GetDataBaseFormattedDate(bsh.StartDate),
//            //                     ApplicationUtil.GetDataBaseFormattedDate(bsh.EndDate)));

//            //                foreach (IncidentNotificationData incidentnotification in incidentsNotification)
//            //                {
//            //                    if (incidentnotification.Status == IncidentNotificationStatusData.Closed)
//            //                    {
//            //                        totalClosedIncidentNotification += 1;
//            //                    }
//            //                    //else if (incidentnotification.Status == IncidentNotificationStatusData.Cancelled)
//            //                    //{
//            //                    //    if (incidentnotification.EndDate < DateTime.Today)
//            //                    //    {
//            //                    //        totalIncidentNotification -= 1;
//            //                    //    }
//            //                    //}
//            //                }
//            //                totalIncidentNotification += incidentsNotification.Count;
//            //            }
//            //        }
//            //    }

//            //    if (totalIncidentNotification > 0)
//            //    {
//            //        percentClosedIncidentNotification = (totalClosedIncidentNotification * 100) / (totalIncidentNotification);
//            //    }
//            //    else
//            //    {
//            //        percentClosedIncidentNotification = 0;
//            //    }

//            //    List<double> resultsValues = new List<double>();
//            //    resultsValues.Add(totalClosedIncidentNotification);
//            //    resultsValues.Add(percentClosedIncidentNotification);
//            //    Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//            //    result.Add("ClosedIncidents", resultsValues);
//            //    ObjectData objectdata = new ObjectData();
//            //    objectdata.Code = supervisor;
//            //    SaveIndicatorMultiplesValues(objectdata, IndicatorClassData.Group, result);        
//            //}
//            #endregion doc
//        }

//        //ArrayList departmentsAllZeroValue = new ArrayList() { 1, 2, 3, 4, 5, 6, 7, 8 };
//        private void CalculateDepartment()
//        {
//            #region new
//            string sql = @" SELECT A.CANTIDAD AS CANTIDAD, ROUND(A.CANTIDAD*100/B.TOTAL,2) AS PORCENTAJE,
//		A.ESTADO AS ESTADO, A.ORGANISMO AS ORGANISMO
//FROM
//(
//SELECT		CONVERT(FLOAT,COUNT(INOT.CODE)) AS CANTIDAD
//			,INS.FRIENDLY_NAME AS ESTADO
//			,DETY.CODE AS ORGANISMO
//FROM		INCIDENT_NOTIFICATION AS INOT
//			,INCIDENT_NOTIFICATION_STATUS  AS INS
//			,DEPARTMENT_TYPE AS DETY
//WHERE		INOT.INCIDENT_NOTIFICATION_STATUS_CODE = INS.CODE
//AND			(CONVERT(DATETIME,CONVERT(CHAR(10),INOT.END_DATE, 101),101) = CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(), 101),101) OR INOT.END_DATE IS NULL)
//AND			INOT.DEPARTMENT_TYPE_CODE = DETY.CODE
//GROUP BY	INS.FRIENDLY_NAME,DETY.CODE
//) AS A,
//(
//SELECT		COUNT(DISTINCT INOT.CODE) AS TOTAL
//			,DETY.CODE AS ORGANISMO
//FROM		INCIDENT_NOTIFICATION AS INOT
//			,DEPARTMENT_TYPE AS DETY
//WHERE		(CONVERT(DATETIME,CONVERT(CHAR(10),INOT.END_DATE, 101),101) = CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(), 101),101) OR INOT.END_DATE IS NULL)
//AND			INOT.DEPARTMENT_TYPE_CODE = DETY.CODE
//GROUP BY	DETY.CODE) AS B
//WHERE A.ORGANISMO = B.ORGANISMO
//ORDER BY ORGANISMO
//
//";

//            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);

//            foreach (object[] item in data)
//            {
//                Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//                List<double> resultsValues = new List<double>();
//                resultsValues.Add(double.Parse(item[0].ToString()));
//                resultsValues.Add(double.Parse(item[1].ToString()));
//                result.Add(item[2].ToString(), resultsValues);
//                ObjectData od1 = new ObjectData();
//                od1.Code = int.Parse(item[3].ToString());
//                SaveIndicatorMultiplesValues(od1, IndicatorClassData.Department, result);
//            }
//            #endregion new
            
//            #region doc
//            //foreach (int item in departmentsAllZeroValue)
//            //{
//            //    Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//            //    List<double> resultsValues = new List<double>();
//            //    resultsValues.Add(0);
//            //    resultsValues.Add(0);
//            //    result.Add("Empty", resultsValues);
//            //    ObjectData od = new ObjectData();
//            //    od.Code = item;
//            //    SaveIndicatorMultiplesValues(od, IndicatorClassData.Department, result);
//            //}

//            //IList IncidentNotificationByDepartment = (IList)SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(
//            //       SmartCadHqls.GetIncidentNotificationCountByEndDateByDepartmentByStatus,
//            //       IncidentNotificationStatusData.Closed.Code, ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today)));


//            //double percentOpenIncidents = 0;

//            //for (int i = 0; i < IncidentNotificationByDepartment.Count; i++)
//            //{
//            //    Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//            //    List<double> resultsValues = new List<double>();

//            //    int currentDTCode = (int)((object[])IncidentNotificationByDepartment[i])[0];
//            //    object[] item = (object[])IncidentNotificationByDepartment[i];

//            //    double totalIncidents = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(
//            //           SmartCadHqls.GetAllIncidentNotificationByDepartment, currentDTCode, 
//            //           ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today)));

//            //    if (totalIncidents > 0)
//            //    {
//            //        percentOpenIncidents = (Convert.ToDouble(item[1]) * 100) / (totalIncidents);
//            //    }
//            //    else
//            //    {
//            //        percentOpenIncidents = 0;
//            //    }

//            //    resultsValues.Add(Convert.ToDouble(item[1]));
//            //    resultsValues.Add(percentOpenIncidents);
//            //    result.Add("ClosedIncidentsByDepartment", resultsValues);
//            //    ObjectData od = new ObjectData();
//            //    od.Code = currentDTCode;
//            //    SaveIndicatorMultiplesValues(od, IndicatorClassData.Department, result);
//            //}
//            #endregion doc
//        }



//    }
//}
