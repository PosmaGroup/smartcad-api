using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using System.Collections;

namespace SmartCadCore.Core.Indicators
{
    public class PN07IndicatorValuesCalculator : IndicatorValuesCalculator
    {
        private double emergencyCloseByGroupTotalValue = 0;

        protected override string GetIndicator()
        {
            return "PN07";
        }

        protected override void Calculate()
        {
            CalculateSystem();
            CalculateGroup();
            CalculateOperators();
        }

        private void CalculateSystem()
        {
            #region new
            string sql = @" SELECT	(CONVERT(FLOAT,RESUELTAS.CANTIDAD))/CONVERT(FLOAT,TOTAL.CANTIDAD) AS CANTIDAD 
FROM
		(
			SELECT		COUNT(PHR.FINISHED_REPORT_TIME) AS CANTIDAD
			FROM		INCIDENT AS INC
						,REPORT_BASE AS REB
						,PHONE_REPORT AS PHR
			WHERE		INC.IS_EMERGENCY = 'TRUE'
			AND			(INC.START_DATE <> INC.END_DATE OR INC.END_DATE IS NULL)
			AND			INC.CODE = REB.INCIDENT_CODE
			AND			REB.CODE = PHR.PARENT_CODE
			AND			CONVERT(DATETIME,CONVERT(CHAR(10),PHR.FINISHED_REPORT_TIME,101),101)= 
						CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
		) AS RESUELTAS,
		(
			SELECT		CASE COUNT(PHR.FINISHED_REPORT_TIME) WHEN 0 THEN 1 ELSE COUNT(PHR.FINISHED_REPORT_TIME) END AS CANTIDAD
			FROM		INCIDENT AS INC
						,REPORT_BASE AS REB
						,PHONE_REPORT AS PHR
			WHERE		INC.IS_EMERGENCY = 'TRUE'
			AND			INC.CODE = REB.INCIDENT_CODE
			AND			REB.CODE = PHR.PARENT_CODE
			AND			CONVERT(DATETIME,CONVERT(CHAR(10),PHR.FINISHED_REPORT_TIME,101),101)= 
						CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
		) AS TOTAL
";


            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);

            SaveIndicatorsValues(IndicatorClassData.System, double.Parse(data[0].ToString()));

            #endregion new


            #region doc
            //double opened = (long)SmartCadDatabase.SearchBasicObject(
            //                        SmartCadHqls.GetCustomHql(
            //                        SmartCadHqls.GetAllIncidentCreatedOpenedByDay,
            //                        ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today)));

            //double all = (long)SmartCadDatabase.SearchBasicObject(
            //                        SmartCadHqls.GetCustomHql(
            //                        SmartCadHqls.GetAllIncidentCreatedByDay,
            //                        ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today)));

            //double result = 0;

            //if (opened > 0 && all > 0)
            //    result = opened / all;

            //SaveIndicatorsValues(IndicatorClassData.System, result);
            #endregion doc
        }

        private void CalculateOperators()
        {
            #region new
            string sql = @" SELECT	(CONVERT(FLOAT,RESUELTAS.CANTIDAD))/CONVERT(FLOAT,TOTAL.CANTIDAD) AS CANTIDAD  , TOTAL.USUARIO AS USUARIO
FROM
		(
			SELECT		COUNT(PHR.FINISHED_REPORT_TIME) AS CANTIDAD, OPE.CODE AS USUARIO
			FROM		INCIDENT AS INC
						,REPORT_BASE AS REB
						,PHONE_REPORT AS PHR
						,OPERATOR AS OPE
			WHERE		INC.IS_EMERGENCY = 'TRUE'
			AND			(INC.START_DATE <> INC.END_DATE OR INC.END_DATE IS NULL)
			AND			INC.CODE = REB.INCIDENT_CODE
			AND			REB.CODE = PHR.PARENT_CODE
			AND			REB.OPERATOR_CODE = OPE.CODE
			AND			CONVERT(DATETIME, CONVERT(CHAR(10),PHR.FINISHED_REPORT_TIME, 101),101) = 
						CONVERT(DATETIME, CONVERT(CHAR(10), GETDATE(), 101),101)
			GROUP BY	OPE.CODE					 
		) AS RESUELTAS,
		(
			SELECT		CASE COUNT(PHR.FINISHED_REPORT_TIME) WHEN 0 THEN 1 ELSE COUNT(PHR.FINISHED_REPORT_TIME) END AS CANTIDAD
						,OPE.CODE AS USUARIO
			FROM		PHONE_REPORT AS PHR
						,REPORT_BASE AS REB
						,INCIDENT AS INC
						,OPERATOR AS OPE
			WHERE		PHR.PARENT_CODE = REB.CODE
			AND			REB.INCIDENT_CODE = INC.CODE
			AND			INC.IS_EMERGENCY = 'TRUE'
			AND			REB.OPERATOR_CODE = OPE.CODE												
			AND			CONVERT(DATETIME, CONVERT(CHAR(10), PHR.PICKED_UP_CALL_TIME, 101),101)= 
						CONVERT(DATETIME, CONVERT(CHAR(10), GETDATE(), 101),101)
			GROUP BY	OPE.CODE
		) AS TOTAL
WHERE	TOTAL.USUARIO = RESUELTAS.USUARIO
";

            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);

            foreach (object[] item in data)
            {
                ObjectData od1 = new ObjectData();
                od1.Code = int.Parse(item[1].ToString());
                SaveIndicatorsValues(od1, IndicatorClassData.Operator,
                    double.Parse(item[0].ToString()));
            }
            #endregion new

            #region doc
            //IList operators = OperatorScheduleManager.GetOperatorsWorkingNowByApplication(UserApplicationData.FirstLevel.Name, false);
            //foreach (int operatorCode in operators)
            //{
            //    double result = 0;

            //    double opened = (long)SmartCadDatabase.SearchBasicObject(
            //        SmartCadHqls.GetCustomHql(
            //        SmartCadHqls.GetIncidentCreatedOpenedByOperator,
            //        operatorCode,
            //        ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today)));

            //    double all = (long)SmartCadDatabase.SearchBasicObject(
            //        SmartCadHqls.GetCustomHql(
            //        SmartCadHqls.GetIncidentCreatedByOperator,
            //        operatorCode,
            //        ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today)));

            //    if (opened > 0 && all > 0)
            //    {
            //        result = opened / all;
            //    }

            //    ObjectData objectData = new ObjectData();
            //    objectData.Code = operatorCode;
            //    SaveIndicatorsValues(objectData, IndicatorClassData.Operator, result);
            //}
            #endregion doc
        }

        private void CalculateGroup()
        {
            #region new
            string sql = @" SELECT	(CONVERT(FLOAT,RESUELTAS.CANTIDAD))/CONVERT(FLOAT,TOTAL.CANTIDAD) AS CANTIDAD  , TOTAL.USUARIO AS USUARIO
FROM
		(
			SELECT		COUNT(PHR.FINISHED_REPORT_TIME) AS CANTIDAD, OPE.CODE AS USUARIO
			FROM		INCIDENT AS INC
						,REPORT_BASE AS REB
						,PHONE_REPORT AS PHR
						,OPERATOR_ASSIGN AS OPA
						,OPERATOR AS OPE
			WHERE		INC.IS_EMERGENCY = 'TRUE'
			AND			(INC.START_DATE <> INC.END_DATE OR INC.END_DATE IS NULL)
			AND			INC.CODE = REB.INCIDENT_CODE
			AND			REB.CODE = PHR.PARENT_CODE
			AND			REB.OPERATOR_CODE = OPA.OPERATOR_CODE
			AND			OPA.DELETED_ID IS NULL			
			AND			(
							(							
							PHR.FINISHED_REPORT_TIME
							BETWEEN OPA.START_DATE AND OPA.END_DATE
							)						
						)
			AND			OPA.SUPERVISOR_CODE = OPE.CODE
			AND			CONVERT(DATETIME, CONVERT(CHAR(10),PHR.FINISHED_REPORT_TIME, 101),101) = 
						CONVERT(DATETIME, CONVERT(CHAR(10), GETDATE(), 101),101)
			GROUP BY	OPE.CODE					 
		) AS RESUELTAS,
		(
			SELECT		CASE COUNT(PHR.FINISHED_REPORT_TIME) WHEN 0 THEN 1 ELSE COUNT(PHR.FINISHED_REPORT_TIME) END AS CANTIDAD
						,OPE.CODE AS USUARIO
			FROM		PHONE_REPORT AS PHR
						,REPORT_BASE AS REB
						,INCIDENT AS INC
						,OPERATOR_ASSIGN AS OPA
						,OPERATOR AS OPE
			WHERE		PHR.PARENT_CODE = REB.CODE
			AND			REB.INCIDENT_CODE = INC.CODE
			AND			INC.IS_EMERGENCY = 'TRUE'
			AND			REB.OPERATOR_CODE = OPA.OPERATOR_CODE
			AND			OPA.DELETED_ID IS NULL		
			AND			(
							(						
							PHR.FINISHED_REPORT_TIME
							BETWEEN OPA.START_DATE AND OPA.END_DATE
							)						
						)
			AND			OPA.SUPERVISOR_CODE = OPE.CODE											
			AND			CONVERT(DATETIME, CONVERT(CHAR(10), PHR.PICKED_UP_CALL_TIME, 101),101)= 
						CONVERT(DATETIME, CONVERT(CHAR(10), GETDATE(), 101),101)
			GROUP BY	OPE.CODE
		) AS TOTAL
WHERE	TOTAL.USUARIO = RESUELTAS.USUARIO
";

            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);

            foreach (object[] item in data)
            {
                ObjectData od1 = new ObjectData();
                od1.Code = int.Parse(item[1].ToString());
                SaveIndicatorsValues(od1, IndicatorClassData.Group,
                    double.Parse(item[0].ToString()));
            }
            #endregion new

            #region doc
            //IList supervisors = OperatorScheduleManager.GetSupervisorsWorkingNowByApplication("FirstLevelSupervisorName");
            //Dictionary<int, double> results = new Dictionary<int, double>();
            //foreach (int supervisorCode in supervisors)
            //{
            //    IList operators = OperatorScheduleManager.GetAllDayOperatorsBySupervisor(supervisorCode);
            //    WorkShiftScheduleVariationData bshSupervisor =
            //       OperatorScheduleManager.GetCurrentWorkShiftSchedulesByOperator(supervisorCode, SmartCadDatabase.GetTimeFromBD().Date);

            //    double total = 0;
            //    double totalOpen = 0;
            //    foreach (OperatorData operatorData in operators)
            //    {

            //        //BaseSessionHistory bsh =
            //        //    OperatorScheduleManager.GetOperatorAssignBySupervisorAndByDayRealHours(supervisorCode, operatorData.Code);
            //        IList bshList =
            //          OperatorScheduleManager.GetAllDayOperatorAssignBySupervisorAndByDayRealHours(supervisorCode, operatorData.Code);
            //        foreach (OperatorAssignData bsh in bshList)
            //        {
            //            if ((bshSupervisor != null) && 
            //                (((bsh.StartDate.TimeOfDay >= bshSupervisor.Start.Value.TimeOfDay) &&
            //               (bsh.StartDate.TimeOfDay <= bshSupervisor.End.Value.TimeOfDay)) || 
            //               ((bsh.EndDate.TimeOfDay <= bshSupervisor.End.Value.TimeOfDay) &&
            //               (bsh.EndDate.TimeOfDay >= bshSupervisor.Start.Value.TimeOfDay))))
            //            {
            //                double opened = (long)SmartCadDatabase.SearchBasicObject(
            //                    SmartCadHqls.GetCustomHql(
            //                    SmartCadHqls.GetIncidentCreatedOpenedByOperatorByWorkShift,
            //                    operatorData.Code,
            //                    ApplicationUtil.GetDataBaseFormattedDate(bsh.StartDate),
            //                    ApplicationUtil.GetDataBaseFormattedDate(bsh.EndDate)));

            //                double all = (long)SmartCadDatabase.SearchBasicObject(
            //                    SmartCadHqls.GetCustomHql(
            //                    SmartCadHqls.GetIncidentCreatedByOperatorByWorkShift,
            //                    operatorData.Code,
            //                    ApplicationUtil.GetDataBaseFormattedDate(bsh.StartDate),
            //                    ApplicationUtil.GetDataBaseFormattedDate(bsh.EndDate)));

            //                totalOpen += opened;
            //                total += all;
            //            }
            //        }
            //    }
            //    if (totalOpen > 0 && total > 0)
            //    {
            //        total = totalOpen / total;
            //    }
            //    else
            //    {
            //        total = 0;
            //    }

            //    ObjectData objectData = new ObjectData();
            //    objectData.Code = supervisorCode;
            //    SaveIndicatorsValues(objectData, IndicatorClassData.Group, total);
            //}
            #endregion doc
        }
    }
}
