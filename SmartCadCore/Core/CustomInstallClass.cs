using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration.Install;
using System.ComponentModel;
using System.Security.AccessControl;
using System.IO;
using System.Reflection;

namespace SmartCadCore.Core
{
    #region Class CustomInstallClass Documentation
    /// <summary>
    /// 
    /// </summary>
    /// <className>CustomInstallClass</className>
    /// <author email="fsilva@smartmatic.com">Fernando Silva</author>
    /// <date>2007/05/14</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    [RunInstaller(true)]
    public class CustomInstallClass : Installer
    {
        public CustomInstallClass()
            : base()
        {
            this.AfterInstall += new InstallEventHandler(CustomInstallClass_AfterInstall);
        }

        private void CustomInstallClass_AfterInstall(object sender, InstallEventArgs e)
        {
            try
            {
                string directoryPath = Directory.GetParent(Assembly.GetExecutingAssembly().Location).Parent.FullName;

                DirectorySecurity directorySecurity = Directory.GetAccessControl(directoryPath, AccessControlSections.All);

                try
                {
                    directorySecurity.AddAccessRule(
                            new FileSystemAccessRule(
                                @"Everyone",
                                FileSystemRights.FullControl,
                                InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit,
                                PropagationFlags.None,
                                AccessControlType.Allow));
                }
                catch
                {
                }

                try
                {
                    directorySecurity.AddAccessRule(
                            new FileSystemAccessRule(
                                @"Todos",
                                FileSystemRights.FullControl,
                                InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit,
                                PropagationFlags.None,
                                AccessControlType.Allow));
                }
                catch
                {
                }

                Directory.SetAccessControl(directoryPath, directorySecurity);
            }
            catch
            {
            }
        }
    }
}
