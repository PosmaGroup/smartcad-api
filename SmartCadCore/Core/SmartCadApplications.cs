﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Core
{
    public class SmartCadApplications 
    {
        
        public static string SmartCadAlarm = "Alarm";
        public static string SmartCadAdministration = "Administration";
        public static string SmartCadMap = "Map";
        public static string SmartCadDispatch = "Dispatch";
        public static string SmartCadReports = "Reports";
        public static string SmartCadFirstLevel = "First Level";
        public static string SmartCadCore = "Core";
        public static string SmartCadCCTV = "CCTV";
        public static string SmartCadSupervision = "Supervision";
        public static string SmartCadParser = "Parser";
        public static string SmartCadTelephony = "Telephony";
        public static string SmartCadLPR = "Lpr";
    }
}
