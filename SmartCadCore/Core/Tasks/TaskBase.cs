using System;
using System.Collections;
using System.Text;

namespace SmartCadCore.Core
{
    [Serializable]
    public abstract class TaskBase
    {
        protected IList parameters;

        public IList Parameters
        {
            get
            {
                return parameters;
            }
        }

        public TaskBase(IList parameters)
        {
            this.parameters = parameters;
        }

        public abstract IList Run();
    }
}
