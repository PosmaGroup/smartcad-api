using System;
using System.Collections;
using System.Text;
using System.Drawing;
using System.Linq;

using SmartCadCore.Model;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using SmartCadCore.ClientData;
using SmartCadCore.Common;

namespace SmartCadCore.Core
{
    [Serializable]
    public class SelectedIncidentTask : TaskBase
    {
        public SelectedIncidentTask(IList parameters)
            : base(parameters)
        {
        }

        public override IList Run()
        {
            ArrayList result = new ArrayList();
            
            int incidentCode = (int)parameters[0];
            //SmartLogger.Print("Begin SelectedIncidentTask");
            string incidentReportSqlQuery = null;
            object res = null;
             IncidentData incident = (SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(
                    SmartCadHqls.GetIncidentByCode, incidentCode)) as IncidentData);

             if (incident.SourceIncidentApp == UserApplicationData.FirstLevel)
             {
                 try
                 {

                     long count = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(SmartCadHqls.ReportBaseCountIncident, incidentCode));
                     if (count <= 200)
                     {
                         incidentReportSqlQuery = File.ReadAllText(SmartCadConfiguration.IncidentSql);                         
                         StringWriter writer = new StringWriter();
                         writer.Write(incidentReportSqlQuery, incidentCode);
                         incidentReportSqlQuery = writer.ToString();
                         res = SmartCadDatabase.SearchBasicObjects(incidentReportSqlQuery, false);
                     }

                 }
                 catch (Exception ex)
                 {
                     try
                     {
                         SmartLogger.Print(ex);
                     }
                     catch
                     { }
                 }

                 if (res != null)
                 {
                     string incidentReportXml = (res as ArrayList)[0] as string;
                     SelectedIncidentTaskResult selectedIncidentLightData = new SelectedIncidentTaskResult();
                     selectedIncidentLightData.Xml = incidentReportXml;
                     selectedIncidentLightData.TotalDispatchOrders = BuildDispatchOrders(incidentCode);
                     selectedIncidentLightData.TotalPhoneReports = BuildPhoneReports(incidentCode, incidentReportXml);
                     selectedIncidentLightData.SourceApplication = UserApplicationData.FirstLevel.Name;
                     result.Add(selectedIncidentLightData);
                     //SmartLogger.Print("End SelectedIncidentTask");
                 }
             }
             else 
             {

                 try
                 {
                     if (incident.SourceIncidentApp == UserApplicationData.Cctv)
                     {
                         incidentReportSqlQuery = File.ReadAllText(SmartCadConfiguration.CctvIncident);
                     }
                     else
                     {
                         incidentReportSqlQuery = File.ReadAllText(SmartCadConfiguration.AlarmIncident);
                     }
                     StringWriter writer = new StringWriter();
                     writer.Write(incidentReportSqlQuery, incidentCode);
                     incidentReportSqlQuery = writer.ToString();
                     res = SmartCadDatabase.SearchBasicObjects(incidentReportSqlQuery, false);                     
                 }
                 catch (Exception ex)
                 {
                     try
                     {
                         SmartLogger.Print(ex);
                     }
                     catch
                     { }
                 }

                 if (res != null)
                 {
                     string incidentReportXml = (res as ArrayList)[0] as string;
                     SelectedIncidentTaskResult selectedIncidentLightData = new SelectedIncidentTaskResult();
                     selectedIncidentLightData.Xml = incidentReportXml;
                     selectedIncidentLightData.TotalDispatchOrders = BuildDispatchOrders(incidentCode);
                     selectedIncidentLightData.TotalPhoneReports = new ArrayList();
                     if (incident.SourceIncidentApp == UserApplicationData.Cctv)
                     {
                         selectedIncidentLightData.SourceApplication = UserApplicationData.Cctv.Name;
                     }
                     else
                     {
                         selectedIncidentLightData.SourceApplication = UserApplicationData.Alarm.Name;
                     }
                     result.Add(selectedIncidentLightData);
                     //SmartLogger.Print("End SelectedIncidentTask");
                     
                 }
             }
            return result;
        }

        #region Methods to calculate associated information to an incident(Dispatchs and Calls)
        private IList BuildPhoneReports(int incidentCode, string incidentReportXml)
        {
            IList result = new ArrayList();
            IOrderedEnumerable<PhoneReportData> tempResult = null; 

            XmlDocument document = new XmlDocument();
            document.LoadXml(incidentReportXml);

            tempResult = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetPhoneReportsByIncidentCode, incidentCode)).Cast < PhoneReportData>().OrderBy(phoneReport => phoneReport.ReceivedCallTime);
            int order = 0;
            foreach (PhoneReportData phoneReport in tempResult)
            {
                PhoneReportClientData phoneReportLight = new PhoneReportClientData();
                phoneReportLight.OrderOfCall = ++order;
                phoneReportLight.RegisteredCallTime = phoneReport.RegisteredCallTime.Value;
                phoneReportLight.PhoneReportLineClient = PhoneReportLineConverter.ToClient(phoneReport.Line, UserApplicationData.Report);
                phoneReportLight.PhoneReportCallerClient = PhoneReportCallerConversion.ToClient(phoneReport.Caller, UserApplicationData.Report);
                phoneReportLight.IncidentTypesText = GetIncidentTypeCustomCodes(phoneReport);
                phoneReportLight.Xml = "";
                try
                {
                    XmlNode node = document.SelectSingleNode(@"/incident/phone-reports/phone-report[@custom-code='" + phoneReport.CustomCode + "']");
                    XmlAttribute orderAttribute = document.CreateAttribute("call-order");
                    orderAttribute.Value = phoneReportLight.OrderOfCall.ToString();
                    node.Attributes.Append(orderAttribute);
                    phoneReportLight.Xml = node.OuterXml;
                }
                catch(Exception ex)
                {
                    SmartLogger.Print(ex);
                }

                phoneReportLight.CustomCode = phoneReport.CustomCode;
                result.Add(phoneReportLight);
            }
            return result;
        }
    

        private string BuildPhoneReportXml(PhoneReportData phoneReport)
        {
            StringWriter writer = new StringWriter();
            XmlTextWriter xml = new XmlTextWriter(writer);
            xml.WriteStartElement("phoneReport");

            xml.WriteAttributeString("pickedUpCallTime", phoneReport.PickedUpCallTime.Value.ToString());
            if (phoneReport.Incomplete.HasValue == true)
            {
                xml.WriteAttributeString("complete", phoneReport.Incomplete.Value.ToString());
            }
            xml.WriteAttributeString("attendedBy", phoneReport.Operator.Login);

            xml.WriteStartElement("caller");

            bool anonymous = false;
            if (phoneReport.Caller.Anonymous.HasValue == true)
                anonymous = true;
            xml.WriteAttributeString("anonymous", anonymous.ToString());
            xml.WriteAttributeString("phone", phoneReport.Caller.Telephone);

            xml.WriteStartElement("address");

            xml.WriteAttributeString("zone", phoneReport.Caller.Address.Zone);
            xml.WriteAttributeString("street", phoneReport.Caller.Address.Street);
            xml.WriteAttributeString("reference", phoneReport.Caller.Address.Reference);
            xml.WriteAttributeString("more", phoneReport.Caller.Address.More);

            xml.WriteEndElement();
            xml.WriteEndElement();

            foreach (IncidentTypeData incidentType in phoneReport.SetIncidentTypes)
            {
                xml.WriteStartElement("incidentType");

                xml.WriteAttributeString("friendlyName", incidentType.FriendlyName);
                xml.WriteAttributeString("customCode", incidentType.CustomCode);

                xml.WriteEndElement();
            }

            foreach (PhoneReportAnswerData phoneReportAnswer in phoneReport.SetAnswers)
            {
                xml.WriteStartElement("question");

                xml.WriteAttributeString("text", phoneReportAnswer.Question.Text);

                foreach (PossibleAnswerAnswerData possibleAnswerAnswer in phoneReportAnswer.SetAnswers)
                {
                    xml.WriteStartElement("answer");

                    xml.WriteAttributeString("text", possibleAnswerAnswer.TextAnswer);

                    xml.WriteEndElement();
                }

                xml.WriteEndElement();
            }

            foreach (IncidentNotificationData incidentNotification in phoneReport.IncidentNotifications)
            {
                xml.WriteStartElement("incidentNotification");

                xml.WriteAttributeString("departmentTypeName", incidentNotification.DepartmentType.Name);
                xml.WriteAttributeString("priorityCustomCode", incidentNotification.Priority.CustomCode);

                xml.WriteEndElement();
            }

            xml.WriteEndElement();
            xml.Flush();
            return writer.ToString();
        }

        private static string GetIncidentTypeCustomCodes(PhoneReportData phoneReport)
        {            
            StringBuilder incidentTypeStr = new StringBuilder();
            int counter = 0;
            int incTypeLength = phoneReport.SetIncidentTypes.Count;
            foreach (IncidentTypeData incidentType in phoneReport.SetIncidentTypes)
            {
                incidentTypeStr.Append(incidentType.CustomCode);
                counter++;
                if (counter < incTypeLength)
                    incidentTypeStr.Append(", ");

            }
            return incidentTypeStr.ToString();
        }

        private IList BuildDispatchOrders(int incidentCode)
        {
            IList result = new ArrayList();
            IList tempResult = new ArrayList();

            tempResult = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetDispatchOrdersByIncidentCode, incidentCode));
            foreach (DispatchOrderData dispatchOrder in tempResult)
            {
                result.Add(DispatchOrderConversion.ToClient(dispatchOrder, UserApplicationData.Report));
            }
            return result;
        }

        #endregion

    }
}
