using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    [Serializable]
    public class SmartCadContext
    {
        private string operatorData;
        private string application;

        public SmartCadContext(string operatorData, string application)
        {
            this.operatorData = operatorData;
            this.application = application;
        }

        public string Operator
        {
            get
            {
                return operatorData;
            }
        }

        public string Application
        {
            get
            {
                return application;
            }
        }
    }
}
