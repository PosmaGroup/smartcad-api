﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    public class UnitOfficerAssignHistoryConversion
    {
        public static UnitOfficerAssignHistoryClientData ToClient(UnitOfficerAssignHistoryData uoaData, UserApplicationData app)
        {
            UnitOfficerAssignHistoryClientData uoaClient = new UnitOfficerAssignHistoryClientData();
            uoaClient.Code = uoaData.Code;
            uoaClient.Version = uoaData.Version;
            uoaClient.Unit = new UnitClientData();
            uoaClient.Unit.Code = uoaData.Unit.Code;
            uoaClient.Unit.CustomCode = uoaData.Unit.CustomCode;

            uoaClient.Officer = new OfficerClientData();
            uoaClient.Officer.Code = uoaData.Officer.Code;
            uoaClient.Officer.FirstName = uoaData.Officer.FirstName;
            uoaClient.Officer.LastName = uoaData.Officer.LastName;
            uoaClient.Officer.Badge = uoaData.Officer.Badge;
            uoaClient.Officer.PersonID = uoaData.Officer.PersonId;
            uoaClient.Officer.Birthday = uoaData.Officer.Birthday;
            
            uoaClient.Start = uoaData.Start;
            uoaClient.End = uoaData.End;

            return uoaClient;
        }

        public static UnitOfficerAssignHistoryData ToObject(UnitOfficerAssignHistoryClientData uoaClient, UserApplicationData app)
        {
            UnitOfficerAssignHistoryData uoaData = new UnitOfficerAssignHistoryData();
            uoaData.Code = uoaClient.Code;
            uoaData.Version = uoaClient.Version;
            uoaData.Unit = new UnitData() { Code = uoaClient.Unit.Code, CustomCode = uoaClient.Unit.CustomCode };
            uoaData.Officer = new OfficerData 
            {   Code = uoaClient.Officer.Code, 
                PersonId = uoaClient.Officer.PersonID, 
                DepartmentStation = DepartmentStationConversion.ToObject(uoaClient.Officer.DepartmentStation,app) 
            };
            uoaData.Start = uoaClient.Start;
            uoaData.End = uoaClient.End;

            return uoaData;
        }
    }
}
