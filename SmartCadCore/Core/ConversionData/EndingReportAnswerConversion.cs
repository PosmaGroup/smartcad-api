﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.Model;
using SmartCadCore.ClientData;

namespace SmartCadCore.Core
{
    public class EndingReportAnswerConversion
    {

        public static EndingReportAnswerClientData ToClient(EndingReportAnswerData data, UserApplicationData app)
        {
            EndingReportAnswerClientData client = new EndingReportAnswerClientData();

            client.AnswerText = data.AnswerText;
            client.Code = data.Code;
            client.Version = data.Version;
            client.QuestionText = data.QuestionText;
            client.AnswerHeaderText = data.AnswerHeaderText; 

            return client;
        }

        public static EndingReportAnswerData ToObject(EndingReportAnswerClientData client)
        {
            EndingReportAnswerData data = new EndingReportAnswerData();

            data.Code = client.Code;
            data.Version = client.Version;
            data.AnswerText = client.AnswerText;
            data.QuestionText = client.QuestionText;
            data.AnswerHeaderText = client.AnswerHeaderText;
            return data;
        }
    }
}
