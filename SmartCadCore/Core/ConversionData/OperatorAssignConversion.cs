using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;
using System.Collections;

namespace SmartCadCore.Core
{
    public class OperatorAssignConversion
    {
        public static OperatorAssignClientData ToClient(OperatorAssignData operatorAssignData, UserApplicationData app)
        {
            OperatorAssignClientData operatorAssignClient = new OperatorAssignClientData();

            operatorAssignClient.Code = operatorAssignData.Code;
            operatorAssignClient.EndDate = operatorAssignData.EndDate;
            operatorAssignClient.StartDate = operatorAssignData.StartDate;
            operatorAssignClient.SupervisedOperatorCode = operatorAssignData.SupervisedOperator.Code;
            operatorAssignClient.OperFirstName = operatorAssignData.SupervisedOperator.FirstName;
            operatorAssignClient.OperLastName = operatorAssignData.SupervisedOperator.LastName;
            operatorAssignClient.SupervisorCode = operatorAssignData.Supervisor.Code;
            operatorAssignClient.SupFirstName = operatorAssignData.Supervisor.FirstName;
            operatorAssignClient.SupLastName =  operatorAssignData.Supervisor.LastName;
            operatorAssignClient.Version = operatorAssignData.Version;
            if (operatorAssignData.SupervisedScheduleVariation != null)
                operatorAssignClient.SupervisedScheduleVariation = WorkShiftScheduleVariationConversion.ToClient(operatorAssignData.SupervisedScheduleVariation,app);


            if (operatorAssignData.SupervisorScheduleVariation != null)
                operatorAssignClient.SupervisorScheduleVariation = WorkShiftScheduleVariationConversion.ToClient(operatorAssignData.SupervisorScheduleVariation, app);

            return operatorAssignClient;
        }

        public static OperatorAssignData ToObject(OperatorAssignClientData clientData, UserApplicationData application)
        {
            OperatorAssignData operatorAssignData = new OperatorAssignData();
            operatorAssignData.Code = clientData.Code;
            operatorAssignData.EndDate = clientData.EndDate;
            operatorAssignData.StartDate = clientData.StartDate;
			OperatorData oper = new OperatorData();
            oper.Code = clientData.SupervisedOperatorCode;
			operatorAssignData.SupervisedOperator = oper;
            OperatorData sup = new OperatorData();
            sup.Code = clientData.SupervisorCode;
			operatorAssignData.Supervisor = sup;
            WorkShiftScheduleVariationData schedule = new WorkShiftScheduleVariationData();
            schedule.Code = clientData.SupervisedScheduleVariation.Code;
            schedule.Start = clientData.SupervisedScheduleVariation.Start;
            schedule.End = clientData.SupervisedScheduleVariation.End;
            schedule.Version = clientData.SupervisedScheduleVariation.Version;
            schedule.WorkShiftVariation = new WorkShiftVariationData();
            schedule.WorkShiftVariation.Name = clientData.SupervisedScheduleVariation.WorkShiftVariationName;
            schedule.WorkShiftVariation.Type = (WorkShiftVariationData.WorkShiftType)clientData.SupervisedScheduleVariation.WorkShiftVariationType;
			schedule.WorkShiftVariation.ObjectType = (WorkShiftVariationData.ObjectRelatedType)clientData.SupervisorScheduleVariation.WorkShiftObjectType;
            schedule.WorkShiftVariation.Code = clientData.SupervisedScheduleVariation.WorkShiftVariationCode;
            operatorAssignData.SupervisedScheduleVariation = schedule;

            WorkShiftScheduleVariationData superSchedule = new WorkShiftScheduleVariationData();
            superSchedule.Code = clientData.SupervisorScheduleVariation.Code;
            superSchedule.Start = clientData.SupervisorScheduleVariation.Start;
            superSchedule.End = clientData.SupervisorScheduleVariation.End;
            superSchedule.Version = clientData.SupervisorScheduleVariation.Version;
            superSchedule.WorkShiftVariation = new WorkShiftVariationData();
            superSchedule.WorkShiftVariation.Name = clientData.SupervisorScheduleVariation.WorkShiftVariationName;
            superSchedule.WorkShiftVariation.Type = (WorkShiftVariationData.WorkShiftType)clientData.SupervisorScheduleVariation.WorkShiftVariationType;
            superSchedule.WorkShiftVariation.Code = clientData.SupervisorScheduleVariation.WorkShiftVariationCode;
            operatorAssignData.SupervisorScheduleVariation = superSchedule;
            
            operatorAssignData.Version = clientData.Version;
            return operatorAssignData;
        }
    }
}
