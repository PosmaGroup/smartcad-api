﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    public class PositionConversion
    {
        public static PositionClientData ToClient(PositionData position, UserApplicationData app)
        {
            PositionClientData positionClientData = new PositionClientData();
            positionClientData.Code = position.Code;
            positionClientData.Version = position.Version;
            positionClientData.Name = position.Name;
            positionClientData.Description = position.Description;
            positionClientData.DepartmentTypeName = position.DepartmentType.Name;
            positionClientData.DepartmentTypeCode = position.DepartmentType.Code;
            return positionClientData;
        }

        public static PositionData ToObject(PositionClientData positionClient, UserApplicationData app)
        {
            PositionData position = new PositionData();
            position.Code = positionClient.Code;
            position.Version = positionClient.Version;
            position.Name = positionClient.Name;
            position.Description = positionClient.Description;
            DepartmentTypeData dep = new DepartmentTypeData(positionClient.DepartmentTypeName);
            dep.Code = positionClient.DepartmentTypeCode;
            position.DepartmentType = (DepartmentTypeData)SmartCadDatabase.RefreshObject(dep);

            return position;
        }
    }
}
