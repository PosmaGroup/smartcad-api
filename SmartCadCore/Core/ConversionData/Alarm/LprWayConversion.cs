﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.Model;
using SmartCadCore.ClientData;

namespace SmartCadCore.Core
{
    public class LprWayConversion
    {
        public static LprWayClientData ToClient(LprWayData cameraWay, UserApplicationData app)
        {
            LprWayClientData client = null;
            if (cameraWay != null)
            {
                client = new LprWayClientData();
                client.Code = cameraWay.Code;
                client.Version = cameraWay.Version;
                client.Name = cameraWay.Name;
            }
            return client;
        }

        public static LprWayData ToObject(LprWayClientData cameraType, UserApplicationData app)
        {
            LprWayData data = new LprWayData();
            data.Code = cameraType.Code;
            data.Name = cameraType.Name;
            data.Version = cameraType.Version;

            return data;
        }
    }
}
