﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.Model;
using SmartCadCore.ClientData;

namespace SmartCadCore.Core
{
    public class LprTypeConversion
    {
        public static LprTypeClientData ToClient(LprTypeData cameraType, UserApplicationData app)
        {
            LprTypeClientData client = null;
            if (cameraType != null)
            {
                client = new LprTypeClientData();
                client.Code = cameraType.Code;
                client.Version = cameraType.Version;
                client.Name = cameraType.Name;
            }
            return client;
        }

        public static LprTypeData ToObject(LprTypeClientData cameraType, UserApplicationData app)
        {
            LprTypeData data = new LprTypeData();
            data.Code = cameraType.Code;
            data.Name = cameraType.Name;

            return data;
        }
    }
}
