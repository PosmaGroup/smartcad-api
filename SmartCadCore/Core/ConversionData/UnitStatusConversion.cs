using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    public class UnitStatusConversion
    {
        public static UnitStatusClientData ToClient(UnitStatusData unitStatus, UserApplicationData app)
        {
            UnitStatusClientData unitStatusClient = new UnitStatusClientData();

            unitStatusClient.Code = unitStatus.Code;
            unitStatusClient.Version = unitStatus.Version;
            unitStatusClient.Name = unitStatus.Name;
            unitStatusClient.FriendlyName = unitStatus.FriendlyName;
            unitStatusClient.Color = unitStatus.Color;
            unitStatusClient.Image = unitStatus.Image;
            unitStatusClient.CustomCode = unitStatus.CustomCode;
            unitStatusClient.Active = unitStatus.Active.Value;
            unitStatusClient.Immutable = unitStatus.Immutable.Value;

            return unitStatusClient;
        }

        public static UnitStatusData ToObject(UnitStatusClientData unitStatusClient)
        {
            UnitStatusData unitStatus = new UnitStatusData();

            unitStatus.Code = unitStatusClient.Code;
            unitStatus.CustomCode = unitStatusClient.CustomCode;
            unitStatus = SmartCadDatabase.SearchObject<UnitStatusData>(unitStatus);

            return unitStatus;
        }
    }
}
