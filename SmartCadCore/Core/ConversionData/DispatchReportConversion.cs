﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    public class DispatchReportConversion
    {
        public static DispatchReportClientData ToClient(DispatchReportData report, UserApplicationData app)
        {
            DispatchReportClientData client = new DispatchReportClientData();

            client.Code = report.Code;
            client.Version = report.Version;

            client.Name = report.Name;
            
            if (report.InitializeCollections == true)
            {
                if (SmartCadDatabase.IsInitialize(report.DepartmentTypes) == false)
                    SmartCadDatabase.InitializeLazy(report, report.DepartmentTypes);
                if (SmartCadDatabase.IsInitialize(report.IncidentTypes) == false)
                    SmartCadDatabase.InitializeLazy(report, report.IncidentTypes);
                if (SmartCadDatabase.IsInitialize(report.Questions) == false)
                    SmartCadDatabase.InitializeLazy(report, report.Questions);
            }

            if (SmartCadDatabase.IsInitialize(report.DepartmentTypes) == true)
            {
                client.DepartmentTypes = new List<DepartmentTypeClientData>();
                foreach (DepartmentTypeData department in report.DepartmentTypes)
                {
                    client.DepartmentTypes.Add(DepartmentTypeConversion.ToClient(department, app));
                }
            }

            if (SmartCadDatabase.IsInitialize(report.IncidentTypes) == true)
            {
                client.IncidentTypes = new List<IncidentTypeClientData>();
                foreach (IncidentTypeData incidentType in report.IncidentTypes)
                {
                    client.IncidentTypes.Add(IncidentTypeConversion.ToClient(incidentType, app));
                }
            }

            if (SmartCadDatabase.IsInitialize(report.Questions) == true)
            {
                client.Questions = new List<DispatchReportQuestionDispatchReportClientData>();
                foreach (DispatchReportQuestionDispatchReportData question in report.Questions)
                {
                    client.Questions.Add(DispatchReportQuestionDispatchReportConversion.ToClient(question, app));
                }
            }
            return client;
        }

        public static DispatchReportData ToObject(DispatchReportClientData client, UserApplicationData app)
        {
            DispatchReportData data = new DispatchReportData();

            data.Code = client.Code;
            data.Version = client.Version;
            data.Name = client.Name;

            data.DepartmentTypes = new List<DepartmentTypeData>();
            foreach (DepartmentTypeClientData department in client.DepartmentTypes)
            {
                data.DepartmentTypes.Add(DepartmentTypeConversion.ToObject(department, app));
            }

            data.IncidentTypes = new List<IncidentTypeData>();
            foreach (IncidentTypeClientData incidentType in client.IncidentTypes)
            {
                data.IncidentTypes.Add(IncidentTypeConversion.ToObject(incidentType, app));
            }

            data.Questions = new List<DispatchReportQuestionDispatchReportData>();
            foreach (DispatchReportQuestionDispatchReportClientData question in client.Questions)
            {
                DispatchReportQuestionDispatchReportData reportQuestion = DispatchReportQuestionDispatchReportConversion.ToObject(question, app);
                reportQuestion.Report = data;
                data.Questions.Add(reportQuestion);
            }

            return data;
        }
    }
}
