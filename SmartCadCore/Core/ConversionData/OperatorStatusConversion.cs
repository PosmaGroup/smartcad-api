using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    public class OperatorStatusConversion
    {
        public static OperatorStatusClientData ToClient(OperatorStatusData statusdata, UserApplicationData userApplication)
        {
            OperatorStatusClientData oscd = new OperatorStatusClientData();
            oscd.Code = statusdata.Code;
            oscd.CustomCode = statusdata.CustomCode;
            oscd.FriendlyName = statusdata.FriendlyName;
            oscd.Image = statusdata.Image;
            oscd.Immutable = statusdata.Immutable;
            oscd.Name = statusdata.Name;
            oscd.NotReady = statusdata.NotReady;
            oscd.Order = statusdata.Order;
            oscd.Version = statusdata.Version;
            oscd.Color = statusdata.Color;
            oscd.Percentage = statusdata.Porcentage.HasValue ? statusdata.Porcentage.Value : 0;
            oscd.Tolerance = statusdata.Tolerance.HasValue ? statusdata.Tolerance.Value: 0;
            return oscd;
        }

        public static OperatorStatusData ToObject(OperatorStatusClientData clientData, UserApplicationData userApplication)
        {
            OperatorStatusData osd = new OperatorStatusData();
            osd.CustomCode = clientData.CustomCode;
            osd = SmartCadDatabase.SearchObject<OperatorStatusData>(osd);
            if (clientData.FriendlyName != null)
                osd.FriendlyName = clientData.FriendlyName;
            if (clientData.Name != null)
                osd.Name = clientData.Name;
            if (clientData.Percentage != null)
                osd.Porcentage = clientData.Percentage;
            if (clientData.Tolerance != null)
                osd.Tolerance = clientData.Tolerance;
            return osd;
        }
    }
}
