using System;
using System.Collections;
using System.Text;

using SmartCadCore.Model;
using SmartCadCore.ClientData;
using Iesi.Collections;

namespace SmartCadCore.Core
{
    public class SupportRequestReportConversion
    {
        public static SupportRequestReportClientData ToClient(SupportRequestReportData supportRequestReportData, UserApplicationData app)
        {
            SupportRequestReportClientData supportRequestReportClientData = new SupportRequestReportClientData();

            if (app.Equals(UserApplicationData.Dispatch))
            {
                supportRequestReportClientData.Code = supportRequestReportData.Code;
                supportRequestReportClientData.Version = supportRequestReportData.Version;
                supportRequestReportClientData.IncidentCode = supportRequestReportData.Incident.Code;
                supportRequestReportClientData.SourceReportBaseCode = supportRequestReportData.SourceReportBase.Code;
                supportRequestReportClientData.IncidentNotifications = new ArrayList();

                if (SmartCadDatabase.IsInitialize(supportRequestReportData.IncidentNotifications) == true)
                {
                    foreach (IncidentNotificationData incidentNotificationData in supportRequestReportData.IncidentNotifications)
                    {
                        supportRequestReportClientData.IncidentNotifications.Add(IncidentNotificationConversion.ToClient(incidentNotificationData, app));
                    }
                }
            }

            return supportRequestReportClientData;
        }

        public static SupportRequestReportData ToObject(SupportRequestReportClientData supportRequestReportClientData)
        {
            SupportRequestReportData supportRequestReportData = new SupportRequestReportData();
            supportRequestReportData.Code = supportRequestReportClientData.Code;
            supportRequestReportData.Version = supportRequestReportClientData.Version;
            supportRequestReportData.Comment = supportRequestReportClientData.Comment;
            supportRequestReportData.CustomCode = supportRequestReportClientData.CustomCode;

            UnitData unit = new UnitData();
            unit.Code = supportRequestReportClientData.UnitCode;
            supportRequestReportData.Unit = unit;

            IncidentData incident = SmartCadDatabase.SearchObjects(
                    SmartCadHqls.GetCustomHql(
                    SmartCadHqls.GetIncidentByCode,
                    supportRequestReportClientData.IncidentCode))[0] as IncidentData;
            supportRequestReportData.Incident = incident;

            OperatorData operatorData = new OperatorData();
            operatorData.Code = supportRequestReportClientData.OperatorCode;
            supportRequestReportData.Operator = operatorData;

            supportRequestReportData.ReportBaseDepartmentTypes = new ArrayList();
            foreach (ReportBaseDepartmentTypeClientData reportBaseDepartmentTypeClientData in supportRequestReportClientData.ReportBaseDepartmentTypes)
            {
                ReportBaseDepartmentTypeData reportBaseDepartmentTypeData = ReportBaseDepartmentTypeConversion.ToObject(reportBaseDepartmentTypeClientData);
                reportBaseDepartmentTypeData.ReportBase = supportRequestReportData;
                supportRequestReportData.ReportBaseDepartmentTypes.Add(reportBaseDepartmentTypeData);
            }

            ReportBaseData sourceReportBase = null;
            if (supportRequestReportClientData.SourceReportBaseType == typeof(PhoneReportData))
            {
                sourceReportBase = SmartCadDatabase.SearchBasicObject(
                    SmartCadHqls.GetCustomHql(SmartCadHqls.GetPhoneReportByCode, supportRequestReportClientData.SourceReportBaseCode))
                    as PhoneReportData;

            }
            else if (supportRequestReportClientData.SourceReportBaseType == typeof(SupportRequestReportData))
            {
                sourceReportBase = SmartCadDatabase.SearchBasicObject(
                    SmartCadHqls.GetCustomHql(SmartCadHqls.GetSupportRequestReportByCode, supportRequestReportClientData.SourceReportBaseCode))
                    as SupportRequestReportData;
            }
            else if (supportRequestReportClientData.SourceReportBaseType == typeof(CctvReportData))
            {
                sourceReportBase = SmartCadDatabase.SearchBasicObject(
                    SmartCadHqls.GetCustomHql(SmartCadHqls.GetCctvReportDataByCode, supportRequestReportClientData.SourceReportBaseCode))
                    as CctvReportData;
            }
            else if (supportRequestReportClientData.SourceReportBaseType == typeof(AlarmReportData))
            {
                sourceReportBase = SmartCadDatabase.SearchBasicObject(
                    SmartCadHqls.GetCustomHql(SmartCadHqls.GetAlarmReportDataByCode, supportRequestReportClientData.SourceReportBaseCode))
                    as AlarmReportData;
            }
            else if (supportRequestReportClientData.SourceReportBaseType == typeof(AlarmLprReportData))
            {
                sourceReportBase = SmartCadDatabase.SearchBasicObject(
                    SmartCadHqls.GetCustomHql(SmartCadHqls.GetAlarmLprReportDataByCode, supportRequestReportClientData.SourceReportBaseCode))
                    as AlarmLprReportData;
            }
            SmartCadDatabase.InitializeLazy(sourceReportBase, sourceReportBase.SetIncidentTypes);
            supportRequestReportData.SourceReportBase = sourceReportBase;
            supportRequestReportData.SetIncidentTypes = new HashedSet();
            foreach (IncidentTypeData incidentType in sourceReportBase.SetIncidentTypes)
            {
                supportRequestReportData.SetIncidentTypes.Add(incidentType);
            }
            
            return supportRequestReportData;
        }
    }
}
