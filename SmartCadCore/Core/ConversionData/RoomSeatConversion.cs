using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using SmartCadCore.ClientData;

namespace SmartCadCore.Core
{
    public class RoomSeatConversion
    {
        public static RoomSeatClientData ToClient(RoomSeatData roomSeatData, UserApplicationData app)
        {
            RoomSeatClientData roomSeatClientData = new RoomSeatClientData();
            roomSeatClientData.Code = roomSeatData.Code;
            roomSeatClientData.Version = roomSeatData.Version;
            if(roomSeatData.Room!=null)
                roomSeatClientData.Room = RoomConversion.ToClient(roomSeatData.Room,app);

            roomSeatClientData.SeatName = roomSeatData.NumberSeat;
            roomSeatClientData.ComputerName = roomSeatData.ComputerName;
            roomSeatClientData.X = roomSeatData.X;
            roomSeatClientData.Y = roomSeatData.Y;
            roomSeatClientData.Height = roomSeatData.Height;
            roomSeatClientData.Width = roomSeatData.Width;


            return roomSeatClientData;
        }

        public static RoomSeatData ToObject(RoomSeatClientData roomSeatClientData, UserApplicationData app)
        {
            RoomSeatData roomSeatData = new RoomSeatData();
            roomSeatData.Code = roomSeatClientData.Code;
            if(roomSeatData.Code != 0)
                roomSeatData = (RoomSeatData)SmartCadDatabase.RefreshObject(roomSeatData);

            roomSeatData.Version = roomSeatClientData.Version;
            roomSeatData.Room = new RoomData();
            roomSeatData.Room.Code = roomSeatClientData.Room.Code;

            roomSeatData.NumberSeat = roomSeatClientData.SeatName;
            roomSeatData.ComputerName = roomSeatClientData.ComputerName;
            roomSeatData.X = roomSeatClientData.X;
            roomSeatData.Y = roomSeatClientData.Y;
            roomSeatData.Height = roomSeatClientData.Height;
            roomSeatData.Width = roomSeatClientData.Width;

            return roomSeatData;
        }
    }
}