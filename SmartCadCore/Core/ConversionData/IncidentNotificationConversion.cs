using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using SmartCadCore.ClientData;
using System.Collections;
using Iesi.Collections;

namespace SmartCadCore.Core
{
    public class IncidentNotificationConversion
    {
        public static IncidentNotificationClientData ToClient(IncidentNotificationData incidentNotificationData, UserApplicationData app)
        {
            IncidentNotificationClientData incidentNotificationClientData = new IncidentNotificationClientData();
            if (app.Name == UserApplicationData.FirstLevel.Name || app.Name == UserApplicationData.Report.Name)
            {
                #region FirtLevel
                incidentNotificationClientData.Code = incidentNotificationData.Code;
                incidentNotificationClientData.Version = incidentNotificationData.Version;
                incidentNotificationClientData.IncidentCode = incidentNotificationData.ReportBase.Incident.Code;
                incidentNotificationClientData.CustomCode = incidentNotificationData.CustomCode;
                incidentNotificationClientData.DepartmentType = new DepartmentTypeClientData() { Name = incidentNotificationData.DepartmentType.Name };
                
                #endregion
            }
            else if (app.Name == UserApplicationData.Dispatch.Name)
            {

                #region Dispatch
                incidentNotificationClientData.Code = incidentNotificationData.Code;
                incidentNotificationClientData.Version = incidentNotificationData.Version;
                incidentNotificationClientData.IncidentCode = incidentNotificationData.ReportBase.Incident.Code;
                incidentNotificationClientData.AvailableRecommendedUnits = (incidentNotificationData.AvailableRecommendedUnits.HasValue) ? incidentNotificationData.AvailableRecommendedUnits.Value : false;
                incidentNotificationClientData.Status = IncidentNotificationStatusConversion.ToClient(incidentNotificationData.Status, app);
                incidentNotificationClientData.CustomCode = incidentNotificationData.CustomCode;
                incidentNotificationClientData.FriendlyCustomCode = incidentNotificationData.FriendlyCustomCode;
                incidentNotificationClientData.Priority = IncidentNotificationPriorityConversion.ToClient(incidentNotificationData.Priority, app);
                if (incidentNotificationData.DepartmentStation != null)
                {
                    incidentNotificationClientData.DepartmentStation = DepartmentStationConversion.ToClient(incidentNotificationData.DepartmentStation, app);
                }
                incidentNotificationClientData.DepartmentType = DepartmentTypeConversion.ToClient(incidentNotificationData.DepartmentType, app);
                incidentNotificationClientData.ReportBaseCode = incidentNotificationData.ReportBase.Code;
                incidentNotificationClientData.ReportBaseType = incidentNotificationData.ReportBase.GetType();
                incidentNotificationClientData.RecalculateStatus = false;
                incidentNotificationClientData.IncidentTypeCustomCodes = new ArrayList();
                incidentNotificationClientData.IncidentTypeNames = new ArrayList();
                incidentNotificationClientData.IncidentTypeCodes = new List<int>();
                FillIncidentTypeData(incidentNotificationData, incidentNotificationClientData);

                incidentNotificationClientData.IncidentCustomCode = incidentNotificationData.ReportBase.Incident.CustomCode;
                incidentNotificationClientData.CreationDate = incidentNotificationData.CreationDate.Value;
                incidentNotificationClientData.MultipleOrganisms = incidentNotificationData.ReportBase.MultipleOrganisms.Value;

                incidentNotificationClientData.DispatchOrders = new ArrayList();

                /*if (SmartCadDatabase.IsInitialize(incidentNotificationData.SetDispatchOrders) == false)
                {
                    SmartCadDatabase.InitializeLazy(incidentNotificationData, incidentNotificationData.SetDispatchOrders);
                }*/

                incidentNotificationClientData.CanBeCancelled = true;
                incidentNotificationClientData.CanBeClosed = false;
                incidentNotificationClientData.CanBeChangedZoneAndStation = true;
                incidentNotificationClientData.CanBeEnded = false;

                IncidentNotificationStatusData lastOperationalStatus = GetLastOperationalStatus(incidentNotificationData);
                incidentNotificationClientData.LastOperationalStatus = IncidentNotificationStatusConversion.ToClient(lastOperationalStatus, app);

                if (incidentNotificationData.Status.Name == IncidentNotificationStatusData.Closed.Name ||
                        incidentNotificationData.Status.Name == IncidentNotificationStatusData.Cancelled.Name)
                {
                    incidentNotificationClientData.CanBeCancelled = false;
                    incidentNotificationClientData.CanBeClosed = false;
                    incidentNotificationClientData.CanBeChangedZoneAndStation = false;
                    incidentNotificationClientData.CanBeEnded = false;
                }//else (Si no est� cerrada o cancelada la notificaci�n hay que determinar que acci�n se puede realizar)
                else if (incidentNotificationData.SetDispatchOrders != null) // Esta verificaci�n deber�a estar de m�s.                
                {
                    int totalCancelledOrders = 0;
                    int totalClosedOrders = 0;
                    int totalOnSceneOrders = 0;

                    if (SmartCadDatabase.IsInitialize(incidentNotificationData.SetDispatchOrders) == false)
                    {
                        SmartCadDatabase.InitializeLazy(incidentNotificationData, incidentNotificationData.SetDispatchOrders);
                    }
                    ArrayList setDispatchOrders = new ArrayList();
                    try
                    {
                        if (incidentNotificationData.SetDispatchOrders.Count > 0)
                        {
                            setDispatchOrders = new ArrayList(incidentNotificationData.SetDispatchOrders);
                        }
                    }
                    catch
                    {
                        IList result = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql("Select dod from DispatchOrderData dod where dod.IncidentNotification.Code = " + incidentNotificationData.Code));
                        if (result != null && result.Count > 0)
                            setDispatchOrders.AddRange(result);
                    }

                    foreach (DispatchOrderData dispatchOrder in setDispatchOrders)
                    {
                        if (dispatchOrder.Status == DispatchOrderStatusData.Cancelled)
                        {
                            totalCancelledOrders++;
                        }
                        else if (dispatchOrder.Status == DispatchOrderStatusData.Closed)
                        {
                            totalClosedOrders++;
                        }
                        else if (dispatchOrder.Status == DispatchOrderStatusData.UnitOnScene)
                        {
                            totalOnSceneOrders++;
                        }

                        if (dispatchOrder.Status != DispatchOrderStatusData.Cancelled &&
                            dispatchOrder.Status != DispatchOrderStatusData.Closed)
                        {
                            incidentNotificationClientData.DispatchOrders.Add(DispatchOrderConversion.ToClient(dispatchOrder, app));
                        }
                    }

                    incidentNotificationClientData.CanBeCancelled =
                        setDispatchOrders.Count == totalCancelledOrders;

                    if (totalClosedOrders > 0)
                        incidentNotificationClientData.CanBeEnded = false;
                    else
                        incidentNotificationClientData.CanBeEnded = (setDispatchOrders.Count == totalOnSceneOrders + totalCancelledOrders);

                    incidentNotificationClientData.CanBeClosed =
                        (totalClosedOrders > 0 &&
                        (totalClosedOrders + totalCancelledOrders == setDispatchOrders.Count));
                    incidentNotificationClientData.CanBeChangedZoneAndStation = setDispatchOrders.Count == 0;

                }

                incidentNotificationClientData.IncidentAddress = AddressConversion.ToClient(incidentNotificationData.ReportBase.Incident.Address, app);

                if (incidentNotificationData.DispatchOperator != null)
                {
                    incidentNotificationClientData.DispatchOperatorCode = incidentNotificationData.DispatchOperator.Code;
                    incidentNotificationClientData.DistpatchOperatorLogin = incidentNotificationData.DispatchOperator.Login;
                    incidentNotificationClientData.DispatchOperatorFullName = incidentNotificationData.DispatchOperator.FirstName +
                        "" + incidentNotificationData.DispatchOperator.LastName;
                }
                else
                {
                    incidentNotificationClientData.DispatchOperatorCode = 0;
                    incidentNotificationClientData.DistpatchOperatorLogin = "";
                }

                incidentNotificationClientData.StartDate = (incidentNotificationData.StartDate.HasValue) ? incidentNotificationData.StartDate.Value : DateTime.MaxValue;
                #endregion
            }
            else if (app.Name == UserApplicationData.Supervision.Name)
            {
                #region Supervision
                incidentNotificationClientData.Code = incidentNotificationData.Code;
                incidentNotificationClientData.Version = incidentNotificationData.Version;
                incidentNotificationClientData.IncidentCode = incidentNotificationData.ReportBase.Incident.Code;
                IncidentNotificationStatusData lastOperationalStatus = GetLastOperationalStatus(incidentNotificationData);
                incidentNotificationClientData.LastOperationalStatus = IncidentNotificationStatusConversion.ToClient(lastOperationalStatus, app);
                incidentNotificationClientData.Status = IncidentNotificationStatusConversion.ToClient(incidentNotificationData.Status, app);
                incidentNotificationClientData.CustomCode = incidentNotificationData.CustomCode;
                incidentNotificationClientData.FriendlyCustomCode = incidentNotificationData.FriendlyCustomCode;
                incidentNotificationClientData.Priority = IncidentNotificationPriorityConversion.ToClient(incidentNotificationData.Priority, app);
                if (incidentNotificationData.DepartmentStation != null)
                {
                    incidentNotificationClientData.DepartmentStation = DepartmentStationConversion.ToClient(incidentNotificationData.DepartmentStation, app);
                }
                incidentNotificationClientData.DepartmentType = DepartmentTypeConversion.ToClient(incidentNotificationData.DepartmentType, app);
                incidentNotificationClientData.ReportBaseCode = incidentNotificationData.ReportBase.Code;
                incidentNotificationClientData.ReportBaseType = incidentNotificationData.ReportBase.GetType();
                incidentNotificationClientData.IncidentTypeCustomCodes = new ArrayList();
                incidentNotificationClientData.IncidentTypeNames = new ArrayList();
                incidentNotificationClientData.IncidentTypeCodes = new List<int>();
                FillIncidentTypeData(incidentNotificationData, incidentNotificationClientData);
                incidentNotificationClientData.RecalculateStatus = false;
                incidentNotificationClientData.AssignedUnits = GetAssignedUnitsToIncidentNotification(incidentNotificationData.Code);

                incidentNotificationClientData.IncidentCustomCode = incidentNotificationData.ReportBase.Incident.CustomCode;
                incidentNotificationClientData.CreationDate = incidentNotificationData.CreationDate.Value;
                incidentNotificationClientData.MultipleOrganisms = incidentNotificationData.ReportBase.MultipleOrganisms.Value;

                incidentNotificationClientData.IncidentAddress = AddressConversion.ToClient(incidentNotificationData.ReportBase.Incident.Address, app);

                if (incidentNotificationData.DispatchOperator != null)
                {
                    incidentNotificationClientData.DispatchOperatorCode = incidentNotificationData.DispatchOperator.Code;
                    incidentNotificationClientData.DistpatchOperatorLogin = incidentNotificationData.DispatchOperator.Login;
                }
                else
                {
                    incidentNotificationClientData.DispatchOperatorCode = 0;
                    incidentNotificationClientData.DistpatchOperatorLogin = "";
                }

                incidentNotificationClientData.StartDate = (incidentNotificationData.StartDate.HasValue) ? incidentNotificationData.StartDate.Value : DateTime.MaxValue;
                #endregion
            }
            return incidentNotificationClientData;
               
        }

        private static int GetAssignedUnitsToIncidentNotification(int incidentNotificationCode)
        {
            int count = 0;
            string queryHQL = SmartCadHqls.GetCustomHql(SmartCadHqls.GetCountAssignedUnitsToNotification, incidentNotificationCode);
            ArrayList list = (ArrayList)SmartCadDatabase.SearchBasicObjects(queryHQL, true);
            if (list.Count > 0)
            {
                count = (int)((long)list[0]);
            }
            return count;
        }

        private static IncidentNotificationStatusData GetLastOperationalStatus(IncidentNotificationData incidentNotification)
        {
            IncidentNotificationStatusData status = incidentNotification.Status;
            if (status.Name == IncidentNotificationStatusData.AutomaticSupervisor.Name ||
                status.Name == IncidentNotificationStatusData.ManualSupervisor.Name ||
                status.Name == IncidentNotificationStatusData.Updated.Name)
            {
                IList historyList = SmartCadDatabase.SearchObjects(
                    SmartCadHqls.GetCustomHql(
                        SmartCadHqls.GetLastIncidentNotificationRealStatus,
                        incidentNotification.Code));
                if (historyList != null && historyList.Count > 0)
                {
                    IncidentNotificationStatusHistoryData lastHistory = historyList[0] as IncidentNotificationStatusHistoryData;
                    status = lastHistory.Status;
                }
            }

            return status;
        }

        private static void FillIncidentTypeData(IncidentNotificationData incidentNotificationData, IncidentNotificationClientData incidentNotificationClientData)
        {
            object objectList = SmartCadDatabase.SearchBasicObjects(
                   SmartCadHqls.GetCustomHql(SmartCadHqls.GetCustomCodeAndNameFromIncidentTypeByIncident, incidentNotificationData.ReportBase.Incident.Code));
            
            if (objectList as IList != null)
            {
                IList incidentTypeProperties = (IList)objectList;
                foreach (object[] temp in incidentTypeProperties)
                {
                    if (incidentNotificationClientData.IncidentTypeCustomCodes.Contains(temp[0]) == false)
                    {
                        try
                        {
                            incidentNotificationClientData.IncidentTypeCustomCodes.Add(temp[0]);
                            incidentNotificationClientData.IncidentTypeNames.Add(temp[1]);
                            incidentNotificationClientData.IncidentTypeCodes.Add((int)temp[2]);
                        }
                        catch (Exception ex)
                        { 

                        }
                    }                    
                }
            }
        }

        public static IncidentNotificationData ToObject(IncidentNotificationClientData incidentNotificationClientData)
        {
            if (incidentNotificationClientData.Code != 0)
            {
                IncidentNotificationData incidentNotification = new IncidentNotificationData();
                incidentNotification.Code = incidentNotificationClientData.Code;
                incidentNotification = (IncidentNotificationData)SmartCadDatabase.SearchObjectData(SmartCadDatabase.GetHQL(incidentNotification));
                incidentNotification.Version = incidentNotificationClientData.Version;
                if (incidentNotification.DispatchOperator == null ||
                    (incidentNotification.DispatchOperator != null &&
                     incidentNotification.DispatchOperator.Code !=
                     incidentNotificationClientData.DispatchOperatorCode))
                {//The notification is being assigned to some operator
                    OperatorData operatorData = new OperatorData();
                    operatorData.Code = incidentNotificationClientData.DispatchOperatorCode;
                    operatorData = (OperatorData)SmartCadDatabase.RefreshObject(operatorData);
                    incidentNotification.DispatchOperator = operatorData;
                }

                IncidentNotificationStatusData incidentNotificationStatus = new IncidentNotificationStatusData();
                if (incidentNotificationClientData.Status != null)
                {
                    incidentNotificationStatus.Code = incidentNotificationClientData.Status.Code;
                    incidentNotificationStatus = SmartCadDatabase.RefreshObject(incidentNotificationStatus) as IncidentNotificationStatusData;
                    incidentNotification.Status = incidentNotificationStatus;
                }
                incidentNotification.StartDate = incidentNotificationClientData.StartDate;

                incidentNotification.DepartmentStation = DepartmentStationConversion.ToObject(incidentNotificationClientData.DepartmentStation, null);

                incidentNotification.CancelDetail = incidentNotificationClientData.Detail;
                incidentNotification.RecalculateStatus = incidentNotificationClientData.RecalculateStatus;
                return incidentNotification;
            }
            return null;
        }        
    }
}
