﻿using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;
using System.Collections;
using Iesi.Collections;
using System.Windows;
using System.Drawing;

namespace SmartCadCore.Core
{
	public class RouteConversion
	{
		public static RouteClientData ToClient(RouteData data, UserApplicationData app)
		{
			RouteClientData clientdata = new RouteClientData();
			clientdata.Code = data.Code;
			clientdata.Version = data.Version;
			clientdata.CustomCode = data.CustomCode;
			clientdata.Name = data.Name;
			clientdata.Image = data.Image;
			clientdata.Type = (RouteClientData.RouteType)((int)data.Type);
			clientdata.StopTimeTol = data.StopTimeTol;
			clientdata.StopTime = data.StopTime;
			if (data.DepartmentType != null)
				clientdata.Department = DepartmentTypeConversion.ToClient(data.DepartmentType, app);
            if (SmartCadDatabase.IsInitialize(data.RouteAddress) )
            {
                data.InitializeCollections = true;
            }
			if (data.InitializeCollections == true)
			{
				clientdata.RouteAddress = new ArrayList();
                if (SmartCadDatabase.IsInitialize(data.RouteAddress) == false)
                    SmartCadDatabase.InitializeLazy(data, data.RouteAddress);    
				if (data.RouteAddress != null)
				{
					foreach (RouteAddressData address in data.RouteAddress)
					{
						RouteAddressClientData addressClient = RouteAddressConversion.ToClient(address, app);
						addressClient.RouteClient = clientdata;
						clientdata.RouteAddress.Add(addressClient);
					}
				}
			}
			return clientdata;
		}

		public static RouteData ToObject(RouteClientData clientdata, UserApplicationData app)
		{
			RouteData data = null;
            if (clientdata.Code != 0)
            {
                data = SmartCadDatabase.SearchObject<RouteData>(SmartCadHqls.GetCustomHql(SmartCadHqls.GetRouteWithAddressByCode, clientdata.Code));
                data.RouteAddress.Clear();
            }
            else
            {
                data = new RouteData();
                data.RouteAddress = new HashedSet();
            }

			if (clientdata.RouteAddress != null)
			{
				foreach (RouteAddressClientData address in clientdata.RouteAddress)
				{
					RouteAddressData addressData = RouteAddressConversion.ToObject(address, app);
					addressData.Route = data;
					data.RouteAddress.Add(addressData);
				}
			}
			data.Name = clientdata.Name;
			data.CustomCode = clientdata.CustomCode;
			data.Image = clientdata.Image;
			data.DepartmentType = DepartmentTypeConversion.ToObject(clientdata.Department, app);
			data.Type = (RouteData.RouteType)((int)clientdata.Type);
			data.StopTime = clientdata.StopTime;
			data.StopTimeTol = clientdata.StopTimeTol;
			return data;
		}
	}
}
