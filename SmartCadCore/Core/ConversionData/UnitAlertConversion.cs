﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    public class UnitAlertConversion
    {
        public static UnitAlertClientData ToClient(UnitAlertData data, UserApplicationData app) 
        {
            UnitAlertClientData client = new UnitAlertClientData();

            client.Code = data.Code;
			client.Active = data.Active;
            client.AlertCode = data.Alert.Code;
            client.AlertCustomCode = data.Alert.CustomCode;
            client.AlertName = data.Alert.Name;
            client.AlertMeasure = data.Alert.Measure;
            client.MaxValue = data.MaxValue;
            client.Priority = (UnitAlertClientData.AlertPriority)(int)data.Priority;
            client.UnitCode = data.Unit.Code;
			client.UnitCustomCode = data.Unit.CustomCode;
			client.UnitDepartmentTypeName = data.Unit.DepartmentType.Name;
            client.Version = data.Version;

            return client;
        }

        public static UnitAlertData ToObject(UnitAlertClientData client, UserApplicationData app) 
        {
            UnitAlertData data = new UnitAlertData();

			data.Active = client.Active;
            data.Code = client.Code;
            data.MaxValue = client.MaxValue;
            data.Priority = (UnitAlertData.AlertPriority)(int)client.Priority;
            data.Version = client.Version;
            
            AlertData alert = new AlertData();
            alert.Code = client.AlertCode;
            alert.Name = client.AlertName;
            alert.CustomCode = client.AlertCustomCode;
            data.Alert = alert;

            UnitData unit = new UnitData();
            unit.Code = client.UnitCode;
			unit = (UnitData)SmartCadDatabase.RefreshObject(unit);
            data.Unit = unit;

            return data;
            
        }
    }
}
