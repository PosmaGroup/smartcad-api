using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    public class OperatorCategoryConversion
    {
        public static OperatorCategoryClientData ToClient(OperatorCategoryData operatorCategory, UserApplicationData app)
        {
            OperatorCategoryClientData client = new OperatorCategoryClientData();
            client.Code = operatorCategory.Code;
            client.Name = operatorCategory.Name;
            client.Version = operatorCategory.Version;
            client.FriendlyName = operatorCategory.FriendlyName;
            client.Description = operatorCategory.Description;
            client.Level = operatorCategory.Level;
            return client;
        }

        public static OperatorCategoryData ToObject(OperatorCategoryClientData client, UserApplicationData app)
        {
            OperatorCategoryData data = new OperatorCategoryData();
            data.Code = client.Code;
            data.Name = client.Name;
            data = SmartCadDatabase.SearchObject<OperatorCategoryData>(data);
            data.FriendlyName = client.FriendlyName;
            data.Description = client.Description;
            return data;
        }
    }
}
