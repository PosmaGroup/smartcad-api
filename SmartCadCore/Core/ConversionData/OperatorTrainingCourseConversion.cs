using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;
using System.Collections;

namespace SmartCadCore.Core
{
    public class OperatorTrainingCourseConversion
    {
        public static OperatorTrainingCourseClientData ToClient(OperatorTrainingCourseData data, UserApplicationData app)
        {
            OperatorTrainingCourseClientData client = new OperatorTrainingCourseClientData();
            client.Code = data.Code;
            client.CourseName = data.TrainingCourseSchedule.TrainingCourse.Name;
            client.OperatorCode = data.Operator.Code;
            client.StarDate = data.TrainingCourseSchedule.Start.Value;
            client.EndDate = data.TrainingCourseSchedule.End.Value;
            client.CourseScheduleCode = data.TrainingCourseSchedule.Code;
            client.Version = data.Version;
            client.OperatorFirstName = data.Operator.FirstName;
            client.OperatorLastName = data.Operator.LastName;
            client.CourseCode = data.TrainingCourseSchedule.TrainingCourse.Code;
            client.CourseScheduleTrainer = data.TrainingCourseSchedule.Trainer;

            client.OperatorWorkShift = (string)OperatorScheduleManager.GetCurrentWorkShiftName(data.Operator.Code);
         
            if (SmartCadDatabase.IsInitialize(data.Operator.CategoryHistoryList) == false)
                SmartCadDatabase.InitializeLazy(data.Operator, data.Operator.CategoryHistoryList);
            
            if (SmartCadDatabase.IsInitialize(data.Operator.CategoryHistoryList) == true)
            {
                foreach (OperatorCategoryHistoryData category in data.Operator.CategoryHistoryList)
                {
                    if (category.EndDate == null)
                        client.OperatorCategory = category.Category.FriendlyName;
                }
            }
            return client;
        }

        public static OperatorTrainingCourseData ToObject(OperatorTrainingCourseClientData client, UserApplicationData app)
        {
            OperatorTrainingCourseData data = new OperatorTrainingCourseData();
            data.Code = client.Code;
            
            TrainingCourseScheduleData schedule = new TrainingCourseScheduleData();
            schedule.Code = client.CourseScheduleCode;
            data.TrainingCourseSchedule = (TrainingCourseScheduleData)SmartCadDatabase.RefreshObject(schedule);

            OperatorData oper = new OperatorData();
            oper.Code = client.OperatorCode;
            data.Operator = (OperatorData)SmartCadDatabase.RefreshObject(oper);
            data.Version = client.Version;
            return data;
        }
    }
}
