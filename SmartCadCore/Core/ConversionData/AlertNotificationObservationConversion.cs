﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
	public class AlertNotificationObservationConversion
	{
		public static AlertNotificationObservationClientData ToClient(AlertNotificationObservationData data, UserApplicationData app)
		{
			AlertNotificationObservationClientData clientData = new AlertNotificationObservationClientData();
			clientData.Code = data.Code;
			clientData.Date = data.Date;
			clientData.Text = data.Text;
			clientData.Version = data.Version;
			clientData.Operator = OperatorConversion.ToClient(data.Operator, app);

			return clientData;
		}

		public static AlertNotificationObservationData ToObject(AlertNotificationObservationClientData clientData, UserApplicationData app)
		{
			AlertNotificationObservationData data = new AlertNotificationObservationData();
			data.Code = clientData.Code;
			data.Date = clientData.Date;
			data.Text = clientData.Text;
			data.Version = clientData.Version;
			data.Operator = OperatorConversion.ToObject(clientData.Operator, app);
			
			return data;
		}
	}
}
