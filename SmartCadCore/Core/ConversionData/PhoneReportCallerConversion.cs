using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    public class PhoneReportCallerConversion
    {
        public static PhoneReportCallerClientData ToClient(PhoneReportCallerData phoneReportCallerData, UserApplicationData userApplication)
        {
            PhoneReportCallerClientData convertedClientObject = new PhoneReportCallerClientData();

            if (userApplication.Equals(UserApplicationData.FirstLevel))
            {
                convertedClientObject.Address = AddressConversion.ToClient(phoneReportCallerData.Address, userApplication);

                if (phoneReportCallerData.Anonymous.HasValue == true)
                {
                    convertedClientObject.Anonymous = phoneReportCallerData.Anonymous.Value;
                }

                convertedClientObject.Name = phoneReportCallerData.Name;
                convertedClientObject.Telephone = phoneReportCallerData.Telephone;
                convertedClientObject.AdditionalNumber = phoneReportCallerData.AdditionalNumber;
            }
            else if (userApplication.Equals(UserApplicationData.Dispatch))
            {
                convertedClientObject.Name = phoneReportCallerData.Name;
                convertedClientObject.Telephone = phoneReportCallerData.Telephone;
                if (phoneReportCallerData.Anonymous.HasValue == true)
                {
                    convertedClientObject.Anonymous = phoneReportCallerData.Anonymous.Value;
                }
            }
            else if (userApplication.Equals(UserApplicationData.Report))
            {
                convertedClientObject.Name = phoneReportCallerData.Name;
                convertedClientObject.Telephone = phoneReportCallerData.Telephone;
                if (phoneReportCallerData.Anonymous.HasValue == true)
                {
                    convertedClientObject.Anonymous = phoneReportCallerData.Anonymous.Value;
                }
            }
            
            return convertedClientObject;
        }

        public static PhoneReportCallerData ToObject(PhoneReportCallerClientData phoneReportCallerClient)
        {
            PhoneReportCallerData convertedCaller = new PhoneReportCallerData(
                phoneReportCallerClient.Name,
                phoneReportCallerClient.Telephone,
                null);

            convertedCaller.Address = new PhoneReportCallerAddressData(AddressConversion.ToObject(phoneReportCallerClient.Address));
            convertedCaller.Anonymous = phoneReportCallerClient.Anonymous;
            convertedCaller.AdditionalNumber = phoneReportCallerClient.AdditionalNumber;

            return convertedCaller;
        }
    }
}
