using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using SmartCadCore.ClientData;

namespace SmartCadCore.Core
{
    public class PhoneReportLineConverter
    {
        public static PhoneReportLineClientData ToClient(PhoneReportLineData phoneReportLineData, UserApplicationData userApplication)
        {
            PhoneReportLineClientData toConvert = new PhoneReportLineClientData(
                phoneReportLineData.Name,
                phoneReportLineData.Telephone,
                null);

            toConvert.Address = AddressConversion.ToClient(phoneReportLineData.Address, userApplication);

            return toConvert;
        }

        public static PhoneReportLineData ToObject(PhoneReportLineClientData phoneReportLineClient)
        {
            PhoneReportLineData toConvert = new PhoneReportLineData(
                phoneReportLineClient.Name,
                phoneReportLineClient.Telephone,
                new PhoneReportLineAddressData(AddressConversion.ToObject(phoneReportLineClient.Address)));

            return toConvert;
        }
    }
}
