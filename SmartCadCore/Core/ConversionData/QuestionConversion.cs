using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Collections;

using SmartCadCore.ClientData;
using SmartCadCore.Model;
using Iesi.Collections;
using SmartCadCore.Common;

namespace SmartCadCore.Core
{
    public class QuestionConversion
    {
        public static QuestionClientData ToClient(QuestionData question, UserApplicationData app)
        {
            QuestionClientData convertedClientObject = new QuestionClientData();

            try
            {
                if (app.Name == UserApplicationData.FirstLevel.Name ||
                    app.Name == UserApplicationData.Cctv.Name ||
                    (UserApplicationData.SocialNetworks!=null && app.Name == UserApplicationData.SocialNetworks.Name) ||
                    (UserApplicationData.Alarm != null && app.Name == UserApplicationData.Alarm.Name) ||
                    (UserApplicationData.Lpr != null && app.Name == UserApplicationData.Lpr.Name))
                {
                    #region FirtLevel || Cctv || ALARM || SN || LPR
                    convertedClientObject.Code = question.Code;
                    convertedClientObject.Version = question.Version;
                    convertedClientObject.CustomCode = question.CustomCode;
                    convertedClientObject.PossibleAnswers = new ArrayList();

                    if (SmartCadDatabase.IsInitialize(question.SetPossibleAnswers) == false)
                        SmartCadDatabase.InitializeLazy(question, question.SetPossibleAnswers);

                    if (SmartCadDatabase.IsInitialize(question.SetPossibleAnswers) == true)
                    {
                        foreach (QuestionPossibleAnswerData qpad in question.SetPossibleAnswers)
                            convertedClientObject.PossibleAnswers.Add(QuestionPossibleAnswerConversion.ToClient(qpad, app));
                    }
                    
                    convertedClientObject.IncidentTypes = new ArrayList();
                    if (question.IncidentTypes != null && SmartCadDatabase.IsInitialize(question.IncidentTypes) == true)
                    {
                        if (question.IncidentTypes.Count > 0)
                        {
                            foreach (IncidentTypeQuestionData itqd in question.IncidentTypes)
                            {
                                if (itqd.DeletedId == null)
                                {
									IncidentTypeQuestionClientData itqcd = new IncidentTypeQuestionClientData();
									itqcd.IncidentType = new IncidentTypeClientData();
									itqcd.IncidentType.Code = itqd.IncidentType.Code;
									itqcd.IncidentType.CustomCode = itqd.IncidentType.CustomCode;

                                    itqcd.Question = convertedClientObject;
                                    if (itqcd != null && itqcd.IncidentType != null)
                                        convertedClientObject.IncidentTypes.Add(itqcd);
                                }
                            }
                        }
                    }

                    if (SmartCadDatabase.IsInitialize(question.App) == true)
                    {
                        //SmartCadDatabase.InitializeLazy(question, question.App);

                        convertedClientObject.Application = new ArrayList();
                        foreach (UserApplicationData var in question.App)
                        {
                            convertedClientObject.Application.Add(UserApplicationConversion.ToClient(var, app));
                        }
                    }

                    convertedClientObject.RenderMethod = question.RenderMethod;
                    convertedClientObject.RequirementType = (RequirementTypeClientEnum)question.RequirementType;
                    convertedClientObject.ShortText = question.ShortText;
                    convertedClientObject.Text = question.Text;
                    #endregion
                }
                else if (app.Name == UserApplicationData.Administration.Name)
                {
                    #region Administration
                    convertedClientObject.Code = question.Code;
                    convertedClientObject.Version = question.Version;
                    convertedClientObject.CustomCode = question.CustomCode;
                    convertedClientObject.ShortText = question.ShortText;
                    convertedClientObject.Text = question.Text;
                    convertedClientObject.RequirementType = (RequirementTypeClientEnum)question.RequirementType;
                    convertedClientObject.RenderMethod = question.RenderMethod;

                    if (SmartCadDatabase.IsInitialize(question.App) == false)
                    {
                        SmartCadDatabase.InitializeLazy(question, question.App);
                    }
                    convertedClientObject.Application = new ArrayList();
                    foreach (UserApplicationData var in question.App)
                    {
                        UserApplicationClientData client = new UserApplicationClientData();
                        client.Code = var.Code;
                        client.FriendlyName = var.FriendlyName;
                        client.Name = var.Name;
                        convertedClientObject.Application.Add(client);
                    }

                    if (question.InitializeCollections == true)
                    {
                        #region initializeCollection
                        convertedClientObject.PossibleAnswers = new ArrayList();
                        convertedClientObject.IncidentTypes = new ArrayList();

                        if (SmartCadDatabase.IsInitialize(question.SetPossibleAnswers) == false)
                            SmartCadDatabase.InitializeLazy(question, question.SetPossibleAnswers);
                        if (question.SetPossibleAnswers != null)
                        {
                            foreach (QuestionPossibleAnswerData qpad in question.SetPossibleAnswers)
                            {
                                convertedClientObject.PossibleAnswers.Add(QuestionPossibleAnswerConversion.ToClient(qpad, app));
                            }
                        }

                        if (SmartCadDatabase.IsInitialize(question.IncidentTypes) == false)
                            SmartCadDatabase.InitializeLazy(question, question.IncidentTypes);
                        if (question.IncidentTypes != null)
                        {
                            foreach (IncidentTypeQuestionData itqd in question.IncidentTypes)
                            {
                                if (itqd.IncidentType != null)
                                {
                                    IncidentTypeQuestionClientData client = new IncidentTypeQuestionClientData();
                                    client.Code = itqd.Code;

                                    client.IncidentType = new IncidentTypeClientData();
                                    client.IncidentType.Code = itqd.IncidentType.Code;
                                    client.IncidentType.CustomCode = itqd.IncidentType.CustomCode;
                                    client.IncidentType.Name = itqd.IncidentType.Name;
                                    client.IncidentType.FriendlyName = itqd.IncidentType.FriendlyName;
                                    client.Question = convertedClientObject;

                                    convertedClientObject.IncidentTypes.Add(client);
                                }
                            }
                        }

                        question.App = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetUserApplicationsByQuestion, question.Code));
                        if (question.App != null)
                        {
                            convertedClientObject.App = new ArrayList();
                            foreach (UserApplicationData uaData in question.App)
                            {
                                UserApplicationClientData client = new UserApplicationClientData();
                                client.Code = uaData.Code;
                                client.FriendlyName = uaData.FriendlyName;
                                client.Name = uaData.Name;
                                convertedClientObject.App.Add(client);
                            }
                        }
                        #endregion
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }

            return convertedClientObject;
        }

        public static QuestionData ToObject(QuestionClientData questionClientData, UserApplicationData app)
        {

            QuestionData questionData = null;
            if (questionClientData.Code != 0)
            {
                questionData = (QuestionData)SmartCadDatabase.SearchObjectData(SmartCadHqls.GetCustomHql(SmartCadHqls.GetQuestionByCode, questionClientData.Code));

                SmartCadDatabase.InitializeLazy(questionData, questionData.IncidentTypes);
                SmartCadDatabase.InitializeLazy(questionData, questionData.App);
                SmartCadDatabase.InitializeLazy(questionData, questionData.SetPossibleAnswers);
            }
            else
            {
                questionData = new QuestionData();
                questionData.App = new ArrayList();
                questionData.IncidentTypes = new HashedSet();
                questionData.SetPossibleAnswers = new HashedSet();
            }

            questionData.Text = questionClientData.Text;
            questionData.ShortText = questionClientData.ShortText;
            questionData.CustomCode = questionClientData.CustomCode;
            questionData.RenderMethod = questionClientData.RenderMethod;

            questionData.RequirementType = QuestionConversion.RequirementTypeToObject(questionClientData, app);

            List<UserApplicationData> AppAux = new List<UserApplicationData>();
            foreach (UserApplicationClientData var in questionClientData.Application)
            {
                AppAux.Add(UserApplicationConversion.ToObject(var, app));
            }

            List<IncidentTypeQuestionData> IncTypeAux = new List<IncidentTypeQuestionData>();
            if (questionClientData.IncidentTypes != null)
            {
                foreach (IncidentTypeQuestionClientData var in questionClientData.IncidentTypes)
                {
                    IncidentTypeQuestionData dat = IncidentTypeQuestionConversion.ToObject(var, app);
                    dat.Question = questionData;
                    IncTypeAux.Add(dat);
                }
            }

            List<QuestionPossibleAnswerData> PossibleAnswerAux = new List<QuestionPossibleAnswerData>();
			if (questionClientData.PossibleAnswers != null)
            {
                foreach (QuestionPossibleAnswerClientData var in questionClientData.PossibleAnswers)
                {
                    QuestionPossibleAnswerData dat = QuestionPossibleAnswerConversion.ToObject(var, app);
                    dat.Question = questionData;
                    PossibleAnswerAux.Add(dat);
                }
            }

            if (questionData.Code != 0)
            {
                #region ApplicationData

                List<int> toDelete = new List<int>();
                foreach (UserApplicationData uad in questionData.App)
                {
                    int size = AppAux.Count;
                    AppAux.RemoveAll(ap => ap.Code == uad.Code);
                    if (size == AppAux.Count)
                    {
                        toDelete.Add(uad.Code);
                    }
                }
                toDelete.ForEach(delegate(int code)
                {
                    UserApplicationData inct = null;
                    foreach (UserApplicationData var in questionData.App)
                    {
                        if (var.Code == code)
                        {
                            inct = var;
                            var.DeletedId = Guid.NewGuid();
                            break;
                        }
                    }
                    //if (inct != null)
                    //    questionData.IncidentTypes.Remove(inct);
                });

                for (int i = questionData.App.Count - 1; i >= 0; i--)
                {
                    UserApplicationData uad = questionData.App[i] as UserApplicationData;
                    if (toDelete.Contains(uad.Code))
                    {
                        questionData.App.RemoveAt(i);
                    }
                }
                AppAux.ForEach(delegate(UserApplicationData ap) { questionData.App.Add(ap); });
                #endregion

                #region IncidentTypes

                toDelete.Clear();
                foreach (IncidentTypeQuestionData incType in questionData.IncidentTypes)
                {
                    int size = IncTypeAux.Count;
                    IncTypeAux.RemoveAll(inct => inct.Code == incType.Code);
                    if (size == IncTypeAux.Count)
                    {
                        toDelete.Add(incType.Code);
                    }
                }

                toDelete.ForEach(delegate(int code)
                {
                    IncidentTypeQuestionData inct = null;
                    foreach (IncidentTypeQuestionData var in questionData.IncidentTypes)
                    {
                        if (var.Code == code)
                        {
                            inct = var;
                            var.DeletedId = Guid.NewGuid();
                            break;
                        }
                    }
                    //if (inct != null)
                    //    questionData.IncidentTypes.Remove(inct);
                });
                IncTypeAux.ForEach(delegate(IncidentTypeQuestionData ap) { questionData.IncidentTypes.Add(ap); });

                #endregion

                #region PossibleAnswers

                toDelete.Clear();

                List<QuestionPossibleAnswerData> answersObsoletes = new List<QuestionPossibleAnswerData>();
                foreach (QuestionPossibleAnswerData var in questionData.SetPossibleAnswers)
                {
                    if (!PossibleAnswerAux.Exists(p => p.Code == var.Code))
                    {
                        answersObsoletes.Add(var);
                    }
                }

                answersObsoletes.ForEach(delegate(QuestionPossibleAnswerData todel) { todel.DeletedId = Guid.NewGuid(); });

                PossibleAnswerAux.ForEach(delegate(QuestionPossibleAnswerData dat)
                {
                    QuestionPossibleAnswerData aux = null;
                    foreach (QuestionPossibleAnswerData qpa in questionData.SetPossibleAnswers)
                    {
                        if (qpa.Code == dat.Code)
                        {
                            aux = qpa;
                            toDelete.Add(dat.Code);
                            break;
                        }
                    }
                    if (aux != null)
                    {
                        aux.Description = dat.Description;
                        aux.Procedure = dat.Procedure;
                    }
                });

                PossibleAnswerAux.RemoveAll(q => toDelete.Contains(q.Code));
                PossibleAnswerAux.ForEach(delegate(QuestionPossibleAnswerData ap) { questionData.SetPossibleAnswers.Add(ap); });


                #endregion
            }
            else
            {
                AppAux.ForEach(delegate(UserApplicationData dat)
                {
                    questionData.App.Add(dat);
                });
                IncTypeAux.ForEach(delegate(IncidentTypeQuestionData dat)
                {
                    questionData.IncidentTypes.Add(dat);
                });
                PossibleAnswerAux.ForEach(delegate(QuestionPossibleAnswerData dat)
                {
                    questionData.SetPossibleAnswers.Add(dat);
                });
            }

            return questionData;
        }

        public static RequirementTypeEnum RequirementTypeToObject(QuestionClientData questionClientData, UserApplicationData app)
        {
            if (questionClientData.RequirementType == RequirementTypeClientEnum.None)
            {
                return RequirementTypeEnum.None;
            }
            else if (questionClientData.RequirementType == RequirementTypeClientEnum.Recommended)
            {
                return RequirementTypeEnum.Recommended;
            }
            else
            {
                return RequirementTypeEnum.Required;
            }
        }
    }
}
