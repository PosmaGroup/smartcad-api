﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
	public class GPSPinSensorConversion
    {
        public static GPSPinSensorClientData ToClient(GPSPinSensorData data, UserApplicationData app)
        {
            GPSPinSensorClientData client = new GPSPinSensorClientData();
            client.Code = data.Code;
            client.GPSCode = data.GPS.Code;
            client.PinCode = data.Pin.Code;
            client.SensorCode = data.Sensor.Code;
            client.SensorName = data.Sensor.Name;
            client.Version = data.Version;
			client.PinNumber = data.Pin.Number;
            
            return client;
        }

        public static GPSPinSensorData ToObject(GPSPinSensorClientData client, UserApplicationData app)
        {
            GPSPinSensorData data = new GPSPinSensorData();
            data.Code = client.Code;
            
            GPSData gps = new GPSData();
            gps.Code = client.GPSCode;
            data.GPS = gps; ;

            GPSPinData pin = new GPSPinData();
            pin.Code = client.PinCode;
            data.Pin = pin;

            SensorData sensor = new SensorData();
            sensor.Code = client.SensorCode;
            sensor.Name = client.SensorName;
            data.Sensor = sensor;

            data.Version = client.Version;

            return data;
        }


    }
}
