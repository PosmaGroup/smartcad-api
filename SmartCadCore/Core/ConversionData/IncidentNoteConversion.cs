using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using SmartCadCore.ClientData;

namespace SmartCadCore.Core
{
    public class IncidentNoteConversion
    {
        public static IncidentNoteClientData ToClient(IncidentNoteData incidentNoteData, UserApplicationData app)
        {
            IncidentNoteClientData note = new IncidentNoteClientData();
            note.Code = incidentNoteData.Code;
            note.Version = incidentNoteData.Version;
            note.CreationDate = incidentNoteData.CreationDate.Value;
            note.CompleteUserName = incidentNoteData.CompleteUserName;
            note.Detail = incidentNoteData.Detail;
            note.UserLogin = incidentNoteData.UserLogin;
            note.IncidentCode = incidentNoteData.Incident.Code;
            return note;
        }

        public static IncidentNoteData ToObject(IncidentNoteClientData incidentClientNote)
        {
            IncidentNoteData note = new IncidentNoteData();
            note.Code = incidentClientNote.Code;
            note.Version = incidentClientNote.Version;
            note.CompleteUserName = incidentClientNote.CompleteUserName;
            note.CreationDate = incidentClientNote.CreationDate;
            note.Detail = incidentClientNote.Detail;
            note.UserLogin = incidentClientNote.UserLogin;
            IncidentData incident = new IncidentData();
            incident.Code = incidentClientNote.IncidentCode;
            note.Incident = incident;
            return note;
        }
    }
}
