﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    /// <summary>
    /// Reperesent a telephony configuration conversion.
    /// Information to login and configuration a PBX extension on a computer.
    /// </summary>
    public class TelephonyConfigurationConversion
    {
        public static TelephonyConfigurationClientData ToClient(TelephonyConfigurationData data, UserApplicationData userApplication)
        {
            return new TelephonyConfigurationClientData()
            {
                Code = data.Code,
                COMPUTER = data.Computer,
                Extension = data.Extension,
                Password = data.Password,
                Version = data.Version,
                Virtual = data.Virtual
            };
        }

        public static TelephonyConfigurationData ToObject(TelephonyConfigurationClientData client)
        {
            return new TelephonyConfigurationData()
            {
                Code = client.Code,
                Computer = client.COMPUTER,
                Extension = client.Extension,
                Password = client.Password,
                Version = client.Version,
                Virtual = client.Virtual
            };
        }
    }
}
