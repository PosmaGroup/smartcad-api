using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using SmartCadCore.ClientData;
using System.Drawing;

namespace SmartCadCore.Core
{
    public class IncidentNotificationPriorityConversion
    {
        public static IncidentNotificationPriorityClientData ToClient(IncidentNotificationPriorityData incidentNotificationPriorityData, UserApplicationData app)
        {
            IncidentNotificationPriorityClientData incidentNotificationPriorityClientData = new IncidentNotificationPriorityClientData();

            incidentNotificationPriorityClientData.Code = incidentNotificationPriorityData.Code;
            incidentNotificationPriorityClientData.Version = incidentNotificationPriorityData.Version;
            incidentNotificationPriorityClientData.Color = incidentNotificationPriorityData.Color;
            incidentNotificationPriorityClientData.CustomCode = incidentNotificationPriorityData.CustomCode;
            incidentNotificationPriorityClientData.Image = incidentNotificationPriorityData.Image;
            incidentNotificationPriorityClientData.Name = incidentNotificationPriorityData.Name;

            return incidentNotificationPriorityClientData;
        }
    }
}
