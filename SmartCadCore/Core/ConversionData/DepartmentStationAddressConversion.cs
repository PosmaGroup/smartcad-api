﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using SmartCadCore.Model;
using SmartCadCore.ClientData;

namespace SmartCadCore.Core
{
    public class DepartmentStationAddressConversion
    {
        public static DepartmentStationAddressClientData ToClient(DepartmentStationAddressData depData, UserApplicationData app)
        {
            DepartmentStationAddressClientData depCLientData = new DepartmentStationAddressClientData();
            depCLientData.Lon = depData.Address.Lon;
            depCLientData.Lat = depData.Address.Lat;
            depCLientData.DepartmentCode = depData.Station.Code;
            depCLientData.Code = depData.Code;
            depCLientData.Station = depData.Station.CustomCode;
            depCLientData.StationName = depData.Station.Name;
            depCLientData.PointNumber = depData.PointNumber;
            depCLientData.Version = depData.Version;          
            return depCLientData;

        }

        public static DepartmentStationAddressData ToObject(DepartmentStationAddressClientData depClientData, UserApplicationData app)
        {
            DepartmentStationAddressData depData = new DepartmentStationAddressData();          
            depData.Address = new MultipleAddressData();
            depData.Address.Lat = depClientData.Lat;
            depData.Address.Lon = depClientData.Lon;            
            depData.CustomCode = depClientData.Lon.ToString() + depClientData.Lat.ToString();
            depData.PointNumber = depClientData.PointNumber;           
            return depData;
        }
    }
}