﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    public class AlertConversion
    {
        public static AlertClientData ToClient(AlertData data, UserApplicationData app)
        {
            AlertClientData client = new AlertClientData();
            client.Code = data.Code;
            client.Name = data.Name;
            client.Measure = data.Measure;
            client.CustomCode = data.CustomCode;
            client.Version = data.Version;

            return client;
         
        }

       

    }
}
