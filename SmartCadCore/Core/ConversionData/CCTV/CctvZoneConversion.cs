using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using SmartCadCore.ClientData;
using System.Collections;
using Iesi.Collections;

namespace SmartCadCore.Core
{
    public class CctvZoneConversion
    {
        public static CctvZoneClientData ToClient(CctvZoneData cctvZoneData, UserApplicationData app)
        {
            CctvZoneClientData cctvZoneClientData = new CctvZoneClientData();
            cctvZoneClientData.Code = cctvZoneData.Code;
            cctvZoneClientData.Version = cctvZoneData.Version;
            cctvZoneClientData.Name = cctvZoneData.Name;

            cctvZoneClientData.StructNumber = Convert.ToInt32(SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(SmartCadHqls.GetAmountStructsByCctvZoneCode, cctvZoneClientData.Code)));
            cctvZoneClientData.Structs = new ArrayList();
            if (cctvZoneData.Structs != null)
            {
                if (SmartCadDatabase.IsInitialize(cctvZoneData.Structs) == true)
                {
                    SmartCadDatabase.InitializeLazy(cctvZoneData, cctvZoneData.Structs);

                    cctvZoneClientData.StructNumber = cctvZoneData.Structs.Count;
                    foreach (StructData structData in cctvZoneData.Structs)
                    {
                        structData.Zone = null;
                        StructClientData strClient = StructConversion.ToClient(structData, app);
                        strClient.CctvZone = cctvZoneClientData;
                        cctvZoneClientData.Structs.Add(strClient);
                    }
                }
            }
            return cctvZoneClientData;
        }

        public static CctvZoneData ToObject(CctvZoneClientData cctvZoneClientData, UserApplicationData app)
        {
            CctvZoneData cctvZoneData = new CctvZoneData();
            cctvZoneData.Code = cctvZoneClientData.Code;
            cctvZoneData.Version = cctvZoneClientData.Version;
            SmartCadDatabase.RefreshObject(cctvZoneData);

            cctvZoneData.Name = cctvZoneClientData.Name;

            if (cctvZoneClientData.Structs != null)
            {
                cctvZoneData.Structs = new HashedSet();
                foreach (StructClientData structClient in cctvZoneClientData.Structs)
                {
                    StructData structData = new StructData();
                    structData.Code = structClient.Code;
                    structData = (StructData)SmartCadDatabase.RefreshObject(structData);

                    cctvZoneData.Structs.Add(structData);
                }
            }

            return cctvZoneData;
        }
    }
}