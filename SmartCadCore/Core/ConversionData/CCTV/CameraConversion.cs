using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using SmartCadCore.ClientData;

namespace SmartCadCore.Core
{
    public class CameraConversion
    {       
        public static CameraClientData ToClient(CameraData cameraData, UserApplicationData app)
        {
            CameraClientData cameraClientData = new CameraClientData();
            cameraClientData.CustomId = cameraData.CustomId;
            cameraClientData.Code = cameraData.Code;
            cameraClientData.Version = cameraData.Version;
            cameraClientData.CustomId = cameraData.CustomId;
            cameraClientData.Name = cameraData.Name;
            cameraClientData.Ip = cameraData.Ip;
            cameraClientData.Port = cameraData.Port;
            cameraClientData.IpServer = cameraData.IpServer;
            cameraClientData.Login = cameraData.Login;
            cameraClientData.Password = cameraData.Password;
            cameraClientData.Resolution = cameraData.Resolution;
            cameraClientData.StreamType = cameraData.StreamType;
            cameraClientData.FrameRate = cameraData.FrameRate;
            cameraClientData.Type = CameraTypeConversion.ToClient(cameraData.Type, app);
            cameraClientData.ConnectionType = ConnectionTypeConversion.ToClient(cameraData.ConnectionType,app);

            if (cameraData.Struct != null)
            {
                cameraClientData.StructClientData = new StructClientData();
                cameraClientData.StructClientData.Code = cameraData.Struct.Code;
                cameraClientData.StructClientData.Name = cameraData.Struct.Name;
                cameraClientData.StructClientData.ZoneSecRef = cameraData.Struct.Zone.Name;
                cameraClientData.StructClientData.Type = StructTypeConversion.ToClient(cameraData.Struct.Type, app);
                if (cameraData.Struct.Zone != null)
                {
                    cameraData.Struct.Zone.Structs = null;
                    cameraClientData.StructClientData.CctvZone = CctvZoneConversion.ToClient(cameraData.Struct.Zone, app);
                    cameraClientData.StructClientData.Street = cameraData.Struct.Addres.Street;
                }
            }
            return cameraClientData;
        }
      
        public static CameraData ToObject(CameraClientData cameraClientData, UserApplicationData app)
        {
            CameraData cameraData = new CameraData();
            cameraData.CustomId = cameraClientData.CustomId;
            cameraData.Code = cameraClientData.Code;
            if(cameraData.Code!= 0)
                cameraData = (CameraData)SmartCadDatabase.RefreshObject(cameraData);
            cameraData.Version = cameraClientData.Version;
            cameraData.Name = cameraClientData.Name;
            cameraData.Ip = cameraClientData.Ip;
            cameraData.Port = cameraClientData.Port;
            cameraData.IpServer = cameraClientData.IpServer;
            cameraData.Login = cameraClientData.Login;
            cameraData.Password = cameraClientData.Password;
            cameraData.Resolution = cameraClientData.Resolution;
            cameraData.StreamType = cameraClientData.StreamType;
            cameraData.FrameRate = cameraClientData.FrameRate;
            cameraData.CustomId = cameraClientData.CustomId;
          
            cameraData.Type = new CameraTypeData();
            cameraData.Type.Code = cameraClientData.Type.Code;
            cameraData.Type = (CameraTypeData)SmartCadDatabase.RefreshObject(cameraData.Type);

            cameraData.ConnectionType = new ConnectionTypeData();
            cameraData.ConnectionType.Code = cameraClientData.ConnectionType.Code;
            cameraData.ConnectionType = (ConnectionTypeData)SmartCadDatabase.RefreshObject(cameraData.ConnectionType);

            if (cameraClientData.StructClientData != null)
            {
                cameraData.Struct = new StructData();
                cameraData.Struct.Code = cameraClientData.StructClientData.Code;
                cameraData.Struct.Name = cameraClientData.StructClientData.Name;
                cameraData.Struct = (StructData)SmartCadDatabase.RefreshObject(cameraData.Struct);
            }
            else
                cameraData.Struct = null;
          
            return cameraData;
        }
    }
}