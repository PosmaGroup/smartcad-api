﻿using SmartCadCore.ClientData;
using SmartCadCore.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Core
{
    class VAAlertCamConversion
    {
        public static VAAlertCamClientData ToClient(VAAlertCamData data, UserApplicationData app)
        {
            VAAlertCamClientData client = new VAAlertCamClientData();
            client.Code = data.Code;
            client.AlertDate = data.AlertDate;
            client.ExternalId = data.ExternalId;
            client.Version = data.Version;
            client.Camera = CameraConversion.ToClient(data.Camera,app);
            client.Rule = VARuleConversion.ToClient(data.Rule, app);
            client.VideoAlert = VAAlertVideoConversion.ToClient(data.VideoAlert, app);
           

            return client;
        }

        public static VAAlertCamData ToObject(VAAlertCamClientData client, UserApplicationData app)
        {
            VAAlertCamData data = new VAAlertCamData();
            data.Code = client.Code;
            data.AlertDate = client.AlertDate;
            data.ExternalId = client.ExternalId;
            data.Version = client.Version;
            data.Camera = CameraConversion.ToObject(client.Camera, app);
            data.Rule = VARuleConversion.ToObject(client.Rule, app);
            data.VideoAlert = VAAlertVideoConversion.ToObject(client.VideoAlert, app);

            return data;
        }
    }
}
