using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

using SmartCadCore.Model;
using SmartCadCore.ClientData;
using NHibernate.Collection;
using Iesi.Collections;

namespace SmartCadCore.Core
{
    public class CctvReportConversion
    {
        public static CctvReportClientData ToClient(CctvReportData cctvReportData, UserApplicationData app)
        {
            CctvReportClientData convertedClientObject = new CctvReportClientData();

            convertedClientObject.Code = cctvReportData.Code;
            convertedClientObject.Version = cctvReportData.Version;
            convertedClientObject.CustomCode = cctvReportData.CustomCode;

            if (cctvReportData.Incident != null)
            {
                convertedClientObject.IncidentCode = cctvReportData.Incident.Code;
                convertedClientObject.IncidentCustomCode = cctvReportData.Incident.CustomCode;
            }

            convertedClientObject.OperatorLogin = (cctvReportData.Operator != null) ? cctvReportData.Operator.Login : "";

            if (SmartCadDatabase.IsInitialize(cctvReportData.SetIncidentTypes) == false)
            {
                convertedClientObject.IncidentTypesCodes = (IList)SmartCadDatabase.SearchBasicObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.IncidentTypeCodesByReportBaseCode, cctvReportData.Code));
            }
            else
            {
                convertedClientObject.IncidentTypesCodes = new ArrayList();
                foreach (IncidentTypeData incidentType in cctvReportData.SetIncidentTypes)
                {
                    convertedClientObject.IncidentTypesCodes.Add(incidentType.Code);
                }
            }
           
            convertedClientObject.Camera = (CameraClientData)DeviceConversion.ToClient(cctvReportData.Camera, app);
            convertedClientObject.StartDate = cctvReportData.StartDate;

            convertedClientObject.ReportBaseDepartmentTypesClient = new ArrayList();
            return convertedClientObject;
        }

        public static CctvReportData ToObject(CctvReportClientData cctvReportClient)
        {
            CctvReportData convertedObject = new CctvReportData();

            convertedObject.SetAnswers = new ListSet();
            if (cctvReportClient.Answers != null)
            {
                foreach (ReportAnswerClientData pracd in cctvReportClient.Answers)
                {
                    CctvReportAnswerData tempCctvReportAnswer = new CctvReportAnswerData();
                    tempCctvReportAnswer = CctvReportAnswerConversion.ToObject(pracd);
                    tempCctvReportAnswer.CctvReport = convertedObject;
                    bool resutl = convertedObject.SetAnswers.Add(tempCctvReportAnswer);
                }              
            }            
            convertedObject.Code = cctvReportClient.Code;
            convertedObject.Version = cctvReportClient.Version;
            convertedObject.CustomCode = cctvReportClient.CustomCode;

           

            if (cctvReportClient.IncidentCode != 0)
            {
                IncidentData incident = new IncidentData();
                incident.Code = cctvReportClient.IncidentCode;
                incident = SmartCadDatabase.RefreshObject(incident) as IncidentData;
                convertedObject.Incident = incident;
            }

            convertedObject.SetIncidentTypes = new HashedSet();

            StringBuilder sb = new StringBuilder();
            sb.Append("(");
            for (int i = 0; i < cctvReportClient.IncidentTypesCodes.Count; i++)
            {
                int itc = (int)cctvReportClient.IncidentTypesCodes[i];
                sb.Append(itc);
                if (i < cctvReportClient.IncidentTypesCodes.Count - 1)
                    sb.Append(",");
            }
            sb.Append(")");

            foreach (IncidentTypeData incidentType in SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.FindIncidentTypesInCodeList, sb.ToString())))
            {
                convertedObject.SetIncidentTypes.Add(incidentType);
            }
           
            convertedObject.Camera = (CameraData)DeviceConversion.ToObject(cctvReportClient.Camera, UserApplicationData.Cctv);
            convertedObject.StartDate = cctvReportClient.StartDate;
            convertedObject.MultipleOrganisms = cctvReportClient.MultipleOrganisms;

            OperatorData operatorData = new OperatorData();
            operatorData.Login = cctvReportClient.OperatorLogin;
            operatorData = SmartCadDatabase.SearchObjects(operatorData)[0] as OperatorData;
            convertedObject.Operator = operatorData;
            convertedObject.ReportBaseDepartmentTypes = new ArrayList();

            foreach (ReportBaseDepartmentTypeClientData rbdtcd in cctvReportClient.ReportBaseDepartmentTypesClient)
            {
                ReportBaseDepartmentTypeData tempReportBaseDepartmentType = ReportBaseDepartmentTypeConversion.ToObject(rbdtcd);
                tempReportBaseDepartmentType.ReportBase = convertedObject as ReportBaseData;
                convertedObject.ReportBaseDepartmentTypes.Add(tempReportBaseDepartmentType);
            }

            return convertedObject;
        }
    }

   
}
