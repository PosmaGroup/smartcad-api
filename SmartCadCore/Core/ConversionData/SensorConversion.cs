﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    public class SensorConversion
    {
        public static SensorClientData ToClient(SensorData data, UserApplicationData app)
        {
            SensorClientData client = new SensorClientData();
            client.Code = data.Code;
            client.CustomCode = data.CustomCode;
            client.Name = data.Name;
            client.Type = (SensorClientData.SensorType)(int)data.Type;
            client.Version = data.Version;
                        
            return client;
        }

        public static SensorData ToObject(SensorClientData client, UserApplicationData app)
        {
            SensorData data = new SensorData();
            data.Code = client.Code;
            data.CustomCode = client.CustomCode;
            data.Name = client.Name;
            data.Type = (SensorData.SensorType)(int)client.Type;
            data.Version = client.Version;

            return data;
        }
    }
}
