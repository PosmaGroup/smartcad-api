﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    public class UserPermissionConversion
    {
        public static UserPermissionClientData ToClient(UserPermissionData permissionData, UserApplicationData app)
        {
            UserPermissionClientData client = new UserPermissionClientData();
            client.Code = permissionData.Code;
            client.Version = permissionData.Version;
            client.Name = permissionData.Name;
            client.UserAction = UserActionConversion.ToClient(permissionData.UserAction,app);
            client.UserResource = UserResourceConversion.ToClient(permissionData.UserResource, app);
            return client;
        }

        public static UserPermissionData ToObject(UserPermissionClientData clientData, UserApplicationData app)
        {
            UserPermissionData permissionData = new UserPermissionData();
            permissionData.Code = clientData.Code;
            permissionData.Name = clientData.Name;
            permissionData = SmartCadDatabase.SearchObject<UserPermissionData>(permissionData);
            return permissionData;
        }
    }
}
