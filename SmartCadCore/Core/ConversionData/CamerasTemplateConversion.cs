﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;
using System.Collections;
using Iesi.Collections;

namespace SmartCadCore.Core
{
    public class CamerasTemplateConversion
    {
        public static CamerasTemplateClientData ToClient(CamerasTemplateData data, UserApplicationData app)
        {
            CamerasTemplateClientData client = new CamerasTemplateClientData();
            client.Code = data.Code;
            client.Name = data.Name;
            client.Version = data.Version;
            client.Cameras = new ArrayList();

            if (data.SetCameras.Count != 0)
            {
                foreach (CameraData cameraData in data.SetCameras)
                {
                    client.Cameras.Add(new CameraClientData() { Code = cameraData.Code, Name = cameraData.Name });
                }
            }

            return client;
         
        }

        public static CamerasTemplateData ToObject(CamerasTemplateClientData client, UserApplicationData app)
        {
            CamerasTemplateData data = new CamerasTemplateData();
            data.Code = client.Code;
            data.Name = client.Name;
            data.Version = client.Version;
            data.SetCameras = new HashedSet();

            if (client.Cameras.Count != 0)
            {
                foreach (CameraClientData cameraClient in client.Cameras)
                {
                    client.Cameras.Add(new CameraData() { Code = cameraClient.Code });
                }
            }

            return data;

        }

    }
}
