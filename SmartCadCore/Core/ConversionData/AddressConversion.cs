using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    public class AddressConversion
    {
        public static AddressClientData ToClient(AddressData addressData, UserApplicationData userApplication)
        {
            AddressClientData addressClientData = new AddressClientData();

            if (userApplication.Equals(UserApplicationData.FirstLevel))
            {
                addressClientData.Zone = addressData.Zone;
                addressClientData.Street = addressData.Street;
                addressClientData.Reference = addressData.Reference;
                addressClientData.More = addressData.More;
                addressClientData.Lon = addressData.Lon;
                addressClientData.Lat = addressData.Lat;
                addressClientData.IsSynchronized = addressData.IsSynchronized;
            }
            else if (userApplication.Equals(UserApplicationData.Dispatch))
            {
                addressClientData.Zone = addressData.Zone;
                addressClientData.Street = addressData.Street;
                addressClientData.State = addressData.State;
                addressClientData.Town = addressData.Town;
                addressClientData.Reference = addressData.Reference;
                addressClientData.More = addressData.More;
                addressClientData.Lon = addressData.Lon;
                addressClientData.Lat = addressData.Lat;
                addressClientData.IsSynchronized = addressData.IsSynchronized;
            }
            else if (userApplication.Equals(UserApplicationData.Report))
            {
                addressClientData.Zone = addressData.Zone;
                addressClientData.Street = addressData.Street;
                addressClientData.Reference = addressData.Reference;
                addressClientData.More = addressData.More;
                addressClientData.Lon = addressData.Lon;
                addressClientData.Lat = addressData.Lat;
                addressClientData.IsSynchronized = addressData.IsSynchronized;
            }
            else if (userApplication.Equals(UserApplicationData.Map))
            {
                addressClientData.Zone = addressData.Zone;
                addressClientData.Street = addressData.Street;
                addressClientData.State = addressData.State;
                addressClientData.Town = addressData.Town;
                addressClientData.Reference = addressData.Reference;
                addressClientData.More = addressData.More;
                addressClientData.Lon = addressData.Lon;
                addressClientData.Lat = addressData.Lat;
                addressClientData.IsSynchronized = addressData.IsSynchronized;
            }
            else if (userApplication.Equals(UserApplicationData.Supervision))
            {
                addressClientData.Zone = addressData.Zone;
                addressClientData.Street = addressData.Street;
                addressClientData.Reference = addressData.Reference;
                addressClientData.More = addressData.More;
                addressClientData.Lon = addressData.Lon;
                addressClientData.Lat = addressData.Lat;
                addressClientData.IsSynchronized = addressData.IsSynchronized;
            }
            else if (userApplication.Equals(UserApplicationData.Cctv))
            {
                addressClientData.Zone = addressData.Zone;
                addressClientData.Street = addressData.Street;
                addressClientData.State = addressData.State;
                addressClientData.Town = addressData.Town;
                addressClientData.Lon = addressData.Lon;
                addressClientData.Lat = addressData.Lat;
                addressClientData.IsSynchronized = addressData.IsSynchronized;
            }
            else if (userApplication.Equals(UserApplicationData.Alarm))
            {
                addressClientData.Zone = addressData.Zone;
                addressClientData.Street = addressData.Street;
                addressClientData.State = addressData.State;
                addressClientData.Town = addressData.Town;
                addressClientData.Lon = addressData.Lon;
                addressClientData.Lat = addressData.Lat;
                addressClientData.IsSynchronized = addressData.IsSynchronized;
            }
            return addressClientData;
        }

        public static AddressData ToObject(AddressClientData addressClientData)
        {
            AddressData addressData = new AddressData();

            addressData.Zone = addressClientData.Zone;
            addressData.Street = addressClientData.Street;
            addressData.Reference = addressClientData.Reference;
            addressData.More = addressClientData.More;
            addressData.State = addressClientData.State;
            addressData.Town = addressClientData.Town;
            addressData.Lon = addressClientData.Lon;
            addressData.Lat = addressClientData.Lat;
            addressData.IsSynchronized = addressClientData.IsSynchronized;

            return addressData;
        }
    }
}
