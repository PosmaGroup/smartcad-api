using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;
using System.Collections;
using Iesi.Collections;

namespace SmartCadCore.Core
{
    public class DepartmentStationConversion
    {
        public static DepartmentStationClientData ToClient(DepartmentStationData departmentStation, UserApplicationData app)
        {
            DepartmentStationClientData departmentStationClient = new DepartmentStationClientData();
            departmentStationClient.Code = departmentStation.Code;
            departmentStationClient.Version = departmentStation.Version;
            departmentStationClient.CustomCode = departmentStation.CustomCode;
            departmentStationClient.Name = departmentStation.Name;
            departmentStationClient.Telephone = departmentStation.Telephone;

            if (departmentStation.DepartmentZone != null)
            {
                departmentStationClient.DepartmentZone = new DepartmentZoneClientData();
                departmentStationClient.DepartmentZone.Code = departmentStation.DepartmentZone.Code;
                departmentStationClient.DepartmentZone.Version = departmentStation.DepartmentZone.Version;
                departmentStationClient.DepartmentZone.CustomCode = departmentStation.DepartmentZone.CustomCode;
                departmentStationClient.DepartmentZone.Name = departmentStation.DepartmentZone.Name;

                departmentStationClient.DepartmentZone.DepartmentType = new DepartmentTypeClientData();
                departmentStationClient.DepartmentZone.DepartmentType.Color = departmentStation.DepartmentZone.DepartmentType.Color;
                departmentStationClient.DepartmentZone.DepartmentType.Code = departmentStation.DepartmentZone.DepartmentType.Code;
                departmentStationClient.DepartmentZone.DepartmentType.Name = departmentStation.DepartmentZone.DepartmentType.Name;
                departmentStationClient.DepartamentTypeCode = departmentStation.DepartmentZone.DepartmentType.Code;
                departmentStationClient.DepartamentTypeName = departmentStation.DepartmentZone.DepartmentType.Name;
            }

            if (app.Name == UserApplicationData.Administration.Name)
            {
                #region Administration
                if (departmentStation.Deleted == false)
                {
                    if (departmentStation.InitializeCollections)
                    {
                        SmartCadDatabase.InitializeLazy(departmentStation, departmentStation.DepartmentStationAddres);
                    }
                    if (departmentStation.DepartmentStationAddres != null && 
                        SmartCadDatabase.IsInitialize(departmentStation.DepartmentStationAddres))
                    {
                        departmentStationClient.DepartmentStationAddress = new ArrayList();
                        foreach (DepartmentStationAddressData address in departmentStation.DepartmentStationAddres)
                        {
                            if (address.DeletedId == null)
                            {
                                departmentStationClient.DepartmentStationAddress.Add(
                                    DepartmentStationAddressConversion.ToClient(address, app));
                            }
                        }
                        departmentStationClient.Points = departmentStationClient.DepartmentStationAddress.Count > 0;
                    }                    
                }
                #endregion
            }
            else if (app.Name == UserApplicationData.Map.Name)
            {
                #region Map
                departmentStationClient.DepartmentStationAddress = new ArrayList();
                if (SmartCadDatabase.IsInitialize(departmentStation.DepartmentStationAddres) == true)
                {
                    //SmartCadDatabase.InitializeLazy(departmentStation, departmentStation.DepartmentStationAddres);
                    if (departmentStation.DepartmentStationAddres != null)
                    {
                        foreach (DepartmentStationAddressData departmentAddress in departmentStation.DepartmentStationAddres)
                        {
                            if (departmentAddress.DeletedId == null)
                            {
                                departmentStationClient.DepartmentStationAddress.Add(DepartmentStationAddressConversion.ToClient(departmentAddress, app));
                            }
                        }
                    }
                    
                }
                #endregion
            }

            return departmentStationClient;
        }

        public static DepartmentStationData ToObject(DepartmentStationClientData departmentStationClient, UserApplicationData app)
        {
            DepartmentStationData departmentStation = new DepartmentStationData();
            departmentStation.Code = departmentStationClient.Code;
            departmentStation.Version = departmentStationClient.Version;

            if(departmentStation.Code!=0)
                departmentStation =
                    SmartCadDatabase.SearchObject<DepartmentStationData>(SmartCadHqls.GetCustomHql(SmartCadHqls.GetDepartmentStationByCode, departmentStationClient.Code));

            departmentStation.Name = departmentStationClient.Name;
            departmentStation.Telephone = departmentStationClient.Telephone;
            departmentStation.CustomCode = departmentStationClient.CustomCode;
            DepartmentZoneData zone = new DepartmentZoneData();
            zone.Code = departmentStationClient.DepartmentZone.Code;
            zone = SmartCadDatabase.SearchObject<DepartmentZoneData>(
                SmartCadHqls.GetCustomHql(SmartCadHqls.GetDepartmentZoneWithAddressAndStationsByCode, zone.Code));
            departmentStation.DepartmentZone = zone;
            
            if (departmentStationClient.DepartmentStationAddress != null)
            {
                departmentStation.DepartmentStationAddres = new HashedSet();

                foreach (DepartmentStationAddressClientData depClientData in departmentStationClient.DepartmentStationAddress)
                {
                    DepartmentStationAddressData dsd = DepartmentStationAddressConversion.ToObject(depClientData, app);
                    dsd.Station = departmentStation;
                    departmentStation.DepartmentStationAddres.Add(dsd);
                }
            }
            return departmentStation;


        }
    }
}
