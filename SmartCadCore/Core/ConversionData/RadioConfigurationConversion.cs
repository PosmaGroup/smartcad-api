﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    public class RadioConfigurationConversion
    {
        public static RadioConfigurationClientData ToClient(RadioConfigurationData data, UserApplicationData userApplication)
        {
            DepartmentTypeClientData departmentTypeClientData = null;
            if (data.Department != null)
            {
                departmentTypeClientData = new DepartmentTypeClientData();
                departmentTypeClientData.Code = data.Department.Code;
                departmentTypeClientData.Name = data.Department.Name;
            }
            
            return new RadioConfigurationClientData()
            {
                Code = data.Code,
                Department = departmentTypeClientData,
                Extension = data.Extension,
                TalkCode = data.TalkCode,
                FrecuencyCode = data.FrecuencyCode,
                Description = data.Description,
                Version = data.Version,
            };
        }

        public static RadioConfigurationData ToObject(RadioConfigurationClientData client)
        {
            DepartmentTypeData departmentTypeData = null;
            if (client.Department != null)
            {
                departmentTypeData = new DepartmentTypeData();
                departmentTypeData.Code = client.Department.Code;
                departmentTypeData.Name = client.Department.Name;
            }

            return new RadioConfigurationData()
            {
                Code = client.Code,
                Department = departmentTypeData,
                Extension = client.Extension,
                TalkCode = client.TalkCode,
                FrecuencyCode = client.FrecuencyCode,
                Description = client.Description,
                Version = client.Version,
            };
        }
    }
}
