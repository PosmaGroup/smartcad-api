﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
	public class IndicatorTypeConversion
	{
        public static IndicatorTypeClientData ToClient(IndicatorTypeData type, UserApplicationData app)
        {
            IndicatorTypeClientData client = new IndicatorTypeClientData();
            client.Code = type.Code;
            client.Version = type.Version;
            client.Name = type.Name;
            client.FriendlyName = type.FriendlyName;
            client.Description = type.Description;
            return client;
        }

        public static IndicatorTypeData ToObject(IndicatorTypeClientData client)
        {
            IndicatorTypeData data = new IndicatorTypeData();
            data.Code = client.Code;
            data.Name = client.Name;
            data = SmartCadDatabase.SearchObject<IndicatorTypeData>(data);
            return data;
        }
	}
}
