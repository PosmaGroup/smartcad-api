﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    public class DepartmentTypeAlertConversion
    {
        public static DepartmentTypeAlertClientData ToClient(DepartmentTypeAlertData data, UserApplicationData app)
        {
            DepartmentTypeAlertClientData client = new DepartmentTypeAlertClientData();

            client.Code = data.Code;
            client.AlertCode = data.Alert.Code;
            client.AlertCustomCode = data.Alert.CustomCode;
            client.AlertName = data.Alert.Name;
            client.AlertMeasure = data.Alert.Measure;
            client.MaxValue = data.MaxValue;
            client.Priority = (DepartmentTypeAlertClientData.AlertPriority)(int)data.Priority;
            client.DepartmentTypeCode = data.DepartmentType.Code;
            client.Version = data.Version;

            return client;
        }

        public static DepartmentTypeAlertData ToObject(DepartmentTypeAlertClientData client, UserApplicationData app)
        {
            DepartmentTypeAlertData data = new DepartmentTypeAlertData();

            data.Code = client.Code;
            data.MaxValue = client.MaxValue;
            data.Priority = (DepartmentTypeAlertData.AlertPriority)(int)client.Priority;
            data.Version = client.Version;

            AlertData alert = new AlertData();
            alert.Code = client.AlertCode;
            data.Alert = (AlertData)SmartCadDatabase.RefreshObject(alert);

            DepartmentTypeData department = new DepartmentTypeData();
            department.Code = client.DepartmentTypeCode;
            data.DepartmentType = (DepartmentTypeData)SmartCadDatabase.RefreshObject(department);

            return data;

        }
    }
}
