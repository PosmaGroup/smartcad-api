using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.ClientData;
using SmartCadCore.Model;
using SmartCadCore.Common;

namespace SmartCadCore.Core
{
    public class IncidentTypeQuestionConversion
    {
        public static IncidentTypeQuestionClientData ToClient(IncidentTypeQuestionData incidentTypeQuestionData, UserApplicationData app)
        {
            IncidentTypeQuestionClientData convertedClientObject = new IncidentTypeQuestionClientData();

            try
            {
                convertedClientObject.Code = incidentTypeQuestionData.Code;
                convertedClientObject.Version = incidentTypeQuestionData.Version;

                if(incidentTypeQuestionData.IncidentType!= null)
                    convertedClientObject.IncidentType = IncidentTypeConversion.ToClient(incidentTypeQuestionData.IncidentType,app);

                //TODO: Chequear
                if (incidentTypeQuestionData.Question != null)
                {
                    if (SmartCadDatabase.IsInitialize(incidentTypeQuestionData.Question.App) == true)
                    {
                        foreach (UserApplicationData user in incidentTypeQuestionData.Question.App)
                        {
                            if (app == user)
                            {
                                incidentTypeQuestionData.Question.IncidentTypes = null;
                                convertedClientObject.Question = QuestionConversion.ToClient(incidentTypeQuestionData.Question, app);
                            }
                        }
                    }
                }
                else
                {
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }

            return convertedClientObject;
        }

        public static IncidentTypeQuestionData ToObject(IncidentTypeQuestionClientData itqClient, UserApplicationData app)
        {
            IncidentTypeQuestionData convertedObject = new IncidentTypeQuestionData();

            try
            {
                convertedObject.Code = itqClient.Code;
                convertedObject.Version = itqClient.Version;

                IncidentTypeData incidentTypeData = new IncidentTypeData();
                incidentTypeData.Code = itqClient.IncidentType.Code;
                incidentTypeData.CustomCode = itqClient.IncidentType.CustomCode;
                incidentTypeData.Name = itqClient.IncidentType.Name;
                incidentTypeData.FriendlyName = itqClient.IncidentType.FriendlyName;

                convertedObject.IncidentType = incidentTypeData;

                //TODO: Chequear
                //if (itqClient.Question != null && itqClient.Question.App != null)
                //{
                //    foreach (UserApplicationClientData user in itqClient.Question.App)
                //    {
                //        if (app.Code == user.Code)
                //        {
                //            //convertedObject.Question = QuestionConversion.ToObject(itqClient.Question, app);
                //            convertedObject.Question = new QuestionData();
                //            convertedObject.Question.Code = itqClient.Question.Code;
                //            convertedObject.Question = (QuestionData)SmartCadDatabase.RefreshObject(convertedObject.Question);
                //        }
                //    }
                //}
                //else
                //{
                //}
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }

            return convertedObject;
        }
    }
}
