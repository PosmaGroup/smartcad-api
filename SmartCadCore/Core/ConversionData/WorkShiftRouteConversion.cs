﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;
using System.Collections;

namespace SmartCadCore.Core
{
    public class WorkShiftRouteConversion
    {
        public static WorkShiftRouteClientData ToClient(WorkShiftRouteData data, UserApplicationData app)
        {
            WorkShiftRouteClientData client = new WorkShiftRouteClientData();
            client.Code = data.Code;
			client.Route = RouteConversion.ToClient(data.Route, app);
			client.WorkShiftCode = data.WorkShift.Code;
            client.WorkShiftName = data.WorkShift.Name;

			if (SmartCadDatabase.IsInitialize(data.WorkShift.Schedules))
			{
				client.WorkShiftShedules = new ArrayList();
				foreach (WorkShiftScheduleVariationData item in data.WorkShift.Schedules)
					client.WorkShiftShedules.Add(WorkShiftScheduleVariationConversion.ToClient(item, app));
			}
            client.UnitCode = data.Unit.Code;
            client.Version = data.Version;

            return client;
        }

        public static WorkShiftRouteData ToObject(WorkShiftRouteClientData client, UserApplicationData app)
        {
            WorkShiftRouteData data = new WorkShiftRouteData();
            data.Code = client.Code;
            RouteData route = new RouteData();
            route.Code = client.Route.Code;
            route.Name = client.Route.Name;
            data.Route = route;

            WorkShiftVariationData ws = new WorkShiftVariationData();
            ws.Code = client.WorkShiftCode;
            ws.Name = client.WorkShiftName;
            data.WorkShift = ws;

            UnitData unit = new UnitData();
            unit.Code = client.UnitCode;
            data.Unit = unit;
            
            data.Version = client.Version;

            return data;
        }
    }
}
