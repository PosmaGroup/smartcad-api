﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.Model;
using SmartCadCore.ClientData;
using System.Collections;

namespace SmartCadCore.Core
{
    public class IncidentTypeCategoryConversion
    {
        public static IncidentTypeCategoryClientData ToClient(IncidentTypeCategoryData incidentTypeCategData, UserApplicationData app)
        {
            IncidentTypeCategoryClientData incidentTypeCategClient = new IncidentTypeCategoryClientData();
            incidentTypeCategClient.Code = incidentTypeCategData.Code;
            incidentTypeCategClient.Name = incidentTypeCategData.Name;
            incidentTypeCategClient.CustomCode = incidentTypeCategData.CustomCode;
            incidentTypeCategClient.IncidentTypes = new ArrayList();
            if (incidentTypeCategData.IncidentTypes != null)
            {
                if (SmartCadDatabase.IsInitialize(incidentTypeCategData.IncidentTypes) == true)
                {
                    //    SmartCadDatabase.InitializeLazy(incidentTypeCategData, incidentTypeCategData.IncidentTypes);

                    foreach (IncidentTypeData incidentType in incidentTypeCategData.IncidentTypes)
                    {
                        IncidentTypeClientData client = new IncidentTypeClientData();
                        client.Code = incidentType.Code;
                        client.FriendlyName = incidentType.FriendlyName;
                        client.Exclusive = (incidentType.Exclusive.HasValue ? incidentType.Exclusive.Value : false);
                        client.IconName = incidentType.IconName;
                        client.Name = incidentType.Name;
                        client.NoEmergency = incidentType.NoEmergency.Value;
                        client.Procedure = incidentType.Procedure;

                        incidentTypeCategClient.IncidentTypes.Add(client);
                    }
                }
            }
            return incidentTypeCategClient;
        }

        public static IncidentTypeCategoryData ToObject(IncidentTypeCategoryClientData incidentTypeCategClient, UserApplicationData app)
        {
            IncidentTypeCategoryData incidentTypeData = new IncidentTypeCategoryData();
            incidentTypeData.Code = incidentTypeCategClient.Code;
            incidentTypeData.CustomCode = incidentTypeCategClient.CustomCode;
            incidentTypeData.Name = incidentTypeCategClient.Name;
            incidentTypeData.Version = incidentTypeCategClient.Version;

            incidentTypeData.IncidentTypes = new ArrayList();
            if (incidentTypeCategClient.IncidentTypes != null)
            {
                foreach (IncidentTypeClientData itClient in incidentTypeCategClient.IncidentTypes)
                {
                    IncidentTypeData inc = new IncidentTypeData(itClient.CustomCode,itClient.Name);
                    inc.Code = itClient.Code;
                    inc.Version = itClient.Version;
                    incidentTypeData.IncidentTypes.Add(inc);
                }
            }
                
            return incidentTypeData;
        }
    }
}
