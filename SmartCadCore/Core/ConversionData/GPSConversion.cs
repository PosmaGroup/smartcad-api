﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;
using System.Collections;

namespace SmartCadCore.Core
{
    public class GPSConversion
    {
        public static GPSClientData ToClient(GPSData gps, UserApplicationData app)
        {
            GPSClientData client = new GPSClientData();
            client.Code = gps.Code;
            client.Type = GPSTypeConversion.ToClient(gps.Type, app);
            client.ParentCode = gps.ParentCode;
        
            if (gps.Sensors != null)
            {
                client.Sensors = new ArrayList();
                foreach (GPSPinSensorData sensor in gps.Sensors)
                {
					client.Sensors.Add(GPSPinSensorConversion.ToClient(sensor, app));
                }
            
            }

            client.Version = gps.Version;
         
            return client;
        }

        public static GPSData ToObject(GPSClientData gps, UserApplicationData app)
        {
            GPSData data = new GPSData();
            data.Code = gps.Code;
            data.Type = GPSTypeConversion.ToObject(gps.Type, app);
            data.ParentCode = gps.ParentCode;

            if (gps.Sensors != null)
            {
                data.Sensors = new ArrayList();
                foreach (GPSPinSensorClientData sensor in gps.Sensors)
                {
					GPSPinSensorData sensorData = GPSPinSensorConversion.ToObject(sensor, app);
                    sensorData.GPS = data;
                    data.Sensors.Add(sensorData);            
                }
                
            }

            data.Version = gps.Version;
           
            return data;
        }

    }
}
