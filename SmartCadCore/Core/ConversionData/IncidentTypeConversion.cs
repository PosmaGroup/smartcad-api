using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using SmartCadCore.ClientData;
using System.Collections;
using Iesi.Collections;
using SmartCadCore.Common;

namespace SmartCadCore.Core
{
    public class IncidentTypeConversion
    {
		public static IncidentTypeClientData ToClient(IncidentTypeData incidentTypeData, UserApplicationData app)
		{
			IncidentTypeClientData incidentTypeClientData = new IncidentTypeClientData();
			incidentTypeClientData.Code = incidentTypeData.Code;
			incidentTypeClientData.Version = incidentTypeData.Version;
			incidentTypeClientData.CustomCode = incidentTypeData.CustomCode;
			incidentTypeClientData.Name = incidentTypeData.Name;
			incidentTypeClientData.FriendlyName = incidentTypeData.FriendlyName;
			if (app.Equals(UserApplicationData.Supervision))
			{

			}
			else if (app.Equals(UserApplicationData.Administration))
            {
                #region Administration
                try
				{
					incidentTypeClientData.NoEmergency = (incidentTypeData.NoEmergency.HasValue) ? incidentTypeData.NoEmergency.Value : false;
					incidentTypeClientData.Procedure = incidentTypeData.Procedure;
					incidentTypeClientData.IncidentTypeQuestions = new ArrayList();
					incidentTypeClientData.IconName = incidentTypeData.IconName;

					if (incidentTypeData.Exclusive.HasValue == true)
						incidentTypeClientData.Exclusive = incidentTypeData.Exclusive.Value;

					if (incidentTypeData.InitializeCollections == true)
					{
						if (SmartCadDatabase.IsInitialize(incidentTypeData.SetIncidentTypeQuestions) == false)
						{
							SmartCadDatabase.InitializeLazy(incidentTypeData, incidentTypeData.SetIncidentTypeQuestions);
						}

						foreach (IncidentTypeQuestionData incidentTypeQuestion in incidentTypeData.SetIncidentTypeQuestions)
						{
							if (SmartCadDatabase.IsInitialize(incidentTypeQuestion.Question.App) == true)
							{
								try
								{
									SmartCadDatabase.InitializeLazy(incidentTypeQuestion.Question, incidentTypeQuestion.Question.App);
									int count = incidentTypeQuestion.Question.App.Count;
								}
								catch
								{
									incidentTypeQuestion.Question.App = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(
										SmartCadHqls.GetUserApplicationsByQuestion, incidentTypeQuestion.Question.Code));
								}
								if (incidentTypeQuestion.Question.App.Contains(app))
								{
									incidentTypeQuestion.IncidentType = null;
									IncidentTypeQuestionClientData client = IncidentTypeQuestionConversion.ToClient(incidentTypeQuestion, app);
									client.IncidentType = incidentTypeClientData;
									if (client != null && client.Question != null)
										incidentTypeClientData.IncidentTypeQuestions.Add(client);
								}
							}
						}


						incidentTypeClientData.IncidentTypeDepartmentTypeClients = new ArrayList();
						if (incidentTypeData.SetIncidentTypeDepartmentTypes != null)
						{
							if (SmartCadDatabase.IsInitialize(incidentTypeData.SetIncidentTypeDepartmentTypes) == false)
							{
								SmartCadDatabase.InitializeLazy(incidentTypeData, incidentTypeData.SetIncidentTypeDepartmentTypes);
							}

							foreach (IncidentTypeDepartmentTypeData itdt in incidentTypeData.SetIncidentTypeDepartmentTypes)
							{
								itdt.IncidentType = null;
								IncidentTypeDepartmentTypeClientData client = IncidentTypeDepartmentTypeConversion.ToClient(itdt, app);
								client.IncidentType = incidentTypeClientData;
								incidentTypeClientData.IncidentTypeDepartmentTypeClients.Add(client);

							}

						}

						incidentTypeClientData.IncidentTypeCategories = new ArrayList();
						if (incidentTypeData.IncidentTypeCategories != null)
						{
							if (SmartCadDatabase.IsInitialize(incidentTypeData.IncidentTypeCategories) == false)
							{
								SmartCadDatabase.InitializeLazy(incidentTypeData, incidentTypeData.IncidentTypeCategories);
							}

							foreach (IncidentTypeCategoryData itdt in incidentTypeData.IncidentTypeCategories)
							{
								itdt.IncidentTypes = null;
								incidentTypeClientData.IncidentTypeCategories.Add(IncidentTypeCategoryConversion.ToClient(itdt, app));
							}

						}
					}
				}
				catch (Exception ex)
				{
                    SmartLogger.Print(ex);
                }
                #endregion
            }
			else
			{
				try
				{
					incidentTypeClientData.Code = incidentTypeData.Code;
					incidentTypeClientData.Version = incidentTypeData.Version;
					incidentTypeClientData.CustomCode = incidentTypeData.CustomCode;
					incidentTypeClientData.NoEmergency = (incidentTypeData.NoEmergency.HasValue) ? incidentTypeData.NoEmergency.Value : false;
					incidentTypeClientData.Name = incidentTypeData.Name;
					incidentTypeClientData.FriendlyName = incidentTypeData.FriendlyName;
					incidentTypeClientData.Procedure = incidentTypeData.Procedure;
					incidentTypeClientData.IncidentTypeQuestions = new ArrayList();
					incidentTypeClientData.IconName = incidentTypeData.IconName;

					if (incidentTypeData.Exclusive.HasValue == true)
						incidentTypeClientData.Exclusive = incidentTypeData.Exclusive.Value;

                    if ((app.Name == UserApplicationData.FirstLevel.Name
                        || app.Name == UserApplicationData.Cctv.Name
                        || (UserApplicationData.Alarm != null && app.Name == UserApplicationData.Alarm.Name)
                        || (UserApplicationData.SocialNetworks != null && app.Name == UserApplicationData.SocialNetworks.Name)
                        || (UserApplicationData.Lpr != null && app.Name == UserApplicationData.Lpr.Name)) &&
                        SmartCadDatabase.IsInitialize(incidentTypeData.SetIncidentTypeQuestions) == false)
                    {
                        SmartCadDatabase.InitializeLazy(incidentTypeData, incidentTypeData.SetIncidentTypeQuestions);
                    }

					if (SmartCadDatabase.IsInitialize(incidentTypeData.SetIncidentTypeQuestions) == true)
					{
						foreach (IncidentTypeQuestionData incidentTypeQuestion in incidentTypeData.SetIncidentTypeQuestions)
						{
                            if ((app.Name == UserApplicationData.FirstLevel.Name
                                || app.Name == UserApplicationData.Cctv.Name
                                || (UserApplicationData.Alarm != null && app.Name == UserApplicationData.Alarm.Name)
                                || (UserApplicationData.SocialNetworks != null && app.Name == UserApplicationData.SocialNetworks.Name)
                                || (UserApplicationData.Lpr != null && app.Name == UserApplicationData.Lpr.Name)) &&
                                SmartCadDatabase.IsInitialize(incidentTypeQuestion.Question.App) == false)
                            {
                                SmartCadDatabase.InitializeLazy(incidentTypeQuestion.Question, incidentTypeQuestion.Question.App);
                            }

							if (SmartCadDatabase.IsInitialize(incidentTypeQuestion.Question.App) == true)
							{
								if (incidentTypeQuestion.Question.App.Contains(app))
								{
									incidentTypeQuestion.IncidentType = null;
									IncidentTypeQuestionClientData client = IncidentTypeQuestionConversion.ToClient(incidentTypeQuestion, app);
									client.IncidentType = incidentTypeClientData;
									incidentTypeClientData.IncidentTypeQuestions.Add(client);
								}
							}
						}
					}

					incidentTypeClientData.IncidentTypeDepartmentTypeClients = new ArrayList();
					if (incidentTypeData.SetIncidentTypeDepartmentTypes != null)
					{
						if (SmartCadDatabase.IsInitialize(incidentTypeData.SetIncidentTypeDepartmentTypes) == true)
						{
							foreach (IncidentTypeDepartmentTypeData itdt in incidentTypeData.SetIncidentTypeDepartmentTypes)
							{
								itdt.IncidentType = null;
								IncidentTypeDepartmentTypeClientData client = IncidentTypeDepartmentTypeConversion.ToClient(itdt, app);
								client.IncidentType = incidentTypeClientData;
								incidentTypeClientData.IncidentTypeDepartmentTypeClients.Add(client);

							}
						}
					}

					incidentTypeClientData.IncidentTypeCategories = new ArrayList();
					if (incidentTypeData.IncidentTypeCategories != null)
					{
						if (SmartCadDatabase.IsInitialize(incidentTypeData.IncidentTypeCategories) == true)
						{
							foreach (IncidentTypeCategoryData itdt in incidentTypeData.IncidentTypeCategories)
							{
								itdt.IncidentTypes = null;
								incidentTypeClientData.IncidentTypeCategories.Add(IncidentTypeCategoryConversion.ToClient(itdt, app));
							}
						}
					}
				}
				catch (Exception ex)
				{
                    SmartLogger.Print(ex);
				}
			}
			return incidentTypeClientData;
		}

		public static IncidentTypeClientData ToClient(NHibernate.ISession session, IncidentTypeData incidentTypeData, UserApplicationData app)
		{
			IncidentTypeClientData incidentTypeClientData = new IncidentTypeClientData();
			incidentTypeClientData.Code = incidentTypeData.Code;
			incidentTypeClientData.Version = incidentTypeData.Version;
			incidentTypeClientData.CustomCode = incidentTypeData.CustomCode;
			incidentTypeClientData.Name = incidentTypeData.Name;
			incidentTypeClientData.FriendlyName = incidentTypeData.FriendlyName;
			try
			{
				incidentTypeClientData.Code = incidentTypeData.Code;
				incidentTypeClientData.Version = incidentTypeData.Version;
				incidentTypeClientData.CustomCode = incidentTypeData.CustomCode;
				incidentTypeClientData.NoEmergency = (incidentTypeData.NoEmergency.HasValue) ? incidentTypeData.NoEmergency.Value : false;
				incidentTypeClientData.Name = incidentTypeData.Name;
				incidentTypeClientData.FriendlyName = incidentTypeData.FriendlyName;
				incidentTypeClientData.Procedure = incidentTypeData.Procedure;
				incidentTypeClientData.IncidentTypeQuestions = new ArrayList();
				incidentTypeClientData.IconName = incidentTypeData.IconName;

				if (incidentTypeData.Exclusive.HasValue == true)
					incidentTypeClientData.Exclusive = incidentTypeData.Exclusive.Value;

                if ((app.Name == UserApplicationData.FirstLevel.Name
                    || app.Name == UserApplicationData.Cctv.Name
                    || (UserApplicationData.Alarm != null && app.Name == UserApplicationData.Alarm.Name)
                    || (UserApplicationData.SocialNetworks != null && app.Name == UserApplicationData.SocialNetworks.Name)
                    || (UserApplicationData.Lpr != null && app.Name == UserApplicationData.Lpr.Name)) &&
                    SmartCadDatabase.IsInitialize(incidentTypeData.SetIncidentTypeQuestions) == false)
                {
                    SmartCadDatabase.InitializeLazy(session, incidentTypeData, incidentTypeData.SetIncidentTypeQuestions);
                }

				if (SmartCadDatabase.IsInitialize(incidentTypeData.SetIncidentTypeQuestions) == true)
				{
					foreach (IncidentTypeQuestionData incidentTypeQuestion in incidentTypeData.SetIncidentTypeQuestions)
					{
                        if ((app.Name == UserApplicationData.FirstLevel.Name
                            || app.Name == UserApplicationData.Cctv.Name
                            || (UserApplicationData.Alarm != null && app.Name == UserApplicationData.Alarm.Name)
                            || (UserApplicationData.SocialNetworks != null && app.Name == UserApplicationData.SocialNetworks.Name)
                            || (UserApplicationData.Lpr != null && app.Name == UserApplicationData.Lpr.Name)) &&
                            SmartCadDatabase.IsInitialize(incidentTypeQuestion.Question.App) == false)
                        {
                            SmartCadDatabase.InitializeLazy(session, incidentTypeQuestion.Question, incidentTypeQuestion.Question.App);
                        }

						if (SmartCadDatabase.IsInitialize(incidentTypeQuestion.Question.App) == true)
						{
							if (incidentTypeQuestion.Question.App.Contains(app))
							{
								incidentTypeQuestion.IncidentType = null;
								IncidentTypeQuestionClientData client = IncidentTypeQuestionConversion.ToClient(incidentTypeQuestion, app);
								client.IncidentType = incidentTypeClientData;
								incidentTypeClientData.IncidentTypeQuestions.Add(client);
							}
						}
					}
				}

				incidentTypeClientData.IncidentTypeDepartmentTypeClients = new ArrayList();
				if (incidentTypeData.SetIncidentTypeDepartmentTypes != null)
				{
					if (SmartCadDatabase.IsInitialize(incidentTypeData.SetIncidentTypeDepartmentTypes) == true)
					{
						foreach (IncidentTypeDepartmentTypeData itdt in incidentTypeData.SetIncidentTypeDepartmentTypes)
						{
							itdt.IncidentType = null;
							IncidentTypeDepartmentTypeClientData client = IncidentTypeDepartmentTypeConversion.ToClient(itdt, app);
							client.IncidentType = incidentTypeClientData;
							incidentTypeClientData.IncidentTypeDepartmentTypeClients.Add(client);

						}
					}
				}

				incidentTypeClientData.IncidentTypeCategories = new ArrayList();
				if (incidentTypeData.IncidentTypeCategories != null)
				{
					if (SmartCadDatabase.IsInitialize(incidentTypeData.IncidentTypeCategories) == true)
					{
						foreach (IncidentTypeCategoryData itdt in incidentTypeData.IncidentTypeCategories)
						{
							itdt.IncidentTypes = null;
							incidentTypeClientData.IncidentTypeCategories.Add(IncidentTypeCategoryConversion.ToClient(itdt, app));
						}
					}
				}
			}
			catch (Exception ex)
			{
                SmartLogger.Print(ex);
			}

			return incidentTypeClientData;
		}

        public static IncidentTypeData ToObject(IncidentTypeClientData incidentTypeClientData, UserApplicationData app)
        {
            IncidentTypeData incidentTypeData = new IncidentTypeData();
            incidentTypeData.Code = incidentTypeClientData.Code;
            incidentTypeData = (IncidentTypeData)SmartCadDatabase.RefreshObject(incidentTypeData);
            incidentTypeData.Version = incidentTypeClientData.Version;
            incidentTypeData.CustomCode = incidentTypeClientData.CustomCode;
            incidentTypeData.Exclusive = incidentTypeClientData.Exclusive;
            incidentTypeData.FriendlyName = incidentTypeClientData.FriendlyName;
            incidentTypeData.IconName = incidentTypeClientData.IconName;
            incidentTypeData.Name = incidentTypeClientData.Name;
            incidentTypeData.NoEmergency = incidentTypeClientData.NoEmergency;
            incidentTypeData.Procedure = incidentTypeClientData.Procedure;

            incidentTypeData.IncidentTypeCategories = new ArrayList();
            if (incidentTypeClientData.IncidentTypeCategories != null)
            {
                foreach (IncidentTypeCategoryClientData itcClient in incidentTypeClientData.IncidentTypeCategories)
                {
                    incidentTypeData.IncidentTypeCategories.Add(IncidentTypeCategoryConversion.ToObject(itcClient, app));
                }
            }

            if (incidentTypeClientData.IncidentTypeDepartmentTypeClients != null)
            {
                incidentTypeData.SetIncidentTypeDepartmentTypes = new HashedSet();
                foreach (IncidentTypeDepartmentTypeClientData itdClient in incidentTypeClientData.IncidentTypeDepartmentTypeClients)
                {
                    itdClient.IncidentType = null;
                    IncidentTypeDepartmentTypeData itdt = IncidentTypeDepartmentTypeConversion.ToObject(itdClient, app);
                    itdt.IncidentType = incidentTypeData;
                    incidentTypeData.SetIncidentTypeDepartmentTypes.Add(itdt);
                }
            }

            
            /*if (incidentTypeClientData.IncidentTypeQuestions != null)
            {
                incidentTypeData.SetIncidentTypeQuestions = new HashedSet();
                foreach (IncidentTypeQuestionClientData itqClient in incidentTypeClientData.IncidentTypeQuestions)
                {
                    incidentTypeData.SetIncidentTypeQuestions.Add(IncidentTypeQuestionConversion.ToObject(itqClient, app));
                }
            }*/

            return incidentTypeData;
        }
    }
}
