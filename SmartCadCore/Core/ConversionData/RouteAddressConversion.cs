﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using SmartCadCore.Model;
using SmartCadCore.ClientData;

namespace SmartCadCore.Core
{
	public class RouteAddressConversion
	{
		public static RouteAddressClientData ToClient(RouteAddressData data, UserApplicationData app)
		{
			RouteAddressClientData clientData = new RouteAddressClientData();
			clientData.Name = data.Name;
			clientData.Lon = data.Address.Lon;
			clientData.Lat = data.Address.Lat;
			clientData.Code = data.Code;
			clientData.PointNumber = data.PointNumber;
			clientData.Version = data.Version;

			return clientData;
		}

		public static RouteAddressData ToObject(RouteAddressClientData clientData, UserApplicationData app)
		{
			RouteAddressData data = new RouteAddressData();
			data.Code = clientData.Code;
			data.Version = clientData.Version;
			data.Name = clientData.Name;
			data.Address = new MultipleAddressData();
			data.Address.Lat = clientData.Lat;
			data.Address.Lon = clientData.Lon;
			data.PointNumber = clientData.PointNumber;

			return data;
		}
	}
}