﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    public class EvaluationQuestionConversion
    {
        public static EvaluationQuestionClientData ToClient(EvaluationQuestionData evaluationQuestionData, UserApplicationData app)
        {
            EvaluationQuestionClientData client = new EvaluationQuestionClientData();
            client.Code = evaluationQuestionData.Code;
            client.Version = evaluationQuestionData.Version;
            client.Name = evaluationQuestionData.Name;
            client.Weighting = evaluationQuestionData.Weighting;
            client.Evaluation = new EvaluationClientData();
            client.Evaluation.Code = evaluationQuestionData.Evaluation.Code;
            client.Evaluation.Version = evaluationQuestionData.Evaluation.Version;
            client.Evaluation.Name = evaluationQuestionData.Evaluation.Name;
            client.Evaluation.Description = evaluationQuestionData.Evaluation.Description;
            return client;
        }

        public static EvaluationQuestionData ToObject(EvaluationQuestionClientData clientData, UserApplicationData app)
        {
            EvaluationQuestionData evaluationQuestionData = new EvaluationQuestionData();
			evaluationQuestionData.Code = clientData.Code;
			evaluationQuestionData.Weighting = clientData.Weighting;
			evaluationQuestionData.Name = clientData.Name;
			evaluationQuestionData.Version = clientData.Version;
            return evaluationQuestionData;
        }
    }
}
