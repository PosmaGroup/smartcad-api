﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    public class IndicatorClassConversion
    {
        public static IndicatorClassClientData ToClient(IndicatorClassData objData, UserApplicationData app)
        {
            IndicatorClassClientData convertedClientObject = new IndicatorClassClientData();
            convertedClientObject.Code = objData.Code;
            convertedClientObject.Version = objData.Version;
            convertedClientObject.Description = objData.Description;
            convertedClientObject.FriendlyName = objData.FriendlyName;
            convertedClientObject.Name = objData.Name;
            return convertedClientObject;
        }

        public static IndicatorClassData ToObject(IndicatorClassClientData client)
        {
            IndicatorClassData dataObject = new IndicatorClassData();
            dataObject.Code = client.Code;
            dataObject.Name = client.Name;
            dataObject = SmartCadDatabase.SearchObject<IndicatorClassData>(dataObject);
            return dataObject;
        }
    }
}
