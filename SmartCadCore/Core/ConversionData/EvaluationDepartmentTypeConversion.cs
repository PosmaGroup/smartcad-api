using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    public class EvaluationDepartmentTypeConversion
    {
        public static EvaluationDepartmentTypeClientData ToClient(EvaluationDepartmentTypeData evalDepData, UserApplicationData app)
        {
            EvaluationDepartmentTypeClientData evalDepClientData = new EvaluationDepartmentTypeClientData();
            evalDepClientData.Code = evalDepData.Code;
            evalDepClientData.Version = evalDepData.Version;
            if (evalDepData.Departament != null)
            {
                evalDepClientData.Department = DepartmentTypeConversion.ToClient(evalDepData.Departament, app);
            }
            if (evalDepData.Evaluation != null)
            {
                evalDepClientData.Evaluation = EvaluationConversion.ToClient(evalDepData.Evaluation, app);
            }
            return evalDepClientData;
        }

        public static EvaluationDepartmentTypeData ToObject(EvaluationDepartmentTypeClientData evaluationClientData, UserApplicationData app)
        {
            EvaluationDepartmentTypeData evaluationDpt = new EvaluationDepartmentTypeData();
            evaluationDpt.Code = evaluationClientData.Code;
            evaluationDpt = (EvaluationDepartmentTypeData)SmartCadDatabase.RefreshObject(evaluationDpt);
            evaluationDpt.Departament = DepartmentTypeConversion.ToObject(evaluationClientData.Department, app);
            return evaluationDpt;
        }
    }
}
