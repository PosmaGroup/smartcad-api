using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    public class IncidentStatusConversion
    {
        public static IncidentStatusClientData ToClient(IncidentStatusData incidentStatus, UserApplicationData app)
        {
            IncidentStatusClientData incidentStatusClient = new IncidentStatusClientData();
            incidentStatusClient.Code = incidentStatus.Code;
            incidentStatusClient.Version = incidentStatus.Version;
            incidentStatusClient.CustomCode = incidentStatus.CustomCode;
            incidentStatusClient.FriendlyName = incidentStatus.FriendlyName;
            incidentStatusClient.Name = incidentStatus.Name;
            return incidentStatusClient;
        }

        public static IncidentStatusData ToObject(IncidentStatusClientData incidentStatusClient)
        {
            IncidentStatusData incidentStatus = new IncidentStatusData();
            incidentStatus.Code = incidentStatusClient.Code;
            incidentStatus.CustomCode = incidentStatusClient.CustomCode;
            incidentStatus = SmartCadDatabase.SearchObject <IncidentStatusData>(incidentStatus);
            return incidentStatus;
        }
    }
}
