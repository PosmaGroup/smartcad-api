using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    public class PublicLineConversion
    {
        public static PublicLineClientData ToClient(PublicLineData publicLine, UserApplicationData userApplication)
        {
            if (publicLine == null)
                return null;

            PublicLineClientData clientLine = new PublicLineClientData();

            if (userApplication.Equals(UserApplicationData.FirstLevel))
            {
                clientLine.Code = publicLine.Code;
                clientLine.Version = publicLine.Version;
                clientLine.Name = publicLine.Name;
                clientLine.Telephone = publicLine.Telephone;
                if (publicLine.Address != null)
                    clientLine.Address = AddressConversion.ToClient(publicLine.Address, userApplication);
                clientLine.Public = publicLine.Public;
                clientLine.CallerName = publicLine.CallerName;
                if (publicLine.CallerAddress != null)
                    clientLine.CallerAddress = AddressConversion.ToClient(publicLine.CallerAddress, userApplication);
                clientLine.Updated = publicLine.Updated;
            }

            return clientLine;
        }

        public static PublicLineData ToObject(PublicLineClientData publicLine)
        {
			if (publicLine == null)
				return null;
			System.Collections.IList list = SmartCadDatabase.SearchObjects(
							SmartCadHqls.GetCustomHql("Select obj From PublicLineData obj where obj.Telephone = '{0}'", publicLine.Telephone));
			PublicLineData lineData = null;
			if (list != null && list.Count > 0)
				lineData = list[0] as PublicLineData;
			else
			{
				lineData = new PublicLineData();

				lineData.Code = publicLine.Code;
				lineData.Version = publicLine.Version;
				lineData.Telephone = publicLine.Telephone;
			}
			lineData.Name = publicLine.Name;
			if (publicLine.Address != null)
				lineData.Address = new PublicLineAddressData(AddressConversion.ToObject(publicLine.Address));
			lineData.Public = publicLine.Public;
			lineData.CallerName = publicLine.CallerName;
			if (publicLine.CallerAddress != null)
				lineData.CallerAddress = new PublicLineCallerAddressData(AddressConversion.ToObject(publicLine.CallerAddress));
			lineData.Updated = DateTime.Now;

			return lineData;
        }
    }
}
