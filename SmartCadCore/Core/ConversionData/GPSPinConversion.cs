﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    public class GPSPinConversion
    {
        public static GPSPinClientData ToClient(GPSPinData gpsPin, UserApplicationData app)
        {
            GPSPinClientData client = new GPSPinClientData();
            client.Code = gpsPin.Code;
            client.Version = gpsPin.Version;
            client.Number = gpsPin.Number;
            client.Type = (GPSPinClientData.PinType)(int)gpsPin.Type;

            return client;
        }

        public static GPSPinData ToObject(GPSPinClientData gpsPin, UserApplicationData app)
        {
            GPSPinData data = new GPSPinData();
            data.Code = gpsPin.Code;
            data.Version = gpsPin.Version;
            data.Type = (GPSPinData.PinType)(int)gpsPin.Type;
            data.Number = gpsPin.Number;

            return data;
        }

    }
}
