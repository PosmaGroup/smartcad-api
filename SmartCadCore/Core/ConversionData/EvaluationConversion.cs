using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using SmartCadCore.ClientData;
using System.Collections;
using Iesi.Collections;

namespace SmartCadCore.Core
{
    public class EvaluationConversion
    {
        public static EvaluationClientData ToClient(EvaluationData evaluationData, UserApplicationData app)
        {
            EvaluationClientData evaluationClientData = new EvaluationClientData();
            evaluationClientData.Code = evaluationData.Code;
            evaluationClientData.Version = evaluationData.Version;
            evaluationClientData.Name = evaluationData.Name;
            evaluationClientData.Description = evaluationData.Description;
            evaluationClientData.Scale = (EvaluationClientData.EvaluationScales) ((int)evaluationData.Scale);
            evaluationClientData.OperatorCategory = OperatorCategoryConversion.ToClient(evaluationData.OperatorCategory, app);

            if (app.Equals(UserApplicationData.Administration))
            {
                #region Administration

                SmartCadDatabase.InitializeLazy(evaluationData, evaluationData.SetEvaluationUserApplications);
                evaluationClientData.Applications = new List<EvaluationUserApplicationClientData>();
                if (evaluationData.SetEvaluationUserApplications != null)
                {
                    foreach (EvaluationUserApplicationData application in evaluationData.SetEvaluationUserApplications)
                    {
                        EvaluationUserApplicationClientData appClient = EvaluationUserAppllicationConversion.ToClient(application, app);
                        appClient.Evaluation = evaluationClientData;
                        evaluationClientData.Applications.Add(appClient);
                    }
                }

                SmartCadDatabase.InitializeLazy(evaluationData, evaluationData.SetEvaluationDepartamentsType);
                evaluationClientData.Departments = new List<EvaluationDepartmentTypeClientData>();
                foreach (EvaluationDepartmentTypeData dep in evaluationData.SetEvaluationDepartamentsType)
                {
                    if (dep.Departament != null)
                    {
                        dep.Evaluation = null;
                        EvaluationDepartmentTypeClientData depClient = EvaluationDepartmentTypeConversion.ToClient(dep, app);
                        depClient.Evaluation = evaluationClientData;
                        evaluationClientData.Departments.Add(depClient);
                    }
                }
                if (evaluationData.InitializeCollections == true || SmartCadDatabase.IsInitialize(evaluationData.SetQuestions))
                {
                    SmartCadDatabase.InitializeLazy(evaluationData, evaluationData.SetQuestions);
                    evaluationClientData.Questions = new List<EvaluationQuestionClientData>();
                    foreach (EvaluationQuestionData question in evaluationData.SetQuestions)
                    {
                        EvaluationQuestionClientData questionClient = EvaluationQuestionConversion.ToClient(question, app);
                        evaluationClientData.Questions.Add(questionClient);
                    }
                }
                #endregion
            }
            else if (app.Equals(UserApplicationData.Supervision))
            {
                #region Supervision
                if (SmartCadDatabase.IsInitialize(evaluationData.SetEvaluationUserApplications) == true)
                {
                    //SmartCadDatabase.InitializeLazy(evaluationData, evaluationData.SetEvaluationUserApplications);
                    evaluationClientData.Applications = new List<EvaluationUserApplicationClientData>();
                    if (evaluationData.SetEvaluationUserApplications != null)
                    {
                        foreach (EvaluationUserApplicationData application in evaluationData.SetEvaluationUserApplications)
                        {
                            application.Evaluation.SetEvaluationUserApplications = null;
                            EvaluationUserApplicationClientData appClient = EvaluationUserAppllicationConversion.ToClient(application, app);
                            evaluationClientData.Applications.Add(appClient);
                        }
                    }
                }
                if (SmartCadDatabase.IsInitialize(evaluationData.SetEvaluationDepartamentsType) == true)
                {
                    //SmartCadDatabase.InitializeLazy(evaluationData, evaluationData.SetEvaluationDepartamentsType);
                    evaluationClientData.Departments = new List<EvaluationDepartmentTypeClientData>();
                    foreach (EvaluationDepartmentTypeData dep in evaluationData.SetEvaluationDepartamentsType)
                    {
                        if (dep.Departament != null)
                        {
                            dep.Evaluation = null;
                            EvaluationDepartmentTypeClientData depClient = EvaluationDepartmentTypeConversion.ToClient(dep, app);
                            depClient.Evaluation = evaluationClientData;
                            evaluationClientData.Departments.Add(depClient);
                        }
                    }
                }
                if (SmartCadDatabase.IsInitialize(evaluationData.SetQuestions) == true)
                {
                    //SmartCadDatabase.InitializeLazy(evaluationData, evaluationData.SetQuestions);
                    evaluationClientData.Questions = new List<EvaluationQuestionClientData>();
                    foreach (EvaluationQuestionData question in evaluationData.SetQuestions)
                    {
                        EvaluationQuestionClientData questionClient = EvaluationQuestionConversion.ToClient(question, app);
                        evaluationClientData.Questions.Add(questionClient);
                    }
                }
                #endregion
            }
            return evaluationClientData;
        }

        public static EvaluationData ToObject(EvaluationClientData evaluationClientData, UserApplicationData app)
        {
            EvaluationData evaluationData = new EvaluationData();
            evaluationData.Code = evaluationClientData.Code;            
            evaluationData = (EvaluationData)SmartCadDatabase.RefreshObject(evaluationData);
            evaluationData.Version = evaluationClientData.Version;
            evaluationData.Name = evaluationClientData.Name;            
            evaluationData.Description = evaluationClientData.Description;
            OperatorCategoryData category = new OperatorCategoryData();
            category.Name = evaluationClientData.OperatorCategory.Name;
            evaluationData.OperatorCategory = SmartCadDatabase.SearchObject<OperatorCategoryData>(category);
            
            evaluationData.Scale = (SmartCadCore.Model.EvaluationData.EvaluationScales)((int)evaluationClientData.Scale);
            SmartCadDatabase.InitializeLazy(evaluationData, evaluationData.SetEvaluationDepartamentsType);
            if (evaluationData.SetEvaluationDepartamentsType != null && evaluationData.SetEvaluationDepartamentsType.Count > 0)
            {
                ArrayList list = new ArrayList();
                list.AddRange(evaluationData.SetEvaluationDepartamentsType);
                foreach (EvaluationDepartmentTypeData edtd in list)
                {
                    evaluationData.SetEvaluationDepartamentsType.Remove(edtd);
                }
            }
            else if (evaluationData.SetEvaluationDepartamentsType == null)
            {
                evaluationData.SetEvaluationDepartamentsType = new ListSet();
            }
            foreach (EvaluationDepartmentTypeClientData edtd in evaluationClientData.Departments)
            {
                EvaluationDepartmentTypeData data = EvaluationDepartmentTypeConversion.ToObject(edtd, app);
                data.Evaluation = evaluationData;
                evaluationData.SetEvaluationDepartamentsType.Add(data);
            }
            SmartCadDatabase.InitializeLazy(evaluationData, evaluationData.SetEvaluationUserApplications);
            try
            {
                if (evaluationData.Code != 0)
                {
                    int count = evaluationData.SetEvaluationUserApplications.Count;
                }
                else
                {
                    evaluationData.SetEvaluationUserApplications = new ListSet();
                }
            }
            catch
            {
                evaluationData.SetEvaluationUserApplications = new ListSet((ICollection)SmartCadDatabase.SearchObjects(
                    SmartCadHqls.GetCustomHql(SmartCadHqls.GetEvaluationUserAppByEvaluationCode, evaluationData.Code)));
            }
            if (evaluationData.SetEvaluationUserApplications != null && evaluationData.SetEvaluationUserApplications.Count > 0)
            {
                ArrayList list = new ArrayList();
                list.AddRange(evaluationData.SetEvaluationUserApplications);
                foreach (EvaluationUserApplicationData euad in list)
                {
                    evaluationData.SetEvaluationUserApplications.Remove(euad);
                }
            }
            else if (evaluationData.SetEvaluationUserApplications == null)
            {
                evaluationData.SetEvaluationUserApplications = new ListSet();
            }
            foreach (EvaluationUserApplicationClientData euacd in evaluationClientData.Applications)
            {
                EvaluationUserApplicationData data = EvaluationUserAppllicationConversion.ToObject(euacd, app);
                data.Evaluation = evaluationData;
                data.Application = UserApplicationConversion.ToObject(euacd.Application, app);
                evaluationData.SetEvaluationUserApplications.Add(data);
            }
            SmartCadDatabase.InitializeLazy(evaluationData, evaluationData.SetQuestions);
            try
            {
                if (evaluationData.SetQuestions != null)
                {
                    int count = evaluationData.SetQuestions.Count;
                }
            }
            catch
            {
                IList questionList = SmartCadDatabase.SearchObjects(
                    SmartCadHqls.GetCustomHql(SmartCadHqls.GetQuestionsByEvaluationCode, evaluationData.Code));
                evaluationData.SetQuestions = questionList;
            }
            if (evaluationData.SetQuestions != null && evaluationData.SetQuestions.Count > 0)
            {
                ArrayList list = new ArrayList();
                list.AddRange(evaluationData.SetQuestions);
                foreach (EvaluationQuestionData question in list)
                {
                    evaluationData.SetQuestions.Remove(question);
                }
            }
            else
            {
                evaluationData.SetQuestions = new ArrayList();
            }
            foreach (EvaluationQuestionClientData question in evaluationClientData.Questions)
            {
                EvaluationQuestionData data = EvaluationQuestionConversion.ToObject(question, app);
                data.Evaluation = evaluationData;
                evaluationData.SetQuestions.Add(data);
            }
            return evaluationData;
        }
    }
}