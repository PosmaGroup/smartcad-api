using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;
using System.Collections;

namespace SmartCadCore.Core
{
    public class ObservationTypeConversion
    {
        public static ObservationTypeClientData ToClient(ObservationTypeData data, UserApplicationData app) 
        {
            ObservationTypeClientData client = new ObservationTypeClientData();
            client.Name = data.Name;
            client.FriendlyName = data.FriendlyName;
            client.Code = data.Code;
			client.UserAccessName = new ArrayList();
			if (SmartCadDatabase.IsInitialize(data.UserAccess) == false)
				SmartCadDatabase.InitializeLazy(data, data.UserAccess);
			foreach (UserAccessData access in data.UserAccess)
			{
				client.UserAccessName.Add(access.Name);
			}
            return client;
        }
    }
}
