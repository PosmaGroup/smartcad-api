﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    public class RankConversion
    {
        public static RankClientData ToClient(RankData rank, UserApplicationData app)
        {
            RankClientData rankClientData = new RankClientData();
            rankClientData.Code = rank.Code;
            rankClientData.Version = rank.Version;
            rankClientData.Name = rank.Name;
            rankClientData.DepartmentTypeName = rank.DepartmentType.Name;
            rankClientData.DepartmentTypeCode = rank.DepartmentType.Code;
            rankClientData.Description = rank.Description;
            return rankClientData;
        }

        public static RankData ToObject(RankClientData rankClient, UserApplicationData app)
        {
            RankData rank = new RankData();
            rank.Code = rankClient.Code;
            rank.Version = rankClient.Version;
            rank.Name = rankClient.Name;
            rank.Description = rankClient.Description;
            DepartmentTypeData dep = new DepartmentTypeData(rankClient.DepartmentTypeName);
            dep.Code = rankClient.DepartmentTypeCode;
            rank.DepartmentType = (DepartmentTypeData)SmartCadDatabase.RefreshObject(dep);
            return rank;
        }
    }
}
