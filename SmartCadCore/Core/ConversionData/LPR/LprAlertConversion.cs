﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core.ConversionData.LPR
{
    class LprAlertConversion
    {
        public static LprAlertClientData ToClient(LprAlertData data, UserApplicationData app)
        {
            LprAlertClientData client = new LprAlertClientData();
            client.Code = data.Code;
            client.AlertDate = data.AlertDate;
            client.ExternalId = data.ExternalId;
            client.Version = data.Version;
            client.Camera = CameraConversion.ToClient(data.Camera, app);
            client.Plate = data.Plate;
            client.VehiculeCode = data.VehiculeCode;


            return client;
        }

        public static LprAlertData ToObject(LprAlertClientData client, UserApplicationData app)
        {  
            LprAlertData data = new LprAlertData();
            data.Code = client.Code;
            data.AlertDate = client.AlertDate;
            data.ExternalId = client.ExternalId;
            data.Version = client.Version;
            data.Camera = CameraConversion.ToObject(client.Camera, app);
            data.Plate = client.Plate;
            data.VehiculeCode = client.VehiculeCode;

            return data;
        }
    }
}
