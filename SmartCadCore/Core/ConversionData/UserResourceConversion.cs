﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.Model;
using SmartCadCore.ClientData;

namespace SmartCadCore.Core
{
    public class UserResourceConversion
    {
        public static UserResourceClientData ToClient(UserResourceData userResourceData, UserApplicationData app)
        {
            UserResourceClientData resource = new UserResourceClientData();
            resource.Code = userResourceData.Code;
            resource.Version = userResourceData.Version;
            resource.Name = userResourceData.Name;
            resource.FriendlyName = userResourceData.FriendlyName;
            return resource;
        }

        public static UserResourceData ToObject(UserResourceClientData userResourceData, UserApplicationData app)
        {
            UserResourceData resource = new UserResourceData();
            resource.Code = userResourceData.Code;
            resource.Name = userResourceData.Name;
            resource = SmartCadDatabase.SearchObject<UserResourceData>(resource);
            return resource;
        }
    }
}
