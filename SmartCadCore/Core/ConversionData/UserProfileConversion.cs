using System;
using System.Collections;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;
using Iesi.Collections;
using System.Collections.Generic;

namespace SmartCadCore.Core
{
    public class UserProfileConversion
    {
        public static UserProfileClientData ToClient(UserProfileData userProfileData, UserApplicationData app)
        {
            UserProfileClientData clientData = new UserProfileClientData();
            clientData.Code = userProfileData.Code;
            clientData.Description = userProfileData.Description;
            clientData.FriendlyName = userProfileData.FriendlyName;
            clientData.Immutable = userProfileData.Immutable;
            clientData.Name = userProfileData.Name;
            clientData.Version = userProfileData.Version;

            if (app.Name == UserApplicationData.Administration.Name)
            {
                if (SmartCadDatabase.IsInitialize(userProfileData.Accesses) == false)
                    SmartCadDatabase.InitializeLazy(userProfileData, userProfileData.Accesses);
                clientData.Accesses = new List<UserAccessClientData>();
                foreach (UserAccessData data in userProfileData.Accesses)
                {
                    data.UserApplication = null;
                    clientData.Accesses.Add(UserAccessConversion.ToClient(data, app));
                }
            }
            else
            {
                clientData.Accesses = new List<UserAccessClientData>();
            }
            return clientData;
        }

        public static UserProfileData ToObject(UserProfileClientData userProfileClientData, UserApplicationData app)
        {
            UserProfileData userProfile = new UserProfileData();
            if (userProfileClientData.Accesses != null)
            {
                userProfile.Accesses = new HashedSet();
                foreach (UserAccessClientData access in userProfileClientData.Accesses)
                {
                    userProfile.Accesses.Add(UserAccessConversion.ToObject(access,app));
                }
            }
            userProfile.Code = userProfileClientData.Code;
            userProfile.Description = userProfileClientData.Description;
            userProfile.FriendlyName = userProfileClientData.FriendlyName;
            userProfile.Immutable = userProfileClientData.Immutable;
            userProfile.Name = userProfileClientData.Name;
            userProfile.Version = userProfileClientData.Version;
            return userProfile;
        }
    }
}
