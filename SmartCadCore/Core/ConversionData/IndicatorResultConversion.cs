using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using SmartCadCore.ClientData;

namespace SmartCadCore.Core
{
    public class IndicatorResultConversion
    {
        public static IndicatorResultClientData ToClient(IndicatorResultData indicatorResult, UserApplicationData app)
        {
            IndicatorResultClientData result = new IndicatorResultClientData();
            result.Code = indicatorResult.Code;
            result.Version = indicatorResult.Version;
            result.IndicatorName = indicatorResult.Indicator.Name;
            result.IndicatorMnemonic = indicatorResult.Indicator.Mnemonic;
            result.IndicatorFrecuency = indicatorResult.Indicator.Frecuency;
            result.IndicatorMeasureUnit = indicatorResult.Indicator.MeasureUnit;
            result.IndicatorTypeFriendlyName = indicatorResult.Indicator.Type.FriendlyName;
            result.IndicatorTypeName = indicatorResult.Indicator.Type.Name;
            result.Time = indicatorResult.Time;
            result.IndicatorCustomCode = indicatorResult.Indicator.CustomCode;
            result.IndicatorCode = indicatorResult.Indicator.Code;
            result.Values = new ArrayList();
           
            if (SmartCadDatabase.IsInitialize(indicatorResult.Result) == true)
            {
                foreach (IndicatorResultValuesData resultValue in indicatorResult.Result)
                {
                    IndicatorResultValuesClientData temp = new IndicatorResultValuesClientData();
                    temp.CustomCode = resultValue.CustomCode;
                    temp.IndicatorClassName = resultValue.IndicatorClass.Name;
                    temp.IndicatorClassFriendlyName = resultValue.IndicatorClass.FriendlyName;
                    temp.Code = resultValue.Code;
                    temp.Version = resultValue.Version;
                    temp.Threshold = resultValue.Threshold;
                    temp.ResultValue = resultValue.Value;
                    temp.MeasureUnit = indicatorResult.Indicator.MeasureUnit;
                    temp.Trend = resultValue.Trend;
                    temp.IndicatorTypeName = indicatorResult.Indicator.Type.Name;
                    temp.IndicatorTypeFriendlyName = indicatorResult.Indicator.Type.FriendlyName;
                    temp.IndicatorName = indicatorResult.Indicator.Name;
                    temp.Date = result.Time;
                    temp.IndicatorResultCode = resultValue.Result.Indicator.Code;
                    temp.IndicatorResultClassCode = resultValue.IndicatorClass.Code;
                    temp.Information = resultValue.Information;
                    temp.IndicatorCustomCode = result.IndicatorCustomCode;
                    temp.Description = resultValue.Description;
                    temp.IndicatorDescription = indicatorResult.Indicator.Description;
                    temp.LocationGrid = indicatorResult.Indicator.IndicatorLocation == IndicatorPosition.Up;
                    result.Values.Add(temp);
                }
            }
            return result;
        }
    }
}
