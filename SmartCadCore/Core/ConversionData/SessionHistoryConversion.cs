using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;
using System.Collections;

namespace SmartCadCore.Core
{
    public class SessionHistoryConversion
    {
        public static SessionHistoryClientData ToClient(SessionHistoryData sessionHistory, UserApplicationData app)
        {
            SessionHistoryClientData sessionHistoryClient = new SessionHistoryClientData();
            sessionHistoryClient.Code = sessionHistory.Code;
            sessionHistoryClient.StartDateLogin = sessionHistory.StartDateLogin.HasValue ? sessionHistory.StartDateLogin.Value : DateTime.MinValue;
            sessionHistoryClient.EndDateLogin = sessionHistory.EndDateLogin.HasValue ? sessionHistory.EndDateLogin.Value : DateTime.MinValue;
            sessionHistoryClient.IsLoggedIn = sessionHistory.IsLoggedIn.HasValue ? sessionHistory.IsLoggedIn.Value : false;
            sessionHistoryClient.UserApplication = sessionHistory.UserApplication.Name;
            sessionHistoryClient.Operator = sessionHistory.UserAccount.Code;
            sessionHistoryClient.Version = sessionHistory.Version;
            sessionHistoryClient.StatusHistoryList = new ArrayList();
            sessionHistoryClient.ComputerName = sessionHistory.ComputerName;
            if (app == UserApplicationData.Supervision)
            {
                if (string.IsNullOrEmpty(sessionHistory.ComputerName) == false)
                {
                    object temp = SmartCadDatabase.SearchBasicObject(
                        SmartCadHqls.GetCustomHql(SmartCadHqls.GetRoomsSeatByComputerName, sessionHistory.ComputerName));
                    if (temp != null)
                    {
                        sessionHistoryClient.NumberSeat = (temp as RoomSeatData).NumberSeat;
                        if((temp as RoomSeatData).Room != null)
                            sessionHistoryClient.Room = (temp as RoomSeatData).Room.Name;
                    }
                }
                if (SmartCadDatabase.IsInitialize(sessionHistory.SessionHistoryStatusList) == false)
                {
                    SmartCadDatabase.InitializeLazy(sessionHistory, sessionHistory.SessionHistoryStatusList);

                    foreach (SessionStatusHistoryData sshd in sessionHistory.SessionHistoryStatusList)
                    {
                        sessionHistoryClient.StatusHistoryList.Add(SessionStatusHistoryConversion.ToClient(sshd, app));
                    }
                }
            }
            return sessionHistoryClient;
        }

    }    
}
