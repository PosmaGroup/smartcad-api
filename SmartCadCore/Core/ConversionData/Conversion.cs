using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using SmartCadCore.ClientData;
using SmartCadCore.Model.Util;
using SmartCadCore.Core.ConversionData;
using SmartCadCore.ClientData.Util;

namespace SmartCadCore.Core
{
    public class Conversion
    {
        public static ClientData.ClientData ToClient(ObjectData objectData, UserApplicationData application)
        { 
            ClientData.ClientData result = null;
            if (objectData is InstalledApplicationData) 
            {
                result = InstalledApplicationConversion.ToClient(objectData as InstalledApplicationData);
            }
            if (objectData is OperatorStatusData)
            {
                result = OperatorStatusConversion.ToClient(objectData as OperatorStatusData, application);
            }
            else if (objectData is UserProfileData)
            {
                result = UserProfileConversion.ToClient(objectData as UserProfileData, application);
            }
            else if (objectData is PublicLineData)
            {
                result = PublicLineConversion.ToClient(objectData as PublicLineData, application);
            }
            else if (objectData is IncidentData)
            {
                result = IncidentConversion.ToClient(objectData as IncidentData, application);
            }
            else if (objectData is SessionHistoryData)
            {
                result = SessionHistoryConversion.ToClient(objectData as SessionHistoryData, application);
            }
            else if (objectData is UserRoleData)
            {
                result = UserRoleConversion.ToClient(objectData as UserRoleData, application);
            }
            else if (objectData is OperatorData)
            {
                result = OperatorConversion.ToClient(objectData as OperatorData, application);
            }
            else if (objectData is IncidentTypeData)
            {
                result = IncidentTypeConversion.ToClient(objectData as IncidentTypeData, application);
            }
            else if (objectData is UnitNoteData)
            {
                result = UnitNoteConversion.ToClient(objectData as UnitNoteData);
            }
            else if (objectData is IncidentNoteData)
            {
                result = IncidentNoteConversion.ToClient(objectData as IncidentNoteData, application);
            }
            else if (objectData is QuestionData)
            {
                result = QuestionConversion.ToClient(objectData as QuestionData, application);
            }
            else if (objectData is SupportRequestReportData)
            {
                result = SupportRequestReportConversion.ToClient(objectData as SupportRequestReportData, application);
            }
            else if (objectData is PhoneReportData)
            {
                result = PhoneReportConversion.ToClient(objectData as PhoneReportData, application);
            }
            else if (objectData is PhoneReportAudioData)
            {
                result = PhoneReportAudioConversion.ToClient(objectData as PhoneReportAudioData, application);
            }
            else if (objectData is UnitTypeData)
            {
                result = UnitTypeConversion.ToClient(objectData as UnitTypeData, application);
            }
            else if (objectData is UnitData)
            {
                result = UnitConversion.ToClient(objectData as UnitData, application);
            }
            else if (objectData is OfficerData)
            {
                result = OfficerConversion.ToClient(objectData as OfficerData, application);
            }
            else if (objectData is DepartmentStationAssignHistoryData)
            {
                result = DepartmentStationAssignHistoryConversion.ToClient(objectData as DepartmentStationAssignHistoryData, application);
            }
            else if (objectData is IncidentNotificationData)
            {
                result = IncidentNotificationConversion.ToClient(objectData as IncidentNotificationData, application);
            }            
            else if (objectData is EvaluationData)
            {
                result = EvaluationConversion.ToClient(objectData as EvaluationData, application);
            }
            else if (objectData is CctvZoneData)
            {
                result = CctvZoneConversion.ToClient(objectData as CctvZoneData, application);
            }
            else if (objectData is StructData)
            {
                result = StructConversion.ToClient(objectData as StructData, application);
            }
            else if (objectData is DeviceData)
            {                
                result = DeviceConversion.ToClient(objectData as DeviceData, application);
            }
            else if (objectData is LprTypeData)
            {
                result = LprTypeConversion.ToClient(objectData as LprTypeData, application);
            }
            else if (objectData is SensorTelemetryData)
            {
                result = SensorTelemetryConversion.ToClient(objectData as SensorTelemetryData, application);
            }
            else if (objectData is LprWayData)
            {
                result = LprWayConversion.ToClient(objectData as LprWayData, application);
            }
            else if (objectData is RoomData)
            {
                result = RoomConversion.ToClient(objectData as RoomData, application);
            }
            else if (objectData is RoomSeatData)
            {
                result = RoomSeatConversion.ToClient(objectData as RoomSeatData, application);
            }
            else if (objectData is OperatorObservationData)
            {
                result = OperatorObservationConversion.ToClient(objectData as OperatorObservationData, application);
            }
            else if (objectData is OperatorEvaluationData)
            {
                result = OperatorEvaluationConversion.ToClient(objectData as OperatorEvaluationData, application);
            }
            else if (objectData is ObservationTypeData)
            {
                result = ObservationTypeConversion.ToClient(objectData as ObservationTypeData, application);
            }
            else if (objectData is OperatorCategoryHistoryData)
            {
                result = OperatorCategoryHistoryConversion.ToClient(objectData as OperatorCategoryHistoryData, application);
            }
            else if (objectData is OperatorAssignData)
            {
                result = OperatorAssignConversion.ToClient(objectData as OperatorAssignData, application);
            }
            else if (objectData is TrainingCourseData) {
                result = TrainingCourseConversion.ToClient(objectData as TrainingCourseData, application);
            }
            else if (objectData is TrainingCourseScheduleData)
            {
                result = TrainingCourseScheduleConversion.ToClient(objectData as TrainingCourseScheduleData, application);
            }
            else if (objectData is TrainingCourseSchedulePartsData)
            {
                result = TrainingCourseSchedulePartsConversion.ToClient(objectData as TrainingCourseSchedulePartsData, application);
            }
            else if (objectData is OperatorTrainingCourseData)
            {
                result = OperatorTrainingCourseConversion.ToClient(objectData as OperatorTrainingCourseData, application);
            }
            else if (objectData is OperatorCategoryData)
            {
                result = OperatorCategoryConversion.ToClient(objectData as OperatorCategoryData, application);
            }
            else if (objectData is IndicatorData)
            {
                result = IndicatorConversion.ToClient(objectData as IndicatorData, application);
            }
            else if (objectData is IndicatorClassDashboardData)
            {
                result = IndicatorClassDashboardConversion.ToClient(objectData as IndicatorClassDashboardData, application);
            }
            else if (objectData is SupervisorCloseReportData)
            {
                result = SupervisorCloseReportConversion.ToClient(objectData as SupervisorCloseReportData, application);
            }
            else if (objectData is SupervisorCloseReportMessageData)
            {
                result = SupervisorCloseReportMessageConversion.ToClient(objectData as SupervisorCloseReportMessageData, application);
            }
            else if (objectData is IndicatorForecastData)
            {
                result = IndicatorForecastConversion.ToClient(objectData as IndicatorForecastData, application);
            }
            else if (objectData is IndicatorResultData)
            {
                result = IndicatorResultConversion.ToClient(objectData as IndicatorResultData, application);
            }
            else if (objectData is WorkShiftVariationData)
            {
                result = WorkShiftVariationConversion.ToClient(objectData as WorkShiftVariationData, application);
            }
            else if (objectData is IndicatorGroupForecastData)
            {
                result = IndicatorGroupForecastConversion.ToClient(objectData as IndicatorGroupForecastData, application);
            }
            else if (objectData is MotiveVariationData)
            {
                result = MotiveVariationConversion.ToClient(objectData as MotiveVariationData, application);
            }
            else if (objectData is IndicatorResultData)
            {
                result = IndicatorResultConversion.ToClient(objectData as IndicatorResultData, application);
            }
            else if (objectData is WorkShiftScheduleVariationData)
            {
                result = WorkShiftScheduleVariationConversion.ToClient(objectData as WorkShiftScheduleVariationData, application);
            }
            else if (objectData is DispatchOrderData)
            {
                result = DispatchOrderConversion.ToClient(objectData as DispatchOrderData, application);
            }
            else if (objectData is IncidentData)
            {
                result = IncidentConversion.ToClient(objectData as IncidentData, application);
            }
            else if (objectData is DepartmentZoneData)
            {
                result = DepartmentZoneConversion.ToClient(objectData as DepartmentZoneData, application);
            }
            else if (objectData is DepartmentZoneAddressData)
            {
                result = DepartmentZoneAddressConversion.ToClient(objectData as DepartmentZoneAddressData, application);
            }
            else if (objectData is DepartmentTypeData)
            {
                result = DepartmentTypeConversion.ToClient(objectData as DepartmentTypeData, application);
            }
            else if (objectData is DepartmentStationData)
            {
                result = DepartmentStationConversion.ToClient(objectData as DepartmentStationData, application);
            }
            else if (objectData is ReportBaseDepartmentTypeData)
            {
                result = ReportBaseDepartmentTypeConversion.ToClient(objectData as ReportBaseDepartmentTypeData, application);
            }
            else if (objectData is RankData)
            {
                result = RankConversion.ToClient(objectData as RankData, application);
            }
            else if (objectData is PositionData)
            {
                result = PositionConversion.ToClient(objectData as PositionData, application);
            }
            else if (objectData is IncidentNotificationPriorityData)
            {
                result = IncidentNotificationPriorityConversion.ToClient(objectData as IncidentNotificationPriorityData, application);
            }
            else if (objectData is ApplicationPreferenceData)
            {
                result = ApplicationPreferenceConversion.ToClient(objectData as ApplicationPreferenceData, application);
            }
            else if (objectData is SessionStatusHistoryData)
            {
                result = SessionStatusHistoryConversion.ToClient(objectData as SessionStatusHistoryData, application);
            }
            else if (objectData is UserResourceData)
            {
                result = UserResourceConversion.ToClient(objectData as UserResourceData, application);
            }
            else if (objectData is UnitStatusData)
            {
                result = UnitStatusConversion.ToClient(objectData as UnitStatusData, application);
            }
            else if (objectData is IncidentNotificationStatusData)
            {
                result = IncidentNotificationStatusConversion.ToClient(objectData as IncidentNotificationStatusData, application);
            }
            else if (objectData is DispatchOrderStatusData)
            {
                result = DispatchOrderStatusConversion.ToClient(objectData as DispatchOrderStatusData, application);
            }
            else if (objectData is UserActionData)
            {
                result = UserActionConversion.ToClient(objectData as UserActionData, application);
            }
            else if (objectData is IndicatorTypeData)
            {
                result = IndicatorTypeConversion.ToClient(objectData as IndicatorTypeData, application);
            }
            else if (objectData is IndicatorClassData)
            {
                result = IndicatorClassConversion.ToClient(objectData as IndicatorClassData, application);
            }
            else if (objectData is IncidentStatusData)
            {
                result = IncidentStatusConversion.ToClient(objectData as IncidentStatusData, application);
            }
            else if (objectData is UserApplicationData)
            {
                result = UserApplicationConversion.ToClient(objectData as UserApplicationData, application);
            }
            else if (objectData is UnitEndingReportTypeData)
            {
                result = UnitEndingReportTypeConversion.ToClient(objectData as UnitEndingReportTypeData, application);
            }
            else if (objectData is StructTypeData)
            {
                result = StructTypeConversion.ToClient(objectData as StructTypeData, application);
            }
            else if (objectData is DepartmentStationAddressData)
            {
                result = DepartmentStationAddressConversion.ToClient(objectData as DepartmentStationAddressData, application);
            }
            else if (objectData is DepartmentZoneAddressData)
            {
                result = DepartmentZoneAddressConversion.ToClient(objectData as DepartmentZoneAddressData, application);
            }
            else if (objectData is UserAccessData)
            {
                result = UserAccessConversion.ToClient(objectData as UserAccessData, application);
            }
            else if (objectData is UserPermissionData)
            {
                result = UserPermissionConversion.ToClient(objectData as UserPermissionData, application);
            }
            else if (objectData is PossibleAnswerAnswerData)
            {
                result = PossibleAnswerAnswerConversion.ToClient(objectData as PossibleAnswerAnswerData, application);
            }
            else if (objectData is CameraTypeData)
            {
                result = CameraTypeConversion.ToClient(objectData as CameraTypeData, application);
            }
            else if (objectData is ConnectionTypeData)
            {
                result = ConnectionTypeConversion.ToClient(objectData as ConnectionTypeData, application);
            }
            else if (objectData is UnitOfficerAssignHistoryData)
            {
                result = UnitOfficerAssignHistoryConversion.ToClient(objectData as UnitOfficerAssignHistoryData, application);
            }
            else if (objectData is OperatorDeviceData)
            {
                result = OperatorDeviceConversion.ToClient(objectData as OperatorDeviceData, application);
            }
			else if (objectData is IncidentTypeQuestionData)
			{
				result = IncidentTypeQuestionConversion.ToClient(objectData as IncidentTypeQuestionData, application);
			}
			else if (objectData is IncidentTypeDepartmentTypeData)
			{
				result = IncidentTypeDepartmentTypeConversion.ToClient(objectData as IncidentTypeDepartmentTypeData, application);
			}
            else if (objectData is AlertData)
            {
                result = AlertConversion.ToClient(objectData as AlertData, application);
            }
			else if (objectData is RouteData)
			{
				result = RouteConversion.ToClient(objectData as RouteData, application);
			}
			else if (objectData is RouteAddressData)
			{
				result = RouteAddressConversion.ToClient(objectData as RouteAddressData, application);
			}
            else if (objectData is UnitAlertData)
            {
                result = UnitAlertConversion.ToClient(objectData as UnitAlertData, application);
            }
            else if (objectData is DepartmentTypeAlertData)
            {
                result = DepartmentTypeAlertConversion.ToClient(objectData as DepartmentTypeAlertData, application);
            }
            else if (objectData is GPSTypeData)
            {
                result = GPSTypeConversion.ToClient(objectData as GPSTypeData, application);
            }
            else if (objectData is GPSPinData)
            {
                result = GPSPinConversion.ToClient(objectData as GPSPinData, application);
            }
            else if (objectData is SensorData)
            {
                result = SensorConversion.ToClient(objectData as SensorData, application);
            }
            else if (objectData is GPSPinSensorData)
            {
				result = GPSPinSensorConversion.ToClient(objectData as GPSPinSensorData, application);
            }
            else if (objectData is WorkShiftRouteData)
            {
                result = WorkShiftRouteConversion.ToClient(objectData as WorkShiftRouteData, application);
            }
            else if (objectData is QuestionDispatchReportData)
            {
                result = QuestionDispatchReportConversion.ToClient(objectData as QuestionDispatchReportData, application);
            }
            else if (objectData is DispatchReportData)
            {
                result = DispatchReportConversion.ToClient(objectData as DispatchReportData, application);
            }
            else if (objectData is DispatchReportQuestionDispatchReportData)
            {
                result = DispatchReportQuestionDispatchReportConversion.ToClient(objectData as DispatchReportQuestionDispatchReportData, application);
            }
			else if (objectData is AlertNotificationData)
			{
				result = AlertNotificationConversion.ToClient(objectData as AlertNotificationData, application);
			}
            else if (objectData is ParkedCallData)
            {
                result = ParkedCallConversion.ToClient(objectData as ParkedCallData, application);
            }
            else if (objectData is AlarmSensorTelemetryData)
            {
                result = AlarmSensorTelemetryConversion.ToClient(objectData as AlarmSensorTelemetryData, application);
            }
            else if (objectData is AlarmLprData)
            {
                result = AlarmLprConversion.ToClient(objectData as AlarmLprData, application);
            }
            else if (objectData is AlarmTwitterData)
            {
                result = AlarmTwitterConversion.ToClient(objectData as AlarmTwitterData, application);
            }
            else if (objectData is AlarmQueryTwitterData)
            {
                result = AlarmQueryTwitterConversion.ToClient(objectData as AlarmQueryTwitterData, application);
            }
            else if (objectData is TweetData)
            {
                result = TweetConversion.ToClient(objectData as TweetData, application);
            }
            else if (objectData is TelephonyConfigurationData)
            {
                result = TelephonyConfigurationConversion.ToClient(objectData as TelephonyConfigurationData, application);
            }
            else if (objectData is RadioConfigurationData)
            {
                result = RadioConfigurationConversion.ToClient(objectData as RadioConfigurationData, application);
            }
            else if (objectData is VehicleRequestData)
            {
                result = VehicleRequestConversion.ToClient(objectData as VehicleRequestData, application);
            }///URDANETA
            else if (objectData is VAAlertCamData)
            {
                result = VAAlertCamConversion.ToClient(objectData as VAAlertCamData, application);
            }
            else if (objectData is VARuleData)
            {
                result = VARuleConversion.ToClient(objectData as VARuleData, application);
            } 
            else if (objectData is VAAlertVideoInstanceData)
            {
                result = VAAlertVideoInstanceConversion.ToClient(objectData as VAAlertVideoInstanceData, application);
            }
            else if (objectData is VARuleData)
            {
                result = VARuleConversion.ToClient(objectData as VARuleData, application);
            }

            return result;
        }

        public static ObjectData ToObject(ClientData.ClientData clientData, UserApplicationData application)
        {
            ObjectData result = null;

            if (clientData is InstalledApplicationClientData) 
            {
                result = InstalledApplicationConversion.ToObject(clientData as InstalledApplicationClientData);
            }
            if (clientData is OperatorStatusClientData)
            {
                result = OperatorStatusConversion.ToObject(clientData as OperatorStatusClientData, application);
            }//
            else if (clientData is UnitEndingReportTypeClientData)
            {
                result = UnitEndingReportTypeConversion.ToObject(clientData as UnitEndingReportTypeClientData);
            }
            else if (clientData is UserProfileClientData)
            {
                result = UserProfileConversion.ToObject(clientData as UserProfileClientData, application);
            }
            else if (clientData is UserRoleClientData)
            {
                result = UserRoleConversion.ToObject(clientData as UserRoleClientData, application);
            }
            else if (clientData is IncidentClientData)
            {
                result = IncidentConversion.ToObject(clientData as IncidentClientData);
            }
            else if (clientData is ReportBaseDepartmentTypeClientData)
            {
                result = ReportBaseDepartmentTypeConversion.ToObject(clientData as ReportBaseDepartmentTypeClientData);
            }
            else if (clientData is OperatorClientData)
            {
                result = OperatorConversion.ToObject(clientData as OperatorClientData, application);
            }
            else if (clientData is UnitNoteClientData)
            {
                result = UnitNoteConversion.ToObject(clientData as UnitNoteClientData);
            }
            else if (clientData is PhoneReportClientData)
            {
                result = PhoneReportConversion.ToObject(clientData as PhoneReportClientData);
            }
            else if (clientData is PhoneReportAudioClientData)
            {
                result = PhoneReportAudioConversion.ToObject(clientData as PhoneReportAudioClientData);
            }
            else if (clientData is IncidentNoteClientData)
            {
                result = IncidentNoteConversion.ToObject(clientData as IncidentNoteClientData);
            }
            else if (clientData is OperatorObservationClientData)
            {
                result = OperatorObservationConversion.ToObject(clientData as OperatorObservationClientData, application);
            }
            else if (clientData is OperatorCategoryHistoryClientData)
            {
                result = OperatorCategoryHistoryConversion.ToObject(clientData as OperatorCategoryHistoryClientData, application);
            }
            else if (clientData is OperatorEvaluationClientData)
            {
                result = OperatorEvaluationConversion.ToObject(clientData as OperatorEvaluationClientData, application);
            }
            else if (clientData is IncidentTypeClientData)
            {
                result = IncidentTypeConversion.ToObject(clientData as IncidentTypeClientData, application);
            }
            else if (clientData is QuestionClientData)
            {
                result = QuestionConversion.ToObject(clientData as QuestionClientData, application);
            }
            else if (clientData is UnitTypeClientData)
            {
                result = UnitTypeConversion.ToObject(clientData as UnitTypeClientData, application);
            }
            else if (clientData is UnitClientData)
            {
                result = UnitConversion.ToObject(clientData as UnitClientData, application);
            }
            else if (clientData is PublicLineClientData)
            {
                result = PublicLineConversion.ToObject(clientData as PublicLineClientData);
            }
            else if (clientData is OfficerClientData)
            {
                result = OfficerConversion.ToObject(clientData as OfficerClientData, application);
            }
            else if (clientData is SupportRequestReportClientData)
            {
                result = SupportRequestReportConversion.ToObject(clientData as SupportRequestReportClientData);
            }
            else if (clientData is DepartmentStationAssignHistoryClientData)
            {
                result = DepartmentStationAssignHistoryConversion.ToObject(clientData as DepartmentStationAssignHistoryClientData, application);
            }
            else if (clientData is IncidentNotificationClientData)
            {
                result = IncidentNotificationConversion.ToObject(clientData as IncidentNotificationClientData);
            }            
            else if (clientData is EvaluationClientData)
            {
                result = EvaluationConversion.ToObject(clientData as EvaluationClientData, application);
            }
            else if (clientData is CctvZoneClientData)
            {
                result = CctvZoneConversion.ToObject(clientData as CctvZoneClientData, application);
            }
            else if (clientData is StructClientData)
            {
                result = StructConversion.ToObject(clientData as StructClientData, application);
            }
            else if (clientData is DeviceClientData)
            {              
                result = DeviceConversion.ToObject(clientData as DeviceClientData, application);
            }
            else if (clientData is SensorTelemetryClientData)
            {
                result = SensorTelemetryConversion.ToObject(clientData as SensorTelemetryClientData, application);
            }
            else if (clientData is RoomClientData)
            {
                result = RoomConversion.ToObject(clientData as RoomClientData, application);
            }
            else if (clientData is RoomSeatClientData) 
            {
                result = RoomSeatConversion.ToObject(clientData as RoomSeatClientData, application); 
            }
            else if (clientData is OperatorAssignClientData)
            {
                result = OperatorAssignConversion.ToObject(clientData as OperatorAssignClientData, application);
            }
            else if (clientData is TrainingCourseClientData)
            {
                result = TrainingCourseConversion.ToObject(clientData as TrainingCourseClientData, application);
            }
            else if (clientData is TrainingCourseScheduleClientData)
            {
                result = TrainingCourseScheduleConversion.ToObject(clientData as TrainingCourseScheduleClientData, application);
            }
            else if (clientData is TrainingCourseSchedulePartsClientData)
            {
                result = TrainingCourseSchedulePartsConversion.ToObject(clientData as TrainingCourseSchedulePartsClientData, application);
            }
            else if (clientData is OperatorTrainingCourseClientData)
            {
                result = OperatorTrainingCourseConversion.ToObject(clientData as OperatorTrainingCourseClientData, application);
            }
            else if (clientData is OperatorCategoryClientData)
            {
                result = OperatorCategoryConversion.ToObject(clientData as OperatorCategoryClientData, application);
            }
            else if (clientData is IndicatorClientData)
            {
                result = IndicatorConversion.ToObject(clientData as IndicatorClientData, application);
            }
            else if (clientData is IndicatorClassDashboardClientData)
            {
                result = IndicatorClassDashboardConversion.ToObject(clientData as IndicatorClassDashboardClientData, application);
            }
            else if (clientData is SupervisorCloseReportClientData)
            {
                result = SupervisorCloseReportConversion.ToObject(clientData as SupervisorCloseReportClientData, application);
            }
            else if (clientData is SupervisorCloseReportMessageClientData)
            {
                result = SupervisorCloseReportMessageConversion.ToObject(clientData as SupervisorCloseReportMessageClientData, application);
            }
            else if (clientData is IndicatorForecastClientData)
            {
                result = IndicatorForecastConversion.ToObject(clientData as IndicatorForecastClientData, application);
            }
            else if (clientData is WorkShiftVariationClientData)
            {
                result = WorkShiftVariationConversion.ToObject(clientData as WorkShiftVariationClientData, application);
            }
            else if (clientData is IndicatorGroupForecastClientData)
            {
                result = IndicatorGroupForecastConversion.ToObject(clientData as IndicatorGroupForecastClientData, application);
            }
            else if (clientData is MotiveVariationClientData)
            {
                result = MotiveVariationConversion.ToObject(clientData as MotiveVariationClientData, application);
            }
            else if (clientData is WorkShiftScheduleVariationClientData)
            {
                result = WorkShiftScheduleVariationConversion.ToObject(clientData as WorkShiftScheduleVariationClientData, application);
            }
            else if (clientData is DepartmentTypeClientData)
            {
                result = DepartmentTypeConversion.ToObject(clientData as DepartmentTypeClientData, application);
            }
            else if (clientData is DepartmentZoneClientData)
            {
                result = DepartmentZoneConversion.ToObject(clientData as DepartmentZoneClientData, application);
            }
            else if (clientData is DepartmentZoneAddressClientData)
            {
                result = DepartmentZoneAddressConversion.ToObject(clientData as DepartmentZoneAddressClientData, application);
            }
            else if (clientData is DepartmentStationAddressClientData)
            {
                result = DepartmentStationAddressConversion.ToObject(clientData as DepartmentStationAddressClientData, application);
            }
            else if (clientData is DepartmentStationClientData)
            {
                result = DepartmentStationConversion.ToObject(clientData as DepartmentStationClientData, application);
            }
            else if (clientData is PositionClientData)
            {
                result = PositionConversion.ToObject(clientData as PositionClientData, application);
            }
            else if (clientData is RankClientData)
            {
                result = RankConversion.ToObject(clientData as RankClientData, application);
            }
            else if (clientData is ApplicationPreferenceClientData)
            {
                result = ApplicationPreferenceConversion.ToObject(clientData as ApplicationPreferenceClientData, application);
            }
            else if (clientData is IncidentStatusClientData)
            {
                result = IncidentStatusConversion.ToObject(clientData as IncidentStatusClientData);
            }
            else if (clientData is UserResourceClientData)
            {
                result = UserResourceConversion.ToObject(clientData as UserResourceClientData, application);
            }
            else if (clientData is UnitStatusClientData)
            {
                result = UnitStatusConversion.ToObject(clientData as UnitStatusClientData);
            }
            else if (clientData is IncidentNotificationStatusClientData)
            {
                result = IncidentNotificationStatusConversion.ToObject(
                    clientData as IncidentNotificationStatusClientData, application);
            }
            else if (clientData is DispatchOrderStatusClientData)
            {
                result = DispatchOrderStatusConversion.ToObject(
                    clientData as DispatchOrderStatusClientData);
            }
            else if (clientData is UserActionClientData)
            {
                result = UserActionConversion.ToObject(
                    clientData as UserActionClientData);
            }
            else if (clientData is IndicatorTypeClientData)
            {
                result = IndicatorTypeConversion.ToObject(
                    clientData as IndicatorTypeClientData);
            }
            else if (clientData is IndicatorClassClientData)
            {
                result = IndicatorClassConversion.ToObject(
                    clientData as IndicatorClassClientData);
            }
            else if (clientData is UserApplicationClientData)
            {
                result = UserApplicationConversion.ToObject(
                    clientData as UserApplicationClientData, application);
            }
            else if (clientData is EvaluationUserApplicationClientData)
            {
                result = EvaluationUserAppllicationConversion.ToObject(
                    clientData as EvaluationUserApplicationClientData, application);
            }
            else if (clientData is DispatchOrderClientData)
            {
                result = DispatchOrderConversion.ToObject(clientData as DispatchOrderClientData);
            }
            else if (clientData is StructTypeClientData)
            {
                result = StructTypeConversion.ToObject(clientData as StructTypeClientData, application);
            }
            else if (clientData is UserAccessClientData)
            {
                result = UserAccessConversion.ToObject(clientData as UserAccessClientData, application);
            }
            else if (clientData is UserPermissionClientData)
            {
                result = UserPermissionConversion.ToObject(clientData as UserPermissionClientData, application);
            }
            else if (clientData is PossibleAnswerAnswerClientData)
            {
                result = PossibleAnswerAnswerConversion.ToObject(clientData as PossibleAnswerAnswerClientData);
            }
            else if (clientData is CameraTypeClientData)
            {
                result = CameraTypeConversion.ToObject(clientData as CameraTypeClientData, application);
            }
            else if (clientData is ConnectionTypeClientData)
            {
                result = ConnectionTypeConversion.ToObject(clientData as ConnectionTypeClientData, application);
            }
            else if (clientData is UnitOfficerAssignHistoryClientData)
            {
                result = UnitOfficerAssignHistoryConversion.ToObject(clientData as UnitOfficerAssignHistoryClientData, application);
            }
            else if (clientData is OperatorDeviceClientData)
            {
                result = OperatorDeviceConversion.ToObject(clientData as OperatorDeviceClientData);
            }
            else if (clientData is RankClientData)
            {
                result = RankConversion.ToObject(clientData as RankClientData, application);
            }
            else if (clientData is PositionClientData)
            {
                result = PositionConversion.ToObject(clientData as PositionClientData, application);
            }
			else if (clientData is IncidentTypeQuestionClientData)
			{
				result = IncidentTypeQuestionConversion.ToObject(clientData as IncidentTypeQuestionClientData, application);
			}
			else if (clientData is IncidentTypeDepartmentTypeClientData)
			{
				result = IncidentTypeDepartmentTypeConversion.ToObject(clientData as IncidentTypeDepartmentTypeClientData, application);
			}
            else if (clientData is UnitAlertClientData)
            {
                result = UnitAlertConversion.ToObject(clientData as UnitAlertClientData, application);
			}
			else if (clientData is RouteClientData)
			{
				result = RouteConversion.ToObject(clientData as RouteClientData, application);
			}
			else if (clientData is RouteAddressClientData)
			{
				result = RouteAddressConversion.ToObject(clientData as RouteAddressClientData, application);
			}
            else if (clientData is DepartmentTypeAlertClientData)
            {
                result = DepartmentTypeAlertConversion.ToObject(clientData as DepartmentTypeAlertClientData, application);
            }
            else if (clientData is GPSTypeClientData)
            {
                result = GPSTypeConversion.ToObject(clientData as GPSTypeClientData, application);
            }
            else if (clientData is GPSPinClientData)
            {
                result = GPSPinConversion.ToObject(clientData as GPSPinClientData, application);
            }
            else if (clientData is SensorClientData)
            {
                result = SensorConversion.ToObject(clientData as SensorClientData, application);
            }
            else if (clientData is GPSPinSensorClientData)
            {
				result = GPSPinSensorConversion.ToObject(clientData as GPSPinSensorClientData, application);
            }
            else if (clientData is WorkShiftRouteClientData)
            {
                result = WorkShiftRouteConversion.ToObject(clientData as WorkShiftRouteClientData, application);
            }
            else if (clientData is QuestionDispatchReportClientData)
            {
                result = QuestionDispatchReportConversion.ToObject(clientData as QuestionDispatchReportClientData, application);
            }
            else if (clientData is DispatchReportClientData)
            {
                result = DispatchReportConversion.ToObject(clientData as DispatchReportClientData, application);
            }
            else if (clientData is DispatchReportQuestionDispatchReportClientData)
            {
                result = DispatchReportQuestionDispatchReportConversion.ToObject(clientData as DispatchReportQuestionDispatchReportClientData, application);
            }
			else if (clientData is AlertNotificationClientData)
			{
				result = AlertNotificationConversion.ToObject(clientData as AlertNotificationClientData, application);
			}
            else if (clientData is ParkedCallClientData)
            {
                result = ParkedCallConversion.ToObject(clientData as ParkedCallClientData, application);
            }
            else if (clientData is AlarmSensorTelemetryClientData)
            {
                result = AlarmSensorTelemetryConversion.ToObject(clientData as AlarmSensorTelemetryClientData, application);
            }
            else if (clientData is AlarmLprClientData)
            {
                result = AlarmLprConversion.ToObject(clientData as AlarmLprClientData, application);
            }
            else if (clientData is AlarmTwitterClientData)
            {
                result = AlarmTwitterConversion.ToObject(clientData as AlarmTwitterClientData, application);
            }
            else if (clientData is AlarmQueryTwitterClientData)
            {
                result = AlarmQueryTwitterConversion.ToObject(clientData as AlarmQueryTwitterClientData, application);
            }
            else if (clientData is TweetClientData)
            {
                result = TweetConversion.ToObject(clientData as TweetClientData, application);
            }
            else if (clientData is TelephonyConfigurationClientData)
            {
                result = TelephonyConfigurationConversion.ToObject(clientData as TelephonyConfigurationClientData);
            }
            else if (clientData is RadioConfigurationClientData)
            {
                result = RadioConfigurationConversion.ToObject(clientData as RadioConfigurationClientData);
            }
            else if (clientData is VehicleRequestClientData)
            {
                result = VehicleRequestConversion.ToObject(clientData as VehicleRequestClientData, application);
            }///URDANETA
            else if (clientData is VAAlertCamClientData)
            {
                result = VAAlertCamConversion.ToObject(clientData as VAAlertCamClientData, application);
            }
/*            else if (clientData is VAAlertVideoClientData)
            {
                result = VAAlertVideoConversion.ToObject(clientData as VAAlertVideoClientData, application);
            }
  */          else if (clientData is VARuleClientData)
            {
                result = VARuleConversion.ToObject(clientData as VARuleClientData, application);
            } 
            else if (clientData is VAAlertVideoInstanceClientData)
            {
                result = VAAlertVideoInstanceConversion.ToObject(clientData as VAAlertVideoInstanceClientData, application);
            }
            else if (clientData is VARuleClientData)
            {
                result = VARuleConversion.ToObject(clientData as VARuleClientData, application);
            }
            return result;
        }

		public static ClientData.ClientData ToClient(NHibernate.ISession session, ObjectData item, UserApplicationData appData)
		{
			if (item is UnitData)
				return UnitConversion.ToClient(session, (UnitData)item, appData);
			if (item is IncidentTypeData)
				return IncidentTypeConversion.ToClient(session, (IncidentTypeData)item, appData);
			else
				return Conversion.ToClient(item, appData);
		}
	}
}
