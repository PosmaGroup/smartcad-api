using System;
using System.Collections;
using System.Text;

using SmartCadCore.Model;
using SmartCadCore.ClientData;

namespace SmartCadCore.Core
{
    public class EndingReportConversion
    {
        public static EndingReportClientData ToClient(EndingReportData endingReportData, UserApplicationData app)
        {
            EndingReportClientData endingReportClient = new EndingReportClientData();
            if (app.Name == UserApplicationData.Dispatch.Name)
            {
                endingReportClient.OfficerCode = endingReportData.Officer.Code;
                endingReportClient.ByUnit = endingReportData.ByUnit;
                endingReportClient.Answers = new ArrayList();

                foreach (EndingReportAnswerData answer in endingReportData.Answers)
                {
                    endingReportClient.Answers.Add(EndingReportAnswerConversion.ToClient(answer, app));
                }
              
            }
            return endingReportClient;
        }

        public static EndingReportData ToObject(EndingReportClientData endingReportClient)
        {
            EndingReportData endingReport = new EndingReportData();
            endingReport.ByUnit = endingReportClient.ByUnit;

            if (endingReportClient.Answers != null)
            {
                endingReport.Answers = new ArrayList();

                foreach (EndingReportAnswerClientData answer in endingReportClient.Answers)
                {
                    EndingReportAnswerData answerData = EndingReportAnswerConversion.ToObject(answer);
                    DispatchOrderData dispatchOrder = new DispatchOrderData();
                    dispatchOrder.Code = endingReportClient.DispatchOrderCode;
                    answerData.DispatchOrder = SmartCadDatabase.RefreshObject(dispatchOrder) as DispatchOrderData;
                    endingReport.Answers.Add(answerData);                   
                }

            }
            //Search Officer
            if (endingReportClient.OfficerCode != 0)
            {
                OfficerData officer = new OfficerData();
                officer.Code = endingReportClient.OfficerCode;
                officer = SmartCadDatabase.RefreshObject(officer) as OfficerData;
                if (officer != null)
                {
                    endingReport.Officer = officer;
                }
            }

            return endingReport;
        }
    }
}
