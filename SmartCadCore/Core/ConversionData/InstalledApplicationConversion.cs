﻿using SmartCadCore.ClientData.Util;
using SmartCadCore.Model.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Core.ConversionData
{
    public class InstalledApplicationConversion
    {

        public static InstalledApplicationClientData ToClient(InstalledApplicationData applicationData) 
        {
            var applicationClientData = new InstalledApplicationClientData() { Name = applicationData.Name,
                                                                               Status = applicationData.Status,
                                                                               Code = applicationData.Code,
                                                                               Version = applicationData.Version };
            return applicationClientData;
        }

        public static InstalledApplicationData ToObject(InstalledApplicationClientData applicationClientData) 
        {
            var applicationData = new InstalledApplicationData() { Name = applicationClientData.Name, 
                                                                   Status = applicationClientData.Status };
            return applicationData;
        }

    }
}
