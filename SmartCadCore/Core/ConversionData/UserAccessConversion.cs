﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    public class UserAccessConversion
    {
        public static UserAccessClientData ToClient(UserAccessData accesData, UserApplicationData app)
        {
            UserAccessClientData client = new UserAccessClientData();
            client.Code = accesData.Code;
            client.Version = accesData.Version;
            client.Name = accesData.Name;
            if(accesData.UserApplication !=null)
                client.UserApplication = UserApplicationConversion.ToClient(accesData.UserApplication,app);
            client.UserPermission = UserPermissionConversion.ToClient(accesData.UserPermission, app);
            return client;
        }

        public static UserAccessData ToObject(UserAccessClientData clientData, UserApplicationData app)
        {
            UserAccessData accesData = new UserAccessData();
            accesData.Code = clientData.Code;
            accesData.Name = clientData.Name;
            accesData = SmartCadDatabase.SearchObject<UserAccessData>(accesData);
            return accesData;
        }
    }
}
