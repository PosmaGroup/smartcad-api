using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using SmartCadCore.ClientData;

namespace SmartCadCore.Core
{
    public class IndicatorClassDashboardConversion
    {
        public static IndicatorClassDashboardClientData ToClient(IndicatorClassDashboardData indicatorData, UserApplicationData app)
        {
            IndicatorClassDashboardClientData icdcd = new IndicatorClassDashboardClientData();
            icdcd.Code = indicatorData.Code;
            icdcd.IndicatorCustomCode = indicatorData.Indicator.CustomCode;
            icdcd.ClassFriendlyName = indicatorData.IndicatorClass.FriendlyName;
            icdcd.ClassName = indicatorData.IndicatorClass.Name;
            icdcd.IndicatorDescription = indicatorData.Indicator.Description;
            icdcd.IndicatorName = indicatorData.Indicator.Name;
            icdcd.IndicatorTypeFriendlyName = indicatorData.Indicator.Type.FriendlyName;
            icdcd.IndicatorTypeName = indicatorData.Indicator.Type.Name;
            icdcd.ShowDashboard = indicatorData.ShowDashboard;
            icdcd.Version = indicatorData.Version;
            return icdcd;
        }

        public static IndicatorClassDashboardData ToObject(IndicatorClassDashboardClientData indicatorClientData, UserApplicationData app)
        {
            IndicatorClassDashboardData icdd = new IndicatorClassDashboardData();
            icdd.Code = indicatorClientData.Code;
            icdd = SmartCadDatabase.SearchObject<IndicatorClassDashboardData>(SmartCadDatabase.GetHQL<IndicatorClassDashboardData>(icdd));
            icdd.ShowDashboard = indicatorClientData.ShowDashboard;
            //icdd.Version = indicatorClientData.Version;
            return icdd;
        }
    }
}
