//using System;
//using System.Collections;
//using System.Text;
//using SmartCadCore.Model;
//using SmartCadCore.ClientData;


//namespace SmartCadCore.Core
//{
//    public class WorkShiftConversion
//    {
//        public static WorkShiftClientData ToClient(WorkShiftData workShift, UserApplicationData application)
//        {
//            WorkShiftClientData workShiftClientData = new WorkShiftClientData();
//            workShiftClientData.Code = workShift.Code;
//            workShiftClientData.Version = workShift.Version;
//            workShiftClientData.Name = workShift.Name;
//            workShiftClientData.Description = workShift.Description;

//            if (SmartCadDatabase.IsInitialize(workShift.Schedules) == true)
//            {
//                //SmartCadDatabase.InitializeLazy(workShift, workShift.Schedules);
//                if (workShift.Schedules != null)
//                {
//                    workShiftClientData.Schedules = new ArrayList();
//                    foreach (WorkShiftScheduleData schedule in workShift.Schedules)
//                    {
//                        workShiftClientData.Schedules.Add(WorkShiftScheduleConversion.ToClient(schedule, application));
//                    }
//                }
//            }

//            if (application.Name == UserApplicationData.Administration.Name)
//            {
//                if (workShift.InitializeCollections == true)
//                {
//                    FillCollections(workShift, ref workShiftClientData,application);
//                }
//            }
//            else
//            {
//                FillCollections(workShift, ref workShiftClientData,application);
//            }
//            return workShiftClientData;
//        }

//        public static WorkShiftData ToObject(WorkShiftClientData workShiftClientData, UserApplicationData application)
//        {
//            WorkShiftData workShift = new WorkShiftData();
//            workShift.Code = workShiftClientData.Code;
//            workShift.Name = workShiftClientData.Name;
//            workShift.Description = workShiftClientData.Description;
//            workShift.Schedules = new ArrayList();

//            if (SmartCadDatabase.IsInitialize(workShiftClientData.Schedules) == true)
//            {
//                foreach (WorkShiftScheduleClientData schedule in workShiftClientData.Schedules)
//                {
//                    WorkShiftScheduleData data = WorkShiftScheduleConversion.ToObject(schedule, application);
//                    data.WorkShift = workShift;
//                    workShift.Schedules.Add(data);
//                }
//            }

//            workShift.Operators = new ArrayList();
//            if (SmartCadDatabase.IsInitialize(workShiftClientData.Operators) == true)
//            {
//                int count = workShiftClientData.Operators.Count;
//                for (int i = 0; i < count; i++)
//                {
//                    OperatorData shiftOperators = OperatorConversion.ToObject((OperatorClientData)workShiftClientData.Operators[i], application);
//                    shiftOperators = (OperatorData)SmartCadDatabase.RefreshObject(shiftOperators);
//                    workShift.Operators.Add(shiftOperators);
//                } 
            
//            }

//            workShift.Version = workShiftClientData.Version;
//            return workShift;
//        }

//        public static void FillCollections(WorkShiftData workShift, ref WorkShiftClientData workShiftClientData, UserApplicationData application)
//        {
//            workShiftClientData.Operators = new ArrayList();
//            if (SmartCadDatabase.IsInitialize(workShift.Operators) == false)
//                SmartCadDatabase.InitializeLazy(workShift, workShift.Operators);
//            int count =0;
//            try
//            {
//                 count = workShift.Operators.Count;
//            }
//            catch 
//            {
//                IList workShifList =  SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorsByWorkShiftCode, workShift.Code));
//                if (workShifList != null)
//                {
//                    workShift.Operators = workShifList;
//                    count = workShifList.Count;
//                }
//            }
//            for (int i = 0; i < count; i++)
//            {
//                OperatorData shiftOperator = workShift.Operators[i] as OperatorData;
//                shiftOperator = (OperatorData)SmartCadDatabase.RefreshObject(shiftOperator);
//                shiftOperator.WorkShift = null;
//                OperatorClientData operClient = OperatorConversion.ToClient(shiftOperator, application);
//                operClient.WorkShift = workShiftClientData;
//                workShiftClientData.Operators.Add(operClient);
//            }
//        }
//    }
//}
