using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

using SmartCadCore.Model;
using SmartCadCore.ClientData;
using NHibernate.Collection;
using Iesi.Collections;

namespace SmartCadCore.Core
{
    public class PhoneReportConversion
    {
        public static PhoneReportClientData ToClient(PhoneReportData phoneReportData, UserApplicationData app)
        {
            PhoneReportClientData convertedClientObject = new PhoneReportClientData();

            if (app.Equals(UserApplicationData.FirstLevel) || app.Equals(UserApplicationData.Report))
            {
                #region FirstLevel
                convertedClientObject.Code = phoneReportData.Code;
                convertedClientObject.Version = phoneReportData.Version;
                convertedClientObject.CustomCode = phoneReportData.CustomCode;

                if (phoneReportData.FinishedReportTime != null)
                    convertedClientObject.FinishedCallTime = phoneReportData.FinishedReportTime.Value;
                if (phoneReportData.HangedUpCallTime != null)
                    convertedClientObject.HangedUpCallTime = phoneReportData.HangedUpCallTime.Value;
                if (phoneReportData.RegisteredCallTime != null)
                    convertedClientObject.RegisteredCallTime = phoneReportData.RegisteredCallTime.Value;
                if (phoneReportData.PickedUpCallTime != null)
                    convertedClientObject.PickedUpCallTime = phoneReportData.PickedUpCallTime.Value;

                if (phoneReportData.Incident != null)
                {
                    convertedClientObject.IncidentCode = phoneReportData.Incident.Code;
                    convertedClientObject.IncidentCustomCode = phoneReportData.Incident.CustomCode;
                }

                convertedClientObject.OperatorLogin = (phoneReportData.Operator != null) ? phoneReportData.Operator.Login : "";

                /*if (SmartCadDatabase.IsInitialize(phoneReportData.SetIncidentTypes) == true)
                {  */
                    convertedClientObject.IncidentTypesCodes = (IList)SmartCadDatabase.SearchBasicObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.IncidentTypeCodesByReportBaseCode, phoneReportData.Code));
                    /*}
                    else
                    {
                    convertedClientObject.IncidentTypesCodes = new ArrayList();
                    foreach (IncidentTypeData incidentType in phoneReportData.SetIncidentTypes)
                    {
                        convertedClientObject.IncidentTypesCodes.Add(incidentType.Code);
                    }
                }    */

                convertedClientObject.PhoneReportCallerClient = PhoneReportCallerConversion.ToClient(phoneReportData.Caller, app);
                convertedClientObject.PhoneReportLineClient = PhoneReportLineConverter.ToClient(phoneReportData.Line, app);

                convertedClientObject.ReportBaseDepartmentTypesClient = new ArrayList();
                #endregion
            }
            else if (app.Equals(UserApplicationData.Supervision))
            {
                #region Supervision
                convertedClientObject.Code = phoneReportData.Code;
                convertedClientObject.Version = phoneReportData.Version;
                convertedClientObject.CustomCode = phoneReportData.CustomCode;

                if (SmartCadDatabase.IsInitialize(phoneReportData.SetIncidentTypes) == true)
                {
                    /*    convertedClientObject.IncidentTypesCodes = (IList)SmartCadDatabase.SearchBasicObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.IncidentTypeCodesByReportBaseCode, phoneReportData.Code));
                    }
                    else
                    {*/
                    convertedClientObject.IncidentTypesCodes = new ArrayList();
                    foreach (IncidentTypeData incidentType in phoneReportData.SetIncidentTypes)
                    {
                        convertedClientObject.IncidentTypesCodes.Add(incidentType.Code);
                    }
                }
                #endregion
            }
            return convertedClientObject;
        }

        public static PhoneReportData ToObject(PhoneReportClientData phoneReportClient)
        {
            PhoneReportData convertedObject = new PhoneReportData();

            convertedObject.SetAnswers = new ListSet();                       
            if (phoneReportClient.Answers != null)
            {
                foreach (ReportAnswerClientData pracd in phoneReportClient.Answers)
                {
                    PhoneReportAnswerData tempPhoneReportAnswer = new PhoneReportAnswerData();
                    tempPhoneReportAnswer = PhoneReportAnswerConversion.ToObject(pracd);
                    tempPhoneReportAnswer.PhoneReport = convertedObject;                  
                    bool resutl = convertedObject.SetAnswers.Add(tempPhoneReportAnswer);
                }              
            }

            convertedObject.Caller = PhoneReportCallerConversion.ToObject(phoneReportClient.PhoneReportCallerClient);
            convertedObject.Code = phoneReportClient.Code;
            convertedObject.Version = phoneReportClient.Version;
            convertedObject.CustomCode = phoneReportClient.CustomCode;

            if (phoneReportClient.FinishedCallTime != DateTime.MinValue)
                convertedObject.FinishedReportTime = phoneReportClient.FinishedCallTime;

            if (phoneReportClient.HangedUpCallTime != DateTime.MinValue)
                convertedObject.HangedUpCallTime = phoneReportClient.HangedUpCallTime;

            if (phoneReportClient.IncidentCode != 0)
            {
                IncidentData incident = new IncidentData();
                incident.Code = phoneReportClient.IncidentCode;
                incident = SmartCadDatabase.RefreshObject(incident) as IncidentData;
                convertedObject.Incident = incident;
            }

            convertedObject.SetIncidentTypes = new HashedSet();

            StringBuilder sb = new StringBuilder();
            sb.Append("(");
            for (int i = 0; i < phoneReportClient.IncidentTypesCodes.Count; i++)
            {
                int itc = (int)phoneReportClient.IncidentTypesCodes[i];
                sb.Append(itc);
                if (i < phoneReportClient.IncidentTypesCodes.Count - 1)
                    sb.Append(",");
            }
            sb.Append(")");

            foreach (IncidentTypeData incidentType in SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.FindIncidentTypesInCodeList, sb.ToString())))
            {
                convertedObject.SetIncidentTypes.Add(incidentType);
            }

            convertedObject.Incomplete = phoneReportClient.Incomplete;
            convertedObject.Line = PhoneReportLineConverter.ToObject(phoneReportClient.PhoneReportLineClient);
            convertedObject.MultipleOrganisms = phoneReportClient.MultipleOrganisms;

            OperatorData operatorData = new OperatorData();
            operatorData.Login = phoneReportClient.OperatorLogin;
            operatorData = SmartCadDatabase.SearchObjects(operatorData)[0] as OperatorData;
            convertedObject.Operator = operatorData;

            convertedObject.PickedUpCallTime = phoneReportClient.PickedUpCallTime;
            convertedObject.ReceivedCallTime = phoneReportClient.ReceivedCallTime;

            if (phoneReportClient.RegisteredCallTime != DateTime.MinValue)
                convertedObject.RegisteredCallTime = phoneReportClient.RegisteredCallTime;

            convertedObject.ReportBaseDepartmentTypes = new ArrayList();
            foreach (ReportBaseDepartmentTypeClientData rbdtcd in phoneReportClient.ReportBaseDepartmentTypesClient)
            {
                ReportBaseDepartmentTypeData tempReportBaseDepartmentType = ReportBaseDepartmentTypeConversion.ToObject(rbdtcd);
                tempReportBaseDepartmentType.ReportBase = convertedObject as ReportBaseData;
                convertedObject.ReportBaseDepartmentTypes.Add(tempReportBaseDepartmentType);
            }

            return convertedObject;
        }
    }

   
}
