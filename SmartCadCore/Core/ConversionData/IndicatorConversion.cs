using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;
using System.Collections;

namespace SmartCadCore.Core
{
    public class IndicatorConversion
    {
        public static IndicatorClientData ToClient(IndicatorData indicatorData, UserApplicationData app)
        {
            IndicatorClientData icd = new IndicatorClientData();
            icd.Classes = new ArrayList();
            if (SmartCadDatabase.IsInitialize(indicatorData.Classes) == true)
            {
                foreach (IndicatorClassDashboardData classData in indicatorData.Classes)
                {
                    icd.Classes.Add(IndicatorClassDashboardConversion.ToClient(classData, UserApplicationData.Supervision));
                }
            }
            icd.CustomCode = indicatorData.CustomCode;
            icd.Code = indicatorData.Code;
            icd.Dashboard = indicatorData.Dashboard;
            icd.Description = indicatorData.Description;
            icd.Frecuency = indicatorData.Frecuency;
            icd.MeasureUnit = indicatorData.MeasureUnit;
            icd.Mnemonic = indicatorData.Mnemonic;
            icd.Name = indicatorData.Name;
            icd.Type = indicatorData.Type.FriendlyName;
            icd.TypeName = indicatorData.Type.Name;
            icd.Version = indicatorData.Version;
            return icd;
        }

        public static IndicatorData ToObject(IndicatorClientData indicatorClientData, UserApplicationData app)
        {
            IndicatorData id = new IndicatorData();
            id.Code = indicatorClientData.Code;
            id = (IndicatorData)SmartCadDatabase.RefreshObject(id);
            id.Version = indicatorClientData.Version;
            return id;
        }
    }
}
