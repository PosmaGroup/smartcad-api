using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using SmartCadCore.ClientData;

namespace SmartCadCore.Core
{
    public class UnitNoteConversion
    {
        public static UnitNoteClientData ToClient(UnitNoteData unitNoteData)
        {
            UnitNoteClientData note = new UnitNoteClientData();
            note.Code = unitNoteData.Code;
            note.Version = unitNoteData.Version;
            note.CreationDate = unitNoteData.CreationDate.Value;
            note.CompleteUserName = unitNoteData.CompleteUserName;
            note.Detail = unitNoteData.Detail;
            note.UserLogin = unitNoteData.UserLogin;
            note.UnitCode = unitNoteData.Unit.Code;
            if (unitNoteData.DispatchOrder != null)
            {
                note.IncidentCode = unitNoteData.DispatchOrder.IncidentNotification.ReportBase.Incident.Code;
            }
            return note;
        }

        public static UnitNoteData ToObject(UnitNoteClientData unitClientNote)
        {
            UnitNoteData note = new UnitNoteData();
            note.Code = unitClientNote.Code;
            note.Version = unitClientNote.Version;
            note.CompleteUserName = unitClientNote.CompleteUserName;
            note.CreationDate = unitClientNote.CreationDate;
            note.Detail = unitClientNote.Detail;
            note.UserLogin = unitClientNote.UserLogin;
            UnitData unit = new UnitData();
            unit.Code = unitClientNote.UnitCode;
            note.Unit = SmartCadDatabase.RefreshObject(unit) as UnitData;
            if (unitClientNote.DispatchOrderCode > 0)
            {
                note.DispatchOrder = new DispatchOrderData();
                note.DispatchOrder.Code = unitClientNote.DispatchOrderCode;
                note.DispatchOrder = SmartCadDatabase.RefreshObject(note.DispatchOrder) as DispatchOrderData;
            }
                
            return note;
        }
    }
}
