using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    //TODO: Borrar
    public class DispatchOrderNoteConversion
    {
        public static DispatchOrderNoteClientData ToClient(DispatchOrderNoteData dispatchOrderNoteData, UserApplicationData app)
        {
            DispatchOrderNoteClientData note = new DispatchOrderNoteClientData();
            note.Code = dispatchOrderNoteData.Code;
            note.Version = dispatchOrderNoteData.Version;
            note.CreationDate = dispatchOrderNoteData.CreationDate.Value;
            note.CompleteUserName = dispatchOrderNoteData.CompleteUserName;
            note.Detail = dispatchOrderNoteData.Detail;
            note.UserLogin = dispatchOrderNoteData.UserLogin;
            note.IncidentCode = dispatchOrderNoteData.DispatchOrder.IncidentNotification.ReportBase.Incident.Code;
            return note;
        }        
    }
}
