using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using SmartCadCore.ClientData;
using Iesi.Collections;

namespace SmartCadCore.Core
{
    public class SensorTelemetryConversion
    {
        public static SensorTelemetryClientData ToClient(SensorTelemetryData sensorTelemetryData, 
            UserApplicationData userApplication)
        {
            SensorTelemetryClientData toConvert = new SensorTelemetryClientData();

            DeviceClientData datalogger = DeviceConversion.ToClient(sensorTelemetryData.Datalogger, UserApplicationData.Alarm);
            toConvert.Datalogger = (datalogger as DataloggerClientData);

            toConvert.Name = sensorTelemetryData.Name;
            toConvert.Variable = sensorTelemetryData.Variable;
            toConvert.ValueDate = sensorTelemetryData.ValueDate;
            toConvert.Value = sensorTelemetryData.Value;
            toConvert.Code = sensorTelemetryData.Code;
            toConvert.Version = sensorTelemetryData.Version;
            toConvert.Threshold = new List<SensorTelemetryThresholdClientData>();
            foreach( SensorTelemetryThresholdData thresholdData in sensorTelemetryData.Thresholds)
            {
                SensorTelemetryThresholdClientData thresholdClientData = new SensorTelemetryThresholdClientData();
                if (thresholdData.High.HasValue) thresholdClientData.High = (double)thresholdData.High;
                if (thresholdData.Low.HasValue) thresholdClientData.Low = (double)thresholdData.Low;
                thresholdClientData.Code = thresholdData.Code;
                thresholdClientData.Version = thresholdData.Version;
                thresholdClientData.SensorTelemetry = toConvert;
                toConvert.Threshold.Add(thresholdClientData);
            }            
            return toConvert;
        }

        public static SensorTelemetryData ToObject(SensorTelemetryClientData sensorTelemetryClient,
            UserApplicationData userApplication)
        {
            SensorTelemetryData toConvert = new SensorTelemetryData();

            if ((sensorTelemetryClient as SensorTelemetryClientData).Datalogger != null)
            {
                (toConvert as SensorTelemetryData).Datalogger = new DataloggerData();
                (toConvert as SensorTelemetryData).Datalogger.Code = (sensorTelemetryClient as  SensorTelemetryClientData).Datalogger.Code;
                (toConvert as SensorTelemetryData).Datalogger.Name = (sensorTelemetryClient as SensorTelemetryClientData).Datalogger.Name;
                (toConvert as SensorTelemetryData).Datalogger = (DataloggerData)SmartCadDatabase.RefreshObject((toConvert as SensorTelemetryData).Datalogger);
            }

            toConvert.Name = sensorTelemetryClient.Name;
            toConvert.Variable = sensorTelemetryClient.Variable;
            toConvert.Code = sensorTelemetryClient.Code;
            toConvert.Version = sensorTelemetryClient.Version;
            toConvert.Thresholds = new ListSet();

            foreach (SensorTelemetryThresholdClientData thresholdClientData in sensorTelemetryClient.Threshold)
            {
                SensorTelemetryThresholdData thresholdData = new SensorTelemetryThresholdData();
                thresholdData.High = thresholdClientData.High;
                thresholdData.Low = thresholdClientData.Low;
                thresholdData.Code = thresholdClientData.Code;
                thresholdData.Version = thresholdClientData.Version;
                thresholdData.Sensor = toConvert;
                SmartCadDatabase.SaveOrUpdate(thresholdData);
                toConvert.Thresholds.Add(thresholdData);
            }           


            return toConvert;
        }
    }

}