using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using SmartCadCore.ClientData;
using System.Collections;

namespace SmartCadCore.Core
{
    public class AlarmSensorTelemetryConversion
    {
        public static AlarmSensorTelemetryClientData ToClient(AlarmSensorTelemetryData alarmReportSensorTelemetryData, 
            UserApplicationData userApplication)
        {
            AlarmSensorTelemetryClientData toConvert = new AlarmSensorTelemetryClientData();

            toConvert.IdSensorTelemetry = alarmReportSensorTelemetryData.SensorTelemetry.Code;
            toConvert.Value = alarmReportSensorTelemetryData.Value;
            toConvert.StartAlarm = alarmReportSensorTelemetryData.StartAlarm;

            if (alarmReportSensorTelemetryData.SensorTelemetry == null ||
                alarmReportSensorTelemetryData.SensorTelemetry.Datalogger == null)
            {

                IList sensor = SmartCadDatabase.SearchObjects(
                                    SmartCadHqls.GetCustomHql(SmartCadHqls.GetSensorTelemetryByCode,
                                        alarmReportSensorTelemetryData.SensorTelemetry.Code));

                SensorTelemetryData sensorTelemetry = (sensor[0] as SensorTelemetryData);
                alarmReportSensorTelemetryData.SensorTelemetry = sensorTelemetry;
            }

            toConvert.SensorTelemetry = SensorTelemetryConversion.
                ToClient(alarmReportSensorTelemetryData.SensorTelemetry, userApplication);

            if (alarmReportSensorTelemetryData.EndAlarm != null)
                toConvert.EndAlarm = alarmReportSensorTelemetryData.EndAlarm;
            if (alarmReportSensorTelemetryData.Operator != null)
                toConvert.Operator = new OperatorClientData() 
                { 
                    Code = alarmReportSensorTelemetryData.Operator.Code,
                    FirstName = alarmReportSensorTelemetryData.Operator.FirstName,
                    LastName = alarmReportSensorTelemetryData.Operator.LastName,
                };
            if (alarmReportSensorTelemetryData.Operator != null)
                toConvert.Observation = alarmReportSensorTelemetryData.Observation;
            toConvert.Code = alarmReportSensorTelemetryData.Code;
            toConvert.Version = alarmReportSensorTelemetryData.Version;
            return toConvert;
        }

        public static AlarmSensorTelemetryData ToObject(AlarmSensorTelemetryClientData alarmReportSensorTelemetryClient,
            UserApplicationData userApplication)
        {
            AlarmSensorTelemetryData toConvert = new AlarmSensorTelemetryData();

            toConvert.SensorTelemetry = new SensorTelemetryData() { Code = alarmReportSensorTelemetryClient.IdSensorTelemetry };
            toConvert.Value = alarmReportSensorTelemetryClient.Value;
            toConvert.StartAlarm = alarmReportSensorTelemetryClient.StartAlarm;
            if (alarmReportSensorTelemetryClient.EndAlarm != null)
                toConvert.EndAlarm = alarmReportSensorTelemetryClient.EndAlarm;
            if (alarmReportSensorTelemetryClient.Operator != null)
                toConvert.Operator = new OperatorData()
                {
                    Code = alarmReportSensorTelemetryClient.Operator.Code,
                    FirstName = alarmReportSensorTelemetryClient.Operator.FirstName,
                    LastName = alarmReportSensorTelemetryClient.Operator.LastName,
                };
            if (alarmReportSensorTelemetryClient.Operator != null)
                toConvert.Observation = alarmReportSensorTelemetryClient.Observation;
            toConvert.Code = alarmReportSensorTelemetryClient.Code;
            toConvert.Version = alarmReportSensorTelemetryClient.Version;

            return toConvert;
        }
    }

}