using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using SmartCadCore.ClientData;
using Iesi.Collections;

namespace SmartCadCore.Core
{
    public class VehicleRequestConversion
    {
        public static VehicleRequestClientData ToClient(VehicleRequestData VehicleRequestData,
            UserApplicationData userApplication)
        {
            VehicleRequestClientData toConvert = new VehicleRequestClientData();

            toConvert.Brand = VehicleRequestData.Brand;
            toConvert.Color = VehicleRequestData.Color;
            toConvert.DateAndTime = VehicleRequestData.DateAndTime;
            toConvert.Details = VehicleRequestData.Details;
            toConvert.Model = VehicleRequestData.Model;
            toConvert.Plate = VehicleRequestData.Plate;
            toConvert.RequestByDepartment = DepartmentTypeConversion.ToClient(VehicleRequestData.RequestByDepartment, userApplication);
            toConvert.RequestByOfficcer = VehicleRequestData.RequestByOfficcer;
            toConvert.RequestFor = VehicleRequestData.RequestFor;
            
            //toConvert.Lpr = new LprClientData(); 
            //toConvert.Lpr.Code = VehicleRequestData.Lpr.Code;
            //toConvert.Lpr.Ip = VehicleRequestData.Lpr.Ip;
            //toConvert.Lpr.IpServer = VehicleRequestData.Lpr.IpServer;
          
            //toConvert.Lpr.Login = VehicleRequestData.Lpr.Login;
            //toConvert.Lpr.Name = VehicleRequestData.Lpr.Name;
            //toConvert.Lpr.Password = VehicleRequestData.Lpr.Password;
            //toConvert.Lpr.Port = VehicleRequestData.Lpr.Port;
            //toConvert.Lpr.Serial = VehicleRequestData.Lpr.Serial;
            //toConvert.Lpr.StructClientData = StructConversion.ToClient(VehicleRequestData.Lpr.Struct, userApplication);
            //toConvert.Lpr.Type = LprTypeConversion.ToClient(VehicleRequestData.Lpr.Type, userApplication);
            //toConvert.Lpr.Version = VehicleRequestData.Lpr.Version;
            //toConvert.Lpr.Way = LprWayConversion.ToClient(VehicleRequestData.Lpr.Way,userApplication);
            


            toConvert.Code = VehicleRequestData.Code;
            toConvert.Version = VehicleRequestData.Version;

            return toConvert;
        }

        public static VehicleRequestData ToObject(VehicleRequestClientData alarmLprRecordClient,
            UserApplicationData userApplication)
        {
            VehicleRequestData toConvert = new VehicleRequestData();

            toConvert.Brand = alarmLprRecordClient.Brand;
            toConvert.Color = alarmLprRecordClient.Color;
            toConvert.DateAndTime = alarmLprRecordClient.DateAndTime;
            toConvert.Details = alarmLprRecordClient.Details;
            toConvert.Model = alarmLprRecordClient.Model;
            toConvert.Plate = alarmLprRecordClient.Plate;
            toConvert.RequestByDepartment = DepartmentTypeConversion.ToObject(alarmLprRecordClient.RequestByDepartment, userApplication);
            toConvert.RequestByOfficcer = alarmLprRecordClient.RequestByOfficcer;
            toConvert.RequestFor = alarmLprRecordClient.RequestFor;
            //toConvert.Lpr = new LprData();
            //toConvert.Lpr.Code = alarmLprRecordClient.Lpr.Code;
            //toConvert.Lpr.Ip = alarmLprRecordClient.Lpr.Ip;
            //toConvert.Lpr.IpServer = alarmLprRecordClient.Lpr.IpServer;
          
            //toConvert.Lpr.Login = alarmLprRecordClient.Lpr.Login;
            //toConvert.Lpr.Name = alarmLprRecordClient.Lpr.Name;
            //toConvert.Lpr.Password = alarmLprRecordClient.Lpr.Password;
            //toConvert.Lpr.Port = alarmLprRecordClient.Lpr.Port;
            //toConvert.Lpr.Serial = alarmLprRecordClient.Lpr.Serial;
            //toConvert.Lpr.Struct = StructConversion.ToObject(alarmLprRecordClient.Lpr.StructClientData, userApplication);
            //toConvert.Lpr.Type = LprTypeConversion.ToObject(alarmLprRecordClient.Lpr.Type, userApplication);
            //toConvert.Lpr.Version = alarmLprRecordClient.Lpr.Version;
            //toConvert.Lpr.Way = LprWayConversion.ToObject(alarmLprRecordClient.Lpr.Way, userApplication);
            toConvert.Code = alarmLprRecordClient.Code;
            toConvert.Version = alarmLprRecordClient.Version;


            return toConvert;
        }
    }

}