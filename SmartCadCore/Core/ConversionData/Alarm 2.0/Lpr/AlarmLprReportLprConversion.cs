using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using SmartCadCore.ClientData;

namespace SmartCadCore.Core
{
    public class AlarmLprReportLprConversion
    {
        public static AlarmLprReportLprClientData ToClient(AlarmLprReportLprData alarmLprReportLprData, UserApplicationData userApplication)
        {
            AlarmLprReportLprClientData toConvert = new AlarmLprReportLprClientData(
                alarmLprReportLprData.Name,
                null);

            toConvert.Address = AddressConversion.ToClient(alarmLprReportLprData.Address, userApplication);

            return toConvert;
        }

        public static AlarmLprReportLprData ToObject(AlarmLprReportLprClientData alarmLprReportLprClient)
        {
            AlarmLprReportLprData toConvert = new AlarmLprReportLprData(
                alarmLprReportLprClient.Name,
                new StructAddressData(AddressConversion.ToObject(alarmLprReportLprClient.Address)));
            return toConvert;
        }
    }

}