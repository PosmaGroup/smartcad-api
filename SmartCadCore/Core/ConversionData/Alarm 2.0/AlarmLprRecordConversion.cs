using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using SmartCadCore.ClientData;
using Iesi.Collections;

namespace SmartCadCore.Core
{
    public class AlarmLprRecordConversion
    {
        public static AlarmLprRecordClientData ToClient(AlarmLprRecordData alarmLprRecordData,
            UserApplicationData userApplication)
        {
            AlarmLprRecordClientData toConvert = new AlarmLprRecordClientData();

            toConvert.Brand = alarmLprRecordData.Brand;
            toConvert.Color = alarmLprRecordData.Color;
            toConvert.DateAndTime = alarmLprRecordData.DateAndTime;
            toConvert.Details = alarmLprRecordData.Details;
            toConvert.Model = alarmLprRecordData.Model;
            toConvert.Plate = alarmLprRecordData.Plate;
            toConvert.RequestByDepartment = DepartmentTypeConversion.ToClient(alarmLprRecordData.RequestByDepartment, userApplication);
            toConvert.RequestByOfficcer = alarmLprRecordData.RequestByOfficcer;
            toConvert.RequestFor = alarmLprRecordData.RequestFor;
            
            toConvert.Lpr = new LprClientData(); 
            toConvert.Lpr.Code = alarmLprRecordData.Lpr.Code;
            toConvert.Lpr.Ip = alarmLprRecordData.Lpr.Ip;
            toConvert.Lpr.IpServer = alarmLprRecordData.Lpr.IpServer;
          
            toConvert.Lpr.Login = alarmLprRecordData.Lpr.Login;
            toConvert.Lpr.Name = alarmLprRecordData.Lpr.Name;
            toConvert.Lpr.Password = alarmLprRecordData.Lpr.Password;
            toConvert.Lpr.Port = alarmLprRecordData.Lpr.Port;
            toConvert.Lpr.Serial = alarmLprRecordData.Lpr.Serial;
            toConvert.Lpr.StructClientData = StructConversion.ToClient(alarmLprRecordData.Lpr.Struct, userApplication);
            toConvert.Lpr.Type = LprTypeConversion.ToClient(alarmLprRecordData.Lpr.Type, userApplication);
            toConvert.Lpr.Version = alarmLprRecordData.Lpr.Version;
            toConvert.Lpr.Way = LprWayConversion.ToClient(alarmLprRecordData.Lpr.Way,userApplication);
            


            toConvert.Code = alarmLprRecordData.Code;
            toConvert.Version = alarmLprRecordData.Version;

            return toConvert;
        }

        public static AlarmLprRecordData ToObject(AlarmLprRecordClientData alarmLprRecordClient,
            UserApplicationData userApplication)
        {
            AlarmLprRecordData toConvert = new AlarmLprRecordData();

            toConvert.Brand = alarmLprRecordClient.Brand;
            toConvert.Color = alarmLprRecordClient.Color;
            toConvert.DateAndTime = alarmLprRecordClient.DateAndTime;
            toConvert.Details = alarmLprRecordClient.Details;
            toConvert.Model = alarmLprRecordClient.Model;
            toConvert.Plate = alarmLprRecordClient.Plate;
            toConvert.RequestByDepartment = DepartmentTypeConversion.ToObject(alarmLprRecordClient.RequestByDepartment, userApplication);
            toConvert.RequestByOfficcer = alarmLprRecordClient.RequestByOfficcer;
            toConvert.RequestFor = alarmLprRecordClient.RequestFor;
            toConvert.Lpr = new LprData();
            toConvert.Lpr.Code = alarmLprRecordClient.Lpr.Code;
            toConvert.Lpr.Ip = alarmLprRecordClient.Lpr.Ip;
            toConvert.Lpr.IpServer = alarmLprRecordClient.Lpr.IpServer;
          
            toConvert.Lpr.Login = alarmLprRecordClient.Lpr.Login;
            toConvert.Lpr.Name = alarmLprRecordClient.Lpr.Name;
            toConvert.Lpr.Password = alarmLprRecordClient.Lpr.Password;
            toConvert.Lpr.Port = alarmLprRecordClient.Lpr.Port;
            toConvert.Lpr.Serial = alarmLprRecordClient.Lpr.Serial;
            toConvert.Lpr.Struct = StructConversion.ToObject(alarmLprRecordClient.Lpr.StructClientData, userApplication);
            toConvert.Lpr.Type = LprTypeConversion.ToObject(alarmLprRecordClient.Lpr.Type, userApplication);
            toConvert.Lpr.Version = alarmLprRecordClient.Lpr.Version;
            toConvert.Lpr.Way = LprWayConversion.ToObject(alarmLprRecordClient.Lpr.Way, userApplication);
            toConvert.Code = alarmLprRecordClient.Code;
            toConvert.Version = alarmLprRecordClient.Version;


            return toConvert;
        }
    }

}