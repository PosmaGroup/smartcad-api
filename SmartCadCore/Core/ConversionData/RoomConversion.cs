using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using SmartCadCore.ClientData;
using System.Collections;

namespace SmartCadCore.Core
{
    public class RoomConversion
    {
        public static RoomClientData ToClient(RoomData roomData, UserApplicationData app)
        {
            RoomClientData roomClientData = new RoomClientData();
            roomClientData.Code = roomData.Code;
            roomClientData.Version = roomData.Version;
            roomClientData.Name = roomData.Name;
            roomClientData.Description = roomData.Description;
            roomClientData.Seats = roomData.Seats;
            roomClientData.Image = roomData.Image;
            roomClientData.SeatsImage = roomData.SeatsImage;
            roomClientData.RenderMethod = roomData.RenderMethod;

            SmartCadDatabase.InitializeLazy(roomData, roomData.ListSeats);//Allways listSeats is used.
            roomClientData.ListSeats = new ArrayList();
            foreach (RoomSeatData seatData in roomData.ListSeats)
            {
                seatData.Room = null;
                RoomSeatClientData seat = (RoomSeatClientData)RoomSeatConversion.ToClient(seatData, app);
                seat.Room = roomClientData;
                roomClientData.ListSeats.Add(seat);
            }

            return roomClientData;
        }

        public static RoomData ToObject(RoomClientData roomClientData, UserApplicationData app)
        {
            RoomData roomData = new RoomData();
            roomData.Code = roomClientData.Code;
            if(roomData.Code!= 0)
                roomData = (RoomData)SmartCadDatabase.RefreshObject(roomData);

            roomData.Version = roomClientData.Version;
            roomData.Name = roomClientData.Name;
            roomData.Description = roomClientData.Description;
            roomData.Seats = roomClientData.Seats;
            roomData.Image = roomClientData.Image;
            roomData.SeatsImage = roomClientData.SeatsImage;
            roomData.RenderMethod = roomClientData.RenderMethod;

            roomData.ListSeats = new ArrayList();
            foreach (RoomSeatClientData seatClient in roomClientData.ListSeats)
            {
                RoomSeatData seat = (RoomSeatData)RoomSeatConversion.ToObject(seatClient, app);
                seat.Room = roomData;
                roomData.ListSeats.Add(seat);
            }

            return roomData;
        }
    }
}