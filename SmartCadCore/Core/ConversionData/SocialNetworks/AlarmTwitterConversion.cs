using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using SmartCadCore.ClientData;
using Iesi.Collections;
using Iesi.Collections.Generic;

namespace SmartCadCore.Core
{
    public class AlarmTwitterConversion
    {
        public static AlarmTwitterClientData ToClient(AlarmTwitterData alarmTwitterData, UserApplicationData userApplication)
        {
            AlarmTwitterClientData toConvert = new AlarmTwitterClientData();

            toConvert.Code = alarmTwitterData.Code;
            toConvert.Version = alarmTwitterData.Version;
            toConvert.Query = AlarmQueryTwitterConversion.ToClient(alarmTwitterData.AlarmQuery, userApplication);
            toConvert.StartAlarm = alarmTwitterData.StartDate;
            toConvert.EndAlarm = alarmTwitterData.EndDate;
            toConvert.Tweets = new List<TweetClientData>();
            foreach (TweetData tweetData in alarmTwitterData.Tweets)
            {
                tweetData.Alarm = null;
                TweetClientData tweetClient = TweetConversion.ToClient(tweetData,userApplication);
                tweetClient.Alarm = toConvert;
                toConvert.Tweets.Add(tweetClient);
            }
            toConvert.Status = (AlarmTwitterClientData.AlarmStatus)(int)alarmTwitterData.Status;
            
            return toConvert;
        }

        public static AlarmTwitterData ToObject(AlarmTwitterClientData alarmTwitterClientData,
            UserApplicationData userApplication)
        {
            AlarmTwitterData toConvert = new AlarmTwitterData();

            toConvert.Code = alarmTwitterClientData.Code;
            toConvert.Version = alarmTwitterClientData.Version;
            toConvert.AlarmQuery = AlarmQueryTwitterConversion.ToObject(alarmTwitterClientData.Query, userApplication);
            toConvert.StartDate = alarmTwitterClientData.StartAlarm;
            toConvert.EndDate = alarmTwitterClientData.EndAlarm;
            toConvert.Tweets = new HashedSet();
            foreach (TweetClientData tweetClient in alarmTwitterClientData.Tweets)
            {
                tweetClient.Alarm = null;
                TweetData tweetData = TweetConversion.ToObject(tweetClient, userApplication);
                tweetData.Alarm = toConvert;
                toConvert.Tweets.Add(tweetData);
            }
            toConvert.Status = (AlarmTwitterData.AlarmStatus)(int)alarmTwitterClientData.Status;

            return toConvert;
        }
    }

}