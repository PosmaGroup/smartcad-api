using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using SmartCadCore.ClientData;
using Iesi.Collections;
using Iesi.Collections.Generic;

namespace SmartCadCore.Core
{
    public class TweetConversion
    {
        public static TweetClientData ToClient(TweetData tweetData, UserApplicationData userApplication)
        {
            TweetClientData toConvert = new TweetClientData();

            toConvert.Code = tweetData.Code;
            toConvert.CustomCode = tweetData.CustomCode;
            toConvert.Version = tweetData.Version;
            if(tweetData.Alarm != null)
                toConvert.Alarm = AlarmTwitterConversion.ToClient(tweetData.Alarm, userApplication);
            toConvert.AuthorName = tweetData.AuthorName;
            toConvert.AuthorUserName = tweetData.AuthorUserName;
            toConvert.AuthorUri = tweetData.AuthorUri;
            toConvert.ImageUrl = tweetData.ImageUrl;
            toConvert.Link = tweetData.Link;
            toConvert.Content = tweetData.Content;
            toConvert.Published = tweetData.Published;

            return toConvert;
        }

        public static TweetData ToObject(TweetClientData tweetClientData, UserApplicationData userApplication)
        {
            TweetData toConvert = new TweetData();

            toConvert.Code = tweetClientData.Code;
            toConvert.CustomCode = tweetClientData.CustomCode;
            toConvert.Version = tweetClientData.Version;
            if (tweetClientData.Alarm != null)
                toConvert.Alarm = AlarmTwitterConversion.ToObject(tweetClientData.Alarm, userApplication);
            toConvert.AuthorName = tweetClientData.AuthorName;
            toConvert.AuthorUserName = tweetClientData.AuthorUserName;
            toConvert.AuthorUri = tweetClientData.AuthorUri;
            toConvert.ImageUrl = tweetClientData.ImageUrl;
            toConvert.Link = tweetClientData.Link;
            toConvert.Content = tweetClientData.Content;
            toConvert.Published = tweetClientData.Published;

            return toConvert;
        }
    }

}