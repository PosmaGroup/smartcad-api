using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using SmartCadCore.ClientData;
using Iesi.Collections;
using Iesi.Collections.Generic;

namespace SmartCadCore.Core
{
    public class AlarmQueryTwitterConversion
    {
        public static AlarmQueryTwitterClientData ToClient(AlarmQueryTwitterData alarmTwitterData, UserApplicationData userApplication)
        {
            AlarmQueryTwitterClientData toConvert = new AlarmQueryTwitterClientData();

            toConvert.Code = alarmTwitterData.Code;
            toConvert.Version = alarmTwitterData.Version;
            toConvert.Name = alarmTwitterData.Name;
            toConvert.Description = alarmTwitterData.Description;
            toConvert.TwitterQuery = alarmTwitterData.TwitterQuery;
            toConvert.TweetsAmount = alarmTwitterData.TweetsAmount;
            toConvert.TimeInterval = alarmTwitterData.TimeInterval;
            toConvert.Type = alarmTwitterData.Type;
            return toConvert;
        }

        public static AlarmQueryTwitterData ToObject(AlarmQueryTwitterClientData alarmTwitterClientData,
            UserApplicationData userApplication)
        {
            AlarmQueryTwitterData toConvert = new AlarmQueryTwitterData();

            toConvert.Code = alarmTwitterClientData.Code;
            toConvert.Version = alarmTwitterClientData.Version;
            toConvert.Name = alarmTwitterClientData.Name;
            toConvert.Description = alarmTwitterClientData.Description;
            toConvert.TwitterQuery = alarmTwitterClientData.TwitterQuery;
            toConvert.TweetsAmount = alarmTwitterClientData.TweetsAmount;
            toConvert.TimeInterval = alarmTwitterClientData.TimeInterval;
            toConvert.Type = alarmTwitterClientData.Type;

            return toConvert;
        }
    }

}