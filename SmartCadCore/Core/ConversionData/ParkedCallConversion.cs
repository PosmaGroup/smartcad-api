﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    public class ParkedCallConversion
    {
        public static ParkedCallClientData ToClient(ParkedCallData data, UserApplicationData app)
        {
            ParkedCallClientData client = new ParkedCallClientData();
            client.Code = data.Code;
            client.ANIReceived = data.ANIReceived;
            client.Version = data.Version;
            client.DateWhenParked = data.DateWhenParked;
            client.OperatorCodeWhoParked = data.OperatorWhoParked.Code;
            if (data.OperatorWhoUnParked != null)
            {
                client.OperatorCodeWhoUnParked = data.OperatorWhoUnParked.Code;
            }
            client.DateWhenUnParked = data.DateWhenUnParked;
            client.PhoneReportCustomCode = data.PhoneReportCustomCode;
            return client;
        }

        public static ParkedCallData ToObject(ParkedCallClientData client, UserApplicationData app)
        {
            ParkedCallData data = new ParkedCallData();
            data.Code = client.Code;
            data.ANIReceived = client.ANIReceived;
            OperatorData operWhoParked = new OperatorData();
            operWhoParked.Code = client.OperatorCodeWhoParked;
            operWhoParked = SmartCadDatabase.SearchObject<OperatorData>(operWhoParked);
            data.OperatorWhoParked = operWhoParked;
            OperatorData operWhoUnParked = new OperatorData();
            operWhoUnParked.Code = client.OperatorCodeWhoUnParked;
            if (operWhoUnParked.Code > 0)
            {
                operWhoUnParked = SmartCadDatabase.SearchObject<OperatorData>(operWhoUnParked);
                data.AgentLoginToUnPark = operWhoUnParked.Login;
                data.OperatorWhoUnParked = operWhoUnParked;
            }
            data.DateWhenParked = client.DateWhenParked;
            data.DateWhenUnParked = client.DateWhenUnParked;
            data.PhoneReportCustomCode = client.PhoneReportCustomCode;
            data.Version = client.Version;
            return data;
        }
    }
}
