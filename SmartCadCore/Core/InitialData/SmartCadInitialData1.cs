using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

using SmartCadCore.Model;
using Iesi.Collections;

namespace SmartCadCore.Core
{
    public partial class SmartCadInitialData
    {
        private static void CreateDevelopmentUserAccounts()
        {
            OperatorData userAccount;

            UserRoleData developerRole = SmartCadDatabase.SearchNamedObject<UserRoleData>("Developer");

            DepartmentTypeData police = SmartCadDatabase.SearchNamedObject<DepartmentTypeData>(DPPOLICE);
            DepartmentTypeData fire = SmartCadDatabase.SearchNamedObject<DepartmentTypeData>(DPFIRE);
            DepartmentTypeData ambumetro = SmartCadDatabase.SearchNamedObject<DepartmentTypeData>(DPAMBUMETRO);
            DepartmentTypeData cicpc = SmartCadDatabase.SearchNamedObject<DepartmentTypeData>(DPCICPC);
            DepartmentTypeData firepre = SmartCadDatabase.SearchNamedObject<DepartmentTypeData>(DPFIREPRE);
            DepartmentTypeData firerescue = SmartCadDatabase.SearchNamedObject<DepartmentTypeData>(DPFIRERESCUE);
            DepartmentTypeData precivil = SmartCadDatabase.SearchNamedObject<DepartmentTypeData>(DPPRECIVIL);
            DepartmentTypeData trans = SmartCadDatabase.SearchNamedObject<DepartmentTypeData>(DPTRANS);
            OperatorCategoryData categorySenior = SmartCadDatabase.SearchNamedObject<OperatorCategoryData>("Senior");
            OperatorCategoryHistoryData operatorCategory = null;

            ArrayList list = new ArrayList();
            list.Add(police);
            list.Add(fire);
            list.Add(ambumetro);
            list.Add(cicpc);
            list.Add(firepre);
            list.Add(firerescue);
            list.Add(precivil);
            list.Add(trans);

            userAccount = new OperatorData();
            userAccount.FirstName = "Alden";
            userAccount.LastName = "Torres";
            userAccount.PersonId = "1101";
            userAccount.AgentID = "1101";
            userAccount.AgentPassword = "1101";
            userAccount.Login = "atorres";
            userAccount.Windows = true;
            userAccount.Role = developerRole;
            userAccount.Birthday = DateTime.Now;
            userAccount.DepartmentTypes = list;
            userAccount.CategoryHistoryList = new HashedSet();

            operatorCategory = new OperatorCategoryHistoryData();
            operatorCategory.Category = categorySenior;
            operatorCategory.StartDate = DateTime.Now;
            operatorCategory.Operator = userAccount;
            userAccount.CategoryHistoryList.Add(operatorCategory);

            SmartCadDatabase.SaveObject<OperatorData>(userAccount);

            userAccount = new OperatorData();
            userAccount.FirstName = "Fernando";
            userAccount.LastName = "Silva";
            userAccount.PersonId = "1102";
            userAccount.AgentID = "1102";
            userAccount.AgentPassword = "1102";
            userAccount.Login = "fsilva";
            userAccount.Windows = true;
            userAccount.Role = developerRole;
            userAccount.Birthday = DateTime.Now;
            userAccount.DepartmentTypes = list;
            userAccount.CategoryHistoryList = new HashedSet();

            operatorCategory = new OperatorCategoryHistoryData();
            operatorCategory.Category = categorySenior;
            operatorCategory.StartDate = DateTime.Now;
            operatorCategory.Operator = userAccount;
            userAccount.CategoryHistoryList.Add(operatorCategory);

            SmartCadDatabase.SaveObject<OperatorData>(userAccount);

            userAccount = new OperatorData();
            userAccount.FirstName = "Annabella";
            userAccount.LastName = "Gajek";
            userAccount.PersonId = "1103";
            userAccount.AgentID = "1103";
            userAccount.AgentPassword = "1103";
            userAccount.Login = "annabella.gajek";
            userAccount.Windows = true;
            userAccount.Role = developerRole;
            userAccount.Birthday = DateTime.Now;
            userAccount.DepartmentTypes = list;
            userAccount.CategoryHistoryList = new HashedSet();

            operatorCategory = new OperatorCategoryHistoryData();
            operatorCategory.Category = categorySenior;
            operatorCategory.StartDate = DateTime.Now;
            operatorCategory.Operator = userAccount;
            userAccount.CategoryHistoryList.Add(operatorCategory);

            SmartCadDatabase.SaveObject<OperatorData>(userAccount);

            userAccount = new OperatorData();
            userAccount.FirstName = "Julio";
            userAccount.LastName = "Ynojosa";
            userAccount.PersonId = "1104";
            userAccount.AgentID = "1104";
            userAccount.AgentPassword = "1104";
            userAccount.Login = "jynojosa";
            userAccount.Windows = true;
            userAccount.Role = developerRole;
            userAccount.Birthday = DateTime.Now;
            userAccount.DepartmentTypes = list;
            userAccount.CategoryHistoryList = new HashedSet();

            operatorCategory = new OperatorCategoryHistoryData();
            operatorCategory.Category = categorySenior;
            operatorCategory.StartDate = DateTime.Now;
            operatorCategory.Operator = userAccount;
            userAccount.CategoryHistoryList.Add(operatorCategory);

            SmartCadDatabase.SaveObject<OperatorData>(userAccount);

            userAccount = new OperatorData();
            userAccount.FirstName = "Eduardo";
            userAccount.LastName = "Campione";
            userAccount.PersonId = "1105";
            userAccount.AgentID = "1105";
            userAccount.Login = "eduardo.campione";
            userAccount.Windows = true;
            userAccount.Role = developerRole;
            userAccount.Birthday = DateTime.Now;
            userAccount.DepartmentTypes = list;
            userAccount.CategoryHistoryList = new HashedSet();

            operatorCategory = new OperatorCategoryHistoryData();
            operatorCategory.Category = categorySenior;
            operatorCategory.StartDate = DateTime.Now;
            operatorCategory.Operator = userAccount;
            userAccount.CategoryHistoryList.Add(operatorCategory);

            SmartCadDatabase.SaveObject<OperatorData>(userAccount);

            userAccount = new OperatorData();
            userAccount.FirstName = "Gustavo";
            userAccount.LastName = "Fandino";
            userAccount.PersonId = "1106";
            userAccount.AgentID = "1106";
            userAccount.AgentPassword = "1106";
            userAccount.Login = "gustavo.fandino";
            userAccount.Windows = true;
            userAccount.Role = developerRole;
            userAccount.Birthday = DateTime.Now;
            userAccount.DepartmentTypes = list;
            userAccount.CategoryHistoryList = new HashedSet();

            operatorCategory = new OperatorCategoryHistoryData();
            operatorCategory.Category = categorySenior;
            operatorCategory.StartDate = DateTime.Now;
            operatorCategory.Operator = userAccount;
            userAccount.CategoryHistoryList.Add(operatorCategory);

            SmartCadDatabase.SaveObject<OperatorData>(userAccount);

            userAccount = new OperatorData();
            userAccount.FirstName = "Arturo";
            userAccount.LastName = "Ag�ero";
            userAccount.PersonId = "1107";
            userAccount.AgentID = "1107";
            userAccount.AgentPassword = "1107";
            userAccount.Login = "arturo.aguero";
            userAccount.Windows = true;
            userAccount.Role = developerRole;
            userAccount.Birthday = DateTime.Now;
            userAccount.DepartmentTypes = list;
            userAccount.CategoryHistoryList = new HashedSet();

            operatorCategory = new OperatorCategoryHistoryData();
            operatorCategory.Category = categorySenior;
            operatorCategory.StartDate = DateTime.Now;
            operatorCategory.Operator = userAccount;
            userAccount.CategoryHistoryList.Add(operatorCategory);

            SmartCadDatabase.SaveObject<OperatorData>(userAccount);

            userAccount = new OperatorData();
            userAccount.FirstName = "Hector";
            userAccount.LastName = "Riccio";
            userAccount.PersonId = "1108";
            userAccount.AgentID = "1108";
            userAccount.AgentPassword = "1108";
            userAccount.Login = "hector.riccio";
            userAccount.Windows = true;
            userAccount.Role = developerRole;
            userAccount.Birthday = DateTime.Now;
            userAccount.DepartmentTypes = list;
            userAccount.CategoryHistoryList = new HashedSet();

            operatorCategory = new OperatorCategoryHistoryData();
            operatorCategory.Category = categorySenior;
            operatorCategory.StartDate = DateTime.Now;
            operatorCategory.Operator = userAccount;
            userAccount.CategoryHistoryList.Add(operatorCategory);

            SmartCadDatabase.SaveObject<OperatorData>(userAccount);

            userAccount = new OperatorData();
            userAccount.FirstName = "Youssef";
            userAccount.LastName = "Tourkmani";
            userAccount.PersonId = "1109";
            userAccount.AgentID = "1109";
            userAccount.AgentPassword = "1109";
            userAccount.Login = "youssef.tourkmani";
            userAccount.Windows = true;
            userAccount.Role = developerRole;
            userAccount.Birthday = DateTime.Now;
            userAccount.DepartmentTypes = list;
            userAccount.CategoryHistoryList = new HashedSet();

            operatorCategory = new OperatorCategoryHistoryData();
            operatorCategory.Category = categorySenior;
            operatorCategory.StartDate = DateTime.Now;
            operatorCategory.Operator = userAccount;
            userAccount.CategoryHistoryList.Add(operatorCategory);

            SmartCadDatabase.SaveObject<OperatorData>(userAccount);
        }
    }
}
