using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using Iesi.Collections;
using SmartCadCore.Common;

namespace SmartCadCore.Core
{
    public partial class SmartCadInitialData
    {       
        private static void CreateJokeIncidentType()
        {
            IncidentTypeData incidentType = new IncidentTypeData();
            incidentType.CustomCode = "JOK";
            incidentType.Name = "Joke";
            incidentType.FriendlyName = ResourceInitialDataLoader.GetString2("Joke");
            incidentType.Exclusive = true;
            incidentType.NoEmergency = true;
            incidentType.SetIncidentTypeQuestions = new HashedSet();

            QuestionData question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("Sex");
            question.ShortText = ResourceInitialDataLoader.GetString2("SexQuestion");
            question.CustomCode = incidentType.CustomCode.ToString() + "Sex";
            question.RequirementType = RequirementTypeEnum.None;           
 
            QuestionPossibleAnswerData answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Male");
            answer1.ControlName = "RdBttn1";
            QuestionPossibleAnswerData answer2 = new QuestionPossibleAnswerData();
            answer2.Question = question;
            answer2.Description = ResourceInitialDataLoader.GetString2("Female");
            answer2.ControlName = "RdBttn2";
            QuestionPossibleAnswerData answer3 = new QuestionPossibleAnswerData();
            answer3.Question = question;
            answer3.Description = ResourceInitialDataLoader.GetString2("NoIdentifiable");
            answer3.ControlName = "RdBttn4";
            question.SetPossibleAnswers = new HashedSet();
            question.SetPossibleAnswers.Add(answer1);
            question.SetPossibleAnswers.Add(answer2);
            question.SetPossibleAnswers.Add(answer3);

            SmartCadDatabase.SaveObject<QuestionData>(question);

            #region renderMethod
            string renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Seleccion simple' itemName='item1' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn1' />
  <Control itemText='Seleccion simple' itemName='item2' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn2' />
  <Control itemText='Seleccion simple' itemName='item4' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn4' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='4'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=156@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Seleccion simple</property>
      <property name='MinSize'>@3,Width=112@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Seleccion simple</property>
      <property name='TextSize'>@2,Width=76@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=312@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Seleccion simple</property>
      <property name='MinSize'>@3,Width=112@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Seleccion simple</property>
      <property name='TextSize'>@2,Width=76@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@3,X=156@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item4' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=157@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Seleccion simple</property>
      <property name='MinSize'>@3,Width=112@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Seleccion simple</property>
      <property name='TextSize'>@2,Width=76@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item4</property>
      <property name='Location'>@3,X=468@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn4</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
            ";
#endregion renderMethod
            question.RenderMethod = renderMethod;
            

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            


            IncidentTypeQuestionData temp1 = new IncidentTypeQuestionData();
            temp1.IncidentType = incidentType;
            temp1.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(temp1);

            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("PersonType");
            question.ShortText = ResourceInitialDataLoader.GetString2("PersonTypeQuestion");
            question.CustomCode = incidentType.CustomCode.ToString() + "Person Type";
            question.RequirementType = RequirementTypeEnum.None;            

            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Child");
            answer1.ControlName = "RdBttn4";

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = question;
            answer2.Description = ResourceInitialDataLoader.GetString2("Teenager");
            answer2.ControlName = "RdBttn5";

            answer3 = new QuestionPossibleAnswerData();
            answer3.Question = question;
            answer3.Description = ResourceInitialDataLoader.GetString2("Adult");
            answer3.ControlName = "RdBttn6";

            QuestionPossibleAnswerData answer4 = new QuestionPossibleAnswerData();
            answer4.Question = question;
            answer4.Description = ResourceInitialDataLoader.GetString2("NoIdentifiable");
            answer4.ControlName = "RdBttn7";

            question.SetPossibleAnswers = new HashedSet();
            question.SetPossibleAnswers.Add(answer1);
            question.SetPossibleAnswers.Add(answer2);
            question.SetPossibleAnswers.Add(answer3);
            question.SetPossibleAnswers.Add(answer4);
            SmartCadDatabase.SaveObject<QuestionData>(question);

            #region renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Seleccion simple' itemName='item1' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn4' />
  <Control itemText='Seleccion simple' itemName='item2' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn5' />
  <Control itemText='Seleccion simple' itemName='item3' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn6' />
  <Control itemText='Seleccion simple' itemName='item4' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn7' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='5'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=156@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Seleccion simple</property>
      <property name='MinSize'>@3,Width=112@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Seleccion simple</property>
      <property name='TextSize'>@2,Width=76@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn4</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=156@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Seleccion simple</property>
      <property name='MinSize'>@3,Width=112@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Seleccion simple</property>
      <property name='TextSize'>@2,Width=76@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@3,X=312@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn5</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item4' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=156@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Seleccion simple</property>
      <property name='MinSize'>@3,Width=112@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Seleccion simple</property>
      <property name='TextSize'>@2,Width=76@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item3</property>
      <property name='Location'>@3,X=156@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn6</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item5' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=157@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Seleccion simple</property>
      <property name='MinSize'>@3,Width=112@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Seleccion simple</property>
      <property name='TextSize'>@2,Width=76@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item4</property>
      <property name='Location'>@3,X=468@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn7</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
            ";
#endregion renderMethod
            question.RenderMethod = renderMethod;
            
            SmartCadDatabase.UpdateObject<QuestionData>(question);

            temp1 = new IncidentTypeQuestionData();
            temp1.IncidentType = incidentType;
            temp1.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(temp1);

            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("ExtraInformation");
            question.ShortText = ResourceInitialDataLoader.GetString2("Extra");
            question.CustomCode = incidentType.CustomCode.ToString() + "Extra";
            question.RequirementType = RequirementTypeEnum.None;            

            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("ExtraInformation");
            answer1.ControlName = "TxtBxx1";

            question.SetPossibleAnswers = new HashedSet();
            question.SetPossibleAnswers.Add(answer1);
            SmartCadDatabase.SaveObject<QuestionData>(question);

            #region renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Texto libre' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.39657, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Texto libre</property>
      <property name='MinSize'>@2,Width=87@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Texto libre</property>
      <property name='TextSize'>@2,Width=51@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
            ";
#endregion renderMethod
            question.RenderMethod = renderMethod;
            

            SmartCadDatabase.UpdateObject<QuestionData>(question);

            temp1 = new IncidentTypeQuestionData();
            temp1.IncidentType = incidentType;
            temp1.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(temp1);
            SmartCadDatabase.SaveObject<IncidentTypeData>(incidentType);
        }

        private static void CreateInformationIncidentType()
        {
            IncidentTypeData incidentType = new IncidentTypeData();
            incidentType.CustomCode = "IFO";
            incidentType.Name = "Info";
            incidentType.FriendlyName = ResourceInitialDataLoader.GetString2("InformationRequest");
            incidentType.Exclusive = true;
            incidentType.NoEmergency = true;
            SmartCadDatabase.SaveObject<IncidentTypeData>(incidentType);
        }

        private static void CreateDispatchOrderNoteType()
        {
            DispatchOrderNoteTypeData type = new DispatchOrderNoteTypeData();
            type.CustomCode = "ENDING_REPORT";
            type.FriendlyName = ResourceInitialDataLoader.GetString2("EndingReport");
            type.Name = "Ending report";
            SmartCadDatabase.SaveObject<DispatchOrderNoteTypeData>(type);

            type = new DispatchOrderNoteTypeData();
            type.CustomCode = "DELAY_NOTE";
            type.FriendlyName = ResourceLoader.GetString2("DelayNote");
            type.Name = "Arrival time was modified";
            SmartCadDatabase.SaveObject<DispatchOrderNoteTypeData>(type);

            type = new DispatchOrderNoteTypeData();
            type.CustomCode = "CLOSE_NOTE";
            type.FriendlyName = ResourceLoader.GetString2("CloseNote");
            type.Name = "Closing note";
            SmartCadDatabase.SaveObject<DispatchOrderNoteTypeData>(type);

            type = new DispatchOrderNoteTypeData();
            type.CustomCode = "CANCEL_NOTE";
            type.FriendlyName = ResourceLoader.GetString2("CancelNote");
            type.Name = "Cancel note";
            SmartCadDatabase.SaveObject<DispatchOrderNoteTypeData>(type);

            type = new DispatchOrderNoteTypeData();
            type.CustomCode = "CURRENT_PROCEDURE";
            type.FriendlyName = ResourceLoader.GetString2("CurrentProcedure");
            type.Name = "Current procedure";
            SmartCadDatabase.SaveObject<DispatchOrderNoteTypeData>(type);

            type = new DispatchOrderNoteTypeData();
            type.CustomCode = "CREATION_NOTE";
            type.FriendlyName = ResourceLoader.GetString2("CreationNote");
            type.Name = "Creation note";
            SmartCadDatabase.SaveObject<DispatchOrderNoteTypeData>(type);
        }

        private static void CreateUnitEndingReportType()
        {
            UnitEndingReportTypeData type = new UnitEndingReportTypeData();
            type.CustomCode = "FALSE_ALARM";
            type.FriendlyName = ResourceInitialDataLoader.GetString2("FalseAlarm");
            type.Name = "False alarm";
            SmartCadDatabase.SaveObject<UnitEndingReportTypeData>(type);

            type = new UnitEndingReportTypeData();
            type.CustomCode = "NOT_LOCATED_INCIDENT";
            type.FriendlyName = ResourceInitialDataLoader.GetString2("NotLocatedIncident"); ;
            type.Name = "Not located incident";
            SmartCadDatabase.SaveObject<UnitEndingReportTypeData>(type);

            type = new UnitEndingReportTypeData();
            type.CustomCode = "NOT_LOCATED_PERSON";
            type.FriendlyName = ResourceInitialDataLoader.GetString2("NotLocatedPerson"); ;
            type.Name = "Not located person";
            SmartCadDatabase.SaveObject<UnitEndingReportTypeData>(type);

            type = new UnitEndingReportTypeData();
            type.CustomCode = "REALIZED_WARNING";
            type.FriendlyName = ResourceInitialDataLoader.GetString2("RealizedWarning"); ;
            type.Name = "Realized warning";
            SmartCadDatabase.SaveObject<UnitEndingReportTypeData>(type);

            type = new UnitEndingReportTypeData();
            type.CustomCode = "DELINCUENCIA_JUVENIL";
            type.FriendlyName = ResourceInitialDataLoader.GetString2("JuvenileDelinquency"); ;
            type.Name = "Delincuencia juvenil";
            SmartCadDatabase.SaveObject<UnitEndingReportTypeData>(type);

            type = new UnitEndingReportTypeData();
            type.CustomCode = "MULTA_REALIZADA";
            type.FriendlyName = ResourceInitialDataLoader.GetString2("MadeTicket"); ;
            type.Name = "Multa realizada";
            SmartCadDatabase.SaveObject<UnitEndingReportTypeData>(type);

            type = new UnitEndingReportTypeData();
            type.CustomCode = "SUSPECT_UNDER_ARREST";
            type.FriendlyName = ResourceInitialDataLoader.GetString2("SuspectUnderArrest"); ;
            type.Name = "Suspect under arrest";
            SmartCadDatabase.SaveObject<UnitEndingReportTypeData>(type);

            type = new UnitEndingReportTypeData();
            type.CustomCode = "SUSPECT_RAN_AWAY";
            type.FriendlyName = ResourceInitialDataLoader.GetString2("SuspectRanAway"); ;
            type.Name = "Suspect ran away";
            SmartCadDatabase.SaveObject<UnitEndingReportTypeData>(type);

            type = new UnitEndingReportTypeData();
            type.CustomCode = "HURT_SUSPECT";
            type.FriendlyName = ResourceInitialDataLoader.GetString2("HurtSuspect"); ;
            type.Name = "Suspect is hurt";
            SmartCadDatabase.SaveObject<UnitEndingReportTypeData>(type);

            type = new UnitEndingReportTypeData();
            type.CustomCode = "NEUTRALIZED_SUSPECT";
            type.FriendlyName = ResourceInitialDataLoader.GetString2("NeutralizedSuspect"); ;
            type.Name = "Neutralized suspect";
            SmartCadDatabase.SaveObject<UnitEndingReportTypeData>(type);

            type = new UnitEndingReportTypeData();
            type.CustomCode = "TAKEN_TO_HOSPITAL";
            type.FriendlyName = ResourceInitialDataLoader.GetString2("TakenToHospital"); ;
            type.Name = "Taken to the hospital";
            SmartCadDatabase.SaveObject<UnitEndingReportTypeData>(type);

            type = new UnitEndingReportTypeData();
            type.CustomCode = "REVIEWED_VEHICLE";
            type.FriendlyName = ResourceInitialDataLoader.GetString2("ReviewedVehicle"); ;
            type.Name = "Reviewed vehicle";
            SmartCadDatabase.SaveObject<UnitEndingReportTypeData>(type);

            type = new UnitEndingReportTypeData();
            type.CustomCode = "VEHICLE_REMOLCADO";
            type.FriendlyName = ResourceInitialDataLoader.GetString2("VehicleTowed"); ;
            type.Name = "Veh�culo remolcado";
            SmartCadDatabase.SaveObject<UnitEndingReportTypeData>(type);
          
        }
    }
}
