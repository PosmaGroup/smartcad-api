using System.Collections;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using Iesi.Collections;

namespace SmartCadCore.Core
{
    public partial class SmartCadInitialData
    {

        public static void CreateGenericQuestionKidnapping()
        {
            #region hora
            QuestionData question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("KidnappingTime");
            question.ShortText = ResourceInitialDataLoader.GetString2("KidnappingTimeShort");
            question.CustomCode = "HORA";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            QuestionPossibleAnswerData answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Time");
            answer1.ControlName = "TxtBxx1";

            ////IncidentTypeProcedure2Data * = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            ////proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);

            
            //////SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();            
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region Render Time
            string renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Hora' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.19885, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Hora</property>
      <property name='MinSize'>@2,Width=93@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Hora</property>
      <property name='TextSize'>@2,Width=57@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
            #endregion
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region testigos
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("AreThereAnyWitnesses");
            question.ShortText = ResourceInitialDataLoader.GetString2("AreThereAnyWitnessesShort");
            question.CustomCode = "TESTIGO";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("AnswerYes");
            answer1.ControlName = "RdBttn1";

            ////proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            ////item*.Description = *
            ////item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);

            QuestionPossibleAnswerData answer2 = new QuestionPossibleAnswerData();
            answer2.Question = question;
            answer2.Description = ResourceInitialDataLoader.GetString2("AnswerNo");
            answer2.ControlName = "RdBttn2";
            ////IncidentTypeProcedure2Data * = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            ////item*.Description = *
            ////item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer2);

            //////SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            //////SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region Render Testigos
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Si' itemName='item1' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn1' />
  <Control itemText='No' itemName='item2' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn2' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='3'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=312@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Si</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Si</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=313@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>No</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>No</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@3,X=312@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
            #endregion
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region secuestradores
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("HijackersDescription");
            question.ShortText = ResourceInitialDataLoader.GetString2("HijackersDescriptionShort");
            question.CustomCode = "HIJACKERS";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Height");
            answer1.ControlName = "TxtBxx1";

            ////proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            ////proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = question;
            answer2.Description = ResourceInitialDataLoader.GetString2("Clothes");
            answer2.ControlName = "TxtBxx3";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer2);

            QuestionPossibleAnswerData answer3 = new QuestionPossibleAnswerData();
            answer3.Question = question;
            answer3.Description = ResourceInitialDataLoader.GetString2("Hair");
            answer3.ControlName = "TxtBxx5";

            ////IncidentTypeProcedure2Data * = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer3);

            QuestionPossibleAnswerData answer4 = new QuestionPossibleAnswerData();
            answer4.Question = question;
            answer4.Description = ResourceInitialDataLoader.GetString2("Eyes");
            answer4.ControlName = "TxtBxx2";

            ////IncidentTypeProcedure2Data * = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer4);

            QuestionPossibleAnswerData answer5 = new QuestionPossibleAnswerData();
            answer5.Question = question;
            answer5.Description = ResourceInitialDataLoader.GetString2("SignificantFeatures");
            answer5.ControlName = "TxtBxx4";

            //////IncidentTypeProcedure2Data * = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer5);


            ////SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            ////SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            ////SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            ////SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            ////SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region Render secuestradores
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Altura' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.36869, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <Control itemText='Ojos' itemName='item2' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.36869, Culture=neutral, PublicKeyToken=null' name='TxtBxx2' />
  <Control itemText='Ropa' itemName='item3' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.36869, Culture=neutral, PublicKeyToken=null' name='TxtBxx3' />
  <Control itemText='Otros' itemName='item4' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.36869, Culture=neutral, PublicKeyToken=null' name='TxtBxx4' />
  <Control itemText='Cabello' itemName='item5' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.36869, Culture=neutral, PublicKeyToken=null' name='TxtBxx5' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='6'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=312@2,Height=31</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Altura</property>
      <property name='MinSize'>@2,Width=71@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Altura</property>
      <property name='TextSize'>@2,Width=35@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=313@2,Height=31</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Ojos</property>
      <property name='MinSize'>@2,Width=71@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Ojos</property>
      <property name='TextSize'>@2,Width=35@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@3,X=312@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item4' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=312@2,Height=31</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Ropa</property>
      <property name='MinSize'>@2,Width=71@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Ropa</property>
      <property name='TextSize'>@2,Width=35@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item3</property>
      <property name='Location'>@1,X=0@2,Y=31</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx3</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item5' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=313@2,Height=68</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Otros</property>
      <property name='MinSize'>@2,Width=71@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Otros</property>
      <property name='TextSize'>@2,Width=35@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item4</property>
      <property name='Location'>@3,X=312@2,Y=31</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx4</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item6' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=312@2,Height=37</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Cabello</property>
      <property name='MinSize'>@2,Width=71@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Cabello</property>
      <property name='TextSize'>@2,Width=35@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item5</property>
      <property name='Location'>@1,X=0@2,Y=62</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx5</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
            #endregion
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region vehiculos
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("UsedVehicle");
            question.ShortText = ResourceInitialDataLoader.GetString2("VehicleShort");
            question.CustomCode = "CAR";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("VehicleDescription");
            answer1.ControlName = "TxtBxx1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);


            ////SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region Render Vehiculo
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Descripcion de vehiculo' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.19885, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Descripcion de vehiculo</property>
      <property name='MinSize'>@2,Width=93@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Descripcion de vehiculo</property>
      <property name='TextSize'>@2,Width=57@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
            #endregion
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region razon
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("KidnappingReason");
            question.ShortText = ResourceInitialDataLoader.GetString2("KidnappingReasonShort");
            question.CustomCode = "REASON";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Political");
            answer1.ControlName = "RdBttn1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = question;
            answer2.Description = ResourceInitialDataLoader.GetString2("Extortion");
            answer2.ControlName = "RdBttn2";
            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer2);

            ////SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            ////SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region Render Reasons
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Politica' itemName='item1' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn1' />
  <Control itemText='Extrosion' itemName='item2' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn2' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='3'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=312@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Politica</property>
      <property name='MinSize'>@2,Width=81@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Politica</property>
      <property name='TextSize'>@2,Width=45@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=313@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Extrosion</property>
      <property name='MinSize'>@2,Width=81@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Extrosion</property>
      <property name='TextSize'>@2,Width=45@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@3,X=312@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
            #endregion
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region escape
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("KidnappingEscapeRoute");
            question.ShortText = ResourceInitialDataLoader.GetString2("KidnappingEscapeRouteShort");
            question.CustomCode = "ESCAPE";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Route");
            answer1.ControlName = "TxtBxx1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);


            ////SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region Render Ruta
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Ruta' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.19885, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Ruta</property>
      <property name='MinSize'>@2,Width=93@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Ruta</property>
      <property name='TextSize'>@2,Width=57@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
            #endregion
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region comunicado
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("HaveBeenCommunicated");
            question.ShortText = ResourceInitialDataLoader.GetString2("HaveBeenCommunicatedShort");
            question.CustomCode = "COMMUNICATED";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("AnswerYes");
            answer1.ControlName = "RdBttn1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = question;
            answer2.Description = ResourceInitialDataLoader.GetString2("AnswerNo");
            answer2.ControlName = "RdBttn2";
            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer2);

            ////SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            ////SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region Render Comunicado
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Si' itemName='item1' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn1' />
  <Control itemText='No' itemName='item2' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn2' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='3'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=312@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Si</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Si</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=313@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>No</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>No</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@3,X=312@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
            #endregion
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region demanda
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("DemandType");
            question.ShortText = ResourceInitialDataLoader.GetString2("DemandTypeShort");
            question.CustomCode = "DEMAND";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Demand");
            answer1.ControlName = "TxtBxx1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);


            ////SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region Render Demanda
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Demanda' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.19885, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Demanda</property>
      <property name='MinSize'>@2,Width=93@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Demanda</property>
      <property name='TextSize'>@2,Width=57@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
            #endregion
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region sonidos
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("CharacteristicSounds");
            question.ShortText = ResourceInitialDataLoader.GetString2("CharacteristicSoundsShort");
            question.CustomCode = "SOUNDS";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Sounds");
            answer1.ControlName = "TxtBxx1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);


            ////SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region Render Sonidos
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Sonidos' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.19885, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Sonidos</property>
      <property name='MinSize'>@2,Width=93@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Sonidos</property>
      <property name='TextSize'>@2,Width=57@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
            #endregion
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region intuidas
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("VoiceHijackersFeatures");
            question.ShortText = ResourceInitialDataLoader.GetString2("VoiceHijackersFeaturesShort");
            question.CustomCode = "INTUIDAS";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Features");
            answer1.ControlName = "TxtBxx1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);


            ////SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Caracteristicas' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.19885, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Caracteristicas</property>
      <property name='MinSize'>@2,Width=93@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Caracteristicas</property>
      <property name='TextSize'>@2,Width=57@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>";
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region posibles
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("KidnappingPossibleSuspected");
            question.ShortText = ResourceInitialDataLoader.GetString2("KidnappingPossibleSuspectedShort");
            question.CustomCode = "POSSIBLE";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Suspects");
            answer1.ControlName = "TxtBxx1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);


            ////SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region Render Sospechosos
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Sospechosos' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.19885, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Sospechosos</property>
      <property name='MinSize'>@2,Width=93@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Sospechosos</property>
      <property name='TextSize'>@2,Width=57@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
            #endregion
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region bien
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("KidnappedGood");
            question.ShortText = ResourceInitialDataLoader.GetString2("KidnappedGoodShort");
            question.CustomCode = "GOOD";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Type");
            answer1.ControlName = "TxtBxx1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);


            ////SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region Render Tipos
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Tipos' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.19885, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Tipos</property>
      <property name='MinSize'>@2,Width=93@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Tipos</property>
      <property name='TextSize'>@2,Width=57@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
            #endregion
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region numero
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("HowManyHijackers");
            question.ShortText = ResourceInitialDataLoader.GetString2("HowManyHijackersShort");
            question.CustomCode = "HOWMANYHIJACKERS";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Amount");
            answer1.ControlName = "TxtBxx1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);


            ////SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region Render Amount
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Cantidad' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.19885, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Cantidad</property>
      <property name='MinSize'>@2,Width=93@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Cantidad</property>
      <property name='TextSize'>@2,Width=57@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
            #endregion
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion
        }

        public static void CreateKidnappingIncident()
        {
            //Este metodo crea la carga inicial para todos los tipos de incidentes de robo
            //Para agregar un tipo de incidente debo realizar los siguientes pasos
            //1.- Crear el incidente.
            //2.- Asociarle los tipos de departamentos que dan soporte a ese incidente
            //3.- Indicar los tipos de procedimientos asociados al incidente
            //4.- Indicar los tipos de preguntas asociadas al incidente con la siguiente estructura
            //4.1.-     Indicar un procedimiento para cada pregunta
            //4.2.-     Indicar las posible respuesta asociadas a la pregunta
            //4.3.-     Indicar la forma en que se renderiza la pregunta en el frontclient indicando los id de las respuestas asociadas
            CreateGenericQuestionKidnapping();

            #region secuestroextorsivo
            //Paso 1
            IncidentTypeData incidentType = new IncidentTypeData();
            incidentType.CustomCode = "KNP";
            incidentType.Name = "KidnappingPolitician";
            incidentType.FriendlyName = ResourceInitialDataLoader.GetString2("ExtortionOrPoliticianKidnapping");
            incidentType.NoEmergency = false;

            //Paso 2
            //Policia
            DepartmentTypeData dpPolice = new DepartmentTypeData(DPPOLICE);
            dpPolice = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpPolice);

            //Bomberos Emergencias
            DepartmentTypeData dpFirePre = new DepartmentTypeData(DPCICPC);
            dpFirePre = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFirePre);

            //Bomberos Emergencias
            DepartmentTypeData dpFire = new DepartmentTypeData(DPFIREPRE);
            dpFire = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFire);



            incidentType.SetIncidentTypeDepartmentTypes = new HashedSet();

            IncidentTypeDepartmentTypeData itdFire = new IncidentTypeDepartmentTypeData();
            itdFire.DepartmentType = dpFire;
            itdFire.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFire);

            IncidentTypeDepartmentTypeData itdFirePre = new IncidentTypeDepartmentTypeData();
            itdFirePre.DepartmentType = dpFirePre;
            itdFirePre.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFirePre);

            IncidentTypeDepartmentTypeData itdPolice = new IncidentTypeDepartmentTypeData();
            itdPolice.DepartmentType = dpPolice;
            itdPolice.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdPolice);

            incidentType.Procedure = @"<b>" + ResourceInitialDataLoader.GetString2("ProceduresTo") + incidentType.FriendlyName + @".</b> <ul> <li>" + ResourceInitialDataLoader.GetString2("Step1") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step2") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step3") + @"</li> </ul> ";



            //Paso 4

            //Indico las preguntas genericas para el incidente
            incidentType.SetIncidentTypeQuestions = new HashedSet();

            QuestionData questionWhere = new QuestionData();
            questionWhere.ShortText = ResourceInitialDataLoader.GetString2(STRQUESTIONWHERE);
            questionWhere = SmartCadDatabase.SearchObject<QuestionData>(questionWhere);


            IncidentTypeQuestionData itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionWhere;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            QuestionData questionNumber = new QuestionData();
            questionNumber.ShortText = ResourceInitialDataLoader.GetString2(STRQUESTIONNUMBERPERSON);
            questionNumber = SmartCadDatabase.SearchObject<QuestionData>(questionNumber);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionNumber;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            QuestionData question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("KidnappingVictim");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("KidnappingTimeShort");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("KidnappingWitnesses");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("Hijackers");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("KidnappingWeapons");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("KidnappingVehicle");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("KidnappingReasonShort");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("KidnappingEscapeRouteShort");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("HaveBeenCommunicatedShort");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("DemandTypeShort");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("CharacteristicSoundsShort");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("VoiceHijackersFeaturesShort");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("KidnappingPossibleSuspectedShort");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //question = new QuestionData();
            //question.ShortText = ResourceInitialDataLoader.GetString2("KidnappingInjured");
            //question = SmartCadDatabase.SearchObject<QuestionData>(question);


            //itqWhere = new IncidentTypeQuestionData();
            //itqWhere.IncidentType = incidentType;
            //itqWhere.Question = question;
            //incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("HowManyHijackersShort");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            SmartCadDatabase.SaveObject<IncidentTypeData>(incidentType);
            #endregion

            #region secuestroexpress
            //Paso 1
            incidentType = new IncidentTypeData();
            incidentType.CustomCode = "KNE";
            incidentType.Name = "KidnappingExpress";
            incidentType.FriendlyName = ResourceInitialDataLoader.GetString2("KidnappingExpress");
            incidentType.NoEmergency = false;

            //Paso 2
            //Policia
            dpPolice = new DepartmentTypeData(DPPOLICE);
            dpPolice = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpPolice);

            //Bomberos Emergencias
            dpFirePre = new DepartmentTypeData(DPCICPC);
            dpFirePre = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFirePre);

            //Bomberos Emergencias
            dpFire = new DepartmentTypeData(DPFIREPRE);
            dpFire = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFire);



            incidentType.SetIncidentTypeDepartmentTypes = new HashedSet();

            itdFire = new IncidentTypeDepartmentTypeData();
            itdFire.DepartmentType = dpFire;
            itdFire.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFire);

            itdFirePre = new IncidentTypeDepartmentTypeData();
            itdFirePre.DepartmentType = dpFirePre;
            itdFirePre.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFirePre);

            itdPolice = new IncidentTypeDepartmentTypeData();
            itdPolice.DepartmentType = dpPolice;
            itdPolice.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdPolice);

            incidentType.Procedure = @"<b>" + ResourceInitialDataLoader.GetString2("ProceduresTo") + incidentType.FriendlyName + @".</b> <ul> <li>" + ResourceInitialDataLoader.GetString2("Step1") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step2") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step3") + @"</li> </ul> ";



            //Paso 4

            //Indico las preguntas genericas para el incidente
            incidentType.SetIncidentTypeQuestions = new HashedSet();

            questionWhere = new QuestionData();
            questionWhere.ShortText = ResourceInitialDataLoader.GetString2(STRQUESTIONWHERE);
            questionWhere = SmartCadDatabase.SearchObject<QuestionData>(questionWhere);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionWhere;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            questionNumber = new QuestionData();
            questionNumber.ShortText = ResourceInitialDataLoader.GetString2(STRQUESTIONNUMBERPERSON);
            questionNumber = SmartCadDatabase.SearchObject<QuestionData>(questionNumber);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionNumber;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("KidnappingVictim");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("KidnappingTimeShort");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("KidnappingWitnesses");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("Hijackers");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("KidnappingWeapons");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("KidnappingVehicle");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);
            

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("KidnappingEscapeRouteShort");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("VoiceHijackersFeaturesShort");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("KidnappingPossibleSuspectedShort");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //question = new QuestionData();
            //question.ShortText = ResourceInitialDataLoader.GetString2("KidnappingInjured");
            //question = SmartCadDatabase.SearchObject<QuestionData>(question);


            //itqWhere = new IncidentTypeQuestionData();
            //itqWhere.IncidentType = incidentType;
            //itqWhere.Question = question;
            //incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("HowManyHijackersShort");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            SmartCadDatabase.SaveObject<IncidentTypeData>(incidentType);
            #endregion

            #region secuestrobienes
            //Paso 1
            incidentType = new IncidentTypeData();
            incidentType.CustomCode = "KIB";
            incidentType.Name = "KidnappingGoods";
            incidentType.FriendlyName = ResourceInitialDataLoader.GetString2("KidnappingGoods");
            incidentType.NoEmergency = false;

            //Paso 2
            //Policia
            dpPolice = new DepartmentTypeData(DPPOLICE);
            dpPolice = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpPolice);

            //Bomberos Emergencias
            dpFirePre = new DepartmentTypeData(DPCICPC);
            dpFirePre = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFirePre);

            //Bomberos Emergencias
            dpFire = new DepartmentTypeData(DPFIREPRE);
            dpFire = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFire);



            incidentType.SetIncidentTypeDepartmentTypes = new HashedSet();

            itdFire = new IncidentTypeDepartmentTypeData();
            itdFire.DepartmentType = dpFire;
            itdFire.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFire);

            itdFirePre = new IncidentTypeDepartmentTypeData();
            itdFirePre.DepartmentType = dpFirePre;
            itdFirePre.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFirePre);

            itdPolice = new IncidentTypeDepartmentTypeData();
            itdPolice.DepartmentType = dpPolice;
            itdPolice.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdPolice);

            incidentType.Procedure = @"<b>" + ResourceInitialDataLoader.GetString2("ProceduresTo") + incidentType.FriendlyName + @".</b> <ul> <li>" + ResourceInitialDataLoader.GetString2("Step1") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step2") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step3") + @"</li> </ul> ";



            //Paso 4

            //Indico las preguntas genericas para el incidente
            incidentType.SetIncidentTypeQuestions = new HashedSet();

            questionWhere = new QuestionData();
            questionWhere.ShortText = ResourceInitialDataLoader.GetString2(STRQUESTIONWHERE);
            questionWhere = SmartCadDatabase.SearchObject<QuestionData>(questionWhere);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionWhere;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            questionNumber = new QuestionData();
            questionNumber.ShortText = ResourceInitialDataLoader.GetString2(STRQUESTIONNUMBERPERSON);
            questionNumber = SmartCadDatabase.SearchObject<QuestionData>(questionNumber);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionNumber;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("KidnappedGoodShort");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("KidnappingTimeShort");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("KidnappingWitnesses");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("Hijackers");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("KidnappingWeapons");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("KidnappingVehicle");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);
            

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("KidnappingEscapeRouteShort");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("HaveBeenCommunicatedShort");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("DemandTypeShort");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("CharacteristicSoundsShort");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("VoiceHijackersFeaturesShort");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("KidnappingPossibleSuspectedShort");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);


            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("HowManyHijackersShort");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            SmartCadDatabase.SaveObject<IncidentTypeData>(incidentType);
            #endregion
        }

        public static void CreateOthersIncident()
        {
            #region others
            //Paso 1
            IncidentTypeData incidentType = new IncidentTypeData();
            incidentType.CustomCode = "OTH";
            incidentType.Name = "Others";
            incidentType.FriendlyName = ResourceInitialDataLoader.GetString2("Others");
            incidentType.NoEmergency = false;

            incidentType.Procedure = @"<b>" + ResourceInitialDataLoader.GetString2("ProceduresTo") + incidentType.FriendlyName + @".</b> <ul> <li>" + ResourceInitialDataLoader.GetString2("Step1") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step2") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step3") + @"</li> </ul> ";



            //Paso 4

            //Indico las preguntas genericas para el incidente
            incidentType.SetIncidentTypeQuestions = new HashedSet();

            QuestionData questionWhere = new QuestionData();
            questionWhere.ShortText = ResourceInitialDataLoader.GetString2(STRQUESTIONWHERE);
            questionWhere = SmartCadDatabase.SearchObject<QuestionData>(questionWhere);


            IncidentTypeQuestionData itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionWhere;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            QuestionData questionNumber = new QuestionData();
            questionNumber.ShortText = ResourceInitialDataLoader.GetString2(STRQUESTIONNUMBERPERSON);
            questionNumber = SmartCadDatabase.SearchObject<QuestionData>(questionNumber);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionNumber;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //Preguntas especificas
            QuestionData questionType = new QuestionData();
            questionType.Text = ResourceInitialDataLoader.GetString2("OtherIncidentType");
            questionType.ShortText = ResourceInitialDataLoader.GetString2("IncidentTypeShorText");
            questionType.CustomCode = incidentType.CustomCode + "TYPE";
            questionType.RequirementType = RequirementTypeEnum.Recommended;
            questionType.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            QuestionPossibleAnswerData answer1 = new QuestionPossibleAnswerData();
            answer1.Question = questionType;
            answer1.Description = ResourceInitialDataLoader.GetString2("Description");
            answer1.ControlName = "TxtBxx1";
            ////IncidentTypeProcedure2Data * = new IncidentTypeProcedure2Data();
            ////proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            questionType.SetPossibleAnswers.Add(answer1);
            

            ////SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();            
            SmartCadDatabase.SaveObject<QuestionData>(questionType);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            string renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Texto libre' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.39657, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Texto libre</property>
      <property name='MinSize'>@2,Width=87@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Texto libre</property>
      <property name='TextSize'>@2,Width=51@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
            ";
#endregion renderMethod
            questionType.RenderMethod = renderMethod;
            SmartCadDatabase.UpdateObject<QuestionData>(questionType);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionType;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            SmartCadDatabase.SaveObject<IncidentTypeData>(incidentType);
            #endregion
        }
    }
}
