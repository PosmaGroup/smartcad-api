using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Drawing;

using SmartCadCore.Model;
using System.Collections;
using SmartCadCore.Common;

namespace SmartCadCore.Core
{
    public partial class SmartCadInitialData
    {
        private static void CreateOperatorStatus()
        {
            byte[] available = FormUtil.ImageToArray(ResourceLoader.GetImage("$Image.Available"));
            byte[] busy = FormUtil.ImageToArray(ResourceLoader.GetImage("$Image.Busy"));
            byte[] rest = FormUtil.ImageToArray(ResourceLoader.GetImage("$Image.Rest"));
            byte[] meeting = FormUtil.ImageToArray(ResourceLoader.GetImage("$Image.Meeting"));
            byte[] absent = FormUtil.ImageToArray(ResourceLoader.GetImage("$Image.Absent"));
            byte[] trainning = FormUtil.ImageToArray(ResourceLoader.GetImage("$Image.Trainning"));
            byte[] bath = FormUtil.ImageToArray(ResourceLoader.GetImage("$Image.Bath"));

            OperatorStatusData operatorStatus;

            operatorStatus = new OperatorStatusData();
            operatorStatus.Name = "Ready";
            operatorStatus.FriendlyName = ResourceInitialDataLoader.GetString2("Available");
            operatorStatus.Order = 1;
            operatorStatus.CustomCode = "READY";
            operatorStatus.NotReady = false;
            operatorStatus.Image = available;
            operatorStatus.Immutable = false;
            operatorStatus.Porcentage = 0;
            operatorStatus.Tolerance = 0;
            operatorStatus.ComponentColor = new ComponentColorData(System.Drawing.Color.FromArgb(57,154,67));
            SmartCadDatabase.SaveObject<OperatorStatusData>(operatorStatus);

            operatorStatus = new OperatorStatusData();
            operatorStatus.Name = "Busy";
            operatorStatus.FriendlyName = ResourceInitialDataLoader.GetString2("NotAvailable");
            operatorStatus.Order = 2;
            operatorStatus.CustomCode = "BUSY";
            operatorStatus.NotReady = true;
            operatorStatus.Image = busy;
            operatorStatus.Immutable = false;
            operatorStatus.Porcentage = 0;
            operatorStatus.Tolerance = 0;
            operatorStatus.ComponentColor = new ComponentColorData(System.Drawing.Color.FromArgb(39,39,183));
            SmartCadDatabase.SaveObject<OperatorStatusData>(operatorStatus);

            operatorStatus = new OperatorStatusData();
            operatorStatus.Name = "Bathroom";
            operatorStatus.FriendlyName = ResourceInitialDataLoader.GetString2("InBathroom");
            operatorStatus.Order = 3;
            operatorStatus.CustomCode = "BATHROOM";
            operatorStatus.NotReady = true;
            operatorStatus.Image = bath;
            operatorStatus.Immutable = false;
            operatorStatus.Porcentage = 10;
            operatorStatus.Tolerance = 5;
            operatorStatus.ComponentColor = new ComponentColorData(System.Drawing.Color.FromArgb(255,192,5));
            SmartCadDatabase.SaveObject<OperatorStatusData>(operatorStatus);

            operatorStatus = new OperatorStatusData();
            operatorStatus.Name = "Training";
            operatorStatus.FriendlyName = ResourceInitialDataLoader.GetString2("Training");
            operatorStatus.Order = 4;
            operatorStatus.CustomCode = "TRAINING";
            operatorStatus.NotReady = true;
            operatorStatus.Image = trainning;
            operatorStatus.Immutable = false;
            operatorStatus.Porcentage = 0;
            operatorStatus.Tolerance = 0;
            operatorStatus.ComponentColor = new ComponentColorData(System.Drawing.Color.FromArgb(255,168,84));
            SmartCadDatabase.SaveObject<OperatorStatusData>(operatorStatus);

            operatorStatus = new OperatorStatusData();
            operatorStatus.Name = "Rest";
            operatorStatus.FriendlyName = ResourceInitialDataLoader.GetString2("Rest");
            operatorStatus.Order = 5;
            operatorStatus.CustomCode = "REST";
            operatorStatus.NotReady = true;
            operatorStatus.Image = rest;
            operatorStatus.Immutable = false;
            operatorStatus.Porcentage = 20;
            operatorStatus.Tolerance = 5;
            operatorStatus.ComponentColor = new ComponentColorData(System.Drawing.Color.FromArgb(203,203,203));
            SmartCadDatabase.SaveObject<OperatorStatusData>(operatorStatus);

            operatorStatus = new OperatorStatusData();
            operatorStatus.Name = "Reunion";
            operatorStatus.FriendlyName = ResourceInitialDataLoader.GetString2("Reunion");
            operatorStatus.Order = 6;
            operatorStatus.CustomCode = "REUNION";
            operatorStatus.NotReady = true;
            operatorStatus.Image = meeting;
            operatorStatus.Immutable = false;
            operatorStatus.Porcentage = 15;
            operatorStatus.Tolerance = 5;
            operatorStatus.ComponentColor = new ComponentColorData(System.Drawing.Color.FromArgb(138,210,255));
            SmartCadDatabase.SaveObject<OperatorStatusData>(operatorStatus);

            operatorStatus = new OperatorStatusData();
            operatorStatus.Name = "Absent";
            operatorStatus.FriendlyName = ResourceInitialDataLoader.GetString2("Absent");
            operatorStatus.Order = 8;
            operatorStatus.CustomCode = "ABSENT";
            operatorStatus.NotReady = true;
            operatorStatus.Image = absent;
            operatorStatus.Immutable = false;
            operatorStatus.Porcentage = 0;
            operatorStatus.Tolerance = 0;
            operatorStatus.ComponentColor = new ComponentColorData(System.Drawing.Color.FromArgb(205,6,6));
            SmartCadDatabase.SaveObject<OperatorStatusData>(operatorStatus);
        }

        private static void CreateDispatchOrderStatus()
        {
            DispatchOrderStatusData dispatchOrderStatus;

            dispatchOrderStatus = new DispatchOrderStatusData();
            dispatchOrderStatus.Name = "Dispatched";
            dispatchOrderStatus.FriendlyName = ResourceInitialDataLoader.GetString2("DispatchOrder_Dispatched");
            dispatchOrderStatus.CustomCode = "DISPATCHED";
            dispatchOrderStatus.Color = System.Windows.Forms.ControlPaint.LightLight(Color.Green);
            dispatchOrderStatus.Immutable = true;
            SmartCadDatabase.SaveObject<DispatchOrderStatusData>(dispatchOrderStatus);

            dispatchOrderStatus = new DispatchOrderStatusData();
            dispatchOrderStatus.Name = "Cancelled";
            dispatchOrderStatus.FriendlyName = ResourceInitialDataLoader.GetString2("DispatchOrder_Cancelled");
            dispatchOrderStatus.CustomCode = "CANCELLED";
            dispatchOrderStatus.Color = Color.Yellow;
            dispatchOrderStatus.Immutable = true;
            SmartCadDatabase.SaveObject<DispatchOrderStatusData>(dispatchOrderStatus);

            dispatchOrderStatus = new DispatchOrderStatusData();
            dispatchOrderStatus.Name = "Closed";
            dispatchOrderStatus.FriendlyName = ResourceInitialDataLoader.GetString2("DispatchOrder_Closed");
            dispatchOrderStatus.CustomCode = "CLOSED";
            dispatchOrderStatus.Color = Color.White;
            dispatchOrderStatus.Immutable = true;
            SmartCadDatabase.SaveObject<DispatchOrderStatusData>(dispatchOrderStatus);

            dispatchOrderStatus = new DispatchOrderStatusData();
            dispatchOrderStatus.Name = "Delayed";
            dispatchOrderStatus.FriendlyName = ResourceInitialDataLoader.GetString2("DispatchOrder_Delayed");
            dispatchOrderStatus.CustomCode = "DELAYED";
            dispatchOrderStatus.Color = Color.Red;
            dispatchOrderStatus.Immutable = true;
            SmartCadDatabase.SaveObject<DispatchOrderStatusData>(dispatchOrderStatus);

            dispatchOrderStatus = new DispatchOrderStatusData();
            dispatchOrderStatus.Name = "Unit on scene";
            dispatchOrderStatus.FriendlyName = ResourceInitialDataLoader.GetString2("DispatchOrder_UnitOnScene");
            dispatchOrderStatus.CustomCode = "UNIT_ONSCENE";
            dispatchOrderStatus.Color = Color.Green;
            dispatchOrderStatus.Immutable = true;
            SmartCadDatabase.SaveObject<DispatchOrderStatusData>(dispatchOrderStatus);
        }

        private static void CreateIncidentNotificationStatus()
        {
            IncidentNotificationStatusData incidentNotificationStatus;

            incidentNotificationStatus = new IncidentNotificationStatusData();
            incidentNotificationStatus.Name = "Automatic Supervisor";
            incidentNotificationStatus.FriendlyName = ResourceInitialDataLoader.GetString2("Automatic Supervisor");
            incidentNotificationStatus.Color = Color.Aquamarine;
            incidentNotificationStatus.CustomCode = "AUTOMATIC_SUPERVISOR";
            incidentNotificationStatus.Active = false;
            incidentNotificationStatus.Immutable = true;
            SmartCadDatabase.SaveObject<IncidentNotificationStatusData>(incidentNotificationStatus);

            incidentNotificationStatus = new IncidentNotificationStatusData();
            incidentNotificationStatus.Name = "Manual Supervisor";
            incidentNotificationStatus.FriendlyName = ResourceInitialDataLoader.GetString2("Manual Supervisor");
            incidentNotificationStatus.Color = Color.AntiqueWhite;
            incidentNotificationStatus.CustomCode = "MANUAL_SUPERVISOR";
            incidentNotificationStatus.Active = false;
            incidentNotificationStatus.Immutable = true;
            SmartCadDatabase.SaveObject<IncidentNotificationStatusData>(incidentNotificationStatus);

            //Assigned is equivalent to New in Assigned incident notification section.
            incidentNotificationStatus = new IncidentNotificationStatusData();
            incidentNotificationStatus.Name = "Assigned";
            incidentNotificationStatus.FriendlyName = ResourceInitialDataLoader.GetString2("IncidentNotificationStatus_Assigned");
            incidentNotificationStatus.Color = Color.White;
            incidentNotificationStatus.CustomCode = "ASSIGNED";
            incidentNotificationStatus.Active = true;
            incidentNotificationStatus.Immutable = true;
            SmartCadDatabase.SaveObject<IncidentNotificationStatusData>(incidentNotificationStatus);

            incidentNotificationStatus = new IncidentNotificationStatusData();
            incidentNotificationStatus.Name = "ReAssigned";
            incidentNotificationStatus.FriendlyName = ResourceInitialDataLoader.GetString2("IncidentNotificationStatus_ReAssigned");
            incidentNotificationStatus.Color = Color.Blue;
            incidentNotificationStatus.CustomCode = "REASSIGNED";
            incidentNotificationStatus.Active = true;
            incidentNotificationStatus.Immutable = true;
            SmartCadDatabase.SaveObject<IncidentNotificationStatusData>(incidentNotificationStatus);

            incidentNotificationStatus = new IncidentNotificationStatusData();
            incidentNotificationStatus.Name = "New";
            incidentNotificationStatus.FriendlyName = ResourceInitialDataLoader.GetString2("IncidentNotificationStatus_New");
            incidentNotificationStatus.Color = Color.White;
            incidentNotificationStatus.CustomCode = "NEW";
            incidentNotificationStatus.Active = false;
            incidentNotificationStatus.Immutable = true;
            SmartCadDatabase.SaveObject<IncidentNotificationStatusData>(incidentNotificationStatus);

            incidentNotificationStatus = new IncidentNotificationStatusData();
            incidentNotificationStatus.Name = "Closed";
            incidentNotificationStatus.FriendlyName = ResourceInitialDataLoader.GetString2("IncidentNotificationStatus_Closed");
            incidentNotificationStatus.Color = Color.White;
            incidentNotificationStatus.CustomCode = "CLOSED";
            incidentNotificationStatus.Active = false;
            incidentNotificationStatus.Immutable = true;
            SmartCadDatabase.SaveObject<IncidentNotificationStatusData>(incidentNotificationStatus);

            incidentNotificationStatus = new IncidentNotificationStatusData();
            incidentNotificationStatus.Name = "Pending";
            incidentNotificationStatus.FriendlyName = ResourceInitialDataLoader.GetString2("IncidentNotificationStatus_Pending");
            incidentNotificationStatus.Color = Color.Orange;
            incidentNotificationStatus.CustomCode = "PENDING";
            incidentNotificationStatus.Active = true;
            incidentNotificationStatus.Immutable = true;
            SmartCadDatabase.SaveObject<IncidentNotificationStatusData>(incidentNotificationStatus);

            incidentNotificationStatus = new IncidentNotificationStatusData();
            incidentNotificationStatus.Name = "InProgress";
            incidentNotificationStatus.FriendlyName = ResourceInitialDataLoader.GetString2("IncidentNotificationStatus_InProgress");
            incidentNotificationStatus.Color = System.Windows.Forms.ControlPaint.LightLight(Color.Green);
            incidentNotificationStatus.CustomCode = "IN_PROGRESS";
            incidentNotificationStatus.Active = true;
            incidentNotificationStatus.Immutable = true;
            SmartCadDatabase.SaveObject<IncidentNotificationStatusData>(incidentNotificationStatus);
        
            incidentNotificationStatus = new IncidentNotificationStatusData();
            incidentNotificationStatus.Name = "Cancelled";
            incidentNotificationStatus.FriendlyName = ResourceInitialDataLoader.GetString2("IncidentNotificationStatus_Cancelled");
            incidentNotificationStatus.Color = Color.White;
            incidentNotificationStatus.CustomCode = "CANCELLED";
            incidentNotificationStatus.Active = false;
            incidentNotificationStatus.Immutable = true;
            SmartCadDatabase.SaveObject<IncidentNotificationStatusData>(incidentNotificationStatus);

            incidentNotificationStatus = new IncidentNotificationStatusData();
            incidentNotificationStatus.Name = "Updated";
            incidentNotificationStatus.FriendlyName = ResourceInitialDataLoader.GetString2("IncidentNotificationStatus_Updated");
            incidentNotificationStatus.Color = Color.LightBlue;
            incidentNotificationStatus.CustomCode = "UPDATED";
            incidentNotificationStatus.Active = true;
            incidentNotificationStatus.Immutable = true;
            SmartCadDatabase.SaveObject<IncidentNotificationStatusData>(incidentNotificationStatus);

            incidentNotificationStatus = new IncidentNotificationStatusData();
            incidentNotificationStatus.Name = "Delayed";
            incidentNotificationStatus.FriendlyName = ResourceInitialDataLoader.GetString2("IncidentNotificationStatus_Delayed");
            incidentNotificationStatus.Color = Color.Red;
            incidentNotificationStatus.CustomCode = "DELAYED";
            incidentNotificationStatus.Active = true;
            incidentNotificationStatus.Immutable = true;
            SmartCadDatabase.SaveObject<IncidentNotificationStatusData>(incidentNotificationStatus);
        }

        private static void CreateIncidentStatus()
        {
            IncidentStatusData incidentStatus;

            incidentStatus = new IncidentStatusData();
            incidentStatus.Name = "Open";
            incidentStatus.FriendlyName = ResourceInitialDataLoader.GetString2("IncidentStatus_Open");
            incidentStatus.Color = System.Windows.Forms.ControlPaint.LightLight(Color.Green);
            incidentStatus.CustomCode = "OPEN";
            incidentStatus.AllowDelete = false;
            SmartCadDatabase.SaveObject<IncidentStatusData>(incidentStatus);

            incidentStatus = new IncidentStatusData();
            incidentStatus.Name = "Closed";
            incidentStatus.FriendlyName = ResourceInitialDataLoader.GetString2("IncidentStatus_Closed");
            incidentStatus.Color = Color.Red;
            incidentStatus.CustomCode = "CLOSED";
            incidentStatus.AllowDelete = false;
            SmartCadDatabase.SaveObject<IncidentStatusData>(incidentStatus);

            incidentStatus = new IncidentStatusData();
            incidentStatus.Name = "Dispatched";
            incidentStatus.FriendlyName = ResourceInitialDataLoader.GetString2("IncidentStatus_Dispatched");
            incidentStatus.Color = Color.Yellow;
            incidentStatus.CustomCode = "DISPATCHED";
            incidentStatus.AllowDelete = false;
            SmartCadDatabase.SaveObject<IncidentStatusData>(incidentStatus);
        }

        private static void CreateUnitStatus()
        {
            UnitStatusData unitStatus;

            unitStatus = new UnitStatusData();
            unitStatus.Name = "Available";
            unitStatus.FriendlyName = ResourceInitialDataLoader.GetString2("UnitStatus_Available");
            unitStatus.Color = Color.White;
            unitStatus.CustomCode = "AVAILABLE";
            unitStatus.Active = true;
            unitStatus.Immutable = true;
            SmartCadDatabase.SaveObject<UnitStatusData>(unitStatus);

            unitStatus = new UnitStatusData();
            unitStatus.Name = "UnitStatus_OutOfOrder";
            unitStatus.FriendlyName = ResourceInitialDataLoader.GetString2("UnitStatus_OutOfOrder");
            unitStatus.Color = Color.Gray;
            unitStatus.CustomCode = "OUTOFORDER";
            unitStatus.Active = false;
            unitStatus.Immutable = true;
            SmartCadDatabase.SaveObject<UnitStatusData>(unitStatus);

            unitStatus = new UnitStatusData();
            unitStatus.Name = "Assigned";
            unitStatus.FriendlyName = ResourceInitialDataLoader.GetString2("UnitStatus_Assigned");
            unitStatus.Color = System.Windows.Forms.ControlPaint.LightLight(Color.Green);
            unitStatus.CustomCode = "ASSIGNED";
            unitStatus.Active = true;
            unitStatus.Immutable = true;
            SmartCadDatabase.SaveObject<UnitStatusData>(unitStatus);

            unitStatus = new UnitStatusData();
            unitStatus.Name = "OnScene";
            unitStatus.FriendlyName = ResourceInitialDataLoader.GetString2("UnitStatus_OnScene");
            unitStatus.Color = Color.Green;
            unitStatus.CustomCode = "ONSCENE";
            unitStatus.Active = true;
            unitStatus.Immutable = true;
            SmartCadDatabase.SaveObject<UnitStatusData>(unitStatus);

            unitStatus = new UnitStatusData();
            unitStatus.Name = "Delayed";
            unitStatus.FriendlyName = ResourceInitialDataLoader.GetString2("UnitStatus_Delayed");
            unitStatus.Color = Color.Red;
            unitStatus.CustomCode = "DELAYED";
            unitStatus.Active = true;
            unitStatus.Immutable = true;
            SmartCadDatabase.SaveObject<UnitStatusData>(unitStatus);

            unitStatus = new UnitStatusData();
            unitStatus.Name = "Not Available";
            unitStatus.FriendlyName = ResourceInitialDataLoader.GetString2("UnitStatus_NotAvailable");
            unitStatus.Color = System.Windows.Forms.ControlPaint.LightLight(Color.Gray);
            unitStatus.CustomCode = "NOTAVAILABLE";
            unitStatus.Active = false;
            unitStatus.Immutable = true;
            SmartCadDatabase.SaveObject<UnitStatusData>(unitStatus);
        }

        private static void CreateOfficer()
        {
            int id = 1;
            IList objects = SmartCadDatabase.SearchObjects(SmartCadHqls.GetAllUnits);

            string name = ResourceInitialDataLoader.GetString2("Names");
            string lastname = ResourceInitialDataLoader.GetString2("LastNames");

            foreach (UnitData unit in objects)
            {
                OfficerData officer = new OfficerData();
                officer.FirstName = ResourceInitialDataLoader.GetString2("OfficerName") + id;
                officer.LastName = ResourceInitialDataLoader.GetString2("OfficerLastName") + id;
                officer.PersonId = id.ToString();
                id++;
                officer.Birthday = DateTime.Now.Subtract(new TimeSpan(365 * 15, 0, 0, 0));
                officer.Badge = officer.PersonId;
                officer.Telephone = "1234567890";
                officer.DepartmentStation = unit.DepartmentStation;
                IList positions = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetPositionByDepartmentCode, unit.DepartmentType.Code));
                officer.Position = positions[0] as PositionData;
				IList ranks = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetRankByDepartmentCode, unit.DepartmentType.Code));
                officer.Rank = ranks[0] as RankData;
                officer = SmartCadDatabase.SaveObject<OfficerData>(officer);

                DepartmentStationAssignHistoryData departmentStationAssign = new DepartmentStationAssignHistoryData();
                departmentStationAssign.DepartmentStation = unit.DepartmentStation;
                departmentStationAssign.Start = DateTime.Now;
                departmentStationAssign.End = departmentStationAssign.Start + TimeSpan.FromDays(365);
                departmentStationAssign.UnitOfficerAssign = new ArrayList();

                UnitOfficerAssignHistoryData assign = new UnitOfficerAssignHistoryData();
                assign.Officer = officer;
                assign.Unit = unit;
                assign.Start = departmentStationAssign.Start;
                assign.End = departmentStationAssign.End;

                departmentStationAssign.UnitOfficerAssign.Add(assign);

                SmartCadDatabase.SaveObject<DepartmentStationAssignHistoryData>(departmentStationAssign);
            }
        }
    }
}
