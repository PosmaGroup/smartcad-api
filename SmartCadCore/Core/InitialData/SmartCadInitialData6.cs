using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using Iesi.Collections;

namespace SmartCadCore.Core
{
    public partial class SmartCadInitialData
    {
        public static void CreateGenericQuestionRobbery()
        {
            #region asaltantes
            QuestionData question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("AssailantsDescription");
            question.ShortText = ResourceInitialDataLoader.GetString2("AssailantQuestion");
            question.CustomCode = "ASSAILANT";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            QuestionPossibleAnswerData answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Height");
            answer1.ControlName = "TxtBxx2";

            //IncidentTypeProcedure2Data * = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);

            QuestionPossibleAnswerData answer2 = new QuestionPossibleAnswerData();
            answer2.Question = question;
            answer2.Description = ResourceInitialDataLoader.GetString2("Clothes");
            answer2.ControlName = "TxtBxx1";

            //IncidentTypeProcedure2Data * = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer2);

            QuestionPossibleAnswerData answer3 = new QuestionPossibleAnswerData();
            answer3.Question = question;
            answer3.Description = ResourceInitialDataLoader.GetString2("Hair");
            answer3.ControlName = "TxtBxx3";

            //IncidentTypeProcedure2Data * = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer3);

            QuestionPossibleAnswerData answer4 = new QuestionPossibleAnswerData();
            answer4.Question = question;
            answer4.Description = ResourceInitialDataLoader.GetString2("Eyes");
            answer4.ControlName = "TxtBxx4";

            //IncidentTypeProcedure2Data * = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer4);

            QuestionPossibleAnswerData answer5 = new QuestionPossibleAnswerData();
            answer5.Question = question;
            answer5.Description = ResourceInitialDataLoader.GetString2("SignificantFeatures");
            answer5.ControlName = "TxtBxx5";

            //IncidentTypeProcedure2Data * = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer5);


            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            string renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Texto libre' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.32118, Culture=neutral, PublicKeyToken=null' name='TxtBxx2' />
  <Control itemText='Texto libre' itemName='item0' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.32118, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <Control itemText='Texto libre' itemName='item2' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.32118, Culture=neutral, PublicKeyToken=null' name='TxtBxx3' />
  <Control itemText='Texto libre' itemName='item3' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.32118, Culture=neutral, PublicKeyToken=null' name='TxtBxx4' />
  <Control itemText='Texto libre' itemName='item4' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.32118, Culture=neutral, PublicKeyToken=null' name='TxtBxx5' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='6'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=610@3,Height=157</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@3,Y=-56</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=608@2,Height=31</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Descripci?n</property>
      <property name='MinSize'>@2,Width=87@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Texto libre</property>
      <property name='TextSize'>@2,Width=51@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=608@2,Height=31</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Nombre</property>
      <property name='MinSize'>@2,Width=87@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Texto libre</property>
      <property name='TextSize'>@2,Width=51@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item0</property>
      <property name='Location'>@1,X=0@2,Y=31</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item4' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=608@2,Height=31</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Texto libre</property>
      <property name='MinSize'>@2,Width=87@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Texto libre</property>
      <property name='TextSize'>@2,Width=51@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@1,X=0@2,Y=62</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx3</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item5' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=608@2,Height=31</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Texto libre</property>
      <property name='MinSize'>@2,Width=87@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Texto libre</property>
      <property name='TextSize'>@2,Width=51@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item3</property>
      <property name='Location'>@1,X=0@2,Y=93</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx4</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item6' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=608@2,Height=31</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Texto libre</property>
      <property name='MinSize'>@2,Width=87@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Texto libre</property>
      <property name='TextSize'>@2,Width=51@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item4</property>
      <property name='Location'>@1,X=0@3,Y=124</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx5</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";
            #endregion renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion
            
            #region arma
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("WeaponTypeUsed");
            question.ShortText = ResourceInitialDataLoader.GetString2("KidnappingWeapons");
            question.CustomCode = "KIDNAPPINGWEAPONS";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Weapons");
            answer1.ControlName = "TxtBxx1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);
            
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Texto libre' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.32118, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Texto libre</property>
      <property name='MinSize'>@2,Width=87@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Texto libre</property>
      <property name='TextSize'>@2,Width=51@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";
            #endregion renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion            

            #region robados
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("StolenItems");
            question.ShortText = ResourceInitialDataLoader.GetString2("StolenQuestion");
            question.CustomCode = "STOLEN";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("SuspectDescription");
            answer1.ControlName = "TxtBxx2";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = question;
            answer2.Description = ResourceInitialDataLoader.GetString2("Amount");
            answer2.ControlName = "TxtBxx1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer2);

            
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();            
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Texto libre' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.32118, Culture=neutral, PublicKeyToken=null' name='TxtBxx2' />
  <Control itemText='Texto libre' itemName='item0' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.32118, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='3'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=31</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Descripci?n</property>
      <property name='MinSize'>@2,Width=87@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Texto libre</property>
      <property name='TextSize'>@2,Width=51@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=68</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Nombre</property>
      <property name='MinSize'>@2,Width=87@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Texto libre</property>
      <property name='TextSize'>@2,Width=51@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item0</property>
      <property name='Location'>@1,X=0@2,Y=31</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";
            #endregion renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion
            
            #region placa
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("VehiclePlaque");
            question.ShortText = ResourceInitialDataLoader.GetString2("Plaques");
            question.CustomCode = "PLATES";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Stolen");
            answer1.ControlName = "TxtBxx1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = question;
            answer2.Description = ResourceInitialDataLoader.GetString2("Assailant");
            answer2.ControlName = "TxtBxx2";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer2);


            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Texto libre' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.32118, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <Control itemText='Texto libre' itemName='item0' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.32118, Culture=neutral, PublicKeyToken=null' name='TxtBxx2' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='3'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=31</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Texto libre</property>
      <property name='MinSize'>@2,Width=87@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Texto libre</property>
      <property name='TextSize'>@2,Width=51@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=68</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Texto libre</property>
      <property name='MinSize'>@2,Width=87@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Texto libre</property>
      <property name='TextSize'>@2,Width=51@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item0</property>
      <property name='Location'>@1,X=0@2,Y=31</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";
#endregion renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion            

            #region heridos
           question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("AreThereInjured");
            question.ShortText = ResourceInitialDataLoader.GetString2("InjuredQuestion");
            question.CustomCode = "INJURED";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("AnswerYes");
            answer1.ControlName = "RdBttn1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = question;
            answer2.Description = ResourceInitialDataLoader.GetString2("AnswerNo");
            answer2.ControlName = "RdBttn2";
            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer2);

            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Seleccion simple' itemName='item1' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn1' />
  <Control itemText='Seleccion simple' itemName='item2' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn2' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='3'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=312@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Seleccion simple</property>
      <property name='MinSize'>@3,Width=112@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Seleccion simple</property>
      <property name='TextSize'>@2,Width=76@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=313@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Seleccion simple</property>
      <property name='MinSize'>@3,Width=112@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Seleccion simple</property>
      <property name='TextSize'>@2,Width=76@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@3,X=312@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";
#endregion renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region heridas
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("KindWounds");
            question.ShortText = ResourceInitialDataLoader.GetString2("WoundsQuestion");
            question.CustomCode = "WOUNDS";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("WhiteWeapons");
            answer1.ControlName = "Chckdt1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = question;
            answer2.Description = ResourceInitialDataLoader.GetString2("FireWeapons");
            answer2.ControlName = "Chckdt2";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer2);

            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Armas' itemName='item1' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt1' />
  <Control itemText='Armas' itemName='item2' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt2' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='3'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=312@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Armas</property>
      <property name='MinSize'>@2,Width=66@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Armas</property>
      <property name='TextSize'>@2,Width=30@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=313@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Armas</property>
      <property name='MinSize'>@2,Width=66@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Armas</property>
      <property name='TextSize'>@2,Width=30@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@3,X=312@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";
            #endregion renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion
        }

        public static void CreateRobberyIncident()
        {
            //Este metodo crea la carga inicial para todos los tipos de incidentes de robo
            //Para agregar un tipo de incidente debo realizar los siguientes pasos
            //1.- Crear el incidente.
            //2.- Asociarle los tipos de departamentos que dan soporte a ese incidente
            //3.- Indicar los tipos de procedimientos asociados al incidente
            //4.- Indicar los tipos de preguntas asociadas al incidente con la siguiente estructura
            //4.1.-     Indicar un procedimiento para cada pregunta
            //4.2.-     Indicar las posible respuesta asociadas a la pregunta
            //4.3.-     Indicar la forma en que se renderiza la pregunta en el frontclient indicando los id de las respuestas asociadas
            CreateGenericQuestionRobbery();

            #region robotranseuntes
            //Paso 1
            IncidentTypeData incidentType = new IncidentTypeData();
            incidentType.CustomCode = "ROB";
            incidentType.Name = "RobberyPasser";
            incidentType.FriendlyName = ResourceInitialDataLoader.GetString2("RobberyPassers");
            incidentType.NoEmergency = false;

            //Paso 2
            //Bomberos
            DepartmentTypeData dpFire = new DepartmentTypeData(DPPOLICE);
            dpFire = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFire);

            //Bomberos (Atenci�n pre-hospitalaria)
            DepartmentTypeData dpFirePre = new DepartmentTypeData(DPFIREPRE);
            dpFirePre = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFirePre);

            

            incidentType.SetIncidentTypeDepartmentTypes = new HashedSet();

            IncidentTypeDepartmentTypeData itdFire = new IncidentTypeDepartmentTypeData();
            itdFire.DepartmentType = dpFire;
            itdFire.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFire);

            IncidentTypeDepartmentTypeData itdFirePre = new IncidentTypeDepartmentTypeData();
            itdFirePre.DepartmentType = dpFirePre;
            itdFirePre.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFirePre);


            incidentType.Procedure = @"<b>" + ResourceInitialDataLoader.GetString2("ProceduresTo") + incidentType.FriendlyName + @".</b> <ul> <li>" + ResourceInitialDataLoader.GetString2("Step1") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step2") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step3") + @"</li> </ul> ";



            //Paso 4

            //Indico las preguntas genericas para el incidente
            incidentType.SetIncidentTypeQuestions = new HashedSet();

            QuestionData questionWhere = new QuestionData();
            questionWhere.ShortText = ResourceInitialDataLoader.GetString2(STRQUESTIONWHERE);
            questionWhere = SmartCadDatabase.SearchObject<QuestionData>(questionWhere);


            IncidentTypeQuestionData itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionWhere;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            QuestionData questionNumber = new QuestionData();
            questionNumber.ShortText = ResourceInitialDataLoader.GetString2(STRQUESTIONNUMBERPERSON);
            questionNumber = SmartCadDatabase.SearchObject<QuestionData>(questionNumber);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionNumber;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            
            //Preguntas especificas
            QuestionData questionType = new QuestionData();
            questionType.Text = ResourceInitialDataLoader.GetString2("RobberyType");
            questionType.ShortText = ResourceInitialDataLoader.GetString2("RobberyTypeShort");
            questionType.CustomCode = incidentType.CustomCode + "TYPE";
            questionType.RequirementType = RequirementTypeEnum.Recommended;
            questionType.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            QuestionPossibleAnswerData answer1 = new QuestionPossibleAnswerData();
            answer1.Question = questionType;
            answer1.Description = ResourceInitialDataLoader.GetString2("AtGunpoint");
            answer1.ControlName = "RdBttn8";
            //IncidentTypeProcedure2Data * = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            questionType.SetPossibleAnswers.Add(answer1);

            QuestionPossibleAnswerData answer2 = new QuestionPossibleAnswerData();
            answer2.Question = questionType;
            answer2.Description = ResourceInitialDataLoader.GetString2("Gang");
            answer2.ControlName = "RdBttn9";
            //IncidentTypeProcedure2Data * = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            questionType.SetPossibleAnswers.Add(answer2);

            QuestionPossibleAnswerData answer3 = new QuestionPossibleAnswerData();
            answer3.Question = questionType;
            answer3.Description = ResourceInitialDataLoader.GetString2("StreetAssault");
            answer3.ControlName = "RdBttn11";
            //IncidentTypeProcedure2Data * = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            questionType.SetPossibleAnswers.Add(answer3);


            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(questionType);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            string renderMethod = @"
            <XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Seleccion simple' itemName='item6' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn8' />
  <Control itemText='Seleccion simple' itemName='item7' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn9' />
  <Control itemText='Seleccion simple' itemName='item9' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn11' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='4'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=173@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Seleccion simple</property>
      <property name='MinSize'>@3,Width=112@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Seleccion simple</property>
      <property name='TextSize'>@2,Width=76@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item6</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn8</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=228@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Seleccion simple</property>
      <property name='MinSize'>@3,Width=112@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Seleccion simple</property>
      <property name='TextSize'>@2,Width=76@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item7</property>
      <property name='Location'>@3,X=173@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn9</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item4' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=224@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Seleccion simple</property>
      <property name='MinSize'>@3,Width=112@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Seleccion simple</property>
      <property name='TextSize'>@2,Width=76@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item9</property>
      <property name='Location'>@3,X=401@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn11</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
            ";
#endregion renderMethod
            questionType.RenderMethod = renderMethod;
            SmartCadDatabase.UpdateObject<QuestionData>(questionType);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionType;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //Asocio las preguntas genericas para cada robo
            QuestionData question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("AssailantQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("WeaponsQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);


            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("StolenQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("Plaques");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("InjuredQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("WoundsQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            
            SmartCadDatabase.SaveObject<IncidentTypeData>(incidentType);
            #endregion

            #region robovehiculos
            //Paso 1
            incidentType = new IncidentTypeData();
            incidentType.CustomCode = "ROV";
            incidentType.Name = "RobberyVehicle";
            incidentType.FriendlyName = ResourceInitialDataLoader.GetString2("RobberyVehicles");
            incidentType.NoEmergency = false;

            //Paso 2
            //Bomberos
            dpFire = new DepartmentTypeData(DPPOLICE);
            dpFire = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFire);

            //Bomberos (Atenci�n pre-hospitalaria)
            dpFirePre = new DepartmentTypeData(DPFIREPRE);
            dpFirePre = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFirePre);



            incidentType.SetIncidentTypeDepartmentTypes = new HashedSet();

            itdFire = new IncidentTypeDepartmentTypeData();
            itdFire.DepartmentType = dpFire;
            itdFire.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFire);

            itdFirePre = new IncidentTypeDepartmentTypeData();
            itdFirePre.DepartmentType = dpFirePre;
            itdFirePre.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFirePre);


            incidentType.Procedure = @"<b>" + ResourceInitialDataLoader.GetString2("ProceduresTo") + incidentType.FriendlyName + @".</b> <ul> <li>" + ResourceInitialDataLoader.GetString2("Step1") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step2") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step3") + @"</li> </ul> ";



            //Paso 4

            //Indico las preguntas genericas para el incidente
            incidentType.SetIncidentTypeQuestions = new HashedSet();

            questionWhere = new QuestionData();
            questionWhere.ShortText = ResourceInitialDataLoader.GetString2(STRQUESTIONWHERE);
            questionWhere = SmartCadDatabase.SearchObject<QuestionData>(questionWhere);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionWhere;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            questionNumber = new QuestionData();
            questionNumber.ShortText = ResourceInitialDataLoader.GetString2(STRQUESTIONNUMBERPERSON);
            questionNumber = SmartCadDatabase.SearchObject<QuestionData>(questionNumber);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionNumber;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);


            //Preguntas especificas
            questionType = new QuestionData();
            questionType.Text = ResourceInitialDataLoader.GetString2("KindCar");
            questionType.ShortText = ResourceInitialDataLoader.GetString2("KindCarShort");
            questionType.CustomCode = incidentType.CustomCode + "TYPE";
            questionType.RequirementType = RequirementTypeEnum.Recommended;
            questionType.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = questionType;
            answer1.Description = ResourceInitialDataLoader.GetString2("Sedan");
            answer1.ControlName = "RdBttn4";
            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            questionType.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = questionType;
            answer2.Description = ResourceInitialDataLoader.GetString2("TruckBus");
            answer2.ControlName = "RdBttn5";
            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            questionType.SetPossibleAnswers.Add(answer2);            


            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(questionType);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            renderMethod = @"
           <XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Seleccion simple' itemName='item1' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn4' />
  <Control itemText='Seleccion simple' itemName='item2' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn5' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='3'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=312@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Seleccion simple</property>
      <property name='MinSize'>@3,Width=112@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Seleccion simple</property>
      <property name='TextSize'>@2,Width=76@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn4</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=313@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Seleccion simple</property>
      <property name='MinSize'>@3,Width=112@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Seleccion simple</property>
      <property name='TextSize'>@2,Width=76@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@3,X=312@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn5</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
            ";
#endregion renderMethod
            questionType.RenderMethod = renderMethod;
            SmartCadDatabase.UpdateObject<QuestionData>(questionType);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionType;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //Asocio las preguntas genericas para cada robo
            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("AssailantQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("WeaponsQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);


            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("StolenQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("Plaques");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("InjuredQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("WoundsQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);


            SmartCadDatabase.SaveObject<IncidentTypeData>(incidentType);
            #endregion

            #region roboresidencias
            //Paso 1
            incidentType = new IncidentTypeData();
            incidentType.CustomCode = "ROR";
            incidentType.Name = "RobberyResidence";
            incidentType.FriendlyName = ResourceInitialDataLoader.GetString2("RobberyResidence");
            incidentType.NoEmergency = false;

            //Paso 2
            //Bomberos
            dpFire = new DepartmentTypeData(DPPOLICE);
            dpFire = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFire);

            //Bomberos (Atenci�n pre-hospitalaria)
            dpFirePre = new DepartmentTypeData(DPFIREPRE);
            dpFirePre = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFirePre);



            incidentType.SetIncidentTypeDepartmentTypes = new HashedSet();

            itdFire = new IncidentTypeDepartmentTypeData();
            itdFire.DepartmentType = dpFire;
            itdFire.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFire);

            itdFirePre = new IncidentTypeDepartmentTypeData();
            itdFirePre.DepartmentType = dpFirePre;
            itdFirePre.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFirePre);


            incidentType.Procedure = @"<b>" + ResourceInitialDataLoader.GetString2("ProceduresTo") + incidentType.FriendlyName + @".</b> <ul> <li>" + ResourceInitialDataLoader.GetString2("Step1") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step2") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step3") + @"</li> </ul> ";



            //Paso 4

            //Indico las preguntas genericas para el incidente
            incidentType.SetIncidentTypeQuestions = new HashedSet();

            questionWhere = new QuestionData();
            questionWhere.ShortText = ResourceInitialDataLoader.GetString2(STRQUESTIONWHERE);
            questionWhere = SmartCadDatabase.SearchObject<QuestionData>(questionWhere);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionWhere;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            questionNumber = new QuestionData();
            questionNumber.ShortText = ResourceInitialDataLoader.GetString2(STRQUESTIONNUMBERPERSON);
            questionNumber = SmartCadDatabase.SearchObject<QuestionData>(questionNumber);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionNumber;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);


            //Preguntas especificas
            questionType = new QuestionData();
            questionType.Text = ResourceInitialDataLoader.GetString2("KindStoleHome");
            questionType.ShortText = ResourceInitialDataLoader.GetString2("KindStoleHomeShort");
            questionType.CustomCode = incidentType.CustomCode + "TYPE";
            questionType.RequirementType = RequirementTypeEnum.Recommended;
            questionType.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = questionType;
            answer1.Description = ResourceInitialDataLoader.GetString2("Residence");
            answer1.ControlName = "Chckdt1";
            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            questionType.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = questionType;
            answer2.Description = ResourceInitialDataLoader.GetString2("BusinessLocal");
            answer2.ControlName = "Chckdt2";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            questionType.SetPossibleAnswers.Add(answer2);

            answer3 = new QuestionPossibleAnswerData();
            answer3.Question = questionType;
            answer3.Description = ResourceInitialDataLoader.GetString2("Winery");
            answer3.ControlName = "Chckdt4";
            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            questionType.SetPossibleAnswers.Add(answer3);


            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(questionType);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Seleccion multiple' itemName='item1' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt1' />
  <Control itemText='Seleccion multiple' itemName='item2' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt2' />
  <Control itemText='Seleccion multiple' itemName='item4' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt4' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='4'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=312@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Seleccion multiple</property>
      <property name='MinSize'>@3,Width=119@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Seleccion multiple</property>
      <property name='TextSize'>@2,Width=83@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@3,X=156@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=156@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Seleccion multiple</property>
      <property name='MinSize'>@3,Width=119@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Seleccion multiple</property>
      <property name='TextSize'>@2,Width=83@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item4' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=157@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Seleccion multiple</property>
      <property name='MinSize'>@3,Width=119@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Seleccion multiple</property>
      <property name='TextSize'>@2,Width=83@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item4</property>
      <property name='Location'>@3,X=468@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt4</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
            ";
#endregion renderMethod
            questionType.RenderMethod = renderMethod;
            SmartCadDatabase.UpdateObject<QuestionData>(questionType);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionType;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //Asocio las preguntas genericas para cada robo
            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("AssailantQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("WeaponsQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);


            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("StolenQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("Plaques");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("InjuredQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("WoundsQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);


            SmartCadDatabase.SaveObject<IncidentTypeData>(incidentType);
            #endregion
        }

        public static void CreateGenericQuestionHomicide()
        {
            #region circustancias
            QuestionData question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("CrimeCircumstances");
            question.ShortText = ResourceInitialDataLoader.GetString2("CircumstancesQuestion");
            question.CustomCode = "CIRCUMSTANCES";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            QuestionPossibleAnswerData answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Place");
            answer1.ControlName = "TxtBxx1";

            //IncidentTypeProcedure2Data * = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);

            

            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            string renderMethod = @"
<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Texto libre' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.32118, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Texto libre</property>
      <property name='MinSize'>@2,Width=87@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Texto libre</property>
      <property name='TextSize'>@2,Width=51@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";
#endregion renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion
            
            #region sospechoso
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("OneSuspectDescriptionQuestion");
            question.ShortText = ResourceInitialDataLoader.GetString2("OneSuspectQuestion");
            question.CustomCode = "SUSPECT";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("SuspectDescription");
            answer1.ControlName = "TxtBxx1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);



            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Texto libre' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.32118, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Texto libre</property>
      <property name='MinSize'>@2,Width=87@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Texto libre</property>
      <property name='TextSize'>@2,Width=51@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";
            #endregion renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion
                        
            #region armas
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("WhatWeaponTypeUsed");
            question.ShortText = ResourceInitialDataLoader.GetString2("WeaponsQuestion");
            question.CustomCode = "WEAPONS";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Weapons");
            answer1.ControlName = "TxtBxx1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);



            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Texto libre' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.32118, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Texto libre</property>
      <property name='MinSize'>@2,Width=87@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Texto libre</property>
      <property name='TextSize'>@2,Width=51@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";
            #endregion renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion
                       
            #region suceso
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("EventDescriptionQuestion");
            question.ShortText = ResourceInitialDataLoader.GetString2("EventQuestion");
            question.CustomCode = "EVENT";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("EventDescription");
            answer1.ControlName = "TxtBxx1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);



            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Texto libre' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.32118, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Texto libre</property>
      <property name='MinSize'>@2,Width=87@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Texto libre</property>
      <property name='TextSize'>@2,Width=51@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";
            #endregion renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion
                       
            #region extinto
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("ExtinctNameOrInformation");
            question.ShortText = ResourceInitialDataLoader.GetString2("ExtinctQuestion");
            question.CustomCode = "EXTINCT";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Information");
            answer1.ControlName = "TxtBxx1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);



            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Texto libre' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.32118, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Texto libre</property>
      <property name='MinSize'>@2,Width=87@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Texto libre</property>
      <property name='TextSize'>@2,Width=51@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";
            #endregion renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion
                       
            #region testigos
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("CrimeSceneWitnesses");
            question.ShortText = ResourceInitialDataLoader.GetString2("WitnessesQuestion");
            question.CustomCode = "WITNESSES";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Witnesses");
            answer1.ControlName = "TxtBxx1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);



            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Texto libre' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.32118, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Texto libre</property>
      <property name='MinSize'>@2,Width=87@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Texto libre</property>
      <property name='TextSize'>@2,Width=51@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";
#endregion renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion            
           
            #region utilizada
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("LocationWeaponUsed");
            question.ShortText = ResourceInitialDataLoader.GetString2("UsedQuestion");
            question.CustomCode = "USED";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("LocationNoQuestion");
            answer1.ControlName = "TxtBxx1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);



            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Texto libre' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.32118, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Texto libre</property>
      <property name='MinSize'>@2,Width=87@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Texto libre</property>
      <property name='TextSize'>@2,Width=51@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";
            #endregion renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion
            
        }

        public static void CreateHomicideIncident()
        {
            //Este metodo crea la carga inicial para todos los tipos de incidentes de robo
            //Para agregar un tipo de incidente debo realizar los siguientes pasos
            //1.- Crear el incidente.
            //2.- Asociarle los tipos de departamentos que dan soporte a ese incidente
            //3.- Indicar los tipos de procedimientos asociados al incidente
            //4.- Indicar los tipos de preguntas asociadas al incidente con la siguiente estructura
            //4.1.-     Indicar un procedimiento para cada pregunta
            //4.2.-     Indicar las posible respuesta asociadas a la pregunta
            //4.3.-     Indicar la forma en que se renderiza la pregunta en el frontclient indicando los id de las respuestas asociadas
            CreateGenericQuestionHomicide();

            #region homicide
            //Paso 1
            IncidentTypeData incidentType = new IncidentTypeData();
            incidentType.CustomCode = "HOM";
            incidentType.Name = "Homicide";
            incidentType.FriendlyName = ResourceInitialDataLoader.GetString2("Homicide");
            incidentType.NoEmergency = false;

            //Paso 2
            //Policia
            DepartmentTypeData dpFire = new DepartmentTypeData(DPPOLICE);
            dpFire = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFire);

            //CICP
            DepartmentTypeData dpFirePre = new DepartmentTypeData(DPCICPC);
            dpFirePre = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFirePre);



            incidentType.SetIncidentTypeDepartmentTypes = new HashedSet();

            IncidentTypeDepartmentTypeData itdFire = new IncidentTypeDepartmentTypeData();
            itdFire.DepartmentType = dpFire;
            itdFire.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFire);

            IncidentTypeDepartmentTypeData itdFirePre = new IncidentTypeDepartmentTypeData();
            itdFirePre.DepartmentType = dpFirePre;
            itdFirePre.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFirePre);


            incidentType.Procedure = @"<b>" + ResourceInitialDataLoader.GetString2("ProceduresTo") + incidentType.FriendlyName + @".</b> <ul> <li>" + ResourceInitialDataLoader.GetString2("Step1") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step2") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step3") + @"</li> </ul> ";



            //Paso 4

            //Indico las preguntas genericas para el incidente
            incidentType.SetIncidentTypeQuestions = new HashedSet();

            QuestionData questionWhere = new QuestionData();
            questionWhere.ShortText = ResourceInitialDataLoader.GetString2(STRQUESTIONWHERE);
            questionWhere = SmartCadDatabase.SearchObject<QuestionData>(questionWhere);


            IncidentTypeQuestionData itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionWhere;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            QuestionData questionNumber = new QuestionData();
            questionNumber.ShortText = ResourceInitialDataLoader.GetString2(STRQUESTIONNUMBERPERSON);
            questionNumber = SmartCadDatabase.SearchObject<QuestionData>(questionNumber);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionNumber;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);


            //Preguntas especificas
            QuestionData questionType = new QuestionData();
            questionType.Text = ResourceInitialDataLoader.GetString2("HomicideType");
            questionType.ShortText = ResourceInitialDataLoader.GetString2("HomicideTypeShort");
            questionType.CustomCode = incidentType.CustomCode + "TYPE";
            questionType.RequirementType = RequirementTypeEnum.Recommended;
            questionType.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            QuestionPossibleAnswerData answer1 = new QuestionPossibleAnswerData();
            answer1.Question = questionType;
            answer1.Description = ResourceInitialDataLoader.GetString2("WhiteWeapons");
            answer1.ControlName = "RdBttn8";
            //IncidentTypeProcedure2Data * = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            questionType.SetPossibleAnswers.Add(answer1);

            QuestionPossibleAnswerData answer2 = new QuestionPossibleAnswerData();
            answer2.Question = questionType;
            answer2.Description = ResourceInitialDataLoader.GetString2("FireWeapons");
            answer2.ControlName = "RdBttn9";
            //IncidentTypeProcedure2Data * = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            questionType.SetPossibleAnswers.Add(answer2);

            QuestionPossibleAnswerData answer3 = new QuestionPossibleAnswerData();
            answer3.Question = questionType;
            answer3.Description = ResourceInitialDataLoader.GetString2("WithoutWeapons");
            answer3.ControlName = "RdBttn11";
            //IncidentTypeProcedure2Data * = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            questionType.SetPossibleAnswers.Add(answer3);


            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(questionType);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            string renderMethod = @"
            <XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Seleccion simple' itemName='item6' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn8' />
  <Control itemText='Seleccion simple' itemName='item7' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn9' />
  <Control itemText='Seleccion simple' itemName='item9' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn11' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='4'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=173@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Seleccion simple</property>
      <property name='MinSize'>@3,Width=112@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Seleccion simple</property>
      <property name='TextSize'>@2,Width=76@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item6</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn8</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=228@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Seleccion simple</property>
      <property name='MinSize'>@3,Width=112@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Seleccion simple</property>
      <property name='TextSize'>@2,Width=76@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item7</property>
      <property name='Location'>@3,X=173@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn9</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item4' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=224@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Seleccion simple</property>
      <property name='MinSize'>@3,Width=112@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Seleccion simple</property>
      <property name='TextSize'>@2,Width=76@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item9</property>
      <property name='Location'>@3,X=401@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn11</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
            ";
#endregion renderMethod
            questionType.RenderMethod = renderMethod;
            SmartCadDatabase.UpdateObject<QuestionData>(questionType);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionType;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //Asocio las preguntas genericas para cada robo
            QuestionData question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("CircumstancesQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("SuspectQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);


            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("WeaponsQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("ExtinctQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("WitnessesQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);
            
            SmartCadDatabase.SaveObject<IncidentTypeData>(incidentType);
            #endregion
        }

        public static void CreateGenericQuestionDisturbance()
        {
            #region ri�a
            QuestionData question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("QuarrelCircumstances");
            question.ShortText = ResourceInitialDataLoader.GetString2("QuarrelCircumstancesShort");
            question.CustomCode = "RI�A";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            QuestionPossibleAnswerData answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Place");
            answer1.ControlName = "TxtBxx2";

            //IncidentTypeProcedure2Data * = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);

            //Esta estructura se maneja de manera generica para todos los incidentes
            QuestionPossibleAnswerData answer2 = new QuestionPossibleAnswerData();
            answer2.Question = question;
            answer2.Description = ResourceInitialDataLoader.GetString2("Circumstances");
            answer2.ControlName = "TxtBxx1";

            //IncidentTypeProcedure2Data * = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer2);



            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            string renderMethod = @"
<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Texto libre' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.32118, Culture=neutral, PublicKeyToken=null' name='TxtBxx2' />
  <Control itemText='Texto libre' itemName='item0' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.32118, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='3'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=31</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Texto libre</property>
      <property name='MinSize'>@2,Width=87@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Texto libre</property>
      <property name='TextSize'>@2,Width=51@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=68</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Texto libre</property>
      <property name='MinSize'>@2,Width=87@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Texto libre</property>
      <property name='TextSize'>@2,Width=51@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item0</property>
      <property name='Location'>@1,X=0@2,Y=31</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";
            #endregion renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region personas
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("PersonNameOrInformation");
            question.ShortText = ResourceInitialDataLoader.GetString2("PersonNameOrInformationShort");
            question.CustomCode = "PERSONAS";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Name");
            answer1.ControlName = "TxtBxx2";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = question;
            answer2.Description = ResourceInitialDataLoader.GetString2("Information");
            answer2.ControlName = "TxtBxx1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer2);



            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            renderMethod = @"
<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Texto libre' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.32118, Culture=neutral, PublicKeyToken=null' name='TxtBxx2' />
  <Control itemText='Texto libre' itemName='item0' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.32118, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='3'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=31</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Texto libre</property>
      <property name='MinSize'>@2,Width=87@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Texto libre</property>
      <property name='TextSize'>@2,Width=51@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=68</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Texto libre</property>
      <property name='MinSize'>@2,Width=87@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Texto libre</property>
      <property name='TextSize'>@2,Width=51@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item0</property>
      <property name='Location'>@1,X=0@2,Y=31</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";
            #endregion renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion
            
            #region victima
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("VictimName");
            question.ShortText = ResourceInitialDataLoader.GetString2("VictimNameShort");
            question.CustomCode = "VICTIMNAME";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Name");
            answer1.ControlName = "TxtBxx1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);
            


            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Texto libre' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.32118, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Texto libre</property>
      <property name='MinSize'>@2,Width=87@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Texto libre</property>
      <property name='TextSize'>@2,Width=51@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";
            #endregion renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion            

            #region amotinados
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("MutineersQuantity");
            question.ShortText = ResourceInitialDataLoader.GetString2("MutineersQuantityShort");
            question.CustomCode = "AMOTINADOS";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Amount");
            answer1.ControlName = "TxtBxx2";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);

            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Texto libre' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.32118, Culture=neutral, PublicKeyToken=null' name='TxtBxx2' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Texto libre</property>
      <property name='MinSize'>@2,Width=87@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Texto libre</property>
      <property name='TextSize'>@2,Width=51@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";
#endregion renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region violacion
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("ViolationSuspectDescription");
            question.ShortText = ResourceInitialDataLoader.GetString2("ViolationSuspectDescriptionShort");
            question.CustomCode = "VIOLACION";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("SuspectDescription");
            answer1.ControlName = "TxtBxx2";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);

            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();

            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Texto libre' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.32118, Culture=neutral, PublicKeyToken=null' name='TxtBxx2' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Texto libre</property>
      <property name='MinSize'>@2,Width=87@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Texto libre</property>
      <property name='TextSize'>@2,Width=51@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";
            #endregion renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region disponibles
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("AvailablesWeapons");
            question.ShortText = ResourceInitialDataLoader.GetString2("AvailablesWeaponsShort");
            question.CustomCode = "DISPONIBLES";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("WeaponsType");
            answer1.ControlName = "TxtBxx1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);

            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();

            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Texto libre' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.32118, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Texto libre</property>
      <property name='MinSize'>@2,Width=87@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Texto libre</property>
      <property name='TextSize'>@2,Width=51@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";
#endregion renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion
        
        }

        public static void CreateDisturbanceIncident()
        {
            //Este metodo crea la carga inicial para todos los tipos de incidentes de Disturbios
            //Para agregar un tipo de incidente debo realizar los siguientes pasos
            //1.- Crear el incidente.
            //2.- Asociarle los tipos de departamentos que dan soporte a ese incidente
            //3.- Indicar los tipos de procedimientos asociados al incidente
            //4.- Indicar los tipos de preguntas asociadas al incidente con la siguiente estructura
            //4.1.-     Indicar un procedimiento para cada pregunta
            //4.2.-     Indicar las posible respuesta asociadas a la pregunta
            //4.3.-     Indicar la forma en que se renderiza la pregunta en el frontclient indicando los id de las respuestas asociadas
            CreateGenericQuestionDisturbance();

            #region ri�a
            //Paso 1
            IncidentTypeData incidentType = new IncidentTypeData();
            incidentType.CustomCode = "PEL";
            incidentType.Name = "Ri�a";
            incidentType.FriendlyName = ResourceInitialDataLoader.GetString2("Quarrel");
            incidentType.NoEmergency = false;

            //Paso 2
            //Policia
            DepartmentTypeData dpFire = new DepartmentTypeData(DPPOLICE);
            dpFire = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFire);

            //Bomberos Emergencias
            DepartmentTypeData dpFirePre = new DepartmentTypeData(DPFIREPRE);
            dpFirePre = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFirePre);



            incidentType.SetIncidentTypeDepartmentTypes = new HashedSet();

            IncidentTypeDepartmentTypeData itdFire = new IncidentTypeDepartmentTypeData();
            itdFire.DepartmentType = dpFire;
            itdFire.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFire);

            IncidentTypeDepartmentTypeData itdFirePre = new IncidentTypeDepartmentTypeData();
            itdFirePre.DepartmentType = dpFirePre;
            itdFirePre.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFirePre);


            incidentType.Procedure = @"<b>" + ResourceInitialDataLoader.GetString2("ProceduresTo") + incidentType.FriendlyName + @".</b> <ul> <li>" + ResourceInitialDataLoader.GetString2("Step1") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step2") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step3") + @"</li> </ul> ";



            //Paso 4

            //Indico las preguntas genericas para el incidente
            incidentType.SetIncidentTypeQuestions = new HashedSet();

            QuestionData questionWhere = new QuestionData();
            questionWhere.ShortText = ResourceInitialDataLoader.GetString2(STRQUESTIONWHERE);
            questionWhere = SmartCadDatabase.SearchObject<QuestionData>(questionWhere);


            IncidentTypeQuestionData itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionWhere;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            QuestionData questionNumber = new QuestionData();
            questionNumber.ShortText = ResourceInitialDataLoader.GetString2(STRQUESTIONNUMBERPERSON);
            questionNumber = SmartCadDatabase.SearchObject<QuestionData>(questionNumber);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionNumber;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);


            //Preguntas especificas
            QuestionData questionType = new QuestionData();
            questionType.Text = ResourceInitialDataLoader.GetString2("QuarrelType");
            questionType.ShortText = ResourceInitialDataLoader.GetString2("QuarrelTypeShort");
            questionType.CustomCode = incidentType.CustomCode + "TYPE";
            questionType.RequirementType = RequirementTypeEnum.Recommended;
            questionType.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            QuestionPossibleAnswerData answer1 = new QuestionPossibleAnswerData();
            answer1.Question = questionType;
            answer1.Description = ResourceInitialDataLoader.GetString2("DomesticQuarrel");
            answer1.ControlName = "Chckdt4";
            //IncidentTypeProcedure2Data * = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            questionType.SetPossibleAnswers.Add(answer1);

            QuestionPossibleAnswerData answer2 = new QuestionPossibleAnswerData();
            answer2.Question = questionType;
            answer2.Description = ResourceInitialDataLoader.GetString2("StreetQuarrel");
            answer2.ControlName = "Chckdt5";
            //IncidentTypeProcedure2Data * = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            questionType.SetPossibleAnswers.Add(answer2);


            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(questionType);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            string renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Seleccion multiple' itemName='item1' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt4' />
  <Control itemText='Seleccion multiple' itemName='item2' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt5' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='3'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=312@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Seleccion multiple</property>
      <property name='MinSize'>@3,Width=119@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Seleccion multiple</property>
      <property name='TextSize'>@2,Width=83@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt4</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=313@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Seleccion multiple</property>
      <property name='MinSize'>@3,Width=119@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Seleccion multiple</property>
      <property name='TextSize'>@2,Width=83@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@3,X=312@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt5</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
            ";
#endregion renderMethod
            questionType.RenderMethod = renderMethod;
            SmartCadDatabase.UpdateObject<QuestionData>(questionType);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionType;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);


            questionType = new QuestionData();
            questionType.Text = ResourceInitialDataLoader.GetString2("Weapons");
            questionType.ShortText = ResourceInitialDataLoader.GetString2("WeaponsShort");
            questionType.CustomCode = incidentType.CustomCode + "ARMAS";
            questionType.RequirementType = RequirementTypeEnum.Recommended;
            questionType.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = questionType;
            answer1.Description = ResourceInitialDataLoader.GetString2("YesWeapons");
            answer1.ControlName = "Chckdt4";
            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            questionType.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = questionType;
            answer2.Description = ResourceInitialDataLoader.GetString2("WhiteWeapons");
            answer2.ControlName = "Chckdt5";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            questionType.SetPossibleAnswers.Add(answer2);

            QuestionPossibleAnswerData answer3 = new QuestionPossibleAnswerData();
            answer3.Question = questionType;
            answer3.Description = ResourceInitialDataLoader.GetString2("FireWeapons");
            answer3.ControlName = "Chckdt7";

            //IncidentTypeProcedure2Data * = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            questionType.SetPossibleAnswers.Add(answer3);


            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(questionType);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region rendereMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Seleccion multiple' itemName='item2' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt4' />
  <Control itemText='Seleccion multiple' itemName='item3' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt5' />
  <Control itemText='Seleccion multiple' itemName='item5' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt7' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='4'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=312@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Seleccion multiple</property>
      <property name='MinSize'>@3,Width=119@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Seleccion multiple</property>
      <property name='TextSize'>@2,Width=83@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@3,X=156@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt4</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=156@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Seleccion multiple</property>
      <property name='MinSize'>@3,Width=119@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Seleccion multiple</property>
      <property name='TextSize'>@2,Width=83@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item3</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt5</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item4' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=157@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Seleccion multiple</property>
      <property name='MinSize'>@3,Width=119@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Seleccion multiple</property>
      <property name='TextSize'>@2,Width=83@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item5</property>
      <property name='Location'>@3,X=468@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt7</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
            ";
#endregion rendereMethod
            questionType.RenderMethod = renderMethod;
            SmartCadDatabase.UpdateObject<QuestionData>(questionType);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionType;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //Asocio las preguntas genericas para cada robo
            QuestionData question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("QuarrelShort");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("SuspectQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);


            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("WeaponsQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("PersonNameOrInformationShort");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            
            SmartCadDatabase.SaveObject<IncidentTypeData>(incidentType);
            #endregion

            #region abusosexual
            //Paso 1
            incidentType = new IncidentTypeData();
            incidentType.CustomCode = "ASX";
            incidentType.Name = "AbusoSexual";
            incidentType.FriendlyName = ResourceInitialDataLoader.GetString2("SexualAbuse");
            incidentType.NoEmergency = false;

            //Paso 2
            //Bomberos
            dpFire = new DepartmentTypeData(DPPOLICE);
            dpFire = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFire);

            //Bomberos (Atenci�n pre-hospitalaria)
            dpFirePre = new DepartmentTypeData(DPFIREPRE);
            dpFirePre = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFirePre);



            incidentType.SetIncidentTypeDepartmentTypes = new HashedSet();

            itdFire = new IncidentTypeDepartmentTypeData();
            itdFire.DepartmentType = dpFire;
            itdFire.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFire);

            itdFirePre = new IncidentTypeDepartmentTypeData();
            itdFirePre.DepartmentType = dpFirePre;
            itdFirePre.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFirePre);


            incidentType.Procedure = @"<b>" + ResourceInitialDataLoader.GetString2("ProceduresTo") + incidentType.FriendlyName + @".</b> <ul> <li>" + ResourceInitialDataLoader.GetString2("Step1") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step2") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step3") + @"</li> </ul> ";



            //Paso 4

            //Indico las preguntas genericas para el incidente
            incidentType.SetIncidentTypeQuestions = new HashedSet();

            questionWhere = new QuestionData();
            questionWhere.ShortText = ResourceInitialDataLoader.GetString2(STRQUESTIONWHERE);
            questionWhere = SmartCadDatabase.SearchObject<QuestionData>(questionWhere);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionWhere;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            questionNumber = new QuestionData();
            questionNumber.ShortText = ResourceInitialDataLoader.GetString2(STRQUESTIONNUMBERPERSON);
            questionNumber = SmartCadDatabase.SearchObject<QuestionData>(questionNumber);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionNumber;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);


            //Preguntas especificas
            questionType = new QuestionData();
            questionType.Text = ResourceInitialDataLoader.GetString2("AbuseType");
            questionType.ShortText = ResourceInitialDataLoader.GetString2("AbuseTypeShort");
            questionType.CustomCode = incidentType.CustomCode + "TYPE";
            questionType.RequirementType = RequirementTypeEnum.Recommended;
            questionType.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = questionType;
            answer1.Description = ResourceInitialDataLoader.GetString2("Woman");
            answer1.ControlName = "Chckdt4";
            questionType.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = questionType;
            answer2.Description = ResourceInitialDataLoader.GetString2("Minor");
            answer2.ControlName = "Chckdt5";
            questionType.SetPossibleAnswers.Add(answer2);


            SmartCadDatabase.SaveObject<QuestionData>(questionType);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Seleccion multiple' itemName='item1' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt4' />
  <Control itemText='Seleccion multiple' itemName='item2' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt5' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='3'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=312@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Seleccion multiple</property>
      <property name='MinSize'>@3,Width=119@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Seleccion multiple</property>
      <property name='TextSize'>@2,Width=83@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt4</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=313@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Seleccion multiple</property>
      <property name='MinSize'>@3,Width=119@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Seleccion multiple</property>
      <property name='TextSize'>@2,Width=83@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@3,X=312@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt5</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
            ";
            #endregion renderMethod
            questionType.RenderMethod = renderMethod;
            SmartCadDatabase.UpdateObject<QuestionData>(questionType);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionType;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //Asocio las preguntas genericas para cada 
            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("ViolationQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);


            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("OneSuspectQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);
            
            SmartCadDatabase.SaveObject<IncidentTypeData>(incidentType);
            #endregion

            #region rebellion
            //Paso 1
            incidentType = new IncidentTypeData();
            incidentType.CustomCode = "REB";
            incidentType.Name = "rebellion";
            incidentType.FriendlyName = ResourceInitialDataLoader.GetString2("Mutineers");
            incidentType.NoEmergency = false;

            //Paso 2
            //Bomberos
            dpFire = new DepartmentTypeData(DPPOLICE);
            dpFire = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFire);

            //Bomberos (Atenci�n pre-hospitalaria)
            dpFirePre = new DepartmentTypeData(DPFIREPRE);
            dpFirePre = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFirePre);



            incidentType.SetIncidentTypeDepartmentTypes = new HashedSet();

            itdFire = new IncidentTypeDepartmentTypeData();
            itdFire.DepartmentType = dpFire;
            itdFire.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFire);

            itdFirePre = new IncidentTypeDepartmentTypeData();
            itdFirePre.DepartmentType = dpFirePre;
            itdFirePre.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFirePre);


            incidentType.Procedure = @"<b>" + ResourceInitialDataLoader.GetString2("ProceduresTo") + incidentType.FriendlyName + @".</b> <ul> <li>" + ResourceInitialDataLoader.GetString2("Step1") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step2") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step3") + @"</li> </ul> ";



            //Paso 4

            //Indico las preguntas genericas para el incidente
            incidentType.SetIncidentTypeQuestions = new HashedSet();

            questionWhere = new QuestionData();
            questionWhere.ShortText = ResourceInitialDataLoader.GetString2(STRQUESTIONWHERE);
            questionWhere = SmartCadDatabase.SearchObject<QuestionData>(questionWhere);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionWhere;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            questionNumber = new QuestionData();
            questionNumber.ShortText = ResourceInitialDataLoader.GetString2(STRQUESTIONNUMBERPERSON);
            questionNumber = SmartCadDatabase.SearchObject<QuestionData>(questionNumber);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionNumber;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //Asocio las preguntas genericas para cada robo
            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("MutineersQuantityShort");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("AvailablesWeaponsShort");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);
            
            SmartCadDatabase.SaveObject<IncidentTypeData>(incidentType);
            #endregion
        }

        public static void CreateBombIncident()
        {
            //Este metodo crea la carga inicial para todos los tipos de incidentes de bombas
            //Para agregar un tipo de incidente debo realizar los siguientes pasos
            //1.- Crear el incidente.
            //2.- Asociarle los tipos de departamentos que dan soporte a ese incidente
            //3.- Indicar los tipos de procedimientos asociados al incidente
            //4.- Indicar los tipos de preguntas asociadas al incidente con la siguiente estructura
            //4.1.-     Indicar un procedimiento para cada pregunta
            //4.2.-     Indicar las posible respuesta asociadas a la pregunta
            //4.3.-     Indicar la forma en que se renderiza la pregunta en el frontclient indicando los id de las respuestas asociadas
            #region bomb
            //Paso 1
            IncidentTypeData incidentType = new IncidentTypeData();
            incidentType.CustomCode = "BOM";
            incidentType.Name = "BOMB";
            incidentType.FriendlyName = ResourceInitialDataLoader.GetString2("BombThreat");
            incidentType.NoEmergency = false;

            //Paso 2
            //Policia
            DepartmentTypeData dpPolice = new DepartmentTypeData(DPPOLICE);
            dpPolice = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpPolice);

            //Bomberos Emergencias
            DepartmentTypeData dpFirePre = new DepartmentTypeData(DPCICPC);
            dpFirePre = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFirePre);

            //Bomberos Emergencias
            
            DepartmentTypeData dpFire = new DepartmentTypeData(DPFIRE);                        
            dpFire = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFire);



            incidentType.SetIncidentTypeDepartmentTypes = new HashedSet();

            IncidentTypeDepartmentTypeData itdFire = new IncidentTypeDepartmentTypeData();
            itdFire.DepartmentType = dpFire;
            itdFire.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFire);

            IncidentTypeDepartmentTypeData itdFirePre = new IncidentTypeDepartmentTypeData();
            itdFirePre.DepartmentType = dpFirePre;
            itdFirePre.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFirePre);

            IncidentTypeDepartmentTypeData itdPolice = new IncidentTypeDepartmentTypeData();
            itdPolice.DepartmentType = dpPolice;
            itdPolice.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdPolice);

            incidentType.Procedure = @"<b>" + ResourceInitialDataLoader.GetString2("ProceduresTo") + incidentType.FriendlyName + @".</b> <ul> <li>" + ResourceInitialDataLoader.GetString2("Step1") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step2") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step3") + @"</li> </ul> ";



            //Paso 4

            //Indico las preguntas genericas para el incidente
            incidentType.SetIncidentTypeQuestions = new HashedSet();

            QuestionData questionWhere = new QuestionData();
            questionWhere.ShortText = ResourceInitialDataLoader.GetString2(STRQUESTIONWHERE);
            questionWhere = SmartCadDatabase.SearchObject<QuestionData>(questionWhere);


            IncidentTypeQuestionData itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionWhere;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            QuestionData questionNumber = new QuestionData();
            questionNumber.ShortText = ResourceInitialDataLoader.GetString2(STRQUESTIONNUMBERPERSON);
            questionNumber = SmartCadDatabase.SearchObject<QuestionData>(questionNumber);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionNumber;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);


            SmartCadDatabase.SaveObject<IncidentTypeData>(incidentType);
            #endregion
        }

    }
}
