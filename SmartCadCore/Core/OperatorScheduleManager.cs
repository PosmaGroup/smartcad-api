using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using System.Collections;
using SmartCadCore;
using SmartCadCore.Core;
using SmartCadCore.ClientData;
using SmartCadCore.Common;

namespace SmartCadCore.Core
{
    public static class OperatorScheduleManager
    {

        #region Private Methods

        /// <summary>
        /// Get the list of worktime schedule that has not finished.
        /// </summary>
        /// <param name="operCode">Operator Code</param>
        /// <returns>List of schedule that has not finished.</returns>
        public static IList GetWorkTimeByOperator(int operCode)
        {
            IList result = GetWorkShiftSchedulesVariationsByOperator(operCode, false, false);
            return result;
        }

        /// <summary>
        /// Get the list of timeoff schedule that has not finished.
        /// </summary>
        /// <param name="operCode">Operator code</param>
        /// <returns>List of schedule that has not finished.</returns>
        public static IList GetTimeOffByOperator(int operCode)
        {
            IList result = GetWorkShiftSchedulesVariationsByOperator(operCode, true, false);
            return result;
        }

        /// <summary>
        /// Get the workshifts schedules by operator that end after the current date.
        /// </summary>
        /// <param name="operCode">Operator Code</param>
        /// <param name="isTimeOff">Indicates if the work shift type is vacations</param>
        /// <param name="returnData">Indicate if the data type to return is ObjectData</param>
        /// <returns>List of workshifts schedules by operator.</returns>
        private static IList GetWorkShiftSchedulesVariationsByOperator(int operCode, bool isTimeOff, bool returnData)
        {
            ArrayList schedules = new ArrayList();
            string HQL = string.Empty;
            IList list = new ArrayList();
            
            if (isTimeOff == true)
                HQL = SmartCadHqls.GetCustomHql(
                    SmartCadHqls.GetWorkShiftSchedulesTimeOffByOperatorCodeAndEndDate, 
                    operCode, 
                    (int)WorkShiftVariationClientData.WorkShiftType.TimeOff,
                    ApplicationUtil.GetNHibernateFormattedDate(DateTime.Now));
            else
                HQL = SmartCadHqls.GetCustomHql(
                    SmartCadHqls.GetWorkShiftSchedulesWorkTimeByOperatorCodeAndEndDate, 
                    operCode,
                    (int)WorkShiftVariationClientData.WorkShiftType.TimeOff,
                    ApplicationUtil.GetNHibernateFormattedDate(DateTime.Now));
        
            
            list = SmartCadDatabase.SearchObjects(HQL);
            if (list != null && list.Count > 0)
            {
                if (returnData == true)
                {
                    schedules.AddRange(list);
                }
                else
                {
                    foreach (WorkShiftScheduleVariationData sc in list)
                    {
                        schedules.Add(WorkShiftScheduleVariationConversion.ToClient(sc, UserApplicationData.Supervision));
                    }
                }
            }
            return schedules;
        }

        /// <summary>
        /// Get the current schedule by operator.
        /// Should be nice to delete the DateTime date parameter or change the name of the method.
        /// </summary>
        /// <param name="oper">Operator Code</param>
        /// <param name="date">This is not used anymore.</param>
        /// <returns>Current work shift used by the operator.</returns>
        public static WorkShiftScheduleVariationData GetCurrentWorkShiftSchedulesByOperator(int oper, DateTime date)
        {
            DateTime now = SmartCadDatabase.GetTimeFromBD();
            WorkShiftScheduleVariationData currentWss = null;

            WorkShiftScheduleVariationData wssv = GetSchedulesVariationsByOperator(oper, now);
            if (wssv != null)
            {
                currentWss = new WorkShiftScheduleVariationData();
                currentWss.Start = wssv.Start.Value;
                currentWss.End = wssv.End.Value;
            }

            return currentWss;
        }

        /// <summary>
        /// Return the schedule by operator (work shift or extra time). 
        /// </summary>
        /// <param name="oper">Operator Code</param>
        /// <param name="date">Date to search for the schedule.</param>
        /// <returns>Return the Schedule for the operator and date searched.</returns>
        private static WorkShiftScheduleVariationData GetSchedulesVariationsByOperator(int oper, DateTime date)
        {
            WorkShiftScheduleVariationData currentWss = null;
            currentWss = (WorkShiftScheduleVariationData)SmartCadDatabase.SearchObjectData(
                SmartCadHqls.GetCustomHql(
                SmartCadHqls.GetWorkShiftScheduleWorkTimeByOperatorCodeSpecificDateTime,
                oper, (int)WorkShiftVariationClientData.WorkShiftType.TimeOff, ApplicationUtil.GetDataBaseFormattedDate(date)));

            return currentWss;
        }

        /// <summary>
        /// Get the schedules for the current date.
        /// </summary>
        /// <param name="operCode">Operator code</param>
        /// <param name="isTimeOff">Indicates if the schedule is work time or vacation</param>
        /// <returns>List of schedules for the current date.</returns>
        private static IList GetWorkShiftSchedulesVariationsTodayByOperator(int operCode, bool isTimeOff)
        {
            DateTime start = DateTime.Now.Date;
            DateTime end = start.AddDays(1);
            ArrayList schedules = new ArrayList();
            string HQL = string.Empty;
            IList list = new ArrayList();

            if (isTimeOff == true)
                HQL = SmartCadHqls.GetCustomHql(
                    SmartCadHqls.GetWorkShiftSchedulesTimeOffByOperatorCodeAndDate,
                    operCode,
                    (int)WorkShiftVariationClientData.WorkShiftType.TimeOff,
                    ApplicationUtil.GetNHibernateFormattedDate(start),
                    ApplicationUtil.GetNHibernateFormattedDate(end));
            else
                HQL = SmartCadHqls.GetCustomHql(
                    SmartCadHqls.GetWorkShiftSchedulesWorkTimeByOperatorCodeAndDate,
                    operCode,
                    (int)WorkShiftVariationClientData.WorkShiftType.TimeOff,
                    ApplicationUtil.GetNHibernateFormattedDate(start),
                    ApplicationUtil.GetNHibernateFormattedDate(end));

            list = SmartCadDatabase.SearchObjects(HQL);
            if (list != null && list.Count > 0)
            {
                schedules.AddRange(list);
            }
            return schedules;
        }

        /// <summary>
        /// Get the schedules for the current date.
        /// </summary>
        /// <param name="operCode">Operator code list</param>
        /// <param name="isTimeOff">Indicates if the schedule is work time or vacation</param>
        /// <returns>List of schedules for the current date.</returns>
        private static Dictionary<string, IList> GetWorkShiftSchedulesVariationsTodayByOperators(IList opersCode, bool isTimeOff)
        {
            DateTime start = DateTime.Now.Date;
            DateTime end = start.AddDays(1);
            string HQL = string.Empty;
            Dictionary<string, IList> list = null;
            Dictionary<string, int> objectsIndex = new Dictionary<string, int>();
            using (var session = SmartCadDatabase.NewSession())
            {
                NHibernate.IMultiQuery multiQuery = session.CreateMultiQuery();
                int index = 0;
                foreach (int code in opersCode)
                {
                    if (isTimeOff == true)
                        HQL = SmartCadHqls.GetCustomHql(
                            SmartCadHqls.GetWorkShiftSchedulesTimeOffByOperatorCodeAndDate,
                            code,
                            (int)WorkShiftVariationClientData.WorkShiftType.TimeOff,
                            ApplicationUtil.GetNHibernateFormattedDate(start),
                            ApplicationUtil.GetNHibernateFormattedDate(end));
                    else
                        HQL = SmartCadHqls.GetCustomHql(
                            SmartCadHqls.GetWorkShiftSchedulesWorkTimeByOperatorCodeAndDate,
                            code,
                            (int)WorkShiftVariationClientData.WorkShiftType.TimeOff,
                            ApplicationUtil.GetNHibernateFormattedDate(start),
                            ApplicationUtil.GetNHibernateFormattedDate(end));

                    multiQuery.Add(session.CreateQuery(HQL));

                    objectsIndex.Add(code.ToString(), index);
                }
                list = SmartCadDatabase.SearchObjects(session, multiQuery, objectsIndex, UserApplicationData.Supervision, false);
            }
            return list;
        }

        private static double CalculateAmountWorkingSeconds(IList freeTimes, IList workTimes)
        {
            double result = 0;
            TimeSpan timeNow = DateTime.Now.TimeOfDay;

            if (workTimes != null)
            {
                foreach (WorkShiftScheduleVariationData wsvs in workTimes)
                {
                    TimeSpan workHours = TimeSpan.Zero;
                    if (((DateTime)wsvs.End).TimeOfDay < timeNow)
                    {
                        workHours = ((DateTime)wsvs.End).TimeOfDay.Subtract(((DateTime)wsvs.Start).TimeOfDay);
                    }
                    else if (((DateTime)wsvs.Start).TimeOfDay < timeNow)
                    {
                        workHours = timeNow.Subtract(((DateTime)wsvs.Start).TimeOfDay);
                    }
                    result += workHours.TotalSeconds;
                }
            }

            if (freeTimes != null)
            {
                foreach (WorkShiftScheduleVariationData wsvs in freeTimes)
                {
                    TimeSpan workHours = TimeSpan.Zero;
                    if (((DateTime)wsvs.End).TimeOfDay < timeNow)
                    {
                        workHours = ((DateTime)wsvs.End).TimeOfDay.Subtract(((DateTime)wsvs.Start).TimeOfDay);
                    }
                    else if (((DateTime)wsvs.Start).TimeOfDay < timeNow)
                    {
                        workHours = timeNow.Subtract(((DateTime)wsvs.Start).TimeOfDay);
                    }
                    result -= workHours.TotalSeconds;
                }
            }
            return result;
        }


        private static double CalculateTotalTimeScheduledWorkingToday(IList freeTimes, IList workTimes)
        {
            double result = 0;
          
            if (workTimes != null)
            {
                foreach (WorkShiftScheduleVariationData wsvs in workTimes)
                {
                  
                    result += ((DateTime)wsvs.End).Subtract(((DateTime)wsvs.Start)).TotalSeconds;
                }
            }
            if (freeTimes != null)
            {
                foreach (WorkShiftScheduleVariationData wsvs in freeTimes)
                {
                   result -= ((DateTime)wsvs.End).Subtract(((DateTime)wsvs.Start)).TotalSeconds;
                }
            }

            
            return result;
        }

        
        #endregion

        /// <summary>
        /// Check that the operator is should be working now.
        /// </summary>
        /// <param name="operatorCode">Operator code</param>
        /// <returns>True if it is working, otherwise false.</returns>
        public static bool IsWorkingNow(int operatorCode)
        {
            DateTime currentDateTime = SmartCadDatabase.GetTimeFromBD();
            return IsWorkingNow(operatorCode, currentDateTime);
        }
        /// <summary>
        /// Check that the operator is working now.
        /// </summary>
        /// <param name="operatorCode">Operator code</param>
        /// <param name="currentVariationTime">Time to check for work shifts (Work Shift, Extra time or Vacation).</param>
        /// <returns>True if it is working, otherwise false.</returns>
        public static bool IsWorkingNow(int operatorCode, DateTime currentTime)
        {
            string HQL_WT = SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorWorkShiftVariationsNowByOperatorCode, operatorCode, (int)WorkShiftVariationData.WorkShiftType.TimeOff, ApplicationUtil.GetNHibernateFormattedDate(currentTime));
            object obj = new object();
            lock (obj)
            {
                IList schedules = SmartCadDatabase.SearchObjects(HQL_WT);
                if (schedules.Count > 0)
                    return CheckVacationByOperator(operatorCode, currentTime) == false;
            }
            return false;
        }

        /// <summary>
        /// Check if an operator is on vacation.
        /// </summary>
        /// <param name="operatorCode">Operator code</param>
        /// <param name="currentTime">DateTime to check</param>
        /// <returns>True if it is on vacation, otherwise false</returns>
        public static bool CheckVacationByOperator(int operatorCode, DateTime currentTime)
        {
            string HQL_FT = SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorTimeOffNowByOperatorCode, operatorCode, (int)WorkShiftVariationData.WorkShiftType.TimeOff, ApplicationUtil.GetNHibernateFormattedDate(currentTime));
            IList freeTimes = SmartCadDatabase.SearchObjects(HQL_FT);
            if (freeTimes.Count == 0)
            {
                return false;
            }
            return true;
        }

        public static IList GetOperatorsBySupervisor(int supervisorCode)
        {
            DateTime currentDateTime = SmartCadDatabase.GetTimeFromBD();
           
            IList operators = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorsForSupervisor, supervisorCode,
                currentDateTime.ToString(ApplicationUtil.DataBaseFormattedDate)));

            return operators;
        }

        public static IList GetAllOperatorsWorkingNow(bool isSupervisor)
        {
            ArrayList result = new ArrayList(OperatorScheduleManager.GetOperatorsWorkingNowByApplication(UserApplicationData.FirstLevel.Name, isSupervisor));
            result.AddRange(OperatorScheduleManager.GetOperatorsWorkingNowByApplication(UserApplicationData.Dispatch.Name, isSupervisor));
            return result;
        }

        public static IList GetAllOperatorsNotWorkingNow()
        {
            ArrayList result = new ArrayList(OperatorScheduleManager.GetOperatorsNotWorkingNowByApplication(UserApplicationData.FirstLevel.Name));
            result.AddRange(OperatorScheduleManager.GetOperatorsNotWorkingNowByApplication(UserApplicationData.Dispatch.Name));
            return result;
        }

        public static IList GetOperatorsWorkingNowByApplication(string applicationName, bool isSupervisor)
        {
            string HQL_OPERATORS = SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorsSchedulesNow, applicationName);

            IList result = new ArrayList();
            IList operators = (IList)SmartCadDatabase.SearchBasicObjects(HQL_OPERATORS);
            foreach (int oper in operators)
            {
                if (IsWorkingNow(oper) == true && result.Contains(oper) == false && IsSupervisor(oper) == isSupervisor)
                    result.Add(oper);
            }
            return result;
        }

        public static IList GetOperatorsNotWorkingNowByApplication(string applicationName)
        {
            string HQL_OPERATORS = SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorsSchedulesNow, applicationName);
            IList result = new ArrayList();
            IList operators = (IList)SmartCadDatabase.SearchBasicObjects(HQL_OPERATORS);
            foreach (int operatorCode in operators)
            {
                if (IsWorkingNow(operatorCode) == false && IsSupervisor(operatorCode) == false)
                {
                    OperatorData userAccount = new OperatorData();
                    userAccount.Code = operatorCode;
                    userAccount = SmartCadDatabase.RefreshObject(userAccount) as OperatorData;
                    if (userAccount != null && result.Contains(userAccount) == false)
                    {
                        result.Add(userAccount);
                    }
                }
            }
            return result;
        }

        public static IList GetOperatorsWorkingNowBySupervisor(int supervisorCode)
        {
            IList result = new ArrayList();
            IList operators = GetOperatorsBySupervisor(supervisorCode);
            foreach (OperatorData oper in operators)
            {
                if (IsWorkingNow(oper.Code) == true && result.Contains(oper) == false)
                    result.Add(oper);               
            }
            return result;
        }

        public static IList GetOperatorsWorkingNowByApp(string applicationName)
        {
            IList result = new ArrayList();
            IList operators = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorsByApplication,applicationName));
            foreach (OperatorData oper in operators)
            {
                if (IsWorkingNow(oper.Code) == true)
                    result.Add(oper);
            }
            return result;
        }

        public static IList GetOperatorsWorkingNowByAppByWorkShift(string applicationName, int wsCode)
        {
            IList result = new ArrayList();
            IList operators = SmartCadDatabase.SearchObjects(
                SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorsByApplicationByWorkShift, applicationName, wsCode));
            foreach (OperatorData oper in operators)
            {
                if (IsWorkingNow(oper.Code) == true)
                    result.Add(oper);
            }
            return result;
        }

        public static IList GetOperatorsWorkingNowByDepartament(int departmentCode)
        {
            IList result = new ArrayList();
            IList operators = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetDispatchOperatorsByDeparment, departmentCode));
            foreach (OperatorData oper in operators)
            {
                if (IsWorkingNow(oper.Code) == true)
                    result.Add(oper);
            }
            return result;
        }
       
        /// <summary>
        /// Obtains a list of start-end dates for the operator in the current date.
        /// </summary>
        /// <param name="oper"></param>
        /// <returns>List of start-end dates</returns>
        public static ICollection GetOperatorTodaysSchedules(IList operators)
        {
            Dictionary<int, ArrayList> results = new Dictionary<int, ArrayList>();
            Dictionary<string, IList> freeTimes = GetWorkShiftSchedulesVariationsTodayByOperators(operators, true);
            Dictionary<string, IList> schedules = GetWorkShiftSchedulesVariationsTodayByOperators(operators, false);
            foreach (int code in operators)
            {
                ArrayList dates = new ArrayList();
                ArrayList starts = new ArrayList();
                ArrayList ends = new ArrayList();
                foreach (WorkShiftScheduleVariationData schedule in schedules[code.ToString()])
                {
                    bool added = false;
                    foreach (WorkShiftScheduleVariationData scheduleVar in freeTimes[code.ToString()])
                    {
                        if (scheduleVar.Start.Value >= schedule.Start.Value && scheduleVar.Start.Value <= schedule.End.Value)
                        {
                            starts.Add((schedule.Start.Value > scheduleVar.Start.Value) ? scheduleVar.End.Value : schedule.Start.Value);
                            ends.Add((schedule.Start.Value > scheduleVar.Start.Value) ? schedule.End.Value : scheduleVar.Start.Value);
                            added = true;
                        }
                        if (scheduleVar.End.Value >= schedule.Start.Value && scheduleVar.End.Value <= schedule.End.Value)
                        {
                            starts.Add((schedule.Start.Value > scheduleVar.Start.Value) ? scheduleVar.End.Value : schedule.Start.Value);
                            ends.Add((schedule.Start.Value > scheduleVar.Start.Value) ? schedule.End.Value : scheduleVar.Start.Value);
                            added = true;
                        }
                    }
                    if (!added)
                    {
                        starts.Add(schedule.Start);
                        ends.Add(schedule.End);
                    }
                }

                dates.Add(starts);
                dates.Add(ends);
                results.Add(code, dates);
            }
            return results;
        }

        public static double GetOperatorTodaysWorkingTimeScheduled(int operatorCode)
        {
            IList freeTimes = GetWorkShiftSchedulesVariationsTodayByOperator(operatorCode, true);
            IList workTimes = GetWorkShiftSchedulesVariationsTodayByOperator(operatorCode, false);

            return CalculateTotalTimeScheduledWorkingToday(freeTimes, workTimes);
        }

		public static double GetOperatorAssignsTotalTime(int operatorCode)
		{
			OperatorData oper = new OperatorData();
			oper.Code = operatorCode;
			oper = ((OperatorData)SmartCadDatabase.RefreshObject(oper));
			if (SmartCadDatabase.IsInitialize(oper.Supervisors) == false)
				SmartCadDatabase.InitializeLazy(oper, oper.Supervisors);
			double result = 0.0;
			foreach (OperatorAssignData opAss in oper.Supervisors)
			{
				if(opAss.SupervisedScheduleVariation == null)
					result += opAss.EndDate.Subtract(opAss.StartDate).TotalMilliseconds;
			}
			return result;
		}

        public static double GetOperatorAssignsTotalTime(int scheduleCode, OperatorClientData oper) 
        {
            double result = 0.0;
            foreach (OperatorAssignClientData opAss in oper.Supervisors)
                if (opAss.SupervisedScheduleVariation != null && opAss.SupervisedScheduleVariation.Code == scheduleCode)
                    result += opAss.EndDate.Subtract(opAss.StartDate).TotalMilliseconds;

            return result;        
        }

        /// <summary>
        /// Get current supervisor code, if the operator does not has supervisor for the current date return 0;
        /// </summary>
        /// <param name="operatorCode">Operator code</param>
        /// <param name="application">Application in which to check for the supervisor.</param>
        /// <returns>Supervisor code if it has, otherwise 0</returns>
        public static int GetSupervisorCode(int operatorCode, UserApplicationData application, DateTime dateTime)
        {
            int operatorData = 0;
            object result = SmartCadDatabase.SearchBasicObject(
                SmartCadHqls.GetCustomHql(SmartCadHqls.GetCurrentSupervisorsCodeByOperatorByApplication,
                operatorCode, dateTime.ToString(ApplicationUtil.DataBaseFormattedDate), application.Name));
            
			if (result != null)
                operatorData = (int)result;

            return operatorData;
        }

        public static OperatorData GetCurrentSupervisor(int userCode, UserApplicationData application)
        {
            OperatorData currentSupervisor = null;
            DateTime currentDateTime = SmartCadDatabase.GetTimeFromBD();
           
            IList supervisors = (IList)SmartCadDatabase.SearchObjects(
                SmartCadHqls.GetCustomHql(SmartCadHqls.GetCurrentSupervisorsByOperatorByApplication,
                userCode, currentDateTime.ToString(ApplicationUtil.DataBaseFormattedDate), application.Name));

            for (int i = 0; i < supervisors.Count && currentSupervisor == null; i++)
            {
                OperatorData temp = (OperatorData)supervisors[i];
                if (IsWorkingNow(temp.Code))
                    currentSupervisor = temp;
            }

            return currentSupervisor;
        }


		public static bool IsSupervisor(int operatorCode)
        {
            bool result = false;
            try
            {
                long accessCount = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(SmartCadHqls.IsSupervisor, operatorCode,
                    "FirstLevelSupervisorName", "DispatchSupervisorName"));
                
				if (accessCount >= 1)
                    result = true;
            }
            catch
            {
            }
            return result;
        }

        public static string GetCurrentWorkShiftName(int operatorCode)
        {
            DateTime now = SmartCadDatabase.GetTimeFromBD();

            string ws = string.Empty;
				
            object result = SmartCadDatabase.SearchBasicObject(
					SmartCadHqls.GetCustomHql(
						SmartCadHqls.GetCurrentWorkShiftNameByOperatorCode, 
						operatorCode, 
						(int)WorkShiftVariationData.WorkShiftType.TimeOff, 
						ApplicationUtil.GetDataBaseFormattedDate(now)));

            if (result != null)
                ws = result.ToString();

            return ws;
        }

        public static void ExistCollisionInDB(IList operators, WorkShiftVariationData wsv)
        {
            string HQL = string.Empty;
            IList result;
            if (SmartCadDatabase.IsInitialize(wsv.Schedules) == false)
                SmartCadDatabase.InitializeLazy(wsv, wsv.Schedules);

            if (SmartCadDatabase.IsInitialize(wsv.Schedules) == true)
            {
                
                    //Test collisions with anothers variations
                    if (wsv.Type != WorkShiftVariationData.WorkShiftType.TimeOff)
                    {

                        HQL = string.Format(
									@"SELECT operators, schedules 
									FROM WorkShiftVariationData variation left join
                                        variation.Schedules schedules left join                                     
                                        variation.Operators operators
                                        WHERE variation.Code != {0} and variation.Type != {1} and variation.ObjectType = 0 and schedules.End > '{2}' and(",
									wsv.Code,
									(int)WorkShiftVariationData.WorkShiftType.TimeOff,
                                    ApplicationUtil.GetDataBaseFormattedDate(DateTime.Now));


                        foreach (WorkShiftScheduleVariationData schedule in wsv.Schedules)
                        {                            
                                HQL += string.Format(@"((schedules.Start < '{0}' and '{0}' < schedules.End) or 
                                       (schedules.Start < '{1}' and '{1}' < schedules.End) or
                                       ('{0}' < schedules.Start and schedules.Start < '{1}') or
                                       ('{0}' < schedules.End and schedules.End < '{1}') or
                                       ('{0}' = schedules.Start and schedules.End = '{1}')) or",
                                            ApplicationUtil.GetDataBaseFormattedDate((DateTime)schedule.Start),
                                            ApplicationUtil.GetDataBaseFormattedDate((DateTime)schedule.End));

                       }

                       HQL = HQL.Substring(0, HQL.Length - 2) + ") AND operators.Operator.Code in (";

                       foreach (WorkShiftOperatorData oper in operators)
                           HQL += string.Format("{0},", oper.Operator.Code);

                       HQL = HQL.Substring(0, HQL.Length - 1) + ")";
                        
                    }
                    else 
                    {
                        HQL = string.Format(
									@"SELECT operators, schedules 
									FROM WorkShiftVariationData variation left join
										variation.Schedules schedules left join                                     
										variation.Operators operators
										WHERE variation.Code != {0} and variation.Type = {1} and variation.ObjectType = 0 and schedules.End > '{2}' and (",
									wsv.Code,
                                    (int)WorkShiftVariationData.WorkShiftType.TimeOff,
                                    ApplicationUtil.GetDataBaseFormattedDate(DateTime.Now));


                        foreach (WorkShiftScheduleVariationData schedule in wsv.Schedules)
                        {                           
                                HQL += string.Format(@"((schedules.Start < '{0}' and '{0}' < schedules.End) or 
                                       (schedules.Start < '{1}' and '{1}' < schedules.End) or
                                       ('{0}' < schedules.Start and schedules.Start < '{1}') or
                                       ('{0}' < schedules.End and schedules.End < '{1}') or
                                       ('{0}' = schedules.Start and schedules.End = '{1}')) or",
                                            ApplicationUtil.GetDataBaseFormattedDate((DateTime)schedule.Start),
                                            ApplicationUtil.GetDataBaseFormattedDate((DateTime)schedule.End));
                        }

                        HQL = HQL.Substring(0, HQL.Length - 2) + ") AND operators.Operator.Code in (";

                        foreach (WorkShiftOperatorData oper in operators)
                            HQL += string.Format("{0},", oper.Operator.Code);

                        HQL = HQL.Substring(0, HQL.Length - 1) + ")";
                    
                    }

                    result = (IList)SmartCadDatabase.SearchBasicObjects(HQL);
                  
                    if (result != null && result.Count > 0)
                    {
                        if (wsv.Type != WorkShiftVariationData.WorkShiftType.TimeOff)
                        {
                            throw new DatabaseObjectException(wsv,
                                string.Format(ResourceLoader.GetString2("CreateWSVwithDateRepeat"), ((WorkShiftOperatorData)((object[])result[0])[0]).Operator.FirstName + " " + ((WorkShiftOperatorData)((object[])result[0])[0]).Operator.LastName, ((WorkShiftScheduleVariationData)((object[])result[0])[1]).Start.Value.ToString("MM/dd/yyyy")));
                        }
                        else if (wsv.Type == WorkShiftVariationData.WorkShiftType.TimeOff)
                        {
                            throw new DatabaseObjectException(wsv,
                                string.Format(ResourceLoader.GetString2("CreateWSVwithVacationDateRepeat"), ((WorkShiftOperatorData)((object[])result[0])[0]).Operator.FirstName + " " + ((WorkShiftOperatorData)((object[])result[0])[0]).Operator.LastName, ((WorkShiftScheduleVariationData)((object[])result[0])[1]).Start.Value.ToString("MM/dd/yyyy")));
                        }
                    }
                
            }
        }

        public static bool CollideWSRoute(int newWSCode, IList<int> wsList)
        {
            IList newWSSchadules = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetSchedulesByWsCode, newWSCode));
            
            ////GET ALL PREVIOUS SCHEDULES 
            string HQL = string.Format(
                                    @"SELECT schedules 
									FROM WorkShiftVariationData variation left join
										variation.Schedules schedules
										WHERE ");
            foreach (int wsCode in wsList)
            {
                HQL += string.Format(@" variation.Code = {0} or", wsCode);
            }
            HQL = HQL.Substring(0, HQL.Length - 2);

            IList schedules = (IList)SmartCadDatabase.SearchObjects(HQL);

            /// CHECK COLLISIONS
            foreach (WorkShiftScheduleVariationData newschedule in newWSSchadules)
            {
                DateTime start = newschedule.Start.Value;
                DateTime end = newschedule.End.Value;

                foreach (WorkShiftScheduleVariationData schedule in schedules)
                {
                    if ((schedule.Start.Value < start && start < schedule.End.Value) ||
                        (schedule.Start.Value < end && end < schedule.End.Value) ||
                        (start < schedule.Start.Value && schedule.Start.Value < end) ||
                        (start < schedule.End.Value && schedule.End.Value < end) ||
                        (start == schedule.Start.Value && schedule.End.Value == end))
                    {
                        return true;
                    }
                }
            }
            return false;
        }



    }
}
