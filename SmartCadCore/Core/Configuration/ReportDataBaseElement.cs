
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Core.Configuration
{
    class ReportDataBaseElement: ConfigurationElement
    {

        [ConfigurationProperty("databaseServer", IsRequired = true, IsKey = true)]
        public string DataBaseServer
        {
            get
            {
                return this["databaseServer"] as string;
            }
            set
            {
                this["databaseServer"] = value;
            }
        }

        [ConfigurationProperty("databaseName", IsRequired = true, IsKey = true)]
        public string DataBaseName
        {
            get
            {
                return this["databaseName"] as string;
            }
            set
            {
                this["databaseName"] = value;
            }
        }

    }
}
