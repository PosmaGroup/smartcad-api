using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace SmartCadCore.Core
{
    #region Class InternalServerAddressElement Documentation
    /// <summary>
    /// TODO:
    /// </summary>
    /// <className>InternalServerAddressElement</className>
    /// <author email="atorres@smartmatic.com">Julio Ynojosa</author>
    /// <date>2007/02/15</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    public class InternalServerAddressElement : ConfigurationElement
    {
        public InternalServerAddressElement()
        {
        }

        [ConfigurationProperty("committedChangesSleep")]
        public int CommittedChangedSleep
        {
            get
            {
                return (int)this["committedChangesSleep"];
            }
            set
            {
                this["committedChangesSleep"] = value;
            }
        }
    }
}