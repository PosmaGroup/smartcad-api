using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace SmartCadCore.Core
{
    public class ReportsElement : ConfigurationElement
    {
        public ReportsElement()
        {
        }

        [ConfigurationProperty("databaseServer")]
        public string DatabaseServer
        {
            get
            {
                return (string)this["databaseServer"];
            }
            set
            {
                this["databaseServer"] = value;
            }
        }

        [ConfigurationProperty("databaseName")]
        public string DatabaseName
        {
            get
            {
                return (string)this["databaseName"];
            }
            set
            {
                this["databaseName"] = value;
            }
        }
    }
}
