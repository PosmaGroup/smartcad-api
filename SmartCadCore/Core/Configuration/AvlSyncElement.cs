using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace SmartCadCore.Core
{
    public class AvlSyncElement : ConfigurationElement
    {
        public AvlSyncElement()
        {
        }

        [ConfigurationProperty("rate")]
        public int Rate
        {
            get
            {
                return (int)this["rate"];
            }
            set
            {
                this["rate"] = value;
            }
        }

        [ConfigurationProperty("maxUnitsUpdated")]
        public int MaxUnitsUpdated
        {
            get
            {
                return (int)this["maxUnitsUpdated"];
            }
            set
            {
                this["maxUnitsUpdated"] = value;
            }
        }
    }
}
