﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace SmartCadCore.Core
{
    public class LayerCollection : ConfigurationElementCollection
    {

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((LayerElement)element).Name;
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new LayerElement();
        }

        public new LayerElement this[string name]
        {
            get
            {
                return BaseGet(name) as LayerElement;
            }
        }

    }
}
