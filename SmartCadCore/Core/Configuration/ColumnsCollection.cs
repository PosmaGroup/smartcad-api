﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace SmartCadCore.Core
{
    public class ColumnsCollection : ConfigurationElementCollection
    {

        protected override ConfigurationElement CreateNewElement()
        {
            return new MapColumnElement();
        }

        public new MapColumnElement this[string name]
        {
            get
            {
                return BaseGet(name) as MapColumnElement;
            }
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((MapColumnElement)element).Name;
        }
    }
}
