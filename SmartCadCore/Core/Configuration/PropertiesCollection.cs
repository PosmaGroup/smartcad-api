using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace SmartCadCore.Core
{
    #region Class PropertiesCollection Documentation
    /// <summary>
    /// TODO:
    /// </summary>
    /// <className>PropertiesCollection</className>
    /// <author email="jynojosa@smartmatic.com">Julio Ynojosa</author>
    /// <date>2006/03/31</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    public class PropertiesCollection : ConfigurationElementCollection
    {
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((PropertyElement)element).Name;
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new PropertyElement();
        }

        public new PropertyElement this[string name]
        {
            get
            {
                return BaseGet(name) as PropertyElement;
            }
        }

        public void Add(PropertyElement property)
        {
            BaseAdd(property);
        }

        public void Clear()
        {
            BaseClear();
        }
    }
}