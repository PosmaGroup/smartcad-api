﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace SmartCadCore.Core
{
    public class TelephonyServerCommunicationElement : ConfigurationElement
    {
        public TelephonyServerCommunicationElement()
        {
        }

        public static string CurrentServerType;

        /// <summary>
        /// This property specify if it is being used genesys, nortel or any other supported pbx.
        /// It would be requiered if this configuration only appeared in server, not in client.
        /// </summary>
        [ConfigurationProperty("serverType", DefaultValue = "Nortel" /*, IsRequired = true*/)]
        public string ServerType
        {
            get
            {
                return this["serverType"] as string;
            }
            set
            {
                this["serverType"] = value;
                CurrentServerType = value;
            }
        }

        [ConfigurationProperty("properties", IsRequired = false)]
        [ConfigurationCollection(typeof(PropertiesCollection), AddItemName = "property")]
        public new PropertiesCollection Properties
        {
            get
            {
                return this["properties"] as PropertiesCollection;
            }
            set
            {
                this["properties"] = value;
            }
        }

        //[ConfigurationProperty("serverPassword")]
        //public string ServerPassword
        //{
        //    get
        //    {
        //        string p = this["serverPassword"] as string;
        //        if (string.IsNullOrEmpty(p) == true)
        //            return string.Empty;
        //        else
        //            return CryptoUtil.DecryptWithRijndael(this["serverPassword"] as string).TrimEnd('\0');
        //    }
        //    set
        //    {
        //        string p = string.Empty;
        //        if (string.IsNullOrEmpty(value) == false)
        //            p = CryptoUtil.EncryptWithRijndael(value);
        //        this["serverPassword"] = p;
        //    }
        //}
    }
}
