using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace SmartCadCore.Core
{
    public class TelephonyElement : ConfigurationElement
    {
        [ConfigurationProperty("extension")]
        public string Extension
        {
            get
            {
                return this["extension"] as string;
            }
            set
            {
                this["extension"] = value;
            }
        }

        [ConfigurationProperty("changeable")]
        public bool Changeable
        {
            get
            {
                return (bool)this["changeable"];
            }
            set
            {
                this["changeable"] = value;
            }
        }

        [ConfigurationProperty("showConsole")]
        public bool ShowConsole
        {
            get
            {
                return (bool)this["showConsole"];
            }
            set
            {
                this["showConsole"] = value;
            }
        }

        [ConfigurationProperty("virtual")]
        public bool Virtual
        {
            get
            {
                return (bool)this["virtual"];
            }
            set
            {
                this["virtual"] = value;
            }
        }
    }
}