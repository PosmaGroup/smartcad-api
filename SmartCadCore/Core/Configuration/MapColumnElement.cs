﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace SmartCadCore.Core
{
    public class MapColumnElement : ConfigurationElement
    {
        public MapColumnElement()
        {
        }

        [ConfigurationProperty("name", IsRequired = true,IsKey = true)]
        public string Name
        {
            get
            {
                return this["name"] as string;
            }
            set
            {
                this["name"] = value;
            }
        }

        [ConfigurationProperty("search", IsRequired = false)]
        public string Search
        {
            get
            {
                return this["search"] as string;
            }
            set
            {
                this["search"] = value;
            }
        }

        [ConfigurationProperty("alias", IsRequired = false)]
        public string Alias
        {
            get
            {
                return this["alias"] as string;
            }
            set
            {
                this["alias"] = value;
            }
        }

        [ConfigurationProperty("selectable", IsRequired = false)]
        public string Selectable
        {
            get
            {
                return this["selectable"] as string;
            }
            set
            {
                this["selectable"] = value;
            }
        }

        [ConfigurationProperty("aszone", IsRequired = false)]
        public string IsZone
        {
            get
            {
                return this["aszone"] as string;
            }
            set
            {
                this["aszone"] = value;
            }
        }

        [ConfigurationProperty("asstreet", IsRequired = false)]
        public string IsStreet
        {
            get
            {
                return this["asstreet"] as string;
            }
            set
            {
                this["asstreet"] = value;
            }
        }
    }
}
