using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace SmartCadCore.Core
{
    #region Class PrintToConsoleElement Documentation
    /// <summary>
    /// TODO:
    /// </summary>
    /// <className>PrintToConsoleElement</className>
    /// <author email="atorres@smartmatic.com">Alden Torres</author>
    /// <date>2007/01/16</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    public class PrintToConsoleElement : ConfigurationElement
    {
        public PrintToConsoleElement()
        {
        }

        [ConfigurationProperty("value")]
        public bool Value
        {
            get
            {
                return (bool)this["value"];
            }
            set
            {
                this["value"] = value;
            }
        }
    }
}
