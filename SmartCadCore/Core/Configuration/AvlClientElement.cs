﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace SmartCadCore.Core
{
    public class AvlClientElement : ConfigurationElement
    {
        public AvlClientElement()
        {
        }

        [ConfigurationProperty("ip")]
        public string IP
        {
            get
            {
                return this["ip"].ToString();
            }
            set
            {
                this["ip"] = value;
            }
        }

        [ConfigurationProperty("port")]
        public int Port
        {
            get
            {
                return (int)this["port"];
            }
            set
            {
                this["port"] = value;
            }
        }
    }
}