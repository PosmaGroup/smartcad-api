using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace SmartCadCore.Core
{
    #region Class PropertyElement Documentation
    /// <summary>
    /// TODO:
    /// </summary>
    /// <className>PropertyElement</className>
    /// <author email="jynojosa@smartmatic.com">Julio Ynojosa</author>
    /// <date>2006/03/31</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    public class PropertyElement : ConfigurationElement
    {
        public PropertyElement()
        {
        }

        [ConfigurationProperty("name", IsRequired = true, IsKey = true)]
        public string Name
        {
            get
            {
                return this["name"] as string;
            }
            set
            {
                this["name"] = value;
            }
        }

        [ConfigurationProperty("value", IsRequired = true)]
        public string Value
        {
            get
            {
                return this["value"] as string;
            }
            set
            {
                this["value"] = value;
            }
        }
    }
}
