using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace SmartCadCore.Core
{
    public class IncidentDetailsElement : ConfigurationElement
    {
        public IncidentDetailsElement()
        {
        }

        [ConfigurationProperty("showHtmlExample")]
        public bool ShowHtmlExample
        {
            get
            {
                return (bool)this["showHtmlExample"];
            }
            set
            {
                this["showHtmlExample"] = value;
            }
        }
    }
}
