﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace SmartCadCore.Core
{
    public class LayerElement : ConfigurationElement
    {
        public LayerElement()
        {
        }

        [ConfigurationProperty("name", IsRequired = true, IsKey = true)]
        public string Name
        {
            get
            {
                return this["name"] as string;
            }
            set
            {
                this["name"] = value;
            }
        }

        [ConfigurationProperty("alias", IsRequired = true)]
        public string Alias
        {
            get
            {
                return this["alias"] as string;
            }
            set
            {
                this["alias"] = value;
            }
        }

        [ConfigurationProperty("search", IsRequired = false)]
        public string Search
        {
            get
            {
                return this["search"] as string;
            }
            set
            {
                this["search"] = value;
            }
        }

        [ConfigurationProperty("selectable", IsRequired = false)]
        public string Selectable
        {
            get
            {
                return this["selectable"] as string;
            }
            set
            {
                this["selectable"] = value;
            }
        }

        [ConfigurationProperty("columns", IsRequired = true)]
        [ConfigurationCollection(typeof(ColumnsCollection), AddItemName = "column")]
        public new ColumnsCollection Columns
        {
            get
            {
                return this["columns"] as ColumnsCollection;
            }
            set
            {
                this["columns"] = value;
            }
        }


    }
}
