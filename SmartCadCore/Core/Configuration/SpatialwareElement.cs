using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace SmartCadCore.Core
{
    public class SpatialwareElement : ConfigurationElement
    {
       /* [ConfigurationProperty("ip")]
        public string IP
        {
            get
            {
                return this["ip"] as string;
            }
            set
            {
                this["ip"] = value;
            }
        }
        
        [ConfigurationProperty("database")]
        public string Database
        {
            get
            {
                return this["database"] as string;
            }
            set
            {
                this["database"] = value;
            }
        }

        [ConfigurationProperty("active")]
        public bool Active
        {
            get
            {
                return (bool)this["active"];
            }
            set
            {
                this["active"] = value;
            }
        }

        [ConfigurationProperty("maxOpenIncidents")]
        public int MaxOpenIncidents
        {
            get
            {
                return (int)this["maxOpenIncidents"];
            }
            set
            {
                this["maxOpenIncidents"] = value;
            }
        }
        */
        [ConfigurationProperty("maxUpdates")]
        public int MaxUpdates
        {
            get
            {
                return (int)this["maxUpdates"];
            }
            set
            {
                this["maxUpdates"] = value;
            }
        }

        [ConfigurationProperty("maxSleepTime")]
        public int MaxSleepTime
        {
            get
            {
                return (int)this["maxSleepTime"];
            }
            set
            {
                this["maxSleepTime"] = value;
            }
        }
    }
}