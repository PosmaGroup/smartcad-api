using System;
using System.Collections.Generic;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Channels;
using System.ServiceModel.Security;
using System.IdentityModel.Policy;
using System.ServiceModel.Configuration;

using SmartCadCore.Core;
using System.Reflection;
using System.ComponentModel;
using System.ServiceModel.PeerResolvers;

namespace SmartCadCore.Core
{
    public class BindingBuilder
    {
        public static void ConfigureParam(object obj, string propertyName, string confPropertyName)
        {
            if (SmartCadConfiguration.SmartCadSection.CommunicationFoundationElement.Properties[confPropertyName] != null)
            {
                string confPropertyValue = SmartCadConfiguration.SmartCadSection.CommunicationFoundationElement.Properties[confPropertyName].Value;

                PropertyInfo propertyInfo = obj.GetType().GetProperty(propertyName);

                TypeConverter typeConverter = TypeDescriptor.GetConverter(propertyInfo.PropertyType);

                if (typeConverter != null)
                {
                    object newValue = typeConverter.ConvertFromString(confPropertyValue);
                    propertyInfo.SetValue(obj, newValue, null);
                }
            }
        }

        public static CustomBinding GetNetTcpBinding()
        {
            NetTcpBinding binding = new NetTcpBinding(SecurityMode.None);

            ConfigureParam(binding.ReaderQuotas, "MaxArrayLength", "binding_ReaderQuotas_MaxArrayLength");
            ConfigureParam(binding, "MaxReceivedMessageSize", "binding_MaxReceivedMessageSize");

            ConfigureParam(binding.ReaderQuotas, "MaxDepth", "binding_ReaderQuotas_MaxDepth");
            ConfigureParam(binding.ReaderQuotas, "MaxBytesPerRead", "binding_ReaderQuotas_MaxBytesPerRead");
            ConfigureParam(binding.ReaderQuotas, "MaxStringContentLength", "binding_ReaderQuotas_MaxStringContentLength");
            ConfigureParam(binding.ReaderQuotas, "MaxNameTableCharCount", "binding_ReaderQuotas_MaxNameTableCharCount");

            ConfigureParam(binding, "ListenBacklog", "binding_ListenBacklog");
            ConfigureParam(binding, "MaxConnections", "binding_MaxConnections");
            ConfigureParam(binding, "CloseTimeout", "binding_CloseTimeout");
            ConfigureParam(binding, "OpenTimeout", "binding_OpenTimeout");
            ConfigureParam(binding, "ReceiveTimeout", "binding_ReceiveTimeout");
            ConfigureParam(binding, "SendTimeout", "binding_SendTimeout");
            ConfigureParam(binding, "MaxBufferPoolSize", "binding_MaxBufferPoolSize");
            ConfigureParam(binding, "MaxBufferSize", "binding_MaxBufferSize");

            ConfigureParam(binding.ReliableSession, "Enabled", "binding_ReliableSession_Enabled");

            BindingElementCollection bindingElementCollection = binding.CreateBindingElements();

            TcpTransportBindingElement tcpTransportBindingElement = (TcpTransportBindingElement)bindingElementCollection[bindingElementCollection.Count - 1];

            ConfigureParam(tcpTransportBindingElement.ConnectionPoolSettings, "IdleTimeout", "tcpTransportBindingElement_ConnectionPoolSettings_IdleTimeout");
            ConfigureParam(tcpTransportBindingElement.ConnectionPoolSettings, "LeaseTimeout", "tcpTransportBindingElement_ConnectionPoolSettings_LeaseTimeout");
            ConfigureParam(tcpTransportBindingElement.ConnectionPoolSettings, "MaxOutboundConnectionsPerEndpoint", "tcpTransportBindingElement_ConnectionPoolSettings_MaxOutboundConnectionsPerEndpoint");

            ConfigureParam(tcpTransportBindingElement, "MaxPendingAccepts", "tcpTransportBindingElement_MaxPendingAccepts");

            if (binding.ReliableSession.Enabled)
            {
                ReliableSessionBindingElement reliableSessionBindingElement = (ReliableSessionBindingElement)bindingElementCollection[1];

                ConfigureParam(reliableSessionBindingElement, "MaxPendingChannels", "reliableSessionBindingElement_MaxPendingChannels");
                ConfigureParam(reliableSessionBindingElement, "AcknowledgementInterval", "reliableSessionBindingElement_AcknowledgementInterval");
                ConfigureParam(reliableSessionBindingElement, "InactivityTimeout", "reliableSessionBindingElement_InactivityTimeout");
                ConfigureParam(reliableSessionBindingElement, "MaxRetryCount", "reliableSessionBindingElement_MaxRetryCount");
                ConfigureParam(reliableSessionBindingElement, "MaxTransferWindowSize", "reliableSessionBindingElement_MaxTransferWindowSize");
                ConfigureParam(reliableSessionBindingElement, "Ordered", "reliableSessionBindingElement_Ordered");
            }

            CustomBinding customBinding = new CustomBinding(bindingElementCollection);

            return customBinding;
        }

        public static CustomBinding GetClientNetTcpBinding()
        {
            NetTcpBinding binding = new NetTcpBinding(SecurityMode.None);

            ConfigureParam(binding.ReaderQuotas, "MaxArrayLength", "binding_ReaderQuotas_MaxArrayLength");
            ConfigureParam(binding, "MaxReceivedMessageSize", "binding_MaxReceivedMessageSize");

            ConfigureParam(binding.ReaderQuotas, "MaxDepth", "binding_ReaderQuotas_MaxDepth");
            ConfigureParam(binding.ReaderQuotas, "MaxBytesPerRead", "binding_ReaderQuotas_MaxBytesPerRead");
            ConfigureParam(binding.ReaderQuotas, "MaxStringContentLength", "binding_ReaderQuotas_MaxStringContentLength");
            ConfigureParam(binding.ReaderQuotas, "MaxNameTableCharCount", "binding_ReaderQuotas_MaxNameTableCharCount");

            ConfigureParam(binding, "CloseTimeout", "binding_CloseTimeout");
            ConfigureParam(binding, "OpenTimeout", "binding_OpenTimeout");
            ConfigureParam(binding, "ReceiveTimeout", "binding_ReceiveTimeout");
            ConfigureParam(binding, "SendTimeout", "binding_SendTimeout");

            ConfigureParam(binding.ReliableSession, "Enabled", "binding_ReliableSession_Enabled");

            BindingElementCollection bindingElementCollection = binding.CreateBindingElements();

            TcpTransportBindingElement tcpTBE = bindingElementCollection.Find<TcpTransportBindingElement>();

            tcpTBE.ConnectionPoolSettings.LeaseTimeout = new TimeSpan(0, 0, 5);

            if (binding.ReliableSession.Enabled)
            {
                ReliableSessionBindingElement reliableSessionBindingElement = (ReliableSessionBindingElement)bindingElementCollection[1];

                ConfigureParam(reliableSessionBindingElement, "AcknowledgementInterval", "reliableSessionBindingElement_AcknowledgementInterval");
                ConfigureParam(reliableSessionBindingElement, "InactivityTimeout", "reliableSessionBindingElement_InactivityTimeout");
                ConfigureParam(reliableSessionBindingElement, "MaxRetryCount", "reliableSessionBindingElement_MaxRetryCount");
                ConfigureParam(reliableSessionBindingElement, "MaxTransferWindowSize", "reliableSessionBindingElement_MaxTransferWindowSize");
                ConfigureParam(reliableSessionBindingElement, "Ordered", "reliableSessionBindingElement_Ordered");
            }

            CustomBinding customBinding = new CustomBinding(bindingElementCollection);

            return customBinding;
        }

        public static NetTcpBinding GetNetTcpBindingStreamed()
        {
            NetTcpBinding binding = new NetTcpBinding(SecurityMode.None);
            //binding.ReaderQuotas.MaxDepth = int.MaxValue;
            //binding.ReaderQuotas.MaxArrayLength = int.MaxValue;
            //binding.ReaderQuotas.MaxBytesPerRead = int.MaxValue;
            //binding.ReaderQuotas.MaxStringContentLength = int.MaxValue;
            //binding.ReaderQuotas.MaxNameTableCharCount = int.MaxValue;
            binding.MaxReceivedMessageSize = int.MaxValue;
            binding.TransferMode = TransferMode.Streamed;
            binding.CloseTimeout = TimeSpan.FromMinutes(60);
            binding.OpenTimeout = TimeSpan.FromMinutes(60);
            binding.ReceiveTimeout = TimeSpan.FromMinutes(60);
            binding.SendTimeout = TimeSpan.FromMinutes(60);
            //binding.ListenBacklog = int.MaxValue;
            //binding.MaxBufferPoolSize = long.MaxValue;
            //binding.MaxBufferSize = int.MaxValue;
            //binding.MaxConnections = int.MaxValue;

            return binding;
        }

        public static NetNamedPipeBinding GetNetNamedPipeBinding()
        {
            NetNamedPipeBinding binding = new NetNamedPipeBinding(NetNamedPipeSecurityMode.None);

            binding.MaxReceivedMessageSize = int.MaxValue;
            binding.TransferMode = TransferMode.Streamed;
            binding.CloseTimeout = TimeSpan.FromMinutes(60);
            binding.OpenTimeout = TimeSpan.FromMinutes(60);
            binding.ReceiveTimeout = TimeSpan.FromMinutes(60);
            binding.SendTimeout = TimeSpan.FromMinutes(60);

            return binding;
        }

        public static CustomBinding GetNetPeerTcpBinding()
        {
            NetPeerTcpBinding binding = new NetPeerTcpBinding();
            binding.Resolver.Mode = PeerResolverMode.Pnrp;
            binding.Security.Mode = SecurityMode.None;
            binding.MaxReceivedMessageSize = 700000L;

            BindingElementCollection bindingElementCollection = binding.CreateBindingElements();

            CustomBinding customBinding = new CustomBinding(bindingElementCollection);

            return customBinding;
        }

        public static CustomBinding GetOnMachineBinding()
        {
            NetNamedPipeBinding binding = new NetNamedPipeBinding();
            binding.Security.Mode = NetNamedPipeSecurityMode.None;
            binding.MaxReceivedMessageSize = 700000L;

            BindingElementCollection bindingElementCollection = binding.CreateBindingElements();

            CustomBinding customBinding = new CustomBinding(bindingElementCollection);

            return customBinding;
        }
    }
}
