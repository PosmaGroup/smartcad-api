using System;
using System.Collections.Generic;
using System.Text;
using System.ServiceModel.Description;
using System.Runtime.Serialization;
using System.Xml;

namespace SmartCadCore.Core
{
    #region Class NetDataContractSerializerOperationBehavior Documentation
    /// <summary>
    /// 
    /// </summary>
    /// <className>NetDataContractSerializerOperationBehavior</className>
    /// <author email="fsilva@smartmatic.com">Fernando Silva</author>
    /// <author email="atorres@smartmatic.com">Alden Torres</author>
    /// <date>2007/01/31</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    public class NetDataContractSerializerOperationBehavior : DataContractSerializerOperationBehavior
    {
        public NetDataContractSerializerOperationBehavior(OperationDescription operation)
            : base(operation)
        {
        }

        public override XmlObjectSerializer CreateSerializer(Type type, string name, string ns, IList<Type> knownTypes)
        {
            return new NetDataContractSerializer();
        }

        public override XmlObjectSerializer CreateSerializer(Type type, XmlDictionaryString name, XmlDictionaryString ns, IList<Type> knownTypes)
        {
            return new NetDataContractSerializer();
        }
    }
}
