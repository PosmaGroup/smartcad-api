using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;
using System.Xml;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;

using NHibernate;
using NHibernate.Cfg;
using NHibernate.Mapping;
using NHibernate.Tool.hbm2ddl;
using NHibernate.Mapping.Attributes;

using SmartCadCore.Model;
using System.Threading;
using NHibernate.Collection;
using System.Diagnostics;
using System.ServiceModel;
using NHibernate.Transform;
using NHibernate.Criterion;
using SmartCadCore.Common;
using SmartCadCore.Model.Util;

namespace SmartCadCore.Core
{
    #region Class UspDatabase Documentation
    /// <summary>
    /// It provides an interface to get access to database operations.
    /// </summary>
    /// <className>UspDatabase</className>
    /// <author email="jynojosa@smartmatic.com">Julio Ynojosa</author>
    /// <date>2006/04/06</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    public class SmartCadDatabase
    {
        internal static readonly string DatabaseAssembly = "SmartCadCore";

        #region Fields
        private static bool isInit = false;
        private static ISessionFactory SessionFactory;
        private static NHibernate.Cfg.Configuration configuration;
        private static Thread endThread = null;
        #endregion

        #region Events
        public static event EventHandler<CommittedDataEventArgs> CommittedChanges; 
        #endregion

        #region Database Initialization

        public static bool IsInit
        {
            get
            {
                return isInit;
            }
        }

        public static void ReInit()
        {
            isInit = false;
            Init();
        }

        public static void Init()
        {
            Init(SmartCadConfiguration.NHibernateProperties);
        }

        public static void Init(IDictionary<string, string> nHibernateProperties)
        {
            SmartLogger.Print("Entro al metodo");
            if (IsInit == false)
            {
                try
                {
                    MemoryStream stream = new MemoryStream();
                    HbmSerializer.Default.Validate = true;
                    HbmSerializer.Default.Serialize(stream, Assembly.Load(DatabaseAssembly));

                    stream.Position = 0;
                    XmlDocument doc = new XmlDocument();
                    doc.Load(stream);

#if (DEBUG)
                    //doc.Save("map.xml");
#endif

                    stream = ChangeInputStream(doc);
                    stream.Position = 0;
                    configuration = new NHibernate.Cfg.Configuration();
                    configuration.SetProperties(nHibernateProperties);
                    configuration.AddInputStream(stream);
                    stream.Close();

                    SessionFactory = configuration.BuildSessionFactory();

                    isInit = true;
                }
                catch
                {
                    //TODO:
                    throw;
                }
            }
        }

        internal static Assembly ChangeDatabaseAssembly(Assembly databaseAssembly)
        {
            return Assembly.GetExecutingAssembly();
        }

        internal static MemoryStream ChangeInputStream(XmlDocument doc)
        {
            //Elimina los DeletedId que esten demas de la base de datos.
            XmlNodeList list = doc.DocumentElement.SelectNodes("/*/*/*/*");
            foreach (XmlNode node in list)
            {
                if (node.Name == "column")
                {
                    foreach (XmlAttribute attribute in node.Attributes)
                    {
                        if (attribute.Name == "unique-key" && attribute.Value.StartsWith("DELETED_ID_") == true)
                        {
                            node.ParentNode.RemoveChild(node);
                        }
                    }
                }
            }
            //Elimina el atributo de unique aquellas propiedades que no son parte de campos unicos.
            list = doc.DocumentElement.SelectNodes("/*/*/*");
            foreach (XmlNode node in list)
            {
                if (node.Name == "property" && node.Attributes["name"].Value == "DeletedId" && node.ChildNodes.Count == 0)
                {
                    node.Attributes.RemoveNamedItem("unique");
                    XmlElement element = (XmlElement)node;
                    element.SetAttribute("column", "DELETED_ID");
                }
            }

            MemoryStream mem = new MemoryStream();
            doc.Save(mem);
#if (DEBUG)
            doc.Save("MAP.XML");
#endif
            return mem;
        } 
        #endregion

        #region Search methods

        private static bool checkJoinFetch(string hql)
        {
            bool result = false;

            if (string.IsNullOrEmpty(hql) == false)
                result = hql.ToLower().IndexOf("join fetch") > -1;
            
            return result;
        }

        private static bool checkDistinct(string hql)
        {
            bool result = false;
         
            if (string.IsNullOrEmpty(hql) == false)
                result = hql.ToLower().IndexOf("distinct") > -1;

            return result;
        }

        public static bool RemoveApplicationFromWorkStation(string workStationHql, string appHql) 
        {
            ISession session = NewSession();
            try 
            {
                

                var workStation = SearchObject<WorkStationData>(session, workStationHql);
                var application = SearchObject<InstalledApplicationData>(session, appHql);               
                var index = 0;
                foreach (InstalledApplicationData app in workStation.InstalledApps) 
                {
                    if (app.Name == application.Name) 
                        break;                                           
                    index++;
                }
                workStation.InstalledApps.RemoveAt(index);

                ITransaction transaction = session.BeginTransaction();
                SmartCadDataValidator.BeforeUpdate(session, workStation as ObjectData);
                UpdateObject(session, workStation);
                SmartCadDataValidator.AfterUpdate(session, workStation as ObjectData);

                transaction.Commit();
                InvokeCommitedChangedEvent(workStation as ObjectData, CommittedDataAction.Update, null);

                return true;
            }
            catch (ADOException ADOEx)
            {
                throw TranslateADOException(ADOEx);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseSession(session);                
            }
        }


        public static bool AddApplicationFromWorkStation(string workStationHql, string appHql, string macAddress)
        {
            ISession session = NewSession();
            try
            {
                //var workStation = SearchObject<WorkStationData>(session, workStationHql);
                var application = SearchObject<InstalledApplicationData>(session, appHql);
                WorkStationData ApplicationWorkStation;
                ApplicationWorkStation = new WorkStationData() { Name = macAddress, DateCreation = DateTime.Now };
                ApplicationWorkStation.InstalledApps = new List<InstalledApplicationData>();
                //ApplicationWorkStation.InstalledApps.Add(application);

                ApplicationWorkStation.InstalledApps.Add(application);

                ITransaction transaction = session.BeginTransaction();
                SmartCadDataValidator.BeforeUpdate(session, ApplicationWorkStation as ObjectData);
                SaveObject(session, ApplicationWorkStation);
                SmartCadDataValidator.AfterUpdate(session, ApplicationWorkStation as ObjectData);

                transaction.Commit();
                InvokeCommitedChangedEvent(ApplicationWorkStation as ObjectData, CommittedDataAction.Save, null);

                return true;
            }
            catch (ADOException ADOEx)
            {
                throw TranslateADOException(ADOEx);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseSession(session);
            }
        }

        public static bool UpdateApplicationFromWorkStation(string workStationHql, string appHql, string macAddress)
        {
            ISession session = NewSession();
            try
            {
                var workStation = SearchObject<WorkStationData>(session, workStationHql);
                var application = SearchObject<InstalledApplicationData>(session, appHql);
                int init = 0;
                WorkStationData ApplicationWorkStation;

                if (workStation.InstalledApps.Count == 0)
                {
                    //ApplicationWorkStation = new WorkStationData() { Name = macAddress, DateCreation = DateTime.Now };
                    workStation.InstalledApps = new List<InstalledApplicationData>();
                    workStation.InstalledApps.Add(application);

                    ITransaction transaction = session.BeginTransaction();
                    SmartCadDataValidator.BeforeUpdate(session, workStation as ObjectData);
                    UpdateObject(session, workStation);
                    SmartCadDataValidator.AfterUpdate(session, workStation as ObjectData);

                    transaction.Commit();
                    InvokeCommitedChangedEvent(workStation as ObjectData, CommittedDataAction.Update, null);
                }
                else
                {
                    foreach (var appWorkStation in workStation.InstalledApps)
                    {
                        if (appWorkStation == application)
                        {
                            init++;
                        }
                    }

                    if (init == 0) 
                        workStation.InstalledApps.Add(application);
 
                    ITransaction transaction = session.BeginTransaction();
                    SmartCadDataValidator.BeforeUpdate(session, workStation as ObjectData);
                    UpdateObject(session, workStation);
                    SmartCadDataValidator.AfterUpdate(session, workStation as ObjectData);

                    transaction.Commit();
                    InvokeCommitedChangedEvent(workStation as ObjectData, CommittedDataAction.Update, null);
                }
                return true;
            }
            catch (ADOException ADOEx)
            {
                throw TranslateADOException(ADOEx);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseSession(session);
            }
        }


        public static int HardDelete(string HQL)
        {
            int result = -1;
            IQuery query = null;
            ISession session = NewSession();
            if (HQL.TrimStart().ToLower().StartsWith("delete") == true)
            {
                query = session.CreateQuery(HQL);

                try
                {
                    result = query.ExecuteUpdate();
                }
                catch (NHibernate.ADOException ADOEx)
                {

                }
            }
            return result;
        }

        public static void HardDeleteObject(ObjectData obj)
        {
            ISession session = NewSession();
            ITransaction transaction = session.BeginTransaction();
            try
            {
                session.Delete(obj);
            }
            catch
            {

            }
            transaction.Commit();
        }

        internal static IList SearchObjects(ISession session, string HQL)
        {
            IList objectList = null;

            IQuery query = null;
            if (HQL.TrimStart().ToLower().StartsWith("select") == true)
            {
                if (checkJoinFetch(HQL) == true)
                    query = session.CreateQuery(HQL).SetResultTransformer(Transformers.DistinctRootEntity);
                else if (checkDistinct(HQL) == true)
                {
                    query = session.CreateQuery(HQL);
                }
                else
                    query = session.CreateQuery(HQL);
                
                bool execute = false;
                bool error = false;
                while (execute == false)
                {
                    try
                    {
                        objectList = query.List();
                        execute = true;
                    }
                    catch (NHibernate.ADOException ADOEx)
                    {
                        Thread.Sleep(100);
                    }
                }
            }
            return objectList;
        }

        internal static IList SearchObjects(ISession session, ObjectData objectData)
        {
            IList objectList = null;
            ICriteria criteria = session.CreateCriteria(objectData.GetType());
            criteria.Add(Example.Create(objectData));

            bool execute = false;
            bool error = false;
            while (execute == false)
            {
                try
                {
                    objectList = criteria.List();
                    execute = true;
                }
                catch (NHibernate.ADOException ADOEx)
                {
                    Thread.Sleep(100);
                }
            }

            return objectList;
        }

        public static object SearchBasicObjects(string HQL) 
        {
            return SearchBasicObjects(HQL, true);
        }

        public static IList SearchObjects(string HQL)
        {
            IList result = null;
            ISession session = NewSession();

            try
            {
                result = SearchObjects(session, HQL);
                SmartCadDataValidator.AfterLoad(session, result);
            }
            catch (ADOException ADOEx)
            {
                throw TranslateADOException(ADOEx);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseSession(session);
            }

            return result;
        }

        public static IList SearchObjects(string HQL, int startIndex, int maxRows)
        {
            IList result = null;
            ISession session = NewSession();

            try
            {
                result = SearchObjects(session, HQL, startIndex, maxRows);
                SmartCadDataValidator.AfterLoad(session, result);
            }
            catch (ADOException ADOEx)
            {
                throw TranslateADOException(ADOEx);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseSession(session);
            }

            return result;
        }

		internal static IList SearchObjects(ISession session, string HQL, int startIndex, int maxRows)
		{
			IList objectList = null;

			if (HQL.TrimStart().ToLower().StartsWith("select") == true)
			{
				IQuery query = null;

				if (checkJoinFetch(HQL) == true)
					query = session.CreateQuery(HQL).SetResultTransformer(Transformers.DistinctRootEntity);
				else if (checkDistinct(HQL) == true)
				{
					query = session.CreateQuery(HQL);
				}
				else
					query = session.CreateQuery(HQL);

				query.SetFirstResult(startIndex);
				query.SetMaxResults(maxRows);
				//query.SetFetchSize(maxRows);

				bool execute = false;
				bool error = false;
				while (execute == false)
				{
					try
					{
						objectList = query.List();
						execute = true;
					}
					catch (NHibernate.ADOException ADOEx)
					{
						Thread.Sleep(100);
					}
				}
			}

			return objectList;
		}

		public static Dictionary<string, IList> SearchObjects(int startIndex, int maxRows, Dictionary<string, IList> querys,UserApplicationData appData, bool fromClient)
		{
			Dictionary<string, int> objectsIndex = new Dictionary<string, int>();
			try
			{
				using (var session = SmartCadDatabase.NewSession())
				{
					IMultiQuery multiQuery = session.CreateMultiQuery();
					int index = 0;
					foreach (string key in querys.Keys)
					{
						for (int i = 0; i < querys[key].Count - 1; i++)
						{
							string hql = (string)querys[key][i];
							multiQuery.Add(session.CreateQuery(hql)).SetResultTransformer(NHibernate.Transform.Transformers.DistinctRootEntity);
						}

						string query = (string)querys[key][querys[key].Count - 1];
						multiQuery.Add(session.CreateQuery(query).SetMaxResults(maxRows).SetFirstResult(startIndex)).SetResultTransformer(NHibernate.Transform.Transformers.DistinctRootEntity);

						objectsIndex.Add(key, index + querys[key].Count - 1);
						index += querys[key].Count;
					}
					return SmartCadDatabase.SearchObjects(session, multiQuery, objectsIndex, appData, fromClient);
				} 				
			}
			catch (ADOException ADOEx)
			{
				throw TranslateADOException(ADOEx);
			}	
		}

		internal static Dictionary<string, IList> SearchObjects(ISession session, IMultiQuery multiQuery, Dictionary<string, int> objectsIndex, UserApplicationData appData, bool fromClient)
		{
			Dictionary<string, IList> objectsClients = new Dictionary<string, IList>();
			IList objects = null;
			using (var t = session.BeginTransaction())
			{
				objects = multiQuery.List();
                ///This commit has commented on March 1st, the following release 3.1.3
                ///should be review the changes aasociate with this.
                ///This commit should not be here on a query.
				//t.Commit();

				foreach (string key in objectsIndex.Keys)
				{
					objectsClients[key] = new ArrayList();
					foreach (ObjectData item in objects[objectsIndex[key]] as IList)
						if (fromClient == true)
							objectsClients[key].Add(Conversion.ToClient(session, item, appData));
						else
							objectsClients[key].Add(item);
				}
			}
			return objectsClients;
		}

		public static IList SearchObjects(ObjectData objectData)
        {
            IList result = null;
            ISession session = NewSession();            

            try
            {
                result = SearchObjects(session, objectData);
                SmartCadDataValidator.AfterLoad(session, result);
            }
            catch (ADOException ADOEx)
            {
                throw TranslateADOException(ADOEx);
            }
            finally
            {
                CloseSession(session);
            }

            return result;
        }

        internal static object SearchObject(ISession session, string HQL)
        {
            object objectValue = null;
            IList objectList = new ArrayList();
            
            if (HQL.TrimStart().ToLower().StartsWith("select") == true)
            {
                IQuery query = session.CreateQuery(HQL);
                bool execute = false;
                bool error = false;
                while (execute == false)
                {
                    try
                    {
                        objectList = query.List();
                        execute = true;
                    }
                    catch (NHibernate.ADOException ADOEx)
                    {
                        Thread.Sleep(100);
                    }
                }
            }
            if (objectList.Count > 0)
                objectValue = objectList[0];

            return objectValue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="HQL"></param>
        /// <returns></returns>
        public static object SearchBasicObject(string HQL)
        {
            object result = null;
            ISession session = NewSession();

            try
            {
                result = SearchObject(session, HQL);
            }
            catch (ADOException ADOEx)
            {
                throw TranslateADOException(ADOEx);
            }
            finally
            {
                CloseSession(session);
            }

            return result;
        }

        public static ObjectData SearchObjectData(string HQL)
        {
            ObjectData result = null;
            ISession session = NewSession();

            try
            {
                result = SearchObject(session, HQL) as ObjectData;
                
                if (result != null)
                {
                    List<ObjectData> list = new List<ObjectData>();
                    list.Add(result);
                    SmartCadDataValidator.AfterLoad(session, list);

                    result = list[0];
                }
            }
            catch (ADOException ADOEx)
            {
                throw TranslateADOException(ADOEx);
            }
            finally
            {
                CloseSession(session);
            }

            return result;
        }

        internal static T SearchObject<T>(ISession session, string HQL) where T : ObjectData
        {
            T objectValue = default(T);
            IQuery query = session.CreateQuery(HQL);
            IList<T> objectList = null;

            bool execute = false;
            bool error = false;
            while (execute == false)
            {
                try
                {
                    objectList = query.List<T>();
                    execute = true;
                }
                catch (NHibernate.ADOException ADOEx)
                {
                    Thread.Sleep(100);
                }
            }

            if (objectList.Count > 0)
                objectValue = objectList[0];
            return objectValue;
        }

        internal static T SearchObject<T>(ISession session, T objectData) where T : ObjectData
        {
            T result = default(T);
            try
            {
                ICriteria criteria = session.CreateCriteria(objectData.GetType());
                criteria.Add(Example.Create(objectData));
                IList objectList = null;

                bool execute = false;
                bool error = false;
                while (execute == false)
                {
                    try
                    {
                        objectList = criteria.List();
                        execute = true;
                    }
                    catch (NHibernate.ADOException ADOEx)
                    {
                        Thread.Sleep(100);
                    }
                }

                if (objectList != null && objectList.Count > 0)
                    result = objectList[0] as T;
            }
            catch (ADOException ADOEx)
            {
                throw TranslateADOException(ADOEx);
            }
            catch
            {
                result = default(T);
                throw;
            }

            return result;
        }

        public static T SearchObject<T>(string HQL) where T : ObjectData
        {
            ISession session = NewSession();
            T result = default(T);
            try
            {
                result = SearchObject<T>(session, HQL);
                if (result != null)
                {
                    ArrayList list = new ArrayList();
                    list.Add(result);
                    SmartCadDataValidator.AfterLoad(session, list);
                }
            }
            catch (ADOException ADOEx)
            {
                throw TranslateADOException(ADOEx);
            }
            catch
            {
                result = default(T);
                throw;
            }
            finally
            {
                CloseSession(session);
            }
            return result;
        }

        public static T SearchObject<T>(T objectData) where T : ObjectData
        {
            ISession session = NewSession();
            T result = default(T);
            try
            {
                result = SearchObject<T>(session, objectData);
                if (result != null)
                {
                    ArrayList list = new ArrayList();
                    list.Add(result);
                    SmartCadDataValidator.AfterLoad(session, list);
                }
            }
            catch (ADOException ADOEx)
            {
                throw TranslateADOException(ADOEx);
            }
            catch
            {
                result = default(T);
                throw;
            }
            finally
            {
                CloseSession(session);
            }
            return result;
        }

        public static object SearchBasicObjects(string queryString, bool isHql)
        {
            object objectList = null;
            ISession session = NewSession();
            IQuery query = null;

            try
            {
                if (isHql == true)
                {
                    query = session.CreateQuery(queryString);
                }
                else
                {
                    query = session.CreateSQLQuery(queryString);
                }

                bool execute = false;
                bool error = false;
                while (execute == false)
                {
                    try
                    {
                        objectList = query.List();
                        execute = true;
                    }
                    catch (NHibernate.ADOException ADOEx)
                    {
                        Thread.Sleep(100);
                    }
                }
            }
            catch (ADOException ADOEx)
            {
                throw TranslateADOException(ADOEx);
            }
            catch(Exception ex)
            {
                SmartLogger.Print(ex);
                return null;
            }
            finally
            {
                CloseSession(session);
            }
            return objectList;
        }

        public static object UpdateSqlWithOutValidator(ISession session, string sql)
        {
            object obj = null;

            try
            {
                //ISQLQuery query = session.CreateSQLQuery(sql).AddScalar("", NHibernateUtil.Int32);
                IQuery query = session.CreateSQLQuery(sql);

                bool execute = false;
                bool error = false;
                while (execute == false)
                {
                    try
                    {
                        obj = query.ExecuteUpdate();
                        execute = true;
                    }
                    catch (NHibernate.ADOException ADOEx)
                    {
                        Thread.Sleep(100);
                    }
                }
            }
            catch (ADOException ADOEx)
            {
                throw TranslateADOException(ADOEx);
            }
            catch(Exception ex)
            {
                SmartLogger.Print(ex);
                return null;
            }

            return obj;
        }

        public static object UpdateSqlWithOutValidator(string sql)
        {
            object obj = null;
            ISession session = NewSession();

            try
            {
                obj = UpdateSqlWithOutValidator(session, sql);
            }
            finally
            {
                session.Close();
            }
            return obj;
        }

        public static object UpdateHqlWithOutValidator(ISession session, string hql)
        {
            object obj = null;

            try
            {
                IQuery query = session.CreateQuery(hql);

                bool execute = false;
                bool error = false;
                while (execute == false)
                {
                    try
                    {
                        obj = query.ExecuteUpdate();
                        execute = true;
                    }
                    catch (NHibernate.ADOException ADOEx)
                    {
                        Thread.Sleep(100);
                    }
                    catch (Exception ex)
                    { 

                    }
                }
            }
            catch (ADOException ADOEx)
            {
                throw TranslateADOException(ADOEx);
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
                return null;
            }

            return obj;
        }

        public static object UpdateHqlWithOutValidator(string hql)
        {
            object obj = null;
            ISession session = NewSession();

            try
            {
                obj = UpdateHqlWithOutValidator(session, hql);
            }
            finally
            {
                session.Close();
            }
            return obj;
        }

        public static T SearchNamedObject<T>(string name) where T : ObjectData, INamedObjectData, new()
        {
            T result = default(T);
            ISession session = NewSession();

            try
            {
                T objectData = ClassUtil.NewInstance<T>();
                objectData.Name = name;

                ICriteria criteria = session.CreateCriteria(objectData.GetType());
                criteria.Add(Example.Create(objectData));

                IList objectList = criteria.List();

                if (objectList.Count > 0)
                {
                    result = objectList[0] as T;
                    ArrayList list = new ArrayList();
                    list.Add(result);
                    SmartCadDataValidator.AfterLoad(session, list);
                }
            }
            catch (ADOException ADOEx)
            {
                throw TranslateADOException(ADOEx);
            }
            catch
            {
                result = default(T);
                throw;
            }
            finally
            {
                CloseSession(session);
            }

            return result;
        }

        #endregion

        #region Update methods

        internal static void UpdateObjectWithValidator(ISession session, ObjectData objectData)
        {
            SmartCadDataValidator.BeforeUpdate(session, objectData);
            UpdateObject(session, objectData);
            SmartCadDataValidator.AfterUpdate(session, objectData);
        }

        public static void UpdateObject(ISession session, ObjectData objectData)
        {
            session.Update(objectData);
        }

        public static void UpdateObject<T>(T objectData) where T : ObjectData, new()
        {
            ISession session = NewSession();
            
            try
            {
                ITransaction transaction = session.BeginTransaction();

                try
                {
                    SmartCadDataValidator.BeforeUpdate(session, objectData);
                    UpdateObject(session, objectData);
                    SmartCadDataValidator.AfterUpdate(session, objectData);

                    transaction.Commit();
                    InvokeCommitedChangedEvent(objectData, CommittedDataAction.Update, null);
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
            }
            catch (ADOException ex)
            {
                throw TranslateADOException(ex, objectData);
            }
            finally
            {
                CloseSession(session);
            }
        }

        #endregion

        #region Delete methods
        private static List<string> deleteTable = new List<string>();
        private static object deleteTableObject = new object();
        internal static void DeleteObject(ISession session, ObjectData objectData, bool sendCommitedChanges)
        {
            if (objectData.DeletedId == null)
            {
                objectData.DeletedId = Guid.NewGuid();
            }
            string key = objectData.GetType().Name + objectData.Code;
            bool contains = false;
            lock (deleteTableObject)
            {
                contains = deleteTable.Contains(key);
            }
            if (contains == false)
            {
                lock (deleteTableObject)
                {
                    deleteTable.Add(key);
                }
                try
                {
                    session.Update(objectData);
					if (sendCommitedChanges == true)
					{
						InvokeCommitedChangedEvent(objectData, CommittedDataAction.Delete, null);
					}
                }
                catch (Exception)
                {
                    lock (deleteTableObject)
                    {
                        deleteTable.Remove(key);
                    }
                }
            }
            else
            {
                throw new DatabaseObjectException(objectData, ResourceLoader.GetString2("$Messaje.Delete") + objectData.GetType().Name);
            }
		}

		public static void DeleteObject(ObjectData objectData, object data)
		{
			//SmartLogger.Print("SmartCadDatabase:-DeleteObject(begin)");

			ISession session = NewSession();

			try
			{
				ITransaction transaction = session.BeginTransaction();
				CommittedDataAction action = CommittedDataAction.Delete;
				try
				{
					SmartCadDataValidator.BeforeDelete(session, objectData);
					DeleteObject(session, objectData,false);
					SmartCadDataValidator.AfterDelete(session, objectData);
					transaction.Commit();
					InvokeCommitedChangedEvent(objectData, action, data);
				}
				catch (Exception)
				{
					transaction.Rollback();
					throw;
				}
			}
			catch (StaleObjectStateException ex)
			{
				throw TranslateStaleObjectStateException(ex, objectData);
			}
			catch (TransactionException)
			{
				throw new DatabaseServerException(
					ResourceLoader.GetString2("ConnectDataBaseExpection"),
					null);
			}
			catch (ADOException ex)
			{
				throw TranslateADOException(ex, objectData);
			}
			finally
			{
				CloseSession(session);
				string key = objectData.GetType().Name + objectData.Code;
				deleteTable.Remove(key);
			}

			//SmartLogger.Print("SmartCadDatabase:-DeleteObject(end)");
		}

        /// <summary>
        /// Added to delete all officer assignments
        /// </summary>
        /// <param name="objectData"></param>
        /// <param name="data"></param>
        /// <param name="officerDelete"></param>
        public static void DeleteObject(ObjectData objectData, object data, bool officerDelete)
        {
            //SmartLogger.Print("SmartCadDatabase:-DeleteObject(begin)");

            ISession session = NewSession();

            try
            {
                ITransaction transaction = session.BeginTransaction();
                CommittedDataAction action = CommittedDataAction.Delete;
                try
                {
                    objectData = RefreshObject(session, objectData) as UnitOfficerAssignHistoryData;
                    DeleteObject(session, objectData, false);
                    SmartCadDataValidator.AfterDelete(session, objectData);
                    transaction.Commit();
                    InvokeCommitedChangedEvent(objectData, action, data);
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
            catch (StaleObjectStateException ex)
            {
                throw TranslateStaleObjectStateException(ex, objectData);
            }
            catch (TransactionException)
            {
                throw new DatabaseServerException(
                    ResourceLoader.GetString2("ConnectDataBaseExpection"),
                    null);
            }
            catch (ADOException ex)
            {
                throw TranslateADOException(ex, objectData);
            }
            finally
            {
                CloseSession(session);
                string key = objectData.GetType().Name + objectData.Code;
                deleteTable.Remove(key);
            }

            //SmartLogger.Print("SmartCadDatabase:-DeleteObject(end)");
        }

		public static void DeleteObjectCollection(IList collection, object data)
		{
			//SmartLogger.Print("SmartCadDatabase:-DeleteObject(begin)");

			ISession session = NewSession();

			try
			{
				ITransaction transaction = session.BeginTransaction();
				CommittedDataAction action = CommittedDataAction.Delete;
				try
				{
					foreach (ObjectData objectData in collection)
					{
						SmartCadDataValidator.BeforeDelete(session, objectData);
						DeleteObject(session, objectData,false);
						SmartCadDataValidator.AfterDelete(session, objectData);
					}
					transaction.Commit();
					if (collection.Count > 0 && (collection[0] is AuditData) == false)
						InvokeCommitedChangedEvent(null, action, collection);
				}
				catch (Exception)
				{
					transaction.Rollback();
					throw;
				}
			}
			catch (StaleObjectStateException ex)
			{
				throw TranslateStaleObjectStateException(ex, (ObjectData)collection[0]);
			}
			catch (TransactionException)
			{
				throw new DatabaseServerException(
					ResourceLoader.GetString2("ConnectDataBaseExpection"),
					null);
			}
			catch (ADOException ex)
			{
				throw TranslateADOException(ex, (ObjectData)collection[0]);
			}
			finally
			{
				CloseSession(session);
				foreach (ObjectData objectData in collection)
				{
					string key = objectData.GetType().Name + objectData.Code;
					deleteTable.Remove(key);
				}
			}

			//SmartLogger.Print("SmartCadDatabase:-DeleteObject(end)");
		}

        #endregion

        #region Refresh Methods
        internal static ObjectData RefreshObject(ISession session, ObjectData objectData)
        {
            if (objectData.Code != 0)
            {
                try
                {
                    session.Refresh(objectData, NHibernate.LockMode.None);
                }
                catch
                {
                    objectData = null;
                }
            }
            return objectData;
        }

        public static ObjectData RefreshObject(ObjectData objectData)
        {
            ISession session = NewSession();

            try
            {
                objectData = RefreshObject(session, objectData);
                if (objectData != null)
                {
                    ArrayList list = new ArrayList();
                    list.Add(objectData);
                    SmartCadDataValidator.AfterLoad(session, list);
                }
            }
            catch (ADOException ADOEx)
            {
                throw TranslateADOException(ADOEx);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseSession(session);
            }

            return objectData;
        }

        public static ObjectData RefreshObject(ObjectData objectData, bool evict)
        {
            ISession session = NewSession();

            try
            {
                RefreshObject(session, objectData);

                if (objectData != null)
                {
                    ArrayList list = new ArrayList();
                    list.Add(objectData);
                    SmartCadDataValidator.AfterLoad(session, list);
                }

                session.Evict(objectData);
            }
            catch (ADOException ADOEx)
            {
                throw TranslateADOException(ADOEx);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                session.Flush();
                session.Clear();
                //session.Close();
                CloseSession(session);
            }

            return objectData;
        }
        #endregion

        #region Save methods

        public static int SaveObject(ISession session, ObjectData objectData)
        {
            return (int)session.Save(objectData);
        }

        public static ObjectData SaveOrUpdate(ObjectData objectData)
        {
            return SaveOrUpdate(objectData, null);
        }

        public static ObjectData SaveOrUpdate(ISession session, ObjectData objectData)
        {
            bool invokeCommited = false;
            CommittedDataAction action = CommittedDataAction.None;

            try
            {
                try
                {
                    if (objectData.Code == 0)
                    {
                        SmartCadDataValidator.BeforeSave(session, objectData);
                        objectData.Code = SaveObject(session, objectData);
                        SmartCadDataValidator.AfterSave(session, objectData);
                        action = CommittedDataAction.Save;
                    }
                    else
                    {
                        SmartCadDataValidator.BeforeUpdate(session, objectData);
                        UpdateObject(session, objectData);
                        SmartCadDataValidator.AfterUpdate(session, objectData);
                        action = CommittedDataAction.Update;
                    }
					if ((objectData is AuditData) == false)
						invokeCommited = true;
                    return objectData;
                }
                catch
                {
                    throw;
                }
            }
            catch (StaleObjectStateException ex)
            {
                throw TranslateStaleObjectStateException(ex, objectData);
            }
            catch (TransactionException ex)
            {
                throw new DatabaseServerException(ResourceLoader.GetString2("ConnectDataBaseException"), ex);
            }
            catch (ADOException ex)
            {
                throw TranslateADOException(ex, objectData);
            }
            finally
            {
                if (invokeCommited == true)
                    InvokeCommitedChangedEvent(objectData, action, null);
            }
        }

        public static void SaveOrUpdateCollection(IList collection, object data)
        {
            ISession session = NewSession();
            bool invokeCommited = false;
            CommittedDataAction action = CommittedDataAction.None;

            try
            {
                ITransaction transaction = session.BeginTransaction();

                try
                {
					foreach (ObjectData objectData in collection)
					{
						if (objectData.Code == 0)
						{
                            
							SmartCadDataValidator.BeforeSave(session, objectData);
							objectData.Code = SaveObject(session, objectData);
							SmartCadDataValidator.AfterSave(session, objectData);
							action = CommittedDataAction.Save;
						}
						else
						{
							SmartCadDataValidator.BeforeUpdate(session, objectData);
							UpdateObject(session, objectData);
							SmartCadDataValidator.AfterUpdate(session, objectData);
							action = CommittedDataAction.Update;
						}
					}

                    transaction.Commit();
					if (collection.Count > 0 && (collection[0] is AuditData) == false)
						invokeCommited = true;
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
            }
            catch (StaleObjectStateException ex)
            {
				throw TranslateStaleObjectStateException(ex, (ObjectData)collection[0]);
            }
            catch (TransactionException ex)
            {
                throw new DatabaseServerException(ResourceLoader.GetString2("ConnectDataBaseException"), ex);
            }
            catch (ADOException ex)
            {
				throw TranslateADOException(ex, (ObjectData)collection[0]);
            }
            finally
            {
                CloseSession(session);
                if (invokeCommited == true)
                    InvokeCommitedChangedEvent(null, action, collection);
            }
        }

		public static ObjectData SaveOrUpdate(ObjectData objectData, object data)
		{
			ISession session = NewSession();
			bool invokeCommited = false;
			CommittedDataAction action = CommittedDataAction.None;

			try
			{
				ITransaction transaction = session.BeginTransaction();

				try
				{
					if (objectData.Code == 0)
					{
						SmartCadDataValidator.BeforeSave(session, objectData);
						objectData.Code = SaveObject(session, objectData);
						SmartCadDataValidator.AfterSave(session, objectData);
						action = CommittedDataAction.Save;
					}
					else
					{
						
						SmartCadDataValidator.BeforeUpdate(session, objectData);
						UpdateObject(session, objectData);
						SmartCadDataValidator.AfterUpdate(session, objectData);
						action = CommittedDataAction.Update;
					}

					transaction.Commit();
					if ((objectData is AuditData) == false)
						invokeCommited = true;
					return objectData;
				}
				catch(Exception ex)
				{
					transaction.Rollback();
					throw;
				}
			}
			catch (StaleObjectStateException ex)
			{
				throw TranslateStaleObjectStateException(ex, objectData);
			}
			catch (TransactionException ex)
			{
				throw new DatabaseServerException(ResourceLoader.GetString2("ConnectDataBaseException"), ex);
			}
			catch (ADOException ex)
			{
				throw TranslateADOException(ex, objectData);
			}
			finally
			{
				CloseSession(session);
				if (invokeCommited == true)
					InvokeCommitedChangedEvent(objectData, action, data);
			}
		}

        public static void InvokeCommitedChangedEvent(ObjectData objectData, CommittedDataAction action, object data)
        {
            ThreadPool.QueueUserWorkItem(delegate(object state)
            {
                if (CommittedChanges != null)
                {
                    CommittedChanges(null, new CommittedDataEventArgs(objectData, action, data));
                }
            });
        }

        /// <summary>
        /// Method use to send objects that are not ObjectData, like clientData from client to client or unitClientData from pthe parser.
        /// </summary>
        /// <param name="obj">Data to send</param>
        /// <param name="action">Action perform to the dara.</param>
        /// <param name="data">Extra information to the data.</param>
        public static void InvokeCommitedChangedEvent(object obj, CommittedDataAction action, object data)
        {
            ThreadPool.QueueUserWorkItem(delegate(object state)
            {
                if (CommittedChanges != null)
                {
                    CommittedChanges(null, new CommittedDataEventArgs(obj, action));
                }
            });
        }

        public static T SaveObject<T>(T objectData) where T : ObjectData, new()
        {
            ISession session = NewSession();
            T result = default(T);
            int key;
            try
            {
                ITransaction transaction = session.BeginTransaction();

                try
                {
                    key = (int) session.Save(objectData);

                    transaction.Commit();

                    if (key != 0)
                    {
                        result = new T();
                        result.Code = key;
                        result = SearchObject<T>(GetHQL<T>(result));
                    }
                    InvokeCommitedChangedEvent(objectData, CommittedDataAction.Save , null);
                }
                catch
                {
                    transaction.Rollback();
                    result = default(T);
                    throw;
                }
            }
            catch (ADOException ex)
            {
                throw TranslateADOException(ex, objectData);
            }
            finally
            {
                CloseSession(session);
            }
            return result;
        }



        public static void SaveObjectList<T>(ICollection<T> listObjectData)
            where T : ObjectData, new ()
        {
            if (listObjectData.Count > 0)
            {
                ISession session = NewSession();
                IList<T> objects = new List<T>();
                try
                {
                    ITransaction transaction = session.BeginTransaction();

                    foreach (T objectData in listObjectData)
                    {
                        try
                        {
                            session.Save(objectData);
                            objects.Add(objectData);
                        }
                        catch
                        {
                            //transaction.Rollback();
                        }
                    }

                    transaction.Commit();
                    if (objects.Count > 0)
                    {
                        InvokeCommitedChangedEvent(objects, CommittedDataAction.Save, null);
                    }

                }
                catch (ADOException ex)
                {
                    //throw TranslateADOException(ex, objectData);
                }
                finally
                {
                    CloseSession(session);
                }
            }
        }

        public static T SaveNamedObject<T>(T objectData) where T : ObjectData, INamedObjectData, new()
        {
            T result = default(T);
            ISession session = NewSession();

            try
            {
                ITransaction transaction = session.BeginTransaction();

                try
                {
                    session.Save(objectData);

                    transaction.Commit();

                    result = SearchNamedObject<T>(objectData.Name);
                    InvokeCommitedChangedEvent(objectData, CommittedDataAction.Save, null);
                }
                catch
                {
                    transaction.Rollback();
                    result = default(T);
                    throw;
                }
            }
            catch (ADOException ADOEx)
            {
                throw TranslateADOException(ADOEx);
            }
            finally
            {
                CloseSession(session);
            }

            return result;
        }

        #endregion

        #region Util

        public static string GetHQL<T>(T objectData) where T : ObjectData
        {
            Type objectDataType = typeof(T);
            return
                "select data from " +
                objectDataType.Name +
                " data where data.Code = " +
                objectData.Code.ToString();
        }

        private static string GetHQL(ObjectData objectData)
        {
            Type objectDataType = objectData.GetType();
            return
                "select object from " +
                objectDataType.Name +
                " object where object.Code = " +
                objectData.Code.ToString();
        }

        public static bool CheckConnection()
        {
            bool result = false;
            try
            {
                GetTimeFromBD();
                result = true;
            }
            catch
            {
                result = false;
            }
            return result;
        }

        public static DateTime GetTimeFromBD()
        {
            return DateTime.Now;

            /*DateTime dateToReturn = new DateTime();
            IDbConnection connection = null;
            ISession session = null;

            try
            {
                connection = new SqlConnection(configuration.Properties["hibernate.connection.connection_string"].ToString());
                session = SessionFactory.OpenSession(connection);
                session.Connection.Open();
                IEnumerable result = session.CreateQuery("select getdate() from DummyData").Enumerable();
                foreach (object obj in result)
                {
                    dateToReturn = (DateTime)obj;
                }
            }
            catch (ADOException ex)
            {
                session.Clear();
                throw TranslateADOException(ex);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (session != null)
                {
                    session.Connection.Close();
                    session = null;
                }
            }

            return dateToReturn;*/
        }

        private static bool canKillProcess = true;

        public static bool CanKillProcess
        {
            get
            {
                return canKillProcess;
            }
            set
            {
                canKillProcess = value;
            }
        }

        public static Exception TranslateStaleObjectStateException(StaleObjectStateException ex, ObjectData obj)
        {
            Exception staleObjectException;
            //Row was updated or deleted by another transaction (or unsaved-value mapping was incorrect)
            staleObjectException = new DatabaseStaleObjectException(obj, GetConcurrencyErrorByObject(obj), ex);
            return staleObjectException;
        }

        public static Exception TranslateADOException(ADOException ex, ObjectData obj)
        {
            Exception databaseObjectException;
            if (ex.InnerException != null && ex.InnerException is SqlException)
            {
                SqlException sqlException = (SqlException)ex.InnerException;

                switch (sqlException.Class)
                {
                    ///Unique key violation
                    case 14:
                        databaseObjectException = new DatabaseObjectException(obj, ResourceLoader.GetString2(ObtainSQLServerUniqueKeyFromError(sqlException.Message)));
                        break;
                    ///Foreign key violation
                    case 16:
                        databaseObjectException = new DatabaseObjectException(obj, ResourceLoader.GetString2(ObtainSQLServerForeignKeyFromError(sqlException.Message)));
                        break;
                    case 20:
                        if (SmartCadDatabase.endThread == null && CanKillProcess)
                        {
                            SmartCadDatabase.endThread = new Thread(new ThreadStart(EndProcess));
                            SmartCadDatabase.endThread.Start();
                        }

                        databaseObjectException = new DatabaseServerDownException(ResourceLoader.GetString2("ConnectDataBaseException"));
                        break;
                    default:
                        databaseObjectException = new DatabaseObjectException(obj, sqlException.Message, sqlException);
                        break;
                }
            }
            else
                databaseObjectException = new DatabaseObjectException(obj, ex.Message, ex);

            return databaseObjectException;
        }

        internal static void EndProcess()
        {
            Thread.Sleep(5000);
            Process.GetCurrentProcess().Kill();
        }

        internal static Exception TranslateADOException(ADOException ex)
        {
            return TranslateADOException(ex, null);            
        }

        private static string ObtainSQLServerUniqueKeyFromError(string errorMessage)
        {
            try
            {
                const int UNIQUE_KEY_POSITION = 1;
                string[] splitedMessage = errorMessage.Split(new string[] { "'" }, 3, StringSplitOptions.RemoveEmptyEntries);
                return splitedMessage[UNIQUE_KEY_POSITION];
            }
            catch
            {
                return errorMessage;
            }
        }

        private static string ObtainSQLServerForeignKeyFromError(string errorMessage)
        {
            try
            {
                const int FOREIGN_KEY_POSITION = 1;
                string[] splitedMessage = errorMessage.Split(new string[] { "\"" }, 3, StringSplitOptions.RemoveEmptyEntries);
                return splitedMessage[FOREIGN_KEY_POSITION];
            }
            catch
            {
                return errorMessage;
            }
        }

        public static string GetConcurrencyErrorByObject(ObjectData objectData)
        {
            object[] args = new object[3];
            args[2] = ResourceLoader.GetString2("Operator").ToLower();
            if (objectData != null)
            {
                Type objectType = objectData.GetType();                
                if (objectType.Equals(typeof(UserProfileData)))
                {
                    args[0] = ResourceLoader.GetString2("thisM");
                    args[1] = ResourceLoader.GetString2("Profile").ToLower();
                }
                else if (objectType.Equals(typeof(UserRoleData)))
                {
                    args[0] = ResourceLoader.GetString2("thisM");
                    args[1] = ResourceLoader.GetString2("Role").ToLower();
                }
                else if (objectType.Equals(typeof(OperatorData)) ||
                    objectType.Equals(typeof(UserAccountData)))
                {
                    args[0] = ResourceLoader.GetString2("thisM");
                    args[1] = ResourceLoader.GetString2("User").ToLower();
                }
                else if (objectType.Equals(typeof(OperatorStatusData)))
                {
                    args[0] = ResourceLoader.GetString2("thisM");
                    args[1] = ResourceLoader.GetString2("OperatorStatus").ToLower();
                }
                else if (objectType.Equals(typeof(IncidentTypeData)))
                {
                    args[0] = ResourceLoader.GetString2("thisM");
                    args[1] = ResourceLoader.GetString2("IncidentType").ToLower();
                }
                else if (objectType.Equals(typeof(UnitTypeData)))
                {
                    args[0] = ResourceLoader.GetString2("thisM");
                    args[1] = ResourceLoader.GetString2("UnitType").ToLower();
                }
                else if (objectType.Equals(typeof(UnitData)))
                {
                    args[0] = ResourceLoader.GetString2("thisF");
                    args[1] = ResourceLoader.GetString2("Unit");
                }
                else if (objectType.Equals(typeof(OfficerData)))
                {
                    args[0] = ResourceLoader.GetString2("thisM");
                    args[1] = ResourceLoader.GetString2("oficial");
                }
                else if (objectType.Equals(typeof(DepartmentStationAssignHistoryData)))
                {
                    args[0] = ResourceLoader.GetString2("thisF");
                    args[1] = ResourceLoader.GetString2("asignacin de unidad");
                }
                else if (objectType.Equals(typeof(IncidentNotificationData)))
                {
                    args[0] = ResourceLoader.GetString2("thisF");
                    args[1] = ResourceLoader.GetString2("solicitud");
                }
                else if (objectType.Equals(typeof(IncidentData)))
                {
                    args[0] = ResourceLoader.GetString2("thisM");
                    args[1] = ResourceLoader.GetString2("incidente");
                }
                else if (objectType.Equals(typeof(DispatchOrderData)))
                {
                    args[0] = ResourceLoader.GetString2("thisF");
                    args[1] = ResourceLoader.GetString2("orden de despacho");
                }
                else if (objectType.Equals(typeof(UnitEndingReportTypeData)))
                {
                    args[0] = ResourceLoader.GetString2("thisM");
                    args[1] = ResourceLoader.GetString2("tipo de reporte de finalizacin de unidad");
                }
                else if (objectType.Equals(typeof(SupportRequestReportData)))
                {
                    args[0] = ResourceLoader.GetString2("thisF");
                    args[1] = ResourceLoader.GetString2("$SupportRequestForm").ToLower();
                }
                else if (objectType.Equals(typeof(QuestionData)))
                {
                    args[0] = ResourceLoader.GetString2("thisF");
                    args[1] = ResourceLoader.GetString2("Question").ToLower();
                }
                else if (objectType.Equals(typeof(PhoneReportData)))
                {
                    args[0] = ResourceLoader.GetString2("thisM");
                    args[1] = ResourceLoader.GetString2("reporte telefnico");
                }
                else if (objectType.Equals(typeof(IncidentNoteData)))
                {
                    args[0] = ResourceLoader.GetString2("thisF");
                    args[1] = ResourceLoader.GetString2("nota de incidente");
                }
                else if (objectType.Equals(typeof(UnitNoteData)))
                {
                    args[0] = ResourceLoader.GetString2("thisF");
                    args[1] = ResourceLoader.GetString2("nota de unidad");
                }
                else if (objectType.Equals(typeof(DepartmentTypeData)))
                {
                    args[0] = ResourceLoader.GetString2("thisM");
                    args[1] = ResourceLoader.GetString2("DepartmentType").ToLower();
                }
                else if (objectType.Equals(typeof(DepartmentZoneData)))
                {
                    args[0] = ResourceLoader.GetString2("thisF");
                    args[1] = ResourceLoader.GetString2("DepartmentZone").ToLower();
                }
                else if (objectType.Equals(typeof(DepartmentStationData)))
                {
                    args[0] = ResourceLoader.GetString2("thisF");
                    args[1] = ResourceLoader.GetString2("DepartmentStation").ToLower();
                }
                else if (objectType.Equals(typeof(PositionData)))
                {
                    args[0] = ResourceLoader.GetString2("thisF");
                    args[1] = ResourceLoader.GetString2("Position").ToLower();
                }
                else if (objectType.Equals(typeof(RankData)))
                {
                    args[0] = ResourceLoader.GetString2("thisF");
                    args[1] = ResourceLoader.GetString2("Rank").ToLower();
                }
                else if (objectType.Equals(typeof(GPSData)))
                {
                    args[0] = ResourceLoader.GetString2("thisM");
                    args[1] = ResourceLoader.GetString2("GPS").ToUpper();
                }
                else if (objectType.Equals(typeof(RouteData)))
                {
                    args[0] = ResourceLoader.GetString2("thisM");
                    args[1] = ResourceLoader.GetString2("Route").ToLower();
                }
                else if (objectType.Equals(typeof(WorkShiftVariationData)))
                {
                    args[0] = ResourceLoader.GetString2("thisM");
                    if (((WorkShiftVariationData)objectData).Type != WorkShiftVariationData.WorkShiftType.TimeOff)
                        args[1] = ResourceLoader.GetString2("WorkShift").ToLower();
                    else
                        args[1] = ResourceLoader.GetString2("VacationAndPermission").ToLower();
                    args[2] = ResourceLoader.GetString2("Supervisor").ToLower();
                }
                else
                {
                    args[0] = ResourceLoader.GetString2("thisM");
                    args[1] = objectType.FullName;
                }
            }
            else
            {
                args[0] = "";
                args[1] = "";
            }
            StringWriter writer = new StringWriter();
            writer.Write(ResourceLoader.GetString2("NoChanges", args[0].ToString(),args[1].ToString(), args[2].ToString()));
            return writer.ToString();
        }
        #endregion

        #region Schema managment

        internal static void ExecuteSql(string sqlFile)
        {
            IDbConnection conn = new SqlConnection(SmartCadConfiguration.DataBaseConnectionString);

            try
            {
                conn.Open();

                string[] lines = File.ReadAllLines(sqlFile);

                string sqlCmd = string.Empty;

                foreach (string line in lines)
                {
                    if (line.StartsWith("--BEGIN_SQL"))
                    {
                        sqlCmd = "";
                    }
                    else if (line.StartsWith("--END_SQL"))
                    {
                        sqlCmd = sqlCmd.Trim();

                        if (sqlCmd != string.Empty)
                        {
                            IDbCommand cmd = conn.CreateCommand();
                            cmd.CommandText = sqlCmd;
                            cmd.ExecuteNonQuery();
                            cmd.Dispose();

                            sqlCmd = string.Empty;
                        }
                    }
                    else
                    {
                        sqlCmd += (line + System.Environment.NewLine);
                    }
                }

                sqlCmd = sqlCmd.Trim();

                if (sqlCmd != string.Empty)
                {
                    IDbCommand cmd = conn.CreateCommand();
                    cmd.CommandText = sqlCmd;
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                }
            }
            finally
            {
                conn.Close();
            }
        }

        public static void CreateDataBaseSchema()
        {
            SchemaExport schemaExport = new SchemaExport(configuration);
            schemaExport.Create(false, true);

            //string sqlPath = SmartCadConfiguration.DistFolder + "\\Sql";

            //string[] sqlFiles = Directory.GetFiles(sqlPath, "*.sql");

            //foreach (string sqlFile in sqlFiles)
            //{
            //    if (new FileInfo(sqlFile).Name != "delete.sql")
            //    {
            //       ExecuteSql(sqlFile);                   
            //    }
            //}
        }

        public static void DeleteDataBaseSchema()
        {
            SchemaExport schemaExport = new SchemaExport(configuration);
            schemaExport.Drop(false, true);

            //string sqlFile = SmartCadConfiguration.DistFolder + "\\Sql\\delete.sql";
            
            //ExecuteSql(sqlFile);
        }

        public static void UpgradeDatabaseSchema()
        {
            if (File.Exists(SmartCadConfiguration.UpgradeSchemaDataDB) == true)
            {
                SqlConnection connection;
                SqlCommand command;
                string sql;
                try
                {
                    sql = File.ReadAllText(SmartCadConfiguration.UpgradeSchemaDataDB);
                }
                catch
                {
                    throw new FileLoadException(ResourceLoader.GetString2("ReadUpdateFileException"));
                }
                try
                {
                    connection = new SqlConnection(SmartCadConfiguration.DataBaseConnectionString);
                    connection.Open();
                }
                catch
                {
                    throw new Exception(ResourceLoader.GetString2("ConnectDataBaseException"));
                }

                try
                {
                    command = new SqlCommand();
                    command.CommandText = sql;
                    command.Connection = connection;
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw new Exception(ResourceLoader.GetString2("UpdateSchemaException", new FileInfo(SmartCadConfiguration.UpgradeSchemaDataDB).FullName));
                }

                if (command != null)
                    command.Dispose();

                if (connection != null)
                {
                    connection.Close();
                    connection.Dispose();
                }
            }
            else
                throw new FileNotFoundException(ResourceLoader.GetString2("UpdateFileNotFound"));
        }

        public static void CheckDatabaseSchema()
        {
            if (File.Exists(SmartCadConfiguration.CheckSchemaDataDB) == true)
            {
                SqlConnection connection = null;
                SqlCommand command = null;
                SqlDataReader reader = null;
                string sql;
                bool throwException = false;
                try
                {
                    sql = File.ReadAllText(SmartCadConfiguration.CheckSchemaDataDB);
                }
                catch
                {
                    throw new FileLoadException(ResourceLoader.GetString2("ReadCheckFileException"));
                }
                try
                {
                    connection = new SqlConnection(SmartCadConfiguration.DataBaseConnectionString);
                    connection.Open();
                }
                catch
                {
                    throw new Exception(ResourceLoader.GetString2("ConnectDataBaseException"));
                }

                try
                {
                    command = new SqlCommand();
                    command.CommandText = sql;
                    command.Connection = connection;
                    reader = command.ExecuteReader();

                    if (reader.Read())
                    {
                        if (reader[0].ToString().Length > 0)
                        {
                            SmartLogger.Print(reader[0].ToString());
                            throwException = true;
                        }
                    }
                }
                catch
                {
                    throw new Exception(ResourceLoader.GetString2("UpdateSchemaException", new FileInfo(SmartCadConfiguration.UpgradeSchemaDataDB).FullName));
                }
                finally
                {
                    if (reader != null)
                    {
                        reader.Close();
                        reader.Dispose();
                    }

                    if (command != null)
                        command.Dispose();

                    if (connection != null)
                    {
                        connection.Close();
                        connection.Dispose();
                    }

                    if (throwException == true)
                        throw new Exception(ResourceLoader.GetString2("DBSchemaNotUpdated"));
                }
            }
            else
                throw new FileNotFoundException(ResourceLoader.GetString2("CheckFileNotFound"));
        }
        #endregion

        #region Lazy initialization

        public static void InitializeLazy(ISession session, ObjectData obj, object list)
        {
            if (obj.Code != 0)
            {
                session.Lock(obj, NHibernate.LockMode.None);
                NHibernateUtil.Initialize(list);
            }
        }

        public static void InitializeLazy(ObjectData obj, object list)
        {
            ISession session = NewSession();

            try
            {
                InitializeLazy(session, obj, list);
            }
            catch
            {
            }
            finally
            {
                CloseSession(session);
            }
        }

        public static bool IsInitialize(object list)
        {
            bool result = false;
            if ((list != null) &&
                (list is PersistentBag))
            {
                result = ((PersistentBag)list).WasInitialized;
            }
            else if ((list != null) &&
                     (list is PersistentList))
            {
                result = ((PersistentList)list).WasInitialized;
            }
            else if ((list != null) &&
                     (list is PersistentSet))
            {
                result = ((PersistentSet)list).WasInitialized;
            }
            else if ((list != null) &&
                (list is ArrayList))
            {
                result = true;
            }
            else if ((list != null) &&
                (list is Iesi.Collections.HashedSet))
            {
                result = true;
            }
            return result;
        }

        #endregion

        #region Session managment

        public static ISession NewSession()
        {
            for (int i = 0; i < 20; i++)
            {
                try
                {
                    ISession session = null;
                    IDbConnection connection = null;
                    connection = new SqlConnection(configuration.Properties["connection.connection_string"].ToString());
                    session = SessionFactory.OpenSession(connection);
                    connection.Open();

                    return session;
                }
                catch (Exception ex)
                {
                    SmartLogger.Print(ex);

                    Thread.Sleep(1000);
                }
            }
            //Terminate server process
            Process.GetCurrentProcess().Kill();

            throw new DatabaseServerException(
                ResourceLoader.GetString2("ConnectDataBaseException"),
                null);
        }

        public static void CloseSession(ISession session)
        {
            try
            {
                if (session != null)
                {
                    session.Close().Dispose();
                    session.Dispose();
                }
            }
            catch
            {
            }
		}

        #endregion				
	}

    [Serializable]
    public enum OperatorActions 
    {
        None,
        Save,
        Update,
        Delete,
        Logout
    }

    [Serializable]
    public class CommittedDataEventArgs : EventArgs
    {
        #region Fields

        private ObjectData objectData;
        private CommittedDataAction action = CommittedDataAction.None;
        private object data;

        #endregion


        #region Properties
        public ObjectData ObjectData
        {
            get
            {
                return objectData;
            }

        }

        public CommittedDataAction Action
        {
            get
            {
                return action;
            }
        }

        public object Data
        {
            get
            {
                return data;
            }
        }
        #endregion

        public CommittedDataEventArgs(ObjectData objectData, CommittedDataAction action)
        {
            this.objectData = objectData;
            this.action = action;
        }

        public CommittedDataEventArgs(ObjectData objectData, CommittedDataAction action, object data)
            : this(objectData, action)
        {
            this.data = data;
        }

        public CommittedDataEventArgs(object obj, CommittedDataAction action)
        {
            this.data = obj;
            this.action = action;
        }
    }

    [Serializable]
    public class DBConnectionLostException : FaultException
    {
        public DBConnectionLostException()
            : base()
        { }

        public DBConnectionLostException(string reason)
            : base(reason)
        {
        }
    }
}
