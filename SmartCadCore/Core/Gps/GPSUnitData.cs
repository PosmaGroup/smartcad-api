using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.Core
{
    [Serializable] 
    public class GPSUnitData
    {
        #region Fields

        public string idGPS = null;
        public double lat;
        public double lon;
        public int alt;
        public double heading;
        public double speed;
        public int satellites;
        public DateTime receiveDate;
        public DateTime date;
        public int unitCode;
        public int stop;
        public IList<int> inputs = new List<int>();
        public IList<int> outputs = new List<int>();
        private string p;

        #endregion

        #region Constructor

        public GPSUnitData()
        {
        }

        public GPSUnitData(string frame)
        {
            string[] data = frame.Split('|');

            idGPS = data[1];
            date = DateTime.ParseExact(data[2], ApplicationUtil.DateNHibernateFormatWithSeconds, System.Globalization.CultureInfo.InvariantCulture);
            lat = double.Parse(data[4], new System.Globalization.CultureInfo("es-VE"));
            lon = double.Parse(data[3], new System.Globalization.CultureInfo("es-VE"));
            alt = int.Parse(data[5]);
            speed = double.Parse(data[6]);
            heading = double.Parse(data[7]);
            satellites = int.Parse(data[8]);
            receiveDate = DateTime.Now;
        }

        public GPSUnitData(string frame, int stop)
        :this(frame)
        {
            this.stop = stop;
        }

        #endregion
    }
}
