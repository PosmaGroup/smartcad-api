﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.Statistic;
using System.Data.Odbc;
using SmartCadCore.Common;
using System.IO;
using System.Threading;
using SmartCadCore.Core.Util;

namespace SmartCadCore.Core
{
    public class AvayaStatWrapper 
    {
        private static string connectionString;

        private static OdbcConnection conn = null;
        private static bool initializing = false;

        public static double GetData(StatisticRetriever.StatisticNames statisticType)
        {
            double value = 0;
            if (IsInitiated())
            {
                switch (statisticType)
                {
                    case StatisticRetriever.StatisticNames.L02TotalCallsAbandoned:
                        //obtiene todas las llamadas que fueron abandonadas // HOY         
                        value = GetValue(string.Format(
                            "SELECT count(*) FROM call_rec WHERE disposition in (3) AND ROW_DATE = '{0}'",
                            DateTime.Now.ToString("MM/dd/yyyy")));
                        return value;
                    case StatisticRetriever.StatisticNames.L05CurrentCallsInQueue:
                     
                        break;
                    case StatisticRetriever.StatisticNames.L06CurrMaxCallWaitingTime:
                        //Obtiene el maximo tiempo que una llamada ha estado en espera. HOY. en secs
                        value = GetValue(string.Format(
                            "SELECT avg(ANSTIME) FROM hsplit WHERE ANSTIME!= 0 AND ROW_DATE = '{0}'",
                            DateTime.Now.ToString("MM/dd/yyyy")));
                        return value;
                    case StatisticRetriever.StatisticNames.L09TotalCallsEntered:
                        //obtiene todas las llamadas que NO fueron abandonadas
                        value = GetValue(string.Format(
                            "SELECT count(*) FROM call_rec WHERE disposition not in (3) AND ROW_DATE = '{0}'",
                            DateTime.Now.ToString("MM/dd/yyyy")));
                        return value;
                    case StatisticRetriever.StatisticNames.L11TotalCallsAbandonedWR:
                        //obtiene el numero de llamadas que se asignaron a un operador y este no las atendio. HOY
                        value = GetValue(string.Format(
                            "SELECT sum(NOANSREDIR) FROM hsplit WHERE ROW_DATE = '{0}'",
                            DateTime.Now.ToString("MM/dd/yyyy")));
                        return value;
                    case StatisticRetriever.StatisticNames.L12ServiceLevel:

                        double answeredOrAbandonedOnTime = GetValue(string.Format(
                           "SELECT count(*) FROM call_rec WHERE disposition in (2,3) " +
                           "AND RINGTIME + QUEUETIME < {0} " +
                           "AND ROW_DATE = '{1}'",
                           10,
                           DateTime.Now.ToString("MM/dd/yyyy")));

                        double totalAnsweredOrAbandoned = GetValue(string.Format(
                          "SELECT count(*) FROM call_rec WHERE disposition in (2,3) " +
                          "AND ROW_DATE = '{0}'",
                          DateTime.Now.ToString("MM/dd/yyyy")));

                        //the app shows this value * 100. AA
                        if (totalAnsweredOrAbandoned > 0)
                            return answeredOrAbandonedOnTime / totalAnsweredOrAbandoned;
                        else
                            return 0; //if no calls -> service level is 0% //issue
                    default:
                        break;
                }
            }
            else
            {
                if (conn == null || conn.State != System.Data.ConnectionState.Open)
                {
                   //InitHistorical();
                }
                else
                {
                    SmartLogger.Print("Statistic database is not connected.");
                }
            }
            return 0;
        }

        public static bool IsInitiated()
        {
            return (conn != null && conn.State == System.Data.ConnectionState.Open /*&& reportID != null*/);
        }

        /// <summary>
        /// CONNECTION STRING EXAMPLE
        ///    "Driver={IBM INFORMIX ODBC DRIVER};" +
        ///    "Host=10.2.16.57;" +
        ///    "Server=cms_net;" +
        ///    "Service=50000;" +
        ///    "Protocol=olsoctcp;" +
        ///    "Database=cms;" +
        ///    "Uid=TrndCMS;" +
        ///    "Pwd=w3lc0me;" +
        ///    "CLIENT_LOCALE=en_US.UTF8;" +
        ///    "DB_LOCALE=en_US.UTF8;"
        /// </summary>
        /// <param name="connectionString"></param>
        public static void Init(string connString)
        {
            if (initializing == false && connString != "")
            {
                initializing = true;

                connectionString = connString;
                //InitHistorical();
                //InitRealTime();

                initializing = false;
            }
        }

        public static void InitRealTime()
        {
            //Thread realTimeThread = new Thread(() =>
            //{
            //    try
            //    {
            //    }
            //    catch (Exception ex)
            //    {
            //        StopRealTime();
            //        SmartLogger.Print(ex.Message);
            //    }
            //});
            //realTimeThread.Start();
        }




        public static void StopRealTime()
        {
            //try
            //{
                
            //}
            //catch { };
        }

        public static void InitHistorical()
        {
            conn = new OdbcConnection();
            conn.ConnectionString = connectionString;
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex.Message);
                StopHistorical();
            }
        }

        public static void StopHistorical()
        {
            try
            {
                if (conn != null)
                {
                    conn.Dispose();
                    conn = null;
                }
            }
            catch { };
        }

        public static double GetValue(string query)
        {
            try
            {
                OdbcCommand command = new OdbcCommand(query);
                command.Connection = conn;

                OdbcDataReader reader = command.ExecuteReader();
                reader.Read();
                return double.Parse(reader.GetValue(0).ToString());
            }
            catch
            {
                return 0;
            }
        }

        public static void Stop()
        {
            StopHistorical();
            StopRealTime();
        }
    }
}
