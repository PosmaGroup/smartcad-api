﻿using MySql.Data.MySqlClient;
using SmartCadCore.Common;
using SmartCadCore.Statistic;
using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Linq;
using System.Text;

namespace SmartCadCore.Core.Util
{
    public class AsteriskStatWrapper
    {
        private static string ConnectionString, Connection;
        private static  MySqlConnection DbConnection = null;
        private static bool Initializing=false;


        public static bool IsInitiated() 
        {
            return (DbConnection != null && DbConnection.State == System.Data.ConnectionState.Open);
        }

        public static void Init(string connection) 
        {
            if (!Initializing && connection != null) 
            {
                Initializing = true;
                ConnectionString = connection;
                Initializing = false; 
            }
        }

        public static void InitRealTime() 
        {
            throw new NotImplementedException();
        }

        public static void StopRealTime() 
        {
            throw new NotImplementedException();
        }

        private static double GetValue(string query) 
        {
            try
            {
                MySqlCommand command = new MySqlCommand(query, DbConnection);
                MySqlDataReader reader = command.ExecuteReader();
                double value=0;

                while (reader.Read())
                {
                    //Console.WriteLine(reader.GetString(0));
                    value = double.Parse(reader.GetString(0));
                }
                reader.Close();

                return value;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public static double GetData(StatisticRetriever.StatisticNames statisticType) 
        {
            double value = 0;
            double totalAnsweredCalls = 0;
            double totalAbandonedCalls = 0;
            double totalAbandonedCallsLess10s = 0;
            double totalAnsweredCallsLess10s = 0;
            if (IsInitiated())
            {
                switch (statisticType)
                {
                    case StatisticRetriever.StatisticNames.L02TotalCallsAbandoned:
                        //obtiene todas las llamadas que fueron abandonadas // HOY         
                        value = GetValue(string.Format(
                            "SELECT COUNT(distinct H.id) as Calls FROM asteriskcdrdb.cdr, (SELECT DATE_FORMAT(calldate, '%Y-%m-%d') AS count_date, uniqueid AS id FROM asteriskcdrdb.cdr WHERE disposition = 'NO ANSWER' and lastapp = 'Queue' and dcontext='from-internal' and uniqueid NOT IN (SELECT cdr1.uniqueid FROM asteriskcdrdb.cdr AS cdr1 WHERE disposition = 'ANSWERED')) AS H WHERE H.count_date = CURDATE();"));
                        return value;

                    case StatisticRetriever.StatisticNames.L05CurrentCallsInQueue:
                        //Cantidad de llamadas en “Espera” HOY
                        value = GetValue(string.Format(
                            "SELECT COUNT(distinct H.id) FROM asteriskcdrdb.cdr, (SELECT DATE_FORMAT(calldate, '%Y-%m-%d') AS count_date, uniqueid AS id FROM asteriskcdrdb.cdr WHERE disposition = 'BUSY' and lastapp = 'Queue' and dcontext='from-internal') AS H WHERE H.count_date = CURDATE(); "));
                        return value;

                    case StatisticRetriever.StatisticNames.L06CurrMaxCallWaitingTime:
                        //Obtiene el maximo tiempo que una llamada ha estado en espera. HOY. en secs
                        value = GetValue(string.Format(
                            "SELECT H.avg_duration as DURATION FROM asteriskcdrdb.cdr, (SELECT AVG(duration) AS avg_duration , DATE_FORMAT(calldate, '%Y-%m-%d') AS avg_date FROM asteriskcdrdb.cdr WHERE lastapp = 'Queue' and (disposition = 'BUSY' or disposition = 'NO ANSWER' and dcontext='from-internal') and uniqueid NOT IN (SELECT cdr1.uniqueid FROM asteriskcdrdb.cdr AS cdr1 WHERE disposition = 'ANSWERED') group by DATE_FORMAT(calldate, '%Y-%m-%d')) AS H WHERE H.avg_date=CURDATE() LIMIT 1;"));
                        return value;

                    case StatisticRetriever.StatisticNames.L09TotalCallsEntered:
                        //Cantidad de llamadas recibidas
                        value = GetValue(string.Format(
                            "SELECT COUNT(distinct H.id) as Calls FROM asteriskcdrdb.cdr, (SELECT DATE_FORMAT(calldate, '%Y-%m-%d') AS count_date, uniqueid AS id FROM asteriskcdrdb.cdr WHERE dcontext='from-internal') AS H WHERE H.count_date = CURDATE();"));
                        return value;
                    case StatisticRetriever.StatisticNames.L11TotalCallsAbandonedWR:
                        //obtiene el numero de llamadas que se asignaron a un operador y este no las atendio. HOY
                        value = GetValue(string.Format(
                            "SELECT H.num as Numbers FROM (SELECT count(distinct uniqueid) AS num , DATE_FORMAT(calldate, '%Y-%m-%d') AS num_date FROM asteriskcdrdb.cdr WHERE lastapp = 'Queue' and dcontext='from-internal' and (disposition = 'BUSY' or disposition = 'NO ANSWER') and uniqueid NOT IN (SELECT cdr1.uniqueid FROM asteriskcdrdb.cdr AS cdr1 WHERE disposition = 'ANSWERED') group by DATE_FORMAT(calldate, '%Y-%m-%d')) AS H WHERE H.num_date=CURDATE() LIMIT 1;"));
                        return value;

                    case StatisticRetriever.StatisticNames.L12ServiceLevel:
                        //Nivel de Servicio del Call Center (TSF)
                        totalAnsweredCalls = GetValue(string.Format(
                            "SELECT COUNT(*) FROM asteriskcdrdb.cdr WHERE disposition = 'ANSWERED' AND dcontext='from-internal';"));
                        totalAbandonedCalls = GetValue(string.Format(
                            "SELECT count(DISTINCT H.id) FROM asteriskcdrdb.cdr, (SELECT DISTINCT uniqueid AS id, COUNT(*) AS cantidad FROM asteriskcdrdb.cdr WHERE disposition = 'NO ANSWER' AND dcontext='from-internal' AND uniqueid NOT IN (SELECT cdr1.uniqueid FROM asteriskcdrdb.cdr AS cdr1 WHERE disposition = 'ANSWERED') group by uniqueid) H; "));
                        totalAbandonedCallsLess10s = GetValue(string.Format(
                            "SELECT count(DISTINCT H.id) FROM asteriskcdrdb.cdr, (SELECT DISTINCT uniqueid AS id, COUNT(*) AS cantidad FROM asteriskcdrdb.cdr WHERE disposition = 'NO ANSWER' AND dcontext='from-internal' AND uniqueid NOT IN (SELECT cdr1.uniqueid FROM asteriskcdrdb.cdr AS cdr1 WHERE disposition = 'ANSWERED') AND (duration - billsec) < 10 group by uniqueid) H;  "));
                        totalAnsweredCallsLess10s = GetValue(string.Format(
                            "SELECT COUNT(*) FROM asteriskcdrdb.cdr WHERE disposition = 'ANSWERED' AND dcontext='from-internal' AND (duration-billsec)<10; "));
                        value = (totalAbandonedCallsLess10s + totalAnsweredCallsLess10s) / (totalAnsweredCalls + totalAbandonedCalls);

                        if (value > 0)
                            return value;
                        else
                            return 0; //if no calls -> service level is 0% 
         

                }
            }
            else
            {
                if (DbConnection == null || DbConnection.State != System.Data.ConnectionState.Open)
                {
                    //Connection = "server=" + SmartCadConfiguration.SmartCadSection.ServerElement.TelephonyServerStatisticElement.Server + "; user=" + SmartCadConfiguration.SmartCadSection.ServerElement.TelephonyServerStatisticElement.ServiceName + "; database= asteriskcdrbd; port=" + SmartCadConfiguration.SmartCadSection.ServerElement.TelephonyServerStatisticElement.Port + "; password=" + SmartCadConfiguration.SmartCadSection.ServerElement.TelephonyServerStatisticElement.ServerPassword + "; ";
                    //TelephonyServerStatisticElement telephonyConfig = SmartCadConfiguration.SmartCadSection.ServerElement.TelephonyServerStatisticElement;
                   // Init(Connection);
                    InitHistorical();
                }
                else
                {
                    SmartLogger.Print("Statistic database is not connected.");
                }
            }
            return 0;
        }

        public static void InitHistorical() 
        {
            DbConnection = new MySqlConnection(ConnectionString);
            try
            {
                DbConnection.Open();
            }
            catch(Exception ex)
            {
                SmartLogger.Print(ex.Message);
                StopHistorical();
            }        
        }

        private static void StopHistorical()
        {
            try
            {
                if (DbConnection != null)
                {
                    DbConnection.Close();
                    DbConnection = null;
                }
            }
            catch 
            { 
            
            };
        }

        public static void Stop() 
        {
            throw new NotImplementedException();
        }
    }
}
