using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.IO;

namespace SmartCadCore.Core
{
    #region Class FormUtil Documentation
    /// <summary>
    /// TODO:
    /// </summary>
    /// <className>FormUtil</className>
    /// <author email="atorres@smartmatic.com">Alden Torres</author>
    /// <date>2007/02/06</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    public static class FormUtil
    {
        public static void InvokeRequired(Control control, MethodInvoker methodInvoker)
        {
            if (control.InvokeRequired)
            {
                control.Invoke(methodInvoker);
            }
            else
            {
                methodInvoker();
            }
        }

        public static Image ImageFromArray(byte[] imageRaw)
        {
            return Image.FromStream(new MemoryStream(imageRaw));
        }

        public static byte[] ImageToArray(Image image)
        {
            MemoryStream ms = new MemoryStream();
            image.Save(ms, image.RawFormat);

            return ms.ToArray();
        }

        public static Color ControlColor
        {
            get
            {
                return Color.FromArgb(255, 236, 233, 216);
            }
        }

        public static Color WhiteColor
        {
            get
            {
                return Color.FromArgb(255, 255, 255, 255);
            }
        }

        public static Color MakeColor(Color color, int diff)
        {
            int r = color.R + diff;
            r = (r < 0) ? 0 : r;
            r = (r > 255) ? 255 : r;

            int g = color.G + diff;
            g = (g < 0) ? 0 : g;
            g = (g > 255) ? 255 : g;

            int b = color.B + diff;
            b = (b < 0) ? 0 : b;
            b = (b > 255) ? 255 : b;

            return Color.FromArgb(color.A, r, g, b);
        }
    }
}
