using System;
using System.Collections.Generic;
using System.Text;
using System.Management;
using SmartCadCore.Common;

namespace SmartCadCore.Core
{
    public class WMI_NlbNode
    {
        #region CONST WMI INFO
        public const int HIGHEST_PRIORITY = 1;
        private const string LOCALMACHINE = ".";
        private const string NAMESPACE = "MicrosoftNLB";
        private const string CLASSNAME = "MicrosoftNLB_Node";
        #endregion
        #region CONST PROPERTIES
        private const string NAME = "Name";
        private const string HOSTPRIORITY = "HostPriority";
        private const string DEDICATEDIPADDRESS = "DedicatedIPAddress";
        private const string CREATIONCLASSNAME = "CreationClassName";
        private const string COMPUTERNAME = "ComputerName";
        private const string STATUSCODE = "StatusCode";
        #endregion
        #region CONST METHODS
        private const string START = "Start";
        private const string STOP = "Stop";
        #endregion

        #region Fields
        private ManagementObject node = null;
        private static List<WMI_NlbNode> allNodes = null;
        private static List<string> allComputerNameNodes = null;
        private static List<string> allDedicatedIPsNodes = null;
        private static WMI_NlbNode current = null;
        #endregion

        #region Constructors
        public WMI_NlbNode()
        {
            GetNode(Environment.MachineName);
        }

        public WMI_NlbNode(ManagementObject node)
        {
            this.node = node;
        }
        #endregion

        #region Properties
        public static WMI_NlbNode Current
        {
            get
            {
                if (current == null)
                    current = new WMI_NlbNode();
                else
                    current.Refresh();
                return current;
            }
        }
        public ManagementObject Node
        {
            get
            {
                return node;
            }
        }

        public string Name
        {
            get
            {
                if (node != null)
                    return node[NAME].ToString();
                else
                    return string.Empty;
            }
        }

        public string ClusterIP
        {
            get
            {
                if (node != null)
                {

                    string name = node[NAME].ToString();
                    return name.Substring(0, name.IndexOf(':'));
                }
                else
                    return string.Empty;
            }
        }

        public int HostPriority
        {
            get
            {
                if (node != null)
                    return int.Parse(node[HOSTPRIORITY].ToString());
                else
                    return 0;
            }
        }

        public string DedicatedIPAddress
        {
            get
            {
                if (node != null)
                    return node[DEDICATEDIPADDRESS].ToString();
                else
                    return string.Empty;
            }
        }

        public string CreationClassName
        {
            get
            {
                if (node != null)
                    return node[CREATIONCLASSNAME].ToString();
                else
                    return string.Empty;
            }
        }

        public string ComputerName
        {
            get
            {
                if (node != null)
                    return node[COMPUTERNAME].ToString();
                else
                    return string.Empty;
            }
        }

        public NLB_ReturnValues StatusCode
        {
            get
            {
                if (node != null)
                    return (NLB_ReturnValues)int.Parse(node[STATUSCODE].ToString());
                else
                    return NLB_ReturnValues.WLBS_UNKNOWN;
            }
        }

        public bool Running
        {
            get
            {
                bool result = false;
                if (node != null)
                    result = StatusCode.Equals(NLB_ReturnValues.WLBS_CONVERGED) ||
                                StatusCode.Equals(NLB_ReturnValues.WLBS_DEFAULT);
                return result;
            }
        }

        public bool StartingOrRunning
        {
            get
            {
                bool result = false;
                if (node != null)
                    result = Running ||
                                StatusCode.Equals(NLB_ReturnValues.WLBS_CONVERGING);
                return result;
            }
        }
        #endregion

        #region Methods
        private static string GetScope(string computerName)
        {
            return string.Format(@"\\{0}\root\{1}", computerName, NAMESPACE);
        }

        private static string GetSearch(string where)
        {
            if (string.IsNullOrEmpty(where) == true)
                return string.Format(@"SELECT * FROM {0}", CLASSNAME);
            else
                return string.Format(@"SELECT * FROM {0} where {1}", CLASSNAME, where);
        }

        private static string GetSearch()
        {
            return GetSearch(string.Empty);
        }

        private void GetNode(string computerName)
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(
                WMI_NlbNode.GetScope(computerName),
                WMI_NlbNode.GetSearch(COMPUTERNAME + " like '" + computerName + "%'"),
                new EnumerationOptions());
            foreach (ManagementObject refreshNode in searcher.Get())
            {
                node = refreshNode;
            }
        }

        public static bool CheckNlb()
        {
            bool result = false;

            try
            {
                ManagementObjectSearcher searcher = new ManagementObjectSearcher(
                    GetScope(Environment.MachineName),
                    GetSearch(),
                    new EnumerationOptions());
                foreach (ManagementObject node in searcher.Get())
                {
                    result = true;
                    break;
                }
            }
            catch (ManagementException)
            {
                //This indicate that the machine actually have nlb but is not configured.
                //It's here in case we use it later.
                //if (ex.ErrorCode == ManagementStatus.ProviderLoadFailure)
                result = false;
            }
            catch
            {
                result = false;
            }
            return result;
        }

        public static List<WMI_NlbNode> AllNodes()
        {
            if (allNodes == null)
            {
                allNodes = new List<WMI_NlbNode>();

                ManagementObjectSearcher searcher = new ManagementObjectSearcher(
                    GetScope(Environment.MachineName),
                    GetSearch(),
                    new EnumerationOptions());
                foreach (ManagementObject node in searcher.Get())
                {
                    allNodes.Add(new WMI_NlbNode(node));
                }
            }

            return allNodes;
        }

        public static List<string> AllComputerNameNodes()
        {
            if (allComputerNameNodes == null)
            {
                allComputerNameNodes = new List<string>();

                ManagementObjectSearcher searcher = new ManagementObjectSearcher(
                    GetScope(Environment.MachineName),
                    GetSearch(),
                    new EnumerationOptions());
                foreach (ManagementObject node in searcher.Get())
                {
                    allComputerNameNodes.Add(new WMI_NlbNode(node).ComputerName);
                }
            }

            return allComputerNameNodes;
        }

        public static List<string> AllDedicatedIPsNodes()
        {
            if (allDedicatedIPsNodes == null)
            {
                allDedicatedIPsNodes = new List<string>();

                ManagementObjectSearcher searcher = new ManagementObjectSearcher(
                    GetScope(Environment.MachineName),
                    GetSearch(),
                    new EnumerationOptions());
                foreach (ManagementObject node in searcher.Get())
                {
                    allDedicatedIPsNodes.Add(new WMI_NlbNode(node).DedicatedIPAddress);
                }
            }

            return allDedicatedIPsNodes;
        }

        public WMI_NlbNode Refresh()
        {
            try
            {
                GetNode(ComputerName);
            }
            catch (Exception)
            {
            }
            return this;
        }

        public void Start()
        {
            if (node != null)
                node.InvokeMethod(START, null);
        }

        public void Stop()
        {
            if (node != null)
                node.InvokeMethod(STOP, null);
        }
        #endregion
    }

    public enum NLB_ReturnValues
    {
        WLBS_UNKNOWN = 0,                      //The node is not specific
        WLBS_OK = 1000,                 //The operation completed successfully.
        WLBS_ALREADY = 1001,            //The specified target is already in the state that the requested operation would produce.
        WLBS_DRAIN_STOP = 1002,	        //One or more nodes reported a drain stop operation.
        WLBS_BAD_PARAMS = 1003,	        //Bad configuration parameters in a node's registry prevented the node from starting cluster operations.
        WLBS_NOT_FOUND = 1004,         //The specified port number was not found in any port rule.
        WLBS_STOPPED = 1005,	        //Cluster operations have stopped on at least one node.
        WLBS_CONVERGING = 1006,	        //The cluster is converging.
        WLBS_CONVERGED = 1007,	        //The cluster has converged successfully.
        WLBS_DEFAULT = 1008,	        //The specified node has converged as the default host.
        WLBS_DRAINING = 1009,	        //One or more nodes are draining.
        WLBS_SUSPENDED = 1013,	        //Cluster operations have been suspended on one or more nodes.
        WLBS_MEDIA_DISCONNECTED = 1014,	//The network adapter cable is disconnected.
        WLBS_REBOOT = 1050,	            //The node must be rebooted for the specified configuration changes to take effect.
        WLBS_INIT_ERROR = 1100,	        //An internal error prevented initialization of the cluster control module.
        WLBS_BAD_PASSW = 1101,	        //The specified password was not accepted by the cluster.
        WLBS_IO_ERROR = 1102,	        //A local I/O error prevents communication with the Network Load Balancing driver.
        WLBS_TIMEOUT = 1103,	        //The requested operation timed out without receiving a response from the specified node.
        WLBS_PORT_OVERLAP = 1150,	    //At least one of the port numbers in the specified port rule is currently listed in at least one other port rule.
        WLBS_BAD_PORT_PARAMS = 1151,	//The settings for one or more port rules contain one or more invalid values.
        WLBS_MAX_PORT_RULES = 1152,	    //The cluster contains the maximum number of port rules.
        WLBS_TRUNCATED = 1153,	        //The return value has been truncated.
        WLBS_REG_ERROR = 1154	        //An internal registry access error occurred.
    }
}
