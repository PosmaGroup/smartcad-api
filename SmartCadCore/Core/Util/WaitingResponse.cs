using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace SmartCadCore.Core
{
    public class WaitingResponseManager
    {
        private static Dictionary<string, WaitingResponse> list = new Dictionary<string, WaitingResponse>();
        private static object sync = new object();

        static WaitingResponseManager()
        { }

        public static void Create(string name)
        {
            Create(name, true);
        }

        public static void Create(string name, bool form)
        {
            lock (sync)
            {
                WaitingResponse wr = new WaitingResponse(name, form);
                wr.Idle += new EventHandler(wr_Idle);
                list.Add(name, wr);
            }
        }

        public static WaitingResponse Get(string name)
        {
            lock (sync)
            {
                if (list.ContainsKey(name) == true)
                    return list[name];
                else
                    return null;
            }
        }

        public static WaitingResponse CreateAndGet(string name, bool form)
        {
            WaitingResponse wr = null;
            if ((wr = Get(name)) == null)
            {
                Create(name, form);
                wr = Get(name);
            }
            return wr;
        }

        private static void Remove(string name)
        {
            lock (sync)
            {
                if (list.ContainsKey(name) == true)
                    list.Remove(name);
            }
        }

        private static void wr_Idle(object sender, EventArgs e)
        {
            WaitingResponse wr = (WaitingResponse)sender;
            Remove(wr.Name);
        }
    }

    public class WaitingResponse
    {
        private const int TIMER_START = 30000;
        private bool wait = false;
        private bool timerExpired = false;
        private System.Threading.Timer timer;
        private object result;
        private object sync = new object();
        private readonly bool Form;
        private EventWaitHandle signal;
        private string name;
        private System.Threading.Timer idle;
        public event EventHandler Idle;

        public WaitingResponse(string name, bool form)
        {
            this.Form = form;
            this.name = name;
            this.idle = new System.Threading.Timer(IdleExpire);
        }

        public WaitingResponse(string name)
            :this(name, false)
        {}

        public bool Wait
        {
            get
            {
                lock (sync)
                {
                    return wait;
                }
            }
            set
            {
                lock (sync)
                {
                    wait = value;
                }
            }
        }

        public object Result
        {
            get
            {
                lock (sync)
                {
                    return result;
                }
            }
            set
            {
                lock (sync)
                {
                    result = value;
                }
            }
        }

        public EventWaitHandle Signal
        {
            get
            {
                lock (sync)
                {
                    return signal;
                }
            }
        }

        public string Name
        {
            get
            {
                lock (sync)
                {
                    return name;
                }
            }
        }

        public void WaitResponse()
        {
            idle.Change(-1, -1);
            while (Wait == true)
            {
                if (Form)
                    Application.DoEvents();
                Thread.Sleep(0);
            }
            idle.Change(60000, -1);
            if (timerExpired == true)
            {
                throw new TimeoutException();
            }
            else
            {
                timer.Change(Timeout.Infinite, Timeout.Infinite);
                timer.Dispose();
            }
        }

        public void Init()
        {
            Wait = true;
            timer = new System.Threading.Timer(new TimerCallback(TimerExpire), null, TIMER_START, Timeout.Infinite);
            signal = new EventWaitHandle(false, EventResetMode.ManualReset, name);
        }

        public void TimerExpire(object info)
        {
            timerExpired = true;
            timer.Dispose();
            Wait = false;
        }

        private void IdleExpire(object info)
        {
            if (Idle != null)
                Idle(this, new EventArgs());
        }
    }
}

