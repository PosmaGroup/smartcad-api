﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Collections;
using System.Threading;
using SmartCadCore.Model;
using SmartCadCore.Common;

namespace SmartCadCore.Core
{
    public class NortelRTDWrapper
    {
        #region Fields
        private static List<NortelRTDStatItem> items = new List<NortelRTDStatItem>();
        private static List<string> columnNames = new List<string>();
        private static List<uint> columnCodes = new List<uint>();
        private static IntPtr authPointer = IntPtr.Zero;
        private static IntPtr query = IntPtr.Zero;
        private static IntPtr myPointer = IntPtr.Zero;
        private static uint requestId = 0;
        private static Dictionary<uint, NIrtd_stValue> historyData = new Dictionary<uint, NIrtd_stValue>();
        private static DateTime lastRun = DateTime.MinValue;
        private static int script = 0;

        private static string[] parameters;
        #endregion

        #region Properties
        public static List<NortelRTDStatItem> Items
        {
            get
            {
                return items;
            }
        }
        #endregion

        #region Import Methods

        [DllImport("nirtd.dll")]
        private static extern uint NIrtd_freeRow(ref IntPtr row);

        [DllImport("nirtd.dll")]
        private static extern uint NIrtd_getCol
            (IntPtr value,
             ref IntPtr row,
             uint index);

        [DllImport("nirtd.dll")]
        private static extern void NIrtd_freeValue(ref IntPtr value);

        [DllImport("nirtd.dll")]
        private static extern uint NIrtd_allocateValue(IntPtr value);

        [DllImport("nirtd.dll")]
        private static extern uint NIrtd_allocateRow(
            ref IntPtr row,
            IntPtr table,
            uint index);

        [DllImport("nirtd.dll")]
        private static extern uint NIrtd_freeTableGroup(IntPtr table);

        [DllImport("nirtd.dll")]
        private static extern uint NIrtd_stopDataStream(
            ref IntPtr authorization,
            uint requestId);

        [DllImport("nirtd.dll")]
        private static extern uint NIrtd_removeNameCacheforDataColumn(
            ref IntPtr authorization,
            uint column);

        [DllImport("nirtd.dll")]
        private static extern uint NIrtd_startDataStream(
                    ref IntPtr authInfo,
                    ref IntPtr query,
                    uint updateRate,
                    RealTimeCallBackDelegate callback,
                    ref IntPtr appPtr,
                    ref uint request);
        
        [DllImport("nirtd.dll")]
        private static extern uint NIrtd_freeQuery(ref IntPtr query);
        
        [DllImport("nirtd.dll")]
        private static extern uint NIrtd_selectColumn(
            ref IntPtr query,
            uint column);

        [DllImport("nirtd.dll")]
        private static extern uint NIrtd_allocateQuery(
            ref IntPtr query,
            uint table);

        [DllImport("nirtd.dll")]
        private static extern uint NIrtd_logout(ref IntPtr authorization);

        [DllImport("nirtd.dll")]
        private static extern uint NIrtd_getNameCacheforDataColumn(
            ref IntPtr authorization,
            uint column);

        [DllImport("nirtd.dll")]
        private static extern uint NIrtd_login(
            ref IntPtr authorization,
            [MarshalAs(UnmanagedType.LPStr)]String servname,
            [MarshalAs(UnmanagedType.LPStr)]String userID,
            [MarshalAs(UnmanagedType.LPStr)]String passWord);

        [DllImport("nirtd.dll")]
        private static extern uint NIrtd_setRecovery(
            uint pullPlugTime,
            uint wakeupGranularity);
        

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        private delegate uint RealTimeCallBackDelegate(uint return_code,
        uint requestId,
        IntPtr tableGroup,
        IntPtr appPtr);

        private static RealTimeCallBackDelegate RealTimeCallBack = new RealTimeCallBackDelegate(RealTimeCallBackHandler);
        #endregion

        private static uint RealTimeCallBackHandler(uint return_code,
               uint requestId,
               IntPtr tableGroupStruct,
               IntPtr appPtr)
        {
            try
            {
                bool recovery = false;
                if (return_code == (int)NRTD_Errors.NIrtd_eOK)
                {
                    TableGroupStruct tgs = (TableGroupStruct)Marshal.PtrToStructure(tableGroupStruct, typeof(TableGroupStruct));
                    if (tgs.DeletedValues.NumberOfRows > 0)
                    {
                        DeleteValues(tgs.DeletedValues);
                    }

                    if (tgs.NewValues.NumberOfRows > 0)
                    {
                        NewValues(tgs.NewValues);
                    }

                    if (tgs.DeltaValues.NumberOfRows > 0)
                    {
                        UpdateValues(tgs.DeltaValues);
                    }

                    if (lastRun != DateTime.MinValue && lastRun.Date < DateTime.Now.Date)
                        historyData.Clear();

                    lastRun = DateTime.Now;

                    SaveValues();

#if DEBUG
                    PrintValues();
#endif

                    recovery = true;
                }
                else if ((int)NRTD_Errors.NIrtd_eSTART_RECOVERY == return_code)
                {
                    recovery = true;
                }
                else if ((int)NRTD_Errors.NIrtd_eOK_RECOVERY == return_code)
                {
                    recovery = true;
                }
                else if ((int)NRTD_Errors.NIrtd_eBAD_RECOVERY == return_code)
                {
                    recovery = true;
                }

                if (recovery == true)
                    NIrtd_freeTableGroup(tableGroupStruct);
            }
            catch (Exception ex)
            {
                SmartLogger.Print("RealTimeCallBackHandler: " +
                ex.ToString());
                Stop();
                bool run = false;
                while (run == false)
                {
                    uint result = Start(parameters);
                    if (result == 0)
                    {
                        SmartLogger.Print(ResourceLoader.GetString2("TelephonyDBStarted"));
                        run = true;
                    }
                    else
                    {
                        SmartLogger.Print(ResourceLoader.GetString2("TelephonyDBError"));
                        Thread.Sleep(10000);
                    }                    
                }
            }
            
            return 0;
        }

        private static void SaveValues()
        {
            List<NortelRTDStatItem> newList = new List<NortelRTDStatItem>(items);
            foreach (NortelRTDStatItem ssi in newList)
            {
                if (ssi.Data[0].Name == NRTD_Values.NIrtd_APPL_APPL_ID.ToString() &&
                    ssi.Data[0].Number == script)
                {
                    foreach (NIrtd_stValue value in ssi.Data)
                    {
                        if (historyData.ContainsKey(value.NameCode) == false)
                        {
                            historyData.Add(value.NameCode, value);

                            //Load value from database if server is restarted.
                            object result = null;
                            if (value.NameCode != (int)NRTD_Values.NIrtd_APPL_CALLS_WAITING &&
                                value.NameCode != (int)NRTD_Values.NIrtd_APPL_MAX_WAITING_TIME)
                            {
                                result = SmartCadDatabase.SearchBasicObject(
                                    SmartCadHqls.GetCustomHql(
                                    SmartCadHqls.GetTotalPerDayTelephonyField,
                                    value.Name,
                                    ApplicationUtil.GetDataBaseFormattedDate(
                                    SmartCadDatabase.GetTimeFromBD().Date)));
                            }
                            else if (value.NameCode == (int)NRTD_Values.NIrtd_APPL_MAX_WAITING_TIME)
                            {
                                result = SmartCadDatabase.SearchBasicObject(
                                    SmartCadHqls.GetCustomHql(
                                    SmartCadHqls.GetMaxPerDayTelephonyField,
                                    value.Name,
                                    ApplicationUtil.GetDataBaseFormattedDate(
                                    SmartCadDatabase.GetTimeFromBD().Date)));
                            }

                            if (result != null)
                            {
                                NIrtd_stValue historyValue = historyData[value.NameCode];
                                historyValue.Total = uint.Parse(result.ToString());
                                historyData[value.NameCode] = historyValue;
                            }
                        }
                        else
                        {
                            NIrtd_stValue historyValue = historyData[value.NameCode];
                            if (value.Number > historyValue.Number ||
                                value.NameCode == (int)NRTD_Values.NIrtd_APPL_CALLS_WAITING)
                            {
                                historyValue.Number = value.Number;
                            }
                            else if (value.Number == 0 && historyValue.Number > 0 &&
                                value.NameCode != (int)NRTD_Values.NIrtd_APPL_CALLS_WAITING)
                            {
                                TelephonyStatsHistoryData tshd = new TelephonyStatsHistoryData();
                                tshd.Name = historyValue.Name;
                                tshd.TimeStamp = SmartCadDatabase.GetTimeFromBD();
                                tshd.Value = historyValue.Number;
                                SmartCadDatabase.SaveOrUpdate(tshd);

                                if (value.NameCode == (int)NRTD_Values.NIrtd_APPL_MAX_WAITING_TIME &&
                                    historyValue.Total < historyValue.Number)
                                    historyValue.Total = historyValue.Number;
                                else if (value.NameCode != (int)NRTD_Values.NIrtd_APPL_MAX_WAITING_TIME)
                                    historyValue.Total += historyValue.Number;
                                historyValue.Number = 0;
                            }
                            historyData[value.NameCode] = historyValue;
                        }
                    }
                    break;
                }
            }
        }

        private static void NewValues(TableStruct newTable)
        {

            // structure used to stored temporary row
            IntPtr tempRow = IntPtr.Zero;
            IntPtr newTablePtr = Marshal.AllocHGlobal(Marshal.SizeOf(newTable));

            NIrtd_stValue value0 = new NIrtd_stValue();
            IntPtr value0Ptr = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(NIrtd_stValue)));
            try
            {
                // return code
                uint rc = (int)NRTD_Errors.NIrtd_eOK;

                Marshal.StructureToPtr(newTable, newTablePtr, false);
                for (int i = 0; i < newTable.NumberOfRows; i++)
                {
                    // copy the new row content to temp row
                    rc = NIrtd_allocateRow(ref tempRow, newTablePtr, (uint)i);
                    if (rc != (int)NRTD_Errors.NIrtd_eOK)
                    {
                        return;
                    }

                    NortelRTDStatItem tempStatItem = new NortelRTDStatItem();

                    for (int j = 0; j < newTable.NumberOfCols; j++)
                    {
                        // allocate the value structures in the tempStatItem

                        rc = NIrtd_allocateValue(value0Ptr);
                        if (rc != (int)NRTD_Errors.NIrtd_eOK)
                        {
                            NIrtd_freeValue(ref value0Ptr);
                            return;
                        }

                        // copy the skillset id
                        rc = NIrtd_getCol(value0Ptr, ref tempRow, (uint)j);
                        if (rc != (int)NRTD_Errors.NIrtd_eOK)
                        {
                            NIrtd_freeValue(ref value0Ptr);
                            return;
                        }
                        else
                        {
                            if (value0Ptr.ToInt32() != 0)
                                value0 = (NIrtd_stValue)Marshal.PtrToStructure(value0Ptr, typeof(NIrtd_stValue));
                        }

                        value0.NameCode = columnCodes[j];
                        value0.Name = columnNames[j];
                        value0.Total = 0;

                        tempStatItem.Data.Add(value0);
                    }

                    items.Add(tempStatItem);
                    // free up tempRow for next round
                    rc = NIrtd_freeRow(ref tempRow);
                }
            }
            catch (Exception ex)
            {
                //SmartLogger.Print(ex);
                throw ex;
            }
            finally
            {
                Marshal.FreeHGlobal(value0Ptr);
                Marshal.FreeHGlobal(newTablePtr);
            }
        }

        private static void UpdateValues(TableStruct newTable)
        {

            // structure used to stored temporary row
            IntPtr tempRow = IntPtr.Zero;
            IntPtr newTablePtr = Marshal.AllocHGlobal(Marshal.SizeOf(newTable));

            NIrtd_stValue value0 = new NIrtd_stValue();
            IntPtr value0Ptr = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(NIrtd_stValue)));
            try
            {
                // return code
                uint rc = (int)NRTD_Errors.NIrtd_eOK;
                Marshal.StructureToPtr(newTable, newTablePtr, false);

                if (items.Count == 0)
                    return;

                for (uint i = 0; i < newTable.NumberOfRows; i++)
                {
                    // copy the new row content to temp row
                    rc = NIrtd_allocateRow(ref tempRow, newTablePtr, i);
                    if (rc != (int)NRTD_Errors.NIrtd_eOK)
                    {
                        return;
                    }

                    // allocate the value structures in the tempStatItem

                    rc = NIrtd_allocateValue(value0Ptr);
                    if (rc != (int)NRTD_Errors.NIrtd_eOK)
                    {
                        NIrtd_freeValue(ref value0Ptr);
                        return;
                    }

                    // copy the skillset id
                    rc = NIrtd_getCol(value0Ptr, ref tempRow, 0);
                    if (rc != (int)NRTD_Errors.NIrtd_eOK)
                    {
                        NIrtd_freeValue(ref value0Ptr);
                        return;
                    }
                    else
                    {
                        if (value0Ptr.ToInt32() != 0)
                            value0 = (NIrtd_stValue)Marshal.PtrToStructure(value0Ptr, typeof(NIrtd_stValue));
                    }

                    value0.NameCode = columnCodes[0];
                    value0.Name = columnNames[0];

                    // flag to indicate whether the udpate item is in the list or not
                    bool found = false;

                    for (int j = 0; j < items.Count && found == false; j++)
                    {
                        NortelRTDStatItem item = (NortelRTDStatItem)items[j];
                        if (item.Data[0].Number == value0.Number && value0.Number != 0)
                        {
                            for (int k = 1; k < newTable.NumberOfCols; k++)
                            {
                                rc = NIrtd_allocateValue(value0Ptr);
                                if (rc != (int)NRTD_Errors.NIrtd_eOK)
                                {
                                    NIrtd_freeValue(ref value0Ptr);
                                    return;
                                }

                                // copy the number of agt avai. value
                                rc = NIrtd_getCol(value0Ptr, ref tempRow, (uint)k);
                                if (rc != (int)NRTD_Errors.NIrtd_eOK)
                                {
                                    NIrtd_freeValue(ref value0Ptr);
                                    return;
                                }
                                else
                                {
                                    if (value0Ptr.ToInt32() != 0)
                                        value0 = (NIrtd_stValue)Marshal.PtrToStructure(value0Ptr, typeof(NIrtd_stValue));
                                }

                                value0.NameCode = columnCodes[k];
                                value0.Name = columnNames[k];
                                value0.Total = 0;

                                item.Data[k] = value0;
                                items[j] = item;
                            }
                            found = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //SmartLogger.Print(ex);
                throw ex;
            }
            finally
            {
                Marshal.FreeHGlobal(value0Ptr);
                Marshal.FreeHGlobal(newTablePtr);
            }
        }

        private static void DeleteValues(TableStruct info)
        { 

        }

        private static void PrintValues()
        {
            Console.WriteLine("------------Saved---Values------------");
            foreach (KeyValuePair<uint, NIrtd_stValue> kvp in historyData)
            {
                Console.WriteLine(kvp.Value.Name + " - " + kvp.Value.Number);
            }
            Console.WriteLine("------------Cumulative---Values------------");
            foreach (KeyValuePair<uint, NIrtd_stValue> kvp in historyData)
            {
                Console.WriteLine(kvp.Value.Name + " - " + kvp.Value.TotalValue);
            }
            Console.WriteLine("------------Service---Level------------");
            Console.WriteLine(GetData(0));
            Console.WriteLine("-------------------------------------");
        }

        public static uint Start(string[] args)
        {
            if (args.Length < 8)
                return 1;
            
            uint rc = 0;
            string serverAddress = args[0];
            string serverUserName = args[1];
            string serverPassword = args[2];
            int refreshRate = int.Parse(args[3]);
            script = int.Parse(args[4]);
            int queryStructure = int.Parse(args[5]);
            List<int> columns = new List<int>();
            for (int z = 6; z < args.Length; z++)
            {
                columns.Add(int.Parse(args[z]));
            }
            
            parameters = args;

            // setup the required variables 
            // return code from API 
            rc = NortelRTDWrapper.NIrtd_setRecovery(90000, 10000);
            if (rc == (int)NRTD_Errors.NIrtd_eLIMIT_REACHED)
            {
                SmartLogger.Print(string.Format("failed to set up the Recovery: return code = {0}", ((NRTD_Errors)rc).ToString()));
                return rc;
            }
            rc = (int)NRTD_Errors.NIrtd_eOK;
            // Query structure 
            // Query request id return from the start data stream function 
            // Perform login to the server 
            SmartLogger.Print("Trying to log in...");
            rc = NortelRTDWrapper.NIrtd_login(ref authPointer, serverAddress, serverUserName, serverPassword);
            if (rc != (int)NRTD_Errors.NIrtd_eOK)
            {
                SmartLogger.Print(string.Format("failed to login: return code = {0}", ((NRTD_Errors)rc).ToString()));
                return rc;
            }

            // setup query 
            // allocate and initialize the query structure 
            rc = NortelRTDWrapper.NIrtd_allocateQuery(ref query, (uint)queryStructure);
            if (rc != (int)NRTD_Errors.NIrtd_eOK)
            {
                SmartLogger.Print(string.Format("\nNIrtd_allocateQuery failed : rc = {0}", ((NRTD_Errors)rc).ToString()));
                rc = NortelRTDWrapper.NIrtd_logout(ref authPointer);
                return rc;
            }

            foreach (int value in columns)
            {
                // select skillset ID as the first column
                rc = NortelRTDWrapper.NIrtd_selectColumn(ref query, (uint)value);
                columnNames.Add(((NRTD_Values)value).ToString());
                columnCodes.Add((uint)value);
                if (rc != (int)NRTD_Errors.NIrtd_eOK)
                {
                    SmartLogger.Print(string.Format("\nNIrtd_selectColumn failed : rc = {0}", ((NRTD_Errors)rc).ToString()));
                    rc = NortelRTDWrapper.NIrtd_freeQuery(ref query);
                    rc = NortelRTDWrapper.NIrtd_logout(ref authPointer);
                    return rc;
                }
            }

            // start data stream
            rc = NortelRTDWrapper.NIrtd_startDataStream(ref authPointer, ref query, (uint)(refreshRate * 1000), NortelRTDWrapper.RealTimeCallBack, ref myPointer, ref requestId);
            if (rc != (int)NRTD_Errors.NIrtd_eOK)
            {
                SmartLogger.Print(string.Format("\nNIrtd_startDataStream failed : rc = {0}", ((NRTD_Errors)rc).ToString()));
                rc = NortelRTDWrapper.NIrtd_freeQuery(ref query);
                rc = NortelRTDWrapper.NIrtd_logout(ref authPointer);
                return rc;
            }

            return 0;
        }

        public static void Stop()
        {
            uint rc = 0;
            if (authPointer != IntPtr.Zero)
            {
                // stop data stream before exit 
                rc = NortelRTDWrapper.NIrtd_stopDataStream(ref authPointer, requestId);
                // deallocate all structures allocated
                rc = NortelRTDWrapper.NIrtd_freeQuery(ref query);
                // perform logout 
                rc = NortelRTDWrapper.NIrtd_logout(ref authPointer);
            }
        }

        public static double GetData(int data)
        {
            double result = 0;
            double ca = 0;
            double cab = 0;
            double cat = 0;
            double cabt = 0;
            Dictionary<uint, NIrtd_stValue> newDictionary = new Dictionary<uint, NIrtd_stValue>(historyData);
            foreach (KeyValuePair<uint, NIrtd_stValue> kvp in newDictionary)
            {
                ///In the case when data is 0 the value that is being calculated is the Service Level.
                if (data == 0)
                {
                    if (kvp.Value.Name == NRTD_Values.NIrtd_APPL_CALLS_ANS.ToString())
                        ca = (double)kvp.Value.TotalValue;
                    else if (kvp.Value.Name == NRTD_Values.NIrtd_APPL_CALLS_ANS_AFT_THRESHOLD.ToString())
                        cat = (double)kvp.Value.TotalValue;
                    else if (kvp.Value.Name == NRTD_Values.NIrtd_APPL_CALLS_ABAN.ToString())
                        cab = (double)kvp.Value.TotalValue;
                    else if (kvp.Value.Name == NRTD_Values.NIrtd_APPL_CALLS_ABAN_AFT_THRESHOLD.ToString())
                        cabt = (double)kvp.Value.TotalValue;
                }
                else if (data == (int)NRTD_Values.NIrtd_APPL_MAX_WAITING_TIME &&
                         data == kvp.Value.NameCode)
                {
                    result = (double)kvp.Value.Total;
                    break;
                }
                else if (data == kvp.Value.NameCode)
                {
                    result = (double)kvp.Value.TotalValue;
                    break;
                }
            }
            if (data == 0)
            {
                double r1 = ca + cab;
                double r2 = cat + cabt;
                if (r1 > 0)
                    result = (r1 - r2) / r1;
                else
                    result = 0;
            }
            return result;
        }
    }

    public class NortelRTDStatItem
    {
        private List<NIrtd_stValue> data = new List<NIrtd_stValue>();

        public List<NIrtd_stValue> Data
        {
            get
            {
                return data;
            }
        }
    }

    #region Enums
    public enum NIrtd_enumType
    {
        NIrtd_eNumber,
        NIrtd_eString,
        NIrtd_eBuffer
    }

    public enum NRTD_Errors
    {
        NIrtd_eOK = 0,
        NIrtd_eSERVER = 60001, 			/* Invalid server id passed. */
        NIrtd_eROW_INIT = 60002, 			/* Allocation and init of row failed. */
        NIrtd_eTABLE = 60003, 			/* Invalid table id = query,  passed. */
        NIrtd_eCOLUMN = 60004, 			/* Invalid column selection = query,  passed. */
        NIrtd_eNOT_AVAIL = 60005, 			/* The query asked for a statistic that is not currently being */
        /* collected by the server. */
        NIrtd_eUSERID = 60006, 			/* Invalid user id passed. */
        NIrtd_ePASSWORD = 60007, 			/* Invalid password passed. */
        NIrtd_eUSERS = 60008, 			/* Too many users logged in to the server. */
        NIrtd_eLOGIN_FAIL = 60009, 			/* General login failure. */
        NIrtd_eLOGOUT_FAIL = 60010, 			/* General logout failure. */
        NIrtd_eUPDATE = 60011, 			/* Invalid update rate passed. */
        NIrtd_eQUERY_INITPARM = 60012, 		/* Query parm found to not be NULL on initialization. */
        NIrtd_eQUERY_NOINIT = 60013, 			/* Query found to be NULL. Should be initialized in call to */
        /* NIrtd_allocateQuery. */
        NIrtd_eQUERY_INIT = 60014, 			/* Failed to initialize Query. */
        NIrtd_eCONJ_INIT = 60015, 			/* Failed to initialize Conjunction. */
        NIrtd_eCONJ_NOINIT = 60016, 			/* Conjunction found to be NULL. Should be initialized in call to */
        /* NIrtd_allocateConjunction. */
        NIrtd_eAUTH_INIT = 60017, 			/* Failed to initialize authorization structure. */
        NIrtd_eSERV_INIT = 60018, 			/* Failed to initialize the server structure. */
        NIrtd_eINVALID_AUTH = 60019, 			/* Failed to validate pre-authorization. Ensure NIrtd_Login has */
        /* been called. */
        NIrtd_eREG_INIT = 60020, 			/* Failed to allocate registration. */
        NIrtd_eROW_INVALID = 60021, 			/* The row indicated does not exist. */
        NIrtd_eCOL_INVALID = 60022, 			/* The column indicated does not exist. */
        NIrtd_eDATA_INVALID = 60023, 			/* The data returned from the remote server was invalid. */
        NIrtd_eMUST_DEREG_FIRST = 60024, 		/* Attempt to logout before deregistration failed. */
        NIrtd_eINVALID_TABLE = 60025, 		/* Attempt to free an un-allocated group table. */
        NIrtd_eLISTENER_INIT = 60026, 		/* Failed to initialize a listener for data propagation. */
        NIrtd_eTABLE_GROUP_INIT = 60027, 		/* Failed to allocate and initialize the group table being returned. */
        NIrtd_eKEY = 60028, 		/* Passed value is not a key or is not a key for this table. */
        NIrtd_eKEY_MISMATCH = 60029, 		/* Keys in this conjunction are from different tables. */
        NIrtd_eNOT_FOUND = 60030, 			/* The requested name was not found. */

        /* Translation of Securiy server errors .. */
        NIrtd_eLOGIN = 60031, 		/* PC user login notification */
        NIrtd_eLOGIN_ERR = 60032, 		/* PC user login error */
        NIrtd_eLOGIN_NO = 60033, 		/* no PC user login yet */
        NIrtd_eLOGIN_ALREADY = 60034, 		/* PC user login already */
        NIrtd_eCOLUMN_INIT = 60035, 		/* Need to initialize the name/column cache. */
        NIrtd_eNAME_INIT = 60036, 		/* Failed to allocate name. */
        NIrtd_eSSLIST_GET = 60037, 		/* Failed to get skillset name cache. */
        NIrtd_eAPPLIST_GET = 60038, 		/* Failed to get application name cache. */
        NIrtd_eAGENTLIST_GET = 60039, 		/* Failed to get agent name cache. */
        NIrtd_eCACHE_REMOVAL = 60040, 		/* Failed to remove a name a cache. */
        NIrtd_eID_NAME_MISMATCH = 60041, 		/* Value content mismatch as compared to the indicated column. */
        NIrtd_eDIDNOTBUY = 60042, 		/* Failed to login to the server due to the real time access feature */
        /* not having been purchased. */
        NIrtd_eREMOTE_SYSREC_FAIL = 60043, 	/* Failed to login due to a failure to read the system record at the remote site. */
        NIrtd_eINVALID_REG = 60044, 		/* Invalid registration identifier passed. */
        NIrtd_eSET_ONLY_ON_INIT = 60045, 		/* The setRecovery routine can only be called */
        /* prior to calls for real-time data propagation. */
        NIrtd_eSTART_RECOVERY = 60046, 		/* A de-registration / re-registration attempt is being made. */
        NIrtd_eOK_RECOVERY = 60047, 		/* The above attempt was successful */
        NIrtd_eBAD_RECOVERY = 60048, 		/* The above attempt was not successful and a retry has been scheduled. */
        NIrtd_eVALUE_INIT = 60049, 		/* The passed value is invalid. */
        NIrtd_eCOL_NOT_FOUND = 60050, 		/* Column not loaded into name cache. */
        NIrtd_eLIMIT_REACHED = 60051, 		/* A passed parameter exceeds the defined limit. */
        NIrtd_eALLOC_FAILED = 60052, 		/* A required memory allocation failed. */
        NIrtd_eNULL_CALLBACK = 60053, 		/* A null call back function was passed. */
        NIrtd_eWORKER_INIT = 60054, 		/* Failed to initialize a worker for registration recovery. */
        NIrtd_eCOMM = 60055, 		/* A communications failure occured. */
        NIrtd_eRDC_FAILURE = 60056, 		/* Failure in DP communication with RDC or a failure of the defined */
        /* query when being processed by RDC. See server logs. */
        NIrtd_eIVRLIST_GET = 60057, 		/* Failed to get IVR name cache. */
        NIrtd_eROUTELIST_GET = 60058, 		/* Failed to get Route name cache. */
        NIrtd_eDP_FAILURE = 60059, 		/* Failed in DP */
        NIrtd_eLOGIN_NO_EULA = 60060,
        NIrtd_eLOGIN_NO_SERVER_VERSION = 60061,
        NIrtd_eLOGIN_FAILED_SERVER_VERSION = 60062
    }

    public enum NRTD_Values
    {
        /* TABLES */
        /* (Values which are assigned to NIrtd_tTableId) */

        /* Interval to date statistics */
        /* starting from 100 */
        NIrtd_INTRVL_APPL = 100,
        NIrtd_INTRVL_SKLST = 101,
        NIrtd_INTRVL_AGENT = 102,
        NIrtd_INTRVL_NODAL = 103,
        NIrtd_INTRVL_IVR = 104,
        NIrtd_INTRVL_ROUTE = 105,

        /* Moving window statistics */
        NIrtd_MWIND_APPL = 106,
        NIrtd_MWIND_SKLST = 107,
        NIrtd_MWIND_AGENT = 108,
        NIrtd_MWIND_NODAL = 109,
        NIrtd_MWIND_IVR = 110,
        NIrtd_MWIND_ROUTE = 111,

        /* OPERATORS */
        /* (Values which are assigned to NIrtd_tOperator) */
        /* starting from 120 */
        NIrtd_EQ = 120,

        /* APPLICATION TABLE COLUMN IDs */
        /* starting from 130 */
        /* RSM Struct Values */
        NIrtd_APPL_APPL_ID = 130, 		/* KEY */	/* ApplicationID */

        NIrtd_APPL_CALLS_ABAN = 131, 					/* CallsAbandoned */
        NIrtd_APPL_CALLS_ABAN_AFT_THRESHOLD = 132, 					/* CallsAbandonedAfterThreshold */
        NIrtd_APPL_CALLS_ABAN_DELAY = 133, 					/* CallsAbandonedDelay */
        NIrtd_APPL_CALLS_ANS = 134, 					/* CallsAnswered */
        NIrtd_APPL_CALLS_ANS_AFT_THRESHOLD = 135, 					/* CallsAnsweredAfterThreshold */
        NIrtd_APPL_CALLS_ANS_DELAY = 136, 					/* CallsAnsweredDelay */
        NIrtd_APPL_CALLS_WAITING = 137, 					/* CallsWaiting */
        NIrtd_APPL_MAX_WAITING_TIME = 138, 					/* MaxWaitingTime */
        NIrtd_APPL_WAITING_TIME = 139, 					/* WaitingTime */
        NIrtd_APPL_CALLS_ANS_DELAY_AT_SKILLSET = 140, 				/* CallsAnsweredDelayAtSkillset */
        NIrtd_APPL_CALLS_GIVEN_TERMINATE = 141, 					/* CallsGivenTerminationTreatment */
        NIrtd_APPL_CALLS_OFFER = 142, 					/* CallsOffered */
        NIrtd_APPL_NETWRK_OUT_CALLS = 143, 					/* NetworkOutCalls */
        NIrtd_APPL_NETWRK_OUT_ABAN = 144, 					/* NetworkOutCallsAbandoned */
        NIrtd_APPL_NETWRK_OUT_ABAN_DELAY = 145, 					/* NetworkOutCallsAbandonedDelay */
        NIrtd_APPL_NETWRK_OUT_ANS = 146, 					/* NetworkOutCallsAnswered */
        NIrtd_APPL_NETWRK_OUT_ANS_DELAY = 147, 					/* NetworkOutCallsAnsweredDelay */
        NIrtd_APPL_NETWRK_OUT_CALLS_WAITING = 148, 					/* NetworkOutCallsWaiting */
        NIrtd_APPL_DELAY_BEF_INTERFLOW = 251, 					/* TimeBeforeInterflow */
        NIrtd_APPL_NETWRK_OUT_CALLS_REQ = 149,					/* NetworkOutCallsRequested */

        /* SKILLSET TABLE COLUMN IDs */
        /* starting from 150 */
        /* the range 150 - 169 are all used up, */
        /* start another range from 250 ( 251 is gone ) */
        NIrtd_SKLST_SKILLSET_ID = 150, 		/* KEY */	/* SkillsetID */
        NIrtd_SKLST_CALL_WAIT = 151, 					/* CallsWaiting */
        NIrtd_SKLST_MAX_WAIT_TIME = 152, 					/* MaxWaitingTime */
        NIrtd_SKLST_AGENT_AVAIL = 153, 					/* AgentsAvailable */
        NIrtd_SKLST_AGENT_IN_SERVICE = 154, 					/* AgentsInService */
        NIrtd_SKLST_LONGEST_WAIT_TIMES_SINCE_LAST_CALL = 155,  		/* LongestWaitingTimeSinceLastCall */
        NIrtd_SKLST_AGENT_NOT_READY = 156, 					/* AgentsNotReady */
        NIrtd_SKLST_AGENT_ON_ICCM_CALL = 157, 					/* AgentsOnSkillsetCall */

        NIrtd_SKLST_TOT_WAIT_TIME = 158, 					/* WaitingTime */

        NIrtd_SKLST_EXPECT_WAIT_TIME = 159, 					/* ExpectedWaitTime */
        NIrtd_SKLST_CALL_ANS_AFT_THRESHOLD = 160, 					/* CallsAnsweredAfterThreshold */
        NIrtd_SKLST_LONGEST_WAIT_TIMES_SINCE_LOGIN = 161, 			/* LongestWaitingTimeSinceLogin */
        NIrtd_SKLST_AGENT_ON_DN_CALL = 162, 					/* AgentsOnDNCalls */
        NIrtd_SKLST_SKILLSET_STATE = 163, 					/* SkillsetState */
        NIrtd_SKLST_AGENT_UNAVAILABLE = 164, 					/* AgentsUnavailable */
        NIrtd_SKLST_NETWRK_CALL_WAIT = 165, 					/* NetworkCallsWaiting */
        NIrtd_SKLST_NETWRK_CALL_ANS = 166, 					/* NetworkCallsAnswered */
        NIrtd_SKLST_QUEUED_CALL_ANS = 167, 					/* QueuedCallAnswered */
        NIrtd_SKLST_TOT_ANS_DELAY = 168, 					/* TotalCallsAnsweredDelay */
        NIrtd_SKLST_TOT_CALL_ANS = 169, 					/* TotalCallsAnswered */
        NIrtd_SKLST_AGENT_ON_NETWRK_ICCM_CALL = 250, 				/* AgentOnNetworkSkillsetCall */
        NIrtd_SKLST_AGENT_ON_OTHER_ICCM_CALL = 252, 				/* AgentOnOtherSkillsetCall */
        NIrtd_SKLST_AGENT_ON_ACD_CALL = 253, 					/* AgentOnACDDNCall */
        NIrtd_SKLST_AGENT_ON_NACD_CALL = 254, 					/* AgentOnNACDDNCall */

        NIrtd_SKLST_CALL_OFFERED = 255,  					/* CallsOffered */
        NIrtd_SKLST_CALL_ABANDON = 256,  	  				/* SkillsetAbandoned */
        NIrtd_SKLST_CALL_ABANDONDELAY = 257, 					/* SkillsetAbandonedDelay */
        NIrtd_SKLST_CALL_ABANDONDELAY_AFTERTHRESHOLD = 258, 		/* SkillsetAbandonedAfterThreshold */
        NIrtd_SKLST_NETWRK_CALL_OFFERED = 259, 					/* NetworkCallsOffered */



        /* AGENT TABLE COLUMN IDs */
        /* starting from 170 */
        NIrtd_AGENT_AGENT_ID = 170, 		/* String KEY */	/* AgentID_High & AgentID_Low */

        NIrtd_AGENT_SUPERVISOR_ID = 171, 		/* String */		/* SupervisorID_High & SupervisorID_Low */
        NIrtd_AGENT_STATE = 172, 							/* State */
        NIrtd_AGENT_TIME_IN_STATE = 173, 							/* TimeInState */
        NIrtd_AGENT_ANS_SKILLSET = 174, 							/* AnsweringSkillset */
        NIrtd_AGENT_DN_IN_TIME_IN_STATE = 175, 							/* DNInTimeInState */
        NIrtd_AGENT_DN_OUT_TIME_IN_STATE = 176, 							/* DNOutTimeInState */
        NIrtd_AGENT_SUPERVISOR_USER_ID = 177, 		/* Buffer */		/* SupervisorUserID */
        NIrtd_AGENT_POSITION_ID = 178, 							/* PositionID */
        NIrtd_AGENT_NOT_READY_REASON = 179, 		/* String */		/* NotReadyReasonCode_High & NotReadyReasonCode_Low */
        NIrtd_AGENT_DN_OUT_CALL_NUM = 180, 		/* String */		/* DNOutCallNumber_High & DNOutCallNumber_Low */
        NIrtd_AGENT_SKLST_CALL_ANS = 181, 		/* String */		/* SkillsetCallAnswered */
        NIrtd_AGENT_DN_IN_CALL_ANS = 182, 		/* String */		/* DNInCallAnswered */
        NIrtd_AGENT_DN_OUT_CALL = 183, 		/* String */		/* DNOutCallMade */
        NIrtd_AGENT_ANS_APP = 184, 		/* String */		/* AnsweringApplication */
        NIrtd_AGENT_ANS_CDN = 185, 		/* String */		/* AnsweringCDN_High & AnsweringCDN_Low */
        NIrtd_AGENT_ANS_DNIS = 186, 		/* String */		/* AnsweringDNIS_High & AnsweringDNIS_Low */


        /* NODAL TABLE COLUMN IDs */
        /* starting from 190 */
        NIrtd_NODAL_DUMMY_KEY = 190, 		/* Numeric Key */
        NIrtd_NODAL_CALL_OFFER = 191,
        NIrtd_NODAL_CALL_ANS = 192,
        NIrtd_NODAL_CALL_WAIT = 193,
        NIrtd_NODAL_NETWRK_CALL_OFFER = 194,
        NIrtd_NODAL_NETWRK_CALL_ANS = 195,
        NIrtd_NODAL_NETWRK_CALL_WAIT = 196,


        /* IVR TABLE COLUMN IDs */
        /* starting from 210 */
        NIrtd_IVR_QUEUE_ID = 210, 		/* String KEY */
        NIrtd_IVR_CALL_WAIT = 211,
        NIrtd_IVR_CALL_ANS = 212,
        NIrtd_IVR_CALL_ANS_DELAY = 213,
        NIrtd_IVR_CALL_ANS_AFT_THRESHOLD = 214,
        NIrtd_IVR_CALL_NOT_TREATED = 215,
        NIrtd_IVR_CALL_NOT_TREATED_DELAY = 216,
        NIrtd_IVR_CALL_NOT_TREATED_AFT_THRESHOLD = 217,


        /* ROUTE TABLE COLUMN IDs */
        /* starting from 230 */
        NIrtd_ROUTE_ROUTE_NO = 230, 		/* KEY */
        NIrtd_ROUTE_ATB_FLAG = 231,
        NIrtd_ROUTE_ATB_TIME = 232,
    }
    #endregion

    #region Structs
    [StructLayout(LayoutKind.Sequential)]
    public struct TableGroupStruct
    {
        public TableStruct DeletedValues;
        public TableStruct NewValues;
        public TableStruct DeltaValues;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct TableStruct
    {
        public uint NumberOfRows;
        public uint NumberOfCols;
        public IntPtr Table;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct NIrtd_stValue
    {
        public NIrtd_enumType Type;
        public uint Number;
        public IntPtr Val; /* NIrtd_cMaxString */
        public string Name;
        public uint NameCode;
        public uint Total;

        public uint TotalValue
        {
            get
            {
                return Total + Number;
            }
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct NIrtd_stName
    {
        string first_name;
        string last_name;
    }

    #endregion
}
