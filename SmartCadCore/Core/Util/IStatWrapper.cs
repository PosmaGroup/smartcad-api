﻿using SmartCadCore.Statistic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadCore.Core.Util
{
    public interface IStatWrapper
    {
        bool IsInitiated();
        void Init(string connectionString);
        void InitHistorical();
        void StopHistorical();
        double GetValue(string query);
        void Stop();
        double GetData(StatisticRetriever.StatisticNames statisticType);
        void InitRealTime();
        void StopRealTime();



    }
}
