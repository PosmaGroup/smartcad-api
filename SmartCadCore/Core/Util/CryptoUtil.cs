using System;
using System.Security.Cryptography;
using System.Text;
using System.IO;

namespace SmartCadCore.Core
{
	/// <summary>
	/// Utilidad de criptografía.
	/// </summary>
	/// <remarks>
	/// Author: Alden Torres Cuon, atorres@smartmatic.com - Smartmatic Corp., Marzo 2005
	/// </remarks>
	public sealed class CryptoUtil
	{
		private CryptoUtil()
		{
		}

		/// <summary>
		/// Encripta una cadena con el password especificado. Solo puede ser desencryptado con la rutina <see cref="CryptoUtil.Decrypt"/>.
		/// </summary>
		/// <param name="text">Texto a encriptar.</param>
		/// <param name="password">Password para encryptar.</param>
		/// <returns>Cadena encriptada.</returns>
		public static string Encrypt(string text, string password)
		{
			MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();

			byte[] hash = md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(password));

			TripleDESCryptoServiceProvider des = new TripleDESCryptoServiceProvider();

			des.Key = hash;
			des.Mode = CipherMode.ECB;

			byte[] buff = ASCIIEncoding.ASCII.GetBytes(text);

			return Convert.ToBase64String(des.CreateEncryptor().TransformFinalBlock(buff, 0, buff.Length));
		}

		/// <summary>
		/// Desencrypta una cadena.
		/// </summary>
		/// <param name="textEncrypted">Cadena encriptada.</param>
		/// <param name="password">Password para desencriptar la cadena.</param>
		/// <returns>Texto en claro.</returns>
		public static string Decrypt(string textEncrypted, string password)
		{
			MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
			byte[] hash = md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(password));

			TripleDESCryptoServiceProvider des = new TripleDESCryptoServiceProvider();

			des.Key = hash;
			des.Mode = CipherMode.ECB;

			byte[] buff = Convert.FromBase64String(textEncrypted);

			return ASCIIEncoding.ASCII.GetString(des.CreateDecryptor().TransformFinalBlock(buff, 0, buff.Length));
		}

		/// <summary>
		/// Diseñado para calcular el hash de un password y compararlo y/o guardarlo en la base de datos.
		/// </summary>
		/// <param name="password">Password a computar hash.</param>
		/// <returns>Representación MD5 del password.</returns>
		public static string Hash(string password)
		{
			MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
			byte[] hash = md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(password));

			return Convert.ToBase64String(hash);
		}

		/// <summary>
		/// MD5 de un arreglo de bytes.
		/// </summary>
		/// <param name="buffer">Buffer para calcular su MD5.</param>
		/// <returns>Representacion en Base64 del MD5 del buffer.</returns>
		public static string Hash(byte[] buffer)
		{
			MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
			byte[] hash = md5.ComputeHash(buffer);

			return Convert.ToBase64String(hash);
		}

        private static RijndaelManaged rijndaelManaged;
        private static byte[] rijndaelSecretKey;
        private static byte[] rijndaelIV;

        private static void InitRijndael()
        {
            rijndaelManaged = new RijndaelManaged();
            rijndaelSecretKey = new byte[] { 54, 128, 149, 77, 235, 247, 33, 149, 159, 143, 163, 65, 199, 236, 156, 166, 255, 153, 100, 91, 11, 111, 93, 42, 250, 57, 216, 81, 47, 29, 146, 67 };
            rijndaelIV = new byte[16] { 94, 185, 81, 243, 192, 32, 118, 39, 87, 169, 16, 0, 245, 172, 200, 15 };
            rijndaelManaged.Key = rijndaelSecretKey;
            rijndaelManaged.IV = rijndaelIV;
        }

        public static string EncryptWithRijndael(string original)
        {
            if (rijndaelManaged == null)
            {
                InitRijndael();
            }

            byte[] toEncrypt;
            byte[] encrypted;

            //Get an encryptor.
            ICryptoTransform encryptor = rijndaelManaged.CreateEncryptor(rijndaelSecretKey, rijndaelIV);

            //Encrypt the data.
            MemoryStream msEncrypt = new MemoryStream();
            CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write);

            //Convert the data to a byte array.
            toEncrypt = ASCIIEncoding.Default.GetBytes(original);

            //Write all data to the crypto stream and flush it.
            csEncrypt.Write(toEncrypt, 0, toEncrypt.Length);
            csEncrypt.FlushFinalBlock();

            //Get encrypted array of bytes.
            encrypted = msEncrypt.ToArray();
            return System.Convert.ToBase64String(encrypted);
        }

        public static string DecryptWithRijndael(string encryptedS)
        {
            if (rijndaelManaged == null)
            {
                InitRijndael();
            }

            //Get a decryptor that uses the same secretKey and IV as the encryptor.
            byte[] encrypted = System.Convert.FromBase64String(encryptedS);
            ICryptoTransform decryptor = rijndaelManaged.CreateDecryptor(rijndaelSecretKey, rijndaelIV);

            //Now decrypt the previously encrypted message using the decryptor
            // obtained in the above step.
            MemoryStream msDecrypt = new MemoryStream(encrypted);
            CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read);

            byte[] fromEncrypt;
            fromEncrypt = new byte[encrypted.Length];

            //Read the data out of the crypto stream.
            csDecrypt.Read(fromEncrypt, 0, fromEncrypt.Length);

            //Convert the byte array back into a string.
            //			string roundtrip = System.Text.ASCIIEncoding.Unicode.GetString(fromEncrypt);
            string roundtrip = ASCIIEncoding.Default.GetString(fromEncrypt);
            return roundtrip;
        }
	}
}
