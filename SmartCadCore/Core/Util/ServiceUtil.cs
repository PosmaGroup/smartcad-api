using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using SmartCadCore.ClientData;
using NHibernate;
using System.Threading;
using Iesi.Collections;
using System.ServiceProcess;
using System.IO;
using System.Diagnostics;
using System.Net.Sockets;
using System.Reflection;
using System.Net;
using System.Drawing;
using System.Linq;
using SmartCadCore.Common;

namespace SmartCadCore.Core
{
    public class ServiceUtil
    {
        private static object lockAssignIncidentNotificationToOperators = new object();
        private static bool isRunningAssignment = false;
        private static object lockAssignIncidentNotificationToOperator = new object();


        public static bool CheckIPAdress(string ipAddress, IPAddress[] addresses)
        {
            bool result = false;
            foreach (IPAddress address in addresses)
            {
                if (address.ToString() == ipAddress)
                {
                    result = true;
                    break;
                }

            }
            return result;
        }

        public static void AssignIncidentNotificationsToAutomaticSupervisor(IList incidentNotifications)
        {
            try
            {
                SmartLogger.Print("Start AssignIncidentNotificationsToAutomaticSupervisor");
                foreach (IncidentNotificationData incidentNotification in incidentNotifications)
                {
                    Thread.Sleep(100);
                    SmartLogger.Print("Initiating cycle AssignIncidentNotificationsToAutomaticSupervisor");
                    if (incidentNotification.Status != IncidentNotificationStatusData.Closed &&
                        incidentNotification.Status != IncidentNotificationStatusData.Cancelled)
                    {
                        incidentNotification.Status = IncidentNotificationStatusData.AutomaticSupervisor;
                        incidentNotification.DispatchOperator = OperatorData.InternalSupervisor;

                        try
                        {
                            SmartCadDatabase.SaveOrUpdate(incidentNotification, CreateContext(OperatorData.InternalSupervisor.Login, UserApplicationData.Dispatch.Name));
                        }
                        catch (Exception ex)
                        {
                            SmartLogger.Print(ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        public static void AssignIncidentNotificationToManualSupervisor(IncidentNotificationData incidentNotification,
            OperatorData supervisor)
        {

            if (incidentNotification.Status != IncidentNotificationStatusData.Closed &&
                    incidentNotification.Status != IncidentNotificationStatusData.Cancelled)
            {
                incidentNotification.Status = IncidentNotificationStatusData.ManualSupervisor;
                incidentNotification.DispatchOperator = supervisor;

                try
                {
                    SmartCadDatabase.SaveOrUpdate(incidentNotification, CreateContext(OperatorData.InternalSupervisor.Login, UserApplicationData.Dispatch.Name));
                }
                catch(Exception ex)
                {
                    throw ex;
                }
            }
        }

        public static void AssignIncidentNotificationsToAutomaticSupervisor(string operatorData)
        {
            lock (lockAssignIncidentNotificationToOperator)
            {
                try
                {
                    IList incidentNotifications = SmartCadDatabase.SearchObjects(
                            SmartCadHqls.GetCustomHql(
                            SmartCadHqls.GetIncidentNotificationsByDispatchOperator, operatorData));

                    foreach (IncidentNotificationData incidentNotification in incidentNotifications)
                    {
                        Thread.Sleep(100);

                        if (incidentNotification.Status != IncidentNotificationStatusData.Closed &&
                            incidentNotification.Status != IncidentNotificationStatusData.Cancelled)
                        {
                            incidentNotification.Status = IncidentNotificationStatusData.AutomaticSupervisor;
                            incidentNotification.DispatchOperator = OperatorData.InternalSupervisor;

                            try
                            {
                                SmartCadDatabase.SaveOrUpdate(incidentNotification, CreateContext(operatorData, UserApplicationData.Dispatch.Name));
                            }
                            catch (DatabaseStaleObjectException ex)
                            {
                                string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetIncidentNotificationByCustomCode, incidentNotification.CustomCode);
                                IncidentNotificationData temp = SmartCadDatabase.SearchBasicObject(hql) as IncidentNotificationData;
                                if (temp != null &&
                                    temp.Status != IncidentNotificationStatusData.Closed &&
                                    temp.Status != IncidentNotificationStatusData.Cancelled)
                                {
                                    temp.Status = IncidentNotificationStatusData.AutomaticSupervisor;
                                    temp.DispatchOperator = OperatorData.InternalSupervisor;

                                    SmartCadDatabase.SaveOrUpdate(temp, CreateContext(operatorData, UserApplicationData.Dispatch.Name));

                                }
                            }
                            catch (Exception ex)
                            {
                                SmartLogger.Print(ex);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    SmartLogger.Print(ex);
                }
            }
        }

        public static void LogoutOperator(string operatorData, string userApplication)
        {
            try
            {
                SessionHistoryData sessionHistory = GetCurrentSessionHistory(userApplication, operatorData);

                if (sessionHistory != null)
                {
                    sessionHistory.IsLoggedIn = false;
                    sessionHistory.EndDateLogin = SmartCadDatabase.GetTimeFromBD();
                    SmartCadDatabase.SaveOrUpdate(sessionHistory, CreateContext(operatorData, userApplication));
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
                throw ex;
            }
        }

        public static SessionHistoryData LoginOperator(UserApplicationData application, OperatorData ope, string computerName)
        {
            SessionHistoryData session = new SessionHistoryData();
            session.UserApplication = application;
            session.UserAccount = ope;
            session.StartDateLogin = SmartCadDatabase.GetTimeFromBD();
            session.IsLoggedIn = true;
            SessionStatusHistoryData osh = new SessionStatusHistoryData();
            osh.Session = session;
            osh.Status = OperatorStatusData.Ready;
            osh.StartDate = session.StartDateLogin;
            session.SessionHistoryStatusList = new HashedSet();
            session.SessionHistoryStatusList.Add(osh);
            session.ComputerName = computerName;

            session = (SessionHistoryData) SmartCadDatabase.SaveOrUpdate(session, CreateContext(ope.Login, application.Name));
            return session;
        }

        public static SessionStatusHistoryData StartSessionStatusHistory(SessionHistoryData session, OperatorStatusData status)
        {
            SessionStatusHistoryData osh = new SessionStatusHistoryData();
            osh.Session = session;
            osh.Status = status;
            osh.StartDate = SmartCadDatabase.GetTimeFromBD();
            return (SessionStatusHistoryData) SmartCadDatabase.SaveOrUpdate(osh, CreateContext(session.UserAccount.Login, session.UserApplication.Name));
        }

        public static void UpdateSessionStatusHistory(UserApplicationData application, string login, OperatorStatusData newStatus)
        {
            EndSessionStatusHistory(application, login);

            StartSessionStatusHistory(GetCurrentSessionHistory(application.Name, login), newStatus);
        }

        public static void EndSessionStatusHistory(UserApplicationData application, string login)
        {
            SessionStatusHistoryData currentSessionStatusHistory = GetCurrentSessionStatusHistory(application, login);
            if (currentSessionStatusHistory != null)
            {
                currentSessionStatusHistory.EndDate = SmartCadDatabase.GetTimeFromBD();
                SmartCadDatabase.SaveOrUpdate(currentSessionStatusHistory, CreateContext(login, application.Name));
            }
        }

        public static SessionStatusHistoryData GetCurrentSessionStatusHistory(UserApplicationData application, string login)
        {
            return SmartCadDatabase.SearchObject<SessionStatusHistoryData>(SmartCadHqls.GetCustomHql(SmartCadHqls.GetCurrentSessionStatusHistory, application.Code, login));
        }

        public static SessionHistoryData GetCurrentSessionHistory(string application, string ope)
        {
            return SmartCadDatabase.SearchObject<SessionHistoryData>(SmartCadHqls.GetCustomHql(SmartCadHqls.GetUserSessionByApplication, application, ope));
        }

        public static IList GetAllCurrentSessionHistory()
        {
            IList t = SmartCadDatabase.SearchObjects(SmartCadHqls.GetAllUserSessions);
            return t;
        }

        public static void LogoutAllOperators()
        {
            //Update all the operator session to logged out.
            IList uaads = GetAllCurrentSessionHistory();
            foreach (SessionHistoryData session in uaads)
            {
                SmartCadAudit.Audit(session.UserApplication, session.UserAccount.Login, session.UserAccount.Code, "CloseSession", null, OperatorActions.Logout);
                session.IsLoggedIn = false;
                session.EndDateLogin = SmartCadDatabase.GetTimeFromBD();
                SmartCadDatabase.SaveOrUpdate(session, CreateContext(session.UserAccount.Login, session.UserApplication.Name));
            }
        }

        public static SmartCadContext CreateContext(string operatorData, string application)
        {
            return new SmartCadContext(operatorData, application);
        }


        private static int UpdateDepartmentTypeCounter(DepartmentTypeData departmentType) 
        {
            DepartmentTypeCounterData counter = new DepartmentTypeCounterData();
            
            counter = ((DepartmentTypeData)SmartCadDatabase.SearchObjectData(SmartCadHqls.GetCustomHql(SmartCadHqls.GetDepartmentTypeCounter, departmentType.Code))).Counter;
            
            int frequency = GetFrequencyNumberOfDays(departmentType.RestartFrequency);

            if (departmentType.RestartFrequency != DepartmentTypeData.Frequency.None && counter.RestartDate.Value.AddDays(frequency) <= DateTime.Now)
            {
                counter.RestartDate = DateTime.Now;
                counter.Value = 0;
            }
            else 
                 counter.Value++;

            try
            {
                counter = (DepartmentTypeCounterData)SmartCadDatabase.SaveOrUpdate(counter);
            }
            catch 
            {
                UpdateDepartmentTypeCounter(departmentType);                
            }

            return counter.Value;
        }

        private static int GetFrequencyNumberOfDays(DepartmentTypeData.Frequency frequency) 
        {
            switch (frequency)
            {  
                case DepartmentTypeData.Frequency.Daily:
                    return 1;
                case DepartmentTypeData.Frequency.Weekly:
                    return 7;
                case DepartmentTypeData.Frequency.Fortnightly:
                    return 15;
                case DepartmentTypeData.Frequency.Monthly:
                    return 30;
                case DepartmentTypeData.Frequency.Annual:
                    return 365;
                default:
                    break;
            }

            return 0;
        
        }


        public static string GenerateNotificationCustomCode(DepartmentTypeData departmentType, int operatorCode)
        {
            string customCode = string.Empty;
            ArrayList parsingPattern = new ArrayList();
            string pattern = string.Empty;
           
         
            if (departmentType.Pattern != null && departmentType.Pattern != string.Empty)
                pattern = departmentType.Pattern;
            else
                pattern = "#";

            int counter = UpdateDepartmentTypeCounter(departmentType);

            //Obtengo un arreglo con cada secci�n del patr�n
            parsingPattern = GetParsingPattern(pattern);

            foreach (string st in parsingPattern)
            {
                if (st == "o")
                    customCode += operatorCode;
                else if (st == "#")
                    customCode += counter.ToString();
                else if (st.Contains("'") == true)
                    customCode += st.Replace("'", "");
                else if (st == "AA" || st == "YY")
                    customCode += DateTime.Now.ToString("yy");
                else if (st == "AAAA" || st == "YYYY")
                    customCode += DateTime.Now.ToString("yyyy");
                else if (st == "MM")
                    customCode += DateTime.Now.ToString("MM");
                else if (st == "DD")
                    customCode += DateTime.Now.ToString("dd");
                else if (st == "hh")
                    customCode += DateTime.Now.ToString("HH");
                else if (st == "ss")
                    customCode += DateTime.Now.ToString("ss");
                else if (st == "mm")
                    customCode += DateTime.Now.ToString("mm");
            }


            return customCode;
        }

      
        private static ArrayList GetParsingPattern(string pattern) 
        {
            ArrayList result = new ArrayList();
            string st = string.Empty;
            bool skip = false;

            for (int i = 0; i < pattern.Length; i++)
            {
                if (pattern[i].ToString() == "'")
                    skip = !skip;

                st += pattern[i].ToString();

                if ((pattern.Length - 1 == i || pattern[i] != pattern[i + 1]) && skip == false)
                {
                    result.Add(st);
                    st = string.Empty;
                }
            }

            return result;
        }
        
        public static void AssignIncidentNotificationToOperator(OperatorData operatorData, int maxNotifications)
        {
            if (isRunningAssignment == false)
            {
                lock (lockAssignIncidentNotificationToOperator)
                {
                    isRunningAssignment = true;
                    double timeIntervalToAvoidReassignment = 5; //5 min default
                    try
                    {

                        ApplicationPreferenceData preference = SmartCadDatabase.SearchObject<ApplicationPreferenceData>(
                            SmartCadHqls.GetCustomHql(SmartCadHqls.GetApplicationPreferenceByName, "TimeIntervalToAvoidReassignmentsToOperator"));
                        Thread.Sleep(1000);
                        timeIntervalToAvoidReassignment = double.Parse(preference.Value);
                    }
                    catch
                    { }
                    try
                    {
                        DateTime currentDateTime = SmartCadDatabase.GetTimeFromBD();
                        DateTime currentDateTimeWithAddedInterval = currentDateTime.AddMinutes(timeIntervalToAvoidReassignment);

                        //DateTime date = new DateTime(ApplicationUtil.SCHEDULE_REFERENCE_DATE.Year,
                        //    ApplicationUtil.SCHEDULE_REFERENCE_DATE.Month,
                        //    ApplicationUtil.SCHEDULE_REFERENCE_DATE.Day,
                        //    currentDateTime.Hour,
                        //    currentDateTime.Minute,
                        //    currentDateTime.Second,
                        //    currentDateTime.Millisecond);

                        //DateTime dateWithAddedInterval = date.AddMinutes(timeIntervalToAvoidReassignment);

                        string statusCustomCodeList = "('" + OperatorStatusData.Ready.CustomCode + "', '" + OperatorStatusData.Busy.CustomCode + "')";
                        long res = (long) SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(false,
                                   SmartCadHqls.GetIfOperatorCanReceiveNotifications,
                                   operatorData.Code,
                                   UserApplicationData.Dispatch.Name,
                                   statusCustomCodeList));

                        bool operatorIsReadyOrBusy = res > 0 &&
                            (OperatorScheduleManager.IsWorkingNow(
                                operatorData.Code,
                                currentDateTimeWithAddedInterval) == true ||
                                GetLogInMode() == false);

                        if (operatorIsReadyOrBusy)
                        {
                            IList supervisorIncidentNotifications = SmartCadDatabase.SearchObjects(SmartCadHqls.GetIncidentNotificationInSupervisorStatus);
                            SmartCadDatabase.InitializeLazy(operatorData, operatorData.DepartmentTypes);

                            int currentNotifications = Convert.ToInt32(SmartCadDatabase.SearchBasicObject(
                                    SmartCadHqls.GetCustomHql(SmartCadHqls.GetCountCurrentIncidentNotificationsByOperator, operatorData.Code)));
                            for (int i = 0; i < supervisorIncidentNotifications.Count && currentNotifications < maxNotifications; i++)
                            {
                                IncidentNotificationData incidentNotification = supervisorIncidentNotifications[i] as IncidentNotificationData;
                                if (operatorData.DepartmentTypes.IndexOf(incidentNotification.DepartmentType) != -1)
                                {
                                    SmartCadDatabase.InitializeLazy(incidentNotification.ReportBase, incidentNotification.ReportBase.SetIncidentTypes);
                                    incidentNotification.DispatchOperator = operatorData;
                                    incidentNotification.Status = IncidentNotificationStatusData.Reassigned;
                                    try
                                    {
                                        SmartCadDatabase.SaveOrUpdate(incidentNotification, ServiceUtil.CreateContext(operatorData.Login, UserApplicationData.Dispatch.Name));
                                        currentNotifications++;
                                    }
                                    catch (DatabaseStaleObjectException ex)
                                    {
                                        string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetIncidentNotificationByCustomCode, incidentNotification.CustomCode);
                                        incidentNotification = SmartCadDatabase.SearchBasicObject(hql) as IncidentNotificationData;
                                        if (incidentNotification != null &&
                                            incidentNotification.Status != IncidentNotificationStatusData.Closed &&
                                            incidentNotification.Status != IncidentNotificationStatusData.Cancelled)
                                        {
                                            incidentNotification.Status = IncidentNotificationStatusData.AutomaticSupervisor;
                                            incidentNotification.DispatchOperator = OperatorData.InternalSupervisor;

                                            SmartCadDatabase.SaveOrUpdate(incidentNotification, CreateContext(operatorData.Login, UserApplicationData.Dispatch.Name));
                                            currentNotifications++;
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        SmartLogger.Print(ex);
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        SmartLogger.Print(ex);
                    }
                    finally
                    {
                        isRunningAssignment = false;
                    }
                }
            }
        }

        /// <summary>
        /// Assign a notification to operatorData.
        /// Before to do this assignment, the notification is reassigned to supervisor and then it is 
        /// reassigned to operatorData. This behavior raises from leave a record in database to express
        /// who was the supervisor who realized the operation.
        /// </summary>
        /// <param name="incidentNotification">notification to be reassigned</param>
        /// <param name="operatorData">operator which the notification willbe reassigned</param>
        /// <param name="supervisor">supervisor that will perform the assignment</param>
        /// <returns></returns>
        public static bool AssignIncidentNotificationToOperator(IncidentNotificationData incidentNotification, OperatorData operatorData, OperatorData supervisor)
        {
            bool result = false;
            if (isRunningAssignment == false)
            {
                lock (lockAssignIncidentNotificationToOperator)
                {
                    isRunningAssignment = true;
                    double timeIntervalToAvoidReassignment = 5; //5 min default
                    try
                    {

                        ApplicationPreferenceData preference = SmartCadDatabase.SearchObject<ApplicationPreferenceData>(
                            SmartCadHqls.GetCustomHql(SmartCadHqls.GetApplicationPreferenceByName, "TimeIntervalToAvoidReassignmentsToOperator"));
                        Thread.Sleep(1000);
                        timeIntervalToAvoidReassignment = double.Parse(preference.Value);
                    }
                    catch
                    { }
                    try
                    {
                        DateTime currentDateTime = SmartCadDatabase.GetTimeFromBD();
                        DateTime currentDateTimeWithAddedInterval = currentDateTime.AddMinutes(timeIntervalToAvoidReassignment);

                        string statusCustomCodeList = "('" + OperatorStatusData.Ready.CustomCode + "', '" + OperatorStatusData.Busy.CustomCode + "')";
                        long res = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(false,
                                   SmartCadHqls.GetIfOperatorCanReceiveNotifications,
                                   operatorData.Code,
                                   UserApplicationData.Dispatch.Name,
                                   statusCustomCodeList));

                        //bool operatorIsReadyOrBusy = res > 0 &&
                        //    (OperatorScheduleManager.IsWorkingNow(
                        //        operatorData.Code,
                        //        currentDateTimeWithAddedInterval) == true ||
                        //        GetLogInMode() == false);

                        //bool operatorIsReadyOrBusy = res > 0 &&
                        //    (true || GetLogInMode() == false);
                        bool operatorIsReadyOrBusy = res > 0 ||
                            (true || GetLogInMode() == false);
                        
                        if (operatorIsReadyOrBusy)
                        {
                            incidentNotification.DispatchOperator = supervisor;
                            incidentNotification = SmartCadDatabase.SaveOrUpdate(incidentNotification) as IncidentNotificationData;
                            DateTime time = SmartCadDatabase.GetTimeFromBD();
                            SmartCadDatabase.InitializeLazy(operatorData, operatorData.DepartmentTypes);

                            if (operatorData.DepartmentTypes.IndexOf(incidentNotification.DepartmentType) != -1)
                            {
                                Thread.Sleep(500);
                                SmartCadDatabase.InitializeLazy(incidentNotification.ReportBase, incidentNotification.ReportBase.SetIncidentTypes);
                                incidentNotification.DispatchOperator = operatorData;
                                incidentNotification.Status = IncidentNotificationStatusData.Reassigned;
                                try
                                {
                                    SmartCadDatabase.SaveOrUpdate(incidentNotification, ServiceUtil.CreateContext(operatorData.Login, UserApplicationData.Dispatch.Name));
                                    result = true;
                                }
                                catch
                                {
                                    throw;
                                }
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        isRunningAssignment = false;
                    }
                }
            }
            return result;
        }

        public static IncidentNotificationStatusData UpdateIncidentNotificationStatus(IncidentNotificationData incidentNotification)
        {
            int countInProgress = 0;
            int countClosed = 0;
            int countCancelled = 0;
            bool delayed = false;
            IncidentNotificationStatusData status = incidentNotification.Status;
            SmartCadDatabase.InitializeLazy(incidentNotification, incidentNotification.SetDispatchOrders);
            IList dispatchOrderList = new ArrayList(incidentNotification.SetDispatchOrders);
            for (int i = 0; i < dispatchOrderList.Count && delayed == false; i++)
            {
                DispatchOrderData dispatchOrder = dispatchOrderList[i] as DispatchOrderData;
                if (dispatchOrder.Status == DispatchOrderStatusData.Delayed)
                {
                    delayed = true;//Con al menos una en delayed salgo del ciclo
                }
                else if (dispatchOrder.Status == DispatchOrderStatusData.UnitOnScene ||
                    dispatchOrder.Status == DispatchOrderStatusData.Dispatched)
                {
                    countInProgress++;//Cuento todas las unidades en escena o despachadas
                }
                else if (dispatchOrder.Status == DispatchOrderStatusData.Closed)
                {
                    countClosed++;
                }
                else if (dispatchOrder.Status == DispatchOrderStatusData.Cancelled)
                {
                    countCancelled++;
                }
            }
            if (delayed == true)
            {
                status = IncidentNotificationStatusData.Delayed;
            }
            else if (countInProgress > 0)
            {
                status = IncidentNotificationStatusData.InProgress;
            }
            else if (countCancelled == incidentNotification.SetDispatchOrders.Count ||
           incidentNotification.SetDispatchOrders.Count == 0)
            {
                status = IncidentNotificationStatusData.Assigned;
            }
            else if (countClosed + countCancelled == incidentNotification.SetDispatchOrders.Count)
            {
                status = IncidentNotificationStatusData.InProgress;
            }
            return status;
        }

        public static bool DuplicatedNotification(ReportBaseDepartmentTypeData reportBaseDepartmentType, IncidentData incidentData)
        {
            IList incidentNotifications = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetIncidentNotificationsByIncidentCustomCode, incidentData.CustomCode));
            return DuplicatedNotification(reportBaseDepartmentType, incidentNotifications);
        }

        public static bool DuplicatedNotification(ReportBaseDepartmentTypeData reportBaseDepartmentType, IList incidentNotifications)
        {
            bool duplicated = false;
            if (incidentNotifications != null)
            {
                for (int i = 0; i < incidentNotifications.Count && duplicated == false; i++)
                {
                    IncidentNotificationData incidentNotification =
                        incidentNotifications[i] as IncidentNotificationData;
                    if (incidentNotification.DepartmentType.Equals(reportBaseDepartmentType.DepartmentType))
                    {
                        duplicated = true;
                    }
                }
            }
            return duplicated;
        }

        public static bool GetLogInMode()
        {
            ApplicationPreferenceData preference = SmartCadDatabase.SearchBasicObject(
                        SmartCadHqls.GetCustomHql(SmartCadHqls.GetApplicationPreferenceByName, "CheckLogInMode"))
                        as ApplicationPreferenceData;
            return Convert.ToBoolean(preference.Value);
        }

        public static void AssignIncidentNotificationToOperators(int maxNotifications)
        {
            //TODO FS 25/08/2008: Query GetLoggedInOperatorsByApplicationStatus cambiar si se cambia implementacin de los WorkShift
            if (isRunningAssignment == false)
            {
                lock (lockAssignIncidentNotificationToOperators)
                {
                    isRunningAssignment = true;
                    double timeIntervalToAvoidReassignment = 5; //5 min default
                    try
                    {

                        ApplicationPreferenceData preference = SmartCadDatabase.SearchObject<ApplicationPreferenceData>(
                            SmartCadHqls.GetCustomHql(SmartCadHqls.GetApplicationPreferenceByName, "TimeIntervalToAvoidReassignmentsToOperator"));
                        Thread.Sleep(1000);
                        timeIntervalToAvoidReassignment = double.Parse(preference.Value);
                    }
                    catch
                    { }
                    try
                    {
                        DateTime currentDateTime = SmartCadDatabase.GetTimeFromBD();
                        DateTime currentDateTimeWithAddedInterval = currentDateTime.AddMinutes(timeIntervalToAvoidReassignment);
                        
                        IList supervisorIncidentNotifications = SmartCadDatabase.SearchObjects(SmartCadHqls.GetIncidentNotificationInSupervisorStatus);
                        ArrayList notificationsWillBeInManualSupervisor = new ArrayList(supervisorIncidentNotifications);
                        
                        string statusCustomCodeList = "('" + OperatorStatusData.Ready.CustomCode + "', '" + OperatorStatusData.Busy.CustomCode + "')";
                        string hql = SmartCadHqls.GetCustomHql(false,
                                   SmartCadHqls.GetLoggedInOperatorsByApplicationStatus,
                                   statusCustomCodeList,
                                   UserApplicationData.Dispatch.Name);
                        IList operators = SmartCadDatabase.SearchObjects(hql);
                        
                        Dictionary<string, int> counterCurrentNotifications = new Dictionary<string, int>();
                        foreach (OperatorData op in operators)
                        {
                            if (counterCurrentNotifications.ContainsKey(op.Login) == false &&
                                (OperatorScheduleManager.IsWorkingNow(
                                op.Code,
                                currentDateTimeWithAddedInterval) == true) ||
                                (GetLogInMode() == false))
                            {
                                int currentNotifications = Convert.ToInt32(SmartCadDatabase.SearchBasicObject(
                                                                    SmartCadHqls.GetCustomHql(
                                                                    SmartCadHqls.GetCountCurrentIncidentNotificationsByOperator, 
                                                                    op.Code)));
                                //TODO: WorkAround, a veces no se cierra una status correctamente en despacho.
                                if (counterCurrentNotifications.ContainsKey(op.Login) == false)
                                    counterCurrentNotifications.Add(op.Login, currentNotifications);
                                Thread.Sleep(100);
                            }
                        }

                        int operatorsIndex = 0;
                        if (operators.Count > 0)
                        {
                            foreach (IncidentNotificationData notification in supervisorIncidentNotifications)
                            {
                                bool assign = false;
                                int operatorsCheck = 0;
                                while (assign == false)
                                {
                                    Thread.Sleep(100);
                                    OperatorData ope = operators[operatorsIndex] as OperatorData;
                                    if (ope != null && counterCurrentNotifications.ContainsKey(ope.Login))
                                    {
                                        if (counterCurrentNotifications[ope.Login] < maxNotifications)
                                        {
                                            SmartCadDatabase.InitializeLazy(ope, ope.DepartmentTypes);

                                            if (ope.DepartmentTypes.IndexOf(notification.DepartmentType) != -1)
                                            {
                                                notification.DispatchOperator = ope;
                                                notification.Status = IncidentNotificationStatusData.Reassigned;

                                                SmartCadDatabase.SaveOrUpdate(notification, ServiceUtil.CreateContext(ope.Login, UserApplicationData.Dispatch.Name));
                                                if (notificationsWillBeInManualSupervisor.Contains(notification) == true)
                                                {
                                                    notificationsWillBeInManualSupervisor.Remove(notification);
                                                }
                                                assign = true;
                                                counterCurrentNotifications[ope.Login]++;
                                            }
                                        }
                                    }
                                    if (++operatorsIndex == operators.Count)
                                    {
                                        operatorsIndex = 0;
                                    }
                                    if (++operatorsCheck == operators.Count)
                                    {
                                        assign = true;
                                    }
                                }
                            }
                        }
                        if (notificationsWillBeInManualSupervisor.Count > 0)
                        {
                            foreach (IncidentNotificationData notification in notificationsWillBeInManualSupervisor)
                            {
                                if (notification.Status.Name != IncidentNotificationStatusData.ManualSupervisor.Name)
                                {
                                    notification.DispatchOperator = OperatorData.InternalSupervisor;
                                    notification.Status = IncidentNotificationStatusData.ManualSupervisor;
                                    SmartCadDatabase.SaveOrUpdate(notification);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        SmartLogger.Print(ex);
                    }
                    isRunningAssignment = false;
                }                
            }
        }

        #region Distance between two points in earth surface
        private static readonly double EARTH_RATIO_METERS = 6378137; 
        private static readonly double EARTH_RATIO = 6371; //In Km
        private static readonly double DEG_TO_RAD = 2 * Math.PI / 360;
        private static readonly double RAD_TO_DEG = 360 / (2 * Math.PI);
        private static readonly double DISTANCE_PER_DEGREE = EARTH_RATIO * 2 * Math.PI / 360;

        /// <summary>
        /// Calculates the incidents occurred in a given ratio in terms of distanceUnit;
        /// </summary>
        /// <param name="point">Central point where to explore other incidents</param>
        /// <param name="ratio">Ratio of searching in meters</param>
        /// <param name="distanceUnit">Units to express the distance(Kilometers, Meters, Centimeters)</param>
        /// <returns>List with codes of all incidents occurred in the specified ratio</returns>
        public static IList<string> GetIncidentCodeInRatio(GeoPoint point, double ratio, DistanceUnit distanceUnit)
        {
            IList<string> result = new List<string>();
            double delta = 0;
            switch (distanceUnit)
            {
                case DistanceUnit.Kilometers:
                    delta = ratio / DISTANCE_PER_DEGREE;
                    break;

                case DistanceUnit.Meters:
                    delta = ratio / (DISTANCE_PER_DEGREE * 1000);
                    break;

                case DistanceUnit.Centimeters:
                    delta = ratio / (DISTANCE_PER_DEGREE * 100000);
                    break;

                default:
                    delta = ratio / DISTANCE_PER_DEGREE;
                    break;
            }
            
            GeoPoint latNorth = CalculateDerivedPosition(point, ratio, 0);
            GeoPoint latSouth = CalculateDerivedPosition(point, ratio, 180);
            GeoPoint lonWest = CalculateDerivedPosition(point, ratio, -90);
            GeoPoint lonEast = CalculateDerivedPosition(point, ratio, 90);

            //string minusDeltaLon = (point.Lon - delta).ToString().Replace(",", ".");
            //string plusDeltaLon = (point.Lon + delta).ToString().Replace(",", ".");
            //string minusDeltaLat = (point.Lat - delta).ToString().Replace(",", ".");
            //string plusDeltaLat = (point.Lat + delta).ToString().Replace(",", ".");

            string minusDeltaLon = (lonWest.Lon).ToString().Replace(",", ".");
            string plusDeltaLon = (lonEast.Lon).ToString().Replace(",", ".");
            string minusDeltaLat = (latSouth.Lat).ToString().Replace(",", ".");
            string plusDeltaLat = (latNorth.Lat).ToString().Replace(",", ".");

            StringBuilder queryStr = new StringBuilder(SmartCadHqls.GetCustomHql(
                    SmartCadHqls.GetIncidentCodesInRatioHql,
                    minusDeltaLon, plusDeltaLon,
                    minusDeltaLat, plusDeltaLat));

            object objectList = SmartCadDatabase.SearchBasicObjects(
                queryStr.ToString(), true);

            if (objectList as IList != null)
            {
                IList resultList = objectList as IList;
                foreach (int code in resultList)
                {
                    result.Add(code.ToString());
                }
            }
            return result;
        }

        /// <summary>
        /// Calculates the end-point from a given source at a given range (meters) and bearing (degrees).
        /// This methods uses simple geometry equations to calculate the end-point.
        /// </summary>
        /// <param name="source">Point of origin</param>
        /// <param name="range">Range in meters</param>
        /// <param name="bearing">Bearing in degrees</param>
        /// <returns>End-point from the source given the desired range and bearing.</returns>
        private static GeoPoint CalculateDerivedPosition(GeoPoint source, double range, double bearing)
        {
            double latA = source.Lat * DEG_TO_RAD;
            double lonA = source.Lon * DEG_TO_RAD;
            double angularDistance = range / EARTH_RATIO_METERS;
            double trueCourse = bearing * DEG_TO_RAD;

            double lat = Math.Asin(
                Math.Sin(latA) * Math.Cos(angularDistance) +
                Math.Cos(latA) * Math.Sin(angularDistance) * Math.Cos(trueCourse));

            double dlon = Math.Atan2(
                Math.Sin(trueCourse) * Math.Sin(angularDistance) * Math.Cos(latA),
                Math.Cos(angularDistance) - Math.Sin(latA) * Math.Sin(lat));

            double lon = ((lonA + dlon + Math.PI) % (2 * Math.PI)) - Math.PI;

            return new GeoPoint(lon * RAD_TO_DEG, lat * RAD_TO_DEG);
        }
        
        #endregion

        #region User Access In Role
        public static bool CheckGeneralSupervisor(object obj)
        {
			if (obj is UserRoleData)
			{
				UserRoleData role = (UserRoleData)obj;
				long result = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(SmartCadHqls.IsAccessInRole, role.Code, "GeneralSupervisorName"));
				if (result >= 1)
					return true;
				return false;
			}
			else if (obj is int)
			{
				int roleCode = (int)obj;
				long result = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(SmartCadHqls.IsAccessInRole, roleCode, "GeneralSupervisorName"));
				if (result >= 1)
					return true;
				return false;
			}
			return false;
        }

        public static bool CheckFirstLevelOperator(int roleCode)
        {
            long result = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(SmartCadHqls.IsAccessInRole, roleCode, "UserApplicationData+Basic+FirstLevel"));
            if (result >= 1)
                return true;
            return false;
        }

        public static bool CheckDispatchOperator(int roleCode)
        {
            long result = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(SmartCadHqls.IsAccessInRole, roleCode, "UserApplicationData+Basic+Dispatch"));
            if (result >= 1)
                return true;
            return false;
        }

        public static bool CheckCctvOperator(int roleCode)
        {
            long result = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(SmartCadHqls.IsAccessInRole, roleCode, "UserApplicationData+Basic+Cctv"));
            if (result >= 1)
                return true;
            return false;
        }

        public static bool CheckSupervisor(int roleCode)
        {
            bool result = false;
            try
            {
                long accessCount = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(SmartCadHqls.IsSupervisorUser, roleCode, 
                    "FirstLevelSupervisorName", "DispatchSupervisorName"));
                if (accessCount >= 1)
                {
                    result = true;
                }
            }
            catch
            {
            }
            return result;
        }

        public static bool CheckApplicationSupervisor(object obj,string application)
        {
			int roleCode = 0;
            if (obj is UserRoleData)
                roleCode = ((UserRoleData)obj).Code;
			else
				roleCode = (int)obj;
				
			long result = 
				(long)SmartCadDatabase.SearchBasicObject(
					SmartCadHqls.GetCustomHql(
						SmartCadHqls.IsApplicationAccessInRoleForSupervisor,
						roleCode,
						application,
						"Supervisor"));
            if (result > 0)
                return true;
            return false;        
        }

		public static bool CheckUserAccess(int roleCode, string accessName)
		{
			long result = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(SmartCadHqls.IsAccessInRole, roleCode, accessName));

			if (result >= 1)
				return true;
			return false;
		}

		public static bool CheckUserApplication(int roleCode, string accessName)
		{
			long result = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(SmartCadHqls.IsAccessInRole, roleCode, accessName));

			if (result >= 1)
				return true;
			return false;
		}

		public static List<OperatorData> GetFirstLevelSupervisors()
        {
            return GetUsersWithAccess("FirstLevelSupervisor");
        }

        public static List<OperatorData> GetDispatchSupervisors()
        {
            return GetUsersWithAccess("DispatchSupervisor");
        }

        public static List<OperatorData> GetUsersWithAccess(params string[] accessPartialName)
        {
            Dictionary<int, OperatorData> values = new Dictionary<int, OperatorData>();
            DateTime now = SmartCadDatabase.GetTimeFromBD();
            
            foreach (string st in accessPartialName)
            {
                IList result = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetSupervisorWorkingNowWithAccess, st, 
                    ApplicationUtil.GetDataBaseFormattedDate(now), 
                    DateTime.Now.DayOfWeek.ToString()));
                              
                foreach (OperatorData ope in result)
                {
                    if (values.ContainsKey(ope.Code) == false)
                    {
                        values.Add(ope.Code, ope);
                    }
                }
            }
            return new List<OperatorData>(values.Values);
        }
        
        #endregion

        #region Indicator Helper

        public static double CalculateAmountSessionStatusHistory(IList sessionStatusHistory)
        {
            double result = 0;
            TimeSpan timeNow = DateTime.Now.TimeOfDay;
            foreach (SessionStatusHistoryData ssh in sessionStatusHistory)
            {
                TimeSpan sshSeconds = TimeSpan.Zero;
                if (ssh.EndDate.HasValue == true && ssh.EndDate.Value.TimeOfDay < timeNow)
                {
                    sshSeconds = ssh.EndDate.Value.TimeOfDay.Subtract(ssh.StartDate.Value.TimeOfDay);
                }
                else if (ssh.StartDate.Value.TimeOfDay < timeNow)
                {
                    sshSeconds = timeNow.Subtract(ssh.StartDate.Value.TimeOfDay);
                }
                result += sshSeconds.TotalSeconds;
            }
            return result;
        }
        
        /// <summary>
        /// Gets the available time an operator has in a specific status with pause.
        /// If result is less than 0, there is not available time in this status.
        /// </summary>
        /// <param name="operatorCode"></param>
        /// <param name="statusCustomCode"></param>
        /// <param name="applicationCode"></param>
        /// <returns></returns>
        public static double CalculateAmountTimeAvailableByStatusWithPause(int operatorCode,
            string statusCustomCode, int applicationCode)
        {
            double result = 0;
            double completeTime = 0;
            double timeUsedInStatus = 0;
            
            //Calculate the amount of working time schedule in this day.
            completeTime = OperatorScheduleManager.GetOperatorTodaysWorkingTimeScheduled(operatorCode);
            //Get operator status using custom code
            OperatorStatusData status = SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(
                SmartCadHqls.GetOperatorStatusByCustomCode, statusCustomCode)) as OperatorStatusData;
            if (status != null && status.IsPause())
            {
                IList workShiftSchedules = null;
                //Get all intervals of time(session status history) by specified status
                workShiftSchedules = SmartCadDatabase.SearchObjects(
                        SmartCadHqls.GetCustomHql(
                            SmartCadHqls.GetSessionStatusHistorySpecificDateOperatorNotReadySpecificStatus,
                                ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today),
                                ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today.AddDays(1)),
                                operatorCode,
                                status.CustomCode,
                                applicationCode));
                //convert previous intervals to an amount of time. This time is all the time available in this
                //status.
                timeUsedInStatus = ServiceUtil.CalculateAmountSessionStatusHistory(workShiftSchedules);

                result = ServiceUtil.CalculateBreakTimeOperatorStatusTime(completeTime, status);
                result -= timeUsedInStatus;
            }
            return result;
        }

        public static double CalculateAmountWorkAndTrainningTime(IList sessionStatusHistory, 
            IList trainingTime)
        {
           
            bool addTraining = false;
            double result = 0;
            TimeSpan timeNow = DateTime.Now.TimeOfDay;           
            TimeSpan sshSeconds = TimeSpan.Zero;
            TimeSpan startTime = TimeSpan.Zero;
            TimeSpan endTime = TimeSpan.Zero;
            TimeSpan endTriningTime = TimeSpan.Zero;
                        
            if (trainingTime.Count > 0)
            {
                //for(int j=0; j< trainingTime.Count; j++)
               // {
                 //   TrainingCourseSchedulePartsData training = (TrainingCourseSchedulePartsData)trainingTime[j];
                foreach (TrainingCourseSchedulePartsData training in trainingTime)
                {

                    addTraining = false;
                    if (training.End.Value.TimeOfDay < timeNow)
                    {
                        endTriningTime = training.End.Value.TimeOfDay;
                    }
                    else
                    {
                        endTriningTime = timeNow;
                    }
                                        
                    for (int i = 0; i < sessionStatusHistory.Count; i++)
                    {
                        SessionStatusHistoryData ssh = (SessionStatusHistoryData)sessionStatusHistory[i];
                        if (((ssh.Status == OperatorStatusData.Ready) ||
                             (ssh.Status == OperatorStatusData.Busy)) &&
                             ((ssh.Session.UserApplication == UserApplicationData.FirstLevel) ||
                             (ssh.Session.UserApplication == UserApplicationData.Dispatch)))
                        {
                            //The session was finished
                            if (ssh.EndDate.HasValue == true && ssh.EndDate.Value.TimeOfDay < timeNow)
                            {
                                //The session overlap the trainning
                                if ((training.Start <= ssh.EndDate) && (endTriningTime > ssh.StartDate.Value.TimeOfDay))
                                {
                                    //1
                                    #region 1
                                    if ((training.Start >= ssh.StartDate) && (endTriningTime >= ssh.EndDate.Value.TimeOfDay))
                                    {
                                        sessionStatusHistory.RemoveAt(i);
                                        i -= 1;
                                        if (endTime != TimeSpan.Zero && endTime > ssh.StartDate.Value.TimeOfDay)
                                        {
                                            sshSeconds += endTriningTime.Subtract(endTime);
                                        }
                                        else
                                        {
                                            sshSeconds += endTriningTime.Subtract(ssh.StartDate.Value.TimeOfDay);
                                        }
                                        addTraining = true;
                                        if (endTime < endTriningTime || endTime == TimeSpan.Zero)
                                        {
                                            endTime = endTriningTime;
                                        }
                                        
                                    }
                                    #endregion 1
                                    //2
                                    #region 2
                                    else if ((training.Start <= ssh.StartDate) && (endTriningTime <= ssh.EndDate.Value.TimeOfDay))
                                    {
                                        sessionStatusHistory.RemoveAt(i);
                                        i -= 1;
                                        if (endTime != TimeSpan.Zero && endTime > training.Start.Value.TimeOfDay)
                                        {
                                            sshSeconds += ssh.EndDate.Value.TimeOfDay.Subtract(endTime);
                                        }
                                        else
                                        {
                                            sshSeconds += ssh.EndDate.Value.TimeOfDay.Subtract(training.Start.Value.TimeOfDay);
                                        }
                                        addTraining = true;
                                        if (endTime < ssh.EndDate.Value.TimeOfDay || endTime == TimeSpan.Zero)
                                        {
                                            endTime = ssh.EndDate.Value.TimeOfDay;
                                        }
                                    }
                                    #endregion 2
                                    //3
                                    #region 3
                                    else if ((training.Start >= ssh.StartDate) && (endTriningTime <= ssh.EndDate.Value.TimeOfDay))
                                    {
                                        sessionStatusHistory.RemoveAt(i);
                                        i -= 1;
                                        if (endTime != TimeSpan.Zero && endTime > ssh.StartDate.Value.TimeOfDay)
                                        {
                                            sshSeconds += ssh.EndDate.Value.TimeOfDay.Subtract(endTime);
                                        }
                                        else
                                        {
                                            sshSeconds += ssh.EndDate.Value.TimeOfDay.Subtract(ssh.StartDate.Value.TimeOfDay);
                                        }
                                        addTraining = true;
                                        if (endTime < ssh.EndDate.Value.TimeOfDay || endTime == TimeSpan.Zero)
                                        {
                                            endTime = ssh.EndDate.Value.TimeOfDay;
                                        }
                                    }
                                    #endregion 3
                                    //4
                                    #region 4
                                    else if ((training.Start <= ssh.StartDate) && (endTriningTime >= ssh.EndDate.Value.TimeOfDay))
                                    {
                                        sessionStatusHistory.RemoveAt(i);
                                        i -= 1;
                                        if (endTime != TimeSpan.Zero && endTime > training.Start.Value.TimeOfDay)
                                        {
                                            sshSeconds += ssh.EndDate.Value.TimeOfDay.Subtract(endTime);
                                        }
                                        else
                                        {
                                            //sshSeconds += endTriningTime.Subtract(training.Start.Value.TimeOfDay);
                                            sshSeconds += ssh.EndDate.Value.TimeOfDay.Subtract(training.Start.Value.TimeOfDay);
                                        }
                                        addTraining = true;
                                        if (endTime < endTriningTime || endTime == TimeSpan.Zero)
                                        {
                                            endTime = endTriningTime;
                                        }
                                    }
                                    #endregion 4
                                }
                            }
                            //The session was not finished
                            else if (ssh.StartDate.Value.TimeOfDay < timeNow)
                            {
                                //The session overlap the trainning
                                if (endTriningTime > ssh.StartDate.Value.TimeOfDay)
                                {
                                    //1 y 3
                                    #region 1 y 3
                                    if (training.Start >= ssh.StartDate)
                                    {
                                        sessionStatusHistory.RemoveAt(i);
                                        i -= 1;
                                        if (endTime != TimeSpan.Zero && endTime > ssh.StartDate.Value.TimeOfDay)
                                        {
                                            sshSeconds += timeNow.Subtract(endTime);
                                        }
                                        else
                                        {
                                            sshSeconds += timeNow.Subtract(ssh.StartDate.Value.TimeOfDay);
                                        }
                                        addTraining = true;
                                        endTime = timeNow;
                                    }
                                    #endregion 1 y 3
                                    //2 y 4
                                    #region 2 y 4
                                    else if (training.Start < ssh.StartDate)
                                    {
                                        sessionStatusHistory.RemoveAt(i);
                                        i -= 1;
                                        if (endTime != TimeSpan.Zero && endTime > training.Start.Value.TimeOfDay)
                                        {
                                            sshSeconds += timeNow.Subtract(endTime);
                                        }
                                        else
                                        {
                                            sshSeconds += timeNow.Subtract(training.Start.Value.TimeOfDay);
                                        }
                                        addTraining = true;
                                        endTime = timeNow;
                                    }
                                    #endregion 2 y 4
                                }
                                
                            }
                        }
                        else if ((ssh.Session.UserApplication == UserApplicationData.FirstLevel) ||
                             (ssh.Session.UserApplication == UserApplicationData.Dispatch))
                        {
                  

                            startTime = ssh.StartDate.Value.TimeOfDay;
                            if ((startTime > endTime)&&(endTime != TimeSpan.Zero)
                                &&(endTriningTime >= startTime) &&(training.Start.Value.TimeOfDay <= endTime)&&(startTime > endTime))
                            {
                                sshSeconds += startTime.Subtract(endTime);
                                result += sshSeconds.TotalSeconds;
                                sshSeconds = TimeSpan.Zero;                               
                            }                           
                            if ((ssh.EndDate.HasValue == true) && (ssh.EndDate.Value.TimeOfDay >= endTriningTime) &&
                      (ssh.StartDate.HasValue == true) && (ssh.StartDate.Value.TimeOfDay <= training.Start.Value.TimeOfDay))
                            {
                                addTraining = true;
                            }
                            else if ((ssh.EndDate.HasValue == false) &&
                      (ssh.StartDate.HasValue == true) && (ssh.StartDate.Value.TimeOfDay <= training.Start.Value.TimeOfDay))
                            {
                                addTraining = true;
                            }

                            else if ((ssh.EndDate.HasValue == true) && (ssh.EndDate.Value.TimeOfDay <= endTriningTime))
                            {
                                
                                endTime = ssh.EndDate.Value.TimeOfDay;
                            }
                            else if(startTime == endTime)
                            {
                                endTime = timeNow;
                            }
                           
                        }                     
                        result += sshSeconds.TotalSeconds;
                        sshSeconds = TimeSpan.Zero;

                    }
                    //if the trainning time was not added
                    if ((addTraining == false) && (training.Start.Value.TimeOfDay<= timeNow))
                    {
                        if (endTime != TimeSpan.Zero && endTime <= training.Start.Value.TimeOfDay)
                        {
                            sshSeconds += endTriningTime.Subtract(training.Start.Value.TimeOfDay);
                            result += sshSeconds.TotalSeconds;
                            sshSeconds = TimeSpan.Zero;
                            endTime = endTriningTime;
                        }
                        else if ((endTime != TimeSpan.Zero && endTime >= training.Start.Value.TimeOfDay) && (endTriningTime > endTime))
                        {
                            sshSeconds += endTriningTime.Subtract(endTime);
                            result += sshSeconds.TotalSeconds;
                            sshSeconds = TimeSpan.Zero;
                            endTime = endTriningTime;
                        }
                    }
                }
            }
            //if the session time was not added
            foreach (SessionStatusHistoryData ssh in sessionStatusHistory)
            {
                if (((ssh.Status == OperatorStatusData.Ready) || 
                    (ssh.Status == OperatorStatusData.Busy)) && 
                    ((ssh.Session.UserApplication == UserApplicationData.FirstLevel) || 
                    (ssh.Session.UserApplication == UserApplicationData.Dispatch)))
                {
                    if (ssh.EndDate.HasValue == true && ssh.EndDate.Value.TimeOfDay < timeNow)
                    {
                        sshSeconds = ssh.EndDate.Value.TimeOfDay.Subtract(ssh.StartDate.Value.TimeOfDay);
                    }
                    else if (ssh.StartDate.Value.TimeOfDay < timeNow)
                    {
                        sshSeconds = timeNow.Subtract(ssh.StartDate.Value.TimeOfDay);
                    }
                    result += sshSeconds.TotalSeconds;
                    sshSeconds = TimeSpan.Zero;
                }
            }

            
            return result;
        }

        public static double CalculateBreakTimeOperatorStatusTime(double value, OperatorStatusData os)
        {
            double first = value * (Convert.ToDouble(os.Porcentage.Value) / 100);
            double second = first * (Convert.ToDouble(os.Tolerance.Value) / 100);
            return first + second;
            //return value * ((100 - os.Tolerance.Value + os.Porcentage.Value) / 100);
        }

        public static IList StatusesToCalculateOperatorComplianceSchedule()
        {
            ArrayList list = new ArrayList();
            list.Add(OperatorStatusData.Absent);
            list.Add(OperatorStatusData.Bathroom);
            list.Add(OperatorStatusData.Rest);
            list.Add(OperatorStatusData.Reunion);
            return list;
        }
        #endregion

        public static object CreateInstanceFromData(string className)
        {
            return Activator.CreateInstance(SmartCadDatabase.DatabaseAssembly, SmartCadDatabase.DatabaseAssembly + "." + className).Unwrap();
        }


		public static DepartmentStationData GetDepartmentStationForThisPoint(AddressClientData incidentAddress, int depCode)
		{
			string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetDepartmentStationAddressByDepartmentTypeCode, depCode);
            IEnumerable<DepartmentStationAddressData> stationsAddress = 
				SmartCadDatabase.SearchObjects(hql).Cast<DepartmentStationAddressData>();
			var stationAddressGrouped = stationsAddress.OrderBy(address => address.PointNumber)
				.GroupBy(address => address.Station.Code);

			GeoPoint point = new GeoPoint(incidentAddress.Lon, incidentAddress.Lat);
			if (stationAddressGrouped != null)
            {
                int i = 0;
                foreach (var station in stationAddressGrouped)
                {
                    if (station.Count() > 2)
                        if (ApplicationUtil.PointInPolygon(point, station
                                                .Select<DepartmentStationAddressData, GeoPoint>(address => new GeoPoint(address.Address.Lon, address.Address.Lat))
                                                .ToList()))
                            return station.ElementAt(0).Station;
                }
            }
			return null;
		}

		public static RouteAddressData GetNearestStop(double lat, double lon, ISet routeAddress)
		{
			RouteAddressData stop = null;
			double distance = double.MaxValue;
			foreach (RouteAddressData racd in routeAddress)
			{
				double newDistance = GeoCodeCalc.CalcDistance(lat, lon, racd.Address.Lat, racd.Address.Lon);
				if (distance > newDistance &&
					string.IsNullOrEmpty(racd.Name) == false)
				{
					stop = racd;
					distance = newDistance;
				}
			}
			return stop;
		}

		public static RouteAddressClientData GetNearestStopClient(double lat, double lon, IList routeAddress)
		{
			RouteAddressClientData stop = null;
			double distance = double.MaxValue;
			foreach (RouteAddressClientData racd in routeAddress)
			{
				double newDistance = GeoCodeCalc.CalcDistance(lat, lon, racd.Lat, racd.Lon);
				if (distance > newDistance &&
					string.IsNullOrEmpty(racd.Name) == false)
				{
					stop = racd;
					distance = newDistance;
				}
			}
			return stop;
		}

		public static WorkShiftRouteData GetCurrentRoute(UnitData unit)
		{
			DateTime now = SmartCadDatabase.GetTimeFromBD();
			foreach (WorkShiftRouteData rt in unit.WorkShiftRoutes)
			{
				if (SmartCadDatabase.IsInitialize(rt.WorkShift.Schedules) == false)
					SmartCadDatabase.InitializeLazy(rt.WorkShift, rt.WorkShift.Schedules);

				foreach (WorkShiftScheduleVariationData schedule in rt.WorkShift.Schedules)
					if (schedule.Start.HasValue &&
						schedule.End.HasValue &&
						schedule.Start.Value <= now &&
						schedule.End.Value > now)					
						return rt;
			}
			return null;
		}

		public static bool CheckUnitInside2PointsRoute(UnitData unit, RouteAddressData p1, RouteAddressData p2,double distTol)
		{
			double u_nominator = ((unit.GPS.Longitude-p1.Address.Lon)*(p2.Address.Lon-p1.Address.Lon) + (unit.GPS.Latitude-p1.Address.Lat)*(p2.Address.Lat-p1.Address.Lat));
			double u_denominator = (Math.Pow((p2.Address.Lon-p1.Address.Lon),2) + Math.Pow((p2.Address.Lat-p1.Address.Lat),2));
			double u = u_nominator /u_denominator;

			if (u > 1 || u < 0)
			{
				double distP1Unit = GeoCodeCalc.CalcDistance(p1.Address.Lat, p1.Address.Lon, unit.GPS.Latitude, unit.GPS.Longitude);
				double distP2Unit = GeoCodeCalc.CalcDistance(p2.Address.Lat, p2.Address.Lon, unit.GPS.Latitude, unit.GPS.Longitude);
				double minDist = Math.Min(distP1Unit, distP2Unit);
			
				return minDist < distTol/1000.0;
			}
			else
			{
				double px = p1.Address.Lon + u * (p2.Address.Lon - p1.Address.Lon);
				double py = p1.Address.Lat + u * (p2.Address.Lat - p1.Address.Lat);
				double distPUnit = GeoCodeCalc.CalcDistance(py, px, unit.GPS.Latitude, unit.GPS.Longitude);

				return distPUnit < distTol / 1000.0;
			}
		}

		public static void UnAssignAlertNotificationFromThisOperator(OperatorData operatorData)
		{
			IList list = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetAlertNotificationByOperatorCode, operatorData.Code));
			foreach (AlertNotificationData item in list)
			{
				item.AttendingOperator = null;
				item.Status = AlertNotificationData.AlertStatus.NotTaken;
			}
			SmartCadDatabase.SaveOrUpdateCollection(list,null);
		}
	}	

    /// <summary>
    /// Base classes of windows services.
    /// This class consist of NLB methods and checking that the program is always running.
    /// </summary>
    public abstract class NLBServiceBase : ServiceBase
    {
        #region Fields
        /// <summary>
        /// Indicates how many times the nlb node has been restarted
        /// </summary>
        private int restartTimes = 0;
        /// <summary>
        /// Indicates if the service is stop.
        /// </summary>
        private bool stoping = false;
        /// <summary>
        /// Local NLB Node.
        /// </summary>
        private WMI_NlbNode localNode = null;
        /// <summary>
        /// Timer to check nodes.
        /// </summary>
        private Timer timer = null;
        /// <summary>
        /// Dictionary to keep to counts of error per NLB Node.
        /// </summary>
        private Dictionary<string, int> errorCount;
        /// <summary>
        /// Handle to indicate exactly when the service is really running.
        /// </summary>
        private EventWaitHandle startNLB = null;
        /// <summary>
        /// The process of the program that the service need to run.
        /// </summary>
        private Process process;

        #endregion
        #region Virtual Methods
        /// <summary>
        /// Start service before NLB Check.
        /// </summary>
        public virtual void OnStartBeforeNLB()
        {}
        /// <summary>
        /// Starting service program.
        /// </summary>
        public virtual void OnStartService()
        { }
        /// <summary>
        /// After the service and program had started
        /// </summary>
        public virtual void OnStartFinished()
        { }
        /// <summary>
        /// The service is beginning to stop.
        /// </summary>
        public virtual void OnStopBeforeNLB()
        { }
        /// <summary>
        /// Before the program is kill.
        /// </summary>
        public virtual void OnStopBeforeKill()
        { }
        /// <summary>
        /// After the program is kill.
        /// </summary>
        public virtual void OnStopAfterKill()
        { }
        /// <summary>
        /// Load the configuration for the program.
        /// </summary>
        public virtual void OnLoadConfiguration(string path)
        { }
        /// <summary>
        /// Check if the node is active.
        /// Return 0 if it is active, 0 otherwise.
        /// </summary>
        /// <param name="node">NLB Node to be check</param>
        public virtual int OnNodeCheck(WMI_NlbNode node)
        {
            return 0;
        }
        /// <summary>
        /// Clean any objects used.
        /// </summary>
        public virtual void OnNodeClean()
        { }
        /// <summary>
        /// The amount of time, in miliseconds, to wait to start up the node.
        /// </summary>
        /// <returns>The amount of time to wait to start up the node.</returns>
        protected abstract int WaitTimeToStartNode
        {
            get;
        }
        /// <summary>
        /// The amount of time to perform a NLB nodes check.
        /// </summary>
        /// <returns>The amount of time to check the NLB nodes.</returns>
        protected abstract int CheckNodeTime
        {
            get;
        }
        /// <summary>
        /// Minimum running process time.
        /// </summary>
        protected abstract int MinimumRunTime
        {
            get;
        }
        /// <summary>
        /// Maximum restart times to try start the process.
        /// </summary>
        protected abstract int MaximumRestartTimes
        {
            get;
        }
        /// <summary>
        /// The port use for the NLB.
        /// </summary>
        protected abstract int NodesPort
        {
            get;
        }
        #endregion

        protected override void OnStart(string[] args)
        {
            try
            {
                Uri uri = new Uri(Assembly.GetExecutingAssembly().EscapedCodeBase);
                FileInfo fi = new FileInfo(uri.LocalPath);
                OnLoadConfiguration(fi.Directory.Parent.FullName);
                OnStartBeforeNLB();

                if (WMI_NlbNode.CheckNlb() == true)
                {
                    WMI_NlbNode.AllComputerNameNodes();
                    WMI_NlbNode.AllDedicatedIPsNodes();
                    WMI_NlbNode.AllNodes();
                    localNode = new WMI_NlbNode();
                    errorCount = new Dictionary<string, int>();
                    timer = new Timer(new TimerCallback(CheckAllNodes), null, -1, -1);
                    startNLB = new EventWaitHandle(false, EventResetMode.ManualReset, ServiceName);
                }

                process = new Process();
                process.StartInfo = new ProcessStartInfo();
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.WorkingDirectory = fi.DirectoryName;
                process.StartInfo.FileName = uri.LocalPath;
                process.EnableRaisingEvents = true;
                process.Exited += new System.EventHandler(process_Exited);
                process.Start();

                OnStartService();

                if (WMI_NlbNode.CheckNlb() == true)
                {
                    if (startNLB.WaitOne(WaitTimeToStartNode, false) == true)
                    {
                        localNode.Start();
                    }
                    timer.Change(0, CheckNodeTime);
                }

                OnStartFinished();
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex.ToString());
            }
        }

        protected override void OnStop()
        {
            try
            {
                OnStopBeforeNLB();

                if (WMI_NlbNode.CheckNlb() == true)
                {
                    localNode.Stop();
                    timer.Change(-1, -1);
                    timer.Dispose();
                    startNLB.Close();
                }

                OnStopBeforeKill();

                stoping = true;
                process.Kill();

                OnStopAfterKill();
            }
            catch
            {
            }
        }

        protected void process_Exited(object sender, EventArgs args)
        {
            try
            {
                TimeSpan runTime = process.ExitTime.Subtract(process.StartTime);

                if (runTime.TotalMinutes < MinimumRunTime)
                    restartTimes++;
                else
                    restartTimes = 0;

                if (restartTimes > MaximumRestartTimes)
                {
                    Stop();
                    SmartLogger.Print(ResourceLoader.GetString2("ProcessStop", restartTimes.ToString()));
                }

                if (!stoping)
                {
                    Uri uri = new Uri(Assembly.GetExecutingAssembly().EscapedCodeBase);
                    FileInfo fi = new FileInfo(uri.LocalPath);

                    process = new Process();
                    process.StartInfo = new ProcessStartInfo();
                    process.StartInfo.UseShellExecute = false;
                    process.StartInfo.CreateNoWindow = true;
                    process.StartInfo.WorkingDirectory = fi.DirectoryName;
                    process.StartInfo.FileName = uri.LocalPath;
                    process.EnableRaisingEvents = true;
                    process.Exited += new System.EventHandler(process_Exited);
                    process.Start();
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex.ToString());
            }
        }

        /// <summary>
        /// Check all the nodes in the cluster, to check that are running normal.
        /// </summary>
        /// <param name="data">Not used.</param>
        protected void CheckAllNodes(object data)
        {
            Uri uri = new Uri(Assembly.GetExecutingAssembly().EscapedCodeBase);
            FileInfo fi = new FileInfo(uri.LocalPath);
            OnLoadConfiguration(fi.Directory.Parent.FullName);

            foreach (WMI_NlbNode node in WMI_NlbNode.AllNodes())
            {
                if (errorCount.ContainsKey(node.ComputerName) == false)
                    errorCount.Add(node.ComputerName, 0);
                try
                {
                    try
                    {
                        int result = OnNodeCheck(node);
                        node.Refresh();
                        if (result == 1)
                        {
                            if ((node.StatusCode == NLB_ReturnValues.WLBS_CONVERGED) ||
                                (node.StatusCode == NLB_ReturnValues.WLBS_CONVERGING) ||
                                (node.StatusCode == NLB_ReturnValues.WLBS_DEFAULT))
                                errorCount[node.ComputerName] += 1;
                        }
                        else if (result == 0)
                        {
                            errorCount[node.ComputerName] = 0;
                            if (!((node.StatusCode == NLB_ReturnValues.WLBS_CONVERGED) ||
                                (node.StatusCode == NLB_ReturnValues.WLBS_CONVERGING) ||
                                (node.StatusCode == NLB_ReturnValues.WLBS_DEFAULT)))
                            {
                                try
                                {
                                    node.Start();
                                    SmartLogger.Print(ResourceLoader.GetString2("NodeStarted", node.ComputerName));
                                }
                                catch { }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        node.Refresh();
                        if (node.Running == true)
                        {
                            errorCount[node.ComputerName] += 1;
                        }
                        else
                        {
                            errorCount[node.ComputerName] = 0;
                        }

                        if (errorCount[node.ComputerName] > 2)
                        {
                            if (ex is ArgumentNullException)
                            {
                                SmartLogger.Print(ResourceLoader.GetString2("InvalidMachineName", node.ComputerName));
                                SmartLogger.Print(ex);
                            }
                            else if (ex is ArgumentOutOfRangeException)
                            {
                                SmartLogger.Print(ResourceLoader.GetString2("InvalidPort", NodesPort.ToString()));
                                SmartLogger.Print(ex);
                            }
                            else if (ex is SocketException)
                            {
                                SmartLogger.Print(ResourceLoader.GetString2("SocketException", ((SocketException)ex).SocketErrorCode.ToString()));
                                SmartLogger.Print(ex);
                            }
                            else if (ex is ObjectDisposedException)
                            {
                                SmartLogger.Print(ResourceLoader.GetString2("TcpClientException"));
                                SmartLogger.Print(ex);
                            }
                            else
                            {
                                SmartLogger.Print(ex);
                            }
                        }
                    }
                    finally
                    {
                        OnNodeClean();
                    }
                    if (errorCount[node.ComputerName] > MaximumRestartTimes)
                    {
                        errorCount[node.ComputerName] = 0;
                        SmartLogger.Print(ResourceLoader.GetString2("NodeStoped", node.ComputerName));

                        node.Stop();
                    }
                }
                catch
                { }
            }
        }		
    }

    public enum SupervisorType
    { 
        General = 0,
        FirstLevel = 1,
        Dispatch = 2
    }	
}
