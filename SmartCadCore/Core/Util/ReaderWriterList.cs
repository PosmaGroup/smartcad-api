using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.Core.Util
{
    public class ReaderWriterList<T> where T : ICloneable
    {
        #region Fields

        List<T> items;
        List<object> listLocks;
        object objLock;

        private bool isSingle;
        private bool isMultiple;
        private int size;

        #endregion

        #region Properties

        /// <summary>
        /// Indica si el bloqueo es simple
        /// </summary>
        public bool Single
        {
            get { return isSingle; }
        }

        /// <summary>
        /// Indica si el bloqueo es multiple
        /// </summary>
        public bool Multiple
        {
            get { return isMultiple; }
        }

        public int ListSize
        {
            get { return items.Count; }
        }

        #endregion

        public ReaderWriterList()
        {
            items = new List<T>();
            objLock = new object();
        }

        public ReaderWriterList(ReaderWriterOptions option)
        {
            items = new List<T>();
            objLock = new object();
            switch (option)
            {
                case ReaderWriterOptions.SingleReaderWriter:
                    this.isSingle = true;
                    this.isMultiple = false;
                    break;
                case ReaderWriterOptions.MultipleReaderWriter:
                    this.isMultiple = true;
                    this.isSingle = false;
                    listLocks = new List<object>();
                    break;
                default:
                    break;
            }

        }

        /// <summary>
        /// Agrega un elemento, bloquea toda la lista.
        /// </summary>
        /// <param name="elem"></param>
        public void AddElement(T elem)
        {
            lock (objLock)
            {
                items.Add(elem);
                if (Multiple)
                    listLocks.Add(new object());
            }
        }

        /// <summary>
        /// actualiza un elemento en la posicion indicada
        /// </summary>
        /// <param name="elem">Elemento a actualizar</param>
        /// <param name="index"></param>
        public void UpdateElement(T elem, int index)
        {
            if (isSingle)
            {
                lock (objLock)
                {
                    items[index] = elem;
                }
            }
            else
            {
                lock (listLocks[index])
                {
                    items[index] = elem;
                }
            }
        }

        public T GetElement(int index)
        {
            T retval = default(T);
            if (Single)
            {
                lock (objLock)
                {
                    retval = items[index];
                    return (T)retval.Clone();
                }
            }
            else
            {
                lock (listLocks[index])
                {
                    retval = items[index];
                    return (T)retval.Clone();
                }
            }
        }

        public void DeleteElement(int index)
        {
            lock (objLock)
            {
                items.RemoveAt(index);
                if (Multiple)
                    listLocks.RemoveAt(index);
            }
        }

    }

    public enum ReaderWriterOptions
    {
        SingleReaderWriter,
        MultipleReaderWriter
    }
}
