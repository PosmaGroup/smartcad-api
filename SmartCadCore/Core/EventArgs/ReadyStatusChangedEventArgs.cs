using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.Core
{
    [Serializable]
    public class ReadyStatusChangedEventArgs : TelephonyActionEventArgs
    {
        public readonly bool IsReady;
        public readonly string NotReadyReasonCode;
        public const string CallNotAnswered = "CALL_NOT_ANSWERED";

        public ReadyStatusChangedEventArgs(bool isReady, string notReadyReasonCode)
        {
            IsReady = isReady;
            NotReadyReasonCode = notReadyReasonCode;
        }
    }
}
