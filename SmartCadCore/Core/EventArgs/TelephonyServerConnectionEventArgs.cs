﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadCore.Core
{
    /// <summary>
    /// Provide data for the connection status of the telephony server.
    /// </summary>
    [Serializable]
    public class TelephonyServerConnectionEventArgs : TelephonyActionEventArgs
    {
        public bool Connected { get; set; }
    }
}
