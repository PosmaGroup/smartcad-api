using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.Core
{
    [Serializable]
    public class DoNotDisturbChangedEventArgs : TelephonyActionEventArgs
    {
        public readonly bool DoNotDisturb;

        public DoNotDisturbChangedEventArgs(bool doNotDisturb)
        {
            DoNotDisturb = doNotDisturb;
        }
    }
}
