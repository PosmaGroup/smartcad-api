﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadCore.Core
{
    /// <summary>
    /// This event refers to set Ready operation
    /// </summary>
    [Serializable]
    public class AgentReadyEventArgs : TelephonyActionEventArgs
    {
        private string thisNumber;
        private string agentID;
        private string queue;
        private TimeSpan time;
        private string reason;

        /// <summary>
        /// It refers to the extension number that the agent is taking control.
        /// </summary>
        public string ThisNumber
        {
            get
            {
                return thisNumber;
            }
            set
            {
                thisNumber = value;
            }
        }
        
        /// <summary>
        /// Agent ID.
        /// </summary>
        public string AgentID
        {
            get
            {
                return agentID;
            }
            set
            {
                agentID = value;
            }
        }

        /// <summary>
        /// Queue identifier 
        /// </summary>
        public string Queue
        {
            get
            {
                return queue;
            }
            set
            {
                queue = value;
            }
        }

        /// <summary>
        /// Time when occurs the event
        /// </summary>
        public TimeSpan Time
        {
            get
            {
                return time;
            }
            set
            {
                time = value;
            }
        }

        public string Reason
        {
            get
            {
                return reason;
            }
            set
            {
                reason = value;
            }
        }

        public AgentReadyEventArgs(string number, string agentID, string queue, TimeSpan eventTime, string reason)
        {
            this.thisNumber = number;
            this.agentID = agentID;
            this.queue = queue;
            this.time = eventTime;
            this.reason = reason;
        }
    }
}
