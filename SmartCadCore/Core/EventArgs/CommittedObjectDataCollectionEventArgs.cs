using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using SmartCadCore.Common;

namespace SmartCadCore.Core
{
    [Serializable]
    public class CommittedObjectDataCollectionEventArgs :  ObjectDataCollectionEventArgs
    {
        private CommittedDataAction action;
        private bool updatingWorkshift = false;
        
        public bool UpdatingWorkshift
        {
            get
            {
                return updatingWorkshift;
            }
            set
            {
                updatingWorkshift = value;
            }
        }

        public CommittedDataAction Action
        {
            get
            {
                return action;
            }
        }

        public CommittedObjectDataCollectionEventArgs(object objectData, CommittedDataAction action, 
            SmartCadContext operatorContext)
            : base(operatorContext)
        { 
            ArrayList list = new ArrayList();
            if (objectData != null)
            list.Add(objectData);

            this.objects = list;
            this.action = action;
        }

        public CommittedObjectDataCollectionEventArgs(IList objects, CommittedDataAction action, bool updatingWS,
            SmartCadContext operatorContext)
            : base(objects, operatorContext)
        {
            this.action = action;
            this.UpdatingWorkshift = updatingWS;
        }
    }
}
