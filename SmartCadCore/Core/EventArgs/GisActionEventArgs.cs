using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Common;
using SmartCadCore.Enums;

namespace SmartCadCore.Core
{
    [Serializable]
    public class GisActionEventArgs : EventArgs
    {
    }

    [Serializable]
    public class GeoPointEventArgs : GisActionEventArgs
    {
        private GeoPoint point;

        public GeoPointEventArgs(GeoPoint point)
        {
            this.point = point;
        }

        public GeoPoint Point
        {
            get
            {
                return point;
            }
            set
            {
                point = value;
            }
        }
    }

    [Serializable]
    public class GeoPointListEventArgs : GisActionEventArgs
    {
        private List<GeoPoint> points;

        public GeoPointListEventArgs(List<GeoPoint> points)
        {
            this.points = points;
        }

        public List<GeoPoint> Points
        {
            get
            {
                return points;
            }
            set
            {
                points = value;
            }
        }
    }

    [Serializable]
    public class DisplayGeoPointEventArgs : GeoPointEventArgs
    {
        public DisplayGeoPointEventArgs(GeoPoint point)
            : base(point)
        {
        }
    }

     [Serializable]
    public class SendACKEventArgs : GisActionEventArgs
    {
         public SendACKEventArgs(bool connected)
        {
            Connected = connected;
        }

         public bool Connected { get; set; }
    }

    [Serializable]
    public class HLObjectsEventArgs : GisActionEventArgs 
    {
        public List<HLGisObject> HLObject { get; set; }

        public HLObjectsEventArgs(List<HLGisObject> obj)
            : base()
        {
            HLObject = obj;
        }
    }

    [Serializable]
    public class SendGeoPointEventArgs : GeoPointEventArgs
    {
        private bool mapIsLoad;
        private int currentZoneCode;
        private string zone;
        private string street;
        private ShapeType type;

        public int ZoneCode 
        {
            get 
            {
                return currentZoneCode;
            }
            set
            {
                currentZoneCode = value;
            }
        }

        public string Zone
        {
            get
            {
                return zone;
            }
            set
            {
                zone = value;
            }
        }

        public string Street
        {
            get
            {
                return street;
            }
            set
            {
                street = value;
            }
        }
        
        public bool MapIsLoad
        {
            get
            {
                return mapIsLoad;
            }
            set
            {
                mapIsLoad = value;
            }
        }

        public ShapeType Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }
        }

        public bool IsSynchronize
        {
            get
            {
                return Point != null && Point.Lat != 0 && Point.Lon != 0;
            }
        }


        public SendGeoPointEventArgs(GeoPoint point, bool load, string zone, string street, ShapeType type)
            : base(point)
        {
            mapIsLoad = load;
            this.zone = zone;
            this.street = street;
            this.type = type;
        }

        public SendGeoPointEventArgs(GeoPoint point)
            : base(point)
        {
        }

    }

    [Serializable]
    public class SendActivateLayerEventArgs: GisActionEventArgs
    {

        public ShapeType Type { get; set; }

        public string Station { get; set; }

        public int Zone { get; set; }

        public int DepartmentType { get; set; }
        public bool Activate { get; set; }


        public SendActivateLayerEventArgs(ShapeType type)
            : base()
        {
            this.Type = type;
            this.Zone = -1;
            this.DepartmentType = 1;
            this.Activate = true;
        }

        public SendActivateLayerEventArgs(ShapeType type, bool activate)
            : base()
        {
            this.Type = type;
            this.Zone = -1;
            this.DepartmentType = 1;
            this.Activate = activate;
        }

        public SendActivateLayerEventArgs(ShapeType type, int departmentType, int zone)
            : base()
        {
            this.Type = type;
            this.Zone = zone;
            this.DepartmentType = departmentType;
            this.Activate = true;
        }

        public SendActivateLayerEventArgs(ShapeType type, int departmentType, int zone, string station)
            : base()
        {
            this.Type = type;
            this.Zone = zone;
            this.DepartmentType = departmentType;
            this.Station = station;
            this.Activate = true;
        }
    }

    [Serializable]
    public class SendGeoPointListEventArgs : GeoPointListEventArgs
    {

        public int DepartmentCode
        {
            get;set;
        }

        public int ZoneCode
        {
            get;set;
        }

        public string StationName
        {
            get;
            set;
        }

        public ShapeType Type { get; set; }

        public bool IsSynchronize
        {
            get
            {
                return Points != null && Points.Count > 0 && Points[0].Lat != 0 && Points[0].Lon != 0;
            }
        }

        public SendGeoPointListEventArgs(List<GeoPoint> points, int departmentType, int zone, ShapeType type)
            : base(points)
        {
            this.DepartmentCode = departmentType;
            this.ZoneCode = zone;
            this.Type = type;
        }

        public SendGeoPointListEventArgs(List<GeoPoint> points, int departmentType, int departmentZone, string departmentStation, ShapeType type)
            : base(points)
        {
            this.DepartmentCode = departmentType;
            this.ZoneCode = departmentZone;
            this.StationName = departmentStation;
            this.Type = type;
        }

    }

    [Serializable]
    public class SearchGeoAddressEventArgs : GisActionEventArgs
    {
        private GeoAddress geoAddress;

        public GeoAddress GeoAddress
        {
            set
            {
                geoAddress = value;
            }

            get
            {
                return geoAddress;
            }
        }

        public SearchGeoAddressEventArgs()
            : base()
        {}
    }

    [Serializable]
    public class SendEnableButtonEventArgs : GisActionEventArgs
    {
        private bool menable;
        private ButtonType button;

        public bool Enable
        {
            get
            {
                return menable;
            }
            set
            {
                menable = value;
            }
        }

        public ButtonType Button
        {
            get
            {
                return button;
            }
            set
            {
                button = value;
            }
        }

        public SendEnableButtonEventArgs(ButtonType buttonType, bool enable)
            : base()
        {
            Button = buttonType;
            Enable = enable;
        }

    }

    [Serializable]
    public class SendSelectDepartmentType : GisActionEventArgs
    {
        public int DepartmentCode
        {
            get;
            set;
        }

        public SendSelectDepartmentType(int departmentCode)
            : base()
        {
            DepartmentCode = departmentCode;
        }

    }

    [Serializable]
    public class VerifyStateFrontClientEventArgs : GisActionEventArgs 
    {
        public VerifyStateFrontClientEventArgs()
        : base ()
        {}
    }

}
