﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadCore.Core
{
    /// <summary>
    /// This event occurs when a call is established after it was made internally.
    /// </summary>
    [Serializable]
    public class CallMadeEventArgs : TelephonyActionEventArgs
    {
    }
}
