using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SmartCadCore.Core
{
    [Serializable]
    public class TaskDataCollectionEventArgs :  ObjectDataCollectionEventArgs
    {
        private TaskBase task;

        public TaskBase Task
        {
            get
            {
                return task;
            }
        }

        public TaskDataCollectionEventArgs(TaskBase task, IList objects)
            : base(objects, null)
        {
            this.task = task;
        }
    }
    /*
    [Serializable]
    public class FilterObjectDataCollectionEventArgs : ObjectDataCollectionEventArgs
    {
        private FilterBase filter;

        public FilterBase Filter
        {
            get
            {
                return filter;
            }
        }

        public FilterObjectDataCollectionEventArgs(IList objects, FilterBase filter)
            : base(objects)
        {
            this.filter = filter;
        }
    }*/
}
