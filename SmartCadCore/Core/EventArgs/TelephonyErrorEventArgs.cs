﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadCore.Core
{
    /// <summary>
    /// Contains information about some error ocurred during all the agent activity.
    /// </summary>
    [Serializable]
    public class TelephonyErrorEventArgs : TelephonyActionEventArgs
    {
        private string errorMessage;
        private string agentID;
        private string queue;
        private string thisNumber;
        private TimeSpan time;

        /// <summary>
        /// Error message
        /// </summary>
        public string ErrorMessage
        {
            get
            {
                return errorMessage;
            }
            set
            {
                errorMessage = value;
            }
        }

        /// <summary>
        /// Agent ID
        /// </summary>
        public string AgentID
        {
            get
            {
                return agentID;
            }
            set
            {
                agentID = value;
            }
        }

        /// <summary>
        /// Phone number which is being used by the agent
        /// </summary>
        public string ThisNumber
        {
            get
            {
                return thisNumber;
            }
            set
            {
                thisNumber = value;
            }
        }

        
        /// <summary>
        /// Queue identifier 
        /// </summary>
        public string Queue
        {
            get
            {
                return queue;
            }
            set
            {
                queue = value;
            }
        }

        /// <summary>
        /// Time when occurs the event
        /// </summary>
        public TimeSpan Time
        {
            get
            {
                return time;
            }
            set
            {
                time = value;
            }
        }

        public TelephonyErrorEventArgs(string error, string agentID, string thisNumber, string queue, TimeSpan eventTime)
        {
            this.errorMessage = error;
            this.agentID = agentID;
            this.thisNumber = thisNumber;
            this.queue = queue;
            this.time = eventTime;
        }
    }
}
