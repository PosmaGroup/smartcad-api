﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadCore.Core
{
    /// <summary>
    /// This event has information about a call that is being placed.
    /// </summary>
    [Serializable]
    public class CallDialingEventArgs : TelephonyActionEventArgs
    {
        private string thisNumber;
        private string agentID;
        private string queue;
        private string destinationNumber;
        private TimeSpan time;

        /// <summary>
        /// Phone number where call was originated. This is the agent extension.
        /// </summary>
        public string ThisNumber
        {
            get
            {
                return thisNumber;
            }
            set
            {
                thisNumber = value;
            }
        }
        
        /// <summary>
        /// This agent ID refers to selected agent who will answer selected call.
        /// </summary>
        public string AgentID
        {
            get
            {
                return agentID;
            }
            set
            {
                agentID = value;
            }
        }

        /// <summary>
        /// Queue identifier 
        /// </summary>
        public string Queue
        {
            get
            {
                return queue;
            }
            set
            {
                queue = value;
            }
        }

        /// <summary>
        /// Time when occurs the event
        /// </summary>
        public TimeSpan Time
        {
            get
            {
                return time;
            }
            set
            {
                time = value;
            }
        }

        /// <summary>
        /// Destination number.
        /// </summary>
        public string DestinationNumber
        {
            get
            {
                return destinationNumber;
            }
            set
            {
                destinationNumber = value;
            }
        }

        public CallDialingEventArgs(string ani, string agentID, string queue, string destinationNumber, TimeSpan eventTime)
        {
            this.thisNumber = ani;
            this.agentID = agentID;
            this.queue = queue;
            this.destinationNumber = destinationNumber;
            this.time = eventTime;
        }
    }
}
