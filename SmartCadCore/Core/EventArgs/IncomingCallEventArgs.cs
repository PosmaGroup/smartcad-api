using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    [Serializable]
    public class IncomingCallEventArgs : EventArgs
    {
        private PublicLineData publicLine;

        public IncomingCallEventArgs()
        { }

        public IncomingCallEventArgs(PublicLineData publicLine)
        {
            this.publicLine = publicLine;
        }

        public PublicLineData PublicLine
        {
            get
            {
                return publicLine;
            }
        }
    }
}
