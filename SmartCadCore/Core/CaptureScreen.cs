using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Diagnostics;
using System.ComponentModel;
using SmartCadCore.Common;

namespace SmartCadCore.Core
{
    class CaptureScreenEngine
    {
        //This structure shall be used to keep the size of the screen.
        public struct SIZE
        {
            public int cx;
            public int cy;
        }

        static Bitmap CaptureScreen(IntPtr screenPtr)
        {
            SIZE size;
            IntPtr hBitmap;
            //IntPtr hDC = Win32Stuff.GetDC(screenPtr);
            IntPtr hDC = Win32Stuff.GetWindowDC(screenPtr);

            Win32Stuff.RECT windowRect = new Win32Stuff.RECT();
            Win32Stuff.GetWindowRect(screenPtr, ref windowRect);
            size.cx = (windowRect.right - windowRect.left)*2;
            size.cy = (windowRect.bottom - windowRect.top);

            IntPtr hMemDC = GDIStuff.CreateCompatibleDC(hDC);

            hBitmap = GDIStuff.CreateCompatibleBitmap(hDC, size.cx, size.cy);

            if (hBitmap != IntPtr.Zero)
            {
                IntPtr hOld = (IntPtr)GDIStuff.SelectObject
                                       (hMemDC, hBitmap);

                GDIStuff.BitBlt(hMemDC, 0, 0, size.cx, size.cy, hDC,
                                               0, 0, GDIStuff.SRCCOPY);

                GDIStuff.SelectObject(hMemDC, hOld);
                GDIStuff.DeleteDC(hMemDC);
                Win32Stuff.ReleaseDC(screenPtr, hDC);
                Bitmap bmp = System.Drawing.Image.FromHbitmap(hBitmap);
                GDIStuff.DeleteObject(hBitmap);
                GC.Collect();
                return bmp;
            }
            return null;

        }


        static Icon CaptureCursor(ref int x, ref int y)
        {
            Icon bmp;
            IntPtr hicon;
            Win32Stuff.CURSORINFO ci = new Win32Stuff.CURSORINFO();
            Win32Stuff.ICONINFO icInfo;
            ci.cbSize = Marshal.SizeOf(ci);
            if (Win32Stuff.GetCursorInfo(out ci))
            {
                if (ci.flags == Win32Stuff.CURSOR_SHOWING)
                {
                    hicon = Win32Stuff.CopyIcon(ci.hCursor);
                    if (Win32Stuff.GetIconInfo(hicon, out icInfo))
                    {
                        x = ci.ptScreenPos.x - ((int)icInfo.xHotspot);
                        y = ci.ptScreenPos.y - ((int)icInfo.yHotspot);

                        bmp = Icon.FromHandle(hicon);
                        //Win32Stuff.DestroyIcon(hicon);
                        return bmp;
                    }
                }
            }

            return null;
        }

        public static Bitmap CaptureScreenWithCursor(IntPtr screenPtr)
        {
            int cursorX = 0;
            int cursorY = 0;
            Bitmap desktopBMP;
            Icon cursorBMP;
            Bitmap finalBMP;
            Graphics g;
            Rectangle r;

            desktopBMP = CaptureScreen(screenPtr);
            cursorBMP = CaptureCursor(ref cursorX, ref cursorY);
            if (desktopBMP != null)
            {
                if (cursorBMP != null)
                {
                    r = new Rectangle(cursorX, cursorY, cursorBMP.Width, cursorBMP.Height);
                    g = Graphics.FromImage(desktopBMP);
                    g.DrawIcon(cursorBMP, r);
                    g.Flush();

                    return desktopBMP;
                }
                else
                    return desktopBMP;
            }

            return null;

        }

        static Bitmap CaptureDesktop()
        {
            return CaptureScreen(Win32Stuff.GetDesktopWindow());
        }

        public static Bitmap CaptureDesktopWithCursor()
        {
            return CaptureScreenWithCursor(Win32Stuff.GetDesktopWindow());
        }
    }

    public class EncodeDecoderImage
    {
        public Image currentImage = null;

        public Image CurrentImage
        {
            get
            {
                return currentImage;
            }
        }

        public Image Encode(Image image)
        {
            Image resultImage = null;
            if (currentImage == null)
            {
                currentImage = image;
                resultImage = currentImage;
            }
            else
            {
                Bitmap bmpImage = (Bitmap)image;
                Bitmap bmpCurrentImage = (Bitmap)currentImage;
                for (int y = 0; y < image.Height; y++)
                {
                    for (int x = 0; x < image.Height; x++)
                    {
                        Color c1 = bmpImage.GetPixel(x, y);
                        Color c2 = bmpCurrentImage.GetPixel(x, y);
                        if (c1 == c2)
                            bmpImage.SetPixel(x, y, Color.Empty);
                    }
                }
                resultImage = bmpImage;
            }
            return resultImage;
        }

        public Image Decode(Image image)
        {
            Image resultImage = null;
            if (currentImage == null)
            {
                currentImage = image;
            }
            else
            {
                Bitmap bmpImage = (Bitmap)image;
                Bitmap bmpCurrentImage = (Bitmap)currentImage;
                for (int y = 0; y < image.Height; y++)
                {
                    for (int x = 0; x < image.Height; x++)
                    {
                        Color c = bmpImage.GetPixel(x, y);
                        if (c != Color.Empty)
                            bmpCurrentImage.SetPixel(x, y, c);
                    }
                }
                currentImage = bmpCurrentImage;
                resultImage = currentImage;
            }
            return resultImage;
        }
    }



    #region "Native Structures"

    [StructLayout(LayoutKind.Sequential)]
    public struct Rect
    {
        public Int32 Left;
        public Int32 Top;
        public Int32 Right;
        public Int32 Bottom;

        /// <summary>
        /// Converts the ICapture.Rect object to System.Drawing.Rectangle object.
        /// </summary>
        [DebuggerHidden()]
        public Rectangle ToRectangle()
        {
            return new Rectangle(this.Left, this.Top, this.Right - this.Left, this.Bottom - this.Top);
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct CursorInfo
    {
        public Int32 cbSize;
        public Int32 flags;
        public IntPtr hCursor;
        public Point ptScreenPos;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct IconInfo
    {
        public bool fIcon;
        public Int32 xHotspot;
        public Int32 yHotspot;
        public IntPtr hbmMask;
        public IntPtr hbmColor;
    }

    #endregion

    #region "NativeMethods"

    /// <summary>
    /// Represents win32 Api shared methods, structures, and constants.
    /// </summary>
    [CLSCompliant(true)]
    public sealed class NativeMethods
    {

        #region "Constants"

        //============== GDI32 CONSTANTS ===============
        public const Int32 CAPTUREBLT = 0x40000000;
        public const Int32 BLACKNESS = 0x42;
        public const Int32 DSTINVERT = 0x550009;
        public const Int32 MERGECOPY = 0xc000ca;
        public const Int32 MERGEPAINT = 0xbb0226;
        public const Int32 NOTSRCCOPY = 0x330008;
        public const Int32 NOTSRCERASE = 0x1100a6;
        public const Int32 PATCOPY = 0xf00021;
        public const Int32 PATINVERT = 0x5a0049;
        public const Int32 PATPAINT = 0xfb0a09;
        public const Int32 SRCAND = 0x8800c6;
        public const Int32 SRCCOPY = 0xcc0020;
        public const Int32 SRCERASE = 0x440328;
        public const Int32 SRCINVERT = 0x660046;
        public const Int32 SRCPAINT = 0xee0086;
        public const Int32 WHITENESS = 0xff0062;

        public const Int32 HORZRES = 8;
        public const Int32 VERTRES = 10;
        //===========================================
        /// <summary>
        /// Represents the cursor showing constant. Used with CURSORINFO structure.
        /// </summary>
        public const int CURSOR_SHOWING = 0x1;

        #endregion

        [DebuggerHidden()]
        private NativeMethods()
        {
        }

        [DllImport("user32", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool GetWindowRect(IntPtr hWnd, ref Rect lpRect);

        [DllImport("user32", SetLastError = true)]
        public static extern Int32 ReleaseDC(IntPtr hWnd, IntPtr hdc);

        [DllImport("user32", SetLastError = true)]
        public static extern IntPtr WindowFromPoint(Point point);

        [DllImport("user32", SetLastError = true)]
        public static extern IntPtr GetForegroundWindow();

        [DllImport("user32", SetLastError = true)]
        public static extern IntPtr GetDC(IntPtr hwnd);

        [DllImport("user32", SetLastError = true)]
        public static extern IntPtr CopyIcon(IntPtr hIcon);

        [DllImport("user32", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool GetCursorInfo(ref CursorInfo pci);

        [DllImport("user32", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool GetIconInfo(IntPtr hIcon, ref IconInfo piconinfo);

        [DllImport("gdi32", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool BitBlt(IntPtr hdcDest, Int32 nXDest, Int32 nYDest, Int32 nWidth, Int32 nHeight, IntPtr hdcSrc, Int32 nXSrc, Int32 nYSrc, Int32 dwRop);

        [DllImport("user32", SetLastError = true)]
        public static extern IntPtr SetCapture(IntPtr hWnd);

        [DllImport("user32", SetLastError = true)]
        public static extern IntPtr GetCapture();

        [DllImport("user32", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool ClientToScreen(IntPtr hWnd, ref Point lpPoint);

        //Overloads
        [DllImport("user32", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool InvalidateRect(IntPtr hWnd, IntPtr lpRect, int bErase);
        //Overloads
        [DllImport("user32", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool InvalidateRect(IntPtr hWnd, ref Rect lpRect, int bErase);

        [DllImport("user32", SetLastError = true)]
        public static extern int GetSystemMetrics(int nIndex);

        [DllImport("user32", SetLastError = true)]
        public static extern IntPtr GetWindowDC(IntPtr hWnd);

        [DllImport("gdi32", SetLastError = true)]
        public static extern IntPtr CreatePen(int nPenStyle, int nWidth, int crColor);

        [DllImport("gdi32", SetLastError = true)]
        public static extern int SetROP2(IntPtr hdc, int nDrawMode);

        [DllImport("gdi32", SetLastError = true)]
        public static extern IntPtr SelectObject(IntPtr hdc, IntPtr hObject);

        [DllImport("gdi32", SetLastError = true)]
        public static extern IntPtr GetStockObject(int fnObjectIndex);

        [DllImport("gdi32", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool Rectangle(IntPtr hdc, int X1, int Y1, int X2, int Y2);

        [DllImport("gdi32", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool DeleteObject(IntPtr hObject);


    }

    #endregion


    /// <summary>
    /// Provides shared methods for capturing images from the screen.
    /// </summary>
    public sealed class SCapture
    {
        //Private Const Color As Integer = 0

        #region "Methods"

        #region "Public Methods"

        [DebuggerHidden()]
        private SCapture()
        {
        }

        /// <summary>
        /// Captures the image of the entire screen (all monitors).
        /// Returns a bitmap if the function succeeded,
        /// otherwise returns nothing/exception.
        /// </summary>
        /// <param name="includeCursor">A value that specifies if the
        /// cursor should be included in the captured image.</param>
        [DebuggerHidden()]
        public static Image FullScreen(bool includeCursor)
        {
            return SCapture.ScreenRectangle(SystemInformation.VirtualScreen, includeCursor);
        }

        /// <summary>
        /// Captures the image of the specified display device.
        /// Returns a bitmap if the function succeeded,
        /// otherwise returns nothing/exception.
        /// </summary>
        /// <param name="monitor">A diplay device.</param>
        /// <param name="includeCursor">A value that specifies if the
        /// cursor should be included in the captured image.</param>
        [DebuggerHidden()]
        public static Image DisplayMonitor(System.Windows.Forms.Screen monitor, bool includeCursor)
        {
            if (monitor != null)
            {
                return SCapture.ScreenRectangle(monitor.Bounds, includeCursor);
            }
            else
            {
                throw new SCaptureException("The monitor cannot be Null (Nothing in VB).");
            }
        }

        /// <summary>
        /// Captures the image of an active window from the screen. Returns
        /// a bitmap if the function succeeded, otherwise returns nothing/exception.
        /// </summary>
        /// <param name="includeCursor">A value that specifies if the
        /// cursor should be included in the captured image.</param>
        [DebuggerHidden()]
        public static Image ActiveWindow(bool includeCursor)
        {
            IntPtr hwnd = default(IntPtr);
            Rect wRect = default(Rect);
            //Get the handle of the selected (active) window.
            hwnd = NativeMethods.GetForegroundWindow();
            //Get the window rectangle.
            if (hwnd != IntPtr.Zero)
            {
                if (!NativeMethods.GetWindowRect(hwnd, ref wRect))
                {
                    int eCode = Marshal.GetLastWin32Error();
                    throw new SCaptureException(new Win32Exception(eCode).Message);
                }
                else
                {
                    return SCapture.ScreenRectangle(wRect.ToRectangle(), includeCursor);
                }
            }
            else
            {
                throw new SCaptureException("Could not find any active window.");
            }
        }

        /// <summary>
        /// Captures the image of a contol or a window from the screen specified by a handle.
        /// Returns a bitmap if the function succeeded, otherwise returns nothing/exception.
        /// </summary>
        /// <param name="hwnd">The handle to the contol or a window whose
        /// image should be captured.</param>
        /// <param name="includeCursor">A value that specifies if the
        /// cursor should be included in the captured image.</param>
        [DebuggerHidden()]
        public static Image Window(IntPtr hwnd, bool includeCursor)
        {
            // A window is also a Control!!! So There is no neeed to double the Code !
            // get Image from Control = Window (vice versa .......)
            return Control(hwnd, includeCursor);
        }


        /// <summary>
        /// Captures an image of a contol or a window.
        /// Returns a bitmap if the function succeeded,
        /// otherwise returns nothing/exception.
        /// </summary>
        /// <param name="p">A point within the contol or a window in the screen coordinates.</param>
        /// <param name="includeCursor">A value that specifies if the
        /// cursor should be included in the captured image.</param>

        [DebuggerHidden()]
        public static Image Window(Point p, bool includeCursor)
        {
            // A window is also a Control!!! So There is no neeed to double the Code !
            // get Image from Control = Window (vice versa .......)
            return Control(p, includeCursor);
        }

        /// <summary>
        /// Captures an image of a contol or a window.
        /// Returns a bitmap if the function succeeded,
        /// otherwise returns nothing/exception.
        /// </summary>
        /// <param name="p">A point within the contol or a window in the screen coordinates.</param>
        /// <param name="includeCursor">A value that specifies if the
        /// cursor should be included in the captured image.</param>
        [DebuggerHidden()]
        public static Image Control(Point p, bool includeCursor)
        {
            IntPtr hwnd = default(IntPtr);
            Rect wRect = default(Rect);
            //Get the handle of a window from a point.
            hwnd = NativeMethods.WindowFromPoint(p);
            //Get the window area.
            if (hwnd != IntPtr.Zero)
            {
                if (!NativeMethods.GetWindowRect(hwnd, ref wRect))
                {
                    int eCode = Marshal.GetLastWin32Error();
                    throw new SCaptureException(new Win32Exception(eCode).Message);
                }
                else
                {
                    return SCapture.ScreenRectangle(wRect.ToRectangle(), includeCursor);
                }
            }
            else
            {
                //Throw an exception
                throw new SCaptureException("Could not find any control or window at the specified point.");
            }
        }

        /// <summary>
        /// Captures an image of a contol or a window.
        /// Returns a bitmap if the function succeeded,
        /// otherwise returns nothing/exception.
        /// </summary>
        /// <param name="hwnd">The handle of the contol or a window.</param>
        /// <param name="includeCursor">A value that specifies if the
        /// cursor should be included in the captured image.</param>
        [DebuggerHidden()]
        public static Image Control(IntPtr hwnd, bool includeCursor)
        {
            Rect wRect = default(Rect);
            //Get the window area.
            if (hwnd != IntPtr.Zero)
            {
                if (!NativeMethods.GetWindowRect(hwnd, ref wRect))
                {
                    int eCode = Marshal.GetLastWin32Error();
                    throw new SCaptureException(new Win32Exception(eCode).Message);
                }
                else
                {
                    return SCapture.ScreenRectangle(wRect.ToRectangle(), includeCursor);
                }
            }
            else
            {
                //Throw an exception
                throw new SCaptureException("Invalid control handle.");
            }
        }

        /// <summary>
        /// Captures a rectangle image from the screen.
        /// Returns a bitmap if the function succeeded,
        /// otherwise returns nothing/exception.
        /// </summary>
        /// <param name="rect">A rectangle within the screen
        /// coordinates whose image should be captured.</param>
        /// <param name="includeCursor">A value that specifies if the
        /// cursor should be included in the captured image.</param>
        [DebuggerHidden()]
        public static Image ScreenRectangle(Rectangle rect, bool includeCursor)
        {
            Image img = SCapture.GetImage(rect);
            //Draw cursor
            if (includeCursor)
            {
                SCapture.DrawCursor(rect, img);
            }
            //The Exception of an Empty Rectangle is trown in the GetImage(ByVal rect As Rectangle) Function !
            return img;
        }


        /// <summary>
        /// Captures a rectangle image of the entire screen (all monitors) with the ClippingTool.
        /// Returns a bitmap if the function succeeded,
        /// otherwise returns nothing/exception.
        /// </summary>
        /// <param name="includeCursor">A value that specifies if the
        /// cursor should be included in the captured image.</param>
        /// <param name="RuberbandStyle">A FrameStyle value that specifies
        /// the boarder Style of the rubber-band rectangle.</param>
        /// <param name="RubberBandColor">A Integer color value = system.Drawing.color.toArgb that specifies the
        /// the boarder color of the rubber-band rectangle.</param>
        [DebuggerHidden()]
        public static Image FromClipperTool(bool includeCursor, FrameStyle RuberbandStyle, int RubberBandColor)
        {

            // Clipping from FullScreen (VirtualScreen)
            SCapture.FrmImageClipper Clipper = new SCapture.FrmImageClipper();
            Clipper.RuberbandStyle = RuberbandStyle;
            Clipper.RubberBandColor = RubberBandColor;
            Clipper.ShowDialog();
            return ScreenRectangle(Clipper.ReturnRectangle, includeCursor);
        }


        /// <summary>
        /// Captures a rectangle image of the specified display device with the ClippingTool.
        /// Returns a bitmap if the function succeeded,
        /// otherwise returns nothing/exception.
        /// </summary>
        /// <param name="monitor">A diplay device.</param>
        /// <param name="includeCursor">A value that specifies if the
        /// cursor should be included in the captured image.</param>
        /// <param name="RuberbandStyle">A FrameStyle value that specifies
        /// the boarder Style of the rubber-band rectangle.</param>
        /// <param name="RubberBandColor">A Integer color value = system.Drawing.color.toArgb that specifies the
        /// the boarder color of the rubber-band rectangle.</param>
        [DebuggerHidden()]
        public static Image FromClipperTool(System.Windows.Forms.Screen monitor, bool includeCursor, FrameStyle RuberbandStyle, int RubberBandColor)
        {

            // Clipping from given Screen
            SCapture.FrmImageClipper Clipper = new SCapture.FrmImageClipper();
            Clipper.RuberbandStyle = RuberbandStyle;
            Clipper.RubberBandColor = RubberBandColor;
            Clipper.Location = monitor.WorkingArea.Location;
            Clipper.Size = monitor.Bounds.Size;
            //workingArea.Size is without Taskbar !
            Clipper.ClientSize = monitor.Bounds.Size;
            //workingArea.Size is without Taskbar !
            Clipper.BackgroundImage = SCapture.DisplayMonitor(monitor, includeCursor);
            Clipper.ShowDialog();
            return ScreenRectangle(Clipper.ReturnRectangle, includeCursor);
        }



        #endregion

        #region "Private Methods"

        /// <summary>
        /// Gets the image of a screen rectangle area.
        /// </summary>
        /// <param name="rect">A rectangle within the screen
        /// coordinates whose image should be captured.</param>
        [DebuggerHidden()]
        private static Image GetImage(Rectangle rect)
        {
            if (!rect.IsEmpty && rect.Width != 0 && rect.Height != 0)
            {
                IntPtr wHdc = SCapture.GetDC();
                //Create graphics object from bitmap.
                Graphics g = default(Graphics);
                Bitmap img = new Bitmap(rect.Width, rect.Height);
                g = Graphics.FromImage(img);
                //Get handle device context from the graphics object.
                IntPtr gHdc = g.GetHdc();
                //Copy the screen to the bitmap.
                if (!NativeMethods.BitBlt(gHdc, 0, 0, rect.Width, rect.Height, wHdc, rect.X, rect.Y, NativeMethods.SRCCOPY | NativeMethods.CAPTUREBLT))
                {
                    int eCode = Marshal.GetLastWin32Error();
                    g.ReleaseHdc(gHdc);
                    g.Dispose();
                    throw new SCaptureException(new Win32Exception(eCode).Message);
                }
                //Release handles to device contexts.
                g.ReleaseHdc(gHdc);
                NativeMethods.ReleaseDC(IntPtr.Zero, wHdc);
                g.Dispose();
                return img;
            }
            else
            {
                //The rectangle is empty, throw an exception.
                throw new SCaptureException("Empty rectangle.");
            }
        }

        /// <summary>
        /// Get the handle device context for the entire screen.
        /// </summary>
        [DebuggerHidden()]
        private static IntPtr GetDC()
        {
            IntPtr wHdc = NativeMethods.GetDC(IntPtr.Zero);
            if (wHdc == IntPtr.Zero)
            {
                int eCode = Marshal.GetLastWin32Error();
                throw new SCaptureException(new Win32Exception(eCode).Message);
            }
            else
            {
                return wHdc;
            }
        }

        /// <summary>
        /// Draws the cursor onto the specified image.
        /// </summary>
        /// <param name="rect">A rectangle within the screen
        /// coordinates whose image is be captured.</param>
        /// <param name="img">An image on which the cursor should be drawn.</param>
        [DebuggerHidden()]
        private static void DrawCursor(Rectangle rect, Image img)
        {
            CursorInfo cInfo = SCapture.GetCursorInfo();
            if (cInfo.flags == NativeMethods.CURSOR_SHOWING && rect.Contains(cInfo.ptScreenPos))
            {
                IntPtr hicon = SCapture.GetCursorIcon(cInfo.hCursor);
                IconInfo iInfo = SCapture.GetIconInfo(hicon);
                Graphics g = Graphics.FromImage(img);
                g.DrawIcon(Icon.FromHandle(hicon), cInfo.ptScreenPos.X - rect.X - iInfo.xHotspot, cInfo.ptScreenPos.Y - rect.Y - iInfo.yHotspot);
                g.Dispose();
            }
        }

        /// <summary>
        /// Retrieves the cursor icon.
        /// </summary>
        /// <param name="hCursor">A cursor handle whose icon should be retrieved.</param>
        [DebuggerHidden()]
        private static IntPtr GetCursorIcon(IntPtr hCursor)
        {
            IntPtr hicon = NativeMethods.CopyIcon(hCursor);
            if (hicon == IntPtr.Zero)
            {
                int eCode = Marshal.GetLastWin32Error();
                throw new SCaptureException(new Win32Exception(eCode).Message);
            }
            else
            {
                return hicon;
            }
        }

        /// <summary>
        /// Retrieves the cursor information.
        /// </summary>
        [DebuggerHidden()]
        private static CursorInfo GetCursorInfo()
        {
            CursorInfo cInfo = new CursorInfo();
            cInfo.cbSize = Marshal.SizeOf(cInfo);
            if (!NativeMethods.GetCursorInfo(ref cInfo))
            {
                int eCode = Marshal.GetLastWin32Error();
                throw new SCaptureException(new Win32Exception(eCode).Message);
            }
            else
            {
                return cInfo;
            }
        }

        /// <summary>
        /// Retrieves the cursor's icon information.
        /// </summary>
        /// <param name="hicon">An icon handle whose information should be retrieved.</param>
        [DebuggerHidden()]
        private static IconInfo GetIconInfo(IntPtr hicon)
        {
            IconInfo iInfo = new IconInfo();
            if (!NativeMethods.GetIconInfo(hicon, ref iInfo))
            {
                int eCode = Marshal.GetLastWin32Error();
                throw new SCaptureException(new Win32Exception(eCode).Message);
            }
            else
            {
                return iInfo;
            }
        }

        #endregion

        #endregion


        #region "Sub Classes"

        #region "ImageClipper Class"
        private class FrmImageClipper : System.Windows.Forms.Form
        {

            private bool isDrag = false;
            private Rectangle PrivateRectangle = new Rectangle(0, 0, 0, 0);
            private Point startPoint;
            //Public ReturnBitmap As Bitmap
            private Rectangle _returnRectangle = new Rectangle(0, 0, 0, 0);
            private Color _rubberBandColor = System.Drawing.Color.FromArgb(0);
            private FrameStyle _ruberbandStyle = FrameStyle.Dashed;
            private Rectangle _screenRect2Work = SystemInformation.VirtualScreen;

            #region "Propertys"

            public Rectangle ReturnRectangle
            {
                get { return _returnRectangle; }
            }

            public FrameStyle RuberbandStyle
            {
                get { return _ruberbandStyle; }
                set { _ruberbandStyle = value; }
            }

            public int RubberBandColor
            {
                // Color as Integer (fromArgb) because its an Optional Parameter
                // Optional parameters cannot have structure types like Color !
                get { return _rubberBandColor.ToArgb(); }
                set { _rubberBandColor = System.Drawing.Color.FromArgb(value); }
            }

            #endregion

            public FrmImageClipper()
            {


                //' Initializisize ClipperForm

                // if FormStartPosition.Manual is not set, then you can�t set the Form Location !!!
                // see msdn documentation
                this.StartPosition = FormStartPosition.Manual;

                // Set the Form to VirtualScreen (FullScreen)
                this.Location = SystemInformation.VirtualScreen.Location;
                this.Size = SystemInformation.VirtualScreen.Size;
                this.ClientSize = SystemInformation.VirtualScreen.Size;

                //maximize The Window to FullScreen without Borders and Titelbar
                // erase WindowBorder
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                // Set Window to Topmost
                this.TopMost = true;

                // Set Window to Max
                //Me.WindowState = FormWindowState.Maximized

                // set KeyPreview = True to enable keyboard reaktions such: Keys.ESC to cancel Aktions
                this.KeyPreview = true;

                // set the Form Cursor
                this.Cursor = System.Windows.Forms.Cursors.Cross;

                // disable the boxes is not nessesary but i�m a bit paranoic ;-)
                this.ControlBox = false;
                this.MaximizeBox = false;
                this.MinimizeBox = false;
                // no Icon neeeded
                this.ShowIcon = false;
                //no Name needed ....??
                this.Name = "";

                // here comes the Trick ....!!
                // Set the Background Image of the Form to the Fullscreen Snap !

                this.BackgroundImage = SCapture.FullScreen(true);
            }

            #region "Mouse events (Rubber-Band)"

            private void Form1_MouseDown(System.Object sender, System.Windows.Forms.MouseEventArgs e)
            {
                // Call this in the Sub in the MouseDown event of a Form
                // Set the isDrag variable to true and get the starting point
                // by using the PointToScreen method to convert form coordinates to
                // screen coordinates.
                if (e.Button == System.Windows.Forms.MouseButtons.Left)
                {
                    isDrag = true;

                    // Calculate the startPoint by using the PointToScreen
                    // method.
                    // Prevend that the Startpoint is outside the Form
                    if (e.X < this.Bounds.X | e.X > (this.Bounds.X + this.Width))
                    {
                        startPoint.X = this.PointToScreen(new Point(this.Bounds.X + this.Width, this.Bounds.Y + this.Height)).X;
                    }
                    else
                    {
                        startPoint.X = this.PointToScreen(new Point(e.X, e.Y)).X;
                    }

                    if (e.Y < this.Bounds.Y | e.Y > (this.Bounds.Y + this.Height))
                    {
                        startPoint.Y = this.PointToScreen(new Point(this.Bounds.X + this.Width, this.Bounds.Y + this.Height)).Y;
                    }
                    else
                    {
                        startPoint.Y = this.PointToScreen(new Point(e.X, e.Y)).Y;

                    }

                }
            }

            private void Form1_MouseMove(System.Object sender, System.Windows.Forms.MouseEventArgs e)
            {
                // Call this in the Sub in the MouseMove event of a Form

                // If the mouse is being dragged, undraw and redraw the rectangle
                // as the mouse moves.
                if ((isDrag))
                {

                    // Hide the previous rectangle by calling the DrawReversibleFrame
                    // method with the same parameters.
                    ControlPaint.DrawReversibleFrame(PrivateRectangle, this.BackColor, FrameStyle.Dashed);

                    // Calculate the endpoint and dimensions for the new rectangle,
                    // again using the PointToScreen method.
                    Point endPoint = default(Point);

                    // Calculate the EndPoint by using the PointToScreen
                    // method.And prevend that the endPoint is outside the Form
                    if (e.X < this.Bounds.X | e.X > (this.Bounds.X + this.Width))
                    {
                        endPoint.X = this.PointToScreen(new Point(this.Bounds.X + this.Width, this.Bounds.Y + this.Height)).X;
                    }
                    else
                    {
                        endPoint.X = this.PointToScreen(new Point(e.X, e.Y)).X;
                    }

                    if (e.Y < this.Bounds.Y | e.Y > (this.Bounds.Y + this.Height))
                    {
                        endPoint.Y = this.PointToScreen(new Point(this.Bounds.X + this.Width, this.Bounds.Y + this.Height)).Y;
                    }
                    else
                    {
                        endPoint.Y = this.PointToScreen(new Point(e.X, e.Y)).Y;
                    }
                    //If the Cursor is out, set the Cursor in :-) (to Formborder)
                    if (e.X < this.Bounds.X | e.X > (this.Bounds.X + this.Width) | e.Y < this.Bounds.Y | e.Y > (this.Bounds.Y + this.Height))
                    {
                        System.Windows.Forms.Cursor.Position = endPoint;
                    }


                    int width = endPoint.X - startPoint.X;
                    int height = endPoint.Y - startPoint.Y;
                    PrivateRectangle = new Rectangle(startPoint.X, startPoint.Y, width, height);

                    // Draw the new rectangle by calling DrawReversibleFrame again.
                    ControlPaint.DrawReversibleFrame(PrivateRectangle, this.BackColor, FrameStyle.Dashed);
                }
            }

            private void Form1_MouseUp(System.Object sender, System.Windows.Forms.MouseEventArgs e)
            {

                // cancel if right mousebutton is pressed (Up)
                // see in KeyPress event there is also a Cancel if the Keys.Escape is Pressed
                if (e.Button == System.Windows.Forms.MouseButtons.Right)
                {
                    this.Close();
                }

                // If the MouseUp event occurs, the user is not dragging.

                if ((isDrag))
                {
                    isDrag = false;
                    // Draw the rectangle to be evaluated. Set a dashed frame style
                    // using the FrameStyle enumeration.
                    ControlPaint.DrawReversibleFrame(PrivateRectangle, this.BackColor, FrameStyle.Dashed);

                    // Set Rectangle Startpoint to UpperLeft
                    if (PrivateRectangle.Width < 0)
                    {
                        PrivateRectangle.Width = PrivateRectangle.Width * -1;
                        // negativ to positiv
                        // shift the X to left
                        PrivateRectangle.X = PrivateRectangle.X - PrivateRectangle.Width;
                    }
                    if (PrivateRectangle.Height < 0)
                    {
                        PrivateRectangle.Height = PrivateRectangle.Height * -1;
                        // negativ to positiv
                        // shift the Y to top
                        PrivateRectangle.Y = PrivateRectangle.Y - PrivateRectangle.Size.Height;
                    }
                    // ##############################################################################
                    // Return rectangle values
                    // ##############################################################################
                    _returnRectangle = PrivateRectangle;
                    // Reset the rectangle.
                    PrivateRectangle = new Rectangle(0, 0, 0, 0);
                }
                //Me.BackgroundImage = TrayForm.SnappedImage
                this.Close();
            }

            #endregion

            #region "Keybord events"

            private void FrmImageClipper_KeyPress(System.Object sender, System.Windows.Forms.KeyPressEventArgs e)
            {

                // Handle the ESC Key to interupt the Snapping
                // cancel if Keys.Escape is Pressed
                // see in MouseUp event there is also a Cancel if the right mousebutton is pressed (Up)
                if (e.KeyChar == (char)Keys.Escape)
                {
                    this.Close();
                }
            }

            #endregion

        }
        #endregion


        #endregion


    }

    #region "SCaptureException"

    [Serializable()]
    public class SCaptureException : Exception
    {

        [DebuggerHidden()]
        public SCaptureException()
        {
        }

        [DebuggerHidden()]
        public SCaptureException(string message)
            : base(message)
        {
        }

        [DebuggerHidden()]
        public SCaptureException(string message, Exception ex)
            : base(message, ex)
        {
        }

        [DebuggerHidden()]
        protected SCaptureException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
            : base(info, context)
        {
        }

    }

    #endregion

    #region "WindowFinderEvent Class"

    public class WindowFinderEvent
    {
        //Inherits Control
        private bool Dragging = false;
        public bool HaveResult = false;
        private Cursor _WFCursor = Cursors.Hand;
        private Control WFControl;
        private IntPtr _mlngHwndCaptured;
        // Holds the handle to the captured window
        private Rectangle _mlngHwndRect;
        // Holds the Rectangle to the captured window


        #region "Propertys"

        public Cursor WFCursor
        {
            get { return _WFCursor; }
            set { _WFCursor = value; }
        }

        public Rectangle WFRectangle
        {
            get { return _mlngHwndRect; }
        }

        public IntPtr WFHandle
        {
            get { return _mlngHwndCaptured; }
        }

        #endregion

        public WindowFinderEvent(Control WinFindControl)
        {
            WFControl = WinFindControl;
            MapControlEvents(WFControl);
            _WFCursor = Cursors.Hand;
        }

        public void dispose()
        {
            UnmapControlEvents(WFControl);
        }


        // Main-Code taken From http://bytes.com/forum/thread389394.html
        // Author : Zorpiedoman
        // Class to Overwrite all of the user-type Events of A Control
        // and re-raise the events manually, so you can do additional event
        // handling for the Control.
        // Just call MapControlEvents(myControl) to remap the events.
        // and UnmapControlEvents(myControl) to release the Control.


        //The Code inside the MouseDown,MouseMove, Mouse Up and Sub InvertTracker are taken from :
        // How To Use SetCapture and WindowFromPoint to Select a Window
        //http://support.microsoft.com/?scid=kb%3Ben-us%3B143045&x=6&y=13




        public event WindowFinderResultEventHandler WindowFinderResult;
        public delegate void WindowFinderResultEventHandler(WindowFinderEvent WF);



        #region "Handlers"
        public event EventHandler Enter;
        public event EventHandler Leave;
        public event EventHandler GotFocus;
        public event EventHandler LostFocus;

        public event EventHandler MouseEnter;
        public event EventHandler MouseHover;
        public event MouseEventHandler MouseDown;
        public event EventHandler Click;
        public event MouseEventHandler MouseUp;
        public event EventHandler DoubleClick;
        public event MouseEventHandler MouseWheel;
        public event MouseEventHandler MouseMove;
        public event EventHandler MouseLeave;

        public event KeyEventHandler KeyDown;
        public event KeyPressEventHandler KeyPress;
        public event KeyEventHandler KeyUp;

        public event DragEventHandler DragDrop;
        public event DragEventHandler DragEnter;
        public event EventHandler DragLeave;
        public event DragEventHandler DragOver;
        public event GiveFeedbackEventHandler GiveFeedback;

        public event System.ComponentModel.CancelEventHandler Validating;
        public event EventHandler Validated;


        private void MapControlEvents(Control C)
        {
            C.Enter += RaiseEnter;
            C.Leave += RaiseLeave;
            C.GotFocus += RaiseGotFocus;
            C.LostFocus += RaiseLostFocus;

            C.MouseEnter += RaiseMouseEnter;
            C.MouseHover += RaiseMouseHover;
            C.MouseDown += RaiseMouseDown;
            C.Click += RaiseClick;
            C.MouseUp += RaiseMouseUp;
            C.DoubleClick += RaiseDoubleClick;
            C.MouseWheel += RaiseMouseWheel;
            C.MouseMove += RaiseMouseMove;
            C.MouseLeave += RaiseMouseLeave;

            C.KeyDown += RaiseKeyDown;
            C.KeyPress += RaiseKeyPress;
            C.KeyUp += RaiseKeyUp;

            C.DragDrop += RaiseDragDrop;
            C.DragEnter += RaiseDragEnter;
            C.DragLeave += RaiseDragLeave;
            C.DragOver += RaiseDragOver;
            C.GiveFeedback += RaiseGiveFeedback;

            C.Validating += RaiseValidating;
            C.Validated += RaiseValidated;
        }



        public void UnmapControlEvents(Control C)
        {

            C.Enter -= RaiseEnter;
            C.Leave -= RaiseLeave;
            C.GotFocus -= RaiseGotFocus;
            C.LostFocus -= RaiseLostFocus;

            C.MouseEnter -= RaiseMouseEnter;
            C.MouseHover -= RaiseMouseHover;
            C.MouseDown -= RaiseMouseDown;
            C.Click -= RaiseClick;
            C.MouseUp -= RaiseMouseUp;
            C.DoubleClick -= RaiseDoubleClick;
            C.MouseWheel -= RaiseMouseWheel;
            C.MouseMove -= RaiseMouseMove;
            C.MouseLeave -= RaiseMouseLeave;

            C.KeyDown -= RaiseKeyDown;
            C.KeyPress -= RaiseKeyPress;
            C.KeyUp -= RaiseKeyUp;

            C.DragDrop -= RaiseDragDrop;
            C.DragEnter -= RaiseDragEnter;
            C.DragLeave -= RaiseDragLeave;
            C.DragOver -= RaiseDragOver;
            C.GiveFeedback -= RaiseGiveFeedback;

            C.Validating -= RaiseValidating;

            C.Validated -= RaiseValidated;
        }

        private void RaiseEnter(object sender, EventArgs e)
        {
            if (Enter != null)
            {
                Enter(this, EventArgs.Empty);
                // Add your Handling Code here !
            }
        }
        private void RaiseLeave(object sender, EventArgs e)
        {
            if (Leave != null)
            {
                Leave(this, EventArgs.Empty);
                // Add your Handling Code here !
            }
        }
        private void RaiseGotFocus(object sender, EventArgs e)
        {
            if (GotFocus != null)
            {
                GotFocus(this, EventArgs.Empty);
                // Add your Handling Code here !
            }
        }
        private void RaiseLostFocus(object sender, EventArgs e)
        {
            if (LostFocus != null)
            {
                LostFocus(this, EventArgs.Empty);
                // Add your Handling Code here !
            }
        }

        private void RaiseMouseEnter(object sender, EventArgs e)
        {
            if (MouseEnter != null)
            {
                MouseEnter(this, EventArgs.Empty);
                // Add your Handling Code here !
            }
        }
        private void RaiseMouseHover(object sender, EventArgs e)
        {
            if (MouseHover != null)
            {
                MouseHover(this, EventArgs.Empty);
                // Add your Handling Code here !
            }
        }
        private void RaiseMouseDown(object sender, MouseEventArgs e)
        {
            Point p = WFControl.PointToClient(System.Windows.Forms.Control.MousePosition);
            MouseEventArgs m = new MouseEventArgs(e.Button, e.Clicks, p.X, p.Y, e.Delta);
            if (MouseDown != null)
            {
                MouseDown(this, m);
            }
            // Add your Handling Code here !

            //**************************************************************************

            // Purpose: Turns on SetCapture and changes the mouse pointer when the user
            // clicks down on the form.
            //**************************************************************************

            if (NativeMethods.SetCapture(WFControl.Handle) != null)
                WFControl.Cursor = _WFCursor;

            Dragging = true;
        }
        private void RaiseClick(object sender, EventArgs e)
        {
            if (Click != null)
            {
                Click(this, EventArgs.Empty);
                // Add your Handling Code here !
            }
        }
        private void RaiseMouseUp(object sender, MouseEventArgs e)
        {
            Point p = WFControl.PointToClient(System.Windows.Forms.Control.MousePosition);
            MouseEventArgs m = new MouseEventArgs(e.Button, e.Clicks, p.X, p.Y, e.Delta);
            if (MouseUp != null)
            {
                MouseUp(this, m);
            }
            // Add your Handling Code here !

            //**************************************************************************

            // Purpose: Display the Handle of the Control that is selected

            //**************************************************************************

            if (_mlngHwndCaptured != null)
            {
                Rect rc = default(Rect);

                //----------------message--------------------------------------------------

                // Refresh the entire screen in case we forgot to erase a rectangle.
                //------------------------------------------------------------------

                NativeMethods.InvalidateRect((IntPtr)0, (IntPtr)0, 1);
                //------------------------------------------------------------------

                // Clear our module-level variable and restore the mouse pointer.
                //------------------------------------------------------------------
                // rescue the Window Rectangle
                NativeMethods.GetWindowRect(_mlngHwndCaptured, ref rc);
                _mlngHwndRect = rc.ToRectangle();
                WFControl.Cursor = Cursors.Default;
                Dragging = false;
                HaveResult = true;
                if (WindowFinderResult != null)
                {
                    WindowFinderResult(this);
                }

            }
        }
        private void RaiseDoubleClick(object sender, EventArgs e)
        {
            if (DoubleClick != null)
            {
                DoubleClick(this, EventArgs.Empty);
                // Add your Handling Code here !
            }
        }
        private void RaiseMouseWheel(object sender, MouseEventArgs e)
        {
            Point p = WFControl.PointToClient(System.Windows.Forms.Control.MousePosition);
            MouseEventArgs m = new MouseEventArgs(e.Button, e.Clicks, p.X, p.Y, e.Delta);
            if (MouseWheel != null)
            {
                MouseWheel(this, m);
                // Add your Handling Code here !
            }
        }
        private void RaiseMouseMove(object sender, MouseEventArgs e)
        {
            Point p = WFControl.PointToClient(System.Windows.Forms.Control.MousePosition);
            MouseEventArgs m = new MouseEventArgs(e.Button, e.Clicks, p.X, p.Y, e.Delta);
            if (MouseMove != null)
            {
                MouseMove(this, m);
            }
            // Add your Handling Code here !


            if (Dragging == true)
            {
                //**************************************************************************

                // Purpose: Draws a rectangle around the window currently under the mouse
                // pointer while the primary mouse key is being held down.
                //**************************************************************************

                Point pt = default(Point);
                // Holds the location of the window.
                // The handle of the last window we drew a
                // a rectangle on.
                //----------------------------------------------------------------------

                // If in capture mode, then draw a rectangle around the active window.
                //----------------------------------------------------------------------

                if (NativeMethods.GetCapture() != null)
                {
                    //------------------------------------------------------------------

                    // Convert the current mouse position to Screen coordinates.
                    //------------------------------------------------------------------

                    pt.X = e.X;
                    pt.Y = e.Y;
                    NativeMethods.ClientToScreen(WFControl.Handle, ref pt);
                    //------------------------------------------------------------------

                    // Pass that value to WindowFromPoint to find out what window we are
                    // pointing to.
                    //------------------------------------------------------------------

                    _mlngHwndCaptured = NativeMethods.WindowFromPoint(pt);
                    //------------------------------------------------------------------

                    // If its not the last window, then erase the previous rectangle
                    // and draw a rectangle around the window under the mouse pointer.
                    //------------------------------------------------------------------

                    if (static_RaiseMouseMove_hWndLast != _mlngHwndCaptured)
                    {
                        if (static_RaiseMouseMove_hWndLast != null)
                            InvertTracker(static_RaiseMouseMove_hWndLast);
                        InvertTracker(_mlngHwndCaptured);
                        static_RaiseMouseMove_hWndLast = _mlngHwndCaptured;
                    }
                }

            }
        }
        static IntPtr static_RaiseMouseMove_hWndLast = default(IntPtr);
        private void RaiseMouseLeave(object sender, EventArgs e)
        {
            if (MouseLeave != null)
            {
                MouseLeave(this, EventArgs.Empty);
                // Add your Handling Code here !
            }
        }

        private void RaiseKeyDown(object sender, KeyEventArgs e)
        {
            if (KeyDown != null)
            {
                KeyDown(this, e);
                // Add your Handling Code here !
            }
        }
        private void RaiseKeyPress(object sender, KeyPressEventArgs e)
        {
            if (KeyPress != null)
            {
                KeyPress(this, e);
                // Add your Handling Code here !
            }
        }

        private void RaiseKeyUp(object sender, KeyEventArgs e)
        {
            if (KeyUp != null)
            {
                KeyUp(this, e);
                // Add your Handling Code here !
            }
        }

        private void RaiseDragDrop(object sender, DragEventArgs e)
        {
            Point p = WFControl.PointToClient(System.Windows.Forms.Control.MousePosition);
            DragEventArgs d = new DragEventArgs(e.Data, e.KeyState, p.X, p.Y, e.AllowedEffect, e.Effect);
            if (DragDrop != null)
            {
                DragDrop(this, d);
                // Add your Handling Code here !
            }
        }
        private void RaiseDragEnter(object sender, DragEventArgs e)
        {
            Point p = WFControl.PointToClient(System.Windows.Forms.Control.MousePosition);
            DragEventArgs d = new DragEventArgs(e.Data, e.KeyState, p.X, p.Y, e.AllowedEffect, e.Effect);
            if (DragEnter != null)
            {
                DragEnter(this, d);
                // Add your Handling Code here !
            }
        }
        private void RaiseDragLeave(object sender, EventArgs e)
        {
            if (DragLeave != null)
            {
                DragLeave(this, EventArgs.Empty);
                // Add your Handling Code here !
            }
        }
        private void RaiseDragOver(object sender, DragEventArgs e)
        {
            Point p = WFControl.PointToClient(System.Windows.Forms.Control.MousePosition);
            DragEventArgs d = new DragEventArgs(e.Data, e.KeyState, p.X, p.Y, e.AllowedEffect, e.Effect);
            if (DragOver != null)
            {
                DragOver(this, d);
                // Add your Handling Code here !
            }
        }
        private void RaiseGiveFeedback(object sender, GiveFeedbackEventArgs e)
        {
            if (GiveFeedback != null)
            {
                GiveFeedback(this, e);
                // Add your Handling Code here !
            }
        }
        private void RaiseValidating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (Validating != null)
            {
                Validating(this, e);
                // Add your Handling Code here !
            }
        }
        private void RaiseValidated(object sender, EventArgs e)
        {
            if (Validated != null)
            {
                Validated(this, e);
                // Add your Handling Code here !
            }
        }


        private void InvertTracker(IntPtr hwndDest)
        {

            //**************************************************************************

            // Purpose: Draws a inverted rectangle around a window on the screen.
            // Inputs: A handle to a enabled and visible window.

            //**************************************************************************

            IntPtr hdcDest = default(IntPtr);
            IntPtr hPen = default(IntPtr);
            IntPtr hOldPen = default(IntPtr);
            IntPtr hOldBrush = default(IntPtr);
            int cxBorder = 0;
            int cxFrame = 0;
            int cyFrame = 0;
            int cxScreen = 0;
            int cyScreen = 0;
            Rect rc = default(Rect);
            int cr = 0;

            const int NULL_BRUSH = 5;
            const int R2_NOT = 6;
            const int PS_INSIDEFRAME = 6;
            //----------------------------------------------------------------------


            // Get the screen, border, and frame sizes.
            //----------------------------------------------------------------------

            cxScreen = NativeMethods.GetSystemMetrics(0);
            cyScreen = NativeMethods.GetSystemMetrics(1);
            cxBorder = NativeMethods.GetSystemMetrics(5);
            cxFrame = NativeMethods.GetSystemMetrics(32);
            cyFrame = NativeMethods.GetSystemMetrics(33);
            //----------------------------------------------------------------------

            // Get the coordinates of the window on the screen.
            //----------------------------------------------------------------------


            NativeMethods.GetWindowRect(hwndDest, ref rc);
            //----------------------------------------------------------------------

            // Get a handle to the window's device context.
            //----------------------------------------------------------------------

            hdcDest = NativeMethods.GetWindowDC(hwndDest);
            //----------------------------------------------------------------------


            // Create an inverse pen that is the size of a window border.
            //----------------------------------------------------------------------

            NativeMethods.SetROP2(hdcDest, R2_NOT);
            cr = Color.FromArgb(0, 0, 0).ToArgb();
            hPen = NativeMethods.CreatePen(PS_INSIDEFRAME, 3 * cxBorder, cr);
            //----------------------------------------------------------------------


            // Draw the rectangle around the window.
            //----------------------------------------------------------------------


            hOldPen = NativeMethods.SelectObject(hdcDest, hPen);
            hOldBrush = NativeMethods.SelectObject(hdcDest, NativeMethods.GetStockObject(NULL_BRUSH));
            NativeMethods.Rectangle(hdcDest, 0, 0, rc.Right - rc.Left, rc.Bottom - rc.Top);
            NativeMethods.SelectObject(hdcDest, hOldBrush);
            NativeMethods.SelectObject(hdcDest, hOldPen);
            //----------------------------------------------------------------------


            // Give the window its device context back, and destroy our pen.
            //----------------------------------------------------------------------


            NativeMethods.ReleaseDC(hwndDest, hdcDest);
            NativeMethods.DeleteObject(hPen);
        }

        #endregion

    }

    #endregion

}
