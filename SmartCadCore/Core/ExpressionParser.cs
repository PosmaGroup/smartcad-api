using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.ComponentModel;

using SmartCadCore.Model;
using NHibernate.Criterion;

namespace SmartCadCore.Core
{
    #region Class ExpressionParser Documentation
    ///<summary>
    /// Clase que convierte el criterio de b�squeda en una expresion ICriterion
    /// de NHibernet.
    ///</summary>
    ///<className>ExpressionParser</className>
    ///<author email="jynojosa@smartmatic.com">Julio Ynojosa</author>
    ///<date>2006/04/04</date>
    ///<copyRight>Smartmatic copyright</copyRight>
    #endregion
    public class ExpressionParser
    {
        private string criteriaStr = null;
        private ObjectData objData = null;

        public string CriteriaStr
        {
            get
            {
                return criteriaStr;
            }
            set
            {
                criteriaStr = value;
            }
        }

        public ExpressionParser(ObjectData objData, string criteriaStr)
        {
            this.objData = objData;
            this.criteriaStr = criteriaStr;
        }

        public ICriterion GetOrCriterion()
        {
            Disjunction disjuntionCriteria = new Disjunction();
            Type objType = objData.GetType();
            PropertyInfo[] propInfoArray = objType.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo propInfo in propInfoArray)
            {
                try
                {
                    SearchablePropertyAttribute searchAttr = 
                        TypeDescriptor.GetProperties(objData)
                        [propInfo.Name].Attributes
                        [typeof(SearchablePropertyAttribute)] as SearchablePropertyAttribute;

                    if ((searchAttr != null) && (searchAttr.IsSearchable == true)
                        /*&& (propInfo.PropertyType.Name == typeof(string).Name
                        || propInfo.PropertyType.Name == typeof(DateTime).Name)*/)
                    {
                        ICriterion likeExpression = Expression.Like(propInfo.Name, criteriaStr, MatchMode.Anywhere);
                        if ((propInfo != null) && (propInfo.PropertyType != null))
                        {
                            disjuntionCriteria.Add(likeExpression);
                        }
                    }
                    /*else if ((searchAttr != null) && (searchAttr.IsSearchable == true)
                        && (propInfo.PropertyType.Name == typeof(int?).Name
                        || propInfo.PropertyType.Name == typeof(int).Name))
                    {
                        ICriterion eqExpression = Expression.Eq(propInfo.Name, criteriaStr);
                        if ((propInfo != null) && (propInfo.PropertyType != null))
                        {
                            disjuntionCriteria.Add(eqExpression);
                        }
                    }*/
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return disjuntionCriteria;
        }

        public ICriterion GetAndCriterion()
        {
            Conjunction conjunctionCriteria = new Conjunction();
            Type objType = objData.GetType();
            PropertyInfo[] propInfoArray = objType.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo propInfo in propInfoArray)
            {
                try
                {
                    SearchablePropertyAttribute searchAttr = TypeDescriptor.GetProperties(objData)[propInfo.Name].Attributes[typeof(SearchablePropertyAttribute)] as SearchablePropertyAttribute;

                    if ((searchAttr != null) && (searchAttr.IsSearchable == true))
                    {
                        ICriterion likeExpression = Expression.Like(propInfo.Name, criteriaStr, MatchMode.Anywhere);
                        if ((propInfo != null) && (propInfo.PropertyType != null))
                        {
                            conjunctionCriteria.Add(likeExpression);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return conjunctionCriteria;
        }

    }
}
