using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace SmartCadCore.Core
{
    public class PictureBoxProperties : GlobalizedObject
    {
        private PictureBox pictureBox = null;

        public PictureBoxProperties(Control pictureBox)
        {
            this.pictureBox = pictureBox as PictureBox;
        }

        public string Label
        {
            get 
            {

                return (string)pictureBox.Tag;
            }
            set 
            {
                int ou;
                bool good = Int32.TryParse((string)value,out ou);
                if (good)
                    pictureBox.Tag = value;
            }
        }
	
    }
}
