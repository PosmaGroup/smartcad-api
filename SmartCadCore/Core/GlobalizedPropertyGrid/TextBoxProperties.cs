using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace SmartCadCore.Core
{
    public class TextBoxProperties : GlobalizedObject
    {
        public Control control = null;

        public TextBoxProperties(Control myControl)
        {
            control = myControl;
        }

        public int Height
        {
            get { return control.Size.Height; }
            set { control.Height = value; }
        }

        public int Width
        {
            get { return control.Size.Width; }
            set { control.Width = value; }
        }

        public bool Multiline
        {
            get { return ((TextBox)control).Multiline; }
            set { ((TextBox)control).Multiline = value; }
        }
        
        public string Text
        {
            get { return control.Text; }
            set { control.Text = value; }
        }
    }
}
