using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace SmartCadCore.Core
{
    public class ControlProperties : GlobalizedObject
    {
        private Control control = null;

        public ControlProperties(Control myControl)
        {
            control = myControl;
        }
        
        public int Height
        {
            get { return control.Size.Height; }
            set { control.Height = value; }
        }

        public int Width
        {
            get { return control.Size.Width; }
            set { control.Width = value; }
        }

        public string Text
        {
            get { return control.Text; }
            set { control.Text = value; }
        }


    }
}
