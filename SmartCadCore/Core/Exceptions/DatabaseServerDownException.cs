using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using System.ServiceModel;

namespace SmartCadCore.Core
{
    [Serializable]
    public class DatabaseServerDownException : FaultException
    {
        public DatabaseServerDownException(string message)
            : base(message)
        {
        }
    }
}
