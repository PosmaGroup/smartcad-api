using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    #region Class DatabaseObjectException Documentation
    /// <summary>
    /// 
    /// </summary>
    /// <className>DatabaseObjectException</className>
    /// <author email="fsilva@smartmatic.com">Fernando Silva</author>
    /// <refactoredBy email="atorres@smartmatic.com">Alden Torres</refactoredBy>
    /// <date>2007/05/17</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    public class DatabaseObjectException : ApplicationException
    {
        private ObjectData objectData;
 
        public DatabaseObjectException(ObjectData objectData)
            : base()
        {
            this.objectData = objectData;
        }

        public DatabaseObjectException(ObjectData objectData, string message)
            : base(message)
        {
            this.objectData = objectData;
        }

        public DatabaseObjectException(ObjectData objectData, string message, Exception innerException)
            : base(message, innerException)
        {
            this.objectData = objectData;
        }

        public DatabaseObjectException(ObjectData objectData, Exception innerException)
            : this(objectData, innerException.Message, innerException)
        {
        }

        public ObjectData ObjectData
        { 
            get
            {
                return objectData;
            }
        }
    }

    public class DatabaseStaleObjectException : ApplicationException
    { 
        private ObjectData objectData;
 
        public DatabaseStaleObjectException(ObjectData objectData)
            : base()
        {
            this.objectData = objectData;
        }

        public DatabaseStaleObjectException(ObjectData objectData, string message)
            : base(message)
        {
            this.objectData = objectData;
        }

        public DatabaseStaleObjectException(ObjectData objectData, string message, Exception innerException)
            : base(message, innerException)
        {
            this.objectData = objectData;
        }

        public DatabaseStaleObjectException(ObjectData objectData, Exception innerException)
            : this(objectData, innerException.Message, innerException)
        {
        }

        public ObjectData ObjectData
        { 
            get
            {
                return objectData;
            }
        }
    }

    public class DatabaseDeletedObjectException : ApplicationException
    {
        public DatabaseDeletedObjectException()
            : base()
        {
        }

        public DatabaseDeletedObjectException(string message)
            : base(message)
        {

        }

        public DatabaseDeletedObjectException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
