using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.Core
{
    public class DatabaseServerException : ApplicationException
    {
        public DatabaseServerException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}