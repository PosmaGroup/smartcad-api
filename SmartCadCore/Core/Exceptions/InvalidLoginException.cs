using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.Core
{
    public class InvalidLoginException : ApplicationException
    {
        public InvalidLoginException(string message)
            : base(message)
        {
        }
    }
}
