using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.Core
{
    public class UniqueKeyException : ApplicationException
    {
        public UniqueKeyException()
            : base()
        {
        }

        public UniqueKeyException(string message)
            : base(message)
        {
        }

        public UniqueKeyException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}