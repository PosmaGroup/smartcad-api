using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    public class SupervisorCloseReportMessageConversion
    {
        public static SupervisorCloseReportMessageClientData ToClient(SupervisorCloseReportMessageData supervisorCloseReportMessageData, UserApplicationData app)
        {
            SupervisorCloseReportMessageClientData client = new SupervisorCloseReportMessageClientData();
            client.Code = supervisorCloseReportMessageData.Code;
            client.Message = supervisorCloseReportMessageData.Message;
            client.SupervisorCloseReportCode = supervisorCloseReportMessageData.Report.Code;
            client.Time = supervisorCloseReportMessageData.Time;
            client.Version = supervisorCloseReportMessageData.Version;
            return client;
        }

        public static SupervisorCloseReportMessageData ToObject(SupervisorCloseReportMessageClientData supervisorCloseReportMessageClientData, UserApplicationData app)
        {
            SupervisorCloseReportMessageData objectData = new SupervisorCloseReportMessageData();
            objectData.Code = supervisorCloseReportMessageClientData.Code;
            if (objectData.Code != 0)
            {
                objectData = (SupervisorCloseReportMessageData)SmartCadDatabase.RefreshObject(objectData);
                objectData.Version = supervisorCloseReportMessageClientData.Version;
            }
            else
            {
                objectData.Message = supervisorCloseReportMessageClientData.Message;
                objectData.Time = supervisorCloseReportMessageClientData.Time;
                if (supervisorCloseReportMessageClientData.SupervisorCloseReportCode != 0)
                {
                    SupervisorCloseReportData report = new SupervisorCloseReportData();
                    report.Code = supervisorCloseReportMessageClientData.SupervisorCloseReportCode;
                    objectData.Report = report;
                }
            }
            return objectData;
        }
    }
}
