﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Enums
{
    public enum UnitButtonFilter
    {
        ALL,
        AVAILABLE,
        ASSIGNED,
        ATTENTION,
        TEXT

    }
}
