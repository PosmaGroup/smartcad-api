﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.ClientData.Util
{
    [Serializable]
    public class InstalledApplicationClientData : ClientData
    {
        public InstalledApplicationClientData() 
        {
        
        }

        public string Name { get; set; }

        public bool Status { get; set; }

        public override bool Equals(object obj)
        {
            if (obj.GetType() == typeof(InstalledApplicationClientData))
            {
                InstalledApplicationClientData stc = (InstalledApplicationClientData)obj;
                if ((this.Name == stc.Name) && (this.Status == stc.Status))
                {
                    return true;
                }
            }
            return false;
        }

    }
}
