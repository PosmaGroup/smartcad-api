﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class ParkedCallClientData : ClientData
    {
        #region Properties

        public int OperatorCodeWhoParked { get; set; }
        public DateTime DateWhenParked { get; set; }
        public DateTime DateWhenUnParked { get; set; }
        public int OperatorCodeWhoUnParked { get; set; }
        public string PhoneReportCustomCode { get; set; }
        public string ANIReceived { get; set; }
                

        #endregion


        #region Overrides

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(ANIReceived).Append(" ").Append(PhoneReportCustomCode);
            return sb.ToString();
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is ParkedCallClientData)
            {
                result = this.Code == ((ParkedCallClientData)obj).Code;
            }
            return result;
        }

        #endregion
    }
}
