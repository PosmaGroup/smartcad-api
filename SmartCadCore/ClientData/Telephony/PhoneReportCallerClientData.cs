using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class PhoneReportCallerClientData : ClientData
    {
        #region Fields

        private string name;

        private string telephone;

        private string additionalNumber;

        private AddressClientData address;

        private bool anonymous;

        #endregion

        #region Properties

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }

        public string Telephone
        {
            get
            {
                return this.telephone;
            }
            set
            {
                this.telephone = value;
            }
        }

        public AddressClientData Address
        {
            get
            {
                return this.address;
            }
            set
            {
                this.address = value;
            }
        }

        public bool Anonymous
        {
            get
            {
                return this.anonymous;
            }
            set
            {
                this.anonymous = value;
            }
        }

        public string AdditionalNumber
        {
            get
            {
                return this.additionalNumber;
            }
            set
            {
                this.additionalNumber = value;
            }
        }

        #endregion
    }
}
