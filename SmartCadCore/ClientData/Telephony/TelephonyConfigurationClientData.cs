﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadCore.ClientData
{
    public class TelephonyConfigurationClientData : ClientData
    {
        #region Properties
        public int Extension { get; set; }
        public string Password { get; set; }
        public string COMPUTER { get; set; }
        public bool Virtual { get; set; }
        #endregion

        [InitialDataClient(PropertyName = "Name", PropertyValue = "TelephonyConfigurationData")]
        public static readonly UserResourceClientData Resource;
    }
}
