using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class PhoneReportLineClientData : ClientData
    {
        #region Fields

        private string name;
        private string telephone;
        private AddressClientData address;

        #endregion

        #region Constructors

        public PhoneReportLineClientData()
        {
        }

        public PhoneReportLineClientData(string name, string telephone, AddressClientData address)
        {
            this.name = name;
            this.telephone = telephone;
            this.address = address;
        }

        #endregion

        #region Properties

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public string Telephone
        {
            get
            {
                return telephone;
            }
            set
            {
                telephone = value;
            }
        }

        public AddressClientData Address
        {
            get
            {
                return address;
            }
            set
            {
                address = value;
            }
        }

        #endregion
    }
}
