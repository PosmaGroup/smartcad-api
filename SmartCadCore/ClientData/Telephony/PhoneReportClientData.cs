using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class PhoneReportClientData : ClientData, IIncidentReference
    {
        #region Fields

        private IList incidentTypesCodes;
        private DateTime registeredCallTime;
        private DateTime finishedCallTime;
        private DateTime hangedUpCallTime;
        private DateTime pickedUpCallTime;
        private DateTime receivedCallTime;
        private string xml;
        private string customCode;
        private string incidentTypesText;
        private int incidentCode;
        private PhoneReportCallerClientData phoneReportCallerClient;
        private IList answers;
        private PhoneReportLineClientData phoneReportLineClient;
        private IList reportBaseDepartmentTypesClient;
        private string operatorLogin;
        private bool multipleOrganisms;
        private IList incidentNotifications;
        private bool incomplete;
        private string incidentCustomCode;

        #endregion

        #region Properties

        /// <summary>
        /// Property used to number this call among the others associated to the inicident.
        /// </summary>
        public int OrderOfCall
        {
            get;
            set;
        }

        public DateTime ReceivedCallTime
        {
            get
            {
                return this.receivedCallTime;
            }
            set
            {
                this.receivedCallTime = value;
            }
        }

        public bool Incomplete
        {
            get
            {
                return this.incomplete;
            }
            set
            {
                this.incomplete = value;
            }
        }

        public IList ReportBaseDepartmentTypesClient
        {
            get
            {
                return this.reportBaseDepartmentTypesClient;
            }
            set
            {
                this.reportBaseDepartmentTypesClient = value;
            }
        }

        public PhoneReportLineClientData PhoneReportLineClient
        {
            get
            {
                return this.phoneReportLineClient;
            }
            set
            {
                this.phoneReportLineClient = value;
            }
        }

        public string IncidentTypesText
        {
            get
            {
                return this.incidentTypesText;
            }
            set
            {
                this.incidentTypesText = value;
            }
        }

        public IList Answers
        {
            get
            {
                return this.answers;
            }
            set
            {
                this.answers = value;
            }
        }

        public DateTime FinishedCallTime
        {
            get
            {
                return this.finishedCallTime;
            }
            set
            {
                this.finishedCallTime = value;
            }
        }

        public DateTime HangedUpCallTime
        {
            get
            {
                return this.hangedUpCallTime;
            }
            set
            {
                this.hangedUpCallTime = value;
            }
        }

        public DateTime PickedUpCallTime
        {
            get
            {
                return this.pickedUpCallTime;
            }
            set
            {
                this.pickedUpCallTime = value;
            }
        }

        public PhoneReportCallerClientData PhoneReportCallerClient
        {
            get
            {
                return this.phoneReportCallerClient;
            }
            set
            {
                this.phoneReportCallerClient = value;
            }
        }

        #region IIncidentReference Members

        public int IncidentCode
        {
            get
            {
                return this.incidentCode;
            }
            set
            {
                this.incidentCode = value;
            }
        }

        #endregion        

        public IList IncidentTypesCodes
        {
            get
            {
                return incidentTypesCodes;
            }
            set
            {
                incidentTypesCodes = value;
            }
        }

        public DateTime RegisteredCallTime
        {
            get
            {
                return registeredCallTime;
            }
            set
            {
                registeredCallTime = value;
            }
        }

        public string Xml
        {
            get
            {
                return xml;
            }
            set
            {
                xml = value;
            }
        }

        public string CustomCode
        {
            get
            {
                return customCode;
            }
            set
            {
                customCode = value;
            }
        }

        public string OperatorLogin
        {
            get
            {
                return operatorLogin;
            }
            set
            {
                operatorLogin = value;
            }
        }

        public bool MultipleOrganisms
        {
            get
            {
                return multipleOrganisms;
            }
            set
            {
                multipleOrganisms = value;
            }
        }

        public IList IncidentNotifications
        {
            get
            {
                return incidentNotifications;
            }
            set
            {
                incidentNotifications = value;
            }
        }

        public string IncidentCustomCode
        {
            get
            {
                return this.incidentCustomCode;
            }
            set
            {
                this.incidentCustomCode = value;
            }
        }
        #endregion
        
    }
}
