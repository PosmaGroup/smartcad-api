using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class OperatorEvaluationQuestionAnswerClientData : ClientData
    {
        private string questionName;
        private int operatorEvaluationCode;
        private int answer;



        public string QuestionName
        {
            get { return questionName; }
            set { questionName = value; }
        }

        public int Answer
        {
            get { return answer; }
            set { answer = value; }
        }

        public int OperatorEvaluationCode
        {
            get { return operatorEvaluationCode; }
            set { operatorEvaluationCode = value; }
        }

    }
}
