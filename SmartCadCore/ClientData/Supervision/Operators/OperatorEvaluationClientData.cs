using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;


namespace SmartCadCore.ClientData
{

    public enum ClientEvaluationScale 
    {
        OneToThree,
        OneToFive,
        YesNo
    }

    [Serializable]
    public class OperatorEvaluationClientData : ClientData
    {
        private string evaluationName;
        private int qualification;
        private DateTime date;
        private string evaluatorFirstName;
        private string evaluatorLastName;
        private int evaluatorCode;
        private OperatorClientData ope;
        private IList questions;
        private ClientEvaluationScale scale;
        private string role;
        private string category;



        public string Category
        {
            get { return category; }
            set { category = value; }
        }
	

        public string RoleOperator
        {
            get { return role; }
            set { role = value; }
        }
	

        public ClientEvaluationScale Scale
        {
            get { return scale; }
            set { scale = value; }
        }
	
	
        public IList Questions 
        {
            get { return questions; }
            set { questions = value; }
        }

        public string EvaluationName
        {
            get { return evaluationName; }
            set { evaluationName = value; }
        }

        public int Qualification
        {
            get { return qualification; }
            set { qualification = value; }
        }

        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public string EvaluatorFirstName
        {
            get { return evaluatorFirstName; }
            set { evaluatorFirstName = value; }
        }

        public string EvaluatorLastName
        {
            get { return evaluatorLastName; }
            set { evaluatorLastName = value; }
        }

        public int EvaluatorCode
        {
            get { return evaluatorCode; }
            set { evaluatorCode = value; }
        }

        public OperatorClientData Operator
        {
            get { return ope; }
            set { ope = value; }
        }


    }
}
