﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class OperatorCameraClientData : ClientData
    {
        #region Fields
        private OperatorClientData user;
        private CameraClientData camera;
        private DateTime? start;
        private DateTime? end; 
        #endregion

        #region Properties

        public OperatorClientData User
        {
            get
            {
                return user;
            }
            set
            {
                user = value;
            }
        }

        public CameraClientData Camera
        {
            get
            {
                return camera;
            }
            set
            {
                camera = value;
            }
        }

        public DateTime? Start
        {
            get
            {
                return start;
            }
            set
            {
                start = value;
            }
        }

        public DateTime? End
        {
            get
            {
                return end;
            }
            set
            {
                end = value;
            }
        } 
        #endregion

        #region Overrides

        public override string ToString()
        {
            return this.Camera.Name;
        }

        public override bool Equals(object obj)
        {
            try
            {
                if (obj.GetType() == typeof(OperatorCameraClientData))
                {
                    OperatorCameraClientData operCam = (OperatorCameraClientData)obj;

                    if ((this.User == null || operCam.User != null && operCam.User.Code == this.User.Code) &&
                        (this.Camera == null || operCam.Camera.Code == this.Camera.Code) &&
                        (this.Start == null || operCam.Start == this.Start) &&
                        (this.End == null || operCam.End == this.End))
                    {
                        return true;
                    }
                }
            }
            catch { }
            return false;
        }

        #endregion
    }
}
