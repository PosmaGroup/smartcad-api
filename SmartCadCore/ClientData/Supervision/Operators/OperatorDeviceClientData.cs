﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class OperatorDeviceClientData : ClientData
    {
        #region Fields
        private OperatorClientData user;
        private DeviceClientData device;
        private DateTime? start;
        private DateTime? end; 
        #endregion

        #region Properties

        public OperatorClientData User
        {
            get
            {
                return user;
            }
            set
            {
                user = value;
            }
        }

        public DeviceClientData Device
        {
            get
            {
                return device;
            }
            set
            {
                device = value;
            }
        }

        public DateTime? Start
        {
            get
            {
                return start;
            }
            set
            {
                start = value;
            }
        }

        public DateTime? End
        {
            get
            {
                return end;
            }
            set
            {
                end = value;
            }
        } 
        #endregion

        #region Overrides

        public override string ToString()
        {
            return this.Device.Name;
        }

        public override bool Equals(object obj)
        {
            try
            {
                if (obj.GetType() == typeof(OperatorDeviceClientData))
                {
                    OperatorDeviceClientData operCam = (OperatorDeviceClientData)obj;

                    if ((this.User == null || operCam.User != null && operCam.User.Code == this.User.Code) &&
                        (this.Device == null || operCam.Device.Code == this.Device.Code) &&
                        (this.Start == null || operCam.Start == this.Start) &&
                        (this.End == null || operCam.End == this.End))
                    {
                        return true;
                    }
                }
            }
            catch { }
            return false;
        }

        #endregion
    }
}
