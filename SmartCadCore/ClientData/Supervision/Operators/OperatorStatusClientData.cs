using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class OperatorStatusClientData : ClientData
    {
        #region Fields

        private string name;
        private string friendlyName;
        private string customCode;
        private bool? notReady;
        private byte[] image;
        private bool? immutable;
        private int? order;
        private Color color;
        private int percentage;
        private int tolerance;
        #endregion

        #region Constructors

        public OperatorStatusClientData()
        {
        }

        public OperatorStatusClientData(string name, string customCode)
        {
            this.name = name;
            this.customCode = customCode;
        }

        #endregion

        #region Properties
        
        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public virtual string FriendlyName
        {
            get
            {
                return friendlyName;
            }
            set
            {
                friendlyName = value;
            }
        }

        public virtual byte[] Image
        {
            get
            {
                return image;
            }
            set
            {
                image = value;
            }
        }
        
        public virtual string CustomCode
        {
            get
            {
                return customCode;
            }
            set
            {
                customCode = value;
            }
        }

        public bool? NotReady
        {
            get
            {
                return notReady;
            }
            set
            {
                notReady = value;
            }
        }
       
        public bool? Immutable
        {
            get
            {
                return immutable;
            }
            set
            {
                immutable = value;
            }
        }

        public virtual int? Order
        {
            get
            {
                return order;
            }
            set
            {
                order = value;
            }
        }

        public Color Color
        {
            get
            {
                return color;
            }
            set
            {
                color = value;
            }
        }

        public int Percentage
        {
            get
            {
                return percentage;
            }
            set
            {
                percentage = value;
            }
        }

        public int Tolerance
        {
            get
            {
                return tolerance;
            }
            set
            {
                tolerance = value;
            }
        }

        #endregion

        public bool IsPause()
        {
            return Percentage > 0 && Tolerance >= 0;
        }

        public override string ToString()
        {
            string text = "";

            if (this.Name != null)
            {
                text = this.Name;
            }

            return text;
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is OperatorStatusClientData)
            {
                OperatorStatusClientData data = (OperatorStatusClientData)obj;
                if (Name == data.Name)
                    result = true;
            }
            return result;
        }

        #region InitialData

        [InitialDataClient(PropertyName = "CustomCode", PropertyValue = "READY")]
        public static OperatorStatusClientData Ready;

        [InitialDataClient(PropertyName = "CustomCode", PropertyValue = "BUSY")]
        public static OperatorStatusClientData Busy;

        [InitialDataClient(PropertyName = "CustomCode", PropertyValue = "BATHROOM")]
        public static OperatorStatusClientData Bathroom;

        [InitialDataClient(PropertyName = "CustomCode", PropertyValue = "TRAINING")]
        public static OperatorStatusClientData Training;

        [InitialDataClient(PropertyName = "CustomCode", PropertyValue = "REST")]
        public static OperatorStatusClientData Rest;

        [InitialDataClient(PropertyName = "CustomCode", PropertyValue = "REUNION")]
        public static OperatorStatusClientData Reunion;

        [InitialDataClient(PropertyName = "CustomCode", PropertyValue = "ABSENT")]
        public static OperatorStatusClientData Absent;

        [InitialDataClient(PropertyName = "Name", PropertyValue = "OperatorStatusData")]
        public static readonly UserResourceClientData Resource;

        #endregion
    }
}
