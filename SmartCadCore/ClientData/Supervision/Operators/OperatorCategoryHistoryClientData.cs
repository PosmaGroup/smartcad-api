using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class OperatorCategoryHistoryClientData : ClientData
    {
        #region Fields



        private string categoryFriendlyName;

        private DateTime? startDate;

        private DateTime? endDate;

        private int codeOperator;
        private int categoryCode;
        
        #endregion

        #region Properties

        public int OperatorCode
        {
            get { return codeOperator; }
            set { codeOperator = value; }
        }

        public int CategoryCode
        {
            get { return categoryCode; }
            set { categoryCode = value; }
        }
	

        /// <summary>
        /// Start date of login
        /// </summary>
        public DateTime? StartDate
        {
            get
            {
                return startDate;
            }
            set
            {
                startDate = value;
            }
        }

        /// <summary>
        /// End date of login
        /// </summary>
        public DateTime? EndDate
        {
            get
            {
                return endDate;
            }
            set
            {
                endDate = value;
            }
        }

        public string CategoryFriendlyName
        {
            get
            {
                return categoryFriendlyName;
            }
            set
            {
                categoryFriendlyName = value;
            }
        }

        #endregion

        public override string ToString()
        {
            return categoryFriendlyName;
        }
    }
}
