﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class OperatorLightClientData : ClientData
    {
        private bool delete;
        private bool dispatchAccess;
        private bool firstLevelAccess;
        private int roleCode;
        private bool isSupervisor;
        private bool isLogged;
        private string currentWorkShiftName;

        public bool IsLogged
        {
            get
            {
                return isLogged;
            }
            set
            {
                isLogged = value;
            }
        }

        public bool IsSupervisor
        {
            get
            {
                return isSupervisor;
            }
            set
            {
                isSupervisor = value;
            }
        }

        public int RoleCode
        {
            get
            {
                return roleCode;
            }
            set
            {
                roleCode = value;
            }
        }

        public bool FirstLevelAccess
        {
            get
            {
                return firstLevelAccess;
            }
            set
            {
                firstLevelAccess = value;
            }
        }

        public bool DispatchAccess
        {
            get
            {
                return dispatchAccess;
            }
            set
            {
                dispatchAccess = value;
            }
        }

        public string CurrentWorkShiftName
        {
            get
            {
                return currentWorkShiftName;
            }
            set
            {
                currentWorkShiftName = value;
            }
        } 

        public bool Delete
        {
            get
            {
                return delete;
            }
            set
            {
                delete = value;
            }
        }


    }    
}
