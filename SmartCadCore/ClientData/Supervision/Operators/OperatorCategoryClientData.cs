using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class OperatorCategoryClientData : ClientData
    {
        private string name;
        private string friendlyName;
        private string description;
        private int? level;
       
        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public string FriendlyName
        {
            get { return friendlyName; }
            set { friendlyName = value; }
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public int? Level
        {
            get { return level; }
            set { level = value; }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is OperatorCategoryClientData)
            {
                result = this.Code == ((OperatorCategoryClientData)obj).Code;
            }
            return result;
        }

        public override string ToString()
        {
            return this.FriendlyName;
        }

        #region InitialData

        [InitialDataClient(PropertyName = "Name", PropertyValue = "OnTraining")]
        public static OperatorCategoryClientData OnTraining;

        #endregion

    }
}
