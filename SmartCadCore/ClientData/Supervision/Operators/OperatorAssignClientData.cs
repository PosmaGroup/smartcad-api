using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class OperatorAssignClientData : ClientData
    {
        private int supervisedOperatorWorkShiftCode;
        private int supervisedOperatorCode;
        private int supervisorWorkShiftCode;
        private int supervisorCode;
        private DateTime startDate;
        private DateTime endDate;
        private string operFirstName;
        private string supFirstName;
        private string operLastName;
        private string supLastName;
        private WorkShiftScheduleVariationClientData supervisedScheduleVariation;
        private WorkShiftScheduleVariationClientData supervisorScheduleVariation;
   
        public OperatorAssignClientData()
        {
        }


        public int SupervisedOperatorWorkShiftCode
        {
            get
            {
                return supervisedOperatorWorkShiftCode;
            }
            set
            {
                supervisedOperatorWorkShiftCode = value;
            }
        }

        public int SupervisedOperatorCode
        {
            get
            {
                return supervisedOperatorCode;
            }
            set
            {
                supervisedOperatorCode = value;
            }
        }

        public int SupervisorWorkShiftCode
        {
            get
            {
                return supervisorWorkShiftCode;
            }
            set
            {
                supervisorWorkShiftCode = value;
            }
        }

        public int SupervisorCode
        {
            get
            {
                return supervisorCode;
            }
            set
            {
                supervisorCode = value;
            }
        }


        public DateTime StartDate
        {
            get
            {
                return startDate;
            }
            set
            {
                startDate = value;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return endDate;
            }
            set
            {
                endDate = value;
            }
        }

        public string OperLastName
        {
            get
            {
                return operLastName;
            }
            set
            {
                operLastName = value;
            }
        }

        public string SupLastName
        {
            get
            {
                return supLastName;
            }
            set
            {
                supLastName = value;
            }
        }

        public string OperFirstName
        {
            get
            {
                return operFirstName;
            }
            set
            {
                operFirstName = value;
            }
        }

        public string SupFirstName
        {
            get
            {
                return supFirstName;
            }
            set
            {
                supFirstName = value;
            }
        }

        public WorkShiftScheduleVariationClientData SupervisedScheduleVariation
        {
            get
            {
                return supervisedScheduleVariation;
            }
            set
            {
                supervisedScheduleVariation = value;
            }
        }

        public WorkShiftScheduleVariationClientData SupervisorScheduleVariation
        {
            get
            {
                return supervisorScheduleVariation;
            }
            set
            {
                supervisorScheduleVariation = value;
            }
        }

        public override bool Equals(Object obj)
        {
            if ((obj is OperatorAssignClientData) == false)
                return false;

            OperatorAssignClientData oad = ((OperatorAssignClientData)obj);
            if (this.Code != 0 && this.Code == oad.Code ||                
                (this.SupervisedScheduleVariation != null &&
                this.SupervisedScheduleVariation.Code == oad.SupervisedScheduleVariation.Code &&
                this.EndDate == oad.EndDate &&
                this.StartDate == oad.StartDate &&
                this.SupervisedOperatorCode == oad.SupervisedOperatorCode &&
                this.SupervisorCode == oad.SupervisorCode &&
                (this.SupervisorScheduleVariation == null || this.SupervisorScheduleVariation.Code == oad.SupervisorScheduleVariation.Code)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override string ToString()
        {
            return this.SupervisedOperatorCode + "/" + startDate.ToString("HH:mm") + "/" + endDate.ToString("HH:mm");
        }

        public OperatorAssignClientData Clone()
        {
            OperatorAssignClientData newOpAss = new OperatorAssignClientData();
            newOpAss.Code = this.Code;
            newOpAss.EndDate = this.EndDate;
            newOpAss.OperFirstName = this.OperFirstName;
            newOpAss.OperLastName = this.OperLastName;
            newOpAss.SupFirstName = this.SupFirstName;
            newOpAss.SupLastName = this.SupLastName;
            newOpAss.StartDate = this.StartDate;
            newOpAss.SupervisedOperatorCode = this.SupervisedOperatorCode;
            newOpAss.SupervisedScheduleVariation = this.SupervisedScheduleVariation;
            newOpAss.SupervisorCode = this.SupervisorCode;
            newOpAss.Version = this.Version;

            return newOpAss;
        }


    }
}
