using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class OperatorTrainingCourseClientData: ClientData
    {
        private int operatorCode;
        private int courseScheduleCode;
        private string courseName;
        private DateTime endDate;
        private DateTime startDate;
        private string operatorFirstName;
        private string operatorLastName;
        private string operatorCategory;
        private string operatorWorkShift;
        private string trainer;

        private int courseCode;

        public int CourseCode
        {
            get { return courseCode; }
            set { courseCode = value; }
        }

        public DateTime StarDate
        {
            get { return startDate; }
            set { startDate = value; }
        }

        public DateTime EndDate
        {
            get { return endDate; }
            set { endDate = value; }
        }
	

        public int OperatorCode
        {
            get { return operatorCode; }
            set { operatorCode = value; }
        }

        public int CourseScheduleCode
        {
            get { return courseScheduleCode; }
            set { courseScheduleCode = value; }
        }

        public string CourseName
        {
            get { return courseName; }
            set { courseName = value; }
        }

        public string OperatorFirstName
        {
            get { return operatorFirstName; }
            set { operatorFirstName = value; }
        }

        public string OperatorLastName
        {
            get { return operatorLastName; }
            set { operatorLastName = value; }
        }

        public string OperatorCategory
        {
            get { return operatorCategory; }
            set { operatorCategory = value; }
        }

        public string OperatorWorkShift
        {
            get { return operatorWorkShift; }
            set { operatorWorkShift = value; }
        }

        public string CourseScheduleTrainer
        {
            get { return trainer; }
            set { trainer = value; }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            OperatorTrainingCourseClientData client = obj as OperatorTrainingCourseClientData;

            if (client != null)
            {
                if (this.courseScheduleCode == client.courseScheduleCode && this.operatorCode == client.operatorCode)
                    result = true;
            }
            return result;
        }
    }
}
