using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class OperatorObservationClientData: ClientData
    {
        private string  type;
        private string supervisor;
        private string observation;
        private DateTime date;
        private int codeSupervisor;
        private int codeOpetator;
        private string  typeFriendlyName;

        public string  FriendlyNameObservationType
        {
            get { return typeFriendlyName; }
            set { typeFriendlyName = value; }
        }	

        public int OperatorCode
        {
            get { return codeOpetator; }
            set { codeOpetator = value; }
        }
	

        public int SupervisorCode
        {
            get { return codeSupervisor; }
            set { codeSupervisor = value; }
        }
	

        public string  NameObservationType
        {
            get { return type; }
            set { type = value; }
        }

        public string Observation
        {
            get { return observation; }
            set { observation = value; }
        }

        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public string SupervisorName
        {
            get { return supervisor; }
            set { supervisor = value; }
        }


    }
}
