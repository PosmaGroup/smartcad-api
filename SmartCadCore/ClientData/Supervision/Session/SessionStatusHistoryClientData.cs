using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class SessionStatusHistoryClientData : ClientData
    {
        #region Fields

        private string statusName;
        private string statusFriendlyName;
        private DateTime startDateStatus;
        private DateTime endDateStatus;
        private bool statusNotReady;

        private int sessionHistoryCode;

        #endregion

        #region Properties

        public string StatusName
        {
            get { return statusName; }
            set { statusName = value; }
        } 

        public string StatusFriendlyName
        {
            get { return statusFriendlyName; }
            set { statusFriendlyName = value; }
        }

        public bool StatusNotReady
        {
            get { return statusNotReady; }
            set { statusNotReady = value; }
        }

        public DateTime StartDateStatus
        {
            get { return startDateStatus; }
            set { startDateStatus = value; }
        }

        public int SessionHistoryCode
        {
            get { return sessionHistoryCode; }
            set { sessionHistoryCode = value; }
        }

        public DateTime EndDateStatus
        {
            get { return endDateStatus; }
            set { endDateStatus = value; }
        }

        #endregion
    }
}
