using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class TrainingCourseScheduleClientData: ClientData, ICloneable
    {

        private string name;
        private string trainer;
        private DateTime start;
        private DateTime end;
        private IList parts;
        private int trainingCourseCode;
        private string trainingCourseName;
        private TrainingCourseClientData trainingCourse;
        private IList operators;
        private int minimumAttendance;
        private DateTime realStart;
        private DateTime realEnd;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Trainer
        {
            get { return trainer; }
            set { trainer = value; }
        }

        public DateTime Start
        {
            get { return start; }
            set { start = value; }
        }

        public DateTime End
        {
            get { return end; }
            set { end = value; }
        }

        public DateTime RealStart
        {
            get { return realStart; }
            set { realStart = value; }
        }

        public DateTime RealEnd
        {
            get { return realEnd; }
            set { realEnd = value; }
        }

        public IList Parts {
            get { return parts; }
            set { parts = value; }
        }

        public int TrainingCourseCode 
        {
            get { return trainingCourseCode; }
            set { trainingCourseCode = value; }
        }

        public string TrainingCourseName
        {
            get { return trainingCourseName; }
            set { trainingCourseName = value; }
        }

        public TrainingCourseClientData TrainingCourse
        {
            get { return trainingCourse; }
            set { trainingCourse = value; }
        }

        public IList TrainingCourseOperators 
        {
            get { return operators; }
            set { operators = value; }   
        
        }

        public int MinimumAttendance
        {
            get { return minimumAttendance; }
            set { minimumAttendance = value; }
        }

        public override string ToString()
        {
            return name;
        }

        public override bool Equals(object obj)
        {

            bool result = false;
            ClientData clientData = obj as ClientData;
            if (clientData != null)
            {
                if (this.Code == clientData.Code)
                    result = true;
            }
            return result;

        }

        public object Clone() 
        {
            TrainingCourseScheduleClientData clone = new TrainingCourseScheduleClientData();
            clone.End = this.end;
            clone.Start = this.start;
            clone.RealStart = this.RealStart;
            clone.RealEnd = this.RealEnd;
            clone.MinimumAttendance = this.minimumAttendance;
            clone.Name = this.name;
            clone.Trainer = this.trainer;
            if (this.parts != null)
            clone.Parts = new ArrayList(this.parts);
            clone.TrainingCourse = this.trainingCourse;
            clone.TrainingCourseCode = this.TrainingCourseCode;
            clone.TrainingCourseName = this.TrainingCourseName;
            if (this.TrainingCourseOperators != null)
            clone.TrainingCourseOperators = new ArrayList(this.TrainingCourseOperators);
            clone.Version = this.Version;

            return clone;
        }
    }
}
