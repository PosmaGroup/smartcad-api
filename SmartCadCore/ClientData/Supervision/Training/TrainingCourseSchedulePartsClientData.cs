using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class TrainingCourseSchedulePartsClientData: ClientData
    {
        private DateTime start;
        private DateTime end;
        private string room;
        private TrainingCourseScheduleClientData trainingCourseSchedule;

        public string Room
        {
            get { return room; }
            set { room = value; }        
        }
        
        public DateTime Start
        {
            get { return start; }
            set { start = value; }
        }

        public DateTime End
        {
            get { return end; }
            set { end = value; }
        }

        public TrainingCourseScheduleClientData TrainingCourseSchedule
        {

            get { return trainingCourseSchedule; }
            set { trainingCourseSchedule = value; }
        }

        public override bool Equals(object obj)
        {

            bool result = false;
            ClientData clientData = obj as ClientData;
            if (clientData != null)
            {
                if (this.Code == clientData.Code)
                    result = true;
            }
            return result;

        }
    }
}
