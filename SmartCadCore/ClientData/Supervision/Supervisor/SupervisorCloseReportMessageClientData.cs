using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class SupervisorCloseReportMessageClientData : ClientData
    {
        private int supervisorCloseReportCode;
        private DateTime time;
        private string message;

        public string Message
        {
            get { return message; }
            set { message = value; }
        }

        public DateTime Time
        {
            get { return time; }
            set { time = value; }
        }

        public int SupervisorCloseReportCode
        {
            get { return supervisorCloseReportCode; }
            set { supervisorCloseReportCode = value; }
        }
    }
}
