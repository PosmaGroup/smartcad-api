using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class SupervisorCloseReportClientData : ClientData
    {
        private int sessionCode;
        private string sessionFullName;
        private DateTime start;
        private DateTime end;
        private string supervisor;
        private string supervisorPersonId;
        private int supervisorType;
        private IList messages;
        private bool finished = false;
        private IList departamentTypes = new ArrayList();

        public IList Messages
        {
            get { return messages; }
            set { messages = value; }
        }

        public int SessionCode
        {
            get { return sessionCode; }
            set { sessionCode = value; }
        }

        public string SessionFullName
        {
            get { return sessionFullName; }
            set { sessionFullName = value; }
        }

        public DateTime Start
        {
            get { return start; }
            set { start = value; }
        }

        public DateTime End
        {
            get { return end; }
            set { end = value; }
        }

        public string Supervisor
        {
            get { return supervisor; }
            set { supervisor = value; }
        }

        public string SupervisorPersonId
        {
            get { return supervisorPersonId; }
            set { supervisorPersonId = value; }
        }

        public int SupervisorType
        {
            get { return supervisorType; }
            set { supervisorType = value; }
        }

        public bool Finished
        {
            get { return finished; }
            set { finished = value; }
        }

        public IList DepartamentTypes
        {
            get { return departamentTypes; }
            set { departamentTypes = value; }
        }
    }
}
