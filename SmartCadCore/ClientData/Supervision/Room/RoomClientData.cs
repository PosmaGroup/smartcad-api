using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
   public class RoomClientData: ClientData
    {

        #region Fields

        private string name;
        private string description;
        private int seats;
        private byte[] image;
        private string seatsImage;
        private string renderMethod;
        private IList listSeats;

        #endregion


        #region Properties

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }


        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }


        public int Seats
        {
            get
            {
                return seats;
            }
            set
            {
                seats = value;
            }
        }

        public byte[] Image
        {
            get
            {
                return image;
            }
            set
            {
                image = value;
            }
        }

       public string SeatsImage
       {
           get
           {
               return seatsImage;
           }
           set
           {
               seatsImage = value;
           }
       }

       public string RenderMethod
       {
           get
           {
               return renderMethod;
           }
           set
           {
               renderMethod = value;
           }
       }

       public IList ListSeats
       {
           get
           {
               return listSeats;
           }
           set
           {
               listSeats = value;
           }
       }
    
        #endregion

        #region Overrides

        public override string ToString()
        {
            return this.Name;
        }

        public override bool Equals(object obj)
        {
            if (obj is RoomClientData)
            {
                if (((RoomClientData)obj).Code == this.Code)
                    return true;
            }
            return false;
        }
        #endregion

        [InitialDataClient(PropertyName = "Name", PropertyValue = "RoomData")]
        public static readonly UserResourceClientData Resource;
    }
}
