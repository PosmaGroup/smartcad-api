using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class RoomSeatClientData : ClientData
    {

        #region Fields

        private string seatName;
        private RoomClientData room;
        private string computerName; 
        private int x;
        private int y;
        private int width;
        private int height;

        #endregion

        #region Properties

        public string SeatName
        {
            get
            {
                return seatName;
            }
            set
            {
                seatName = value;
            }
        }


        public RoomClientData Room
        {
            get
            {
                return room;
            }
            set
            {
                room = value;
            }
        }

        public string ComputerName
        {
            get
            {
                return computerName;
            }
            set
            {
                computerName = value;
            }
        }

        public int X
        {
            get
            {
                return x;
            }
            set
            {
                x = value;
            }
        }

        public int Y
        {
            get
            {
                return y;
            }
            set
            {
                y = value;
            }
        }

        public int Height
        {
            get
            {
                return height;
            }
            set
            {
                height = value;
            }
        }

        public int Width
        {
            get
            {
                return width;
            }
            set
            {
                width = value;
            }
        }

        #endregion

        #region Overrides

      
        #endregion

    }
}
