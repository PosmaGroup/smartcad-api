using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;


namespace SmartCadCore.ClientData
{
    [Serializable]
    public class WorkShiftVariationClientData: ClientData
    {   
        #region Constructors

        public WorkShiftVariationClientData()
        {
        }

        #endregion

        #region Properties

        public enum WorkShiftType
        {
            WorkShift = 0,
            ExtraTime = 1,
            TimeOff = 2
        }

        public enum ObjectRelatedType
        {
            Operators = 0,
            Units = 1
        }

		public virtual string Name { get; set;}

        public virtual string Description { get; set; }

        public virtual string MotiveName { get; set;}

        public virtual int MotiveCode { get; set; }
              
		public virtual WorkShiftType Type { get; set; }

		public ObjectRelatedType ObjectType { get; set; }

		public virtual IList Schedules { get; set; }

        public IList Operators { get; set; }

		#endregion

        #region Methods
        public object Clone()
        {
            WorkShiftVariationClientData clone = new WorkShiftVariationClientData();
            clone.Code = this.Code;
            clone.Description = this.Description;
            clone.MotiveCode = this.MotiveCode;
            clone.MotiveName = this.MotiveName;
            clone.Name = this.Name;
            clone.ObjectType = this.ObjectType;
            if (this.Operators != null)
            clone.Operators = new ArrayList(this.Operators);
            if (this.Schedules != null)
            clone.Schedules = new ArrayList(this.Schedules);
            clone.Type = this.Type;
            clone.Version = this.Version;

            return clone;
        }
        #endregion

        #region Overrides

        public override string ToString()
        {
            return this.Name;
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            WorkShiftVariationClientData objectData = obj as WorkShiftVariationClientData;
            if (objectData != null)
            {
                if (this.Code == objectData.Code)
                    result = true;
            }
            return result;
        }

        #endregion

		#region Initial Data
		[InitialDataClient(PropertyName = "Name", PropertyValue = "WorkShiftVariationData")]
		public static readonly UserResourceClientData Resource;
		#endregion
    }
}
