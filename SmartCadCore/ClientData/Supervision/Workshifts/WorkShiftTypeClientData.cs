﻿using System;
using System.Collections.Generic;
using System.Text;


namespace SmartCadCore.ClientData
{
    [Serializable]
    public class WorkShiftTypeClientData : ClientData
    {
        private string name;
        private string friendlyName;

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public string FriendlyName
        {
            get
            {
                return friendlyName;
            }
            set
            {
                friendlyName = value;
            }
        }
        
        [InitialDataClient(PropertyName = "Name", PropertyValue = "ExtraTime")]
        public static WorkShiftTypeClientData ExtraTime;

        [InitialDataClient(PropertyName = "Name", PropertyValue = "WorkShift")]
        public static WorkShiftTypeClientData WorkShift;

        [InitialDataClient(PropertyName = "Name", PropertyValue = "TimeOff")]
        public static WorkShiftTypeClientData TimeOff;

        public override string ToString()
        {
            string text = "";

            if (this.FriendlyName != null)
            {
                text = this.FriendlyName;
            }

            return text;
        }
        
    }
}
