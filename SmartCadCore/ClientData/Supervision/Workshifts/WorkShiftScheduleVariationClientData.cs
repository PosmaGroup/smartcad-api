using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData 
{
    [Serializable]
    public class WorkShiftScheduleVariationClientData : ClientData
    {
        #region Constructors

        public WorkShiftScheduleVariationClientData()
        {
        }

        #endregion

        #region Properties

            
        public virtual DateTime Start { get; set; }

        public virtual DateTime End { get; set; }

        public virtual string WorkShiftVariationName { get; set; }

        public virtual WorkShiftVariationClientData.WorkShiftType WorkShiftVariationType { get; set; }

        public virtual int WorkShiftVariationCode { get; set; }

		public virtual WorkShiftVariationClientData.ObjectRelatedType WorkShiftObjectType { get; set; }
		#endregion

		#region Overrides

		public override bool Equals(Object obj)
        {
            if ((obj is WorkShiftScheduleVariationClientData) == false)
                return false;

            WorkShiftScheduleVariationClientData sc = ((WorkShiftScheduleVariationClientData)obj);
            if (this.Code == sc.Code)
                 return true;
            else
               return false;            
        }

		public override string ToString()
		{
			return this.Start.ToString("HH:mm") + " a " + this.End.ToString("HH:mm");
		}
        #endregion
    }
}
