﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class EvaluationUserApplicationClientData : ClientData, ICloneable
    {
        private UserApplicationClientData application;
        private EvaluationClientData evaluation;
        
        #region Constructors

        public EvaluationUserApplicationClientData()
        {
        }

        #endregion

        #region Properties
        public UserApplicationClientData Application
        {
            get
            {
                return application;
            }
            set
            {
                application = value;
            }
        }

        public EvaluationClientData Evaluation
        {
            get
            {
                return evaluation;
            }
            set
            {
                evaluation = value;
            }
        }

        #endregion

        public override string ToString()
        {
            return application.FriendlyName;
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            EvaluationUserApplicationClientData client = obj as EvaluationUserApplicationClientData;
            if (client != null)
            {
                if (this.Application.Code == client.Application.Code)
                    result = true;
            }

            return result;
        }


        #region ICloneable Members

        public object Clone()
        {
            EvaluationUserApplicationClientData evaUserApp = new EvaluationUserApplicationClientData();
            evaUserApp.Evaluation = this.Evaluation;
            evaUserApp.Application = this.Application;
            return evaUserApp;
        }

        #endregion
    }
}
