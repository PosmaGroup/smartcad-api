﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class EvaluationDepartmentTypeClientData : ClientData, ICloneable
    {
        private DepartmentTypeClientData department;
        private EvaluationClientData evaluation;
        
        public virtual EvaluationClientData Evaluation
        {
            get
            {
                return evaluation;
            }
            set
            {
                evaluation = value;
            }
        }

        public virtual DepartmentTypeClientData Department
        {
            get
            {
                return department;
            }
            set
            {
                department = value;
            }
        }

        public override string ToString()
        {
            string deptName = string.Empty;
            if (this.department != null)
            {
                deptName = this.department.Name;
            }
            return deptName;
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is EvaluationDepartmentTypeClientData)
            {
                if (this.department != null && ((EvaluationDepartmentTypeClientData)obj).Department != null)
                {
                    if (this.department.Code == ((EvaluationDepartmentTypeClientData)obj).Department.Code)
                    {
                        result = true;
                    }                    
                }
                else
                {
                    result = this.Code == ((EvaluationDepartmentTypeClientData)obj).Code;
                }
            }
            return result;
        }

        #region ICloneable Members

        public object Clone()
        {
            EvaluationDepartmentTypeClientData evaDep = new EvaluationDepartmentTypeClientData();
            evaDep.Department = this.Department;
            evaDep.Evaluation = this.Evaluation;
            return evaDep;
        }

        #endregion
    }
}
