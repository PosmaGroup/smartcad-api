﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class EvaluationQuestionClientData:ClientData, ICloneable
    {
        #region Fields

        private EvaluationClientData evaluation;
        private string name;
        private float weighting; 

        #endregion

        #region Constructors

        public EvaluationQuestionClientData()
        {
        }

        public EvaluationQuestionClientData(string name)
        {
            this.name = name;
        }

        #endregion

        #region Properties
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public float Weighting
        {
            get
            {
                return weighting;
            }
            set
            {
                weighting = value;
            }
        }

        public EvaluationClientData Evaluation
        {
            get
            {
                return evaluation;
            }
            set
            {
                evaluation = value;
            }
        }

        #endregion

        #region ICloneable Members

        public object Clone()
        {
            EvaluationQuestionClientData evaQuestion = new EvaluationQuestionClientData();
            evaQuestion.Name = this.Name;
            evaQuestion.Weighting = this.Weighting;
            evaQuestion.Evaluation = this.Evaluation;
            return evaQuestion;
        }

        #endregion
    }
}
