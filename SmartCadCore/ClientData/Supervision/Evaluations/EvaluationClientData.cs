using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class EvaluationClientData : ClientData
    {
        private string name;
        private List<EvaluationUserApplicationClientData> applications;
        private List<EvaluationDepartmentTypeClientData> departments;
        private OperatorCategoryClientData operatorCategory;
        private string description;
        private List<EvaluationQuestionClientData> questions;
        private EvaluationScales scale;

        public enum EvaluationScales
        {
            OneToThree,
            OneToFive,
            YesNo
        }

        #region Constructors

        public EvaluationClientData()
        {
        }

        public EvaluationClientData(string name)
        {
            this.name = name;
        }

        #endregion

        #region Properties
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }

        public EvaluationScales Scale
        {
            get
            {
                return scale;
            }
            set
            {
                scale = value;
            }
        }

        public List<EvaluationUserApplicationClientData> Applications
        {
            get
            {
                return applications;
            }
            set
            {
                applications = value;
            }
        }

        public OperatorCategoryClientData OperatorCategory
        {
            get
            {
                return operatorCategory;
            }
            set
            {
                operatorCategory = value;
            }
        }

        public List<EvaluationDepartmentTypeClientData> Departments
        {
            get
            {
                return departments;
            }
            set
            {
                departments = value;
            }
        }

        public List<EvaluationQuestionClientData> Questions
        {
            get
            {
                return questions;
            }
            set
            {
                questions = value;
            }
        }
        #endregion

        public override string ToString()
        {
            return name;
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            EvaluationClientData evaluation = obj as EvaluationClientData;
            if (evaluation != null)
            {
                if (this.Code == evaluation.Code)
                    result = true;
            }

            return result;
        }

        #region Resource
        [InitialDataClient(PropertyName = "Name", PropertyValue = "EvaluationData")]
        public static readonly UserResourceClientData Resource;
        #endregion
    }
}
