﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class UserPermissionClientData : ClientData
    {
        #region Fields
        
        private string name;
        private UserResourceClientData userResource;        
        private UserActionClientData userAction;

	    #endregion

        #region Constructors
        
        public UserPermissionClientData()
        {
        }

        public UserPermissionClientData(string name,
            UserResourceClientData userResource, UserActionClientData userAction)
        {
            this.Name = name;
            this.UserResource = userResource;
            this.UserAction = userAction;
        }

        #endregion

        #region Properties

        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public virtual UserResourceClientData UserResource
        {
            get
            {
                return userResource;
            }
            set
            {
                userResource = value;
            }
        }

        public virtual UserActionClientData UserAction
        {
            get
            {
                return userAction;
            }
            set
            {
                userAction = value;
            }
        }

        #endregion
    }
}
