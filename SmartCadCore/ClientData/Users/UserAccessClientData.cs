﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class UserAccessClientData: ClientData
    {
        #region Fields

        private string name;

        private UserPermissionClientData userPermission;

        private UserApplicationClientData userApplication;

        #endregion

        #region Constructors

        public UserAccessClientData()
        {
        }

        public UserAccessClientData(string name, UserPermissionClientData userPermission,
            UserApplicationClientData userApplication)
        {
            this.Name = name;
            this.UserPermission = userPermission;
            this.UserApplication = userApplication;
        }

        #endregion

        #region Properties

        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public virtual UserApplicationClientData UserApplication
        {
            get
            {
                return userApplication;
            }
            set
            {
                userApplication = value;
            }
        }

        public virtual UserPermissionClientData UserPermission
        {
            get
            {
                return userPermission;
            }
            set
            {
                userPermission = value;
            }
        }

        #endregion
    }
}
