using System;
using System.Collections.Generic;
using System.Text;


namespace SmartCadCore.ClientData
{
    [Serializable]
    public class UserActionClientData : ClientData
    {
        #region Fields

        private string name;
        private string friendlyName;

        #endregion

        #region Properties

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public string FriendlyName
        {
            get
            {
                return friendlyName;
            }
            set
            {
                friendlyName = value;
            }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is UserActionClientData)
            {
                result = this.Code == ((UserActionClientData)obj).Code;
            }
            return result;
        }
        #endregion

        #region Initial Data
        [InitialDataClient(PropertyName = "Name", PropertyValue = "Basic")]
        public static readonly UserActionClientData Basic;
        [InitialDataClient(PropertyName = "Name", PropertyValue = "Search")]
        public static readonly UserActionClientData Search;
        [InitialDataClient(PropertyName = "Name", PropertyValue = "Insert")]
        public static readonly UserActionClientData Insert;
        [InitialDataClient(PropertyName = "Name", PropertyValue = "Delete")]
        public static readonly UserActionClientData Delete;
        [InitialDataClient(PropertyName = "Name", PropertyValue = "Update")]
        public static readonly UserActionClientData Update;
        [InitialDataClient(PropertyName = "Name", PropertyValue = "Duplicate")]
        public static readonly UserActionClientData Duplicate;

        #endregion
    }
}
