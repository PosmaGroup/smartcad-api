using System;
using System.Collections;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class UserRoleClientData : ClientData, ICloneable
    {
        #region Fields

        private string name;
        private string friendlyName;
        private string description;
        private bool? immutable;

        private IList profiles;

        #endregion

        #region Properties

        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public virtual string FriendlyName
        {
            get
            {
                return friendlyName;
            }
            set
            {
                friendlyName = value;
            }
        }

        public virtual string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }

        public bool? Immutable
        {
            get
            {
                return immutable;
            }
            set
            {
                immutable = value;
            }
        }

        public virtual IList Profiles
        {
            get
            {
                return profiles;
            }
            set
            {
                profiles = value;
            }
        }


        #endregion

        #region Overrides

        public override string ToString()
        {
            return this.FriendlyName;
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is UserRoleClientData)
            {
                result = this.Code == ((UserRoleClientData)obj).Code;
            }
            return result;
        }

        #endregion

        #region Initial Data

        [InitialDataClient(PropertyName = "Name", PropertyValue = "UserRoleData")]
        public static readonly UserResourceClientData Resource;

        [InitialDataClient(PropertyName = "Name", PropertyValue = "Developer")]
        public static readonly UserRoleClientData Developer;

        #endregion

        #region ICloneable Members

        public object Clone()
        {
            UserRoleClientData role = new UserRoleClientData();
            role.Code = this.Code;
            role.Version = this.Version;
            role.Description = this.Description;
            role.FriendlyName = this.FriendlyName;
            role.Immutable = this.Immutable;
            role.Name = this.Name;
            role.Profiles = new ArrayList();
            if (this.Profiles != null)
            {
                foreach (UserProfileClientData profile in this.Profiles)
                {
                    role.Profiles.Add(profile);
                }

            }
            return role;
        }

        #endregion
    }
}
