using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class UserApplicationClientData: ClientData
    {
        public enum UserApplications
        {
            Administration,
            FirstLevel,
            Dispatch,
            Map,
            Report,
            Server,
            Supervision,
            Cctv,
            Alarm,
            Lpr,
            SocialNetworks
        }

        #region Fields

        private string name;
        private string friendlyName;
        private IList<UserAccessClientData> accesses;

        #endregion

        #region Properties

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public string FriendlyName
        {
            get
            {
                return friendlyName;
            }
            set
            {
                friendlyName = value;
            }
        }

        
        public IList<UserAccessClientData> Accesses
        {
            get
            {
                return accesses;
            }
            set
            {
                accesses = value;
            }
        }
    

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is UserApplicationClientData)
            {
                result = this.Code == ((UserApplicationClientData)obj).Code;
            }
            return result;
        }
        #endregion

        #region Methods
        public static UserApplicationClientData GetUserApplicationClientData(string name)
        {
            if (FirstLevel.Name == name)
                return FirstLevel;
            else if (Dispatch.Name == name)
                return Dispatch;
            else if (Map.Name == name)
                return Map;
            else if (Administration.Name == name)
                return Administration;
            else if (Report.Name == name)
                return Report;
            else if (Server.Name == name)
                return Server;
            else if (Supervision.Name == name)
                return Supervision;
            else if (Cctv.Name == name)
                return Cctv;
            else if (Alarm.Name == name)
                return Alarm;
            else if (Lpr.Name == name)
                return Lpr;
            else if (SocialNetworks.Name == name)
                return Lpr;
            else
                return null;
        }

        public static int GetUserApplicationPortByName(string name)
        {
            if (FirstLevel.Name == name)
                return 2020;
            else if (Dispatch.Name == name)
                return 2021;
            else if (Map.Name == name)
                return 2022;
            else if (Administration.Name == name)
                return 2023;
            else if (Report.Name == name)
                return 2024;
            else if (Supervision.Name == name)
                return 2025;
            else if (Cctv.Name == name)
                return 2026;
            else if (Alarm.Name == name)
                return 2027;
            else if (Lpr.Name == name)
                return 2028;
            else if (SocialNetworks.Name == name)
                return 2029;
            else
                return 0;
        }
        #endregion

        [InitialDataClient(PropertyName = "Name", PropertyValue = "FirstLevel")]
        public static UserApplicationClientData FirstLevel;

        [InitialDataClient(PropertyName = "Name", PropertyValue = "Dispatch")]
        public static UserApplicationClientData Dispatch;

        [InitialDataClient(PropertyName = "Name", PropertyValue = "Map")]
        public static UserApplicationClientData Map;

        [InitialDataClient(PropertyName = "Name", PropertyValue = "Administration")]
        public static UserApplicationClientData Administration;

        [InitialDataClient(PropertyName = "Name", PropertyValue = "Report")]
        public static UserApplicationClientData Report;

        [InitialDataClient(PropertyName = "Name", PropertyValue = "Server")]
        public static UserApplicationClientData Server;

        [InitialDataClient(PropertyName = "Name", PropertyValue = "Supervision")]
        public static UserApplicationClientData Supervision;

        [InitialDataClient(PropertyName = "Name", PropertyValue = "Cctv")]
        public static UserApplicationClientData Cctv;

        [InitialDataClient(PropertyName = "Name", PropertyValue = "Alarm")]
        public static UserApplicationClientData Alarm;

        [InitialDataClient(PropertyName = "Name", PropertyValue = "Lpr")]
        public static UserApplicationClientData Lpr;

        [InitialDataClient(PropertyName = "Name", PropertyValue = "SocialNetworks")]
        public static UserApplicationClientData SocialNetworks;

        [InitialDataClient(PropertyName = "Name", PropertyValue = "UserApplicationData")]
        public static readonly UserResourceClientData Resource;
    }
}
