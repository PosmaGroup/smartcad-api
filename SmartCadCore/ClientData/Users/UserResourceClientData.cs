using System;
using System.Collections.Generic;
using System.Text;


namespace SmartCadCore.ClientData
{
    [Serializable]
    public class UserResourceClientData : ClientData
    {
        #region Fields

        private string name;
        private string friendlyName; 

        #endregion

        #region Properties

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public string FriendlyName
        {
            get
            {
                return friendlyName;
            }
            set
            {
                friendlyName = value;
            }
        } 

        #endregion

        #region Overrides
        public override string ToString()
        {
            return this.Name;
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is UserResourceClientData)
            {
                result = this.Code == ((UserResourceClientData)obj).Code || this.name.Equals(((UserResourceClientData)obj).name);
            }
            return result;
        }
        #endregion

        #region InitialData
		[InitialDataClient(PropertyName = "Name", PropertyValue = "UserApplicationData")]
		public static UserResourceClientData Application;
		[InitialDataClient(PropertyName = "Name", PropertyValue = "FirstLevelSupervisorName")]
		public static UserResourceClientData FirstLevelSupervisor;
		[InitialDataClient(PropertyName = "Name", PropertyValue = "DispatchSupervisorName")]
		public static UserResourceClientData DispatchSupervisor;
		[InitialDataClient(PropertyName = "Name", PropertyValue = "GeneralSupervisorName")]
		public static UserResourceClientData GeneralSupervisor;
		#endregion
    }
}
