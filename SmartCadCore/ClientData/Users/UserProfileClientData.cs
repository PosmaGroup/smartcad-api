using System;
using System.Collections.Generic;
using System.Text;


namespace SmartCadCore.ClientData
{
    [Serializable]
    public class UserProfileClientData : ClientData, ICloneable
    {
        #region Properties

        private string name;

        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        private string friendlyName;

        public virtual string FriendlyName
        {
            get
            {
                return friendlyName;
            }
            set
            {
                friendlyName = value;
            }
        }

        private string description;

        public virtual string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }

        private bool? immutable;

        public bool? Immutable
        {
            get
            {
                return immutable;
            }
            set
            {
                immutable = value;
            }
        }

        private IList<UserAccessClientData> accesses;

        public virtual IList<UserAccessClientData> Accesses
        {
            get
            {
                return accesses;
            }
            set
            {
                accesses = value;
            }
        }

        #endregion

        #region Overrides

        public override string ToString()
        {
            return this.FriendlyName;
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is UserProfileClientData)
            {
                result = this.Code == ((UserProfileClientData)obj).Code;
            }
            return result;
        }

        #endregion

        [InitialDataClient(PropertyName = "Name", PropertyValue = "UserProfileData")]
        public static readonly UserResourceClientData Resource;

        #region ICloneable Members

        public object Clone()
        {
            UserProfileClientData profile = new UserProfileClientData();
            profile.Code = this.Code;
            profile.Version = this.Version;
            profile.Description = this.Description;
            profile.FriendlyName = this.FriendlyName;
            profile.Immutable = this.Immutable;
            profile.Name = this.Name;
            profile.Accesses = new List<UserAccessClientData>();
            if (this.Accesses != null)
            {
                foreach (UserAccessClientData item in this.Accesses)
                {
                    profile.Accesses.Add(item);
                }
            }
            return profile;
        }


        #endregion
    
    }
}
