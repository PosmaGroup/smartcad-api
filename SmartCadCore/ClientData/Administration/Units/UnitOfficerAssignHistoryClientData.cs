﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace SmartCadCore.ClientData
{
    [Serializable]
    public class UnitOfficerAssignHistoryClientData: ClientData
    {
        #region Fields
        private OfficerClientData officer;
        private UnitClientData unit;
        private DateTime? start;
        private DateTime? end; 
        #endregion

        #region Properties
        public OfficerClientData Officer
        {
            get
            {
                return officer;
            }
            set
            {
                officer = value;
            }
        }

        public UnitClientData Unit
        {
            get
            {
                return unit;
            }
            set
            {
                unit = value;
            }
        }

        public DateTime? Start
        {
            get
            {
                return start;
            }
            set
            {
                start = value;
            }
        }

        public DateTime? End
        {
            get
            {
                return end;
            }
            set
            {
                end = value;
            }
        } 
        #endregion

        #region Resource
        [InitialDataClient(PropertyName = "Name", PropertyValue = "UnitOfficerAssignHistoryData")]
        public static readonly UserResourceClientData Resource; 
        #endregion
    }
}
