﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
	[Serializable]
	public class RouteClientData : ClientData
	{
		public enum RouteType
		{
			Track = 0,
			Area = 1
		}
		
		#region Properties

		public string Name { get; set; }

		public DepartmentTypeClientData Department { get; set; }

		public string CustomCode { get; set; }

		public RouteType Type { get; set; }

		public IList RouteAddress { get; set; }

		public string Image { get; set; }

		public int StopTime { get; set; }

		public int StopTimeTol { get; set; }

		
		#endregion

		#region Overrides

		public override bool Equals(object obj)
		{
			if (obj is RouteClientData == false)
				return false;
			RouteClientData route = obj as RouteClientData;
			if (route.Code == this.Code)
				return true;
			return false;
		}

		#endregion

		#region Initial Data
		[InitialDataClient(PropertyName = "Name", PropertyValue = "RouteData")]
		public static readonly UserResourceClientData Resource;
		#endregion
	}
}
