﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class RankClientData : ClientData, ICloneable
    {
        #region Fields
        private string name;
        private string description;

        private string departmentTypeName;
       

        private int departmentTypeCode;
      

        #endregion

        #region Properties
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }

        public string DepartmentTypeName
        {
            get
            {
                return departmentTypeName;
            }
            set
            {
                departmentTypeName = value;
            }
        }


        public int DepartmentTypeCode
        {
            get
            {
                return departmentTypeCode;
            }
            set
            {
                departmentTypeCode = value;
            }
        }


        #endregion

        #region Overrides
        public override bool Equals(object obj)
        {
            bool result = false;
            RankClientData rank = obj as RankClientData;
            if (rank != null)
            {
                if (this.Code == rank.Code)
                    result = true;
            }

            return result;
        }

        public override int GetHashCode()
        {
            return this.GetType().GetHashCode() * 10000000 + this.Code.GetHashCode();
        }

        public override string ToString()
        {
            return this.Name;
        }
        #endregion

        #region ICloneable Members

        public object Clone()
        {
            RankClientData rank = new RankClientData();
            rank.Code = this.Code;
            rank.Version = this.Version;
            rank.Name = this.Name;
            rank.Description = this.Description;
            rank.departmentTypeCode = this.departmentTypeCode;
            rank.DepartmentTypeName = this.DepartmentTypeName;
            return rank;
        }

        #endregion

        #region Initial Data
        [InitialDataClient(PropertyName = "Name", PropertyValue = "RankData")]
        public static readonly UserResourceClientData Resource;
        #endregion
    }
}
