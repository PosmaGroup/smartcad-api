﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class UnitAlertClientData: ClientData
    {

        public enum AlertPriority
        {
            Low = 0,
            Medium = 1,
            High = 2
        }

        #region Properties

        public int AlertCode
        {
            get;
            set;
        }

        public string AlertName
        {
            get;
            set;
        }

        public string AlertCustomCode
        {
            get;
            set;
        }

        public string AlertMeasure
        {
            get;
            set;
        }


        public int UnitCode
        {
            get;
            set;
        }

        public AlertPriority Priority
        {
            get;
            set;
        }

        public int MaxValue
        {
            get;
            set;
        }

		public string UnitCustomCode 
		{
			get;
			set;
		}

		public string UnitDepartmentTypeName 
		{ 
			get; 
			set; 
		}

		public bool Active { get; set; }
        #endregion
    }
}
