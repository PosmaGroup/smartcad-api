using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class UnitStatusClientData : ClientData
    {
        #region Fields

        private string name;
        private string friendlyName;
        private Color color;
        private byte[] image;
        private string customCode;
        private bool active;
        private bool immutable;

        #endregion

        #region Properties

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public string FriendlyName
        {
            get
            {
                return friendlyName;
            }
            set
            {
                friendlyName = value;
            }
        }

        public Color Color
        {
            get
            {
                return color;
            }
            set
            {
                color = value;
            }
        }

        public byte[] Image
        {
            get
            {
                return image;
            }
            set
            {
                image = value;
            }
        }

        public string CustomCode
        {
            get
            {
                return customCode;
            }
            set
            {
                customCode = value;
            }
        }

        public bool Active
        {
            get
            {
                return active;
            }
            set
            {
                active = value;
            }
        }

        public bool Immutable
        {
            get
            {
                return immutable;
            }
            set
            {
                immutable = value;
            }
        }

        #endregion

        public override string ToString()
        {
            string text = "";
            if (this.Name != null)
            {
                text = this.FriendlyName;
            }
            return text;
        }

        #region InitialData

        [InitialDataClient(PropertyName = "CustomCode", PropertyValue = "AVAILABLE")]
        public static UnitStatusClientData Available;

        [InitialDataClient(PropertyName = "CustomCode", PropertyValue = "OUTOFORDER")]
        public static UnitStatusClientData OutOfOrder;

        [InitialDataClient(PropertyName = "CustomCode", PropertyValue = "ASSIGNED")]
        public static UnitStatusClientData Assigned;

        [InitialDataClient(PropertyName = "CustomCode", PropertyValue = "ONSCENE")]
        public static UnitStatusClientData OnScene;

        [InitialDataClient(PropertyName = "CustomCode", PropertyValue = "DELAYED")]
        public static UnitStatusClientData Delayed;

        [InitialDataClient(PropertyName = "CustomCode", PropertyValue = "NOTAVAILABLE")]
        public static UnitStatusClientData NotAvailable;

        #endregion
    }
}
