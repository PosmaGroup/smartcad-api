using System;
using System.Collections.Generic;
using System.Text;


namespace SmartCadCore.ClientData
{
    [Serializable]
    public class UnitEndingReportTypeClientData : ClientData
    {
        #region Fields
        private string name;
        private string customCode;
        private string friendlyName;
        #endregion

        #region Properties
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public string CustomCode
        {
            get
            {
                return customCode;
            }
            set
            {
                customCode = value;
            }
        }

        public string FriendlyName
        {
            get
            {
                return friendlyName;
            }
            set
            {
                friendlyName = value;
            }
        }
        #endregion

        public override string ToString()
        {
            return this.FriendlyName;
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            UnitEndingReportTypeClientData objectData = obj as UnitEndingReportTypeClientData;
            if (objectData != null)
            {
                if (this.CustomCode.Equals(objectData.CustomCode) &&
                    this.Name.Equals(objectData.Name))
                    result = true;
            }
            return result;
        }

        #region InitialData
        [InitialDataClient(PropertyName = "CustomCode", PropertyValue = "FALSE_ALARM")]
        public static UnitEndingReportTypeClientData FalseAlarmUnitEndingReportType;
        #endregion
        
    }
}
