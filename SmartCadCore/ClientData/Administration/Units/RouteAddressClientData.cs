﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
	public class RouteAddressClientData : ClientData
	{
		#region Properties

		public string Name { get; set; }

		public double Lat { get; set; }

		public double Lon { get; set; }

		public int PointNumber { get; set; }

		public RouteClientData RouteClient { get; set; }
		
		public TimeSpan StopTime{ get; set; }

		#endregion

		#region Overrides

		public override bool Equals(object obj)
		{
			if (obj is RouteAddressClientData == false)
				return false;
			RouteAddressClientData routeAddress = obj as RouteAddressClientData;
			if (routeAddress.Lon == Lon &&
				routeAddress.Lat == Lat &&
				routeAddress.PointNumber == PointNumber)
				return true;
			return false;
		}

		#endregion
	}
}
