﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace SmartCadCore.ClientData
{
	[Serializable]
	public class UnitPositionHistoryClientData : ClientData
	{
		private double lon;
		private double lat;
		private DateTime date;

		private UnitClientData unit;

		public double Lon
		{
			get
			{
				return lon;
			}
			set
			{
				lon = value;
			}
		}

		public double Lat
		{
			get
			{
				return lat;
			}
			set
			{
				lat = value;
			}
		}

		public DateTime Date
		{
			get
			{
				return date;
			}
			set
			{
				date = value;
			}
		}

		public UnitClientData Unit
		{
			get
			{
				return unit;
			}
			set
			{
				unit = value;
			}
		}

		public override bool Equals(object obj)
		{
			if (obj is UnitPositionHistoryClientData == false)
				return false;
			UnitPositionHistoryClientData uphd = obj as UnitPositionHistoryClientData;
			if (uphd.Code != uphd.Code)
				return false;
			return true;
		}
	}
}

