﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class DepartmentTypeAlertClientData: ClientData
    {
              
        public enum AlertPriority
        {
            Low = 0,
            Medium = 1,
            High = 2
        }

        #region Properties

        public int AlertCode
        {
            get;
            set;
        }

        public string AlertName
        {
            get;
            set;
        }

        public string AlertCustomCode
        {
            get;
            set;
        }

        public string AlertMeasure
        {
            get;
            set;
        }


        public int DepartmentTypeCode
        {
            get;
            set;
        }

        public AlertPriority Priority
        {
            get;
            set;
        }

        public int MaxValue
        {
            get;
            set;
        }

        #endregion

		#region Overrides
		public override bool Equals(object obj)
		{
			if (obj is DepartmentTypeAlertClientData == false)
				return false;
			DepartmentTypeAlertClientData dtacd = obj as DepartmentTypeAlertClientData;
			if (dtacd.Code == Code)
				return true;
			return false;
		}
		#endregion
	}
}
