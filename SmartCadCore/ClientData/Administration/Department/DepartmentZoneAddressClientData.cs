﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class DepartmentZoneAddressClientData : ClientData
    {
        private double lat;
        private double lon;
        private string zone;
        private string pointNumber;
        

        public int DepartamentTypeCode 
        {
            get ;
            set ;
        }

        public int DepartamentZoneCode { get; set; }

        public string Name { get; set; }

        public double Lat
        {
            get { return lat; }
            set { lat = value; }
        }
        public double Lon
        {
            get { return lon; }
            set { lon = value; }
        }

        public string Zone
        {
            get { return zone; }
            set { zone = value; }
        }
        public string PointNumber
        {
            get { return pointNumber; }
            set { pointNumber = value; }
        }

      
        public override bool Equals(object obj)
        {
            if (obj.GetType() == typeof(DepartmentZoneAddressClientData))
            {
                DepartmentZoneAddressClientData stc = (DepartmentZoneAddressClientData)obj;
                if ((this.lon == stc.lon) && (this.lat == stc.lat))
                {
                    return true;
                }
            }
            return false;
        }
    }
}