﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class DepartmentStationAddressClientData : ClientData
    {
        private double lat;
        private double lon;
        private string station;
        private string pointNumber;

        public string StationName { get; set; }

        public int DepartmentCode { get; set; }

        public double Lat
        {
            get { return lat; }
            set { lat = value; }
        }
        public double Lon
        {
            get { return lon; }
            set { lon = value; }
        }

        public string Station
        {
            get { return station; }
            set { station = value; }
        }
        public string PointNumber
        {
            get { return pointNumber; }
            set { pointNumber = value; }
        }

      
        public override bool Equals(object obj)
        {
            if (obj.GetType() == typeof(DepartmentStationAddressClientData))
            {
                DepartmentStationAddressClientData stc = (DepartmentStationAddressClientData)obj;
                if ((this.lon == stc.lon) && (this.lat == stc.lat))
                {
                    return true;
                }
            }
            return false;
        }
    }
}