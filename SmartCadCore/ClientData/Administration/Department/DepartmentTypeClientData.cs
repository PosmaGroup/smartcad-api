using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Drawing;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class DepartmentTypeClientData : ClientData, ICloneable, INamedObjectClientData
    {
        public enum Frequency
        {
            None = 0,
            Daily = 1,
            Weekly = 2,
            Fortnightly = 3,
            Monthly = 4,
            Annual = 5
        }

        private string name;
        private IList departmentZones;
        private Color color;
        private IList alerts;
        private bool dispatch;
        private Frequency restartFrequency;
        private string pattern;

        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public IList DepartmentZones
        {
            get
            {
                return departmentZones;
            }
            set
            {
                departmentZones = value;
            }
        }

        public Color Color
        {
            get
            {
                return color;
            }
            set
            {
                color = value;
            }
        }

        public bool Dispatch
        {
            get
            {
                return dispatch;
            }
            set
            {
                dispatch = value;
            }
        }


        public string Pattern
        {
            get
            {
                return pattern;
            }
            set
            {
                pattern = value;
            }
        }

        public Frequency RestartFrequency
        {
            get
            {
                return restartFrequency;
            }
            set
            {
               restartFrequency =  value;
            }
        }

        public IList Alerts
        {
            get {
                return alerts;
            }
            set {
                alerts = value;
            }
        
        }

        #region Overrides
        public override string ToString()
        {
            return this.Name;
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is DepartmentTypeClientData)
            {
                result = this.Code == ((DepartmentTypeClientData)obj).Code;
            }
            return result;
        }
        #endregion

        #region ICloneable Members

        public object Clone()
        {
            DepartmentTypeClientData department = new DepartmentTypeClientData();
            department.Code = this.Code;
            department.Version = this.Version;
            department.name = this.Name;
            return department;
        }

        #endregion

        #region Initial data

        [InitialDataClient(PropertyName = "Name", PropertyValue = "DepartmentTypeData")]
        public static readonly UserResourceClientData Resource;

        #endregion
    }

    public class DepartmentTypeNameComparer : IComparer
    {
        #region IComparer Members

        public int Compare(object x, object y)
        {
            DepartmentTypeClientData dt1 = (DepartmentTypeClientData)x;
            DepartmentTypeClientData dt2 = (DepartmentTypeClientData)y;

            return dt1.Name.CompareTo(dt2.Name);
        }

        #endregion
    }
}
