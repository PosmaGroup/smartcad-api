using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class IncidentNotificationStatusClientData : ClientData
    {
         #region Fields

        private string name;
        private string friendlyName;
        private Color color;
        private byte[] image;
        private string customCode;
        private bool active;
        private bool immutable;

        #endregion

        #region Properties

        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public virtual string FriendlyName
        {
            get
            {
                return friendlyName;
            }
            set
            {
                friendlyName = value;
            }
        }

        public virtual Color Color
        {
            get
            {
                return color;
            }
            set
            {
                color = value;
            }
        }

        public virtual byte[] Image
        {
            get
            {
                return image;
            }
            set
            {
                image = value;
            }
        }

        public virtual string CustomCode
        {
            get
            {
                return customCode;
            }
            set
            {
                customCode = value;
            }
        }

        public virtual bool Active
        {
            get
            {
                return active;
            }
            set
            {
                active = value;
            }
        }

        public override string ToString()
        {
            string text = "";
            if (this.Name != null)
                text = this.Name;
            return text;
        }

        #region IImmutableObjectData Members

        public virtual bool Immutable
        {
            get
            {
                return immutable;
            }
            set
            {
                immutable = value;
            }
        }

        #endregion

        #endregion

        #region InitialData
        [InitialDataClient(PropertyName = "CustomCode", PropertyValue = "AUTOMATIC_SUPERVISOR")]
        public static IncidentNotificationStatusClientData AutomaticSupervisor;

        [InitialDataClient(PropertyName = "CustomCode", PropertyValue = "MANUAL_SUPERVISOR")]
        public static IncidentNotificationStatusClientData ManualSupervisor;

        [InitialDataClient(PropertyName = "CustomCode", PropertyValue = "ASSIGNED")]
        public static IncidentNotificationStatusClientData Assigned;

        [InitialDataClient(PropertyName = "CustomCode", PropertyValue = "REASSIGNED")]
        public static IncidentNotificationStatusClientData Reassigned;

        [InitialDataClient(PropertyName = "CustomCode", PropertyValue = "NEW")]
        public static IncidentNotificationStatusClientData New;

        [InitialDataClient(PropertyName = "CustomCode", PropertyValue = "CLOSED")]
        public static IncidentNotificationStatusClientData Closed;

        [InitialDataClient(PropertyName = "CustomCode", PropertyValue = "IN_PROGRESS")]
        public static IncidentNotificationStatusClientData InProgress;

        [InitialDataClient(PropertyName = "CustomCode", PropertyValue = "CANCELLED")]
        public static IncidentNotificationStatusClientData Cancelled;

        [InitialDataClient(PropertyName = "CustomCode", PropertyValue = "UPDATED")]
        public static IncidentNotificationStatusClientData Updated;

        [InitialDataClient(PropertyName = "CustomCode", PropertyValue = "PENDING")]
        public static IncidentNotificationStatusClientData Pending;

        [InitialDataClient(PropertyName = "CustomCode", PropertyValue = "DELAYED")]
        public static IncidentNotificationStatusClientData Delayed;
        #endregion
    }
}
