using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class IncidentTypeQuestionClientData : ClientData
    {
        private IncidentTypeClientData incidentType;
        private QuestionClientData question;

        public IncidentTypeClientData IncidentType
        {
            get
            {
                return this.incidentType;
            }
            set
            {
                this.incidentType = value;
            }
        }

        public QuestionClientData Question
        {
            get
            {
                return this.question;
            }
            set
            {
                this.question = value;
            }
        }

		public override bool Equals(object obj)
		{
			if (obj is IncidentTypeQuestionClientData == false)
				return false;
			IncidentTypeQuestionClientData itqcd = obj as IncidentTypeQuestionClientData;
			if (itqcd.question.Code == question.Code &&
				itqcd.incidentType.Code == incidentType.Code)
				return true;
			return false;
		}
    }
}
