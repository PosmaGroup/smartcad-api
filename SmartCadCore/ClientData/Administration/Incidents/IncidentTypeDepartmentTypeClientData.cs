using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class IncidentTypeDepartmentTypeClientData : ClientData
    {
        #region Fields

        private IncidentTypeClientData incidentType;
        private DepartmentTypeClientData departmentType;

        #endregion

        #region Properties

        public IncidentTypeClientData IncidentType
        {
            get
            {
                return this.incidentType;
            }
            set
            {
                this.incidentType = value;
            }
        }

        public DepartmentTypeClientData DepartmentType
        {
            get
            {
                return this.departmentType;
            }
            set
            {
                this.departmentType = value;
            }
        }

        #endregion
    }
}
