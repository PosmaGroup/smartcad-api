using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class IncidentTypeCategoryClientData : ClientData
    {
        #region Fields

        private string customCode;

        private string name;

        private IList incidentTypes;

        #endregion

        #region Properties

        public string CustomCode
        {
            get 
            {
                return this.customCode;
            }
            set
            {
                this.customCode = value;
            }
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }

        public IList IncidentTypes
        {
            get
            {
                return this.incidentTypes;
            }
            set
            {
                this.incidentTypes = value;
            }
        }

        #endregion
    }
}
