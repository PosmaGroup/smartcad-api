﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.ClientData
{
    public enum SourceIncidentAppEnum
    {
        Cctv,
        FirstLevel,
        Alarm,
        Lpr,
        SocialNetworks,
        None
    }


    [Serializable]
    public class IncidentClientData : ClientData, IIncidentReference, ICloneable
    {
        #region Fields

        private IList phoneReports;
        private IList cctvReports;
        private IList alarmReports;
        private IList twitterReports;
        private IList lprReports;
        private AddressClientData address;
        private bool isEmergency;
        private string customCode;
        private IncidentStatusClientData status;
        private DateTime startDate;
        private DateTime endDate;
        private IList notes;
        private IList incidentNotifications;
        private IList incidentTypes;
        private IList supportRequestReports;
        private string iconName;
        private int phoneReportCount;
        private int cctvReportCount;
        private int alarmReportCount;
        private SourceIncidentAppEnum sourceIncidentApp = SourceIncidentAppEnum.None;
        #endregion

        #region Properties

        public SourceIncidentAppEnum SourceIncidentApp
        {
            get
            {
                return sourceIncidentApp;
            }
            set
            {
                sourceIncidentApp = value;
            }
        }

        public DateTime StartDate
        {
            get
            {
                return this.startDate;
            }
            set
            {
                this.startDate = value;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return this.endDate;
            }
            set
            {
                this.endDate = value;
            }
        }
        public string CustomCode
        {
            get
            {
                return this.customCode;
            }
            set
            {
                this.customCode = value;
            }
        }

        public IncidentStatusClientData Status
        {
            get
            {
                return this.status;
            }
            set
            {
                this.status = value;
            }
        }

        public bool IsEmergency
        {
            get
            {
                return this.isEmergency;
            }
            set
            {
                this.isEmergency = value;
            }
        }

        public AddressClientData Address
        {
            get
            {
                return this.address;
            }
            set
            {
                this.address = value;
            }
        }

        public IList PhoneReports
        {
            get
            {
                return this.phoneReports;
            }
            set
            {
                this.phoneReports = value;
            }
        }

        public IList CctvReports
        {
            get
            {
                return this.cctvReports;
            }
            set
            {
                this.cctvReports = value;
            }
        }

        public IList AlarmReports
        {
            get
            {
                return this.alarmReports;
            }
            set
            {
                this.alarmReports = value;
            }
        }

        public IList TwitterReports
        {
            get
            {
                return this.twitterReports;
            }
            set
            {
                this.twitterReports = value;
            }
        }

        public IList LprReports
        {
            get
            {
                return this.lprReports;
            }
            set
            {
                this.lprReports = value;
            }
        }

        public IList SupportRequestReports
        {
            get
            {
                return supportRequestReports;
            }
            set
            {
                supportRequestReports = value;
            }
        }

        public IList Notes
        {
            get
            {
                return notes;
            }
            set
            {
                notes = value;
            }
        }

        public IList IncidentNotifications
        {
            get
            {
                return incidentNotifications;
            }
            set
            {
                incidentNotifications = value;
            }
        }

        public IList IncidentTypes
        {
            get
            {
                return incidentTypes;
            }
            set
            {
                incidentTypes = value;
            }
        }

        public string IconName
        {
            get
            {
                return iconName;
            }
            set
            {
                iconName = value;
            }
        }

        public int PhoneReportCount
        {
            get
            {
                return phoneReportCount;
            }
            set
            {
                phoneReportCount = value;
            }
        }

        public int CctvReportCount
        {
            get
            {
                return cctvReportCount;
            }
            set
            {
                cctvReportCount = value;
            }
        }


        public int AlarmReportCount
        {
            get
            {
                return alarmReportCount;
            }
            set
            {
                alarmReportCount = value;
            }
        }

        public int TwitterReportCount { get; set; }

        public int LprReportCount { get; set; }

        #region IIncidentReference Members

        public int IncidentCode
        {
            get
            {
                return Code;
            }
            set
            {
                Code = value;
            }
        }

        #endregion
        #endregion


        #region ICloneable Members

        /// <summary>
        /// Metodo para crear un clon de IncidentClientData, solo se clonan los
        /// datos necesarios para el despliege de los incidentes en mapas.
        /// </summary>
        /// <returns> object </returns>
        public object Clone()
        {
            IncidentClientData retval = new IncidentClientData();
            retval.Address = this.Address;
            retval.Code = this.Code;
            retval.CustomCode = CustomCode;
            retval.IconName = this.IconName;
            retval.IncidentCode = this.IncidentCode;
            retval.IncidentNotifications = this.IncidentNotifications;
            retval.IncidentTypes = this.IncidentTypes;
            retval.IsEmergency = this.IsEmergency;
            retval.Notes = this.Notes;
            retval.PhoneReports = this.PhoneReports;
            retval.StartDate = this.StartDate;
            retval.Status = this.Status;
            retval.SupportRequestReports = this.SupportRequestReports;
            return retval;
        }

        #endregion

        #region overrides

        public override bool Equals(object obj)
        {
            bool result = false;
            IncidentClientData incident = obj as IncidentClientData;
            if (incident != null)
            {
                if (this.Code == incident.Code)
                    result = true;
            }

            return result;
        }

        public override int GetHashCode()
        {
            return this.GetType().GetHashCode() * 10000000 + this.Code.GetHashCode();
        }

        #endregion
    }
}
