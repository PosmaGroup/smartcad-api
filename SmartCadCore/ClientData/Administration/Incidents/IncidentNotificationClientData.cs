using System;
using System.Collections;
using System.Text;
using System.Collections.Generic;

namespace SmartCadCore.ClientData
{   
    [Serializable]
    public class IncidentNotificationClientData : ClientData, IIncidentReference
    {
        #region Fields
        private bool availableRecommendedUnits;
        private IncidentNotificationStatusClientData status;
        private IncidentNotificationStatusClientData lastOperationalStatus;
        private string customCode;
        private string friendlyCustomCode;
        private IncidentNotificationPriorityClientData priority;
        private DepartmentStationClientData departmentStation;
        private DepartmentTypeClientData departmentType;
        private string incidentCustomCode;
        private int incidentCode;
        private IList incidentTypeNames;
        private IList incidentTypeCustomCodes;
        private List<int> incidentTypeCodes;
        private AddressClientData incidentAddress;
        private string distpatchOperatorLogin;
        private string dispatchOperatorFullName;
        private int distpatchOperatorCode;
        private IList dispatchOrders;
        private DateTime creationDate;
        private bool multipleOrganisms;
        private DateTime startDate;
        private DateTime lastStatusUpdate = DateTime.MinValue;
        private int lastOperator;
        private string detail;
        private bool canBeCancelled;
        private bool canBeClosed;
        private bool canBeChangedZoneAndStation;
        private bool inAttention;
        private int reportBaseCode;
        private Type reportBaseType;
        private bool recalculateStatus;
        private int assignedUnits;
        private bool canBeEnded;

        #endregion

        #region Properties
        public bool AvailableRecommendedUnits
        {
            get
            {
                return availableRecommendedUnits;
            }
            set
            {
                availableRecommendedUnits = value;
            }
        }

        public IncidentNotificationStatusClientData Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }

        public IncidentNotificationStatusClientData LastOperationalStatus
        {
            get
            {
                return lastOperationalStatus;
            }
            set
            {
                lastOperationalStatus = value;
            }
        }

        public string CustomCode
        {
            get
            {
                return customCode;
            }
            set
            {
                customCode = value;
            }
        }

        public string FriendlyCustomCode
        {
            get
            {
                return friendlyCustomCode;
            }
            set
            {
                friendlyCustomCode = value;
            }
        }

        public IncidentNotificationPriorityClientData Priority
        {
            get
            {
                return priority;
            }
            set
            {
                priority = value;
            }
        }

        public DepartmentStationClientData DepartmentStation
        {
            get
            {
                return departmentStation;
            }
            set
            {
                departmentStation = value;
            }
        }

        public DepartmentTypeClientData DepartmentType
        {
            get
            {
                return departmentType;
            }
            set
            {
                departmentType = value;
            }
        }

        public string IncidentCustomCode
        {
            get
            {
                return incidentCustomCode;
            }
            set
            {
                incidentCustomCode = value;
            }
        }

        public IList IncidentTypeNames
        {
            get
            {
                return incidentTypeNames;
            }
            set
            {
                incidentTypeNames = value;
            }
        }

        public IList IncidentTypeCustomCodes
        {
            get
            {
                return incidentTypeCustomCodes;
            }
            set
            {
                incidentTypeCustomCodes = value;
            }
        }

        public List<int> IncidentTypeCodes
        {
            get
            {
                return incidentTypeCodes;
            }
            set
            {
                incidentTypeCodes = value;
            }
        }

        public AddressClientData IncidentAddress
        {
            get
            {
                return incidentAddress;
            }
            set
            {
                incidentAddress = value;
            }
        }

        public string DistpatchOperatorLogin
        {
            get
            {
                return distpatchOperatorLogin;
            }
            set
            {
                distpatchOperatorLogin = value;
            }
        }

        public string DispatchOperatorFullName
        {
            get
            {
                return dispatchOperatorFullName;
            }
            set
            {
                dispatchOperatorFullName = value;
            }
        }

        public int DispatchOperatorCode
        {
            get
            {
                return distpatchOperatorCode;
            }
            set
            {
                distpatchOperatorCode = value;
            }
        }

        public IList DispatchOrders
        {
            get
            {
                return dispatchOrders;
            }
            set
            {
                dispatchOrders = value;
            }
        }

        public DateTime CreationDate
        {
            get
            {
                return creationDate;
            }
            set
            {
                creationDate = value;
            }
        }

        public int AssignedUnits
        {
            get
            {
                return assignedUnits;
            }
            set
            {
                assignedUnits = value;
            }
        }

        public bool MultipleOrganisms
        {
            get
            {
                return multipleOrganisms;
            }
            set
            {
                multipleOrganisms = value;
            }
        }

        public DateTime LastStatusUpdate
        {
            get
            {
                return lastStatusUpdate;
            }
            set
            {
                lastStatusUpdate = value;
            }
        }

        public DateTime StartDate
        {
            get
            {
                return startDate;
            }
            set
            {
                startDate = value;
            }
        }

        public int LastOperator
        {
            get
            {
                return lastOperator;
            }
            set
            {
                lastOperator = value;
            }
        }

        public string Detail
        {
            get
            {
                return detail;
            }
            set
            {
                detail = value;
            }
        }

        public bool CanBeCancelled
        {
            get
            {
                return canBeCancelled;
            }
            set
            {
                canBeCancelled = value;
            }
        }

        public bool CanBeEnded
        {
            get
            {
                return canBeEnded;
            }
            set
            {
                canBeEnded = value;
            }
        }

        public bool CanBeClosed
        {
            get
            {
                return canBeClosed;
            }
            set
            {
                canBeClosed = value;
            }
        }

        public bool CanBeChangedZoneAndStation
        {
            get
            {
                return canBeChangedZoneAndStation;
            }
            set
            {
                canBeChangedZoneAndStation = value;
            }
        }

        public bool InAttention
        {
            get
            {
                return inAttention;
            }
            set
            {
                inAttention = value;
            }
        }

        public Type ReportBaseType
        {
            get
            {
                return reportBaseType;
            }
            set
            {
                reportBaseType = value;
            }
        }

        public int ReportBaseCode
        {
            get
            {
                return reportBaseCode;
            }
            set
            {
                reportBaseCode = value;
            }
        }

        public bool RecalculateStatus
        {
            get
            {
                return recalculateStatus;
            }
            set
            {
                recalculateStatus = value;
            }
        }
        #region IIncidentReference Members

        public int IncidentCode
        {
            get
            {
                return incidentCode;
            }
            set
            {
                incidentCode = value;
            }
        }

        #endregion 
        #endregion

        #region overrides

        public override bool Equals(object obj)
        {
            bool result = false;
            IncidentNotificationClientData notification = obj as IncidentNotificationClientData;
            if (notification != null)
            {
                if (this.Code == notification.Code)
                    result = true;
            }

            return result;
        }

        public override int GetHashCode()
        {
            return this.GetType().GetHashCode() * 10000000 + this.Code.GetHashCode();
        }

        #endregion
    }
}
