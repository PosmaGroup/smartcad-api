using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class IncidentTypeClientData : ClientData
    {
        #region Fields

        private string name;
        private string customCode;
        private bool noEmergency;
        private string friendlyName;
        private bool exclusive;
        private IList incidentTypeDepartmentTypeClients;
        private IList incidentTypeQuestions;
        private IList incidentTypeCategories;
        private string procedure;
        private string iconName;

        #endregion

        #region Properties

        

        public string IconName
        {
            get 
            { 
                return this.iconName; 
            }
            set 
            { 
                this.iconName = value; 
            }
        }
	

        public string Procedure
        {
            get
            {
                return this.procedure;
            }
            set
            {
                this.procedure = value;
            }
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }

        public string CustomCode
        {
            get
            {
                return this.customCode;
            }
            set
            {
                this.customCode = value;
            }
        }

        public bool NoEmergency
        {
            get
            {
                return this.noEmergency;
            }
            set
            {
                this.noEmergency = value;
            }
        }

        public string FriendlyName
        {
            get
            {
                return this.friendlyName;
            }
            set
            {
                this.friendlyName = value;
            }
        }

        public bool Exclusive
        {
            get
            {
                return this.exclusive;
            }
            set
            {
                this.exclusive = value;
            }
        }

        public IList IncidentTypeDepartmentTypeClients
        {
            get
            {
                return this.incidentTypeDepartmentTypeClients;
            }
            set
            {
                this.incidentTypeDepartmentTypeClients = value;
            }
        }
        public IList IncidentTypeQuestions
        {
            get
            {
                return this.incidentTypeQuestions;
            }
            set
            {
                this.incidentTypeQuestions = value;
            }
        }
        public IList IncidentTypeCategories
        {
            get
            {
                return this.incidentTypeCategories;
            }
            set
            {
                this.incidentTypeCategories = value;
            }
        }

        #endregion

        public override string ToString()
        {
            string ToReturn = this.friendlyName + " (";
            if (this.noEmergency == true)
                ToReturn = ToReturn + "NE";
            else
                ToReturn = ToReturn + this.customCode;
            ToReturn = ToReturn + ")";

            //if (this.Exclusive == true)
            //{
            //    ToReturn = ToReturn + " (-) ";
            //}
            return ToReturn;
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            IncidentTypeClientData objectData = obj as IncidentTypeClientData;
            if (objectData != null)
            {
                if (this.Code == objectData.Code)
                    result = true;
            }
            return result;
        }

        [InitialDataClient(PropertyName = "Name", PropertyValue = "IncidentTypeData")]
        public static readonly UserResourceClientData Resource;
    }

    public class IncidentTypeComparer : IComparer
    {
        #region IComparer Members

        public int Compare(object x, object y)
        {
            IncidentTypeClientData it1 = (IncidentTypeClientData)x;
            IncidentTypeClientData it2 = (IncidentTypeClientData)y;

            return it1.FriendlyName.CompareTo(it2.FriendlyName);
        }

        #endregion
    }
}
