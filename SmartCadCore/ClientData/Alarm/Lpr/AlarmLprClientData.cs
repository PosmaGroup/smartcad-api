using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class AlarmLprClientData : ClientData
    {
        public enum AlarmStatus
        {
            NotTaken = 0,
            InProcess = 1,
            Ended = 2
        }

        public VehicleRequestClientData VehicleRequest 
        { 
            get;
            set; 
        }

        public LprClientData Lpr 
        { 
            get;
            set;
        }

        public virtual DateTime StartDate
        {
            get;
            set;
        }
       
        public virtual DateTime? EndDate
        {
            get;
            set;
        }

        public byte[] ColorImage
        {
            get;
            set;
        }
       
        public byte[] BWImage
        {
            get;
            set;
        }
      
        public string plate
        {
            get;
            set;
        }

        public AlarmStatus Status
        {
            get;
            set;
        }

        [InitialDataClient(PropertyName = "Name", PropertyValue = "AlarmLprClientData")]
        public static readonly UserResourceClientData Resource;

        public override string ToString()
        {
            return Lpr.Name;
        }

        public override bool Equals(object obj)
        {
            try
            {
                if (obj.GetType() == typeof(AlarmLprClientData))
                {
                    AlarmLprClientData cam = (AlarmLprClientData)obj;
                    if (this.VehicleRequest.Code == cam.VehicleRequest.Code)
                    {
                        return true;
                    }
                }
            }
            catch { }
            return false;
        }

    }
}