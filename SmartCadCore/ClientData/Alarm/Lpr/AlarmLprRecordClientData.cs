using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class AlarmLprRecordClientData : ClientData
    {

      
        private string requestFor;
        private DepartmentTypeClientData requestByDepartment;

        private string requestByOfficcer;
        private string plate;
        private string brand;
        private string model;
        private string color;
        private LprClientData myLpr;
        private string details;
        private DateTime dateAndTime;

        
       
        
        public string RequestFor
        {
            get
            {
                return requestFor;
            }
            set
            {
                requestFor = value;
            }
        }

        public LprClientData Lpr
        {
            get
            {
                return myLpr;
            }
            set
            {
                myLpr = value;
            }
        }

        
        public DepartmentTypeClientData RequestByDepartment
        {
            get
            {
                return requestByDepartment;
            }
            set
            {
                requestByDepartment = value;
            }
        }


        
        public string RequestByOfficcer
        {
            get
            {
                return requestByOfficcer;
            }
            set
            {
                requestByOfficcer = value;
            }
        }
        
        public string Plate
        {
            get
            {
                return plate;
            }
            set
            {
                plate = value;
            }
        }
        
        public string Brand
        {
            get
            {
                return brand;
            }
            set
            {
                brand = value;
            }
        }
        
        public string Model
        {
            get
            {
                return model;
            }
            set
            {
                model = value;
            }
        }
        
        public string Color
        {
            get
            {
                return color;
            }
            set
            {
                color = value;
            }
        }
        
        public string Details
        {
            get
            {
                return details;
            }
            set
            {
                details = value;
            }
        }
        
        public DateTime DateAndTime
        {
            get
            {
                return dateAndTime;
            }
            set
            {
                dateAndTime = value;
            }
        }


        [InitialDataClient(PropertyName = "Name", PropertyValue = "AlarmLprRecordData")]
        public static readonly UserResourceClientData Resource;

        

        public override bool Equals(object obj)
        {
            if (obj.GetType() == typeof(AlarmLprRecordClientData))
            {
                AlarmLprRecordClientData cam = (AlarmLprRecordClientData)obj;
                if ((this.plate == cam.Plate)&&(this.dateAndTime == cam.DateAndTime))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
