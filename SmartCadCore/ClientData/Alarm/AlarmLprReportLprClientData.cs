using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class AlarmLprReportLprClientData : ClientData
    {
        #region Fields

        private string name;      
        private AddressClientData address;

        #endregion

        #region Constructors

        public AlarmLprReportLprClientData()
        {
        }

        public AlarmLprReportLprClientData(string name, AddressClientData address)
        {
            this.name = name;            
            this.address = address;
        }

        #endregion

        #region Properties

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }        

        public AddressClientData Address
        {
            get
            {
                return address;
            }
            set
            {
                address = value;
            }
        }

        #endregion
    }
}
