﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class LprAlertClientData: ClientData
    {
        #region Fields

        private CameraClientData camera;

        private DateTime alertDate;

        private int status;

        private int externalId;

        private String plate;

        private String vehiculeCode;

        #endregion

        #region propierties

        public virtual CameraClientData Camera
        {
            get { return camera; }
            set { camera = value; }
        }

        public virtual DateTime AlertDate
        {
            get { return alertDate; }
            set { alertDate = value; }
        }

        public virtual int Status
        {
            get { return status; }
            set { status = value; }
        }

        public virtual int ExternalId
        {
            get { return externalId; }
            set { externalId = value; }
        }

        public virtual String Plate
        {
            get { return plate; }
            set { plate = value; }
        }

        public virtual String VehiculeCode
        {
            get { return vehiculeCode; }
            set { vehiculeCode = value; }
        }
        #endregion

        #region initilData

        [InitialDataClient(PropertyName = "Name", PropertyValue = "lprAlertData")]
        public static readonly UserResourceClientData Resource;
        #endregion
    }
}
