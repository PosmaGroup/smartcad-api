using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class VideoDeviceClientData : DeviceClientData
    {

       
        private StructClientData structClientData;
               

        public StructClientData StructClientData
        {
            get
            {
                return structClientData;
            }
            set
            {
                structClientData = value;
            }
        }


        [InitialDataClient(PropertyName = "Name", PropertyValue = "VideoDeviceData")]
        public static readonly UserResourceClientData Resource;
        public override string ToString()
        {
            return Name;
        }
        public override bool Equals(object obj)
        {
            try
            {
                if (obj.GetType() == typeof(VideoDeviceClientData))
                {
                    VideoDeviceClientData cam = (VideoDeviceClientData)obj;
                    if (this.Name == cam.Name)
                    {
                        return true;
                    }
                }
            }
            catch { }
            return false;
        }
    }

}
