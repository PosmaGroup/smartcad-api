using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class CameraClientData : VideoDeviceClientData
    {
        private string resolution;
        private string streamType;
        private string frameRate;
        private CameraTypeClientData type;
        private ConnectionTypeClientData connectionType;

        public string CustomId { get; set; }

        public string Resolution
        {
            get
            {
                return resolution;
            }
            set
            {
                resolution = value;
            }
        }       
        public string StreamType
        {
            get
            {
                return streamType;
            }
            set
            {
                streamType = value;
            }
        }        
        public string FrameRate
        {
            get
            {
                return frameRate;
            }
            set
            {

                frameRate = value;
            }
        }
        public CameraTypeClientData Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }
        }
        public ConnectionTypeClientData ConnectionType
        {
            get
            {
                return connectionType;
            }
            set
            {
                connectionType = value;
            }
        }
        //public StructClientData StructClientData
        //{
        //    get
        //    {
        //        return StructClientData;
        //    }
        //    set
        //    {
        //        StructClientData = value;
        //    }
        //}

        [InitialDataClient(PropertyName = "Name", PropertyValue = "CameraData")]
        public static readonly UserResourceClientData Resource;

        public override string ToString()
        {
            return Name;
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() == typeof(CameraClientData))
            {
                CameraClientData cam = (CameraClientData)obj;
                if (this.Code == cam.Code)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
