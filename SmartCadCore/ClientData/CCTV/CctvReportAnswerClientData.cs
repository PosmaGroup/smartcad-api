using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class ReportAnswerClientData : ClientData
    {
        #region Fields

        private int questionCode;
        private IList answers = new ArrayList();
        private int reportCode;
        private string questionText;
        private string questionDesc;
        private Hashtable htAswers;
        
		#endregion

        #region Contructors

        public ReportAnswerClientData()
        { 
		
		}

        public ReportAnswerClientData(int questionCode, QuestionPossibleAnswerClientData answer, string answerText)
        {
            this.questionCode = questionCode;

            PossibleAnswerAnswerClientData paacd = new PossibleAnswerAnswerClientData();
            paacd.TextAnswer = answerText;
            paacd.ReportAnswerCode = this.Code;
            paacd.QuestionPossibleAnswer = answer;           

            this.answers.Add(paacd);
        }

        public ReportAnswerClientData(int questionCode, string answer)
        {
            this.questionCode = questionCode;
        }

        #endregion

        #region Properties

        public int QuestionCode
        {
            get
            {
                return this.questionCode;
            }
            set
            {
                this.questionCode = value;
            }
        }

        public string QuestionDesc 
        {
            get { return this.questionDesc; }
            set { this.questionDesc = value; }
        }

        public string QuestionText
        {
            get
            {
                return this.questionText;
            }
            set
            {
                this.questionText = value;
            }
        }

        public IList Answers
        {
            get
            {
                return this.answers;
            }
            set
            {
                this.answers = value;
            }
        }

        public int ReportCode
        {
            get
            {
                return this.reportCode;
            }
            set
            {
                this.reportCode = value;
            }
        }

        #endregion


        /// <summary>
        /// If else of zone, street and reference for addressing the issue # 152 
        /// for sorting of order of typify of answers
        /// </summary>
        /// <param name="pracd"></param>
        /// <param name="deleting"></param>
        public void UpdateAnswers(ReportAnswerClientData pracd, bool deleting)
        {
            foreach (PossibleAnswerAnswerClientData paacd in pracd.Answers)
            {
                string desc = pracd.QuestionDesc;
                int index = this.answers.IndexOf(paacd);
                if (index >= 0)
                    if (deleting == false)
                        ((PossibleAnswerAnswerClientData)this.answers[index]).TextAnswer = paacd.TextAnswer;
                    else
                    {
                        this.answers.RemoveAt(index);
                        return;
                    }
                if (!deleting == false) continue;
                if (index > -1)
                    this.answers.RemoveAt(index);
                if (desc == "Zone")
                    this.answers.Insert(0, paacd);
                else if (desc == "Street")
                    if (answers.Count >= 1)
                        if (((PossibleAnswerAnswerClientData)this.answers[0]).QuestionPossibleAnswer.Description == "Zone") this.answers.Insert(1, paacd);
                        else this.answers.Insert(0, paacd);
                    else this.answers.Add(paacd);
                else if (desc == "Reference")
                    if (answers.Count >= 2)
                        if (((PossibleAnswerAnswerClientData)this.answers[0]).QuestionPossibleAnswer.Description == "Street") this.answers.Insert(1, paacd);
                        else if (((PossibleAnswerAnswerClientData)this.answers[0]).QuestionPossibleAnswer.Description == "Zone") this.answers.Insert(1, paacd);
                        else if (((PossibleAnswerAnswerClientData)this.answers[1]).QuestionPossibleAnswer.Description == "Street") this.answers.Insert(2, paacd);
                        else if (((PossibleAnswerAnswerClientData)this.answers[0]).QuestionPossibleAnswer.Description == "More") this.answers.Insert(0, paacd);
                        else this.answers.Insert(0, paacd);
                    else this.answers.Add(paacd);
                else if (desc == "More")
                    if (answers.Count >= 3)
                        this.answers.Insert(3, paacd);
                    else
                        this.answers.Add(paacd);
                else this.answers.Add(paacd);
            }
        }

    }
}
