using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class CameraClientDataAlarm : CameraClientData
    {
        private DateTime alarmDate;
        private string alarmRule;

        public DateTime AlarmDate
        {
            get
            {
                return alarmDate;
            }
            set
            {

                alarmDate = value;
            }
        }

        public string AlarmRule
        {
            get
            {
                return alarmRule;
            }
            set
            {

                alarmRule = value;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() == typeof(CameraClientDataAlarm))
            {
                CameraClientDataAlarm cam = (CameraClientDataAlarm)obj;
                if (this.Code == cam.Code)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
