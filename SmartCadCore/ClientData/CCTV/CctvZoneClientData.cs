using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class CctvZoneClientData : ClientData
    {
        private string name;
        private int structNumber;
        private IList structs;
        
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public int StructNumber
        {
            get
            {
                return structNumber;
            }
            set
            {
                structNumber = value;
            }
        }

        public IList Structs
        {
            get
            {
                return structs;
            }
            set
            {
                structs = value;
            }
        }


        public override string ToString()
        {
            return name;
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() == typeof(CctvZoneClientData))
            {
                CctvZoneClientData zone = (CctvZoneClientData)obj;
                if (this.Code == zone.Code)
                {
                    return true;
                }
            }
            return false;
        }

        [InitialDataClient(PropertyName = "Name", PropertyValue = "CctvZoneData")]
        public static readonly UserResourceClientData Resource;
    }
}
