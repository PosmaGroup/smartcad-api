﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class ConnectionTypeClientData : ClientData
    {
        private string name = string.Empty;

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        [InitialDataClient(PropertyName = "Name", PropertyValue = "ConnectionTypeData")]
        public static readonly UserResourceClientData Resource;

        public override string ToString()
        {
            return Name;
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() == typeof(ConnectionTypeClientData))
            {
                ConnectionTypeClientData ctc = (ConnectionTypeClientData)obj;
                if (this.Name == ctc.Name)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
