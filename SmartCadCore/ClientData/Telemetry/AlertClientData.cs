﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class AlertClientData: ClientData
    {
   
        public AlertClientData()
        {
        }


        public string CustomCode
        {
            get;
            set;
        }


        public string Name
        {
            get;
            set;
        }


        public string Measure
        {
            get;
            set;
        }
               

        #region Overrides

        public override string ToString()
        {
            return this.Name;
        }

        #endregion

    }

}

