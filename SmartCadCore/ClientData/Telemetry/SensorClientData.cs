﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class SensorClientData : ClientData
    {
        
        public enum SensorType
        {
            Input = 0,
            Output = 1
        }

        public SensorClientData()
        {
        }

        public string CustomCode
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public SensorType Type
        {
            get;
            set;
        }



        
        #region Overrides

        public override bool Equals(object obj)
        {
            try
            {
                if (obj.GetType() == typeof(SensorClientData))
                {
                    SensorClientData sensor = (SensorClientData)obj;
                    if (this.Code == sensor.Code)
                    {
                        return true;
                    }
                }
            }
            catch { }
            return false;
        }

        public override string ToString()
        {
            return this.Name;
        }

        #endregion

    }
}
