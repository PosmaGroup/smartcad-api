using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class QuestionPossibleAnswerClientData : ClientData
    {
        private int questionCode;
        private string description;
        private string procedure;
        private string controlName;

        public string ControlName
        {
            get
            {
                return this.controlName;
            }
            set
            {
                this.controlName = value;
            }
        }

        public int QuestionCode
        {
            get
            {
                return this.questionCode;
            }
            set
            {
                this.questionCode = value;
            }
        }


        public string Description
        {
            get
            {
                return this.description;
            }
            set
            {
                this.description = value;
            }
        }

        public string Procedure
        {
            get
            {
                return this.procedure;
            }
            set
            {
                this.procedure = value;
            }
        }

        public override bool Equals(object obj)
		{
			if (obj is QuestionPossibleAnswerClientData == false)
				return false;
			QuestionPossibleAnswerClientData qpacd = obj as QuestionPossibleAnswerClientData;
			if (qpacd.questionCode == questionCode &&
				qpacd.controlName == controlName)
			{
				return true;
			}
			return false;
		}
    }
}
