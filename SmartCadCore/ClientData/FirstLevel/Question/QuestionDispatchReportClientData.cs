﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class QuestionDispatchReportClientData : ClientData
    {
        /// <summary>
        /// A possible question of the phone report
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// XML that contains the way the question would be painted.
        /// </summary>
        public string Render { get; set; }

        public bool Required { get; set; }

        #region Initial data

        [InitialDataClient(PropertyName = "Name", PropertyValue = "QuestionDispatchReportData")]
        public static readonly UserResourceClientData Resource;

        #endregion
    }
}
