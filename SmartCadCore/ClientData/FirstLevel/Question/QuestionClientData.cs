using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class QuestionClientData : ClientData
    {
        private string shortText;
        private string text;
        private RequirementTypeClientEnum? requirementType;
        private string customCode;
        private IList possibleAnswers;
        private string renderMethod;
        private IList incidentTypes;
        private IList app;

        public IList Application
        {
            get
            {
                return this.app;
            }
            set
            {
                this.app = value;
            }
        }

        public string ShortText
        {
            get
            {
                return this.shortText;
            }
            set
            {
                this.shortText = value;
            }
        }

        public string Text
        {
            get
            {
                return this.text;
            }
            set
            {
                this.text = value;
            }
        }

        public RequirementTypeClientEnum? RequirementType
        {
            get
            {
                return this.requirementType;
            }
            set
            {
                this.requirementType = value;
            }
        }

        public string CustomCode
        {
            get
            {
                return this.customCode;
            }
            set
            {
                this.customCode = value;
            }
        }

        public string RenderMethod
        {
            get
            {
                return this.renderMethod;
            }
            set
            {
                this.renderMethod = value;
            }
        }

        public IList PossibleAnswers
        {
            get
            {
                return this.possibleAnswers;
            }
            set
            {
                this.possibleAnswers = value;
            }
        }

        public IList IncidentTypes
        {
            get
            {
                return incidentTypes;
            }
            set
            {
                incidentTypes = value;
            }
        }

        public IList App
        {
            get
            {
                return app;
            }
            set
            {
                app = value;
            }
        }

		public override string ToString()
		{
			return "(" + this.shortText + ") " + this.Text;
		}

        public override bool Equals(object obj)
        {
            bool result = false;
            QuestionClientData objectData = obj as QuestionClientData;
            if (objectData != null)
            {
                if (this.Code == objectData.Code)
                    result = true;
            }
            return result;
        }

        [InitialDataClient(PropertyName = "Name", PropertyValue = "QuestionData")]
        public static readonly UserResourceClientData Resource;  
    }

    public enum RequirementTypeClientEnum
    {
        None = 0,
        Recommended = 1,
        Required = 2
    }

    public enum QuestionClientType
    {
        Incident = 0,
        DispatchReport = 1
    }
}
