﻿using System;
using System.Linq;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class DispatchReportClientData : ClientData
    {
        /// <summary>
        /// Name of the dispatch report.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// List of incident types of the dispatch report.
        /// </summary>
        public IList IncidentTypes { get; set; }
        /// <summary>
        /// List of department types of the dispatch report.
        /// </summary>
        public IList DepartmentTypes { get; set; }
        /// <summary>
        /// List of question of the dispatch report.
        /// </summary>
        public IList Questions { get; set; }

        [InitialDataClient(PropertyName = "Name", PropertyValue = "DispatchReportData")]
        public static readonly UserResourceClientData Resource; 
    }
}
