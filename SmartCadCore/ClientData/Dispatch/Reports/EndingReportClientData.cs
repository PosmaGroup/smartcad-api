using System;
using System.Collections;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class EndingReportClientData
    {
        #region Fields
        private int officerCode;
        private DateTime endingTime;
        private int dispatchOrderCode;
        private int closingDispatchOperatorCode;
        private IList answers;
        private bool byUnit;

        #endregion

        #region Properties
        
        public int OfficerCode
        {
            get { return officerCode; }
            set { officerCode = value; }
        }

        public DateTime EndingTime
        {
            get { return endingTime; }
            set { endingTime = value; }
        }
                         
        public int DispatchOrderCode
        {
            get { return dispatchOrderCode; }
            set { dispatchOrderCode = value; }
        }
              
        public int ClosingDispatchOperatorCode
        {
            get { return closingDispatchOperatorCode; }
            set { closingDispatchOperatorCode = value; }
        }

        public IList Answers
        {
            get { return answers; }
            set { answers = value; }
        }

        public bool ByUnit
        {
            get { return byUnit; }
            set { byUnit = value; }
        }

        #endregion
	
    }
}
