using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class ReportBaseDepartmentTypeClientData : ClientData
    {
        int departmentTypeCode;
        int priorityCode;
        int reportBaseCode;

        public int DepartmentTypeCode
        {
            get
            {
                return this.departmentTypeCode;
            }
            set
            {
                this.departmentTypeCode = value;
            }
        }

        public int PriorityCode
        {
            get
            {
                return this.priorityCode;
            }
            set
            {
                this.priorityCode = value;
            }
        }

        public int ReportBaseCode
        {
            get
            {
                return this.reportBaseCode;
            }
            set
            {
                this.reportBaseCode = value;
            }
        }
    }
}
