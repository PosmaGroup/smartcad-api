﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class EndingReportAnswerClientData:ClientData
    {
        public string QuestionText
        {
            get;
            set;
        }

        public string AnswerHeaderText
        {
            get;
            set;
        }

        public string AnswerText
        {
            get;
            set;
        }

        public int EndingReportCode
        {
            get;
            set;

        }

    }
}
