using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class DispatchOrderStatusClientData : ClientData
    {
        #region Fields

        private string name;
        private string friendlyName;
        private Color color;
        private byte[] image;
        private string customCode;
        private bool immutable;

        #endregion

        #region Properties

        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public virtual string FriendlyName
        {
            get
            {
                return friendlyName;
            }
            set
            {
                friendlyName = value;
            }
        }

        public virtual Color Color
        {
            get
            {
                return color;
            }
            set
            {
                color = value;
            }
        }

        public virtual byte[] Image
        {
            get
            {
                return image;
            }
            set
            {
                image = value;
            }
        }

        public virtual string CustomCode
        {
            get
            {
                return customCode;
            }
            set
            {
                customCode = value;
            }
        }

        #region IImmutableObjectData Members

        public virtual bool Immutable
        {
            get
            {
                return immutable;
            }
            set
            {
                immutable = value;
            }
        }

        #endregion

        #endregion

        #region InitialData

        [InitialDataClient(PropertyName = "CustomCode", PropertyValue = "DISPATCHED")]
        public static DispatchOrderStatusClientData Dispatched;

        [InitialDataClient(PropertyName = "CustomCode", PropertyValue = "CANCELLED")]
        public static DispatchOrderStatusClientData Cancelled;

        [InitialDataClient(PropertyName = "CustomCode", PropertyValue = "CLOSED")]
        public static DispatchOrderStatusClientData Closed;

        [InitialDataClient(PropertyName = "CustomCode", PropertyValue = "DELAYED")]
        public static DispatchOrderStatusClientData Delayed;

        [InitialDataClient(PropertyName = "CustomCode", PropertyValue = "UNIT_ONSCENE")]
        public static DispatchOrderStatusClientData UnitOnScene;

        #endregion
        public override string ToString()
        {
            string text = "";
            if (this.Name != null)
            {
                text = this.Name;
            }
            return text;
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            DispatchOrderStatusClientData status = obj as DispatchOrderStatusClientData;
            if (status != null)
            {
                if (this.Code == status.Code)
                    result = true;
            }
            return result;
        }
    }
}
