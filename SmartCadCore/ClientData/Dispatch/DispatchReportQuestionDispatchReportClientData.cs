﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class DispatchReportQuestionDispatchReportClientData: ClientData
    {

        public int QuestionCode
        {
            get;
            set;
        }

        public string QuestionText
        {
            get;
            set;
        }

        public string QuestionRender
        {
            get;
            set;
        }

        public bool QuestionRequired
        {
            get;
            set;
        }

        public int ReportCode
        {
            get;
            set;
        }

        public int Order
        {
            get;
            set;
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            DispatchReportQuestionDispatchReportClientData objectData = obj as DispatchReportQuestionDispatchReportClientData;
            if (objectData != null)
            {
                if (this.Code == objectData.Code)
                    result = true;
            }
            return result;
        }
    }
}
