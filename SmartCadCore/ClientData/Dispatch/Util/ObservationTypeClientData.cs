using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class ObservationTypeClientData: ClientData
    {
        private string name;
        private string friendlyName;
		private IList userAccessName;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string FriendlyName
        {
            get { return friendlyName; }
            set { friendlyName = value; }
        }

        public override string ToString()
        {
            return this.friendlyName;
        }

		public IList UserAccessName
		{
			get
			{
				return userAccessName;
			}
			set
			{
				userAccessName = value;
			}
		}
    }
}
