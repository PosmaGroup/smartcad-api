﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.ClientData
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
    public class InitialDataClientAttribute : Attribute
    {
        private string propertyName;

        public string PropertyName
        {
            get
            {
                return propertyName;
            }
            set
            {
                propertyName = value;
            }
        }

        private string propertyValue;

        public string PropertyValue
        {
            get
            {
                return propertyValue;
            }
            set
            {
                propertyValue = value;
            }
        }
    }
}
