using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class ClientData
    {
        internal const string DateNHibernateFormatWithSeconds = "yyyyMMdd HH:mm:ss";

        private int code;

        private int version;

        public ClientData()
        {
        }

        public int Code
        {
            get
            {
                return code;
            }
            set
            {
                code = value;
            }
        }

        public int Version
        {
            get
            {
                return version;
            }
            set
            {
                version = value;
            }
        }
    }

    public class ClientDataCodeComparer : IComparer
    {
        #region IComparer Members

        public int Compare(object x, object y)
        {
            ClientData cd1 = x as ClientData;
            ClientData cd2 = y as ClientData;
            int result = 0;

            if (cd1 == null && cd2 == null)
                result = 0;
            else if (cd1 == null)
                result = -1;
            else if (cd2 == null)
                result = 1;
            else
                result = cd1.Code.CompareTo(cd2.Code);
            return result;
        }

        #endregion
    }
}
