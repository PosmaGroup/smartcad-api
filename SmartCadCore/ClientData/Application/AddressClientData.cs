﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class AddressClientData
    {
        #region Fields

        private string zone;
        private string street;
        private string reference;
        private string more;
        private string state;
        private string town;
        private double lon;
        private double lat;
        private bool isSynchronized;
        private string fullText;
        #endregion

        #region Constructors

        public AddressClientData()
        {
        }

        public AddressClientData(double lon, double lat)
        {
            this.lat = lat;
            this.lon = lon;
        }

        public AddressClientData(string zone, string street, string reference, string more, bool isSynchronized)
        {
            this.zone = zone;
            this.street = street;
            this.reference = reference;
            this.more = more;
            this.IsSynchronized = isSynchronized;
        }

        public AddressClientData(string fullText, bool isSynchronized)
        {
            this.zone = "";
            this.street = "";
            this.reference = "";
            this.more = "";
            this.fullText = fullText;
            this.IsSynchronized = isSynchronized;
        }

        #endregion

        #region Properties

        public virtual string FullText
        {
            get
            {
                return fullText;
            }
            set
            {
                fullText = value;
            }
        }

        public virtual string Zone
        {
            get
            {
                return zone;
            }
            set
            {
                zone = value;
            }
        }

        public virtual string Street
        {
            get
            {
                return street;
            }
            set
            {
                street = value;
            }
        }

        public virtual string Reference
        {
            get
            {
                return reference;
            }
            set
            {
                reference = value;
            }
        }

        public virtual string More
        {
            get
            {
                return more;
            }
            set
            {
                more = value;
            }
        }
        public virtual string State
        {
            get
            {
                return state;
            }
            set
            {
                state = value;
            }
        }

        public virtual string Town
        {
            get
            {
                return town;
            }
            set
            {
                town = value;
            }
        }
        public virtual double Lon
        {
            get
            {
                return lon;
            }
            set
            {
                lon = value;
            }
        }

        public virtual double Lat
        {
            get
            {
                return lat;
            }
            set
            {
                lat = value;
            }
        }

        public bool IsSynchronized
        {
            get
            {
                return isSynchronized;
            }

            set
            {
                isSynchronized = value;
            }
        }

        #endregion

        #region Overrides

        public override string ToString()
        {
            return this.fullText + "[" + this.zone + "][" + this.street + "][" + this.reference + this.more + "]";
        }

        #endregion
    }
}
