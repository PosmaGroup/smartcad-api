﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class ApplicationPreferenceClientData : ClientData, ICloneable
    {
        #region Fields
        
        private string name;
        private string type;
        private string value;
        private string module;
        private string description;
        private string measureUnit;
        private string valueRange;
        private string minValue;
        private string maxValue;

        #endregion

        #region Properties

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public string Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }
        }

        public string Value
        {
            get
            {
                return value;
            }
            set
            {
                this.value = value;
            }
        }

        public string Module
        {
            get
            {
                return module;
            }
            set
            {
                module = value;
            }
        }

        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }

        public string MeasureUnit
        {
            get
            {
                return measureUnit;
            }
            set
            {
                measureUnit = value;
            }
        }

        public string ValueRange
        {
            get
            {
                return valueRange;
            }
            set
            {
                this.valueRange = value;
            }
        }

        public string MinValue
        {
            get
            {
                return minValue;
            }
            set
            {
                minValue = value;
            }
        }

        public string MaxValue
        {
            get
            {
                return maxValue;
            }
            set
            {
                maxValue = value;
            }
        }
        #endregion

        #region Overrides

        public override bool Equals(object obj)
        {
            bool result = false;
            ApplicationPreferenceClientData preference = obj as ApplicationPreferenceClientData;
            if (preference != null)
            {
                if (this.Code == preference.Code)
                    result = true;
            }

            return result;
        }

        public override int GetHashCode()
        {
            return this.GetType().GetHashCode() * 10000000 + this.Code.GetHashCode();
        }

        public override string ToString()
        {
            return this.Name;
        }

        #endregion

        #region Initial data

        [InitialDataClient(PropertyName = "Name", PropertyValue = "ApplicationPreferenceData")]
        public static readonly UserResourceClientData Resource;

        #endregion

        #region ICloneable Members

        public object Clone()
        {
            ApplicationPreferenceClientData preference = new ApplicationPreferenceClientData();
            preference.Code = this.Code;
            preference.Version = this.Version;
            preference.Name = this.Name;
            preference.Type = this.Type;
            preference.Value = this.Value;
            preference.Module = this.Module;
            preference.Description = this.Description;
            preference.MeasureUnit = this.MeasureUnit;
            preference.ValueRange = this.ValueRange;
            preference.MinValue = this.MinValue;
            preference.MaxValue = this.MaxValue;
            return preference;
        }

        #endregion
    }
}
