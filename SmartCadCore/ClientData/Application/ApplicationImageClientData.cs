﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class ApplicationClientData: ClientData
    {
		private string app;
		private OperatorClientData oper;
        private string[] toApps;
        private IList toOperators;
        private string message;

        public ApplicationClientData()
        {
        }

        public ApplicationClientData(string a,OperatorClientData o, string[] toA, IList toO)
        {
            app = a;
            oper = o;
            toApps = toA;
            toOperators = toO;
        }

        public string Application
        {
            get
            {
                return app;
            }
            set
            {
                app = value as string;
            }
        }

        public OperatorClientData Operator
        {
            get
            {
                return oper;
            }
            set
            {
                oper = value as OperatorClientData;
            }
        }

        public string[] ToApplications
        {
            get
            {
                return this.toApps;
            }
            set
            {
                toApps = value;
            }
        }

        public IList ToOperators
        {
           get
           {
               return toOperators;
           }
           set
           {
               toOperators = value;
           }
        }

        public string Message
        {
           get
           {
               return message;
           }
           set
           {
               message = value;
           }
        }
    }

	[Serializable]
	public class ApplicationImageClientData : ApplicationClientData
	{
		private byte[] image;
        private int originalLength;

		public byte[] Image
		{
			get { return this.image; }
			set { this.image = value as byte[]; }
		}

        public int OriginalLength
        {
            get
            {

                return originalLength;
            }
            set
            {
                originalLength = value;
            }
        }
	}

	[Serializable]
	public class ApplicationMessageClientData : ApplicationClientData
	{
        public ApplicationMessageClientData()
        {
            ToApplications = new string[] {"Dispatch", "FirstLevel", "Supervision"};
        }

		private DateTime date;
        

		public DateTime Date
		{
			get
			{

				return date;
			}
			set
			{
				date = value;
			}
		}		
	}
}
