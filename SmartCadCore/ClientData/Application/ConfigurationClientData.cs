using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class ConfigurationClientData : ClientData
    {
        #region Fields

        private IDictionary<string, string> nHibernateProperties;
        private string databaseServer;
        private string databaseName;
        private string avlDatabaseServer;
        private string avlDatabaseName;
        private int maxUpdateMaps;
        private int maxTimeSleepMaps;
        private string reportsDatabaseServer;
        private string reportsDatabaseName;
        private string telephonyServerType;
        private Dictionary<string, string> telephonyServerProperties;
        //private string telephonyServerIp;
        //private string telephonyServerPort;
        //private string telephonyServerIpBackup;
        //private string telephonyServerPortBackup;
        //private string telephonyServiceName;
        //private string telephonyQueue;
        //private string telephonyServerLogin;
        //private string telephonyServerPassword;
        private bool virtualMode;
        private string vmsDllName;
        private string vmsServerIp;
        private string vmsEventsServerIp;
        private string vmsLogin;
        private string vmsPassword;
        private string language;
        private string vmsPort;
        private string mapDll;
        private double mapLon;
        private double mapLat;
        private string mapName;

        #endregion

        #region Properties

        public string Extension { get; set; }
        public string ExtensionPassword { get; set; }
        public string DevicePort { get; set; }

        public int MaxTimeSleepMaps
        {
            get 
            { 
                return maxTimeSleepMaps; 
            }
            set 
            { 
                maxTimeSleepMaps = value; 
            }
        }
	
	

        public int MaxUpdateMaps
        {
            get 
            { 
                return maxUpdateMaps; 
            }
            set 
            { 
                maxUpdateMaps = value; 
            }
        }
	

        public IDictionary<string, string> NHibernateProperties
        {
            get
            {
                return nHibernateProperties;
            }
            set
            {
                nHibernateProperties = value;
            }
        }

        public string DatabaseServer
        {
            get
            {
                return databaseServer;
            }
            set
            {
                databaseServer = value;
            }
        }

        public string DatabaseName
        {
            get
            {
                return databaseName;
            }
            set
            {
                databaseName = value;
            }
        }

        public string AvlDatabaseServer
        {
            get
            {
                return avlDatabaseServer;
            }
            set
            {
                avlDatabaseServer = value;
            }
        }

        public string AvlDatabaseName
        {
            get
            {
                return avlDatabaseName;
            }
            set
            {
                avlDatabaseName = value;
            }
        }

        public string ReportsDatabaseServer
        {
            get
            {
                return reportsDatabaseServer;
            }
            set
            {
                reportsDatabaseServer = value;
            }
        }

        public string ReportsDatabaseName
        {
            get
            {
                return reportsDatabaseName;
            }
            set
            {
                reportsDatabaseName = value;
            }
        }

        public string TelephonyServerType
        {
            get
            {
                return telephonyServerType;
            }
            set
            {
                telephonyServerType = value;
            }
        }

        public Dictionary<string,string> TelephonyServerProperties
        {
            get
            {
                return telephonyServerProperties;
            }
            set
            {
                telephonyServerProperties = value;
            }
        }

        //public string TelephonyServerIp
        //{
        //    get
        //    {
        //        return telephonyServerIp;
        //    }
        //    set
        //    {
        //        telephonyServerIp = value;
        //    }
        //}

        //public string TelephonyServerPort
        //{
        //    get
        //    {
        //        return telephonyServerPort;
        //    }
        //    set
        //    {
        //        telephonyServerPort = value;
        //    }
        //}

        //public string TelephonyServerIpBackup
        //{
        //    get
        //    {
        //        return telephonyServerIpBackup;
        //    }
        //    set
        //    {
        //        telephonyServerIpBackup = value;
        //    }
        //}

        //public string TelephonyServerPortBackup
        //{
        //    get
        //    {
        //        return telephonyServerPortBackup;
        //    }
        //    set
        //    {
        //        telephonyServerPortBackup = value;
        //    }
        //}

        //public string TelephonyServiceName
        //{
        //    get
        //    {
        //        return telephonyServiceName;
        //    }
        //    set
        //    {
        //        telephonyServiceName = value;
        //    }
        //}

        //public string TelephonyQueue
        //{
        //    get
        //    {
        //        return telephonyQueue;
        //    }
        //    set
        //    {
        //        telephonyQueue = value;
        //    }
        //}

        //public string TelephonyServerLogin
        //{
        //    get
        //    {
        //        return telephonyServerLogin;
        //    }
        //    set
        //    {
        //        telephonyServerLogin = value;
        //    }
        //}

        //public string TelephonyServerPassword
        //{
        //    get
        //    {
        //        return telephonyServerPassword;
        //    }
        //    set
        //    {
        //        telephonyServerPassword = value;
        //    }
        //}

        public bool VirtualMode
        {
            get
            {
                return virtualMode;
            }
            set
            {
                virtualMode = value;
            }
        }

        public string VmsDllName
        {
            get
            {
                return vmsDllName;
            }
            set
            {
                vmsDllName = value;
            }
        }

        public string VmsServerIp
        {
            get
            {
                return vmsServerIp;
            }
            set
            {
                vmsServerIp = value;
            }
        }

        public string VmsLogin
        {
            get
            {
                return vmsLogin;
            }
            set
            {
                vmsLogin = value;
            }
        }

        public string VmsPort
        {
            get
            {
                return vmsPort;
            }
            set
            {
                vmsPort = value;
            }
        }


        public string VmsPassword
        {
            get
            {
                return vmsPassword;
            }
            set
            {
                vmsPassword = value;
            }
        }

        public string VmsEventsServerIp
        {
            get
            {
                return vmsEventsServerIp;
            }
            set
            {
                vmsEventsServerIp = value;
            }
        }

        public string Language
        {
            get
            {
                return language;
            }
            set
            {
                language = value;
            }
        }

        public String MapDLL
        {
            get
            {
                return mapDll;
            }
            set
            {
                mapDll = value;
            }
        }

        public double MapLon
        {
            get
            {
                return mapLon;
            }
            set
            {
                mapLon = value;
            }
        }

        public double MapLat
        {
            get
            {
                return mapLat;
            }
            set
            {
                mapLat = value;
            }
        }

        public string MapName
        {
            get
            {
                return mapName;
            }
            set
            {
                mapName = value;
            }
        }

        #endregion
    }
}
