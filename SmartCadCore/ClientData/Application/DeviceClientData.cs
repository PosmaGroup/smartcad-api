using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]    
    public class DeviceClientData : ClientData
    {

        private string ip;
        private string port;
        private string ipServer;
        private string login;
        private string password;
        private string name;
        //private StructClientData structClientData;
       
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                if (value != "")
                    name = value;
            }
        }
       
        public string Port
        {
            get
            {
                return port;
            }
            set
            {
                if (value != "")
                    port = value;
            }
        }
      
        public string IpServer
        {
            get
            {
                return ipServer;
            }
            set
            {
                ipServer = value;
            }
        }



        public string Ip
        {
            get
            {
                return ip;
            }
            set
            {
                ip = value;
            }
        }


        
        public string Login
        {
            get
            {
                return login;
            }
            set
            {
                if (value != "")
                    login = value;
            }
        }
        
        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                if (value != "")
                    password = value;
            }
        }



        //public StructClientData StructClientData
        //{
        //    get
        //    {
        //        return structClientData;
        //    }
        //    set
        //    {
        //        structClientData = value;
        //    }
        //}




        
        [InitialDataClient(PropertyName = "Name", PropertyValue = "DeviceData")]
        public static readonly UserResourceClientData Resource;
        public override string ToString()
        {
            return name;
        }
        public override bool Equals(object obj)
        {
            try
            {
                if (obj.GetType() == typeof(DeviceClientData))
                {
                    DeviceClientData cam = (DeviceClientData)obj;
                    if (this.Name == cam.Name)
                    {
                        return true;
                    }
                }
            }
            catch { }
            return false;
        }
    }

}
