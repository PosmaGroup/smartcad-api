﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadCore.ClientData
{
    #region Interface INamedObjectClientData Documentation
    /// <summary>
    /// This interface must be implemented for that classes that owns a custom code
    /// that allows to uniquely identify an instance of this class.
    /// </summary>
    /// <className>INamedObjectClientData</className>
    /// <author email="arturo.aguero@smartmatic.com">Arturo Agüero</author>
    /// <date>2009/22/05</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    public interface INamedObjectClientData
    {
        string Name
        {
            get;
            set;
        }
    }
}
