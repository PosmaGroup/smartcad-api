﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class GPSPinClientData : ClientData
    {
        
        public enum PinType
        {
            Input = 0,
            Output = 1,
            Input_Output = 2
        }

        public int Number
        {
            get;
            set;
        }

        public PinType Type
        {
            get;
            set;
        }

        public GPSTypeClientData GPSType
        {
            get;
            set;

        }

    }
}
