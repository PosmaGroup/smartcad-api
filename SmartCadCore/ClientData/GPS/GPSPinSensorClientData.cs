﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class GPSPinSensorClientData: ClientData
    {
        #region Properties

        public int SensorCode
        {
            get;
            set;
        }

        public string SensorName
        {
            get;
            set;
        }

        public int PinCode
        {
            get;
            set;
        }

        public int GPSCode
        {
            get;
            set;
        }

		public int PinNumber { get; set; }

        #endregion


        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GPSPinSensorClientData)
            {
                result = this.Code == ((GPSPinSensorClientData)obj).Code;
            }
            return result;
        }
    }
}
