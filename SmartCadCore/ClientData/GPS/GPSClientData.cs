﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class GPSClientData: DeviceClientData
    {
        public GPSClientData()
        {
        }

        public GPSClientData(string frame)
        {
            string[] data = frame.Split('|');

            this.Name = data[1];
            this.CoordinatesDate = DateTime.ParseExact(data[2], DateNHibernateFormatWithSeconds, System.Globalization.CultureInfo.InvariantCulture);
            int i; double d;
            this.Lon = double.Parse(data[3], new System.Globalization.CultureInfo("es-VE"));
            this.Lat = double.Parse(data[4], new System.Globalization.CultureInfo("es-VE"));
           
            this.Alt = int.TryParse(data[5], out i) ? int.Parse(data[5]) : 0;
            this.Speed = double.TryParse(data[6],out d) ? double.Parse(data[6]) : 0.0;
            this.Heading = double.TryParse(data[7],out d) ? double.Parse(data[7]) : 0.0;
            this.Satellites = int.TryParse(data[8], out i) ? int.Parse(data[8]) : 0;
        }

        public int ParentCode
        {
            get;
            set;
        }

        public GPSTypeClientData Type
        {
            get;
            set;
        }

        public IList Sensors
        {
            get;
            set;
        }

        public int UnitCode
        {
            get;
            set;
        }

        public double Lon
        {
            get;
            set;
        }

        public double Lat
        {
            get;
            set;
        }

        public int Alt
        {
            get;
            set;
        }

        public double Heading
        {
            get;
            set;
        }

        public double Speed
        {
            get;
            set;
        }

        public double Satellites
        {
            get;
            set;
        }

        public DateTime CoordinatesDate
        {
            get;
            set;
        }

        public bool IsSynchronized
        {
            get;
            set;
        }

        public string UnitCustomCode
        {
            get;
            set;
        }

        public override string ToString()
        {
            if (this.Name != null)
                return this.Name;
            else
                return string.Empty;
        }

        public override bool Equals(object obj)
        {
            try
            {
                if (obj.GetType() == typeof(GPSClientData))
                {
                    GPSClientData gps = (GPSClientData)obj;
                    if (this.Code == gps.Code)
                    {
                        return true;
                    }
                }
            }
            catch { }
            return false;
        }

        #region Initial Data
        [InitialDataClient(PropertyName = "Name", PropertyValue = "GPSData")]
        public static readonly UserResourceClientData Resource;
        #endregion
    }
}
