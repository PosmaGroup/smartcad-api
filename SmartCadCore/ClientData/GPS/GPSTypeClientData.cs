﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class GPSTypeClientData : ClientData
    {
        public string Brand
        {
            get;
            set;
        }

        public string Model
        {
            get;
            set;
        }

        public IList Pines
        {
            get;
            set;
        }

        public override string ToString()
        {
            return this.Model;
        }

        public override bool Equals(object obj)
        {
            try
            {
                if (obj.GetType() == typeof(GPSTypeClientData))
                {
                    GPSTypeClientData gpsType = (GPSTypeClientData)obj;
                    if (this.Code == gpsType.Code)
                    {
                        return true;
                    }
                }
            }
            catch { }
            return false;
        }

    }
}
