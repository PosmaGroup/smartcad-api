﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace SmartCadCore.ClientData
{
    [Serializable]
    public class IndicatorClassClientData : ClientData
    {
        private string name;
        private string friendlyName;
        private string description;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string FriendlyName
        {
            get
            {
                return friendlyName;
            }
            set
            {
                friendlyName = value;
            }
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is IndicatorClassClientData)
            {
                result = this.Code == ((IndicatorClassClientData)obj).Code;
            }
            return result;
        }

        [InitialDataClient(PropertyName = "Name", PropertyValue = "SYSTEM")]
        public static IndicatorClassClientData System;

        [InitialDataClient(PropertyName = "Name", PropertyValue = "GROUP")]
        public static IndicatorClassClientData Group;

        [InitialDataClient(PropertyName = "Name", PropertyValue = "DEPARTMENT")]
        public static IndicatorClassClientData Department;

        [InitialDataClient(PropertyName = "Name", PropertyValue = "OPERATOR")]
        public static IndicatorClassClientData Operator;
    }
}
