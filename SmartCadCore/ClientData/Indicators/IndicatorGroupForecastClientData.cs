using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class IndicatorGroupForecastClientData : ClientData, ICloneable
    {
        private string name;
        private string description;
        private bool general;
        private DateTime start;
        private DateTime end;
        private IList forecastValues;
        private bool factory;

        public bool FactoryData 
        { 
            get
            {
                return factory;
            }
            set
            {
                factory = value;
            }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public bool General
        {
            get { return general; }
            set { general = value; }
        }

        public DateTime Start
        {
            get { return start; }
            set { start = value; }
        }

        public DateTime End
        {
            get { return end; }
            set { end = value; }
        }

        public IList ForecastValues
        {
            get { return forecastValues; }
            set { forecastValues = value; }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is IndicatorGroupForecastClientData)
            {
                IndicatorGroupForecastClientData data = (IndicatorGroupForecastClientData)obj;
                result = this.Name == data.Name &&
                         this.General == data.General &&
                         this.Start == data.Start &&
                         this.End == data.End;
            }
            return result;
        }

        #region ICloneable Members

        public object Clone()
        {
            IndicatorGroupForecastClientData clone = new IndicatorGroupForecastClientData();
            clone.Description = this.Description;
            clone.End = this.End;
            if (this.ForecastValues != null)
            {
                clone.ForecastValues = new ArrayList();
                foreach (IndicatorForecastClientData item in this.ForecastValues)
                {
                    clone.forecastValues.Add(item.Clone());
                }
            }
            clone.General = this.General;
            clone.FactoryData = this.FactoryData;
            clone.Name = this.Name;
            clone.Start = this.Start;
            return clone;
        }

        #endregion
    }
}
