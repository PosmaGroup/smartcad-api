using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class IndicatorForecastClientData : ClientData, ICloneable
    {
        private int low;
        private int high;
        private string pattern;
        private int positivePattern;
        private IndicatorClientData indicator;
        private int indicatorGroupForecastCode;
        private DepartmentTypeClientData departmentType;

        public int Low
        {
            get { return low; }
            set { low = value; }
        }

        public int High
        {
            get { return high; }
            set { high = value; }
        }

        public string Pattern
        {
            get { return pattern; }
            set { pattern = value; }
        }

        public int PositivePattern
        {
            get { return positivePattern; }
            set { positivePattern = value; }
        }

        public IndicatorClientData Indicator
        {
            get { return indicator; }
            set { indicator = value; }
        }

        public int IndicatorGroupForecastCode
        {
            get { return indicatorGroupForecastCode; }
            set { indicatorGroupForecastCode = value; }
        }

        public DepartmentTypeClientData DepartmentType
        {
            get { return departmentType; }
            set { departmentType = value; }
        }

        public bool Threshold()
        {
            return Low == 0 && High == 0;
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is IndicatorForecastClientData)
            {
                IndicatorForecastClientData compare = (IndicatorForecastClientData)obj;
                if (DepartmentType == null)
                {
                    result = this.Indicator == compare.Indicator &&
                             this.IndicatorGroupForecastCode == compare.IndicatorGroupForecastCode;
                }
                else
                {
                    result = this.Indicator == compare.Indicator &&
                             this.IndicatorGroupForecastCode == compare.IndicatorGroupForecastCode &&
                             this.DepartmentType == compare.DepartmentType;
                }
            }
            return result;
        }

        #region ICloneable Members

        public object Clone()
        {
            IndicatorForecastClientData clone = new IndicatorForecastClientData();
            if (this.DepartmentType != null)
                clone.DepartmentType = this.DepartmentType;
            clone.High = this.High;
            clone.Indicator = this.Indicator;
            clone.Low = this.Low;
            clone.Pattern = this.Pattern;
            clone.PositivePattern = this.PositivePattern;
            return clone;
        }

        #endregion
    }
}
