using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class IndicatorClientData : ClientData
    {
        private string name;
        private string mnemonic;
        private string description;
        private string measureUnit;
        private bool dashboard;
        private bool showDashboard;
        private IList classes;
        private string type;
        private string typeName;
        private int frecuency;
        private string customCode;

        public string CustomCode
        {
            get { return customCode; }
            set { customCode = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Mnemonic
        {
            get { return mnemonic; }
            set { mnemonic = value; }
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public string MeasureUnit
        {
            get { return measureUnit; }
            set { measureUnit = value; }
        }

        public bool Dashboard
        {
            get { return dashboard; }
            set { dashboard = value; }
        }

        public bool ShowDashboard
        {
            get { return showDashboard; }
            set { showDashboard = value; }
        }

        public IList Classes
        {
            get { return classes; }
            set { classes = value; }
        }

        public string Type
        {
            get { return type; }
            set { type = value; }
        }

        public string TypeName
        {
            get { return typeName; }
            set { typeName = value; }
        }

        public int Frecuency
        {
            get { return frecuency; }
            set { frecuency = value; }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is IndicatorClientData)
            {
                result = this.customCode == ((IndicatorClientData)obj).customCode;
            }
            return result;
        }

        public override string ToString()
        {
            return this.Name;
        }
    }
}
