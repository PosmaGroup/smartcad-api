﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class AlarmQueryTwitterClientData: ClientData
    {

        public AlarmQueryTwitterClientData()
        {
            Type = 1;
        }
        
        public string Name
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public string TwitterQuery
        {
            get;
            set;
        }

        public int TweetsAmount
        {
            get;
            set;
        }

        public int TimeInterval
        {
            get;
            set;
        }

        public int Type
        {
            get;
            set;
        }

        [InitialDataClient(PropertyName = "Name", PropertyValue = "AlarmQueryTwitterData")]
        public static readonly UserResourceClientData Resource;

        public override string ToString()
        {
            return this.Name;
        }
        public override bool Equals(object obj)
        {
            try
            {
                if (obj.GetType() == typeof(AlarmQueryTwitterClientData))
                {
                    AlarmQueryTwitterClientData str = (AlarmQueryTwitterClientData)obj;
                    if (this.Code == str.Code)
                    {
                        return true;
                    }
                }
            }
            catch { }
            return false;
        }
    }

}

