using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class TwitterReportAnswerClientData : ClientData
    {
        #region Fields

        private int questionCode;
        private IList answers = new ArrayList();
        private int reportCode;
        private string questionText;
        
		#endregion

        #region Contructors

        public TwitterReportAnswerClientData()
        { 
		
		}

        public TwitterReportAnswerClientData(int questionCode, QuestionPossibleAnswerClientData answer, string answerText)
        {
            this.questionCode = questionCode;
            
			PossibleAnswerAnswerClientData paacd = new PossibleAnswerAnswerClientData();
            paacd.TextAnswer = answerText;
            paacd.ReportAnswerCode = this.Code;
            paacd.QuestionPossibleAnswer = answer;

            this.answers.Add(paacd);
        }

        public TwitterReportAnswerClientData(int questionCode, string answer)
        {
            this.questionCode = questionCode;
        }

        #endregion

        #region Properties

        public int QuestionCode
        {
            get
            {
                return this.questionCode;
            }
            set
            {
                this.questionCode = value;
            }
        }

        public string QuestionText
        {
            get
            {
                return this.questionText;
            }
            set
            {
                this.questionText = value;
            }
        }

        public IList Answers
        {
            get
            {
                return this.answers;
            }
            set
            {
                this.answers = value;
            }
        }

        public int ReportCode
        {
            get
            {
                return this.reportCode;
            }
            set
            {
                this.reportCode = value;
            }
        }

        #endregion

		public void UpdateAnswers(TwitterReportAnswerClientData pracd,bool deleting)
		{
			foreach (PossibleAnswerAnswerClientData paacd in pracd.Answers)
			{
				int index = this.answers.IndexOf(paacd);
				if (index >= 0)
					if (deleting == false)
						((PossibleAnswerAnswerClientData)this.answers[index]).TextAnswer = paacd.TextAnswer;
					else
						this.answers.RemoveAt(index);
				else if (deleting == false)
					this.answers.Add(paacd);
			}
		}
    }
}
