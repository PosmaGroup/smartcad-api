﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class AlarmTwitterClientData: ClientData
    {

        public enum AlarmStatus
        {
            NotTaken = 0,
            InProcess = 1,
            Ended = 2
        }

        public AlarmTwitterClientData()
        {
        }


        public AlarmQueryTwitterClientData Query
        {
            get;
            set;
        }

        public DateTime StartAlarm
        {
            get;
            set;
        }

        public DateTime? EndAlarm
        {
            get;
            set;
        }

        public AlarmStatus Status
        {
            get;
            set;
        }

        public List<TweetClientData> Tweets
        {
            get;
            set;
        }

    }

}

