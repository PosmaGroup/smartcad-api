﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using SmartCadCore.Common;
using SmartCadCore.Core;

namespace SmartCadCore.Telephony
{
    public abstract class TelephonyManagerAbstract
    {
        //public event EventHandler<IncomingCallEventArgs> IncomingCall;
        //public event EventHandler<EstablishedCallEventArgs> EstablishedCall;
        //public event EventHandler<EndingCallEventArgs> EndingCall;
        
        public event EventHandler<ReadyStatusChangedEventArgs> ReadyStatusChanged;
        public event EventHandler<DoNotDisturbChangedEventArgs> DoNotDisturbChanged;
       
        public event EventHandler<AgentLoginEventArgs> AgentLogin;
        public event EventHandler<AgentLogoutEventArgs> AgentLogout;
        public event EventHandler<CallAbandonedEventArgs> CallAbandoned;
        public event EventHandler<AgentReadyEventArgs> AgentReady;//Maybe ReadyStatusChanged
        public event EventHandler<AgentNotReadyEventArgs> AgentNotReady;
        public event EventHandler<CallDialingEventArgs> CallDialing;
        public event EventHandler<TelephonyErrorEventArgs> OnError;
        public event EventHandler<CallEstablishedEventArgs> CallEstablished;//EstablishedCall
        public event EventHandler<DigitsReceivedEventArgs> DigitsReceive;//Received DTMF tones.
        public event EventHandler<CallMadeEventArgs> CallMade;//Answer call by external party.
        public event EventHandler<CallHeldEventArgs> CallHeld;
        public event EventHandler<AgentRegisteredEventArgs> AgentRegistered;
        public event EventHandler<TerminalUnRegisteredEventArgs> TerminalUnregistered; //someone else took the control
        public event EventHandler<CallReleasedEventArgs> CallReleased;//Ending call
        public event EventHandler<CallRetrievedEventArgs> CallRetrieved;
        public event EventHandler<CallRingingEventArgs> CallRinging;//Maybe incoming call
        public event EventHandler<CallRecordedEventArgs> CallRecorded;

       
        protected string Extension
        {
            get { return configurationProperties[CommonProperties.Extension].ToString(); }
        }

        protected string ExtensionPassword
        {
            get { return configurationProperties[CommonProperties.ExtensionPassword].ToString(); }
        }

        protected string AgentId
        {
            get { return configurationProperties[CommonProperties.AgentId].ToString(); }
        }

        protected string AgentPassword
        {
            get { return configurationProperties[CommonProperties.AgentPassword].ToString(); }
        }

        protected string UserLogin
        {
            get { return configurationProperties[CommonProperties.UserLogin].ToString(); }
        }

        protected string UserPassword
        {
            get { return configurationProperties[CommonProperties.UserPassword].ToString(); }
        }

        protected string Ready
        {
            get { return configurationProperties[CommonProperties.Ready].ToString(); }
        }

        protected string VirtualMode
        {
            get { return configurationProperties[CommonProperties.VirtualMode].ToString(); }
        }

        protected string ServerType
        {
            get { return configurationProperties[CommonProperties.ServerType].ToString(); }
        }

        protected void SendTelephonyEventArgs(EventArgs args)
        {
            //Incoming call or call ringing
            if (args.GetType() == typeof(CallRingingEventArgs))
            {
                if (CallRinging != null)
                    CallRinging(null, (CallRingingEventArgs)args);
            }
            else if (args.GetType() == typeof(CallEstablishedEventArgs))
            {
                if (CallEstablished != null)
                    CallEstablished(null, (CallEstablishedEventArgs)args);
            }
            else if (args.GetType() == typeof(CallReleasedEventArgs))
            {
                if (CallReleased != null)
                    CallReleased(null, (CallReleasedEventArgs)args);
            }
            else if (args.GetType() == typeof(ReadyStatusChangedEventArgs))
            {
                if (ReadyStatusChanged != null)
                    ReadyStatusChanged(null, (ReadyStatusChangedEventArgs) args);
            }
            else if (args.GetType() == typeof(DoNotDisturbChangedEventArgs))
            {
                if (DoNotDisturbChanged != null)
                    DoNotDisturbChanged(null, (DoNotDisturbChangedEventArgs) args);
            }
            else if (args.GetType() == typeof(AgentLoginEventArgs))
            {
                if (AgentLogin != null)
                    AgentLogin(null, (AgentLoginEventArgs)args);
            }
            else if (args.GetType() == typeof(AgentLogoutEventArgs))
            {
                if (AgentLogout != null)
                    AgentLogout(null, (AgentLogoutEventArgs)args);
            }
            else if (args.GetType() == typeof(CallAbandonedEventArgs))
            {
                if (CallAbandoned != null)
                    CallAbandoned(null, (CallAbandonedEventArgs)args);
            }
            else if (args.GetType() == typeof(AgentReadyEventArgs))
            {
                if (AgentReady != null)
                    AgentReady(null, (AgentReadyEventArgs)args);
            }
            else if (args.GetType() == typeof(AgentNotReadyEventArgs))
            {
                if (AgentNotReady != null)
                    AgentNotReady(null, (AgentNotReadyEventArgs)args);
            }
            else if (args.GetType() == typeof(CallDialingEventArgs))
            {
                if (CallDialing != null)
                    CallDialing(null, (CallDialingEventArgs)args);
            }
            else if (args.GetType() == typeof(TelephonyErrorEventArgs))
            {
                if (OnError != null)
                    OnError(null, (TelephonyErrorEventArgs)args);
            }
            else if (args.GetType() == typeof(CallHeldEventArgs))
            {
                if (CallHeld != null)
                    CallHeld(null, (CallHeldEventArgs)args);
            }
            else if (args.GetType() == typeof(AgentRegisteredEventArgs))
            {
                if (AgentRegistered != null)
                    AgentRegistered(null, (AgentRegisteredEventArgs)args);
            }
            else if (args.GetType() == typeof(TerminalUnRegisteredEventArgs))
            {
                if (TerminalUnregistered != null)
                    TerminalUnregistered(null, (TerminalUnRegisteredEventArgs)args);
            }
            else if (args.GetType() == typeof(CallRetrievedEventArgs))
            {
                if (CallRetrieved != null)
                    CallRetrieved(null, (CallRetrievedEventArgs)args);
            }
            else if (args.GetType() == typeof(CallMadeEventArgs))
            {
                if (CallMade != null)
                    CallMade(null, (CallMadeEventArgs)args);
            }
            else if (args.GetType() == typeof(DigitsReceivedEventArgs))
            {
                if (DigitsReceive != null)
                    DigitsReceive(null, (DigitsReceivedEventArgs)args);
            }
            else if (args.GetType() == typeof(CallRecordedEventArgs))
            {
                if (CallRecorded != null)
                    CallRecorded(null, (CallRecordedEventArgs)args);
            }   
        }

        public abstract string TestCTIState();

        protected void OnDoNotDisturbChanged(DoNotDisturbChangedEventArgs e)
        {
            if (DoNotDisturbChanged != null)
            {
                SmartLogger.Print("Invoking");
                DoNotDisturbChanged(this, e);
            }
        }

        protected void OnAgentLogin(AgentLoginEventArgs e)
        {
            if (AgentLogin != null)
            {
                SmartLogger.Print("Invoking");
                AgentLogin(this, e);
            }
        }

        protected void OnAgentLogout(AgentLogoutEventArgs e)
        {
            if (AgentLogout != null)
            {
                SmartLogger.Print("Invoking");
                AgentLogout(this, e);
            }
        }

        protected void OnCallAbandoned(CallAbandonedEventArgs e)
        {
            if (CallAbandoned != null)
            {
                SmartLogger.Print("Invoking");
                CallAbandoned(this, e);
            }
        }

        protected void OnAgentReady(AgentReadyEventArgs e)
        {
            if (ReadyStatusChanged != null)
            {
                ReadyStatusChanged(this, new ReadyStatusChangedEventArgs(true, e.Reason));
            }
        }

        protected void OnAgentNotReady(AgentNotReadyEventArgs e)
        {
            if (ReadyStatusChanged != null)
            {
                ReadyStatusChanged(this, new ReadyStatusChangedEventArgs(false, e.Reason));
            }
        }

        protected void OnCallDialing(CallDialingEventArgs e)
        {
            if (CallDialing != null)
            {
                SmartLogger.Print("Invoking");
                CallDialing(this, e);
            }
        }

        protected void OnTelephonyError(TelephonyErrorEventArgs e)
        {
            if (OnError != null)
            {
                SmartLogger.Print("Invoking");
                OnError(this, e);
            }
        }

        protected void OnCallEstablished(CallEstablishedEventArgs e)
        {
            if (CallEstablished != null)
            {
                SmartLogger.Print("Invoking");
                CallEstablished(this, e);
            }
        }

        /// <summary>
        /// After receiving the confirmation of the sented DTFM tones this event will be raised.
        /// </summary>
        /// <param name="e"></param>
        protected void OnDigitsReceived(DigitsReceivedEventArgs e)
        {
            if (DigitsReceive != null)
            {
                SmartLogger.Print("DigitsReceived");
                DigitsReceive(this, e);
            }
        }

        /// <summary>
        /// A call has been answer by an external party.
        /// </summary>
        /// <param name="e"></param>
        protected void OnCallMade(CallMadeEventArgs e)
        {
            if (CallMade != null)
            {
                SmartLogger.Print("CallMade");
                CallMade(this, e);
            }
        }

        protected void OnCallHeld(CallHeldEventArgs e)
        {
            if (CallHeld != null)
            {
                SmartLogger.Print("Invoking");
                CallHeld(this, e);
            }
        }

        protected void OnAgentRegistered(AgentRegisteredEventArgs e)
        {
            if (AgentRegistered != null)
            {
                SmartLogger.Print("Invoking");
                AgentRegistered(this, e);
            }
        }

        protected void OnCallReleased(CallReleasedEventArgs e)
        {
            if (CallReleased != null)
            {
                SmartLogger.Print("Invoking");
                CallReleased(this, e);
            }
        }

        protected void OnCallRetrieved(CallRetrievedEventArgs e)
        {
            if (CallRetrieved != null)
            {
                SmartLogger.Print("Invoking");
                CallRetrieved(this, e);
            }
        }

        protected void OnCallRinging(CallRingingEventArgs e)
        {
            if (CallRinging != null)
            {
                SmartLogger.Print("Invoking");
                CallRinging(this, e);
            }
        }

        protected void OnTerminalUnregistered(TerminalUnRegisteredEventArgs e)
        {
            if (TerminalUnregistered != null)
            {
                SmartLogger.Print("Invoking");
                TerminalUnregistered(this, e);
            }
        }

        protected void OnCallRecorded(CallRecordedEventArgs e)
        {
            if (CallRecorded != null)
            {
                SmartLogger.Print("Invoking");
                CallRecorded(this, e);
            }
        }

        protected Hashtable configurationProperties;

        public TelephonyManagerAbstract(Hashtable configProperties)
        {
            this.configurationProperties = configProperties;
        }

        protected abstract void Init();

        public abstract void Login(string agentId, string agentPassword, bool isReady);

        public abstract void Logout();

        public abstract void Hold();

        public abstract void SetReadyStatus(bool ready);

        public abstract bool GetReadyStatus();

        public abstract void Answer();

        public abstract void Release();

        public abstract void Transfer();

        public abstract void Call(string number);

        /// <summary>
        /// Puts the current call in parking state, waiting for a dispatcher to re-take it.
        /// </summary>
        /// <param name="extensionParking">Extension number where call will be parked(queue number)</param>
        /// <param name="parkId">call id used to park and unpark the call</param>
        public abstract void ParkCall(string extensionParking, string parkId);

        public abstract void Conference();

        public abstract void Close();

        public abstract void RecoverConnection();

        public abstract void SetPhoneReport(string customCode);
    }
}
