﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using SmartCadCore.Core;
using SmartCadCore.Common;
using System.Reflection;
using System.IO;

namespace SmartCadCore.Telephony
{
    public class TelephonyFactory
    {
        private static TelephonyManagerAbstract telephonyManager;
        private static TelephonyServerTypes selectedFactory = TelephonyServerTypes.None;
        private static Hashtable configurationProperties;

        private static Type TelephonyManagerType { get; set; }
        private static Type ControlType { get; set; }


        public static void SetConfiguration(Hashtable properties)
        {
            try
            {
                if (telephonyManager != null)
                {
                    telephonyManager.Close();
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
            finally
            {
                telephonyManager = null;
            }
            configurationProperties = properties;
        }

        public static TelephonyManagerAbstract GetTelephonyManager()
        {
            if (telephonyManager == null)
            {
                if (configurationProperties != null)
                {
                    string assemblyName = Assembly.GetExecutingAssembly().GetName().Name + "." +
                        configurationProperties[CommonProperties.ServerType] + ".dll";
                    telephonyManager = GetInstance(assemblyName);
                }
                else
                {
                    throw new Exception("Factory was not configured");
                }
            }
            return telephonyManager;
        }

        public static IConfigurationControl GetConfigurationControl(string assemblyName)
        {
            try
            {
                ControlType = null;

                Assembly assembly = Assembly.LoadFrom(
                    Assembly.GetExecutingAssembly().GetName().Name + "." + assemblyName + ".dll");

                Type[] types = assembly.GetTypes();

                foreach (Type type in types)
                {
                    if (type.IsClass == true
                        && type.IsAbstract == false
                        && type.GetInterface("IConfigurationControl") != null)
                    {
                        ControlType = type;
                        break;
                    }
                }

                if (ControlType != null)
                {
                    return (IConfigurationControl)ControlType.GetConstructor(new Type[0] { }).Invoke(new object[0] { });
                }
                else
                    throw new Exception("Missing file " + assemblyName);
            }
            catch (Exception e)
            {
                SmartLogger.Print(e);
                return null;
            }
        }  

        public static TelephonyManagerAbstract GetInstance(string assemblyName)
        {
            try
            {
                //SmartLogger.Print("Inició el GetInstance");
                //Console.WriteLine(Environment.CurrentDirectory);
                if (TelephonyManagerType == null)
                {
                    Assembly assembly = Assembly.LoadFrom(assemblyName);

                    Type[] types = assembly.GetTypes();

                    foreach (Type type in types)
                    {
                        if (type.IsClass == true
                            && type.IsAbstract == false
                            && type.IsSubclassOf(typeof(TelephonyManagerAbstract)))
                        {
                            TelephonyManagerType = type;
                            break;
                        }
                    }
                }

                if (TelephonyManagerType != null)
                {
                    Type[] types = new Type[1];
                    types[0] = typeof(Hashtable);

                    object[] parameters = new object[1];
                    parameters[0] = configurationProperties;

                    return (TelephonyManagerAbstract)TelephonyManagerType.GetConstructor(types).Invoke(parameters);
                }
                else
                    throw new Exception("Missing file " + assemblyName);
            }
            catch (Exception e)
            {
                SmartLogger.Print(e);
                //return null;
                if (e.InnerException != null)
                    throw e.InnerException;
                else
                    throw e;
            }
        }

        public static string[] GetAvailableTypes()
        {
            try
            {
                string assemblyName = Assembly.GetExecutingAssembly().GetName().Name;
                string[] files = Directory.GetFiles(Environment.CurrentDirectory,
                    assemblyName + ".*.dll", SearchOption.TopDirectoryOnly);

                for (int i = 0; i < files.Length; i++)
                {
                    files[i] = files[i].Replace(Environment.CurrentDirectory + "\\"+ assemblyName + ".", "").Replace(".dll", "");
                }
                return files;
            }
            catch (Exception e)
            {
                SmartLogger.Print(e);
                return null;
            }
        }
    }
}
