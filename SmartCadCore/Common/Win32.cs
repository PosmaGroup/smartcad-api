using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Collections;

namespace SmartCadCore.Common
{
	public enum NotifyCommand
	{
		Add = 0,
		Modify = 1,
		Delete = 2,
		SetFocus = 3,
		SetVersion = 4
	}

	public enum NotifyFlags
	{
		Message = 1,
		Icon = 2,
		Tip = 4,
		State = 8,
		Info = 16,
		Guid = 32
	}

	public delegate Int32 EnumThreadWndProc(IntPtr hWnd, UInt32 lParam);

	#region Structures

	public struct MIB_IPADDRROW
	{
		public uint dwAddr;
		public int dwIndex;
		public int dwMask;
		public int dwBCastAddr;
		public int dwReasmSize;
		public short unused1;
		public short wType;
	}

	[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
	public struct NETRESOURCE
	{
		public int Scope;
		public int Type;
		public int DisplayType;
		public int Usage;
		public string LocalName;
		public string RemoteName;
		public string Comment;
		public string Provider;
	}

	public struct KBDLLHOOKSTRUCT
	{
		public int vkCode;
		public int scanCode;
		public int flags;
		public int time;
		public int dwExtraInfo;
	}

	[StructLayout(LayoutKind.Sequential)]
	public struct NOTIFYICONDATA
	{
		public int cbSize;
		public IntPtr hWnd;
		public int uID;
		public int uFlags;
		public int uCallbackMessage;
		public IntPtr hIcon;
		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)] public string szTip;
		public int dwState;
		public int dwStateMask;
		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)] public string szInfo;
		public int uTimeoutOruVersion;
		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)] public string szInfoTitle;
		public int dwInfoFlags;
		//public Guid guidItem;
	}

	public struct CAPTUREPARAMS
	{
		public int dwRequestMicroSecPerFrame;
		public int fMakeUserHitOKToCapture;
		public uint wPercentDropForError;
		public int fYield;
		public int dwIndexSize;
		public uint wChunkGranularity;
		public int fUsingDOSMemory;
		public uint wNumVideoRequested;
		public int fCaptureAudio;
		public uint wNumAudioRequested;
		public uint vKeyAbort;
		public int fAbortLeftMouse;
		public int fAbortRightMouse;
		public int fLimitEnabled;
		public uint wTimeLimit;
		public int fMCIControl;
		public int fStepMCIDevice;
		public int dwMCIStartTime;
		public int dwMCIStopTime;
		public int fStepCaptureAt2x;
		public uint wStepCaptureAverageFrames;
		public int dwAudioBufferSize;
		public int fDisableWriteCache;
		public uint AVStreamMaster;
	}

	[StructLayout(LayoutKind.Sequential)]
	public struct NotifyIconData
	{
		public UInt32 cbSize; // DWORD
		public IntPtr hWnd; // HWND
		public UInt32 uID; // UINT
		public NotifyFlags uFlags; // UINT
		public UInt32 uCallbackMessage; // UINT
		public IntPtr hIcon; // HICON
		[MarshalAs(UnmanagedType.ByValTStr, SizeConst=128)] public String szTip; // char[128]
		public UInt32 dwState; // DWORD
		public UInt32 dwStateMask; // DWORD
		[MarshalAs(UnmanagedType.ByValTStr, SizeConst=256)] public String szInfo; // char[256]
		public UInt32 uTimeoutOrVersion; // UINT
		[MarshalAs(UnmanagedType.ByValTStr, SizeConst=64)] public String szInfoTitle; // char[64]
		public UInt32 dwInfoFlags; // DWORD
		//GUID guidItem; > IE 6
	}

	#endregion

	public delegate int HookProc(int nCode, int wParam, int lParam);

	internal delegate int BrowseCallBackProc(IntPtr hwnd, int msg, IntPtr lp, IntPtr wp);

	[StructLayout(LayoutKind.Sequential)]
	internal struct BrowseInfo
	{
		public IntPtr hwndOwner;
		public IntPtr pidlRoot;
		[MarshalAs(UnmanagedType.LPTStr)] public string displayname;
		[MarshalAs(UnmanagedType.LPTStr)] public string title;
		public int flags;
		[MarshalAs(UnmanagedType.FunctionPtr)] public BrowseCallBackProc callback;
		public IntPtr lparam;
	}

	[ComImport]
	[Guid("00000002-0000-0000-C000-000000000046")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	internal interface IMalloc
	{
		[PreserveSig]
		IntPtr Alloc(IntPtr cb);

		[PreserveSig]
		IntPtr Realloc(IntPtr pv, IntPtr cb);

		[PreserveSig]
		void Free(IntPtr pv);

		[PreserveSig]
		IntPtr GetSize(IntPtr pv);

		[PreserveSig]
		int DidAlloc(IntPtr pv);

		[PreserveSig]
		void HeapMinimize();
	}

    [StructLayout(LayoutKind.Sequential)]
    public struct RECT
    {
        public int left;
        public int top;
        public int right; 
        public int bottom;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct CHARRANGE
    {
        public int cpMin;         //First character of range (0 for start of doc)
        public int cpMax;           //Last character of range (-1 for end of doc)
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct FORMATRANGE
    {
        public IntPtr hdc;             //Actual DC to draw on
        public IntPtr hdcTarget;       //Target DC for determining text formatting
        public RECT rc;                //Region of the DC to draw to (in twips)
        public RECT rcPage;            //Region of the whole DC (page size) (in twips)
        public CHARRANGE chrg;         //Range of text to draw (see earlier declaration)
    }

	/// <summary>
	/// Wrapper de Win32 API.
	/// </summary>
	/// <remarks>
	/// Author: Alden Torres Cuon, atorres@smartmatic.com - Smartmatic Corp., Marzo 2004
	/// </remarks>
	public sealed class Win32
	{
		private Win32()
		{
		}

		#region Check Routines

		public static IntPtr Check(IntPtr HANDLE)
		{
			if (HANDLE == IntPtr.Zero)
			{
				throw new Win32Exception(Marshal.GetLastWin32Error());
			}

			return HANDLE;
		}

		public static bool Check(bool result)
		{
			if (result == false)
			{
				throw new Win32Exception(Marshal.GetLastWin32Error());
			}

			return result;
		}

		public static int Check(int result)
		{
			if (result != NO_ERROR)
			{
				throw new Win32Exception(Marshal.GetLastWin32Error());
			}

			return result;
		}

		#endregion

		#region IP Helper

		[DllImport("Iphlpapi.dll", SetLastError = true)]
		public static extern int NotifyAddrChange(IntPtr Handle, IntPtr overlapped);

		[DllImport("Iphlpapi.dll", SetLastError = true)]
		public static extern int GetIpAddrTable(IntPtr pIpAddrTable, ref int pdwSize, bool bOrder);

		#endregion

		#region Windows Networking

		public const int RESOURCETYPE_DISK = 1;
		public const int RESOURCE_GLOBALNET = 2;
		public const int RESOURCEDISPLAYTYPE_SERVER = 2;
		public const int RESOURCEDISPLAYTYPE_SHARE = 3;
		public const int RESOURCEUSAGE_CONNECTABLE = 1;

		public const int ERROR_ACCESS_DENIED = 0005;
		public const int ERROR_ALREADY_ASSIGNED = 0085;
		public const int ERROR_ALREADY_CONNECTED = 1219;

		[DllImport("MPR.Dll", SetLastError = true)]
		public static extern int WNetAddConnection2A(ref NETRESOURCE lpNetResource, [MarshalAs(UnmanagedType.LPStr)] string lpPassword, [MarshalAs(UnmanagedType.LPStr)] string lpUsername, int dwFlags);

		[DllImport("MPR.Dll", SetLastError = true)]
		public static extern int WNetCancelConnection2A([MarshalAs(UnmanagedType.LPStr)] string lpName, int dwFlags, bool fForce);

		#endregion

		#region Kernel

		[DllImport("kernel32.dll", SetLastError = true)]
		public static extern int FormatMessage(int dwFlags, ref IntPtr pMessageSource, int dwMessageID, int dwLanguageID, ref string lpBuffer, int nSize, ref IntPtr pArguments);

		[DllImport("kernel32.dll", SetLastError = true)]
		public static extern bool CloseHandle(IntPtr hObject);

		[DllImport("kernel32.dll", SetLastError = true)]
		public static extern void MoveMemory(IntPtr Destination, byte[] Source, int Length);

		[DllImport("kernel32.dll", SetLastError = true)]
		public static extern void MoveMemory(byte[] Destination, IntPtr Source, int Length);

		[DllImport("kernel32.dll", SetLastError = true)]
		public static extern void MoveMemory(IntPtr Destination, IntPtr Source, int Length);

		[DllImport("kernel32.dll")]
		public static extern int GetUserDefaultLCID();

		[DllImport("Kernel32.Dll")]
		public static extern UInt32 GetCurrentThreadId();

		#endregion

		#region User

		public const int SM_CXSCREEN = 0x0;
		public const int SM_CYSCREEN = 0x1;

		public const int WM_KEYDOWN = 0x0100;
		public const int WM_KEYUP = 0x0101;
		public const int WM_SYSKEYDOWN = 0x0104;
		public const int WM_SYSKEYUP = 0x0105;

		public const int WM_MOUSEMOVE = 0x0200;

		public const int WH_KEYBOARD_LL = 13;

        public const int SW_HIDE = 1;
        public const int SW_NORMAL = 2;
        public const int SW_SHOWMINIMIZED = 3;
        public const int SW_SHOWMAXIMIZED = 4;
        public const int SW_SHOWNOACTIVATE = 5;
        public const int SW_SHOW = 6;
        public const int SW_MINIMIZE = 7;
        public const int SW_SHOWMINNOACTIVE = 8;
        public const int SW_SHOWNA = 9;
        public const int SW_RESTORE = 10;
        public const int SW_SHOWDEFAULT = 11;
        public const int SW_MAXIMIZE = 4;
        public const int SW_SHOWNORMAL = 2;

        public const int LSFW_LOCK = 1;
        public const int LSFW_UNLOCK = 2;

		[DllImport("user32.dll", SetLastError = true)]
		public static extern IntPtr GetDC(IntPtr ptr);

		[DllImport("user32.dll", SetLastError = true)]
		public static extern int GetSystemMetrics(int nIndex);

		[DllImport("user32.dll", CharSet=CharSet.Auto, CallingConvention=CallingConvention.StdCall, SetLastError = true)]
		public static extern IntPtr SetWindowsHookEx(int idHook, HookProc lpfn, IntPtr hInstance, int threadId);

		[DllImport("user32.dll", CharSet=CharSet.Auto, CallingConvention=CallingConvention.StdCall, SetLastError = true)]
		public static extern bool UnhookWindowsHookEx(IntPtr hHook);

		[DllImport("user32.dll", EntryPoint="CallNextHookEx", CharSet=CharSet.Auto, SetLastError = true)]
		public static extern int CallNextHookEx(int hHook, int nCode, int wParam, int lParam);

		[DllImport("user32.dll", CharSet=CharSet.Auto, CallingConvention=CallingConvention.StdCall, SetLastError = true)]
		public static extern IntPtr GetWindowDC(IntPtr hWnd);

		[DllImport("user32.dll", CharSet=CharSet.Auto, CallingConvention=CallingConvention.StdCall, SetLastError = true)]
		public static extern IntPtr GetDesktopWindow();

		[DllImport("user32.dll", CharSet=CharSet.Auto, CallingConvention=CallingConvention.StdCall, SetLastError = true)]
		public static extern int ReleaseDC(IntPtr hWnd, IntPtr hDC);

		[DllImport("user32.dll", CharSet=CharSet.Auto, CallingConvention=CallingConvention.StdCall, SetLastError = true)]
		public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);

        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public extern static IntPtr GetDlgItem(IntPtr hDlg, int nControlID);

        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public extern static int GetClientRect(IntPtr hWnd, ref RECT rc);

        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern bool LockSetForegroundWindow(int uLockCode);

        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        public static extern IntPtr LoadCursorFromFile(string lpFileName);

		#endregion

		#region GDI

		/* constants for the biCompression field */
		public const int BI_RGB = 0;
		public const int BI_RLE8 = 1;
		public const int BI_RLE4 = 2;
		public const int BI_BITFIELDS = 3;
		public const int BI_JPEG = 4;
		public const int BI_PNG = 5;

		/* DIB color table identifiers */
		public const int DIB_RGB_COLORS = 0; /* color table in RGBs */
		public const int DIB_PAL_COLORS = 1; /* color table in palette indices */

        public const int WM_USER = 0x0400;
        public const int EM_FORMATRANGE = WM_USER + 57;
        public const int EM_SETTARGETDEVICE = WM_USER + 72;
        public const int PHYSICALOFFSETX = 112;
        public const int PHYSICALOFFSETY = 113;

		[DllImport("gdi32.dll", SetLastError = true)]
		public static extern IntPtr CreateCompatibleBitmap(IntPtr hdc, int nWidth, int nHeight);

		[DllImport("gdi32.dll", SetLastError = true)]
		public static extern bool DeleteObject(IntPtr hObject);

		[DllImport("gdi32.dll", SetLastError = true)]
		public static extern IntPtr CreateCompatibleDC(IntPtr hdc);

		[DllImport("gdi32.dll", SetLastError = true)]
		public static extern int GetDIBits(IntPtr hdc, IntPtr hbmp, int uStartScan, int cScanLines, IntPtr lpvBits, byte[] lpbi, int uUsage);

		[DllImport("gdi32.dll", SetLastError = true)]
		public static extern int GetDeviceCaps(IntPtr hdc, int nIndex);

		[DllImport("gdi32.dll", SetLastError = true)]
		public static extern IntPtr SelectObject(IntPtr hdc, IntPtr hgdiobj);

		[DllImport("gdi32.dll", SetLastError = true)]
		public static extern bool BitBlt(IntPtr hdcDest, int nXDest, int nYDest, int nWidth, int nHeight, IntPtr hdcSrc, int nXSrc, int nYSrc, int dwRop);

		[DllImport("gdi32.dll", SetLastError = true)]
		public static extern bool DeleteDC(IntPtr hdc);

        [DllImport("gdi32.dll", SetLastError = true)]
        public static extern IntPtr CreateDC(string lpszDriver, string lpszDevice, string lpszOutput, IntPtr lpInitData);

		#endregion

		#region ADVAPI

		//
		// Value to indicate no change to an optional parameter
		//
		public const int SERVICE_NO_CHANGE = unchecked((int) 0xffffffff);

		//
		// Service Types (Bit Mask)
		//
		public const int SERVICE_KERNEL_DRIVER = 0x00000001;
		public const int SERVICE_FILE_SYSTEM_DRIVER = 0x00000002;
		public const int SERVICE_ADAPTER = 0x00000004;
		public const int SERVICE_RECOGNIZER_DRIVER = 0x00000008;

		public const int SERVICE_DRIVER =
			(
				SERVICE_KERNEL_DRIVER |
					SERVICE_FILE_SYSTEM_DRIVER |
					SERVICE_RECOGNIZER_DRIVER);

		public const int SERVICE_WIN32_OWN_PROCESS = 0x00000010;
		public const int SERVICE_WIN32_SHARE_PROCESS = 0x00000020;
		public const int SERVICE_WIN32 =
			(
				SERVICE_WIN32_OWN_PROCESS |
					SERVICE_WIN32_SHARE_PROCESS);

		public const int SERVICE_INTERACTIVE_PROCESS = 0x00000100;

		public const int SERVICE_TYPE_ALL =
			(
				SERVICE_WIN32 |
					SERVICE_ADAPTER |
					SERVICE_DRIVER |
					SERVICE_INTERACTIVE_PROCESS);

		public const int STANDARD_RIGHTS_REQUIRED = unchecked((int) (0x000F0000L));

		//
		// Service Control Manager object specific access types
		//
		public const int SC_MANAGER_CONNECT = 0x0001;
		public const int SC_MANAGER_CREATE_SERVICE = 0x0002;
		public const int SC_MANAGER_ENUMERATE_SERVICE = 0x0004;
		public const int SC_MANAGER_LOCK = 0x0008;
		public const int SC_MANAGER_QUERY_LOCK_STATUS = 0x0010;
		public const int SC_MANAGER_MODIFY_BOOT_CONFIG = 0x0020;

		public const int SC_MANAGER_ALL_ACCESS = unchecked((int)
			(
				STANDARD_RIGHTS_REQUIRED |
					SC_MANAGER_CONNECT |
					SC_MANAGER_CREATE_SERVICE |
					SC_MANAGER_ENUMERATE_SERVICE |
					SC_MANAGER_LOCK |
					SC_MANAGER_QUERY_LOCK_STATUS |
					SC_MANAGER_MODIFY_BOOT_CONFIG));

		//
		// Service object specific access type
		//
		public const int SERVICE_QUERY_CONFIG = 0x0001;
		public const int SERVICE_CHANGE_CONFIG = 0x0002;
		public const int SERVICE_QUERY_STATUS = 0x0004;
		public const int SERVICE_ENUMERATE_DEPENDENTS = 0x0008;
		public const int SERVICE_START = 0x0010;
		public const int SERVICE_STOP = 0x0020;
		public const int SERVICE_PAUSE_CONTINUE = 0x0040;
		public const int SERVICE_INTERROGATE = 0x0080;
		public const int SERVICE_USER_DEFINED_CONTROL = 0x0100;

		public const int SERVICE_ALL_ACCESS = unchecked((int)
			(
				STANDARD_RIGHTS_REQUIRED |
					SERVICE_QUERY_CONFIG |
					SERVICE_CHANGE_CONFIG |
					SERVICE_QUERY_STATUS |
					SERVICE_ENUMERATE_DEPENDENTS |
					SERVICE_START |
					SERVICE_STOP |
					SERVICE_PAUSE_CONTINUE |
					SERVICE_INTERROGATE |
					SERVICE_USER_DEFINED_CONTROL));

		[DllImport("advapi32.dll", SetLastError = true)]
		public static extern IntPtr LockServiceDatabase(IntPtr hSCManager);

		[DllImport("advapi32.dll", SetLastError = true)]
		public static extern bool UnlockServiceDatabase(IntPtr ScLock);

		[DllImport("advapi32.dll", SetLastError = true)]
		public static extern bool ChangeServiceConfig(IntPtr hService, int dwServiceType, int dwStartType, int dwErrorControl, string lpBinaryPathName, string lpLoadOrderGroup, IntPtr lpdwTagId, string lpDependencies, string lpServiceStartName, string lpPassword, string lpDisplayName);

		[DllImport("advapi32.dll", SetLastError = true)]
		public static extern IntPtr OpenService(IntPtr hSCManager, string lpServiceName, int dwDesiredAccess);

		[DllImport("advapi32.dll", SetLastError = true)]
		public static extern IntPtr OpenSCManager(string lpMachineName, string lpDatabaseName, int dwDesiredAccess);

		[DllImport("advapi32.dll", SetLastError = true)]
		public static extern bool CloseServiceHandle(IntPtr hSCObject);

		#endregion

		#region ERROR

		//
		// MessageId: ERROR_SUCCESS
		//
		// MessageText:
		//
		//  The operation completed successfully.
		//
		public const int ERROR_SUCCESS = unchecked((int) 0L);

		public const int NO_ERROR = unchecked((int) 0L); // dderror
		public const int SEC_E_OK = unchecked((int) (0x00000000L));

		//
		// MessageId: ERROR_SERVICE_DATABASE_LOCKED
		//
		// MessageText:
		//
		//  The service database is locked.
		//
		public const int ERROR_SERVICE_DATABASE_LOCKED = unchecked((int) 1055L);

		#endregion

		#region Shell

		public const int CSIDL_DESKTOP = 0x0000; // <desktop>
		public const int CSIDL_INTERNET = 0x0001; // Internet Explorer (icon on desktop)
		public const int CSIDL_PROGRAMS = 0x0002; // Start Menu\Programs
		public const int CSIDL_CONTROLS = 0x0003; // My Computer\Control Panel
		public const int CSIDL_PRINTERS = 0x0004; // My Computer\Printers
		public const int CSIDL_PERSONAL = 0x0005; // My Documents
		public const int CSIDL_FAVORITES = 0x0006; // <user name>\Favorites
		public const int CSIDL_STARTUP = 0x0007; // Start Menu\Programs\Startup
		public const int CSIDL_RECENT = 0x0008; // <user name>\Recent
		public const int CSIDL_SENDTO = 0x0009; // <user name>\SendTo
		public const int CSIDL_BITBUCKET = 0x000a; // <desktop>\Recycle Bin
		public const int CSIDL_STARTMENU = 0x000b; // <user name>\Start Menu
		public const int CSIDL_MYDOCUMENTS = 0x000c; // logical "My Documents" desktop icon
		public const int CSIDL_MYMUSIC = 0x000d; // "My Music" folder
		public const int CSIDL_MYVIDEO = 0x000e; // "My Videos" folder
		public const int CSIDL_DESKTOPDIRECTORY = 0x0010; // <user name>\Desktop
		public const int CSIDL_DRIVES = 0x0011; // My Computer
		public const int CSIDL_NETWORK = 0x0012; // Network Neighborhood (My Network Places)
		public const int CSIDL_NETHOOD = 0x0013; // <user name>\nethood
		public const int CSIDL_FONTS = 0x0014; // windows\fonts
		public const int CSIDL_TEMPLATES = 0x0015;
		public const int CSIDL_COMMON_STARTMENU = 0x0016; // All Users\Start Menu
		public const int CSIDL_COMMON_PROGRAMS = 0X0017; // All Users\Start Menu\Programs
		public const int CSIDL_COMMON_STARTUP = 0x0018; // All Users\Startup
		public const int CSIDL_COMMON_DESKTOPDIRECTORY = 0x0019; // All Users\Desktop
		public const int CSIDL_APPDATA = 0x001a; // <user name>\Application Data
		public const int CSIDL_PRINTHOOD = 0x001b; // <user name>\PrintHood

		[DllImport("shell32.dll", SetLastError = true)]
		public static extern int SHGetFolderPath(IntPtr hwndOwner, int nFolder, IntPtr hToken, int dwFlags, out string pszPath);

		[DllImport("Shell32.dll", CharSet=CharSet.Auto)]
		internal static extern IntPtr SHBrowseForFolder(ref BrowseInfo bi);

		[DllImport("Shell32.dll", CharSet=CharSet.Auto)]
		[return : MarshalAs(UnmanagedType.Bool)]
		internal static extern bool SHGetPathFromIDList(IntPtr pidl, [MarshalAs(UnmanagedType.LPTStr)] StringBuilder pszPath);

		[DllImport("User32.Dll")]
		public static extern IntPtr SendMessage(IntPtr hwnd, int msg, IntPtr wp, IntPtr lp);

		[DllImport("Shell32.dll")]
		internal static extern int SHGetMalloc([MarshalAs(UnmanagedType.IUnknown)] out object shmalloc);

		[DllImport("Shell32.dll")]
		public static extern bool Shell_NotifyIcon(int dwMessage, ref NOTIFYICONDATA lpdata);

		//Helper routine to free memory allocated using shells malloc object
		internal static void SHMemFree(IntPtr ptr)
		{
			object shmalloc = null;

			if (SHGetMalloc(out shmalloc) == 0)
			{
				IMalloc malloc = (IMalloc) shmalloc;

				(malloc).Free(ptr);
			}
		}

		[DllImport("user32.Dll")]
		public static extern Int32 EnumThreadWindows(UInt32 threadId, EnumThreadWndProc callback, UInt32 param);

		[DllImport("user32.Dll")]
		public static extern Int32 GetClassName(IntPtr hWnd, StringBuilder className, Int32 maxCount);

		[DllImport("shell32.Dll")]
		public static extern Int32 Shell_NotifyIcon(NotifyCommand cmd, ref NotifyIconData data);

		#endregion
	}

	/// <summary>
	/// Flags that control display and behaviour of folder browse dialog
	/// </summary>
	[Flags]
	public enum BrowseFlags : int
	{
		/// <summary>
		/// Same as BIF_RETURNONLYFSDIRS 
		/// </summary>
		ReturnOnlyFSDirs = 0x0001,
		/// <summary>
		/// Same as BIF_DONTGOBELOWDOMAIN 
		/// </summary>
		DontGoBelowDomain = 0x0002,
		/// <summary>
		/// Same as BIF_STATUSTEXT 
		/// </summary>
		ShowStatusText = 0x0004,
		/// <summary>
		/// Same as BIF_RETURNFSANCESTORS 
		/// </summary>
		ReturnFSancestors = 0x0008,
		/// <summary>
		/// Same as BIF_EDITBOX 
		/// </summary>
		EditBox = 0x0010,
		/// <summary>
		/// Same as BIF_VALIDATE 
		/// </summary>
		Validate = 0x0020,
		/// <summary>
		/// Same as BIF_NEWDIALOGSTYLE
		/// </summary>
		NewDialogStyle = 0x0040,
		/// <summary>
		/// Same as BIF_BROWSEINCLUDEURLS 
		/// </summary>
		BrowseIncludeURLs = 0x0080,
		/// <summary>
		/// Same as BIF_UAHINT
		/// </summary>
		AddUsageHint = 0x0100,
		/// <summary>
		/// Same as BIF_NONEWFOLDERBUTTON 
		/// </summary>
		NoNewFolderButton = 0x0200,
		/// <summary>
		/// Same as BIF_BROWSEFORCOMPUTER
		/// </summary>
		BrowseForComputer = 0x1000,
		/// <summary>
		/// Same as BIF_BROWSEFORPRINTER 
		/// </summary>
		BrowseForPrinter = 0x2000,
		/// <summary>
		/// Same as BIF_BROWSEINCLUDEFILES 
		/// </summary>
		IncludeFiles = 0x4000,
		/// <summary>
		/// Same as BIF_SHAREABLE 
		/// </summary>
		ShowShareable = 0x8000,
	}

	#region Delegate and Event Arg Decalarations

	/// <summary>
	/// Provides data for folder selection changed event
	/// </summary>
	public class FolderSelChangedEventArgs : EventArgs, IDisposable
	{
		private IntPtr pidlNewSelect;

		internal FolderSelChangedEventArgs(IntPtr pidlNewSelect)
		{
			this.pidlNewSelect = pidlNewSelect;
		}

		/// <summary>
		/// Return ITEMIDLIST for the currently selected folder
		/// </summary>
		public IntPtr CurSelFolderPidl
		{
			get { return pidlNewSelect; }
		}

		/// <summary>
		/// Gets the path of the folder which is currently selected
		/// </summary>
		public string CurSelFolderPath
		{
			get
			{
				StringBuilder path = new StringBuilder(260);
				Win32.SHGetPathFromIDList(pidlNewSelect, path);

				return path.ToString();
			}
		}

		public void Dispose()
		{
			Win32.SHMemFree(pidlNewSelect);
		}
	} ;

	public delegate void FolderSelChangedEventHandler(object sender, FolderSelChangedEventArgs e);

	/// <summary>
	/// Provides data for the IUnknownObtainedEvent.
	/// </summary>
	public class IUnknownObtainedEventArgs : EventArgs
	{
		private object siteUnknown;

		internal IUnknownObtainedEventArgs(object siteUnknown)
		{
			this.siteUnknown = siteUnknown;
		}

		/// <summary>
		/// Object that corrensponds to the IUnknown obtained
		/// </summary>
		public object SiteUnknown
		{
			get { return siteUnknown; }
		}
	}

	public delegate void IUnknownObtainedEventHandler(object sender, IUnknownObtainedEventArgs args);

	/// <summary>
	/// Provides data for validation failed event.
	/// </summary>
	public class ValidateFailedEventArgs
	{
		private string invalidText;
		private bool dismissDialog = false;

		internal ValidateFailedEventArgs(string invalidText)
		{
			this.invalidText = invalidText;
		}

		/// <summary>
		/// The text which called validation to fail
		/// </summary>
		public string InvalidText
		{
			get { return invalidText; }
		}

		/// <summary>
		/// Sets whether the dialog needs to be dismissed or not
		/// </summary>
		public bool DismissDialog
		{
			get { return dismissDialog; }
			set { dismissDialog = value; }
		}
	}

	public delegate void ValidateFailedEventHandler(object sender, ValidateFailedEventArgs args);

	#endregion

	/// <summary>
	/// Encapsulates the shell folder browse dialog shown by SHBrowseForFolder
	/// </summary>
	public class ShellFolderBrowser
	{
		private string title;
		private IntPtr pidlReturned = IntPtr.Zero;
		private IntPtr handle;
		private string displayName;
		private BrowseFlags flags;

		/// <summary>
		/// 
		/// </summary>
		public ShellFolderBrowser()
		{
		}

		#region Component properties

		/// <summary>
		/// String that is displayed above the tree view control in the dialog box. 
		/// This string can be used to specify instructions to the user. 
		/// Can only be modified if the dalog is not currently displayed.
		/// </summary>
		[Description("String that is displayed above the tree view control in the dialog box. This string can be used to specify instructions to the user.")]
		public string Title
		{
			get { return title; }
			set
			{
				if (handle != IntPtr.Zero)
				{
					throw new InvalidOperationException();
				}

				title = value;
			}
		}

		/// <summary>
		/// The display name of the folder selected by the user
		/// </summary>
		[Description("The display name of the folder selected by the user")]
		public string FolderDisplayName
		{
			get { return displayName; }
		}

		/// <summary>
		/// The folder path that was selected
		/// </summary>
		public string FolderPath
		{
			get
			{
				if (pidlReturned == IntPtr.Zero)
				{
					return string.Empty;
				}

				StringBuilder pathReturned = new StringBuilder(260);

				Win32.SHGetPathFromIDList(pidlReturned, pathReturned);
				return pathReturned.ToString();
			}
			set
			{
				//if (pidlReturned == IntPtr.Zero)
				//	return string.Empty;

				StringBuilder pathReturned = new StringBuilder(260);
				pathReturned.Append(value);

				Win32.SHGetPathFromIDList(pidlReturned, pathReturned);
			}
		}

		/// <summary>
		/// Sets the flags that control the behaviour of the dialog
		/// </summary>
		public BrowseFlags BrowseFlags
		{
			get { return flags; }
			set { flags = value; }
		}

		#endregion

		#region ShowDialog and related methods

		private bool ShowDialogInternal(ref BrowseInfo bi)
		{
			bi.title = title;
			bi.displayname = new string('\0', 260);
			bi.callback = new BrowseCallBackProc(this.CallBack);
			bi.flags = (int) flags;

			//Free any old pidls
			if (pidlReturned != IntPtr.Zero)
			{
				Win32.SHMemFree(pidlReturned);
			}

			bool ret = (pidlReturned = Win32.SHBrowseForFolder(ref bi)) != IntPtr.Zero;

			if (ret)
			{
				displayName = bi.displayname;
			}

			//Reset the handle
			handle = IntPtr.Zero;

			return ret;
		}

		/// <summary>
		/// Shows the dialog
		/// </summary>
		/// <param name="owner">The window to use as the owner</param>
		/// <returns></returns>
		public bool ShowDialog(IWin32Window owner)
		{
			if (handle != IntPtr.Zero)
			{
				throw new InvalidOperationException();
			}

			BrowseInfo bi = new BrowseInfo();

			if (owner != null)
			{
				bi.hwndOwner = owner.Handle;
			}

			return ShowDialogInternal(ref bi);
		}

		/// <summary>
		/// Shows the dialog using active window as the owner
		/// </summary>
//		public bool ShowDialog()
//		{
//			return ShowDialog(Form.ActiveForm);
//		}

		#endregion

		#region Functions that send messages to the dialog
		private const int WM_USER = 0x0400;

		private const int BFFM_SETSTATUSTEXTA = (WM_USER + 100);
		private const int BFFM_SETSTATUSTEXTW = (WM_USER + 104);

		/// <summary>
		/// Sets the text of the staus area of the folder dialog
		/// </summary>
		/// <param name="text">Text to set</param>
		public void SetStatusText(string text)
		{
			if (handle == IntPtr.Zero)
			{
				throw new InvalidOperationException();
			}

			int msg = (Environment.OSVersion.Platform == PlatformID.Win32NT) ? BFFM_SETSTATUSTEXTW : BFFM_SETSTATUSTEXTA;
			IntPtr strptr = Marshal.StringToHGlobalAuto(text);

			Win32.SendMessage(handle, msg, IntPtr.Zero, strptr);

			Marshal.FreeHGlobal(strptr);
		}

		private const int BFFM_ENABLEOK = (WM_USER + 101);

		/// <summary>
		/// Enables or disables the ok button
		/// </summary>
		/// <param name="bEnable">true to enable false to diasble the OK button</param>
		public void EnableOkButton(bool bEnable)
		{
			if (handle == IntPtr.Zero)
			{
				throw new InvalidOperationException();
			}

			IntPtr lp = bEnable ? new IntPtr(1) : IntPtr.Zero;

			Win32.SendMessage(handle, BFFM_ENABLEOK, IntPtr.Zero, lp);
		}

		private const int BFFM_SETSELECTIONA = (WM_USER + 102);
		private const int BFFM_SETSELECTIONW = (WM_USER + 103);

		/// <summary>
		/// Sets the selection the text specified
		/// </summary>
		/// <param name="newsel">The path of the folder which is to be selected</param>
		public void SetSelection(string newsel)
		{
			if (handle == IntPtr.Zero)
			{
				throw new InvalidOperationException();
			}

			int msg = (Environment.OSVersion.Platform == PlatformID.Win32NT) ? BFFM_SETSELECTIONA : BFFM_SETSELECTIONW;

			IntPtr strptr = Marshal.StringToHGlobalAuto(newsel);

			Win32.SendMessage(handle, msg, new IntPtr(1), strptr);

			Marshal.FreeHGlobal(strptr);
		}

		private const int BFFM_SETOKTEXT = (WM_USER + 105);

		/// <summary>
		/// Sets the text of the OK button in the dialog
		/// </summary>
		/// <param name="text">New text of the OK button</param>
		public void SetOkButtonText(string text)
		{
			if (handle == IntPtr.Zero)
			{
				throw new InvalidOperationException();
			}

			IntPtr strptr = Marshal.StringToHGlobalUni(text);

			Win32.SendMessage(handle, BFFM_SETOKTEXT, new IntPtr(1), strptr);

			Marshal.FreeHGlobal(strptr);
		}

		private const int BFFM_SETEXPANDED = (WM_USER + 106);

		/// <summary>
		/// Expand a path in the folder
		/// </summary>
		/// <param name="path">The path to expand</param>
		public void SetExpanded(string path)
		{
			IntPtr strptr = Marshal.StringToHGlobalUni(path);

			Win32.SendMessage(handle, BFFM_SETEXPANDED, new IntPtr(1), strptr);

			Marshal.FreeHGlobal(strptr);
		}

		#endregion

		#region Callback Handling and Event Propogation

		/// <summary>
		/// Fired when the dialog is initialized
		/// </summary>
		public event EventHandler Initialized;

		/// <summary>
		/// Fired when selection changes
		/// </summary>
		public event FolderSelChangedEventHandler SelChanged;

		/// <summary>
		/// Shell provides an IUnknown through this event. For details see documentation of SHBrowseForFolder
		/// </summary>
		public event IUnknownObtainedEventHandler IUnknownObtained;

		/// <summary>
		/// Fired when validation of text typed by user fails
		/// </summary>
		public event ValidateFailedEventHandler ValidateFailed;

		private const int BFFM_INITIALIZED = 1;
		private const int BFFM_SELCHANGED = 2;
		private const int BFFM_VALIDATEFAILEDA = 3;
		private const int BFFM_VALIDATEFAILEDW = 4;
		private const int BFFM_IUNKNOWN = 5;

		private int CallBack(IntPtr hwnd, int msg, IntPtr lp, IntPtr lpData)
		{
			int ret = 0;

			switch (msg)
			{
				case BFFM_INITIALIZED:
					handle = hwnd;
					if (Initialized != null)
					{
						Initialized(this, null);
					}
					break;
				case BFFM_IUNKNOWN:
					if (IUnknownObtained != null)
					{
						IUnknownObtained(this, new IUnknownObtainedEventArgs(Marshal.GetObjectForIUnknown(lp)));
					}
					break;
				case BFFM_SELCHANGED:
					if (SelChanged != null)
					{
						FolderSelChangedEventArgs e = new FolderSelChangedEventArgs(lp);
						SelChanged(this, e);
					}
					break;
				case BFFM_VALIDATEFAILEDA:
					if (ValidateFailed != null)
					{
						ValidateFailedEventArgs e = new ValidateFailedEventArgs(Marshal.PtrToStringAnsi(lpData));
						ValidateFailed(this, e);

						ret = (e.DismissDialog) ? 0 : 1;
					}
					break;
				case BFFM_VALIDATEFAILEDW:
					if (ValidateFailed != null)
					{
						ValidateFailedEventArgs e = new ValidateFailedEventArgs(Marshal.PtrToStringUni(lpData));
						ValidateFailed(this, e);

						ret = (e.DismissDialog) ? 0 : 1;
					}
					break;
			}

			return ret;
		}

		#endregion
	}

    public class Win32Stuff
    {

        #region Class Variables

        public const int SM_CXSCREEN = 0;
        public const int SM_CYSCREEN = 1;

        public const Int32 CURSOR_SHOWING = 0x00000001;

        [StructLayout(LayoutKind.Sequential)]
        public struct ICONINFO
        {
            public bool fIcon;         // Specifies whether this structure defines an icon or a cursor. A value of TRUE specifies 
            public Int32 xHotspot;     // Specifies the x-coordinate of a cursor's hot spot. If this structure defines an icon, the hot 
            public Int32 yHotspot;     // Specifies the y-coordinate of the cursor's hot spot. If this structure defines an icon, the hot 
            public IntPtr hbmMask;     // (HBITMAP) Specifies the icon bitmask bitmap. If this structure defines a black and white icon, 
            public IntPtr hbmColor;    // (HBITMAP) Handle to the icon color bitmap. This member can be optional if this 
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            public int left;
            public int top;
            public int right;
            public int bottom;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct POINT
        {
            public Int32 x;
            public Int32 y;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct CURSORINFO
        {
            public Int32 cbSize;        // Specifies the size, in bytes, of the structure. 
            public Int32 flags;         // Specifies the cursor state. This parameter can be one of the following values:
            public IntPtr hCursor;          // Handle to the cursor. 
            public POINT ptScreenPos;       // A POINT structure that receives the screen coordinates of the cursor. 
        }

        #endregion


        #region Class Functions

        [DllImport("user32.dll", EntryPoint = "GetDesktopWindow")]
        public static extern IntPtr GetDesktopWindow();

        [DllImport("user32.dll", EntryPoint = "GetDC")]
        public static extern IntPtr GetDC(IntPtr ptr);

        [DllImport("user32.dll", EntryPoint = "GetSystemMetrics")]
        public static extern int GetSystemMetrics(int abc);

        [DllImport("user32.dll", EntryPoint = "GetWindowDC")]
        public static extern IntPtr GetWindowDC(IntPtr ptr);

        [DllImport("user32.dll", EntryPoint = "ReleaseDC")]
        public static extern IntPtr ReleaseDC(IntPtr hWnd, IntPtr hDc);

        [DllImport("user32.dll", EntryPoint = "GetCursorInfo")]
        public static extern bool GetCursorInfo(out CURSORINFO pci);

        [DllImport("user32.dll", EntryPoint = "CopyIcon")]
        public static extern IntPtr CopyIcon(IntPtr hIcon);

        [DllImport("user32.dll", EntryPoint = "GetIconInfo")]
        public static extern bool GetIconInfo(IntPtr hIcon, out ICONINFO piconinfo);

        [DllImport("user32.dll", EntryPoint = "DestroyIcon")]
        public static extern bool DestroyIcon(IntPtr hIcon);

        [DllImport("user32.dll")]
        public static extern IntPtr GetWindowRect(IntPtr hWnd, ref RECT rect);
        #endregion
    }

    public class GDIStuff
    {
        #region Class Variables
        public const int SRCCOPY = 13369376;
        #endregion


        #region Class Functions
        [DllImport("gdi32.dll", EntryPoint = "CreateDC")]
        public static extern IntPtr CreateDC(IntPtr lpszDriver, string lpszDevice, IntPtr lpszOutput, IntPtr lpInitData);

        [DllImport("gdi32.dll", EntryPoint = "DeleteDC")]
        public static extern IntPtr DeleteDC(IntPtr hDc);

        [DllImport("gdi32.dll", EntryPoint = "DeleteObject")]
        public static extern IntPtr DeleteObject(IntPtr hDc);

        [DllImport("gdi32.dll", EntryPoint = "BitBlt")]
        public static extern bool BitBlt(IntPtr hdcDest, int xDest,
                                         int yDest, int wDest,
                                         int hDest, IntPtr hdcSource,
                                         int xSrc, int ySrc, int RasterOp);

        [DllImport("gdi32.dll", EntryPoint = "CreateCompatibleBitmap")]
        public static extern IntPtr CreateCompatibleBitmap
                                    (IntPtr hdc, int nWidth, int nHeight);

        [DllImport("gdi32.dll", EntryPoint = "CreateCompatibleDC")]
        public static extern IntPtr CreateCompatibleDC(IntPtr hdc);

        [DllImport("gdi32.dll", EntryPoint = "SelectObject")]
        public static extern IntPtr SelectObject(IntPtr hdc, IntPtr bmp);
        #endregion
    }
}