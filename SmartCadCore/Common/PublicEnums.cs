﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadCore.Common
{
    [Serializable]
    public enum CommittedDataAction
    {
        None,
        Save,
        Update,
        Delete,
        HighLight
    }

    public enum DistanceUnit
    {
        Kilometers = 1,
        Centimeters = 6,
        Meters = 7
    }

    public enum AddPositionToolState
    {
        Normal,
        NotPainted,
        Moving,
        Deleted,
        None
    }

    public enum ImageSearchState
    {
        Normal,
        Moving,
        Deleted,
        NotPainted,
        None
    }
}
