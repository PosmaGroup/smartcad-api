using System;
using System.Drawing;
using System.Collections;
using System.Windows.Forms;

using DevExpress.XtraEditors;

namespace SmartCadCore.Common
{
    #region Class MessageForm Documentation
    /// <summary>
    /// Es una copia de MessageForm pero no muestra el boton de details
    /// </summary>
    /// <className>MessageForm</className>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    public class MessageForm : XtraForm
    {
        private bool expand = false;
        private ICollection exception = null;
        private MessageFormType type = MessageFormType.Question;
        private System.Windows.Forms.Label labelMessage;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.ImageList imageList;
        private SimpleButton buttonOk;
        private new System.ComponentModel.IContainer components;
        private static readonly Point button1Location = new Point(232, 80);
        private static readonly Point button2Location = new Point(320, 80);
        private static readonly Point buttonYesLocation = new Point(156, 80);
        private static readonly Point buttonNoLocation = new Point(240, 80);
        private static readonly Point buttonCancelLocation = new Point(324, 80);
        private static readonly Size normalSize = new Size(416, 140);
        private SimpleButton buttonDetails;
        private System.Windows.Forms.TreeView treeView;
        private SimpleButton buttonNo;
        private SimpleButton buttonCancel;
        private static readonly Size specialSize = new Size(416, 240);
        private static bool checkPreference = false;

        public static bool CheckPreference
        {
            set
            {
            	checkPreference = value;
            }
            get
            {
                return checkPreference;
            }
        }

        public static DialogResult Show(string message, ICollection col)
        {
            message = ResourceLoader.GetString2(message);
            MessageForm form = new MessageForm(message, col);
            return form.ShowDialog();
        }

        public static DialogResult Show(string message, Exception e)
        {
            message = ResourceLoader.GetString2(message);
            MessageForm form = new MessageForm(message, e);
            form.buttonDetails.Visible = checkPreference;
            return form.ShowDialog();
        }

        public static void ShowNonModal(string message, Exception e)
        {
            message = ResourceLoader.GetString2(message);
            MessageForm form = new MessageForm(message, e);
            form.buttonDetails.Visible = checkPreference;
            form.ShowDialog();
        }

        public static DialogResult Show(string caption, string message, Exception e)
        {
            message = ResourceLoader.GetString2(message);
            MessageForm form = new MessageForm(caption, message, e);
            form.buttonDetails.Visible = checkPreference;
            return form.ShowDialog();
        }

        public static DialogResult Show(string message, MessageFormType type)
        {
            message = ResourceLoader.GetString2(message);
            MessageForm form = new MessageForm(message, type);
            if(type == MessageFormType.Error)
            {
                if (message == ResourceLoader.GetString2("InvalidLoginOrPassword"))
                {
                    form.buttonDetails.Visible = false;
                }
                else
                {
                    form.buttonDetails.Visible = true;
                }
            }
            return form.ShowDialog();
        }

        public static DialogResult Show(string caption, string message, MessageFormType type)
        {
            message = ResourceLoader.GetString2(message);
            MessageForm form = new MessageForm(caption, message, type);
            if (type == MessageFormType.Error)
            {
                form.buttonDetails.Visible = true;
            }
            return form.ShowDialog();
        }

        private bool showDetails = true;

        public MessageForm(string message, Exception e)
            : this(message, new ArrayList(new object[1] { e }))
        {
        }

        public MessageForm(string message, ICollection e)
        {
            FontFix();
            InitializeComponent();
            labelMessage.Text = message;
            Exception = e;
        }

        public MessageForm(string caption, string message, Exception e)
            : this(message, e)
        {
            Text = caption;
        }

        public MessageForm(string message, MessageFormType typeI)
        {
            FontFix();
            InitializeComponent();
            labelMessage.Text = message;
            Type = typeI;

            try
            {
                if (typeI == MessageFormType.Error)
                {
                    Exception = new Exception[] { new Exception(message) };
                    showDetails = checkPreference;
                }                
                else
                    showDetails = false;
            }
            catch
            {
            }

            buttonDetails.Visible = showDetails;
        }

        public MessageForm(string caption, string message, MessageFormType typeI)
            : this(message, typeI)
        {
            Text = caption;
        }
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MessageForm));
            this.labelMessage = new System.Windows.Forms.Label();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.buttonOk = new DevExpress.XtraEditors.SimpleButton();
            this.buttonNo = new DevExpress.XtraEditors.SimpleButton();
            this.buttonDetails = new DevExpress.XtraEditors.SimpleButton();
            this.treeView = new System.Windows.Forms.TreeView();
            this.buttonCancel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // labelMessage
            // 
            this.labelMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMessage.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelMessage.Location = new System.Drawing.Point(49, 8);
            this.labelMessage.Name = "labelMessage";
            this.labelMessage.Size = new System.Drawing.Size(352, 64);
            this.labelMessage.TabIndex = 0;
            this.labelMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox
            // 
            this.pictureBox.Location = new System.Drawing.Point(8, 8);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(32, 32);
            this.pictureBox.TabIndex = 4;
            this.pictureBox.TabStop = false;
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "");
            this.imageList.Images.SetKeyName(1, "");
            this.imageList.Images.SetKeyName(2, "");
            this.imageList.Images.SetKeyName(3, "");
            // 
            // buttonOk
            // 
            this.buttonOk.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOk.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonOk.Appearance.Options.UseFont = true;
            this.buttonOk.Appearance.Options.UseForeColor = true;
            this.buttonOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOk.Location = new System.Drawing.Point(156, 80);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(75, 23);
            this.buttonOk.TabIndex = 2;
            this.buttonOk.TabStop = false;
            this.buttonOk.Text = "S�";
            this.buttonOk.Visible = false;
            // 
            // buttonNo
            // 
            this.buttonNo.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNo.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonNo.Appearance.Options.UseFont = true;
            this.buttonNo.Appearance.Options.UseForeColor = true;
            this.buttonNo.DialogResult = System.Windows.Forms.DialogResult.No;
            this.buttonNo.Enabled = false;
            this.buttonNo.Location = new System.Drawing.Point(240, 80);
            this.buttonNo.Name = "buttonNo";
            this.buttonNo.Size = new System.Drawing.Size(75, 23);
            this.buttonNo.TabIndex = 3;
            this.buttonNo.TabStop = false;
            this.buttonNo.Text = "No";
            this.buttonNo.Visible = false;
            // 
            // buttonDetails
            // 
            this.buttonDetails.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDetails.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonDetails.Appearance.Options.UseFont = true;
            this.buttonDetails.Appearance.Options.UseForeColor = true;
            this.buttonDetails.Location = new System.Drawing.Point(8, 80);
            this.buttonDetails.Name = "buttonDetails";
            this.buttonDetails.Size = new System.Drawing.Size(75, 23);
            this.buttonDetails.TabIndex = 1;
            this.buttonDetails.TabStop = false;
            this.buttonDetails.Text = ResourceLoader.GetString2("buttonDetails").ToString();
            this.buttonDetails.Visible = false;
            this.buttonDetails.Click += new System.EventHandler(this.buttonDetails_Click);
            // 
            // treeView
            // 
            this.treeView.Location = new System.Drawing.Point(0, 112);
            this.treeView.Name = "treeView";
            this.treeView.Size = new System.Drawing.Size(410, 104);
            this.treeView.TabIndex = 5;
            this.treeView.TabStop = false;
            this.treeView.Visible = false;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCancel.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonCancel.Appearance.Options.UseFont = true;
            this.buttonCancel.Appearance.Options.UseForeColor = true;
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Enabled = false;
            this.buttonCancel.Location = new System.Drawing.Point(324, 80);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 4;
            this.buttonCancel.TabStop = false;
            this.buttonCancel.Text = "Cancelar";
            this.buttonCancel.Visible = false;
            // 
            // MessageForm
            // 
            this.AcceptButton = this.buttonOk;
            this.Appearance.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(42)))), ((int)(((byte)(103)))));
            this.Appearance.Options.UseFont = true;
            this.Appearance.Options.UseForeColor = true;
            this.AutoScaleBaseSize = new System.Drawing.Size(6, 14);
            this.CancelButton = this.buttonNo;
            this.ClientSize = new System.Drawing.Size(410, 216);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.treeView);
            this.Controls.Add(this.buttonDetails);
            this.Controls.Add(this.buttonNo);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.labelMessage);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.LookAndFeel.SkinName = "Blue";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.MaximizeBox = false;
            this.Name = "MessageForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion

        private void buttonDetails_Click(object sender, System.EventArgs e)
        {
            
            if (!expand)
            {
                Size = specialSize;
                expand = true;
                treeView.Visible = true;
                treeView.TabStop = true;
            }
            else
            {
                Size = normalSize;
                expand = false;
                treeView.Visible = false;
                treeView.TabStop = false;
            }
        }


        public ICollection Exception
        {
            get
            {
                return exception;
            }
            set
            {
                if (value != null)
                {
                    exception = value;
                    Type = MessageFormType.Error;
                }
                else
                    throw new ArgumentNullException("value", "The value can not be null");
            }
        }

        public MessageFormType Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
                switch (value)
                {
                    case MessageFormType.Warning:
                        Text = ResourceLoader.GetString2("$Message.Alert");
                        pictureBox.Image = imageList.Images[0];
                        buttonOk.Text = ResourceLoader.GetString2("$Message.Accept");
                        buttonOk.DialogResult = DialogResult.OK;
                        buttonOk.Location = button2Location;
                        buttonOk.Visible = true;
                        buttonOk.TabStop = true;
                        Size = normalSize;
                        break;
                    case MessageFormType.WarningQuestion:
                        Text = ResourceLoader.GetString2("$Message.Alert");
                        pictureBox.Image = imageList.Images[0];
                        buttonOk.Text = ResourceLoader.GetString2("$Message.Yes");
                        buttonOk.DialogResult = DialogResult.Yes;
                        buttonOk.Location = buttonNoLocation;
                        buttonOk.Visible = true;
                        buttonOk.TabStop = true;
                        buttonNo.Enabled = true;
                        buttonNo.Text = ResourceLoader.GetString2("$Message.No");
                        buttonNo.DialogResult = DialogResult.No;
                        buttonNo.Location = buttonCancelLocation;
                        buttonNo.Visible = true;
                        buttonNo.TabStop = true;
                        Size = normalSize;
                        break;
                    case MessageFormType.Question:
                        Text = ResourceLoader.GetString2("$Message.Question");
                        pictureBox.Image = imageList.Images[1];
                        buttonOk.Text = ResourceLoader.GetString2("$Message.Yes");
                        buttonOk.DialogResult = DialogResult.Yes;
                        buttonOk.Location = buttonNoLocation;
                        buttonOk.Visible = true;
                        buttonOk.TabStop = true;
                        buttonNo.Enabled = true;
                        buttonNo.Text = ResourceLoader.GetString2("$Message.No");
                        buttonNo.DialogResult = DialogResult.No;
                        buttonNo.Location = buttonCancelLocation;
                        buttonNo.Visible = true;
                        buttonNo.TabStop = true;
                        treeView.Enabled = false;
                        buttonCancel.TabStop = false;
                        buttonDetails.TabStop = false;

                        Size = normalSize;
                        break;
                    case MessageFormType.Error:
                        Text = ResourceLoader.GetString2("$Message.Error");
                        pictureBox.Image = imageList.Images[2];
                        buttonOk.Text = ResourceLoader.GetString2("$Message.Ok");
                        buttonOk.DialogResult = DialogResult.OK;
                        buttonOk.Location = button2Location;
                        buttonOk.Visible = true;
                        buttonOk.TabStop = true;
                        
                        buttonCancel.TabStop = false;
                        buttonCancel.Visible = false;
                        buttonNo.Visible = false;
                        buttonNo.TabStop = false;
                        
                        Size = normalSize;
                        if (Exception != null)
                        {
                            buttonDetails.Visible = true;
                            buttonDetails.TabStop = true;
                            FillTreeView();
                        }
                        break;
                    case MessageFormType.Information:
                        Text = ResourceLoader.GetString2("$Message.Information");
                        pictureBox.Image = imageList.Images[3];
                        buttonOk.Text = ResourceLoader.GetString2("$Message.Accept");
                        buttonOk.DialogResult = DialogResult.OK;
                        buttonOk.Location = button2Location;
                        buttonOk.Visible = true;
                        buttonOk.TabStop = true;
                        Size = normalSize;
                        break;
                    case MessageFormType.YesNoCancel:
                        Text = ResourceLoader.GetString2("$Message.Confirm");
                        pictureBox.Image = imageList.Images[0];
                        buttonOk.Text = ResourceLoader.GetString2("$Message.Yes");
                        buttonOk.DialogResult = DialogResult.Yes;
                        buttonOk.Location = buttonYesLocation;
                        buttonOk.Visible = true;
                        buttonOk.TabStop = true;
                        buttonNo.Enabled = true;
                        buttonNo.Text = ResourceLoader.GetString2("$Message.No");
                        buttonNo.DialogResult = DialogResult.No;
                        buttonNo.Location = buttonNoLocation;
                        buttonNo.Visible = true;
                        buttonNo.TabStop = true;
                        buttonCancel.Enabled = true;
                        buttonCancel.Text = ResourceLoader.GetString2("$Message.Cancel");
                        buttonCancel.DialogResult = DialogResult.Cancel;
                        buttonCancel.Location = buttonCancelLocation;
                        buttonCancel.Visible = true;
                        buttonCancel.TabStop = true;
                        Size = normalSize;
                        break;
                }
            }
        }

        private void FillTreeView()
        {
            TreeNode node = treeView.Nodes.Add(ResourceLoader.GetString2("Excepciones"));

            foreach (Exception e in Exception)
            {
                FillTreeViewHelpFunction1(e, node);
                if (e.StackTrace != "" && e.StackTrace != null)
                {
                    node = treeView.Nodes.Add(ResourceLoader.GetString2("StackTrace"));

                    string pattern = Convert.ToChar((int)0x0D).ToString() + Convert.ToChar((int)0x0A).ToString() + Convert.ToChar((int)0x20).ToString() + Convert.ToChar((int)0x20).ToString() + Convert.ToChar((int)0x20).ToString();
                    string[] array = System.Text.RegularExpressions.Regex.Split(e.StackTrace, pattern);
                    foreach (string s in array)
                        node = node.Nodes.Add(s);
                }
            }
        }

        private void FillTreeViewHelpFunction1(Exception e, TreeNode node)
        {
            if (e != null)
                FillTreeViewHelpFunction1(e.InnerException, node.Nodes.Add(e.Message));
        }

        #region Font Fix

        [System.Runtime.InteropServices.DllImport("msvcr120.dll")]
        public static extern uint _controlfp(uint controlword, uint mask);

        public static void FontFix()
        {
            const uint controlword = 0x0008001F; // _MCW_EM
            const uint mask = 0x00000010; // _EM_INVALID
            _controlfp(controlword, mask);
        }

        #endregion


    }

	public enum MessageFormType
	{
		Warning,
        WarningQuestion,
		Question,
		Error,
		Information,
		YesNoCancel
	}
}
