﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.DirectoryServices.AccountManagement;

namespace SmartCadCore.Common
{

    /// <summary>
    /// Help class that perform action against the current Domain
    /// </summary>
    public class Domain
    {

        /// <summary>
        /// Validates the user.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="password">The password.</param>
        /// <param name="domain">The domain.</param>
        /// <returns></returns>
        public static bool ValidateUser(string user, string password, string domain)
        {
            bool isvalid = false;
            using (PrincipalContext pc = new PrincipalContext(ContextType.Domain, domain))
            {
                // validate the credentials 
                isvalid = pc.ValidateCredentials(user, password);
            }
            return isvalid;
        }

        /// <summary>
        /// Validates the user.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        public static bool ValidateUser(string user, string password)
        {
            return ValidateUser(user, password, Environment.UserDomainName);
        }

        /// <summary>
        /// Determines whether [is user in group] [the specified username].
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="groupname">The groupname.</param>
        /// <param name="context">The context.</param>
        /// <returns>
        ///   <c>true</c> if [is user in group] [the specified username]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsUserInGroup(string username, ContextType userContextType, string groupname, ContextType groupContextType) 
        {
            bool result = false;
            PrincipalContext userContext = new PrincipalContext(userContextType);
            PrincipalContext groupContext = new PrincipalContext(groupContextType);

            UserPrincipal user = UserPrincipal.FindByIdentity(userContext, IdentityType.SamAccountName, username);

            if (user != null)
            {
                GroupPrincipal group = GroupPrincipal.FindByIdentity(groupContext, groupname);
                if (group != null)
                    result = user.IsMemberOf(group);

                user.Dispose();
            }

            return result;
        }

        /// <summary>
        /// Determines whether [is user local admin] [the specified username].
        /// </summary>
        /// <param name="username">The username.</param>
        /// <returns>
        ///   <c>true</c> if [is user local admin] [the specified username]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsDomainUserLocalAdmin(string username)
        { 
            return IsUserInGroup(username, ContextType.Domain, "Administrators", ContextType.Machine);
        }
    }
}
