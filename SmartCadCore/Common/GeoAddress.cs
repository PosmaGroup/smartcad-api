using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.Common
{
    [Serializable]
    public class GeoAddress
    {
        #region Fields

        private string zone;
        private string street;
        private string reference;
        private string more;
        private GeoPoint point;

        #endregion

        #region Constructors

        public GeoAddress()
        {
        }

        public GeoAddress(string zone, string street, string reference, string more)
        {
            this.zone = zone;
            this.street = street;
            this.reference = reference;
            this.more = more;
            this.point = new GeoPoint(0.0, 0.0);
        }

        public GeoAddress(string zone, string street, string reference, string more, GeoPoint point)
        {
            this.zone = zone;
            this.street = street;
            this.reference = reference;
            this.more = more;
            this.point = point;
        }

        #endregion

        #region Properties


        public string FullText { get; set; }

        public string Zone
        {
            get
            {
                return zone;
            }
            set
            {
                zone = value;
            }
        }

        public string Street
        {
            get
            {
                return street;
            }
            set
            {
                street = value;
            }
        }

        public string Reference
        {
            get
            {
                return reference;
            }
            set
            {
                reference = value;
            }
        }

        public string More
        {
            get
            {
                return more;
            }
            set
            {
                more = value;
            }
        }

        public GeoPoint Point
        {
            get
            {
                return point;
            }
            set 
            {
                point = value;
            }
        }

        #endregion

        #region Overrides

        public override string ToString()
        {
            if (string.IsNullOrEmpty(FullText) == false)
            {
                return FullText;
            }

            string result =  zone + (string.IsNullOrEmpty(street) ? "" :(", " + street)) 
                + (string.IsNullOrEmpty(reference) ? "" : (", " + reference))
                + (string.IsNullOrEmpty(more) ? "" : (", " + more));

            return result;
        }

        #endregion
    }
}
