using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.Common
{
    [Serializable]
    public class GeoPoint
    {
        public GeoPoint(double lon, double lat)
        {
            this.lon = lon;
            this.lat = lat;
        }

        private double lon;

        public double Lon
        {
            get
            {
                return lon;
            }
            set
            {
                lon = value;
            }
        }

        private double lat;

        public double Lat
        {
            get
            {
                return lat;
            }
            set
            {
                lat = value;
            }
        }
    }
}
