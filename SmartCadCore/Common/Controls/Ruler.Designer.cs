﻿namespace SmartCadCore.Common
{
    partial class Ruler
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControlTotalHigh = new DevExpress.XtraEditors.LabelControl();
            this.labelControlHigh = new DevExpress.XtraEditors.LabelControl();
            this.rulerText = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControlZoom = new DevExpress.XtraEditors.LabelControl();
            this.labelLatLon = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControlTotalHigh
            // 
            this.labelControlTotalHigh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControlTotalHigh.Appearance.Options.UseFont = true;
            this.labelControlTotalHigh.Location = new System.Drawing.Point(404, 3);
            this.labelControlTotalHigh.Name = "labelControlTotalHigh";
            this.labelControlTotalHigh.Size = new System.Drawing.Size(75, 13);
            this.labelControlTotalHigh.TabIndex = 0;
            this.labelControlTotalHigh.Text = "labelControl1";
            // 
            // labelControlHigh
            // 
            this.labelControlHigh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControlHigh.Appearance.Options.UseFont = true;
            this.labelControlHigh.Location = new System.Drawing.Point(137, 3);
            this.labelControlHigh.Name = "labelControlHigh";
            this.labelControlHigh.Size = new System.Drawing.Size(75, 13);
            this.labelControlHigh.TabIndex = 1;
            this.labelControlHigh.Text = "labelControl2";
            // 
            // rulerText
            // 
            this.rulerText.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.rulerText.Appearance.Options.UseFont = true;
            this.rulerText.Location = new System.Drawing.Point(12, 3);
            this.rulerText.Name = "rulerText";
            this.rulerText.Size = new System.Drawing.Size(75, 13);
            this.rulerText.TabIndex = 2;
            this.rulerText.Text = "labelControl3";
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Location = new System.Drawing.Point(3, 11);
            this.pictureEdit1.Margin = new System.Windows.Forms.Padding(0);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.Appearance.Options.UseBorderColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.StretchHorizontal;
            this.pictureEdit1.Size = new System.Drawing.Size(100, 14);
            this.pictureEdit1.TabIndex = 3;
            // 
            // labelControlZoom
            // 
            this.labelControlZoom.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControlZoom.Appearance.Options.UseFont = true;
            this.labelControlZoom.Location = new System.Drawing.Point(284, 3);
            this.labelControlZoom.Name = "labelControlZoom";
            this.labelControlZoom.Size = new System.Drawing.Size(33, 13);
            this.labelControlZoom.TabIndex = 4;
            this.labelControlZoom.Text = "ZOOM";
            // 
            // labelLatLon
            // 
            this.labelLatLon.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelLatLon.Appearance.ImageAlign = System.Drawing.ContentAlignment.BottomRight;
            this.labelLatLon.Appearance.Options.UseFont = true;
            this.labelLatLon.Appearance.Options.UseTextOptions = true;
            this.labelLatLon.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelLatLon.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelLatLon.Location = new System.Drawing.Point(587, 0);
            this.labelLatLon.Name = "labelLatLon";
            this.labelLatLon.Size = new System.Drawing.Size(75, 13);
            this.labelLatLon.TabIndex = 5;
            this.labelLatLon.Text = "labelControl1";
            this.labelLatLon.Visible = false;
            // 
            // Ruler
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.labelLatLon);
            this.Controls.Add(this.labelControlZoom);
            this.Controls.Add(this.rulerText);
            this.Controls.Add(this.labelControlHigh);
            this.Controls.Add(this.labelControlTotalHigh);
            this.Controls.Add(this.pictureEdit1);
            this.Name = "Ruler";
            this.Size = new System.Drawing.Size(662, 25);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        public DevExpress.XtraEditors.LabelControl labelControlTotalHigh;
        public DevExpress.XtraEditors.LabelControl labelControlHigh;
        public DevExpress.XtraEditors.LabelControl rulerText;
        public DevExpress.XtraEditors.LabelControl labelControlZoom;
        public DevExpress.XtraEditors.LabelControl labelLatLon;
    }
}
