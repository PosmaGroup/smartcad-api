//===============================================================================
// Genesys GIS Connection Application Block for .NET
//===============================================================================
// Any authorized distribution of any copy of this code (including any related 
// documentation) must reproduce the following restrictions, disclaimer and 
// copyright notice:
// 
// The Genesys name, trademarks and/or logo(s) of Genesys shall not be used to 
// name (even as a part of another name), endorse and/or promote products derived 
// from this code without prior written permission from Genesys Telecommunications 
// Laboratories, Inc.
// 
// The use, copy, and/or distribution of this code is subject to the terms of the 
// Genesys Developer License Agreement. This code shall not be used, copied, 
// and/or distributed under any other license agreement.
// 
//                                         
// THIS CODE IS PROVIDED BY GENESYS TELECOMMUNICATIONS LABORATORIES, INC. 
// (�GENESYS�) �AS IS� WITHOUT ANY WARRANTY OF ANY KIND. GENESYS HEREBY DISCLAIMS 
// ALL EXPRESS, IMPLIED, OR STATUTORY CONDITIONS, REPRESENTATIONS AND WARRANTIES 
// WITH RESPECT TO THIS CODE (OR ANY PART THEREOF), INCLUDING, BUT NOT LIMITED TO, 
// IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR 
// NON-INFRINGEMENT. GENESYS AND ITS SUPPLIERS SHALL NOT BE LIABLE FOR ANY DAMAGE 
// SUFFERED AS A RESULT OF USING THIS CODE. IN NO EVENT SHALL GENESYS AND ITS 
// SUPPLIERS BE LIABLE FOR ANY DIRECT, INDIRECT, CONSEQUENTIAL, ECONOMIC, 
// INCIDENTAL, OR SPECIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, ANY LOST 
// REVENUES OR PROFITS).
// 
//                                         
// Copyright � 2006 Genesys Telecommunications Laboratories, Inc. All rights 
// reserved.
//===============================================================================

using System;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Remoting.Metadata;
using System.Runtime.Remoting.Metadata.W3cXsd2001;

namespace Genesyslab.Desktop.ApplicationBlocks.StatConnection
{
	/// <summary>
	/// Statistics notification class.
	/// </summary>
	[Serializable, SoapType(XmlElementName=@"Notification",
					   XmlNamespace=@"http://www.genesyslab.com/notification.wsdl",
					   XmlTypeName=@"Notification",
					   XmlTypeNamespace=@"http://www.genesyslab.com/notification.wsdl")]
	public class Notification : System.MarshalByRefObject
	{
		/// <summary>
		/// Statistics notification function called on statistic changes.
		/// </summary>
		[SoapMethod(SoapAction=@"notify", ResponseXmlElementName=@"notify", 
			 XmlNamespace=@"http://www.genesyslab.com/notification.wsdl",
			 ResponseXmlNamespace=@"http://www.genesyslab.com/notification.wsdl")]
		public void notify(String eventType, String[][] keyValue)
		{
			//call to notification listener class
            Genesyslab.Desktop.ApplicationBlocks.StatConnection.Connection.OnNotification(eventType, keyValue);
			return;
		}

	}
}
