//===============================================================================
// Genesys GIS Connection Application Block for .NET
//===============================================================================
// Any authorized distribution of any copy of this code (including any related 
// documentation) must reproduce the following restrictions, disclaimer and 
// copyright notice:
// 
// The Genesys name, trademarks and/or logo(s) of Genesys shall not be used to 
// name (even as a part of another name), endorse and/or promote products derived 
// from this code without prior written permission from Genesys Telecommunications 
// Laboratories, Inc.
// 
// The use, copy, and/or distribution of this code is subject to the terms of the 
// Genesys Developer License Agreement. This code shall not be used, copied, 
// and/or distributed under any other license agreement.
// 
//                                         
// THIS CODE IS PROVIDED BY GENESYS TELECOMMUNICATIONS LABORATORIES, INC. 
// (�GENESYS�) �AS IS� WITHOUT ANY WARRANTY OF ANY KIND. GENESYS HEREBY DISCLAIMS 
// ALL EXPRESS, IMPLIED, OR STATUTORY CONDITIONS, REPRESENTATIONS AND WARRANTIES 
// WITH RESPECT TO THIS CODE (OR ANY PART THEREOF), INCLUDING, BUT NOT LIMITED TO, 
// IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR 
// NON-INFRINGEMENT. GENESYS AND ITS SUPPLIERS SHALL NOT BE LIABLE FOR ANY DAMAGE 
// SUFFERED AS A RESULT OF USING THIS CODE. IN NO EVENT SHALL GENESYS AND ITS 
// SUPPLIERS BE LIABLE FOR ANY DIRECT, INDIRECT, CONSEQUENTIAL, ECONOMIC, 
// INCIDENTAL, OR SPECIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, ANY LOST 
// REVENUES OR PROFITS).
// 
//                                         
// Copyright � 2006 Genesys Telecommunications Laboratories, Inc. All rights 
// reserved.
//===============================================================================

using System;
using System.ComponentModel;
using System.Reflection;
using System.Globalization;
using System.Runtime.Serialization;
using System.Security.Permissions;


namespace Genesyslab.Desktop.ApplicationBlocks.StatConnection
{
	#region StatConnectionException

	/// <summary>
	/// Exception definition used for the toolkit.
	/// </summary>
	[Serializable]
	public class StatConnectionException : Exception
	{
		private string mCode = "gad.error.unknown";
		private object[] mStatistics;
		/// <summary>
		/// Initializes a new instance of the exception class.
		/// </summary>
		public StatConnectionException() : base() { }

		/// <summary>
		/// Initializes a new instance of the exception class with the specified error code and message.
		/// </summary>
		/// <param name="code">string containing a specific code for this exception.</param>
		/// <param name="message">string containing a text message for this exception.</param>
		public StatConnectionException(string code, string message) : base(message)
		{
			mCode = code;
		}

		/// <summary>
		/// Initializes a new instance of the exception class with the specified error code and message, and a reference to the inner exception that is the cause of this exception.
		/// </summary>
		/// <param name="code">string containing a specific code for this exception.</param>
		/// <param name="message">string containing a text message for this exception.</param>
		/// <param name="innerException">The exception that is the cause of the current exception. If the innerException parameter is not a null reference, the current exception is raised in a catch block that handles the inner exception.</param>
		public StatConnectionException(string code, string message, Exception innerException) : base(message, innerException)
		{
			mCode = code;
		}

		/// <summary>
		/// Initializes a new instance of the exception class with the specified error code and message.
		/// </summary>
		/// <param name="statistics">statistic objects that causes the exception.</param>
		/// <param name="message">string containing a text message for this exception.</param>
		public StatConnectionException(object[] statistics, string message) : base(message)
		{
			mStatistics = statistics;
		}

		/// <summary>
		/// Initializes a new instance of the exception class with a reference to the inner exception that is the cause of this exception.
		/// </summary>
		/// <param name="ex">The exception that is the cause of the current exception. If the innerException parameter is not a null reference, the current exception is raised in a catch block that handles the inner exception.</param>
		public StatConnectionException(Exception ex) : base(null, ex)
		{
			if(ex != null && ex is StatConnectionException)
				mCode = ((StatConnectionException)ex).Code;
			else
				mCode = "";
		}

		/// <summary>
		/// Sets the SerializationInfo with information about the exception
		/// </summary>
		/// <param name="info">The SerializationInfo that holds the serialized object data about the exception being thrown.</param>
		/// <param name="context">The StreamingContext that contains contextual information about the source or destination.</param>
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if(info != null)
			{
				info.AddValue("GADCode", Code);
				base.GetObjectData(info, context);
			}
		}

		/// <summary>
		/// Specific code for this exception.
		/// </summary>
		public string Code { get { return mCode; } }

		/// <summary>
		/// Gets the statistics which generate the exception.
		/// </summary>
		public object[] Statistics { get { return mStatistics; } }
	}

	#endregion

}
