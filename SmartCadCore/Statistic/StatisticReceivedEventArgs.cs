﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadCore.Statistic
{
    [Serializable]
    public class StatisticReceivedEventArgs : EventArgs
    {
        private string eventType;
        private string[][] keyVal;

        public string[][] KeyVal
        {
            get
            {
                return keyVal;
            }
            set
            {
                keyVal = value;
            }
        }


        public string EventType
        {
            get
            {
                return eventType;
            }
            set
            {
                eventType = value;
            }
        }

        public StatisticReceivedEventArgs(string evtType, string[][] keyVal)
        {
            this.eventType = evtType;
            this.keyVal = keyVal;
        }

    }
}
