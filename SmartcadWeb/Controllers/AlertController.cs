﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SmartcadWeb.Controllers
{
    [RoutePrefix("Alert")]
    public class AlertController : ApiController
    {
        // GET Alert/AlertData
        [Route("AlertRecords")]
        public List<Alert> Get()
        {


            return new List<Alert>()
            {
                 new Alert { AlertID="Alert 155", Camera="Camera 34", Date="3/23/2018", Time="1:33 pm", Plate="444 ttt", Model="Chevrolet", Year="2010", Colour="blue", Name="Pedro Cruz", Id="3.786.899", Phone="424-762 82 32", Img="car1.jpg", PlateImg="plate1.jpg" }
                ,new Alert { AlertID="Alert 312", Camera="Camera 23", Date="5/21/2018", Time="2:54 pm", Plate="123 abf", Model="Ford", Year="2017", Colour="black", Name="Jaime Rodriguez", Id="4.906.769", Phone="424-456 78 90", Img="car2.jpg", PlateImg="plate2.jpg" }
                ,new Alert { AlertID="Alert 102", Camera="Camera 59", Date="3/2/2018", Time="2:54 pm", Plate="656 frt", Model="Toyota", Year="2000", Colour="red", Name="Jesús Rojas", Id="12.400.775", Phone="424-332 12 23", Img="car3.jpg", PlateImg="plate4.jpg" }
                ,new Alert { AlertID="Alert 417", Camera="Camera 43", Date="1/1/2018", Time="9:28 am", Plate="876 566", Model="Ferrari", Year="2018", Colour="white", Name="John Perez", Id="8.778.009", Phone="424-456 78 90", Img="car4.jpg", PlateImg="plate3.jpg" }

            };
        }
    }

    public class Alert
    {
        public string AlertID { get; set; }
        public string Camera { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string Plate { get; set; }
        public string Model { get; set; }
        public string Year { get; set; }
        public string Colour { get; set; }
        public string Name { get; set; }
        public string Id { get; set; }
        public string Phone { get; set; }
        public string Img { get; set; }
        public string PlateImg { get; set; }

    }


}