﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Text;
using System.Net.Sockets;
using SmartCadCore.ClientData;
using Smartmatic.SmartCad.Service;
using SmartCadCore.Core;
using System.Collections;

namespace SmartcadWeb.Controllers
{

    [RoutePrefix("Alert")]
    public class AlertController : ApiController
    {
        // GET Alert/AlertData
        [Route("AlertRecords")]
        public List<Alert> Get()
        {
            AlarmLprClientData dataExample = new AlarmLprClientData();
            dataExample.StartDate = new DateTime();

            return new List<Alert>()
            {
                 new Alert { AlertID="Alert 155", Camera="Camera 34", Date=dataExample.StartDate.ToString(), Time=dataExample.StartDate.ToString(), Plate="444 ttt", Model="Chevrolet", Year="2010", Colour="blue", Name="Pedro Cruz", Id="3.786.899", Phone="424-762 82 32", Img="car1.jpg", PlateImg="plate1.jpg" }
                ,new Alert { AlertID="Alert 312", Camera="Camera 23", Date="5/21/2018", Time="2:54 pm", Plate="123 abf", Model="Ford", Year="2017", Colour="black", Name="Jaime Rodriguez", Id="4.906.769", Phone="424-456 78 90", Img="car2.jpg", PlateImg="plate2.jpg" }
                ,new Alert { AlertID="Alert 102", Camera="Camera 59", Date="3/2/2018", Time="2:54 pm", Plate="656 frt", Model="Toyota", Year="2000", Colour="red", Name="Jesús Rojas", Id="12.400.775", Phone="424-332 12 23", Img="car3.jpg", PlateImg="plate4.jpg" }
                ,new Alert { AlertID="Alert 417", Camera="Camera 43", Date="1/1/2018", Time="9:28 am", Plate="876 566", Model="Ferrari", Year="2018", Colour="white", Name="John Perez", Id="8.778.009", Phone="424-456 78 90", Img="car4.jpg", PlateImg="plate3.jpg" }

            };

        }

        // GET Alert/AlertRecords2
        [Route("AlertRecords2")]
        public AlertVA Gets()
        {
            //int operCode = (int)33;
            IList AlertsList = new List<object>();
            List<VaAlertCam> AlertLista = new List<VaAlertCam>();
            AlertVA alerta = new AlertVA();
            VaAlertCam alertVa = new VaAlertCam();
            SmartCadConfiguration.Load();
            // SmartCadConfiguration.Load("SmartcadWeb\\Dist\\Configuration\\Configuration.xml");

            string myString = SmartCadConfiguration.ConfigFilename;

         string[] lines = { myString, myString, myString };

            object result = ServerServiceClient.GetServerService();

            if (result != null)
            {
               
                ServerServiceClient.GetInstance().SetApplication(UserApplicationClientData.UserApplications.Cctv.ToString());
                ServerServiceClient.GetInstance().ActivateApplication(SmartCadApplications.SmartCadCCTV, ApplicationUtil.GetMACAddress());
                AlertsList = ServerServiceClient.GetInstance().SearchClientObjectsMaxRows(SmartCadHqls.GetCustomHql(SmartCadHqls.GetVAAlertByCameraOperatorId, 33), true);

                foreach (VAAlertCamClientData AlertList in AlertsList)
                {
                    CameraClientData camera = new CameraClientData();
                    ConnectionTypeClientData conectionType = new ConnectionTypeClientData();
                    conectionType.code = AlertList.Camera.ConnectionType.Code;
                    conectionType.Name = AlertList.Camera.ConnectionType.Name;
                    conectionType.version = AlertList.Camera.ConnectionType.Version;
                    camera.connectionType = conectionType;
                    camera.CustomId = AlertList.Camera.CustomId;
                    camera.frameRate = AlertList.Camera.FrameRate;
                    camera.resolution = AlertList.Camera.Resolution;
                    camera.streamType = AlertList.Camera.StreamType;
                    camera.ip = AlertList.Camera.Ip;
                    camera.ipServer = AlertList.Camera.IpServer;
                    camera.login = AlertList.Camera.Login;
                    camera.name = AlertList.Camera.Name;
                    camera.password = AlertList.Camera.Password;
                    camera.port = AlertList.Camera.Port;
                    StructClientData structClientData = new StructClientData();
                    structClientData.camerasNumber = AlertList.Camera.StructClientData.CamerasNumber;
                    CctvZoneClientData cctvZoneClientData = new CctvZoneClientData();
                    cctvZoneClientData.code = AlertList.Camera.StructClientData.CctvZone.Code;
                    cctvZoneClientData.name = AlertList.Camera.StructClientData.CctvZone.Name;
                    cctvZoneClientData.structNumber = AlertList.Camera.StructClientData.CctvZone.StructNumber;
                    cctvZoneClientData.structs = AlertList.Camera.StructClientData.CctvZone.Structs;
                    cctvZoneClientData.version = AlertList.Camera.StructClientData.CctvZone.Version;
                    structClientData.cctvZone = cctvZoneClientData;
                    structClientData.code = AlertList.Camera.StructClientData.Code;
                    structClientData.devices = AlertList.Camera.StructClientData.Devices;
                    structClientData.isSynchronized = AlertList.Camera.StructClientData.IsSynchronized;
                    structClientData.lat = AlertList.Camera.StructClientData.Lat;
                    structClientData.lon = AlertList.Camera.StructClientData.Lon;
                    structClientData.name = AlertList.Camera.StructClientData.Name;
                    structClientData.state = AlertList.Camera.StructClientData.State;
                    structClientData.street = AlertList.Camera.StructClientData.Street;
                    structClientData.town = AlertList.Camera.StructClientData.Town;
                    StructTypeClientData strucTypeClientData = new StructTypeClientData();
                    strucTypeClientData.code = AlertList.Camera.StructClientData.Type.Code;
                    strucTypeClientData.name = AlertList.Camera.StructClientData.Type.Name;
                    strucTypeClientData.version = AlertList.Camera.StructClientData.Type.Version;
                    structClientData.type = strucTypeClientData;
                    structClientData.version = AlertList.Camera.StructClientData.Version;
                    structClientData.zone = AlertList.Camera.StructClientData.ZoneSecRef;
                    camera.structClientData = structClientData;
                    CameraTypeClientData cameraType = new CameraTypeClientData();
                    cameraType.code = AlertList.Camera.Type.Code;
                    cameraType.Name = AlertList.Camera.Type.Name;
                    cameraType.version = AlertList.Camera.Type.Version;
                    camera.type = cameraType;
                    alertVa.camera = camera;
                    alertVa.alertDate = AlertList.AlertDate;
                    alertVa.code = AlertList.Code;
                    alertVa.externalId = AlertList.ExternalId;
                    //alertVa.rule = AlertList.Rule;
                    VARuleClientData vaRule = new VARuleClientData();
                    vaRule.code = AlertList.Rule.Code;
                    vaRule.name = AlertList.Rule.Name;
                    vaRule.saviCode = AlertList.Rule.SaviCode;
                    vaRule.version = AlertList.Rule.Version;
                    alertVa.rule = vaRule;
                    alertVa.status = AlertList.Status;
                    alertVa.version = AlertList.Version;
                    VaVideoAlert vaVideoAlert = new VaVideoAlert();
                    vaVideoAlert.birthTimeValue = AlertList.VideoAlert.BirthTimeValue;
                    vaVideoAlert.birtTimeMilliseconds = AlertList.VideoAlert.BirtTimeMilliseconds;
                    vaVideoAlert.code = AlertList.VideoAlert.Code;
                    vaVideoAlert.deathTimeMilliseconds = AlertList.VideoAlert.DeathTimeMilliseconds;
                    vaVideoAlert.deathTimeValue = AlertList.VideoAlert.DeathTimeValue;
                    vaVideoAlert.objectId = AlertList.VideoAlert.ObjectId;
                    vaVideoAlert.totalObjectInstances = AlertList.VideoAlert.TotalObjectInstances;
                    vaVideoAlert.version = AlertList.VideoAlert.Version;
                    //List<VAAlertVideoInstanceClientDataApi> vaAlertVideoInstancelist = new List<VAAlertVideoInstanceClientDataApi>();
                    //if (AlertList.VideoAlert.VideoInstances != null) { 
                    //    foreach (VAAlertVideoInstanceClientData vaIntance in AlertList.VideoAlert.VideoInstances)
                    //    {
                    //        VAAlertVideoInstanceClientDataApi vaAlertVideoInstance = new VAAlertVideoInstanceClientDataApi();
                    //        vaAlertVideoInstance.boundingBoxButtom = vaIntance.BoundingBoxButtom;
                    //        vaAlertVideoInstance.boundingBoxLeft = vaIntance.BoundingBoxLeft;
                    //        vaAlertVideoInstance.boundingBoxRight = vaIntance.BoundingBoxRight;
                    //        vaAlertVideoInstance.boundingBoxTop = vaIntance.BoundingBoxTop;
                    //        vaAlertVideoInstance.code = vaIntance.Code;
                    //        vaAlertVideoInstance.footX = vaIntance.FootX;
                    //        vaAlertVideoInstance.footY = vaIntance.FootY;
                    //        vaAlertVideoInstance.objectId = vaIntance.ObjectId;
                    //        vaAlertVideoInstance.timestampMilliseconds = vaIntance.TimestampMilliseconds;
                    //        vaAlertVideoInstance.timestampValue = vaIntance.TimestampValue;
                    //        vaAlertVideoInstance.version = vaIntance.Version;
                    //        vaAlertVideoInstancelist.Add(vaAlertVideoInstance);
                    //    }
                    //}
                    //vaVideoAlert.videoInstances = vaAlertVideoInstancelist;
                    alertVa.VaVideoAlert = vaVideoAlert;
                    AlertLista.Add(alertVa);
                }
                alerta.response = AlertLista;
            }
            
            return alerta;
        }
    }

    public class ClientData
    {
        internal const string DateNHibernateFormatWithSeconds = "yyyyMMdd HH:mm:ss";

        public int code { get; set; }

        public int version { get; set; }
    }
    
    public class VaAlertCam : ClientData
    {
        public CameraClientData camera { get; set; }
        public DateTime alertDate { get; set; }
        public int status { get; set; }
        public int externalId { get; set; }
        public VARuleClientData rule { get; set; }
        public VaVideoAlert VaVideoAlert { get; set; }
    }

    public class CameraClientData : VideoDeviceClientData
    {
        public string resolution { get; set; }
        public string streamType { get; set; }
        public string frameRate { get; set; }
        public CameraTypeClientData type { get; set; }
        public ConnectionTypeClientData connectionType { get; set; }

        public string CustomId { get; set; }
    }

    public class StructClientData : ClientData
    {
        public string name { get; set; }
        public double lat { get; set; }
        public double lon { get; set; }
        public string zone { get; set; }
        public string street { get; set; }
        public string town { get; set; }
        public string state { get; set; }
        public CctvZoneClientData cctvZone { get; set; }
        public StructTypeClientData type { get; set; }
        public int camerasNumber { get; set; }
        public IList devices { get; set; }
        public bool isSynchronized { get; set; }
    }

    public class CctvZoneClientData : ClientData
    {
        public string name { get; set; }
        public int structNumber { get; set; }
        public IList structs { get; set; }
    }

    public class StructTypeClientData : ClientData
    {
        public string name { get; set; }
    }

    public class VideoDeviceClientData : DeviceClientData
    {
        public StructClientData structClientData { get; set; }
    }

    public class DeviceClientData : ClientData
    {
        public string ip { get; set; }
        public string port { get; set; }
        public string ipServer { get; set; }
        public string login { get; set; }
        public string password { get; set; }
        public string name { get; set; }
    }

    public class ConnectionTypeClientData : ClientData
    {
        public string Name { get; set; }
    }

    public class CameraTypeClientData : ClientData
    {
        public string Name { get; set; }
    }

    public class VARuleClientData : ClientData
    {
        public string name { get; set; }
        public string saviCode { get; set; }
    }

        public class VaVideoAlert : ClientData
    {
        public long objectId { get; set; }
        
        public DateTime? birthTimeValue { get; set; }
        
        public int? birtTimeMilliseconds { get; set; }
        
        public DateTime? deathTimeValue { get; set; }
        
        public int? deathTimeMilliseconds { get; set; }
        
        public int totalObjectInstances { get; set; }
        public List<VAAlertVideoInstanceClientDataApi> videoInstances { get; set; }
    }

    public class VAAlertVideoInstanceClientDataApi : ClientData
    {
        public long objectId { get; set; }

        public enum TypeObjectEnum
        {
            ObjectInstanceOnEvent,
            ObjectInstanceOnBestVIew,
            ObjectInstance
        }

        public VaAlertCam alert { get; set; }

        public TypeObjectEnum typeObject { get; set; }

        public DateTime timestampValue { get; set; }

        public int timestampMilliseconds { get; set; }

        public float boundingBoxLeft { get; set; }

        public float boundingBoxTop { get; set; }

        public float boundingBoxRight { get; set; }

        public float boundingBoxButtom { get; set; }

        public float footX { get; set; }

        public float footY { get; set; }

        public virtual TypeObjectEnum TypeObject
        {
            get { return typeObject; }
            set { typeObject = value; }
        }
    }

        public class AlertVA
    {
        public object response { get; set; }
    }

        public class Alert
        {
            public string AlertID { get; set; }
            public string Camera { get; set; }
            public string Date { get; set; }
            public string Time { get; set; }
            public string Plate { get; set; }
            public string Model { get; set; }
            public string Year { get; set; }
            public string Colour { get; set; }
            public string Name { get; set; }
            public string Id { get; set; }
            public string Phone { get; set; }
            public string Img { get; set; }
            public string PlateImg { get; set; }

        }


}
 