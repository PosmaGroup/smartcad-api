﻿var app = angular.module('app', []);


app.factory('Users', function ($http) {
    var factory = {};
    factory.getUsers = function () {
        var urlBase = 'http://localhost:6763/Alert'
        return $http.get(urlBase + '/AlertRecords');
    };

    return factory;
});

app.factory('Users2', function ($http) {
    var factory = {};
    factory.getUsers2 = function () {
        var urlBase = 'http://localhost:6763/Alert'
        return $http.get(urlBase + '/AlertRecords2');
    };

    return factory;
});


app.controller('UsersCtrl', function ($scope, Users, Users2) {
    Users.getUsers().success(function (data) {
        $scope.users = data;
        $scope.selIdx = -1;

        $scope.selUser = function (user, idx, img) {
            $scope.selectedUser = user;
            $scope.selIdx = idx;
        }

        $scope.isSelUser = function (user) {
            return $scope.selectedUser === user;
        }

    });
   
    Users2.getUsers2().success(function (data) {
        $scope.data = data;
    });
});